Imports FAME.Advantage.Common

Public Class StudentSearchDB
    ''Optional Parameter added by Saraswathi Lakshmanan on 29 Sept 2008
    ''Cohort Start Date added as Optional parameter
    Public Function StudentSearchResults(ByVal strStudentID As String, ByVal strLastName As String, ByVal strFirstName As String, ByVal strSSN As String, ByVal strStatus As String, ByVal strEnrollmentID As String, ByVal strProgramID As String, ByVal strCampus As String, ByVal strStudentNumber As String, ByVal strStudentStatus As String, Optional ByVal leadGrpId As String = "", Optional ByVal CohortStartDate As String = "") As DataSet
        '   connect to the database
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'If strProgramID = Guid.Empty.ToString And strCampus = Guid.Empty.ToString Then
        'Check If the Student Is Not Enrolled
        ''Enrollment StatusCode added in Query BY Saraswathi Lakshmanan
        ''on Sept 29 2008
        With sb
            If leadGrpId = "" Then
                .Append(" SELECT Distinct  S1.StudentId,  S1.LastName,S1.FirstName, ")
                .Append(" S1.SSN,S1.StudentStatus as Status,s2.StuEnrollId, ")
                .Append(" s3.PrgVerDescrip  ")
                ''Enrollment Status Code Added
                .Append(",( Select StatusCodeDescrip from syStatusCodes where StatusCodeId=s2.StatusCodeId)EnrollStatus  ")
                .Append(" FROM  arStudent S1,arStuEnrollments s2,arPrgVersions s3,syStatuses s4 where  ")
                .Append(" s1.StudentId = s2.StudentId and s2.PrgVerId = s3.PrgVerId and s1.StudentStatus = s4.StatusId ")
                .Append(" and S2.StatusCodeId is not null ")
            Else
                .Append(" SELECT Distinct  S1.StudentId,  S1.LastName,S1.FirstName, ")
                .Append(" S1.SSN,S1.StudentStatus as Status,s2.StuEnrollId, ")
                .Append(" s3.PrgVerDescrip  ")
                ''Enrollment Status Code Added
                .Append(",( Select StatusCodeDescrip from syStatusCodes where StatusCodeId=s2.StatusCodeId)EnrollStatus  ")

                .Append(" FROM  arStudent S1,arStuEnrollments s2,arPrgVersions s3,syStatuses s4, adLeadByLeadGroups s5 where  ")
                .Append(" s1.StudentId = s2.StudentId and s2.PrgVerId = s3.PrgVerId and s1.StudentStatus = s4.StatusId ")
                .Append(" and S2.StatusCodeId is not null ")
                .Append(" and s2.StuEnrollId=s5.StuEnrollId ")
                .Append(" and s5.LeadGrpId=? ")

                db.AddParameter("@leadGrpId", leadGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'the relationship logic between checkboxes is "OR" instead of "AND". Only the first one is AND
            Dim andOrOrOperator As String = " AND "

            If Not strStudentID = "" Then
                .Append(andOrOrOperator)
                .Append(" StudentID = ? ")
                db.AddParameter("@StudentID", strStudentID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            If Not strStudentNumber = "" Then
                .Append(andOrOrOperator)
                .Append(" StudentNumber like  + '%' + ? + '%'")
                db.AddParameter("@StudentNumber", strStudentNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            If Not strStudentStatus = "" Then
                .Append(andOrOrOperator)
                .Append(" s4.StatusId = ? ")
                db.AddParameter("@StudentStatus", strStudentStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            If Not strLastName = "" Then
                .Append(andOrOrOperator)
                '.Append(" LastName like  + '%' + ? + '%'")
                .Append(" LastName like  + ? + '%'")
                db.AddParameter("@LastName", strLastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            If Not strFirstName = "" Then
                .Append(andOrOrOperator)
                '.Append(" FirstName like  + '%' + ? + '%'")
                .Append(" FirstName like  + ? + '%'")
                db.AddParameter("@FirstName", strFirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            'And strEnrollmentID = ""
            If Not strEnrollmentID = "" Then
                .Append(andOrOrOperator)
                .Append(" EnrollmentId like  + '%' + ? + '%'")
                db.AddParameter("@EnrollmentId", strEnrollmentID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            If Not strSSN = "" Then
                .Append(andOrOrOperator)
                '.Append(" SSN like  + '%' + ? + '%'")
                ' .Append(" SSN like + ? + '%'")
                .Append(" SSN = ? ")
                db.AddParameter("@SSN", strSSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            If Not strStatus = Guid.Empty.ToString Then
                .Append(andOrOrOperator)
                .Append(" s2.StatusCodeId = ? ")
                db.AddParameter("@StatusCodeId", strStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            'check for Program Is Selected
            If Not strProgramID = Guid.Empty.ToString Then
                .Append(andOrOrOperator)
                .Append(" S3.PrgVerId = ? ")
                db.AddParameter("@ProgID", strProgramID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            'If Not CampusId is Empty
            If Not strCampus = Guid.Empty.ToString Then
                .Append(andOrOrOperator)
                .Append(" s2.CampusID = ? ")
                db.AddParameter("@CampusID", strCampus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            ''Added by Saraswathi Lakshmanan on 29 Sept 2008
            ''Cohort StartDate included in the where clause of the Query String
            'If Not Cohort StartDate is Empty
            If Not CohortStartDate = "" Then
                .Append(andOrOrOperator)
                .Append(" s2.CohortStartDate = ? ")
                db.AddParameter("@CohortStartDate", CohortStartDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If



            'If strProgramID = Guid.Empty.ToString Then
            '    .Append(" Union ")
            '    .Append(" Select Distinct StudentId,LastName,FirstName,SSN,StudentStatus,Null,Null ")
            '    ''NUll for EnrollMentStatusCode Added
            '    .Append(" ,Null  ")
            '    .Append(" from arStudent where StudentId not in (select StudentId  from arStuEnrollments) ")

            '    'the relationship logic between checkboxes is "OR" instead of "AND". Only the first one is AND
            '    Dim andOrOrOperator1 As String = " AND "

            '    If Not strStudentID = "" Then
            '        .Append(andOrOrOperator1)
            '        .Append(" StudentID = ? ")
            '        db.AddParameter("@StudentID", strStudentID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '        andOrOrOperator = " AND "
            '    End If

            '    If Not strStudentNumber = "" Then
            '        .Append(andOrOrOperator)
            '        .Append(" StudentNumber like  + '%' + ? + '%'")
            '        db.AddParameter("@StudentNumber", strStudentNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '        andOrOrOperator = " AND "
            '    End If

            '    If Not strStudentStatus = "" Then
            '        .Append(andOrOrOperator)
            '        .Append(" StudentStatus = ? ")
            '        db.AddParameter("@StudentStatus", strStudentStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '        andOrOrOperator = " AND "
            '    End If

            '    If Not strLastName = "" Then
            '        .Append(andOrOrOperator1)
            '        '.Append(" LastName like  + '%' + ? + '%'")
            '        .Append(" LastName like  + ? + '%'")
            '        db.AddParameter("@LastName", strLastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '        andOrOrOperator = " AND "
            '    End If

            '    If Not strFirstName = "" Then
            '        .Append(andOrOrOperator1)
            '        '.Append(" FirstName like  + '%' + ? + '%'")
            '        .Append(" FirstName like  + ? + '%'")
            '        db.AddParameter("@FirstName", strFirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '        andOrOrOperator = " AND "
            '    End If

            '    If Not strSSN = "" Then
            '        .Append(andOrOrOperator1)
            '        .Append(" SSN like  + '%' + ? + '%'")
            '        db.AddParameter("@SSN", strSSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '        andOrOrOperator = " AND "
            '    End If

            '    If Not strStatus = Guid.Empty.ToString Then
            '        .Append(andOrOrOperator1)
            '        .Append(" StudentStatus = ? ")
            '        db.AddParameter("@StudentStatus", strStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '        andOrOrOperator = " AND "
            '    End If



            '    'If Not CampusId is Empty
            '    'If Not strCampus = Guid.Empty.ToString Then
            '    '    .Append(andOrOrOperator)
            '    '    .Append(" CampusID = ? ")
            '    '    db.AddParameter("@CampusID", strCampus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '    'End If
            'End If
            .Append(" ORDER BY S1.LastName,S1.FirstName ")
        End With
        'Execute the query and return Dataset
        Return db.RunParamSQLDataSet(sb.ToString)
        'Else
        '    With sb
        '        .Append(" SELECT  Distinct  ")
        '        .Append(" S1.StudentId, ")
        '        .Append(" S1.FirstName, ")
        '        .Append(" S1.LastName, ")
        '        .Append(" S1.SSN,  ")
        '        .Append(" s3.PrgVerDescrip ")
        '        .Append(" FROM ")
        '        .Append(" arStudent S1,arStuEnrollments S2,arPrgVersions S3,arPrograms S4 ")
        '        .Append(" where S1.StudentID = S2.StudentID and S2.PrgVerID = S3.PrgVerID  ")

        '        'the relationship logic between checkboxes is "OR" instead of "AND". Only the first one is AND
        '        Dim andOrOrOperator As String = " AND "

        '        If Not strStudentID = "" Then
        '            .Append(andOrOrOperator)
        '            .Append(" StudentID = ? ")
        '            db.AddParameter("@StudentID", strStudentID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '            andOrOrOperator = " AND "
        '        End If

        '        '   check if the selection is "All Campus Groups"
        '        If Not strLastName = "" Then
        '            .Append(andOrOrOperator)
        '            .Append(" LastName like  + '%' + ? + '%'")
        '            db.AddParameter("@LastName", strLastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '            andOrOrOperator = " AND "
        '        End If

        '        If Not strFirstName = "" Then
        '            .Append(andOrOrOperator)
        '            .Append(" FirstName like  + '%' + ? + '%'")
        '            db.AddParameter("@FirstName", strFirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '            andOrOrOperator = " AND "
        '        End If

        '        If Not strSSN = "" Then
        '            .Append(andOrOrOperator)
        '            .Append(" SSN like  + '%' + ? + '%'")
        '            db.AddParameter("@SSN", strSSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '            andOrOrOperator = " AND "
        '        End If

        '        If Not strStatus = Guid.Empty.ToString Then
        '            .Append(andOrOrOperator)
        '            .Append(" StudentStatus = ? ")
        '            db.AddParameter("@StudentStatus", strStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '            andOrOrOperator = " AND "
        '        End If

        '        'check for Transaction Amount To
        '        If Not strEnrollmentID = "" Then
        '            .Append(andOrOrOperator)
        '            .Append(" S2.EnrollmentId like  + '%' + ? + '%'")
        '            db.AddParameter("@EnrollmentID", strEnrollmentID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '            andOrOrOperator = " AND "
        '        End If

        '        'check for Program Is Selected
        '        If Not strProgramID = Guid.Empty.ToString Then
        '            .Append(andOrOrOperator)
        '            .Append(" S3.PrgVerId = ? ")
        '            db.AddParameter("@ProgID", strProgramID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '            andOrOrOperator = " AND "
        '        End If

        '        'check if the Campus Is Selected
        '        If Not strCampus = Guid.Empty.ToString Then
        '            .Append(andOrOrOperator)
        '            .Append(" s2.CampusID = ? ")
        '            db.AddParameter("@CampusID", strCampus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '        End If
        '        .Append(" ORDER BY LastName ")
        '    End With
        '    '   Execute the query and return Dataset
        '    Return db.RunParamSQLDataSet(sb.ToString)

        ' End If
    End Function
    'Public Function LeadSearchResults(ByVal strLastName As String, ByVal strFirstName As String, ByVal strSSN As String, ByVal strDOB As String, ByVal strStatus As String, ByVal strPhone As String, ByVal userId As String, ByVal campusId As String) As DataSet
    '    'connect to the database
    '    'connect to the database
    '    Dim db As New DataAccess
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append(" SELECT    ")
    '        .Append(" FirstName, ")
    '        .Append(" MiddleName, ")
    '        .Append(" LastName,LeadID,BirthDate,SSN,LeadStatus,Phone ")
    '        .Append(" FROM ")
    '        .Append(" adLeads S1 ")
    '        .Append(" where ")
    '        .Append("CampusId = ? ")

    '        db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        Dim andOrOrOperator As String = " AND "

    '        'Director of Admissions will get all leads from this campus
    '        If Not (New UserSecurityDB).IsSAOrDirectorOfAdmissions(userId) Then
    '            .Append(andOrOrOperator)
    '            .Append(" AdmissionsRep = ? ")
    '            db.AddParameter("@repid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        '   check if the selection is "All Campus Groups"
    '        If Not strLastName = "" Then
    '            .Append(andOrOrOperator)
    '            .Append(" LastName like  + ? + '%'")
    '            db.AddParameter("@LastName", strLastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        If Not strFirstName = "" Then
    '            .Append(andOrOrOperator)
    '            .Append(" FirstName like  + ? + '%'")
    '            db.AddParameter("@FirstName", strFirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        If Not strSSN = "" Then
    '            .Append(andOrOrOperator)
    '            '.Append(" SSN like  + ? + '%'")
    '            .Append(" SSN = ? ")
    '            db.AddParameter("@SSN", strSSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        If Not strPhone = "" Then
    '            .Append(andOrOrOperator)
    '            .Append(" Phone like  + ? + '%'")
    '            db.AddParameter("@Phone", strPhone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        If Not strDOB = Guid.Empty.ToString Then
    '            .Append(andOrOrOperator)
    '            .Append(" BirthDate = ? ")
    '            db.AddParameter("@DOB", strDOB, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        If Not strStatus = Guid.Empty.ToString Then
    '            .Append(andOrOrOperator)
    '            .Append(" LeadStatus like + ? + '%'")
    '            db.AddParameter("@Status", strStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        .Append(" order by LastName ")
    '    End With

    '    '   Execute the query and return Dataset
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function
    '' Code modified by kamalesh Ahuja on 11 June 2010 to Resolve mantis issue id 18411
    Public Function LeadSearchResults(ByVal strLastName As String, ByVal strFirstName As String, ByVal strSSN As String, ByVal strDOB As String, ByVal strStatus As String, ByVal strPhone As String, ByVal userId As String, ByVal campusId As String, ByVal LeadCampuses As String, Optional ByVal HasEditOtherPerm As Boolean = False, Optional ByVal AdmissionRep As String = "") As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'connect to the database
        'connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            '' Code changes made on 17 August 2010 to resolve mantis issue id 19560
            '.Append(" SELECT    ")
            '.Append(" FirstName, ")
            '.Append(" MiddleName, ")
            '.Append(" LastName,LeadID,BirthDate,SSN,LeadStatus,Phone ")
            '.Append(" FROM ")
            '.Append(" adLeads S1 ")
            '.Append(" where ")

            .Append(" SELECT    ")
            .Append(" S1.FirstName, ")
            .Append(" S1.MiddleName, ")
            .Append(" S1.LastName,S1.LeadID,S1.BirthDate,S1.SSN,S1.LeadStatus,S1.Phone,U1.FullName as AdmissionRep,CA.CampDescrip as Campus, S1.CampusId  ")
            .Append(" FROM ")
            .Append(" adLeads S1, syUsers U1,syCampuses CA ")
            .Append(" ,syStatuses ST ")
            .Append(" where S1.AdmissionsRep=U1.UserId and S1.CampusId=CA.CampusId and ")
            .Append(" ST.StatusId=CA.StatusId and ST.Status='Active' and ")
            '''''''''
            If Not campusId = Guid.Empty.ToString Then
                .Append("S1.CampusId = ? ")
                db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                If LeadCampuses.ToLower = "ownleads" Then
                    .Append("S1.CampusId in (SELECT C.CampusId FROM syCampuses C, syStatuses S WHERE C.StatusId = S.StatusId AND S.Status = 'Active' AND CampusId in (Select CampusId from syCmpGrpCmps where CampGrpId in  (select CampgrpId from syUsersRolesCampGrps where UserId = ? )) ) ")
                    db.AddParameter("@Userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                ElseIf LeadCampuses.ToLower = "permissiblecampuses" Then
                    .Append("S1.CampusId in (SELECT C.CampusId FROM syCampuses C, syStatuses S WHERE C.StatusId = S.StatusId AND S.Status = 'Active' AND CampusId in (Select CampusId from syCmpGrpCmps where CampGrpId in  (select CampgrpId from syUsersRolesCampGrps where UserId = ? )) ) ")
                    db.AddParameter("@Userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    .Append("S1.CampusId in (select CampusId from syCampuses ) ")
                End If
            End If

            Dim andOrOrOperator As String = " AND "

            ''If HasEditOtherPerm = False Then
            'Director of Admissions will get all leads from this campus

            ''' Code change to Resolve Mantis Issue id 19448
            If (Not (New UserSecurityDB).IsSAOrDirectorOfAdmissions(userId) And HasEditOtherPerm = False And Not LeadCampuses.ToLower = "permissiblecampuses" And Not LeadCampuses.ToLower = "allcampuses") Or (Not (New UserSecurityDB).IsSAOrDirectorOfAdmissions(userId) And LeadCampuses.ToLower = "ownleads") Then
                '''''''''
                .Append(andOrOrOperator)
                .Append(" S1.AdmissionsRep = ? ")
                db.AddParameter("@repid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            ''End If

            '   check if the selection is "All Campus Groups"
            If Not strLastName = "" Then
                .Append(andOrOrOperator)
                .Append(" S1.LastName like  + ? + '%'")
                db.AddParameter("@LastName", strLastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Not strFirstName = "" Then
                .Append(andOrOrOperator)
                .Append(" S1.FirstName like  + ? + '%'")
                db.AddParameter("@FirstName", strFirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Not strSSN = "" Then
                .Append(andOrOrOperator)
                '.Append(" SSN like  + ? + '%'")
                .Append(" S1.SSN = ? ")
                db.AddParameter("@SSN", strSSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Not strPhone = "" Then
                .Append(andOrOrOperator)
                .Append(" S1.Phone like  + ? + '%'")
                db.AddParameter("@Phone", strPhone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Not strDOB = Guid.Empty.ToString Then
                .Append(andOrOrOperator)
                .Append(" BirthDate = ? ")
                db.AddParameter("@DOB", strDOB, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Not strStatus = Guid.Empty.ToString Then
                .Append(andOrOrOperator)
                .Append(" S1.LeadStatus like + ? + '%'")
                db.AddParameter("@Status", strStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '' Code changes made on 17 August 2010 to resolve mantis issue id 19560
            If Not AdmissionRep = "" Then
                .Append(andOrOrOperator)
                .Append(" S1.AdmissionsRep like  + ? + '%'")
                db.AddParameter("@AdmissionRep", AdmissionRep, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            ''''''

            .Append(" order by LastName ")
        End With

        '   Execute the query and return Dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetStudentNameByID(ByVal StudentID As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim strLastName As String
        Dim strFirstName As String
        Dim strMiddleName As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'connect to the database

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'build the sql query
        With sb
            'With subqueries
            .Append("SELECT B.LastName,B.FirstName,B.MiddleName ")
            .Append("FROM  arStudent B ")
            .Append("WHERE B.StudentID = ? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@StudentID", StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read()
            'set properties with data from DataReader
            If Not (dr("LastName") Is System.DBNull.Value) Then strLastName = dr("LastName")
            If Not (dr("FirstName") Is System.DBNull.Value) Then strFirstName = dr("FirstName")
            If Not (dr("MiddleName") Is System.DBNull.Value) Then strMiddleName = dr("MiddleName")
        End While

        If Not dr.IsClosed Then dr.Close()
        
        Return strFirstName & " " & strMiddleName & " " & strLastName
    End Function
    Public Function GetStudentNameByTransactionId(ByVal transactionId As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim strLastName As String
        Dim strFirstName As String
        Dim strMiddleName As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'connect to the database

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'build the sql query
        With sb
            'With subqueries
            .Append("SELECT B.LastName,B.FirstName,B.MiddleName ")
            .Append("FROM  arStudent B ")
            .Append("WHERE B.StudentID = (Select StudentId from arStuEnrollments where stuEnrollId =(select StuEnrollId from saTransactions where TransactionId= ? and saTransactions.Voided=0)) ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@TransactionId", transactionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read()
            'set properties with data from DataReader
            If Not (dr("LastName") Is System.DBNull.Value) Then strLastName = dr("LastName")
            If Not (dr("FirstName") Is System.DBNull.Value) Then strFirstName = dr("FirstName")
            If Not (dr("MiddleName") Is System.DBNull.Value) Then strMiddleName = dr("MiddleName")
        End While

        If Not dr.IsClosed Then dr.Close()
        
        Return strFirstName & " " & strMiddleName & " " & strLastName
    End Function
    Public Function GetLeadNameByID(ByVal LeadID As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim strLastName As String
        Dim strFirstName As String
        Dim strMiddleName As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'connect to the database

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'build the sql query
        With sb
            'With subqueries
            .Append("SELECT B.LastName,B.FirstName,B.MiddleName ")
            .Append("FROM  adLeads B ")
            .Append("WHERE B.LeadID = ? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@LeadID", LeadID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read()
            'set properties with data from DataReader
            If Not (dr("LastName") Is System.DBNull.Value) Then strLastName = dr("LastName")
            If Not (dr("FirstName") Is System.DBNull.Value) Then strFirstName = dr("FirstName")
            If Not (dr("MiddleName") Is System.DBNull.Value) Then strMiddleName = dr("MiddleName")
        End While

        If Not dr.IsClosed Then dr.Close()
       
        Return strFirstName & " " & strMiddleName & " " & strLastName
    End Function
    Public Function GetAllStudentsForEmail(ByVal statusCodeId As String, ByVal campusId As String, ByVal prgVerId As String) As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT Distinct ")
            '.Append("       (S.LastName + ', ' + S.FirstName) As StudentName, ")
            .Append("       S.LastName, ")
            .Append("       S.FirstName + (Select Case Len(Coalesce(S.WorkEmail, S.HomeEmail, '')) when 0 then '(*)' else '' end) FirstName, ")
            .Append("       (Select Case Len(Coalesce(S.WorkEmail, S.HomeEmail, '')) when 0 then 0 else 1 end) As HasEmail, ")
            .Append("       Coalesce(S.WorkEmail, S.HomeEmail, '') Email ")
            .Append("FROM   arStudent S, arStuEnrollments SE, syStatusCodes SC, sySysStatus SS  ")
            '.Append("WHERE  (WorkEmail is not null OR HomeEmail Is not null) ")
            .Append("WHERE  S.StudentId = SE.StudentId ")
            .Append("AND	SE.StatusCodeId=SC.StatusCodeId ")
            .Append("AND	SC.SysStatusId=SS.SysStatusId ")

            '   check if the selection is "All Statuses"
            If Not statusCodeId = Guid.Empty.ToString Then
                .Append("AND    SC.StatusCodeId=? ")
                db.AddParameter("@StatusCodeId", statusCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   check if the selection is "All Campuses"
            If Not campusId = Guid.Empty.ToString Then
                .Append("AND    SE.CampusId  = ? ")
                db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   check if the selection is "All Programs" (Enrollments)
            If Not prgVerId = Guid.Empty.ToString Then
                .Append("AND    S.StudentId=SE.StudentId ")
                .Append("AND    SE.PrgVerId = ? ")
                db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   order by student name
            .Append("ORDER BY LastName, FirstName ")

        End With

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        '   add a column with the concatenation of last and first names
        ds.Tables(0).Columns.Add("Name", GetType(String), "LastName + ' ' + FirstName")

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    'Public Function GetAllEnrollmentsUsedByStudents() As DataSet

    '    '   connect to the database
    '    Dim db As New DataAccess
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

    '    '   build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append("SELECT   Distinct ")
    '        .Append("         PV.PrgVerId, PV.PrgVerDescrip ")
    '        .Append("FROM     arStudent S, arStuEnrollments SE, arPrgVersions PV,  syStatuses ST ")
    '        .Append("WHERE    S.StudentId = SE.StudentId")
    '        .Append(" AND     SE.PrgVerId = PV.PrgVerId ")
    '        .Append(" AND     PV.StatusId = ST.StatusId ")
    '        .Append(" AND     ST.Status = 'Active' ")
    '        .Append("ORDER BY PV.PrgVerDescrip ")
    '    End With

    '    '   add parameters values to the query

    '    '   return dataset
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function

    Public Function GetAllEnrollmentsUsedByStudents(Optional ByVal campusId As String = "") As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   Distinct ")
            .Append("         PV.PrgVerId,  ")
            '              .Append("         PV.PrgVerId, PV.PrgVerDescrip ")
            ''Modified by saraswathi lakshmanan on may 20 2009
            ''the shift description is added to the program version
            .Append(" case when (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=PV.ProgId) is null  then ")
            .Append(" PV.PrgVerDescrip ")
            .Append(" else ")
            .Append(" PV.PrgVerDescrip + ' (' + (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=PV.ProgId) + ')'  ")
            .Append(" end as PrgVerDescrip  ")

            .Append("FROM     arStudent S, arStuEnrollments SE, arPrgVersions PV,  syStatuses ST ")
            .Append("WHERE    S.StudentId = SE.StudentId")
            .Append(" AND     SE.PrgVerId = PV.PrgVerId ")
            .Append(" AND     PV.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")

            If campusId <> "" Then
                .Append("AND (pv.CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId = ? ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR pv.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If

            'New Code Added By Vijay Ramteke On May 28, 2010
            '.Append("ORDER BY PV.PrgVerDescrip ")
            .Append("ORDER BY PrgVerDescrip ")
            'New Code Added By Vijay Ramteke On May 28, 2010
        End With

        '   add parameters values to the query
        If campusId <> "" Then
            db.AddParameter("cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function GetAllLeadDOB() As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT  Distinct BirthDate from adLeads where BirthDate is not null ")
            .Append("ORDER BY BirthDate ")
        End With

        '   add parameters values to the query
        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    'Public Function GetAllGroupsUsedByStudents() As DataSet

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    Dim db As New DataAccess
    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append("SELECT     Distinct ")
    '        .Append("           SG.SGroupId, SG.Descrip ")
    '        .Append("FROM       syStudentSGroupsRel SSG, sySGroups SG ")
    '        .Append("WHERE      SSG.SGroupId=SG.SGroupId ")
    '        .Append("ORDER BY   SG.Descrip")
    '    End With

    '    '   Execute the query
    '    Dim ds As DataSet = db.RunSQLDataSet(sb.ToString)

    '    'Close Connection
    '    db.CloseConnection()

    '    'Return Dataset
    '    Return ds
    'End Function
    'Public Function GetAllStudentEmailsByStudentGroup(ByVal groupsIdList(,) As String) As DataSet
    '    'if the groups list is empty ... return nothing
    '    If groupsIdList.GetLength(1) = 0 Then Return Nothing

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    Dim db As New DataAccess
    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder

    '    'build a string with all roleIds
    '    For i As Integer = 0 To groupsIdList.GetLength(1) - 1
    '        If i > 0 Then sb.Append(",")
    '        sb.Append("'")
    '        sb.Append(groupsIdList(0, i))
    '        sb.Append("'")
    '    Next
    '    Dim groupsList As String = sb.ToString
    '    'build the sql query
    '    sb = New StringBuilder
    '    With sb
    '        .Append("SELECT     Distinct ")
    '        .Append("           (S.FirstName + ' ' + S.Lastname) Name, ")
    '        .Append("           Coalesce(S.WorkEmail, S.HomeEmail) as Email ")
    '        .Append("FROM       arStudent S, syStudentSGroupsRel SG ")
    '        .Append("WHERE      S.StudentId=SG.StudentId ")
    '        .Append("AND        SG.SGroupId IN (" + groupsList + ") ")
    '        .Append("ORDER BY   Email")
    '    End With
    '    'execute the query
    '    Return db.RunSQLDataSet(sb.ToString)
    'End Function
    Public Function GetStudentSearchDS(ByVal lastName As String, ByVal firstName As String, ByVal ssn As String, ByVal enrollment As String, ByVal statusId As String, ByVal prgVerId As String, ByVal campusId As String) As DataSet

        '   create datasetb
        Dim ds As New DataSet

        '   build select command
        Dim sc As New OleDbCommand
        Dim sc1 As New OleDbCommand

        '   build query to select students
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder
        Dim sb2 As New StringBuilder
        Dim sb3 As New StringBuilder
        Dim sb4 As New StringBuilder
        Dim sb5 As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT DISTINCT ")
            .Append("       S.StudentId, ")
            .Append("       S.LastName, ")
            .Append("       S.FirstName, ")
            .Append("       S.SSN, ")
            .Append(" (select Top 1  t3.PrgVerId from arStudent t2,arStuEnrollments t3 where t2.StudentId = S.StudentId and t2.StudentId=t3.StudentId) as PrgVerId, ")
            .Append(" (select Top 1  t1.PrgVerDescrip from arPrgVersions t1,arStudent t2,arStuEnrollments t3 where t2.StudentId = S.StudentId and t2.StudentId=t3.StudentId and t3.PrgVerId=t1.PrgVerId) as PrgVerName, ")
            'get the current term for the student
            .Append("       (SELECT TOP 1 ")
            .Append("                T1.TermId ")
            .Append("       FROM arStudent S1, arStuEnrollments SE1, arResults R1, arClassSections CS1, arTerm T1")
            .Append("       WHERE ")
            .Append("               S1.StudentId = SE1.StudentId ")
            .Append("       AND		SE1.StuEnrollId=R1.StuEnrollId ")
            .Append("       AND		R1.TestId=CS1.ClsSectionId ")
            .Append("       AND		CS1.TermId=T1.TermId ")
            .Append("       AND		S1.StudentId=S.StudentId ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T1.StatusId in (Select StatusID from systatuses where status='Active') ")

            .Append("       ORDER BY T1.StartDate desc) As CurrentTerm, ")
            .Append("       S.StudentStatus as Status ")
            .Append("FROM   arStudent S , arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")

            ' Only the first relationship is AND
            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb1.Append(andOrOrOperator)
                sb1.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
        End With

        'Build select command
        sc.CommandText = sb.ToString + sb1.ToString
        sc.Connection = New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))

        'Create adapter to handle Students table
        Dim da As New OleDbDataAdapter(sc)
        da.SelectCommand.CommandTimeout = 600
        'Fill Students table
        da.Fill(ds, "Students")

        'Build query to select students enrollments
        sb = New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       SE.StuEnrollId, ")
            .Append("       SE.StudentId, ")
            .Append("       SE.ExpStartDate AS StartDate, ")
            .Append("       SE.PrgVerId ")
            .Append("FROM   arStudent S, arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
        End With

        'Modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString + " ORDER BY SE.StudentId, StartDate desc "

        'Fill StudentEnrollments table
        da.Fill(ds, "StudentEnrollments")

        '   build query to select enrollments for selected students
        sb = New StringBuilder
        With sb
            .Append("SELECT DISTINCT")
            .Append("       SE.PrgVerId, ")
            .Append("       (Select PrgVerDescrip from arPrgVersions where PrgVerId=SE.PrgVerId) As Enrollment ")
            .Append("FROM   arStudent S, arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")

        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString

        '   fill Enrollments table
        da.Fill(ds, "Enrollments")

        '   build query to select StudentTerms
        sb = New StringBuilder
        With sb
            .Append("Select DISTINCT ")
            .Append("		S.StudentId, ")
            .Append("       T.TermId, ")
            '.Append("       S.FirstName, ")
            '.Append("       S.LastName, ")
            '.Append("       T.Termdescrip, ")
            .Append("       T.StartDate, ")
            .Append("       SE.StuEnrollId  ")
            .Append("FROM arStudent S, arStuEnrollments SE, arResults R, arClassSections CS, arClassSectionTerms CST, arTerm T, ")
            .Append("syCmpGrpCmps t1, syCampuses t2 ")
            .Append("WHERE ")
            .Append("		S.StudentId=SE.StudentId ")
            .Append("AND	SE.StuEnrollId=R.StuEnrollId ")
            .Append("AND	R.TestId=CS.ClsSectionId ")
            .Append("AND    CST.ClsSectionId=CS.ClsSectionId ")
            .Append("AND	CST.TermId=T.TermId ")
            .Append("AND    t1.CampusId = t2.CampusId ")
            .Append("AND    t1.CampGrpId=T.CampGrpId")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")

            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With

        '   check if the selection is "Campus Groups" = ""
        If Not campusId = Guid.Empty.ToString Then
            Dim andOrOrOperator As String = " AND "

            sb1.Append(andOrOrOperator)
            sb1.Append(" T1.CampusId= ? ")
            sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
        End If


        sb2 = New StringBuilder
        With sb2
            .Append(" Select DISTINCT ")
            .Append("		S.StudentId, ")
            .Append("       T.TermId, ")
            '.Append("       S.FirstName, ")
            '.Append("       S.LastName, ")
            '.Append("       T.Termdescrip, ")
            .Append("       T.StartDate, ")
            .Append("       SE.StuEnrollId  ")
            .Append(" FROM arStudent S, arStuEnrollments SE, arTransferGrades R, arTerm T, ")
            .Append("syCmpGrpCmps t1, syCampuses t2")
            .Append(" WHERE 		S.StudentId=SE.StudentId AND ")
            .Append(" SE.StuEnrollId = R.StuEnrollId AND ")
            .Append(" t1.CampusId = t2.CampusId AND ")
            .Append(" t1.CampGrpId=T.CampGrpId AND ")
            .Append(" R.TermId=T.TermId ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")

            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb2.Append(andOrOrOperator)
                sb2.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb2.Append(andOrOrOperator)
                sb2.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With

        sb3 = New StringBuilder
        With sb3
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" arStuEnrollments SE, ")
            .Append(" arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE S.StudentId = SE.StudentId ")
            .Append(" and SE.PrgVerId=PV.PrgVerId and PV.IsContinuingEd=1 and T.StartDate >= SE.ExpStartDate ")
            ' .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            ''Added by Saraswathi lakshmanan on March 5 2010
            ''18574: QA: Issue with the terms in the Term dropdown on the Student Search pop ups. 
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")

            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb3.Append(andOrOrOperator)
                sb3.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb3.Append(andOrOrOperator)
                sb3.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            ''Added by Saraswathi Lakshmanan on Feb 2 2011 to fix rally case DE4967 Name: QA: Terms in the student search pop up pages shows terms that does not belong to the student's program. 
            .Append("  AND (T.ProgID=PV.ProgId OR T.progId IS NULL) ")
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With



        sb4 = New StringBuilder
        With sb4
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" arStuEnrollments SE, ")
            .Append(" arPrgVersions PV ,arTerm T ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE S.StudentId = SE.StudentId ")
            .Append(" and SE.PrgVerId=PV.PrgVerId and T.StartDate = SE.ExpStartDate ")
            '  .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            ''Added by Saraswathi lakshmanan on March 5 2010
            ''18574: QA: Issue with the terms in the Term dropdown on the Student Search pop ups. 
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")

            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb4.Append(andOrOrOperator)
                sb4.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb4.Append(andOrOrOperator)
                sb4.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            ''Added by Saraswathi Lakshmanan on Feb 2 2011 to fix rally case DE4967 Name: QA: Terms in the student search pop up pages shows terms that does not belong to the student's program. 
            .Append("  AND (T.ProgID=PV.ProgId OR T.progId IS NULL) ")
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With

        sb5 = New StringBuilder
        With sb5
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" arStuEnrollments SE,  arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2   WHERE S.StudentId = SE.StudentId  and SE.PrgVerId=PV.PrgVerId ")
            .Append(" and (PV.ProgId=T.ProgId or T.ProgId is null) AND T.StartDate <=SE.ExpStartDate and SE.ExpStartDate <= T.EndDate ")
            '  .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            ''Added by Saraswathi lakshmanan on March 5 2010
            ''18574: QA: Issue with the terms in the Term dropdown on the Student Search pop ups. 
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")

            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb5.Append(andOrOrOperator)
                sb5.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb5.Append(andOrOrOperator)
                sb5.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            ''Added by Saraswathi Lakshmanan on Feb 2 2011 to fix rally case DE4967 Name: QA: Terms in the student search pop up pages shows terms that does not belong to the student's program. 
            .Append("  AND (T.ProgID=PV.ProgId OR T.progId IS NULL) ")
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With
        '   modify select command
        'da.SelectCommand.CommandText = sb.ToString
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString + " Union " + sb2.ToString + " Union " + sb3.ToString + " Union " + sb4.ToString + " Union " + sb5.ToString + " ORDER BY S.StudentId, T.TermId, T.StartDate desc "
        'da.SelectCommand.CommandText = sb2.ToString + sb1.ToString + " ORDER BY S.StudentId, T.TermId, T.StartDate desc "

        '   fill StudentTerms table
        da.Fill(ds, "StudentTerms")

        '   build query to select StudentTerms
        sc.Parameters.Clear()
        sc.CommandText = ""

        sb = New StringBuilder
        With sb

            .Append("SELECT  T.TermId, ")
            .Append("         T.TermDescrip, ")
            .Append("         T.StartDate ")
            .Append(" FROM     arTerm T  ,syCmpGrpCmps t1, syCampuses t2 ")
            .Append(" where t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            '.Append("ORDER BY T.StartDate desc ")

            If Not campusId = Guid.Empty.ToString Then
                sb.Append(" and t1.campusid = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
        End With


        'Create adapter to handle Students table
        'Dim da As New OleDbDataAdapter(sc1)

        'sc.CommandText = sb.ToString + " ORDER BY T.StartDate desc "
        'sc.Connection = New OleDbConnection(SingletonAppSettings.AppSettings("ConString"))

        ''Create adapter to handle Students table
        'Dim da As New OleDbDataAdapter(sc)

        ''Fill Students table
        'da.Fill(ds, "Terms")

        da.SelectCommand.CommandText = sb.ToString + " ORDER BY T.StartDate desc "

        ' ''   fill Terms table
        da.Fill(ds, "Terms")

        '   create primary and foreign key constraints

        '   set primary key for Students table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("Students").Columns("StudentId")
        ds.Tables("Students").PrimaryKey = pk0

        '   set primary key for Enrollments table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("Enrollments").Columns("PrgVerId")
        ds.Tables("Enrollments").PrimaryKey = pk1

        '   set primary key for Terms table
        Dim pk2(0) As DataColumn
        pk2(0) = ds.Tables("Terms").Columns("TermId")
        ds.Tables("Terms").PrimaryKey = pk2

        '   set foreign key column in StudentEnrollments
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("StudentEnrollments").Columns("StudentId")

        '   set foreign key column in StudentEnrollments
        Dim fk1(0) As DataColumn
        fk1(0) = ds.Tables("StudentEnrollments").Columns("PrgVerId")

        '   set foreign key column in Students
        Dim fk2(0) As DataColumn
        fk2(0) = ds.Tables("StudentTerms").Columns("StudentId")

        '   set foreign key column in Students
        Dim fk3(0) As DataColumn
        fk3(0) = ds.Tables("StudentTerms").Columns("TermId")

        '   set foreign keys in GradeBookResults table
        ds.Relations.Add("StudentsStudentEnrollments", pk0, fk0)
        ds.Relations.Add("EnrollmentsStudentEnrollments", pk1, fk1)
        ds.Relations.Add("StudentStudentTerms", pk0, fk2, False)
        ds.Relations.Add("TermsStudentTerms", pk2, fk3)

        '   return dataset
        Return ds

    End Function
    Public Function GetStudentSearchDSWithStudentNumber(ByVal lastName As String, ByVal firstName As String, ByVal ssn As String, ByVal enrollment As String, ByVal statusId As String, ByVal prgVerId As String, ByVal campusId As String, ByVal studentNumber As String) As DataSet

        '   create dataset
        Dim ds As New DataSet

        '   build select command
        Dim sc As New OleDbCommand
        Dim sc1 As New OleDbCommand

        '   build query to select students
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder
        Dim sb2 As New StringBuilder
        Dim sb3 As New StringBuilder
        Dim sb4 As New StringBuilder
        Dim sb5 As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT DISTINCT ")
            .Append("       S.StudentId, ")
            .Append("       S.LastName, ")
            .Append("       S.FirstName, ")
            .Append("       S.SSN, ")
            .Append(" (select Top 1  t3.PrgVerId from arStudent t2,arStuEnrollments t3 where t2.StudentId = S.StudentId and t2.StudentId=t3.StudentId) as PrgVerId, ")
            .Append(" (select Top 1  t1.PrgVerDescrip from arPrgVersions t1,arStudent t2,arStuEnrollments t3 where t2.StudentId = S.StudentId and t2.StudentId=t3.StudentId and t3.PrgVerId=t1.PrgVerId) as PrgVerName, ")
            'get the current term for the student
            .Append("       (SELECT TOP 1 ")
            .Append("                T1.TermId ")
            .Append("       FROM arStudent S1, arStuEnrollments SE1, arResults R1, arClassSections CS1, arTerm T1")
            .Append("       WHERE ")
            .Append("               S1.StudentId = SE1.StudentId ")
            .Append("       AND		SE1.StuEnrollId=R1.StuEnrollId ")
            .Append("       AND		R1.TestId=CS1.ClsSectionId ")
            .Append("       AND		CS1.TermId=T1.TermId ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T1.StatusId in (Select StatusID from systatuses where status='Active') ")

            .Append("       AND		S1.StudentId=S.StudentId ")
            .Append("       ORDER BY T1.StartDate desc) As CurrentTerm, ")
            .Append("       S.StudentStatus as Status ")
            .Append("FROM   arStudent S , arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")

            ' Only the first relationship is AND
            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb1.Append(andOrOrOperator)
                sb1.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If
            'check for studentid
            If Not studentNumber = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" S.studentNumber like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("studentNumber", studentNumber))
                andOrOrOperator = " AND "
            End If
            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If


        End With

        'Build select command
        sc.CommandText = sb.ToString + sb1.ToString
        sc.Connection = New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))

        'Create adapter to handle Students table
        Dim da As New OleDbDataAdapter(sc)
        da.SelectCommand.CommandTimeout = 600
        'Fill Students table
        da.Fill(ds, "Students")

        'Build query to select students enrollments
        sb = New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       SE.StuEnrollId, ")
            .Append("       SE.StudentId, ")
            .Append("       SE.ExpStartDate AS StartDate, ")
            .Append("       SE.PrgVerId ")
            .Append("FROM   arStudent S, arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
        End With

        'Modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString + " ORDER BY SE.StudentId, StartDate desc "

        'Fill StudentEnrollments table
        da.Fill(ds, "StudentEnrollments")

        '   build query to select enrollments for selected students
        sb = New StringBuilder
        With sb
            .Append("SELECT DISTINCT")
            .Append("       SE.PrgVerId, ")
            .Append("       (Select PrgVerDescrip from arPrgVersions where PrgVerId=SE.PrgVerId) As Enrollment ")
            .Append("FROM   arStudent S, arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")

        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString

        '   fill Enrollments table
        da.Fill(ds, "Enrollments")

        '   build query to select StudentTerms
        sb = New StringBuilder
        With sb
            .Append("Select DISTINCT ")
            .Append("		S.StudentId, ")
            .Append("       T.TermId, ")
            '.Append("       S.FirstName, ")
            '.Append("       S.LastName, ")
            '.Append("       T.Termdescrip, ")
            .Append("       T.StartDate, ")
            .Append("       SE.StuEnrollId  ")
            .Append("FROM arStudent S, arStuEnrollments SE, arResults R, arClassSections CS, arClassSectionTerms CST, arTerm T, ")
            .Append("syCmpGrpCmps t1, syCampuses t2 ")
            .Append("WHERE ")
            .Append("		S.StudentId=SE.StudentId ")
            .Append("AND	SE.StuEnrollId=R.StuEnrollId ")
            .Append("AND	R.TestId=CS.ClsSectionId ")
            .Append("AND    CST.ClsSectionId=CS.ClsSectionId ")
            .Append("AND	CST.TermId=T.TermId ")
            .Append("AND    t1.CampusId = t2.CampusId ")
            .Append("AND    t1.CampGrpId=T.CampGrpId")


            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With

        '   check if the selection is "Campus Groups" = ""
        If Not campusId = Guid.Empty.ToString Then
            Dim andOrOrOperator As String = " AND "

            sb1.Append(andOrOrOperator)
            sb1.Append(" T1.CampusId= ? ")
            sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
        End If

        sb2 = New StringBuilder
        With sb2
            .Append(" Select DISTINCT ")
            .Append("		S.StudentId, ")
            .Append("       T.TermId, ")
            '.Append("       S.FirstName, ")
            '.Append("       S.LastName, ")
            '.Append("       T.Termdescrip, ")
            .Append("       T.StartDate, ")
            .Append("       SE.StuEnrollId  ")
            .Append(" FROM arStudent S, arStuEnrollments SE, arTransferGrades R, arTerm T, ")
            .Append("syCmpGrpCmps t1, syCampuses t2")
            .Append(" WHERE 		S.StudentId=SE.StudentId AND	SE.StuEnrollId=R.StuEnrollId AND ")
            .Append(" t1.CampusId = t2.CampusId AND ")
            .Append(" t1.CampGrpId=T.CampGrpId AND ")

            .Append(" R.TermId=T.TermId ")

            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb2.Append(andOrOrOperator)
                sb2.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If
            'studentid
            If Not studentNumber = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" S.studentNumber like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("studentNumber", studentNumber))
                andOrOrOperator = " AND "
            End If
            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb2.Append(andOrOrOperator)
                sb2.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With

        sb3 = New StringBuilder
        With sb3
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" arStuEnrollments SE, ")
            .Append(" arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE S.StudentId = SE.StudentId ")
            .Append(" and SE.PrgVerId=PV.PrgVerId and PV.IsContinuingEd=1 and T.StartDate >= SE.ExpStartDate ")
            ' .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            ''Added by Saraswathi lakshmanan on March 5 2010
            ''18574: QA: Issue with the terms in the Term dropdown on the Student Search pop ups. 
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")

            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")

            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb3.Append(andOrOrOperator)
                sb3.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If
            'check for studentid
            If Not studentNumber = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" S.studentNumber like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("studentNumber", studentNumber))
                andOrOrOperator = " AND "
            End If
            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb3.Append(andOrOrOperator)
                sb3.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With



        sb4 = New StringBuilder
        With sb4
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" arStuEnrollments SE, ")
            .Append(" arPrgVersions PV ,arTerm T ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE S.StudentId = SE.StudentId ")
            .Append(" and SE.PrgVerId=PV.PrgVerId and T.StartDate = SE.ExpStartDate ")
            '.Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            ''Added by Saraswathi lakshmanan on March 5 2010
            ''18574: QA: Issue with the terms in the Term dropdown on the Student Search pop ups. 
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")

            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")

            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb4.Append(andOrOrOperator)
                sb4.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If
            'check for studentId
            If Not studentNumber = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" S.studentNumber like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("studentNumber", studentNumber))
                andOrOrOperator = " AND "
            End If
            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb4.Append(andOrOrOperator)
                sb4.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With

        sb5 = New StringBuilder
        With sb5
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" arStuEnrollments SE,  arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2   WHERE S.StudentId = SE.StudentId  and SE.PrgVerId=PV.PrgVerId ")
            .Append(" and (PV.ProgId=T.ProgId or T.ProgId is null) AND T.StartDate <=SE.ExpStartDate and SE.ExpStartDate <= T.EndDate ")
            ''Added by Vijay Ramteke on Feb 02, 2010
            ''.Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            ''Added by Saraswathi lakshmanan on March 5 2010
            ''18574: QA: Issue with the terms in the Term dropdown on the Student Search pop ups. 
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            ''Added by Vijay Ramteke on Feb 02, 2010
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")

            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb5.Append(andOrOrOperator)
                sb5.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If
            'check for studentId
            If Not studentNumber = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" S.studentNumber like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("studentNumber", studentNumber))
                andOrOrOperator = " AND "
            End If
            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb5.Append(andOrOrOperator)
                sb5.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))

            End If
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With
        '   modify select command
        'da.SelectCommand.CommandText = sb.ToString
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString + " Union " + sb2.ToString + " Union " + sb3.ToString + " Union " + sb4.ToString + " Union " + sb5.ToString + " ORDER BY S.StudentId, T.TermId, T.StartDate desc "
        'da.SelectCommand.CommandText = sb2.ToString + sb1.ToString + " ORDER BY S.StudentId, T.TermId, T.StartDate desc "

        '   fill StudentTerms table
        da.Fill(ds, "StudentTerms")

        '   build query to select StudentTerms
        sc.Parameters.Clear()
        sc.CommandText = ""

        sb = New StringBuilder
        With sb

            .Append("SELECT  T.TermId, ")
            .Append("         T.TermDescrip, ")
            .Append("         T.StartDate ")
            .Append(" FROM     arTerm T  ,syCmpGrpCmps t1, syCampuses t2 ")
            .Append(" where t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            '.Append("ORDER BY T.StartDate desc ")

            If Not campusId = Guid.Empty.ToString Then
                sb.Append(" and t1.campusid = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
        End With


        'Create adapter to handle Students table
        'Dim da As New OleDbDataAdapter(sc1)

        'sc.CommandText = sb.ToString + " ORDER BY T.StartDate desc "
        'sc.Connection = New OleDbConnection(SingletonAppSettings.AppSettings("ConString"))

        ''Create adapter to handle Students table
        'Dim da As New OleDbDataAdapter(sc)

        ''Fill Students table
        'da.Fill(ds, "Terms")

        da.SelectCommand.CommandText = sb.ToString + " ORDER BY T.StartDate desc "
        da.SelectCommand.CommandTimeout = 600
        ' ''   fill Terms table
        da.Fill(ds, "Terms")

        '   create primary and foreign key constraints

        '   set primary key for Students table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("Students").Columns("StudentId")
        ds.Tables("Students").PrimaryKey = pk0

        '   set primary key for Enrollments table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("Enrollments").Columns("PrgVerId")
        ds.Tables("Enrollments").PrimaryKey = pk1

        '   set primary key for Terms table
        Dim pk2(0) As DataColumn
        pk2(0) = ds.Tables("Terms").Columns("TermId")
        ds.Tables("Terms").PrimaryKey = pk2

        '   set foreign key column in StudentEnrollments
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("StudentEnrollments").Columns("StudentId")

        '   set foreign key column in StudentEnrollments
        Dim fk1(0) As DataColumn
        fk1(0) = ds.Tables("StudentEnrollments").Columns("PrgVerId")

        '   set foreign key column in Students
        Dim fk2(0) As DataColumn
        fk2(0) = ds.Tables("StudentTerms").Columns("StudentId")

        '   set foreign key column in Students
        Dim fk3(0) As DataColumn
        fk3(0) = ds.Tables("StudentTerms").Columns("TermId")

        '   set foreign keys in GradeBookResults table
        ds.Relations.Add("StudentsStudentEnrollments", pk0, fk0)
        ds.Relations.Add("EnrollmentsStudentEnrollments", pk1, fk1)
        ds.Relations.Add("StudentStudentTerms", pk0, fk2, False)
        ds.Relations.Add("TermsStudentTerms", pk2, fk3)

        '   return dataset
        Return ds

    End Function
    Public Function GetQuickStudentContactInfoDS(ByVal search As String, ByVal campusId As String) As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build query to select students
        Dim sb As New StringBuilder

        'With sb
        '    .Append("SELECT DISTINCT ")
        '    .Append("       S.StudentId, ")
        '    .Append("       S.LastName + ' ' + S.FirstName Name, ")
        '    .Append("       Coalesce(S.Address1, '') Address1, ")
        '    .Append("       Coalesce(S.Address2, '') Address2, ")
        '    .Append("       Coalesce(S.City, '') City, ")
        '    .Append("       Coalesce((Select StateDescrip from syStates where StateId=S.State), '') State, ")
        '    '.Append("       Coalesce(S.State, '') State, ")
        '    .Append("       Coalesce(S.Zip, '') Zip, ")
        '    .Append("       S.SSN, ")
        '    .Append("       S.EnrollmentId, ")
        '    .Append("       S.HomePhone + ' ' + (Select PhoneTypeDescrip from syPhoneType where PhoneTypeId=S.PhoneType) Phones, ")
        '    .Append("       S.Email ")
        '    .Append("FROM   arStudent S ")
        '    .Append("WHERE  ")
        '    If IsAlpha(search) Then
        '        .Append("       S.LastName like '%" + search.Trim + "%' ")
        '        .Append("OR     S.FirstName like '%" + search.Trim + "%' ")
        '    Else
        '        .Append("       S." + Ctype(SingletonAppSettings.AppSettings("StudentIdentifier"), String).ToString + " like '%" + search.Trim + "%' ")
        '    End If

        'End With
        With sb
            .Append("SELECT Distinct ")
            .Append("    S.StudentId, ")
            .Append("    S.LastName + ' ' + S.FirstName Name, ")
            .Append("    Coalesce(SA.Address1, ' ') Address1, ")
            .Append("    Coalesce(SA.Address2, ' ') Address2, ")
            .Append("    Coalesce(SA.City, ' ') City, ")
            .Append("    (Case SA.ForeignZip when 0 then ")
            .Append("                            Coalesce((Select StateDescrip from syStates where StateId=SA.StateId), ' ') ")
            .Append("            			else ")
            .Append("                            Coalesce(SA.OtherState, ' ') ")
            .Append("           			end) State, ")
            .Append("    Coalesce(SA.Zip, ' ') Zip, ")
            .Append("    Coalesce((Select CountryDescrip from adCountries where CountryId=SA.CountryId), ' ') Country, ")
            '.Append("    Coalesce(Case Len(S.SSN) When 9 then '***-**-' + SUBSTRING(SSN,6,4) else ' ' end, ' ') SSN, ")

            'include SSN or student identifier
            If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
                .Append("   (Select Coalesce(Case Len(S.SSN) When 9 then '***-**-' + SUBSTRING(SSN,6,4) else ' ' end, ' ') ")
            ElseIf CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToLower = "studentid" Then
                .Append("       (Select Coalesce(StudentNumber, ' ') ")
            Else
                .Append("   (Select Coalesce(" + CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToString + ", ' ') ")
            End If
            .Append("       from arStudent S ")
            .Append("       where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId) ")
            .Append("       ) As StudentIdentifier, ")

            .Append("    Coalesce(SE.EnrollmentId, ' ') EnrollmentId, ")
            .Append("    ((Case SP.ForeignPhone when 0 then ")
            .Append("                            '(' + SUBSTRING (Phone,1,3) + ')-' + SUBSTRING(Phone,4,3) + '-' + SUBSTRING(Phone, 7,4) ")
            .Append("                       else ")
            .Append("                             Phone ")
            .Append("                       end) + ' ' + ")
            .Append("                       (Select PhoneTypeDescrip from syPhoneType where PhoneTypeId=SP.PhoneTypeId)) Phones, ")
            .Append("    Coalesce(S.HomeEmail, S.WorkEmail, ' ') Email ")
            .Append("FROM	arStudent S, arStuEnrollments SE, syStatusCodes SC, arStudAddresses SA, arStudentPhone SP ")
            .Append("WHERE ")
            .Append("       SA.StudentId=S.StudentId ")
            .Append("AND	SP.StudentId=S.StudentId ")
            .Append("AND	S.StudentId=SE.StudentId ")
            .Append("AND	SE.StatusCodeId=SC.StatusCodeId ")
            .Append("AND	SC.SysStatusId=9 ")
            If IsValidGuid(campusId) Then
                .Append("AND    S.StudentId in (select distinct StudentId from arStuEnrollments where CampusId='" + campusId + "') ")
            Else
                .Append("AND    1 = 2 ")
            End If
            .Append("AND	SA.StdAddressId=(Select Top 1 StdAddressId from arStudAddresses where StudentId=S.StudentId) ")
            If IsAlpha(search) Then
                .Append("AND    (S.LastName like '" + search.Trim + "%' ")
                .Append("OR     S.FirstName like '" + search.Trim + "%') ")
            Else
                If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
                    .Append("AND    S.SSN='" + search.Trim + "' ")
                ElseIf CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToLower = "studentid" Then
                    .Append("AND    S.StudentNumber like '%" + search.Trim + "' ")
                Else
                    .Append("AND    S." + CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToString + " like '%" + search.Trim + "%' ")
                End If
            End If
            .Append("ORDER BY Name")
        End With

        'Execute the query and return Dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Private Function IsAlpha(ByVal text As String) As Boolean
        If text.IndexOfAny("1234567890".ToCharArray) > -1 Then
            Return False
        Else
            Return True
        End If
    End Function
    Public Function GetStudentDataForHeaderInfo(ByVal studentId As String, ByVal Campusid As String) As StudentDataForHeaderInfo

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        'With sb
        '    .Append("SELECT TOP 1 ")
        '    .Append("       S.LastName + ', ' + S.FirstName as Name, ")
        '    .Append("       PV.PrgVerDescrip as ProgramVersion, ")
        '    .Append("       SC.StatusCodeDescrip As StatusCode, ")
        '    .Append("		SE.StartDate, ")
        '    .Append("		SE.ExpGradDate, ")
        '    .Append("       (select max(CreatedDate) from syStudentNotes where StudentId = ?) As LastNoteDate ")
        '    .Append("FROM	arStudent S, arStuEnrollments SE, arPrgVersions PV, syStatusCodes SC ")
        '    .Append("WHERE ")
        '    .Append("		S.StudentId=SE.StudentId ")
        '    .Append("AND	SE.PrgVerId=PV.PrgVerId ")
        '    .Append("AND	SE.StatusCodeId=SC.StatusCodeId ")
        '    .Append("AND	S.StudentId=? ")
        'End With

        With sb
            .Append("SELECT top 1 ")
            .Append("		S.LastName + ', ' + S.FirstName AS Name,  ")
            .Append("		(SELECT PrgVerDescrip FROM arPrgVersions WHERE PrgVerId = SE.PrgVerId) AS ProgramVersion,  ")
            .Append("		SC.StatusCodeDescrip AS StatusCode,  ")
            .Append("		StartDate,  ")
            .Append("		ExpGradDate,  ")
            .Append("		(SELECT MAX(CreatedDate) FROM syStudentNotes WHERE StudentId = S.StudentId) As LastNoteDate,SE.CohortStartDate  ")
            .Append("FROM	arStudent S, arStuEnrollments SE, syStatusCodes SC, sySysStatus SS  ")
            .Append("WHERE	S.StudentId = ? ")
            .Append("AND	S.StudentId=SE.StudentId ")
            .Append("AND	SE.CampusId= ? ")
            .Append("AND    SE.StatusCodeId=SC.StatusCodeId ")
            .Append("AND    SC.SysStatusId=SS.SysStatusId ")
            .Append("Order by SS.InSchool Desc,SE.StartDate Desc,SE.ExpGradDate Desc ")
        End With

        ' Add the studentId to the parameter list
        db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add the campusId to the parameter list
        db.AddParameter("@CampusId", Campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim studentDataForHeaderInfo As New StudentDataForHeaderInfo(studentId)

        While dr.Read()
            '   populate fields with data from DataReader
            With studentDataForHeaderInfo
                .Name = dr("Name")
                If Not dr("ProgramVersion") Is System.DBNull.Value Then
                    .ProgramVersion = dr("ProgramVersion")
                End If
                If Not dr("StatusCode") Is System.DBNull.Value Then
                    .StatusCode = dr("StatusCode")
                End If
                If Not dr("StartDate") Is System.DBNull.Value Then
                    .StartDate = dr("StartDate")
                Else
                    .StartDate = Date.MinValue
                End If
                If Not dr("CohortStartDate") Is System.DBNull.Value Then
                    .CohortStartDate = dr("CohortStartDate")
                Else
                    .CohortStartDate = Date.MinValue
                End If
                If Not dr("ExpGradDate") Is System.DBNull.Value Then
                    .GraduationDate = dr("ExpGradDate")
                Else
                    .GraduationDate = Date.MinValue
                End If
                If Not dr("LastNoteDate") Is System.DBNull.Value Then
                    .LastNoteDate = dr("LastNoteDate")
                Else
                    .LastNoteDate = Date.MinValue
                End If
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return studentDataForHeaderInfo
        Return studentDataForHeaderInfo

    End Function
    Private Function IsValidGuid(ByVal guid As String) As Boolean
        '   if it is nothing it is not a guid
        If guid Is Nothing Then Return False

        '   if it is all zeroes is invalid
        If guid = "00000000-0000-0000-0000-000000000000" Then Return False

        '   these is the regular expression used to validate guids
        Dim guidRegEx As New System.Text.RegularExpressions.Regex("[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}")

        '   validate guid
        If guidRegEx.Match(guid).Success Then Return True Else Return False
    End Function

    Public Function getStudentTerm(ByVal stuEnrollId As String, ByVal campusid As String) As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append(" Select distinct ")
            .Append(" T.TermId, T.TermDescrip,T.StartDate ")
            .Append(" FROM  arStuEnrollments SE, arResults R, arClassSections CS, ")
            .Append(" arTerm T WHERE 		SE.StuEnrollId=R.StuEnrollId AND	R.TestId=CS.ClsSectionId AND 	")
            .Append(" CS.TermId=T.TermId  AND  SE.StuEnrollId= ? AND  SE.CampusId = ? ")
            .Append(" Union ")
            .Append(" Select distinct ")
            .Append(" T.TermId, T.TermDescrip,T.StartDate ")
            .Append(" FROM  arStuEnrollments SE, arTransferGrades R,arTerm T ")
            .Append("  WHERE 		SE.StuEnrollId=R.StuEnrollId AND	R.TermId=T.TermId ")
            .Append(" AND  SE.StuEnrollId= ? AND  SE.CampusId = ? ")
            .Append(" Union ")
            .Append(" Select DISTINCT 		  T.TermId, T.TermDescrip,T.StartDate from  arStuEnrollments SE, ")
            .Append(" arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2   ")
            .Append(" WHERE  SE.PrgVerId = PV.PrgVerId And PV.IsContinuingEd = 1 And T.StartDate >= SE.ExpStartDate ")
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            .Append(" AND  SE.StuEnrollId= ? AND  SE.CampusId = ? ")
            .Append(" Union ")
            .Append(" Select DISTINCT 		  T.TermId, T.TermDescrip,T.StartDate from  arStuEnrollments SE, ")
            .Append(" arPrgVersions PV ,arTerm T   ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE  SE.PrgVerId = PV.PrgVerId And T.StartDate = SE.ExpStartDate ")
            .Append(" and (PV.ProgId=T.ProgId or T.ProgId is null) ")
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            .Append(" AND  SE.StuEnrollId= ? AND  SE.CampusId = ? ")
            .Append(" Union ")
            .Append(" Select DISTINCT 		T.TermId, T.TermDescrip,T.StartDate  FROM  arStuEnrollments SE, ")
            .Append(" arPrgVersions PV ,arTerm T   ,syCmpGrpCmps t1, syCampuses t2   WHERE  SE.PrgVerId=PV.PrgVerId ")
            .Append(" and (PV.ProgId=T.ProgId or T.ProgId is null) ")
            .Append(" AND T.StartDate <=SE.ExpStartDate and SE.ExpStartDate <= T.EndDate ")
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            .Append(" AND  SE.StuEnrollId= ? AND  SE.CampusId = ? ")
            .Append(" order by T.StartDate desc")

        End With
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
    End Function

    Public Function getAllStudentTerm(ByVal stuEnrollId As String, ByVal campusid As String) As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append(" Select DISTINCT 		  T.TermId, T.TermDescrip,T.StartDate from  arStuEnrollments SE, ")
            .Append(" arPrgVersions PV ,arTerm T ,syCmpGrpCmps t1, syCampuses t2    ")
            .Append(" WHERE  SE.PrgVerId = PV.PrgVerId And T.StartDate >= SE.ExpStartDate ")
            ''Added by Saraswathi Lakshmanan on feb 15 2011
            ''To fix rally case DE4967 Name: QA: Terms in the student search pop up pages shows terms that does not belong to the student's program. 

            .Append(" and (PV.ProgId=T.ProgId or T.ProgId is null) ")
            .Append(" AND   t1.CampusId = t2.CampusId and ")
            't1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            .Append(" T.CampGrpId IN (SELECT Distinct CampGrpId FROM syCmpGrpCmps WHERE CampusId = ? ")
            .Append("    AND CampGrpId not in (SELECT Distinct CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL') ")
            .Append(" Union ")
            .Append(" SELECT Distinct CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL') ")
            .Append(" and PV.CampGrpId IN (SELECT Distinct CampGrpId FROM syCmpGrpCmps WHERE CampusId = ? ")
            .Append("    AND CampGrpId not in (SELECT Distinct CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL') ")
            .Append(" Union ")
            .Append(" SELECT Distinct CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL') ")
            .Append(" AND  SE.StuEnrollId= ? AND  SE.CampusId = ? ")
            .Append(" Union ")
            .Append(" Select DISTINCT 		T.TermId, T.TermDescrip,T.StartDate  FROM  arStuEnrollments SE, ")
            .Append(" arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2    WHERE  SE.PrgVerId=PV.PrgVerId ")
            .Append(" and (PV.ProgId=T.ProgId or T.ProgId is null) ")
            .Append(" AND T.StartDate <=SE.ExpStartDate and SE.ExpStartDate <= T.EndDate ")
            .Append(" AND   t1.CampusId = t2.CampusId and ")
            't1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            .Append(" T.CampGrpId IN (SELECT Distinct CampGrpId FROM syCmpGrpCmps WHERE CampusId = ? ")
            .Append("    AND CampGrpId not in (SELECT Distinct CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL') ")
            .Append(" Union ")
            .Append(" SELECT Distinct CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL') ")
            .Append(" and PV.CampGrpId IN (SELECT Distinct CampGrpId FROM syCmpGrpCmps WHERE CampusId = ? ")
            .Append("    AND CampGrpId not in (SELECT Distinct CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL') ")
            .Append(" Union ")
            .Append(" SELECT Distinct CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL') ")
            .Append(" AND  SE.StuEnrollId= ? AND  SE.CampusId = ? ")
            .Append(" order by T.StartDate desc")

        End With
        db.AddParameter("@campusId", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
    End Function

    Public Function getStudentAllTerm(ByVal studentId As String, ByVal campusid As String) As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append(" Select distinct ")
            .Append(" T.TermId, T.TermDescrip,T.StartDate ")
            .Append(" FROM  arStudent S ,arStuEnrollments SE, arResults R, arClassSections CS, ")
            .Append(" arTerm T WHERE 	S.StudentId=SE.StudentId and SE.StuEnrollId=R.StuEnrollId AND	R.TestId=CS.ClsSectionId AND 	")
            .Append(" CS.TermId=T.TermId   AND  S.StudentId= ? AND  SE.CampusId = ? ")
            .Append(" Union ")
            .Append(" Select distinct ")
            .Append(" T.TermId, T.TermDescrip, T.StartDate ")
            .Append(" FROM  arStudent S ,arStuEnrollments SE, arTransferGrades R,arTerm T WHERE 	S.StudentId=SE.StudentId and ")
            .Append(" SE.StuEnrollId=R.StuEnrollId AND	R.TermId=T.TermId   AND ")
            .Append(" S.StudentId= ? AND  SE.CampusId = ? ")
            .Append(" order by T.StartDate desc")

        End With
        db.AddParameter("@studentId", studentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@studentId", studentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
    End Function
    ''Added by saraswathi Lakshmanan on June 10 2009 to fix issue 14829
    ''To show the inschool enrollments to nonSa users
    Public Function GetStudentSearchDSWithStudentNumberforcurrentEnrollment(ByVal lastName As String, ByVal firstName As String, ByVal ssn As String, ByVal enrollment As String, ByVal statusId As String, ByVal prgVerId As String, ByVal campusId As String, ByVal studentNumber As String) As DataSet

        '   create dataset
        Dim ds As New DataSet

        '   build select command
        Dim sc As New OleDbCommand
        Dim sc1 As New OleDbCommand

        '   build query to select students
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder
        Dim sb2 As New StringBuilder
        Dim sb3 As New StringBuilder
        Dim sb4 As New StringBuilder
        Dim sb5 As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT DISTINCT ")
            .Append("       S.StudentId, ")
            .Append("       S.LastName, ")
            .Append("       S.FirstName, ")
            .Append("       S.SSN, ")
            .Append(" (select Top 1  t3.PrgVerId from arStudent t2,")
            '' .Append(" arStuEnrollments t3 ")
            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append("( ( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))))  t3 ")

            .Append(" where t2.StudentId = S.StudentId and t2.StudentId=t3.StudentId) as PrgVerId, ")
            .Append(" (select Top 1  t1.PrgVerDescrip from arPrgVersions t1,arStudent t2,")
            '' .Append(" arStuEnrollments t3 ")
            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append("( ( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))))  t3 ")

            .Append(" where t2.StudentId = S.StudentId and t2.StudentId=t3.StudentId and t3.PrgVerId=t1.PrgVerId) as PrgVerName, ")
            'get the current term for the student
            .Append("       (SELECT TOP 1 ")
            .Append("                T1.TermId ")
            .Append("       FROM arStudent S1, ")

            ''.Append(" arStuEnrollments SE1 ")
            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append("( ( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))))  SE1 ")


            .Append(" , arResults R1, arClassSections CS1, arTerm T1")
            .Append("       WHERE ")
            .Append("               S1.StudentId = SE1.StudentId ")
            .Append("       AND		SE1.StuEnrollId=R1.StuEnrollId ")
            .Append("       AND		R1.TestId=CS1.ClsSectionId ")
            .Append("       AND		CS1.TermId=T1.TermId ")
            .Append("       AND		S1.StudentId=S.StudentId ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T1.StatusId in (Select StatusID from systatuses where status='Active') ")

            .Append("       ORDER BY T1.StartDate desc) As CurrentTerm, ")
            .Append("       S.StudentStatus as Status ")
            .Append("FROM   arStudent S ,")
            '' .Append("  arStuEnrollments SE ")
            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append("( ( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE ")


            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''InSchool, Status Externship added -- 22
            ''  .Append(" And  SE.StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22) ) ")

            ' Only the first relationship is AND
            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb1.Append(andOrOrOperator)
                sb1.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If
            'check for studentid
            If Not studentNumber = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" S.studentNumber like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("studentNumber", studentNumber))
                andOrOrOperator = " AND "
            End If
            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If


        End With

        'Build select command
        sc.CommandText = sb.ToString + sb1.ToString
        sc.Connection = New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))

        'Create adapter to handle Students table
        Dim da As New OleDbDataAdapter(sc)
        da.SelectCommand.CommandTimeout = 600
        'Fill Students table
        da.Fill(ds, "Students")

        'Build query to select students enrollments
        sb = New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       SE.StuEnrollId, ")
            .Append("       SE.StudentId, ")
            .Append("       SE.ExpStartDate AS StartDate, ")
            .Append("       SE.PrgVerId ")
            .Append("FROM   arStudent S, ")
            '' .Append("  arStuEnrollments SE ")
            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE ")


            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''Added 22 Externship inschool status
            ''        .Append(" And  SE.StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22) ) ")


        End With

        'Modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString + " ORDER BY SE.StudentId, StartDate desc "

        'Fill StudentEnrollments table
        da.Fill(ds, "StudentEnrollments")

        '   build query to select enrollments for selected students
        sb = New StringBuilder
        With sb
            .Append("SELECT DISTINCT")
            .Append("       SE.PrgVerId, ")
            .Append("       (Select PrgVerDescrip from arPrgVersions where PrgVerId=SE.PrgVerId) As Enrollment ")
            .Append("FROM   arStudent S, ")
            ''   .Append("  arStuEnrollments SE ")

            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))))  SE ")

            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            ''   .Append(" And  SE.StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22) ) ")


        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString

        '   fill Enrollments table
        da.Fill(ds, "Enrollments")

        '   build query to select StudentTerms
        sb = New StringBuilder
        With sb
            .Append("Select DISTINCT ")
            .Append("		S.StudentId, ")
            .Append("       T.TermId, ")
            '.Append("       S.FirstName, ")
            '.Append("       S.LastName, ")
            '.Append("       T.Termdescrip, ")
            .Append("       T.StartDate, ")
            .Append("       SE.StuEnrollId  ")
            .Append("FROM arStudent S, ")
            ''  .Append(" arStuEnrollments SE ")
            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append("( ( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE ")


            .Append(" , arResults R, arClassSections CS, arClassSectionTerms CST, arTerm T, ")
            .Append("syCmpGrpCmps t1, syCampuses t2 ")
            .Append("WHERE ")
            .Append("		S.StudentId=SE.StudentId ")
            .Append("AND	SE.StuEnrollId=R.StuEnrollId ")
            .Append("AND	R.TestId=CS.ClsSectionId ")
            .Append("AND    CST.ClsSectionId=CS.ClsSectionId ")
            .Append("AND	CST.TermId=T.TermId ")
            .Append("AND    t1.CampusId = t2.CampusId ")
            .Append("AND    t1.CampGrpId=T.CampGrpId")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            ''      .Append(" And  SE.StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")

            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With

        '   check if the selection is "Campus Groups" = ""
        If Not campusId = Guid.Empty.ToString Then
            Dim andOrOrOperator As String = " AND "

            sb1.Append(andOrOrOperator)
            sb1.Append(" T1.CampusId= ? ")
            sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
        End If

        sb2 = New StringBuilder
        With sb2
            .Append(" Select DISTINCT ")
            .Append("		S.StudentId, ")
            .Append("       T.TermId, ")
            '.Append("       S.FirstName, ")
            '.Append("       S.LastName, ")
            '.Append("       T.Termdescrip, ")
            .Append("       T.StartDate, ")
            .Append("       SE.StuEnrollId  ")
            .Append(" FROM arStudent S, ")
            ''.Append(" arStuEnrollments SE ")
            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE ")


            .Append(" , arTransferGrades R, arTerm T, ")
            .Append("syCmpGrpCmps t1, syCampuses t2")
            .Append(" WHERE 		S.StudentId=SE.StudentId AND	SE.StuEnrollId=R.StuEnrollId AND ")
            .Append(" t1.CampusId = t2.CampusId AND ")
            .Append(" t1.CampGrpId=T.CampGrpId AND ")
            .Append(" R.TermId=T.TermId ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            ''     .Append(" And  SE.StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")

            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb2.Append(andOrOrOperator)
                sb2.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If
            'studentid
            If Not studentNumber = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" S.studentNumber like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("studentNumber", studentNumber))
                andOrOrOperator = " AND "
            End If
            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb2.Append(andOrOrOperator)
                sb2.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With

        sb3 = New StringBuilder
        With sb3
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            ''  .Append(" arStuEnrollments SE ")

            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE ")


            .Append(" ,arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE S.StudentId = SE.StudentId ")
            .Append(" and SE.PrgVerId=PV.PrgVerId and PV.IsContinuingEd=1 and T.StartDate >= SE.ExpStartDate ")
            ' .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            ''Added by Saraswathi lakshmanan on March 5 2010
            ''18574: QA: Issue with the terms in the Term dropdown on the Student Search pop ups. 
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            ''   .Append(" And  SE.StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")

            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb3.Append(andOrOrOperator)
                sb3.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If
            'check for studentid
            If Not studentNumber = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" S.studentNumber like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("studentNumber", studentNumber))
                andOrOrOperator = " AND "
            End If
            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb3.Append(andOrOrOperator)
                sb3.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With



        sb4 = New StringBuilder
        With sb4
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            ''    .Append(" arStuEnrollments SE ")
            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append("( ( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))))  SE ")


            .Append(" ,arPrgVersions PV ,arTerm T ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE S.StudentId = SE.StudentId ")
            .Append(" and SE.PrgVerId=PV.PrgVerId and T.StartDate = SE.ExpStartDate ")
            '  .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            ''Added by Saraswathi lakshmanan on March 5 2010
            ''18574: QA: Issue with the terms in the Term dropdown on the Student Search pop ups. 
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            ''      .Append(" And  SE.StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")


            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb4.Append(andOrOrOperator)
                sb4.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If
            'check for studentId
            If Not studentNumber = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" S.studentNumber like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("studentNumber", studentNumber))
                andOrOrOperator = " AND "
            End If
            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb4.Append(andOrOrOperator)
                sb4.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With

        sb5 = New StringBuilder
        With sb5
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            ''   .Append(" arStuEnrollments SE")
            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE ")


            .Append(" ,  arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2   WHERE S.StudentId = SE.StudentId  and SE.PrgVerId=PV.PrgVerId ")
            .Append(" and (PV.ProgId=T.ProgId or T.ProgId is null) AND T.StartDate <=SE.ExpStartDate and SE.ExpStartDate <= T.EndDate ")
            ''Added by Vijay Ramteke on Feb 02, 2010
            '.Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            '.Append(" and (t1.CampGrpId=PV.CampGrpId Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            ''Added by Saraswathi lakshmanan on March 5 2010
            ''18574: QA: Issue with the terms in the Term dropdown on the Student Search pop ups. 
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            ''Added by Vijay Ramteke on Feb 02, 2010
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            ''   .Append(" And  SE.StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")


            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb5.Append(andOrOrOperator)
                sb5.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If
            'check for studentId
            If Not studentNumber = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" S.studentNumber like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("studentNumber", studentNumber))
                andOrOrOperator = " AND "
            End If
            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb5.Append(andOrOrOperator)
                sb5.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With
        '   modify select command
        'da.SelectCommand.CommandText = sb.ToString
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString + " Union " + sb2.ToString + " Union " + sb3.ToString + " Union " + sb4.ToString + " Union " + sb5.ToString + " ORDER BY S.StudentId, T.TermId, T.StartDate desc "
        'da.SelectCommand.CommandText = sb2.ToString + sb1.ToString + " ORDER BY S.StudentId, T.TermId, T.StartDate desc "

        '   fill StudentTerms table
        da.Fill(ds, "StudentTerms")

        '   build query to select StudentTerms
        sc.Parameters.Clear()
        sc.CommandText = ""

        sb = New StringBuilder
        With sb

            .Append("SELECT  T.TermId, ")
            .Append("         T.TermDescrip, ")
            .Append("         T.StartDate ")
            .Append(" FROM     arTerm T  ,syCmpGrpCmps t1, syCampuses t2 ")
            .Append(" where t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            '.Append("ORDER BY T.StartDate desc ")

            If Not campusId = Guid.Empty.ToString Then
                sb.Append(" and t1.campusid = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
        End With


        'Create adapter to handle Students table
        'Dim da As New OleDbDataAdapter(sc1)

        'sc.CommandText = sb.ToString + " ORDER BY T.StartDate desc "
        'sc.Connection = New OleDbConnection(SingletonAppSettings.AppSettings("ConString"))

        ''Create adapter to handle Students table
        'Dim da As New OleDbDataAdapter(sc)

        ''Fill Students table
        'da.Fill(ds, "Terms")

        da.SelectCommand.CommandText = sb.ToString + " ORDER BY T.StartDate desc "

        ' ''   fill Terms table
        da.Fill(ds, "Terms")

        '   create primary and foreign key constraints

        '   set primary key for Students table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("Students").Columns("StudentId")
        ds.Tables("Students").PrimaryKey = pk0

        '   set primary key for Enrollments table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("Enrollments").Columns("PrgVerId")
        ds.Tables("Enrollments").PrimaryKey = pk1

        '   set primary key for Terms table
        Dim pk2(0) As DataColumn
        pk2(0) = ds.Tables("Terms").Columns("TermId")
        ds.Tables("Terms").PrimaryKey = pk2

        '   set foreign key column in StudentEnrollments
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("StudentEnrollments").Columns("StudentId")

        '   set foreign key column in StudentEnrollments
        Dim fk1(0) As DataColumn
        fk1(0) = ds.Tables("StudentEnrollments").Columns("PrgVerId")

        '   set foreign key column in Students
        Dim fk2(0) As DataColumn
        fk2(0) = ds.Tables("StudentTerms").Columns("StudentId")

        '   set foreign key column in Students
        Dim fk3(0) As DataColumn
        fk3(0) = ds.Tables("StudentTerms").Columns("TermId")

        '   set foreign keys in GradeBookResults table
        ds.Relations.Add("StudentsStudentEnrollments", pk0, fk0)
        ds.Relations.Add("EnrollmentsStudentEnrollments", pk1, fk1)
        ds.Relations.Add("StudentStudentTerms", pk0, fk2, False)
        ds.Relations.Add("TermsStudentTerms", pk2, fk3)

        '   return dataset
        Return ds

    End Function
    ''Added by Sarawsathi Lakshmanan on June 11 2009
    ''To show the InSchoolEnrollments only
    ''For mantis case
    Public Function GetStudentSearchDSForInSchoolEnrollment(ByVal lastName As String, ByVal firstName As String, ByVal ssn As String, ByVal enrollment As String, ByVal statusId As String, ByVal prgVerId As String, ByVal campusId As String) As DataSet

        '   create datasetb
        Dim ds As New DataSet

        '   build select command
        Dim sc As New OleDbCommand
        Dim sc1 As New OleDbCommand

        '   build query to select students
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder
        Dim sb2 As New StringBuilder
        Dim sb3 As New StringBuilder
        Dim sb4 As New StringBuilder
        Dim sb5 As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT DISTINCT ")
            .Append("       S.StudentId, ")
            .Append("       S.LastName, ")
            .Append("       S.FirstName, ")
            .Append("       S.SSN, ")
            .Append(" (select Top 1  t3.PrgVerId from arStudent t2, ")
            '' .Append(" arStuEnrollments t3 ")
            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))))  t3 ")

            .Append(" where t2.StudentId = S.StudentId and t2.StudentId=t3.StudentId) as PrgVerId, ")
            .Append(" (select Top 1  t1.PrgVerDescrip from arPrgVersions t1,arStudent t2, ")
            ''.Append(" arStuEnrollments t3")
            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append("( ( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) t3 ")

            .Append("  where t2.StudentId = S.StudentId and t2.StudentId=t3.StudentId and t3.PrgVerId=t1.PrgVerId) as PrgVerName, ")
            'get the current term for the student
            .Append("       (SELECT TOP 1 ")
            .Append("                T1.TermId ")
            .Append("       FROM arStudent S1, ")
            '' .Append(" arStuEnrollments SE1")
            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))))  SE1 ")

            .Append(" , arResults R1, arClassSections CS1, arTerm T1")
            .Append("       WHERE ")
            .Append("               S1.StudentId = SE1.StudentId ")
            .Append("       AND		SE1.StuEnrollId=R1.StuEnrollId ")
            .Append("       AND		R1.TestId=CS1.ClsSectionId ")
            .Append("       AND		CS1.TermId=T1.TermId ")
            .Append("       AND		S1.StudentId=S.StudentId ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T1.StatusId in (Select StatusID from systatuses where status='Active') ")

            .Append("       ORDER BY T1.StartDate desc) As CurrentTerm, ")
            .Append("       S.StudentStatus as Status ")
            .Append("FROM   arStudent S ,  ")
            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))))  SE ")

            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''Externship inschool Status added - 22
            '' .Append(" And  SE.StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22) ) ")

            ' Only the first relationship is AND
            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb1.Append(andOrOrOperator)
                sb1.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
        End With

        'Build select command
        sc.CommandText = sb.ToString + sb1.ToString
        sc.Connection = New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))

        'Create adapter to handle Students table
        Dim da As New OleDbDataAdapter(sc)
        da.SelectCommand.CommandTimeout = 600
        'Fill Students table
        da.Fill(ds, "Students")

        'Build query to select students enrollments
        sb = New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       SE.StuEnrollId, ")
            .Append("       SE.StudentId, ")
            .Append("       SE.ExpStartDate AS StartDate, ")
            .Append("       SE.PrgVerId ")
            .Append("FROM   arStudent S,  ")
            ''
            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))))  SE ")


            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            ''  .Append(" And  SE.StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")

        End With

        'Modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString + " ORDER BY SE.StudentId, StartDate desc "

        'Fill StudentEnrollments table
        da.Fill(ds, "StudentEnrollments")

        '   build query to select enrollments for selected students
        sb = New StringBuilder
        With sb
            .Append("SELECT DISTINCT")
            .Append("       SE.PrgVerId, ")
            .Append("       (Select PrgVerDescrip from arPrgVersions where PrgVerId=SE.PrgVerId) As Enrollment ")
            .Append("FROM   arStudent S,  ")

            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append("( ( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE ")

            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")

        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString

        '   fill Enrollments table
        da.Fill(ds, "Enrollments")

        '   build query to select StudentTerms
        sb = New StringBuilder
        With sb
            .Append("Select DISTINCT ")
            .Append("		S.StudentId, ")
            .Append("       T.TermId, ")
            '.Append("       S.FirstName, ")
            '.Append("       S.LastName, ")
            '.Append("       T.Termdescrip, ")
            .Append("       T.StartDate, ")
            .Append("       SE.StuEnrollId  ")
            .Append("FROM arStudent S,  ")

            '.Append(" arStuEnrollments SE")
            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE ")

            .Append(" , arResults R, arClassSections CS, arClassSectionTerms CST, arTerm T, ")
            .Append("syCmpGrpCmps t1, syCampuses t2 ")
            .Append("WHERE ")
            .Append("		S.StudentId=SE.StudentId ")
            .Append("AND	SE.StuEnrollId=R.StuEnrollId ")
            .Append("AND	R.TestId=CS.ClsSectionId ")
            .Append("AND    CST.ClsSectionId=CS.ClsSectionId ")
            .Append("AND	CST.TermId=T.TermId ")
            .Append("AND    t1.CampusId = t2.CampusId ")
            .Append("AND    t1.CampGrpId=T.CampGrpId")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            ''   .Append(" And  SE.StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")

            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With

        '   check if the selection is "Campus Groups" = ""
        If Not campusId = Guid.Empty.ToString Then
            Dim andOrOrOperator As String = " AND "

            sb1.Append(andOrOrOperator)
            sb1.Append(" T1.CampusId= ? ")
            sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
        End If


        sb2 = New StringBuilder
        With sb2
            .Append(" Select DISTINCT ")
            .Append("		S.StudentId, ")
            .Append("       T.TermId, ")
            '.Append("       S.FirstName, ")
            '.Append("       S.LastName, ")
            '.Append("       T.Termdescrip, ")
            .Append("       T.StartDate, ")
            .Append("       SE.StuEnrollId  ")
            ''.Append(" FROM arStudent S, arStuEnrollments SE, arTransferGrades R, arTerm T, ")

            .Append("FROM arStudent S,  ")

            '.Append(" arStuEnrollments SE")
            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))))  SE ")

            .Append(" , arTransferGrades R, arTerm T, ")

            .Append("syCmpGrpCmps t1, syCampuses t2")
            .Append(" WHERE 		S.StudentId=SE.StudentId AND ")
            .Append(" SE.StuEnrollId = R.StuEnrollId AND ")
            .Append(" t1.CampusId = t2.CampusId AND ")
            .Append(" t1.CampGrpId=T.CampGrpId AND ")
            .Append(" R.TermId=T.TermId ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            ''   .Append(" And  SE.StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")

            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb2.Append(andOrOrOperator)
                sb2.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb2.Append(andOrOrOperator)
                sb2.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With

        sb3 = New StringBuilder
        With sb3
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            ''  .Append(" arStuEnrollments SE, ")

            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE, ")





            .Append(" arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE S.StudentId = SE.StudentId ")
            .Append(" and SE.PrgVerId=PV.PrgVerId and PV.IsContinuingEd=1 and T.StartDate >= SE.ExpStartDate ")
            '  .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            ''Added by Saraswathi lakshmanan on March 5 2010
            ''18574: QA: Issue with the terms in the Term dropdown on the Student Search pop ups. 
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")

            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            '' .Append(" And  SE.StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")

            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb3.Append(andOrOrOperator)
                sb3.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb3.Append(andOrOrOperator)
                sb3.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            ''Added by Saraswathi Lakshmanan on Feb 2 2011 to fix rally case DE4967 Name: QA: Terms in the student search pop up pages shows terms that does not belong to the student's program. 
            .Append("  AND (T.ProgID=PV.ProgId OR T.progId IS NULL) ")
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With



        sb4 = New StringBuilder
        With sb4
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            ''  .Append(" arStuEnrollments SE, ")
            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE, ")


            .Append(" arPrgVersions PV ,arTerm T ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE S.StudentId = SE.StudentId ")
            .Append(" and SE.PrgVerId=PV.PrgVerId and T.StartDate = SE.ExpStartDate ")
            '  .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            ''Added by Saraswathi lakshmanan on March 5 2010
            ''18574: QA: Issue with the terms in the Term dropdown on the Student Search pop ups. 
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            ''      .Append(" And  SE.StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")


            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb4.Append(andOrOrOperator)
                sb4.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb4.Append(andOrOrOperator)
                sb4.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            ''Added by Saraswathi Lakshmanan on Feb 2 2011 to fix rally case DE4967 Name: QA: Terms in the student search pop up pages shows terms that does not belong to the student's program. 
            .Append("  AND (T.ProgID=PV.ProgId OR T.progId IS NULL) ")
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With

        sb5 = New StringBuilder
        With sb5
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            ''.Append(" arStuEnrollments SE, ")
            ''MOdified by Saraswathi lakshmanan
            ''August 11 2009
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE, ")


            .Append("  arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2   WHERE S.StudentId = SE.StudentId  and SE.PrgVerId=PV.PrgVerId ")
            .Append(" and (PV.ProgId=T.ProgId or T.ProgId is null) AND T.StartDate <=SE.ExpStartDate and SE.ExpStartDate <= T.EndDate ")
            '.Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            ''Added by Saraswathi lakshmanan on March 5 2010
            ''18574: QA: Issue with the terms in the Term dropdown on the Student Search pop ups. 
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            ''  .Append(" And  SE.StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")


            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb5.Append(andOrOrOperator)
                sb5.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb5.Append(andOrOrOperator)
                sb5.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            ''Added by Saraswathi Lakshmanan on Feb 2 2011 to fix rally case DE4967 Name: QA: Terms in the student search pop up pages shows terms that does not belong to the student's program. 
            .Append("  AND (T.ProgID=PV.ProgId OR T.progId IS NULL) ")
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With
        '   modify select command
        'da.SelectCommand.CommandText = sb.ToString
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString + " Union " + sb2.ToString + " Union " + sb3.ToString + " Union " + sb4.ToString + " Union " + sb5.ToString + " ORDER BY S.StudentId, T.TermId, T.StartDate desc "
        'da.SelectCommand.CommandText = sb2.ToString + sb1.ToString + " ORDER BY S.StudentId, T.TermId, T.StartDate desc "

        '   fill StudentTerms table
        da.Fill(ds, "StudentTerms")

        '   build query to select StudentTerms
        sc.Parameters.Clear()
        sc.CommandText = ""

        sb = New StringBuilder
        With sb

            .Append("SELECT  T.TermId, ")
            .Append("         T.TermDescrip, ")
            .Append("         T.StartDate ")
            .Append(" FROM     arTerm T  ,syCmpGrpCmps t1, syCampuses t2 ")
            .Append(" where t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            '.Append("ORDER BY T.StartDate desc ")

            If Not campusId = Guid.Empty.ToString Then
                sb.Append(" and t1.campusid = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
        End With


        'Create adapter to handle Students table
        'Dim da As New OleDbDataAdapter(sc1)

        'sc.CommandText = sb.ToString + " ORDER BY T.StartDate desc "
        'sc.Connection = New OleDbConnection(SingletonAppSettings.AppSettings("ConString"))

        ''Create adapter to handle Students table
        'Dim da As New OleDbDataAdapter(sc)

        ''Fill Students table
        'da.Fill(ds, "Terms")

        da.SelectCommand.CommandText = sb.ToString + " ORDER BY T.StartDate desc "

        ' ''   fill Terms table
        da.Fill(ds, "Terms")

        '   create primary and foreign key constraints

        '   set primary key for Students table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("Students").Columns("StudentId")
        ds.Tables("Students").PrimaryKey = pk0

        '   set primary key for Enrollments table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("Enrollments").Columns("PrgVerId")
        ds.Tables("Enrollments").PrimaryKey = pk1

        '   set primary key for Terms table
        Dim pk2(0) As DataColumn
        pk2(0) = ds.Tables("Terms").Columns("TermId")
        ds.Tables("Terms").PrimaryKey = pk2

        '   set foreign key column in StudentEnrollments
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("StudentEnrollments").Columns("StudentId")

        '   set foreign key column in StudentEnrollments
        Dim fk1(0) As DataColumn
        fk1(0) = ds.Tables("StudentEnrollments").Columns("PrgVerId")

        '   set foreign key column in Students
        Dim fk2(0) As DataColumn
        fk2(0) = ds.Tables("StudentTerms").Columns("StudentId")

        '   set foreign key column in Students
        Dim fk3(0) As DataColumn
        fk3(0) = ds.Tables("StudentTerms").Columns("TermId")

        '   set foreign keys in GradeBookResults table
        ds.Relations.Add("StudentsStudentEnrollments", pk0, fk0)
        ds.Relations.Add("EnrollmentsStudentEnrollments", pk1, fk1)
        ds.Relations.Add("StudentStudentTerms", pk0, fk2, False)
        ds.Relations.Add("TermsStudentTerms", pk2, fk3)

        '   return dataset
        Return ds

    End Function
    'Added by  lakshmanan on August 11 2009
    ''To get the Out of School Enrollments
    Public Function GetStudentSearchDSForOutofSchoolEnrollment(ByVal lastName As String, ByVal firstName As String, ByVal ssn As String, ByVal enrollment As String, ByVal statusId As String, ByVal prgVerId As String, ByVal campusId As String) As DataSet

        '   create datasetb
        Dim ds As New DataSet

        '   build select command
        Dim sc As New OleDbCommand
        Dim sc1 As New OleDbCommand

        '   build query to select students
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder
        Dim sb2 As New StringBuilder
        Dim sb3 As New StringBuilder
        Dim sb4 As New StringBuilder
        Dim sb5 As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT DISTINCT ")
            .Append("       S.StudentId, ")
            .Append("       S.LastName, ")
            .Append("       S.FirstName, ")
            .Append("       S.SSN, ")
            .Append(" (select Top 1  t3.PrgVerId from arStudent t2,arStuEnrollments t3 where t2.StudentId = S.StudentId and t2.StudentId=t3.StudentId) as PrgVerId, ")
            .Append(" (select Top 1  t1.PrgVerDescrip from arPrgVersions t1,arStudent t2,arStuEnrollments t3 where t2.StudentId = S.StudentId and t2.StudentId=t3.StudentId and t3.PrgVerId=t1.PrgVerId) as PrgVerName, ")
            'get the current term for the student
            .Append("       (SELECT TOP 1 ")
            .Append("                T1.TermId ")
            .Append("       FROM arStudent S1, arStuEnrollments SE1, arResults R1, arClassSections CS1, arTerm T1")
            .Append("       WHERE ")
            .Append("               S1.StudentId = SE1.StudentId ")
            .Append("       AND		SE1.StuEnrollId=R1.StuEnrollId ")
            .Append("       AND		R1.TestId=CS1.ClsSectionId ")
            .Append("       AND		CS1.TermId=T1.TermId ")
            .Append("       AND		S1.StudentId=S.StudentId ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T1.StatusId in (Select StatusID from systatuses where status='Active') ")

            .Append("       ORDER BY T1.StartDate desc) As CurrentTerm, ")
            .Append("       S.StudentStatus as Status ")
            .Append("FROM   arStudent S , arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''Externship inschool Status added - 22
            .Append(" And  SE.StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22) ) ")

            ' Only the first relationship is AND
            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb1.Append(andOrOrOperator)
                sb1.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
        End With

        'Build select command
        sc.CommandText = sb.ToString + sb1.ToString
        sc.Connection = New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))

        'Create adapter to handle Students table
        Dim da As New OleDbDataAdapter(sc)
        da.SelectCommand.CommandTimeout = 600
        'Fill Students table
        da.Fill(ds, "Students")

        'Build query to select students enrollments
        sb = New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       SE.StuEnrollId, ")
            .Append("       SE.StudentId, ")
            .Append("       SE.ExpStartDate AS StartDate, ")
            .Append("       SE.PrgVerId ")
            .Append("FROM   arStudent S, arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''Outofschool Status - externship  added 22
            .Append(" And  SE.StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")

        End With

        'Modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString + " ORDER BY SE.StudentId, StartDate desc "

        'Fill StudentEnrollments table
        da.Fill(ds, "StudentEnrollments")

        '   build query to select enrollments for selected students
        sb = New StringBuilder
        With sb
            .Append("SELECT DISTINCT")
            .Append("       SE.PrgVerId, ")
            .Append("       (Select PrgVerDescrip from arPrgVersions where PrgVerId=SE.PrgVerId) As Enrollment ")
            .Append("FROM   arStudent S, arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")

        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString

        '   fill Enrollments table
        da.Fill(ds, "Enrollments")

        '   build query to select StudentTerms
        sb = New StringBuilder
        With sb
            .Append("Select DISTINCT ")
            .Append("		S.StudentId, ")
            .Append("       T.TermId, ")
            '.Append("       S.FirstName, ")
            '.Append("       S.LastName, ")
            '.Append("       T.Termdescrip, ")
            .Append("       T.StartDate, ")
            .Append("       SE.StuEnrollId  ")
            .Append("FROM arStudent S, arStuEnrollments SE, arResults R, arClassSections CS, arClassSectionTerms CST, arTerm T, ")
            .Append("syCmpGrpCmps t1, syCampuses t2 ")
            .Append("WHERE ")
            .Append("		S.StudentId=SE.StudentId ")
            .Append("AND	SE.StuEnrollId=R.StuEnrollId ")
            .Append("AND	R.TestId=CS.ClsSectionId ")
            .Append("AND    CST.ClsSectionId=CS.ClsSectionId ")
            .Append("AND	CST.TermId=T.TermId ")
            .Append("AND    t1.CampusId = t2.CampusId ")
            .Append("AND    t1.CampGrpId=T.CampGrpId")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            .Append(" And  SE.StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")

            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With

        '   check if the selection is "Campus Groups" = ""
        If Not campusId = Guid.Empty.ToString Then
            Dim andOrOrOperator As String = " AND "

            sb1.Append(andOrOrOperator)
            sb1.Append(" T1.CampusId= ? ")
            sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
        End If


        sb2 = New StringBuilder
        With sb2
            .Append(" Select DISTINCT ")
            .Append("		S.StudentId, ")
            .Append("       T.TermId, ")
            '.Append("       S.FirstName, ")
            '.Append("       S.LastName, ")
            '.Append("       T.Termdescrip, ")
            .Append("       T.StartDate, ")
            .Append("       SE.StuEnrollId  ")
            .Append(" FROM arStudent S, arStuEnrollments SE, arTransferGrades R, arTerm T, ")
            .Append("syCmpGrpCmps t1, syCampuses t2")
            .Append(" WHERE 		S.StudentId=SE.StudentId AND ")
            .Append(" SE.StuEnrollId = R.StuEnrollId AND ")
            .Append(" t1.CampusId = t2.CampusId AND ")
            .Append(" t1.CampGrpId=T.CampGrpId AND ")
            .Append(" R.TermId=T.TermId ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            .Append(" And  SE.StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")

            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb2.Append(andOrOrOperator)
                sb2.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb2.Append(andOrOrOperator)
                sb2.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With

        sb3 = New StringBuilder
        With sb3
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" arStuEnrollments SE, ")
            .Append(" arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE S.StudentId = SE.StudentId ")
            .Append(" and SE.PrgVerId=PV.PrgVerId and PV.IsContinuingEd=1 and T.StartDate >= SE.ExpStartDate ")
            ' .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            ''Added by Saraswathi lakshmanan on March 5 2010
            ''18574: QA: Issue with the terms in the Term dropdown on the Student Search pop ups. 
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            .Append(" And  SE.StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")

            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb3.Append(andOrOrOperator)
                sb3.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb3.Append(andOrOrOperator)
                sb3.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With



        sb4 = New StringBuilder
        With sb4
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" arStuEnrollments SE, ")
            .Append(" arPrgVersions PV ,arTerm T ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE S.StudentId = SE.StudentId ")
            .Append(" and SE.PrgVerId=PV.PrgVerId and T.StartDate = SE.ExpStartDate ")
            ' .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            ''Added by Saraswathi lakshmanan on March 5 2010
            ''18574: QA: Issue with the terms in the Term dropdown on the Student Search pop ups. 
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            .Append(" And  SE.StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")


            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb4.Append(andOrOrOperator)
                sb4.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb4.Append(andOrOrOperator)
                sb4.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With

        sb5 = New StringBuilder
        With sb5
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" arStuEnrollments SE,  arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2   WHERE S.StudentId = SE.StudentId  and SE.PrgVerId=PV.PrgVerId ")
            .Append(" and (PV.ProgId=T.ProgId or T.ProgId is null) AND T.StartDate <=SE.ExpStartDate and SE.ExpStartDate <= T.EndDate ")
            '  .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            ''Added by Saraswathi lakshmanan on March 5 2010
            ''18574: QA: Issue with the terms in the Term dropdown on the Student Search pop ups. 
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            .Append(" And  SE.StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")


            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb5.Append(andOrOrOperator)
                sb5.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb5.Append(andOrOrOperator)
                sb5.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With
        '   modify select command
        'da.SelectCommand.CommandText = sb.ToString
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString + " Union " + sb2.ToString + " Union " + sb3.ToString + " Union " + sb4.ToString + " Union " + sb5.ToString + " ORDER BY S.StudentId, T.TermId, T.StartDate desc "
        'da.SelectCommand.CommandText = sb2.ToString + sb1.ToString + " ORDER BY S.StudentId, T.TermId, T.StartDate desc "

        '   fill StudentTerms table
        da.Fill(ds, "StudentTerms")

        '   build query to select StudentTerms
        sc.Parameters.Clear()
        sc.CommandText = ""

        sb = New StringBuilder
        With sb

            .Append("SELECT  T.TermId, ")
            .Append("         T.TermDescrip, ")
            .Append("         T.StartDate ")
            .Append(" FROM     arTerm T  ,syCmpGrpCmps t1, syCampuses t2 ")
            .Append(" where t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            '.Append("ORDER BY T.StartDate desc ")

            If Not campusId = Guid.Empty.ToString Then
                sb.Append(" and t1.campusid = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
        End With


        'Create adapter to handle Students table
        'Dim da As New OleDbDataAdapter(sc1)

        'sc.CommandText = sb.ToString + " ORDER BY T.StartDate desc "
        'sc.Connection = New OleDbConnection(SingletonAppSettings.AppSettings("ConString"))

        ''Create adapter to handle Students table
        'Dim da As New OleDbDataAdapter(sc)

        ''Fill Students table
        'da.Fill(ds, "Terms")

        da.SelectCommand.CommandText = sb.ToString + " ORDER BY T.StartDate desc "

        ' ''   fill Terms table
        da.Fill(ds, "Terms")

        '   create primary and foreign key constraints

        '   set primary key for Students table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("Students").Columns("StudentId")
        ds.Tables("Students").PrimaryKey = pk0

        '   set primary key for Enrollments table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("Enrollments").Columns("PrgVerId")
        ds.Tables("Enrollments").PrimaryKey = pk1

        '   set primary key for Terms table
        Dim pk2(0) As DataColumn
        pk2(0) = ds.Tables("Terms").Columns("TermId")
        ds.Tables("Terms").PrimaryKey = pk2

        '   set foreign key column in StudentEnrollments
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("StudentEnrollments").Columns("StudentId")

        '   set foreign key column in StudentEnrollments
        Dim fk1(0) As DataColumn
        fk1(0) = ds.Tables("StudentEnrollments").Columns("PrgVerId")

        '   set foreign key column in Students
        Dim fk2(0) As DataColumn
        fk2(0) = ds.Tables("StudentTerms").Columns("StudentId")

        '   set foreign key column in Students
        Dim fk3(0) As DataColumn
        fk3(0) = ds.Tables("StudentTerms").Columns("TermId")

        '   set foreign keys in GradeBookResults table
        ds.Relations.Add("StudentsStudentEnrollments", pk0, fk0)
        ds.Relations.Add("EnrollmentsStudentEnrollments", pk1, fk1)
        ds.Relations.Add("StudentStudentTerms", pk0, fk2, False)
        ds.Relations.Add("TermsStudentTerms", pk2, fk3)

        '   return dataset
        Return ds

    End Function

    ''Added ny Saraswathi lakshmanan on August 2009
    ''To get the students of outofSchool Enrollments
    ''To show the inschool enrollments to nonSa users
    Public Function GetStudentSearchDSWithStudentNumberforPastEnrollment(ByVal lastName As String, ByVal firstName As String, ByVal ssn As String, ByVal enrollment As String, ByVal statusId As String, ByVal prgVerId As String, ByVal campusId As String, ByVal studentNumber As String) As DataSet

        '   create dataset
        Dim ds As New DataSet

        '   build select command
        Dim sc As New OleDbCommand
        Dim sc1 As New OleDbCommand

        '   build query to select students
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder
        Dim sb2 As New StringBuilder
        Dim sb3 As New StringBuilder
        Dim sb4 As New StringBuilder
        Dim sb5 As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT DISTINCT ")
            .Append("       S.StudentId, ")
            .Append("       S.LastName, ")
            .Append("       S.FirstName, ")
            .Append("       S.SSN, ")
            .Append(" (select Top 1  t3.PrgVerId from arStudent t2,arStuEnrollments t3 where t2.StudentId = S.StudentId and t2.StudentId=t3.StudentId) as PrgVerId, ")
            .Append(" (select Top 1  t1.PrgVerDescrip from arPrgVersions t1,arStudent t2,arStuEnrollments t3 where t2.StudentId = S.StudentId and t2.StudentId=t3.StudentId and t3.PrgVerId=t1.PrgVerId) as PrgVerName, ")
            'get the current term for the student
            .Append("       (SELECT TOP 1 ")
            .Append("                T1.TermId ")
            .Append("       FROM arStudent S1, arStuEnrollments SE1, arResults R1, arClassSections CS1, arTerm T1")
            .Append("       WHERE ")
            .Append("               S1.StudentId = SE1.StudentId ")
            .Append("       AND		SE1.StuEnrollId=R1.StuEnrollId ")
            .Append("       AND		R1.TestId=CS1.ClsSectionId ")
            .Append("       AND		CS1.TermId=T1.TermId ")
            .Append("       AND		S1.StudentId=S.StudentId ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T1.StatusId in (Select StatusID from systatuses where status='Active') ")

            .Append("       ORDER BY T1.StartDate desc) As CurrentTerm, ")
            .Append("       S.StudentStatus as Status ")
            .Append("FROM   arStudent S , arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''outof School, Status Externship added -- 22
            .Append(" And  SE.StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22) ) ")

            ' Only the first relationship is AND
            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb1.Append(andOrOrOperator)
                sb1.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If
            'check for studentid
            If Not studentNumber = "" Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" S.studentNumber like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("studentNumber", studentNumber))
                andOrOrOperator = " AND "
            End If
            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb1.Append(andOrOrOperator)
                sb1.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If


        End With

        'Build select command
        sc.CommandText = sb.ToString + sb1.ToString
        sc.Connection = New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))

        'Create adapter to handle Students table
        Dim da As New OleDbDataAdapter(sc)
        da.SelectCommand.CommandTimeout = 600
        'Fill Students table
        da.Fill(ds, "Students")

        'Build query to select students enrollments
        sb = New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       SE.StuEnrollId, ")
            .Append("       SE.StudentId, ")
            .Append("       SE.ExpStartDate AS StartDate, ")
            .Append("       SE.PrgVerId ")
            .Append("FROM   arStudent S, arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''Added 22 Externship inschool status
            .Append(" And  SE.StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22) ) ")


        End With

        'Modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString + " ORDER BY SE.StudentId, StartDate desc "

        'Fill StudentEnrollments table
        da.Fill(ds, "StudentEnrollments")

        '   build query to select enrollments for selected students
        sb = New StringBuilder
        With sb
            .Append("SELECT DISTINCT")
            .Append("       SE.PrgVerId, ")
            .Append("       (Select PrgVerDescrip from arPrgVersions where PrgVerId=SE.PrgVerId) As Enrollment ")
            .Append("FROM   arStudent S, arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            .Append(" And  SE.StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22) ) ")


        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString

        '   fill Enrollments table
        da.Fill(ds, "Enrollments")

        '   build query to select StudentTerms
        sb = New StringBuilder
        With sb
            .Append("Select DISTINCT ")
            .Append("		S.StudentId, ")
            .Append("       T.TermId, ")
            '.Append("       S.FirstName, ")
            '.Append("       S.LastName, ")
            '.Append("       T.Termdescrip, ")
            .Append("       T.StartDate, ")
            .Append("       SE.StuEnrollId  ")
            .Append("FROM arStudent S, arStuEnrollments SE, arResults R, arClassSections CS, arClassSectionTerms CST, arTerm T, ")
            .Append("syCmpGrpCmps t1, syCampuses t2 ")
            .Append("WHERE ")
            .Append("		S.StudentId=SE.StudentId ")
            .Append("AND	SE.StuEnrollId=R.StuEnrollId ")
            .Append("AND	R.TestId=CS.ClsSectionId ")
            .Append("AND    CST.ClsSectionId=CS.ClsSectionId ")
            .Append("AND	CST.TermId=T.TermId ")
            .Append("AND    t1.CampusId = t2.CampusId ")
            .Append("AND    t1.CampGrpId=T.CampGrpId")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            .Append(" And  SE.StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")

            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With

        '   check if the selection is "Campus Groups" = ""
        If Not campusId = Guid.Empty.ToString Then
            Dim andOrOrOperator As String = " AND "

            sb1.Append(andOrOrOperator)
            sb1.Append(" T1.CampusId= ? ")
            sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
        End If

        sb2 = New StringBuilder
        With sb2
            .Append(" Select DISTINCT ")
            .Append("		S.StudentId, ")
            .Append("       T.TermId, ")
            '.Append("       S.FirstName, ")
            '.Append("       S.LastName, ")
            '.Append("       T.Termdescrip, ")
            .Append("       T.StartDate, ")
            .Append("       SE.StuEnrollId  ")
            .Append(" FROM arStudent S, arStuEnrollments SE, arTransferGrades R, arTerm T, ")
            .Append("syCmpGrpCmps t1, syCampuses t2")
            .Append(" WHERE 		S.StudentId=SE.StudentId AND	SE.StuEnrollId=R.StuEnrollId AND ")
            .Append(" t1.CampusId = t2.CampusId AND ")
            .Append(" t1.CampGrpId=T.CampGrpId AND ")
            .Append(" R.TermId=T.TermId ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            .Append(" And  SE.StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")

            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb2.Append(andOrOrOperator)
                sb2.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If
            'studentid
            If Not studentNumber = "" Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" S.studentNumber like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("studentNumber", studentNumber))
                andOrOrOperator = " AND "
            End If
            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb2.Append(andOrOrOperator)
                sb2.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb2.Append(andOrOrOperator)
                sb2.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With

        sb3 = New StringBuilder
        With sb3
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" arStuEnrollments SE, ")
            .Append(" arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE S.StudentId = SE.StudentId ")
            .Append(" and SE.PrgVerId=PV.PrgVerId and PV.IsContinuingEd=1 and T.StartDate >= SE.ExpStartDate ")
            ' .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            ''Added by Saraswathi lakshmanan on March 5 2010
            ''18574: QA: Issue with the terms in the Term dropdown on the Student Search pop ups. 
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            .Append(" And  SE.StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")

            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb3.Append(andOrOrOperator)
                sb3.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If
            'check for studentid
            If Not studentNumber = "" Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" S.studentNumber like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("studentNumber", studentNumber))
                andOrOrOperator = " AND "
            End If
            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb3.Append(andOrOrOperator)
                sb3.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb3.Append(andOrOrOperator)
                sb3.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With



        sb4 = New StringBuilder
        With sb4
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" arStuEnrollments SE, ")
            .Append(" arPrgVersions PV ,arTerm T ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE S.StudentId = SE.StudentId ")
            .Append(" and SE.PrgVerId=PV.PrgVerId and T.StartDate = SE.ExpStartDate ")
            '  .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            ''Added by Saraswathi lakshmanan on March 5 2010
            ''18574: QA: Issue with the terms in the Term dropdown on the Student Search pop ups. 
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            .Append(" And  SE.StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")


            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb4.Append(andOrOrOperator)
                sb4.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If
            'check for studentId
            If Not studentNumber = "" Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" S.studentNumber like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("studentNumber", studentNumber))
                andOrOrOperator = " AND "
            End If
            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb4.Append(andOrOrOperator)
                sb4.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb4.Append(andOrOrOperator)
                sb4.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With

        sb5 = New StringBuilder
        With sb5
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" arStuEnrollments SE,  arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2   WHERE S.StudentId = SE.StudentId  and SE.PrgVerId=PV.PrgVerId ")
            .Append(" and (PV.ProgId=T.ProgId or T.ProgId is null) AND T.StartDate <=SE.ExpStartDate and SE.ExpStartDate <= T.EndDate ")
            '  .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  and t1.CampGrpId=PV.CampGrpId  ")
            ''Added by Saraswathi lakshmanan on March 5 2010
            ''18574: QA: Issue with the terms in the Term dropdown on the Student Search pop ups. 
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            ''Added by saraswathi to get the inschool enrollments
            ''modified on June 10 2009
            ''To fix issue 14829
            ''inschool Status - externship  added 22
            .Append(" And  SE.StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20, 21,22) ) ")


            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "Last Name" = ""
            If Not lastName = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" S.LastName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("LastName", lastName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "First Name" = ""
            If Not firstName = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" S.FirstName like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("FirstName", firstName))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "SSN" = ""
            If Not ssn = "" Then

                'remove spaces, dashes from the search
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                sb5.Append(andOrOrOperator)
                sb5.Append(" S.SSN like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("SSN", ssn))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is enrollmentId = ""
            '   note: EnrollmentId is the ID of the student .. NO the GUID of enrollment
            If Not enrollment = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.EnrollmentId like  + '%' + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("Enrollment", enrollment))
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "Student Status" = ""
            If Not statusId = Guid.Empty.ToString Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.StatusCodeId = ? ")
                sc.Parameters.Add(New OleDbParameter("StatusId", statusId))
                andOrOrOperator = " AND "
            End If

            '   check for PrgVerId
            If Not prgVerId = Guid.Empty.ToString Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.PrgVerId = ? ")
                sc.Parameters.Add(New OleDbParameter("PrgVerId", prgVerId))
                andOrOrOperator = " AND "
            End If
            'check for studentId
            If Not studentNumber = "" Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" S.studentNumber like  + ? + '%'")
                sc.Parameters.Add(New OleDbParameter("studentNumber", studentNumber))
                andOrOrOperator = " AND "
            End If
            '   check if the selection is "Campus Groups" = ""
            If Not campusId = Guid.Empty.ToString Then
                sb5.Append(andOrOrOperator)
                sb5.Append(" SE.CampusId = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
                sb5.Append(andOrOrOperator)
                sb5.Append(" T1.CampusId= ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
            '.Append("ORDER BY S.StudentId, T.TermId, T.StartDate desc ")
        End With
        '   modify select command
        'da.SelectCommand.CommandText = sb.ToString
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString + " Union " + sb2.ToString + " Union " + sb3.ToString + " Union " + sb4.ToString + " Union " + sb5.ToString + " ORDER BY S.StudentId, T.TermId, T.StartDate desc "
        'da.SelectCommand.CommandText = sb2.ToString + sb1.ToString + " ORDER BY S.StudentId, T.TermId, T.StartDate desc "

        '   fill StudentTerms table
        da.Fill(ds, "StudentTerms")

        '   build query to select StudentTerms
        sc.Parameters.Clear()
        sc.CommandText = ""

        sb = New StringBuilder
        With sb

            .Append("SELECT  T.TermId, ")
            .Append("         T.TermDescrip, ")
            .Append("         T.StartDate ")
            .Append(" FROM     arTerm T  ,syCmpGrpCmps t1, syCampuses t2 ")
            .Append(" where t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId ")
            ''Added by Saraswathi lakshmanan on July 9 2009
            ''To fetch the active terms only
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            '.Append("ORDER BY T.StartDate desc ")

            If Not campusId = Guid.Empty.ToString Then
                sb.Append(" and t1.campusid = ? ")
                sc.Parameters.Add(New OleDbParameter("CampusId", campusId))
            End If
        End With


        'Create adapter to handle Students table
        'Dim da As New OleDbDataAdapter(sc1)

        'sc.CommandText = sb.ToString + " ORDER BY T.StartDate desc "
        'sc.Connection = New OleDbConnection(SingletonAppSettings.AppSettings("ConString"))

        ''Create adapter to handle Students table
        'Dim da As New OleDbDataAdapter(sc)

        ''Fill Students table
        'da.Fill(ds, "Terms")

        da.SelectCommand.CommandText = sb.ToString + " ORDER BY T.StartDate desc "

        ' ''   fill Terms table
        da.Fill(ds, "Terms")

        '   create primary and foreign key constraints

        '   set primary key for Students table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("Students").Columns("StudentId")
        ds.Tables("Students").PrimaryKey = pk0

        '   set primary key for Enrollments table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("Enrollments").Columns("PrgVerId")
        ds.Tables("Enrollments").PrimaryKey = pk1

        '   set primary key for Terms table
        Dim pk2(0) As DataColumn
        pk2(0) = ds.Tables("Terms").Columns("TermId")
        ds.Tables("Terms").PrimaryKey = pk2

        '   set foreign key column in StudentEnrollments
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("StudentEnrollments").Columns("StudentId")

        '   set foreign key column in StudentEnrollments
        Dim fk1(0) As DataColumn
        fk1(0) = ds.Tables("StudentEnrollments").Columns("PrgVerId")

        '   set foreign key column in Students
        Dim fk2(0) As DataColumn
        fk2(0) = ds.Tables("StudentTerms").Columns("StudentId")

        '   set foreign key column in Students
        Dim fk3(0) As DataColumn
        fk3(0) = ds.Tables("StudentTerms").Columns("TermId")

        '   set foreign keys in GradeBookResults table
        ds.Relations.Add("StudentsStudentEnrollments", pk0, fk0)
        ds.Relations.Add("EnrollmentsStudentEnrollments", pk1, fk1)
        ds.Relations.Add("StudentStudentTerms", pk0, fk2, False)
        ds.Relations.Add("TermsStudentTerms", pk2, fk3)

        '   return dataset
        Return ds

    End Function


    Function GetIsRoleSystemAdministarator(ByVal UserId As String) As Boolean

        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim count As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '''SysRoleId=1 is the role of System Administrator
        ''' 
        With sb
            .Append("select Count(roleId) from syRoles where roleId in ")
            .Append("(select roleId from syUsersRolesCampGrps where ")
            .Append("UserId = ? ")
            .Append("and CampgrpId in (select CampgrpId ")
            .Append("from syCmpGrpCmps where CampusId=(Select CampusId from ")
            .Append("syUsers where UserId= ? )))")
            .Append(" and  SysRoleId=1 ")
        End With

        db.AddParameter("@UserId", UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@UserId", UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        count = db.RunParamSQLScalar(sb.ToString)

        If count = 0 Then
            Return False
        Else
            Return True
        End If

    End Function
    Public Function GetStudentSearchDSWithStudentNumberMRU(ByVal dsMRU As DataSet, ByVal CampusId As String) As DataSet

        '   create dataset
        Dim ds As New DataSet

        '   build select command
        Dim sc As New OleDbCommand
        Dim sc1 As New OleDbCommand

        '   build query to select students
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder
        Dim sb2 As New StringBuilder
        Dim sb3 As New StringBuilder
        Dim sb4 As New StringBuilder
        Dim sb5 As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim StudentIds As String = ""
        For Each dr As DataRow In dsMRU.Tables("MRUList").Rows
            If StudentIds = "" Then
                StudentIds = "'" & dr("ChildId").ToString & "'"
            Else
                StudentIds = StudentIds & ",'" & dr("ChildId").ToString & "'"
            End If
        Next


        With sb
            .Append("SELECT DISTINCT ")
            .Append("       S.StudentId, ")
            .Append("       S.LastName, ")
            .Append("       S.FirstName, ")
            .Append("       S.SSN, ")
            .Append(" (select Top 1  t3.PrgVerId from arStudent t2,arStuEnrollments t3 where t2.StudentId = S.StudentId and t2.StudentId=t3.StudentId) as PrgVerId, ")
            .Append(" (select Top 1  t1.PrgVerDescrip from arPrgVersions t1,arStudent t2,arStuEnrollments t3 where t2.StudentId = S.StudentId and t2.StudentId=t3.StudentId and t3.PrgVerId=t1.PrgVerId) as PrgVerName, ")
            .Append("       (SELECT TOP 1 ")
            .Append("                T1.TermId ")
            .Append("       FROM arStudent S1, arStuEnrollments SE1, arResults R1, arClassSections CS1, arTerm T1")
            .Append("       WHERE ")
            .Append("               S1.StudentId = SE1.StudentId ")
            .Append("       AND		SE1.StuEnrollId=R1.StuEnrollId ")
            .Append("       AND		R1.TestId=CS1.ClsSectionId ")
            .Append("       AND		CS1.TermId=T1.TermId ")
            .Append(" and T1.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append("       AND		S1.StudentId=S.StudentId ")
            .Append("       ORDER BY T1.StartDate desc) As CurrentTerm, ")
            .Append("       S.StudentStatus as Status ")
            .Append("FROM   arStudent S , arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
        End With

        'Build select command
        sc.CommandText = sb.ToString + sb1.ToString
        sc.Connection = New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))

        'Create adapter to handle Students table
        Dim da As New OleDbDataAdapter(sc)
        da.SelectCommand.CommandTimeout = 600
        'Fill Students table
        da.Fill(ds, "Students")

        'Build query to select students enrollments
        sb = New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       SE.StuEnrollId, ")
            .Append("       SE.StudentId, ")
            .Append("       SE.ExpStartDate AS StartDate, ")
            .Append("       SE.PrgVerId ")
            .Append("FROM   arStudent S, arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
        End With

        'Modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString + " ORDER BY SE.StudentId, StartDate desc "

        'Fill StudentEnrollments table
        da.Fill(ds, "StudentEnrollments")

        '   build query to select enrollments for selected students
        sb = New StringBuilder
        With sb
            .Append("SELECT DISTINCT")
            .Append("       SE.PrgVerId, ")
            .Append("       (Select PrgVerDescrip from arPrgVersions where PrgVerId=SE.PrgVerId) As Enrollment ")
            .Append("FROM   arStudent S, arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString

        '   fill Enrollments table
        da.Fill(ds, "Enrollments")

        '   build query to select StudentTerms
        sb = New StringBuilder
        With sb
            .Append("Select DISTINCT ")
            .Append("		S.StudentId, ")
            .Append("       T.TermId, ")
            .Append("       T.StartDate, ")
            .Append("       SE.StuEnrollId  ")
            .Append("FROM arStudent S, arStuEnrollments SE, arResults R, arClassSections CS, arClassSectionTerms CST, arTerm T, ")
            .Append("syCmpGrpCmps t1, syCampuses t2 ")
            .Append("WHERE ")
            .Append("		S.StudentId=SE.StudentId ")
            .Append("AND	SE.StuEnrollId=R.StuEnrollId ")
            .Append("AND	R.TestId=CS.ClsSectionId ")
            .Append("AND    CST.ClsSectionId=CS.ClsSectionId ")
            .Append("AND	CST.TermId=T.TermId ")
            .Append("AND    t1.CampusId = t2.CampusId ")
            .Append("AND    t1.CampGrpId=T.CampGrpId")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
            .Append(" And T1.CampusId='" & CampusId & "'")
        End With

        sb2 = New StringBuilder
        With sb2
            .Append(" Select DISTINCT ")
            .Append("		S.StudentId, ")
            .Append("       T.TermId, ")
            .Append("       T.StartDate, ")
            .Append("       SE.StuEnrollId  ")
            .Append(" FROM arStudent S, arStuEnrollments SE, arTransferGrades R, arTerm T, ")
            .Append("syCmpGrpCmps t1, syCampuses t2")
            .Append(" WHERE 		S.StudentId=SE.StudentId AND	SE.StuEnrollId=R.StuEnrollId AND ")
            .Append(" t1.CampusId = t2.CampusId AND ")
            .Append(" t1.CampGrpId=T.CampGrpId AND ")
            .Append(" R.TermId=T.TermId ")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
            .Append(" And T1.CampusId='" & CampusId & "'")

        End With

        sb3 = New StringBuilder
        With sb3
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" arStuEnrollments SE, ")
            .Append(" arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE S.StudentId = SE.StudentId ")
            .Append(" and SE.PrgVerId=PV.PrgVerId and PV.IsContinuingEd=1 and T.StartDate >= SE.ExpStartDate ")
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
            .Append(" And T1.CampusId='" & CampusId & "'")
        End With



        sb4 = New StringBuilder
        With sb4
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" arStuEnrollments SE, ")
            .Append(" arPrgVersions PV ,arTerm T ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE S.StudentId = SE.StudentId ")
            .Append(" and SE.PrgVerId=PV.PrgVerId and T.StartDate = SE.ExpStartDate ")
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
            .Append(" And T1.CampusId='" & CampusId & "'")
        End With

        sb5 = New StringBuilder
        With sb5
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" arStuEnrollments SE,  arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2   WHERE S.StudentId = SE.StudentId  and SE.PrgVerId=PV.PrgVerId ")
            .Append(" and (PV.ProgId=T.ProgId or T.ProgId is null) AND T.StartDate <=SE.ExpStartDate and SE.ExpStartDate <= T.EndDate ")
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")

            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
            .Append(" And T1.CampusId='" & CampusId & "'")
        End With
        '   modify select command
        'da.SelectCommand.CommandText = sb.ToString
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString + " Union " + sb2.ToString + " Union " + sb3.ToString + " Union " + sb4.ToString + " Union " + sb5.ToString + " ORDER BY S.StudentId, T.TermId, T.StartDate desc "
        'da.SelectCommand.CommandText = sb2.ToString + sb1.ToString + " ORDER BY S.StudentId, T.TermId, T.StartDate desc "

        '   fill StudentTerms table
        da.Fill(ds, "StudentTerms")

        '   build query to select StudentTerms
        sc.Parameters.Clear()
        sc.CommandText = ""

        sb = New StringBuilder
        With sb

            .Append("SELECT  T.TermId, ")
            .Append("         T.TermDescrip, ")
            .Append("         T.StartDate ")
            .Append(" FROM     arTerm T  ,syCmpGrpCmps t1, syCampuses t2 ")
            .Append(" where t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId ")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And T1.CampusId='" & CampusId & "'")
        End With

        da.SelectCommand.CommandText = sb.ToString + " ORDER BY T.StartDate desc "
        da.SelectCommand.CommandTimeout = 600
        ' ''   fill Terms table
        da.Fill(ds, "Terms")

        '   create primary and foreign key constraints

        '   set primary key for Students table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("Students").Columns("StudentId")
        ds.Tables("Students").PrimaryKey = pk0

        '   set primary key for Enrollments table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("Enrollments").Columns("PrgVerId")
        ds.Tables("Enrollments").PrimaryKey = pk1

        '   set primary key for Terms table
        Dim pk2(0) As DataColumn
        pk2(0) = ds.Tables("Terms").Columns("TermId")
        ds.Tables("Terms").PrimaryKey = pk2

        '   set foreign key column in StudentEnrollments
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("StudentEnrollments").Columns("StudentId")

        '   set foreign key column in StudentEnrollments
        Dim fk1(0) As DataColumn
        fk1(0) = ds.Tables("StudentEnrollments").Columns("PrgVerId")

        '   set foreign key column in Students
        Dim fk2(0) As DataColumn
        fk2(0) = ds.Tables("StudentTerms").Columns("StudentId")

        '   set foreign key column in Students
        Dim fk3(0) As DataColumn
        fk3(0) = ds.Tables("StudentTerms").Columns("TermId")

        '   set foreign keys in GradeBookResults table
        ds.Relations.Add("StudentsStudentEnrollments", pk0, fk0)
        ds.Relations.Add("EnrollmentsStudentEnrollments", pk1, fk1)
        ds.Relations.Add("StudentStudentTerms", pk0, fk2, False)
        ds.Relations.Add("TermsStudentTerms", pk2, fk3)

        '   return dataset
        Return ds

    End Function
    Public Function GetStudentSearchDSWithStudentNumberforcurrentEnrollmentMRU(ByVal dsMRU As DataSet, ByVal CampusId As String) As DataSet

        '   create dataset
        Dim ds As New DataSet

        '   build select command
        Dim sc As New OleDbCommand
        Dim sc1 As New OleDbCommand

        '   build query to select students
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder
        Dim sb2 As New StringBuilder
        Dim sb3 As New StringBuilder
        Dim sb4 As New StringBuilder
        Dim sb5 As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim StudentIds As String = ""
        For Each dr As DataRow In dsMRU.Tables("MRUList").Rows
            If StudentIds = "" Then
                StudentIds = "'" & dr("ChildId").ToString & "'"
            Else
                StudentIds = StudentIds & ",'" & dr("ChildId").ToString & "'"
            End If
        Next

        With sb
            .Append("SELECT DISTINCT ")
            .Append("       S.StudentId, ")
            .Append("       S.LastName, ")
            .Append("       S.FirstName, ")
            .Append("       S.SSN, ")
            .Append(" (select Top 1  t3.PrgVerId from arStudent t2,")
            .Append("( ( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))))  t3 ")
            .Append(" where t2.StudentId = S.StudentId and t2.StudentId=t3.StudentId) as PrgVerId, ")
            .Append(" (select Top 1  t1.PrgVerDescrip from arPrgVersions t1,arStudent t2,")
            .Append("( ( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))))  t3 ")
            .Append(" where t2.StudentId = S.StudentId and t2.StudentId=t3.StudentId and t3.PrgVerId=t1.PrgVerId) as PrgVerName, ")
            .Append("       (SELECT TOP 1 ")
            .Append("                T1.TermId ")
            .Append("       FROM arStudent S1, ")
            .Append("( ( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))))  SE1 ")
            .Append(" , arResults R1, arClassSections CS1, arTerm T1")
            .Append("       WHERE ")
            .Append("               S1.StudentId = SE1.StudentId ")
            .Append("       AND		SE1.StuEnrollId=R1.StuEnrollId ")
            .Append("       AND		R1.TestId=CS1.ClsSectionId ")
            .Append("       AND		CS1.TermId=T1.TermId ")
            .Append("       AND		S1.StudentId=S.StudentId ")
            .Append(" and T1.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append("       ORDER BY T1.StartDate desc) As CurrentTerm, ")
            .Append("       S.StudentStatus as Status ")
            .Append("FROM   arStudent S ,")
            .Append("( ( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
        End With

        'Build select command
        sc.CommandText = sb.ToString + sb1.ToString
        sc.Connection = New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))

        'Create adapter to handle Students table
        Dim da As New OleDbDataAdapter(sc)
        da.SelectCommand.CommandTimeout = 600
        'Fill Students table
        da.Fill(ds, "Students")

        'Build query to select students enrollments
        sb = New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       SE.StuEnrollId, ")
            .Append("       SE.StudentId, ")
            .Append("       SE.ExpStartDate AS StartDate, ")
            .Append("       SE.PrgVerId ")
            .Append("FROM   arStudent S, ")
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
        End With

        'Modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString + " ORDER BY SE.StudentId, StartDate desc "

        'Fill StudentEnrollments table
        da.Fill(ds, "StudentEnrollments")

        '   build query to select enrollments for selected students
        sb = New StringBuilder
        With sb
            .Append("SELECT DISTINCT")
            .Append("       SE.PrgVerId, ")
            .Append("       (Select PrgVerDescrip from arPrgVersions where PrgVerId=SE.PrgVerId) As Enrollment ")
            .Append("FROM   arStudent S, ")
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))))  SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString

        '   fill Enrollments table
        da.Fill(ds, "Enrollments")

        '   build query to select StudentTerms
        sb = New StringBuilder
        With sb
            .Append("Select DISTINCT ")
            .Append("		S.StudentId, ")
            .Append("       T.TermId, ")
            .Append("       T.StartDate, ")
            .Append("       SE.StuEnrollId  ")
            .Append("FROM arStudent S, ")
            .Append("( ( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE ")
            .Append(" , arResults R, arClassSections CS, arClassSectionTerms CST, arTerm T, ")
            .Append("syCmpGrpCmps t1, syCampuses t2 ")
            .Append("WHERE ")
            .Append("		S.StudentId=SE.StudentId ")
            .Append("AND	SE.StuEnrollId=R.StuEnrollId ")
            .Append("AND	R.TestId=CS.ClsSectionId ")
            .Append("AND    CST.ClsSectionId=CS.ClsSectionId ")
            .Append("AND	CST.TermId=T.TermId ")
            .Append("AND    t1.CampusId = t2.CampusId ")
            .Append("AND    t1.CampGrpId=T.CampGrpId")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
            .Append(" And T1.CampusId='" & CampusId & "'")
        End With

        sb2 = New StringBuilder
        With sb2
            .Append(" Select DISTINCT ")
            .Append("		S.StudentId, ")
            .Append("       T.TermId, ")
            .Append("       T.StartDate, ")
            .Append("       SE.StuEnrollId  ")
            .Append(" FROM arStudent S, ")
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE ")
            .Append(" , arTransferGrades R, arTerm T, ")
            .Append("syCmpGrpCmps t1, syCampuses t2")
            .Append(" WHERE 		S.StudentId=SE.StudentId AND	SE.StuEnrollId=R.StuEnrollId AND ")
            .Append(" t1.CampusId = t2.CampusId AND ")
            .Append(" t1.CampGrpId=T.CampGrpId AND ")
            .Append(" R.TermId=T.TermId ")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
            .Append(" And T1.CampusId='" & CampusId & "'")
        End With

        sb3 = New StringBuilder
        With sb3
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE ")
            .Append(" ,arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE S.StudentId = SE.StudentId ")
            .Append(" and SE.PrgVerId=PV.PrgVerId and PV.IsContinuingEd=1 and T.StartDate >= SE.ExpStartDate ")
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
            .Append(" And T1.CampusId='" & CampusId & "'")
        End With

        sb4 = New StringBuilder
        With sb4
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append("( ( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))))  SE ")
            .Append(" ,arPrgVersions PV ,arTerm T ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE S.StudentId = SE.StudentId ")
            .Append(" and SE.PrgVerId=PV.PrgVerId and T.StartDate = SE.ExpStartDate ")
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
            .Append(" And T1.CampusId='" & CampusId & "'")
        End With

        sb5 = New StringBuilder
        With sb5
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE ")
            .Append(" ,  arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2   WHERE S.StudentId = SE.StudentId  and SE.PrgVerId=PV.PrgVerId ")
            .Append(" and (PV.ProgId=T.ProgId or T.ProgId is null) AND T.StartDate <=SE.ExpStartDate and SE.ExpStartDate <= T.EndDate ")
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
            .Append(" And T1.CampusId='" & CampusId & "'")
        End With
        '   modify select command
        'da.SelectCommand.CommandText = sb.ToString
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString + " Union " + sb2.ToString + " Union " + sb3.ToString + " Union " + sb4.ToString + " Union " + sb5.ToString + " ORDER BY S.StudentId, T.TermId, T.StartDate desc "
        'da.SelectCommand.CommandText = sb2.ToString + sb1.ToString + " ORDER BY S.StudentId, T.TermId, T.StartDate desc "

        '   fill StudentTerms table
        da.Fill(ds, "StudentTerms")

        '   build query to select StudentTerms
        sc.Parameters.Clear()
        sc.CommandText = ""

        sb = New StringBuilder
        With sb

            .Append("SELECT  T.TermId, ")
            .Append("         T.TermDescrip, ")
            .Append("         T.StartDate ")
            .Append(" FROM     arTerm T  ,syCmpGrpCmps t1, syCampuses t2 ")
            .Append(" where t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId ")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And T1.CampusId='" & CampusId & "'")
        End With

        da.SelectCommand.CommandText = sb.ToString + " ORDER BY T.StartDate desc "

        ' ''   fill Terms table
        da.Fill(ds, "Terms")

        '   create primary and foreign key constraints

        '   set primary key for Students table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("Students").Columns("StudentId")
        ds.Tables("Students").PrimaryKey = pk0

        '   set primary key for Enrollments table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("Enrollments").Columns("PrgVerId")
        ds.Tables("Enrollments").PrimaryKey = pk1

        '   set primary key for Terms table
        Dim pk2(0) As DataColumn
        pk2(0) = ds.Tables("Terms").Columns("TermId")
        ds.Tables("Terms").PrimaryKey = pk2

        '   set foreign key column in StudentEnrollments
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("StudentEnrollments").Columns("StudentId")

        '   set foreign key column in StudentEnrollments
        Dim fk1(0) As DataColumn
        fk1(0) = ds.Tables("StudentEnrollments").Columns("PrgVerId")

        '   set foreign key column in Students
        Dim fk2(0) As DataColumn
        fk2(0) = ds.Tables("StudentTerms").Columns("StudentId")

        '   set foreign key column in Students
        Dim fk3(0) As DataColumn
        fk3(0) = ds.Tables("StudentTerms").Columns("TermId")

        '   set foreign keys in GradeBookResults table
        ds.Relations.Add("StudentsStudentEnrollments", pk0, fk0)
        ds.Relations.Add("EnrollmentsStudentEnrollments", pk1, fk1)
        ds.Relations.Add("StudentStudentTerms", pk0, fk2, False)
        ds.Relations.Add("TermsStudentTerms", pk2, fk3)

        '   return dataset
        Return ds

    End Function
    Public Function GetStudentSearchDSMRU(ByVal dsMRU As DataSet, ByVal CampusId As String) As DataSet

        '   create datasetb
        Dim ds As New DataSet

        '   build select command
        Dim sc As New OleDbCommand
        Dim sc1 As New OleDbCommand

        '   build query to select students
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder
        Dim sb2 As New StringBuilder
        Dim sb3 As New StringBuilder
        Dim sb4 As New StringBuilder
        Dim sb5 As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim StudentIds As String = ""
        For Each dr As DataRow In dsMRU.Tables("MRUList").Rows
            If StudentIds = "" Then
                StudentIds = "'" & dr("ChildId").ToString & "'"
            Else
                StudentIds = StudentIds & ",'" & dr("ChildId").ToString & "'"
            End If
        Next

        With sb
            .Append("SELECT DISTINCT ")
            .Append("       S.StudentId, ")
            .Append("       S.LastName, ")
            .Append("       S.FirstName, ")
            .Append("       S.SSN, ")
            .Append(" (select Top 1  t3.PrgVerId from arStudent t2,arStuEnrollments t3 where t2.StudentId = S.StudentId and t2.StudentId=t3.StudentId) as PrgVerId, ")
            .Append(" (select Top 1  t1.PrgVerDescrip from arPrgVersions t1,arStudent t2,arStuEnrollments t3 where t2.StudentId = S.StudentId and t2.StudentId=t3.StudentId and t3.PrgVerId=t1.PrgVerId) as PrgVerName, ")
            .Append("       (SELECT TOP 1 ")
            .Append("                T1.TermId ")
            .Append("       FROM arStudent S1, arStuEnrollments SE1, arResults R1, arClassSections CS1, arTerm T1")
            .Append("       WHERE ")
            .Append("               S1.StudentId = SE1.StudentId ")
            .Append("       AND		SE1.StuEnrollId=R1.StuEnrollId ")
            .Append("       AND		R1.TestId=CS1.ClsSectionId ")
            .Append("       AND		CS1.TermId=T1.TermId ")
            .Append("       AND		S1.StudentId=S.StudentId ")
            .Append(" and T1.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append("       ORDER BY T1.StartDate desc) As CurrentTerm, ")
            .Append("       S.StudentStatus as Status ")
            .Append("FROM   arStudent S , arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
        End With

        'Build select command
        sc.CommandText = sb.ToString + sb1.ToString
        sc.Connection = New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))

        'Create adapter to handle Students table
        Dim da As New OleDbDataAdapter(sc)
        da.SelectCommand.CommandTimeout = 600
        'Fill Students table
        da.Fill(ds, "Students")

        'Build query to select students enrollments
        sb = New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       SE.StuEnrollId, ")
            .Append("       SE.StudentId, ")
            .Append("       SE.ExpStartDate AS StartDate, ")
            .Append("       SE.PrgVerId ")
            .Append("FROM   arStudent S, arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
        End With

        'Modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString + " ORDER BY SE.StudentId, StartDate desc "

        'Fill StudentEnrollments table
        da.Fill(ds, "StudentEnrollments")

        '   build query to select enrollments for selected students
        sb = New StringBuilder
        With sb
            .Append("SELECT DISTINCT")
            .Append("       SE.PrgVerId, ")
            .Append("       (Select PrgVerDescrip from arPrgVersions where PrgVerId=SE.PrgVerId) As Enrollment ")
            .Append("FROM   arStudent S, arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString

        '   fill Enrollments table
        da.Fill(ds, "Enrollments")

        '   build query to select StudentTerms
        sb = New StringBuilder
        With sb
            .Append("Select DISTINCT ")
            .Append("		S.StudentId, ")
            .Append("       T.TermId, ")
            .Append("       T.StartDate, ")
            .Append("       SE.StuEnrollId  ")
            .Append("FROM arStudent S, arStuEnrollments SE, arResults R, arClassSections CS, arClassSectionTerms CST, arTerm T, ")
            .Append("syCmpGrpCmps t1, syCampuses t2 ")
            .Append("WHERE ")
            .Append("		S.StudentId=SE.StudentId ")
            .Append("AND	SE.StuEnrollId=R.StuEnrollId ")
            .Append("AND	R.TestId=CS.ClsSectionId ")
            .Append("AND    CST.ClsSectionId=CS.ClsSectionId ")
            .Append("AND	CST.TermId=T.TermId ")
            .Append("AND    t1.CampusId = t2.CampusId ")
            .Append("AND    t1.CampGrpId=T.CampGrpId")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
            .Append(" And T1.CampusId='" & CampusId & "'")
        End With

        sb2 = New StringBuilder
        With sb2
            .Append(" Select DISTINCT ")
            .Append("		S.StudentId, ")
            .Append("       T.TermId, ")
            .Append("       T.StartDate, ")
            .Append("       SE.StuEnrollId  ")
            .Append(" FROM arStudent S, arStuEnrollments SE, arTransferGrades R, arTerm T, ")
            .Append("syCmpGrpCmps t1, syCampuses t2")
            .Append(" WHERE 		S.StudentId=SE.StudentId AND ")
            .Append(" SE.StuEnrollId = R.StuEnrollId AND ")
            .Append(" t1.CampusId = t2.CampusId AND ")
            .Append(" t1.CampGrpId=T.CampGrpId AND ")
            .Append(" R.TermId=T.TermId ")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")

            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
            .Append(" And T1.CampusId='" & CampusId & "'")
        End With

        sb3 = New StringBuilder
        With sb3
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" arStuEnrollments SE, ")
            .Append(" arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE S.StudentId = SE.StudentId ")
            .Append(" and SE.PrgVerId=PV.PrgVerId and PV.IsContinuingEd=1 and T.StartDate >= SE.ExpStartDate ")
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
            .Append(" And T1.CampusId='" & CampusId & "'")
            ''Added by Saraswathi Lakshmanan on Feb 2 2011 to fix rally case DE4967 Name: QA: Terms in the student search pop up pages shows terms that does not belong to the student's program. 
            .Append("  AND (T.ProgID=PV.ProgId OR T.progId IS NULL) ")
        End With



        sb4 = New StringBuilder
        With sb4
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" arStuEnrollments SE, ")
            .Append(" arPrgVersions PV ,arTerm T ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE S.StudentId = SE.StudentId ")
            .Append(" and SE.PrgVerId=PV.PrgVerId and T.StartDate = SE.ExpStartDate ")
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
            .Append(" And T1.CampusId='" & CampusId & "'")
            ''Added by Saraswathi Lakshmanan on Feb 2 2011 to fix rally case DE4967 Name: QA: Terms in the student search pop up pages shows terms that does not belong to the student's program. 
            .Append("  AND (T.ProgID=PV.ProgId OR T.progId IS NULL) ")
        End With

        sb5 = New StringBuilder
        With sb5
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" arStuEnrollments SE,  arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2   WHERE S.StudentId = SE.StudentId  and SE.PrgVerId=PV.PrgVerId ")
            .Append(" and (PV.ProgId=T.ProgId or T.ProgId is null) AND T.StartDate <=SE.ExpStartDate and SE.ExpStartDate <= T.EndDate ")
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
            .Append(" And T1.CampusId='" & CampusId & "'")
            ''Added by Saraswathi Lakshmanan on Feb 2 2011 to fix rally case DE4967 Name: QA: Terms in the student search pop up pages shows terms that does not belong to the student's program. 
            .Append("  AND (T.ProgID=PV.ProgId OR T.progId IS NULL) ")


        End With
        '   modify select command
        'da.SelectCommand.CommandText = sb.ToString
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString + " Union " + sb2.ToString + " Union " + sb3.ToString + " Union " + sb4.ToString + " Union " + sb5.ToString + " ORDER BY S.StudentId, T.TermId, T.StartDate desc "
        'da.SelectCommand.CommandText = sb2.ToString + sb1.ToString + " ORDER BY S.StudentId, T.TermId, T.StartDate desc "

        '   fill StudentTerms table
        da.Fill(ds, "StudentTerms")

        '   build query to select StudentTerms
        sc.Parameters.Clear()
        sc.CommandText = ""

        sb = New StringBuilder
        With sb

            .Append("SELECT  T.TermId, ")
            .Append("         T.TermDescrip, ")
            .Append("         T.StartDate ")
            .Append(" FROM     arTerm T  ,syCmpGrpCmps t1, syCampuses t2 ")
            .Append(" where t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId ")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And T1.CampusId='" & CampusId & "'")
        End With

        da.SelectCommand.CommandText = sb.ToString + " ORDER BY T.StartDate desc "

        ' ''   fill Terms table
        da.Fill(ds, "Terms")

        '   create primary and foreign key constraints

        '   set primary key for Students table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("Students").Columns("StudentId")
        ds.Tables("Students").PrimaryKey = pk0

        '   set primary key for Enrollments table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("Enrollments").Columns("PrgVerId")
        ds.Tables("Enrollments").PrimaryKey = pk1

        '   set primary key for Terms table
        Dim pk2(0) As DataColumn
        pk2(0) = ds.Tables("Terms").Columns("TermId")
        ds.Tables("Terms").PrimaryKey = pk2

        '   set foreign key column in StudentEnrollments
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("StudentEnrollments").Columns("StudentId")

        '   set foreign key column in StudentEnrollments
        Dim fk1(0) As DataColumn
        fk1(0) = ds.Tables("StudentEnrollments").Columns("PrgVerId")

        '   set foreign key column in Students
        Dim fk2(0) As DataColumn
        fk2(0) = ds.Tables("StudentTerms").Columns("StudentId")

        '   set foreign key column in Students
        Dim fk3(0) As DataColumn
        fk3(0) = ds.Tables("StudentTerms").Columns("TermId")

        '   set foreign keys in GradeBookResults table
        ds.Relations.Add("StudentsStudentEnrollments", pk0, fk0)
        ds.Relations.Add("EnrollmentsStudentEnrollments", pk1, fk1)
        ds.Relations.Add("StudentStudentTerms", pk0, fk2, False)
        ds.Relations.Add("TermsStudentTerms", pk2, fk3)

        '   return dataset
        Return ds

    End Function
    Public Function GetStudentSearchDSForInSchoolEnrollmentMRU(ByVal dsMRU As DataSet, ByVal CampusId As String) As DataSet

        '   create datasetb
        Dim ds As New DataSet

        '   build select command
        Dim sc As New OleDbCommand
     
        '   build query to select students
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder
        Dim sb2 As StringBuilder
        Dim sb3 As StringBuilder
        Dim sb4 As StringBuilder
        Dim sb5 As StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim StudentIds As String = ""
        For Each dr As DataRow In dsMRU.Tables("MRUList").Rows
            If StudentIds = "" Then
                StudentIds = "'" & dr("ChildId").ToString & "'"
            Else
                StudentIds = StudentIds & ",'" & dr("ChildId").ToString & "'"
            End If
        Next

        With sb
            .Append("SELECT DISTINCT ")
            .Append("       S.StudentId, ")
            .Append("       S.LastName, ")
            .Append("       S.FirstName, ")
            .Append("       S.SSN, ")
            .Append(" (select Top 1  t3.PrgVerId from arStudent t2, ")
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))))  t3 ")
            .Append(" where t2.StudentId = S.StudentId and t2.StudentId=t3.StudentId) as PrgVerId, ")
            .Append(" (select Top 1  t1.PrgVerDescrip from arPrgVersions t1,arStudent t2, ")
            .Append("( ( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) t3 ")
            .Append("  where t2.StudentId = S.StudentId and t2.StudentId=t3.StudentId and t3.PrgVerId=t1.PrgVerId) as PrgVerName, ")
            .Append("       (SELECT TOP 1 ")
            .Append("                T1.TermId ")
            .Append("       FROM arStudent S1, ")
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))))  SE1 ")
            .Append(" , arResults R1, arClassSections CS1, arTerm T1")
            .Append("       WHERE ")
            .Append("               S1.StudentId = SE1.StudentId ")
            .Append("       AND		SE1.StuEnrollId=R1.StuEnrollId ")
            .Append("       AND		R1.TestId=CS1.ClsSectionId ")
            .Append("       AND		CS1.TermId=T1.TermId ")
            .Append("       AND		S1.StudentId=S.StudentId ")
            .Append(" and T1.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append("       ORDER BY T1.StartDate desc) As CurrentTerm, ")
            .Append("       S.StudentStatus as Status ")
            .Append("FROM   arStudent S ,  ")
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))))  SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
        End With

        'Build select command
        sc.CommandText = sb.ToString + sb1.ToString
        sc.Connection = New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))

        'Create adapter to handle Students table
        Dim da As New OleDbDataAdapter(sc)
        da.SelectCommand.CommandTimeout = 600
        'Fill Students table
        da.Fill(ds, "Students")

        'Build query to select students enrollments
        sb = New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       SE.StuEnrollId, ")
            .Append("       SE.StudentId, ")
            .Append("       SE.ExpStartDate AS StartDate, ")
            .Append("       SE.PrgVerId ")
            .Append("FROM   arStudent S,  ")
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))))  SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
        End With

        'Modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString + " ORDER BY SE.StudentId, StartDate desc "

        'Fill StudentEnrollments table
        da.Fill(ds, "StudentEnrollments")

        '   build query to select enrollments for selected students
        sb = New StringBuilder
        With sb
            .Append("SELECT DISTINCT")
            .Append("       SE.PrgVerId, ")
            .Append("       (Select PrgVerDescrip from arPrgVersions where PrgVerId=SE.PrgVerId) As Enrollment ")
            .Append("FROM   arStudent S,  ")
            .Append("( ( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE ")
            .Append("WHERE  ")
            .Append("       S.StudentId=SE.StudentId ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString

        '   fill Enrollments table
        da.Fill(ds, "Enrollments")

        '   build query to select StudentTerms
        sb = New StringBuilder
        With sb
            .Append("Select DISTINCT ")
            .Append("		S.StudentId, ")
            .Append("       T.TermId, ")
            .Append("       T.StartDate, ")
            .Append("       SE.StuEnrollId  ")
            .Append("FROM arStudent S,  ")
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE ")
            .Append(" , arResults R, arClassSections CS, arClassSectionTerms CST, arTerm T, ")
            .Append("syCmpGrpCmps t1, syCampuses t2 ")
            .Append("WHERE ")
            .Append("		S.StudentId=SE.StudentId ")
            .Append("AND	SE.StuEnrollId=R.StuEnrollId ")
            .Append("AND	R.TestId=CS.ClsSectionId ")
            .Append("AND    CST.ClsSectionId=CS.ClsSectionId ")
            .Append("AND	CST.TermId=T.TermId ")
            .Append("AND    t1.CampusId = t2.CampusId ")
            .Append("AND    t1.CampGrpId=T.CampGrpId")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
            .Append(" And T1.CampusId='" & CampusId & "'")
        End With

        sb2 = New StringBuilder
        With sb2
            .Append(" Select DISTINCT ")
            .Append("		S.StudentId, ")
            .Append("       T.TermId, ")
            .Append("       T.StartDate, ")
            .Append("       SE.StuEnrollId  ")
            .Append("FROM arStudent S,  ")
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))))  SE ")
            .Append(" , arTransferGrades R, arTerm T, ")
            .Append("syCmpGrpCmps t1, syCampuses t2")
            .Append(" WHERE 		S.StudentId=SE.StudentId AND ")
            .Append(" SE.StuEnrollId = R.StuEnrollId AND ")
            .Append(" t1.CampusId = t2.CampusId AND ")
            .Append(" t1.CampGrpId=T.CampGrpId AND ")
            .Append(" R.TermId=T.TermId ")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
            .Append(" And T1.CampusId='" & CampusId & "'")
        End With

        sb3 = New StringBuilder
        With sb3
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE, ")
            .Append(" arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE S.StudentId = SE.StudentId ")
            .Append(" and SE.PrgVerId=PV.PrgVerId and PV.IsContinuingEd=1 and T.StartDate >= SE.ExpStartDate ")
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
            .Append(" And T1.CampusId='" & CampusId & "'")
            ''Added by Saraswathi Lakshmanan on Feb 2 2011 to fix rally case DE4967 Name: QA: Terms in the student search pop up pages shows terms that does not belong to the student's program. 
            .Append("  AND (T.ProgID=PV.ProgId OR T.progId IS NULL) ")
        End With



        sb4 = New StringBuilder
        With sb4
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE, ")
            .Append(" arPrgVersions PV ,arTerm T ,syCmpGrpCmps t1, syCampuses t2  ")
            .Append(" WHERE S.StudentId = SE.StudentId ")
            .Append(" and SE.PrgVerId=PV.PrgVerId and T.StartDate = SE.ExpStartDate ")
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
            .Append(" And T1.CampusId='" & CampusId & "'")
            ''Added by Saraswathi Lakshmanan on Feb 2 2011 to fix rally case DE4967 Name: QA: Terms in the student search pop up pages shows terms that does not belong to the student's program. 
            .Append("  AND (T.ProgID=PV.ProgId OR T.progId IS NULL) ")
        End With

        sb5 = New StringBuilder
        With sb5
            .Append(" Select DISTINCT 		S.StudentId,        T.TermId,        T.StartDate,        SE.StuEnrollId  FROM arStudent S, ")
            .Append(" (( Select * from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))) ")
            .Append(" union  ( Select * from arStuEnrollMents where   StatusCodeId not in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22))and ")
            .Append(" StudentID not in   (Select StudentID from arStuEnrollMents where StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22)))) ) SE, ")
            .Append("  arPrgVersions PV ,arTerm T  ,syCmpGrpCmps t1, syCampuses t2   WHERE S.StudentId = SE.StudentId  and SE.PrgVerId=PV.PrgVerId ")
            .Append(" and (PV.ProgId=T.ProgId or T.ProgId is null) AND T.StartDate <=SE.ExpStartDate and SE.ExpStartDate <= T.EndDate ")
            .Append(" AND   t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId  ")
            .Append(" and (t1.CampGrpId=PV.CampGrpId  Or  PV.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL') Or  T1.CampGrpId in (Select CampGrpId from syCampGrps where CampGrpDescrip='ALL')) ")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And S.StudentId in (" & StudentIds & ")")
            .Append(" And SE.CampusId='" & CampusId & "'")
            .Append(" And T1.CampusId='" & CampusId & "'")
            ''Added by Saraswathi Lakshmanan on Feb 2 2011 to fix rally case DE4967 Name: QA: Terms in the student search pop up pages shows terms that does not belong to the student's program. 
            .Append("  AND (T.ProgID=PV.ProgId OR T.progId IS NULL) ")
        End With
        '   modify select command
        'da.SelectCommand.CommandText = sb.ToString
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString + " Union " + sb2.ToString + " Union " + sb3.ToString + " Union " + sb4.ToString + " Union " + sb5.ToString + " ORDER BY S.StudentId, T.TermId, T.StartDate desc "
        'da.SelectCommand.CommandText = sb2.ToString + sb1.ToString + " ORDER BY S.StudentId, T.TermId, T.StartDate desc "

        '   fill StudentTerms table
        da.Fill(ds, "StudentTerms")

        '   build query to select StudentTerms
        sc.Parameters.Clear()
        sc.CommandText = ""

        sb = New StringBuilder
        With sb

            .Append("SELECT  T.TermId, ")
            .Append("         T.TermDescrip, ")
            .Append("         T.StartDate ")
            .Append(" FROM     arTerm T  ,syCmpGrpCmps t1, syCampuses t2 ")
            .Append(" where t1.CampusId = t2.CampusId and t1.CampGrpId=T.CampGrpId ")
            .Append(" and T.StatusId in (Select StatusID from systatuses where status='Active') ")
            .Append(" And T1.CampusId='" & CampusId & "'")
        End With
        da.SelectCommand.CommandText = sb.ToString + " ORDER BY T.StartDate desc "

        ' ''   fill Terms table
        da.Fill(ds, "Terms")

        '   create primary and foreign key constraints

        '   set primary key for Students table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("Students").Columns("StudentId")
        ds.Tables("Students").PrimaryKey = pk0

        '   set primary key for Enrollments table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("Enrollments").Columns("PrgVerId")
        ds.Tables("Enrollments").PrimaryKey = pk1

        '   set primary key for Terms table
        Dim pk2(0) As DataColumn
        pk2(0) = ds.Tables("Terms").Columns("TermId")
        ds.Tables("Terms").PrimaryKey = pk2

        '   set foreign key column in StudentEnrollments
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("StudentEnrollments").Columns("StudentId")

        '   set foreign key column in StudentEnrollments
        Dim fk1(0) As DataColumn
        fk1(0) = ds.Tables("StudentEnrollments").Columns("PrgVerId")

        '   set foreign key column in Students
        Dim fk2(0) As DataColumn
        fk2(0) = ds.Tables("StudentTerms").Columns("StudentId")

        '   set foreign key column in Students
        Dim fk3(0) As DataColumn
        fk3(0) = ds.Tables("StudentTerms").Columns("TermId")

        '   set foreign keys in GradeBookResults table
        ds.Relations.Add("StudentsStudentEnrollments", pk0, fk0)
        ds.Relations.Add("EnrollmentsStudentEnrollments", pk1, fk1)
        ds.Relations.Add("StudentStudentTerms", pk0, fk2, False)
        ds.Relations.Add("TermsStudentTerms", pk2, fk3)

        '   return dataset
        Return ds

    End Function
End Class
