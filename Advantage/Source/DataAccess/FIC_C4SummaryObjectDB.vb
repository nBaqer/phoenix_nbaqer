Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class FIC_C4SummaryObjectDB

	Public Shared Function GetReportDatasetRaw(ByVal RptParamInfo As ReportParamInfoIPEDS) As DataSet
		Dim DateStart As DateTime = RptParamInfo.RptEndDate.AddYears(-1)

		' get list of students to include, based on report parameters
		Dim StudentList As String = IPEDSDB.GetStudentList(RptParamInfo, , "EnrollDate >= '" & FmtRptDateParam(DateStart) & "'")

		' if there are no students to include, return empty dataset
		If StudentList = "" Then
			Return New DataSet
		End If

		Dim sb As New System.Text.StringBuilder
		Dim dsRaw As New DataSet

		With sb
			' get Students
			.Append("SELECT arStudent.StudentId FROM arStudent ")

			' append list of appropriate students to include
			.Append("WHERE arStudent.StudentId IN (" & StudentList & ") ")


			.Append(";")


			.Append("SELECT DISTINCT ")
			.Append("adLeadEntranceTest.StudentId, ")

			' retrieve all other needed columns
			.Append("syRptAgencyFldValues.RptAgencyFldValId AS TestAgencyFldValId, ")
			.Append("syRptAgencyFldValues.AgencyDescrip AS TestDescrip, ")
			.Append("(SELECT MAX(CAST(A.ActualScore AS FLOAT)) ")
			.Append("	FROM adLeadEntranceTest A ")
			.Append("	WHERE A.StudentId = adLeadEntranceTest.StudentId AND ")
			.Append("	      A.EntrTestId = adLeadEntranceTest.EntrTestId) AS Score ")
			.Append("FROM ")
			.Append("adLeadEntranceTest, adReqs, arStudent, ")
			.Append("syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")

			' establish necessary relationships
			.Append("WHERE ")
			.Append("adLeadEntranceTest.EntrTestId = adReqs.adReqId AND ")
			.Append("adLeadEntranceTest.StudentId = arStudent.StudentId AND ")
			.Append("adReqs.adReqId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")

			' append list of appropriate students to include
			.Append("adLeadEntranceTest.StudentId IN (" & StudentList & ") ")

			.Append("ORDER BY TestAgencyFldValId, Score ")
		End With

		dsRaw = IPEDSDB.DataAccessIPEDS().RunSQLDataSet(sb.ToString)

		With dsRaw
			.Tables(0).TableName = TblNameStudents
			.Tables(1).TableName = TblNameScores

			With .Tables(TblNameStudents)
				Dim PKey() As DataColumn = {.Columns("StudentId")}
				.PrimaryKey = PKey
			End With
		End With

		Return dsRaw

	End Function

End Class
