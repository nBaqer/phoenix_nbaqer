﻿Imports FAME.Advantage.Common

Public Class SetDictationTestRulesDB
    Dim db As New SQLDataAccess
    Public Function GetTestByCourse(ByVal CourseId As String) As DataSet
        Dim ds As New DataSet
        Dim intRowCount As Integer = 0

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@ReqId", New Guid(CourseId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("USP_DictationTestByCourse_GetList")
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Sub SetupRules(ByVal dtEffectiveDate As DateTime, _
                          ByVal xmlRules As String, _
                          Optional ByVal dtOldEffectiveDate As DateTime = #1/1/1900#)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@OldEffectiveDate", dtOldEffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@EffectiveDate", dtEffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@RuleValues", xmlRules, SqlDbType.VarChar, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("USP_RulesForCourses_Insert", Nothing)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Sub
    Public Sub SetupMentorProctoredTestRequirement(ByVal dtEffectiveDate As DateTime, _
                          ByVal xmlRules As String, _
                          Optional ByVal dtOldEffectiveDate As DateTime = #1/1/1900#, _
                         Optional ByVal strModUser As String = "sa", _
                              Optional ByVal dtModDate As DateTime = #1/1/1900#)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@OldEffectiveDate", dtOldEffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@EffectiveDate", dtEffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@RuleValues", xmlRules, SqlDbType.VarChar, , ParameterDirection.Input)
            db.AddParameter("@ModUser", strModUser, SqlDbType.VarChar, , ParameterDirection.Input)
            db.AddParameter("@ModDate", dtModDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("USP_MentorProctoredRequirement_Insert", Nothing)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Sub
    Public Function GetEffectiveDates(ByVal reqId As String) As DataSet
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@ReqId", New Guid(reqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("USP_RulesByEffectiveDate_GetList", Nothing)
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function GetRulesByEffectiveDateAndCourse(ByVal EffectiveDate As DateTime, _
                                                     ByVal reqId As String) As DataSet
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@EffectiveDate", EffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@ReqId", New Guid(reqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("USP_RulesByEffectiveDateAndCourse_GetList", Nothing)
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Sub DeleteRulesByEffectiveDateAndCourse(ByVal EffectiveDate As DateTime, _
                                                   ByVal reqId As String)
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@ReqId", New Guid(reqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@EffectiveDate", EffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("usp_deleterulesbyeffectivedate", Nothing)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Sub
    Public Sub DeletePreRequisitesByEffectiveDateAndCourse(ByVal EffectiveDate As DateTime, _
                                                   ByVal reqId As String)
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@ReqId", New Guid(reqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@EffectiveDate", EffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("usp_prerequisitesbyeffectivedate_deletelist", Nothing)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Sub
    Public Sub DeleteMentorTestByEffectiveDateAndCourse(ByVal EffectiveDate As DateTime, _
                                                   ByVal reqId As String)
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@ReqId", New Guid(reqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@EffectiveDate", EffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("usp_mentortestbyeffectivedate_deletelist", Nothing)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Sub
    Public Sub SetupPreRequisites(ByVal dtEffectiveDate As DateTime, _
                                  ByVal ReqId As String, _
                                  ByVal MinperCategory As Integer, _
                                  ByVal MaxperCategory As Integer, _
                                  ByVal MinperCombination As Integer, _
                                  ByVal MaxperCombination As Integer, _
                                  ByVal PreReqId As String, _
                                  ByVal mentorproctored As Integer, _
                                  ByVal moduser As String, _
                                  ByVal moddate As DateTime, _
                                  Optional ByVal dtOldEffectiveDate As DateTime = #1/1/1900#)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@OldEffectiveDate", dtOldEffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@EffectiveDate", dtEffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@ReqId", New Guid(ReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@MinperCategory", MinperCategory, SqlDbType.Int, , ParameterDirection.Input)
            db.AddParameter("@MaxperCategory", MaxperCategory, SqlDbType.Int, , ParameterDirection.Input)
            db.AddParameter("@MinperCombination", MinperCombination, SqlDbType.Int, , ParameterDirection.Input)
            db.AddParameter("@MaxperCombination", MaxperCombination, SqlDbType.Int, , ParameterDirection.Input)
            db.AddParameter("@PreReqId", New Guid(PreReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@mentorproctored", mentorproctored, SqlDbType.Bit, , ParameterDirection.Input)
            db.AddParameter("@moduser", moduser, SqlDbType.VarChar, , ParameterDirection.Input)
            db.AddParameter("@moddate", moddate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("USP_PreRequisitesForCourses_Insert", Nothing)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try

        'if exists (select * from sysobjects where type='P' and name='USP_PreRequisitesForCourses_Insert')
        '            begin()
        '        drop procedure USP_PreRequisitesForCourses_Insert
        '            End
        'go
        'Create Procedure USP_PreRequisitesForCourses_Insert
        '        @OldEffectiveDate datetime,
        '        @EffectiveDate datetime,
        '        @ReqId uniqueidentifier,
        '		@MinperCategory int,
        '		@MaxperCategory int,
        '		@MinperCombination int,
        '		@MaxperCombination int,
        '		@PreReqId uniqueidentifier,
        '		@mentorproctored bit,
        '		@moduser varchar(50),
        '		@moddate datetime
        '    as
        '        Declare @hDoc int

        '        -- Delete all records from bridge table based on GrdComponentTypeId
        '        if Year(@OldEffectiveDate) <> 1900
        '                begin()
        '            delete from USP_PreRequisitesForCourses_Insert
        '                where()
        '                    EffectiveDate = @OldEffectiveDate and
        '                    ReqId = @ReqId
        '                End

        '		insert into USP_PreRequisitesForCourses_Insert values(@ReqId,@EffectiveDate,@MinperCategory,@MaxperCategory,
        '		@MinperCombination,@MaxperCombination,@PreReqId,@mentorproctored,@moduser,@moddate)
        'go
    End Sub
    Public Function GetPreRequisitesByEffectiveDateAndCourse(ByVal EffectiveDate As DateTime, _
                                                    ByVal reqId As String) As DataSet
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@EffectiveDate", EffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@ReqId", New Guid(reqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("USP_PreRequisitesForCourses_GetList", Nothing)
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function GetMentorProctoredTestRequirement(ByVal reqId As String, ByVal PrereqId As String) As DataSet
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'db.AddParameter("@ReqId", New Guid(reqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            'If Not PrereqId = "" Then
            '    db.AddParameter("@PreReqId", New Guid(PrereqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            'End If
            ds = db.RunParamSQLDataSet_SP("USP_MentorProctoredTestRequirement_GetList", Nothing)
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function GetMentorProctoredTestByEffectiveDateAndCourse(ByVal EffectiveDate As DateTime, _
                                                     ByVal reqId As String) As DataSet
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@EffectiveDate", EffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@ReqId", New Guid(reqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("USP_MentorProctoredTest_GetList", Nothing)
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function GetEffectivedatesbyCourse(ByVal reqId As String) As DataSet
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@ReqId", New Guid(reqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("usp_effectivedates_getlist", Nothing)
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function GetEffectivedatesbyPrerequisiteCourse(ByVal PrereqId As String) As DataSet
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@ReqId", New Guid(PrereqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("usp_effectivedatesforprerequisitecourse_getlist", Nothing)
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function GetTopSpeedTestCategory(ByVal EffectiveDate As DateTime, _
                                            ByVal reqId As String) As DataSet
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@ReqId", New Guid(reqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@EffectiveDate", EffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("usp_topspeedtestcategory_getlist", Nothing)
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function GetMinimumCombination(ByVal EffectiveDate As DateTime, _
                                         ByVal reqId As String, _
                                         ByVal StuEnrollId As String) As DataSet
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@ReqId", New Guid(reqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@EffectiveDate", EffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("usp_minimumpercombination_getlist", Nothing)
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function GetMentorCombination(ByVal reqId As String, _
                                        ByVal StuEnrollId As String) As DataSet
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@ReqId", New Guid(reqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("usp_mentorpercombination_getlist", Nothing)
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function GetMentorRules(ByVal EffectiveDate As DateTime, ByVal reqId As String) As DataSet
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@ReqId", New Guid(reqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@EffectiveDate", EffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("usp_mentorrequirementsbyeffectivedates_getlist", Nothing)
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function GetMentorTestByStudent(ByVal StuEnrollId As String, _
                                           ByVal EffectiveDate As DateTime, ByVal reqId As String) As DataSet
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ReqId", New Guid(reqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@EffectiveDate", EffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("usp_mentortestbystudent_getlist", Nothing)
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function GetNumberofTimesStudentRegisteredInSHCourses(ByVal StuEnrollId As String, _
                                                                                                                                        ByVal reqId As String, _
                                                                                                                                        ByVal CourseCode As String) As Integer
        Dim ds As New DataSet
        Dim intReturnValue As Integer = 0

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ReqId", New Guid(reqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@CourseCode", CourseCode, SqlDbType.VarChar, , ParameterDirection.Input)
            intReturnValue = CType(db.RunParamSQLScalar_SP("usp_NumberofTimesStudentRegisteredInSHCourses_getcount"), Integer)
            Return intReturnValue
        Catch ex As System.Exception
            Return intReturnValue
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function GetSAPShortHandSkillLevel() As DataSet
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            ds = db.RunParamSQLDataSet_SP("usp_SHSAPSkillLevel_getlist", Nothing)
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function GetShortHandCourses() As Integer
        Dim ds As New DataSet
        Dim intRowCount As Integer = 0

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            intRowCount = db.RunParamSQLScalar_SP("usp_ShortHandCourses_GetList")
            Return intRowCount
        Catch ex As System.Exception
            Return intRowCount
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function BindMentorProctoredTestRequirement(ByVal EffectiveDate As DateTime, _
                                         ByVal reqId As String) As DataSet
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@EffectiveDate", EffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@ReqId", New Guid(reqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("USP_BindMentorProctoredTestRequirement_GetList", Nothing)
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Sub SetupSAPShortHandSkillRequirement(ByVal SAPDetailId As String, _
                                                                                                    ByVal xmlRules As String, _
                                                                                                    Optional ByVal strModUser As String = "sa", _
                                                                                                    Optional ByVal dtModDate As DateTime = #1/1/1900#)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@SAPDetailId", New Guid(SAPDetailId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@RuleValues", xmlRules, SqlDbType.VarChar, , ParameterDirection.Input)
            db.AddParameter("@ModUser", strModUser, SqlDbType.VarChar, , ParameterDirection.Input)
            db.AddParameter("@ModDate", dtModDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("USP_SAPShortHandSkillRequirement_Insert", Nothing)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Sub
    Public Function BindShortHandSkillRequirement(ByVal SAPDetailid As String) As DataSet
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@SAPDetailid", New Guid(SAPDetailid), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("USP_BindShortHandSkillRequirement_GetList", Nothing)
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function GetSpeedPostedForSAP(ByVal SAPDetailid As String, ByVal StuEnrollId As String) As DataSet
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@SAPDetailid", New Guid(SAPDetailid), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("usp_speedpostedforsap_getlist", Nothing)
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function GetMentorRequirementText(ByVal EffectiveDate As DateTime, _
                                      ByVal reqId As String) As DataSet
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@ReqId", New Guid(reqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@EffectiveDate", EffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("usp_mentorrequirementtext_getlist", Nothing)
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function GetSAPRequirement(ByVal SAPDetailid As String) As DataSet
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@SAPDetailid", New Guid(SAPDetailid), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("usp_SAPrequirementtext_getlist", Nothing)
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function RegistrationInSHCourses(ByVal StuEnrollId As String, _
                                                                                      ByVal CourseCode As String, _
                                                                                      ByVal SHRepeatLimit As Integer, _
                                                                                      ByVal ReqId As String, _
                                                                                      ByVal ValidForregistration As String, _
                                                                                      ByVal SHSemesterLimit As Integer) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Dim ds As New DataSet
        'db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            'db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            'db.AddParameter("@CourseCode", CourseCode, SqlDbType.VarChar, , ParameterDirection.Input)
            'db.AddParameter("@SHRepeatLimit", SHRepeatLimit, SqlDbType.Int, , ParameterDirection.Input)
            'db.AddParameter("@ReqId", New Guid(ReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            'db.AddParameter("@ValidForregistration", ValidForregistration, SqlDbType.VarChar, , ParameterDirection.Output)
            'db.RunParamSQLExecuteNoneQuery_SP("usp_RegistrationInSHCourses_getcount", Nothing)

            Dim cmdSqlParam1, cmdSqlParam2, cmdSqlParam3, cmdSqlParam4, cmdSqlParam5, cmdSqlParam6 As New SqlParameter

            Using Conn As SqlConnection = New SqlConnection(MyAdvAppSettings.AppSettings("ConnectionString"))
                Using Cmd As SqlCommand = New SqlCommand("usp_RegistrationInSHCourses_Validate")   'Old Code: New SqlCommand("usp_RegistrationInSHCourses_getcount")
                    Cmd.CommandType = CommandType.StoredProcedure

                    With cmdSqlParam1
                        .ParameterName = "@StuEnrollId"
                        .SqlDbType = SqlDbType.UniqueIdentifier
                        .Direction = ParameterDirection.Input
                        .Value = New Guid(StuEnrollId)
                    End With

                    With cmdSqlParam2
                        .ParameterName = "@CourseCode"
                        .SqlDbType = SqlDbType.VarChar
                        .Direction = ParameterDirection.Input
                        .Value = CourseCode
                    End With

                    With cmdSqlParam3
                        .ParameterName = "@SHRepeatLimit"
                        .SqlDbType = SqlDbType.Int
                        .Direction = ParameterDirection.Input
                        .Value = SHRepeatLimit
                    End With

                    With cmdSqlParam6
                        .ParameterName = "@SHSemesterLimit"
                        .SqlDbType = SqlDbType.Int
                        .Direction = ParameterDirection.Input
                        .Value = SHSemesterLimit
                    End With

                    With cmdSqlParam4
                        .ParameterName = "@ReqId"
                        .SqlDbType = SqlDbType.UniqueIdentifier
                        .Direction = ParameterDirection.Input
                        .Value = New Guid(ReqId)
                    End With

                    With cmdSqlParam5
                        .ParameterName = "@ValidForregistration"
                        .SqlDbType = SqlDbType.VarChar
                        .Direction = ParameterDirection.Output
                        .Value = ValidForregistration
                    End With

                    Cmd.Parameters.Add(cmdSqlParam1)
                    Cmd.Parameters.Add(cmdSqlParam2)
                    Cmd.Parameters.Add(cmdSqlParam3)
                    Cmd.Parameters.Add(cmdSqlParam6)
                    Cmd.Parameters.Add(cmdSqlParam4)
                    Cmd.Parameters.Add(cmdSqlParam5)

                    Conn.Open()
                    Cmd.Connection = Conn
                    Cmd.ExecuteNonQuery()

                    Dim strOutputValue As String
                    strOutputValue = Cmd.Parameters("@ValidForregistration").Value.ToString

                    If strOutputValue.ToString.Trim.Length = 1 AndAlso strOutputValue.ToString.Trim = "F" Then
                        strOutputValue = "False"
                    ElseIf strOutputValue.ToString.Trim.Length = 1 AndAlso strOutputValue.ToString.Trim = "T" Then
                        strOutputValue = "True"
                    End If
                    Return strOutputValue  'The return value will be true or false
                End Using
            End Using
        Catch ex As System.Exception
            Return ""
        Finally
        End Try
    End Function
    Public Sub ResetPreRequisites(ByVal ReqId As String, Optional ByVal dtEffectiveDate As DateTime = #1/1/1900#)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@EffectiveDate", dtEffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@ReqId", New Guid(ReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("USP_PreRequisitesForCourses_Reset", Nothing)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Sub
    Public Sub ResetMentorRequirement(ByVal ReqId As String, Optional ByVal dtEffectiveDate As DateTime = #1/1/1900#)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@EffectiveDate", dtEffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@ReqId", New Guid(ReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("USP_MentorProctoredRequirement_Reset", Nothing)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Sub
    Public Function IsCourseASHLevelCourse(ByVal ReqId As String, ByVal coursecode As String) As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim intRowCount As Integer = 0
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@ReqId", New Guid(ReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@coursecode", coursecode, SqlDbType.VarChar, , ParameterDirection.Input)
            intRowCount = db.RunParamSQLScalar_SP("USP_IsCourseASHLevelCourse", Nothing)
        Catch ex As System.Exception
            intRowCount = 0
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return intRowCount
    End Function
    Public Function CCR_CheckIfCourseHasAPassingGrade(ByVal StuEnrollId As String, ByVal ReqId As String) As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim intRowCount As Integer = 0
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ReqId", New Guid(ReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            intRowCount = db.RunParamSQLScalar_SP("USP_CCR_CheckIfCourseHasAPassingGrade", Nothing)
        Catch ex As System.Exception
            intRowCount = 0
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return intRowCount
    End Function
    'Public Function CCR_CheckIfCourseHasAPassingGrade(ByVal StuEnrollId As String, ByVal ReqId As String) As Integer
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '    Dim intRowCount As Integer = 0
    '    Try
    '        'Call the procedure to insert more than one record all at once
    '        db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
    '        db.AddParameter("@ReqId", New Guid(ReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
    '        intRowCount = db.RunParamSQLScalar_SP("USP_CCR_CheckIfCourseHasAPassingGrade", Nothing)
    '    Catch ex As System.Exception
    '        intRowCount = 0
    '    Finally
    '        db.ClearParameters()
    '        db.CloseConnection()
    '    End Try
    '    Return intRowCount
    'End Function
    Public Function GetCourseDescriptionByClass(ByVal ClsSectionId As String) As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim ds As New DataSet
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@ClsSectionId", New Guid(ClsSectionId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("USP_GetCourseDescriptionByClass", Nothing)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return ds.Tables(0)
    End Function
    Public Sub SetupPageLevelPermission(ByVal xmlRules As String)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@RuleValues", xmlRules, SqlDbType.VarChar, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("USP_PageLevelPermissions_Insert", Nothing)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Sub
    Public Sub AddStudentPagesToOtherModules(ByVal xmlRules As String)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@RuleValues", xmlRules, SqlDbType.VarChar, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("USP_SaveStudentPagesToOtherModules", Nothing)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Sub
End Class


