Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' StatesDB.vb
'
' StatesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class StatesDB
    Public Function GetAllStates() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            '.Append("SELECT   S.StateId, S.StatusId, S.StateCode, S.StateDescrip ")
            '.Append("FROM     syStates S, syStatuses ST ")
            '.Append("WHERE    S.StatusId = ST.StatusId ")
            '.Append(" AND     ST.Status = 'Active' ")
            '.Append("ORDER BY S.StateDescrip ")
            .Append("Select Distinct StateId,StateCode,StateDescrip from syStates order by StateDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function

    Public Shared Function GetStatesIPEDS() As DataTable
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        sb.Append("SELECT StateId, StateDescrip, FIPSCode FROM syStates")

        With New DataAccess
            .ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            .OpenConnection()
            Return .RunSQLDataSet(sb.ToString).Tables(0)
        End With

    End Function
End Class
