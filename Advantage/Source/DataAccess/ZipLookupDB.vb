Option Strict On
Public Class ZipLookupDB
    Public Function GetCityFromZip(ByVal ZipToFind As String) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder

        With strSQLString
            .Append("SELECT City, State, Zip, StateId")
            .Append(" FROM syZipLook")
            .Append(" WHERE Zip = ? ")
        End With
        db.OpenConnection()
        db.AddParameter("@Zip", ZipToFind, DataAccess.OleDbDataType.OleDbString, 9, ParameterDirection.Input)
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "CityList")
            Return ds
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try
    End Function
End Class
