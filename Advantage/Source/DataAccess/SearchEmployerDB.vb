Imports FAME.Advantage.Common

Public Class SearchEmployerDB
    Public Function GetAllCity() As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   Distinct City ")
            .Append("FROM    plEmployers where city is not null ")
        End With
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllState() As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   PV.StateId, PV.StateDescrip ")
            .Append("FROM     syStates PV, syStatuses ST ")
            .Append(" Where    PV.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY PV.StateDescrip ")
        End With
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllZip() As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   Distinct Zip ")
            .Append("FROM    plEmployers where zip is not null")
        End With
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllCounty() As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   PV.CountyId, PV.CountyDescrip ")
            .Append("FROM     adCounties PV, syStatuses ST ")
            .Append(" Where     PV.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY PV.CountyDescrip ")
        End With
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllEmployers(ByVal strCode As String, ByVal strEmployerID As String, ByVal strEmployerName As String, ByVal strCity As String, ByVal strState As String, ByVal strZip As String, ByVal strCounty As String, ByVal strLocation As String, ByVal strStatus As String, ByVal strIndustry As String, ByVal strCampus As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb

            .Append(" SELECT   Code,EmployerID, ")
            .Append(" EmployerDescrip, ")
            .Append(" City, ")
            .Append(" (Select StateDescrip from syStates where StateID = plEmployers.StateID) as State, ")
            .Append(" Zip,Address1 ")
            .Append(" FROM  plEmployers  where ")

            'the relationship logic between checkboxes is "OR" instead of "AND". Only the first one is AND
            Dim andOrOrOperator As String = " "
            If Not strCode = "" Then
                .Append(andOrOrOperator)
                .Append(" Code like  + '%' + ? + '%'")
                db.AddParameter("@Code", strCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "All Campus Groups"
            If Not strEmployerID = "" Then
                .Append(andOrOrOperator)
                .Append(" EmployerID like  + '%' + ? + '%'")
                db.AddParameter("@EmployerID", strEmployerID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            If Not strEmployerName = "" Then
                .Append(andOrOrOperator)
                .Append(" EmployerDescrip like  + '%' + ? + '%'")
                db.AddParameter("@EmployerDescrip", strEmployerName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            If Not strCity = Guid.Empty.ToString Then
                .Append(andOrOrOperator)
                .Append(" City = ? ")
                db.AddParameter("@City", strCity, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            If Not strState = Guid.Empty.ToString Then
                .Append(andOrOrOperator)
                .Append(" StateID = ? ")
                db.AddParameter("@StateID", strState, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            If Not strZip = Guid.Empty.ToString Then
                .Append(andOrOrOperator)
                .Append(" Zip like  + '%' + ? + '%'")
                db.AddParameter("@Zip", strZip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If
            If Not strCounty = Guid.Empty.ToString Then
                .Append(andOrOrOperator)
                .Append(" CountyId = ? ")
                db.AddParameter("@County", strCounty, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If
            If Not strLocation = Guid.Empty.ToString Then
                .Append(andOrOrOperator)
                .Append(" LocationID = ? ")
                db.AddParameter("@Location", strLocation, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If
            If Not strStatus = Guid.Empty.ToString Then
                .Append(andOrOrOperator)
                .Append(" StatusID = ? ")
                db.AddParameter("@StatusID", strStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If
            If Not strIndustry = Guid.Empty.ToString Then
                .Append(andOrOrOperator)
                .Append(" IndustryID = ? ")
                db.AddParameter("@IndustryID", strIndustry, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If
            If Not strCampus = Guid.Empty.ToString Then
                .Append(andOrOrOperator)
                .Append(" CampGrpId in (Select distinct CampGrpId from syCmpGrpCmps where CampusId= ?) ")
                db.AddParameter("@CampGrpId", strCampus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            .Append(" ORDER BY EmployerDescrip ")
        End With

        'Execute the query and return Dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
End Class
