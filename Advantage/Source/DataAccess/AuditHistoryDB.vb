Imports System
Imports System.Data
Imports Microsoft.VisualBasic
Imports FAME.AdvantageV1.Common
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer

Public Class AuditHistoryDB
#Region "Private Variables and objects"
    Private Const ExcludedTables As String = "" 'leading/trailing commas are required
    Private Const EXCLUDED_FIELDS As String = ",AddUser,AddDate,ModUser,ModDate," 'leading/trailing commas are required
    Private Const ExcludedTypes As String = ",binary,varbinary,image,timestamp," 'leading/trailing commas are required
    Private Const TableNameToken As String = "%TABLENAME%"
    Private Const ColumnNameToken As String = "%COLUMNNAME%"
    Private Const RowIdToken As String = "%ROWID%"
    'Private Const YourTableName As String = "YourTableName"
    Public str As String
#End Region

#Region "Private Methods"
    Private Function GetTableMetaData(ByVal tblId As Integer) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        db.ConnectionString = "ConString"

        With sb
            .Append("SELECT t1.FldId,t2.TblName,t3.FldName,t4.FldType ")
            .Append("FROM syTblFlds t1,syTables t2, syFields t3, syFieldTypes t4 ")
            .Append("WHERE t1.TblId=t2.TblId ")
            .Append("AND t1.FldId=t3.FldId ")
            .Append("AND t3.FldTypeId=t4.FldTypeId ")
            .Append("AND t1.TblId=?")
        End With

        db.AddParameter("@tblid", tblId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds.Tables(0)

    End Function

    Private Function GetTablePK(ByVal tblId As Integer) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        With sb
            .Append("SELECT t1.TblPk,t2.FldName ")
            .Append("FROM syTables t1, syFields t2 ")
            .Append("WHERE t1.TblPk=t2.FldId ")
            .Append("AND t1.TblId=?")
        End With

        db.AddParameter("@tblid", tblId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0).Rows(0)("FldName")

    End Function

#End Region

#Region "Public Methods"
    Public Sub AddAuditTriggers(ByVal Tables As String, Optional ByVal TriggerTypes As String = "IUD")
        '*************************************************************************************************
        'Purpose:       Adds audit Hist triggers to specified tables
        'Parameters:
        '[Tables]       Comma-separated list of table names
        'Returns:       N/A
        'Created:       Troy Richards, 4/18/2003
        'Notes:         The original version was created for VB Advantage by Mitch McHenry on 4/15/2002
        '*************************************************************************************************
        Dim tablesArray() As String
        Dim numTable As Integer
        Dim numTables As Integer
        Dim templInsHdr As String
        Dim templInsCol As String
        Dim templInsFtr As String
        Dim templUpdHdr As String
        Dim templUpdCol As String
        Dim templUpdFtr As String
        Dim templDelHdr As String
        Dim templDelCol As String
        Dim templDelFtr As String
        Dim sInsert As String
        Dim sUpdate As String
        Dim sDelete As String
        Dim tblName As String
        Dim colName As String
        Dim pkey As String
        Dim numAuditCols As Integer
        Dim dt As DataTable
        'Dim col As DataColumn
        Dim row As DataRow
        Dim db As New DataAccess
        'Dim sSQL As String

        Try
            'Put the table list into an array
            tablesArray = Tables.Split(",")
            numTables = tablesArray.GetUpperBound(0)

            'Get all of the trigger section templates
            Dim objTemplate As New AuditTriggerTemplates
            templInsHdr = objTemplate.GetTriggerTemplate("INSHDR")
            templInsCol = objTemplate.GetTriggerTemplate("INSCOL")
            templInsFtr = objTemplate.GetTriggerTemplate("INSFTR")
            templUpdHdr = objTemplate.GetTriggerTemplate("UPDHDR")
            templUpdCol = objTemplate.GetTriggerTemplate("UPDCOL")
            templUpdFtr = objTemplate.GetTriggerTemplate("UPDFTR")
            templDelHdr = objTemplate.GetTriggerTemplate("DELHDR")
            templDelCol = objTemplate.GetTriggerTemplate("DELCOL")
            templDelFtr = objTemplate.GetTriggerTemplate("DELFTR")

            For numTable = 0 To numTables
                'Set number of primary keys to 0
                numAuditCols = 0 'Count of columns to be audited for the current table



                'Get datatable with schema information for current table
                dt = GetTableMetaData(CInt(Trim(tablesArray(numTable))))
                tblName = dt.Rows(0)("TblName")
                pkey = GetTablePK(CInt(Trim(tablesArray(numTable))))

                'Don't create triggers if current table is in the excluded tables list
                If Not ExcludedTables.ToUpper Like "*," & tblName.ToUpper & ",*" Then
                    'Get the header section template for each of the triggers
                    sInsert = templInsHdr
                    sUpdate = templUpdHdr
                    sDelete = templDelHdr

                    'Add each column to be audited to the triggers
                    For Each row In dt.Rows
                        colName = Trim(row("FldName"))
                        'Don't add current column to trigger if column is in the excluded column list or is a primary key
                        If Not (EXCLUDED_FIELDS.ToUpper & pkey.ToUpper & ",") Like "*," & colName.ToUpper & ",*" Then

                            'Don't add current colun to trigger if column data type is in the excluded data type list
                            If Not ExcludedTypes.ToUpper Like "*," & row("FldType").ToString() & ",*" Then

                                'Add the current column section to each of the triggers
                                sInsert = sInsert & templInsCol.Replace(ColumnNameToken, colName)
                                sUpdate = sUpdate & templUpdCol.Replace(ColumnNameToken, colName)
                                sDelete = sDelete & templDelCol.Replace(ColumnNameToken, colName)

                                numAuditCols += 1

                            End If  'Not excluded type
                        End If  'Not excluded column
                    Next

                    'Only create triggers if we found some columns to audit
                    If numAuditCols > 0 Then

                        'Get the footer section for each of the trigger
                        sInsert = sInsert & templInsFtr
                        sUpdate = sUpdate & templUpdFtr
                        sDelete = sDelete & templDelFtr

                        'Replace the table name tokens in the template wih the actual table name
                        sInsert = sInsert.Replace(TableNameToken, tblName)
                        sUpdate = sUpdate.Replace(TableNameToken, tblName)
                        sDelete = sDelete.Replace(TableNameToken, tblName)

                        'Replace the row id tokens in the template with the actual row id column name
                        sInsert = sInsert.Replace(RowIdToken, pkey)
                        sUpdate = sUpdate.Replace(RowIdToken, pkey)
                        sDelete = sDelete.Replace(RowIdToken, pkey)
                        str = sInsert
                        'Drop any existing audit hist triggers for current table
                        DropAuditTriggers(tblName, TriggerTypes)
                        'Add new audit hist triggers to current table
                        If TriggerTypes.IndexOf("I") <> -1 Then
                            db.RunParamSQLExecuteNoneQuery(sInsert)
                        End If
                        If TriggerTypes.IndexOf("U") <> -1 Then
                            db.RunParamSQLExecuteNoneQuery(sUpdate)
                        End If
                        If TriggerTypes.IndexOf("D") <> -1 Then
                            db.RunParamSQLExecuteNoneQuery(sDelete)
                        End If

                    End If

                End If 'Not excluded table
            Next

        Catch Ex As Exception
            If Not Ex.InnerException Is Nothing Then
                Throw New Exception("Problem occurred adding triggers:" & Ex.InnerException.ToString)
            Else
                Throw Ex
            End If
        End Try
    End Sub

    Public Sub DropAuditTriggers(ByVal Tables As String, Optional ByVal TriggerTypes As String = "IUD")
        '************************************************************************************************
        'Purpose:       Drops audit hist triggers from specified tables
        'Parameters:
        '[Tables]       Comma-separated list of table names
        'Returns:       N/A
        'Created:       Troy Richards, 4/10/2003
        'Notes:         The original version was created for VB Advantage by Mitch McHenry on 4/15/2002        
        '************************************************************************************************
        Dim TablesArray() As String
        Dim NumTable As Integer
        Dim NumTables As Integer
        Dim TblName As String
        Dim TemplDropIns As String
        Dim TemplDropUpd As String
        Dim TemplDropDel As String
        Dim sDropIns As String
        Dim sDropUpd As String
        Dim sDropDel As String
        Dim db As New DataAccess

        Try
            TablesArray = Tables.Split(",")
            NumTables = TablesArray.GetUpperBound(0)

            'Get all of the Drop Trigger templates
            Dim objTemplate As New AuditTriggerTemplates
            TemplDropIns = objTemplate.GetTriggerTemplate("DROPINS")
            TemplDropUpd = objTemplate.GetTriggerTemplate("DROPUPD")
            TemplDropDel = objTemplate.GetTriggerTemplate("DROPDEL")

            For NumTable = 0 To NumTables
                TblName = Trim(TablesArray(NumTable))
                If TriggerTypes.IndexOf("I") <> -1 Then
                    sDropIns = TemplDropIns.Replace(TableNameToken, TblName)
                    db.RunParamSQLExecuteNoneQuery(sDropIns)
                End If
                If TriggerTypes.IndexOf("U") <> -1 Then
                    sDropUpd = TemplDropUpd.Replace(TableNameToken, TblName)
                    db.RunParamSQLExecuteNoneQuery(sDropUpd)
                End If
                If TriggerTypes.IndexOf("D") <> -1 Then
                    sDropDel = TemplDropDel.Replace(TableNameToken, TblName)
                    db.RunParamSQLExecuteNoneQuery(sDropDel)
                End If
            Next

        Catch Ex As Exception
            Throw New Exception("Problem occurred dropping triggers:" & Ex.Message)
        End Try
    End Sub


    Public Function GetAuditHist(ByVal resourceId As Integer, ByVal strRowIds As String) As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim dt As DataTable
        Dim dr As DataRow
        Dim sb As New StringBuilder

        db.ConnectionString = "ConString"
        db.OpenConnection()

        With sb
            .Append("SELECT DISTINCT t1.ColumnName,t4.Caption,t3.FldId,t3.DDLId,t1.OldValue, ")
            .Append("t1.NewValue,t2.Event,t2.EventDate,t2.UserName,t2.AppName,t8.FldType ")
            .Append("FROM syAuditHistDetail t1,syAuditHist t2,syFields t3, ")
            .Append("syFldCaptions t4, syLangs t5,syTblFlds t6,syResTblFlds t7,syFieldTypes t8 ")
            .Append("WHERE t1.RowId IN(?) ")
            .Append("AND t1.AuditHistId=t2.AuditHistId ")
            .Append("AND t1.ColumnName=t3.FldName ")
            .Append("AND t3.FldId=t4.FldId ")
            .Append("AND t5.LangName='EN-US' ")
            .Append("AND t3.FldId=t6.FldId ")
            .Append("AND t6.TblFldsId=t7.TblFldsId ")
            .Append("AND t7.ResourceId=? ")
            .Append("AND t3.FldTypeId=t8.fldTypeId ")
            .Append("AND CASE WHEN t7.ResourceId = 170 ")
            .Append("          AND (   (    (t1.OldValue IS NULL OR t1.OldValue = '') ")
            .Append("                   AND (t1.NewValue IS NULL OR t1.NewValue = '') ")
            .Append("   			   ) ")
            .Append("               OR (SUBSTRING(t1.NewValue,1,4) = 'Mig*' ) ")
            .Append("              )  ")
            .Append("        THEN 0   ")
            .Append("        ELSE 1   ")
            .Append("    END = 1      ")
            .Append("ORDER BY t2.EventDate DESC ")
        End With

        db.AddParameter("@rowids", strRowIds, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
        db.AddParameter("@resid", resourceId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)


        ds = db.RunParamSQLDataSet(sb.ToString)
        dt = ds.Tables(0)

        For Each dr In dt.Rows
            '   List user-friendly values
            Select Case dr("FldType").ToString
                Case "Datetime"
                    If dr("FldId") = 295 Then
                        ' special case: TimeIntervalDescrip (FldId=295) stores time values
                        If Not dr.IsNull("OldValue") Then dr("OldValue") = CType(dr("OldValue"), DateTime).ToShortTimeString
                        If Not dr.IsNull("NewValue") Then dr("NewValue") = CType(dr("NewValue"), DateTime).ToShortTimeString
                    Else
                        If Not dr.IsNull("OldValue") Then dr("OldValue") = CType(dr("OldValue"), DateTime).ToShortDateString
                        If Not dr.IsNull("NewValue") Then dr("NewValue") = CType(dr("NewValue"), DateTime).ToShortDateString
                    End If

                Case "Bit"
                    If Not dr.IsNull("OldValue") Then dr("OldValue") = CType(dr("OldValue"), Boolean).ToString
                    If Not dr.IsNull("NewValue") Then dr("NewValue") = CType(dr("NewValue"), Boolean).ToString

                Case Else
                    '   If the field has a DDLId then we need to lookup the values
                    If Not dr.IsNull("DDLId") Then
                        Dim objDdls As New DDLS
                        If Not dr.IsNull("OldValue") Then
                            dr("OldValue") = objDdls.GetDDLValue(dr("DDLId"), dr("OldValue").ToString())
                        End If
                        If Not dr.IsNull("NewValue") Then
                            dr("NewValue") = objDdls.GetDDLValue(dr("DDLId"), dr("NewValue").ToString())
                        End If
                    Else
                        '   It's not a DDL

                        '   Special cases.
                        Select Case CType(dr("FldId"), Integer)
                            Case 760, 718
                                '   Relationship Enumeration
                                If Not dr.IsNull("OldValue") Then dr("OldValue") = [Enum].GetName(GetType(Relationship), Integer.Parse(dr("OldValue").ToString))
                                If Not dr.IsNull("NewValue") Then dr("NewValue") = [Enum].GetName(GetType(Relationship), Integer.Parse(dr("NewValue").ToString))

                            Case 642
                                '   BillingMethod Enumeration
                                If Not dr.IsNull("OldValue") Then dr("OldValue") = [Enum].GetName(GetType(BillingMethod), Integer.Parse(dr("OldValue").ToString))
                                If Not dr.IsNull("NewValue") Then dr("NewValue") = [Enum].GetName(GetType(BillingMethod), Integer.Parse(dr("NewValue").ToString))

                            Case 427
                                '   EducationInstitution on plStudentEducation. 
                                '   Field can refer a College on adColleges or a Highschool on syInstitutions.

                                db.ClearParameters()

                                Dim eduInstType As String
                                Dim objDdls As New DDLS
                                sb = New StringBuilder

                                Select Case resourceId
                                    Case 90
                                        With sb
                                            .Append("SELECT EducationInstType ")
                                            .Append("FROM adLeadEducation ")
                                            .Append("WHERE LeadEducationId = ?")
                                            '.Append("SELECT EducationInstType ")
                                            '.Append("FROM plStudentEducation ")
                                            '.Append("WHERE StuEducationId = ?")
                                        End With
                                        db.AddParameter("@StuEducationId", strRowIds, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)

                                    Case 145
                                        With sb
                                            .Append("SELECT EducationInstType ")
                                            .Append("FROM adLeadEducation ")
                                            .Append("WHERE LeadEducationId = ?")
                                        End With
                                        db.AddParameter("@LeadEducationId", strRowIds, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
                                End Select

                                eduInstType = db.RunParamSQLScalar(sb.ToString)

                                Select Case eduInstType
                                    Case "Schools"
                                        dr("DDLId") = 119   '   HighSchools DDL
                                        dr("Caption") = "School Name"
                                        If Not dr.IsNull("OldValue") Then dr("OldValue") = objDdls.GetDDLValue(dr("DDLId"), dr("OldValue").ToString())
                                        If Not dr.IsNull("NewValue") Then dr("NewValue") = objDdls.GetDDLValue(dr("DDLId"), dr("NewValue").ToString())

                                    Case "College"
                                        dr("DDLId") = 59    '   Colleges DDL
                                        dr("Caption") = "College Name"
                                        If Not dr.IsNull("OldValue") Then dr("OldValue") = objDdls.GetDDLValue(dr("DDLId"), dr("OldValue").ToString())
                                        If Not dr.IsNull("NewValue") Then dr("NewValue") = objDdls.GetDDLValue(dr("DDLId"), dr("NewValue").ToString())
                                End Select

                            Case 941
                                '   MeetDays (Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday)

                                If Not dr.IsNull("OldValue") Then dr("OldValue") = ConvertIntegerToMeetDaysValues(dr("OldValue").ToString)
                                If Not dr.IsNull("NewValue") Then dr("NewValue") = ConvertIntegerToMeetDaysValues(dr("NewValue").ToString)

                        End Select

                        '   If it's a uniqueidentifier and not a DDL, delete it from the table because we do not want to show it.
                        If dr("FldType") = "Uniqueidentifier" And dr.IsNull("DDLId") Then dr.Delete()
                    End If
            End Select
        Next

        dt.AcceptChanges()  'Commit changes if there were any.

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return dt
    End Function

    Public Function GetLeadIdFromStudentId(ByVal StudentId As String) As String
        Dim leadId As String
        Try
            '   connect to the database
            Dim db As New DataAccess
            Dim ReturnStudentId As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("Select LeadId from adLeads where StudentId = ? ")
            End With

            db.AddParameter("@stdId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query
            ReturnStudentId = db.RunParamSQLScalar(sb.ToString).ToString

            Return ReturnStudentId
            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try


    End Function

#End Region


#Region "Private Methods"

    Private Function ConvertIntegerToMeetDaysValues(ByVal cblValue As Integer) As String
        Dim resultString As String = String.Empty

        For i As Integer = 6 To 0 Step -1
            Dim power2 As Integer = PowerOfTwo(i)
            If (cblValue >= power2) Then

                cblValue -= power2

                If resultString <> "" Then resultString = ", " & resultString

                Select Case power2
                    Case AdvantageCommonValues.MeetDays.Monday
                        resultString = "Monday" & resultString

                    Case AdvantageCommonValues.MeetDays.Tuesday
                        resultString = "Tuesday" & resultString

                    Case AdvantageCommonValues.MeetDays.Wednesday
                        resultString = "Wednesday" & resultString

                    Case AdvantageCommonValues.MeetDays.Thursday
                        resultString = "Thursday" & resultString

                    Case AdvantageCommonValues.MeetDays.Friday
                        resultString = "Friday" & resultString

                    Case AdvantageCommonValues.MeetDays.Saturday
                        resultString = "Saturday" & resultString

                    Case AdvantageCommonValues.MeetDays.Sunday
                        resultString = "Sunday" & resultString
                End Select

            End If
        Next

        Return resultString
    End Function

    Private Function PowerOfTwo(ByVal power As Integer) As Integer
        PowerOfTwo = 1
        If power = 0 Then Return PowerOfTwo
        For i = 1 To power
            PowerOfTwo = PowerOfTwo * 2
        Next
        Return PowerOfTwo
    End Function

#End Region


End Class

