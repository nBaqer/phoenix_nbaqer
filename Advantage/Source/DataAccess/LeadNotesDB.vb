Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' LeadNotesDB.vb
'
' LeadNotesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class LeadNotesDB
    Public Function GetNotesForLeadByModuleDS(ByVal LeadId As String, ByVal modCode As String, ByVal fromDate As Date, ByVal toDate As Date) As DataSet

        '   create dataset
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        ' US2166 Janet Robinson 1/19/2012 Added LeadName
        '   build the sql query for the LeadNotes dataadapter
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("         SN.LeadNoteId, ")
            .Append("         SN.LeadNoteDescrip, ")
            .Append("         SN.ModuleCode, ")
            .Append("         SN.UserId, ")
            .Append("         (Select FullName from syUsers where UserId=SN.UserId) As UserName, ")
            .Append("         SN.CreatedDate, ")
            .Append("         SN.ModUser, ")
            .Append("         SN.ModDate, ")
            .Append("         SL.FirstName + ' ' + SL.LastName AS LeadName ")
            .Append("FROM     adLeadNotes SN ")
            .Append("         INNER JOIN syStatuses ST ON SN.StatusId = ST.StatusId ")
            .Append("         INNER JOIN adLeads SL ON SN.LeadId = SL.LeadId ")
            .Append("WHERE    SN.LeadId = ? ")
            .Append("AND      SN.CreatedDate > ? ")
            .Append("AND      SN.CreatedDate < ? ")
            If Not modCode = "UK" Then
                .Append("AND      SN.ModuleCode = ? ")
            End If
            'DE7169 2/9/2012 Janet Robinson added sort order 
            .Append("ORDER BY SN.CreatedDate desc ")
        End With

        '   build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
        sc.Parameters.Add(New OleDbParameter("@LeadNoteId", LeadId))
        sc.Parameters.Add(New OleDbParameter("@FromDate", fromDate))
        sc.Parameters.Add(New OleDbParameter("@ToDate", toDate.AddDays(1)))
        If Not modCode = "UK" Then
            sc.Parameters.Add(New OleDbParameter("@ModCode", modCode))
        End If

        '   Create adapter to handle GradeSystems table
        Dim da As New OleDbDataAdapter(sc)

        '   Fill LeadNotes table
        da.Fill(ds, "LeadNotes")

        '   build select query for the Modules data adapter
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         ModuleCode, ")
            .Append("         ModuleName ")
            .Append("FROM     syModules ")
            .Append("WHERE  ")
            .Append("         ModuleCode In (Select ModuleCode from adLeadNotes where LeadId = ?) ")
        End With

        '   build select command
        Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
        sc1.Parameters.Add(New OleDbParameter("@LeadId", LeadId))

        '   Create adapter to handle GradeSystems table
        Dim da1 As New OleDbDataAdapter(sc1)

        '   Fill LeadNotes table
        da1.Fill(ds, "Modules")

        '   create primary and foreign key constraints

        '   set primary key for syModules table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("Modules").Columns("ModuleCode")
        ds.Tables("Modules").PrimaryKey = pk0

        '   set foreign key column in adLeadNotes
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("LeadNotes").Columns("ModuleCode")

        '   set ModulesLeadNotes relation
        ds.Relations.Add("ModulesLeadNotes", pk0, fk0)

        '   return dataset
        Return ds

    End Function
    Public Function GetLeadNoteInfo(ByVal LeadNoteId As String) As LeadNoteInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("    SN.LeadNoteId, ")
            .Append("    SN.StatusId, ")
            .Append("    SN.LeadId, ")
            .Append("    SN.LeadNoteDescrip, ")
            .Append("    SN.UserId, ")
            .Append("    (Select FullName from syUsers where UserId=SN.UserId) As UserName, ")
            .Append("    SN.ModuleCode, ")
            .Append("    SN.CreatedDate, ")
            .Append("    SN.ModUser, ")
            .Append("    SN.ModDate ")
            .Append("FROM  adLeadNotes SN ")
            .Append("WHERE SN.LeadNoteId= ? ")
        End With

        ' Add the LeadNoteId to the parameter list
        db.AddParameter("@LeadNoteId", LeadNoteId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim LeadNoteInfo As New LeadNoteInfo

        While dr.Read()

            '   set properties with data from DataReader
            With LeadNoteInfo
                .LeadNoteId = LeadNoteId
                .IsInDB = True
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .LeadId = CType(dr("LeadId"), Guid).ToString
                .Description = dr("LeadNoteDescrip")
                .UserId = CType(dr("UserId"), Guid).ToString
                .UserName = dr("UserName")
                .ModuleCode = dr("ModuleCode")
                .CreatedDate = dr("CreatedDate")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return LeadNoteInfo

    End Function
    Public Function UpdateLeadNoteInfo(ByVal LeadNoteInfo As LeadNoteInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE adLeadNotes Set LeadNoteId = ?, ")
                .Append(" StatusId = ?, LeadId = ?, LeadNoteDescrip = ?, ")
                .Append(" ModuleCode = ?, UserId = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE LeadNoteId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from adLeadNotes where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   LeadNoteId
            db.AddParameter("@LeadNoteId", LeadNoteInfo.LeadNoteId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", LeadNoteInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   LeadId
            db.AddParameter("@LeadId", LeadNoteInfo.LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            '   LeadNoteDescrip
            db.AddParameter("@LeadNoteDescrip", LeadNoteInfo.Description, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModuleCode
            db.AddParameter("@ModuleCode", LeadNoteInfo.ModuleCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   UserId
            db.AddParameter("@UserId", LeadNoteInfo.UserId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   LeadNoteId
            db.AddParameter("@LeadNoteId", LeadNoteInfo.LeadNoteId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", LeadNoteInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddLeadNoteInfo(ByVal LeadNoteInfo As LeadNoteInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT adLeadNotes (LeadNoteId, LeadId, StatusId, ")
                .Append("   LeadNoteDescrip, ModuleCode, UserId, CreatedDate, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   LeadNoteId
            db.AddParameter("@LeadNoteId", LeadNoteInfo.LeadNoteId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   LeadId
            db.AddParameter("@LeadId", LeadNoteInfo.LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", LeadNoteInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   LeadNoteDescrip
            db.AddParameter("@LeadNoteDescrip", LeadNoteInfo.Description, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModuleCode
            db.AddParameter("@ModuleCode", LeadNoteInfo.ModuleCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   UserId
            db.AddParameter("@UserId", LeadNoteInfo.UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CreatedDate
            Dim now As Date = Date.Now
            db.AddParameter("@CreatedDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteLeadNoteInfo(ByVal LeadNoteId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM adLeadNotes ")
                .Append("WHERE LeadNoteId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM adLeadNotes WHERE LeadNoteId = ? ")
            End With

            '   add parameters values to the query

            '   LeadNoteId
            db.AddParameter("@LeadNoteId", LeadNoteId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   LeadNoteId
            db.AddParameter("@LeadNoteId", LeadNoteId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetMinAndMaxDatesFromNotes() As Date()
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New System.Text.StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       Min(SN.CreatedDate) As MinDate, ")
            .Append("       Max(SN.CreatedDate) As MaxDate ")
            .Append("FROM   adLeadNotes SN ")
        End With

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   fill date Array 
        Dim dateArray(1) As DateTime
        While dr.Read()
            If Not dr("MinDate") Is System.DBNull.Value Then dateArray(0) = CType(dr("MinDate"), DateTime) Else dateArray(0) = Date.Now.Subtract(New TimeSpan(1824, 0, 0, 0))
            If Not dr("MaxDate") Is System.DBNull.Value Then dateArray(1) = CType(dr("MaxDate"), DateTime) Else dateArray(1) = Date.Now.Add(New TimeSpan(1824, 0, 0, 0))
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return date array
        Return dateArray

    End Function
End Class