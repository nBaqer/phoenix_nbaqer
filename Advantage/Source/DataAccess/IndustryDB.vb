Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' IndustryDB.vb
'
' IndustryDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class IndustryDB
    Public Function GetAllIndustry() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("select Ind.IndustryId,Ind.IndustryCode,Ind.IndustryDescrip ")
            .Append("FROM     plIndustries Ind,syStatuses ST ")
            .Append("WHERE    Ind.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY Ind.IndustryDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function

    Public Function GetAllIndustryForCampus(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   Ind.IndustryId,Ind.IndustryCode,Ind.IndustryDescrip ")
            .Append("FROM  plIndustries Ind,syStatuses ST ")
            .Append("WHERE Ind.StatusId = ST.StatusId ")
            .Append("AND ST.Status = 'Active' ")
            .Append("AND Ind.CampGrpId IN (SELECT CampGrpId FROM   syCampuses C ,syCmpGrpCmps CGC WHERE  CGC.CampusId = C.CampusId AND C.CampusId = ?) ")
            .Append("ORDER BY Ind.IndustryDescrip ")
        End With

        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function

End Class


