
Public Class PhoneTypesDB

    Public Function GetPhoneList() As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strSQLString As New StringBuilder
        Dim da As New OleDbDataAdapter
        db.OpenConnection()
        With strSQLString
            .Append("SELECT PhoneTypeId, PhoneTypeDescrip, Sequence ")
            .Append("FROM syPhoneType")
            .Append(" ORDER BY Sequence")
        End With
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "PhoneList")
        Catch ex As System.Exception
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)
        db.CloseConnection()
        Return ds
    End Function
    Public Function AddPhoneType(ByVal PhoneTypeId As Guid, ByVal PhoneTypeDescrip As String, ByVal Sequence As Int32)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("INSERT INTO syPhoneType")
            .Append("(PhoneTypeId, PhoneTypeDescrip, Sequence) ")
            .Append("VALUES(?,?,?)")
        End With
        db.AddParameter("@phonetypeid", PhoneTypeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@phonetypedescrip", PhoneTypeDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sequence", Sequence, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
    End Function
    Public Function UpdatePhoneType(ByVal PhoneTypeId As Guid, ByVal PhoneTypeDescrip As String, ByVal Sequence As Int32)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("UPDATE syPhoneType SET ")
            .Append("PhoneTypeDescrip = ?")
            .Append(",Sequence = ?")
            .Append(" WHERE PhoneTypeId = ? ")
        End With

        db.AddParameter("@phonetypedescrip", PhoneTypeDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sequence", Sequence, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        db.AddParameter("@phonetypeid", PhoneTypeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Function
    Public Function DeletePhoneType(ByVal PhoneTypeId As Guid)

        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("DELETE FROM syPhoneType WHERE PhoneTypeId = ?")
        End With
        db.AddParameter("@phonetypeid", PhoneTypeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Function
    Public Function SaveChangesToDB(ByRef newDS As DataSet)
        Dim dt1, dt2, dt3 As DataTable
        Dim dr As DataRow
        Dim ds1 As New DataSet
        Dim ds As New DataSet
        Try
            'Pull the dataset from Session.
            ds = newDS

            ' CODE BELOW HANDLES AN INSERT/UPDATE INTO THE TABLE(S)

            'Run getchanges method on the dataset to see which rows have changed.
            'This section handles the changes to the Advantage Required fields
            dt1 = newDS.Tables("PhoneList")
            dt2 = dt1.GetChanges(DataRowState.Added Or DataRowState.Deleted Or DataRowState.Modified)

            If Not IsNothing(dt2) Then
                For Each dr In dt2.Rows
                    If dr.RowState = DataRowState.Added Then
                        Try
                            AddPhoneType(dr("PhoneTypeId"), dr("PhoneTypeDescrip"), dr("Sequence"))
                            'Insert the new record
                        Catch ex As System.Exception
                            'Throw New BaseException(ex.InnerException.Message)
                        End Try
                    ElseIf dr.RowState = DataRowState.Deleted Then
                        'Delete this record
                        DeletePhoneType(dr("PhoneTypeId", DataRowVersion.Original))
                    ElseIf dr.RowState = DataRowState.Modified Then
                        'Update the record (change of sequence)
                        UpdatePhoneType(dr("PhoneTypeId"), dr("PhoneTypeDescrip"), dr("Sequence"))
                    End If
                Next
            End If
            dt1.AcceptChanges()
            'Return
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try

    End Function
End Class
