
Imports FAME.Advantage.Common

Public Class TransferStudentsToAnotherProgramDB
    Public Function StudentSearchResults(ByVal strLastName As String, ByVal strFirstName As String, ByVal strSSN As String, ByVal strDOB As String, ByVal strCampusId As String, ByVal PrgVerId As String) As DataSet
        '   connect to the database
        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append(" SELECT Distinct  S1.StudentId,S1.FirstName,S1.LastName,S1.MiddleName,S1.SSN ")
            .Append(" FROM  arStudent S1,arStuEnrollments S2 where  ")
            .Append(" S1.StudentId = S2.StudentId AND S2.CampusId= '" & strCampusId & "' ")
            Dim andOrOrOperator As String = " AND "
            If Not strLastName = "" Then
                .Append(andOrOrOperator)
                .Append(" LastName like  + ? + '%'")
                db.AddParameter("@LastName", strLastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If
            If Not strFirstName = "" Then
                .Append(andOrOrOperator)
                .Append(" FirstName like  + ? + '%'")
                db.AddParameter("@FirstName", strFirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If
            If Not strSSN = "" Then
                .Append(andOrOrOperator)
                .Append(" SSN = ? ")
                db.AddParameter("@SSN", strSSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If
            If Not strDOB = "" Then
                .Append(andOrOrOperator)
                .Append(" DOB = ? ")
                db.AddParameter("@DOB", strSSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            If Not PrgVerId = "" Then
                .Append(andOrOrOperator)
                .Append(" S2.PrgVerId = ? ")
                db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            .Append(" ORDER BY S1.LastName ")
        End With
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function IsStudentAlreadyEnrolled(ByVal StudentId As String, ByVal PrgVerId As String) As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim intAlreadyExists As Integer = 0

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("Select Count(*) from arStuEnrollments where StudentId=? and PrgVerId=? ")
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            intAlreadyExists = db.RunParamSQLScalar(sb.ToString)
        Catch ex As Exception
            intAlreadyExists = 0
        Finally
            sb.Remove(0, sb.Length - 1)
            db.ClearParameters()
        End Try
        Return intAlreadyExists
    End Function
    Public Function GetCommonCoursesAndGrades(ByVal StudentId As String, ByVal SourcePrgVerId As String, ByVal TargetPrgVerId As String, Optional ByVal Campusid As String = "", Optional ByVal GetScheduledClasses As Boolean = False) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("Select Distinct Top 1 StuEnrollId from arStuEnrollments where StudentId=? and PrgVerId=? ")
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", SourcePrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Dim StuEnrollId As String = db.RunParamSQLScalar(sb.ToString).ToString
        sb.Remove(0, sb.Length - 1)
        db.ClearParameters()

        With sb
            '.Append("SELECT DISTINCT ")
            '.Append("       t4.TermId,t3.TermDescrip,t4.ReqId,t2.Code,t2.Descrip AS Descrip,")
            '.Append("       t2.Credits,t2.Hours,t2.CourseCategoryId,t3.StartDate AS StartDate,t3.EndDate AS EndDate,")
            '.Append("       t1.GrdSysDetailId,t1.TestId,t1.ResultId,")
            '.Append("       (Select Grade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId)as Grade,")
            '.Append("       (Select IsPass from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsPass,")
            '.Append("       (Select GPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as GPA,")
            '.Append("       (Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsAttempted,")
            '.Append("       (Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsEarned,")
            '.Append("       (Select IsInGPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInGPA,")
            '.Append("       (Select IsDrop from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsDrop,")
            '.Append("       (Select Descrip from arCourseCategories where CourseCategoryId = t2.CourseCategoryId) as CourseCategory ")
            '.Append("FROM   arResults t1, arReqs t2, arTerm t3, arClassSections t4, arClassSectionTerms t5, arStuEnrollments t6  ")
            '.Append("WHERE  t1.TestId = t4.ClsSectionId")
            '.Append("       AND t4.ClsSectionId = t5.ClsSectionId")
            '.Append("       AND t5.TermId = t3.TermId")
            '.Append("       AND t4.ReqId = t2.ReqId")
            '.Append("       AND t1.StuEnrollId = t6.StuEnrollId")
            '.Append("       AND t1.GrdSysDetailId is not null")
            '.Append("       AND t1.StuEnrollId = ? ")
            '.Append("       AND t2.ReqId in (select Distinct ReqId from arProgVerDef where PrgVerId=?) ")
            '.Append("       AND t1.IsTransfered = 0 ")
            '.Append(" UNION ")
            '.Append("SELECT DISTINCT ")
            '.Append("       t10.TermId,t30.TermDescrip,t10.ReqId,t20.Code,t20.Descrip AS Descrip,")
            '.Append("       t20.Credits,t20.Hours,t20.CourseCategoryId,t30.StartDate AS StartDate,t30.EndDate AS EndDate,")
            '.Append("       t10.GrdSysDetailId,'{00000000-0000-0000-0000-000000000000}' AS TestId,t10.TransferId AS ResultId,")
            '.Append("       (Select Grade from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId)as Grade,")
            '.Append("       (Select IsPass from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsPass,")
            '.Append("       (Select GPA from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as GPA,")
            '.Append("       (Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsCreditsAttempted,")
            '.Append("       (Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsCreditsEarned,")
            '.Append("       (Select IsInGPA from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsInGPA,")
            '.Append("       (Select IsDrop from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsDrop,")
            '.Append("       (Select Descrip from arCourseCategories where CourseCategoryId = t20.CourseCategoryId) as CourseCategory ")
            '.Append("FROM   arTransferGrades t10, arReqs t20, arTerm t30 ")
            '.Append("WHERE  t10.TermId = t30.TermId")
            '.Append("       AND t10.ReqId = t20.ReqId ")
            '.Append("       AND t10.GrdSysDetailId is not null ")
            '.Append("       AND t10.StuEnrollId = ? ")
            '.Append("       AND t20.ReqId in (select Distinct ReqId from arProgVerDef where PrgVerId=?) ")
            '.Append("ORDER BY StartDate,EndDate,TermDescrip")

            .Append("   Select DISTINCT ")
            .Append("       (select Distinct TermDescrip from arTerm where TermId=t4.TermId) as TermDescrip, ")
            .Append("       (select Distinct StartDate from arTerm where TermId=t4.TermId) as StartDate, ")
            .Append("       t4.ReqId,t2.Code,t2.Descrip AS Descrip,t2.Credits,t1.GrdSysDetailId,t1.TestId,t1.ResultId,t4.TermId,t1.IsCourseCompleted AS IsCourseCompleted, ")
            If Not MyAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                .Append("       (Select Distinct Grade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId)as Grade ")
            Else
                .Append("       t1.Score as Grade ")
            End If
            .Append("   FROM   arResults t1, arReqs t2, arClassSections t4 ")
            .Append("   WHERE  t1.StuEnrollId=? and t1.TestId = t4.ClsSectionId and t2.ReqId = t4.ReqId  ")

            If GetScheduledClasses = False Then
                If Not MyAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                    .Append("   and t1.GrdSysDetailId is not null  ")
                Else
                    .Append("  and t1.score is not null ")
                End If
            End If

            'Commented by balaji on 03/08/2008 
            'schools may require to transfer courses that are not common,so the following 3 lines are commented
            '.Append("   AND t2.ReqId in (select Distinct ReqId from arProgVerDef where PrgVerId=? ")
            '.Append("   Union Select Distinct ReqId from arReqGrpDef where GrpId in (select Distinct ReqId from arProgVerDef where PrgVerId=?) ")
            '.Append("   ) ")
            .Append("   AND t1.IsTransfered = 0 ")
            .Append("   UNION ")
            .Append("   Select  DISTINCT ")
            .Append("       t30.TermDescrip,t30.StartDate AS StartDate,t10.ReqId,t20.Code,t20.Descrip AS Descrip, ")
            .Append("       t20.Credits,t10.GrdSysDetailId,'{00000000-0000-0000-0000-000000000000}' AS TestId,t10.TransferId AS ResultId,t30.TermId,1 AS IsCourseCompleted, ")
            If Not MyAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                .Append("       (Select Distinct Grade from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId)as Grade ")
            Else
                .Append("       t10.Score as Grade ")
            End If
            .Append("       FROM   arTransferGrades t10, arReqs t20, arTerm t30  ")
            .Append("       WHERE t10.TermId = t30.TermId AND t10.ReqId = t20.ReqId and t10.IsTransferred=0 ")
            If Not MyAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                .Append("       AND t10.GrdSysDetailId is not null ")
            Else
                .Append("  and t10.score is not null ")
            End If
            .Append("       AND t10.StuEnrollId = ? ")
            'Commented by balaji on 03/08/2008 
            'schools may require to transfer courses that are not common,so the following 3 lines are commented
            '.Append("       AND t20.ReqId in (select Distinct ReqId from arProgVerDef where PrgVerId=?  ")
            '.Append("       Union Select Distinct ReqId from arReqGrpDef where GrpId in (select Distinct ReqId from arProgVerDef where PrgVerId=?) ")
            '.Append("   ) ")
            'If SingletonAppSettings.AppSettings("GradesFormat",CampusId).ToString.ToLower = "numeric" Then
            '    .Append(" Union ")
            '    .Append(" Select Distinct ")
            '    .Append("   (select Distinct TermDescrip from arTerm where TermId=t1.TermId) as TermDescrip,  ")
            '    .Append("   (select Distinct StartDate from arTerm where TermId=t1.TermId) as StartDate,  ")
            '    .Append("   t2.ReqId,t2.Code,t2.Descrip AS Descrip,t2.Credits,t1.GrdSysDetailId, ")
            '    .Append("   NULL as TestId,t1.TransferId as ResultId,  ")
            '    .Append("   t1.Score as Grade  FROM   arTransferGrades t1, arReqs t2 ")
            '    .Append("   WHERE  t1.StuEnrollId=? and  t1.ReqId = t2.ReqId and t1.IsTransferred=0 ")
            '    .Append("   and Score is not null ")
            '    .Append("   AND t2.ReqId in (select Distinct ReqId from arProgVerDef where PrgVerId=? ")
            '    .Append("   Union Select Distinct ReqId from arReqGrpDef where GrpId in (select Distinct ReqId from arProgVerDef where PrgVerId=?) ")
            '    .Append("   ) ")
            'End If
            .Append("   ORDER BY StartDate,TermDescrip ")
        End With
        db.AddParameter("@stuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@PrgVerId", TargetPrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@PrgVerId", TargetPrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@PrgVerId", TargetPrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ' db.AddParameter("@PrgVerId", TargetPrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'If SingletonAppSettings.AppSettings("GradesFormat",CampusId).ToString.ToLower = "numeric" Then
        '    db.AddParameter("@stuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '    db.AddParameter("@PrgVerId", TargetPrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '    db.AddParameter("@PrgVerId", TargetPrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'End If
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds
    End Function
    Public Function GetSourceCampusId(ByVal CampusId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("Select Distinct CampDescrip from syCampuses where CampusId=? ")
        End With
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Dim drStatusCode As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim strCampusDescrip As String
        While drStatusCode.Read()
            If Not (drStatusCode("CampDescrip") Is DBNull.Value) Then strCampusDescrip = CType(drStatusCode("CampDescrip"), String).ToString Else strCampusDescrip = ""
        End While
        If Not drStatusCode.IsClosed Then drStatusCode.Close()
        db.ClearParameters()
        Return strCampusDescrip
    End Function
    Public Function GetAllCampusId() As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("Select Distinct CampusId,CampDescrip from syCampuses ")
        End With

        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetTargetCampusId(ByVal userid As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet
        Dim strActiveStatus As String = AdvantageCommonValues.ActiveGuid

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append(" select Distinct C.CampusId,C.CampDescrip from  ")
            .Append(" syUsersRolesCampGrps URC,syCmpGrpCmps CGC,syCampuses C ")
            .Append(" where URC.UserId=? and ")
            .Append(" URC.CampGrpId = CGC.CampGrpId And CGC.CampusId = C.CampusId ")
            .Append(" and C.StatusId=? ")
        End With
        db.AddParameter("@UserId", userid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StatusId", strActiveStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds
    End Function
    Public Function GetProgram(ByVal CampusId As String, ByVal UserId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet
        Dim strActiveStatus As String = AdvantageCommonValues.ActiveGuid

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'With sb
        '    .Append("Select Distinct ProgId,ProgDescrip from arPrograms ")
        'End With
        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        With sb
            .Append("   SELECT Distinct P.ProgId,P.ProgDescrip from arPrograms P ")
            .Append("   WHERE   ")
            .Append("   P.StatusId = '" & strActiveGUID & "' ")
            .Append("   AND P.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("   ORDER BY P.ProgDescrip ")
        End With
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds
    End Function
    Public Function GetTargetProgram(ByVal CampusId As String, ByVal UserId As String, Optional ByVal SourceProgramId As String = "") As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet
        Dim strActiveStatus As String = AdvantageCommonValues.ActiveGuid

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'With sb
        '    .Append("Select Distinct ProgId,ProgDescrip from arPrograms ")
        '    If Not SourceProgramId = "" Then
        '        .Append(" where ProgId <> ? ")
        '    End If
        'End With
        Dim dsGetCmpGrps As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        With sb
            .Append("   SELECT Distinct P.ProgId,P.ProgDescrip,")
            .Append(" case when (select ShiftDescrip from arShifts where shiftid=P.shiftid) is null  then ")
            .Append(" P.ProgDescrip ")
            .Append(" else ")
            .Append("P.ProgDescrip + ' (' + (select ShiftDescrip from arShifts where shiftid=P.shiftid) + ')'  ")
            .Append(" end as ShiftDescrip  ")
            .Append(" from arPrograms P ")
            .Append("   WHERE   ")
            .Append("   P.StatusId = '" & strActiveGUID & "' ")
            If Not SourceProgramId = "" Then
                .Append(" AND P.ProgId <> ? ")
            End If
            .Append("   AND P.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("   ORDER BY P.ProgDescrip ")
        End With
        db.AddParameter("@ProgId", SourceProgramId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds
    End Function
    Public Function GetSchedules(ByVal CampusId As String, ByVal UserId As String, Optional ByVal TargetPrgVerId As String = "", Optional ByVal SourcePrgVerId As String = "") As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet
        Dim strActiveStatus As String = AdvantageCommonValues.ActiveGuid

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim dsGetCmpGrps As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        With sb
            .Append(" select Distinct t1.ScheduleId,t1.Descrip from arProgSchedules t1,arPrgVersions t2 where " + vbCrLf)
            .Append(" t1.PrgVerId = t2.PrgVerId and t1.PrgVerId=? and " + vbCrLf)
            .Append(" t2.UnitTypeId in (select Distinct UnitTypeId from arPrgVersions where PrgVerId=?)" + vbCrLf)
            .Append(" and t2.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("   ORDER BY t1.Descrip ")
        End With
        db.AddParameter("@PrgVerId", TargetPrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", SourcePrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds
    End Function
    Public Function GetSourceProgramVersion(ByVal ProgramId As String, ByVal StudentId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet
        Dim strActiveStatus As String = AdvantageCommonValues.ActiveGuid

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("Select Distinct t1.PrgVerId,t1.PrgVerDescrip from arPrgVersions t1,arStuEnrollments t2,arStudent t3 ")
            .Append("where t1.ProgId=? and t1.PrgVerId=t2.PrgVerId and t2.StudentId = t3.StudentId and t3.StudentId=? ")
        End With
        db.AddParameter("@ProgId", ProgramId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds
    End Function
    Public Function GetTargetProgramVersion(ByVal ProgramId As String, ByVal CampusId As String, ByVal SourcePrgVerId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet
        Dim strActiveStatus As String = AdvantageCommonValues.ActiveGuid

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("Select Distinct t1.PrgVerId,t1.PrgVerDescrip,")
            .Append(" case when (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=t1.ProgId) is null  then ")
            .Append(" t1.PrgVerDescrip ")
            .Append(" else ")
            .Append(" t1.PrgVerDescrip + ' (' + (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=t1.ProgId) + ')'  ")
            .Append(" end as PrgVerShiftDescrip  ")
            .Append(" from arPrgVersions t1,syCmpGrpCmps t2 ")
            .Append("where t1.ProgId=? and t1.CampGrpId=t2.CampGrpId and t2.CampusId=? and t1.PrgVerId <> ? ")
            .Append(" and t1.StatusId=? ")
        End With
        db.AddParameter("@ProgId", ProgramId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", SourcePrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StatusId", strActiveStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds
    End Function
    Public Function GetProgramVersion(ByVal CampusId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet
        Dim strActiveStatus As String = AdvantageCommonValues.ActiveGuid

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("Select Distinct PrgVerId,PrgVerDescrip from arPrgVersions t1,syCmpGrpCmps t2 ")
            .Append("where t1.CampGrpId=t2.CampGrpId and t2.CampusId=? ")
        End With
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds
    End Function
    Public Function GetGradesByResultId(ByVal PrgVerId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT Distinct ")
            .Append("       GrdSysDetailId, ")
            .Append("       Grade ")
            .Append("FROM  ")
            .Append("       arGradeSystemDetails ")
            .Append("WHERE ")
            .Append("       GrdSystemId in (select GrdSystemId from arPrgVersions where PrgVerId=?) ")
            .Append("ORDER BY Grade ")
        End With

        'add resultId to the parameter list
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function

    Public Function GetTermsByProgram(ByVal TargetProgramId As String, Optional ByVal CampusId As String = "") As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query

        Dim sb As New StringBuilder
        Dim dsGetCmpGrps As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        With sb
            .Append("SELECT Distinct ")
            .Append("       TermId, ")
            .Append("       TermDescrip ")
            .Append("FROM  ")
            .Append("       arTerm ")
            .Append("WHERE ")
            .Append("       (ProgId=? or ProgId is NULL) and StatusId=? ")
            .Append(" and CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("ORDER BY TermDescrip ")
        End With

        'add resultId to the parameter list
        db.AddParameter("@ProgId", TargetProgramId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StatusId", strActiveGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function

    Public Function InsertStudentEnrollment(ByVal obj As TransferEnrollmentObj) As String
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        Dim sb As New StringBuilder
        Dim strReturnMessage As String

        Dim myAdvAppSettings As AdvAppSettings = New AdvAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Dim academicAdvisor, admissionsRep, billingMethodId, edLvlId As String
        Dim tuitionCategoryId, faAdvisorId, attendTypeId, degCertSeekingId As String
        'Dim originalEnrollment As String
        Dim badgeNo As String = ""

        ''While Transferring the student from one program to another a new enrollment is created and the details are moved from old enrollment to new Enrollment.
        With sb
            .Append(" select distinct StuEnrollId,AcademicAdvisor, AdmissionsRep, BillingMethodId, EdLvlId, ShiftId, ")
            .Append(" TuitionCategoryId,FAAdvisorId,AttendTypeId,DegCertSeekingId,BadgeNumber from ")
            .Append(" arStuEnrollments where StudentId=? and PrgVerId=? ")
        End With
        db.AddParameter("@Studentid", obj.StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@SourcePrgVerId", obj.SourcePrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()

            If Not (dr("AcademicAdvisor") Is DBNull.Value) Then academicAdvisor = CType(dr("AcademicAdvisor"), Guid).ToString Else academicAdvisor = Nothing
            If Not (dr("AdmissionsRep") Is DBNull.Value) Then admissionsRep = CType(dr("AdmissionsRep"), Guid).ToString Else admissionsRep = Nothing
            If Not (dr("BillingMethodId") Is DBNull.Value) Then billingMethodId = CType(dr("BillingMethodId"), Guid).ToString Else billingMethodId = Nothing
            If Not (dr("EdLvlId") Is DBNull.Value) Then edLvlId = CType(dr("EdLvlId"), Guid).ToString Else edLvlId = Nothing
            If Not (dr("TuitionCategoryId") Is DBNull.Value) Then tuitionCategoryId = CType(dr("TuitionCategoryId"), Guid).ToString Else tuitionCategoryId = Nothing
            If Not (dr("FAAdvisorId") Is DBNull.Value) Then faAdvisorId = CType(dr("FAAdvisorId"), Guid).ToString Else faAdvisorId = Nothing
            If Not (dr("AttendTypeId") Is DBNull.Value) Then attendTypeId = CType(dr("AttendTypeId"), Guid).ToString Else attendTypeId = Nothing
            If Not (dr("DegCertSeekingId") Is DBNull.Value) Then degCertSeekingId = CType(dr("DegCertSeekingId"), Guid).ToString Else degCertSeekingId = Nothing
            'If Not (dr("StuEnrollId") Is DBNull.Value) Then originalEnrollment = CType(dr("StuEnrollId"), Guid).ToString Else originalEnrollment = ""
            ''To transfer Badge No from old enrollment to the new one.
            If Not (dr("BadgeNumber") Is DBNull.Value) Then badgeNo = CType(dr("BadgeNumber"), String).ToString Else badgeNo = Nothing
            ''''''''''''''''''''
        End While
        If Not dr.IsClosed Then dr.Close()
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        strReturnMessage = "Student Enrollment"

        '' DE 12167  -- The column ScheduleId was removed from arStuEnrollments table so we don't need this information
        '' Sanitize some values
        '' obj.ScheduleID = If(String.IsNullOrEmpty(obj.ScheduleID), Nothing, obj.ScheduleID)

        ' Prepare to init a transaction over the necessary changes............................................... 
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sqlTrans As SqlTransaction

        With strSQL
            .Append("INSERT INTO arStuEnrollments ")
            .Append(" (StuEnrollID, StudentId, LeadId, PrgVerId, AcademicAdvisor, AdmissionsRep, BillingMethodId, CampusId, EdLvlId, ShiftId, StatusCodeId, EnrollDate, ExpGradDate, ExpStartDate, LDA, MidPtDate, StartDate,TransferDate,DateDetermined,TuitionCategoryId,FAAdvisorId,AttendTypeId,DegCertSeekingId,ModUser,ModDate,EnrollmentId,BadgeNumber,TransferHours,TransferHoursFromThisSchoolEnrollmentId,TotalTransferHoursFromThisSchool)  ")
            .Append("  VALUES(@StuEnrollID, @StudentId,@LeadId, @PrgVerId, @AcademicAdvisor, @AdmissionsRep, @BillingMethodId, @CampusId, @EdLvlId, @ShiftId, @StatusCodeId, @EnrollDate, @ExpGradDate, @ExpStartDate, @LDA, @MidPtDate, @StartDate, @TransferDate, @DateDetermined, @TuitionCategoryId, @FAAdvisorId, @AttendTypeId, @DegCertSeekingId, @ModUser, @ModDate, @EnrollmentId, @BadgeNumber, @TransferHours,@TransferHoursFromThisSchoolEnrollmentId,@TotalTransferHoursFromThisSchool) ")
            '''''''''''
        End With


        Dim insertEnrollCommand As SqlCommand = New SqlCommand(strSQL.ToString(), sqlconn)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@StuEnrollID", obj.StuEnrollId)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@StudentID", obj.StudentID)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@LeadId", obj.LeadId)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@PrgVerId", obj.PrgVerId)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@AcademicAdvisor", academicAdvisor)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@AdmissionsRep", admissionsRep)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@BillingMethodId", billingMethodId)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@CampusId", obj.CampusId)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@EdLvlId", edLvlId)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@ShiftId", obj.ShiftId)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@StatusCodeId", obj.SchoolGuidForFutureStart)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@EnrollDate", obj.EnrollDate)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@ExpGradDate", obj.ExpGradDate)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@ExpStartDate", obj.ExpStartDate)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@LDA", Nothing)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@MidPtDate", Nothing)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@StartDate", obj.ExpStartDate)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@TransferDate", Nothing)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@DateDetermined", Nothing)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@TuitionCategoryId", tuitionCategoryId)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@FAAdvisorId", faAdvisorId)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@AttendTypeId", attendTypeId)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@DegCertSeekingId", degCertSeekingId)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@ModUser", obj.User)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@ModDate", DateTime.Now)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@EnrollmentId", obj.EnrollId)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@BadgeNumber", badgeNo)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@TransferHours", obj.TransferHours)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@TransferHoursFromThisSchoolEnrollmentId", obj.OldStuEnrollId)
        AddWithValueSafe(insertEnrollCommand.Parameters, "@TotalTransferHoursFromThisSchool", obj.TransferHours)

        '' DE 12167  -- The column ScheduleId was removed from arStuEnrollments table so we don't need this information
        '' AddWithValueSafe(insertEnrollCommand.Parameters, "@ScheduleId", obj.ScheduleID)

        ' Prepare Update of the Old enrollment. We must to put this enrollment to TRANSFER OUT and update the data of transfer
        strSQL = New StringBuilder()
        strSQL.Append(" UPDATE arStuEnrollments SET BadgeNumber = Null, StatusCodeId=@StatusCodeId, TransferDate= @TransferDate, DateDetermined= @DateDetermined  ")
        strSQL.Append(" WHERE StudentId= @StudentId and PrgVerId= @PrgVerId ")

        Dim updateOldEnrollCommand As SqlCommand = New SqlCommand(strSQL.ToString(), sqlconn)
        AddWithValueSafe(updateOldEnrollCommand.Parameters, "@StatusCodeId", obj.SchoolGuidForTransferOut)
        AddWithValueSafe(updateOldEnrollCommand.Parameters, "@TransferDate", DateTime.Now)
        AddWithValueSafe(updateOldEnrollCommand.Parameters, "@DateDetermined", DateTime.Now)
        AddWithValueSafe(updateOldEnrollCommand.Parameters, "@StudentId", obj.StudentID)
        AddWithValueSafe(updateOldEnrollCommand.Parameters, "@PrgVerId", obj.SourcePrgVerId)

        ' Prepare Insert The Transfer in the table of arTrackTransfer
        Dim sbTrackTransfer As New StringBuilder
        With sbTrackTransfer
            .Append(" INSERT INTO arTrackTransfer(TrackTransferId,StuEnrollId,SourcePrgVerId,TargetPrgVerId,SourceCampusId,TargetCampusId,ModDate,ModUser) ")
            .Append(" values(@TrackTransferId,@StuEnrollId,@SourcePrgVerId,@TargetPrgVerId,@SourceCampusId,@TargetCampusId,@ModDate,@ModUser) ")
        End With

        Dim insertTransferInTracTransferTableCommand As SqlCommand = New SqlCommand(sbTrackTransfer.ToString(), sqlconn)
        AddWithValueSafe(insertTransferInTracTransferTableCommand.Parameters, "@TrackTransferId", Guid.NewGuid.ToString)
        AddWithValueSafe(insertTransferInTracTransferTableCommand.Parameters, "@StuEnrollId", obj.OldStuEnrollId)
        AddWithValueSafe(insertTransferInTracTransferTableCommand.Parameters, "@SourcePrgVerId", obj.SourcePrgVerId)
        AddWithValueSafe(insertTransferInTracTransferTableCommand.Parameters, "@TargetPrgVerId", obj.PrgVerId)
        AddWithValueSafe(insertTransferInTracTransferTableCommand.Parameters, "@SourceCampusId", obj.SourceCampusId)
        AddWithValueSafe(insertTransferInTracTransferTableCommand.Parameters, "@TargetCampusId", obj.CampusId)
        AddWithValueSafe(insertTransferInTracTransferTableCommand.Parameters, "@ModDate", DateTime.Now)
        AddWithValueSafe(insertTransferInTracTransferTableCommand.Parameters, "@ModUser", obj.User)

        'Prepare to insert into AdLeadByLeadGroup....
        Dim sbLeadGroups As New StringBuilder
        With sbLeadGroups
            .Append(" INSERT INTO adLeadByLeadGroups(LeadGrpLeadId, StuEnrollId,LeadGrpId, ModDate, ModUser,LeadId) ")
            .Append(" SELECT NewId(),@StuEnrollId,LeadGrpId,@ModDate,@ModUser,@LeadId FROM adLeadByLeadGroups ")
            .Append(" WHERE StuEnrollId= @OldStuEnrollId ")
        End With

        Dim insertAdLeadByGroupCommand As SqlCommand = New SqlCommand(sbLeadGroups.ToString(), sqlconn)

        AddWithValueSafe(insertAdLeadByGroupCommand.Parameters, "@StuEnrollID", obj.StuEnrollId)
        AddWithValueSafe(insertAdLeadByGroupCommand.Parameters, "@ModDate", DateTime.Now)
        AddWithValueSafe(insertAdLeadByGroupCommand.Parameters, "@ModUser", obj.User)
        AddWithValueSafe(insertAdLeadByGroupCommand.Parameters, "@LeadId", Nothing)
        AddWithValueSafe(insertAdLeadByGroupCommand.Parameters, "@OldStuEnrollId", obj.OldStuEnrollId)

        ' Prepare to insert Status Change for the old enrollment
        Dim changeStatusOldEnrollmentCommand = PrepareInsertStatusChangeCommand(sqlconn)

        AddWithValueSafe(changeStatusOldEnrollmentCommand.Parameters, "@StuEnrollId", obj.OldStuEnrollId)
        AddWithValueSafe(changeStatusOldEnrollmentCommand.Parameters, "@OrigStatusId", obj.SchoolGuidCurrentState)
        AddWithValueSafe(changeStatusOldEnrollmentCommand.Parameters, "@NewStatusId", obj.SchoolGuidForTransferOut)
        AddWithValueSafe(changeStatusOldEnrollmentCommand.Parameters, "@CampusId", obj.CampusId)
        AddWithValueSafe(changeStatusOldEnrollmentCommand.Parameters, "@ModDate", DateTime.Now)
        AddWithValueSafe(changeStatusOldEnrollmentCommand.Parameters, "@ModUser", obj.User)
        AddWithValueSafe(changeStatusOldEnrollmentCommand.Parameters, "@IsRevelsal", 0)
        AddWithValueSafe(changeStatusOldEnrollmentCommand.Parameters, "@DropReasonId", Nothing)
        AddWithValueSafe(changeStatusOldEnrollmentCommand.Parameters, "@DateOfChange", DateTime.Now)

        'Prepare to Insert Status Change for the New Enrollment

        Dim changeStatusNewEnrollmentCommand = PrepareInsertStatusChangeCommand(sqlconn)
        AddWithValueSafe(changeStatusNewEnrollmentCommand.Parameters, "@StuEnrollId", obj.StuEnrollId)
        AddWithValueSafe(changeStatusNewEnrollmentCommand.Parameters, "@OrigStatusId", obj.SchoolGuidForTransferOut)
        AddWithValueSafe(changeStatusNewEnrollmentCommand.Parameters, "@NewStatusId", obj.SchoolGuidForFutureStart)
        AddWithValueSafe(changeStatusNewEnrollmentCommand.Parameters, "@CampusId", obj.CampusId)
        AddWithValueSafe(changeStatusNewEnrollmentCommand.Parameters, "@ModDate", DateTime.Now)
        AddWithValueSafe(changeStatusNewEnrollmentCommand.Parameters, "@ModUser", obj.User)
        AddWithValueSafe(changeStatusNewEnrollmentCommand.Parameters, "@IsRevelsal", 0)
        AddWithValueSafe(changeStatusNewEnrollmentCommand.Parameters, "@DropReasonId", Nothing)
        AddWithValueSafe(changeStatusNewEnrollmentCommand.Parameters, "@DateOfChange", DateTime.Now)

        '   execute Transaction......
        sqlconn.Open()
        sqlTrans = sqlconn.BeginTransaction("CreateEnrollment")
        insertEnrollCommand.Transaction = sqlTrans
        updateOldEnrollCommand.Transaction = sqlTrans
        insertTransferInTracTransferTableCommand.Transaction = sqlTrans
        insertAdLeadByGroupCommand.Transaction = sqlTrans
        changeStatusOldEnrollmentCommand.Transaction = sqlTrans
        changeStatusNewEnrollmentCommand.Transaction = sqlTrans
        Try
            ' Execute queries inside the transaction....
            insertEnrollCommand.ExecuteNonQuery()
            updateOldEnrollCommand.ExecuteNonQuery()
            insertTransferInTracTransferTableCommand.ExecuteNonQuery()
            insertAdLeadByGroupCommand.ExecuteNonQuery()
            changeStatusOldEnrollmentCommand.ExecuteNonQuery()
            changeStatusNewEnrollmentCommand.ExecuteNonQuery()

            sqlTrans.Commit()

        Catch ex As Exception
            sqlTrans.Rollback("CreateEnrollment")
            Throw
        Finally
            sqlconn.Close()
        End Try
        Return strReturnMessage
    End Function


    Private Function PrepareInsertStatusChangeCommand(sqlConn As SqlConnection) As SqlCommand

        Dim sb As StringBuilder = New StringBuilder()
        'To implement active triggers and avoid error with trigger  The target table '<Table Name>' of the DML statement cannot have any enabled triggers if the statement contains an OUTPUT clause without INTO clause.
        'we declare a memory table variable to support StudentStatusChangeID and use it with INTO Clausula"
        sb.Append(" DECLARE @OutputTable TABLE ( StudentStatusChangeIdInserted  UNIQUEIDENTIFIER ) ")
        sb.Append(" INSERT INTO syStudentStatusChanges  (StudentStatusChangeId, StuEnrollId, OrigStatusId, NewStatusId, CampusId, ModDate, ModUser, IsReversal, DropReasonId, DateOfChange) ")
        sb.Append(" OUTPUT INSERTED.StudentStatusChangeId INTO @OutputTable ")
        sb.Append(" VALUES(NewId(), @StuEnrollId,@OrigStatusId,@NewStatusId,@CampusId,@ModDate,@ModUser,@IsRevelsal,@DropReasonId,@DateOfChange); ")
        sb.Append(" SELECT TOP 1 StudentStatusChangeIdInserted FROM @OutputTable  ")
        Dim command As SqlCommand = New SqlCommand(sb.ToString(), sqlConn)
        command.CommandType = CommandType.Text
        Return command
    End Function

    'Public Function InsertStudentEnrollment(ByVal StuEnrollId As String, ByVal StudentID As String, ByVal CampusId As String, ByVal PrgVerId As String, ByVal User As String, ByVal SourcePrgVerId As String, Optional ByVal EnrollDate As String = "", Optional ByVal ExpStartDate As String = "", Optional ByVal ExpGradDate As String = "", Optional ByVal EnrollId As String = "", Optional ByVal SourceCampusId As String = "", Optional ByVal ShiftId As String = "", Optional ByVal ScheduleId As String = "", Optional ByVal TransferHours As Decimal = 0) As String
    '    Dim db As New DataAccess
    '    Dim strSQL As New StringBuilder
    '    Dim sbStatusCode As New StringBuilder
    '    Dim sbStatusCode1 As New StringBuilder
    '    Dim sb As New StringBuilder
    '    Dim strReturnMessage As String = ""
    '    Dim strStuEnrollmentsStatus, strStuEnrollmentsStatus1 As String

    '    Dim myAdvAppSettings As AdvAppSettings = New AdvAppSettings()

    '    db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

    '    Dim academicAdvisor, admissionsRep, BillingMethodId, EdLvlId As String
    '    Dim TuitionCategoryId, FAAdvisorId, AttendTypeId, DegCertSeekingId As String
    '    Dim DropReasonId As String = ""
    '    Dim originalEnrollment As String
    '    Dim BadgeNo As String = ""

    '    With sb
    '        .Append(" select distinct StuEnrollId,AcademicAdvisor, AdmissionsRep, BillingMethodId, EdLvlId, ShiftId, ")
    '        ''While Transferring the student from one program to another a new enrollment is created and the details are moved from old enrollment to new Enrollment.
    '        ''The drop reasonid should not be moved. So removing drop reason id from query
    '        '  .Append(" TuitionCategoryId,DropReasonId,FAAdvisorId,AttendTypeId,DegCertSeekingId from ")

    '        .Append(" TuitionCategoryId,FAAdvisorId,AttendTypeId,DegCertSeekingId,BadgeNumber from ")
    '        ''''''''''''''''''''''''''''''
    '        .Append(" arStuEnrollments where StudentId=? and PrgVerId=? ")
    '    End With
    '    db.AddParameter("@Studentid", StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@SourcePrgVerId", SourcePrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    'Execute the query
    '    Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
    '    While dr.Read()

    '        If Not (dr("AcademicAdvisor") Is DBNull.Value) Then academicAdvisor = CType(dr("AcademicAdvisor"), Guid).ToString Else academicAdvisor = ""
    '        If Not (dr("AdmissionsRep") Is DBNull.Value) Then admissionsRep = CType(dr("AdmissionsRep"), Guid).ToString Else admissionsRep = ""
    '        If Not (dr("BillingMethodId") Is DBNull.Value) Then BillingMethodId = CType(dr("BillingMethodId"), Guid).ToString Else BillingMethodId = ""
    '        If Not (dr("EdLvlId") Is DBNull.Value) Then EdLvlId = CType(dr("EdLvlId"), Guid).ToString Else EdLvlId = ""
    '        If Not (dr("TuitionCategoryId") Is DBNull.Value) Then TuitionCategoryId = CType(dr("TuitionCategoryId"), Guid).ToString Else TuitionCategoryId = ""
    '        If Not (dr("FAAdvisorId") Is DBNull.Value) Then FAAdvisorId = CType(dr("FAAdvisorId"), Guid).ToString Else FAAdvisorId = ""
    '        If Not (dr("AttendTypeId") Is DBNull.Value) Then AttendTypeId = CType(dr("AttendTypeId"), Guid).ToString Else AttendTypeId = ""
    '        If Not (dr("DegCertSeekingId") Is DBNull.Value) Then DegCertSeekingId = CType(dr("DegCertSeekingId"), Guid).ToString Else DegCertSeekingId = ""
    '        If Not (dr("StuEnrollId") Is DBNull.Value) Then originalEnrollment = CType(dr("StuEnrollId"), Guid).ToString Else originalEnrollment = ""
    '        ''To transfer Badge No from old enrollment to the new one.
    '        If Not (dr("BadgeNumber") Is DBNull.Value) Then BadgeNo = CType(dr("BadgeNumber"), String).ToString Else BadgeNo = ""
    '        ''''''''''''''''''''
    '    End While
    '    If Not dr.IsClosed Then dr.Close()
    '    db.ClearParameters()
    '    sb.Remove(0, sb.Length)

    '    With sbStatusCode
    '        .Append(" SELECT Distinct  A.StatusCodeID as StatusCodeID,A.StatusCodeDescrip as StatusCodeDescrip FROM ")
    '        .Append(" syStatusCodes A,sySysStatus B,syStatuses C,syStatusLevels D ")
    '        .Append(" where A.sysStatusID = B.sysStatusID And A.StatusID = C.StatusID And B.StatusLevelId = D.StatusLevelId ")
    '        .Append(" and C.Status = 'Active' and D.StatusLevelId = 2 and B.sysStatusId = 7 ")
    '        .Append(" ORDER BY A.StatusCodeDescrip  ")
    '    End With
    '    Dim drStatusCode As OleDbDataReader = db.RunParamSQLDataReader(sbStatusCode.ToString)
    '    While drStatusCode.Read()
    '        If Not (drStatusCode("StatusCodeId") Is DBNull.Value) Then strStuEnrollmentsStatus = CType(drStatusCode("StatusCodeId"), Guid).ToString Else strStuEnrollmentsStatus = ""
    '    End While
    '    If Not drStatusCode.IsClosed Then drStatusCode.Close()
    '    db.ClearParameters()
    '    sbStatusCode.Remove(0, sbStatusCode.Length)

    '    With sbStatusCode1
    '        .Append(" SELECT Distinct  A.StatusCodeID as StatusCodeID,A.StatusCodeDescrip as StatusCodeDescrip FROM ")
    '        .Append(" syStatusCodes A,sySysStatus B,syStatuses C,syStatusLevels D ")
    '        .Append(" where A.sysStatusID = B.sysStatusID And A.StatusID = C.StatusID And B.StatusLevelId = D.StatusLevelId ")
    '        .Append(" and C.Status = 'Active' and D.StatusLevelId = 2 and B.sysStatusId = 19 ")
    '        .Append(" ORDER BY A.StatusCodeDescrip  ")
    '    End With
    '    Dim drStatusCode1 As OleDbDataReader = db.RunParamSQLDataReader(sbStatusCode1.ToString)
    '    While drStatusCode1.Read()
    '        If Not (drStatusCode1("StatusCodeId") Is DBNull.Value) Then strStuEnrollmentsStatus1 = CType(drStatusCode1("StatusCodeId"), Guid).ToString Else strStuEnrollmentsStatus1 = ""
    '    End While
    '    If Not drStatusCode1.IsClosed Then drStatusCode1.Close()
    '    db.ClearParameters()
    '    sbStatusCode1.Remove(0, sbStatusCode1.Length)

    '    Dim intCheckRecExists As Integer = 0
    '    Dim sbCheckRecExists As New StringBuilder
    '    With sbCheckRecExists
    '        .Append(" Select Count(*) from arStuEnrollments enroll")
    '        .Append(" inner join  syStatusCodes A on enroll.StatusCodeID= A.StatusCodeID ")
    '        .Append(" inner join sySysStatus B on A.sysStatusID = B.sysStatusID ")
    '        .Append(" inner join syStatuses C on A.StatusID = C.StatusID ")
    '        .Append(" inner join syStatusLevels D on B.StatusLevelId = D.StatusLevelId ")
    '        .Append(" where C.Status = 'Active' and D.StatusLevelId = 2 and B.sysStatusId = 9 ")
    '        .Append(" and StudentId=? and PrgVerId=? ")
    '    End With
    '    db.AddParameter("@StudentId", StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    intCheckRecExists = db.RunParamSQLScalar(sbCheckRecExists.ToString)
    '    db.ClearParameters()
    '    sbCheckRecExists.Remove(0, sbCheckRecExists.Length)

    '    If Not intCheckRecExists >= 1 Then
    '        strReturnMessage = "Student Enrollment"
    '        With strSQL
    '            .Append("INSERT INTO arStuEnrollments ")
    '            ''Code Commented and Added by Kamalesh ahuja on 14 Jan 2010 to Mantis Issue Id 16935''''
    '            ''To transfer Badge No from old enrollment to the new one.
    '            '.Append(" (StuEnrollId, StudentId, PrgVerId, AcademicAdvisor, AdmissionsRep, BillingMethodId, CampusId, EdLvlId, ShiftId, StatusCodeId, EnrollDate, ExpGradDate, ExpStartDate, LDA, MidPtDate, StartDate,TransferDate,DateDetermined,TuitionCategoryId,DropReasonId,FAAdvisorId,AttendTypeId,DegCertSeekingId,ModUser,ModDate,EnrollmentId)  ")
    '            '.Append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
    '            .Append(" (StuEnrollId, StudentId, PrgVerId, AcademicAdvisor, AdmissionsRep, BillingMethodId, CampusId, EdLvlId, ShiftId, StatusCodeId, EnrollDate, ExpGradDate, ExpStartDate, LDA, MidPtDate, StartDate,TransferDate,DateDetermined,TuitionCategoryId,DropReasonId,FAAdvisorId,AttendTypeId,DegCertSeekingId,ModUser,ModDate,EnrollmentId,BadgeNumber,TransferHours)  ")
    '            .Append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
    '            '''''''''''
    '        End With
    '        db.AddParameter("@StuEnrollID", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@StudentID", StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@PrgVerID", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        If Not academicAdvisor = "" Then
    '            db.AddParameter("@EmpId", academicAdvisor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@EmpID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If
    '        If Not admissionsRep = "" Then
    '            db.AddParameter("@AdmRep", admissionsRep, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@AdmRep", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If
    '        If Not BillingMethodId = "" Then
    '            db.AddParameter("@Billingmthd", BillingMethodId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Billingmthd", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If
    '        If CampusId = "" Then
    '            db.AddParameter("@campusid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@campusid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If
    '        If Not EdLvlId = "" Then
    '            db.AddParameter("@EdLvlid", EdLvlId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@EdLvlId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If
    '        If Not ShiftId = "" Then
    '            db.AddParameter("@shiftid", ShiftId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@shiftid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If
    '        If strStuEnrollmentsStatus = "" Then
    '            db.AddParameter("@statuscodeid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@statuscodeid", strStuEnrollmentsStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If
    '        If Not EnrollDate = "" Then
    '            db.AddParameter("@enrolldate", EnrollDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@enrolldate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If
    '        If Not ExpGradDate = "" Then
    '            db.AddParameter("@expgraddate", ExpGradDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@expgraddate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If
    '        If Not ExpStartDate = "" Then
    '            db.AddParameter("@expstartdate", ExpStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@expstartdate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If
    '        db.AddParameter("@lda", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@midptdate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@startdate", ExpStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@transferdate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@datedetermined", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        If Not TuitionCategoryId = "" Then
    '            db.AddParameter("@TuitionCategory", TuitionCategoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@TuitionCategory", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If
    '        If Not DropReasonId = "" Then
    '            db.AddParameter("@DropReason", DropReasonId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@DropReason", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If
    '        If Not FAAdvisorId = "" Then
    '            db.AddParameter("@faadvisorid", FAAdvisorId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@faadvisorid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If
    '        If Not AttendTypeId = "" Then
    '            db.AddParameter("@AttendTypeId", AttendTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@AttendTypeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If
    '        If Not DegCertSeekingId = "" Then
    '            db.AddParameter("@DegCertSeekingId", DegCertSeekingId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@DegCertSeekingId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If
    '        ''ModUser
    '        db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        ''ModDate
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        'EnrollId
    '        db.AddParameter("@EnrollId", EnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        ''Code Added by Kamalesh ahuja on 14 Jan 2010 to Mantis Issue Id 16935''''
    '        ''To transfer Badge No from old enrollment to the new one.

    '        'Badge Number
    '        db.AddParameter("@BadgeNo", BadgeNo, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        ''''''''''''''''''''
    '        ''TransferHours added by Saraswathi lakshmanan on June 7 2010
    '        db.AddParameter("@TransferHours", TransferHours, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '        'Try
    '        db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
    '        db.ClearParameters()

    '        Dim sbUpdateEnrollments As New StringBuilder
    '        If Not ScheduleId = "" Then
    '            With sbUpdateEnrollments
    '                .Append("Update arStuEnrollments set ScheduleId=? where StuEnrollId=? ")
    '            End With
    '            db.AddParameter("@ScheduleId", ScheduleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            Try
    '                db.RunParamSQLExecuteNoneQuery(sbUpdateEnrollments.ToString)
    '            Catch ex As Exception
    '            Finally
    '                db.ClearParameters()
    '                sbUpdateEnrollments.Remove(0, sbUpdateEnrollments.Length)
    '            End Try
    '        End If


    '        With sbUpdateEnrollments
    '            'Code Added and commented by Kamalesh ahuja on 14 Jan 2010 to Mantis Issue Id 16935''''
    '            ''To transfer Badge No from old enrollment to the new one.
    '            ''.Append(" Update arStuEnrollments set StatusCodeId=?,TransferDate=?,DateDetermined=? where StudentId=? and PrgVerId=? ")
    '            .Append(" Update arStuEnrollments set BadgeNumber = Null, StatusCodeId=?,TransferDate=?,DateDetermined=? where StudentId=? and PrgVerId=? ")
    '        End With
    '        db.AddParameter("@StatusCodeId", strStuEnrollmentsStatus1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@TransferDate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@DateDetermined", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@StudentId", StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@PrgVerId", SourcePrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.RunParamSQLExecuteNoneQuery(sbUpdateEnrollments.ToString)
    '        db.ClearParameters()


    '        Dim sbTrackTransfer As New StringBuilder
    '        With sbTrackTransfer
    '            .Append(" insert into arTrackTransfer(TrackTransferId,StuEnrollId,SourcePrgVerId,TargetPrgVerId,SourceCampusId,TargetCampusId,ModDate,ModUser) ")
    '            .Append(" values(?,?,?,?,?,?,?,?) ")
    '        End With
    '        db.AddParameter("@TrackTransferId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@StuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@SourcePrgVerId", SourcePrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@TargetPrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@SourceCampusId", SourceCampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@TargetCampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.RunParamSQLExecuteNoneQuery(sbTrackTransfer.ToString)
    '        db.ClearParameters()
    '        sbTrackTransfer.Remove(0, sbTrackTransfer.Length)

    '        Dim sbLeadGroups As New StringBuilder
    '        With sbLeadGroups
    '            .Append(" INSERT INTO adLeadByLeadGroups(LeadGrpLeadId, StuEnrollId,LeadGrpId, ModDate, ModUser,LeadId) ")
    '            .Append(" Select newId(),?,LeadGrpId,?,?,? from adLeadByLeadGroups where StuEnrollId=? ")
    '        End With
    '        ''Added by Saraswathi lakshmanan on oct 07 2009
    '        ''Creating a new Guuid from here was giving error with duplicate primary key violation
    '        ''So, Changed the insert statement to get the newid from the Sql
    '        ''For mantis case 17369

    '        '   add parameters
    '        ' db.AddParameter("@LeadGrpLeadId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@LeadId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@StuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        'If Not Trim(LeadId) = Guid.Empty.ToString Then
    '        '    db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        'Else
    '        'End If

    '        '   execute query
    '        db.RunParamSQLExecuteNoneQuery(sbLeadGroups.ToString)
    '        db.ClearParameters()
    '        sbLeadGroups.Remove(0, sbLeadGroups.Length)


    '        'Copy the Transactions for new enrollment


    '        db.CloseConnection()
    '    End If
    '    Return strReturnMessage
    'End Function

    Public Function TransferGrades(ByVal StuEnrollId As String, ByVal selectedCourseId() As String, ByVal selectedGrade() As String, ByVal selectedTerm() As String, ByVal user As String, ByVal selectedResult() As String, ByVal selectedIsCourseCompleted() As Integer, Optional ByVal StudentId As String = "", Optional ByVal SourcePrgVerId As String = "", Optional ByVal TargetCampusId As String = "", Optional ByVal ScheduleID As String = "", Optional ByVal Campusid As String = "", Optional ByVal RemoveReminingScheduledClasses As Boolean = False) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim intCourseId As Integer
        Dim x As Integer
        Dim originalEnrollment As String
        Dim sbTargetProgramVersion As New StringBuilder
        Dim strTargetPrgVerId As String
        Dim sbResult As New StringBuilder
        Dim decScore As Decimal = 0.0
        Dim boolIsCommonCourse As Boolean = False
        Dim strReturnMessage As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            If Not selectedCourseId Is Nothing Then
                intCourseId = selectedCourseId.Length
            End If
            With sb
                .Append(" select distinct StuEnrollId from ")
                .Append(" arStuEnrollments where StudentId=? and PrgVerId=? ")
            End With
            db.AddParameter("@Studentid", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@SourcePrgVerId", SourcePrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute the query
            Dim dr As OleDbDataReader
            Try
                dr = db.RunParamSQLDataReader(sb.ToString)
                While dr.Read()
                    If Not dr("StuEnrollId") Is DBNull.Value Then originalEnrollment = CType(dr("StuEnrollId"), Guid).ToString Else originalEnrollment = ""
                End While
            Catch ex As Exception
                originalEnrollment = ""
            Finally
                dr.Close()
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            End Try

            With sbTargetProgramVersion
                .Append("select Distinct PrgVerId from arStuEnrollments where StuEnrollId=? ")
            End With
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                strTargetPrgVerId = CType(db.RunParamSQLScalar(sbTargetProgramVersion.ToString), Guid).ToString
            Catch ex As Exception
                strTargetPrgVerId = ""
            Finally
                db.ClearParameters()
                sbTargetProgramVersion.Remove(0, sbTargetProgramVersion.Length)
                db.CloseConnection()
            End Try
            If Not selectedCourseId Is Nothing Then
                While x < intCourseId

                    boolIsCommonCourse = IsCommonCourse(strTargetPrgVerId, DirectCast(selectedCourseId.GetValue(x), String))

                    If boolIsCommonCourse = True Then
                        'Get the student names that are registered for this classsection
                        strReturnMessage = "Grade(s)"
                        Dim GrdPostingObj As New FinalGradeInfo
                        GrdPostingObj.StuEnrollId = StuEnrollId
                        GrdPostingObj.ReqId = DirectCast(selectedCourseId.GetValue(x), String)
                        If MyAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                            GrdPostingObj.Score = If(DirectCast(selectedGrade.GetValue(x), String) = "", 0, CDec(DirectCast(selectedGrade.GetValue(x), String)))
                            GrdPostingObj.Grade = DBNull.Value.ToString
                        Else
                            GrdPostingObj.Score = decScore
                            GrdPostingObj.Grade = DirectCast(selectedGrade.GetValue(x), String)
                        End If
                        GrdPostingObj.TermId = DirectCast(selectedTerm.GetValue(x), String)
                        GrdPostingObj.ModUser = user
                        GrdPostingObj.ModDate = DateTime.Now
                        Dim tgDB As New TransferGradesDB

                        'Troy: We are bringing all the courses for the student both graded and ungraded. However, we only want to added the graded ones
                        'to arTransferGrades. 
                        'If ((MyAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "letter" And GrdPostingObj.Grade <> "") Or (MyAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" And GrdPostingObj.Score <> 0)) Then
                        If DirectCast(selectedIsCourseCompleted.GetValue(x), Integer) = 1 Then
                            Try
                                tgDB.UpdateTransferGrade(GrdPostingObj, user, Campusid)
                            Catch ex As Exception
                                Throw ex
                            Finally

                            End Try
                        Else
                            'Troy: We are dealing with a scheduled class and need to update the enrollment on it to the new enrollment
                            With sbResult
                                .Append("UPDATE rs ")
                                .Append("SET rs.StuEnrollId = ? ")
                                .Append("FROM dbo.arResults rs ")
                                .Append("WHERE rs.ResultId = ? ")
                            End With
                            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                            db.AddParameter("@ResultId", DirectCast(selectedResult.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


                            Try
                                db.RunParamSQLExecuteNoneQuery(sbResult.ToString)
                            Catch ex As Exception
                                Throw ex
                            Finally
                                db.ClearParameters()
                                sbResult.Remove(0, sbResult.Length)
                            End Try
                        End If

                        'Troy: Add class section attendance for the new enrollment for common courses
                        With sbResult
                            .Append("INSERT INTO dbo.atClsSectAttendance (ClsSectAttId,StudentId,ClsSectionId,ClsSectMeetingId,MeetDate,Actual,Tardy,Excused,Comments,StuEnrollId,Scheduled,ModDate,ModUser) ")
                            .Append("SELECT  NEWID(),StudentId,csa2.ClsSectionId,ClsSectMeetingId,MeetDate,Actual,Tardy,Excused,Comments,?,Scheduled,GETDATE(),? ")
                            .Append("FROM dbo.atClsSectAttendance csa2 ")
                            .Append("INNER JOIN dbo.arClassSections cs2  ON cs2.ClsSectionId = csa2.ClsSectionId ")
                            .Append("WHERE csa2.StuEnrollId = ? ")
                            .Append("AND cs2.ReqId = ? ")
                            .Append("AND NOT EXISTS ")
                            .Append("(SELECT * ")
                            .Append(" FROM atClsSectAttendance csa ")
                            .Append(" INNER JOIN arClassSections cs ON cs.ClsSectionId = csa.ClsSectionId")
                            .Append(" WHERE csa.StuEnrollId = ? ")
                            .Append(" AND cs.ReqId = ?) ")
                        End With
                        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@OrigStuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ReqId", DirectCast(selectedCourseId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ReqId", DirectCast(selectedCourseId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                        Try
                            db.RunParamSQLExecuteNoneQuery(sbResult.ToString)
                            If Not strReturnMessage = "" Then
                                strReturnMessage &= ","
                            End If
                            strReturnMessage &= "class section attendance"
                        Catch ex As Exception
                            Throw ex
                        Finally
                            db.ClearParameters()
                            sbResult.Remove(0, sbResult.Length)
                        End Try

                    End If



                    With sbResult
                        .Append(" Update arResults set IsTransfered=1 Where ResultId =?")
                    End With
                    db.AddParameter("@ResultId", DirectCast(selectedResult.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    Try
                        db.RunParamSQLExecuteNoneQuery(sbResult.ToString)
                    Catch ex As Exception
                        Throw ex
                    Finally
                        db.ClearParameters()
                        sbResult.Remove(0, sbResult.Length)
                    End Try

                    With sbResult
                        .Append(" Update arTransferGrades set IsTransferred=1 Where StuEnrollId =? And ReqId =?")
                    End With
                    db.AddParameter("@StuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@ReqId", DirectCast(selectedCourseId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    Try
                        db.RunParamSQLExecuteNoneQuery(sbResult.ToString)
                    Catch ex As Exception
                        Throw ex
                    Finally
                        db.ClearParameters()
                        sbResult.Remove(0, sbResult.Length)
                    End Try


                    'Copy the student ledger information to new enrollment
                    'With sbResult
                    '    .Append(" Insert into saTransactions(StuEnrollId,TermId,CampusId,TransDate,TransCodeId,TransReference, ")
                    '    .Append(" AcademicYearId,TransDescrip,TransAmount,TransTypeId,IsPosted,CreateDate,BatchPaymentId, ")
                    '    .Append(" ViewOrder, IsAutomatic, ModUser, ModDate, Voided, FeeLevelId, FeeId) ")
                    '    .Append(" Select ?,?,?,TransDate,TransCodeId,TransReference,AcademicYearId,TransDescrip, ")
                    '    .Append(" TransAmount,TransTypeId,IsPosted,CreateDate,BatchPaymentId,ViewOrder, IsAutomatic, ")
                    '    .Append(" ?, ?, Voided, FeeLevelId, FeeId from saTransactions ")
                    '    .Append(" where StuEnrollId=? ")
                    'End With
                    'db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    'db.AddParameter("@TermId", DirectCast(selectedTerm.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    'db.AddParameter("@TargetCampusId", TargetCampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    'db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    'db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    'db.AddParameter("@StuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    'update saTransactions table
                    With sbResult
                        .Append("update saTransactions set StuEnrollId=? where StuEnrollId=? ")
                    End With
                    db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@StuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    Try
                        db.RunParamSQLExecuteNoneQuery(sbResult.ToString)
                        If Not strReturnMessage = "" Then
                            strReturnMessage &= ","
                        End If
                        strReturnMessage &= "ledger"
                    Catch ex As Exception
                        Throw ex
                    Finally
                        db.ClearParameters()
                        sbResult.Remove(0, sbResult.Length)
                    End Try

                    'update faStudentAwards
                    With sbResult
                        .Append("update faStudentAwards  set StuEnrollId=? where StuEnrollId=? ")
                    End With
                    db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@StuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    Try
                        db.RunParamSQLExecuteNoneQuery(sbResult.ToString)
                    Catch ex As Exception
                        Throw ex
                    Finally
                        db.ClearParameters()
                        sbResult.Remove(0, sbResult.Length)
                    End Try

                    'update faStudentPaymentPlans
                    With sbResult
                        .Append("update faStudentPaymentPlans  set StuEnrollId=? where StuEnrollId=? ")
                    End With
                    db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@StuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    Try
                        db.RunParamSQLExecuteNoneQuery(sbResult.ToString)
                    Catch ex As Exception
                        Throw ex
                    Finally
                        db.ClearParameters()
                        sbResult.Remove(0, sbResult.Length)
                    End Try

                    'Move the conversion attendance data for new enrollment
                    If MyAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                        With sbResult
                            .Append("   insert into atConversionAttendance(AttendanceId,StuEnrollId,MeetDate,Actual,Tardy, ")
                            .Append("   Excused,Schedule,PostByException)  ")
                            .Append("   Select ?,?,MeetDate,Actual,Tardy,Excused,Schedule,PostByException from atConversionAttendance ")
                            .Append("   where StuEnrollId=? ")
                            .Append("   And Not EXISTS ")
                            .Append("   (SELECT * FROM dbo.atConversionAttendance WHERE StuEnrollId=? )")
                        End With
                        db.AddParameter("@AttendanceId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@StuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Try
                            db.RunParamSQLExecuteNoneQuery(sbResult.ToString)
                            If Not strReturnMessage = "" Then
                                strReturnMessage &= ","
                            End If
                            strReturnMessage &= "attendance"
                        Catch ex As Exception
                            Throw ex
                        Finally
                            db.ClearParameters()
                            sbResult.Remove(0, sbResult.Length)
                        End Try
                    End If

                    'Move the (new student) attendance data for new enrollment
                    If Not ScheduleID = "" Then
                        With sbResult
                            .Append("   insert into arStudentClockAttendance(StuEnrollId,ScheduleId,RecordDate, ")
                            .Append("   SchedHours,ActualHours,ModUser,ModDate, ")
                            .Append("   isTardy,PostByException) ")
                            .Append("   Select ?,?,RecordDate,SchedHours,ActualHours,?,?,isTardy,PostByException from arStudentClockAttendance ")
                            .Append("   where StuEnrollId=?  And Not EXISTS ")
                            .Append(" (SELECT * FROM dbo.arStudentClockAttendance WHERE StuEnrollId=? )")
                        End With
                        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ScheduleId", ScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                        db.AddParameter("@StuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Try
                            db.RunParamSQLExecuteNoneQuery(sbResult.ToString)
                            If Not InStr(strReturnMessage, "attendance") >= 1 Then
                                If Not strReturnMessage = "" Then
                                    strReturnMessage &= ","
                                End If
                                strReturnMessage &= "attendance"
                            End If
                        Catch ex As Exception
                            Throw ex
                        Finally
                            db.ClearParameters()
                            sbResult.Remove(0, sbResult.Length)
                        End Try
                    End If
                    'Move the score of grade book components
                    With sbResult
                        .Append(" insert into arGrdBkResults(ClsSectionId,InstrGrdBkWgtDetailId,Score, ")
                        .Append(" Comments,StuEnrollId,ModUser,ModDate,ResNum,PostDate) ")
                        .Append("   Select t1.ClsSectionId,InstrGrdBkWgtDetailId,Score,Comments,?,?,?, ")
                        .Append("   ResNum,PostDate from arGrdBkResults t1,arClassSections t2 ")
                        .Append("   where t1.ClsSectionId = t2.ClsSectionId And t2.ReqId=? ")
                        .Append("   And StuEnrollId=? ")
                    End With
                    db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@ReqId", DirectCast(selectedCourseId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@StuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    Try
                        db.RunParamSQLExecuteNoneQuery(sbResult.ToString)
                        If Not strReturnMessage = "" Then
                            strReturnMessage &= ","
                        End If
                        strReturnMessage &= "grade components"
                    Catch ex As Exception
                        Throw ex
                    Finally
                        db.ClearParameters()
                        sbResult.Remove(0, sbResult.Length)
                    End Try
                    If MyAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                        With sbResult
                            .Append(" insert into arGrdBkConversionResults( ")
                            .Append(" ReqId,GrdComponentTypeId,Score,Comments,StuEnrollId, ")
                            .Append(" ModUser, ModDate, ResNum, PostDate, PostUser, MinResult, TermId, Required, MustPass) ")
                            .Append(" Select ReqId,GrdComponentTypeId,Score,Comments,?, ")
                            .Append(" ?,?,ResNum, PostDate, PostUser, MinResult, TermId, Required, MustPass ")
                            .Append(" from arGrdBkConversionResults where StuEnrollId=? And ReqId=? ")
                        End With
                        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                        db.AddParameter("@StuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ReqId", DirectCast(selectedCourseId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Try
                            db.RunParamSQLExecuteNoneQuery(sbResult.ToString)
                            If Not strReturnMessage = "" Then
                                strReturnMessage &= ","
                            End If
                            strReturnMessage &= "grade components"
                        Catch ex As Exception
                            Throw ex
                        Finally
                            db.ClearParameters()
                            sbResult.Remove(0, sbResult.Length)
                        End Try
                    End If

                    'If SingletonAppSettings.AppSettings("GradesFormat",CampusId).ToString.ToLower = "numeric" Then
                    If Not ScheduleID = "" Then
                        With sbResult
                            .Append(" insert into arStudentSchedules( ")
                            .Append(" StuEnrollId,ScheduleId,StartDate,EndDate, ")
                            .Append(" Active,ModUser,ModDate,Source) ")
                            .Append(" Select ?,?,StartDate,EndDate, ")
                            .Append(" Active,?,?,Source ")
                            .Append(" from arStudentSchedules where StuEnrollId=? ")
                            .Append(" And Not EXISTS ")
                            .Append(" (SELECT * FROM dbo.arStudentSchedules WHERE StuEnrollId=? )")
                        End With
                        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ScheduleId", ScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                        db.AddParameter("@StuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Try
                            db.RunParamSQLExecuteNoneQuery(sbResult.ToString)
                            If Not strReturnMessage = "" Then
                                strReturnMessage &= ","
                            End If
                            strReturnMessage &= "schedule"
                        Catch ex As Exception
                            Throw ex
                        Finally
                            db.ClearParameters()
                            sbResult.Remove(0, sbResult.Length)
                        End Try
                    End If




                    x += 1
                End While
            Else
                'If there are no Graded Courses
                'update saTransactions table
                With sbResult
                    .Append("update saTransactions set StuEnrollId=? where StuEnrollId=? ")
                End With
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@StuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                Try
                    db.RunParamSQLExecuteNoneQuery(sbResult.ToString)
                    If Not strReturnMessage = "" Then
                        strReturnMessage &= ","
                    End If
                    strReturnMessage &= "ledger"
                Catch ex As Exception
                    Throw ex
                Finally
                    db.ClearParameters()
                    sbResult.Remove(0, sbResult.Length)
                End Try

                'update faStudentAwards
                With sbResult
                    .Append("update faStudentAwards  set StuEnrollId=? where StuEnrollId=? ")
                End With
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@StuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                Try
                    db.RunParamSQLExecuteNoneQuery(sbResult.ToString)
                Catch ex As Exception
                    Throw ex
                Finally
                    db.ClearParameters()
                    sbResult.Remove(0, sbResult.Length)
                End Try

                'update faStudentPaymentPlans
                With sbResult
                    .Append("update faStudentPaymentPlans  set StuEnrollId=? where StuEnrollId=? ")
                End With
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@StuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                Try
                    db.RunParamSQLExecuteNoneQuery(sbResult.ToString)
                Catch ex As Exception
                    Throw ex
                Finally
                    db.ClearParameters()
                    sbResult.Remove(0, sbResult.Length)
                End Try


                'Move the conversion attendance data for new enrollment
                If MyAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                    If Not ScheduleID = "" Then
                        With sbResult
                            .Append("   insert into atConversionAttendance(AttendanceId,StuEnrollId,MeetDate,Actual,Tardy, ")
                            .Append("   Excused,Schedule,PostByException)  ")
                            .Append("   Select ?,?,MeetDate,Actual,Tardy,Excused,Schedule,PostByException from atConversionAttendance ")
                            .Append("   where StuEnrollId=? ")
                        End With
                        db.AddParameter("@AttendanceId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@StuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Try
                            db.RunParamSQLExecuteNoneQuery(sbResult.ToString)
                            If Not strReturnMessage = "" Then
                                strReturnMessage &= ","
                            End If
                            strReturnMessage &= "attendance"
                        Catch ex As Exception
                            Throw ex
                        Finally
                            db.ClearParameters()
                            sbResult.Remove(0, sbResult.Length)
                        End Try
                    End If
                End If

                'Move the (new student) attendance data for new enrollment
                If Not ScheduleID = "" Then
                    With sbResult
                        .Append("   insert into arStudentClockAttendance(StuEnrollId,ScheduleId,RecordDate, ")
                        .Append("   SchedHours,ActualHours,ModUser,ModDate, ")
                        .Append("   isTardy,PostByException) ")
                        .Append("   Select ?,?,RecordDate,SchedHours,ActualHours,?,?,isTardy,PostByException from arStudentClockAttendance ")
                        .Append("   where StuEnrollId=? ")
                    End With
                    db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@ScheduleId", ScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@StuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    Try
                        db.RunParamSQLExecuteNoneQuery(sbResult.ToString)
                        If Not InStr(strReturnMessage, "attendance") >= 1 Then
                            If Not strReturnMessage = "" Then
                                strReturnMessage &= ","
                            End If
                            strReturnMessage &= "attendance"
                        End If
                    Catch ex As Exception
                        Throw ex
                    Finally
                        db.ClearParameters()
                        sbResult.Remove(0, sbResult.Length)
                    End Try
                End If

                'Move the score of grade book components
                ' if there are no common courses no need to transfer
                'With sbResult
                '    .Append(" insert into arGrdBkResults(ClsSectionId,InstrGrdBkWgtDetailId,Score, ")
                '    .Append(" Comments,StuEnrollId,ModUser,ModDate,ResNum,PostDate) ")
                '    .Append("   Select ClsSectionId,InstrGrdBkWgtDetailId,Score,Comments,?,?,?, ")
                '    .Append("   ResNum,PostDate from arGrdBkResults where StuEnrollId=? ")
                'End With
                'db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                'db.AddParameter("@StuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'Try
                '    db.RunParamSQLExecuteNoneQuery(sbResult.ToString)
                '    If Not strReturnMessage = "" Then
                '        strReturnMessage &= ","
                '    End If
                '    strReturnMessage &= "grade components"
                'Catch ex As System.Exception
                'Finally
                '    db.ClearParameters()
                '    sbResult.Remove(0, sbResult.Length)
                'End Try


                'If SingletonAppSettings.AppSettings("GradesFormat",CampusId).ToString.ToLower = "numeric" Then
                If Not ScheduleID = "" Then
                    With sbResult
                        .Append(" insert into arStudentSchedules( ")
                        .Append(" StuEnrollId,ScheduleId,StartDate,EndDate, ")
                        .Append(" Active,ModUser,ModDate,Source) ")
                        .Append(" Select ?,?,StartDate,EndDate, ")
                        .Append(" Active,?,?,Source ")
                        .Append(" from arStudentSchedules where StuEnrollId=? ")
                    End With
                    db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@ScheduleId", ScheduleID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@StuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    Try
                        db.RunParamSQLExecuteNoneQuery(sbResult.ToString)
                        If Not strReturnMessage = "" Then
                            strReturnMessage &= ","
                        End If
                        strReturnMessage &= "schedules"
                    Catch ex As Exception
                        Throw ex
                    Finally
                        db.ClearParameters()
                        sbResult.Remove(0, sbResult.Length)
                    End Try
                End If
                ' End If
            End If

            'Remove remaining scheduled classes if specified in parameter
            If RemoveReminingScheduledClasses Then
                With sbResult
                    .Append("DELETE FROM arResults ")
                    .Append("WHERE StuEnrollId = ? ")
                    .Append("AND IsCourseCompleted = 0 ")
                    'If MyAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "letter" Then
                    '    .Append("AND GrdSysDetailId IS NULL ")
                    'Else
                    '    .Append("AND Score = 0 ")
                    'End If
                End With
                db.AddParameter("@StuEnrollId", originalEnrollment, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Try
                    db.RunParamSQLExecuteNoneQuery(sbResult.ToString)
                    If Not strReturnMessage = "" Then
                        strReturnMessage &= ","
                    End If
                    strReturnMessage &= "schedules"
                Catch ex As Exception
                    Throw ex
                Finally
                    db.ClearParameters()
                    sbResult.Remove(0, sbResult.Length)
                End Try

            End If
        Catch ex As Exception

        Finally
        End Try
        Return strReturnMessage
    End Function
    Public Function GetTransferResult(ByVal StuEnrollId As String, ByVal StudentName As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append(" select distinct SourcePrgVerId, ")
            .Append(" (select Top 1 PrgVerDescrip from arPrgVersions where PrgVerId=SourcePrgVerId) as SourcePrgVersion, ")
            .Append(" TargetPrgVerId, ")
            .Append(" (select Top 1 PrgVerDescrip from arPrgVersions where PrgVerId=TargetPrgVerId) as TargetPrgVersion, ")
            .Append(" SourceCampusId,TargetCampusId, ")
            .Append(" (select Top 1 CampDescrip from syCampuses where CampusId=SourceCampusId) as SourceCampus, ")
            .Append(" (select Top 1 CampDescrip from syCampuses where CampusId=TargetCampusId) as TargetCampus, ")
            .Append(" ModDate,ModUser, ")
            .Append(" (select Top 1 FullName from syUsers where UserName=ModUser) as Users ")
            .Append(" from ")
            .Append(" arTrackTransfer where StuEnrollId=?")
        End With
        db.AddParameter("@StuEnrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim SourcePrgVersion, TargetPrgVersion, SourceCampus, TargetCampus, Users, TransferredDate As String
        Dim intRecords As Integer = 0
        While dr.Read()
            If Not (dr("SourcePrgVersion") Is DBNull.Value) Then SourcePrgVersion = CType(dr("SourcePrgVersion"), String).ToString Else SourcePrgVersion = ""
            If Not (dr("TargetPrgVersion") Is DBNull.Value) Then TargetPrgVersion = CType(dr("TargetPrgVersion"), String).ToString Else TargetPrgVersion = ""
            If Not (dr("SourceCampus") Is DBNull.Value) Then SourceCampus = CType(dr("SourceCampus"), String).ToString Else SourceCampus = ""
            If Not (dr("TargetCampus") Is DBNull.Value) Then TargetCampus = CType(dr("TargetCampus"), String).ToString Else TargetCampus = ""
            If Not (dr("Users") Is DBNull.Value) Then Users = CType(dr("Users"), String).ToString Else Users = ""
            If Not (dr("ModDate") Is DBNull.Value) Then TransferredDate = CType(dr("ModDate"), DateTime).ToShortDateString Else TransferredDate = ""
            intRecords += 1
        End While
        Dim strMessage As String
        If intRecords >= 1 Then
            strMessage = StudentName & " has been transfered from " & SourcePrgVersion & " (in " & SourceCampus & ") to " & TargetPrgVersion & " (in " & TargetCampus & ")" & " by " & Users & " on " & TransferredDate
        Else
            strMessage = ""
        End If
        If Not dr.IsClosed Then dr.Close()
        db.ClearParameters()
        sb.Remove(0, sb.Length)
        Return strMessage
    End Function
    Public Function GetAllCampGroupsByCampusIdAndUserId(ByVal campusId As String, ByVal UserId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid

        '   build the sql query
        With sb
            .Append("  SELECT Distinct t2.CampGrpId, t2.CampGrpDescrip FROM syCmpGrpCmps t1,syCampGrps t2,syUsersRolesCampGrps t3 ")
            .Append("  WHERE t1.CampGrpId=t2.CampGrpId And t1.CampGrpId=t3.CampGrpId And t2.CampGrpId=t3.CampGrpId And ")
            .Append("  t1.CampusId=? And t2.StatusId=? And t3.UserId=? ")
        End With

        '   add CampusId parameter
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StatusId", strActiveGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@UserId", UserId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetStudentEnrollmentByProgramVersion(ByVal studentId As String, ByVal prgVerId As String) As String
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim strEnrollId As String = ""
        With sb
            .Append(" Select Distinct StuEnrollId from arStuEnrollments enroll ")
            .Append(" inner join  syStatusCodes A on enroll.StatusCodeID= A.StatusCodeID ")
            .Append(" inner join sySysStatus B on A.sysStatusID = B.sysStatusID ")
            .Append(" inner join syStatuses C on A.StatusID = C.StatusID ")
            .Append(" inner join syStatusLevels D on B.StatusLevelId = D.StatusLevelId ")
            .Append(" where C.Status = 'Active' and D.StatusLevelId = 2 and B.sysStatusId = 9 ")
            .Append(" and StudentId=? and PrgVerId=? ")
        End With
        db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            If Not (dr("StuEnrollId") Is DBNull.Value) Then strEnrollId = CType(dr("StuEnrollId"), Guid).ToString Else strEnrollId = ""
        End While
        If Not dr.IsClosed Then dr.Close()
        db.ClearParameters()
        Return strEnrollId
    End Function

    Public Function GetProgramByProgramVersion(ByVal PrgVerId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim strEnrollId As String = ""
        With sb
            .Append(" select Distinct t1.ProgId,t1.ProgDescrip from arPrograms t1,arPrgVersions t2 ")
            .Append(" where t1.ProgId = t2.ProgId and t2.PrgVerId=? ")
        End With
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetProgramByStuEnrollId(ByVal StuEnrollid As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim strEnrollId As String = ""
        With sb
            .Append(" select Distinct t1.ProgId,t1.ProgDescrip,t2.PrgVerid,t2.PrgVerDescrip from arPrograms t1,arPrgVersions t2,arStuEnrollments t3 ")
            .Append(" where t1.ProgId = t2.ProgId and t2.PrgVerId=t3.PrgVerid and t3.StuEnrollid= ? ")
        End With
        db.AddParameter("@PrgVerId", StuEnrollid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function GetDatesByEnrollment(ByVal StuEnrollId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim strEnrollId As String = ""
        With sb
            .Append(" select Distinct Top 1 EnrollDate,ExpStartDate,ExpGradDate,ShiftId from arStuEnrollments where StuEnrollId=? ")
        End With
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetTransferResultDS(ByVal StuEnrollId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append(" select distinct t1.SourcePrgVerId, ")
            .Append(" (select Top 1 PrgVerDescrip from arPrgVersions where PrgVerId=t1.SourcePrgVerId) as SourcePrgVersion, ")
            .Append(" t1.TargetPrgVerId, ")
            .Append(" (select Top 1 PrgVerDescrip from arPrgVersions where PrgVerId=t1.TargetPrgVerId) as TargetPrgVersion, ")
            .Append(" t1.SourceCampusId,t1.TargetCampusId, ")
            .Append(" (select Top 1 CampDescrip from syCampuses where CampusId=t1.SourceCampusId) as SourceCampus, ")
            .Append(" (select Top 1 CampDescrip from syCampuses where CampusId=t1.TargetCampusId) as TargetCampus, ")
            .Append(" t1.ModDate,t1.ModUser, ")
            .Append(" (select Top 1 FullName from syUsers where UserName=t1.ModUser) as Users ")
            .Append(" from ")
            .Append(" arTrackTransfer t1 where t1.StuEnrollId=?")
        End With
        db.AddParameter("@StuEnrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function IsCommonCourse(ByVal PrgVerId As String, ByVal adReqId As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim intRecordCount As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append(" Select Count(*) from arReqs where ReqId=? and ReqId in ")
            .Append("   (select Distinct ReqId from arProgVerDef where PrgVerId=? ")
            .Append("   Union Select Distinct ReqId from arReqGrpDef where GrpId in (select Distinct ReqId from arProgVerDef where PrgVerId=?) ")
            .Append("   ) ")
        End With
        db.AddParameter("@ReqId", adReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            intRecordCount = db.RunParamSQLScalar(sb.ToString)
            If intRecordCount >= 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            db.CloseConnection()
        End Try
    End Function


    ''Added by Saraswathi lakshmanan on June 2010
    Public Function IsProgramClockHourType(ByVal ProgID As String) As Boolean

        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@ProgID", New Guid(ProgID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Try
            ds = db.RunParamSQLDataSet_SP("dbo.USP_IsProgramClockHourType")
        Catch ex As Exception
            Return False
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False

            End If
        Else
            Return False
        End If
    End Function

    Private Sub AddWithValueSafe(ByVal parameters As SqlParameterCollection, ByVal parameterName As String, ByVal value As Object)

        parameters.AddWithValue(parameterName, If(value Is Nothing, DBNull.Value, value))
        Return
    End Sub
End Class
