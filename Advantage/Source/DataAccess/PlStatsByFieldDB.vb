Imports FAME.Advantage.Common

Public Class PlStatsByFieldDB

    Public Function GetStatsByField(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= "AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= " ORDER BY " & paramInfo.OrderBy
        End If

        With sb
            .Append("SELECT ")
            .Append("       E.PrgVerId,arPrgVersions.PrgVerCode,arPrgVersions.PrgVerDescrip,")
            .Append("       (SELECT COUNT(*) ")
            .Append("           FROM arStuEnrollments A,plExitInterView X ")
            .Append("           WHERE X.Eligible='Yes' AND X.EnrollmentId=A.StuEnrollId ")
            .Append("           AND A.PrgVerId=E.PrgVerId) AS EligibleGraduates,")
            .Append("       (SELECT COUNT(*) ")
            .Append("           FROM arStuEnrollments A,plStudentsPlaced B ")
            .Append("           WHERE A.StuEnrollId = B.StuEnrollId ")
            .Append("           AND A.PrgVerId=E.PrgVerId) AS PlacedGraduates,")
            .Append("       (SELECT COUNT(*) ")
            .Append("           FROM plStudentsPlaced X,arStuEnrollments Y ")
            .Append("           WHERE X.FldStudyID=(SELECT FldStudyId FROM plFldStudy WHERE FldStudyDescrip='Yes') ")
            .Append("           AND Y.PrgVerId=E.PrgVerId AND X.StuEnrollId=Y.StuEnrollId) AS InFieldOfStudy,")
            .Append("       (SELECT COUNT(*) ")
            .Append("           FROM plStudentsPlaced X,arStuEnrollments Y ")
            .Append("           WHERE X.FldStudyID=(SELECT FldStudyId FROM plFldStudy WHERE FldStudyDescrip='No') ")
            .Append("           AND Y.PrgVerId=E.PrgVerId AND X.StuEnrollId=Y.StuEnrollId) AS OutFieldOfStudy, ")
            .Append("       (SELECT COUNT(*) ")
            .Append("           FROM plStudentsPlaced X,arStuEnrollments Y ")
            .Append("           WHERE X.FldStudyID=(SELECT FldStudyId FROM plFldStudy WHERE FldStudyDescrip='Related') ")
            .Append("           AND Y.PrgVerId=E.PrgVerId AND X.StuEnrollId=Y.StuEnrollId) AS RelatedFieldOfStudy ")
            .Append("       FROM arStuEnrollments E,arPrgVersions ")
            .Append("       WHERE E.PrgVerId = arPrgVersions.PrgVerId ")
            .Append(strWhere)
            .Append("       GROUP BY E.PrgVerId,arPrgVersions.PrgVerCode,arPrgVersions.PrgVerDescrip  ")
            .Append(strOrderBy)


        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "StatsByField"
            'Add new columns
            ds.Tables(0).Columns.Add(New DataColumn("EligiblePercentage", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("PlacedPercentage", System.Type.GetType("System.String")))

            Dim temp As Integer
            Dim inFld As Integer

            For Each dr As DataRow In ds.Tables(0).Rows
                If Not dr.IsNull("EligibleGraduates") And Not dr.IsNull("PlacedGraduates") _
                    And Not dr.IsNull("InFieldOfStudy") And Not dr.IsNull("OutFieldOfStudy") _
                    And Not dr.IsNull("RelatedFieldOfStudy") Then

                    If dr("EligibleGraduates") = 0 And dr("PlacedGraduates") = 0 And _
                            dr("InFieldOfStudy") = 0 And dr("OutFieldOfStudy") = 0 And _
                            dr("RelatedFieldOfStudy") = 0 Then
                        dr.Delete()
                    End If
                End If

                If Not dr.RowState = DataRowState.Deleted Then
                    'Compute PlacedPercentage as InFieldOfStudy / (InFieldOfStudy + OutFieldOfStudy + RelatedFieldOfStudy) * 100
                    temp = 0
                    inFld = 0
                    If Not dr.IsNull("InFieldOfStudy") Then
                        temp += dr("InFieldOfStudy")
                        inFld = dr("InFieldOfStudy")
                    End If
                    If Not dr.IsNull("OutFieldOfStudy") Then
                        temp += dr("OutFieldOfStudy")
                    End If
                    If Not dr.IsNull("RelatedFieldOfStudy") Then
                        temp += dr("RelatedFieldOfStudy")
                    End If
                    If temp <> 0 And inFld <> 0 Then
                        dr("PlacedPercentage") = Convert.ToInt32(inFld / temp * 100) & "%"
                    End If
                    'Compute EligiblePercentage as InFieldOfStudy / EligibleGraduates * 100
                    If dr("EligibleGraduates") <> 0 And inFld <> 0 Then
                        dr("EligiblePercentage") = Convert.ToInt32(inFld / dr("EligibleGraduates") * 100) & "%"
                    End If
                End If
            Next

            ds.AcceptChanges()
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

End Class
