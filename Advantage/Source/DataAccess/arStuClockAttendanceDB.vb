Imports System.Data
Imports FAME.Advantage.Common

Public Class arStuClockAttendanceDB

#Region "Private Data Members"
    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")
    Public m_AsOfDate As DateTime = Convert.ToDateTime(Date.Now)

#End Region

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

    Public ReadOnly Property EndDate() As DateTime
        Get
            Return m_AsOfDate
        End Get
    End Property


#Region "Public Methods"

    Public Function GetCompletedHoursForStudent(ByVal stuEnrollId As String) As Integer
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        'Dim ds As New DataSet
        Dim CompletedHours As Integer = 0

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("select sum(schedhours) as schedhours from arStudentClockAttendance where stuenrollid = ? and istardy = 0 ")
        End With

        db.AddParameter("StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            CompletedHours = dr("schedhours").ToString
        End While

        If Not dr.IsClosed Then dr.Close()

        Return CompletedHours

    End Function

    Public Function GetScheduledHoursForStudent(ByVal stuEnrollId As String) As Integer
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        'Dim ds As New DataSet
        Dim ScheduledHours As Integer = 0

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("select distinct actualhours from arStudentClockAttendance where stuenrollid = ? and istardy = 0 ")
        End With

        db.AddParameter("StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'ds = db.RunParamSQLDataSet(sb.ToString)
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            ScheduledHours = dr("actualhours").ToString
        End While
        'Return ds.Tables(0)

        If Not dr.IsClosed Then dr.Close()

        Return ScheduledHours

    End Function

    Public Function GetDayAttendance(ByVal paramInfo As ReportParamInfo, ByVal CampusId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strWhere As String
        Dim percentage As Integer
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOtherString <> "" Then
            percentage = CInt(paramInfo.FilterOtherString)
        End If

        Dim strSuppressDate As String = "no"
        Try
            strSuppressDate = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
        Catch ex As System.Exception
            strSuppressDate = "no"
        End Try

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select  distinct SE.StuEnrollID, ")
            '.Append(" select sum(schedhours) as schedhours from arStudentClockAttendance where stuenrollid = ?, ")
            .Append(" SCA.ActualHours,ARS.lastname +' '+ ARS.firstname as StudentName,ARS.ssn as StudentIdentifier,")
            .Append("'" & strSuppressDate & "'")
            .Append(" as SuppressDate ")
            .Append("   from ")
            .Append("   arStudentClockAttendance SCA,")
            .Append("   arStudentSchedules SS,")
            .Append(" arProgSchedules PS, arPrgVersions PV,  ")
            .Append(" arPrograms P, arStuEnrollments SE, ")
            .Append(" arStudent S, arAttUnitType AUT, adLeadByLeadGroups LLG, arstudent ARS,")
            .Append(" arClassSections, syCampGrps,arStuEnrollments ,atclssectattendance	 ")
            .Append(" where SCA.StuEnrollId = arStuEnrollments.StuEnrollId ")
            .Append(" and   arStuEnrollments.StudentId = ars.StudentId and ")
            .Append("  arStuEnrollments.CampusId = ? ")
            .Append(" and SCA.ScheduleId = SS.ScheduleId ")
            .Append(" and ars.StudentId=S.StudentId ")
            .Append(" and SCA.StuEnrollId = SS.StuEnrollId ")
            .Append("  and SS.StuEnrollId = SE.StuEnrollId ")
            .Append(" and SE.StudentId = S.StudentId ")
            .Append(" and SCA.ScheduleId = SS.ScheduleId ")
            .Append(" and SCA.ScheduleId = PS.ScheduleId ")
            .Append(" and PS.PrgVerId = PV.PrgVerId ")
            .Append(" and PV.ProgId = P.ProgId ")
            .Append(" and PV.UnitTypeId = AUT.UnitTypeId ")
            .Append(" and SS.Active = 1 ")
            .Append(" and SCA.StuEnrollId = LLG.StuEnrollId ")
            .Append(strWhere)
            .Append(" and atclssectattendance.actual=1 ")
            .Append(" group by arStuEnrollments.PrgVerId,SE.StuEnrollID, ")
            .Append(" ARS.ssn, ARS.lastname,ARS.firstname,SCA.ActualHours ")
            .Append(" order by StudentName")
        End With

        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim ds1 As New DataSet

        Dim dt As New DataTable("StudentClockAttendance")
        If ds.Tables.Count > 0 Then

            dt.Columns.Add(New DataColumn("StuEnrollId", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("StudentIdentifier", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ScheduledHours", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CompletedHours", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Attendance", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("SuppressDate", System.Type.GetType("System.String")))

            Dim row As DataRow
            While dr.Read()

                row = dt.NewRow()
                row("StudentIdentifier") = dr("StudentIdentifier").ToString
                row("StudentName") = dr("StudentName").ToString

                row("ScheduledHours") = GetScheduledHoursForStudent(dr("StuEnrollId").ToString)
                row("CompletedHours") = GetCompletedHoursForStudent(dr("StuEnrollId").ToString)

                'row("CompletedHours") = dr("ActualHours").ToString
                'row("Attendance") = dr("StudentName").ToString

                'row("ScheduledHours") = GetScheduledHoursForStudent(dr("StuEnrollId").ToString, m_AsOfDate) / 60
                'row("CompletedHours") = GetCompletedHoursForStudent(dr("StuEnrollId").ToString, m_AsOfDate) / 60

                If row("ScheduledHours") = 0 Then
                    row("Attendance") = 0
                Else
                    'row("Attendance") = ((GetCompletedHoursForStudent(dr("StuEnrollId").ToString, m_AsOfDate) / GetScheduledHoursForStudent(dr("StuEnrollId").ToString, m_AsOfDate))) * 100
                    row("Attendance") = CInt((row("CompletedHours")) / CInt(row("ScheduledHours"))) * 100
                End If

                Try
                    row("SuppressDate") = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
                Catch ex As System.Exception
                    row("SuppressDate") = "no"
                End Try

                If CInt(row("Attendance")) <= percentage Then
                    dt.Rows.Add(row)
                End If

            End While
            ds1.Tables.Add(dt)
        End If

        If Not dr.IsClosed Then dr.Close()

        Return ds1
    End Function

#End Region


    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

End Class
