Imports Microsoft.VisualBasic
Imports FAME.Advantage.Common

Public Class AcreditationAgenciesDB
    Public Function GetFieldsUsedByAcreditationAgency(ByVal acreditationAgencyId As Integer) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("		RAF.RptAgencyFldId as Value, ")
            .Append("       RF.Descrip as Text ")
            .Append("FROM ")
            .Append("       syRptAgencyFields RAF, syRptFields RF ")
            .Append("WHERE ")
            .Append("       RAF.RptFldId = RF.RptFldId ")
            .Append("AND	RptAgencyId = ? ")
            .Append("ORDER BY RF.Descrip ")
        End With

        ' Add the AgencyId to the parameter list
        db.AddParameter("@AcreditationAgencyId", acreditationAgencyId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    'Public Function GetMappingOptionsByField(ByVal acreditationAgencyFieldId As Integer) As DataSet

    '    '   connect to the database
    '    Dim db As New DataAccess
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

    '    '   build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append("SELECT ")
    '        .Append("		RAFV.RptAgencyFldValId, ")
    '        .Append("       RF.Descrip, ")
    '        .Append("       RF.DDL, ")
    '        .Append("       RAFV.AgencyDescrip, ")
    '        .Append("       RAFV.MapToThisValue, ")
    '        .Append("       RAFV.ModUser, ")
    '        .Append("       RAFV.ModDate ")
    '        .Append("FROM ")
    '        .Append("		syRptAgencyFields RAF, syRptAgencyFldValues RAFV, syRptFields RF ")
    '        .Append("WHERE ")
    '        .Append("       RAF.RptAgencyFldId = RAFV.RptAgencyFldId ")
    '        .Append("AND    RAF.RptFldId = RF.RptFldId ")
    '        If acreditationAgencyFieldId > 0 Then
    '            .Append("AND	RAF.RptAgencyFldId = ? ")
    '            ' Add the AgencyId to the parameter list
    '            db.AddParameter("@AcreditationAgencyFieldId", acreditationAgencyFieldId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
    '        End If
    '    End With

    '    '   return dataset
    '    Return db.RunParamSQLDataSet(sb.ToString)

    'End Function

    Private Function GetDeletedRows(ByVal dt As DataTable) As DataRow()
        Dim Rows() As DataRow
        If dt Is Nothing Then Return Rows
        Rows = dt.Select("", "", DataViewRowState.Deleted)
        If Rows.Length = 0 OrElse Not (Rows(0) Is Nothing) Then Return Rows
        '
        ' Workaround:
        ' With a remoted DataSet, Select returns the array elements
        ' filled with Nothing/null, instead of DataRow objects.
        '
        Dim r As DataRow, I As Integer = 0
        For Each r In dt.Rows
            If r.RowState = DataRowState.Deleted Then
                Rows(I) = r
                I += 1
            End If
        Next
        Return Rows
    End Function

    Public Function UpdateMappedValues(ByVal mappingOptionsDS As DataSet) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        '   all updates must be encapsulated as one transaction
        Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()


        Try
            '   build select query for the RptAgencyFldValues data adapter
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("       MappingId, ")
                .Append("		RptAgencyFldValId, ")
                .Append("       SchoolDescripId, ")
                .Append("       ModUser, ")
                .Append("       ModDate ")
                .Append("FROM ")
                .Append("		syRptAgencySchoolMapping ")
                '.Append(" where RptAgencyFldValId <> 29 ")
            End With

            '   build select command
            Dim mappedValuesSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle mappedValues table
            Dim mappedValuesDataAdapter As New OleDbDataAdapter(mappedValuesSelectCommand)

            '   build insert, update and delete commands for mappedValues table
            Dim cb As New OleDbCommandBuilder(mappedValuesDataAdapter)

            '   delete rows in mappedValues table

            mappedValuesDataAdapter.Update(mappingOptionsDS.Tables("RptAgencySchoolMappings").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   delete rows in mappedValues table
            mappedValuesDataAdapter.Update(mappingOptionsDS.Tables("RptAgencySchoolMappings").Select(Nothing, Nothing, DataViewRowState.Added))

            'Get agency
            Dim RptAgencyFldId As Integer
            Dim AgencyDescrip As String
            RptAgencyFldId = CType(mappingOptionsDS.Tables("RptAgencyFields").Rows(0)("RptAgencyFldId").ToString(), Integer)
            AgencyDescrip = GetAgencyDescipFromRptAgencyFldId(RptAgencyFldId)


            Select Case AgencyDescrip
                Case "IPEDS"
                    ' additions
                    Dim BuildIPEDsDB As New ReportingAgencyDB
                    For Each dr As DataRow In mappingOptionsDS.Tables("RptAgencySchoolMappings").Rows
                        Try
                            BuildIPEDsDB.BuildIPEDSValue(dr("RptAgencyFldValId").ToString, dr("SchoolDescripId").ToString,, groupTrans)
                        Catch ex As System.Exception
                        End Try
                    Next
                Case "GE"
                    ' additions
                    Dim BuildGainfulEmploymentDB As New ReportingAgencyDB
                    For Each dr As DataRow In mappingOptionsDS.Tables("RptAgencySchoolMappings").Rows
                        Try
                            BuildGainfulEmploymentDB.UpdateGainfulEmploymentValues(dr("RptAgencyFldValId").ToString, dr("SchoolDescripId").ToString,, groupTrans)
                        Catch ex As System.Exception
                        End Try
                    Next

            End Select

            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            Return ""

        Catch ex As OleDb.OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            '   close connection
            connection.Close()
        End Try
    End Function
    Public Function GetDDLValuesDS(ByVal agencyId As Integer, ByVal agencyFieldId As Integer) As DataSet
        'create dataset with initial tables
        Dim ds As DataSet = CreateDataset()
        Dim dt As DataTable = ds.Tables("DDLValues")

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       RAF.RptAgencyFldId, ")
            .Append("       RF.DDL, ")
            .Append("       RF.Descrip ")
            .Append("FROM ")
            .Append("       syRptAgencyFields RAF, syRptFields RF ")
            .Append("WHERE ")
            .Append("       RAF.RptFldId = RF.RptFldId ")
            .Append("AND	RAF.RptAgencyId = ? ")
            If agencyFieldId > 0 Then
                .Append("AND    RAF.RptAgencyFldId = ? ")
            End If
            ''FOR IPEDS -- Ethnic Codes Dropdown get rid of Nonresident Alien
            'If agencyId = 1 And agencyFieldId = 15 Then
            '    .Append(" AND RAF.RptAgencyFldValId <> 29 ")
            'End If
            '   DE5159 Janet Robinson 4/26/2011 Remove States from dropdown list for IPEDS only
            If agencyId = 1 Then
                .Append(" AND RAF.RptAgencyFldId <> 34 ")
            End If
        End With

        ' Add the AgencyId to the parameter list
        db.AddParameter("@AgencyId", agencyId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        If agencyFieldId > 0 Then
            ' Add the AgencyId to the parameter list
            db.AddParameter("@AgencyFieldId", agencyFieldId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        End If

        'get datareader
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()

            'instantiate the proper ddl's metadata
            Dim ddlMD As AdvantageDropDownListMetadata = Activator.CreateInstance(GetType(AdvantageDropDownListMetadata).Module.Assembly.GetType("FAME.AdvantageV1.Common." + CType(dr("DDL"), String) + "DDLMetadata"))

            'add metadata ddl's to a list
            Dim ddlList As New System.Collections.Generic.List(Of AdvantageDropDownListMetadata)
            ddlList.Add(ddlMD)

            'get ds with dropdownlists tables
            Dim ddlDS As DataSet = (New DropDownListsDB).GetDropDownLists(ddlList)
            For i As Integer = 0 To ddlDS.Tables(0).Rows.Count - 1
                Dim row As DataRow = dt.NewRow()
                row("DDLValueId") = Guid.NewGuid.ToString
                row("RptAgencyFldId") = dr("RptAgencyFldId")
                row("Descrip") = dr("Descrip")
                Dim tabRow As DataRow = ddlDS.Tables(0).Rows(i)
                row("SchoolValue") = tabRow(0)
                row("SchoolText") = tabRow(1)
                dt.Rows.Add(row)
            Next
        End While

        'close datareader
        dr.Close()

        'Create query for the RptAgencyFields
        '   build the sql query
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       RAF.RptAgencyFldId ")
            .Append("FROM ")
            .Append("       syRptAgencyFields RAF ")
            .Append("WHERE ")
            .Append("       RptAgencyId = ? ")
            If agencyFieldId > 0 Then
                .Append("AND    RAF.RptAgencyFldId = ? ")
            End If
            '   DE5159 Janet Robinson 4/26/2011 Remove States from dropdown list for IPEDS only
            If agencyId = 1 Then
                .Append(" AND RAF.RptAgencyFldId <> 34 ")
            End If
        End With

        '   build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

        '   add AcreditationAgencyId parameter
        sc.Parameters.Add(New OleDbParameter("AgencyId", agencyId))

        If agencyFieldId > 0 Then
            '   add AgencyFieldId parameter
            sc.Parameters.Add(New OleDbParameter("AgencyFieldId", agencyFieldId))
        End If

        '   Create adapter to handle RptAgencyFields table
        Dim da As New OleDbDataAdapter(sc)

        '   Fill RptAgencyFields table
        da.Fill(ds, "RptAgencyFields")

        'Create query for the RptAgencyFldValues
        '   build the sql query
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       RAFV.RptAgencyFldValId, ")
            .Append("       RAFV.RptAgencyFldId, ")
            .Append("       RAFV.AgencyDescrip ")
            .Append("FROM ")
            .Append("       syRptAgencyFldValues RAFV, syRptAgencyFields RAF ")
            .Append("WHERE ")
            .Append("       RAF.RptAgencyFldId=RAFV.RptAgencyFldId ")
            .Append("AND	RAF.RptAgencyId = ? ")
            If agencyFieldId > 0 Then
                .Append("AND    RAF.RptAgencyFldId = ? ")
            End If
            'FOR IPEDS -- Ethnic Codes Dropdown get rid of Nonresident Alien
            If agencyId = 1 And agencyFieldId = 15 Then
                .Append(" AND RAFV.RptAgencyFldValId <> 29 ")
            End If
            '   DE5159 Janet Robinson 4/26/2011 Remove States from dropdown list for IPEDS only
            If agencyId = 1 Then
                .Append(" AND RAFV.RptAgencyFldId <> 34 ")
            End If
        End With

        '   build select command
        sc = New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

        '   add AgencyId parameter
        sc.Parameters.Add(New OleDbParameter("AgencyId", agencyId))

        If agencyFieldId > 0 Then
            '   add AgencyId parameter
            sc.Parameters.Add(New OleDbParameter("AgencyFieldId", agencyFieldId))
        End If

        '   Create adapter to handle RptAgencyFldValues table
        da = New OleDbDataAdapter(sc)

        '   Fill GradeSystems table
        da.Fill(ds, "RptAgencyFldValues")

        'Create query for the RptSchoolMappings
        '   build the sql query
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       RASM.MappingId, ")
            .Append("       RASM.RptAgencyFldValId, ")
            .Append("       RASM.SchoolDescripId, ")
            .Append("       RASM.ModUser, ")
            .Append("       RASM.ModDate, ")
            .Append("       ' ' AS MappingIDOrig, ' ' AS TblName ")
            .Append("FROM ")
            .Append("       syRptAgencySchoolMapping RASM, syRptAgencyFldValues RAFV, syRptAgencyFields RAF ")
            .Append("WHERE ")
            .Append("       RASM.RptAgencyFldValId=RAFV.RptAgencyFldValId ")
            .Append("AND    RAFV.RptAgencyFldId=RAF.RptAgencyFldId ")
            .Append("AND	RAF.RptAgencyId = ? ")
            If agencyFieldId > 0 Then
                .Append("AND    RAF.RptAgencyFldId = ? ")
            End If
            'FOR IPEDS -- Ethnic Codes Dropdown get rid of Nonresident Alien
            If agencyId = 1 And agencyFieldId = 15 Then
                .Append(" AND RAFV.RptAgencyFldValId <> 29 ")
            End If
            '   DE6408 Janet Robinson 4/26/2011 Remove States from dropdown list for IPEDS only
            If agencyId = 1 Then
                .Append(" AND RAFV.RptAgencyFldId <> 34 ")
            End If
        End With

        '   build select command
        sc = New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

        '   add AgencyId parameter
        sc.Parameters.Add(New OleDbParameter("AgencyId", agencyId))

        If agencyFieldId > 0 Then
            '   add AgencyFieldId parameter
            sc.Parameters.Add(New OleDbParameter("AgencyFieldId", agencyFieldId))
        End If

        '   Create adapter to handle RptAgencyFldValues table
        da = New OleDbDataAdapter(sc)

        '   Fill GradeSystems table
        da.Fill(ds, "RptAgencySchoolMappings")

        '   create primary and foreign key constraints

        '   set primary key for DDLValues table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("DDLValues").Columns("DDLValueId")
        ds.Tables("DDLValues").PrimaryKey = pk0

        '   set primary key for RptAgencyFields table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("RptAgencyFields").Columns("RptAgencyFldId")
        ds.Tables("RptAgencyFields").PrimaryKey = pk1

        '   set primary key for RptAgencyFldValues table
        Dim pk2(0) As DataColumn
        pk2(0) = ds.Tables("RptAgencyFldValues").Columns("RptAgencyFldValId")
        ds.Tables("RptAgencyFldValues").PrimaryKey = pk2

        '   set primary key for RptAgencySchoolMappings table
        Dim pk3(0) As DataColumn
        pk3(0) = ds.Tables("RptAgencySchoolMappings").Columns("MappingId")
        ds.Tables("RptAgencySchoolMappings").PrimaryKey = pk3

        '   set foreign key column in DDLValues
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("DDLValues").Columns("RptAgencyFldId")

        '   set foreign key column in RptAgencyFldValues
        Dim fk1(0) As DataColumn
        fk1(0) = ds.Tables("RptAgencyFldValues").Columns("RptAgencyFldId")

        '   set foreign key column in RptAgencySchoolMappings
        Dim fk2(0) As DataColumn
        fk2(0) = ds.Tables("RptAgencySchoolMappings").Columns("RptAgencyFldValId")

        '   set relation between RptAgencyFields and DDLValues tables
        ds.Relations.Add("RptAgencyFieldsDDLValues", pk1, fk0)

        '   set relation between RptAgencyFields and RptAgencyFldValues tables
        ds.Relations.Add("RptAgencyFieldsRptAgencyFldValues", pk1, fk1)

        '   set relation between RptAgencyFldValues and RptAgencySchoolMappings tables
        ds.Relations.Add("RptAgencyFldValuesRptAgencySchoolMappings", pk2, fk2)

        '   return dataset
        Return ds

    End Function
    Private Function CreateDataset() As DataSet
        Dim ds As New DataSet
        Dim dt As New DataTable("DDLValues")
        dt.Columns.Add("DDLVAlueId", GetType(String))
        dt.Columns.Add("RptAgencyFldId", GetType(Integer))
        dt.Columns.Add("Descrip", GetType(String))
        dt.Columns.Add("SchoolValue", GetType(String))
        dt.Columns.Add("SchoolText", GetType(String))
        ds.Tables.Add(dt)
        Return ds
    End Function
    Public Function ValidateDDLValues(ByVal agencyId As Integer, ByVal agencyFieldId As Integer) As String

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       RAF.RptAgencyFldId, ")
            .Append("       RF.DDL, ")
            .Append("       RF.Descrip ")
            .Append("FROM ")
            .Append("       syRptAgencyFields RAF, syRptFields RF ")
            .Append("WHERE ")
            .Append("       RAF.RptFldId = RF.RptFldId ")
            .Append("AND	RAF.RptAgencyId = ? ")
            If agencyFieldId > 0 Then
                .Append("AND    RAF.RptAgencyFldId = ? ")
            End If
            '   DE5159 Janet Robinson 4/26/2011 Remove States from dropdown list for IPEDS only
            If agencyId = 1 Then
                .Append(" AND RAF.RptAgencyFldId <> 34 ")
            End If
        End With

        ' Add the AgencyId to the parameter list
        db.AddParameter("@AgencyId", agencyId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        If agencyFieldId > 0 Then
            ' Add the AgencyId to the parameter list
            db.AddParameter("@AgencyFieldId", agencyFieldId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        End If

        'get datareader
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim errorMessage As String = ""
        While dr.Read()
            Try
                'instantiate the proper ddl's metadata
                Dim ddlMD As AdvantageDropDownListMetadata = Activator.CreateInstance(GetType(AdvantageDropDownListMetadata).Module.Assembly.GetType("FAME.AdvantageV1.Common." + CType(dr("DDL"), String) + "DDLMetadata"))
                Dim arrTypes As System.Type() = GetType(AdvantageDropDownListMetadata).Module.Assembly.GetTypes()
                Dim str As String = ""
            Catch ex As Exception
                errorMessage += "The DropDownList: " + CType(dr("DDL"), String) + " has no class." + vbCrLf
            End Try
        End While

        'close datareader
        dr.Close()

        'return error Message
        Return errorMessage

    End Function

    Public Function GetAgencyDescipFromRptAgencyFldId(ByVal RptAgencyFldId As Integer) As String
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT TOP 1 SRA.Descrip ")
            .Append("FROM dbo.syRptAgencyFields AS SRAF ")
            .Append("    INNER JOIN dbo.syRptAgencies AS SRA ON SRA.RptAgencyId = SRAF.RptAgencyId ")
            .Append("WHERE RptAgencyFldId = ? ")
        End With

        ' Add the AgencyId to the parameter list
        db.AddParameter("@RptAgencyFldId", RptAgencyFldId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        Return db.RunParamSQLScalar(sb.ToString).ToString


    End Function
End Class
