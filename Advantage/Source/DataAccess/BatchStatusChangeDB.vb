Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.AdvantageV1.Common
Imports FAME.Advantage.Common
Imports FAME.Advantage.DataAccess.LINQ

Public Class BatchStatusChangeDB


#Region "Student Status Changes"

    ''' <summary>
    ''' Change the status to NO START (8)
    ''' </summary>
    ''' <param name="change"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ChangeStatusToNoStart(change As StudentChangeHistoryObj) As String

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sqlTrans As SqlTransaction

        '------------------------------------------
        'JG - 07/09/2015 - As per Lori and US76452
        '------------------------------------------
        change.Lda = Nothing
        change.DateDetermined = Nothing
        '------------------------------------------


        'Prepare Update Enrollment table
        Dim updateEnrollmentCommand As SqlCommand = PrepareUpdateEnrollmentNoStartCommand(change)
        updateEnrollmentCommand.Connection = sqlconn

        'Prepare Insert The Status Change Student Table
        'change.DateOfChange = change.DateOfReenrollment
        Dim insertStatusChangeCommand As SqlCommand = PrepareInsertStatusChangeCommand(change)
        insertStatusChangeCommand.Connection = sqlconn

        'Delete from ArResult
        Const sqlDeleteResult As String = "DELETE from arResults where StuEnrollId= @StuEnrollId"
        Dim deleteArResultCommand As New SqlCommand(sqlDeleteResult, sqlconn)
        deleteArResultCommand.Parameters.AddWithValue("@StuEnrollId", change.StuEnrollId)

        'Delete from AtClsAttendance
        Const sqlDeleteClsSectAtten As String = "DELETE from atClsSectAttendance where StuEnrollId= @StuEnrollId;"
        Dim deleteClsSectAttenCommand As New SqlCommand(sqlDeleteClsSectAtten, sqlconn)
        deleteClsSectAttenCommand.Parameters.AddWithValue("@StuEnrollId", change.StuEnrollId)

        'Delete from arStudentClockAttendance
        Const sqlDeleteClockAtten As String = "DELETE from arStudentClockAttendance where StuEnrollId= @StuEnrollId;"
        Dim deleteClockAttenCommand As New SqlCommand(sqlDeleteClockAtten, sqlconn)
        deleteClockAttenCommand.Parameters.AddWithValue("@StuEnrollId", change.StuEnrollId)

        'Execute the transaction
        sqlconn.Open()
        sqlTrans = sqlconn.BeginTransaction("ChangeStatus")
        updateEnrollmentCommand.Transaction = sqlTrans
        insertStatusChangeCommand.Transaction = sqlTrans
        deleteArResultCommand.Transaction = sqlTrans
        deleteClsSectAttenCommand.Transaction = sqlTrans
        deleteClockAttenCommand.Transaction = sqlTrans

        Try
            updateEnrollmentCommand.ExecuteNonQuery()
            deleteArResultCommand.ExecuteNonQuery()
            deleteClsSectAttenCommand.ExecuteNonQuery()
            deleteClockAttenCommand.ExecuteNonQuery()
            insertStatusChangeCommand.ExecuteNonQuery()
            sqlTrans.Commit()
            Return String.Empty

        Catch ex As Exception

            sqlTrans.Rollback()
            Return DALExceptions.BuildErrorMessage(ex)
        Finally

            sqlconn.Close()
        End Try
    End Function

    Public Function TerminateStudent(change As StudentChangeHistoryObj)
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sqlTrans As SqlTransaction

        'Prepare Update Enrollment table
        Dim updateEnrollmentCommand As SqlCommand = PrepareUpdateEnrollmentCommand(change)
        updateEnrollmentCommand.Connection = sqlconn

        'Prepare Insert The Status Change Student Table
        'change.DateOfChange = change.DateOfReenrollment
        Dim insertStatusChangeCommand As SqlCommand = PrepareInsertStatusChangeCommand(change)
        insertStatusChangeCommand.Connection = sqlconn

        'Prepare to update Student Table
        Dim updateStudentTableCommand As SqlCommand = PrepareInactivStudentIfAllEnrollmentAreDroppedCommand(change)
        updateStudentTableCommand.Connection = sqlconn

        Dim deleteScheduleCoursesCommand As SqlCommand = GetDeleteScheduleCoursesCommand(change)
        deleteScheduleCoursesCommand.Connection = sqlconn

        'Prepare Update arStdSuspensions Table
        Dim updateSuspension As Boolean = False
        Dim updateStudentSuspensionCommand As SqlCommand
        updateSuspension = GetLastSuspensionGuid(change.StuEnrollId) IsNot Nothing

        If updateSuspension Then
            updateStudentSuspensionCommand = PrepareUpdateStudentSuspensionCommand(change)
            updateStudentSuspensionCommand.Connection = sqlconn
        End If

        'Execute the transaction
        sqlconn.Open()
        sqlTrans = sqlconn.BeginTransaction("ChangeStatus")
        updateEnrollmentCommand.Transaction = sqlTrans
        insertStatusChangeCommand.Transaction = sqlTrans
        updateStudentTableCommand.Transaction = sqlTrans
        deleteScheduleCoursesCommand.Transaction = sqlTrans
        If updateSuspension Then updateStudentSuspensionCommand.Transaction = sqlTrans


        Try
            updateEnrollmentCommand.ExecuteNonQuery()
            updateStudentTableCommand.ExecuteNonQuery()
            insertStatusChangeCommand.ExecuteNonQuery()
            deleteScheduleCoursesCommand.ExecuteNonQuery()
            If updateSuspension Then updateStudentSuspensionCommand.ExecuteNonQuery()
            sqlTrans.Commit()
            Return String.Empty

        Catch ex As Exception

            sqlTrans.Rollback()
            Throw
        Finally

            sqlconn.Close()
        End Try
    End Function

    ''' <summary>
    ''' Change Status To Transfer OUT (19)
    ''' </summary>
    ''' <param name="change"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ChangeStatusToTransferOut(change As StudentChangeHistoryObj) As String
        '   connect to the database
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sqlTrans As SqlTransaction

        'Prepare Update Enrollment table
        Dim updateEnrollmentCommand As SqlCommand = PrepareUpdateEnrollmentCommandNoDateChanged(change)
        updateEnrollmentCommand.Connection = sqlconn

        'Prepare Update Student Status Change Table
        'change.DateOfChange = change.DateOfReenrollment
        Dim insertStatusChangeCommand As SqlCommand = PrepareInsertStatusChangeCommand(change)
        insertStatusChangeCommand.Connection = sqlconn

        'Execute the transaction
        sqlconn.Open()
        sqlTrans = sqlconn.BeginTransaction("ChangeStatus")
        updateEnrollmentCommand.Transaction = sqlTrans
        insertStatusChangeCommand.Transaction = sqlTrans
        Try
            updateEnrollmentCommand.ExecuteNonQuery()
            insertStatusChangeCommand.ExecuteNonQuery()
            sqlTrans.Commit()
            Return String.Empty

        Catch ex As Exception

            sqlTrans.Rollback()
            Return ex.Message
        Finally

            sqlconn.Close()
        End Try


    End Function

    ''' <summary>
    ''' Change Status to Graduated (14)
    ''' </summary>
    ''' <param name="change"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ChangeStatusToGraduated(change As StudentChangeHistoryObj) As String

        '   connect to the database
        '   connect to the database
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sqlTrans As SqlTransaction

        'Prepare Update Enrollment table
        Dim updateEnrollmentCommand As SqlCommand = PrepareUpdateEnrollmentCommandGradOrCurrent(change)
        updateEnrollmentCommand.Connection = sqlconn

        'Prepare to update Student Table
        Dim updateStudentTableCommand As SqlCommand = PrepareInactivStudentIfAllEnrollmentAreDroppedCommand(change)
        updateStudentTableCommand.Connection = sqlconn

        'Prepare Update Student Status Change Table
        'change.DateOfChange = change.DateOfReenrollment
        Dim insertStatusChangeCommand As SqlCommand = PrepareInsertStatusChangeCommand(change)
        insertStatusChangeCommand.Connection = sqlconn

        'Execute the transaction
        sqlconn.Open()
        sqlTrans = sqlconn.BeginTransaction("ChangeStatus")
        updateEnrollmentCommand.Transaction = sqlTrans
        updateStudentTableCommand.Transaction = sqlTrans
        insertStatusChangeCommand.Transaction = sqlTrans
        Try
            updateEnrollmentCommand.ExecuteNonQuery()
            updateStudentTableCommand.ExecuteNonQuery()
            insertStatusChangeCommand.ExecuteNonQuery()
            sqlTrans.Commit()
            Return String.Empty

        Catch ex As Exception

            sqlTrans.Rollback()
            Return ex.Message
        Finally

            sqlconn.Close()
        End Try

    End Function


#Region " Region Change Status to Currently Attending (9)"

    Public Function ChangeStatusFromLOAToCurrentlyAttending(change As StudentChangeHistoryObj) As String
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sqlTrans As SqlTransaction

        'Prepare Update Enrollment table
        Dim updateEnrollmentCommand As SqlCommand = PrepareUpdateEnrollmentCommandNoDateChanged(change)
        updateEnrollmentCommand.Connection = sqlconn

        'Prepare Update arStudentLOAs Table
        Dim updateStudentLoaCommand As SqlCommand = PrepareUpdateStudentLoaCommand(change)
        updateStudentLoaCommand.Connection = sqlconn

        'Prepare Update Student Status Change Table
        'change.DateOfChange = change.DateOfReenrollment
        Dim insertStatusChangeCommand As SqlCommand = PrepareInsertStatusChangeCommand(change)
        insertStatusChangeCommand.Connection = sqlconn

        'Execute the transaction
        sqlconn.Open()
        sqlTrans = sqlconn.BeginTransaction("ChangeStatus")
        insertStatusChangeCommand.Transaction = sqlTrans
        updateStudentLoaCommand.Transaction = sqlTrans
        updateEnrollmentCommand.Transaction = sqlTrans

        Try
            updateEnrollmentCommand.ExecuteNonQuery()
            updateStudentLoaCommand.ExecuteNonQuery()
            insertStatusChangeCommand.ExecuteNonQuery()
            sqlTrans.Commit()
            Return String.Empty

        Catch ex As Exception

            sqlTrans.Rollback()
            Return ex.Message
        Finally

            sqlconn.Close()
        End Try


    End Function

    'Public Function ChangeStatusFromLOAToCurrentlyAttending(ByVal StuEnrollId As String, ByVal user As String, _
    '                    Optional ByVal currAttendingStatusId As String = "", _
    '                    Optional ByVal returnFromLOADate As String = "") As String
    '    '   connect to the database
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim currAttending As String

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    If currAttendingStatusId = "" Then
    '        currAttending = GetCurrentlyAttendingStatus()
    '    Else
    '        currAttending = currAttendingStatusId
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   Encapsulate all DB updates in one transaction
    '    Dim groupTrans As OleDbTransaction = db.StartTransaction()

    '    Try
    '        '   Update arStuEnrollments table
    '        With sb
    '            .Append("UPDATE     arStuEnrollments ")
    '            .Append("SET        StatusCodeId=?, ModDate=?, ModUser=? ")
    '            .Append("WHERE      StuEnrollId=? ")
    '        End With

    '        '   StatusCodeId
    '        db.AddParameter("@StatusCodeId", currAttending, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   ModDate
    '        Dim modDate As DateTime = Date.Now
    '        db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   StuEnrollId
    '        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
    '        '
    '        '
    '        '
    '        '   Clear out parameter list
    '        db.ClearParameters()
    '        '
    '        '   Clear out string builder
    '        sb.Remove(0, sb.Length)
    '        '
    '        '
    '        '
    '        '   Update arStudentLOAs table
    '        With sb
    '            '   Update arStudentLOAs table
    '            .Append("UPDATE     arStudentLOAs ")
    '            .Append("SET ")
    '            If Not (returnFromLOADate = "") Then
    '                .Append("       LOAReturnDate=?,")
    '            End If
    '            .Append("           ModDate=?, ModUser=?  ")
    '            .Append("WHERE      StuEnrollId=? and LOAReturnDate is null ")
    '        End With

    '        '   LOAReturnDate
    '        If Not (returnFromLOADate = "") Then
    '            db.AddParameter("@LOAReturnDate", returnFromLOADate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        End If

    '        '   ModDate
    '        db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   StuEnrollId
    '        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
    '        '
    '        '   commit transaction 
    '        groupTrans.Commit()

    '        '   return without errors
    '        Return ""

    '    Catch ex As OleDbException
    '        '   rollback transaction if there were errors
    '        groupTrans.Rollback()

    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)

    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try

    'End Function

    Public Function ChangeStatusFromSuspendedToCurrentlyAttending(change As StudentChangeHistoryObj) As String
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sqlTrans As SqlTransaction

        'Prepare Update Enrollment table
        Dim updateEnrollmentCommand As SqlCommand = PrepareUpdateEnrollmentCommandNoDateChanged(change)
        updateEnrollmentCommand.Connection = sqlconn

        'Prepare Update arStdSuspensions Table
        Dim updateStudentSuspensionCommand As SqlCommand = PrepareUpdateStudentSuspensionCommand(change)
        updateStudentSuspensionCommand.Connection = sqlconn

        'Prepare Insert Student Status Change Table
        Dim insertStatusChangeCommand As SqlCommand = PrepareInsertStatusChangeCommand(change)
        insertStatusChangeCommand.Connection = sqlconn

        'Execute the transaction
        sqlconn.Open()
        sqlTrans = sqlconn.BeginTransaction("ChangeStatus")
        updateEnrollmentCommand.Transaction = sqlTrans
        updateStudentSuspensionCommand.Transaction = sqlTrans
        insertStatusChangeCommand.Transaction = sqlTrans
        Try
            updateEnrollmentCommand.ExecuteNonQuery()
            updateStudentSuspensionCommand.ExecuteNonQuery()
            insertStatusChangeCommand.ExecuteNonQuery()
            sqlTrans.Commit()
            Return String.Empty

        Catch ex As Exception

            sqlTrans.Rollback()
            Return ex.Message
        Finally

            sqlconn.Close()
        End Try


    End Function

    'Public Function ChangeStatusFromSuspendedToCurrentlyAttending(ByVal StuEnrollId As String, _
    '                    ByVal suspensionEndDate As String, ByVal user As String, _
    '                    Optional ByVal currAttendingStatusId As String = "") As String
    '    '   connect to the database
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim currAttending As String

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    If currAttendingStatusId = "" Then
    '        currAttending = GetCurrentlyAttendingStatus()
    '    Else
    '        currAttending = currAttendingStatusId
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   Encapsulate all DB updates in one transaction
    '    Dim groupTrans As OleDbTransaction = db.StartTransaction()

    '    Try
    '        '   Update arStuEnrollments table
    '        With sb
    '            .Append("UPDATE     arStuEnrollments ")
    '            .Append("SET        StatusCodeId=?, ModDate=?, ModUser=? ")
    '            .Append("WHERE      StuEnrollId=? ")
    '        End With

    '        '   StatusCodeId
    '        db.AddParameter("@StatusCodeId", currAttending, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   ModDate
    '        Dim modDate As DateTime = Date.Now
    '        db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   StuEnrollId
    '        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
    '        '
    '        '
    '        '   Clear out parameter list
    '        db.ClearParameters()
    '        '
    '        '
    '        '   Clear out string builder
    '        sb.Remove(0, sb.Length)
    '        '
    '        '
    '        '
    '        '   Update arStdSuspensions table
    '        With sb
    '            '   Update arStdSuspensions table
    '            .Append("UPDATE     arStdSuspensions ")
    '            .Append("SET        EndDate=?, ModDate=?, ModUser=?  ")
    '            .Append("WHERE      StuEnrollId=? ")
    '        End With

    '        '   EndDate
    '        db.AddParameter("@EndDate", suspensionEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   ModDate
    '        db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   StuEnrollId
    '        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

    '        '   commit transaction 
    '        groupTrans.Commit()

    '        '   return without errors
    '        Return ""

    '    Catch ex As OleDbException
    '        '   rollback transaction if there were errors
    '        groupTrans.Rollback()

    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)

    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try

    'End Function

    Public Function ChangeStatusToCurrentlyAttendingAndActive(change As StudentChangeHistoryObj)

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sqlTrans As SqlTransaction

        'Prepare Update Enrollment table
        'change.DateOfChange = Nothing
        change.Lda = Nothing
        change.DropReasonGuid = Nothing
        change.StudentStatus = AdvantageCommonValues.ActiveGuid
        change.DateDetermined = Nothing

        Dim updateEnrollmentCommand As SqlCommand = PrepareUpdateEnrollmentCommand(change)
        updateEnrollmentCommand.Connection = sqlconn


        'Prepare Update Student Table
        Dim updateStudentCommand As SqlCommand = PrepareUpdateStudentCommand(change)
        updateStudentCommand.Connection = sqlconn


        'Prepare Update Student Status Change Table
        change.DateOfChange = change.DateOfReenrollment
        Dim insertStatusChangeCommand As SqlCommand = PrepareInsertStatusChangeCommand(change)
        insertStatusChangeCommand.Connection = sqlconn


        'Execute the transaction
        sqlconn.Open()
        sqlTrans = sqlconn.BeginTransaction("ChangeStatus")
        updateEnrollmentCommand.Transaction = sqlTrans
        updateStudentCommand.Transaction = sqlTrans
        insertStatusChangeCommand.Transaction = sqlTrans

        Try
            updateEnrollmentCommand.ExecuteNonQuery()
            updateStudentCommand.ExecuteNonQuery()
            insertStatusChangeCommand.ExecuteNonQuery()
            sqlTrans.Commit()
            Return String.Empty

        Catch ex As Exception

            sqlTrans.Rollback()
            Return ex.Message
        Finally

            sqlconn.Close()
        End Try
    End Function

    'Public Function ChangeStatusToCurrentlyAttendingAndActive(ByVal StuEnrollId As String, _
    '                    ByVal user As String, Optional ByVal currAttendingStatusId As String = "", Optional ByVal ReEnrollmentDate As String = "") As String
    '    '   connect to the database
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim currAttending As String

    '    Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
    '    currAttending = If((currAttendingStatusId = String.Empty), GetCurrentlyAttendingStatus(), currAttendingStatusId)




    '    db.ConnectionString = myAdvAppSettings.AppSettings("ConString")




    '    '   Encapsulate all DB updates in one transaction
    '    Dim groupTrans As OleDbTransaction = db.StartTransaction()


    '    Try
    '        '   Update arStuEnrollments table
    '        With sb
    '            .Append("UPDATE     arStuEnrollments ")
    '            .Append("SET          DateDetermined= null,LDA = null,DropReasonId = null,   StatusCodeId=?, ModDate=?, ModUser=? ,ReEnrollmentDate=? ")
    '            .Append("WHERE      StuEnrollId=? ")
    '        End With

    '        db.AddParameter("@StatusCodeId", currAttending, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input) '   StatusCodeId
    '        Dim modDate As DateTime = Date.Now '   ModDate
    '        db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input) '   ModUser
    '        If (ReEnrollmentDate <> String.Empty) Then 'ReEnrollmentDate
    '            db.AddParameter("@ReEnrollDate", ReEnrollmentDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@ReEnrollDate", DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        End If
    '        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input) '   StuEnrollId
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

    '        '   Clear out parameter list
    '        db.ClearParameters()

    '        '   Clear out string builder
    '        sb.Remove(0, sb.Length)
    '        '
    '        '   Update arStudent table
    '        With sb
    '            .Append("UPDATE     arStudent ")
    '            .Append("SET        StudentStatus=?, ModDate=?, ModUser=?  ")
    '            .Append("WHERE      arStudent.StudentId = (SELECT StudentId FROM arStuEnrollments WHERE StuEnrollId=?)")
    '        End With

    '        '   StudentStatus
    '        db.AddParameter("@StudentStatus", Common.AdvantageCommonValues.ActiveGuid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   ModDate
    '        db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   StuEnrollId
    '        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

    '        '   commit transaction 
    '        groupTrans.Commit()

    '        '   return without errors
    '        Return ""

    '    Catch ex As OleDbException
    '        '   rollback transaction if there were errors
    '        groupTrans.Rollback()

    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)

    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    'End Function

    Public Function ChangeStatusToCurrentlyAttending(change As StudentChangeHistoryObj)

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn = New SqlConnection(connStr)
        Dim sqlTrans As SqlTransaction

        'Prepare Update Enrollment table
        Dim prepareUpdateEnrollmentCommand As SqlCommand = PrepareUpdateEnrollmentCommandNoDateChanged(change)
        prepareUpdateEnrollmentCommand.Connection = sqlconn

        'Prepare Update Student Status Change Table
        Dim insertStatusChangeCommand As SqlCommand = PrepareInsertStatusChangeCommand(change)
        insertStatusChangeCommand.Connection = sqlconn

        'Execute the transaction
        sqlconn.Open()
        sqlTrans = sqlconn.BeginTransaction("ChangeStatus")
        prepareUpdateEnrollmentCommand.Transaction = sqlTrans
        insertStatusChangeCommand.Transaction = sqlTrans
        Try
            prepareUpdateEnrollmentCommand.ExecuteNonQuery()
            insertStatusChangeCommand.ExecuteNonQuery()
            sqlTrans.Commit()
            Return String.Empty

        Catch ex As Exception

            sqlTrans.Rollback()
            Return ex.Message
        Finally

            sqlconn.Close()
        End Try
    End Function

    'Public Function ChangeStatusToCurrentlyAttending(ByVal StuEnrollId As String, _
    '                ByVal user As String, Optional ByVal currAttendingStatusId As String = "") As String
    '    '   connect to the database
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim currAttending As String

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    If currAttendingStatusId = "" Then
    '        currAttending = GetCurrentlyAttendingStatus()
    '    Else
    '        currAttending = currAttendingStatusId
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    Try
    '        '   Update arStuEnrollments table
    '        With sb
    '            .Append("UPDATE     arStuEnrollments ")
    '            .Append("SET        StatusCodeId=?, ModDate=?, ModUser=? ")
    '            .Append("WHERE      StuEnrollId=? ")
    '        End With

    '        '   StatusCodeId
    '        db.AddParameter("@StatusCodeId", currAttending, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   ModDate
    '        Dim modDate As DateTime = Date.Now
    '        db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   StuEnrollId
    '        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.RunParamSQLExecuteNoneQuery(sb.ToString)

    '        '   return without errors
    '        Return ""

    '    Catch ex As OleDbException
    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)

    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    'End Function

    Public Function ChangeStatusFromGradToCurrentlyAttending(change As StudentChangeHistoryObj)
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sqlTrans As SqlTransaction

        'Prepare Update Enrollment table

        Dim updateEnrollmentCommand As SqlCommand = PrepareUpdateEnrollmentCommandGradOrCurrent(change)
        updateEnrollmentCommand.Connection = sqlconn

        'Prepare Update Student Table
        Dim updateStudentCommand As SqlCommand = PrepareUpdateStudentCommand(change)
        updateStudentCommand.Connection = sqlconn

        'Prepare Update Student Status Change Table
        change.DateOfChange = change.DateOfReenrollment
        Dim insertStatusChangeCommand As SqlCommand = PrepareInsertStatusChangeCommand(change)
        insertStatusChangeCommand.Connection = sqlconn

        'Execute the transaction
        sqlconn.Open()
        sqlTrans = sqlconn.BeginTransaction("ChangeStatus")
        updateStudentCommand.Transaction = sqlTrans
        updateEnrollmentCommand.Transaction = sqlTrans
        insertStatusChangeCommand.Transaction = sqlTrans

        Try
            updateEnrollmentCommand.ExecuteNonQuery()
            updateStudentCommand.ExecuteNonQuery()
            insertStatusChangeCommand.ExecuteNonQuery()
            sqlTrans.Commit()
            Return String.Empty

        Catch ex As Exception

            sqlTrans.Rollback()
            Return ex.Message
        Finally

            sqlconn.Close()
        End Try

    End Function

    'Public Function ChangeStatusFromGradToActive(ByVal StuEnrollId As String, _
    '                 ByVal userName As String, _
    '                 Optional ByVal currAttendingStatusId As String = "", _
    '                 Optional ByVal RevisedExpGradDate As String = "") As String
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim currAttStatus As String


    '    'Set the connection string
    '    db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

    '    If currAttendingStatusId = "" Then
    '        currAttStatus = GetCurrAttendingStatus()
    '    Else
    '        currAttStatus = currAttendingStatusId
    '    End If

    '    '   we must encapsulate all DB updates in one transaction
    '    Dim groupTrans As OleDbTransaction = db.StartTransaction()

    '    Try

    '        With sb
    '            .Append("UPDATE     arStuEnrollments ")
    '            .Append("SET        StatusCodeId=?,")
    '            If Not (RevisedExpGradDate = "") Then
    '                .Append("           ExpGradDate=?,")
    '            End If
    '            .Append("           ModDate=?, ModUser=? ")
    '            .Append("WHERE      StuEnrollId=? ")
    '        End With

    '        '   StatusCodeId
    '        db.AddParameter("@StatusCodeId", currAttStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   ExpGradDate
    '        If Not (RevisedExpGradDate = "") Then
    '            db.AddParameter("@ExpGradDate", RevisedExpGradDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        End If

    '        '   ModDate
    '        Dim now As DateTime = Date.Now
    '        db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   ModUser
    '        db.AddParameter("@ModUser", userName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.AddParameter("@stdenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
    '        '
    '        '   Clear out parameter list
    '        db.ClearParameters()
    '        '
    '        '
    '        '   Clear out string builder
    '        sb.Remove(0, sb.Length)
    '        '
    '        '
    '        '
    '        '   Update arStudent table
    '        With sb
    '            .Append("UPDATE     arStudent ")
    '            .Append("SET        StudentStatus=?, ModDate=?, ModUser=?  ")
    '            .Append("WHERE      arStudent.StudentId = (SELECT StudentId FROM arStuEnrollments WHERE StuEnrollId=?)")
    '        End With

    '        '   StudentStatus
    '        db.AddParameter("@StudentStatus", AdvantageCommonValues.ActiveGuid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   ModDate
    '        db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   ModUser
    '        db.AddParameter("@ModUser", userName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   StuEnrollId
    '        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

    '        '   commit transaction 
    '        groupTrans.Commit()

    '        '   return without errors
    '        Return ""

    '    Catch ex As OleDbException
    '        '   rollback transaction if there were errors
    '        groupTrans.Rollback()

    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)

    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    'End Function

#End Region

    ''' <summary>
    ''' Change to Extern-ship status (22)
    ''' </summary>
    ''' <param name="change"></param>
    ''' <returns></returns>
    ''' <remarks>In school Status</remarks>
    Public Function ChangeStatusToExternship(change As StudentChangeHistoryObj)

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sqlTrans As SqlTransaction

        'Prepare Update Enrollment table
        Dim prepareUpdateEnrollmentCommand As SqlCommand = PrepareUpdateEnrollmentCommandNoDateChanged(change)
        prepareUpdateEnrollmentCommand.Connection = sqlconn

        'Prepare Insert Student Status Change Table
        Dim insertStatusChangeCommand As SqlCommand = PrepareInsertStatusChangeCommand(change)
        insertStatusChangeCommand.Connection = sqlconn

        'Execute the transaction
        sqlconn.Open()
        sqlTrans = sqlconn.BeginTransaction("ChangeStatus")
        prepareUpdateEnrollmentCommand.Transaction = sqlTrans
        insertStatusChangeCommand.Transaction = sqlTrans
        Try
            prepareUpdateEnrollmentCommand.ExecuteNonQuery()
            insertStatusChangeCommand.ExecuteNonQuery()
            sqlTrans.Commit()
            Return String.Empty

        Catch ex As Exception

            sqlTrans.Rollback()
            Return ex.Message
        Finally

            sqlconn.Close()
        End Try
    End Function


    ''' <summary>
    ''' Change To status Future Start (7)
    ''' </summary>
    ''' <param name="change"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ChangeStatusToFutureStart(change As StudentChangeHistoryObj)

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sqlTrans As SqlTransaction

        'Prepare Update Enrollment table

        change.DateOfChange = Nothing
        change.Lda = Nothing
        change.DropReasonGuid = Nothing


        change.StudentStatus = AdvantageCommonValues.ActiveGuid

        Dim updateEnrollmentCommand As SqlCommand = PrepareUpdateEnrollmentCommand(change)
        If change.OldSysStatusId = 9 Then
            updateEnrollmentCommand = PrepareUpdateEnrollmentCurrentlyAttendingToFutureStartCommand(change)
        End If
        updateEnrollmentCommand.Connection = sqlconn

        'Prepare Update Student Table
        Dim updateStudentCommand As SqlCommand = PrepareUpdateStudentCommand(change)
        updateStudentCommand.Connection = sqlconn


        'Prepare Update Student Status Change Table
        If change.OldSysStatusId = 9 Then
            change.DateOfChange = DateTime.Today
        Else
            change.DateOfChange = change.DateOfReenrollment
        End If

        Dim insertStatusChangeCommand As SqlCommand = PrepareInsertStatusChangeCommand(change)
        insertStatusChangeCommand.Connection = sqlconn

        If change.OldSysStatusId = 8 Then
            InsertArResults(change.StuEnrollId, change.ModUser)
        End If




        'Execute the transaction
        sqlconn.Open()
        sqlTrans = sqlconn.BeginTransaction("ChangeStatus")
        updateEnrollmentCommand.Transaction = sqlTrans
        updateStudentCommand.Transaction = sqlTrans
        insertStatusChangeCommand.Transaction = sqlTrans

        Try
            updateEnrollmentCommand.ExecuteNonQuery()
            updateStudentCommand.ExecuteNonQuery()
            insertStatusChangeCommand.ExecuteNonQuery()
            sqlTrans.Commit()
            Return String.Empty

        Catch ex As Exception

            sqlTrans.Rollback()
            Return ex.Message
        Finally

            sqlconn.Close()
        End Try
    End Function


    ''' <summary>
    ''' Suspension only schedule the Suspension. The suspension is done by a JOB
    ''' That run at night.
    ''' </summary>
    ''' <param name="change"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ChangeStatusToSuspension(change As StudentChangeHistoryObj) As String
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sqlTrans As SqlTransaction

        ' The LOA is a posteriori, then enter the changes directly
        If CDate(change.StartDate) <= Date.Now.ToShortDateString Then

            'Prepare Update Enrollment table
            'change.DateOfChange = Nothing
            'change.Lda = Nothing
            change.DropReasonGuid = Nothing
            change.StudentStatus = AdvantageCommonValues.ActiveGuid

            Dim updateEnrollmentCommand As SqlCommand = PrepareUpdateEnrollmentCommandNoDateChanged(change)
            updateEnrollmentCommand.Connection = sqlconn

            'Prepare Update Student Status Change Table
            'change.DateOfChange = change.DateOfReenrollment
            Dim insertStatusChangeCommand As SqlCommand = PrepareInsertStatusChangeCommand(change)
            insertStatusChangeCommand.Connection = sqlconn

            'Insert in arStdSuspensions Table
            Dim insertSuspensionCommand As SqlCommand = PrepareInsertInSuspensionTable(change)
            insertSuspensionCommand.Connection = sqlconn

            'Execute the transaction
            sqlconn.Open()
            sqlTrans = sqlconn.BeginTransaction("ChangeStatus")
            updateEnrollmentCommand.Transaction = sqlTrans
            insertSuspensionCommand.Transaction = sqlTrans
            insertStatusChangeCommand.Transaction = sqlTrans

            Try
                updateEnrollmentCommand.ExecuteNonQuery()
                Dim id As Guid = insertStatusChangeCommand.ExecuteScalar()
                AddWithValueSafe(insertSuspensionCommand.Parameters, "@StudentStatusChangeId", id)
                'insertSuspensionCommand.Parameters.AddWithValue("@StudentStatusChangeId", id)
                insertSuspensionCommand.ExecuteNonQuery()
                sqlTrans.Commit()
                Return String.Empty

            Catch ex As Exception
                sqlTrans.Rollback()
                Return ex.Message
            Finally

                sqlconn.Close()
            End Try

        Else
            'Insert in arStdSuspensions Table
            'Insert in arStdSuspensions Table if the date is in the future....
            Dim insertSuspensionCommand As SqlCommand = PrepareInsertInSuspensionTable(change)
            insertSuspensionCommand.Connection = sqlconn
            AddWithValueSafe(insertSuspensionCommand.Parameters, "@StudentStatusChangeId", Nothing)
            sqlconn.Open()
            Try
                insertSuspensionCommand.ExecuteNonQuery()
            Finally
                sqlconn.Close()
            End Try
            Return String.Empty
        End If
    End Function

    ''' <summary>
    ''' Place Student in Probation
    ''' </summary>
    ''' <param name="change"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function PlaceStudentOnProbation(change As StudentChangeHistoryObj) As String
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sqlTrans As SqlTransaction

        If (change.ProbWarningTypeId = 1) Then

            change.NewStatusGuid = GetSchoolDefinedStatusGuid(20, change.CampusId)

            'Prepare Update of arSAPChkResults Table
            Dim updateSAPCheckResult As SqlCommand = PrepareUpdateSapResultsTable(change)
            updateSAPCheckResult.Connection = sqlconn

            'Prepare Update Enrollment table
            Dim updateEnrollmentCommand As SqlCommand = PrepareUpdateEnrollmentCommandNoDateChanged(change)
            updateEnrollmentCommand.Connection = sqlconn

            'Prepare insert Student Status Change Table
            Dim insertStatusChangeCommand As SqlCommand = PrepareInsertStatusChangeCommand(change)
            insertStatusChangeCommand.Connection = sqlconn

            'Prepare insert arStuProbWarnings (Probation) Table
            Dim insertInProbationTableCommand As SqlCommand = PrepareInsertInProbationTable(change)
            insertInProbationTableCommand.Connection = sqlconn

            'Execute the transaction
            sqlconn.Open()
            sqlTrans = sqlconn.BeginTransaction("ChangeStatus")
            Try
                updateSAPCheckResult.Transaction = sqlTrans
                updateEnrollmentCommand.Transaction = sqlTrans
                insertInProbationTableCommand.Transaction = sqlTrans
                insertStatusChangeCommand.Transaction = sqlTrans

                updateSAPCheckResult.ExecuteNonQuery()
                updateEnrollmentCommand.ExecuteNonQuery()
                Dim id As Guid = insertStatusChangeCommand.ExecuteScalar()
                insertInProbationTableCommand.Parameters.AddWithValue("@StudentStatusChangeId", id)
                insertInProbationTableCommand.ExecuteNonQuery()
                sqlTrans.Commit()
                Return String.Empty

            Catch ex As Exception
                sqlTrans.Rollback()
                Throw
            Finally
                sqlconn.Close()
            End Try

        Else
            'Probation 2 and 3 does not change the status, only is stored in the probation table 


            'Prepare insert arStuProbWarnings (Probation) Table
            Dim insertInProbationTableCommand As SqlCommand = PrepareInsertInProbationTable(change)
            insertInProbationTableCommand.Connection = sqlconn

            'Execute the transaction
            sqlconn.Open()
            Try
                AddWithValueSafe(insertInProbationTableCommand.Parameters, "@StudentStatusChangeId", Nothing)
                insertInProbationTableCommand.ExecuteNonQuery()
                Return String.Empty

            Catch ex As Exception

                Return ex.Message
            Finally

                sqlconn.Close()
            End Try


        End If



    End Function

    ''' Place Student in Probation from LOA
    ''' </summary>
    ''' <param name="change"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ChangeStatusFromLOAToProbation(change As StudentChangeHistoryObj) As String
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sqlTrans As SqlTransaction

        'Select the probation status code by Probation identifier

        'Select Case (change.ProbWarningTypeId)
        '    Case 1
        '        change.NewStatusGuid = GetSchoolDefinedStatusGuid(20, change.CampusId)
        '    Case 2
        '        change.NewStatusGuid = GetSchoolDefinedStatusGuid(23, change.CampusId)
        '    Case 3
        '        change.NewStatusGuid = GetSchoolDefinedStatusGuid(24, change.CampusId)
        '    Case Else
        '        Return "Probation Status undefined"

        'End Select

        If String.IsNullOrEmpty(change.NewStatusGuid) Then
            Return "Undefined School Probation Status"
        End If

        'Prepare Update Enrollment table
        Dim updateEnrollmentCommand As SqlCommand = PrepareUpdateEnrollmentCommandNoDateChanged(change)
        updateEnrollmentCommand.Connection = sqlconn

        'Prepare Update arStudentLOAs Table
        Dim updateStudentLoaCommand As SqlCommand = PrepareUpdateStudentLoaCommand(change)
        updateStudentLoaCommand.Connection = sqlconn

        'Prepare Update Student Status Change Table
        Dim insertStatusChangeCommand As SqlCommand = PrepareInsertStatusChangeCommand(change)
        insertStatusChangeCommand.Connection = sqlconn

        'Prepare insert arStuProbWarnings (Probation) Table  Repeate old record with new StudentStatusChangeId
        Dim insertInProbationTableCommand As SqlCommand = PrepareInsertRepitedRecordInProbationTable(change)
        insertInProbationTableCommand.Connection = sqlconn

        'Execute the transaction
        sqlconn.Open()
        sqlTrans = sqlconn.BeginTransaction("ChangeStatus")
        updateEnrollmentCommand.Transaction = sqlTrans
        updateStudentLoaCommand.Transaction = sqlTrans
        insertStatusChangeCommand.Transaction = sqlTrans
        insertInProbationTableCommand.Transaction = sqlTrans
        Try
            updateEnrollmentCommand.ExecuteNonQuery()
            updateStudentLoaCommand.ExecuteNonQuery()
            Dim id As Guid = insertStatusChangeCommand.ExecuteScalar()
            insertInProbationTableCommand.Parameters.AddWithValue("@StudentStatusChangeId", id)
            insertInProbationTableCommand.ExecuteNonQuery()
            sqlTrans.Commit()
            Return String.Empty

        Catch ex As Exception

            sqlTrans.Rollback()
            Return ex.Message
        Finally

            sqlconn.Close()
        End Try

    End Function
    ''' <summary>
    ''' This function only execute the LOA status if the start date is today or in the past
    ''' If it is in the future, then only schedule the LOA, but does not change status 
    ''' Only put the LOA requirement in LOA Table. A JOB should be beginning and 
    ''' finish automatically the LOA status.
    ''' </summary>
    ''' <param name="change">
    '''  
    ''' </param>
    ''' <returns></returns>
    ''' <remarks>
    ''' </remarks>
    Public Function PlaceStudentOnLOA(change As StudentChangeHistoryObj)

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sqlTrans As SqlTransaction

        ' The LOA is a posteriori, then enter the changes directly
        If CDate(change.StartDate) <= Date.Now.ToShortDateString Then

            'Execute the LOA

            'Prepare Update Enrollment table
            Dim updateEnrollmentCommand As SqlCommand = PrepareUpdateEnrollmentCommandNoDateChanged(change)
            updateEnrollmentCommand.Connection = sqlconn

            'Prepare Update Student Status Change Table
            Dim insertStatusChangeCommand As SqlCommand = PrepareInsertStatusChangeCommand(change)
            insertStatusChangeCommand.Connection = sqlconn

            'Prepare Insert in the LOA  Table
            Dim insertLoaTableCommand As SqlCommand = PrepareInsertInLoaTable(change)
            insertLoaTableCommand.Connection = sqlconn

            'Execute the transaction
            sqlconn.Open()
            sqlTrans = sqlconn.BeginTransaction("ChangeStatus")
            updateEnrollmentCommand.Transaction = sqlTrans
            insertStatusChangeCommand.Transaction = sqlTrans
            insertLoaTableCommand.Transaction = sqlTrans

            Try
                updateEnrollmentCommand.ExecuteNonQuery()
                Dim id As Guid = insertStatusChangeCommand.ExecuteScalar()
                insertLoaTableCommand.Parameters.AddWithValue("@StudentStatusChangeId", id)
                insertLoaTableCommand.ExecuteNonQuery()
                sqlTrans.Commit()

                Dim attendanceDA As New AttendanceDA(myAdvAppSettings.AppSettings("ConnectionString").ToString)

                attendanceDA.ResetScheduleHours(change.StuEnrollId, change.StartDate, change.EndDate)



                Return String.Empty

            Catch ex As Exception

                sqlTrans.Rollback()
                Return ex.Message
            Finally

                sqlconn.Close()
            End Try

        Else
            ' The LOA is in the Future, then only schedule it. The JOB made this automatically
            'Prepare Insert in the LOA  Table
            Dim insertLoaTableCommand As SqlCommand = PrepareInsertInLoaTable(change)
            AddWithValueSafe(insertLoaTableCommand.Parameters, "@StudentStatusChangeId", Nothing)
            insertLoaTableCommand.Connection = sqlconn
            sqlconn.Open()
            Try
                insertLoaTableCommand.ExecuteNonQuery()
                Return String.Empty
            Catch ex As Exception
                Return ex.Message

            Finally

                sqlconn.Close()
            End Try

        End If



        ' US2135 01/05/2012 Janet Robinson added RequestDate
        '    With sb
        '        .Append("INSERT INTO arStudentLOAs(StuEnrollId,StartDate,EndDate,LOAReasonId,ModUser,ModDate,StudentStatusChangeId,StatusCodeId,LOARequestDate) ")
        '        .Append("VALUES(?,?,?,?,?,?,?,?,?)")
        '    End With
        '    db.AddParameter("@courseid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '    db.AddParameter("@code", StartDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '    db.AddParameter("@descrip", EndDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '    db.AddParameter("@hours", LOAReasonId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        '    'ModUser
        '    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '    'ModDate
        '    db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '    db.AddParameter("@StatusChangeId", StatusChangeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '    db.AddParameter("@StatusCodeId", Status, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '    db.AddParameter("@LOARequestDate", RequestDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '    db.RunParamSQLExecuteNoneQuery(sb.ToString)

        '    db.ClearParameters()
        '    sb.Remove(0, sb.Length)

        '    Return ""
        '    'Catch ex As System.Exception
        '    '    'Return an Error To Client
        '    '    Return -1
        'Catch ex As OleDbException

        '    Return DALExceptions.BuildErrorMessage(ex)

        '    'Close Connection
        '    db.CloseConnection()
        'End Try
    End Function


#End Region

#Region "Validate"

    ''' <summary>
    ''' Return true if the student has Attendance or grades posted
    ''' </summary>
    ''' <param name="enrollmentGuid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ValidateIfStudentEnrollmentHasAttendanceOrGrades(enrollmentGuid As String) As Boolean
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sb As New StringBuilder
        ' Update arStudentLOAs table
        With sb
            .Append(" DECLARE @Att INT = ( SELECT COUNT(*) ")
            .Append("                      FROM   dbo.syStudentAttendanceSummary ")
            .Append("                      WHERE  StuEnrollId = @StuEnrollId ")
            .Append("                    )")
            .Append(" DECLARE @Grade INT = ( SELECT   COUNT(*) ")
            .Append("                        FROM     arResults ")
            .Append("                        WHERE    StuEnrollId = @StuEnrollId ")
            .Append("                                 AND ( IsCourseCompleted = 1 ")
            .Append("                                       OR ( Score != 0")
            .Append("                                            AND Score IS NOT NULL ")
            .Append("                                          )")
            .Append("                                       OR GrdSysDetailId IS NOT NULL ")
            .Append("                                     ) ")
            .Append("                      )")
            .Append(" SELECT  @Att + @Grade;")

        End With

        Dim command As SqlCommand = New SqlCommand(sb.ToString(), sqlconn)
        command.CommandText = sb.ToString()
        command.CommandType = CommandType.Text
        command.Parameters.AddWithValue("@StuEnrollId", enrollmentGuid)
        sqlconn.Open()
        Try
            Dim result As Integer = command.ExecuteScalar()
            Return (result > 0)
        Finally
            sqlconn.Close()
        End Try
    End Function

#End Region

#Region "Consulting..."

    Private Function GetEndLoaDateForUnfinishedLOA(enrollmentGuid As String) As DateTime
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sb As New StringBuilder
        ' Update arStudentLOAs table
        With sb
            .Append("SELECT EndDate FROM  arStudentLOAs ")
            .Append("WHERE  StuEnrollId = @StuEnrollId AND LOAReturnDate is NULL ")
        End With

        Dim command As SqlCommand = New SqlCommand(sb.ToString(), sqlconn)
        command.CommandText = sb.ToString()
        command.CommandType = CommandType.Text
        command.Parameters.AddWithValue("@StuEnrollId", enrollmentGuid)
        sqlconn.Open()
        Try
            Dim result As DateTime = command.ExecuteScalar()
            Return result
        Finally
            sqlconn.Close()
        End Try
    End Function

    Private Function GetEndSuspensionDateForUnfinishedSuspension(ByVal suspensionId As String) As DateTime
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sb As New StringBuilder
        ' Update arStudentLOAs table
        With sb
            .Append("SELECT EndDate FROM  arStdSuspensions ")
            .Append("WHERE  StuSuspensionId = @StuSuspensionId")
        End With

        Dim command As SqlCommand = New SqlCommand(sb.ToString(), sqlconn)
        command.CommandText = sb.ToString()
        command.CommandType = CommandType.Text
        command.Parameters.AddWithValue("@StuSuspensionId", suspensionId)
        sqlconn.Open()
        Try
            Dim result As DateTime = command.ExecuteScalar()
            Return result
        Finally
            sqlconn.Close()
        End Try
    End Function


    ''' <summary>
    ''' Return all states id and description for inactive and active status
    ''' </summary>
    ''' <param name="campusId"></param>
    ''' <param name="userName"></param>
    ''' <returns></returns>
    ''' <remarks>Transfer Out and Graduated state are not include here. This is use only by Batch Status page.</remarks>
    Public Function GetActiveInactiveStatusesForBatchChanges(ByVal campusId As String, Optional ByVal userName As String = "") As DataSet
        Dim sb As New StringBuilder
        Dim ds As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Try
            '   build the sql query to get Advantage system statuses that can be used on batch changes:
            '       SysStatusId = 7     => Future Start
            '       SysStatusId = 9     => Currently Attending
            '       SysStatusId = 8     => No Start
            '       SysStatusId = 10    => Leave of Absence
            '       SysStatusId = 11    => Suspended
            '       SysStatusId = 12    => Dropped
            '       SysStatusId = 14    => Graduated
            '       SysStatusId = 22    => Extern ship
            ' If user belongs to role of Director of Academics , then show Academic Probation
            Dim boolIsUserDirectorofAcademics As Boolean
            boolIsUserDirectorofAcademics = (New LeadDB).IsDirectorofAcademic(userName, campusId)

            With sb
                .Append("SELECT ")
                .Append("       A.StatusCodeId,")
                .Append("       A.StatusCodeDescrip ")
                .Append("FROM   syStatusCodes A,syStatuses B,sySysStatus C ")
                .Append("WHERE  A.StatusId=B.StatusId ")
                '.Append("       AND B.Status='Active' ")
                .Append("       AND A.SysStatusId=C.SysStatusId ")
                .Append("       AND C.SysStatusId IN (7,8,9,10,11,12,14,22")
                If boolIsUserDirectorofAcademics = True Then
                    .Append(",20")
                End If
                .Append(") ")
                .Append("       AND A.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                .Append("ORDER BY A.StatusCodeDescrip")
            End With

            db.AddParameter("@campid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        '   Return dataset
        Return ds
    End Function

    ''' <summary>
    ''' Given a School State Id return the system Status enum related to the given status code
    ''' </summary>
    ''' <param name="statusCodeId">The school status code (Guid)</param>
    ''' <param name="campusid"></param>
    ''' <returns></returns>
    ''' <remarks>Because school select his propers status ID here we can get the related advantage status </remarks>
    Public Function GetAdvantageSystemStatus(ByVal statusCodeId As String, ByVal campusid As String) As StateEnum
        Dim status As Integer
        Dim enumType As StateEnum

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        Try
            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT SysStatusId ")
                .Append("FROM syStatusCodes ")
                .Append("WHERE StatusCodeId = ?  and CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?)")
            End With

            '   Add StatusCodeId to the parameter list
            db.AddParameter("@StatusCodeId", statusCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@campid", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                status = dr("SysStatusId")
            End While

            enumType = CType(status, StateEnum)

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        '   Return status
        Return enumType

    End Function

    ''' <summary>
    ''' Given a list of school status return the same list with the description 
    ''' the returned values are checked again the Advantage Status and campus group.
    ''' </summary>
    ''' <param name="sysStatusList"></param>
    ''' <param name="campusid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetStatuses(ByVal sysStatusList As String, ByVal campusid As String) As DataTable
        '   connect to the database
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   get lead statuses that map to 'New Lead' system status.
        '   build the sql query
        With sb
            .Append("SELECT ")
            .Append("       A.StatusCodeId, ")
            .Append("       A.StatusCodeDescrip ")
            .Append("FROM   syStatusCodes A, syStatuses B ")
            .Append("WHERE  A.sysStatusId IN (" & sysStatusList & ") ")
            .Append("       AND A.StatusId=B.StatusId ")
            .Append("       AND B.Status='Active'")
            .Append("       AND A.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("ORDER BY A.StatusCodeDescrip")
        End With
        db.AddParameter("@campid", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        '   return dataset
        Return ds.Tables(0)
    End Function

    Public Function GetStatuses(ByVal sysStatusList As String, ByVal campusid As String, ByVal statusCodeId As String) As DataTable
        '   connect to the database
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   get lead statuses that map to 'New Lead' system status.
        '   build the sql query
        With sb
            .Append("SELECT ")
            .Append("       A.StatusCodeId, ")
            .Append("       A.StatusCodeDescrip ")
            .Append("FROM   syStatusCodes A, syStatuses B ")
            .Append("WHERE  A.sysStatusId IN (" & sysStatusList & ") ")
            .Append("       AND A.StatusId=B.StatusId and A.StatusCodeId <> ? ")
            .Append("       AND B.Status='Active'")
            .Append("       AND A.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("ORDER BY A.StatusCodeDescrip")
        End With
        db.AddParameter("@stausCodeId", statusCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campid", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        '   return dataset
        Return ds.Tables(0)
    End Function


    Public Function GetSysStatus(ByVal statusCodeId As String) As Integer
        '   connect to the database
        Dim db As New DataAccess
        'Dim ds As DataSet

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT sysStatusId ")
            .Append("FROM   syStatusCodes ")
            .Append("WHERE  StatusCodeId = ?")
        End With

        db.AddParameter("@StatusCodeId", statusCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim sysStatusId As Integer = db.RunParamSQLScalar(sb.ToString)

        '   return integer
        Return sysStatusId
    End Function

    Public Function GetExternshipStatus() As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim statusCodeId As String

        Try

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

            '   connect to the database
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            statusCodeId = Nothing
            '   Build the sql query
            '   SysStatusId = 9     => Currently Attending
            With sb
                .Append("SELECT DISTINCT ")
                .Append("       A.StatusCodeID AS StatusCodeID,")
                .Append("       A.StatusCodeDescrip AS StatusCodeDescrip ")
                .Append("FROM   syStatusCodes A ")
                .Append("WHERE  A.sysStatusId = 22 ")
            End With

            db.OpenConnection()

            '   Execute the query
            Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString)

            While dr.Read()
                statusCodeId = dr("StatusCodeID").ToString
            End While

            If Not dr.IsClosed Then dr.Close()

        Catch ex As Exception
            Throw New BaseException(ex.Message)

        Finally
            '   Close Connection
            db.CloseConnection()
        End Try

        '   Return StatusCodeId
        Return statusCodeId

    End Function

    'Public Function GetExternshipStatusById(ByVal strStatusCodeID As String) As String
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim statusCodeId As String = ""

    '    Try

    '        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
    '        '   connect to the database
    '        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

    '        '   Build the sql query
    '        '   SysStatusId = 9     => Currently Attending
    '        With sb
    '            .Append("SELECT DISTINCT ")
    '            .Append("       A.StatusCodeID AS StatusCodeID,")
    '            .Append("       A.StatusCodeDescrip AS StatusCodeDescrip ")
    '            .Append("FROM   syStatusCodes A ")
    '            .Append("WHERE  A.sysStatusId = 22 ")
    '        End With

    '        db.OpenConnection()

    '        '   Execute the query
    '        Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString)

    '        While dr.Read()
    '            statusCodeId = dr("StatusCodeID").ToString
    '        End While

    '        If Not dr.IsClosed Then dr.Close()

    '    Catch ex As Exception
    '        Throw New BaseException(ex.Message)

    '    Finally
    '        '   Close Connection
    '        db.CloseConnection()
    '    End Try

    '    '   Return StatusCodeId
    '    Return statusCodeId

    'End Function

    Public Function GetGradStatus() As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim statusCodeId As String = String.Empty

        Try

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            '   connect to the database
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")


            '   Build the sql query
            '   SysStatusId = 14    => Graduated
            With sb
                .Append("SELECT DISTINCT ")
                .Append("       A.StatusCodeID AS StatusCodeID,")
                .Append("       A.StatusCodeDescrip AS StatusCodeDescrip ")
                .Append("FROM   syStatusCodes A ")
                .Append("WHERE  A.sysStatusId = 14 ")
            End With

            db.OpenConnection()

            '   Execute the query
            Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString)

            While dr.Read()
                statusCodeId = dr("StatusCodeID").ToString
            End While

            If Not dr.IsClosed Then dr.Close()

        Catch ex As Exception
            Throw New BaseException(ex.Message)

        Finally
            '   Close Connection
            db.CloseConnection()
        End Try

        '   Return StatusCodeId
        Return statusCodeId

    End Function

    ''' <summary>
    ''' Get the records based in the filters
    ''' Used only in batch status change page.
    ''' </summary>
    ''' <param name="prgVerId"></param>
    ''' <param name="startDate"></param>
    ''' <param name="campusId"></param>
    ''' <param name="lastname"></param>
    ''' <param name="firstname"></param>
    ''' <param name="ssn"></param>
    ''' <param name="cohortStartDate"></param>
    ''' <param name="userName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetStudentsDSbasedonNameandSSN(ByVal fromStatusCodeId As String, ByVal prgVerId As String,
                                         ByVal startDate As String, ByVal campusId As String, ByVal lastname As String, ByVal firstname As String, ByVal ssn As String, ByVal studentGroups As String, Optional ByVal cohortStartDate As String = "", Optional ByVal userName As String = "") As DataTable
        '   connect to the database
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sb As New StringBuilder

        '   build the sql query
        Dim boolIsUserDirectorofAcademics As Boolean
        boolIsUserDirectorofAcademics = (New LeadDB).IsDirectorofAcademic(userName)

        With sb
            .Append("SELECT   ").AppendLine()
            .Append("       E.StuEnrollId ").AppendLine()
            .Append("     , S.LastName    ").AppendLine()
            .Append("     , S.FirstName   ").AppendLine()
            .Append("     , S.SSN         ").AppendLine()
            .Append("     , E.PrgVerId    ").AppendLine()
            .Append("     , (SELECT PrgVerDescrip     FROM arPrgVersions WHERE PrgVerId = E.PrgVerId)         AS PrgVerDescrip     ").AppendLine()
            .Append("     , E.StatusCodeId ").AppendLine()
            .Append("     , (SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId = E.StatusCodeId) AS StatusCodeDescrip ").AppendLine()
            .Append("     , E.StartDate   ").AppendLine()
            .Append("     , E.ExpGradDate ").AppendLine()
            .Append("     , A.SysStatusId ").AppendLine()
            .Append("     , E.LDA         ").AppendLine()
            .Append("     , E.DateDetermined   ").AppendLine()
            .Append("     , LOA.StartDate                AS LOAStartDate   ").AppendLine()
            .Append("     , LOA.EndDate                  AS LOAEndDate     ").AppendLine()
            .Append("     , LOA.StudentStatusChangeId    AS PreviousLOAStudentStatusChangeId  ").AppendLine()
            .Append("     , LOA.OrigStatusId             AS PreviousStatusCodeIdToLOA         ").AppendLine()
            .Append("     , LOA.SysStatusId              AS LoaPreviousSysStatusId   ").AppendLine()
            .Append("     , SUS.StartDate                AS SusStartDate   ").AppendLine()
            .Append("     , SUS.EndDate                  AS SusEndDate     ").AppendLine()
            .Append("	  , SUS.StudentStatusChangeId    AS PreviousSusStudentStatusChangeId  ").AppendLine()
            .Append("     , SUS.OrigStatusId             AS PreviousStatusCodeIdToSus         ").AppendLine()
            .Append("     , SUS.SysStatusId              AS SusPreviousSysStatusId            ").AppendLine()
            .Append("     , dbo.EnrollmentHasAttendance(E.StuEnrollId) as HasAttendance       ").AppendLine()
            .Append("     , dbo.EnrollmentHasGrades(E.StuEnrollId) as HasGrades               ").AppendLine()
            .Append("     , (SELECT ProgramRegistrationType     FROM arPrgVersions WHERE PrgVerId = E.PrgVerId)         AS IsProgramRegistrationType               ").AppendLine()
            .Append("FROM arStuEnrollments           E ").AppendLine()
            .Append("    INNER JOIN arStudent        S   ON E.StudentId = S.StudentId         ").AppendLine()
            .Append("    INNER JOIN syStatusCodes    A   ON E.StatusCodeId = A.StatusCodeId   ").AppendLine()
            .Append("    INNER JOIN syStatuses       B   ON A.StatusId = B.StatusId           ").AppendLine()
            .Append("    INNER JOIN sySysStatus      C   ON A.SysStatusId = C.SysStatusId     ").AppendLine()
            .Append("    LEFT JOIN  (                  ").AppendLine()
            .Append("			        SELECT ASLA.StuEnrollId, ASLA.StartDate, ASLA.EndDate, ASLA.StudentStatusChangeId   ").AppendLine()
            .Append("                        , SSSC.OrigStatusId, SSSC.DateOfChange, SSSC.ModDate, SSC.SysStatusId          ").AppendLine()
            .Append("                        , RANK() OVER (PARTITION BY ASLA.StuEnrollId ORDER BY SSSC.DateOfChange DESC, SSSC.ModDate DESC ) AS N  ").AppendLine()
            .Append("			        FROM  arStudentLOAs  AS ASLA                                                                                 ").AppendLine()
            .Append("                        INNER JOIN syStudentStatusChanges AS SSSC ON SSSC.StudentStatusChangeId = ASLA.StudentStatusChangeId    ").AppendLine()
            .Append("                        INNER JOIN dbo.syStatusCodes AS SSC ON SSC.StatusCodeId = SSSC.NewStatusId                              ").AppendLine()
            .Append("		       ) AS LOA ON LOA.StuEnrollId = E.StuEnrollId AND LOA.N = 1 ").AppendLine()
            .Append("     LEFT JOIN  (                ").AppendLine()
            .Append("			        SELECT ASS.StuEnrollId,  ASS.StartDate,  ASS.EndDate,  ASS.StudentStatusChangeId    ").AppendLine()
            .Append("                        , SSSC.OrigStatusId, SSSC.DateOfChange, SSSC.ModDate, SSC.SysStatusId          ").AppendLine()
            .Append("                        , RANK() OVER (PARTITION BY ASS.StuEnrollId ORDER BY SSSC.DateOfChange DESC, SSSC.ModDate DESC ) AS N   ").AppendLine()
            .Append("			        FROM  dbo.arStdSuspensions AS ASS                                                                            ").AppendLine()
            .Append("                        INNER JOIN syStudentStatusChanges AS SSSC ON SSSC.StudentStatusChangeId = ASS.StudentStatusChangeId     ").AppendLine()
            .Append("                        INNER JOIN dbo.syStatusCodes AS SSC ON SSC.StatusCodeId = SSSC.NewStatusId                              ").AppendLine()
            .Append("   		   ) AS SUS ON SUS.StuEnrollId = E.StuEnrollId AND SUS.N = 1 ").AppendLine()
            If (Not String.IsNullOrEmpty(studentGroups)) Then
                .Append("    INNER JOIN  (                  ").AppendLine()
                .Append("			        SELECT DISTINCT lg.StuEnrollId  ").AppendLine()
                .Append("			        FROM  adLeadByLeadGroups  AS lg  WHERE lg.LeadGrpId in (" + studentGroups + ")     ").AppendLine()
                .Append("		       ) AS StudentGroups ON StudentGroups.StuEnrollId = E.StuEnrollId ").AppendLine()
            End If
            .Append("  ").AppendLine()
            .Append("WHERE C.SysStatusId IN (7, 8, 9, 10, 11, 12, 14, 19 ,22")
            If boolIsUserDirectorofAcademics = True Then
                .Append(", 20")
            End If
            .Append(" ) ").AppendLine()
            .Append("  AND B.Status = 'Active' ").AppendLine()
            .Append("  AND E.CampusId = @CampusId").AppendLine()
            If fromStatusCodeId <> "" Then
                .Append("  AND E.StatusCodeId = @StatusCodeId ").AppendLine()
            End If
            If prgVerId <> "" Then
                .Append("  AND E.PrgVerId = @PrgVerId ").AppendLine()
            End If
            If startDate <> "" Then
                .Append("  AND E.StartDate = @StartDate ").AppendLine()
            End If
            ''CohortStartDate added
            If cohortStartDate <> "" Then
                .Append("  AND E.CohortStartDate = @CohortStartDate ").AppendLine()
            End If

            If lastname <> "" Then
                .Append("  AND S.lastname like '%" + lastname + "%'  ").AppendLine()
            End If
            If firstname <> "" Then
                .Append("  AND S.Firstname like '%" + firstname + "%'  ").AppendLine()
            End If
            If ssn <> "" Then
                .Append("  AND S.SSN ='" + ssn + "'  ").AppendLine()
            End If
            .Append(" ORDER BY S.LastName, S.FirstName, PrgVerDescrip, E.StartDate, E.ExpGradDate ").AppendLine()
        End With

        '   Add parameters to the parameter list
        Dim command As SqlCommand = New SqlCommand(sb.ToString(), sqlconn)
        command.Parameters.AddWithValue("@CampusId", campusId)

        If Not String.IsNullOrEmpty(fromStatusCodeId) Then
            command.Parameters.AddWithValue("@StatusCodeId", fromStatusCodeId)
        End If
        If Not String.IsNullOrEmpty(prgVerId) Then
            command.Parameters.AddWithValue("@PrgVerId", prgVerId)
        End If
        If Not String.IsNullOrEmpty(startDate) Then
            command.Parameters.AddWithValue("@StartDate", startDate)
        End If
        ''Cohort StartDate added
        If Not String.IsNullOrEmpty(cohortStartDate) Then
            command.Parameters.AddWithValue("@CohortStartDate", cohortStartDate)
        End If
        Dim table As DataTable = New DataTable()
        sqlconn.Open()
        Try
            table.Load(command.ExecuteReader())
            Return table
        Finally
            sqlconn.Close()
        End Try
    End Function



    Public Function GetProgramAcademicCalendar(ByVal progVersionDescrip As String) As Integer
        Dim db As New DataAccess
        Dim response As Integer = -1

        ' connect to the database
        db.ConnectionString = GetConnectionString()
        Const sql As String = "SELECT ap.ACid FROM dbo.arPrgVersions pv JOIN dbo.arPrograms ap ON ap.ProgId = pv.ProgId WHERE pv.PrgVerDescrip = ?"

        'Clear Parameter
        db.ClearParameters()

        ' Add Parameters 
        db.AddParameter("@progVersionDescrip", progVersionDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Try
            db.OpenConnection()

            '   Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sql)

            While dr.Read()
                response = dr(0)
            End While
            If Not dr.IsClosed Then dr.Close()
        Finally
            '   Close Connection
            db.CloseConnection()
        End Try
        Return response
    End Function

    ''' <summary>
    ''' Get the required hours for program version.
    ''' </summary>
    ''' <param name="progVersionDescrip">String Program Version Description</param>
    ''' <returns>Decimal</returns>
    ''' <remarks></remarks>
    Public Function GetHoursRequiredForProgramVersion(ByVal progVersionDescrip As String) As Decimal
        Dim db As New DataAccess
        Dim result As Decimal

        ' connect to the database
        db.ConnectionString = GetConnectionString()
        Const sql As String = "SELECT TOP(1) Hours FROM dbo.arPrgVersions WHERE PrgVerDescrip = ?"
        'Clear Parameter
        db.ClearParameters()

        ' Add Parameters 
        db.AddParameter("@progVersionDescrip", progVersionDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Try
            db.OpenConnection()
            result = db.RunParamSQLScalar(sql) '   Execute the query
        Finally
            db.CloseConnection() '   Close Connection
        End Try
        Return result
    End Function

    Public Function GetStudentAttendanceForProgramVersion(ByVal stuEnrollmentId As String) As Decimal
        Dim db As New DataAccess
        Dim result As Decimal

        ' connect to the database
        db.ConnectionString = GetConnectionString()
        Const sql As String = "SELECT Actual FROM dbo.atClsSectAttendance WHERE Actual < 9999 AND StuEnrollId = ?"
        'Clear Parameter
        db.ClearParameters()

        ' Add Parameters 
        db.AddParameter("@progVersionDescrip", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Try
            db.OpenConnection()
            '   Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sql)

            While dr.Read()
                result += dr(0)
            End While
            If Not dr.IsClosed Then dr.Close()
        Finally
            db.CloseConnection() '   Close Connection
        End Try
        Return result
    End Function


    Private Function GetConnectionString() As String
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Return myAdvAppSettings.AppSettings("ConString")
    End Function

    Public Function GetEnrollmentTransferHours(ByVal stuEnrollId As String) As Decimal
        Dim db As New DataAccess
        Dim result As Decimal

        ' connect to the database
        db.ConnectionString = GetConnectionString()
        Const sql As String = "SELECT ISNULL(TransferHours,0) FROM dbo.arStuEnrollments WHERE StuEnrollId = ?"
        'Clear Parameter
        db.ClearParameters()

        ' Add Parameters 
        db.AddParameter("@stuenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Try
            db.OpenConnection()
            result = db.RunParamSQLScalar(sql) '   Execute the query
        Finally
            db.CloseConnection() '   Close Connection
        End Try
        Return result
    End Function


    Private Function GetLastSuspensionGuid(ByVal stuEnrollId As String) As Object

        Dim sb As StringBuilder = New StringBuilder()
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)


        ' Get The last suspension for the enrollment
        With sb
            .Append("SELECT TOP 1  StuSuspensionId  FROM arStdSuspensions ")
            .Append("WHERE      StuEnrollId= @StuEnrollId ORDER BY EndDate DESC ")
        End With

        Dim command As SqlCommand = New SqlCommand(sb.ToString(), sqlconn)
        command.CommandText = sb.ToString()
        command.CommandType = CommandType.Text
        command.Parameters.AddWithValue("@StuEnrollId", stuEnrollId)
        sqlconn.Open()
        Try
            Dim result = command.ExecuteScalar()
            Return result
        Finally
            sqlconn.Close()
        End Try
    End Function

    ''' <summary>
    ''' Return the actual school status Guid from the student
    ''' </summary>
    ''' <param name="stuEnrollId"></param>
    ''' <returns></returns>
    ''' <remarks>A exception is returned if the Enrollment does not exits</remarks>
    Public Function GetActualEnrollmentSchoolStatusGuid(ByVal stuEnrollId As String) As String

        Dim sb As StringBuilder = New StringBuilder()
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)


        ' Get The last suspension for the enrollment
        With sb
            .Append(" SELECT  StatusCodeId FROM dbo.arStuEnrollments ")
            .Append(" WHERE   StuEnrollId= @StuEnrollId ")
        End With

        Dim command As SqlCommand = New SqlCommand(sb.ToString(), sqlconn)
        command.CommandText = sb.ToString()
        command.CommandType = CommandType.Text
        command.Parameters.AddWithValue("@StuEnrollId", stuEnrollId)
        sqlconn.Open()
        Try
            Dim result = command.ExecuteScalar()
            Return result.ToString()
        Finally
            sqlconn.Close()
        End Try
    End Function

    ''' <summary>
    ''' Return the StuEnrollId based in the StudentId and program version Id.
    ''' </summary>
    ''' <param name="studentId"></param>
    ''' <param name="prgVerId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetActualEnrollmentByStudentIdAndProgramVersion(ByVal studentId As String, ByVal prgVerId As String) As String

        Dim sb As StringBuilder = New StringBuilder()
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)


        ' Get The last suspension for the enrollment
        With sb
            .Append(" Select Distinct StuEnrollId from arStuEnrollments enroll ")
            .Append(" inner join  syStatusCodes A on enroll.StatusCodeID= A.StatusCodeID ")
            .Append(" inner join syStatuses C on A.StatusID = C.StatusID ")
            .Append(" where C.Status = 'Active' ")
            .Append(" and StudentId=@StudentGuid and PrgVerId= @PrgVerGuid ")
        End With

        Dim command As SqlCommand = New SqlCommand(sb.ToString(), sqlconn)
        command.CommandText = sb.ToString()
        command.CommandType = CommandType.Text
        command.Parameters.AddWithValue("@StudentGuid", studentId)
        command.Parameters.AddWithValue("@PrgVerGuid", prgVerId)
        sqlconn.Open()
        Try
            Dim result = command.ExecuteScalar()
            Return result.ToString()
        Finally
            sqlconn.Close()
        End Try
    End Function
    ''' <summary>
    ''' Return the LeadId based on the StudentId.
    ''' </summary>
    ''' <param name="studentId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetLeadIdByStudentId(ByVal studentId As String) As String

        Dim sb As StringBuilder = New StringBuilder()
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)

        With sb
            .Append(" Select LeadId from adLeads leads ")
            .Append(" where StudentId=@StudentGuid")
        End With

        Dim command As SqlCommand = New SqlCommand(sb.ToString(), sqlconn)
        command.CommandText = sb.ToString()
        command.CommandType = CommandType.Text
        command.Parameters.AddWithValue("@StudentGuid", studentId)
        sqlconn.Open()
        Try
            Dim result = command.ExecuteScalar()
            Return result.ToString()
        Finally
            sqlconn.Close()
        End Try
    End Function
#End Region

#Region "Helpers for Queries"

    ''' <summary>
    ''' Prepare To Update Enrollment Table with the status
    ''' </summary>
    ''' <param name="change"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' 
    Private Function PrepareUpdateEnrollmentCommand(change As StudentChangeHistoryObj) As SqlCommand
        Dim command As SqlCommand = New SqlCommand()
        Dim sb As StringBuilder = New StringBuilder()
        '   Prepare arStuEnrollments table update SQL
        With sb
            .Append(" UPDATE     arStuEnrollments ")
            .Append(" SET        DateDetermined=  @DateDetermined, LDA = @Lda, DropReasonId = @DropReasonId,   StatusCodeId= @StatusCodeId ")
            .Append("           , ModDate=@ModDate, ModUser=@ModUser ,ReEnrollmentDate=@ReEnrollmentDate ")
            .Append(" WHERE      StuEnrollId=@StuEnrollId ")
        End With

        command.CommandType = CommandType.Text
        command.CommandText = sb.ToString()
        AddWithValueSafe(command.Parameters, "@DateDetermined", change.DateDetermined)
        AddWithValueSafe(command.Parameters, "@Lda", change.Lda)
        AddWithValueSafe(command.Parameters, "@DropReasonId", change.DropReasonGuid)
        AddWithValueSafe(command.Parameters, "@StatusCodeId", change.NewStatusGuid)
        AddWithValueSafe(command.Parameters, "@ModDate", change.ModDate)
        AddWithValueSafe(command.Parameters, "@ModUser", change.ModUser)
        AddWithValueSafe(command.Parameters, "@ReEnrollmentDate", change.DateOfReenrollment)
        AddWithValueSafe(command.Parameters, "@StuEnrollId", change.StuEnrollId)

        Return command

    End Function

    Private Function PrepareUpdateEnrollmentCurrentlyAttendingToFutureStartCommand(change As StudentChangeHistoryObj) As SqlCommand
        Dim command As SqlCommand = New SqlCommand()
        Dim sb As StringBuilder = New StringBuilder()
        '   Prepare arStuEnrollments table update SQL
        With sb
            .Append(" UPDATE     arStuEnrollments ")
            .Append(" SET        DateDetermined=  @DateDetermined, LDA = @Lda, StartDate = @StartDate,   StatusCodeId= @StatusCodeId ")
            .Append("           , ModDate=@ModDate, ModUser=@ModUser ")
            .Append(" WHERE      StuEnrollId=@StuEnrollId ")
        End With

        command.CommandType = CommandType.Text
        command.CommandText = sb.ToString()
        AddWithValueSafe(command.Parameters, "@DateDetermined", change.DateDetermined)
        AddWithValueSafe(command.Parameters, "@Lda", change.Lda)
        AddWithValueSafe(command.Parameters, "@StartDate", change.StartDate)
        AddWithValueSafe(command.Parameters, "@StatusCodeId", change.NewStatusGuid)
        AddWithValueSafe(command.Parameters, "@ModDate", change.ModDate)
        AddWithValueSafe(command.Parameters, "@ModUser", change.ModUser)
        AddWithValueSafe(command.Parameters, "@StuEnrollId", change.StuEnrollId)

        Return command

    End Function

    Private Function PrepareUpdateEnrollmentCommandNoDateChanged(change As StudentChangeHistoryObj) As SqlCommand
        Dim command As SqlCommand = New SqlCommand()
        Dim sb As StringBuilder = New StringBuilder()
        '   Prepare arStuEnrollments table update SQL
        With sb
            .Append(" UPDATE     arStuEnrollments ")
            .Append(" SET        StatusCodeId= @StatusCodeId, ModDate=@ModDate, ModUser=@ModUser  ")
            .Append(" WHERE      StuEnrollId=@StuEnrollId ")
        End With

        command.CommandText = sb.ToString()
        command.Parameters.AddWithValue("@StatusCodeId", change.NewStatusGuid)
        command.Parameters.AddWithValue("@ModDate", change.ModDate)
        command.Parameters.AddWithValue("@ModUser", change.ModUser)
        command.Parameters.AddWithValue("@StuEnrollId", change.StuEnrollId)

        Return command

    End Function

    Private Function PrepareUpdateEnrollmentNoStartCommand(change As StudentChangeHistoryObj) As SqlCommand



        Dim command As SqlCommand = New SqlCommand()
        Dim sb As StringBuilder = New StringBuilder()
        '   Prepare arStuEnrollments table update SQL
        With sb
            .Append(" UPDATE     arStuEnrollments ")
            .Append(" SET        DateDetermined=  @DateDetermined, LDA = @Lda, StatusCodeId= @StatusCodeId, ModDate=@ModDate, ModUser=@ModUser ")
            .Append(" WHERE      StuEnrollId=@StuEnrollId ")
        End With

        command.CommandType = CommandType.Text
        command.CommandText = sb.ToString()
        AddWithValueSafe(command.Parameters, "@DateDetermined", change.DateDetermined)
        AddWithValueSafe(command.Parameters, "@Lda", change.Lda)
        AddWithValueSafe(command.Parameters, "@StatusCodeId", change.NewStatusGuid)
        AddWithValueSafe(command.Parameters, "@ModDate", change.ModDate)
        AddWithValueSafe(command.Parameters, "@ModUser", change.ModUser)
        AddWithValueSafe(command.Parameters, "@StuEnrollId", change.StuEnrollId)

        Return command


    End Function

    Private Function PrepareUpdateEnrollmentCommandGradOrCurrent(ByVal change As StudentChangeHistoryObj) As SqlCommand
        Dim command As SqlCommand = New SqlCommand()
        Dim sb As StringBuilder = New StringBuilder()

        '   Prepare arStuEnrollments table update SQL
        With sb
            .Append("UPDATE     arStuEnrollments ")
            .Append("SET        StatusCodeId= @StatusCodeId,")
            .Append("           ExpGradDate= @ExpGradDate,")
            .Append("           ModDate=@ModDate, ModUser=@ModUser ")
            .Append("WHERE      StuEnrollId=@StuEnrollId ")
        End With

        command.CommandText = sb.ToString()
        command.Parameters.AddWithValue("@StatusCodeId", change.NewStatusGuid)
        command.Parameters.AddWithValue("@ExpGradDate", change.ExpGradDate)
        command.Parameters.AddWithValue("@ModDate", change.ModDate)
        command.Parameters.AddWithValue("@ModUser", change.ModUser)
        command.Parameters.AddWithValue("@StuEnrollId", change.StuEnrollId)

        Return command
    End Function

    Private Function PrepareUpdateStudentCommand(ByRef change As StudentChangeHistoryObj) As SqlCommand
        Dim command As SqlCommand = New SqlCommand()
        Dim sb As StringBuilder = New StringBuilder()
        Dim studentStatus As String

        studentStatus = GetStudentStatus(change.NewStatusGuid)
        'With sb
        '    .Append("UPDATE     arStudent ")
        '    .Append("SET        StudentStatus= @StudentStatus, ModDate=@ModDate, ModUser=@ModUser  ")
        '    .Append("WHERE      arStudent.StudentId = (SELECT StudentId FROM arStuEnrollments WHERE StuEnrollId= @StuEnrollId)")
        'End With
        With sb
            .Append("UPDATE     adLeads ")
            .Append("SET        StudentStatusId = @StudentStatus, ModDate=@ModDate, ModUser=@ModUser  ")
            .Append("WHERE      adLeads.StudentId = (SELECT StudentId FROM arStuEnrollments WHERE StuEnrollId= @StuEnrollId)")
        End With

        command.CommandText = sb.ToString()
        command.CommandType = CommandType.Text
        command.Parameters.AddWithValue("@StudentStatus", studentStatus)
        command.Parameters.AddWithValue("@ModDate", change.ModDate)
        command.Parameters.AddWithValue("@ModUser", change.ModUser)
        command.Parameters.AddWithValue("@StuEnrollId", change.StuEnrollId)

        Return command

        'Throw New NotImplementedException()

        ''   Update arStudent table
        'With sb
        '    .Append("UPDATE     arStudent ")
        '    .Append("SET        StudentStatus=?, ModDate=?, ModUser=?  ")
        '    .Append("WHERE      arStudent.StudentId = (SELECT StudentId FROM arStuEnrollments WHERE StuEnrollId=?)")
        'End With

        ''   StudentStatus
        'db.AddParameter("@StudentStatus", Common.AdvantageCommonValues.ActiveGuid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ''   ModDate
        'db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ''   ModUser
        'db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ''   StuEnrollId
        'db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    End Function

    Private Function GetStudentStatus(ByVal statusCodeId As String) As String
        Dim result As String
        Dim inSchool As Integer

        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT ISNULL(ss.InSchool,0) ")
            .Append("FROM systatuscodes sc, dbo.sySysStatus ss ")
            .Append("WHERE sc.SysStatusId=ss.SysStatusId ")
            .Append("AND sc.StatusCodeId = ? ")
        End With

        db.AddParameter("@StatusCodeId", statusCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        inSchool = db.RunParamSQLScalar(sb.ToString)

        db.ClearParameters()
        Dim sb2 As New StringBuilder

        If inSchool = 1 Then
            With sb2
                .Append("SELECT statusid FROM dbo.syStatuses WHERE Status='active'")
            End With
        Else
            With sb2
                .Append("SELECT statusid FROM dbo.syStatuses WHERE Status='inactive'")
            End With
        End If

        result = (db.RunParamSQLScalar(sb2.ToString)).ToString

        Return result

    End Function

    ''' <summary>
    ''' Query to analyze if the student should be passed to INACTIVE status in ArStudent
    ''' </summary>
    ''' <param name="change"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function PrepareInactivStudentIfAllEnrollmentAreDroppedCommand(ByRef change As StudentChangeHistoryObj) As SqlCommand
        Dim command As SqlCommand = New SqlCommand()
        Dim sb As StringBuilder = New StringBuilder()
        'With sb
        '    .Append(" UPDATE arStudent  SET StudentStatus= @StudentStatusId, ModDate =@ModDate, ModUser = @ModUser")
        '    .Append(" WHERE StudentId IN")
        '    .Append(" (SELECT  en.StudentId ")
        '    .Append("  FROM dbo.arStuEnrollments en")
        '    .Append("  WHERE en.StudentId = (SELECT StudentId FROM dbo.arStuEnrollments WHERE StuEnrollId = @StuEnrollId)")
        '    .Append("	  And en.StatusCodeId Not In  (Select StatusCodeId  ")
        '    .Append("                                  FROM syStatusCodes JOIN dbo.sySysStatus sy On sy.SysStatusId = syStatusCodes.SysStatusId")
        '    .Append("                                  WHERE sy.SysStatusId In (12,14))")
        '    .Append("     And en.StuEnrollId <> @StuEnrollId")
        '    .Append(") ")
        'End With
        With sb
            .Append(" UPDATE adLeads  Set StudentStatusId = @StudentStatusId, ModDate=@ModDate, ModUser= @ModUser")
            .Append(" WHERE StudentId In")
            .Append(" (Select  en.StudentId ")
            .Append("  FROM dbo.arStuEnrollments en")
            .Append("  WHERE en.StudentId = (Select StudentId FROM dbo.arStuEnrollments WHERE StuEnrollId = @StuEnrollId)")
            .Append("	  And en.StatusCodeId Not In  (Select StatusCodeId  ")
            .Append("                                  FROM syStatusCodes JOIN dbo.sySysStatus sy On sy.SysStatusId = syStatusCodes.SysStatusId")
            .Append("                                  WHERE sy.SysStatusId In (12,14))")
            .Append("     And en.StuEnrollId <> @StuEnrollId")
            .Append(") ")
        End With
        command.CommandText = sb.ToString()
        command.CommandType = CommandType.Text
        command.Parameters.AddWithValue("@StudentStatusId", AdvantageCommonValues.InactiveGuid)
        command.Parameters.AddWithValue("@ModDate", change.ModDate)
        command.Parameters.AddWithValue("@ModUser", change.ModUser)
        command.Parameters.AddWithValue("@StuEnrollId", change.StuEnrollId)

        Return command
    End Function

    Private Function PrepareInsertStatusChangeCommand(ByRef change As StudentChangeHistoryObj) As SqlCommand
        Dim command As SqlCommand = New SqlCommand()
        Dim sb As StringBuilder = New StringBuilder()
        'To implement active triggers and avoid error with trigger  The target table '<Table Name>' of the DML statement cannot have any enabled triggers if the statement contains an OUTPUT clause without INTO clause.
        'we declare a memory table variable to support StudentStatusChangeID and use it with INTO Clausula"
        sb.Append(" DECLARE @OutputTable TABLE ( StudentStatusChangeIdInserted  UNIQUEIDENTIFIER ) ")
        sb.Append(" INSERT INTO syStudentStatusChanges  (StudentStatusChangeId, StuEnrollId, OrigStatusId, NewStatusId, CampusId, ModDate, ModUser, IsReversal, DropReasonId, DateOfChange) ")
        sb.Append(" OUTPUT INSERTED.StudentStatusChangeId INTO @OutputTable ")
        sb.Append(" VALUES(NewId(), @StuEnrollId,@OrigStatusId,@NewStatusId,@CampusId,@ModDate,@ModUser,@IsRevelsal,@DropReasonId,@DateOfChange); ")
        sb.Append(" SELECT TOP 1 StudentStatusChangeIdInserted FROM @OutputTable  ")
        command.CommandText = sb.ToString()
        command.CommandType = CommandType.Text
        command.Parameters.AddWithValue("@StuEnrollId", change.StuEnrollId)
        AddWithValueSafe(command.Parameters, "@OrigStatusId", change.OrigStatusGuid)
        command.Parameters.AddWithValue("@NewStatusId", change.NewStatusGuid)
        command.Parameters.AddWithValue("@CampusId", change.CampusId)
        command.Parameters.AddWithValue("@ModDate", DateTime.Now)
        AddWithValueSafe(command.Parameters, "@ModUser", change.ModUser)
        command.Parameters.AddWithValue("@IsRevelsal", change.IsRevelsal)
        AddWithValueSafe(command.Parameters, "@DropReasonId", change.DropReasonGuid)
        command.Parameters.AddWithValue("@DateOfChange", change.DateOfChange)
        Return command
    End Function

    ''' <summary>
    ''' Update the StudentLOA table with the status changed.
    ''' </summary>
    ''' <param name="change"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function PrepareUpdateStudentLoaCommand(ByVal change As StudentChangeHistoryObj) As SqlCommand

        Dim enddate As DateTime = GetEndLoaDateForUnfinishedLOA(change.StuEnrollId)

        Dim command As SqlCommand = New SqlCommand()
        Dim sb As StringBuilder = New StringBuilder()

        ' Update arStudentLOAs table
        With sb
            .Append("UPDATE     arStudentLOAs ")
            .Append("SET ")
            If enddate > change.DateOfChange Then
                .Append("       LOAReturnDate = @DateOfChange, EndDate = @DateOfChange, ")
            Else
                .Append("       LOAReturnDate = @DateOfChange, ")
            End If

            .Append("       ModDate = @ModDate, ModUser = @ModUser  ")
            .Append("WHERE  StuEnrollId = @StuEnrollId AND LOAReturnDate is NULL ")
        End With

        command.CommandText = sb.ToString()
        command.CommandType = CommandType.Text
        command.Parameters.AddWithValue("@StuEnrollId", change.StuEnrollId.ToString())
        command.Parameters.AddWithValue("@ModDate", DateTime.Now)
        command.Parameters.AddWithValue("@ModUser", change.ModUser)
        command.Parameters.AddWithValue("@DateOfChange", change.DateOfChange)
        Return command

    End Function

    ''' <summary>
    ''' Prepare to update the Student Suspension Table
    ''' </summary>
    ''' <param name="change"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function PrepareUpdateStudentSuspensionCommand(ByVal change As StudentChangeHistoryObj) As SqlCommand
        Dim command As SqlCommand = New SqlCommand()
        Dim sb As StringBuilder = New StringBuilder()

        'You must to get first the last suspension of the enrollment in the table.
        Dim suspensionId = GetLastSuspensionGuid(change.StuEnrollId)
        Dim endDate = GetEndSuspensionDateForUnfinishedSuspension(suspensionId.ToString())

        ' Update arStdSuspensions table
        With sb
            .Append("UPDATE     arStdSuspensions ")
            If endDate > change.DateOfChange Then
                .Append("SET        EndDate= @DateOfChange, ModDate= @ModDate, ModUser=@ModUser  ")
            Else
                .Append("SET        ModDate= @ModDate, ModUser=@ModUser  ")
            End If
            .Append("WHERE      StuSuspensionID=@StuSuspensionID ")
        End With

        command.CommandText = sb.ToString()
        command.CommandType = CommandType.Text
        command.Parameters.AddWithValue("@StuSuspensionID", suspensionId)
        command.Parameters.AddWithValue("@ModDate", DateTime.Now)
        command.Parameters.AddWithValue("@ModUser", change.ModUser)
        command.Parameters.AddWithValue("@DateOfChange", change.DateOfChange)
        Return command
    End Function


    ''' <summary>
    ''' Prepare to insert in Suspension Table. Because this table depend of the value inserted in
    ''' Student Change Status history. The Last parameter should be supplied inside the query.
    ''' </summary>
    ''' <param name="change"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function PrepareInsertInSuspensionTable(ByVal change As StudentChangeHistoryObj) As SqlCommand
        Dim command As SqlCommand = New SqlCommand()
        Dim sb As StringBuilder = New StringBuilder()

        With sb
            .Append("INSERT INTO arStdSuspensions(StuSuspensionId,StuEnrollId,StartDate,EndDate,Reason,ModUser,ModDate,StudentStatusChangeId) ")
            .Append("VALUES(@StuSuspensionId,@StuEnrollId,@StartDate,@EndDate,@Reason,@ModUser,@ModDate,@StudentStatusChangeId)")

        End With

        command.CommandText = sb.ToString()
        command.CommandType = CommandType.Text
        command.Parameters.AddWithValue("@StuSuspensionId", Guid.NewGuid.ToString)
        command.Parameters.AddWithValue("@StuEnrollId", change.StuEnrollId.ToString())
        command.Parameters.AddWithValue("@ModDate", DateTime.Now)
        command.Parameters.AddWithValue("@ModUser", change.ModUser)
        AddWithValueSafe(command.Parameters, "@StartDate", change.StartDate)
        AddWithValueSafe(command.Parameters, "@EndDate", change.EndDate)
        AddWithValueSafe(command.Parameters, "@Reason", change.Reason)
        'StatusChangeId is dynamic and calculate during the transaction
        Return command
    End Function

    Private Function PrepareInsertInLoaTable(ByVal change As StudentChangeHistoryObj) As SqlCommand
        Dim command As SqlCommand = New SqlCommand()
        Dim sb As StringBuilder = New StringBuilder()

        With sb
            .Append(" INSERT INTO arStudentLOAs(StuEnrollId,StartDate,EndDate,LOAReasonId,ModUser,ModDate,StudentStatusChangeId,StatusCodeId,LOARequestDate) ")
            .Append(" VALUES(@StuEnrollId,@StartDate,@EndDate,@LOAReasonId,@ModUser,@ModDate,@StudentStatusChangeId,@StatusCodeId,@LOARequestDate) ")
        End With
        command.CommandText = sb.ToString()
        command.CommandType = CommandType.Text
        command.Parameters.AddWithValue("@StuEnrollId", change.StuEnrollId.ToString())
        AddWithValueSafe(command.Parameters, "@StartDate", change.StartDate)
        AddWithValueSafe(command.Parameters, "@EndDate", change.EndDate)
        AddWithValueSafe(command.Parameters, "@LOAReasonId", change.LoaReasonGuid)
        command.Parameters.AddWithValue("@ModDate", DateTime.Now)
        command.Parameters.AddWithValue("@ModUser", change.ModUser)
        command.Parameters.AddWithValue("@StatusCodeId", change.NewStatusGuid)
        AddWithValueSafe(command.Parameters, "@LOARequestDate", change.LOARequestDate)


        'StudentStatusChangeId is dynamic and calculate during the transaction
        Return command

    End Function

    Private Function PrepareInsertInProbationTable(ByVal change As StudentChangeHistoryObj) As SqlCommand
        Dim command As SqlCommand = New SqlCommand()
        Dim sb As StringBuilder = New StringBuilder()

        With sb
            .Append("INSERT INTO arStuProbWarnings(StuProbWarningId,StuEnrollId,ProbWarningTypeId,StartDate,EndDate,Reason,ModUser,ModDate,StudentStatusChangeId) ")
            .Append("VALUES(@StuProbWarningId,@StuEnrollId,@ProbWarningTypeId,@StartDate,@EndDate,@Reason,@ModUser,@ModDate,@StudentStatusChangeId)")
        End With


        command.CommandText = sb.ToString()
        command.CommandType = CommandType.Text
        command.Parameters.AddWithValue("@StuProbWarningId", Guid.NewGuid)
        command.Parameters.AddWithValue("@StuEnrollId", change.StuEnrollId)
        command.Parameters.AddWithValue("@ProbWarningTypeId", change.ProbWarningTypeId)
        AddWithValueSafe(command.Parameters, "@StartDate", change.StartDate)
        AddWithValueSafe(command.Parameters, "@EndDate", change.EndDate)
        AddWithValueSafe(command.Parameters, "@Reason", change.Reason)
        command.Parameters.AddWithValue("@ModDate", DateTime.Now)
        command.Parameters.AddWithValue("@ModUser", change.ModUser)

        'StudentStatusChangeId is dynamic and calculate during the transaction
        Return command

    End Function

    Private Function PrepareUpdateSapResultsTable(ByVal change As StudentChangeHistoryObj) As SqlCommand
        Dim command As SqlCommand = New SqlCommand()
        Dim sb As StringBuilder = New StringBuilder()

        With sb
            .Append("Update arSAPChkResults set PreviewSAPCheck = 0, DatePerformed= @DatePerformed ")
            .Append(" WHERE stuEnrollid = @StuEnrollId and Period = @SapPeriod ")
        End With


        command.CommandText = sb.ToString()
        command.CommandType = CommandType.Text
        command.Parameters.AddWithValue("@DatePerformed", change.DateOfChange)
        command.Parameters.AddWithValue("@StuEnrollId", change.StuEnrollId)
        command.Parameters.AddWithValue("@SapPeriod", change.SapPeriod)

        'StudentStatusChangeId is dynamic and calculate during the transaction
        Return command

    End Function

    Private Function PrepareInsertRepitedRecordInProbationTable(ByVal change As StudentChangeHistoryObj) As SqlCommand

        Dim command As SqlCommand = New SqlCommand()
        Dim sb As StringBuilder = New StringBuilder()

        With sb
            .Append("INSERT INTO dbo.arStuProbWarnings ")
            .Append("     ( StuProbWarningId,  StuEnrollId, StartDate, EndDate, ProbWarningTypeId, Reason, ModDate,  ModUser,  StudentStatusChangeId ) ")
            .Append("SELECT @StuProbWarningId, StuEnrollId, StartDate, EndDate, ProbWarningTypeId, Reason, @ModDate, @ModUser, @StudentStatusChangeId ")
            .Append("FROM dbo.arStuProbWarnings AS ASPW ")
            .Append("WHERE ASPW.StudentStatusChangeId = @previousStudentStatusChangeId ")
        End With

        command.CommandText = sb.ToString()
        command.CommandType = CommandType.Text
        command.Parameters.AddWithValue("@StuProbWarningId", Guid.NewGuid)
        command.Parameters.AddWithValue("@ModDate", DateTime.Now)
        command.Parameters.AddWithValue("@ModUser", change.ModUser)
        command.Parameters.AddWithValue("@previousStudentStatusChangeId", change.PreviousStudentStatusChangeId)
        'StudentStatusChangeId is dynamic and calculate during the transaction
        Return command

    End Function


    ''' <summary>
    ''' Test the Query if some value is null to pass it to DB Null
    ''' </summary>
    ''' <param name="parameters"></param>
    ''' <param name="parameterName"></param>
    ''' <param name="value"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Sub AddWithValueSafe(ByVal parameters As SqlParameterCollection, ByVal parameterName As String, ByVal value As Object)

        parameters.AddWithValue(parameterName, If(value Is Nothing, DBNull.Value, value))
        Return
    End Sub


    ''' <summary>
    ''' Given the sysysStatus (Advantage system status id) get the school associated Guid status. 
    ''' </summary>
    ''' <param name="sysStatusId">Integer with the sySysStatus</param>
    ''' <param name="campusid"></param>
    ''' <returns>The Guid of the school status as String</returns>
    ''' <remarks></remarks>
    Public Function GetSchoolDefinedStatusGuid(ByVal sysStatusId As Integer, ByVal campusid As String) As String
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sb As New StringBuilder

        '   get lead statuses that map to 'New Lead' system status.
        '   build the sql query
        With sb
            .Append("SELECT ")
            .Append("       A.StatusCodeId ")
            .Append(" FROM   syStatusCodes A, syStatuses B ")
            .Append(" WHERE  A.sysStatusId = @SysStatusId ")
            .Append("       AND A.StatusId=B.StatusId ")
            .Append("       AND B.Status='Active'")
            .Append("       AND A.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = @CampusId) ")
        End With

        Dim command As SqlCommand = New SqlCommand(sb.ToString(), sqlconn)

        command.CommandType = CommandType.Text
        command.Parameters.AddWithValue("@SysStatusId", sysStatusId)
        command.Parameters.AddWithValue("@CampusId", campusid)
        sqlconn.Open()
        Try
            'Execute the query
            Dim result = command.ExecuteScalar()
            Dim cleanresult = If(result Is DBNull.Value OrElse result Is Nothing, StatusNotDefined(sysStatusId), result.ToString())
            Return cleanresult

        Finally
            sqlconn.Close()
        End Try
    End Function

    ''' <summary>
    ''' Return the Description of the sysStatusId
    ''' </summary>
    ''' <param name="sysStatusId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetAdvantageStatusName(ByVal sysStatusId) As String
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sb As New StringBuilder

        '   get lead statuses that map to 'New Lead' system status.
        '   build the sql query
        With sb
            .Append("SELECT SysStatusDescrip FROM sySysStatus  ")
            .Append(" JOIN dbo.syStatuses ON syStatuses.StatusId = sySysStatus.StatusId ")
            .Append(" WHERE StatusCode = 'A' ")
            .Append(" AND SysStatusId = @SysStatusId ")
        End With

        Dim command As SqlCommand = New SqlCommand(sb.ToString(), sqlconn)
        command.CommandType = CommandType.Text
        command.Parameters.AddWithValue("@SysStatusId", sysStatusId)
        sqlconn.Open()
        Try
            'Execute the query
            Dim result = command.ExecuteScalar()
            Return result
        Finally
            sqlconn.Close()
        End Try
    End Function

    ''' <summary>
    ''' Used to return the not defined status by school as exception. This always produce a exception.
    ''' </summary>
    ''' <param name="sysStatusId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function StatusNotDefined(ByVal sysStatusId As Integer) As String

        Dim description = GetAdvantageStatusName(sysStatusId)
        If String.IsNullOrEmpty(description) Then
            Throw New ArgumentOutOfRangeException("sysStatusId", String.Format("{0} does not correspond to  a defined Advantage Student Status", sysStatusId))
        End If

        Throw New NotSupportedException(String.Format("The School has not defined a Custom Status for {0} ", description))

    End Function

    Private Function GetDeleteScheduleCoursesCommand(change As StudentChangeHistoryObj) As SqlCommand
        Dim command As SqlCommand = New SqlCommand()
        Dim sb As StringBuilder = New StringBuilder()
        '   Prepare arStuEnrollments table update SQL
        With sb
            .Append("DELETE  results  ")
            .Append("FROM arResults results  ")
            .Append("INNER JOIN arClassSections class on class.ClsSectionId = results.TestId  ")
            .Append("WHERE class.StartDate > GETDATE() and results.StuEnrollId = @StuEnrollId  ")
        End With

        command.CommandType = CommandType.Text
        command.CommandText = sb.ToString()

        AddWithValueSafe(command.Parameters, "@StuEnrollId", change.StuEnrollId)

        Return command

    End Function

    Public Function InsertArResults(ByVal StuEnrollId As String, ByVal ModUser As String) As Boolean

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim connString As String = MyAdvAppSettings.AppSettings("ConnectionString")
        Dim conn As New SqlConnection(connString)

        Try
            Dim cmd1 As SqlCommand = New SqlCommand("USP_RE_REGISTER_STUDENT_RESULTS", conn)
            cmd1.CommandType = CommandType.StoredProcedure
            cmd1.Parameters.Add("@StuEnrollId", SqlDbType.UniqueIdentifier, ParameterDirection.Input).Value = New Guid(StuEnrollId)
            cmd1.Parameters.Add("@ModUser", SqlDbType.VarChar, 50, ParameterDirection.Input).Value = ModUser

            conn.Open()
            cmd1.ExecuteNonQuery()

        Catch ex As Exception
            conn.Close()
            Return False
        Finally
            conn.Close()
        End Try
        Return True

    End Function

#End Region


End Class
