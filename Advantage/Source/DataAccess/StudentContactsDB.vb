Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' StudentContactsDB.vb
'
' StudentContactsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class StudentContactsDB
    Public Function GetAllStudentContacts(ByVal studentId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        ''   instantiate names and values for the relationShip
        'Dim names() As String = System.Enum.GetNames(GetType(Relationship))
        'Dim values() As Integer = System.Enum.GetValues(GetType(Relationship))

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         SC.StudentContactId, ")
            .Append("         SC.StatusId, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            '.Append("         (Case SC.Relationship ")
            'For i As Integer = 0 To names.Length - 1
            '    .Append("   when " + values(i).ToString + " then '" + names(i) + "' ")
            'Next
            '.Append("         end ) as RelationShip, ")
            .Append("          SC.RelationId, ")
            .Append("          (Select RelationDescrip from syRelations where RelationId=SC.RelationId) As Relation, ")
            .Append("         (SC.LastName + ', ' + SC.FirstName) As ContactName ")
            '.Append("         SC.StudentId, ")
            '.Append("         SC.RelationShip, ")
            '.Append("         SC.FirstName, ")
            '.Append("         SC.MI, ")
            '.Append("         SC.LastName, ")
            '.Append("         SC.PrefixId, ")
            '.Append("         SC.SuffixId, ")
            '.Append("         SC.Comments, ")
            '.Append("         SC.Email ")
            .Append("FROM     syStudentContacts SC, syStatuses ST ")
            .Append("WHERE    SC.StatusId = ST.StatusId ")
            .Append("AND      SC.StudentId = ? ")
        End With

        ' Add StudentId to the parameter list
        db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetStudentContactInfo(ByVal studentContactId As String) As StudentContactInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("         SC.StudentContactId, ")
            .Append("         SC.StatusId, ")
            .Append("         SC.StudentId, ")
            .Append("         SC.RelationId, ")
            .Append("         (Select RelationDescrip from syRelations where RelationId=SC.RelationId) As Relation, ")
            .Append("         SC.FirstName, ")
            .Append("         SC.MI, ")
            .Append("         SC.LastName, ")
            .Append("         SC.PrefixId, ")
            .Append("         (Select PrefixDescrip from syPrefixes where PrefixId=SC.PrefixId) As Prefix, ")
            .Append("         SC.SuffixId, ")
            .Append("         (Select SuffixDescrip from sySuffixes where SuffixId=SC.SuffixId) As Suffix, ")
            .Append("         SC.Comments, ")
            .Append("         SC.Email, ")
            .Append("         SC.ModUser, ")
            .Append("         SC.ModDate ")
            .Append("FROM  syStudentContacts SC ")
            .Append("WHERE SC.StudentContactId = ? ")
        End With

        ' Add the StudentContactId to the parameter list
        db.AddParameter("@StudentContactId", studentContactId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim StudentContactInfo As New StudentContactInfo

        While dr.Read()

            '   set properties with data from DataReader
            With StudentContactInfo
                .IsInDB = True
                .StudentContactId = studentContactId
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .StudentId = CType(dr("StudentId"), Guid).ToString
                .RelationId = CType(dr("RelationId"), Guid).ToString
                .Relation = dr("Relation")
                .FirstName = dr("FirstName")
                If Not (dr("MI") Is System.DBNull.Value) Then .MI = dr("MI")
                .LastName = dr("LastName")
                If Not (dr("PrefixId") Is System.DBNull.Value) Then .PrefixId = CType(dr("PrefixId"), Guid).ToString
                If Not (dr("Prefix") Is System.DBNull.Value) Then .Prefix = dr("Prefix")
                If Not (dr("SuffixId") Is System.DBNull.Value) Then .SuffixId = CType(dr("SuffixId"), Guid).ToString
                If Not (dr("Suffix") Is System.DBNull.Value) Then .Suffix = dr("Suffix")
                If Not (dr("Comments") Is System.DBNull.Value) Then .Comments = dr("Comments")
                If Not (dr("Email") Is System.DBNull.Value) Then .Email = dr("Email")
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return StudentContactInfo
        Return StudentContactInfo

    End Function
    Public Function UpdateStudentContactInfo(ByVal StudentContactInfo As StudentContactInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syStudentContacts ")
                .Append("   Set StudentContactId = ?, StatusId = ?, StudentId =?, ")
                .Append("       RelationId = ?,  FirstName = ?, MI = ?, ")
                .Append("       LastName = ?, PrefixId = ?, SuffixId = ?,")
                .Append("       Comments = ?, Email = ?, ")
                .Append("       ModUser = ?, ModDate = ? ")
                .Append("WHERE StudentContactId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from syStudentContacts where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   StudentContactId
            db.AddParameter("@StudentContactId", StudentContactInfo.StudentContactId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", StudentContactInfo.StatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   StudentId
            db.AddParameter("@StudentId", StudentContactInfo.StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   RelationShip
            db.AddParameter("@RelationId", StudentContactInfo.RelationId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   FirstName
            db.AddParameter("@FirstName", StudentContactInfo.FirstName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   MI
            If StudentContactInfo.MI = "" Then
                db.AddParameter("@MI", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@MI", StudentContactInfo.MI, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   LastName
            db.AddParameter("@LastName", StudentContactInfo.LastName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   PrefixId
            If StudentContactInfo.PrefixId = Guid.Empty.ToString Then
                db.AddParameter("@PrefixId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PrefixId", StudentContactInfo.PrefixId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   SuffixId
            If StudentContactInfo.SuffixId = Guid.Empty.ToString Then
                db.AddParameter("@SuffixId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@SuffixId", StudentContactInfo.SuffixId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Comments
            If StudentContactInfo.Comments = "" Then
                db.AddParameter("@Comments", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Comments", StudentContactInfo.Comments, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Email
            If StudentContactInfo.Email = "" Then
                db.AddParameter("@Email", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Email", StudentContactInfo.Email, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   StudentContactId
            db.AddParameter("@StudentContactId", StudentContactInfo.StudentContactId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", StudentContactInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddStudentContactInfo(ByVal StudentContactInfo As StudentContactInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT syStudentContacts ( ")
                .Append("       StudentContactId, StatusId, StudentId, ")
                .Append("       RelationId, FirstName, MI, ")
                .Append("       LastName, PrefixId, SuffixId,")
                .Append("       Comments, Email, ")
                .Append("       ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   StudentContactId
            db.AddParameter("@StudentContactId", StudentContactInfo.StudentContactId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", StudentContactInfo.StatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   StudentId
            db.AddParameter("@StudentId", StudentContactInfo.StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   RelationShip
            db.AddParameter("@RelationId", StudentContactInfo.RelationId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   FirstName
            db.AddParameter("@FirstName", StudentContactInfo.FirstName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   MI
            If StudentContactInfo.MI = "" Then
                db.AddParameter("@MI", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@MI", StudentContactInfo.MI, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   LastName
            db.AddParameter("@LastName", StudentContactInfo.LastName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   PrefixId
            If StudentContactInfo.PrefixId = Guid.Empty.ToString Then
                db.AddParameter("@PrefixId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PrefixId", StudentContactInfo.PrefixId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   SuffixId
            If StudentContactInfo.SuffixId = Guid.Empty.ToString Then
                db.AddParameter("@SuffixId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@SuffixId", StudentContactInfo.SuffixId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Comments
            If StudentContactInfo.Comments = "" Then
                db.AddParameter("@Comments", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Comments", StudentContactInfo.Comments, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Email
            If StudentContactInfo.Email = "" Then
                db.AddParameter("@Email", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Email", StudentContactInfo.Email, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteStudentContactInfo(ByVal StudentContactId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syStudentContacts ")
                .Append("WHERE StudentContactId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM syStudentContacts WHERE StudentContactId = ? ")
            End With

            '   add parameters values to the query

            '   StudentContactId
            db.AddParameter("@StudentContactId", StudentContactId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   StudentContactId
            db.AddParameter("@StudentContactId", StudentContactId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetAllStudentContactAddresses(ByVal studentContactId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         SCA.StudentContactAddressId, ")
            .Append("         SCA.StatusId, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("         SCA.Address1 ")
            '.Append("         SCA.StudentId, ")
            '.Append("         SCA.AddrTypId, ")
            '.Append("         SCA.Address1, ")
            '.Append("         SCA.Address2, ")
            '.Append("         SCA.City, ")
            '.Append("         SCA.StateId, ")
            '.Append("         SCA.CountryId, ")
            '.Append("         SCA.Zip ")
            .Append("FROM     syStudentContactAddresses SCA, syStatuses ST ")
            .Append("WHERE    SCA.StatusId = ST.StatusId ")
            .Append("AND      SCA.StudentContactId = ? ")
        End With

        ' Add StudentContactId to the parameter list
        db.AddParameter("@StudentContactId", studentContactId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetStudentContactAddressInfo(ByVal StudentContactAddressId As String) As StudentContactAddressInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("         SCA.StudentContactAddressId, ")
            .Append("         SCA.StatusId, ")
            .Append("         SCA.StudentContactId, ")
            .Append("         SCA.AddrTypId, ")
            .Append("         SCA.Address1, ")
            .Append("         SCA.Address2, ")
            .Append("         SCA.City, ")
            .Append("         SCA.StateId, ")
            .Append("         SCA.CountryId, ")
            .Append("         SCA.Zip, ")
            .Append("         SCA.OtherState, ")
            .Append("         SCA.ForeignZip, ")
            .Append("         SCA.ModUser, ")
            .Append("         SCA.ModDate ")
            .Append("FROM  syStudentContactAddresses SCA ")
            .Append("WHERE SCA.StudentContactAddressId = ? ")
        End With

        ' Add the StudentContactAddressId to the parameter list
        db.AddParameter("@StudentContactAddressId", StudentContactAddressId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim StudentContactAddressInfo As New StudentContactAddressInfo

        While dr.Read()

            '   set properties with data from DataReader
            With StudentContactAddressInfo
                .IsInDB = True
                .StudentContactAddressId = StudentContactAddressId
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .StudentContactId = CType(dr("StudentContactId"), Guid).ToString
                If Not (dr("AddrTypId") Is System.DBNull.Value) Then .AddrTypId = CType(dr("AddrTypId"), Guid).ToString
                .Address1 = dr("Address1")
                If Not (dr("Address2") Is System.DBNull.Value) Then .Address2 = dr("Address2")
                .City = dr("City")
                If Not (dr("StateId") Is System.DBNull.Value) Then .StateId = CType(dr("StateId"), Guid).ToString
                If Not (dr("CountryId") Is System.DBNull.Value) Then .CountryId = CType(dr("CountryId"), Guid).ToString
                If Not (dr("Zip") Is System.DBNull.Value) Then .Zip = dr("Zip")
                If Not (dr("OtherState") Is System.DBNull.Value) Then .OtherState = dr("OtherState")
                .ForeignZip = dr("ForeignZip")
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return StudentContactAddressInfo
        Return StudentContactAddressInfo

    End Function
    Public Function UpdateStudentContactAddressInfo(ByVal StudentContactAddressInfo As StudentContactAddressInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syStudentContactAddresses ")
                .Append("   Set StudentContactAddressId = ?, StatusId = ?, StudentContactId =?, ")
                .Append("       AddrTypId = ?,  Address1 = ?, Address2 = ?, ")
                .Append("       City = ?, StateId = ?, CountryId = ?, ")
                .Append("       Zip = ?, OtherState = ?, ForeignZip = ?, ")
                .Append("       ModUser = ?, ModDate = ? ")
                .Append("WHERE StudentContactAddressId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from syStudentContactAddresses where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   StudentContactAddressId
            db.AddParameter("@StudentContactAddressId", StudentContactAddressInfo.StudentContactAddressId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", StudentContactAddressInfo.StatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   StudentContactId
            db.AddParameter("@StudentContactId", StudentContactAddressInfo.StudentContactId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   AddrTypId
            If StudentContactAddressInfo.AddrTypId = Guid.Empty.ToString Then
                db.AddParameter("@AddrTypId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AddrTypId", StudentContactAddressInfo.AddrTypId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Address1
            db.AddParameter("@Address1", StudentContactAddressInfo.Address1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Address2
            If StudentContactAddressInfo.Address2 = "" Then
                db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", StudentContactAddressInfo.Address2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   City
            db.AddParameter("@City", StudentContactAddressInfo.City, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   StateId
            If StudentContactAddressInfo.StateId = Guid.Empty.ToString Then
                db.AddParameter("@StateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@StateId", StudentContactAddressInfo.StateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   CountryId
            If StudentContactAddressInfo.CountryId = Guid.Empty.ToString Then
                db.AddParameter("@CountryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CountryId", StudentContactAddressInfo.CountryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Zip
            If StudentContactAddressInfo.Zip = "" Then
                db.AddParameter("@Zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Zip", StudentContactAddressInfo.Zip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'OtherState
            If StudentContactAddressInfo.OtherState = "" Then
                db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@OtherState", StudentContactAddressInfo.OtherState, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ForeignZip
            db.AddParameter("@ForeignZip", StudentContactAddressInfo.ForeignZip, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   StudentContactAddressId
            db.AddParameter("@StudentContactAddressId", StudentContactAddressInfo.StudentContactAddressId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", StudentContactAddressInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddStudentContactAddressInfo(ByVal StudentContactAddressInfo As StudentContactAddressInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT syStudentContactAddresses ( ")
                .Append("       StudentContactAddressId, StatusId, StudentContactId, ")
                .Append("       AddrTypId, Address1, Address2, ")
                .Append("       City, StateId, CountryId,")
                .Append("       Zip, OtherState, ForeignZip, ")
                .Append("       ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   StudentContactAddressId
            db.AddParameter("@StudentContactAddressId", StudentContactAddressInfo.StudentContactAddressId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", StudentContactAddressInfo.StatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   StudentContactId
            db.AddParameter("@StudentContactId", StudentContactAddressInfo.StudentContactId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   AddrTypId
            If StudentContactAddressInfo.AddrTypId = Guid.Empty.ToString Then
                db.AddParameter("@AddrTypId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AddrTypId", StudentContactAddressInfo.AddrTypId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Address1
            db.AddParameter("@Address1", StudentContactAddressInfo.Address1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Address2
            If StudentContactAddressInfo.Address2 = "" Then
                db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", StudentContactAddressInfo.Address2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   City
            db.AddParameter("@City", StudentContactAddressInfo.City, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   StateId
            If StudentContactAddressInfo.StateId = Guid.Empty.ToString Then
                db.AddParameter("@StateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@StateId", StudentContactAddressInfo.StateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   CountryId
            If StudentContactAddressInfo.CountryId = Guid.Empty.ToString Then
                db.AddParameter("@CountryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CountryId", StudentContactAddressInfo.CountryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Zip
            If StudentContactAddressInfo.Zip = "" Then
                db.AddParameter("@Zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Zip", StudentContactAddressInfo.Zip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'OtherState
            If StudentContactAddressInfo.OtherState = "" Then
                db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@OtherState", StudentContactAddressInfo.OtherState, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ForeignZip
            db.AddParameter("@ForeignZip", StudentContactAddressInfo.ForeignZip, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteStudentContactAddressInfo(ByVal StudentContactAddressId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syStudentContactAddresses ")
                .Append("WHERE StudentContactAddressId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM syStudentContactAddresses WHERE StudentContactAddressId = ? ")
            End With

            '   add parameters values to the query

            '   StudentContactAddressId
            db.AddParameter("@StudentContactAddressId", StudentContactAddressId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   StudentContactAddressId
            db.AddParameter("@StudentContactAddressId", StudentContactAddressId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetAllStudentContactPhones(ByVal studentContactId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         SCP.StudentContactPhoneId, ")
            .Append("         SCP.StatusId, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("         SCP.Phone, ")
            '.Append("         SCP.StudentId, ")
            '.Append("         SCP.PhoneTypeId, ")
            '.Append("         SCP.Phone, ")
            '.Append("         SCP.Ext, ")
            '.Append("         SCP.BestTime ")
            .Append("         SCP.ForeignPhone ")
            .Append("FROM     syStudentContactPhones SCP, syStatuses ST ")
            .Append("WHERE    SCP.StatusId = ST.StatusId ")
            .Append("AND      SCP.StudentContactId = ? ")
        End With

        ' Add StudentId to the parameter list
        db.AddParameter("@StudentId", studentContactId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetStudentContactPhoneInfo(ByVal StudentContactPhoneId As String) As StudentContactPhoneInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("         SCP.StudentContactPhoneId, ")
            .Append("         SCP.StatusId, ")
            .Append("         SCP.StudentContactId, ")
            .Append("         SCP.PhoneTypeId, ")
            .Append("         (Select PhoneTypeDescrip from syPhoneType where PhoneTypeId=SCP.PhoneTypeId) As PhoneType, ")
            .Append("         SCP.Phone, ")
            .Append("         SCP.Ext, ")
            .Append("         SCP.BestTime, ")
            .Append("         SCP.ForeignPhone, ")
            .Append("         SCP.ModUser, ")
            .Append("         SCP.ModDate ")
            .Append("FROM  syStudentContactPhones SCP ")
            .Append("WHERE SCP.StudentContactPhoneId = ? ")
        End With

        ' Add the StudentContactPhoneId to the parameter list
        db.AddParameter("@StudentContactPhoneId", StudentContactPhoneId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim StudentContactPhoneInfo As New StudentContactPhoneInfo

        While dr.Read()

            '   set properties with data from DataReader
            With StudentContactPhoneInfo
                .IsInDB = True
                .StudentContactPhoneId = StudentContactPhoneId
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .StudentContactId = CType(dr("StudentContactId"), Guid).ToString
                .PhoneTypeId = CType(dr("PhoneTypeId"), Guid).ToString
                .PhoneType = dr("PhoneType")
                .Phone = dr("Phone")
                If Not (dr("Ext") Is System.DBNull.Value) Then .Ext = dr("Ext")
                If Not (dr("BestTime") Is System.DBNull.Value) Then .BestTime = dr("BestTime")
                .ForeignPhone = dr("ForeignPhone")
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return StudentContactPhoneInfo
        Return StudentContactPhoneInfo

    End Function
    Public Function UpdateStudentContactPhoneInfo(ByVal StudentContactPhoneInfo As StudentContactPhoneInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syStudentContactPhones ")
                .Append("   Set StudentContactPhoneId = ?, StatusId = ?, StudentContactId =?, ")
                .Append("       PhoneTypeId = ?,  Phone = ?, Ext = ?, ")
                .Append("       BestTime = ?, ForeignPhone = ?, ")
                .Append("       ModUser = ?, ModDate = ? ")
                .Append("WHERE StudentContactPhoneId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from syStudentContactPhones where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   StudentContactPhoneId
            db.AddParameter("@StudentContactPhoneId", StudentContactPhoneInfo.StudentContactPhoneId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", StudentContactPhoneInfo.StatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   StudentContactId
            db.AddParameter("@StudentContactId", StudentContactPhoneInfo.StudentContactId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   PhoneTypeId
            db.AddParameter("@PhoneTypeId", StudentContactPhoneInfo.PhoneTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Phone
            db.AddParameter("@Phone", StudentContactPhoneInfo.Phone, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Ext
            If StudentContactPhoneInfo.Ext = "" Then
                db.AddParameter("@Ext", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Ext", StudentContactPhoneInfo.Ext, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   BestTime
            db.AddParameter("@BestTime", StudentContactPhoneInfo.BestTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Foreign Phone
            db.AddParameter("@ForeignPhone", StudentContactPhoneInfo.ForeignPhone, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   StudentContactPhoneId
            db.AddParameter("@StudentContactPhoneId", StudentContactPhoneInfo.StudentContactPhoneId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", StudentContactPhoneInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddStudentContactPhoneInfo(ByVal StudentContactPhoneInfo As StudentContactPhoneInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT syStudentContactPhones ( ")
                .Append("       StudentContactPhoneId, StatusId, StudentContactId, ")
                .Append("       PhoneTypeId, Phone, Ext, ")
                .Append("       BestTime, ForeignPhone, ")
                .Append("       ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   StudentContactPhoneId
            db.AddParameter("@StudentContactPhoneId", StudentContactPhoneInfo.StudentContactPhoneId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", StudentContactPhoneInfo.StatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   StudentContactId
            db.AddParameter("@StudentContactId", StudentContactPhoneInfo.StudentContactId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   PhoneTypeId
            db.AddParameter("@PhoneTypeId", StudentContactPhoneInfo.PhoneTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Phone
            db.AddParameter("@Phone", StudentContactPhoneInfo.Phone, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Ext
            If StudentContactPhoneInfo.Ext = "" Then
                db.AddParameter("@Ext", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Ext", StudentContactPhoneInfo.Ext, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   BestTime
            db.AddParameter("@BestTime", StudentContactPhoneInfo.BestTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Foreign Phone
            db.AddParameter("@ForeignPhone", StudentContactPhoneInfo.ForeignPhone, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteStudentContactPhoneInfo(ByVal StudentContactPhoneId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syStudentContactPhones ")
                .Append("WHERE StudentContactPhoneId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM syStudentContactPhones WHERE StudentContactPhoneId = ? ")
            End With

            '   add parameters values to the query

            '   StudentContactPhoneId
            db.AddParameter("@StudentContactPhoneId", StudentContactPhoneId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   StudentContactPhoneId
            db.AddParameter("@StudentContactPhoneId", StudentContactPhoneId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
End Class


