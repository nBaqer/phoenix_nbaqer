Imports System.Data.OleDb
Imports System.Data
Imports System.Text
Imports FAME.AdvantageV1.Common
Imports FAME.Advantage.Common

Public Class ProgVerDB
    ''' <summary>
    ''' Store the connection string for this class
    ''' </summary>
    ''' <remarks></remarks>
    Private ReadOnly conString As String
    Private ReadOnly sqlConnectionString As String

    ''' <summary>
    ''' Application Setting for get some other different values from config.
    ''' </summary>
    ''' <remarks></remarks>                                                                                                             
    Private myAdvAppSettings As AdvAppSettings

    Sub New()
        myAdvAppSettings = AdvAppSettings.GetAppSettings()
        conString = myAdvAppSettings.AppSettings("ConString")
        sqlConnectionString = myAdvAppSettings.AppSettings("ConnectionString").ToString
    End Sub

    Public Function GetProgVersions(Optional ByVal ProgId As String = "", Optional ByVal CampusId As String = "") As DataSet

        '   connect to the database
        Dim db As New DataAccess
        Dim da As OleDbDataAdapter
        Dim ds As New DataSet

        db.ConnectionString = conString
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT Distinct t1.PrgVerId,t1.PrgVerDescrip as PrgVerDescrip, t1.StatusId  ")
            .Append(" ,(select Distinct Status from syStatuses where StatusId=t1.StatusId) as Status ")
            .Append("FROM arPrgVersions t1 ")
            .Append("WHERE t1.ProgId = ? ")
            .Append("AND t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("Order by PrgVerDescrip ")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@ProgId", ProgId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        '   Execute the query
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "PrgVersions")
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return ds
    End Function
    Public Function UpdateFASAP(FASAPid As String, strPKValue As String, User As String) As Boolean

        Dim sqlConnection As SqlConnection
        Dim mTransaction As SqlTransaction

        Dim connectionString = sqlConnectionString ' myAdvAppSettings.AppSettings("ConnectionString").ToString

        sqlConnection = New SqlConnection(connectionString)
        If sqlConnection.State = ConnectionState.Open Then sqlConnection.Close()
        sqlConnection.Open()
        mTransaction = sqlConnection.BeginTransaction()

        Try

            Dim myCommand As New SqlCommand("usp_UpdateSAOrFASAP", sqlConnection)
            myCommand.CommandType = CommandType.StoredProcedure
            myCommand.Parameters.Add("@FASAPid", SqlDbType.UniqueIdentifier).Value = New Guid(FASAPid)
            myCommand.Parameters.Add("@PrgVerId", SqlDbType.UniqueIdentifier).Value = New Guid(strPKValue)
            myCommand.Parameters.Add("@ModUser", SqlDbType.VarChar, 50).Value = User
            myCommand.Transaction = mTransaction
            myCommand.ExecuteNonQuery()
            mTransaction.Commit()
            Return True

        Catch ex As Exception
            mTransaction.Rollback()
            Return False
        Finally
            sqlConnection.Close()
        End Try
    End Function

    Public Function GETFASAPSetup(ProgId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess
        Dim da As OleDbDataAdapter
        Dim ds As New DataSet

        db.ConnectionString = conString ' myAdvAppSettings.AppSettings("ConString")
        '   build the sql query
        Dim sb As New StringBuilder
        'US54383 By Tatiana Timochina IF FASAP turn on and OFF

        With sb
            .Append("SELECT t1.PrgVerId, t1.FASAPId, t2.SAPDescrip, t2.FASAPPolicy,t2.StatusId")
            .Append(" FROM arPrgVersions t1 JOIN arSAP t2 on  t1.FASAPId=t2.SAPId ")
            .Append(" WHERE t1.PrgVerId =? and t2.FASAPPolicy=1 ")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@ProgId", ProgId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ' Execute the query
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "PrgVersFASAP")

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return ds
    End Function
    Public Function GetProgVersionsByProgram(progId As String) As DataSet

        '  connect to the database
        Dim db As New DataAccess
        Dim da As OleDbDataAdapter
        Dim ds As New DataSet

        db.ConnectionString = conString ' myAdvAppSettings.AppSettings("ConString")
        '   build the sql query
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT Distinct t1.PrgVerId,t1.PrgVerDescrip as PrgVerDescrip, t1.StatusId ")
            .Append(" ,(select Distinct Status from syStatuses where StatusId=t1.StatusId) as Status, t1.CampGrpId ")
            .Append("FROM arPrgVersions t1 ")
            .Append("WHERE t1.ProgId = ? ")
            .Append("Order by PrgVerDescrip ")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@ProgId", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Execute the query
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "PrgVersions")
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
        'Return the datatable in the dataset
        Return ds
    End Function

    '''<summary>
    ''' This method returns program details for the specified program version id.  
    ''' </summary>
    ''' <param name="programVersionId">The program Version Id.</param>
    ''' <returns>Returns the program details for the specified program version id</returns>
    Public Function GetProgVersionsByProgramVerId(programVersionId As String, campusId As String) As DataSet
        Dim sqlConnection As SqlConnection
        Dim dataSet As New DataSet()

        Dim connectionString = sqlConnectionString

        sqlConnection = New SqlConnection(connectionString)

        Try
            If sqlConnection.State = ConnectionState.Open Then sqlConnection.Close()

            sqlConnection.Open()

            Dim myCommand As New SqlCommand("USP_GetProgramVersionDetailsById", sqlConnection)
            myCommand.CommandType = CommandType.StoredProcedure
            myCommand.Parameters.Add("@PrgVerId", SqlDbType.UniqueIdentifier).Value = New Guid(programVersionId)
            myCommand.Parameters.Add("@CampusId", SqlDbType.UniqueIdentifier).Value = New Guid(campusId)

            Dim adapter As New SqlDataAdapter(myCommand)
            adapter.Fill(dataSet, "PrgVersions")

        Catch ex As Exception
            Throw ex
        Finally
            sqlConnection.Close()
        End Try

        Return dataSet
    End Function

    Public Function IsProgramVersionPartOfCampusUserIsLoggedInTo(campusId As String, progId As String, PrgVerId As String) As Boolean
        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = conString ' myAdvAppSettings.AppSettings("ConString")
        '   build the sql query
        Dim sb As New StringBuilder
        Dim intRecordCount
        With sb
            .Append("SELECT Count(*)   ")
            .Append("FROM arPrgVersions t1 ")
            .Append("WHERE t1.ProgId = ? and t1.PrgVerId=? ")
            .Append("AND t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
        End With

        db.AddParameter("@ProgId", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgverId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        db.OpenConnection()
        Try
            intRecordCount = db.RunParamSQLScalar(sb.ToString)
        Catch ex As Exception
            intRecordCount = 0
        Finally
            db.ClearParameters()
            db.CloseConnection()
            sb.Remove(0, sb.Length)
        End Try
        If intRecordCount >= 1 Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function GetFASAPProgVersionsByUser(Optional ByVal campusId As String = "", Optional ByVal username As String = "", Optional ByVal userid As String = "") As DataTable

        '   connect to the database
        Dim ds As New DataSet
        Dim dr As SqlDataAdapter
        Dim db As New SQLDataAccess

        db.ConnectionString = sqlConnectionString ' myAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@CampId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        If Not username = "sa" And Not userid = "" Then
            db.AddParameter("@UserID", New Guid(userid), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If

        db.OpenConnection()
        Try
            dr = db.RunParamSQLDataAdapter_SP("USP_FASAPPopulatePrgVersion")
            dr.Fill(ds, "PrgVersions")

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return ds.Tables(0)
    End Function


    Public Function GetProgVersionsByUser(Optional ByVal campusId As String = "", Optional ByVal username As String = "", Optional ByVal userid As String = "") As DataTable

        '   connect to the database
        Dim db As New DataAccess
        Dim da As OleDbDataAdapter
        Dim ds As New DataSet

        db.ConnectionString = conString ' myAdvAppSettings.AppSettings("ConString")
        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT t1.PrgVerId,t1.PrgVerDescrip, ")
            .Append(" case when (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=t1.ProgId) is null  then ")
            .Append(" t1.PrgVerDescrip ")
            .Append(" else ")
            .Append(" t1.PrgVerDescrip + ' (' + (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=t1.ProgId) + ')'  ")
            .Append(" end as PrgVerShiftDescrip  ")
            .Append("FROM arPrgVersions t1, syStatuses t2 ")
            .Append("WHERE t1.StatusId = t2.StatusId ")
            .Append("AND t2.Status = 'Active' ")

            If campusId <> "" Then
                .Append("AND (t1.CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId = ? ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR t1.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
            If Not username = "sa" And Not userid = "" Then
                .Append(" and T1.CampGrpId in ")
                .Append(" ( ")
                .Append("SELECT DISTINCT A.CampGrpId ")
                .Append("FROM syUsersRolesCampGrps A, syCampGrps B ")
                .Append("WHERE A.CampGrpId = B.CampGrpId ")
                .Append("AND A.UserId=? ")
                .Append(")")

            End If

            .Append("ORDER BY t1.PrgVerDescrip ")
        End With

        db.AddParameter("cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        If Not username = "sa" And Not userid = "" Then
            db.AddParameter("@UserId", userid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        '   Execute the query
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "PrgVersions")

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return ds.Tables(0)
    End Function
    Public Function GetPrgVerStartDates(progVerId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess
        Dim da As OleDbDataAdapter
        Dim ds As New DataSet

        db.ConnectionString = conString ' myAdvAppSettings.AppSettings("ConString")
        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT t1.SDateSetupId,t1.SDateSetupDescrip, t1.StatusId  ")
            .Append(" ,(select Distinct Status from syStatuses where StatusId=t1.StatusId) as Status ")
            .Append("FROM arSDateSetup t1 ")
            .Append("WHERE t1.PrgVerId = ?")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@ProgverId", progVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "SDates")
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return ds
    End Function

    Public Function HasThisProgramVersionStudentsEnrolled(prgVerID As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        With sb
            .Append("Select ")
            .Append("       (select count(*) from arStuEnrollments where PrgVerId= ? ) ")
        End With

        'add parameters
        db.AddParameter("@PrgVerId", prgVerID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return 
        Return CType(db.RunParamSQLScalar(sb.ToString), Boolean)

    End Function

    Public Function HasThisProgramVersionhaveCourses(prgVerID As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        With sb
            .Append("Select ")
            .Append("       (select count(*) from arProgVerDef where PrgVerId= ? ) ")
        End With

        'add parameters
        db.AddParameter("@PrgVerId", prgVerID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return 
        Return CType(db.RunParamSQLScalar(sb.ToString), Boolean)

    End Function

    Public Function GetSchedulingMethods() As DataSet
        Dim db As New DataAccess
        Dim ds As DataSet

        'Set the connection string
        db.ConnectionString = conString ' myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT SchedMethodId,Descrip ")
            .Append("FROM sySchedulingMethods")
        End With

        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds
    End Function

    Public Function GetTermTypes() As DataSet
        Dim db As New DataAccess
        Dim ds As DataSet

        'Set the connection string
        db.ConnectionString = conString ' myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT TermTypeId,Descrip ")
            .Append("FROM syTermTypes where TermTypeId in (1,2,3)")
        End With

        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds
    End Function
    Public Function GetProgVerAttendanceRecords(prgVerId As String) As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim result

        Try
            With sb
                'Query to retrieve number of attendance records belonging to a particular Program Version
                .Append("SELECT ")
                .Append("       COUNT(*) AS AttRecords ")
                .Append("FROM   atClsSectAttendance X ")
                .Append("WHERE  EXISTS (SELECT  *")
                .Append("               FROM    arStuEnrollments")
                .Append("               WHERE   PrgVerId=?")
                .Append("                       AND StuEnrollId=X.StuEnrollId)")
            End With

            'add parameters
            db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            result = CType(db.RunParamSQLScalar(sb.ToString), Integer)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        Return result
    End Function

    Public Function DoesProgVerHasSameAttenddanceTypeAsCourse(ByVal prgVerId As String, ByVal attType As String) As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim result
        Dim rtn As String = String.Empty

        Try
            With sb
                'Query to retrieve number of attendance records belonging to a particular Program Version
                .Append("Select top 1 UnitTypeId from arReqs a,arClassSections b,atClsSectAttendance c,arStuEnrollments d where ")
                .Append(" a.ReqId = b.Reqid And b.ClsSectionId = c.ClsSectionId And c.StuEnrollId = d.StuEnrollid and d.PrgVerId = ? ")
            End With

            'add parameters
            db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            While dr.Read()
                rtn = CType(dr("UnitTypeId"), Guid).ToString
            End While
            If rtn.ToLower = attType.ToLower Then
                result = 0
            Else
                result = 1
            End If

            If Not dr.IsClosed Then dr.Close()
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        Return result
    End Function

    Public Function GetAllCampusGroupsByProgram(ByVal ProgId As String, Optional ByVal PrgVerId As String = "") As DataSet
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim strCampGrpId As String
        Dim strActiveStatus As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveStatus As String = AdvantageCommonValues.InactiveGuid

        'Set the connection string
        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append(" Select Distinct CampGrpId from arPrograms where ProgId=? ")
        End With
        db.AddParameter("@ProgId", ProgId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            If dr(0) IsNot DBNull.Value Then
                strCampGrpId = CType(dr("CampGrpId"), Guid).ToString
            End If
        End While

        If Not dr.IsClosed Then dr.Close()

        db.ClearParameters()
        sb.Remove(0, sb.Length)

        If Not strCampGrpId = "" Then
            With sb
                .Append(" select Distinct t2.CampGrpId as CampGrpId,t2.CampGrpDescrip as CampGrpDescrip from syCmpGrpCmps t1,syCampGrps t2 ")
                .Append(" where t1.CampGrpId = t2.CampGrpId and t1.CampusId in ")
                ' Get all Campuses that are part of the campus groups that is assigned to a program
                .Append(" (select Distinct t3.CampusId from syCmpGrpCmps t3,syCampuses t4 ")
                .Append(" where t3.CampGrpId='" & strCampGrpId & "' and t3.CampusId=t4.CampusId  ")
                .Append(" and t4.StatusId='" & strActiveStatus & "') ")
                If Not PrgVerId = "" Then
                    .Append(" Union ")
                    .Append(" Select CampGrpId,'(X) ' + CampGrpDescrip as CampGrpDescrip from arPrgVersions t1,syCampGrps t2 ")
                    .Append(" where t1.CampGrpId = t2.CampGrpId and t2.StatusId = '" & strInActiveStatus & "' ")
                    .Append(" and t1.PrgVerId='" & PrgVerId & "' ")
                End If
                .Append(" order by CampGrpDescrip ")
            End With
            Return db.RunParamSQLDataSet(sb.ToString)
        End If
        Return Nothing
    End Function

    Public Function GetAllCampusGroupsByProgramAndProgramVersion(ByVal progId As String, Optional ByVal prgVerId As String = "") As DataSet
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim strCampGrpId As String
        Dim strActiveStatus As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveStatus As String = AdvantageCommonValues.InactiveGuid

        'Set the connection string
        db.ConnectionString = conString ' myAdvAppSettings.AppSettings("ConString")
        With sb
            .Append(" Select Distinct CampGrpId from arPrograms where ProgId=? ")
        End With
        db.AddParameter("@ProgId", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            strCampGrpId = CType(dr("CampGrpId"), Guid).ToString
        End While

        If Not dr.IsClosed Then dr.Close()

        db.ClearParameters()
        sb.Remove(0, sb.Length)

        If Not strCampGrpId = "" Then
            With sb
                .Append(" select Distinct t2.CampGrpId as CampGrpId,t2.CampGrpDescrip as CampGrpDescrip from syCmpGrpCmps t1,syCampGrps t2 ")
                .Append(" where t1.CampGrpId = t2.CampGrpId and t1.CampusId in ")
                ' Get all Campuses that are part of the campus groups that is assigned to a program
                .Append(" (select Distinct t3.CampusId from syCmpGrpCmps t3,syCampuses t4 ")
                .Append(" where t3.CampGrpId='" & strCampGrpId & "' and t3.CampusId=t4.CampusId  ")
                .Append(" and t4.StatusId='" & strActiveStatus & "') ")
                If Not prgVerId = "" Then
                    .Append(" Union ")
                    .Append(" Select Distinct t1.CampGrpId,'(X) ' + CampGrpDescrip as CampGrpDescrip from arPrgVersions t1,syCampGrps t2 ")
                    .Append(" where t1.CampGrpId = t2.CampGrpId and t2.StatusId = '" & strInActiveStatus & "' ")
                    .Append(" and t1.PrgVerId='" & prgVerId & "' ")
                    .Append(" Union ")
                    .Append(" Select Distinct t1.CampGrpId,'(X) ' + CampGrpDescrip as CampGrpDescrip from arPrgVersions t1,syCampGrps t2 ")
                    .Append(" where t1.CampGrpId = t2.CampGrpId  ")
                    .Append(" and t1.PrgVerId='" & prgVerId & "' ")
                    .Append(" and t1.CampGrpId not in ( ")
                    .Append(" select Distinct t2.CampGrpId from syCmpGrpCmps t1,syCampGrps t2 ")
                    .Append(" where t1.CampGrpId = t2.CampGrpId and t1.CampusId in ")
                    .Append(" (select Distinct t3.CampusId from syCmpGrpCmps t3,syCampuses t4 ")
                    .Append(" where t3.CampGrpId='" & strCampGrpId & "' and t3.CampusId=t4.CampusId  ")
                    .Append(" and t4.StatusId='" & strActiveStatus & "') ")
                    .Append(" Union ")
                    .Append(" Select Distinct t1.CampGrpId from arPrgVersions t1,syCampGrps t2 ")
                    .Append(" where t1.CampGrpId = t2.CampGrpId and t2.StatusId = '" & strInActiveStatus & "' ")
                    .Append(" and t1.PrgVerId='" & prgVerId & "') ")
                End If
                .Append(" order by CampGrpDescrip ")
            End With
            Return db.RunParamSQLDataSet(sb.ToString)
        End If
        Return Nothing
    End Function

    Public Function GetProgVersionsByCampus(progId As String, campusId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT Distinct t1.PrgVerId  ")
            .Append("FROM arPrgVersions t1 ")
            .Append("WHERE t1.ProgId = ? ")
            .Append("AND t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
        End With
        db.AddParameter("@ProgId", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function GetProgVersionsByCampusGroupandProgram(progId As String, campGrpId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT Distinct t1.PrgVerId,t1.PrgVerDescrip  ")
            .Append("FROM arPrgVersions t1 ")
            .Append("WHERE t1.ProgId = ? ")
            .Append("AND t1.CampGrpId = ? ")
        End With
        db.AddParameter("@ProgId", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampGrpId", campGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function IsCampusPartOfGroup(campusId As String, campGrpId As String)
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim intRecordsAffected
        With sb
            .Append(" select Count(*) as CampusExistInGroup from syCmpGrpCmps where CampusId=? ")
            .Append(" and CampGrpId=? ")
        End With
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampGrpId", campGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            intRecordsAffected = db.RunParamSQLScalar(sb.ToString)
        Catch ex As Exception
            intRecordsAffected = 0
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try
        Return intRecordsAffected
    End Function

    Public Function AllowCampusGroupChange(ByVal ProgId As String, ByVal NewCampGrpId As String, Optional ByVal CampusId As String = "") As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim intRecordsAffected
        Dim intAllowCampusGroupChange
        With sb
            .Append(" Select Count(*) from arPrgVersions where ProgId=?")
        End With
        db.AddParameter("@ProgId", ProgId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            intRecordsAffected = db.RunParamSQLScalar(sb.ToString)
        Catch ex As Exception
            intRecordsAffected = 0
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try
        If intRecordsAffected >= 1 Then
            With sb
                .Append(" Select Count(*) from syCmpGrpCmps t2 where t2.CampGrpId=? and t2.CampusId in ")
                .Append(" (Select distinct campusId from syCmpGrpCmps where CampGrpId in ")
                .Append(" (Select Distinct CampGrpId from arPrgVersions where ProgId=?)) ")
            End With
            db.AddParameter("@CampGrpId", NewCampGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ProgId", ProgId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                intAllowCampusGroupChange = db.RunParamSQLScalar(sb.ToString)
            Catch ex As Exception
                intAllowCampusGroupChange = 0
            Finally
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            End Try
            If intAllowCampusGroupChange < 1 Then
                Return False
            Else
                Return True
            End If
        Else
            Return True
        End If
    End Function

    Public Function HasThisProgramPrgVersionsActive(progId As String) As Boolean
        Dim hasActivesRecords As Boolean
        Dim intActivePrgVersions
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        sb.Append(" SELECT Count(*) FROM arPrgVersions AS PV INNER JOIN syStatuses AS S ON PV.StatusId = S.StatusId WHERE S.Status = 'Active' AND PV.ProgId = ?")
        db.AddParameter("@ProgId", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            intActivePrgVersions = db.RunParamSQLScalar(sb.ToString)
        Finally
            'Close Connection
            db.CloseConnection()
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try
        If intActivePrgVersions > 0 Then
            hasActivesRecords = True
        Else
            hasActivesRecords = False
        End If
        Return hasActivesRecords
    End Function
    Public Function PrgVersionBelowToInactiveProgram(progId As String) As Boolean
        Dim isInactiveProgram As Boolean
        Dim intInactiveProgram
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        sb.Append(" SELECT Count(*) FROM arPrograms AS P INNER JOIN syStatuses AS S ON P.StatusId = S.StatusId WHERE S.Status = 'Inactive' AND P.ProgId  =  ?")
        db.AddParameter("@ProgId", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            intInactiveProgram = db.RunParamSQLScalar(sb.ToString)
        Finally
            db.ClearParameters()
            'Close Connection
            db.CloseConnection()
            sb.Remove(0, sb.Length)
        End Try
        If intInactiveProgram > 0 Then
            isInactiveProgram = True
        Else
            isInactiveProgram = False
        End If
        Return isInactiveProgram
    End Function

    Public Function HasThisProgramVersionStudentsGraded(prgVerID As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        With sb
            .Append(" select count(stuenrollId) as StudentCount from ")
            .Append(" (select stuenrollId from arResults where GrdSysDetailId is not null and  ")
            .Append(" stuenrollId in(select stuenrollId from arStuEnrollments where PrgVerId= ? ) ")
            .Append(" union ")
            .Append(" select stuenrollId from arTransferGrades where GrdSysDetailId is not null and ")
            .Append(" stuenrollId in(select stuenrollId from arStuEnrollments where PrgVerId= ? ))AR ")
        End With

        'add parameters
        db.AddParameter("@PrgVerId", prgVerID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", prgVerID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return 
        Return CType(db.RunParamSQLScalar(sb.ToString), Boolean)

    End Function

    Public Function GetProgVerCodeRecords(prgVerIdCode As String, prgVerDescrip As String, campGrpId As String) As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim result

        Try
            With sb
                'Query to retrieve number of attendance records belonging to a particular Program Version
                .Append(" select count(*) from arPrgVersions where PrgVerCode = ? and PrgVerDescrip= ? and CampGrpId = ? ")
            End With

            'add parameters
            db.AddParameter("@prgVerIdCode", prgVerIdCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@prgVerDescrip", prgVerDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@cmpGrpId", campGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            result = CType(db.RunParamSQLScalar(sb.ToString), Integer)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        Return result
    End Function

    ''' <summary>
    ''' This method is to determine whether program is Non-Term based or not.
    ''' </summary>
    ''' <param name="prgVerIdCode">The prgVerIdCode is program version code which is a Id of type string</param>
    ''' <param name="prgVerDescrip">The prgVerDescrip is program version description of type string</param>
    ''' <param name="campGrpId">The campGrpId is campus group Id of type string</param>
    ''' <param name="acId">The acId is academic calender Id of type Integer</param>
    ''' <returns>Returns integer value that determines whether the specified program is Non-Term based.</returns>
    Public Function GetProgramType(prgVerIdCode As String, prgVerDescrip As String, campGrpId As String, acId As Integer) As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim result

        Try
            With sb
                .Append(" SELECT COUNT(ac.ACId) FROM ")
                .Append(" dbo.syAcademicCalendars ac INNER JOIN dbo.arPrograms ap ON ap.ACId = ac.ACId ")
                .Append(" LEFT JOIN dbo.arPrgVersions pv ON pv.ProgId = ap.ProgId ")
                .Append(" WHERE ac.ACId = ? AND ap.ProgDescrip = ? ")
                If (prgVerIdCode <> "" And campGrpId <> "") Then
                    .Append(" AND pv.PrgVerCode = ? AND ap.CampGrpId = ?")
                End If
            End With

            'add parameters
            db.AddParameter("@acId", acId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@prgVerDescrip", prgVerDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If (prgVerIdCode <> "" And campGrpId <> "") Then
                db.AddParameter("@prgVerIdCode", prgVerIdCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@cmpGrpId", campGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            result = CType(db.RunParamSQLScalar(sb.ToString), Integer)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        Return result
    End Function

    Public Function GetProgVerCodeRecords(prgVerIdCode As String, prgVerDescrip As String, campGrpId As String, prgVerId As String) As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim result

        Try
            With sb
                'Query to retrieve number of attendance records belonging to a particular Program Version
                .Append(" select count(*) from arPrgVersions where PrgVerCode = ? and PrgVerDescrip= ? and CampGrpId = ? and PrgVerId <> ? ")
            End With

            'add parameters
            db.AddParameter("@prgVerIdCode", prgVerIdCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@prgVerDescrip", prgVerDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@cmpGrpId", campGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@prgVerID", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            result = CType(db.RunParamSQLScalar(sb.ToString), Integer)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        Return result
    End Function

    Public Function GetResourceRoles(resource As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        With sb
            .Append("SELECT Distinct t1.RoleId, t1.AccessLevel,t2.ResourceURL ")
            .Append("FROM syRlsResLvls t1, syResources t2 ")
            .Append("WHERE t1.ResourceID = t2.ResourceID ")
            .Append("AND t2.Resource like '" + resource + "';")
        End With

        db.ConnectionString = conString ' myAdvAppSettings.AppSettings("ConString")
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds.Tables(0)
    End Function


    Public Function GetUserRolesForCurrentCampus(userId As String, campusId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        With sb
            .Append("SELECT t1.RoleId ")
            .Append("FROM syUsersRolesCampGrps t1 ")
            .Append("WHERE t1.UserId = ? ")
            .Append("AND t1.CampGrpId IN ")
            .Append("(SELECT t2.CampGrpId ")
            .Append("FROM syCmpGrpCmps t2 ")
            .Append("WHERE t2.CampusId = ?)")
        End With

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@campusid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds.Tables(0)

    End Function

    Public Function GetUserRoles(userId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        With sb
            .Append("SELECT t1.RoleId ")
            .Append("FROM syUsersRolesCampGrps t1 ")
            .Append("WHERE t1.UserId = ? ")
        End With

        db.ConnectionString = conString ' myAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds.Tables(0)
    End Function


    ''To Find the Schedules and UseTimeClock fields related to a Program Version
    Public Function GetScheduleforProgVerCode(prgVerId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        Try
            With sb
                'Query to retrieve Schedule and UseTimeClock Fields  belonging to a particular Program Version
                .Append(" Select ScheduleId,UseTimeClock from arProgSchedules PS,arPrgVersions PV where PS.PrgverId= ?  and PV.PrgVerId=PS.PrgVerId  ")
            End With

            'add parameters
            db.AddParameter("@prgVerID", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet(sb.ToString)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
        Return ds
    End Function

    Public Function IsClockHourSchool() As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim result

        Try
            With sb
                'Query to retrieve number of attendance records belonging to a particular Program Version
                .Append(" select count(*) as cnt from arPrograms,syAcademicCalendars ")
                .Append(" where  arPrograms.ACId=syAcademicCalendars.ACId and lower(syAcademicCalendars.ACDescrip)='clock hour' ")
            End With

            'add parameters
            result = CType(db.RunParamSQLScalar(sb.ToString), Integer)
            If result > 0 Then Return True
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
        Return False
    End Function

    Public Function IsClockHourProgramVersion(ByVal stuEnrollid As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim result

        Try
            With sb
                'Query to retrieve number of attendance records belonging to a particular Program Version
                .Append(" select count(*) as cnt from arPrograms,arprgVersions,arStuEnrollments,syAcademicCalendars  ")
                .Append(" where  arPrograms.ACId=syAcademicCalendars.ACId and lower(syAcademicCalendars.ACDescrip)='clock hour' ")
                .Append(" and arPrograms.Progid=arPrgVersions.progid and arprgVersions.PrgVerid=arStuEnrollments.prgVerid and ")
                .Append(" arStuEnrollments.StuEnrollid= ? ")
            End With

            'add parameters
            db.AddParameter("@stuEnrollID", stuEnrollid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            result = CType(db.RunParamSQLScalar(sb.ToString), Integer)
            If result > 0 Then Return True
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        Return False
    End Function

    'InsertIntoPrgChargePeriodSeqTable
    Public Function InsertIntoPrgChargePeriodSeqTable(prgVerID As String) As String
        Dim db As New SQLDataAccess

        db.ConnectionString = sqlConnectionString ' myAdvAppSettings.AppSettings("ConnectionString")

        'add student enrollment parameter
        db.AddParameter("@PrgVerID", New Guid(prgVerID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_ARProgramVersionChargeSequence_Insert")
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return String.Empty
    End Function

    Public Function DoesCampusIsASubsetOfCampusGroup(campGrpId As String, campusId As String, prgverId As String) As Boolean
        'return boolean value
        Dim db As New SQLDataAccess
        Dim result As Boolean
        db.ConnectionString = sqlConnectionString ' myAdvAppSettings.AppSettings("ConnectionString")

        'add student enrollment parameter
        db.AddParameter("@campGrpId", New Guid(campGrpId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@campusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@prgVerID", New Guid(prgverId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Try
            result = CType(db.RunParamSQLScalar_SP("usp_DoesCampusIsASubsetOfCampusGroup"), Boolean)
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return result

    End Function

    Public Function IsPrgVerAttTypeMatchWithCourses(prgverid As String, unitTypeid As String) As DataTable

        Dim ds As DataSet
        Dim db As New SQLDataAccess

        db.ConnectionString = sqlConnectionString ' myAdvAppSettings.AppSettings("ConnectionString")

        'add student enrollment parameter
        db.AddParameter("@prgverid", New Guid(prgverid), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@unitTypeid", New Guid(unitTypeid), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Try
            ds = db.RunParamSQLDataSet_SP("USP_GetCoursesFortheCourseAndCourseGroupsForProgramVersion")
            Return ds.Tables(0)
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function

    Public Function PrgVersionsGetListActiveAvailableForABillingMethod(billingMethodId As String) As DataSet
        'Get the list of active PrgVersions that are attached to the giving BillingMethod 
        Dim db As New SQLDataAccess
        Dim dr As SqlDataAdapter
        Dim ds As New DataSet

        db.ConnectionString = conString 'myAdvAppSettings.AppSettings("ConString")

        ' Add the BillingMethodId to parameter list
        db.AddParameter("@BillingMethodId", New Guid(billingMethodId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        ' Execute the query
        db.OpenConnection()
        Try

            dr = db.RunParamSQLDataAdapter_SP("USP_PrgVersionsGetListActiveAvailableForABillingMethod")
            dr.Fill(ds, "PrgVersions")
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return ds
    End Function

    Public Function PrgVersionsGetListForABillingMethod(billingMethodId As String) As DataSet

        'Get the list of active PrgVersions that are attached to the giving BillingMethod 
        Dim db As New SQLDataAccess
        Dim dr As SqlDataAdapter
        Dim ds As New DataSet

        db.ConnectionString = conString ' myAdvAppSettings.AppSettings("ConString")
        ' Add the BillingMethodId to parameter list
        db.AddParameter("@BillingMethodId", New Guid(billingMethodId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        ' Execute the query
        db.OpenConnection()
        Try
            dr = db.RunParamSQLDataAdapter_SP("USP_PrgVersionsGetListForABillingMethod")
            dr.Fill(ds, "PrgVersions")
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return ds
    End Function

    Public Function GetPrgVersionsForCurrentBillingMethod(billingMethodId As String) As DataSet
        'Get the list of active PrgVersions that are attached to the giving BillingMethod 
        Dim db As New DataAccess
        Dim da As OleDbDataAdapter
        Dim ds As New DataSet

        db.ConnectionString = conString ' myAdvAppSettings.AppSettings("ConString")
        ' build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT Distinct   ")
            .Append("       APV.PrgVerId ")
            .Append("     , APV.PrgVerDescrip as PrgVerDescrip ")
            .Append("FROM arPrgVersions AS APV ")
            .Append("    INNER JOIN syStatuses AS SS ")
            .Append("        ON APV.StatusId = SS.StatusId ")
            .Append("WHERE SS.StatusCode = 'A' ")
            .Append("  AND APV.BillingMethodId = ? ")
            .Append("ORDER BY APV.PrgVerDescrip ")
        End With

        ' Add the BillingMethodId to parameter list
        db.AddParameter("@BillingMethodId", billingMethodId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add the IncrementType to parameter list
        'db.AddParameter("@IncrementType", IncrementType, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        ' Execute the query
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "PrgVersions")
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return ds
    End Function

    Public Function UpdatePrgVersionsWithBillingMethod(strXml As String, billingMethodId As String) As String

        Dim db As New SQLDataAccess

        myAdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@strXML", strXml, SqlDbType.NText, , ParameterDirection.Input)

            ' Add the BillingMethodId to parameter list
            db.AddParameter("@BillingMethodId", New Guid(billingMethodId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            'Call the procedure to insert more than one record all at once
            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_PrgVersionsWithBillingMethod_Update", Nothing)
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return String.Empty
    End Function

    Public Function CheckIfAPrgVersionTrackCredits(ByVal pPrgVerId As Guid) As Boolean

        Dim db As New SQLDataAccess
        db.ConnectionString = sqlConnectionString ' myAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@prgVerId", pPrgVerId, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Try
            Dim rtn = CType(db.RunParamSQLScalar_SP("dbo.[Usp_CheckIfAPrgVersionTrackCredits]"), Boolean)
            Return rtn
        Finally
            db.CloseConnection()
        End Try
    End Function

    ''' <summary>
    ''' Get program version Id from Student Enrollment
    ''' </summary>
    ''' <param name="stuEnrollId">The enrollment</param>
    ''' <returns>The Program version ID</returns>
    Public Function GetPrgVerIdForAGivenStuEnrollId(stuEnrollId As String) As Guid
        'Get PrgVerId from StuEnrollId 
        Dim result As Guid

        ' connect to the database
        Dim conn = New SqlConnection(sqlConnectionString) ' myAdvAppSettings.AppSettings("ConnectionString"))
        Dim comm = New SqlCommand("dbo.Usp_GetPrgVerIdForAGivenStuEnrollId", conn)
        comm.CommandType = CommandType.StoredProcedure
        comm.Parameters.AddWithValue("@StuEnrollId", New Guid(stuEnrollId))
        conn.Open()
        Try
            result = comm.ExecuteScalar() '  
            Return result
        Finally
            conn.Close()
        End Try
    End Function

#Region "PrgVerInstructionType"


    Public Function GetAllProgVersionInstructionType(ByVal prgVerId As String) As DataTable

        Dim ds As DataSet
        Dim db As New SQLDataAccess
        db.ConnectionString = sqlConnectionString ' MyAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@PrgVerId", New Guid(prgVerId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Try
            ds = db.RunParamSQLDataSet_SP("dbo.USP_AR_PrgVerInstructionType_GetList")
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return ds.Tables(0)
    End Function

    Public Sub UpdateProgVersionInstructionType(ByVal dtPrgVerInstructionType As DataTable, ByVal UserName As String, ByVal PrgVerId As Guid)
        Dim db As New SQLDataAccess

        'Set the connection string
        db.ConnectionString = conString ' myAdvAppSettings.AppSettings("ConString")


        Dim insertquery = From progVersionInstructionTypeTable In dtPrgVerInstructionType.AsEnumerable()
                          Where progVersionInstructionTypeTable.Field(Of Integer)("CmdType") = 1 Select progVersionInstructionTypeTable = progVersionInstructionTypeTable

        For Each row In insertquery
            db.AddParameter("@PrgVerID", PrgVerId, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@InstructionTypeId", New Guid(row("InstructionTypeId").ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@Hours", row("Hours"), SqlDbType.Decimal, , ParameterDirection.Input)
            db.AddParameter("@UserName", UserName, SqlDbType.VarChar, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_AR_PrgVerInstructionType_Insert")
            db.ClearParameters()
        Next

        Dim updatequery = From progVersionInstructionTypeTable In dtPrgVerInstructionType.AsEnumerable()
                          Where progVersionInstructionTypeTable.Field(Of Integer)("CmdType") = 2 Select progVersionInstructionTypeTable = progVersionInstructionTypeTable

        For Each row In updatequery
            db.AddParameter("@PrgVerInstructionTypeId", New Guid(row("PrgVerInstructionTypeId").ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@PrgVerID", PrgVerId, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@InstructionTypeId", New Guid(row("InstructionTypeId").ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@Hours", row("Hours"), SqlDbType.Decimal, , ParameterDirection.Input)
            db.AddParameter("@UserName", UserName, SqlDbType.VarChar, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_AR_PrgVerInstructionType_Update")
            db.ClearParameters()
        Next

        Dim deletequery = From progVersionInstructionTypeTable In dtPrgVerInstructionType.AsEnumerable()
                          Where progVersionInstructionTypeTable.Field(Of Integer)("CmdType") = 3 Select progVersionInstructionTypeTable = progVersionInstructionTypeTable

        For Each row In deletequery
            db.AddParameter("@PrgVerInstructionTypeId", New Guid(row("PrgVerInstructionTypeId").ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_AR_PrgVerInstructionType_Delete")
            db.ClearParameters()
        Next
    End Sub

#End Region

    Public Function UpdateDoCourseWeightIntoOverallGPA(prgVerId As String, checked As Boolean) As Boolean
        Dim db As New DataAccess
        db.ConnectionString = conString ' myAdvAppSettings.AppSettings("ConString")

        ' build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("UPDATE dbo.arPrgVersions   ")
            .Append("SET DoCourseWeightOverallGPA = ? ")
            .Append("WHERE PrgVerId = ? ")
        End With


        ' Add the BillingMethodId to parameter list
        db.AddParameter("@DoCourseWeightOverallGPA", checked, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        Try
            Dim result = db.RunParamSQLScalar(sb.ToString)
        Catch ex As Exception
            Return False
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
        Return True
    End Function

    Public Function UpdateProgramVersionDefinitionWeightValue(prgVerDefId As String, weight As Decimal) As Boolean
        Dim db As New DataAccess
        db.ConnectionString = conString ' myAdvAppSettings.AppSettings("ConString")

        ' build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("UPDATE dbo.arProgVerDef   ")
            .Append("SET CourseWeight = ? ")
            .Append("WHERE ProgVerDefId = ? ")
        End With


        ' Add the BillingMethodId to parameter list
        db.AddParameter("@CourseWeight", weight, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
        db.AddParameter("@ProgVerDefId", prgVerDefId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        Try
            Dim result = db.RunParamSQLScalar(sb.ToString)
        Catch ex As Exception
            Return False
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
        Return True
    End Function

    Public Function GetProgramVersionDefinitionID(ByVal programVersionId As String, ByVal requirementId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim programVersionDefinitionIdQuery As New StringBuilder
        Dim programVersionDefinitionId As String

        With programVersionDefinitionIdQuery
            .Append("SELECT ProgVerDefId FROM arProgVerDef WHERE PrgVerId = ? And ReqId = ?")
        End With

        Try
            db.AddParameter("@PrgVerId", programVersionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ReqId", requirementId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Dim programVersionDefinitionIdGPAObject = db.RunParamSQLScalar(programVersionDefinitionIdQuery.ToString)
            programVersionDefinitionId = Convert.ToString(programVersionDefinitionIdGPAObject)
            db.ClearParameters()
            db.CloseConnection()

        Catch ex As Exception
        End Try

        Return programVersionDefinitionId
    End Function

    Public Function GetDoCourseWeightOverallGPAValue(programVersionId As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim programVersionIdQuery As New StringBuilder
        Dim doCourseWeightOverallGPA As Boolean

        With programVersionIdQuery
            .Append("SELECT DoCourseWeightOverallGPA FROM arPrgVersions WHERE PrgVerId = ?")
        End With

        Try
            db.AddParameter("@PrgVerId", programVersionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Dim doCourseWeightOverallGPAObject = db.RunParamSQLScalar(programVersionIdQuery.ToString)
            doCourseWeightOverallGPA = Convert.ToBoolean(doCourseWeightOverallGPAObject)
            db.ClearParameters()
            db.CloseConnection()

        Catch ex As Exception
        End Try

        Return doCourseWeightOverallGPA
    End Function
End Class
