Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' TransCodesDB.vb
'
' TransCodesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class TransCodesDB
    Public Function GetAllTransCodes(ByVal showActiveOnly As Boolean) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   TC.TransCodeId, TC.StatusId, TC.TransCodeCode, TC.TransCodeDescrip, ")
            .Append("         TC.BillTypeId ")
            .Append("FROM     saTransCodes TC, syStatuses ST ")
            .Append("WHERE    TC.StatusId = ST.StatusId ")
            If showActiveOnly Then
                .Append(" AND     ST.Status = 'Active' ")
            End If
            .Append("ORDER BY TC.TransCodeDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllTransCodes(ByVal sysTransCodeId As Integer, ByVal showActiveOnly As Boolean) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   TC.TransCodeId, ")
            .Append("         TC.StatusId, ")
            .Append("         TC.TransCodeCode, ")
            .Append("         TC.TransCodeDescrip, ")
            .Append("         TC.BillTypeId ")
            .Append("FROM     saTransCodes TC, saSysTransCodes SSC, syStatuses ST ")
            .Append("WHERE    TC.StatusId = ST.StatusId ")
            If showActiveOnly Then
                .Append(" AND     ST.Status = 'Active' ")
            End If
            .Append("AND      TC.SysTransCodeId=SSC.SysTransCodeId ")
            .Append("AND      SSC.SysTransCodeId = ? ")
            .Append("ORDER BY TC.TransCodeDescrip ")
        End With

        '   SysTransCodeId
        db.AddParameter("@SysTransCodeId", sysTransCodeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetTransCodeInfo(ByVal TransCodeId As String) As TransCodeInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT TC.TransCodeId, ")
            .Append("    TC.TransCodeCode, ")
            .Append("    TC.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=TC.StatusId) As Status, ")
            .Append("    TC.TransCodeDescrip, ")
            .Append("    TC.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=TC.CampGrpId) As CampGrpdescrip, ")
            .Append("    TC.BillTypeId, ")
            .Append("    (Select BillTypeDescrip from BillTyps where BillTypeId=TC.BillTypeId) As BillTypeDescrip ")
            .Append("FROM  saTransCodes TC ")
            .Append("WHERE TC.TransCodeId= ? ")
        End With

        ' Add the TransCodeId to the parameter list
        db.AddParameter("@TransCodeId", TransCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim TransCodeInfo As New TransCodeInfo

        While dr.Read()

            '   set properties with data from DataReader
            With TransCodeInfo
                .TransCodeId = TransCodeId
                .IsInDB = True
                .Code = dr("TransCodeCode")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Description = dr("TransCodeDescrip")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpdescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                If Not (dr("BillTypeId") Is System.DBNull.Value) Then .BillTypeId = CType(dr("BillTypeId"), Guid).ToString
                If Not (dr("BillTypeDescrip") Is System.DBNull.Value) Then .BillTypeDescrip = dr("BillTypeDescrip")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return TransCodeInfo

    End Function
    Public Function UpdateTransCodeInfo(ByVal TransCodeInfo As TransCodeInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saTransCodes Set TransCodeId = ?, TransCodeCode = ?, ")
                .Append(" StatusId = ?, TransCodeDescrip = ?, CampGrpId = ?, ")
                .Append(" BillTypeId = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE TransCodeId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saTransCodes where ModDate = ? ")

            End With

            '   add parameters values to the query

            '   TransCodeId
            db.AddParameter("@TransCodeId", TransCodeInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TransCodeCode
            db.AddParameter("@TransCodeCode", TransCodeInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", TransCodeInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TransCodeDescrip
            db.AddParameter("@TransCodeDescrip", TransCodeInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If TransCodeInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", TransCodeInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   BillTypeId
            If TransCodeInfo.BillTypeId = Guid.Empty.ToString Then
                db.AddParameter("@BillTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@BillTypeId", TransCodeInfo.BillTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TransCodeId
            db.AddParameter("@TransCodeId", TransCodeInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", TransCodeInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddTransCodeInfo(ByVal TransCodeInfo As TransCodeInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saTransCodes (TransCodeId, TransCodeCode, StatusId, ")
                .Append("   TransCodeDescrip, CampGrpId, BillTypeId, ")
                .Append("   ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   TransCodeId
            db.AddParameter("@TransCodeId", TransCodeInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TransCodeCode
            db.AddParameter("@TransCodeCode", TransCodeInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", TransCodeInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TransCodeDescrip
            db.AddParameter("@TransCodeDescrip", TransCodeInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If TransCodeInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", TransCodeInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   BillTypeId
            If TransCodeInfo.BillTypeId = Guid.Empty.ToString Then
                db.AddParameter("@BillTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@BillTypeId", TransCodeInfo.BillTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteTransCodeInfo(ByVal TransCodeId As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saTransCodes ")
                .Append("WHERE TransCodeId = ? ")
            End With

            '   add parameters values to the query

            '   BankId
            db.AddParameter("@TransCodeId", TransCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetTransCodesDS(ByVal showActiveOnly As String) As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   create dataset
        Dim ds As New DataSet

        '   build the sql query for the TransCodesGLAccounts data adapter
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       TCGLA.TransCodeGLAccountId, ")
            .Append("       TCGLA.TransCodeId, ")
            .Append("       TCGLA.GLAccount, ")
            .Append("       TCGLA.Percentage, ")
            .Append("       TCGLA.TypeId, ")
            .Append("       (Select Case TCGLA.TypeId When 0 then 'Debit' when 2 then 'Debit' else 'Credit' end) Type, ")
            .Append("       TCGLA.ViewOrder,ST.StatusId,ST.Status, ")
            .Append("       TCGLA.ModDate, ")
            .Append("       TCGLA.ModUser ")
            .Append("FROM   saTransCodeGLAccounts TCGLA, saTransCodes TC, syStatuses ST ")
            .Append("WHERE  TCGLA.TransCodeId=TC.TransCodeId ")
            .Append(" AND   TC.StatusId = ST.StatusId ")
            If showActiveOnly = "True" Then
                .Append("AND    ST.Status = 'Active' ")
                .Append(" Order By TC.TransCodeDescrip ")
            ElseIf showActiveOnly = "False" Then
                .Append("AND    ST.Status = 'Inactive' ")
                .Append(" Order By TC.TransCodeDescrip ")
            Else
                .Append("ORDER BY ST.Status,TC.TransCodeDescrip asc")
            End If
        End With

        '   build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

        '   Create adapter to handle TransCodesGLAccounts table
        Dim da As New OleDbDataAdapter(sc)

        '   Fill TransCodeGLAccounts table
        da.Fill(ds, "TransCodeGLAccounts")

        '   build select query for the TransCodes data adapter
        '   build the sql query
        sb = New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT TC.TransCodeId, ")
            .Append("    TC.TransCodeCode, ")
            .Append("    TC.StatusId, ")
            .Append("    TC.TransCodeDescrip, ")
            .Append("    TC.CampGrpId, ")
            .Append("    TC.BillTypeId, ")
            .Append("    TC.DefEarnings, ")
            .Append("    TC.IsInstCharge, ")
            .Append("    TC.Is1098T, ")
            .Append("    TC.SysTransCodeId, ")
            .Append("    TC.ModUser, ")
            .Append("    TC.ModDate,ST.Status,ST.StatusId ")
            .Append("FROM  saTransCodes TC, syStatuses ST ")
            .Append("WHERE  TC.StatusId = ST.StatusId ")
            If showActiveOnly = "True" Then
                .Append("AND    ST.Status = 'Active' ")
                .Append(" Order By TC.TransCodeDescrip ")
            ElseIf showActiveOnly = "False" Then
                .Append("AND    ST.Status = 'Inactive' ")
                .Append(" Order By TC.TransCodeDescrip ")
            Else
                .Append("ORDER BY ST.Status,TC.TransCodeDescrip asc")
            End If
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   fill saTransCodes table
        da.Fill(ds, "TransCodes")

        '   create primary and foreign key constraints

        '   set primary key for saTransCodeGLAccounts table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("TransCodeGLAccounts").Columns("TransCodeGLAccountId")
        ds.Tables("TransCodeGLAccounts").PrimaryKey = pk0

        '   set primary key for saTransCodes table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("TransCodes").Columns("TransCodeId")
        ds.Tables("TransCodes").PrimaryKey = pk1

        '   get foreign key column in saTransCodesGLAccounts
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("TransCodeGLAccounts").Columns("TransCodeId")

        '   add relationship
        ds.Relations.Add("TransCodesTransCodeGLAccounts", pk1, fk0)

        '   return dataset
        Return ds

    End Function
    Public Function UpdateTransCodesDS(ByVal ds As DataSet) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   all updates must be encapsulated as one transaction
        Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            '   build the sql query for the TransCodeGLAccounts data adapter
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("       TCGLA.TransCodeGLAccountId, ")
                .Append("       TCGLA.TransCodeId, ")
                .Append("       TCGLA.GLAccount, ")
                .Append("       TCGLA.Percentage, ")
                .Append("       TCGLA.TypeId, ")
                .Append("       TCGLA.ViewOrder, ")
                .Append("       TCGLA.ModDate, ")
                .Append("       TCGLA.ModUser ")
                .Append("FROM   saTransCodeGLAccounts TCGLA ")
            End With

            '   build select command
            Dim TransCodeGLAccountsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle TransCodesGLAccounts table
            Dim TransCodeGLAccountsDataAdapter As New OleDbDataAdapter(TransCodeGLAccountsSelectCommand)

            '   build insert, update and delete commands for TransCodesGLAccounts table
            Dim cb As New OleDbCommandBuilder(TransCodeGLAccountsDataAdapter)

            '   build select query for the TransCodes data adapter
            sb = New StringBuilder
            With sb
                '   with subqueries
                .Append("SELECT TC.TransCodeId, ")
                .Append("    TC.TransCodeCode, ")
                .Append("    TC.StatusId, ")
                .Append("    TC.TransCodeDescrip, ")
                .Append("    TC.CampGrpId, ")
                .Append("    TC.BillTypeId, ")
                .Append("    TC.DefEarnings, ")
                .Append("    TC.IsInstCharge, ")
                .Append("    TC.Is1098T, ")
                .Append("    TC.SysTransCodeId, ")
                .Append("    TC.ModUser, ")
                .Append("    TC.ModDate ")
                .Append("FROM  saTransCodes TC ")
            End With

            '   build select command
            Dim TransCodesSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle TransCodes table
            Dim TransCodesDataAdapter As New OleDbDataAdapter(TransCodesSelectCommand)

            '   build insert, update and delete commands for TransCodes table
            Dim cb1 As New OleDbCommandBuilder(TransCodesDataAdapter)

            '   insert added rows in TransCodes table
            TransCodesDataAdapter.Update(ds.Tables("TransCodes").Select(Nothing, Nothing, DataViewRowState.Added))

            '   insert added rows in TransCodesGLAccounts table
            TransCodeGLAccountsDataAdapter.Update(ds.Tables("TransCodeGLAccounts").Select(Nothing, Nothing, DataViewRowState.Added))

            '   delete rows in TransCodesGLAccounts table
            TransCodeGLAccountsDataAdapter.Update(ds.Tables("TransCodeGLAccounts").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   delete rows in TransCodes table
            TransCodesDataAdapter.Update(ds.Tables("TransCodes").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   update rows in TransCodesGLAccounts table
            TransCodeGLAccountsDataAdapter.Update(ds.Tables("TransCodeGLAccounts").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   update rows in TransCodes table
            TransCodesDataAdapter.Update(ds.Tables("TransCodes").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            Return ""

            '   Catch only OleDbExceptions
        Catch Ex As OleDb.OleDbException

            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return DALExceptions.BuildErrorMessage(Ex)
        Finally

            '   close connection
            connection.Close()
        End Try

    End Function
End Class
