Imports System.Xml
Public Class RoleEmpStudentDB

    Public Function GetRoleList() As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Dim strMessageListString As New StringBuilder
        With strMessageListString
            .Append("SELECT RoleId, Role from syRoles")
        End With
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strMessageListString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
        End Try
        da.Fill(ds, "RoleList")
        db.CloseConnection()
        db.ClearParameters()
        Return ds
    End Function
    Public Function GetStudentList() As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Dim strMessageListString As New StringBuilder
        With strMessageListString
            .Append("SELECT StudentId, FirstName, LastName from arStudent")
        End With
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strMessageListString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
        End Try
        da.Fill(ds, "StudentList")
        db.CloseConnection()
        db.ClearParameters()
        Return ds
    End Function


    Public Function GetEmployeeList(ByRef RoleId As Guid) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strMessageListString As New StringBuilder
        Dim da As New OleDbDataAdapter
        With strMessageListString
            .Append("select a.EmpRoleId, a.EmpId, b.LastName, b.FirstName from hrEmpRoles a, hrEmployees b")
            .Append(" where (a.EmpId = b.EmpId)")
            .Append(" and (a.RoleId = ?) ")
        End With

        db.AddParameter("@RoleId", RoleId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strMessageListString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
        End Try
        da.Fill(ds, "EmployeeList")
        db.CloseConnection()
        db.ClearParameters()
        Return ds
    End Function
    Public Function UpdateRoleEmpStudentTable(ByVal st As SortedList, ByVal RoleId As Guid, ByVal EmpId As Guid)
        Dim ds As New DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        ' Remember - we need to lock the table in read only format until we are finished this transaction.
        ' We are getting the latest lising here so because there could have been a modification to the table when the user first started
        ds = GetRoleEmpStudentList(RoleId, EmpId)
        tbl = ds.Tables("SelectedList")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("StudentId")}
        End With
        Dim InTable As String
        Dim InSorted As String
        ' Begin by looping through what is on file and deleting any records that were removed by the user
        If ds.Tables("SelectedList").Rows.Count > 0 Then
            For Each row In tbl.Rows
                'Loop through the SortedList - if we find the record in both - jump out of the loop - if we do not find the record - deleted it
                Dim o As Guid
                Dim found As Boolean = False
                For Each o In st.Keys
                    InTable = XmlConvert.ToString(row("StudentId"))
                    InSorted = XmlConvert.ToString(o)
                    If XmlConvert.ToString(o) = XmlConvert.ToString(row("StudentId")) Then
                        found = True
                        Exit For
                    End If
                Next
                If found = False Then
                    'delete the record -- we did not find it in the new listing
                    DeleteRecord(RoleId, EmpId, row("StudentId"))
                End If
            Next
        End If
        ' Next loop through the sortedlist and add any records that are currently not on file
        If st.Count > 0 Then
            Dim o As Guid
            For Each o In st.Keys
                ' Loop through the table - if we do not find the record - add it to the table
                Dim found As Boolean = False
                For Each row In tbl.Rows
                    InTable = XmlConvert.ToString(row("StudentId"))
                    InSorted = XmlConvert.ToString(o)
                    If XmlConvert.ToString(o) = XmlConvert.ToString(row("StudentId")) Then
                        found = True
                        Exit For
                    End If
                Next
                If found = False Then
                    'Insert the record -- we did not find it in the db table
                    AddRecord(RoleId, EmpId, o)
                End If
            Next
        End If
    End Function
    Public Function GetRoleEmpStudentList(ByVal RoleId As Guid, ByVal EmpId As Guid) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strMessageListString As New StringBuilder
        Dim da As New OleDbDataAdapter
        With strMessageListString
            .Append("select a.RoleId, a.EmpId, a.StudentId, b.LastName, b.FirstName, b.MiddleName from cmRoleEmpStudent a, arStudent b")
            .Append(" where (a.StudentId = b.StudentId)")
            .Append(" and (RoleId = ?) ")
            .Append(" and (EmpId = ?) ")
        End With

        db.AddParameter("@RoleId", RoleId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@EmpId", EmpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strMessageListString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
        End Try
        da.Fill(ds, "SelectedList")
        db.CloseConnection()
        db.ClearParameters()
        Return ds
    End Function
    Public Function AddRecord(ByVal RoleId As Guid, ByVal EmpId As Guid, ByVal StudentId As Guid)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("INSERT INTO cmRoleEmpStudent")
            .Append("(RoleId, EmpId, StudentId) ")
            .Append("VALUES(?,?,?)")
        End With
        db.AddParameter("@roleid", RoleId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@empid", EmpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@studentid", StudentId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
    End Function

    Public Function DeleteRecord(ByVal RoleId As Guid, ByVal EmpId As Guid, ByVal StudentId As Guid)

        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("DELETE FROM cmRoleEmpStudent WHERE")
            .Append(" RoleId = ? AND ")
            .Append(" EmpId = ? AND ")
            .Append(" StudentId = ?")
        End With
        db.AddParameter("@roleid", RoleId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@empid", EmpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@studentid", StudentId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
    End Function
End Class
