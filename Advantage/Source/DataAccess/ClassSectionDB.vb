Imports System.Data
Imports System.Data.OleDb
Imports System.Text
Imports System.Xml
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common


Public Class ClassSectionDB

    Private ReadOnly myAdvAppSettings As AdvAppSettings
    Private ReadOnly dbSqlDataAccess As SQLDataAccess
    Private ReadOnly dbDataAccess As DataAccess

    Sub New()
        'Get settings 
        myAdvAppSettings = AdvAppSettings.GetAppSettings()

        'Create acceso a datos
        dbSqlDataAccess = New SQLDataAccess()
        dbDataAccess = New DataAccess()
    End Sub


    Public Function GetDropDownList(ByVal campusId As String) As DataSet
       Dim ds As New DataSet
        Dim strSqlString As New StringBuilder
        dbDataAccess.OpenConnection()
        Dim da4 As OleDbDataAdapter

        With strSqlString
            .Append("Select WorkDaysID, WorkDaysDescrip from plWorkDays ORDER BY ViewOrder")
        End With
        da4 = dbDataAccess.RunParamSQLDataAdapter(strSqlString.ToString)
        Try
            da4.Fill(ds, "DayDT")
        Catch ex As Exception

        End Try

        strSqlString.Remove(0, strSqlString.Length)


        Dim da5 As OleDbDataAdapter

        With strSqlString
            .Append("Select TimeIntervalID, TimeIntervalDescrip from cmTimeInterval  where ")
            .Append(" CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) AND ")
            .Append(" StatusId=(select StatusId from syStatuses where Status='Active') order by TimeIntervalDescrip")
        End With
        dbDataAccess.AddParameter("@cmpid2", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        da5 = dbDataAccess.RunParamSQLDataAdapter(strSqlString.ToString)
        Try
            da5.Fill(ds, "TimeIntervalList")
        Catch ex As Exception

        End Try

        strSqlString.Remove(0, strSqlString.Length)

        Dim da8 As OleDbDataAdapter

        With strSqlString
            .Append("SELECT distinct a.UserId,a.FullName ")
            .Append("FROM syUsers a, syUsersRolesCampGrps b, syRoles c ")
            .Append("WHERE a.UserId = b.UserId ")
            .Append("AND b.RoleId = c.RoleId ")
            .Append("AND c.SysRoleId = 2 and a.AccountActive=1 ")
            .Append("AND	CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
            .Append("ORDER BY a.FullName")
        End With

        dbDataAccess.AddParameter("@cmpid2", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        da8 = dbDataAccess.RunParamSQLDataAdapter(strSqlString.ToString)

        Try
            da8.Fill(ds, "InstructorDT")
        Catch ex As Exception

        End Try

        strSqlString.Remove(0, strSqlString.Length)
        dbDataAccess.ClearParameters()

        Dim da10 As OleDbDataAdapter

        With strSqlString
            .Append("SELECT StatusId,Status ")
            .Append("FROM syStatuses ")
        End With

        da10 = dbDataAccess.RunParamSQLDataAdapter(strSqlString.ToString)
        Try
            da10.Fill(ds, "StatusDT")
        Catch ex As Exception

        End Try
        strSqlString.Remove(0, strSqlString.Length)

        Dim da12 As OleDbDataAdapter

        With strSqlString
            .Append("SELECT PrgVerId,PrgVerDescrip ")
            .Append("FROM arPrgVersions ")
            .Append("ORDER BY PrgVerDescrip ")
        End With

        da12 = dbDataAccess.RunParamSQLDataAdapter(strSqlString.ToString)
        Try
            da12.Fill(ds, "PrgVersionsDT")
        Catch ex As Exception

        End Try
        strSqlString.Remove(0, strSqlString.Length)


        dbDataAccess.CloseConnection()
        Return ds
    End Function
    Public Function GetDropDownList(ByVal campusId As String, ByVal prgVerId As String) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da1 As OleDbDataAdapter
        Dim strSqlString As New StringBuilder

        With strSqlString
            .Append("SELECT t1.TermId,t1.TermDescrip ")
            .Append("FROM arTerm t1  ")
            '.Append("WHERE t2.Status = 'Active' and t1.StatusId = t2.StatusId ")
        End With
        db.OpenConnection()
        da1 = db.RunParamSQLDataAdapter(strSqlString.ToString)
        Try
            da1.Fill(ds, "TermDT")
        Catch ex As Exception

        End Try
        strSqlString.Remove(0, strSqlString.Length)


        Dim da2 As OleDbDataAdapter
        With strSqlString
            If prgVerId = "" Then
                .Append("Select ReqId,Descrip,Code  from arReqs Where ReqTypeId = 1 ")
                .Append("ORDER BY Code,Descrip")

                da2 = db.RunParamSQLDataAdapter(strSqlString.ToString)
                Try
                    da2.Fill(ds, "CourseDT")
                Catch ex As Exception

                End Try

            Else
                'Special Case: retrieve only courses on the program version definition
                Dim dtCourses As DataTable = (New TermProgressDB).GetCoursesForProgramVersion(prgVerId)
                dtCourses.TableName = "CourseDT"
                ds.Tables.Add(dtCourses.Copy)
            End If
        End With
        strSqlString.Remove(0, strSqlString.Length)


        Dim da4 As OleDbDataAdapter
        With strSqlString
            .Append("Select WorkDaysID, WorkDaysDescrip from plWorkDays ORDER BY ViewOrder")
        End With
        da4 = db.RunParamSQLDataAdapter(strSqlString.ToString)
        Try
            da4.Fill(ds, "DayDT")
        Catch ex As Exception

        End Try
        strSqlString.Remove(0, strSqlString.Length)


        Dim da5 As OleDbDataAdapter
        With strSqlString
            .Append("Select TimeIntervalID, TimeIntervalDescrip from cmTimeInterval order by TimeIntervalDescrip")
        End With
        da5 = db.RunParamSQLDataAdapter(strSqlString.ToString)
        Try
            da5.Fill(ds, "TimeIntervalList")
        Catch ex As Exception

        End Try
        strSqlString.Remove(0, strSqlString.Length)


        Dim da6 As OleDbDataAdapter
        With strSqlString
            .Append("SELECT a.EmpId, a.LastName, a.FirstName ")
            .Append("FROM hrEmployees a ")
            .Append("WHERE a.Faculty = 1 ")
        End With
        da6 = db.RunParamSQLDataAdapter(strSqlString.ToString)
        Try
            da6.Fill(ds, "EmployeeDT")
        Catch ex As Exception

        End Try
        strSqlString.Remove(0, strSqlString.Length)


        Dim da9 As OleDbDataAdapter
        With strSqlString
            .Append("SELECT a.ShiftId, a.ShiftDescrip ")
            .Append("FROM arShifts a ")
        End With
        da9 = db.RunParamSQLDataAdapter(strSqlString.ToString)
        Try
            da9.Fill(ds, "ShiftDT")
        Catch ex As Exception

        End Try
        strSqlString.Remove(0, strSqlString.Length)


        Dim da8 As OleDbDataAdapter
        With strSqlString
            .Append("SELECT distinct a.UserId,a.FullName ")
            .Append("FROM syUsers a, syUsersRolesCampGrps b, syRoles c ")
            .Append("WHERE a.UserId = b.UserId ")
            .Append("AND b.RoleId = c.RoleId ")
            .Append("AND c.SysRoleId = 2 ")
            .Append("AND	CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
            .Append("ORDER BY a.FullName")
        End With
        db.AddParameter("@cmpid2", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        da8 = db.RunParamSQLDataAdapter(strSqlString.ToString)
        Try
            da8.Fill(ds, "InstructorDT")
        Catch ex As Exception

        End Try
        strSqlString.Remove(0, strSqlString.Length)
        db.ClearParameters()


        Dim da7 As OleDbDataAdapter
        With strSqlString
            .Append("SELECT a.CampusId, a.CampDescrip ")
            .Append("FROM syCampuses a where a.CampusId = ? ")
        End With
        db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        da7 = db.RunParamSQLDataAdapter(strSqlString.ToString)
        Try
            da7.Fill(ds, "CampusDT")
        Catch ex As Exception

        End Try
        strSqlString.Remove(0, strSqlString.Length)
        db.ClearParameters()

        Dim da10 As OleDbDataAdapter
        With strSqlString
            .Append("SELECT StatusId,Status ")
            .Append("FROM syStatuses ")
        End With
        da10 = db.RunParamSQLDataAdapter(strSqlString.ToString)
        Try
            da10.Fill(ds, "StatusDT")
        Catch ex As Exception

        End Try
        strSqlString.Remove(0, strSqlString.Length)


        Dim da11 As OleDbDataAdapter
        With strSqlString
            .Append("SELECT Descrip,GrdScaleId ")
            .Append("FROM arGradeScales ")
        End With
        da11 = db.RunParamSQLDataAdapter(strSqlString.ToString)
        Try
            da11.Fill(ds, "GrdScaleDT")
        Catch ex As Exception

        End Try
        strSqlString.Remove(0, strSqlString.Length)


        Dim da12 As OleDbDataAdapter
        With strSqlString
            If prgVerId = "" Then
                .Append("SELECT PrgVerId,PrgVerDescrip ")
                .Append("FROM arPrgVersions ")
                .Append("ORDER BY PrgVerDescrip ")
            Else
                'Special case: Retrieve only a particular program version
                .Append("SELECT PrgVerId,PrgVerDescrip ")
                .Append("FROM arPrgVersions ")
                .Append("WHERE PrgVerId=?")

                db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
        End With
        da12 = db.RunParamSQLDataAdapter(strSqlString.ToString)
        Try
            da12.Fill(ds, "PrgVersionsDT")
        Catch ex As Exception

        End Try
        strSqlString.Remove(0, strSqlString.Length)
        db.ClearParameters()

        db.CloseConnection()

        Return ds
    End Function
    ''OPtional Parameter CohortStartDate added by Saraswathi
    Public Function GetClsSectDataList(ByVal Term As String, ByVal Course As String, ByVal Instructor As String, ByVal Shift As String, ByVal CampusId As String, Optional ByVal CohortStartDate As String = "") As OleDbDataAdapter
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim strSql As New StringBuilder
        Dim objClsSect As OleDbDataAdapter


        With strSql
            .Append("Select a.ClsSectionId, a.TermId, a.ReqId, a.InstructorId,a.ShiftId, a.ClsSection, a.StartDate,a.EndDate, a.MaxStud, b.TermDescrip,c.Descrip,c.Code,a.InstrGrdBkWgtId from arClassSections a, arTerm b, arReqs c ")
            .Append(" where (a.TermId = b.TermId) ")
            .Append(" and (a.ReqId = c.ReqId) ")
            .Append(" and (a.CampusId = ?) ")
            If Term.ToString <> "" Then
                .Append(" and (a.TermId = ?) ")
            End If
            ''Added by Saraswathi lakshmanan
            If CohortStartDate.ToString <> "" Then
                .Append(" and (a.CohortStartDate = ?) ")
            End If

            If Course.ToString <> "" Then
                .Append(" and (a.ReqId = ?) ")
            End If
            If Instructor.ToString <> "" Then
                .Append(" and (a.InstructorId = ?) ")
            End If
            If Shift.ToString <> "" Then
                .Append(" and (a.ShiftId = ?) ")
            End If
            '.Append("and a.EndDate > ? ")
        End With
        db.AddParameter("@cmpid", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        If Term.ToString <> "" Then
            Dim termGuid As Guid = XmlConvert.ToGuid(Term)
            db.AddParameter("@Term", termGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        ''Added by Saraswathi lakshmanan
        If CohortStartDate.ToString <> "" Then
            db.AddParameter("@CohortStartDate", CohortStartDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        If Course.ToString <> "" Then
            Dim courseGuid As Guid = XmlConvert.ToGuid(Course)

            db.AddParameter("@Course", courseGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        If Instructor.ToString <> "" Then
            Dim instructGuid As Guid = XmlConvert.ToGuid(Instructor)

            db.AddParameter("@Instructor", instructGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        If Shift.ToString <> "" Then
            Dim shiftGuid As Guid = XmlConvert.ToGuid(Shift)

            db.AddParameter("@Shift", shiftGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        'db.AddParameter("@Sdate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        db.OpenConnection()
        Try
            objClsSect = db.RunParamSQLDataAdapter(strSql.ToString)
        Catch ex As Exception
            'Redirect to error page.
            Throw New Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        db.ClearParameters()
        Return objClsSect
    End Function
    Public Function GetCampusRooms(ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try
            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT a.BldgId, b.RoomId, b.Capacity,c.CampusId, b.Descrip ")
                .Append("FROM arBuildings a,arRooms b, syCmpGrpCmps c ")
                .Append("WHERE a.BldgId = b.BldgId ")
                .Append("AND a.CampGrpId = c.CampGrpId ")
                .Append("AND(c.CampusId = ?) ")
                .Append(" and b.StatusId=(select StatusId from syStatuses where Status='Active') ")

            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@CampId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "RoomDT")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception

        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    ''Optional Parameter CohortStartDate added by Saraswathi
    Public Function PopulateClsSectDataList(ByVal Term As String, ByVal Course As String, ByVal Instructor As String, ByVal Shift As String, ByVal CampusId As String, Optional ByVal dataset As DataSet = Nothing, Optional ByVal CohortStartDate As String = "") As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Try
            da = GetClsSectDataList(Term, Course, Instructor, Shift, CampusId, CohortStartDate)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        If dataset Is Nothing Then
            da.Fill(ds, "ClassSection")
            Return ds
        Else
            da.Fill(dataset, "ClassSection")
            Return dataset
        End If
    End Function

    Public Function GetTermModuleSummary(ByVal status As String, ByVal campusId As String) As DataSet
        Dim ds As DataSet
        Dim db As New DataAccess
        Dim strSql As New StringBuilder

        With strSql
            .Append("SELECT t1.TermId,t1.StartDate,t3.Descrip,t2.ClsSection ")
            .Append("FROM arTerm t1, arClassSections t2, arReqs t3 ")
            .Append("WHERE t1.TermId = t2.TermId ")
            .Append("AND t2.ReqId = t3.ReqId ")
            .Append("AND t1.IsModule = 1 ")
            .Append("AND t2.CampusId = ? ")
        End With

        If status <> "" Then
            strSql.Append("AND t1.StatusId = ? ")
        End If

        db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        If status <> "" Then
            db.AddParameter("@status", status, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        ds = db.RunParamSQLDataSet(strSql.ToString)

        Return ds

    End Function

    Public Function GetTermModuleSummary(ByVal prgVerId As String, ByVal status As String, ByVal campusId As String) As DataSet
        Dim ds As DataSet
        Dim db As New DataAccess
        Dim strSql As New StringBuilder

        With strSql
            .Append("SELECT t1.TermId,t1.StartDate,t3.Descrip,t2.ClsSection ")
            .Append("FROM arTerm t1, arClassSections t2, arReqs t3 ")
            .Append("WHERE t1.TermId = t2.TermId ")
            .Append("AND t2.ReqId = t3.ReqId ")
            .Append("AND t1.IsModule = 1 ")
            .Append("AND t2.CampusId = ? ")
            .Append("AND t2.ReqId IN( ")
            .Append("SELECT distinct t400.ReqId as ReqId ")
            .Append("FROM arReqs t3,arProgVerDef t400 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t400.PrgVerId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t700.ReqId as ReqId ")
            .Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t400.PrgVerId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t800.ReqId as ReqId ")
            .Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t400.PrgVerId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t900.ReqId as ReqId ")
            .Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append(" t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t400.PrgVerId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t1000.ReqId as ReqId ")
            .Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t400.PrgVerId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t1100.ReqId as ReqId ")
            .Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000, arReqGrpDef t1100 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t1000.ReqId = t1100.GrpId AND ")
            .Append("t400.PrgVerId = ? ")
            .Append(") ")
        End With

        If status <> "" Then
            strSql.Append("AND t1.StatusId = ? ")
        End If

        db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        If status <> "" Then
            db.AddParameter("@status", status, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        ds = db.RunParamSQLDataSet(strSql.ToString)

        Return ds
    End Function

    Public Function AddClassSection(ByVal ClassSectionObject As ClassSectionInfo, ByVal user As String) As ClsSectionResultInfo
        Dim db As New DataAccess
        Dim strSql As New StringBuilder
        Dim count As Integer
        Dim resultInfo As New ClsSectionResultInfo


        '   Validate uniqueness of class section.
        '   Get the number of class section with same values in these fields: ClsSection,TermId,ReqId and CampusId
        count = DoesClsSectionExist(ClassSectionObject, ClassSectionObject.TermId.ToString)

        If count >= 1 Then
            resultInfo.Exists = True
            Return resultInfo
            'Exit Function
        End If


        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try
            With strSql
                .Append("INSERT INTO arClassSections ")
                .Append("(ClsSectionId, TermId, ReqId, InstructorId,ShiftId,CampusId, GrdScaleId, ClsSection, StartDate, EndDate, MaxStud,ModUser,ModDate,CohortStartDate) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?);")
                .Append("SELECT COUNT(*) FROM arClassSections WHERE ClsSectionId=? and ModDate=? ")
            End With

            ' Set the DateCreated time to now
            db.AddParameter("@clsSectid", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.AddParameter("@termid", ClassSectionObject.TermId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.AddParameter("@courseid", ClassSectionObject.CourseId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            If ClassSectionObject.InstructorId2.ToString = Guid.Empty.ToString Or ClassSectionObject.InstructorId2.ToString = "" Then
                db.AddParameter("@empid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@empid", ClassSectionObject.InstructorId2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If ClassSectionObject.ShiftId2.ToString = Guid.Empty.ToString Or ClassSectionObject.ShiftId2.ToString = "" Then
                db.AddParameter("@shiftid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@shiftid", ClassSectionObject.ShiftId2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            If ClassSectionObject.CampusId.ToString = Guid.Empty.ToString Or ClassSectionObject.CampusId.ToString = "" Then
                db.AddParameter("@campusid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@campusid", ClassSectionObject.CampusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            End If
            db.AddParameter("@grdscaleid", ClassSectionObject.GrdScaleId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            db.AddParameter("@section", ClassSectionObject.Section, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@startdate", ClassSectionObject.StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            db.AddParameter("@enddate", ClassSectionObject.EndDate, DataAccess.OleDbDataType.OleDbDateTime, 16, ParameterDirection.Input)
            db.AddParameter("@maxstud", ClassSectionObject.MaxStud, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)

            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Dim now As Date = Convert.ToDateTime(Date.Now.ToShortDateString)
            Dim strnow As Date
            strnow = Date.Now
            Dim sDate As String = strnow.Date.ToShortDateString
            Dim sHour As String = strnow.Hour.ToString
            Dim sMinute As String = strnow.Minute.ToString
            Dim iSecs As Integer = strnow.Second
            strnow = Convert.ToDateTime(sDate + " " + sHour + ":" + sMinute + ":" + iSecs.ToString)

            'ModDate
            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'CohortStartDate
            If ClassSectionObject.CohortStartDate = Date.MaxValue Or ClassSectionObject.CohortStartDate = Date.MinValue Then
                db.AddParameter("@cohortstartdate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@cohortstartdate", ClassSectionObject.CohortStartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            End If


            db.AddParameter("@clsSectid", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            'ModDate
            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)



            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(strSql.ToString, groupTrans)
            'db.RunParamSQLExecuteNoneQuery(strSQL.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If Not (rowCount = 1) Then

                '   rollback transaction
                groupTrans.Rollback()

                '   return an error message
                resultInfo.ErrorString = DALExceptions.BuildConcurrencyExceptionMessage()
                Return resultInfo
            End If

            db.ClearParameters()
            strSql.Remove(0, strSql.Length)

            With strSql
                .Append("INSERT INTO arClassSectionTerms(ClsSectTermId,ClsSectionId,TermId,ModDate,ModUser) ")
                .Append("VALUES(?,?,?,?,?);")
                .Append("SELECT COUNT(*) FROM arClassSectionTerms WHERE ClsSectTermId=? and ModDate=? ")
            End With

            Dim ClsSectTermId As Guid = Guid.NewGuid
            'ClsSectTermId
            db.AddParameter("@ClsSectTermId", ClsSectTermId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            'ClsSectionId
            db.AddParameter("@ClsSectionId", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            'TermId
            db.AddParameter("@TermId", ClassSectionObject.TermId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            'ModDate
            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.AddParameter("@ClsSectTermId", ClsSectTermId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            'ModDate
            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            rowCount = db.RunParamSQLScalar(strSql.ToString, groupTrans)

            '   If there were no updated rows then there was a concurrency problem
            If Not (rowCount = 1) Then

                '   rollback transaction
                groupTrans.Rollback()

                '   return an error message
                resultInfo.ErrorString = DALExceptions.BuildConcurrencyExceptionMessage()
                Return resultInfo
            End If


            ' 09/08/2011 JRobinson Clock Hour Project - Add new insert for arClsSectionTimeClockPolicy table

            db.ClearParameters()
            strSql.Remove(0, strSql.Length)

            With strSql
                .Append("INSERT INTO arClsSectionTimeClockPolicy(ClsSectionPolicyId,ClsSectionId,AllowEarlyIn,AllowLateOut,AllowExtraHours,CheckTardyIn,MaxInBeforeTardy,AssignTardyInTime) ")
                .Append("VALUES(?,?,?,?,?,?,?,?);")
                '.Append("SELECT COUNT(*) FROM arClassSectionTerms WHERE ModDate=? ")
            End With

            'ClsSectionPolicyId
            db.AddParameter("@ClsSectionPolicyId", Guid.NewGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            'ClsSectionId
            db.AddParameter("@ClsSectionId", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            'AllowEarlyIn
            db.AddParameter("@AllowEarlyIn", ClassSectionObject.AllowEarlyIn, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'AllowLateOut
            db.AddParameter("@AllowLateOut", ClassSectionObject.AllowLateOut, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'AllowExtraHours
            db.AddParameter("@AllowExtraHours", ClassSectionObject.AllowExtraHours, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'CheckTardyIn
            db.AddParameter("@CheckTardyIn", ClassSectionObject.CheckTardyIn, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'MaxInBeforeTardy
            db.AddParameter("@MaxInBeforeTardy", ClassSectionObject.MaxInBeforeTardy, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'AssignTardyInTime
            db.AddParameter("@AssignTardyInTime", ClassSectionObject.AssignTardyInTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)


            db.RunParamSQLExecuteNoneQuery(strSql.ToString, groupTrans)

            groupTrans.Commit()

            resultInfo.ModDate = strnow
            resultInfo.NotExists = True

            Return resultInfo

        Catch ex As OleDbException

            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return an error to the client
            resultInfo.ErrorString = DALExceptions.BuildErrorMessage(ex)
            Return resultInfo

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function UpdateClassSection(ByVal ClassSectionObject As ClassSectionInfo, ByVal user As String) As ClsSectionResultInfo
        Dim db As New DataAccess
        Dim strSql As New StringBuilder
        Dim resultInfo As New ClsSectionResultInfo
        Dim strnow As Date

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try
            With strSql
                .Append("UPDATE arClassSections SET ")
                .Append("TermId = ?")
                .Append(",ReqId = ?")
                .Append(",InstructorId = ?")
                .Append(",ShiftId = ?")
                .Append(",CampusId = ?")
                .Append(",GrdScaleId = ?")
                .Append(",ClsSection = ?")
                .Append(",StartDate = ?")
                .Append(",EndDate = ?")
                .Append(",MaxStud = ?")
                .Append(",ModUser=? ")
                .Append(",ModDate=? ")
                .Append(",CohortStartDate=? ")
                .Append(",InstrGrdBkWgtId=? ")
                .Append(" WHERE ClsSectionId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from arClassSections where ClsSectionId = ? and ModDate = ? ")
            End With


            db.AddParameter("@termid", ClassSectionObject.TermId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.AddParameter("@courseid", ClassSectionObject.CourseId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            If ClassSectionObject.InstructorId2.ToString = Guid.Empty.ToString Or ClassSectionObject.InstructorId2.ToString = "" Then
                db.AddParameter("@empid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@empid", ClassSectionObject.InstructorId2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If ClassSectionObject.ShiftId2.ToString = Guid.Empty.ToString Or ClassSectionObject.ShiftId2.ToString = "" Then
                db.AddParameter("@shiftid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@shiftid", ClassSectionObject.ShiftId2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            If ClassSectionObject.CampusId.ToString = Guid.Empty.ToString Or ClassSectionObject.CampusId.ToString = "" Then
                db.AddParameter("@campid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@campusid", ClassSectionObject.CampusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            End If
            db.AddParameter("@grdscaleid", ClassSectionObject.GrdScaleId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            db.AddParameter("@section", ClassSectionObject.Section, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@startdate", ClassSectionObject.StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            db.AddParameter("@enddate", ClassSectionObject.EndDate, DataAccess.OleDbDataType.OleDbDateTime, 16, ParameterDirection.Input)
            db.AddParameter("@maxstud", ClassSectionObject.MaxStud, DataAccess.OleDbDataType.OleDbInteger, 16, ParameterDirection.Input)
            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            'Troy: I commented out the code below on 8/15/2005. I think all we really need to do is drop the 
            'milliseconds from the date since that is what is causing the problem in the first place.
            strnow = Utilities.GetAdvantageDBDateTime(Date.Now)

            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   CohortStartDate
            If ClassSectionObject.CohortStartDate = Date.MinValue Or ClassSectionObject.CohortStartDate = Date.MaxValue Then
                db.AddParameter("@cohortstartdate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@cohortstartdate", ClassSectionObject.CohortStartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            End If

            If ClassSectionObject.InstrGrdBkWgtId.ToString = Guid.Empty.ToString Or ClassSectionObject.InstrGrdBkWgtId.ToString = "" Then
                db.AddParameter("@InstrGrdBkWgtId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@InstrGrdBkWgtId", ClassSectionObject.InstrGrdBkWgtId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            End If

            db.AddParameter("@clsSectid", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            '   ModDate
            db.AddParameter("@Original_ModDate", ClassSectionObject.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.AddParameter("@clsSectid", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            '   ModDate
            db.AddParameter("@Updated_ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)



            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(strSql.ToString, groupTrans)

            '   If there were no updated rows then there was a concurrency problem
            If Not (rowCount = 1) Then

                groupTrans.Rollback()

                'Return DALExceptions.BuildConcurrencyExceptionMessage()
                resultInfo.ErrorString = DALExceptions.BuildConcurrencyExceptionMessage()
                Return resultInfo
            End If

            db.ClearParameters()
            strSql.Remove(0, strSql.Length)

            With strSql
                .Append("UPDATE     arClassSectionTerms ")
                .Append("SET        TermId=?,ModDate=?,ModUser=? ")
                .Append("WHERE      ClsSectionId=? AND TermId=? ;")     'AND ModDate=?
                .Append("SELECT COUNT(*) FROM arClassSectionTerms WHERE ClsSectionId= ? AND TermId = ? and  ModDate=?")
            End With

            '   TermId
            db.AddParameter("@TermId", ClassSectionObject.TermId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            '   ModDate
            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '   ClsSectionId
            db.AddParameter("@ClsSectionId", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            '   Original_TermId
            db.AddParameter("@Original_TermId", ClassSectionObject.OriginalTermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            ''   Original_ModDate
            'db.AddParameter("@Original_ModDate", ClassSectionObject.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.AddParameter("@ClsSectionId", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            db.AddParameter("@TermId", ClassSectionObject.TermId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            '   ModDate
            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            rowCount = db.RunParamSQLScalar(strSql.ToString, groupTrans)

            '   If there were no updated rows then there was a concurrency problem
            If Not (rowCount = 1) Then

                groupTrans.Rollback()

                'Return DALExceptions.BuildConcurrencyExceptionMessage()
                resultInfo.ErrorString = DALExceptions.BuildConcurrencyExceptionMessage()
                Return resultInfo
            End If


            ' 09/12/2011 JRobinson Clock Hour Project - Add new insert/update for arClsSectionTimeClockPolicy table


            db.ClearParameters()
            strSql.Remove(0, strSql.Length)

            With strSql
                .Append("SELECT COUNT(*) FROM  arClsSectionTimeClockPolicy ")
                .Append("WHERE  ClsSectionId=? ")

            End With

            'ClsSectionId
            db.AddParameter("@ClsSectionId", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            rowCount = db.RunParamSQLScalar(strSql.ToString, groupTrans)

            db.ClearParameters()
            strSql.Remove(0, strSql.Length)

            If rowCount = 0 Then
                'insert
                With strSql
                    .Append("INSERT INTO arClsSectionTimeClockPolicy(ClsSectionPolicyId,ClsSectionId,AllowEarlyIn,AllowLateOut,AllowExtraHours,CheckTardyIn,MaxInBeforeTardy,AssignTardyInTime) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?);")
                    '.Append("SELECT COUNT(*) FROM arClassSectionTerms WHERE ModDate=? ")
                End With

                'ClsSectionPolicyId
                db.AddParameter("@ClsSectionPolicyId", Guid.NewGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                'ClsSectionId
                db.AddParameter("@ClsSectionId", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                'AllowEarlyIn
                db.AddParameter("@AllowEarlyIn", ClassSectionObject.AllowEarlyIn, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                'AllowLateOut
                db.AddParameter("@AllowLateOut", ClassSectionObject.AllowLateOut, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                'AllowExtraHours
                db.AddParameter("@AllowExtraHours", ClassSectionObject.AllowExtraHours, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                'CheckTardyIn
                db.AddParameter("@CheckTardyIn", ClassSectionObject.CheckTardyIn, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                'MaxInBeforeTardy
                db.AddParameter("@MaxInBeforeTardy", ClassSectionObject.MaxInBeforeTardy, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                'AssignTardyInTime
                db.AddParameter("@AssignTardyInTime", ClassSectionObject.AssignTardyInTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)


                db.RunParamSQLExecuteNoneQuery(strSql.ToString, groupTrans)

            Else
                ' update
                With strSql
                    .Append("UPDATE     arClsSectionTimeClockPolicy ")
                    .Append("SET        ClsSectionId=?,AllowEarlyIn=?,AllowLateOut=?,AllowExtraHours=?,CheckTardyIn=?,MaxInBeforeTardy=?,AssignTardyInTime=? ")
                    .Append("WHERE      ClsSectionId=? ;")

                End With

                'ClsSectionId
                db.AddParameter("@ClsSectionId", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                'AllowEarlyIn
                db.AddParameter("@AllowEarlyIn", ClassSectionObject.AllowEarlyIn, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                'AllowLateOut
                db.AddParameter("@AllowLateOut", ClassSectionObject.AllowLateOut, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                'AllowExtraHours
                db.AddParameter("@AllowExtraHours", ClassSectionObject.AllowExtraHours, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                'CheckTardyIn
                db.AddParameter("@CheckTardyIn", ClassSectionObject.CheckTardyIn, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                'MaxInBeforeTardy
                db.AddParameter("@MaxInBeforeTardy", ClassSectionObject.MaxInBeforeTardy, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                'AssignTardyInTime
                db.AddParameter("@AssignTardyInTime", ClassSectionObject.AssignTardyInTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                'ClsSectionId
                db.AddParameter("@ClsSectionId_1", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)


                db.RunParamSQLExecuteNoneQuery(strSql.ToString, groupTrans)

            End If

            '   return without errors

            groupTrans.Commit()

            resultInfo.ModDate = strnow

            Return resultInfo

        Catch ex As OleDbException

            groupTrans.Rollback()

            '   return an error to the client
            resultInfo.ErrorString = DALExceptions.BuildErrorMessage(ex)
            Return resultInfo

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function DeleteClassSection(ByVal ClsSectID As Guid, ByVal modDate As DateTime) As String
        Dim db As New DataAccess
        Dim strSql As New StringBuilder
        Dim rowCount As Integer

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            ' Clock Hour Project - Add new delete for arClsSectionTimeClockPolicy table

            With strSql
                .Append("SELECT COUNT(*) FROM arClsSectionTimeClockPolicy WHERE ClsSectionId=?")
            End With

            '   ClsSectionId
            db.AddParameter("@ClsSectionId", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            '   execute the query
            rowCount = db.RunParamSQLScalar(strSql.ToString, groupTrans)

            If rowCount = 1 Then

                db.ClearParameters()
                strSql.Remove(0, strSql.Length)

                With strSql
                    .Append("DELETE FROM arClsSectionTimeClockPolicy ")
                    .Append("WHERE ClsSectionId=?;")
                    .Append("SELECT COUNT(*) FROM arClsSectionTimeClockPolicy WHERE ClsSectionId=?")
                End With


                '   ClsSectionId
                db.AddParameter("@ClsSectionId", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                '   ClsSectionId
                db.AddParameter("@ClsSectionId_1", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

                '   execute the query
                rowCount = db.RunParamSQLScalar(strSql.ToString, groupTrans)

                '   If the row was not deleted then there was a concurrency problem
                If Not (rowCount = 0) Then
                    groupTrans.Rollback()

                    Return DALExceptions.BuildConcurrencyExceptionMessage()
                End If
            End If

            db.ClearParameters()
            strSql.Remove(0, strSql.Length)

            With strSql
                .Append("DELETE FROM arClassSectionTerms ")
                '.Append("WHERE ClsSectionId=? AND TermId=?;")       'ModDate=?
                .Append("WHERE ClsSectionId= ? AND ModDate = ?;")       'ModDate=?
                .Append("SELECT COUNT(*) FROM arClassSectionTerms WHERE ClsSectionId=?")
            End With

            '   ClsSectionId
            db.AddParameter("@ClsSectionId", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            '   ClsSectionId
            db.AddParameter("@ClsSectionId_1", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            '   execute the query
            rowCount = db.RunParamSQLScalar(strSql.ToString, groupTrans)

            '   If the row was not deleted then there was a concurrency problem
            If Not (rowCount = 0) Then
                groupTrans.Rollback()

                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

            db.ClearParameters()
            strSql.Remove(0, strSql.Length)

            With strSql
                .Append("DELETE FROM arClassSections WHERE ClsSectionId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM arClassSections WHERE ClsSectionId = ? ")
            End With
            db.AddParameter("@clssectionid", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@clssectionid2", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            '   execute the query
            rowCount = db.RunParamSQLScalar(strSql.ToString, groupTrans)

            '   If the row was not deleted then there was a concurrency problem
            If Not (rowCount = 0) Then
                groupTrans.Rollback()

                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

            groupTrans.Commit()

            Return ""

        Catch ex As OleDbException
            groupTrans.Rollback()
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
        'db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        'db.ClearParameters()
        'strSQL.Remove(0, strSQL.Length)
    End Function

    Public Function DeleteClassSection(ByVal ClsSectID As Guid, ByVal modDate As DateTime, ByVal termId As String) As String
        Dim db As New DataAccess
        Dim strSql As New StringBuilder
        Dim rowCount As Integer

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            ' 09/12/2011 JRobinson Clock Hour Project - Add new delete for arClsSectionTimeClockPolicy table

            With strSql
                .Append("SELECT COUNT(*) FROM arClsSectionTimeClockPolicy WHERE ClsSectionId=?")
            End With

            '   ClsSectionId
            db.AddParameter("@ClsSectionId", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            '   execute the query
            rowCount = db.RunParamSQLScalar(strSql.ToString, groupTrans)

            If rowCount = 1 Then

                db.ClearParameters()
                strSql.Remove(0, strSql.Length)

                With strSql
                    .Append("DELETE FROM arClsSectionTimeClockPolicy ")
                    .Append("WHERE ClsSectionId=?;")
                    .Append("SELECT COUNT(*) FROM arClsSectionTimeClockPolicy WHERE ClsSectionId=?")
                End With


                '   ClsSectionId
                db.AddParameter("@ClsSectionId", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                '   ClsSectionId
                db.AddParameter("@ClsSectionId_1", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

                '   execute the query
                rowCount = db.RunParamSQLScalar(strSql.ToString, groupTrans)

                '   If the row was not deleted then there was a concurrency problem
                If Not (rowCount = 0) Then
                    groupTrans.Rollback()

                    Return DALExceptions.BuildConcurrencyExceptionMessage()
                End If
            End If

            db.ClearParameters()
            strSql.Remove(0, strSql.Length)

            With strSql
                .Append("DELETE FROM arClassSectionTerms ")
                .Append("WHERE ClsSectionId=? AND TermId=?;")       ' AND ModDate=?
                .Append("SELECT COUNT(*) FROM arClassSectionTerms WHERE ClsSectionId=?")
            End With

            '   ClsSectionId
            db.AddParameter("@ClsSectionId", ClsSectID.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   TermId
            db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   ClsSectionId
            db.AddParameter("@ClsSectionId1", ClsSectID.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   execute the query
            rowCount = db.RunParamSQLScalar(strSql.ToString, groupTrans)

            '   If the row was not deleted then there was a concurrency problem
            If Not (rowCount = 0) Then
                groupTrans.Rollback()

                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

            db.ClearParameters()
            strSql.Remove(0, strSql.Length)

            With strSql
                .Append("DELETE FROM arClassSections ")
                .Append("WHERE ClsSectionId = ? AND ModDate = ?;")
                .Append("SELECT count(*) FROM arClassSections WHERE ClsSectionId = ? ")
            End With

            '   ClsSectionId
            db.AddParameter("@ClsSectionId", ClsSectID.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            '   ClsSectionId
            db.AddParameter("@ClsSectionId1", ClsSectID.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   execute the query
            rowCount = db.RunParamSQLScalar(strSql.ToString, groupTrans)

            '   If the row was not deleted then there was a concurrency problem
            If Not (rowCount = 0) Then
                groupTrans.Rollback()

                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

            groupTrans.Commit()

            Return ""

        Catch ex As OleDbException
            groupTrans.Rollback()
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function


    Public Function AddOverridenConflicts(ByVal clsSectionId As String, ByVal clsSectionList As ArrayList, ByVal arrDeleteList As ArrayList, ByVal user As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try
            '   Delete entries in arOverridenConflicts
            With sb
                .Append("DELETE FROM arOverridenConflicts ")
                .Append("WHERE LeftClsSectionId=? OR RightClsSectionId=? ")
            End With

            db.AddParameter("@LeftClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@RightClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            db.ClearParameters()

            If arrDeleteList.Count > 0 Then
                For i As Integer = 0 To arrDeleteList.Count - 1
                    db.AddParameter("@LeftClsSectionId", arrDeleteList(i).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@RightClsSectionId", arrDeleteList(i).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                    db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

                    db.ClearParameters()
                Next
            End If

            sb.Remove(0, sb.Length)

            '   Add entries in arOverridenConflicts
            If clsSectionList.Count > 0 Then

                With sb
                    .Append("INSERT INTO ")
                    .Append("   arOverridenConflicts(OverridenConflictId,LeftClsSectionId,RightClsSectionId,ModDate,ModUser) ")
                    .Append("VALUES(?,?,?,?,?)")
                End With

                Dim conflictInfo As OverridenConflictInfo
                Dim strnow As Date
                strnow = Date.Now

                Dim sDate As String = strnow.Date.ToShortDateString
                Dim sHour As String = strnow.Hour.ToString
                Dim sMinute As String = strnow.Minute.ToString
                Dim iSecs As Integer = strnow.Second
                strnow = Convert.ToDateTime(sDate + " " + sHour + ":" + sMinute + ":" + iSecs.ToString)

                For i As Integer = 0 To clsSectionList.Count - 1
                    conflictInfo = clsSectionList(i)

                    db.AddParameter("@OverridenConflictId", conflictInfo.OverridenConflictId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@LeftClsSectionId", conflictInfo.LeftClsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@RightClsSectionId", conflictInfo.RightClsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                    db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                    db.ClearParameters()
                Next

            End If

            groupTrans.Commit()

            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        End Try


    End Function

    Public Sub DeleteTerm(ByVal termId As String)
        Dim db As New DataAccess
        Dim strSql As New StringBuilder

        With strSql
            .Append("DELETE FROM arTerm ")
            .Append("WHERE TermId = ? ")
        End With

        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(strSql.ToString)

    End Sub

    Public Function AddSingleClsSectMeeting(ByVal ClsSectMeeting As ClsSectMeetingInfo, ByVal user As String) As ClsSectMtgIdObjInfo
        Dim db As New DataAccess
        Dim strSql As New StringBuilder
        Dim resinfo As New ClsSectMtgIdObjInfo

        ' 09/12/2011 JRobinson Clock Hour Project - Add new field

        Try
            With strSql
                .Append("INSERT INTO arClsSectMeetings ")
                .Append("(ClsSectMeetingId, ClsSectionId, WorkDaysId, RoomId, TimeIntervalId, EndIntervalId,ModUser,ModDate,PeriodId,AltPeriodId,startDate,EndDate,InstructionTypeID) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)")
            End With
            ' Set the DateCreated time to now
            db.AddParameter("@clsSectMtgid", ClsSectMeeting.ClsSectMtgId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.AddParameter("@clsSectid", ClsSectMeeting.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            If ClsSectMeeting.Day.ToString = "00000000-0000-0000-0000-000000000000" Then
                db.AddParameter("@Day", DBNull.Value, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            Else
                db.AddParameter("@Day", ClsSectMeeting.Day, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            End If

            db.AddParameter("@Room", ClsSectMeeting.Room, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            If ClsSectMeeting.StartTime.ToString = "00000000-0000-0000-0000-000000000000" Then
                db.AddParameter("@Stime", DBNull.Value, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            Else
                db.AddParameter("@Stime", ClsSectMeeting.StartTime, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            End If
            If ClsSectMeeting.EndTime.ToString = "00000000-0000-0000-0000-000000000000" Then
                db.AddParameter("@Etime", DBNull.Value, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            Else
                db.AddParameter("@Etime", ClsSectMeeting.EndTime, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            End If

            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            'Dim strnow As Date = Convert.ToDateTime(Date.Now.ToShortDateString)
            Dim strnow As Date
            strnow = Date.Now

            Dim sDate As String = strnow.Date.ToShortDateString
            Dim sHour As String = strnow.Hour.ToString
            Dim sMinute As String = strnow.Minute.ToString
            Dim iSecs As Integer = strnow.Second
            strnow = Convert.ToDateTime(sDate + " " + sHour + ":" + sMinute + ":" + iSecs.ToString)

            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            If ClsSectMeeting.PeriodId.ToString = "00000000-0000-0000-0000-000000000000" Then
                db.AddParameter("@PeriodId", DBNull.Value, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            Else
                db.AddParameter("@PeriodId", ClsSectMeeting.PeriodId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            End If
            If ClsSectMeeting.AltPeriodId.ToString = "00000000-0000-0000-0000-000000000000" Then
                db.AddParameter("@AltPeriodId", DBNull.Value, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            Else
                db.AddParameter("@AltPeriodId", ClsSectMeeting.AltPeriodId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            End If
            db.AddParameter("@StartDate", ClsSectMeeting.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@EndDate", ClsSectMeeting.EndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@InstructionTypeId", ClsSectMeeting.InstructionTypeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(strSql.ToString)
            db.ClearParameters()
            strSql.Remove(0, strSql.Length)
            '   return without errors
            resinfo.ModDate = strnow
            resinfo.ClsSectMtgId = ClsSectMeeting.ClsSectMtgId
            Return resinfo

        Catch ex As OleDbException
            '   return an error to the client
            'Return DALExceptions.BuildErrorMessage(ex)
            resinfo.ErrorString = DALExceptions.BuildErrorMessage(ex)
            Return resinfo
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Function UpdateSingleClsSectMtg(ByVal ClsSectMeeting As ClsSectMeetingInfo, ByVal user As String) As ClsSectMtgIdObjInfo
        Dim db As New DataAccess
        Dim strSql As New StringBuilder
        Dim resultInfo As New ClsSectMtgIdObjInfo
        Dim strnow As Date

        ' 09/12/2011 JRobinson Clock Hour Project - Add new field

        'Retrieve elements from the array list.
        Try
            With strSql
                .Append("UPDATE arClsSectMeetings SET ")
                .Append("WorkDaysId = ?")
                .Append(",RoomId = ?")
                .Append(",TimeIntervalId = ? ")
                .Append(",EndIntervalId = ? ")
                .Append(",ModUser=? ")
                .Append(",ModDate=? ")
                .Append(",StartDate=? ")
                .Append(",EndDate=? ")
                .Append(",InstructionTypeID=? ")
                .Append(" WHERE ClsSectMeetingId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("Select count(*) from arClsSectMeetings where ModDate = ? ")
                .Append("And ClsSectMeetingId = ? ")
            End With

            db.AddParameter("@dayid", ClsSectMeeting.Day, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.AddParameter("@roomid", ClsSectMeeting.Room, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.AddParameter("@timeintid", ClsSectMeeting.StartTime, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.AddParameter("@etimeintid", ClsSectMeeting.EndTime, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            strnow = Date.Now

            Dim sDate As String = strnow.Date.ToShortDateString
            Dim sHour As String = strnow.Hour.ToString
            Dim sMinute As String = strnow.Minute.ToString
            Dim iSecs As Integer = strnow.Second
            strnow = Convert.ToDateTime(sDate + " " + sHour + ":" + sMinute + ":" + iSecs.ToString)

            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.AddParameter("@StartDate", ClsSectMeeting.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@EndDate", ClsSectMeeting.EndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@InstructionTypeId", ClsSectMeeting.InstructionTypeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            db.AddParameter("@clsSectid", ClsSectMeeting.ClsSectMtgId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            '   ModDate
            db.AddParameter("@Original_ModDate", ClsSectMeeting.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.AddParameter("@clsSectid", ClsSectMeeting.ClsSectMtgId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(strSql.ToString)
            db.ClearParameters()
            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                'If 1 = 1 Then

                '   return without errors
                'Return ""

                resultInfo.ModDate = strnow
                resultInfo.ClsSectMtgId = ClsSectMeeting.ClsSectMtgId

                Return resultInfo
            Else
                'Return DALExceptions.BuildConcurrencyExceptionMessage()
                resultInfo.ErrorString = DALExceptions.BuildConcurrencyExceptionMessage()
                Return resultInfo
            End If

        Catch ex As OleDbException

            '   return an error to the client
            'DisplayOleDbErrorCollection(ex)
            resultInfo.ErrorString = DALExceptions.BuildErrorMessage(ex)
            Return resultInfo

        Finally
            'Close Connection
            'db.CloseConnection()
        End Try
    End Function

    Public Sub DeleteClsSectMeeting(ByVal ClsSectID As Guid)
        Dim db As New DataAccess
        Dim strSql As New StringBuilder
        With strSql
            .Append("DELETE FROM arClsSectMeetings WHERE ClsSectionId = ?")
        End With
        db.AddParameter("@clssectionid", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(strSql.ToString)
        db.ClearParameters()
        strSql.Remove(0, strSql.Length)

    End Sub
    Public Sub DeleteOneClsSectMeeting(ByVal ClsSectMtgID As Guid)
        Dim db As New DataAccess
        Dim strSql As New StringBuilder
        With strSql
            .Append("DELETE FROM arClsSectMeetings WHERE ClsSectMeetingId = ?")
        End With
        db.AddParameter("@clssectionmtgid", ClsSectMtgID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(strSql.ToString)
        db.ClearParameters()
        strSql.Remove(0, strSql.Length)

    End Sub

    Public Function GetSingleClsSect(ByVal ClsSectId As Guid) As DataSet
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim strSqlString As New StringBuilder
        With strSqlString
            .Append("Select * from arClassSections")
            .Append(" where ClsSectionId = ? ")
        End With
        Dim objSingleClsSect As OleDbDataAdapter
        db.OpenConnection()
        db.AddParameter("@clssectId", ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        objSingleClsSect = db.RunParamSQLDataAdapter(strSqlString.ToString)
        objSingleClsSect.Fill(ds, "SingleClsSect")
        strSqlString.Remove(0, strSqlString.Length)

        db.ClearParameters()
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetClassSection(ByVal ClsSectId As Guid) As DataSet
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.OpenConnection()

        With sb
            .Append("SELECT CCT.*, ")
            .Append("    (Select TermDescrip from arTerm where TermId=CCT.TermId) As Term, ")
            .Append("    (Select Descrip from arReqs where ReqId=CCT.ReqId) As Course, ")
            .Append("    (Select ShiftDescrip from arShifts where ShiftId=CCT.ShiftId) As Shift, ")
            .Append("    (Select FullName from syUsers where UserId=CCT.InstructorId) As Instructor, ")
            .Append("    (Select Descrip from arGradeScales where GrdScaleId=CCT.GrdScaleId) As GrdScale, ")
            .Append("    (Select CampDescrip from syCampuses where CampusId=CCT.CampusId) As Campus, ")
            .Append("    TCP.AllowEarlyIn, TCP.AllowLateOut, TCP.AllowExtraHours, TCP.CheckTardyIn, TCP.MaxInBeforeTardy, TCP.AssignTardyInTime ")

            .Append(" FROM arClassSections CCT ")
            .Append(" LEFT OUTER JOIN arClsSectionTimeClockPolicy TCP ON TCP.ClsSectionId = CCT.ClsSectionId ")
            .Append("WHERE CCT.ClsSectionId = ? ")
        End With

        db.AddParameter("@clssectId", ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

        Dim da As OleDbDataAdapter
        da = db.RunParamSQLDataAdapter(sb.ToString)
        da.Fill(ds, "SingleClsSect")

        sb.Remove(0, sb.Length)
        db.ClearParameters()


        With sb
            .Append("SELECT RightClsSectionId ")
            .Append("FROM arOverridenConflicts ")
            .Append("WHERE LeftClsSectionId = ? ")
        End With

        db.AddParameter("@ClsSectionId", ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

        Dim da1 As OleDbDataAdapter
        da1 = db.RunParamSQLDataAdapter(sb.ToString)
        da1.Fill(ds, "OverridenConflicts")
        sb.Remove(0, sb.Length)

        db.ClearParameters()
        db.CloseConnection()

        Return ds
    End Function

    Public Function GetOverridenConflicts(ByVal ClsSectId As String) As DataTable
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.OpenConnection()

        With sb
            .Append("SELECT RightClsSectionId ")
            .Append("FROM arOverridenConflicts ")
            .Append("WHERE LeftClsSectionId = ? ")
        End With

        db.AddParameter("@ClsSectionId", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim da1 As OleDbDataAdapter
        da1 = db.RunParamSQLDataAdapter(sb.ToString)
        da1.Fill(ds, "OverridenConflicts")

        db.CloseConnection()

        Return ds.Tables(0)
    End Function

    Public Sub DeleteOverridenConflicts(ByVal ClsSectId As String)
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.OpenConnection()

        With sb
            .Append("DELETE FROM arOverridenConflicts ")
            .Append("WHERE LeftClsSectionId = ? OR RightClsSectionId = ? ")
        End With

        db.AddParameter("@LeftClsSectionId", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@RightClsSectionId", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)

        db.CloseConnection()
    End Sub
    Public Sub DeleteStdToClsSect(ByVal ClsSectId As String, ByVal StuEnrollId As String)
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.OpenConnection()

        With sb
            .Append("DELETE FROM arResults ")
            .Append("WHERE TestId = ? and  stuEnrollId = ? ")
        End With

        db.AddParameter("@ClsSectionId", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)

        db.CloseConnection()
    End Sub

    Public Function DeleteOverridenConflicts(ByVal arrDeleteList As ArrayList) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try
            '   Delete entries in arOverridenConflicts
            With sb
                .Append("DELETE FROM arOverridenConflicts ")
                .Append("WHERE LeftClsSectionId=? OR RightClsSectionId=? ")
            End With

            If arrDeleteList.Count > 0 Then
                For i As Integer = 0 To arrDeleteList.Count - 1
                    db.AddParameter("@LeftClsSectionId", arrDeleteList(i).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@RightClsSectionId", arrDeleteList(i).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                    db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

                    db.ClearParameters()
                Next
            End If

            sb.Remove(0, sb.Length)

            groupTrans.Commit()

            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        End Try

    End Function
    Public Function GetClsSectForTermModule(ByVal termId As String) As DataSet
        Dim db As New DataAccess
        Dim strSqlString As New StringBuilder

        With strSqlString
            .Append("SELECT * FROM arClassSections ")
            .Append("WHERE TermId = ? ")
        End With

        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Return db.RunParamSQLDataSet(strSqlString.ToString)

    End Function

    Public Function GetSingleTerm(ByVal termId As String) As DataSet
        Dim db As New DataAccess
        Dim strSqlString As New StringBuilder

        With strSqlString
            .Append("SELECT TermId,TermCode,TermDescrip,StartDate,EndDate,StatusId ")
            .Append("FROM arTerm ")
            .Append("WHERE TermId = ? ")
        End With

        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Return db.RunParamSQLDataSet(strSqlString.ToString)

    End Function

    Public Function GetClsSectMtgs(ByVal ClsSectId As Guid) As DataSet
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim strSQLString As New StringBuilder
        Dim objSingleClsSectMtg As OleDbDataAdapter

        db.OpenConnection()


        With strSQLString

            .Append(" SELECT a.ClsSectMeetingId,a.WorkDaysId,a.RoomId,a.TimeIntervalId,a.ClsSectionId,a.EndIntervalId,a.ModDate,a.InstructionTypeId ,  ")
            .Append(" (SELECT TimeIntervalDescrip  ")
            .Append(" FROM cmTimeInterval b  ")
            .Append(" WHERE b.TimeIntervalId = a.TimeIntervalId) AS StartTime,  ")
            .Append(" (SELECT TimeIntervalDescrip ")
            .Append(" FROM cmTimeInterval c  ")
            .Append(" WHERE c.TimeIntervalId = a.EndIntervalId) AS EndTime,a.StartDate,a.EndDate,a.PeriodId,a.AltPeriodId,0 as vieworder, ")
            .Append(" (select COUNT(*) from dbo.atClsSectAttendance t where t.ClsSectMeetingId = a.ClsSectMeetingId) AS AttUsedCnt ")
            .Append(" FROM arClsSectMeetings a  ")
            .Append(" WHERE   a.ClsSectionId = ?  ")
            .Append(" and a.PeriodId is not null and a.PeriodId <> '00000000-0000-0000-0000-000000000000' order by ViewOrder ")

        End With

        db.AddParameter("@clssectId", ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

        objSingleClsSectMtg = db.RunParamSQLDataAdapter(strSQLString.ToString)
        objSingleClsSectMtg.Fill(ds, "ClsSectMtgs")
        strSQLString.Remove(0, strSQLString.Length)
        db.ClearParameters()
        db.CloseConnection()
        Return ds
    End Function


    Public Function GetClsSectMtgs_Sp(ByVal clsSectId As Guid) As DataSet
        Dim ds As New DataSet

        Dim strSqlString As New StringBuilder
        Dim objSingleClsSectMtg As SqlDataAdapter

        dbSqlDataAccess.OpenConnection()
        dbSqlDataAccess.AddParameter("@clssectId", clsSectId, SqlDbType.UniqueIdentifier, 16, ParameterDirection.Input)
        objSingleClsSectMtg = dbSqlDataAccess.RunParamSQLDataAdapter_SP("[USP_AR_RegStd_GetClsSectMeetings]")
        objSingleClsSectMtg.Fill(ds, "ClsSectMtgs")
        strSqlString.Remove(0, strSqlString.Length)
        dbSqlDataAccess.ClearParameters()
        dbSqlDataAccess.CloseConnection()
        Return ds
    End Function


    Public Function DoesClsSectionExist(ByVal ClsSection As ClassSectionInfo, ByVal Term As String) As Integer

        Dim rowCount As Integer

        Try

            '   connect to the database

            Dim db As New DataAccess

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder

            With sb

                .Append("Select count(*) as Count ")
                .Append("from arClassSections a ")
                .Append("where a.ClsSection = ? and a.TermId = ? ")
                .Append("and a.ReqId = ? ")
                .Append("and a.CampusId = ? ")

            End With


            ' Add the PrgVerId and ChildId to the parameter list

            db.AddParameter("@Section", ClsSection.Section, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@term", Term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@clssectionid", ClsSection.CourseId.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@cmpid", ClsSection.CampusId.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query

            '   execute the query
            rowCount = CInt(db.RunParamSQLScalar(sb.ToString))


            'Close Connection

            db.CloseConnection()


        Catch ex As Exception

            Throw New BaseException(ex.InnerException.ToString)

        End Try


        'Return the datatable in the dataset
        Return rowCount

    End Function

    Public Function GetStartEndDates(ByVal TermId As String) As String
        Dim sdate As String
        Dim edate As String
        Dim sDateEDate As String
        Try

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder



            With sb
                .Append("SELECT a.StartDate, a.EndDate ")
                .Append("FROM arTerm a ")
                .Append("WHERE a. TermId = ?  ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StdId", TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)



            '   build the sql query

            '   Execute the query
            db.OpenConnection()

            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                sdate = dr("StartDate")
                edate = dr("EndDate")
            End While
            sDateEDate = sdate & "," & edate

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return sDateEDate

    End Function

    Public Function DoesClsSectHaveGradesPosted(ByVal ClsSectId As String) As Integer

        Dim rowCount As Integer
        Dim rowcount2 As Integer

        '   connect to the database

        Dim db As New DataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder

        With sb

            .Append("Select count(*) as Count from arGrdBkResults a where a.ClsSectionId = ?")

        End With


        ' Add the stdenrollid and testid to the parameter list

        db.AddParameter("@Testid", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()

        Try
            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)
        Catch ex As Exception

            Throw New BaseException(ex.InnerException.ToString)

        End Try

        Dim da2 As New OleDbDataAdapter
        sb.Remove(0, sb.Length)

        If rowCount = 0 Then
            With sb
                .Append("Select count(*) as Count from arResults a where a.TestId = ? and a.GrdSysDetailId is not null ")
            End With

            Try
                'db.OpenConnection()
                rowcount2 = db.RunParamSQLScalar(sb.ToString)
            Catch ex As Exception

                Throw New BaseException(ex.InnerException.ToString)

            End Try
            db.CloseConnection()
            Return rowcount2
        End If



        'Close Connection

        db.CloseConnection()
        'Return the datatable in the dataset
        Return rowCount


    End Function

    Public Function StudentsRegisteredCount(ByVal ClsSectId As String) As Integer
        Dim rowCount As Integer

        '   connect to the database

        Dim db As New DataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb

            .Append("Select count(*)  from arResults a where a.TestId = ?")

        End With

        ' Add the stdenrollid and testid to the parameter list
        db.AddParameter("@Testid", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.OpenConnection()

        Try
            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)
        Catch ex As Exception

            Throw New BaseException(ex.InnerException.ToString)

        End Try

        db.CloseConnection()
        'Return the datatable in the dataset
        Return rowCount


    End Function
    Public Function GetMaxCapOnRoom(ByVal Room As String) As Integer
        Dim rowCount As Integer
        Try

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("Select Capacity from arRooms where RoomId = ? ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@BldgId", Room, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            db.OpenConnection()

            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try

        'Return the datatable in the dataset
        Return rowCount
    End Function
    Public Function GetDaysInPeriod(ByVal PeriodId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT a.WorkDayId, c.StartTimeId, c.EndTimeId ")
                .Append("FROM syPeriodsWorkDays a, syPeriods c ")
                .Append("WHERE a.PeriodId = ? ")
                .Append("AND a.PeriodId = c.PeriodId ")

            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@CampId", PeriodId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "PeriodDT")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception

        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetCourseForClassSection(ByVal clsSectionId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT cs.ReqId,rq.Descrip ")
            .Append("FROM arClassSections cs, arReqs rq ")
            .Append("WHERE ClsSectionId = ? ")
            .Append("AND cs.ReqId=rq.ReqId ")
        End With

        db.AddParameter("@csid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function

    Public Function GetClassesForTermBasedOnCourse(ByVal reqId As String, ByVal termId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT cs.InstructorId, cs.MaxStud, cs.StartDate, cs.EndDate, tm.TermDescrip, rq.Descrip ")
            .Append("FROM arClassSections cs, arClassSectionTerms cst, arTerm tm, arReqs rq ")
            .Append("WHERE cs.ClsSectionId=cst.ClsSectionId ")
            .Append("AND tm.TermId=cst.TermId ")
            .Append("AND cs.ReqId=rq.ReqId ")
            .Append("AND cs.ReqId =  ? ")
            .Append("AND cst.TermId = ? ")
        End With

        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function

    Public Function GetTermDates(ByVal termId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT TermDescrip, StartDate, EndDate ")
            .Append("FROM arTerm ")
            .Append("WHERE TermId = ? ")
        End With

        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
    End Function
    Private Function BuildInsertQueryForClassSections() As String
        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("insert into arClassSections (TermId, ReqId, ClsSection, StartDate, EndDate, MaxStud, CampusId, ShiftId, GrdScaleId, ModUser, ModDate) ")
            .Append("select ")
            .Append("		? as TermId, ")
            .Append("		R2.ReqId, ")
            .Append("		'A' as ClsSection, ")
            .Append("		(select StartDate from arTerm where TermId=?) As StartDate, ")
            .Append("		(select EndDate from arTerm where TermId=?) As EndDate, ")
            .Append("		'100' as MaxStud, ")
            .Append("       ? as CampusId, ")
            .Append("  		(select ShiftId from arPrograms where ProgId=(Select ProgId from arTerm where TermId= ? )) as ShiftId, ")
            .Append("		(select coalesce((select top 1 GrdScaleId from	arClassSections where ReqId=R2.ReqId),(select	top 1 GrdScaleId from arGradeScales))) As GrdScaleId, ")
            .Append("		? as ModUser, ")
            .Append("		? as ModDate ")
            .Append("from	arReqs R1, arReqGrpDef RGD, arReqs R2, syStatuses ST ")
            .Append("where ")
            .Append("		RGD.GrpId=R1.ReqId ")
            .Append("and	RGD.ReqId=R2.ReqId ")
            .Append("and    R1.StatusId=ST.StatusId ")
            .Append("and    R2.StatusId=ST.StatusId ")
            .Append("and    ST.Status = 'Active' ")
            .Append("and	RGD.GrpId=? ")
        End With
        Return sb.ToString()
    End Function

    ''Added BY Saraswathi Lakshmanan on May 12 2010
    ''To fix data issues for Ross issue 18921
    Private Function BuildInsertQueryForClassSections_CampusID() As String
        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("insert into arClassSections (TermId, ReqId, ClsSection, StartDate, EndDate, MaxStud, CampusId, ShiftId, GrdScaleId, ModUser, ModDate) ")
            .Append("select ")
            .Append("		? as TermId, ")
            .Append("		R2.ReqId, ")
            .Append("		'A' as ClsSection, ")
            .Append("		(select StartDate from arTerm where TermId=?) As StartDate, ")
            .Append("		(select EndDate from arTerm where TermId=?) As EndDate, ")
            .Append("		'100' as MaxStud, ")
            .Append("       SY1.CampusID as CampusId, ")
            .Append("  		(select ShiftId from arPrograms where ProgId=(Select ProgId from arTerm where TermId= ? )) as ShiftId, ")
            .Append("		(select coalesce((select top 1 GrdScaleId from	arClassSections where ReqId=R2.ReqId),(select	top 1 GrdScaleId from arGradeScales))) As GrdScaleId, ")
            .Append("		? as ModUser, ")
            .Append("		? as ModDate ")
            .Append("from	arReqs R1, arReqGrpDef RGD, arReqs R2, syStatuses ST ,syCmpGrpCmps SY1, syCmpGrpCmps SY2 ")
            .Append(" , (Select CampusId from syCmpGrpCmps where CampGrpId= ?  ) Table1 ")
            .Append("  where  ")
            .Append("		RGD.GrpId=R1.ReqId ")
            .Append(" and	RGD.ReqId=R2.ReqId ")
            .Append(" and    R1.StatusId=ST.StatusId ")
            .Append(" and    R2.StatusId=ST.StatusId ")
            .Append(" and    ST.Status = 'Active' ")
            .Append(" and	RGD.GrpId=? ")

            .Append("  and R1.CampGrpId=SY1.CampGrpId ")
            .Append(" and SY1.CampusId=SY2.CampusId ")
            .Append(" And R2.CampGrpId=SY2.CampGrpId ")
            .Append(" And SY1.CampusId=Table1.CampusId ")

        End With
        Return sb.ToString()
    End Function


    'Private Function BuildInsertQueryForClassSectionTerms(ByVal termId As String, ByVal user As String, ByVal dateNow As DateTime) As String
    Private Function BuildInsertQueryForClassSectionTerms() As String
        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("insert into arClassSectionTerms (ClsSectTermId,TermId, ClsSectionId,  ModUser, ModDate) values ")
            .Append("( ? , ? , ? , ? , ? )")
        End With
        Return sb.ToString()
    End Function
    Public Function GetClassSections(ByVal termId As String, ByVal user As String, ByVal dateNow As DateTime) As DataTable
        'Dim ds As New DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        With sb
            .Append(" select ClsSectionId from arClassSections where termid= ? ")
            .Append(" and ModUser = ?  and ModDate =  ? ")
        End With

        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '   ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   ModDate
        db.AddParameter("@ModDate", dateNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
    End Function
    Public Function InsertAllClassSectionsBelongingToACourseGroup(ByVal termId As String, ByVal groups() As String, ByVal campusId As String, ByVal user As String) As String
        '   Connect to the database
        Dim db As New DataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim db1 As New DataAccess
        db1.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Dim now As DateTime = Date.Now
        Dim sqlCommand As String = BuildInsertQueryForClassSections()
        Dim sqlCommand1 As String = BuildInsertQueryForClassSectionTerms()
        Dim dt As DataTable
        Dim rowCount As Integer = 0
        Dim dateNow As Date
        Try
            For i As Integer = 0 To groups.Length - 1
                dateNow = now
                BuildParameterListForInsertClassSectionQuery(db, termId, groups(i), campusId, user, now)
                db.RunParamSQLExecuteNoneQuery(sqlCommand, groupTrans)
            Next
            groupTrans.Commit()
            '   commit transaction 
            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Catch ex As Exception

            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return an error to the client
            Throw New Exception("Error Updating Term Fees", ex.InnerException)

        Finally
            'Close Connection
            db.CloseConnection()
            dt = GetClassSections(termId, user, dateNow)
            If dt.Rows.Count > 0 Then
                For rowCount = 0 To dt.Rows.Count - 1
                    BuildParameterListForInsertClassSectionTermsQuery(db1, termId, dt.Rows(rowCount)("ClsSectionId").ToString(), user, now)
                    db1.RunParamSQLExecuteNoneQuery(sqlCommand1)
                Next
                db1.CloseConnection()
            End If

        End Try
    End Function

    ''Added by SAraswathi lakshmanan on May 12 2010
    Public Function InsertAllClassSectionsBelongingToACourseGroup_CampusID(ByVal termId As String, ByVal groups() As String, ByVal CampGrpID As String, ByVal user As String) As String
        '   Connect to the database
        Dim db As New DataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim db1 As New DataAccess
        db1.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Dim now As DateTime = Date.Now
        Dim sqlCommand As String = BuildInsertQueryForClassSections_CampusID()
        Dim sqlCommand1 As String = BuildInsertQueryForClassSectionTerms()
        Dim dt As DataTable
        Dim rowCount As Integer
        Dim dateNow As Date
        Try
            For i As Integer = 0 To groups.Length - 1
                dateNow = now
                BuildParameterListForInsertClassSectionQuery_CampusID(db, termId, groups(i), CampGrpID, user, now)
                db.RunParamSQLExecuteNoneQuery(sqlCommand, groupTrans)
            Next
            groupTrans.Commit()
            '   commit transaction 

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Catch ex As Exception

            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return an error to the client
            Throw New Exception("Error Updating Term Fees", ex.InnerException)

        Finally
            'Close Connection
            db.CloseConnection()
            dt = GetClassSections(termId, user, dateNow)
            If dt.Rows.Count > 0 Then
                For rowCount = 0 To dt.Rows.Count - 1
                    BuildParameterListForInsertClassSectionTermsQuery(db1, termId, dt.Rows(rowCount)("ClsSectionId").ToString(), user, now)
                    db1.RunParamSQLExecuteNoneQuery(sqlCommand1)
                Next
                db1.CloseConnection()
            End If

        End Try
    End Function
    Private Sub BuildParameterListForInsertClassSectionQuery(ByVal db As DataAccess, ByVal termId As String, ByVal groupId As String, ByVal campusId As String, ByVal user As String, ByVal now As DateTime)
        '   add parameters values to the query
        db.ClearParameters()

        '   TermId
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   TermId
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   TermId
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   CampusId
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   TermId
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   ModDate
        db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        '   GroupId
        db.AddParameter("@GroupId", groupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    End Sub
    ''Modified by Saraswathi lakshmanan on May 12 2010
    ''For mantris case 18921

    Private Sub BuildParameterListForInsertClassSectionQuery_CampusID(ByVal db As DataAccess, ByVal termId As String, ByVal groupId As String, ByVal CAmpGrpID As String, ByVal user As String, ByVal now As DateTime)
        '   add parameters values to the query
        db.ClearParameters()

        '   TermId
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   TermId
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   TermId
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)



        '   TermId
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   ModDate
        db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '   CampusId
        db.AddParameter("@CampusId", CAmpGrpID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '   GroupId
        db.AddParameter("@GroupId", groupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    End Sub

    Private Sub BuildParameterListForInsertClassSectionTermsQuery(ByVal db As DataAccess, ByVal termId As String, ByVal clsSectionId As String, ByVal user As String, ByVal now As DateTime)
        '   add parameters values to the query
        db.ClearParameters()

        '   TermId
        'db.AddParameter("@ClsSectionId", ClsSectionID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Dim clsSectTermId As Guid = Guid.NewGuid
        'ClsSectTermId
        db.AddParameter("@ClsSectTermId", clsSectTermId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        '   TermId
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.AddParameter("@clSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        '   ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   ModDate
        db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)



    End Sub
    Public Function GetGroupsWithMissingClassSections(ByVal termId As String, Optional ByVal campusId As String = "") As DataTable
        '   Connect to the database
        Dim db As New DataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("select	distinct ")
            .Append("		R1.ReqId As GrpId, ")
            .Append("		R1.Descrip ")
            .Append("from	arReqs R1, arReqGrpDef RGD, arReqs R2, syStatuses ST ")
            .Append("where ")
            .Append("		RGD.GrpId=R1.ReqId ")
            .Append("and	RGD.ReqId=R2.ReqId ")
            .Append("and    R1.StatusId=ST.StatusId ")
            .Append("and	ST.Status='Active' ")
            .Append("and		RGD.GrpId not in ( ")
            .Append("				select distinct	  ")
            .Append("					RGD.GrpId ")
            .Append("				from arClassSections CS, arReqGrpDef RGD   ")
            .Append("				where TermId = ? ")
            If Not campusId = "" Then
                .Append("				and CampusId = ? ")
            End If
            .Append("				and		CS.ReqId=RGD.ReqId ")
            .Append("				group by CS.ReqId, RGD.GrpId ")
            .Append("				having count(*) > 0) ")
        End With

        '   TermId
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        If Not campusId = "" Then
            db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        '   return table
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
    End Function
    ''Addewd by SAraswathi lakshmanan on May 12 2010
    'new stored procedure to get the course groups available in current selelcted terms campus Group
    ''To fix mantis issue 18921 Data fix issue for Ross
    Public Function GetGroupsWithMissingClassSectionsForCampGrps_SP(ByVal termId As String, ByVal campGrpId As String) As DataTable
        '   Connect to the database
        Dim db As New DataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        ''Query modified on June 23 2010
        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("select	distinct ")
            .Append("		R1.ReqId As GrpId, ")
            .Append("		R1.Descrip ")
            .Append("from	arReqs R1, arReqGrpDef RGD, arReqs R2, syStatuses ST ")
            .Append("where ")
            .Append("		RGD.GrpId=R1.ReqId ")
            .Append("and	RGD.ReqId=R2.ReqId ")
            .Append("and    R1.StatusId=ST.StatusId ")
            .Append("and	ST.Status='Active' ")
            .Append("and		RGD.GrpId not in ( ")
            .Append("				select distinct	  ")
            .Append("					RGD.GrpId ")
            .Append("				from arClassSections CS, arReqGrpDef RGD   ")
            .Append("				where TermId = ? ")
            .Append("				and CampusId in ")

            .Append("   (Select table1.CampusID from  ")
            .Append(" (Select CampusId from syCmpGrpCmps where CampGrpId=? )Table1 Inner Join ")
            .Append("(Select CampusId from syCmpGrpCmps where CampGrpId in (Select CampGrpId from arReqs where arReqs.ReqId=CS.ReqId)) table2 ")
            .Append("on Table1.CampusId=table2.CampusId )")

            .Append("				and		CS.ReqId=RGD.ReqId ")
            .Append("				group by CS.ReqId, RGD.GrpId ")
            .Append("				having count(*) > 0) ")
            ''Added on June 23 2010
            ''To fix mantis case 18921 terms Page validation for Ross
            .Append("			   and RGD.GrpId  in ( select distinct	  			RGD.GrpId 				from  ")
            .Append("			 arReqGrpDef RGD ,arreqs t1 ,syCmpGrpCmps SCG ")
            .Append("			 	where	 t1.reqid=rgd.grpid and 	")
            .Append("			SCG.CampGrpId=t1.CampGrpId and SCG.CampusId  in    (Select CampusId from syCmpGrpCmps ")
            .Append("			where CampGrpId=?  ) ) ")


        End With

        '   TermId
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@CampusId", campGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@CampusId", campGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   return table
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
    End Function
    Public Function CheckScheduleConflictsWhileAddingClasses(ByVal ClsSectionId As String, _
                                                             ByVal ClsMeetingStartDate As Date,
                                                             ByVal ClsMeetingEndDate As Date, _
                                                             ByVal ClsMeetingRoomId As String, _
                                                             ByVal InstructorId As String, _
                                                             ByVal CurrentTermId As String, _
                                                             ByVal CurrentCourseId As String, _
                                                             ByVal CurrentPeriodId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As DataSet

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@CurrentClassSectionId", New Guid(ClsSectionId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ClsMeetingStartDate", ClsMeetingStartDate, SqlDbType.Date, , ParameterDirection.Input)
            db.AddParameter("@ClsMeetingEndDate", ClsMeetingEndDate, SqlDbType.Date, , ParameterDirection.Input)
            db.AddParameter("@ClsMeetingRoomId", New Guid(ClsMeetingRoomId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@InstructorId", New Guid(InstructorId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@CurrentTermId", New Guid(CurrentTermId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@CurrentCourseId", New Guid(CurrentCourseId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@CurrentPeriodId", New Guid(CurrentPeriodId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_ScheduleConflicts_AddingClass", "ScheduleConflictsDuringClass")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function CheckScheduleConflictsWhileAddingClassesArray(ByVal ClsSectionId As String, _
                                                            ByVal ClsMeetingStartDate As String,
                                                            ByVal ClsMeetingEndDate As String, _
                                                            ByVal ClsMeetingRoomId As String, _
                                                            ByVal InstructorId As String, _
                                                            ByVal CurrentTermId As String, _
                                                            ByVal CurrentCourseId As String, _
                                                            ByVal CurrentPeriodId As String, _
                                                            ByVal CurrentCampusId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As DataSet

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@CurrentClassSectionId", New Guid(ClsSectionId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ClsMeetingStartDate", ClsMeetingStartDate, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            db.AddParameter("@ClsMeetingEndDate", ClsMeetingEndDate, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            db.AddParameter("@ClsMeetingRoomId", New Guid(ClsMeetingRoomId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@InstructorId", New Guid(InstructorId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@CurrentTermId", New Guid(CurrentTermId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@CurrentCourseId", New Guid(CurrentCourseId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@CurrentPeriodId", New Guid(CurrentPeriodId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@CurrentCampusId", New Guid(CurrentCampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_ScheduleConflicts_AddingClass_Array", "ScheduleConflictsDuringClass")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function CheckScheduleConflictsWhileAddingClassesNoPeriods(ByVal ClsSectionId As String, _
                                                           ByVal ClsMeetingStartDate As String,
                                                           ByVal ClsMeetingEndDate As String, _
                                                           ByVal ClsMeetingRoomId As String, _
                                                           ByVal InstructorId As String, _
                                                           ByVal CurrentTermId As String, _
                                                           ByVal CurrentCourseId As String, _
                                                           ByVal WorkDayId As String, _
                                                           ByVal StartTimeId As String, _
                                                           ByVal EndTimeId As String, _
                                                           ByVal CurrentCampusId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As DataSet

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@CurrentClassSectionId", New Guid(ClsSectionId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ClsMeetingStartDate", ClsMeetingStartDate, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            db.AddParameter("@ClsMeetingEndDate", ClsMeetingEndDate, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            db.AddParameter("@ClsMeetingRoomId", New Guid(ClsMeetingRoomId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@InstructorId", New Guid(InstructorId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@CurrentTermId", New Guid(CurrentTermId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@CurrentCourseId", New Guid(CurrentCourseId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@WorkDayId", WorkDayId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            db.AddParameter("@StartTimeId", StartTimeId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            db.AddParameter("@EndTimeId", EndTimeId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            db.AddParameter("@CurrentCampusId", New Guid(CurrentCampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_ScheduleConflicts_AddingClass_NoPeriods", "ScheduleConflictsDuringClass")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function CheckScheduleConflictsDuringRegistration(ByVal clsSectionId As String, ByVal stuEnrollId As String) As DataSet
        Dim db As New DataAccess
        Dim ds As DataSet

        Try
            db.AddParameter("@CurrentClassSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 8000, ParameterDirection.Input)
            db.AddParameter("@StuEnrollID", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 8000, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet("dbo.USP_ScheduleConflicts_DuringRegistration", Nothing, "SP")
        Catch ex As Exception
            Return Nothing
        Finally
            db.Dispose()
        End Try

        Return ds

    End Function
    Public Function CheckScheduleConflictsForClassesInsideTerm(ByVal ClsSectionId As String, ByVal TargetTermId As String, ByVal CampusId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As DataSet

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@CurrentClassSectionId", ClsSectionId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            db.AddParameter("@TargetTermId", TargetTermId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            db.AddParameter("@CampusId", CampusId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_ConflictsClassesInsideSameTerm_GetList", "ScheduleConflictsDuringClass")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function CheckScheduleConflictsForClassesInsideTermIgnoreRooms(ByVal ClsSectionId As String, ByVal TargetTermId As String, ByVal CampusId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As DataSet


        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@CurrentClassSectionId", ClsSectionId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            db.AddParameter("@TargetTermId", TargetTermId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            db.AddParameter("@CampusId", CampusId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_ConflictsClassesInsideSameTermIgnoreRooms_GetList", "ScheduleConflictsDuringClassIgnoreRooms")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function CheckScheduleConflictsDuringCopy(ByVal ClsSectionId As String, ByVal TargetTermId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As DataSet

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@CurrentClassSectionId", ClsSectionId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            db.AddParameter("@TargetTermId", TargetTermId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_ScheduleConflicts_ModifyExistingClass", "ScheduleConflictsDuringClass")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function

    Public Function CheckScheduleConflictsWhileCopyingToAnotherTerm(ByVal ClsSectionId As String, ByVal SourceTermId As String, ByVal TargetTermId As String, ByVal CampusId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As DataSet

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@CurrentClassSectionId", ClsSectionId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            db.AddParameter("@SourceTermId", SourceTermId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            db.AddParameter("@TargetTermId", TargetTermId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            db.AddParameter("@CampusId", CampusId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_ScheduleConflicts_CopyClassSections", "ScheduleConflictsDuringClass")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function GetStudentInfo(ByVal StuEnrollId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As DataSet

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@StuEnrollId", StuEnrollId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_StudentByEnrollments_getList", "GetStudentInfo")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function GetTimeInterval(ByVal PeriodId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As DataSet

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            If String.IsNullOrEmpty(PeriodId) Then
                db.AddParameter("@PeriodId", DBNull.Value, SqlDbType.VarChar, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PeriodId", PeriodId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            End If

            ds = db.RunParamSQLDataSet_SP("dbo.USP_StartAndEndTimeByPeriod_GetList", "GetStartAndEndTime")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function GetWorkDaysOverlap(ByVal PeriodId1 As String, ByVal PeriodId2 As String) As Integer
        Dim db As New SQLDataAccess
        Dim ds As DataSet
        Dim intDaysOverlapCount As Integer = 0

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@PeriodId1", New Guid(PeriodId1), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@PeriodId2", New Guid(PeriodId2), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GetWorkDaysOverlap_Count", "GetWorkDaysOverlap")

            If Not ds Is Nothing Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    Try
                        If dr(0) >= 1 Then
                            intDaysOverlapCount = CType(dr(0), Integer)
                        End If
                    Catch ex As Exception
                        intDaysOverlapCount = 0
                    End Try
                Next
            End If
            Return intDaysOverlapCount
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function CheckScheduleConflictsWhileAddingClassesArrayWithInSameClass(ByVal ClsSectionId As String, _
                                                            ByVal ClsMeetingStartDate As String,
                                                            ByVal ClsMeetingEndDate As String, _
                                                            ByVal ClsMeetingRoomId As String, _
                                                            ByVal InstructorId As String, _
                                                            ByVal CurrentTermId As String, _
                                                            ByVal CurrentCourseId As String, _
                                                            ByVal CurrentPeriodId As String, _
                                                            ByVal CurrentCampusId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As DataSet

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@CurrentClassSectionId", New Guid(ClsSectionId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ClsMeetingStartDate", ClsMeetingStartDate, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            db.AddParameter("@ClsMeetingEndDate", ClsMeetingEndDate, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            db.AddParameter("@ClsMeetingRoomId", New Guid(ClsMeetingRoomId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@InstructorId", New Guid(InstructorId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@CurrentTermId", New Guid(CurrentTermId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@CurrentCourseId", New Guid(CurrentCourseId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@CurrentPeriodId", New Guid(CurrentPeriodId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@CurrentCampusId", New Guid(CurrentCampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_ScheduleConflicts_AddingClass_Array_InSameClass", "ScheduleConflictsForSameClass")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function CheckScheduleConflictsDuringRegistrationMan(ByVal ClsSectionId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As DataSet

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@CurrentClassSectionId", ClsSectionId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_ScheduleConflicts_DuringRegistrationMan", "ScheduleConflictsDuringClassMan")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
End Class
