Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' LateFeesDB.vb
'
' LateFeesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2006 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class LateFeesDB
    Public Function GetAllLateFees() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("	      (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("	      CCT.LateFeesId, ")
            .Append("	      CCT.StatusId, ")
            .Append("	      CCT.LateFeesCode, ")
            .Append("	      CCT.LateFeesDescrip, ")
            .Append("         CCT.EffectiveDate ")
            .Append("FROM     saLateFees CCT, syStatuses ST ")
            .Append("WHERE    CCT.StatusId = ST.StatusId ")
            .Append("ORDER BY ST.Status,CCT.LateFeesDescrip asc")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetCurrentLateFeesInfo(ByVal campusId As String) As LateFeesInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT top 1 ")
            .Append("	      CCT.LateFeesId ")
            .Append("FROM     saLateFees CCT, syStatuses ST ")
            .Append("WHERE    CCT.StatusId = ST.StatusId ")
            .Append("AND      CCT.EffectiveDate <= GetDate() ")
            .Append("AND      ST.Status = 'Active' ")
            .Append("AND      CampGrpId in (select CampGrpId from syCmpGrpCmps where CampusId=? ) ")
            .Append("ORDER BY CCT.EffectiveDate desc ")
        End With

        ' Add campusId to parameter list
        db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   return lateFeesInfo
        Dim lateFeesId As Object = db.RunParamSQLScalar(sb.ToString)
        If lateFeesId Is Nothing Then
            Return Nothing
        Else
            Return GetLateFeesInfo(CType(lateFeesId, Guid).ToString())
        End If

    End Function
    Public Function GetLateFeesInfo(ByVal LateFeesId As String) As LateFeesInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT CCT.LateFeesId, ")
            .Append("    CCT.LateFeesCode, ")
            .Append("    CCT.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=CCT.StatusId) As Status, ")
            .Append("    CCT.LateFeesDescrip, ")
            .Append("    CCT.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=CCT.CampGrpId) As CampGrpDescrip, ")
            .Append("    CCT.TransCodeId, ")
            .Append("    CCT.EffectiveDate, ")
            .Append("    CCT.FlatAmount, ")
            .Append("    CCT.Rate, ")
            .Append("    CCT.GracePeriod, ")
            .Append("    CCT.ModUser, ")
            .Append("    CCT.ModDate ")
            .Append("FROM  saLateFees CCT ")
            .Append("WHERE CCT.LateFeesId= ? ")
        End With

        ' Add the LateFeesId to the parameter list
        db.AddParameter("@LateFeesId", LateFeesId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim LateFeesInfo As New LateFeesInfo

        While dr.Read()

            '   set properties with data from DataReader
            With LateFeesInfo
                .LateFeesId = LateFeesId
                .IsInDB = True
                .Code = dr("LateFeesCode")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Description = dr("LateFeesDescrip")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                If Not (dr("TransCodeId") Is System.DBNull.Value) Then .TransCodeId = CType(dr("TransCodeId"), Guid).ToString()
                .EffectiveDate = dr("EffectiveDate")
                .FlatAmount = dr("FlatAmount")
                .Rate = dr("Rate")
                .GracePeriod = dr("GracePeriod")
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return LateFeesInfo
        Return LateFeesInfo

    End Function
    Public Function UpdateLateFeesInfo(ByVal LateFeesInfo As LateFeesInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saLateFees Set LateFeesId = ?, LateFeesCode = ?, ")
                .Append(" StatusId = ?, LateFeesDescrip = ?, CampGrpId = ?, ")
                .Append(" TransCodeId = ?, EffectiveDate = ?, FlatAmount = ?, ")
                .Append(" Rate = ?, GracePeriod = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE LateFeesId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saLateFees where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   LateFeesId
            db.AddParameter("@LateFeesId", LateFeesInfo.LateFeesId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   EnrollmentId
            db.AddParameter("@LateFeesCode", LateFeesInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", LateFeesInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   LateFeesDescrip
            db.AddParameter("@LateFeesDescrip", LateFeesInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If LateFeesInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", LateFeesInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   TransCodeId
            db.AddParameter("@TransCodeId", LateFeesInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Effective Date
            db.AddParameter("@EffectiveDate", LateFeesInfo.EffectiveDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   Flat Amount
            db.AddParameter("@FlatAmount", LateFeesInfo.FlatAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            '   Rate
            db.AddParameter("@Rate", LateFeesInfo.Rate, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            '   Grace Period
            db.AddParameter("@GracePeriod", LateFeesInfo.GracePeriod, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   LateFeesId
            db.AddParameter("@LateFeesId", LateFeesInfo.LateFeesId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", LateFeesInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If


        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)


        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddLateFeesInfo(ByVal LateFeesInfo As LateFeesInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saLateFees (LateFeesId, LateFeesCode, StatusId, ")
                .Append("   LateFeesDescrip, CampGrpId, TransCodeId, EffectiveDate, ")
                .Append("   FlatAmount, Rate, GracePeriod, ")
                .Append("   ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   LateFeesId
            db.AddParameter("@LateFeesId", LateFeesInfo.LateFeesId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   LateFeesCode
            db.AddParameter("@LateFeesCode", LateFeesInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", LateFeesInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   LateFeesDescrip
            db.AddParameter("@LateFeesDescrip", LateFeesInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If LateFeesInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", LateFeesInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   TransCodeId
            db.AddParameter("@TransCodeId", LateFeesInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Effective Date
            db.AddParameter("@EffectiveDate", LateFeesInfo.EffectiveDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   Flat Amount
            db.AddParameter("@FlatAmount", LateFeesInfo.FlatAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            '   Rate
            db.AddParameter("@Rate", LateFeesInfo.Rate, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            '   Grace Period
            db.AddParameter("@GracePeriod", LateFeesInfo.GracePeriod, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteLateFeesInfo(ByVal LateFeesId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saLateFees ")
                .Append("WHERE LateFeesId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM saLateFees WHERE LateFeesId = ? ")
            End With

            '   add parameters values to the query

            '   LateFeesId
            db.AddParameter("@LateFeesId", LateFeesId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   LateFeesId
            db.AddParameter("@LateFeesId", LateFeesId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If


        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
End Class