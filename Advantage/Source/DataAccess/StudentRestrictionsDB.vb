Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' StudentRestrictionsDB.vb
'
' StudentRestrictionsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class StudentRestrictionsDB
    Public Function GetAllStudentRestrictionTypes(ByVal showActiveOnly As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   SRT.RestrictionTypeId, SRT.StatusId, SRT.Code, SRT.Descrip, ")
            .Append("         SRT.Reason, SRT.EnableAlertButton, SRT.BlkGradeBook, ")
            .Append("         SRT.BlkAttendancePosting, SRT.BlkFA, SRT.BlkStuPortalClsSched, ")
            .Append("         SRT.BlkStuPortalGrdBk, SRT.BlkStuPortalResume, SRT.BlkStuPortalEntire, ")
            .Append("         SRT.BlkStuPortalCareer, SRT.RaiseToRestrictionId, SRT.DelayDays,ST.StatusId,ST.Status ")
            .Append("FROM     syStuRestrictionTypes SRT, syStatuses ST ")
            .Append("WHERE    SRT.StatusId = ST.StatusId ")
            If showActiveOnly = "True" Then
                .Append(" AND     ST.Status = 'Active' ")
                .Append("ORDER BY SRT.Descrip ")
            ElseIf showActiveOnly = "False" Then
                .Append(" AND     ST.Status = 'Inactive' ")
                .Append("ORDER BY SRT.Descrip ")
            Else
                .Append("ORDER BY ST.Status,SRT.Descrip asc")
            End If
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetStudentRestrictionTypeInfo(ByVal studentRestrictionTypeId As String) As StudentRestrictionTypeInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT SRT.RestrictionTypeId, ")
            .Append("    SRT.Code, ")
            .Append("    SRT.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=SRT.StatusId) As Status, ")
            .Append("    SRT.Descrip, ")
            .Append("    SRT.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=SRT.CampGrpId) As CampGrpdescrip, ")
            .Append("    SRT.Reason, ")
            .Append("    SRT.EnableAlertButton, ")
            .Append("    SRT.BlkGradeBook, ")
            .Append("    SRT.BlkAttendancePosting, ")
            .Append("    SRT.BlkFA, ")
            .Append("    SRT.BlkStuPortalClsSched, ")
            .Append("    SRT.BlkStuPortalGrdBk, ")
            .Append("    SRT.BlkStuPortalResume, ")
            .Append("    SRT.BlkStuPortalEntire, ")
            .Append("    SRT.BlkStuPortalCareer, ")
            .Append("    SRT.RaiseToRestrictionId, ")
            .Append("    (Select Descrip from syStuRestrictionTypes where RestrictionTypeId = SRT.RaiseToRestrictionId) As RaiseToRestriction, ")
            .Append("    SRT.DelayDays, ")
            .Append("    SRT.ModUser, ")
            .Append("    SRT.ModDate ")
            .Append("FROM  syStuRestrictionTypes SRT ")
            .Append("WHERE SRT.RestrictionTypeId= ? ")
        End With

        ' Add the StuRestrictionId to the parameter list
        db.AddParameter("@StuRestrictionId", studentRestrictionTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim StudentRestrictionTypeInfo As New StudentRestrictionTypeInfo

        While dr.Read()

            '   set properties with data from DataReader
            With StudentRestrictionTypeInfo
                .StudentRestrictionTypeId = studentRestrictionTypeId
                .IsInDB = True
                If Not dr("Code") Is System.DBNull.Value Then .Code = dr("Code")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Description = dr("Descrip")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpdescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                If Not (dr("Reason") Is System.DBNull.Value) Then .Reason = dr("Reason")
                .EnableAlertButton = dr("EnableAlertButton")
                .BlkGradeBook = dr("BlkGradeBook")
                .BlkAttendancePosting = dr("BlkAttendancePosting")
                .BlkFA = dr("BlkFA")
                .BlkStuPortalClsSched = dr("BlkStuPortalClsSched")
                .BlkStuPortalGrdBk = dr("BlkStuPortalGrdBk")
                .BlkStuPortalResume = dr("BlkStuPortalResume")
                .BlkStuPortalEntire = dr("BlkStuPortalEntire")
                .BlkStuPortalCareer = dr("BlkStuPortalCareer")
                If Not (dr("RaiseToRestrictionId") Is System.DBNull.Value) Then .RaiseToRestrictionId = CType(dr("RaiseToRestrictionId"), Guid).ToString
                If Not (dr("RaiseToRestriction") Is System.DBNull.Value) Then .RaiseToRestriction = dr("RaiseToRestriction")
                .DelayDays = dr("DelayDays")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return StudentRestrictionTypeInfo

    End Function
    Public Function UpdateStudentRestrictionTypeInfo(ByVal StudentRestrictionTypeInfo As StudentRestrictionTypeInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syStuRestrictionTypes Set RestrictionTypeId = ?, Code = ?, ")
                .Append("   StatusId = ?, Descrip = ?, CampGrpId = ?, ")
                .Append("   Reason = ?, EnableAlertButton = ?, BlkGradeBook = ?, ")
                .Append("   BlkAttendancePosting = ?, BlkFA = ?, BlkStuPortalClsSched = ?, ")
                .Append("   BlkStuPortalGrdBk = ?, BlkStuPortalResume = ?, BlkStuPortalEntire = ?, ")
                .Append("   BlkStuPortalCareer = ?, RaiseToRestrictionId = ?, DelayDays = ?, ")
                .Append("   ModUser = ?, ModDate = ? ")
                .Append("WHERE RestrictionTypeId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("Select count(*) from syStuRestrictionTypes where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   StudentRestrictionTypeId
            db.AddParameter("@RestrictionTypeId", StudentRestrictionTypeInfo.StudentRestrictionTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Code
            db.AddParameter("@StuRestrictionCode", StudentRestrictionTypeInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", StudentRestrictionTypeInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StudentRestrictionDescrip
            db.AddParameter("@StuRestrictionDescrip", StudentRestrictionTypeInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If StudentRestrictionTypeInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", StudentRestrictionTypeInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Reason
            If StudentRestrictionTypeInfo.Reason = "" Then
                db.AddParameter("@Reason", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 100, ParameterDirection.Input)
            Else
                db.AddParameter("@Reason", StudentRestrictionTypeInfo.Reason, DataAccess.OleDbDataType.OleDbString, 100, ParameterDirection.Input)
            End If

            '   EnableAlertButton
            db.AddParameter("@EnableAlertButton", StudentRestrictionTypeInfo.EnableAlertButton, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   BlkGradeBook
            db.AddParameter("@BlkGradeBook", StudentRestrictionTypeInfo.BlkGradeBook, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   BlkAttendancePosting
            db.AddParameter("@BlkAttendancePosting", StudentRestrictionTypeInfo.BlkAttendancePosting, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   BlkFA
            db.AddParameter("@BlkFA", StudentRestrictionTypeInfo.BlkFA, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   BlkStuPortalClsSched
            db.AddParameter("@BlkStuPortalClsSched", StudentRestrictionTypeInfo.BlkStuPortalClsSched, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   BlkStuPortalGrdBk
            db.AddParameter("@BlkStuPortalGrdBk", StudentRestrictionTypeInfo.BlkStuPortalGrdBk, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   BlkStuPortalResume
            db.AddParameter("@BlkStuPortalResume", StudentRestrictionTypeInfo.BlkStuPortalResume, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   BlkStuPortalEntire
            db.AddParameter("@BlkStuPortalEntire", StudentRestrictionTypeInfo.BlkStuPortalEntire, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   BlkStuPortalCareer
            db.AddParameter("@BlkStuPortalCareer", StudentRestrictionTypeInfo.BlkStuPortalCareer, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   RaiseToRestrictionId
            If StudentRestrictionTypeInfo.RaiseToRestrictionId = Guid.Empty.ToString Then
                db.AddParameter("@RaiseToRestrictionId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@RaiseToRestrictionId", StudentRestrictionTypeInfo.RaiseToRestrictionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   DelayDays
            db.AddParameter("@DelayDays", StudentRestrictionTypeInfo.DelayDays, DataAccess.OleDbDataType.OleDbInteger, 2, ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   StudentRestrictionTypeId
            db.AddParameter("@RestrictionTypeId", StudentRestrictionTypeInfo.StudentRestrictionTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", StudentRestrictionTypeInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddStudentRestrictionTypeInfo(ByVal StudentRestrictionTypeInfo As StudentRestrictionTypeInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT syStuRestrictionTypes (RestrictionTypeId, Code, StatusId, ")
                .Append("         Descrip, CampGrpId, ")
                .Append("         Reason, EnableAlertButton, BlkGradeBook, ")
                .Append("         BlkAttendancePosting, BlkFA, BlkStuPortalClsSched, ")
                .Append("         BlkStuPortalGrdBk, BlkStuPortalResume, BlkStuPortalEntire, ")
                .Append("         BlkStuPortalCareer, RaiseToRestrictionId, ")
                .Append("         DelayDays, ")
                .Append("         ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   StudentRestrictionTypeId
            db.AddParameter("@RestrictionTypeId", StudentRestrictionTypeInfo.StudentRestrictionTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StudentRestrictionCode
            If StudentRestrictionTypeInfo.Code = "" Then
                db.AddParameter("@Code", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)
            Else
                db.AddParameter("@Code", StudentRestrictionTypeInfo.Code, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)
            End If

            '   StatusId
            db.AddParameter("@StatusId", StudentRestrictionTypeInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StudentRestrictionDescrip
            db.AddParameter("@Descrip", StudentRestrictionTypeInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If StudentRestrictionTypeInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", StudentRestrictionTypeInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Reason
            If StudentRestrictionTypeInfo.Reason = "" Then
                db.AddParameter("@Reason", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 100, ParameterDirection.Input)
            Else
                db.AddParameter("@Reason", StudentRestrictionTypeInfo.Reason, DataAccess.OleDbDataType.OleDbString, 100, ParameterDirection.Input)
            End If

            '   EnableAlertButton
            db.AddParameter("@EnableAlertButton", StudentRestrictionTypeInfo.EnableAlertButton, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   BlkGradeBook
            db.AddParameter("@BlkGradeBook", StudentRestrictionTypeInfo.BlkGradeBook, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   BlkAttendancePosting
            db.AddParameter("@BlkAttendancePosting", StudentRestrictionTypeInfo.BlkAttendancePosting, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   BlkFA
            db.AddParameter("@BlkFA", StudentRestrictionTypeInfo.BlkFA, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   BlkStuPortalClsSched
            db.AddParameter("@BlkStuPortalClsSched", StudentRestrictionTypeInfo.BlkStuPortalClsSched, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   BlkStuPortalGrdBk
            db.AddParameter("@BlkStuPortalGrdBk", StudentRestrictionTypeInfo.BlkStuPortalGrdBk, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   BlkStuPortalResume
            db.AddParameter("@BlkStuPortalResume", StudentRestrictionTypeInfo.BlkStuPortalResume, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   BlkStuPortalEntire
            db.AddParameter("@BlkStuPortalEntire", StudentRestrictionTypeInfo.BlkStuPortalEntire, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   BlkStuPortalCareer
            db.AddParameter("@BlkStuPortalCareer", StudentRestrictionTypeInfo.BlkStuPortalCareer, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   RaiseToRestrictionId
            If StudentRestrictionTypeInfo.RaiseToRestrictionId = Guid.Empty.ToString Then
                db.AddParameter("@RaiseToRestrictionId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@RaiseToRestrictionId", StudentRestrictionTypeInfo.RaiseToRestrictionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   DelayDays
            db.AddParameter("@DelayDays", StudentRestrictionTypeInfo.DelayDays, DataAccess.OleDbDataType.OleDbInteger, 2, ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteStudentRestrictionTypeInfo(ByVal StudentRestrictionTypeId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syStuRestrictionTypes ")
                .Append("WHERE RestrictionTypeId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("Select count(*) from syStuRestrictionTypes where RestrictionTypeId = ? ")
            End With

            '   add parameters values to the query

            '   StudentRestrictionTypeId
            db.AddParameter("@RestrictionTypeId", StudentRestrictionTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   StudentRestrictionTypeId
            db.AddParameter("@StuRestrictionId", StudentRestrictionTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetAllGrpStuRestrictions(ByVal showActiveOnly As Boolean) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   SR.StuRestrictionId, SR.SGroupId, SR.RestrictionTypeId, SR.Descrip, ")
            .Append("         SR.Reason, SR.DepartmentId, SR.UserId, SR.CreateDate ")
            .Append("FROM     syStuRestrictions SR ")
            .Append("ORDER BY SR.Descrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    'Public Function GetGrpStuRestrictionInfo(ByVal stuRestrictionId As String) As GrpStuRestrictionInfo

    '    '   connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        '   with subqueries
    '        .Append("SELECT SR.StuRestrictionId, ")
    '        .Append("    SR.Descrip, ")
    '        .Append("    SR.SGroupId, ")
    '        .Append("    (Select Descrip from sySGroups where SGroupId=SR.SGroupId) As SGroup, ")
    '        .Append("    SR.RestrictionTypeId, ")
    '        .Append("    (Select Descrip from syStuRestrictionTypes where RestrictionTypeId=SR.RestrictionTypeId) as RestrictionType, ")
    '        .Append("    SR.Reason, ")
    '        .Append("    SR.DepartmentId, ")
    '        .Append("    (Select DepartmentDescrip from syDepartments where DepartmentId=SR.DepartmentId) As Department, ")
    '        .Append("    SR.UserId, ")
    '        .Append("    (Select FirstName + ' ' + LastName from hrEmployees where EmpId=SR.UserId) As UserName, ")
    '        .Append("    SR.CreateDate, ")
    '        .Append("    SR.ModUser, ")
    '        .Append("    SR.ModDate ")
    '        .Append("FROM  syStuRestrictions SR ")
    '        .Append("WHERE SR.StuRestrictionId= ? ")
    '    End With

    '    ' Add the StuRestrictionId to the parameter list
    '    db.AddParameter("@StuRestrictionId", stuRestrictionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    '   Execute the query
    '    Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

    '    Dim GrpStuRestrictionInfo As New GrpStuRestrictionInfo

    '    While dr.Read()

    '        '   set properties with data from DataReader
    '        With GrpStuRestrictionInfo
    '            .StuRestrictionId = stuRestrictionId
    '            .IsInDB = True
    '            .Descrip = dr("Descrip")
    '            .SGroupId = CType(dr("SGroupId"), Guid).ToString
    '            .SGroup = dr("SGroup")
    '            .RestrictionTypeId = CType(dr("RestrictionTypeId"), Guid).ToString
    '            .RestrictionType = dr("RestrictionType")
    '            .Reason = dr("Reason")
    '            .DepartmentId = CType(dr("DepartmentId"), Guid).ToString
    '            .Department = dr("Department")
    '            .UserId = CType(dr("UserId"), Guid).ToString
    '            .User = dr("UserName")
    '            .CreateDate = dr("CreateDate")
    '            .ModUser = dr("ModUser")
    '            .ModDate = dr("ModDate")
    '        End With

    '    End While

    '    If Not dr.IsClosed Then dr.Close()
    '    If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    '    '   Return BankInfo
    '    Return GrpStuRestrictionInfo

    'End Function
    Public Function UpdateGrpStuRestrictionInfo(ByVal GrpStuRestrictionInfo As GrpStuRestrictionInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syStuRestrictions Set StuRestrictionId = ?, SGroupId = ?, ")
                .Append("   RestrictionTypeId = ?, Descrip = ?, Reason = ?, ")
                .Append("   DepartmentId = ?, UserId = ?, CreateDate = ?, ")
                .Append("   ModUser = ?, ModDate = ? ")
                .Append("WHERE StuRestrictionId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("Select count(*) from syStuRestrictions where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   GrpStuRestrictionId
            db.AddParameter("@StuRestrictionId", GrpStuRestrictionInfo.StuRestrictionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   SGroupId
            db.AddParameter("@SGroupId", GrpStuRestrictionInfo.SGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   RestrictionTypeId
            db.AddParameter("@RestrictionTypeId", GrpStuRestrictionInfo.RestrictionTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Descrip
            db.AddParameter("@Descrip", GrpStuRestrictionInfo.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Reason
            db.AddParameter("@Reason", GrpStuRestrictionInfo.Reason, DataAccess.OleDbDataType.OleDbString, 100, ParameterDirection.Input)

            '   DepartmentId
            db.AddParameter("@DepartmentId", GrpStuRestrictionInfo.DepartmentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   UserId
            db.AddParameter("@UserId", GrpStuRestrictionInfo.UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CreateDate
            db.AddParameter("@CreateDate", GrpStuRestrictionInfo.CreateDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   GrpStuRestrictionId
            db.AddParameter("@StuRestrictionId", GrpStuRestrictionInfo.StuRestrictionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", GrpStuRestrictionInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddGrpStuRestrictionInfo(ByVal GrpStuRestrictionInfo As GrpStuRestrictionInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT syStuRestrictions (StuRestrictionId, SGroupId, RestrictionTypeId, ")
                .Append("         Descrip, Reason, DepartmentId, ")
                .Append("         UserId, CreateDate, ")
                .Append("         ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   GrpStuRestrictionId
            db.AddParameter("@StuRestrictionId", GrpStuRestrictionInfo.StuRestrictionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   SGroupId
            db.AddParameter("@SGroupId", GrpStuRestrictionInfo.SGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   RestrictionTypeId
            db.AddParameter("@RestrictionTypeId", GrpStuRestrictionInfo.RestrictionTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Descrip
            db.AddParameter("@Descrip", GrpStuRestrictionInfo.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Reason
            db.AddParameter("@Reason", GrpStuRestrictionInfo.Reason, DataAccess.OleDbDataType.OleDbString, 100, ParameterDirection.Input)

            '   DepartmentId
            db.AddParameter("@DepartmentId", GrpStuRestrictionInfo.DepartmentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   UserId
            db.AddParameter("@UserId", GrpStuRestrictionInfo.UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CreateDate
            db.AddParameter("@CreateDate", GrpStuRestrictionInfo.CreateDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteGrpStuRestrictionInfo(ByVal GrpStuRestrictionId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syStuRestrictions ")
                .Append("WHERE StuRestrictionId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("Select count(*) from syStuRestrictions where StuRestrictionId = ? ")
            End With

            '   add parameters values to the query

            '   GrpStuRestrictionId
            db.AddParameter("@StuRestrictionId", GrpStuRestrictionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   GrpStuRestrictionId
            db.AddParameter("@StuRestrictionId", GrpStuRestrictionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

End Class
