Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' AdvantageDB.vb
'
' AdvantageDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class AdvantageDB
    Public Function GetAdvantagePagesDS() As DataSet

        '   create dataset
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   build select query for the AdvantagePages table
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       t1.ResourceId, ")
            .Append("       t1.ResDefId, ")
            .Append("       t2.FldId, ")
            .Append("       t5.TblName as TableName, ")
            .Append("       t3.FldName as FieldName, ")
            .Append("       t1.Required as Required, ")
            .Append("       t3.DDLId, ")
            .Append("       t3.FldLen as FieldLength, ")
            .Append("       t4.FldType as FieldType, ")
            .Append("       t5.TblPK, ")
            .Append("       t6.Caption as Caption ")
            .Append("FROM ")
            .Append("       syResTblFlds t1, ")
            .Append("       syTblFlds t2, ")
            .Append("       syFields t3, ")
            .Append("       syFieldTypes t4, ")
            .Append("       syTables t5, ")
            .Append("       syFldCaptions t6, ")
            .Append("       syLangs t7 ")
            .Append("WHERE ")
            .Append("       (t1.TblFldsId = t2.TblFldsId) ")
            .Append("AND    (t2.FldId=t3.FldId) ")
            .Append("AND    (t2.TblId=t5.TblId) ")
            .Append("AND    (t3.FldTypeId=t4.FldTypeId) ")
            .Append("AND    (t2.FldId=t6.FldId) ")
            .Append("AND    (t6.LangId=t7.LangId) ")
            '.Append("AND    (t1.ResourceId=55) ")
            '.Append("AND    (t7.LangName= 'EN-US' ")

        End With

        '   build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
        'sc.Parameters.Add(New OleDbParameter("GrdSystemId", OleDbType.Guid, 16, "GrdSystemId"))

        '   Create adapter to handle AdvantagePages table
        Dim da As New OleDbDataAdapter(sc)

        '   Fill AdvantagePages table
        da.Fill(ds, "AdvantagePages")

        '   build select query for the Resources table
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       ResourceId, ")
            .Append("       Resource, ")
            .Append("       ResourceTypeId, ")
            .Append("       ResourceURL ")
            .Append("FROM ")
            .Append("       syResources ")
            .Append("ORDER BY ")
            .Append("       ResourceURL ")
        End With

        '   fill select command
        da.SelectCommand.CommandText = sb.ToString

        '   Fill AdvantagePages table
        da.Fill(ds, "Resources")

        '   create primary and foreign key constraints

        '   set primary key for AdvantagePages table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("Resources").Columns("ResourceId")
        ds.Tables("Resources").PrimaryKey = pk0

        '   set foreign key column in AdvantagePages
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("AdvantagePages").Columns("ResourceId")

        '   set foreign key in AdvantagePages table
        ds.Relations.Add(pk0, fk0)

        '   return dataset
        Return ds

    End Function

End Class
