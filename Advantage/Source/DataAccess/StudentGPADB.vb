﻿Imports FAME.Advantage.Common

Public Class StudentGPADB
#Region "Private Data Members"
    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")
    Private m_MinimumGPA As String = MyAdvAppSettings.AppSettings("MinimumGPA")
    Private m_GradeCourseRepetitionsMethod As String = MyAdvAppSettings.AppSettings("GradeCourseRepetitionsMethod")
    Private m_ShowProgramOnTranscript As Boolean = MyAdvAppSettings.AppSettings("ShowProgramOnTranscript")
    Private m_IncludeHoursForFailedGrade As String = MyAdvAppSettings.AppSettings("IncludeHoursForFailingGrade")


#End Region


#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

    Public ReadOnly Property MinimumGPA() As String
        Get
            Return m_MinimumGPA
        End Get
    End Property

    Public ReadOnly Property GradeCourseRepetitionsMethod() As String
        Get
            Return m_GradeCourseRepetitionsMethod
        End Get
    End Property

    Public ReadOnly Property ShowProgramOnTranscript() As Boolean
        Get
            Return m_ShowProgramOnTranscript
        End Get
    End Property

    Public ReadOnly Property IncludeHoursForFailedGrade() As Boolean
        Get
            If m_IncludeHoursForFailedGrade.ToLower = "true" Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

#End Region


#Region "Public Methods"

    'Public Function GetEnrollment(ByVal paramInfo As ReportParamInfo) As DataSet
    '    Dim sb As New System.Text.StringBuilder
    '    Dim db As New DataAccess
    '    Dim ds As DataSet
    '    Dim strWhere As String
    '    Dim strOrderBy As String
    '    Dim strStuID As String
    '    Dim strStuEnrollId As String

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    'Code Added by Vijay Ramteke on May, 06 2009
    '    Dim strTempWhere As String = ""
    '    Dim strTerm As String = ""
    '    Dim strClassDate As String = ""
    '    'Code Added by Vijay Ramteke on May, 06 2009

    '    If paramInfo.FilterList <> "" Then
    '        strWhere &= "AND " & paramInfo.FilterList
    '    End If

    '    'Get StudentId and rest of Where Clause from paramInfo.FilterOther
    '    'If paramInfo.FilterOther <> "" Then
    '    '    strWhere &= "AND " & paramInfo.FilterOther
    '    'End If

    '    'Get ProgVerId and Where Clause from paramInfo.FilterList
    '    If paramInfo.SORT_LastName = True Then
    '        strOrderBy &= " ORDER BY A.LastName "
    '    End If

    '    'Order By Clause.
    '    'If paramInfo.OrderBy <> "" Then
    '    '    strOrderBy &= " ORDER BY " & paramInfo.OrderBy
    '    'End If

    '    'Code Added by Vijay Ramteke on May, 06 2009
    '    strTempWhere = strWhere
    '    strTempWhere = strTempWhere.ToLower.Replace("and", ";")
    '    strWhere = ""
    '    Dim strArr As String() = strTempWhere.Remove(strTempWhere.IndexOf(";"), 1).Split(";")
    '    Dim i As Integer
    '    For i = 0 To strArr.Length - 1
    '        If strArr(i).ToLower.IndexOf("arterm") > 0 Then
    '            'strTerm &= strArr(i)
    '            'strWhere &= " AND " & strTerm
    '        ElseIf strArr(i).ToLower.IndexOf("arclasssections") > 0 Then
    '            strClassDate &= " AND " & strArr(i)
    '        Else
    '            strWhere &= " AND " & strArr(i)
    '        End If
    '    Next
    '    'Code Added by Vijay Ramteke on May, 06 2009
    '    'AND  arterm.termid = ''6354a63e-afa3-4317-8679-6bcb2d6726ff'''

    '    'Get student identifier depending on what field school is using.
    '    If StudentIdentifier = "SSN" Then
    '        strStuID = "A.SSN AS StudentIdentifier,"
    '    ElseIf StudentIdentifier = "EnrollmentId" Then
    '        strStuID = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
    '    ElseIf StudentIdentifier = "StudentId" Then
    '        strStuID = "A.StudentNumber AS StudentIdentifier,"
    '    End If

    '    With sb
    '        .Append("SELECT DISTINCT")
    '        .Append("       arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId,")
    '        .Append("       (SELECT PrgVerDescrip FROM arPrgVersions WHERE PrgVerId=arStuEnrollments.PrgVerId) AS PrgVerDescrip,")
    '        .Append("       (SELECT dg.DegreeDescrip FROM arPrgVersions pv, arDegrees dg WHERE pv.PrgVerId=arStuEnrollments.PrgVerId AND pv.DegreeId=dg.DegreeId) AS DegreeDescrip,")
    '        .Append("       (SELECT ProgDescrip FROM arPrograms X,arPrgVersions Y WHERE Y.PrgVerId=arStuEnrollments.PrgVerId AND Y.ProgId=X.ProgId) AS ProgramDescrip,")
    '        .Append("       A.LastName,A.FirstName,A.MiddleName,A.SSN,A.StudentNumber," & strStuID)
    '        .Append("       arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip,")
    '        .Append("       (SELECT SysStatusId FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS SysStatusId, ")
    '        .Append("       arStuEnrollments.StartDate,arStuEnrollments.ExpGradDate,arStuEnrollments.EnrollmentId,arStuEnrollments.LDA,arStuEnrollments.DateDetermined,")
    '        .Append("       A.DOB,(SELECT TOP 1 Address1 FROM arStudAddresses T,syStatuses Y WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS Address1,")
    '        .Append("       (SELECT TOP 1 Address2 FROM arStudAddresses T,syStatuses Y WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS Address2,")
    '        .Append("       (SELECT TOP 1 City FROM arStudAddresses T,syStatuses Y WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS City,")
    '        .Append("       (SELECT TOP 1 StateDescrip FROM arStudAddresses T,syStates S,syStatuses Y WHERE T.StudentId=A.StudentId AND T.StateId=S.StateId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS StateDescrip,")
    '        .Append("       (SELECT TOP 1 Zip FROM arStudAddresses T,syStatuses Y WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY Default1 DESC) AS Zip,")
    '        .Append("       (SELECT TOP 1 ForeignZip FROM arStudAddresses T,syStatuses Y WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS ForeignZip,")
    '        .Append("       (SELECT TOP 1 OtherState FROM arStudAddresses T,syStatuses Y WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS OtherState,")
    '        .Append("       (SELECT TOP 1 CountryDescrip FROM arStudAddresses T,syStatuses Y,adCountries C WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' AND T.CountryId=C.CountryId ORDER BY T.Default1 DESC) AS CountryDescrip,")
    '        .Append("       (SELECT TOP 1 Phone FROM arStudentPhone T,syStatuses Y WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS Phone,")
    '        .Append("       (SELECT TOP 1 ForeignPhone FROM arStudentPhone T,syStatuses Y WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS ForeignPhone, ")
    '        .Append("       (select max(PostDate) from arGrdBkResults where StuEnrollId=arStuEnrollments.stuEnrollId) AS ExtDate, ")
    '        '.Append("       (select max(RecordDate) from arStudentClockAttendance where StuEnrollId=arStuEnrollments.stuEnrollId) AS AttDate, ")
    '        .Append("(select Max(LDA) from ")
    '        .Append("( ")
    '        .Append("	select max(AttendedDate)as LDA from arExternshipAttendance where StuEnrollId=arStuEnrollments.stuEnrollId ")
    '        .Append("	union all ")
    '        .Append("	select max(MeetDate) as LDA from atClsSectAttendance where StuEnrollId=arStuEnrollments.stuEnrollId and Actual >= 1 ")
    '        .Append("	union all ")
    '        .Append("	select max(AttendanceDate) as LDA from atAttendance where EnrollId=arStuEnrollments.stuEnrollId and Actual >=1 ")
    '        .Append("	union all ")
    '        .Append("	select max(RecordDate) as LDA from arStudentClockAttendance where StuEnrollId=arStuEnrollments.stuEnrollId and (ActualHours >=1.00 and  ActualHours <> 99.00 and ActualHours <> 999.00 and ActualHours <> 9999.00) ")
    '        .Append("	union all ")
    '        .Append("	select max(MeetDate) as LDA from atConversionAttendance where StuEnrollId=arStuEnrollments.stuEnrollId and (Actual >=1.00 and  Actual <> 99.00 and Actual <> 999.00 and Actual <> 9999.00) ")
    '        '.Append(")TR) as AttDate ")
    '        'Code Added by Vijay Ramteke on May, 06 2009,Add new column ShowDate Issue on May, 11 2009
    '        .Append(")TR) as AttDate, ")
    '        '.Append("'" & IIf(strTerm <> "", strTerm.Replace("'", "''"), "") & "' AS TermCond, ")
    '        .Append("'' AS TermCond, ")
    '        .Append("'" & IIf(paramInfo.ShowRptDateIssue = True, "1", "0") & "' AS ShowDateIssue ")
    '        'Code Added by Vijay Ramteke on May, 06 2009
    '        .Append("FROM   arStuEnrollments,arStudent A, sycampgrps, arprgversions ")
    '        .Append("WHERE  arStuEnrollments.StudentId=A.StudentId AND arprgversions.PrgVerId=arStuEnrollments.PrgVerId AND arprgversions.CampGrpId = sycampgrps.campgrpid  ")
    '        .Append(strWhere)
    '        .Append(strOrderBy)
    '    End With

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '    db.OpenConnection()

    '    ds = db.RunParamSQLDataSet(sb.ToString)

    '    If ds.Tables.Count > 0 Then
    '        ds.Tables(0).TableName = "StudentTranscript"
    '        'Need to add these fields so they are to link the tables in report.
    '        ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
    '        ds.Tables(0).Columns.Add(New DataColumn("StrStuEnrollId", System.Type.GetType("System.String")))
    '        ds.Tables(0).Columns.Add(New DataColumn("FullAddress", System.Type.GetType("System.String")))
    '        ds.Tables(0).Columns.Add(New DataColumn("CreditsDesc", System.Type.GetType("System.String")))
    '        ds.Tables(0).Columns.Add(New DataColumn("SignDesc", System.Type.GetType("System.String")))
    '        ds.Tables(0).Columns.Add(New DataColumn("SuppressGrades", System.Type.GetType("System.String")))
    '        ds.Tables(0).Columns.Add(New DataColumn("SuppressStudentAddress", System.Type.GetType("System.String")))
    '        ds.Tables(0).Columns.Add(New DataColumn("SuppressStudentId", System.Type.GetType("System.String")))
    '        ds.Tables(0).Columns.Add(New DataColumn("SuppressLDA", System.Type.GetType("System.String")))
    '        ds.Tables(0).Columns.Add(New DataColumn("DisplayAsCourseCode", System.Type.GetType("System.String")))
    '        ds.Tables(0).Columns.Add(New DataColumn("SuppressDate", System.Type.GetType("System.String")))
    '        ds.Tables(0).Columns.Add(New DataColumn("IncludeAllCreditsAttempted", System.Type.GetType("System.String")))
    '        'Add new table.
    '        Dim dt As New DataTable("EnrollmentSummary")
    '        dt.Columns.Add(New DataColumn("StrStuEnrollId", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("TotalClasses", System.Type.GetType("System.Int32")))
    '        dt.Columns.Add(New DataColumn("TotalCreditsAttempted", System.Type.GetType("System.Decimal")))
    '        dt.Columns.Add(New DataColumn("TotalCreditsEarned", System.Type.GetType("System.Decimal")))
    '        dt.Columns.Add(New DataColumn("TotalHours", System.Type.GetType("System.Decimal")))
    '        dt.Columns.Add(New DataColumn("TotalGPA", System.Type.GetType("System.Decimal")))
    '        dt.Columns.Add(New DataColumn("ShowROSSOnlyTabsForStudent", System.Type.GetType("System.String")))
    '        ds.Tables.Add(dt)

    '        'Add new table.
    '        dt = New DataTable("EnrollmentCourses")
    '        dt.Columns.Add(New DataColumn("StrStuEnrollId", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("TermId", System.Type.GetType("System.Guid")))
    '        dt.Columns.Add(New DataColumn("TermDescrip", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("DescripXTranscript", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("ReqId", System.Type.GetType("System.Guid")))
    '        dt.Columns.Add(New DataColumn("Code", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("Descrip", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("ClsSection", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("Credits", System.Type.GetType("System.Decimal")))
    '        dt.Columns.Add(New DataColumn("CreditsAttempted", System.Type.GetType("System.Decimal")))
    '        dt.Columns.Add(New DataColumn("StartDate", System.Type.GetType("System.DateTime")))
    '        dt.Columns.Add(New DataColumn("EndDate", System.Type.GetType("System.DateTime")))
    '        dt.Columns.Add(New DataColumn("TestId", System.Type.GetType("System.Guid")))
    '        dt.Columns.Add(New DataColumn("GrdSysDetailId", System.Type.GetType("System.Guid")))
    '        dt.Columns.Add(New DataColumn("Grade", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("IsPass", System.Type.GetType("System.Boolean")))
    '        dt.Columns.Add(New DataColumn("GPA", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("IsCreditsAttempted", System.Type.GetType("System.Boolean")))
    '        dt.Columns.Add(New DataColumn("IsCreditsEarned", System.Type.GetType("System.Boolean")))
    '        dt.Columns.Add(New DataColumn("IsInGPA", System.Type.GetType("System.Boolean")))
    '        dt.Columns.Add(New DataColumn("CourseCategory", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("Hours", System.Type.GetType("System.Decimal")))
    '        dt.Columns.Add(New DataColumn("StrCourseCategory", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("TotalCredits", System.Type.GetType("System.Decimal")))
    '        dt.Columns.Add(New DataColumn("TotalHours", System.Type.GetType("System.Decimal")))
    '        dt.Columns.Add(New DataColumn("TermGPA", System.Type.GetType("System.Decimal")))
    '        dt.Columns.Add(New DataColumn("CumGPA", System.Type.GetType("System.Decimal")))
    '        dt.Columns.Add(New DataColumn("Score", System.Type.GetType("System.Decimal")))
    '        'Code Column DateIssue Addded By Vijay Ramteke on May, 11 2009
    '        dt.Columns.Add(New DataColumn("DateIssue", System.Type.GetType("System.DateTime")))
    '        dt.Columns.Add(New DataColumn("ShowDateIssue", System.Type.GetType("System.Int32")))
    '        'Code Column DateIssue Addded By Vijay Ramteke on May, 11 2009
    '        dt.Columns.Add(New DataColumn("TermCredits", System.Type.GetType("System.Decimal")))
    '        ds.Tables.Add(dt)

    '        'Add new table.
    '        dt = New DataTable("CourseCategoriesTotals")
    '        dt.Columns.Add(New DataColumn("StrStuEnrollId", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("CourseCategory", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("TotalCredits", System.Type.GetType("System.Decimal")))
    '        dt.Columns.Add(New DataColumn("TotalHours", System.Type.GetType("System.Decimal")))
    '        ds.Tables.Add(dt)

    '        '   Build ReportParams table, containing special information for Transcript report
    '        ''dt = GetTranscriptParams(paramInfo)
    '        ''ds.Tables.Add(dt.Copy)

    '        '   Build ReportParams table
    '        dt = New DataTable("ReportParams")
    '        dt.Columns.Add("CorporateName", System.Type.GetType("System.String"))
    '        dt.Columns.Add("FullAddress", System.Type.GetType("System.String"))
    '        dt.Columns.Add("Phone", System.Type.GetType("System.String"))
    '        dt.Columns.Add("Fax", System.Type.GetType("System.String"))
    '        dt.Columns.Add("SchoolLogo", System.Type.GetType("System.Byte[]"))
    '        dt.Columns.Add("StudentIdentifier", System.Type.GetType("System.String"))
    '        dt.Columns.Add("TranscriptAuthznTitle", System.Type.GetType("System.String"))
    '        dt.Columns.Add("TranscriptAuthznName", System.Type.GetType("System.String"))
    '        dt.Columns.Add("Website", System.Type.GetType("System.String"))
    '        dt.Columns.Add("SchoolName", System.Type.GetType("System.String"))
    '        dt.Columns.Add("OrderByString", System.Type.GetType("System.String"))
    '        dt.Columns.Add("FilterByString", System.Type.GetType("System.String"))
    '        dt.Columns.Add("FilterOtherString", System.Type.GetType("System.String"))
    '        dt.Columns.Add("ShowFilters", System.Type.GetType("System.Boolean"))
    '        dt.Columns.Add("ShowSortBy", System.Type.GetType("System.Boolean"))
    '        dt.Columns.Add("IsLetterGradeSchool", System.Type.GetType("System.Boolean"))
    '        dt.Columns.Add("TermAvgStart", System.Type.GetType("System.Decimal"))
    '        dt.Columns.Add("TermAvgEnd", System.Type.GetType("System.Decimal"))
    '        dt.Columns.Add("CumAvgStart", System.Type.GetType("System.Decimal"))
    '        dt.Columns.Add("CumAvgEnd", System.Type.GetType("System.Decimal"))
    '        dt.Columns.Add("TermCreditsStart", System.Type.GetType("System.Decimal"))
    '        dt.Columns.Add("TermCreditsEnd", System.Type.GetType("System.Decimal"))


    '        Dim dr As DataRow
    '        dr = dt.NewRow
    '        If Not (paramInfo.SchoolLogo Is Nothing) Then
    '            dr("SchoolLogo") = paramInfo.SchoolLogo
    '        End If
    '        dr("StudentIdentifier") = MyAdvAppSettings.AppSettings("StudentIdentifier") & ":"
    '        dt.Rows.Add(dr)
    '        ds.Tables.Add(dt)

    '        'Troy:2/26/2007:Added this to bring the letter grades and their gpa and ranges, if any.
    '        dt = New DataTable("GradeDescriptions")
    '        dt.Columns.Add("Grade", System.Type.GetType("System.String"))
    '        dt.Columns.Add("GPA", System.Type.GetType("System.String"))
    '        dt.Columns.Add("Range", System.Type.GetType("System.String"))
    '        dt.Columns.Add("GradeDescription", System.Type.GetType("System.String"))
    '        dt.Columns.Add("Quality", System.Type.GetType("System.String"))
    '        dt.Columns.Add("SuppressGradeDescription", System.Type.GetType("System.String"))
    '        ds.Tables.Add(dt)

    '        dt = New DataTable("Legend")
    '        dt.Columns.Add("GradeDescription", System.Type.GetType("System.String"))
    '        ds.Tables.Add(dt)

    '        ''Troy:2/27/2007:Term Progress with term and cummulative GPA
    '        'dt = New DataTable("TermProgress")
    '        'With dt
    '        '    .Columns.Add("TermId", Type.GetType("System.Guid"))
    '        '    .Columns.Add("TermDescrip", Type.GetType("System.String"))
    '        '    .Columns.Add("StartDate", Type.GetType("System.DateTime"))
    '        '    .Columns.Add("EndDate", Type.GetType("System.DateTime"))
    '        '    .Columns.Add("TakenCourses", Type.GetType("System.Int32"))
    '        '    .Columns.Add("CreditsEarned", Type.GetType("System.Decimal"))
    '        '    .Columns.Add("CreditsAttempted", Type.GetType("System.Decimal"))
    '        '    .Columns.Add("TermGPA", Type.GetType("System.Decimal"))
    '        '    .Columns.Add("GPA", Type.GetType("System.Decimal"))
    '        'End With
    '        'ds.Tables.Add(dt)

    '        'Troy:2/27/2007:Datatable to store the terms to be used for the links for the report
    '        'dt = New DataTable("Terms")
    '        'With dt
    '        '    .Columns.Add("TermId", Type.GetType("System.Guid"))
    '        '    .Columns.Add("TermDescrip", Type.GetType("System.String"))
    '        'End With
    '        'ds.Tables.Add(dt)


    '    End If

    '    If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    '    Return ds
    'End Function
    Public Function GetEnrollmentList(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStuID As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Code Added by Vijay Ramteke on May, 06 2009
        Dim strTempWhere As String = ""
        Dim strTerm As String = ""
        Dim strClassDate As String = ""
        'Code Added by Vijay Ramteke on May, 06 2009

        If paramInfo.FilterList <> "" Then
            strWhere &= "AND " & paramInfo.FilterList
        End If

        'Get StudentId and rest of Where Clause from paramInfo.FilterOther
        'If paramInfo.FilterOther <> "" Then
        '    strWhere &= "AND " & paramInfo.FilterOther
        'End If

        'Get ProgVerId and Where Clause from paramInfo.FilterList
        If paramInfo.SORT_LastName = True Then
            strOrderBy &= " ORDER BY arStudent.LastName "
        End If

        'Order By Clause.
        'If paramInfo.OrderBy <> "" Then
        '    strOrderBy &= " ORDER BY " & paramInfo.OrderBy
        'End If

        'Code Added by Vijay Ramteke on May, 06 2009
        strTempWhere = strWhere
        strTempWhere = strTempWhere.ToLower.Replace("and", ";")
        strWhere = ""
        Dim strArr As String() = strTempWhere.Remove(strTempWhere.IndexOf(";"), 1).Split(";")
        Dim i As Integer
        For i = 0 To strArr.Length - 1
            If strArr(i).ToLower.IndexOf("arterm") > 0 Then
                strTerm &= " AND " & strArr(i)
            ElseIf strArr(i).ToLower.IndexOf("arclasssections") > 0 Then
                strClassDate &= " AND " & strArr(i)
            Else
                strWhere &= " AND " & strArr(i)
            End If
        Next
        'Code Added by Vijay Ramteke on May, 06 2009

        'Get student identifier depending on what field school is using.
        If StudentIdentifier = "SSN" Then
            strStuID = "arStudent.SSN"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStuID = "arStuEnrollments.EnrollmentId"
        ElseIf StudentIdentifier = "StudentId" Then
            strStuID = "arStudent.StudentNumber"
        End If

        With sb
            .Append("SELECT distinct ")
            .Append("       arStuEnrollments.StuEnrollId,arStudent.LastName,arStudent.MiddleName,arStudent.FirstName,arStudent.SSN,arStudent.StudentNumber,arStudent.DOB," & strStuID & " AS StudentIdentifier,")
            .Append("       arStuEnrollments.StartDate,arStuEnrollments.ExpGradDate,arStuEnrollments.PrgVerId,arPrgVersions.PrgVerDescrip,")
            .Append(" arPrograms.ProgDescrip AS ProgramDescrip ,")
            .Append(" arDegrees.DegreeDescrip ,")
            .Append("       syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId,syCampuses.CampDescrip,")
            .Append("       arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip,")
            .Append("       arStuEnrollments.StartDate,arStuEnrollments.ExpGradDate,arStuEnrollments.EnrollmentId,arStuEnrollments.LDA,arStuEnrollments.DateDetermined,")
            .Append(" syStatusCodes.SysStatusId ,SA.address1 AS Address1 ,SA.Address2 AS Address2 ,SA.city AS City ,SA.statedescrip AS StateDescrip ,")
            .Append("SA.Zip AS Zip ,SA.foreignZip AS ForeignZip ,SA.otherstate AS OtherState ,SA.countryDescrip AS CountryDescrip ,")
            .Append("       (SELECT TOP 1 Phone FROM arStudentPhone T,syStatuses Y WHERE T.StudentId=arStudent.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS Phone,")
            .Append("       (SELECT TOP 1 ForeignPhone FROM arStudentPhone T,syStatuses Y WHERE T.StudentId=arStudent.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS ForeignPhone ,")
            .Append("       (select max(PostDate) from arGrdBkResults where StuEnrollId=arStuEnrollments.stuEnrollId) AS ExtDate, ")
            .Append("  dbo.getlda(arStuEnrollments.StuEnrollId) as AttDate, '" & IIf(strTerm <> "", strTerm.Replace("'", "''"), "") & "' AS TermCond, '" & IIf(strClassDate <> "", strClassDate.Replace("'", "''"), "") & "' AS ClassCond, " & IIf(paramInfo.ShowRptDateIssue = True, "1", "0") & " AS ShowDateIssue  ")
            'Code Added by Vijay Ramteke on May, 06 2009
            .Append("FROM   arStuEnrollments,syStatusCodes ,arPrgVersions, arDegrees ,arPrograms ,syCmpGrpCmps,syCampGrps,syCampuses,arResults,arStudent ")
            .Append(" OUTER APPLY ( SELECT TOP 1 T.* ,C.CountryDescrip,S.StateDescrip ")
            .Append(" FROM      arStudAddresses T ,syStatuses Y ,adCountries C ,syStates S WHERE T.StudentId = arStudent.StudentId ")
            .Append(" AND T.StatusId = Y.StatusId AND T.CountryId = C.CountryId AND T.StateId = S.StateId AND Y.Status = 'Active' ORDER BY  T.Default1 DESC ) SA ")
            .Append("WHERE  arStuEnrollments.StudentId = arStudent.StudentId ")
            .Append("AND syStatusCodes.statuscodeid = arStuEnrollments.statuscodeid ")
            .Append("       AND arPrgVersions.PrgVerId=arStuEnrollments.PrgVerId ")
            .Append(" AND arPrgVersions.degreeid = arDegrees.degreeid ")
            .Append(" and arPrgVersions.ProgId = arPrograms.Progid ")
            .Append("       AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId ")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
            .Append("       AND syCampuses.CampusId=arStuEnrollments.CampusId ")
            .Append(" and arResults.StuEnrollId=arStuEnrollments.StuEnrollId ")
            .Append(strWhere)
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then


                ds.Tables(0).TableName = "MultiTranscripts"
                'Need to add these fields so they are to link the tables in report.
                ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("StudentName2", System.Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("StrStuEnrollId", System.Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("FullAddress", System.Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("CreditsDesc", System.Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("SignDesc", System.Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("SuppressGrades", System.Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("SuppressStudentAddress", System.Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("SuppressStudentId", System.Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("SuppressLDA", System.Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("DisplayAsCourseCode", System.Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("SuppressDate", System.Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("IncludeAllCreditsAttempted", System.Type.GetType("System.String")))
                'Add new table.
                Dim dt As New DataTable("EnrollmentSummary")
                dt.Columns.Add(New DataColumn("StrStuEnrollId", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("TotalClasses", System.Type.GetType("System.Int32")))
                dt.Columns.Add(New DataColumn("TotalCreditsAttempted", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("TotalCreditsEarned", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("TotalHours", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("TotalGPA", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("FirstName", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("LastName", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("ShowROSSOnlyTabsForStudent", System.Type.GetType("System.String")))
                ds.Tables.Add(dt)
                'Add new table.
                dt = New DataTable("EnrollmentCourses")
                dt.Columns.Add(New DataColumn("StrStuEnrollId", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("TermId", System.Type.GetType("System.Guid")))
                dt.Columns.Add(New DataColumn("TermDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("DescripXTranscript", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("ReqId", System.Type.GetType("System.Guid")))
                dt.Columns.Add(New DataColumn("Code", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Descrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("ClsSection", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Credits", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("CreditsAttempted", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("StartDate", System.Type.GetType("System.DateTime")))
                dt.Columns.Add(New DataColumn("EndDate", System.Type.GetType("System.DateTime")))
                dt.Columns.Add(New DataColumn("TestId", System.Type.GetType("System.Guid")))
                dt.Columns.Add(New DataColumn("GrdSysDetailId", System.Type.GetType("System.Guid")))
                dt.Columns.Add(New DataColumn("Grade", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("IsPass", System.Type.GetType("System.Boolean")))
                dt.Columns.Add(New DataColumn("GPA", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("IsCreditsAttempted", System.Type.GetType("System.Boolean")))
                dt.Columns.Add(New DataColumn("IsCreditsEarned", System.Type.GetType("System.Boolean")))
                dt.Columns.Add(New DataColumn("IsInGPA", System.Type.GetType("System.Boolean")))
                dt.Columns.Add(New DataColumn("CourseCategory", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Hours", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("StrCourseCategory", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("TotalCredits", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("TotalHours", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("TermGPA", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("CumGPA", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Score", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("FirstName", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("LastName", System.Type.GetType("System.String")))
                'Code Column DateIssue Addded By Vijay Ramteke on May, 11 2009
                dt.Columns.Add(New DataColumn("DateIssue", System.Type.GetType("System.DateTime")))
                dt.Columns.Add(New DataColumn("ShowDateIssue", System.Type.GetType("System.Int32")))
                'Add a TermCredits column
                dt.Columns.Add(New DataColumn("TermCredits", System.Type.GetType("System.Decimal")))
                'Code Column DateIssue Addded By Vijay Ramteke on May, 11 2009
                ds.Tables.Add(dt)


                '   Build ReportParams table, containing special information for Transcript report
                'dt = GetTranscriptParams(paramInfo)
                'ds.Tables.Add(dt.Copy)

                '   Build ReportParams table
                dt = New DataTable("ReportParams")
                dt.Columns.Add("CorporateName", System.Type.GetType("System.String"))
                dt.Columns.Add("FullAddress", System.Type.GetType("System.String"))
                dt.Columns.Add("Phone", System.Type.GetType("System.String"))
                dt.Columns.Add("Fax", System.Type.GetType("System.String"))
                dt.Columns.Add("SchoolLogo", System.Type.GetType("System.Byte[]"))
                dt.Columns.Add("StudentIdentifier", System.Type.GetType("System.String"))
                dt.Columns.Add("TranscriptAuthznTitle", System.Type.GetType("System.String"))
                dt.Columns.Add("TranscriptAuthznName", System.Type.GetType("System.String"))
                dt.Columns.Add("Website", System.Type.GetType("System.String"))
                dt.Columns.Add("SchoolName", System.Type.GetType("System.String"))
                dt.Columns.Add("OrderByString", System.Type.GetType("System.String"))
                dt.Columns.Add("FilterByString", System.Type.GetType("System.String"))
                dt.Columns.Add("FilterOtherString", System.Type.GetType("System.String"))
                dt.Columns.Add("ShowFilters", System.Type.GetType("System.Boolean"))
                dt.Columns.Add("ShowSortBy", System.Type.GetType("System.Boolean"))
                dt.Columns.Add("IsLetterGradeSchool", System.Type.GetType("System.Boolean"))
                dt.Columns.Add("TermAvgStart", System.Type.GetType("System.Decimal"))
                dt.Columns.Add("TermAvgEnd", System.Type.GetType("System.Decimal"))
                dt.Columns.Add("CumAvgStart", System.Type.GetType("System.Decimal"))
                dt.Columns.Add("CumAvgEnd", System.Type.GetType("System.Decimal"))
                dt.Columns.Add("TermCreditsStart", System.Type.GetType("System.Decimal"))
                dt.Columns.Add("TermCreditsEnd", System.Type.GetType("System.Decimal"))



                Dim dr As DataRow
                dr = dt.NewRow
                If Not (paramInfo.SchoolLogo Is Nothing) Then
                    dr("SchoolLogo") = paramInfo.SchoolLogo
                End If
                dr("StudentIdentifier") = MyAdvAppSettings.AppSettings("StudentIdentifier") & ":"
                dt.Rows.Add(dr)
                ds.Tables.Add(dt)

                'Troy:4/15/2007:Added this to bring the letter grades and their gpa and ranges, if any.
                dt = New DataTable("GradeDescriptions")
                dt.Columns.Add("Grade", System.Type.GetType("System.String"))
                dt.Columns.Add("GPA", System.Type.GetType("System.String"))
                dt.Columns.Add("Range", System.Type.GetType("System.String"))
                dt.Columns.Add("GradeDescription", System.Type.GetType("System.String"))
                dt.Columns.Add("Quality", System.Type.GetType("System.String"))
                dt.Columns.Add("SuppressGradeDescription", System.Type.GetType("System.String"))
                ds.Tables.Add(dt)

                dt = New DataTable("Legend")
                dt.Columns.Add("GradeDescription", System.Type.GetType("System.String"))
                ds.Tables.Add(dt)
            End If
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Public Function GetStuLowGPA(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStuId As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        'Order By Clause.
        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        Else
            strOrderBy &= ",arPrgVersions.PrgVerDescrip,arStuEnrollments.PrgVerId,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName"
        End If

        If StudentIdentifier = "SSN" Then
            strStuId = "arStudent.SSN AS StudentIdentifier,"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStuId = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
        ElseIf StudentIdentifier = "StudentId" Then
            strStuId = "arStudent.StudentNumber AS StudentIdentifier,"
        End If

        With sb
            .Append("SELECT ")
            .Append("       arStuEnrollments.StuEnrollId,arStudent.LastName,")
            .Append("       arStudent.MiddleName,arStudent.FirstName,arStudent.DOB," & strStuId)
            .Append("       arStuEnrollments.ExpGradDate,arStuEnrollments.PrgVerId,arPrgVersions.PrgVerDescrip,")
            .Append("       syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId,syCampuses.CampDescrip,")
            .Append("       arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip ")
            .Append("FROM   arStuEnrollments,arStudent,arPrgVersions,syCmpGrpCmps,syCampGrps,syCampuses ")
            .Append("WHERE  arStuEnrollments.StudentId=arStudent.StudentId")
            .Append("       AND arPrgVersions.PrgVerId=arStuEnrollments.PrgVerId")
            .Append("       AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId")
            .Append("       AND syCampuses.CampusId=arStuEnrollments.CampusId")
            .Append(strWhere)
            .Append(" ORDER BY syCampGrps.CampGrpDescrip,syCmpGrpCmps.CampGrpId,syCampuses.CampDescrip,arStuEnrollments.CampusId")
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "StudentsWithLowGPA"
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("StudentCount", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("GPA", System.Type.GetType("System.Decimal")))
            ds.Tables(0).Columns.Add(New DataColumn("PrgVerIdStr", System.Type.GetType("System.String")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

#End Region

#Region "Private Members"

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

    Private Function GetTranscriptParams(ByVal paramInfo As ReportParamInfo) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim dt As DataTable
        Dim dr As DataRow

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If MyAdvAppSettings.AppSettings("CorporateName") = "" Then
            '   Retrieve information from syCampuses
            With sb
                .Append("SELECT ")
                .Append("       CampDescrip AS CorporateName,Address1,Address2,City,Zip,")
                .Append("       (SELECT StateDescrip FROM syStates WHERE StateId=C.StateId) AS State,")
                .Append("       (SELECT CountryDescrip FROM adCountries WHERE CountryId=C.CountryId) AS Country,Fax,")
                .Append("       Phone = CASE WHEN (Phone1 IS NULL) THEN (CASE WHEN (Phone2 IS NULL) THEN (CASE WHEN (Phone3 IS NULL) THEN '' ELSE Phone3 END) ELSE Phone2 END) ELSE Phone1 END ")
                .Append("FROM   syCampuses C ")
                .Append("WHERE  IsCorporate = 1 ")
            End With

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            ds = db.RunParamSQLDataSet(sb.ToString)
            If ds.Tables.Count > 0 Then dt = ds.Tables(0)

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Else
            '   Build ReportParams table
            dt = New DataTable
            dt.Columns.Add("CorporateName", System.Type.GetType("System.String"))
            dt.Columns.Add("Address1", System.Type.GetType("System.String"))
            dt.Columns.Add("Address2", System.Type.GetType("System.String"))
            dt.Columns.Add("City", System.Type.GetType("System.String"))
            dt.Columns.Add("State", System.Type.GetType("System.String"))
            dt.Columns.Add("Zip", System.Type.GetType("System.String"))
            dt.Columns.Add("Phone", System.Type.GetType("System.String"))
            dt.Columns.Add("Fax", System.Type.GetType("System.String"))
            dt.Columns.Add("Country", System.Type.GetType("System.String"))

            dr = dt.NewRow
            '   Retrieve information from Web.Config
            dr("CorporateName") = MyAdvAppSettings.AppSettings("CorporateName").ToUpper
            dr("Address1") = MyAdvAppSettings.AppSettings("CorporateAddress1").ToUpper
            dr("Address2") = MyAdvAppSettings.AppSettings("CorporateAddress2").ToUpper
            dr("City") = MyAdvAppSettings.AppSettings("CorporateCity").ToUpper
            dr("State") = MyAdvAppSettings.AppSettings("CorporateState").ToUpper
            dr("Zip") = MyAdvAppSettings.AppSettings("CorporateZip").ToUpper
            '   Get first phone that it is not blank
            If MyAdvAppSettings.AppSettings("CorporatePhone1") <> "" Then
                dr("Phone") = MyAdvAppSettings.AppSettings("CorporatePhone1").ToUpper
            ElseIf MyAdvAppSettings.AppSettings("CorporatePhone2") <> "" Then
                dr("Phone") = MyAdvAppSettings.AppSettings("CorporatePhone2").ToUpper
            ElseIf MyAdvAppSettings.AppSettings("CorporatePhone3") <> "" Then
                dr("Phone") = MyAdvAppSettings.AppSettings("CorporatePhone3").ToUpper
            End If
            dr("Fax") = MyAdvAppSettings.AppSettings("CorporateFax").ToUpper
            dr("Country") = MyAdvAppSettings.AppSettings("CorporateCountry").ToUpper

            dt.Rows.Add(dr)
        End If

        If Not dt Is Nothing Then
            '   Name table and add additional columns
            dt.TableName = "ReportParams"
            dt.Columns.Add("SchoolLogo", System.Type.GetType("System.Byte[]"))
            dt.Columns.Add("StudentIdentifier", System.Type.GetType("System.String"))
            dt.Columns.Add("FullAddress", System.Type.GetType("System.String"))
            If Not dr Is Nothing Then
                dr("SchoolLogo") = paramInfo.SchoolLogo
                dr("StudentIdentifier") = MyAdvAppSettings.AppSettings("StudentIdentifier") & ":"
            End If
        End If

        Return dt
    End Function

    Private Function GetStuEnrollId(ByVal rptparaminfo As ReportParamInfo) As String
        'The FilterOther property has the enrollment and term start cutoff 
        'The format is like this 'arStuEnrollments.StuEnrollId='xxxx' AND arTerm.StartDate='xxxx'
        Dim strArr() As String
        Dim strItem As String

        strArr = rptparaminfo.FilterOther.ToString.Split("AND")
        Return strArr(0)

    End Function

#End Region
    Public Function GetWorkUnitResults(ByVal strStuEnrollId As String, ByVal reqId As String) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT ")
            .Append("   GBCR.ReqId,GBCR.TermId,GCT.Descrip,GBCR.Score,GBCR.MinResult, ")
            .Append("   GBCR.Required,GBCR.MustPass, ")
            .Append("   (CASE ")
            .Append("       WHEN Score > MinResult THEN NULL ")
            .Append("       WHEN Score = MinResult THEN 0 ")
            .Append("       WHEN MinResult > Score THEN (MinResult - Score) ")
            .Append("    END) AS Remaining ")
            .Append("FROM arGrdBkConversionResults GBCR, arGrdComponentTypes GCT ")
            .Append("WHERE GBCR.GrdComponentTypeId=GCT.GrdComponentTypeId ")
            .Append("AND GBCR.StuEnrollId=? and ReqId= ? ")
            .Append(" Union ")
            .Append("SELECT ")
            .Append("   ReqId,TermId,Descrip,Score,MinResult,isnull(Required,0) as Required,isnull(MustPass,0) as MustPass , ")
            .Append("   (CASE ")
            .Append("     WHEN Score > MinResult THEN NULL ")
            .Append("     WHEN Score = MinResult THEN 0 ")
            .Append("     WHEN MinResult > Score THEN (MinResult - Score) ")
            .Append("    END) AS Remaining ")
            .Append("   FROM ")
            .Append("   (SELECT ")
            .Append("    CS.ReqId,CS.TermId,GCT.Descrip, ")
            .Append("     (CASE GCT.SysComponentTypeId ")
            .Append("       WHEN 544 THEN (SELECT SUM(HoursAttended) FROM arExternshipAttendance WHERE StuEnrollId= ? ) ")
            .Append("       ELSE GBRS.Score ")
            .Append("       END ")
            .Append("     ) AS Score, ")
            .Append("    (CASE GCT.SysComponentTypeId ")
            .Append("       WHEN 500 THEN GBWD.Number ")
            .Append("       WHEN 503 THEN GBWD.Number ")
            .Append("       ELSE (SELECT MIN(MinVal) ")
            .Append("             FROM arGradeScaleDetails GSD, arGradeSystemDetails GSS ")
            .Append("             WHERE GSD.GrdSysDetailId=GSS.GrdSysDetailId ")
            .Append("             AND GSS.IsPass=1) ")
            .Append("       END ")
            .Append("       )	AS MinResult, ")
            .Append("       GBWD.Required, GBWD.MustPass ")
            .Append("       FROM arGrdBkResults GBRS, arClassSections CS, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT ")
            .Append("       WHERE GBRS.StuEnrollId=? ")
            .Append("       AND GBRS.ClsSectionId=CS.ClsSectionId ")
            .Append("       AND GBRS.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
            .Append("       AND GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId  and ReqId= ? ) P ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()


        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function
End Class
