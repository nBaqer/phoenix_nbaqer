Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class PrgVerDefDB
    Public Function GetNCReqTypes() As DataSet
        Dim ds As DataSet

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.ReqTypeId,Descrip ")
                .Append("FROM arReqTypes t1 ")
                .Append("WHERE t1.ReqTypeId NOT IN (1,2) ")
            End With

            '   Execute the query
            ds = db.RunSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function


    Public Function GetReqsNotAssigned(ByVal ReqTypeId As String, ByVal PrgVerID As String) As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim da As New OleDbDataAdapter
        Dim sb As New System.Text.StringBuilder
        Dim reccount As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        With sb
            .Append("Select count(*) from arProgVerDef")
        End With

        db.OpenConnection()
        reccount = db.RunParamSQLScalar(sb.ToString)
        sb.Remove(0, sb.Length)

        If reccount <> 0 Then

            With sb
                .Append("SELECT t1.ReqId,t1.Descrip,t1.ReqTypeId ")
                .Append("FROM arReqs t1, arReqTypes t2 ")
                .Append("WHERE t1.ReqTypeId = ? and t1.ReqTypeId = t2.ReqTypeId and t2.IsGroup = 0 and ")
                .Append("t1.ReqId not in (Select ReqId from arProgVerDef t3 where PrgVerId = ?) ")
                .Append("ORDER BY t1.Descrip")
            End With
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ReqTypeId", ReqTypeId, DataAccess.OleDbDataType.OleDbInteger, 16, ParameterDirection.Input)
            db.AddParameter("@PrgVerId", PrgVerID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            With sb
                .Append("SELECT t1.ReqId,t1.Descrip,t1.ReqTypeId ")
                .Append("FROM arReqs t1, arReqTypes t2 ")
                .Append("WHERE t1.ReqTypeId = ? and t1.ReqTypeId = t2.ReqTypeId and t2.IsGroup = 0 ")
                .Append("ORDER BY t1.Descrip")
            End With
            db.AddParameter("@ReqTypeId", ReqTypeId, DataAccess.OleDbDataType.OleDbInteger, 16, ParameterDirection.Input)

        End If

        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds.Tables(0)
    End Function

    Public Function GetReqGrpsNotAssigned(ByVal ChildType As Integer, ByVal PrgVerID As String) As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim reccount As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        With sb
            .Append("Select count(*) from arProgVerDef")
        End With

        db.OpenConnection()
        reccount = db.RunParamSQLScalar(sb.ToString)
        sb.Remove(0, sb.Length)

        If reccount <> 0 Then

            With sb
                .Append("SELECT t1.ReqId,t1.Descrip,t2.ChildType ")
                .Append("FROM arReqs t1, arReqTypes t2 ")
                .Append("WHERE t1.ReqTypeId = t2.ReqTypeId and t2.IsGroup = 1 and t2.ChildType = ? and ")
                .Append("t1.ReqId not in (Select ReqId from arProgVerDef t3 where PrgVerId = ?) ")
                .Append("ORDER BY t1.Descrip")
            End With
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ChildType", ChildType, DataAccess.OleDbDataType.OleDbInteger, 16, ParameterDirection.Input)
            db.AddParameter("@PrgVerId", PrgVerID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            With sb
                .Append("SELECT t1.ReqId,t1.Descrip,t2.ChildType ")
                .Append("FROM arReqs t1, arReqTypes t2 ")
                .Append("WHERE t1.ReqTypeId = t2.ReqTypeId and t2.IsGroup = 0 and t2.ChildType = ? ")
                .Append("ORDER BY t1.Descrip")
            End With
            db.AddParameter("@ReqTypeId", ChildType, DataAccess.OleDbDataType.OleDbInteger, 16, ParameterDirection.Input)

        End If

        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds.Tables(0)
    End Function

    Public Function GetReqsReqGrpsAssgd(ByVal ID As String) As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'Build query to obtain course/coursegrpids and course/coursegrpdescrips assgd to a prog ver.
        With sb

            .Append("SELECT t1.ReqId,ReqSeq,IsRequired,TrkForCompletion,t2.Descrip,t2.ReqTypeId,t1.Hours,t1.Cnt,t3.IsGroup,t3.ChildType ")
            .Append("FROM arProgVerDef t1, arReqs t2, arReqTypes t3 ")
            .Append("WHERE t1.ReqId = t2.ReqId and t2.ReqTypeId NOT IN (1,2) and t1.PrgVerId = ? and t2.ReqTypeId = t3.ReqTypeId ")
            .Append("ORDER BY t1.ReqSeq")

        End With
        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@PrgVerId", ID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds.Tables(0)
    End Function

    Public Function GetTrackHrsCnt(ByVal ReqTypeId As Integer) As DataSet
        Dim ds As DataSet
        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.TrkHours,TrkCount ")
                .Append("FROM arReqTypes t1 ")
                .Append("WHERE t1.ReqTypeId = ? ")
            End With
            db.AddParameter("@ReqTypeId", ReqTypeId, DataAccess.OleDbDataType.OleDbInteger, 16, ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()
        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function LoadReqsReqGrps(ByVal ID As String, ByVal PrgVerID As String) As DataSet

        Dim LoadedChildDS As New DataSet
        Dim dt1 As New DataTable
        Dim dt2 As New DataTable
        Dim dt3 As New DataTable
        Dim row As DataRow
        Dim isreq As Boolean

        dt1 = GetReqsNotAssigned(ID, PrgVerID).Copy
        dt1.TableName = "ReqsNotAssgd"
        LoadedChildDS.Tables.Add(dt1)


        dt2 = GetReqGrpsNotAssigned(ID, PrgVerID).Copy
        dt2.TableName = "ReqGrpsNotAssgd"
        LoadedChildDS.Tables.Add(dt2)

        Return LoadedChildDS

    End Function

    Public Function LoadChildDS(ByVal ID As String) As DataSet

        Dim LoadedChildDS As New DataSet
        Dim dt1 As New DataTable
        Dim dt2 As New DataTable
        Dim dt3 As New DataTable
        Dim row As DataRow
        Dim isreq As Boolean

        dt3 = GetReqsReqGrpsAssgd(ID).Copy
        dt3.TableName = "FldsSelected"
        LoadedChildDS.Tables.Add(dt3)

        'return dataset to facade
        Return LoadedChildDS

    End Function

    Public Function InsertChildToDB(ByVal ChildInfoObj As ChildInfo, ByVal ID As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim childID As String
        Dim childTyp As String
        Dim childSeq As Integer
        Dim req As Integer
        Dim trk As Integer
        Dim hours As Decimal
        Dim cnt As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        childID = ChildInfoObj.ChildId
        ' childTyp = ChildInfoObj.ChildType
        childSeq = ChildInfoObj.ChildSeq
        req = ChildInfoObj.Req
        hours = ChildInfoObj.Hours
        cnt = ChildInfoObj.Cnt
        trk = ChildInfoObj.TrkForCompletion

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("INSERT INTO arProgVerDef(PrgVerId,ReqId,ReqSeq,IsRequired,TrkForCompletion,Hours,Cnt) ")
            .Append("VALUES(?,?,?,?,?,?,?)")

            db.AddParameter("@prgverid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@childid", childID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@childseq", childSeq, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@req", req, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@trk", trk, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@hours", hours, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@credits", cnt, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        End With

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function

    Public Function UpdateChildInDB(ByVal ChildInfoObj As ChildInfo, ByVal ID As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim childID As String
        Dim childTyp As String
        Dim childSeq As Integer
        Dim req As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        childID = ChildInfoObj.ChildId
        childSeq = ChildInfoObj.ChildSeq

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("UPDATE arProgVerDef Set ReqSeq = ? ")
            .Append("WHERE  ReqId = ? and PrgVerId = ?")

            db.AddParameter("@childseq", childSeq, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@childid", childID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@prgverid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        End With

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function

    Public Function DeleteChildFrmDB(ByVal ChildInfoObj As ChildInfo, ByVal ID As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("DELETE FROM arProgVerDef ")
            .Append("WHERE PrgVerId = ? ")
            .Append("AND ReqId = ? ")
        End With

        db.AddParameter("@prgverid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", ChildInfoObj.ChildId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function

    Public Function GetProgramForPrgVersion(ByVal prgVerId As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim obj As New Object

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT ProgId ")
            .Append("FROM arPrgVersions ")
            .Append("WHERE PrgVerId = ?")
        End With

        db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Return (CType(db.RunParamSQLScalar(sb.ToString), Guid)).ToString

    End Function

    Public Function GetAreaForPrgVersion(ByVal prgVerId As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim obj As New Object

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT PrgGrpId ")
            .Append("FROM arPrgVersions ")
            .Append("WHERE PrgVerId = ?")
        End With

        db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Return (CType(db.RunParamSQLScalar(sb.ToString), Guid)).ToString

    End Function

    Public Function GetAreaForProgram(ByVal programId As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim obj As New Object

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT TOP 1 pv.PrgGrpId ")
            .Append("FROM arprograms pg ")
            .Append("INNER JOIN dbo.arPrgVersions pv ON pv.ProgId = pg.ProgId ")
            .Append("WHERE pg.ProgId=? ")
        End With

        db.AddParameter("@programid", programId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Return (CType(db.RunParamSQLScalar(sb.ToString), Guid)).ToString

    End Function


    Public Function IsProgramVersionTimeClock(ByVal prgVerId As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT ProgId ")
            .Append(" FROM arPrgVersions programVersion")
            .Append(" INNER JOIN arAttUnitType unitTypes on programVersion.UnitTypeId = unitTypes.UnitTypeId ")
            .Append(" WHERE PrgVerId = ? AND unitTypes.UnitTypeDescrip in ('" + AttendanceUnitType.ClockHour + "','" + AttendanceUnitType.Minutes + "')")
        End With

        db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Dim data As DataSet = db.RunParamSQLDataSet(sb.ToString())

        If (data.Tables.Count > 0) Then
            If (data.Tables(0).Rows.Count > 0) Then
                Return True
            End If
        End If

        Return False

    End Function

    Public Function IsProgramVersionClockHour(ByVal prgVerId As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim clockHour = CType(Enumerations.AcademicCalendars.ClockHour, Integer)
        With sb
            .Append("SELECT progs.ProgId ")
            .Append(" FROM arPrgVersions programVersion")
            .Append(" INNER JOIN arPrograms progs on programVersion.ProgId = progs.ProgId ")
            .Append(" WHERE programVersion.PrgVerId = ? AND progs.ACId = ?")
        End With

        db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@academicCalendar", clockHour, DataAccess.OleDbDataType.OleDbInteger, ParameterDirection.Input)

        Dim data As DataSet = db.RunParamSQLDataSet(sb.ToString())

        If (data.Tables.Count > 0) Then
            If (data.Tables(0).Rows.Count > 0) Then
                Return True
            End If
        End If

        Return False

    End Function

End Class
