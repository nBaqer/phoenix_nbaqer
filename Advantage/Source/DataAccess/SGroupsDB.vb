Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' SGroupsDB.vb
'
' SGroupsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class SGroupsDB
    'Public Function GetAllSGroups(ByVal showActiveOnly As Boolean) As DataSet

    '    '   connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append("SELECT   SG.SGroupId, SG.StatusId, SG.Descrip, SG.EmpId, ")
    '        .Append("         SG.IsPublic, SG.SGroupTypeId, SG.SqlStatement ")
    '        .Append("FROM     sySGroups SG, syStatuses ST ")
    '        .Append("WHERE    SG.StatusId = ST.StatusId ")
    '        If showActiveOnly Then
    '            .Append(" AND     ST.Status = 'Active' ")
    '        End If
    '        .Append("ORDER BY SG.Descrip ")
    '    End With

    '    '   return dataset
    '    Return db.RunSQLDataSet(sb.ToString)

    'End Function
    'Public Function GetSGroupInfo(ByVal SGroupId As String) As SGroupInfo

    '    '   connect to the database
    '    Dim db As New DataAccess
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

    '    '   build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        '   with subqueries
    '        .Append("SELECT CCT.SGroupId, ")
    '        .Append("    CCT.SGroupCode, ")
    '        .Append("    CCT.StatusId, ")
    '        .Append("    (Select Status from syStatuses where StatusId=CCT.StatusId) As Status, ")
    '        .Append("    CCT.SGroupDescrip, ")
    '        .Append("    CCT.CampGrpId, ")
    '        .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=CCT.CampGrpId) As CampGrpdescrip, ")
    '        .Append("    CCT.ModUser, ")
    '        .Append("    CCT.ModDate ")
    '        .Append("FROM  saSGroups CCT ")
    '        .Append("WHERE CCT.SGroupId= ? ")
    '    End With

    '    ' Add the SGroupId to the parameter list
    '    db.AddParameter("@SGroupId", SGroupId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    '   Execute the query
    '    Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

    '    Dim SGroupInfo As New SGroupInfo

    '    While dr.Read()

    '        '   set properties with data from DataReader
    '        With SGroupInfo
    '            .SGroupId = SGroupId
    '            .IsInDB = True
    '            .Code = dr("SGroupCode")
    '            .StatusId = CType(dr("StatusId"), Guid).ToString
    '            .Status = dr("Status")
    '            .Description = dr("SGroupDescrip")
    '            If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
    '            If Not (dr("CampGrpdescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
    '            .ModUser = dr("ModUser")
    '            .ModDate = dr("ModDate")
    '        End With

    '    End While

    '    'Close Connection
    '    db.CloseConnection()

    '    '   Return BankInfo
    '    Return SGroupInfo

    'End Function
    'Public Function UpdateSGroupInfo(ByVal SGroupInfo As SGroupInfo, ByVal user As String) As String

    '    '   Connect to the database
    '    Dim db As New DataAccess
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

    '    '   do an update
    '    Try
    '        '   build the query
    '        Dim sb As New StringBuilder
    '        With sb
    '            .Append("UPDATE saSGroups Set SGroupId = ?, SGroupCode = ?, ")
    '            .Append(" StatusId = ?, SGroupDescrip = ?, CampGrpId = ?, ")
    '            .Append(" ModUser = ?, ModDate = ? ")
    '            .Append("WHERE SGroupId = ? ")
    '            .Append("AND ModDate = ? ;")
    '            .Append("Select count(*) from saSGroups where ModDate = ? ")
    '        End With

    '        '   add parameters values to the query

    '        '   SGroupId
    '        db.AddParameter("@SGroupId", SGroupInfo.SGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '  SGroupCode
    '        db.AddParameter("@SGroupCode", SGroupInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   StatusId
    '        db.AddParameter("@StatusId", SGroupInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   SGroupDescrip
    '        db.AddParameter("@SGroupDescrip", SGroupInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   CampGrpId
    '        If SGroupInfo.CampGrpId = Guid.Empty.ToString Then
    '            db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@CampGrpId", SGroupInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        '   ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   ModDate
    '        Dim now As Date = Date.Now
    '        db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   SGroupId
    '        db.AddParameter("@AdmDepositId", SGroupInfo.SGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   ModDate
    '        db.AddParameter("@Original_ModDate", SGroupInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   ModDate
    '        db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   execute the query
    '        Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

    '        '   If there were no updated rows then there was a concurrency problem
    '        If rowCount = 1 Then
    '            '   return without errors
    '            Return ""
    '        Else
    '            Return DALExceptions.BuildConcurrencyExceptionMessage()
    '        End If

    '    Catch ex As OleDbException

    '        '   return an error to the client
    '        'DisplayOleDbErrorCollection(ex)
    '        Return DALExceptions.BuildErrorMessage(ex)

    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try

    'End Function
    'Public Function AddSGroupInfo(ByVal SGroupInfo As SGroupInfo, ByVal user As String) As String

    '    '   Connect to the database
    '    Dim db As New DataAccess
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

    '    '   do an insert
    '    Try
    '        '   build the query
    '        Dim sb As New StringBuilder
    '        With sb
    '            .Append("INSERT saSGroups (SGroupId, SGroupCode, StatusId, ")
    '            .Append("   SGroupDescrip, CampGrpId, ModUser, ModDate) ")
    '            .Append("VALUES (?,?,?,?,?,?,?) ")
    '        End With

    '        '   add parameters values to the query

    '        '   SGroupId
    '        db.AddParameter("@SGroupId", SGroupInfo.SGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   SGroupCode
    '        db.AddParameter("@SGroupCode", SGroupInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   StatusId
    '        db.AddParameter("@StatusId", SGroupInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   SGroupDescrip
    '        db.AddParameter("@SGroupDescrip", SGroupInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   CampGrpId
    '        If SGroupInfo.CampGrpId = Guid.Empty.ToString Then
    '            db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@CampGrpId", SGroupInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        '   ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   ModDate
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   execute the query
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString)

    '        '   return without errors
    '        Return ""

    '    Catch ex As OleDbException
    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)
    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try

    'End Function
    'Public Function DeleteSGroupInfo(ByVal SGroupId As String, ByVal modDate As DateTime) As String

    '    '   Connect to the database
    '    Dim db As New DataAccess
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

    '    '   do a delete
    '    Try
    '        '   build the query
    '        Dim sb As New StringBuilder
    '        With sb
    '            .Append("DELETE FROM saSGroups ")
    '            .Append("WHERE SGroupId = ? ")
    '            .Append(" AND ModDate = ? ;")
    '            .Append("Select count(*) from saSGroups where SGroupId = ? ")
    '        End With

    '        '   add parameters values to the query

    '        '   SGroupId
    '        db.AddParameter("@SGroupId", SGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   ModDate
    '        db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   SGroupId
    '        db.AddParameter("@SGroupId", SGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   execute the query
    '        Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

    '        '   If the row was not deleted then there was a concurrency problem
    '        If rowCount = 0 Then
    '            '   return without errors
    '            Return ""
    '        Else
    '            Return DALExceptions.BuildConcurrencyExceptionMessage()
    '        End If


    '    Catch ex As OleDbException
    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)
    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    'End Function

End Class
