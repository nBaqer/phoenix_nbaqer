Imports FAME.Advantage.Common

Public Class DocumentManagementDB
    Public Function GetAllDocuments() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("select Distinct CT.DocumentId,CT.DocumentDescrip ")
            .Append("FROM     cmDocuments CT,syStatuses ST ")
            .Append("WHERE    CT.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY CT.DocumentDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllStudentsNames() As DataSet

        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim da6 As New OleDbDataAdapter
        Dim ds As New DataSet
        'Build the sql query
        With sb
            .Append(" select Studentid,firstname,middlename,lastname from arStudent ")
        End With
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorDT")
        Catch ex As System.Exception
            Throw New System.Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetAllStudentsNamesById(ByVal StudentId As String) As DataSet

        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim da6 As New OleDbDataAdapter
        Dim ds As New DataSet
        'Build the sql query
        With sb
            .Append(" select Studentid,firstname,middlename,lastname from arStudent where StudentId=? ")
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorDT1")
        Catch ex As System.Exception
            Throw New System.Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        Return ds
    End Function
    Public Function InsertDocument(ByVal strFileName As String, ByVal strFileExtension As String, ByVal User As String, ByVal strDocumentType As String, ByVal strStudentId As String, ByVal strDocumentId As String, ByVal strModuleId As String, ByVal strDisplayName As String) As Integer
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append("IF EXISTS( SELECT * from syDocumentHistory where FileName = '" & strFileName & "'" & " AND DocumentType = '" & strDocumentType & "')")
            .Append(" BEGIN ")
            .Append(" Update syDocumentHistory set ModUser = '" & User & "'," & " ModDate = '" & Date.Now & "'" & " where FileName = '" & strFileName & "'" & " AND DocumentType = '" & strDocumentType & "'")
            .Append(" END ")
            .Append(" ELSE ")
            .Append(" BEGIN ")
            .Append("INSERT INTO syDocumentHistory (FileID, FileName,FileExtension, ModDate, ModUser,DocumentType,StudentId,DocumentId,ModuleId, DisplayName) ")
            .Append("VALUES(?,?,?,?,?,?,?,?,?,?)")
            .Append(" END ")
        End With

        'add parameters
        db.AddParameter("@FileId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@FileName", strFileName, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
        db.AddParameter("@FileExtension", strFileExtension, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DocumentType", strDocumentType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@StudentId", strStudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DocumentId", strDocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModuleId", strModuleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DisplayName", strDisplayName, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)


        '   execute query
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)
        Return 0
        'Close Connection
        db.CloseConnection()
    End Function

    Public Function InsertDocumentbyID(ByVal StrFileName As String, ByVal strFileExtension As String, ByVal User As String, ByVal strDocumentType As String, ByVal strStudentId As String, ByVal strDocumentId As String, ByVal strModuleId As String, ByVal strDisplayName As String) As String
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append("INSERT INTO syDocumentHistory (FileID, FileName,FileExtension, ModDate, ModUser,DocumentType,StudentId,DocumentId,ModuleId, DisplayName) ")
            .Append("VALUES(newID(),?,?,?,?,?,?,?,?,?)")

        End With

        'add parameters
        db.AddParameter("@FileName", StrFileName, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
        db.AddParameter("@FileExtension", strFileExtension, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DocumentType", strDocumentType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@StudentId", strStudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DocumentId", strDocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModuleId", strModuleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DisplayName", strDisplayName, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)


        '   execute query
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        Dim ds As New DataSet
        With sb
            .Append("Select FileID from syDocumentHistory where Filename=? and FileExtension=?  and ModUser=? and DocumentType=? and StudentId=? and DocumentId=? and ModuleId=? and DisplayName=? ")
        End With

        db.AddParameter("@FileName", StrFileName, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
        db.AddParameter("@FileExtension", strFileExtension, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DocumentType", strDocumentType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@StudentId", strStudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DocumentId", strDocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModuleId", strModuleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DisplayName", strDisplayName, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)(0).ToString
            End If
        End If
        Return ""
        'Close Connection
        db.CloseConnection()
    End Function


    Public Function UpdateDocument(ByVal FileID As String, ByVal strFileName As String, ByVal strFileExtension As String, ByVal User As String, ByVal strDocumentType As String, ByVal strStudentId As String, ByVal strDocumentId As String, ByVal strModuleId As String, ByVal strDisplayName As String) As Integer
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append("IF EXISTS( SELECT * from syDocumentHistory where FileID = '" & FileID & "')")
            .Append(" BEGIN ")
            .Append(" Update syDocumentHistory set ModUser = '" & User & "'," & " ModDate = '" & Date.Now & "', FileName= ?, FileExtension= ? , DocumentType= ? , StudentId= ? , DocumentId= ? , ModuleId= ?  , DisplayName= ?   " & " where FileID = '" & FileID & "'")
            .Append(" END ")

        End With

        'add parameters

        db.AddParameter("@FileName", strFileName, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
        db.AddParameter("@FileExtension", strFileExtension, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DocumentType", strDocumentType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@StudentId", strStudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DocumentId", strDocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModuleId", strModuleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DisplayName", strDisplayName, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)

        '   execute query
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)
        Return 0
        'Close Connection
        db.CloseConnection()
    End Function
    Public Function GetDocumentExtension(ByVal strFileName As String, ByVal strDocumentType As String) As String
        Dim db As New DataAccess
        Dim strExtension As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append(" Select FileExtension from syDocumentHistory where FileName = ? ")
            .Append("  and DocumentType=? ")
        End With
        db.AddParameter("@FileName", strFileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@DocumentType", strDocumentType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            strExtension = dr("FileExtension").ToString()
        End While

        If Not dr.IsClosed Then dr.Close()

        Return strExtension
        db.CloseConnection()
    End Function
    Public Function GetStudentPhotoFileName(ByVal StudentId As String, ByVal strDocumentType As String) As String
        Dim db As New DataAccess
        Dim strFileName As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append(" Select Distinct Convert(VARCHAR(50),FileID)+Convert(VARCHAR(10),FileExtension) as FileName from syDocumentHistory where StudentId = ? ")
            .Append("  and DocumentType like '" & strDocumentType & "%' ")
            '.Append(" Select Distinct FileName + FileExtension  as FileName from syDocumentHistory where StudentId = ? ")
            '.Append("  and DocumentType like '" & strDocumentType & "%' ")
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            strFileName = dr("FileName").ToString()
            Exit While
        End While

        If Not dr.IsClosed Then dr.Close()
        
        Return strFileName
        db.CloseConnection()
    End Function

    Public Function GetLeadPhotoFileName(ByVal LeadId As String, ByVal strDocumentType As String) As String
        Dim db As New DataAccess
        Dim strFileName As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            '   .Append(" Select Distinct FileName + FileExtension as FileName from syDocumentHistory where LeadId = ? ")
            .Append(" Select Distinct Convert(VARCHAR(50),FileID)+Convert(VARCHAR(10),FileExtension) as FileName from syDocumentHistory where LeadId = ? ")
            .Append("  and DocumentType like '" & strDocumentType & "%' ")
        End With
        db.AddParameter("@StudentId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            strFileName = dr("FileName").ToString()
            Exit While
        End While

        If Not dr.IsClosed Then dr.Close()
        
        Return strFileName
        db.CloseConnection()
    End Function
    Public Function GetLeadPhotoDocumentType(ByVal LeadId As String, ByVal strDocumentType As String, ByVal EntityId As String) As String
        Dim db As New DataAccess
        Dim strFileName As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append(" Select Distinct Top 1 DocumentType from syDocumentHistory where " & EntityId & " =? ")
            .Append("  and DocumentType like '" & strDocumentType & "%' ")
        End With
        db.AddParameter("@StudentId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'Execute the query
        strFileName = db.RunParamSQLScalar(sb.ToString)
        Return strFileName
        db.CloseConnection()
    End Function
    Public Function DeleteDocument(ByVal strFileName As String, ByVal strModuleId As Integer, Optional ByVal adReqId As String = "") As String
        Dim db As New DataAccess
        Dim strExtension As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim intModule As Integer = 0
        Dim sb6 As New StringBuilder
        With sb6
            .Append("select Distinct ModuleId from adReqs where adReqId=? ")
        End With
        db.AddParameter("@DocumentId", adReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb6.ToString)
        While dr.Read()
            intModule = CType(dr("ModuleId"), Integer)
        End While
        db.ClearParameters()
        sb6.Remove(0, sb6.Length)

        Try
            Dim sb As New StringBuilder
            With sb
                .Append(" Delete from syDocumentHistory where FileName = ? and ModuleId=? ")
            End With
            db.AddParameter("@FileName", strFileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModuleId", intModule, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'Execute the query
            db.RunParamSQLDataReader(sb.ToString)
            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
            Return ""
        Catch ex As Exception
            Return "File Cannot Be Deleted"
        End Try
    End Function

    Public Function DeleteDocumentbyID(ByVal FileID As String, byVal IsDelete as Boolean, ByVal User As String) As String
        Dim db As New DataAccess
        Dim strExtension As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            Dim sb As New StringBuilder
            With sb
                If IsDelete Then
                    .Append("Delete From syDocumentHistory Where FileID = ? ")
                Else
                    .Append("Update syDocumentHistory Set IsArchived = 1,ModUser = '" & User & "'," & " ModDate = '" & Date.Now & "'" & " Where FileID = ? ")
                End If
                End With
            db.AddParameter("@FileID", FileID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
          
            'Execute the query
            db.RunParamSQLDataReader(sb.ToString)
            db.CloseConnection()
            Return ""
        Catch ex As Exception
            Return "Record Cannot Be Updated"
        End Try
    End Function
    Public Function GetAllDocumentsByStudentDocType(ByVal FileName As String, ByVal DocumentType As String, Optional ByVal isSupportUser As Boolean=False) As DataSet
        Dim db As New DataAccess
        Dim strExtension As String
        Dim isArchived = False
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append(" select 'Document' as Document, DisplayName,FileName, ")
            .Append(" upper(replace(FileExtension,'.','')) as DocumentCategory, ")
            .Append(" FileExtension,DocumentType,modUser,modDate, IsArchived  from syDocumentHistory where ")
            .Append(" FileName like  + ? + '%'")
            .Append(" and DocumentType = ? and studentid = ? ")
            If Not isSupportUser Then 
                .Append("and (IsArchived IS NULL OR IsArchived = 0)") 
            End If
        End With

        db.AddParameter("@FileName", FileName.Substring(0, FileName.IndexOf("_")), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@DocumentType", DocumentType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StudentID", FileName.Substring(FileName.IndexOf("_") + 1), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Return db.RunParamSQLDataSet(sb.ToString)
        db.CloseConnection()
    End Function

    Public Function GetAllDocumentsByStudentIdandDocumentID(ByVal StudentID As String, ByVal DocumentID As String, Optional ByVal isSupportUser As Boolean=False) As DataSet
        Dim db As New DataAccess
        Dim strExtension As String
        Dim isArchived = False
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append(" select FileID,'Document' as Document, DisplayName,FileName, ")
            .Append("  (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=32)+DocumentType+'\'+CONVERT(varchar(50),FileID)+FileExtension  AS FileURL, ")
            .Append(" upper(replace(FileExtension,'.','')) as DocumentCategory, ")
            .Append(" FileExtension,DocumentType,modUser,modDate, IsArchived from syDocumentHistory where ")
            ''.Append(" FileName like  + ? + '%'")
            .Append("   studentid = ? ")
            If DocumentID <> "" Then
                .Append(" and DocumentID = ? ")
            End If 
            If Not isSupportUser Then
                .Append("and (IsArchived IS NULL OR IsArchived = 0)")
            End If
        End With
        ''    db.AddParameter("@FileName", FileName.Substring(0, FileName.IndexOf("_")), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StudentID", StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If DocumentID <> "" Then
            db.AddParameter("@DocumentID", DocumentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        'Execute the query
        Return db.RunParamSQLDataSet(sb.ToString)
        db.CloseConnection()
    End Function
    Public Function GetAllDocumentsByIdentifier(ByVal StudentDocId As String) As DataSet
        Dim db As New DataAccess
        Dim strExtension As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append(" select t1.DocumentId,t1.StudentDocId,t2.DocumentDescrip from ")
            .Append(" plStudentDocs t1,cmDocuments t2 ")
            .Append(" where t1.DocumentId = t2.DocumentId and t1.StudentDocId = ? and img_ContentType Is not null")
        End With

        db.AddParameter("@StudentDocId", StudentDocId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Return db.RunParamSQLDataSet(sb.ToString)
        db.CloseConnection()
    End Function

    Public Function IsStudentDocumentAlreadyCreated(ByVal StudentID As String, ByVal DocumentID As String) As Boolean

        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@StudentID", New Guid(StudentID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@DocumentID", New Guid(DocumentID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Try
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GetStudentDocumentID")
        Catch ex As System.Exception

        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Return True
            End If
        End If
        Return False
    End Function

    Public Function IsLeadDocumentAlreadyCreated(ByVal LeadID As String, ByVal DocumentID As String) As Boolean

        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@LeadID", New Guid(LeadID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@DocumentID", New Guid(DocumentID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Try
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GetLeadDocumentID")
        Catch ex As System.Exception

        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Return True
            End If
        End If
        Return False
    End Function

End Class
