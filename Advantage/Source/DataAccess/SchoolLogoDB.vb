Imports System.Data.SqlClient
Imports System.Text
Imports FAME.Advantage.Common

Public Class SchoolLogoDB

    Public Function InsertImage(ByVal images As Byte(), ByVal contentLength As Int64, 
                                ByVal imgFile As String, ByVal isOfficialUse As Boolean,
                                Optional ByVal imageCode As String = "") As String

        Dim imgId As Integer
        Dim connection As String = GetConnectionString()
        Dim db As New SqlConnection(connection)
        db.Open()

        Try
            Dim sb As New StringBuilder
            'build query to get maximum ImgId
            sb.Append("SELECT MAX(ImgId)AS MaxImgId FROM sySchoolLogo")

            Dim cmd As SqlCommand = New SqlCommand(sb.ToString, db)
            cmd.Prepare()
            Dim obj As Object = cmd.ExecuteScalar
            If Not (obj Is DBNull.Value) Then
                imgId = obj
            End If

            sb.Length = 0
            With sb
                'build query to insert into sySchoolLogo
                .Append("INSERT INTO sySchoolLogo (ImgId,Image,ImgLen,ImgFile,OfficialUse,ImageCode) ")
                .Append("VALUES (@ImgId,@Image,@ImgLen,@ImgFile,@OfficialUse,@ImageCode)")
            End With

            cmd = New SqlCommand(sb.ToString, db)

            cmd.Parameters.Add("@ImgId", SqlDbType.Int).Value = imgId + 1
            cmd.Parameters.Add("@Image", SqlDbType.Image, contentLength).Value = images
            cmd.Parameters.Add("@ImgLen", SqlDbType.BigInt).Value = contentLength
            cmd.Parameters.Add("@ImgFile", SqlDbType.VarChar, 50).Value = imgFile
            cmd.Parameters.Add("@OfficialUse", SqlDbType.Bit, 1).Value = isOfficialUse
            cmd.Parameters.Add("@ImageCode",SqlDbType.Text,30).Value = imageCode
            
            cmd.Prepare()

            cmd.ExecuteNonQuery()

            Return ""

        Finally
            db.Close()
        End Try
    End Function

    Public Function GetImage(Optional ByVal imgId As Integer = 0) As Byte()
        Dim arrImage() As Byte

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        Dim db As New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString"))
        db.Open()
        Dim cmd As SqlCommand
        Try
            Dim sb As New StringBuilder
            If ImgId = 0 Then
                'build query to retrieve image as an array of bytes
                sb.Append("SELECT Image FROM sySchoolLogo WHERE ImgId=(SELECT MAX(ImgId)AS MaxImgId FROM sySchoolLogo)")

                cmd = New SqlCommand(sb.ToString, db)

            Else
                'build query to retrieve image as an array of bytes
                sb.Append("SELECT Image FROM sySchoolLogo WHERE ImgId=@ImageId")

                cmd = New SqlCommand(sb.ToString, db)

                cmd.Parameters.Add("@ImageId", SqlDbType.Int).Value = ImgId
            End If

            cmd.Prepare()
            Dim dr As SqlDataReader = cmd.ExecuteReader()

            dr.Read()
            arrImage = CType(dr("Image"), Byte())

            Return arrImage

        Finally

            db.Close()

        End Try
    End Function

    Public Function GetImageByCode(ByVal logoCode As String) As Byte()
        Dim arrImage() As Byte
        Const sql As String = "SELECT Image FROM sySchoolLogo WHERE ImageCode=@LogoCode"
        Dim connection As String = GetConnectionString()
        Dim db As New SqlConnection(connection)
        db.Open()
        Dim cmd As SqlCommand
        Dim sb As New StringBuilder
        'build query to retrieve image as an array of bytes
        sb.Append(sql)
        cmd = New SqlCommand(sb.ToString, db)
        cmd.Parameters.Add("@LogoCode", SqlDbType.VarChar,30).Value = logoCode
        cmd.Prepare()
        Dim dr As SqlDataReader = cmd.ExecuteReader()
        dr.Read()
        arrImage = CType(dr("Image"), Byte())
        Return arrImage
    End Function

    ''' <summary>
    ''' Return -1 if the image does not exists
    ''' else return the imageid.
    ''' </summary>
    ''' <param name="logoCode">The code logo.</param>
    ''' <returns>The imageId else -1</returns>
    Public Function GetImageIdByCode(ByVal logoCode As String) As Int32
        Dim idImage As Int32 = -1
        Const sql As String = "SELECT ImgId FROM sySchoolLogo WHERE ImageCode=@LogoCode"
        Dim connection As String = GetConnectionString()
        Dim db As New SqlConnection(connection)
        db.Open()
        Try
            Dim cmd As SqlCommand
            Dim sb As New StringBuilder
            'build query to retrieve image as an array of bytes
            sb.Append(sql)
            cmd = New SqlCommand(sb.ToString, db)
            cmd.Parameters.Add("@LogoCode", SqlDbType.VarChar,30).Value = logoCode
            cmd.Prepare()
            Dim dr As SqlDataReader = cmd.ExecuteReader()
            If dr.HasRows Then
                dr.Read()
                idImage = dr("ImgId")
            End If
            dr.Close()
            Return idImage
        Finally
            db.Close()
        End Try
    End Function



    Private Function GetConnectionString() As String
        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        Return myAdvAppSettings.AppSettings("ConnectionString")

    End Function


    Public Function UpdateImage(ByVal imageId As Int32, ByVal imageStr As Byte(), ByVal contentLength As Long, ByVal strImgFile As String,
                                ByVal isOfficialUse As Boolean, ByVal logoCode As String) As String
        Dim connection As String = GetConnectionString()
        Dim db As New SqlConnection(connection)
        db.Open()

        Try
            Dim sb As New StringBuilder
            With sb
                'build query to insert into sySchoolLogo
                .Append("UPDATE sySchoolLogo SET Image = @Image,ImgLen = @ImgLen,ImgFile = @ImgFile,OfficialUse=@OfficialUse,ImageCode=@ImageCode ")
                .Append("WHERE ImgId = @ImgId")
            End With

            Dim cmd As SqlCommand = New SqlCommand(sb.ToString, db)

            cmd.Parameters.Add("@ImgId", SqlDbType.Int).Value = imageId
            cmd.Parameters.Add("@Image", SqlDbType.Image, contentLength).Value = imageStr
            cmd.Parameters.Add("@ImgLen", SqlDbType.BigInt).Value = contentLength
            cmd.Parameters.Add("@ImgFile", SqlDbType.VarChar, 50).Value = strImgFile
            cmd.Parameters.Add("@OfficialUse", SqlDbType.Bit, 1).Value = isOfficialUse
            cmd.Parameters.Add("@ImageCode",SqlDbType.Text,30).Value = logoCode
            
            cmd.Prepare()

            cmd.ExecuteNonQuery()

            Return ""

        Finally
            db.Close()
        End Try
    End Function
End Class
