Imports System.Data
Imports FAME.Advantage.Common

Public Class atClsSectAttedanceDB

#Region "Private Data Members"

    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")
    Public m_AsOfDate As DateTime = Convert.ToDateTime(Date.Now)

#End Region


#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

    Public ReadOnly Property EndDate() As DateTime
        Get
            Return m_AsOfDate
        End Get
    End Property

    Public Function GetScheduledHoursForStudent(ByVal stuEnrollId As String, ByVal OffSetDate As String) As Integer
        Dim attDB As New ClsSectAttendanceDB
        Dim attDt As New DataTable
        Dim i As Integer = 0
        Dim ScheduledHours As Integer = 0
        attDt = attDB.GetAllClsSectionsForStudent(stuEnrollId)
        If attDt.Rows.Count > 0 Then
            For i = 0 To attDt.Rows.Count - 1
                ScheduledHours = ScheduledHours + GetCollectionOfMeetingDates(attDt.Rows(i)("ClsSectionId").ToString, stuEnrollId, OffSetDate, stuEnrollId)
            Next

        End If
        Return ScheduledHours

    End Function

    Public Function GetCompletedHoursForStudent(ByVal stuEnrollId As String, ByVal OffSetDate As String) As Integer
        Dim attDB As New ClsSectAttendanceDB
        Dim attDt As New DataTable
        Dim i As Integer = 0
        Dim CompletedHours As Integer = 0
        attDt = attDB.GetAllClsSectionsForStudent(stuEnrollId)
        If attDt.Rows.Count > 0 Then
            For i = 0 To attDt.Rows.Count - 1
                CompletedHours = CompletedHours + GetCollectionOfCompletedDates(attDt.Rows(i)("ClsSectionId").ToString, stuEnrollId, OffSetDate, stuEnrollId)
            Next

        End If
        Return CompletedHours

    End Function

    Private Function GetHoursCompleted(ByVal csm As List(Of AdvantageClassSectionMeeting), ByVal csma As List(Of AdvantageClassSectionMeetingAttendance), ByVal reportDate As Date) As Decimal

        'update hoursCompleted
        Dim hoursCompleted As Decimal = 0.0
        For Each meeting As AdvantageClassSectionMeetingAttendance In csma
            If meeting.MeetingDateAndTime <= reportDate.Add(New TimeSpan(23, 59, 59)) Then
                'hoursCompleted += meeting.Actual * GetMeetingDuration(csm, meeting.MeetingDateAndTime)
                hoursCompleted += meeting.Actual
            End If
        Next

        Return hoursCompleted

    End Function

    Public Function GetCollectionOfMeetingDates(ByVal clsSectionId As String, ByVal stuEnrollId As String, ByVal OffSetDate As String, ByVal EnrollId As String) As Integer

        'get collection of holidays
        Dim collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday) = (New HolidaysDB).GetCollectionOfHolidayDates()
        Dim totalDuration As Integer = 0
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("       0 as Type, ")
            .Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            .Append("		CSM.StartDate, ")
            .Append("       CSM.EndDate, ")
            '.Append("		P.MeetDays, ")
            .Append("       (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("		(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime ")
            .Append("FROM	arClassSections CS, arClsSectMeetings CSM, syPeriods P,atClsSectAttendance atcls ")
            .Append("WHERE ")
            .Append("       CS.ClsSectionId = CSM.ClsSectionId ")
            .Append("AND	CSM.PeriodId=P.PeriodId ")
            .Append("AND    CS.ClsSectionId = ? ")
            .Append(" and  CSM.StartDate>=(select startDate from  arStuEnrollments where StuEnrollId= ? ) ")
            .Append(" and CSM.EndDate<= ? ")
            .Append(" and atcls.stuenrollid = ? ")
            .Append("UNION ALL ")
            .Append("SELECT ")
            .Append("       1 as Type, ")
            .Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            .Append("		CSM.StartDate, ")
            .Append("       CSM.EndDate, ")
            '.Append("		P.MeetDays, ")
            .Append("       (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("		(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime ")
            .Append("FROM	arClassSections CS, arClsSectMeetings CSM, syPeriods P,atClsSectAttendance atcls ")
            .Append("WHERE ")
            .Append("       CS.ClsSectionId = CSM.ClsSectionId ")
            .Append("AND	CSM.AltPeriodId=P.PeriodId ")
            .Append("AND    CS.ClsSectionId = ? ")
            .Append(" and  CSM.StartDate>=(select startDate from  arStuEnrollments where StuEnrollId= ? ) ")
            .Append(" and CSM.EndDate<= ? ")
            .Append(" and atcls.stuenrollid = ? ")
        End With

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@EndDate", Date.Now, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", EnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@EndDate", Date.Now, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", EnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        'this is the collection 
        'Dim collectionOfClassSectionMeetings As System.Collections.Generic.List(Of AdvantageClassSectionMeeting) = New System.Collections.Generic.List(Of AdvantageClassSectionMeeting)

        While dr.Read()

            'set the type of week to process
            Dim selectWeek As Integer = 0 'all weeks
            If Not dr("IsAltPeriod") Is System.DBNull.Value Then
                If dr("Type") = 0 Then
                    selectWeek = 1  'even weeks
                Else
                    selectWeek = 2  'odd weeks
                End If
            End If

            'generate list of meeting dates
            Dim listOfDates As System.Collections.Generic.List(Of Date) = AdvantageCommonValues.GetCollectionOfDates(dr("StartDate"), dr("EndDate"), dr("MeetDays"), selectWeek)

            For Each [date] As Date In listOfDates
                'parse the start time of the meeting
                Dim meetingDateAndTime As DateTime = Date.Parse([date].Year.ToString + "-" + [date].Month.ToString + "-" + [date].Day.ToString + " " + CType(dr("StartTime"), Date).Hour.ToString + ":" + CType(dr("StartTime"), Date).Minute.ToString)

                'calculate the duration of the meeting
                Dim ts As TimeSpan = CType(dr("EndTime"), Date).Subtract(CType(dr("StartTime"), Date))
                Dim duration As Integer = ts.Hours * 60 + ts.Minutes

                'add the Class Section Meeting to the collection
                Dim acsm As AdvantageClassSectionMeeting = New AdvantageClassSectionMeeting(meetingDateAndTime, duration)

                'add it to the collection only if it is not a holiday
                If Not IsHoliday(acsm, collectionOfHolidays) Then
                    totalDuration = totalDuration + duration
                    'collectionOfClassSectionMeetings.Add(acsm)
                End If
            Next
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return collection of AdvantageClassSectionMeetings. Sort it by date
        'collectionOfClassSectionMeetings.Sort()
        Return totalDuration

    End Function

    Public Function GetCollectionOfCompletedDates(ByVal clsSectionId As String, ByVal stuEnrollId As String, ByVal OffSetDate As String, ByVal EnrollId As String) As Integer
        'Dim csma As List(Of AdvantageClassSectionMeetingAttendance)
        'Dim csm As List(Of AdvantageClassSectionMeeting)
        'Dim hoursCompleted As Decimal = 0.0

        'csma = (New AttendanceDB).GetCollectionOfMeetingDatesAttendance(clsSectionId, stuEnrollId)
        'csm = (New PeriodsDB).GetCollectionOfMeetingDates(clsSectionId)

        'hoursCompleted += GetHoursCompleted(csm, csma, m_AsOfDate)



        'get collection of holidays
        Dim collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday) = (New HolidaysDB).GetCollectionOfHolidayDates()
        Dim CompletedDuration As Integer = 0
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("       0 as Type, ")
            .Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            .Append("		CSM.StartDate, ")
            .Append("       CSM.EndDate, ")
            '.Append("		P.MeetDays, ")
            .Append("       (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("		(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime ")
            .Append("FROM	arClassSections CS, arClsSectMeetings CSM, syPeriods P,atClsSectAttendance atcls,arStuEnrollments ars ")
            .Append("WHERE ")
            .Append("       CS.ClsSectionId = CSM.ClsSectionId ")
            .Append("AND	CSM.PeriodId=P.PeriodId ")
            .Append("AND    CS.ClsSectionId = ? ")
            .Append(" and  CSM.StartDate>=(select startDate from  arStuEnrollments where StuEnrollId= ? ) ")
            .Append(" and CSM.EndDate<= ? ")
            .Append(" and atcls.stuenrollid = ? ")
            .Append(" and atcls.StuEnrollId = ars.StuEnrollId ")
            .Append("UNION ALL ")
            .Append("SELECT ")
            .Append("       1 as Type, ")
            .Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            .Append("		CSM.StartDate, ")
            .Append("       CSM.EndDate, ")
            '.Append("		P.MeetDays, ")
            .Append("       (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("		(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime ")
            .Append("FROM	arClassSections CS, arClsSectMeetings CSM, syPeriods P,atClsSectAttendance atcls,arStuEnrollments ars ")
            .Append("WHERE ")
            .Append("       CS.ClsSectionId = CSM.ClsSectionId ")
            .Append("AND	CSM.AltPeriodId=P.PeriodId ")
            .Append("AND    CS.ClsSectionId = ? ")
            .Append(" and  CSM.StartDate>=(select startDate from  arStuEnrollments where StuEnrollId= ? ) ")
            .Append(" and CSM.EndDate<= ? ")
            .Append(" and atcls.stuenrollid = ? ")
            .Append(" and atcls.StuEnrollId = ars.StuEnrollId ")

        End With

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@EndDate", Date.Now, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", EnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@EndDate", Date.Now, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", EnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        'this is the collection 
        'Dim collectionOfClassSectionMeetings As System.Collections.Generic.List(Of AdvantageClassSectionMeeting) = New System.Collections.Generic.List(Of AdvantageClassSectionMeeting)

        While dr.Read()

            'set the type of week to process
            Dim selectWeek As Integer = 0 'all weeks
            If Not dr("IsAltPeriod") Is System.DBNull.Value Then
                If dr("Type") = 0 Then
                    selectWeek = 1  'even weeks
                Else
                    selectWeek = 2  'odd weeks
                End If
            End If

            'generate list of meeting dates
            Dim listOfDates As System.Collections.Generic.List(Of Date) = AdvantageCommonValues.GetCollectionOfDates(dr("StartDate"), dr("EndDate"), dr("MeetDays"), selectWeek)

            For Each [date] As Date In listOfDates
                'parse the start time of the meeting
                Dim meetingDateAndTime As DateTime = Date.Parse([date].Year.ToString + "-" + [date].Month.ToString + "-" + [date].Day.ToString + " " + CType(dr("StartTime"), Date).Hour.ToString + ":" + CType(dr("StartTime"), Date).Minute.ToString)

                'calculate the duration of the meeting
                Dim ts As TimeSpan = CType(dr("EndTime"), Date).Subtract(CType(dr("StartTime"), Date))
                Dim duration As Integer = ts.Hours * 60 + ts.Minutes

                'add the Class Section Meeting to the collection

                Dim acsm As AdvantageClassSectionMeeting = New AdvantageClassSectionMeeting(meetingDateAndTime, duration)
                'hoursCompleted += GetHoursCompleted(csm, csma, m_AsOfDate)
                'add it to the collection only if it is not a holiday
                If Not IsHoliday(acsm, collectionOfHolidays) Then
                    CompletedDuration = CompletedDuration + duration
                    'collectionOfClassSectionMeetings.Add(acsm)
                End If
            Next
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return collection of AdvantageClassSectionMeetings. Sort it by date
        'collectionOfClassSectionMeetings.Sort()
        Return CompletedDuration
    End Function

    Private Function IsHoliday(ByVal classSectionMeeting As AdvantageClassSectionMeeting, ByVal collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday)) As Boolean
        'calculate end of class Section meeting
        Dim endOfClassSectionMeeting As DateTime = classSectionMeeting.MeetingDateAndTime.AddMinutes(classSectionMeeting.Duration)

        For Each holiday As AdvantageHoliday In collectionOfHolidays

            'calculate end of the holiday
            Dim endOfHoliday As DateTime = holiday.HolidayDateAndTime.AddMinutes(holiday.Duration)

            'it is not a holiday if the endtime of the class section meeting is earlier than the start time of the holiday OR
            'the start time of the class section meeting is later that the start time of the holiday.
            If (endOfClassSectionMeeting.CompareTo(holiday.HolidayDateAndTime) > -1) And (classSectionMeeting.MeetingDateAndTime.CompareTo(endOfHoliday) < 1) Then Return True
        Next
        'return false
        Return False
    End Function
#End Region


#Region "Public Methods"

    Public Function GetclassSectionAttendance(ByVal paramInfo As ReportParamInfo) As DataSet

        '   connect to the database
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strWhere As String
        Dim strStartDate As String = String.Empty
        '' Dim percentage As Integer
        Dim strStuID As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If
        ''Commented by saraswathi lakshmanan while testing reports
        'If paramInfo.FilterOtherString <> "" Then
        '    percentage = CInt(paramInfo.FilterOtherString)
        'End If
        If StudentIdentifier = "SSN" Then
            strStuID = "arStudent.SSN AS StudentIdentifier,"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStuID = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
        ElseIf StudentIdentifier = "StudentId" Then
            strStuID = "arStudent.StudentNumber AS StudentIdentifier,"

        End If

        If MyAdvAppSettings.AppSettings("UseCohortStartDateForFilters", paramInfo.CampusId).ToLower = "yes" Then
            strStartDate = "arstuenrollments.CohortStartDate"
            paramInfo.ShowSortBy = True
        Else
            strStartDate = "arstuenrollments.StartDate"
        End If


        Try
            '   build the sql query
            Dim sb As New StringBuilder
            With sb
                If MyAdvAppSettings.AppSettings("TrackSapAttendance", paramInfo.CampusId).ToLower = "byclass" Then
                    .Append("SELECT distinct arstuenrollments.StuEnrollId,   arstudent.lastname +' '+ arstudent.firstname as StudentName," & strStuID)
                    .Append("  syCampGrps.CampGrpDescrip as CampGrpDescrip, ")
                    .Append(" (select CampDescrip from syCampuses C where C.CampusId=syCmpGrpCmps.CampusId) as CampDesrip,")
                    .Append(" (select PrgVerDescrip from arPrgVersions where PrgVerid = arStuEnrollments.PrgVerId) as prgVerDescrip," & strStartDate & " as CohortStartDate ")
                    .Append(",dbo.UDF_IsMakingSAP(arstuenrollments.StuEnrollId) AS MakingSAP ")
                    .Append(" from ")
                    .Append("   arstudent,arstuenrollments,atclssectattendance,arClassSections,syCampGrps ,syCmpGrpCmps,syCampuses where  ")
                    .Append("   atclssectattendance.StuEnrollId = arstuenrollments.StuEnrollId and ")
                    .Append(" arstuenrollments.StudentId = arstudent.StudentId ")
                    .Append(" and arstuenrollments.Campusid=syCmpGrpCmps.campusid ")
                    .Append(" and syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
                    .Append(" AND arStuEnrollments.CampusId=syCampuses.CampusId ")
                    .Append(" and atclssectattendance.ClsSectionId=arClassSections.ClsSectionId ")
                    .Append(strWhere)
                    '.Append("and atclssectattendance.actual=1  ")
                    .Append(" group by arStuEnrollments.PrgVerId,arstuenrollments.StuEnrollId," & strStartDate & ", " & strStuID.Replace(" AS StudentIdentifier", ""))
                    .Append(" arstudent.lastname,arstudent.firstname,syCampGrps.CampGrpDescrip,syCmpGrpCmps.CampusId   ")
                    If MyAdvAppSettings.AppSettings("UseImportedAttendance", paramInfo.CampusId).ToLower = "true" Then
                        .Append("union SELECT distinct arstuenrollments.StuEnrollId,   arstudent.lastname +' '+ arstudent.firstname as StudentName," & strStuID)
                        .Append("  syCampGrps.CampGrpDescrip as CampGrpDescrip, ")
                        .Append(" (select CampDescrip from syCampuses C where C.CampusId=syCmpGrpCmps.CampusId) as CampDesrip,")
                        .Append(" (select PrgVerDescrip from arPrgVersions where PrgVerid = arStuEnrollments.PrgVerId) as prgVerDescrip," & strStartDate & " as CohortStartDate ")
                        .Append(",dbo.UDF_IsMakingSAP(arstuenrollments.StuEnrollId) AS MakingSAP ")
                        .Append(" from ")
                        .Append("   arstudent,arstuenrollments,arStudentClockAttendance,syCampGrps ,syCmpGrpCmps,syCampuses where  ")
                        .Append("   arStudentClockAttendance.StuEnrollId = arstuenrollments.StuEnrollId and ")
                        .Append(" arstuenrollments.StudentId = arstudent.StudentId ")
                        .Append(" and arstuenrollments.Campusid=syCmpGrpCmps.campusid ")
                        .Append(" and syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
                        .Append(" AND arStuEnrollments.CampusId=syCampuses.CampusId ")
                        .Append(" and arStudentClockAttendance.converted = 1 ")
                        .Append(strWhere.Replace("atclssectattendance.MeetDate", "arStudentClockAttendance.RecordDate"))
                        '.Append("and atclssectattendance.actual=1  ")
                        .Append(" group by arStuEnrollments.PrgVerId,arstuenrollments.StuEnrollId," & strStartDate & ", " & strStuID.Replace(" AS StudentIdentifier", ""))
                        .Append(" arstudent.lastname,arstudent.firstname,syCampGrps.CampGrpDescrip,syCmpGrpCmps.CampusId   ")
                    End If

                    If paramInfo.OrderBy <> "" Then
                        .Append(" order by " & strStartDate & ",StudentName")
                    Else
                        .Append(" order by StudentName")
                    End If


                Else
                    .Append("SELECT distinct arstuenrollments.StuEnrollId,   arstudent.lastname +' '+ arstudent.firstname as StudentName," & strStuID)
                    .Append("  syCampGrps.CampGrpDescrip as CampGrpDescrip, ")
                    .Append(" (select CampDescrip from syCampuses C where C.CampusId=syCmpGrpCmps.CampusId) as CampDesrip,")
                    .Append(" (select PrgVerDescrip from arPrgVersions where PrgVerid = arStuEnrollments.PrgVerId) as prgVerDescrip,arstuenrollments.CohortStartDate ")
                    .Append(",dbo.UDF_IsMakingSAP(arstuenrollments.StuEnrollId) AS MakingSAP ")
                    .Append(" from ")
                    .Append("   arstudent,arstuenrollments,arStudentClockAttendance,syCampGrps ,syCmpGrpCmps,Sycampuses where  ")
                    .Append("   arStudentClockAttendance.StuEnrollId = arstuenrollments.StuEnrollId and ")
                    .Append(" arstuenrollments.StudentId = arstudent.StudentId ")
                    .Append(" and arstuenrollments.Campusid=syCmpGrpCmps.campusid ")
                    .Append(" and syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
                    .Append(" AND arStuEnrollments.CampusId=syCampuses.CampusId ")
                    If strWhere.Contains("atclssectattendance.MeetDate") Then
                        .Append(strWhere.Replace("atclssectattendance.MeetDate", "arStudentClockAttendance.RecordDate"))
                    Else
                        .Append(strWhere.Replace("arClassSections.StartDate", "arStudentClockAttendance.RecordDate"))
                    End If

                    '.Append("and atclssectattendance.actual=1  ")
                    .Append(" group by arStuEnrollments.PrgVerId,arstuenrollments.StuEnrollId,arstuenrollments.CohortStartDate, " & strStuID.Replace(" AS StudentIdentifier", ""))
                    .Append(" arstudent.lastname,arstudent.firstname,syCampGrps.CampGrpDescrip,syCmpGrpCmps.CampusId   ")
                    .Append(" order by StudentName")
                End If


            End With

            ds = db.RunParamSQLDataSet(sb.ToString)
            'Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            'Dim ds1 As New DataSet

            'Dim dt As New DataTable("ClassSectionAttendance")
            'If ds.Tables.Count > 0 Then

            '    dt.Columns.Add(New DataColumn("StuEnrollId", System.Type.GetType("System.String")))
            '    dt.Columns.Add(New DataColumn("StudentIdentifier", System.Type.GetType("System.String")))
            '    dt.Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            '    dt.Columns.Add(New DataColumn("ScheduledHours", System.Type.GetType("System.String")))
            '    dt.Columns.Add(New DataColumn("CompletedHours", System.Type.GetType("System.String")))
            '    dt.Columns.Add(New DataColumn("Attendance", System.Type.GetType("System.String")))

            '    Dim row As DataRow
            '    While dr.Read()

            '        row = dt.NewRow()
            '        row("StuEnrollId") = dr("StuEnrollId").ToString
            '        row("StudentIdentifier") = dr("StudentIdentifier").ToString
            '        row("StudentName") = dr("StudentName").ToString

            '        row("ScheduledHours") = GetScheduledHoursForStudent(dr("StuEnrollId").ToString, m_AsOfDate) / 60
            '        row("CompletedHours") = GetCompletedHoursForStudent(dr("StuEnrollId").ToString, m_AsOfDate) / 60

            '        'If row("ScheduledHours") = 0 Then
            '        '    row("Attendance") = 0
            '        'Else
            '        '    'row("Attendance") = ((GetCompletedHoursForStudent(dr("StuEnrollId").ToString, m_AsOfDate) / GetScheduledHoursForStudent(dr("StuEnrollId").ToString, m_AsOfDate))) * 100
            '        '    row("Attendance") = CInt((row("ScheduledHours")) / CInt(row("CompletedHours"))) * 100
            '        'End If

            '        'If CInt(row("Attendance")) <= percentage Then
            '        dt.Rows.Add(row)
            '        'End If

            '    End While

            '    ds1.Tables.Add(dt)
            'End If
            Return ds
        Catch ex As Exception
            Return ds
        End Try
    End Function

#End Region

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

End Class
