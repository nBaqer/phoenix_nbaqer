Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' TuitionCategoriesDB.vb
'
' TuitionCategoriesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class TuitionCategoriesDB
    Public Function GetAllTuitionCategories(ByVal showActiveOnly As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   TC.TuitionCategoryId, TC.StatusId, TC.TuitionCategoryCode, TC.TuitionCategoryDescrip,ST.Status,ST.StatusId ")
            .Append("FROM     saTuitionCategories TC, syStatuses ST ")
            .Append("WHERE    TC.StatusId = ST.StatusId ")
            If showActiveOnly = "True" Then
                .Append("AND    ST.Status = 'Active' ")
                .Append(" Order By TC.TuitionCategoryDescrip ")
            ElseIf showActiveOnly = "False" Then
                .Append("AND    ST.Status = 'Inactive' ")
                .Append(" Order By TC.TuitionCategoryDescrip ")
            Else
                .Append("ORDER BY ST.Status,TC.TuitionCategoryDescrip asc")
            End If
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetTuitionCategoryInfo(ByVal TuitionCategoryId As String) As TuitionCategoryInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT TC.TuitionCategoryId, ")
            .Append("    TC.TuitionCategoryCode, ")
            .Append("    TC.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=TC.StatusId) As Status, ")
            .Append("    TC.TuitionCategoryDescrip, ")
            .Append("    TC.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=TC.CampGrpId) As CampGrpDescrip, ")
            .Append("    TC.ModUser, ")
            .Append("    TC.ModDate ")
            .Append("FROM  saTuitionCategories TC ")
            .Append("WHERE TC.TuitionCategoryId= ? ")
        End With

        ' Add the TuitionCategoryId to the parameter list
        db.AddParameter("@TuitionCategoryId", TuitionCategoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim TuitionCategoryInfo As New TuitionCategoryInfo

        While dr.Read()

            '   set properties with data from DataReader
            With TuitionCategoryInfo
                .TuitionCategoryId = TuitionCategoryId
                .IsInDB = True
                .Code = dr("TuitionCategoryCode")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Description = dr("TuitionCategoryDescrip")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return TuitionCategoryInfo
        Return TuitionCategoryInfo

    End Function
    Public Function UpdateTuitionCategoryInfo(ByVal TuitionCategoryInfo As TuitionCategoryInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saTuitionCategories Set TuitionCategoryId = ?, TuitionCategoryCode = ?, ")
                .Append(" StatusId = ?, TuitionCategoryDescrip = ?, CampGrpId = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE TuitionCategoryId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saTuitionCategories where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   TuitionCategoryId
            db.AddParameter("@TuitionCategoryId", TuitionCategoryInfo.TuitionCategoryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TuitionCategoryCode
            db.AddParameter("@TuitionCategoryCode", TuitionCategoryInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", TuitionCategoryInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TuitionCategoryDescrip
            db.AddParameter("@TuitionCategoryDescrip", TuitionCategoryInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If TuitionCategoryInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", TuitionCategoryInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TuitionCategoryId
            db.AddParameter("@AdmDepositId", TuitionCategoryInfo.TuitionCategoryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", TuitionCategoryInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddTuitionCategoryInfo(ByVal TuitionCategoryInfo As TuitionCategoryInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saTuitionCategories (TuitionCategoryId, TuitionCategoryCode, StatusId, ")
                .Append("   TuitionCategoryDescrip, CampGrpId, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   TuitionCategoryId
            db.AddParameter("@TuitionCategoryId", TuitionCategoryInfo.TuitionCategoryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TuitionCategoryCode
            db.AddParameter("@TuitionCategoryCode", TuitionCategoryInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", TuitionCategoryInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TuitionCategoryDescrip
            db.AddParameter("@TuitionCategoryDescrip", TuitionCategoryInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If TuitionCategoryInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", TuitionCategoryInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteTuitionCategoryInfo(ByVal TuitionCategoryId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saTuitionCategories ")
                .Append("WHERE TuitionCategoryId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM saTuitionCategories WHERE TuitionCategoryId = ? ")
            End With

            '   add parameters values to the query

            '   TuitionCategoryId
            db.AddParameter("@TuitionCategoryId", TuitionCategoryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TuitionCategoryId
            db.AddParameter("@TuitionCategoryId", TuitionCategoryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
End Class
