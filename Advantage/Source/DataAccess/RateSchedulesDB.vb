Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' RateSchedulesDB.vb
'
' RateSchedulesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class RateSchedulesDB
    Public Function GetAllRateSchedules() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   CCT.RateScheduleId, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As StatusSelect, ")
            .Append("         CCT.RateScheduleCode, ")
            .Append("         CCT.RateScheduleDescrip ")
            .Append("FROM     saRateSchedules CCT, syStatuses ST ")
            .Append("WHERE    CCT.StatusId = ST.StatusId ")
            .Append("ORDER BY CCT.RateScheduleDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetRateScheduleInfo(ByVal RateScheduleId As String) As RateScheduleInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT CCT.RateScheduleId, ")
            .Append("    CCT.RateScheduleCode, ")
            .Append("    CCT.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=CCT.StatusId) As Status, ")
            .Append("    CCT.RateScheduleDescrip, ")
            .Append("    CCT.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=CCT.CampGrpId) As CampGrpDescrip, ")
            .Append("    CCT.ModUser, ")
            .Append("    CCT.ModDate ")
            .Append("FROM  saRateSchedules CCT ")
            .Append("WHERE CCT.RateScheduleId= ? ")
        End With

        ' Add the RateScheduleId to the parameter list
        db.AddParameter("@RateScheduleId", RateScheduleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim RateScheduleInfo As New RateScheduleInfo

        While dr.Read()

            '   set properties with data from DataReader
            With RateScheduleInfo
                .RateScheduleId = RateScheduleId
                .IsInDB = True
                .Code = dr("RateScheduleCode")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Description = dr("RateScheduleDescrip")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return RateScheduleInfo
        Return RateScheduleInfo

    End Function
    Public Function UpdateRateScheduleInfo(ByVal RateScheduleInfo As RateScheduleInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saRateSchedules Set RateScheduleId = ?, RateScheduleCode = ?, ")
                .Append(" StatusId = ?, RateScheduleDescrip = ?, CampGrpId = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE RateScheduleId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saRateSchedules where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   RateScheduleId
            db.AddParameter("@RateScheduleId", RateScheduleInfo.RateScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   RateScheduleCode
            db.AddParameter("@RateScheduleCode", RateScheduleInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", RateScheduleInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   RateScheduleDescrip
            db.AddParameter("@RateScheduleDescrip", RateScheduleInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If RateScheduleInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", RateScheduleInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   RateScheduleId
            db.AddParameter("@AdmDepositId", RateScheduleInfo.RateScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", RateScheduleInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddRateScheduleInfo(ByVal RateScheduleInfo As RateScheduleInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saRateSchedules (RateScheduleId, RateScheduleCode, StatusId, ")
                .Append("   RateScheduleDescrip, CampGrpId, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   RateScheduleId
            db.AddParameter("@RateScheduleId", RateScheduleInfo.RateScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   RateScheduleCode
            db.AddParameter("@RateScheduleCode", RateScheduleInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", RateScheduleInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   RateScheduleDescrip
            db.AddParameter("@RateScheduleDescrip", RateScheduleInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If RateScheduleInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", RateScheduleInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteRateScheduleInfo(ByVal RateScheduleId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saRateSchedules ")
                .Append("WHERE RateScheduleId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM saRateSchedules WHERE RateScheduleId = ? ")
            End With

            '   add parameters values to the query

            '   RateScheduleId
            db.AddParameter("@RateScheduleId", RateScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   RateScheduleId
            db.AddParameter("@RateScheduleId", RateScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetRateSchedulesDS() As DataSet

        '   create dataset
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   build the sql query for the RateScheduleDetails data adapter
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       RSD.RateScheduleDetailId, ")
            .Append("       RSD.RateScheduleId, ")
            .Append("       RSD.MinUnits, ")
            .Append("       RSD.MaxUnits, ")
            .Append("       RSD.TuitionCategoryId, ")
            .Append("       RSD.FlatAmount, ")
            .Append("       RSD.Rate, ")
            .Append("       RSD.UnitId, ")
            .Append("       (Select Case RSD.UnitId When 0 then 'Credit Hour' else 'Clock Hour' end) Unit, ")
            .Append("       RSD.ViewOrder, ")
            .Append("       RSD.ModUser, ")
            .Append("       RSD.ModDate, ")
            .Append("       ST.StatusId, ")
            .Append("       ST.Status ")
            .Append("FROM   saRateScheduleDetails RSD, saRateSchedules RS, syStatuses ST ")
            .Append("WHERE  RSD.RateScheduleId=RS.RateScheduleId ")
            .Append(" AND   RS.StatusId = ST.StatusId ")
            .Append("ORDER BY ST.Status,RS.RateScheduleDescrip asc")
        End With

        '   build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

        '   Create adapter to handle RateScheduleDetails table
        Dim da As New OleDbDataAdapter(sc)

        '   Fill RateScheduleDetails table
        da.Fill(ds, "RateScheduleDetails")

        '   build select query for the RateSchedules data adapter
        '   build the sql query
        sb = New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT RS.RateScheduleId, ")
            .Append("    RS.RateScheduleCode, ")
            .Append("    (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("    RS.RateScheduleDescrip, ")
            .Append("    RS.CampGrpId, ")
            .Append("    RS.ModUser, ")
            .Append("    RS.ModDate, ")
            .Append("    ST.StatusId, ")
            .Append("    ST.Status ")
            .Append("FROM  saRateSchedules RS, syStatuses ST ")
            .Append("WHERE  RS.StatusId = ST.StatusId ")
            .Append("ORDER BY ST.Status,RS.RateScheduleDescrip asc")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   fill saRateSchedules table
        da.Fill(ds, "RateSchedules")

        '   build select query for the TuitionCategories data adapter
        '   build the sql query
        sb = New StringBuilder
        With sb
            .Append("SELECT   TC.TuitionCategoryId, TC.StatusId, TC.TuitionCategoryCode, TC.TuitionCategoryDescrip ")
            .Append("FROM     saTuitionCategories TC, syStatuses ST ")
            .Append("WHERE    TC.StatusId = ST.StatusId ")
            .Append("ORDER BY TC.TuitionCategoryDescrip ")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   fill TuitionCategories table
        da.Fill(ds, "TuitionCategories")

        '   create primary and foreign key constraints

        '   set primary key for saRateScheduleDetails table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("RateScheduleDetails").Columns("RateScheduleDetailId")
        ds.Tables("RateScheduleDetails").PrimaryKey = pk0

        '   set primary key for saRateSchedules table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("RateSchedules").Columns("RateScheduleId")
        ds.Tables("RateSchedules").PrimaryKey = pk1

        '   set primary key for saTuitionCategories table
        Dim pk2(0) As DataColumn
        pk2(0) = ds.Tables("TuitionCategories").Columns("TuitionCategoryId")
        ds.Tables("TuitionCategories").PrimaryKey = pk2

        '   set foreign key column in saRateScheduleDetails
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("RateScheduleDetails").Columns("RateScheduleId")

        '   set foreign key column in saRateScheduleDetails
        Dim fk1(0) As DataColumn
        fk1(0) = ds.Tables("RateScheduleDetails").Columns("TuitionCategoryId")

        '   add relationship
        ds.Relations.Add("RateSchedulesRateScheduleDetails", pk1, fk0)

        '   add relationship
        ds.Relations.Add("TuitionCategoriesRateScheduleDetails", pk2, fk1)

        '   return dataset
        Return ds

    End Function
    Public Function UpdateRateSchedulesDS(ByVal ds As DataSet) As String

        '   all updates must be encapsulated as one transaction
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            '   build the sql query for the RateScheduleDetails data adapter
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("       RSD.RateScheduleDetailId, ")
                .Append("       RSD.RateScheduleId, ")
                .Append("       RSD.MinUnits, ")
                .Append("       RSD.MaxUnits, ")
                .Append("       RSD.TuitionCategoryId, ")
                .Append("       RSD.FlatAmount, ")
                .Append("       RSD.Rate, ")
                .Append("       RSD.UnitId, ")
                .Append("       RSD.ViewOrder, ")
                .Append("       RSD.ModUser, ")
                .Append("       RSD.ModDate ")
                .Append("FROM   saRateScheduleDetails RSD ")
            End With

            '   build select command
            Dim RateScheduleDetailsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle RateScheduleDetails table
            Dim RateScheduleDetailsDataAdapter As New OleDbDataAdapter(RateScheduleDetailsSelectCommand)

            '   build insert, update and delete commands for RateScheduleDetails table
            Dim cb As New OleDbCommandBuilder(RateScheduleDetailsDataAdapter)

            '   build select query for the RateSchedules data adapter
            sb = New StringBuilder
            With sb
                '   with subqueries
                .Append("SELECT RS.RateScheduleId, ")
                .Append("    RS.RateScheduleCode, ")
                .Append("    RS.StatusId, ")
                .Append("    RS.RateScheduleDescrip, ")
                .Append("    RS.CampGrpId, ")
                .Append("    RS.ModUser, ")
                .Append("    RS.ModDate ")
                .Append("FROM  saRateSchedules RS ")
            End With

            '   build select command
            Dim RateSchedulesSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle RateSchedules table
            Dim RateSchedulesDataAdapter As New OleDbDataAdapter(RateSchedulesSelectCommand)

            '   build insert, update and delete commands for RateSchedules table
            Dim cb1 As New OleDbCommandBuilder(RateSchedulesDataAdapter)

            '   insert added rows in RateSchedules table
            RateSchedulesDataAdapter.Update(ds.Tables("RateSchedules").Select(Nothing, Nothing, DataViewRowState.Added))

            '   insert added rows in RateScheduleDetails table
            RateScheduleDetailsDataAdapter.Update(ds.Tables("RateScheduleDetails").Select(Nothing, Nothing, DataViewRowState.Added))

            '   delete rows in RateScheduleDetails table
            RateScheduleDetailsDataAdapter.Update(ds.Tables("RateScheduleDetails").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   delete rows in RateSchedules table
            RateSchedulesDataAdapter.Update(ds.Tables("RateSchedules").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   update rows in RateScheduleDetails table
            RateScheduleDetailsDataAdapter.Update(ds.Tables("RateScheduleDetails").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   update rows in RateSchedules table
            RateSchedulesDataAdapter.Update(ds.Tables("RateSchedules").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            Return ""

        Catch ex As OleDb.OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally

            '   close connection
            connection.Close()
        End Try

    End Function
End Class