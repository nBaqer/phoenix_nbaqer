
Imports FAME.AdvantageV1.DataAccess.FAME.DataAccessLayer
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common.Reports.NoAttendance

Public Class PortalAttendanceDB

#Region "Private Data Members"
    'Private MyAdvAppSettings as PortalAdvAppSettings = GetAdvAppSettings()
    Private ReadOnly mStudentIdentifier As String = GetAdvAppSettings().AppSettings("StudentIdentifier")
    Private ReadOnly mStartDate As DateTime = Convert.ToDateTime("04/25/2005")
    Private ReadOnly mEndDate As DateTime = Convert.ToDateTime("05/31/2005")

#End Region

#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return mStudentIdentifier
        End Get
    End Property

    Public ReadOnly Property EndDate() As DateTime
        Get
            Return mEndDate
        End Get
    End Property

    Public ReadOnly Property StartDate() As DateTime
        Get
            Return mStartDate
        End Get
    End Property

#End Region

#Region "Public Methods"

    Public Function GetClsSectionAttendance(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strWhere As String
        Dim strStuID
        Dim strCmpGrp As String

        Dim myAdvAppSettings As PortalAdvAppSettings = GetAdvAppSettings()
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        If paramInfo.FilterList <> "" Then
            Dim temp As String = paramInfo.FilterList
            Dim idx1 As Integer = temp.IndexOf("syCampGrps.CampGrpId ", StringComparison.Ordinal)
            Dim idx2 As Integer = temp.IndexOf(" AND ", StringComparison.Ordinal)
            If idx1 > -1 And idx2 > idx1 Then
                strCmpGrp = temp.Substring(idx1, idx2 - idx1)
            End If
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
        End If

        'Get student identifier depending on what field school is using.
        If StudentIdentifier = "SSN" Then
            strStuID = "S.SSN AS StudentIdentifier,"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStuID = "E.EnrollmentId AS StudentIdentifier,"
        ElseIf StudentIdentifier = "StudentId" Then
            strStuID = "S.StudentNumber AS StudentIdentifier,"
        End If

        Dim strSuppressDate As String
        Try
            strSuppressDate = myAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
        Catch ex As Exception
            strSuppressDate = "no"
        End Try
        With sb
            'Query with subqueries
            'Class Section Information
            .Append("SELECT ")
            .Append("       arClassSections.ClsSectionId,arClassSections.TermId,arTerm.TermDescrip,")
            .Append("       arClassSections.ReqId,arReqs.Descrip,arReqs.Code,arReqs.Credits,arClassSections.ClsSection,")
            .Append("       arClassSections.InstructorId,(SELECT FullName FROM syUsers WHERE UserId=arClassSections.InstructorId) AS InstructorName,")
            .Append("       arClassSections.StartDate,arClassSections.EndDate,arClassSections.MaxStud,")
            .Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,arClassSections.CampusId,")
            .Append("       (SELECT CampDescrip FROM syCampuses WHERE CampusId=arClassSections.CampusId) AS CampusDescrip,")
            .Append("       (SELECT COUNT(*) FROM arResults WHERE TestId=arClassSections.ClsSectionId) AS EnrollmentCount ")
            .Append("FROM   arClassSections,arTerm,arReqs,syCampGrps,syCmpGrpCmps F ")
            .Append("WHERE  arClassSections.TermId=arTerm.TermId AND arClassSections.ReqId=arReqs.ReqId ")
            .Append("       AND syCampGrps.CampGrpId=F.CampGrpId AND arClassSections.CampusId=F.CampusId ")
            .Append(strWhere)
            .Append("ORDER BY syCampGrps.CampGrpDescrip,CampusDescrip,arTerm.TermDescrip")
            .Append(";")
            'Enrolled Students
            .Append("SELECT ")
            .Append("       t1.StuEnrollId,S.LastName,S.FirstName,S.MiddleName," & strStuID)
            .Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,E.CampusId,")
            .Append("       (SELECT CampDescrip FROM syCampuses WHERE CampusId=E.CampusId) AS CampusDescrip,t1.TestId, ")
            .Append("'" & strSuppressDate & "'")
            .Append(" as SuppressDate ")
            .Append("FROM   arResults t1,arStuEnrollments E,arStudent S,syCampGrps,syCmpGrpCmps F ")
            .Append("WHERE  t1.StuEnrollId=E.StuEnrollId AND E.StudentId=S.StudentId ")
            .Append("       AND EXISTS (SELECT arClassSections.ClsSectionId ")
            .Append("                   FROM arClassSections,arTerm,arReqs,syCampGrps,syCmpGrpCmps F ")
            .Append("                   WHERE arClassSections.TermId = arTerm.TermId ")
            .Append("                   AND arClassSections.ReqId=arReqs.ReqId ")
            .Append("                   AND syCampGrps.CampGrpId=F.CampGrpId ")
            .Append("                   AND arClassSections.CampusId=F.CampusId ")
            .Append("                   AND t1.TestId=arClassSections.ClsSectionId " & strWhere & ")")
            .Append("       AND syCampGrps.CampGrpId=F.CampGrpId ")
            .Append("       AND E.CampusId=F.CampusId AND " & strCmpGrp)
            .Append("ORDER BY S.LastName,S.FirstName,S.MiddleName")
            .Append(";")
            'Meeting Days
            .Append("SELECT DISTINCT ")
            .Append("       t1.WorkDaysId,t2.WorkDaysDescrip,t3.TimeIntervalDescrip,t4.TimeIntervalDescrip AS EndTime,t1.ClsSectionId ")
            .Append("FROM   arClsSectMeetings t1,plWorkDays t2,cmTimeInterval t3,cmTimeInterval t4 ")
            .Append("WHERE  t1.WorkDaysId=t2.WorkDaysId AND t1.TimeIntervalId=t3.TimeIntervalId ")
            .Append("       AND t1.EndIntervalId=t4.TimeIntervalId ")
            .Append("       AND EXISTS (SELECT arClassSections.ClsSectionId ")
            .Append("                   FROM arClassSections,arTerm,arReqs,syCampGrps,syCmpGrpCmps F ")
            .Append("                   WHERE arClassSections.TermId=arTerm.TermId ")
            .Append("                   AND arClassSections.ReqId=arReqs.ReqId ")
            .Append("                   AND syCampGrps.CampGrpId=F.CampGrpId ")
            .Append("                   AND arClassSections.CampusId=F.CampusId ")
            .Append("                   AND t1.ClsSectionId=arClassSections.ClsSectionId " & strWhere & ")")
            .Append(";")
            'Attendance Posted
            .Append("SELECT ")
            .Append("       t1.StuEnrollId,t1.MeetDate,t1.Actual,t1.Tardy,t1.Excused,t1.ClsSectionId ")
            .Append("FROM   atClsSectAttendance t1 ")
            .Append("WHERE  EXISTS (SELECT arClassSections.ClsSectionId ")
            .Append("               FROM arClassSections,arTerm,arReqs,syCampGrps,syCmpGrpCmps F ")
            .Append("               WHERE arClassSections.TermId = arTerm.TermId ")
            .Append("               AND arClassSections.ReqId=arReqs.ReqId ")
            .Append("               AND syCampGrps.CampGrpId=F.CampGrpId ")
            .Append("               AND arClassSections.CampusId=F.CampusId ")
            .Append("               AND t1.ClsSectionId=arClassSections.ClsSectionId " & strWhere & ")")
            .Append("       AND t1.MeetDate >= '04/25/2005' ")
            .Append("       AND t1.MeetDate <= '05/31/2005' ")
        End With

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count = 4 Then
            ds.Tables(0).TableName = "ClsSectionInfo"
            ds.Tables(1).TableName = "StudentsAttendance"
            ds.Tables(2).TableName = "MeetingDays"
            ds.Tables(3).TableName = "AttendancePosted"
            'Add new column on ClsSectionInfo table
            ds.Tables(0).Columns.Add(New DataColumn("ClsSectionIdStr", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("Date1", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("Date2", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("Date3", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("Date4", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("Date5", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("Date6", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("Date7", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("Date8", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("Date9", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("Date10", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("Date11", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("Date12", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("Date13", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("Date14", Type.GetType("System.String")))


            'Add new columns on StudentsAttendance table
            ds.Tables(1).Columns.Add(New DataColumn("TestIdStr", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("StudentName", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("StuEnrollIdStr", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("Att1", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("Att2", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("Att3", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("Att4", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("Att5", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("Att6", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("Att7", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("Att8", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("Att9", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("Att10", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("Att11", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("Att12", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("Att13", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("Att14", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("Att15", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("Att16", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("Att17", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("Att18", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("Att19", Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("Att20", Type.GetType("System.String")))
            'Add new column on AttendancePosted table
            ds.Tables(3).Columns.Add(New DataColumn("ClsSectionIdStr", Type.GetType("System.String")))
            ds.Tables(3).Columns.Add(New DataColumn("StuEnrollIdStr", Type.GetType("System.String")))
            'Add new table for DatesInRange
            Dim dt As New DataTable("DatesInRange")
            ds.Tables.Add(dt)
            dt.Columns.Add(New DataColumn("DateInRange", Type.GetType("System.DateTime")))
            dt.Columns.Add(New DataColumn("Label", Type.GetType("System.String")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Public Function GetNoAttendancePosted(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strDateRange As String
        Dim clsSectionStartDate As String = String.Empty
        Dim clsSectionEndDate As String = String.Empty

        Dim myAdvAppSettings As PortalAdvAppSettings = GetAdvAppSettings()

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            'Only one FilterOther param
            strDateRange = paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy = "," & paramInfo.OrderBy
        End If
        Dim dates() As String = paramInfo.FilterOtherString.Trim().Replace("AND", ",").Replace("Meeting Dates Between (", "").Replace(")", "").Split(",")
        If dates.Length = 2 Then
            clsSectionStartDate = dates(0)
            clsSectionEndDate = dates(1)
        End If

        Dim strSuppressDate As String
        Try
            strSuppressDate = myAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
        Catch ex As Exception
            strSuppressDate = "no"
        End Try
        'US3580 2/6/2013 Janet Robinson
        With sb
            'Query with subqueries
            .Append("SELECT ")
            .Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,arClassSections.CampusId,")
            .Append("       (SELECT CampDescrip FROM syCampuses WHERE CampusId=arClassSections.CampusId) AS CampusDescrip,")
            .Append("       arClassSections.ClsSectionId,arClassSections.TermId,arTerm.TermDescrip,arClassSections.ReqId,arReqs.Descrip,arReqs.Code,")
            .Append("       arReqs.Credits,arClassSections.ClsSection,arTerm.StartDate,arTerm.EndDate,arClassSections.InstructorId,")
            .Append("       syUsers.FullName AS InstructorName,syUsers.Email AS InstructorEmail, ")
            .Append("'" & strSuppressDate & "'")
            .Append(" as SuppressDate ")
            .Append("FROM   arClassSections,syCampGrps,syCmpGrpCmps F,arClassSectionTerms,arTerm,arReqs,syUsers,syCampuses  ")
            .Append("WHERE  arClassSections.InstructorId IS NOT NULL ")
            .Append(" and arClassSections.StartDate >='" & clsSectionStartDate & "' and arClassSections.EndDate <= '" & clsSectionEndDate & "'")
            '.Append(" AND UnitTypeId <> (SELECT UnitTypeId FROM arAttUnitType WHERE UnitTypeDescrip = 'None') ")
            .Append("       AND NOT EXISTS (SELECT * FROM atClsSectAttendance WHERE ClsSectionId=arClassSections.ClsSectionId AND " & strDateRange & ")")
            .Append("       AND syCampGrps.CampGrpId=F.CampGrpId ")
            .Append("       AND arClassSections.CampusId=F.CampusId ")
            .Append("       AND arClassSections.ClsSectionId=arClassSectionTerms.ClsSectionId ")
            .Append("       AND arClassSectionTerms.TermId=arTerm.TermId ")
            .Append("       AND arClassSections.ReqId=arReqs.ReqId ")
            .Append("       AND syUsers.UserId=arClassSections.InstructorId ")
            .Append(" AND dbo.arClassSections.CampusId=dbo.syCampuses.CampusId ")
            .Append(strWhere)
            .Append(" ORDER BY syCampGrps.CampGrpDescrip,CampusDescrip,InstructorName,arTerm.StartDate,arTerm.EndDate")
            .Append(strOrderBy)
        End With

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count = 1 Then
            ds.Tables(0).TableName = "NoAttendancePosted"
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Public Function GetAbsencesSummary(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strWhere As String
        'Dim strOrderBy As String
        Dim strStuId As String

        Dim myAdvAppSettings As PortalAdvAppSettings = GetAdvAppSettings()

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        'If paramInfo.OrderBy <> "" Then
        '    strOrderBy = "," & paramInfo.OrderBy
        'End If

        If StudentIdentifier = "SSN" Then
            strStuId = "arStudent.SSN"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStuId = "arStuEnrollments.EnrollmentId"
        ElseIf StudentIdentifier = "StudentId" Then
            strStuId = "arStudent.StudentNumber"
        End If

        Dim strSuppressDate As String
        Try
            strSuppressDate = myAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
        Catch ex As Exception
            strSuppressDate = "no"
        End Try
        With sb
            'Query with subqueries

            .Append("select distinct ClsSectionId,InstructorId,InstructorName,ReqId,ReqDescrip,ReqCode,ClsSection,StuEnrollId,LastName, ")
            .Append(" FirstName,MiddleName,StudentIdentifier,PrgVerId,PrgVerDescrip,StatusCodeId,StatusCodeDescrip,sum(AbsenceCount) as AbsenceCount,")
            .Append(" TermId,StartDate,EndDate,TermDescrip,CampGrpId,CampGrpDescrip,CampusId,CampDescrip, ")
            .Append("'" & strSuppressDate & "'")
            .Append(" as SuppressDate ")
            .Append(" from ")
            .Append("(SELECT ")
            .Append("       A.ClsSectionId,arClassSections.InstructorId,(SELECT FullName FROM syUsers WHERE UserId=arClassSections.InstructorId) AS InstructorName,")
            .Append("       arClassSections.ReqId,(SELECT Descrip FROM arReqs WHERE ReqId=arClassSections.ReqId) AS ReqDescrip,")
            .Append("       (SELECT Code FROM arReqs WHERE ReqId=arClassSections.ReqId) AS ReqCode,arClassSections.ClsSection,")
            .Append("       A.StuEnrollId,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName," & strStuId & " AS StudentIdentifier,")
            .Append("       arStuEnrollments.PrgVerId,(SELECT PrgVerDescrip FROM arPrgVersions WHERE PrgVerId=arStuEnrollments.PrgVerId) AS PrgVerDescrip,")
            .Append("       arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip,")


            'Added new code by Theresa G on 21 oct 2010
            .Append(" (Case (select tracktardies from arprgversions where prgverid=arStuEnrollments.PrgVerId and tracktardies=1 ) when 1 then ")
            .Append(" (case isnull((SELECT COUNT(*) FROM atClsSectAttendance T WHERE T.ClsSectionId=A.ClsSectionId  AND T.StuEnrollId=A.StuEnrollId AND  T.Actual=1 and T.Tardy=1 and T.Excused=0),0) ")
            .Append(" when 0 then (SELECT COUNT(*) FROM  atClsSectAttendance T WHERE T.ClsSectionId=A.ClsSectionId AND T.StuEnrollId=A.StuEnrollId AND T.Actual=0 and T.Excused=0)  else ((SELECT ")
            .Append(" COUNT(*) FROM       atClsSectAttendance T WHERE T.ClsSectionId=A.ClsSectionId AND T.StuEnrollId=A.StuEnrollId AND  T.Actual=0 and T.Excused=0)+   case (isnull((SELECT  ")
            .Append(" TardiesMakingAbsence FROM    arprgversions WHERE  TrackTardies=1 and prgverid=arStuEnrollments.PrgVerId),0)) when 0 then 0 else  (SELECT COUNT(*) FROM ")
            .Append(" atClsSectAttendance T WHERE T.ClsSectionId=A.ClsSectionId AND  T.StuEnrollId=A.StuEnrollId AND T.Actual=1 and T.Tardy=1 and T.Excused=0)/  (SELECT TardiesMakingAbsence FROM   arprgversions WHERE  ")
            .Append(" TrackTardies=1 and prgverid=arStuEnrollments.PrgVerId)  end  ) end) ")
            .Append(" else ")
            .Append(" (case isnull((SELECT COUNT(*) FROM atClsSectAttendance T WHERE T.ClsSectionId=A.ClsSectionId  AND T.StuEnrollId=A.StuEnrollId AND  T.Actual=1 and T.Tardy=1 and T.Excused=0),0) when 0 then ")
            .Append(" (SELECT COUNT(*) FROM  atClsSectAttendance T WHERE T.ClsSectionId=A.ClsSectionId AND T.StuEnrollId=A.StuEnrollId AND T.Actual=0 and T.Excused=0)  else ((SELECT COUNT(*) FROM ")
            .Append(" atClsSectAttendance T WHERE T.ClsSectionId=A.ClsSectionId AND T.StuEnrollId=A.StuEnrollId AND  T.Actual=0 and T.Excused=0)+   case (isnull((SELECT TardiesMakingAbsence FROM   arClassSections C,arReqs ")
            .Append(" R WHERE  C.ClsSectionId=A.ClsSectionId AND C.ReqId=R.ReqId AND R.TrackTardies=1),0)) when 0 then 0 else  (SELECT COUNT(*) FROM atClsSectAttendance T WHERE T.ClsSectionId=A.ClsSectionId AND ")
            .Append("  T.StuEnrollId=A.StuEnrollId AND T.Actual=1 and T.Tardy=1 and T.Excused=0)/  ( SELECT TardiesMakingAbsence FROM    arClassSections C,arReqs R WHERE  C.ClsSectionId=A.ClsSectionId AND ")
            .Append(" C.ReqId=R.ReqId AND R.TrackTardies=1)  end  ) end) ")
            .Append(" end)  AS AbsenceCount,")


            'end new code

            .Append("       arClassSections.TermId,arTerm.StartDate,arTerm.EndDate,arTerm.TermDescrip,")
            .Append("       syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,arClassSections.CampusId,syCampuses.CampDescrip ")
            .Append("FROM   atClsSectAttendance A,arClassSections,syUsers U,arStuEnrollments,arStudent,arTerm,arClassSectionTerms,syCmpGrpCmps,syCampGrps,syCampuses ")
            .Append("WHERE  A.ClsSectionId=arClassSections.ClsSectionId")
            .Append("       AND arStuEnrollments.StuEnrollId=A.StuEnrollId")
            .Append("       AND arStuEnrollments.StudentId=arStudent.StudentId")
            .Append("       AND arClassSections.ClsSectionId=arClassSectionTerms.ClsSectionId")
            .Append("       AND arClassSectionTerms.TermId=arTerm.TermId")
            .Append("       AND syCmpGrpCmps.CampusId=arClassSections.CampusId")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId")
            .Append("       AND syCampuses.CampusId=arClassSections.CampusId")
            .Append(strWhere)
            .Append("GROUP BY syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,arClassSections.CampusId,")
            .Append("           syCampuses.CampDescrip,arTerm.StartDate,arTerm.EndDate,arTerm.TermDescrip,")
            .Append("           arClassSections.TermId,arClassSections.InstructorId,A.ClsSectionId,")
            .Append("           arClassSections.ReqId,arClassSections.ClsSection,A.StuEnrollId,A.Actual,")
            .Append("           arStudent.LastName,arStudent.FirstName,arStudent.MiddleName," & strStuId & ",")
            .Append("           arStuEnrollments.PrgVerId,arStuEnrollments.StatusCodeId ")
            .Append("HAVING A.Actual=0 ")
            .Append(" and (SELECT COUNT(*) FROM atClsSectAttendance T WHERE ")
            .Append(" T.ClsSectionId=A.ClsSectionId AND T.StuEnrollId=A.StuEnrollId AND T.Actual=0 and T.Excused=0)>0 ")
            .Append(" Union ")
            .Append(" SELECT   A.ClsSectionId,arClassSections.InstructorId,(SELECT FullName FROM syUsers WHERE UserId=arClassSections.InstructorId) AS ")
            .Append(" InstructorName,arClassSections.ReqId,(SELECT Descrip FROM arReqs WHERE ReqId=arClassSections.ReqId) AS ReqDescrip, ")
            .Append(" (SELECT Code FROM arReqs WHERE ReqId=arClassSections.ReqId) AS ReqCode,arClassSections.ClsSection, ")
            .Append(" A.StuEnrollId,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName," & strStuId & " AS StudentIdentifier, ")
            .Append(" arStuEnrollments.PrgVerId,(SELECT PrgVerDescrip FROM arPrgVersions WHERE PrgVerId=arStuEnrollments.PrgVerId) AS PrgVerDescrip, ")
            .Append(" arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) ")
            .Append(" AS StatusCodeDescrip,")


            'Added new code by Theresa G on 21 oct 2010
            .Append(" (Case (select tracktardies from arprgversions where prgverid=arStuEnrollments.PrgVerId and tracktardies=1 ) when 1 then ")
            .Append(" (case isnull((SELECT COUNT(*) FROM atClsSectAttendance T WHERE T.ClsSectionId=A.ClsSectionId  AND T.StuEnrollId=A.StuEnrollId AND  T.Actual=1 and T.Tardy=1 and T.Excused=0),0) ")
            .Append(" when 0 then (SELECT COUNT(*) FROM  atClsSectAttendance T WHERE T.ClsSectionId=A.ClsSectionId AND T.StuEnrollId=A.StuEnrollId AND T.Actual=0 and T.Excused=0)  else ((SELECT ")
            .Append(" COUNT(*) FROM       atClsSectAttendance T WHERE T.ClsSectionId=A.ClsSectionId AND T.StuEnrollId=A.StuEnrollId AND  T.Actual=0 and T.Excused=0)+   case (isnull((SELECT  ")
            .Append(" TardiesMakingAbsence FROM    arprgversions WHERE  TrackTardies=1 and prgverid=arStuEnrollments.PrgVerId),0)) when 0 then 0 else  (SELECT COUNT(*) FROM ")
            .Append(" atClsSectAttendance T WHERE T.ClsSectionId=A.ClsSectionId AND  T.StuEnrollId=A.StuEnrollId AND T.Actual=1 and T.Tardy=1 and T.Excused=0)/  (SELECT TardiesMakingAbsence FROM   arprgversions WHERE  ")
            .Append(" TrackTardies=1 and prgverid=arStuEnrollments.PrgVerId)  end  ) end) ")
            .Append(" else ")
            .Append(" (case isnull((SELECT COUNT(*) FROM atClsSectAttendance T WHERE T.ClsSectionId=A.ClsSectionId  AND T.StuEnrollId=A.StuEnrollId AND  T.Actual=1 and T.Tardy=1 and T.Excused=0),0) when 0 then ")
            .Append(" (SELECT COUNT(*) FROM  atClsSectAttendance T WHERE T.ClsSectionId=A.ClsSectionId AND T.StuEnrollId=A.StuEnrollId AND T.Actual=0 and T.Excused=0)  else ((SELECT COUNT(*) FROM ")
            .Append(" atClsSectAttendance T WHERE T.ClsSectionId=A.ClsSectionId AND T.StuEnrollId=A.StuEnrollId AND  T.Actual=0 and T.Excused=0)+   case (isnull((SELECT TardiesMakingAbsence FROM   arClassSections C,arReqs ")
            .Append(" R WHERE  C.ClsSectionId=A.ClsSectionId AND C.ReqId=R.ReqId AND R.TrackTardies=1),0)) when 0 then 0 else  (SELECT COUNT(*) FROM atClsSectAttendance T WHERE T.ClsSectionId=A.ClsSectionId AND ")
            .Append("  T.StuEnrollId=A.StuEnrollId AND T.Actual=1 and T.Tardy=1 and T.Excused=0)/  ( SELECT TardiesMakingAbsence FROM    arClassSections C,arReqs R WHERE  C.ClsSectionId=A.ClsSectionId AND ")
            .Append(" C.ReqId=R.ReqId AND R.TrackTardies=1)  end  ) end) ")
            .Append(" end)  AS AbsenceCount,")


            'end new code
            .Append(" arClassSections.TermId,arTerm.StartDate,arTerm.EndDate,arTerm.TermDescrip,    ")
            .Append(" syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,arClassSections.CampusId,syCampuses.CampDescrip FROM  ")
            .Append(" atClsSectAttendance A,arClassSections,syUsers U,arStuEnrollments,arStudent,arTerm,arClassSectionTerms,syCmpGrpCmps,")
            .Append(" syCampGrps,syCampuses WHERE  A.ClsSectionId=arClassSections.ClsSectionId       AND arStuEnrollments.StuEnrollId=A.StuEnrollId    ")
            .Append(" AND arStuEnrollments.StudentId=arStudent.StudentId       AND arClassSections.ClsSectionId=arClassSectionTerms.ClsSectionId  ")
            .Append(" AND arClassSectionTerms.TermId=arTerm.TermId       AND syCmpGrpCmps.CampusId=arClassSections.CampusId       AND ")
            .Append(" syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId       AND syCampuses.CampusId=arClassSections.CampusId ")
            .Append(strWhere)
            .Append(" GROUP BY syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip, ")
            .Append(" arClassSections.CampusId,  syCampuses.CampDescrip,arTerm.StartDate,arTerm.EndDate,arTerm.TermDescrip,  ")
            .Append(" arClassSections.TermId,arClassSections.InstructorId,A.ClsSectionId,           arClassSections.ReqId,arClassSections.ClsSection,")
            .Append(" A.StuEnrollId,A.Actual, A.Tardy,A.Excused ,         arStudent.LastName,arStudent.FirstName,arStudent.MiddleName," & strStuId & " , ")
            .Append(" arStuEnrollments.PrgVerId, arStuEnrollments.StatusCodeId  HAVING A.Tardy = 1 ")
            .Append(" and (SELECT COUNT(*) FROM atClsSectAttendance T WHERE  T.ClsSectionId=A.ClsSectionId AND T.StuEnrollId=A.StuEnrollId  ")
            .Append(" AND T.Actual=0 and T.Excused=0 and T.Tardy=0)=0  )")
            '.Append(" ORDER BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,arTerm.StartDate,")
            '.Append("           arTerm.EndDate,arTerm.TermDescrip,InstructorName,ReqDescrip,ReqCode,")
            '.Append("           arStudent.LastName,arStudent.FirstName,arStudent.MiddleName," & strStuId & ",")
            '.Append("           PrgVerDescrip,StatusCodeDescrip")
            '.Append(strOrderBy)

            ' This is for Program Version Attendance.Right now disabled by Theresa 04/03/2007

            ' ''.Append(vbCrLf + " Union all " + vbCrLf)

            ' ''.Append("SELECT ")
            ' ''.Append("       A.ClsSectionId,arClassSections.InstructorId,(SELECT FullName FROM syUsers WHERE UserId=arClassSections.InstructorId) AS InstructorName,")
            ' ''.Append("       arClassSections.ReqId,(SELECT Descrip FROM arReqs WHERE ReqId=arClassSections.ReqId) AS ReqDescrip,")
            ' ''.Append("       (SELECT Code FROM arReqs WHERE ReqId=arClassSections.ReqId) AS ReqCode,arClassSections.ClsSection,")
            ' ''.Append("       A.StuEnrollId,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName," & strStuId & " AS StudentIdentifier,")
            ' ''.Append("       arStuEnrollments.PrgVerId,(SELECT PrgVerDescrip FROM arPrgVersions WHERE PrgVerId=arStuEnrollments.PrgVerId) AS PrgVerDescrip,")
            ' ''.Append("       arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip,")
            '' ''.Append("      (SELECT COUNT(*) FROM atClsSectAttendance T WHERE T.ClsSectionId=A.ClsSectionId AND T.StuEnrollId=A.StuEnrollId AND T.Actual=0 and T.Excused=0) AS AbsenceCount,")
            ' ''.Append(" (SELECT COUNT(*) FROM atClsSectAttendance T WHERE T.ClsSectionId=A.ClsSectionId AND T.StuEnrollId=A.StuEnrollId AND T.Actual=1 and T.Tardy=1 and T.Excused=0)/ ")
            ' ''.Append(" (select TardiesMakingAbsence from dbo.arPrgVersions where PrgVerId= arStuEnrollments.PrgVerId and TrackTardies=1)  AS AbsenceCount,")
            ' ''.Append("       arClassSections.TermId,arTerm.StartDate,arTerm.EndDate,arTerm.TermDescrip,")
            ' ''.Append("       syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,arClassSections.CampusId,syCampuses.CampDescrip ")
            ' ''.Append("FROM   atClsSectAttendance A,arClassSections,syUsers U,arStuEnrollments,arStudent,arTerm,arClassSectionTerms,syCmpGrpCmps,syCampGrps,syCampuses ")
            ' ''.Append("WHERE  A.ClsSectionId=arClassSections.ClsSectionId")
            ' ''.Append("       AND arStuEnrollments.StuEnrollId=A.StuEnrollId")
            ' ''.Append("       AND arStuEnrollments.StudentId=arStudent.StudentId")
            ' ''.Append("       AND arClassSections.ClsSectionId=arClassSectionTerms.ClsSectionId")
            ' ''.Append("       AND arClassSectionTerms.TermId=arTerm.TermId")
            ' ''.Append("       AND syCmpGrpCmps.CampusId=arClassSections.CampusId")
            ' ''.Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId")
            ' ''.Append("       AND syCampuses.CampusId=arClassSections.CampusId")
            ' ''.Append(strWhere)
            ' ''.Append(" GROUP BY syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,arClassSections.CampusId,")
            ' ''.Append("           syCampuses.CampDescrip,arTerm.StartDate,arTerm.EndDate,arTerm.TermDescrip,")
            ' ''.Append("           arClassSections.TermId,arClassSections.InstructorId,A.ClsSectionId,")
            ' ''.Append("           arClassSections.ReqId,arClassSections.ClsSection,A.StuEnrollId,A.Actual,")
            ' ''.Append("           arStudent.LastName,arStudent.FirstName,arStudent.MiddleName," & strStuId & ",")
            ' ''.Append("           arStuEnrollments.PrgVerId,arStuEnrollments.StatusCodeId ")
            ' ''.Append("HAVING A.Actual= ")
            ' ''.Append(" (case (select count(Actual) from atClsSectAttendance where Actual=0 and StuEnrollId=A.StuEnrollId) ")
            ' ''.Append(" when 0 then 1 else 0 end) ")
            ' ''.Append(" and (SELECT COUNT(*) FROM atClsSectAttendance T WHERE T.ClsSectionId=A.ClsSectionId AND T.StuEnrollId=A.StuEnrollId AND T.Actual=1 and T.Tardy=1 and T.Excused=0)>= ")
            ' ''.Append("(select TardiesMakingAbsence from dbo.arPrgVersions where PrgVerId= arStuEnrollments.PrgVerId and TrackTardies=1))")
            '' ''.Append(" ORDER BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,arTerm.StartDate,")
            '' ''.Append("           arTerm.EndDate,arTerm.TermDescrip,InstructorName,ReqDescrip,ReqCode,")
            '' ''.Append("           arStudent.LastName,arStudent.FirstName,arStudent.MiddleName," & strStuId & ",")
            '' ''.Append("           PrgVerDescrip,StatusCodeDescrip")
            '' ''.Append(strOrderBy)

            .Append(" AbsentTable where AbsenceCount>0 group by ClsSectionId,InstructorId,InstructorName,ReqId,ReqDescrip,ReqCode,ClsSection,StuEnrollId,LastName, ")
            .Append(" FirstName,MiddleName,StudentIdentifier,PrgVerId,PrgVerDescrip,StatusCodeId, ")
            .Append(" StatusCodeDescrip,AbsenceCount,TermId,StartDate,EndDate,TermDescrip,CampGrpId,CampGrpDescrip,CampusId, CampDescrip, ")
            .Append(" StatusCodeDescrip,TermId,StartDate,EndDate,TermDescrip,CampGrpId,CampGrpDescrip,CampusId, CampDescrip ")


            .Append(" ORDER BY CampGrpDescrip,CampDescrip,StartDate, EndDate,TermDescrip,InstructorName,ReqDescrip,ReqCode, ")
            .Append(" LastName, FirstName, MiddleName, StudentIdentifier, PrgVerDescrip, StatusCodeDescrip ")

        End With

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count = 1 Then
            ds.Tables(0).TableName = "StuAbsencesSummary"
            ' Add new columns.
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("StudentCount", Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("ClsSectionIdStr", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("TermStuCount", Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("InstructorStuCount", Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("ClsSectionStuCount", Type.GetType("System.Int32")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Public Function GetCollectionOfMeetingDatesAttendance(ByVal clsSectionId As String, ByVal stuEnrollId As String) As List(Of AdvantageClassSectionMeetingAttendance)

        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As PortalAdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select ")
            .Append("		MeetDate, ")
            .Append("		Actual, ")
            .Append("		Tardy, ")
            .Append("		Excused ")
            .Append("from	atClsSectAttendance ")
            .Append("where ")
            .Append("		ClsSectionId = ? ")
            .Append("and	StuEnrollId = ? ")
        End With

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        'this is the collection 
        Dim collectionOfClassSectionMeetingsAttendance As List(Of AdvantageClassSectionMeetingAttendance) = New List(Of AdvantageClassSectionMeetingAttendance)

        While dr.Read()

            'add the Class Section Meeting to the collection
            Dim acsma As AdvantageClassSectionMeetingAttendance = New AdvantageClassSectionMeetingAttendance(dr("MeetDate"), dr("Actual"), dr("Tardy"), dr("Excused"))
            collectionOfClassSectionMeetingsAttendance.Add(acsma)
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return collection of AdvantageClassSectionMeetingsAttendance. Sort it by date
        collectionOfClassSectionMeetingsAttendance.Sort()
        Return collectionOfClassSectionMeetingsAttendance

    End Function

    Public Function GetCollectionOfMeetingDatesAttendance(ByVal stuEnrollId As String) As List(Of AdvantageClassSectionMeetingAttendance)

        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As PortalAdvAppSettings = GetAdvAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select  ")
            .Append("		RecordDate, ")
            .Append("		SchedHours, ")
            .Append("		ActualHours  ")
            .Append("from arStudentClockAttendance  ")
            .Append("where ")
            .Append("       StuEnrollId = ? ")
            .Append("and    ActualHours < 999.00 ")
        End With

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        'this is the collection 
        Dim collectionOfClassSectionMeetingsAttendance As List(Of AdvantageClassSectionMeetingAttendance) = New List(Of AdvantageClassSectionMeetingAttendance)

        While dr.Read()

            'add the Class Section Meeting to the collection
            Dim acsma As AdvantageClassSectionMeetingAttendance = New AdvantageClassSectionMeetingAttendance(dr("RecordDate"), CType(dr("ActualHours"), Decimal), False, False)
            collectionOfClassSectionMeetingsAttendance.Add(acsma)
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()


        '   Return collection of AdvantageClassSectionMeetingsAttendance. Sort it by date
        collectionOfClassSectionMeetingsAttendance.Sort()
        Return collectionOfClassSectionMeetingsAttendance

    End Function

    Public Function GetLda(ByVal stuEnrollId As String) As DateTime

        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As PortalAdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select DATEADD(dd, 0, DATEDIFF(dd, 0, MAX(LDA))) from ")
            .Append("( ")
            .Append("	select max(AttendedDate)as LDA from arExternshipAttendance where StuEnrollId=? ")
            .Append("	union all ")
            .Append("	select max(MeetDate) as LDA from atClsSectAttendance where StuEnrollId=? and Actual >= 1 and (Actual <> 99.00 and Actual <> 999.00 and Actual <> 9999.00) ")
            .Append("	union all ")
            .Append("	select max(AttendanceDate) as LDA from atAttendance where EnrollId=? and Actual >=1 ")
            .Append("	union all ")
            .Append("	select max(RecordDate) as LDA from arStudentClockAttendance where StuEnrollId=? and (ActualHours >=1.00 and  ActualHours <> 99.00 and ActualHours <> 999.00 and ActualHours <> 9999.00) ")
            .Append("	union all ")
            .Append("	select max(MeetDate) as LDA from atConversionAttendance where StuEnrollId=? and (Actual >=1.00 and  Actual <> 99.00 and Actual <> 999.00 and Actual <> 9999.00) ")
            .Append(")	T0 ")
        End With

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim obj As Object = db.RunParamSQLScalar(sb.ToString)
        If obj Is Nothing Then
            Return Date.MinValue
        ElseIf obj Is DBNull.Value Then
            Return Date.MinValue
        Else
            Return CType(obj, Date)
        End If
    End Function

    Public Function GetLdaForTermCutOffDate(ByVal stuEnrollId As String, ByVal termCutOffDate As Date) As DateTime

        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As PortalAdvAppSettings = GetAdvAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select DATEADD(dd, 0, DATEDIFF(dd, 0, MAX(LDA))) from ")
            .Append("( ")
            .Append("	select max(AttendedDate)as LDA from arExternshipAttendance where StuEnrollId=?  and AttendedDate <= ? ")
            .Append("	union all ")
            .Append("	select max(MeetDate) as LDA from atClsSectAttendance where StuEnrollId=? and Actual >= 1 and MeetDate <= ? and (ActualHours >=1.00 and  ActualHours <> 99.00 and ActualHours <> 999.00 and ActualHours <> 9999.00)")
            .Append("	union all ")
            .Append("	select max(AttendanceDate) as LDA from atAttendance where EnrollId=? and Actual >=1 and AttendanceDate <= ? ")
            .Append("	union all ")
            .Append("	select max(RecordDate) as LDA from arStudentClockAttendance where StuEnrollId=? and (ActualHours >=1.00 and  ActualHours <> 99.00 and ActualHours <> 999.00 and ActualHours <> 9999.00) and RecordDate <= ? ")
            .Append("	union all ")
            .Append("	select max(MeetDate) as LDA from atConversionAttendance where StuEnrollId=? and (Actual >=1.00 and  Actual <> 99.00 and Actual <> 999.00 and Actual <> 9999.00) and MeetDate <= ? ")
            .Append("	union all ")
            .Append("	select LDA from arStuEnrollments where StuEnrollId=? and LDA <= ? ")
            .Append(")	T0 ")
        End With

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@cutoff", termCutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@cutoff", termCutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@cutoff", termCutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@cutoff", termCutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@cutoff", termCutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@cutoff", termCutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        '   Execute the query
        Dim obj As Object = db.RunParamSQLScalar(sb.ToString)
        If obj Is Nothing Then
            Return Date.MinValue
        ElseIf obj Is DBNull.Value Then
            Return Date.MinValue
        Else
            Return CType(obj, Date)
        End If
    End Function

    Public Function GetLDAForTermCutOffDate_SP(ByVal stuEnrollId As String, ByVal termCutOffDate As Date) As DateTime
        'Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim myAdvAppSettings As PortalAdvAppSettings = GetAdvAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        '   connect to the database
        '   Execute the query

        db.AddParameter("@stuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        'add cutOff date parameter
        db.AddParameter("@cutOffDate", termCutOffDate, SqlDbType.DateTime, , ParameterDirection.Input)
        Try
            Dim obj As Object = db.RunParamSQLScalar_SP("dbo.usp_GetLDAForTermCutOffDate")
            If obj Is Nothing Then
                Return Date.MinValue
            ElseIf obj Is DBNull.Value Then
                Return Date.MinValue
            Else
                Return CType(obj, Date)
            End If
        Catch ex As Exception
        Finally
            db.CloseConnection()
        End Try
        Return Date.MinValue
    End Function

    Public Function GetStudentLda(ByVal stuEnrollId As String) As DateTime

        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), PortalAdvAppSettings)
        Else
            myAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select DATEADD(dd, 0, DATEDIFF(dd, 0, LDA)) from arStuEnrollments where stuEnrollId= ? ")
        End With

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)



        '   Execute the query
        Dim obj As Object = db.RunParamSQLScalar(sb.ToString)
        If obj Is Nothing Then
            Return Date.MinValue
        ElseIf obj Is DBNull.Value Then
            Return Date.MinValue
        Else
            Return CType(obj, Date)
        End If
    End Function

    Public Function GetCalendarTemplate(ByVal repParamInfo As ReportParamInfo) As List(Of NoAttendanceCourses)
        Dim sb As StringBuilder = New StringBuilder()
        Dim resultList As List(Of NoAttendanceCourses) = New List(Of NoAttendanceCourses)()
        Dim att As NoAttendanceCourses
        Dim strWhere = If(String.IsNullOrWhiteSpace(repParamInfo.FilterList), String.Empty, " AND " & repParamInfo.FilterList)
        Dim strOrderBy = If(String.IsNullOrWhiteSpace(repParamInfo.OrderBy), String.Empty, "," & repParamInfo.OrderBy)

        'Get necesary information from database
        With sb
            .Append("SELECT    ")
            .Append("	arReqs.ReqId                   ")
            .Append("	, cm.ClsSectMeetingId          ")
            .Append("	, arReqs.Descrip               ")
            .Append("	, arClassSections.ClsSection   ")
            .Append("	, plWorkDays.WorkDaysDescrip   ")
            .Append("	, arClassSections.StartDate     ")
            .Append("	, arClassSections.EndDate  ")
            .Append("	, arClassSections.InstructorId ")
            .Append("   , arReqs.Code ")
            .Append("   , syUsers.Email ")
            .Append("   , syUsers.FullName AS InstructorName        ")
            .Append("   , ti1.TimeIntervalDescrip AS StartClassTime ")
            .Append("   , ti2.TimeIntervalDescrip AS EndClassTime ")
            .Append("   , arTerm.TermDescrip ")
            .Append("   , syCampGrps.CampGrpDescrip  ")
            .Append("   , syCampuses.CampDescrip As CampusDescrip  ")
            .Append("FROM arClsSectMeetings cm         ")
            .Append("JOIN arClassSections ON arClassSections.ClsSectionId = cm.ClsSectionId     ")
            .Append("JOIN  syCampuses ON syCampuses.CampusId = arClassSections.CampusId ")
            .Append("JOIN  syCmpGrpCmps ON syCmpGrpCmps.CampusId = syCampuses.CampusId ")
            .Append("JOIN  syCampGrps ON syCampGrps.CampGrpId = syCmpGrpCmps.CampGrpId ")
            .Append("JOIN arReqs ON arReqs.ReqId = arClassSections.ReqId                        ")
            .Append("JOIN syPeriods ON syPeriods.PeriodId = cm.PeriodId                         ")
            .Append("JOIN syPeriodsWorkDays ON syPeriodsWorkDays.PeriodId = syPeriods.PeriodId  ")
            .Append("JOIN  cmTimeInterval ti1 ON ti1.TimeIntervalId = syPeriods.StartTimeId  ")
            .Append("JOIN  cmTimeInterval ti2 ON ti2.TimeIntervalId = syPeriods.EndTimeId    ")
            .Append("JOIN  plWorkDays ON plWorkDays.WorkDaysId =  syPeriodsWorkDays.WorkDayId   ")
            .Append("JOIN  syUsers ON syUsers.UserId = dbo.arClassSections.InstructorId ")
            .Append("JOIN  arTerm ON arTerm.TermId = arClassSections.TermId ")
            .Append("WHERE arClassSections.InstructorId IS NOT NULL                     ")
            .Append("AND arClassSections.EndDate >= @StartDate                          ")
            .Append("AND arClassSections.StartDate <= @EndDate                      ")
            .Append(strWhere)
            .Append(" ORDER BY syCampGrps.CampGrpDescrip, CampusDescrip,InstructorName,arTerm.StartDate")
            .Append(strOrderBy)
        End With
        ''Get connection String
        Dim myAdvAppSettings As PortalAdvAppSettings = GetAdvAppSettings()
        Dim connS As String = EliminateProvider(myAdvAppSettings.AppSettings("ConString"))
        Dim conn As SqlConnection = New SqlConnection(connS)

        'Create command
        Dim command As SqlCommand = New SqlCommand(sb.ToString, conn)
        command.Parameters.AddWithValue("@StartDate", repParamInfo.StartDate)
        command.Parameters.AddWithValue("@EndDate", repParamInfo.EndDate)
        conn.Open()
        Dim isNew As Boolean = False
        Try
            Dim reader As SqlDataReader = command.ExecuteReader()
            If reader.HasRows Then
                Do While reader.Read()
                    Dim cls As Guid = reader.GetGuid(1)
                    att = resultList.FirstOrDefault(Function(x) x.ClsSectionMeetingId = cls)
                    If (att Is Nothing) Then
                        att = NoAttendanceCourses.Factory()
                        att.ClsSectionMeetingId = cls
                        isNew = True
                    End If
                    att.ReqId = reader.GetGuid(0)
                    att.CourseDescription = reader.GetString(2)
                    att.ClassSectionDescription = reader.GetString(3)
                    att.DateEnd = reader.GetDateTime(6)
                    att.DateStart = reader.GetDateTime(5)
                    att.InstructorId = reader.GetGuid(7)
                    att.CourseCode = reader.GetString(8)
                    att.WorkDaysList.Add(CType([Enum].Parse(GetType(DayOfWeekEnum), reader.GetString(4)), DayOfWeekEnum))
                    att.Email = reader.GetString(9)
                    att.Instructor = reader.GetString(10)
                    att.PeriodTimeBegin = reader.GetDateTime(11)
                    att.PeriodTimeEnd = reader.GetDateTime(12)
                    att.TermDescription = reader.GetString(13)
                    att.CampusGrpDescription = reader.GetString(14)
                    att.CampusDescription = reader.GetString(15)
                    If (isNew) Then
                        resultList.Add(att)
                        isNew = False
                    End If
                Loop
            Else
                Console.WriteLine("No rows found.")
            End If
            reader.Close()
        Finally
            conn.Close()
        End Try

        Return resultList
    End Function

    Public Function GetAttendancePerMeetingId(ByVal meetingGuid As Guid, ByVal iniDate As DateTime, ByVal lastDate As DateTime)
        Dim sb As StringBuilder = New StringBuilder()
        Dim resultList As List(Of DateTime) = New List(Of DateTime)()

        'Create the query
        With sb
            .Append("SELECT atClsSectAttendance.MeetDate ")
            .Append("FROM dbo.atClsSectAttendance ")
            .Append("WHERE  ")
            .Append("ClsSectMeetingId = @ClassSectionMeeting ")
            .Append("AND MeetDate BETWEEN @StartDate AND @EndDate ")
        End With
        ''Get connection String
        Dim myAdvAppSettings As PortalAdvAppSettings = GetAdvAppSettings()
        Dim connS As String = EliminateProvider(myAdvAppSettings.AppSettings("ConString"))
        Dim conn As SqlConnection = New SqlConnection(connS)

        'Create command
        Dim command As SqlCommand = New SqlCommand(sb.ToString, conn)
        command.Parameters.AddWithValue("@ClassSectionMeeting", meetingGuid)
        command.Parameters.AddWithValue("@StartDate", iniDate)
        command.Parameters.AddWithValue("@EndDate", lastDate)
        conn.Open()
        Try
            Dim reader As SqlDataReader = command.ExecuteReader()
            If reader.HasRows Then
                Do While reader.Read()
                    resultList.Add(reader.GetDateTime(0))
                Loop
            Else
                Debug.WriteLine("No rows found.")
            End If
            reader.Close()
        Finally
            conn.Close()
        End Try

        Return resultList
    End Function
#End Region

#Region "Private Help Functions"
    Private Function GetAdvAppSettings() As PortalAdvAppSettings
        Dim myAdvApp As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            myAdvApp = DirectCast(HttpContext.Current.Session("AdvAppSettings"), PortalAdvAppSettings)
        Else
            myAdvApp = New PortalAdvAppSettings
        End If
        Return myAdvApp
    End Function

    Private Function EliminateProvider(connectionString As String) As String

        If (connectionString.ToUpper().Contains("PROVIDER")) Then
            Dim co As String() = connectionString.Split(";")
            Return (From s In co Where Not s.ToUpper().Contains("PROVIDER")).Aggregate(String.Empty, Function(current, s) current + (s + ";"))
        End If

        Return connectionString
    End Function
#End Region

End Class
