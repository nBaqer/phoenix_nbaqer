
Imports FAME.AdvantageV1.Common.IPEDSCommon
Imports FAME.Advantage.Common

Public Class IPEDSDB
    ' General database-level functions used by IPEDS reports

    Public Shared Function GetSchoolRptTypes() As DataTable
        ' Create and return info on IPEDS School Reporting Types

        Dim dt As New DataTable
        Dim dr As DataRow

        With dt
            With .Columns
                .Add("IPEDSSchoolRptTypeId", GetType(Integer))
                .Add("IPEDSSchoolRptTypeDesc", GetType(String))
                .Add("IsDefault", GetType(Boolean))
            End With

            dr = .NewRow
            dr("IPEDSSchoolRptTypeId") = 1
            dr("IPEDSSchoolRptTypeDesc") = SchoolProgramReporter
            dr("IsDefault") = True
            .Rows.Add(dr)

            dr = .NewRow
            dr("IPEDSSchoolRptTypeId") = 2
            dr("IPEDSSchoolRptTypeDesc") = SchoolAcademicYearReporter
            dr("IsDefault") = False
            .Rows.Add(dr)

            Return .Copy
        End With

    End Function

    Public Shared Function GetIPEDSCohortTypes() As DataTable
        ' Create and return info on IPEDS Cohort Types

        Dim dt As New DataTable
        Dim dr As DataRow

        With dt
            With .Columns
                .Add("IPEDSCohortTypeId", GetType(Integer))
                .Add("IPEDSCohortTypeDesc", GetType(String))
                .Add("IsDefault", GetType(Boolean))
            End With

            dr = .NewRow
            dr("IPEDSCohortTypeId") = 1
            dr("IPEDSCohortTypeDesc") = CohortFullYear
            dr("IsDefault") = True
            .Rows.Add(dr)

            dr = .NewRow
            dr("IPEDSCohortTypeId") = 2
            dr("IPEDSCohortTypeDesc") = CohortFall
            dr("IsDefault") = False
            .Rows.Add(dr)

            Return .Copy
        End With

    End Function

    Public Shared Function GetIPEDSEthnicInfo() As DataTable
        ' Create and return info on IPEDS Ethnic Types - rows are added in specific order as 
        '	outlined in IPEDS specs

        Dim dt As New DataTable
        Dim dr As DataRow

        With dt
            .Columns.Add("EthCodeDescrip", GetType(String))

            dr = .NewRow
            dr("EthCodeDescrip") = "Nonresident alien"
            .Rows.Add(dr)

            dr = .NewRow
            dr("EthCodeDescrip") = "Black, non-Hispanic"
            .Rows.Add(dr)

            dr = .NewRow
            dr("EthCodeDescrip") = "American Indian/Alaska Native"
            .Rows.Add(dr)

            dr = .NewRow
            dr("EthCodeDescrip") = "Asian/Pacific Islander"
            .Rows.Add(dr)

            dr = .NewRow
            dr("EthCodeDescrip") = "Hispanic"
            .Rows.Add(dr)

            dr = .NewRow
            dr("EthCodeDescrip") = "White, non-Hispanic"
            .Rows.Add(dr)

            dr = .NewRow
            dr("EthCodeDescrip") = "Race/ethnicity unknown"
            .Rows.Add(dr)

            Return .Copy
        End With

    End Function

    Public Shared Function GetIPEDSGenderInfo() As DataTable
        ' Create and return info on IPEDS Genders - rows are added in specific order as 
        '	outlined in IPEDS specs

        Dim dt As New DataTable
        Dim dr As DataRow

        With dt
            .Columns.Add("GenderDescrip", GetType(String))

            dr = .NewRow
            dr("GenderDescrip") = "Men"
            .Rows.Add(dr)

            dr = .NewRow
            dr("GenderDescrip") = "Women"
            .Rows.Add(dr)

            Return .Copy
        End With

    End Function

    Public Shared Function GetIPEDSAgeCatInfo() As DataTable
        ' Create and return info on IPEDS Age Categories - rows are added in specific order as 
        '	outlined in IPEDS specs

        Dim dt As New DataTable
        Dim dr As DataRow

        With dt
            With .Columns
                .Add("IPEDSAgeCatId", GetType(Integer))
                .Add("AgeCatRangeLower", GetType(Integer))
                .Add("AgeCatRangeUpper", GetType(Integer))
                .Add("AgeCatDescrip", GetType(String))
            End With

            dr = .NewRow
            dr("IPEDSAgeCatId") = 1
            dr("AgeCatRangeLower") = DBNull.Value
            dr("AgeCatRangeUpper") = 17
            dr("AgeCatDescrip") = "Under 18"
            .Rows.Add(dr)

            dr = .NewRow
            dr("IPEDSAgeCatId") = 2
            dr("AgeCatRangeLower") = 18
            dr("AgeCatRangeUpper") = 19
            dr("AgeCatDescrip") = "18 - 19"
            .Rows.Add(dr)

            dr = .NewRow
            dr("IPEDSAgeCatId") = 3
            dr("AgeCatRangeLower") = 20
            dr("AgeCatRangeUpper") = 21
            dr("AgeCatDescrip") = "20 - 21"
            .Rows.Add(dr)

            dr = .NewRow
            dr("IPEDSAgeCatId") = 4
            dr("AgeCatRangeLower") = 22
            dr("AgeCatRangeUpper") = 24
            dr("AgeCatDescrip") = "22 - 24"
            .Rows.Add(dr)

            dr = .NewRow
            dr("IPEDSAgeCatId") = 5
            dr("AgeCatRangeLower") = 25
            dr("AgeCatRangeUpper") = 29
            dr("AgeCatDescrip") = "25 - 29"
            .Rows.Add(dr)

            dr = .NewRow
            dr("IPEDSAgeCatId") = 6
            dr("AgeCatRangeLower") = 30
            dr("AgeCatRangeUpper") = 34
            dr("AgeCatDescrip") = "30 - 34"
            .Rows.Add(dr)

            dr = .NewRow
            dr("IPEDSAgeCatId") = 7
            dr("AgeCatRangeLower") = 35
            dr("AgeCatRangeUpper") = 39
            dr("AgeCatDescrip") = "35 - 39"
            .Rows.Add(dr)

            dr = .NewRow
            dr("IPEDSAgeCatId") = 8
            dr("AgeCatRangeLower") = 40
            dr("AgeCatRangeUpper") = 49
            dr("AgeCatDescrip") = "40 - 49"
            .Rows.Add(dr)

            dr = .NewRow
            dr("IPEDSAgeCatId") = 9
            dr("AgeCatRangeLower") = 50
            dr("AgeCatRangeUpper") = 64
            dr("AgeCatDescrip") = "50 - 64"
            .Rows.Add(dr)

            dr = .NewRow
            dr("IPEDSAgeCatId") = 10
            dr("AgeCatRangeLower") = 65
            dr("AgeCatRangeUpper") = DBNull.Value
            dr("AgeCatDescrip") = "65 and over"
            .Rows.Add(dr)

            Return .Copy
        End With

    End Function

    Public Shared Function GetStudentList(ByVal RptParamInfo As ReportParamInfoIPEDS, Optional ByVal FilterProgramIDs As String = Nothing, Optional ByVal DateFilterParam As String = Nothing) As String
        ' Gets "preliminary" list of students to include in a report, based on the campus and program(s)
        '	filters as originally selected by the user.  Returns comma-delimited list of StudentIds.

        Dim dtStudents As New DataTable
        Dim drStudent As DataRow
        Dim dvEnrollments As DataView
        Dim sb As New System.Text.StringBuilder
        Dim objDA As DataAccess = DataAccessIPEDS()
        Dim ynInclude As Boolean
        Dim ProgramIdList As String

        Dim DateFilter As String
        Dim dvAuditHist As New DataView
        Dim i As Integer
        Dim StudentList As String
        Dim StuDateFilter As String

        ' extra filter conditions for Program Reporter schools
        ' no students with status of "No Start"
        ' no students with status of "Graduated", or if status is "Graduated", then the grad
        '   date should be later than the report start date
        'Dim StatusFilter As String = "SysStatusDescrip NOT LIKE 'No Start' AND " & _
        '    "((SysStatusDescrip NOT LIKE 'Graduated') OR " & _
        '    "(SysStatusDescrip LIKE 'Graduated' AND ExpGradDate >= '" & FmtRptDateParam(RptParamInfo.CohortStartDate) & "'))"
        Dim StatusFilter As String = "SysStatusDescrip NOT LIKE 'No Start' " 'Graduates should not be excluded. Modified by Anatoly 11/7/06

        ' set list of filter Program Ids for use in queries, to either:
        '	- Program Ids within passed-in report params, or 
        '	- explicitly passed-in list of Program Ids
        ProgramIdList = IIf(FilterProgramIDs Is Nothing, RptParamInfo.FilterProgramIDs, FilterProgramIDs)


        ' get list of students and relevant info based on passed in report param info
        With sb
            ' get needed columns
            .Append("SELECT DISTINCT ")
            .Append("arStudent.StudentId, ")
            .Append("(SELECT TOP 1 A.EducationInstType ")
            .Append("	FROM plStudentEducation A ")
            .Append("	WHERE A.StudentId = arStudent.StudentId ")
            .Append("	ORDER BY A.GraduatedDate DESC) AS LastEducation ")
            .Append("FROM ")
            .Append("arStudent, arStuEnrollments, arPrgVersions, arPrograms ")

            ' establish necessary relationships
            .Append("WHERE ")
            .Append("arStudent.StudentId = arStuEnrollments.StudentId AND ")
            .Append("arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId AND ")
            .Append("arPrgVersions.ProgId = arPrograms.ProgId AND ")

            ' append filter information for Campus and Programs 
            .Append("arStuEnrollments.CampusId = '" & RptParamInfo.FilterCampusID & "' AND ")
            .Append("arPrograms.ProgId IN (" & ProgramIdList & ") ")

            ' apply sort on StudentId
            .Append("ORDER BY arStudent.StudentId")
        End With
        ' run SQL to get DataTable with list of students
        dtStudents = objDA.RunSQLDataSet(sb.ToString).Tables(0).Copy

        ' if no students matching passed-in report param info, return empty list
        If dtStudents.Rows.Count = 0 Then
            Return ""
        End If

        ' build list of students to include for enrollments query, from data returned 
        '	from student list query
        With sb
            StudentList = ""
            .Remove(0, .Length)
            For Each drStudent In dtStudents.Rows
                .Append("'" & drStudent("StudentId").ToString & "',")
            Next
        End With
        StudentList = sb.ToString.TrimEnd(",".ToCharArray) ' get rid of extra "," at end

        ' get list of all enrollment info for same list of students 
        With sb
            .Remove(0, .Length)

            ' get needed columns
            .Append("SELECT ")
            .Append("arStuEnrollments.StudentId, arStuEnrollments.LDA, ")
            .Append("arStuEnrollments.DateDetermined, arStuEnrollments.EnrollDate, ")
            .Append("arStuEnrollments.ExpStartDate, arStuEnrollments.StartDate, ")
            .Append("arStuEnrollments.ExpGradDate, arPrgVersions.Weeks, ")
            .Append("arPrgVersions.IsContinuingEd, ")
            .Append("(SELECT COUNT(*) ")
            .Append("   FROM arTransferGrades A, arGradeSystemDetails B ")
            .Append("   WHERE A.StuEnrollId = arStuEnrollments.StuEnrollId ")
            .Append("   AND A.GrdSysDetailId=B.GrdSysDetailId ")
            .Append("   AND B.IsTransferGrade = 1	) AS NumTransfer, ")
            .Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
            .Append("	FROM adDegCertSeeking, ")
            .Append("		 syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
            .Append("	WHERE adDegCertSeeking.DegCertSeekingId = syRptAgencySchoolMapping.SchoolDescripId AND ")
            .Append("	      syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
            .Append("	      syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
            .Append("	      syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
            .Append("	      syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
            .Append("	      adDegCertSeeking.DegCertSeekingId = arStuEnrollments.DegCertSeekingId) As DegCertSeekingDescrip, ")
            .Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
            .Append("	FROM arAttendTypes, ")
            .Append("		 syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
            .Append("	WHERE arAttendTypes.AttendTypeId = syRptAgencySchoolMapping.SchoolDescripId AND ")
            .Append("	      syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
            .Append("	      syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
            .Append("	      syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
            .Append("	      syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
            .Append("	      arAttendTypes.AttendTypeId = arStuEnrollments.AttendTypeId) As AttendTypeDescrip, ")
            .Append("(SELECT sySysStatus.SysStatusDescrip ")
            .Append("	FROM syStatusCodes, sySysStatus ")
            .Append("	WHERE syStatusCodes.SysStatusId = sySysStatus.SysStatusId AND ")
            .Append("	      syStatusCodes.StatusCodeId = arStuEnrollments.StatusCodeId) As SysStatusDescrip ")
            .Append("FROM ")
            .Append("arStuEnrollments, arPrgVersions, arPrograms ")

            ' establish necessary relationships
            .Append("WHERE ")
            .Append("arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId AND ")
            .Append("arPrgVersions.ProgId = arPrograms.ProgId AND ")

            ' filter on list of selected programs
            .Append("arPrograms.ProgId IN (" & ProgramIdList & ") AND ")

            ' filter on list of students returned by student list query
            .Append("arStuEnrollments.StudentId IN (" & StudentList & ") ")

            ' apply sort on StudentId
            .Append("ORDER BY arStuEnrollments.StudentId")
        End With
        ' run SQL to get DataTable with list of student enrollments, create DataView
        dvEnrollments = New DataView(objDA.RunSQLDataSet(sb.ToString).Tables(0))

        ' set date filter to be used with enrollments DataView, according to either:
        '	- passed-in report parameters, or
        '	- explicitly passed-in date filter
        If DateFilterParam Is Nothing Then
            With RptParamInfo
                Dim EndDate As Date
                If .SchoolRptType IsNot Nothing Then
                    ' report uses School Reporting type, want enrollments begun on or prior to:
                    '	Cohort End Date (Program Reporter)
                    '	Report End Date (Academic Year Reporter)
                    Select Case .SchoolRptType
                        Case SchoolProgramReporter
                            EndDate = .CohortEndDate
                        Case SchoolAcademicYearReporter
                            EndDate = .RptEndDate
                    End Select
                ElseIf .CohortType IsNot Nothing Then
                    ' report uses Cohort type, want enrollments begun on or prior to:
                    '	Cohort End Date (Full Year Cohort)
                    '	Report End Date (Fall Cohort)
                    Select Case .CohortType
                        Case CohortFullYear
                            EndDate = .CohortEndDate
                        Case CohortFall
                            EndDate = .RptEndDate
                    End Select
                Else
                    ' report uses neither School Reporting type nor Cohort type, want enrollments begun
                    '	on or prior to Report End Date
                    EndDate = .RptEndDate
                End If
                DateFilter = "StartDate <= '" & FmtRptDateParam(EndDate) & "'"
            End With
        Else
            DateFilter = DateFilterParam
        End If

        ' now loop through records in Student list, including all appropriate records
        sb.Remove(0, sb.Length)

        For Each drStudent In dtStudents.Rows
            ' get enrollments for this student, according to date filter set above
            StuDateFilter = "StudentId = '" & drStudent("StudentId").ToString & "' AND " & DateFilter
            ' extra filter conditions for Program Reporter schools
            If RptParamInfo.SchoolRptType = SchoolProgramReporter Then
                StuDateFilter &= " AND " & StatusFilter
            End If
            dvEnrollments.RowFilter = StuDateFilter

            ' if no enrollments for this student/ending date, exclude student
            If dvEnrollments.Count = 0 Then
                Continue For
            End If

            ' see if student should be excluded from report based on standard IPEDS exclusions

            ' Not Degree/Certificate Seeking, and report doesn't include non-Degree/Certificate Seeking students
            If Not RptParamInfo.StuList_InclNonDegCertSeeking Then
                dvEnrollments.RowFilter = StuDateFilter & " AND DegCertSeekingDescrip NOT LIKE '" & DegCertSeekingNonDegree & "'"
                If dvEnrollments.Count = 0 Then
                    Continue For
                End If
            End If

            ' Part Time, and report doesn't include Part Time students
            If Not RptParamInfo.StuList_InclPartTime Then
                dvEnrollments.RowFilter = StuDateFilter & " AND AttendTypeDescrip NOT LIKE '" & AttendTypePartTime & "'"
                If dvEnrollments.Count = 0 Then
                    Continue For
                End If
            End If

            ' All of student's enrollments are for Continuing Education
            dvEnrollments.RowFilter = StuDateFilter & " AND IsContinuingEd = 0"
            If dvEnrollments.Count = 0 Then
                Continue For
            End If

            ' Student is a transfer in, and report doesn't include transfer in students
            If Not RptParamInfo.StuList_InclTransferIn Then
                dvEnrollments.RowFilter = StuDateFilter & " AND NumTransfer > 0"
                If dvEnrollments.Count > 0 Then
                    Continue For
                End If
            End If

            ' student shouldn't be excluded based on standard exclusions, check other criteria for inclusion

            ' start by assuming student won't be included
            ynInclude = False

            ' if student has at least one enrollment whose status is NOT 'Dropped', include student
            'dvEnrollments.RowFilter = StuDateFilter & " AND SysStatusDescrip NOT LIKE 'Dropped'"
            dvEnrollments.RowFilter = StuDateFilter 'include drop students ... modified by Anatoly 11/5/06.
            If dvEnrollments.Count > 0 Then
                ynInclude = True
            Else
                ' otherwise examine the student's dropped enrollments to decide if student should be included
                dvEnrollments.RowFilter = StuDateFilter

                Select Case RptParamInfo.SchoolRptType
                    Case SchoolProgramReporter
                        For i = 0 To dvEnrollments.Count - 1
                            If GetStuList_ChkDateDetLDA(dvEnrollments(i), RptParamInfo) OrElse _
                               GetStuList_ChkEnrollExpGrad(dvEnrollments(i), RptParamInfo) Then
                                ynInclude = True
                                Exit For
                            End If
                        Next

                    Case SchoolAcademicYearReporter
                        For i = 0 To dvEnrollments.Count - 1
                            If GetStuList_ChkDateDetLDA(dvEnrollments(i), RptParamInfo) Then
                                ynInclude = True
                                Exit For
                            End If
                        Next

                    Case Nothing ' report doesn't use School Reporting type, use Cohort Start and End Dates
                        '	as indicators of reporting period
                        For i = 0 To dvEnrollments.Count - 1
                            If GetStuList_ChkDateDetLDA(dvEnrollments(i), RptParamInfo) OrElse _
                               GetStuList_ChkEnrollExpGrad(dvEnrollments(i), RptParamInfo) Then
                                ynInclude = True
                                Exit For
                            End If
                        Next

                End Select
            End If

            If ynInclude Then
                ' if student should be included in report, append StudentId
                sb.Append("'" & drStudent("StudentId").ToString & "',")
            End If
        Next

        ' return list of StudentIds
        Return sb.ToString.TrimEnd(",".ToCharArray) ' get rid of extra "," at end

    End Function

    Private Shared Function GetStuList_ChkDateDetLDA(ByVal EnrollData As DataRowView, ByVal RptParamInfo As ReportParamInfoIPEDS) As Boolean
        ' Check a Student's Enrollment to determine if:
        '	- there is a Date Determined, and
        '	- there is a Last Date Attended, and
        '	- Date Determined is later than Report End Date, and
        '	- Last Date Attended is earlier than Report End Date

        Dim DateDet As New DateTime, DateLDA As New DateTime

        If IsDate(EnrollData("DateDetermined").ToString) And _
           IsDate(EnrollData("LDA").ToString) Then
            DateDet = EnrollData("DateDetermined")
            DateLDA = EnrollData("LDA")
            If Date.Compare(DateDet, RptParamInfo.RptEndDate) > 0 And Date.Compare(DateLDA, RptParamInfo.RptEndDate) < 0 Then
                Return True
            End If
        End If

        Return False
    End Function

    Private Shared Function GetStuList_ChkEnrollExpGrad(ByVal EnrollData As DataRowView, ByVal RptParamInfo As ReportParamInfoIPEDS) As Boolean
        ' Check a Student's Enrollment to determine if:
        '	- there is a valid Enroll Date, and
        '	- there is a valid Expected Grad Date, and
        '	- both the Enroll Date and Expected Grad Date are within the date range described by 
        '	  the Cohort Start and Cohort End Dates, and
        '	- the student attented for:
        '		at least 15 days for an enrollment scheduled for less than 52 weeks, or
        '		at least 30 days for an enrollment scheduled for 52 or more weeks

        Dim DateEnroll As New DateTime, DateExpGrad As New DateTime
        Dim NumDays As Integer, MinDays As Integer

        If IsDate(EnrollData("EnrollDate").ToString) And _
           IsDate(EnrollData("ExpGradDate").ToString) Then
            DateEnroll = EnrollData("EnrollDate")
            DateExpGrad = EnrollData("ExpGradDate")
            If Date.Compare(DateEnroll, RptParamInfo.CohortStartDate) >= 0 And Date.Compare(DateExpGrad, RptParamInfo.CohortEndDate) <= 0 Then
                NumDays = DateExpGrad.Subtract(DateEnroll).Days
                MinDays = IIf(EnrollData("Weeks") <= 52, 15, 30)
                If NumDays >= MinDays Then
                    Return True
                End If
            End If
        End If

        Return False
    End Function

    Public Shared Function GetAllProgramsByType(ByVal ProgType As String) As DataTable
        ' Get info on programs of types mapped to passed-in IPEDS Program Type

        Dim sb As New System.Text.StringBuilder

        With sb
            ' get needed columns
            .Append("SELECT DISTINCT ")
            .Append("arPrograms.ProgId, ")
            .Append("arPrograms.ProgCode, ")
            .Append("arPrograms.ProgDescrip ")
            .Append("FROM ")
            .Append("arPrograms, arPrgVersions, arProgTypes, syStatuses, ")
            .Append("syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")

            ' establish necessary relationships
            .Append("WHERE ")
            .Append("arPrograms.ProgId = arPrgVersions.ProgId AND ")
            .Append("arPrograms.StatusId = syStatuses.StatusId AND ")
            .Append("arPrgVersions.ProgTypId = arProgTypes.ProgTypId AND ")
            .Append("arProgTypes.ProgTypId = syRptAgencySchoolMapping.SchoolDescripId AND ")
            .Append("syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
            .Append("syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
            .Append("syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")

            ' filter to only get active Programs
            .Append("syStatuses.Status LIKE 'Active' AND ")

            ' filter to ensure only get programs mapped to IPEDS Program Types
            .Append("syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")

            ' filter on the passed-in Program Type
            .Append("syRptAgencyFldValues.AgencyDescrip LIKE '" & ProgType & "' ")

            ' sort by Program Type description
            .Append("ORDER BY ")
            .Append("arPrograms.ProgDescrip ")
        End With

        ' run query, return DataTable containing program info
        Return DataAccessIPEDS().RunSQLDataSet(sb.ToString).Tables(0).Copy

    End Function

    Public Shared Function GetAuditHistRecords(Optional ByVal TableNames As String = Nothing, _
                 Optional ByVal EventTypes As AuditHistEvents = Nothing, _
                 Optional ByVal EventDateFilter As String = Nothing, _
                 Optional ByVal RowIds As String = Nothing, _
                 Optional ByVal ColumnNames As String = Nothing, _
                 Optional ByVal OldValues As String = Nothing, _
                 Optional ByVal NewValues As String = Nothing) As DataTable
        ' Retrieve audit history records based on passed-in parameters

        Dim sb As New System.Text.StringBuilder

        With sb
            ' obtain needed columns
            .Append("SELECT ")
            .Append("syAuditHist.AuditHistId, syAuditHist.TableName, ")
            .Append("syAuditHist.Event, syAuditHist.EventRows, syAuditHist.EventDate, ")
            .Append("syAuditHistDetail.AuditHistDetailId, ")
            .Append("syAuditHistDetail.RowId, syAuditHistDetail.ColumnName, ")
            .Append("syAuditHistDetail.OldValue, syAuditHistDetail.NewValue ")
            .Append("FROM ")
            .Append("syAuditHist, syAuditHistDetail ")

            ' establish necessary relationships
            .Append("WHERE syAuditHist.AuditHistId = syAuditHistDetail.AuditHistId")

            ' apply filter on passed-in Table names, if any
            If Not (TableNames Is Nothing) Then
                .Append(" AND syAuditHist.TableName IN (" & PutDBQuotesList(TableNames) & ")")
            End If

            ' apply filter on passed-in Event Types, if any
            If Not IsNothing(EventTypes) AndAlso _
               EventTypes <> AuditHistEvents.Any Then
                .Append(" AND syAuditHist.Event ")
                Select Case EventTypes
                    Case AuditHistEvents.InsertOrUpdate
                        .Append("IN ('I','U')")
                    Case AuditHistEvents.InsertOrDelete
                        .Append("IN ('I','D')")
                    Case AuditHistEvents.UpdateOrDelete
                        .Append("IN ('U','D')")
                    Case AuditHistEvents.Insert
                        .Append("= 'I'")
                    Case AuditHistEvents.Update
                        .Append("= 'U'")
                    Case AuditHistEvents.Delete
                        .Append("= 'D'")
                End Select
            End If

            ' apply passed-in Event Date filter, if any
            If Not (EventDateFilter Is Nothing) Then
                .Append(" AND EventDate " & EventDateFilter)
            End If

            ' apply filter on passed-in specific RowIds, if any
            If Not (RowIds Is Nothing) Then
                .Append(" AND syAuditHistDetail.RowId IN (" & PutDBQuotesList(RowIds) & ")")
            End If

            ' apply filter on passed-in Column names, if any
            If Not (ColumnNames Is Nothing) Then
                .Append(" AND syAuditHistDetail.ColumnName IN (" & PutDBQuotesList(ColumnNames) & ")")
            End If

            ' apply filter on passed-in old value, if any
            If Not (OldValues Is Nothing) Then
                .Append(" AND syAuditHistDetail.OldValue IN (" & PutDBQuotesList(OldValues) & ")")
            End If

            ' apply filter on passed-in new value, if any
            If Not (NewValues Is Nothing) Then
                .Append(" AND syAuditHistDetail.NewValue IN (" & PutDBQuotesList(NewValues) & ")")
            End If

            ' sort records in order of Event Date, Event type, RowId, Column name
            .Append(" ORDER BY ")
            .Append("syAuditHist.EventDate DESC, ")
            .Append("syAuditHist.Event, ")
            .Append("syAuditHistDetail.RowId, ")
            .Append("syAuditHistDetail.ColumnName")
        End With

        ' return DataTable containing list of audit records
        Return DataAccessIPEDS().RunSQLDataSet(sb.ToString).Tables(0).Copy
    End Function

    Public Shared Function GetSQL_StudentIdentifier() As String
        ' Get standard SQL to retrieve Student Identifier - 
        '	either SSN, Student Number, or Enrollment ID, depending on system-wide setting  

        Dim SQL As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Select Case MyAdvAppSettings.AppSettings("StudentIdentifier")
            Case "SSN"
                SQL = "arStudent.SSN"

            Case "StudentId"
                SQL = "arStudent.StudentNumber"

            Case "EnrollmentId"
                SQL = "(SELECT TOP 1 A.EnrollmentId " & _
                   "		FROM arStuEnrollments A, arStudent B" & _
                   "		WHERE A.StudentId = B.StudentId AND " & _
                   "			  B.StudentId = arStudent.StudentId " & _
                   "		ORDER BY A.EnrollDate DESC)"

            Case Else ' this is an error condition, default to SSN
                SQL = "arStudent.SSN"

        End Select

        Return SQL & " AS " & RptStuIdentifierColName

    End Function

    Public Shared Function GetSQL_StudentSort(ByVal RptParamInfo As ReportParamInfoIPEDS) As String
        ' Get standard SQL to apply sort from report param info - either Student Identifier or Last Name

        Select Case RptParamInfo.SortByStu
            Case SortByStudentStudentId
                Return RptStuIdentifierColName
            Case SortByStudentLastName
                Return "arStudent.LastName, arStudent.FirstName, arStudent.MiddleName"
        End Select
    End Function

    Public Shared Function GetSQL_EnrollmentInfo(ByVal StudentList As String, ByVal ProgIdList As String) As String
        'Public Shared Function GetSQL_EnrollmentInfo(ByVal StudentList As String) As String
        ' Get standard SQL for list of enrollments and relevant info for passed-in list of students

        With New System.Text.StringBuilder
            .Append("SELECT arStuEnrollments.StuEnrollId, ")

            ' get StudentId and Enrollment Date
            .Append("arStuEnrollments.StudentId, arStuEnrollments.EnrollDate, arStuEnrollments.LeadId, ")

            ' get enrollment Program Type
            .Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
            .Append("	FROM arPrgVersions, arProgTypes, ")
            .Append("		 syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
            .Append("	WHERE arProgTypes.ProgTypId = syRptAgencySchoolMapping.SchoolDescripId AND ")
            .Append("	      syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
            .Append("	      syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
            .Append("	      syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
            .Append("	      syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
            .Append("	      arProgTypes.ProgTypId = arPrgVersions.ProgTypId AND ")
            .Append("	      arPrgVersions.PrgVerId = arStuEnrollments.PrgVerId) AS ProgTypeDescrip, ")

            ' get enrollment Attendance Type
            .Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
            .Append("	FROM arAttendTypes, ")
            .Append("		 syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
            .Append("	WHERE arAttendTypes.AttendTypeId = syRptAgencySchoolMapping.SchoolDescripId AND ")
            .Append("	      syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
            .Append("	      syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
            .Append("	      syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
            .Append("	      syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
            .Append("	      arAttendTypes.AttendTypeId = arStuEnrollments.AttendTypeId) AS AttendTypeDescrip, ")

            ' get enrollment Degree/Certificate Seeking Type
            .Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
            .Append("	FROM adDegCertSeeking, ")
            .Append("		 syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
            .Append("	WHERE adDegCertSeeking.DegCertSeekingId = syRptAgencySchoolMapping.SchoolDescripId AND ")
            .Append("	      syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
            .Append("	      syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
            .Append("	      syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
            .Append("	      syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
            .Append("	      adDegCertSeeking.DegCertSeekingId = arStuEnrollments.DegCertSeekingId) AS DegCertSeekingDescrip ")

            .Append("FROM arStuEnrollments, arPrgVersions, arPrograms ")

            ' establish necessary relationships
            .Append("WHERE ")
            .Append("arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId AND ")
            .Append("arPrgVersions.ProgId = arPrograms.ProgId AND ")

            '' append list of appropriate students to include
            '.Append("arStuEnrollments.StudentId IN (" & PutDBQuotesList(StudentList) & ") ")

            ' append list of appropriate students to include
            .Append("arStuEnrollments.StudentId IN (" & PutDBQuotesList(StudentList) & ") AND ")

            ' filter on passed-in list of programs as selected by user
            .Append("arPrograms.ProgId IN (" & ProgIdList & ") ")

            ' sort by StudentId (for faster processing by Business Facade objects)
            .Append("ORDER BY arStuEnrollments.StudentId")

            Return .ToString
        End With
    End Function

    Public Function GetProgramMisingCIPCodebyProgAndCampus(ByVal CampusId As String,
                                                            Optional ByVal ProgId As String = Nothing,
                                                            Optional ByVal bCheckCIP As Boolean = True) As DataSet

        Dim db As New SQLDataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            If ProgId.Length > 0 Then
                db.AddParameter("@ProgId", ProgId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            End If
            db.AddParameter("@CampusId", CampusId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            If bCheckCIP Then
                ds = db.RunParamSQLDataSet_SP("dbo.USP_IPEDS_ProgramsMissingCIPCode")
            Else
                ds = db.RunParamSQLDataSet_SP("dbo.USP_IPEDS_ProgramsMissingCredentialLvl")
            End If
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function

    Public Shared Function GetIPEDSFldValues(ByVal FldName As String) As DataTable
        ' Get the values for a specified IPEDS Field

        Return ReportCommonUtilsDB.GetRptAgencyFldValues(AgencyName, FldName)
    End Function

    Public Shared Function DataAccessIPEDS() As DataAccess
        Dim objDA As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With objDA
            .ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            .OpenConnection()
            .CommandTimeout = CInt(MyAdvAppSettings.AppSettings("CommandTimeoutIPEDS"))
            .CloseConnection()
        End With

        Return objDA
    End Function

End Class