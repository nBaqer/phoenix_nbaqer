﻿Imports System
Imports System.Data
Imports FAME.Advantage.Common
Imports System.Data.SqlClient
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer


Public Class LeadTransactionsDB

    Public Function PostLeadTransactions(ByVal PostLeadPaymentInfo As PostLeadPaymentInfo, ByVal user As String) As String

        Dim result As Integer
        Dim Transaction As SqlTransaction
        Dim strConn As SqlConnection
        Dim strCmd As SqlCommand
        Dim strOutputParam As SqlParameter

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Dim ConnectionString As String = MyAdvAppSettings.AppSettings("ConnectionString")

        Try

            strConn = New SqlConnection(ConnectionString)
            strConn.Open()

            strCmd = New SqlCommand("USP_AD_PostLeadTransactions", strConn)
            strCmd.CommandTimeout = 0
            strCmd.CommandType = CommandType.StoredProcedure

            strOutputParam = strCmd.Parameters.Add("@LeadTransactionId", SqlDbType.UniqueIdentifier)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam.Value = New Guid(PostLeadPaymentInfo.LeadTransactionId.ToString)

            strOutputParam = strCmd.Parameters.Add("@LeadId", SqlDbType.UniqueIdentifier)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam.Value = New Guid(PostLeadPaymentInfo.LeadId.ToString)

            strOutputParam = strCmd.Parameters.Add("@TransCodeId", SqlDbType.UniqueIdentifier)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam.Value = New Guid(PostLeadPaymentInfo.TransCodeId.ToString)

            strOutputParam = strCmd.Parameters.Add("@TransReference", SqlDbType.VarChar)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam.Value = PostLeadPaymentInfo.Reference

            strOutputParam = strCmd.Parameters.Add("@TransDescrip", SqlDbType.VarChar)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam.Value = PostLeadPaymentInfo.Descrip

            strOutputParam = strCmd.Parameters.Add("@TransAmount", SqlDbType.Decimal)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam.Size = 19
            strOutputParam.Value = PostLeadPaymentInfo.Amount

            strOutputParam = strCmd.Parameters.Add("@TransTypeId", SqlDbType.Int)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam.Value = PostLeadPaymentInfo.TransTypeId

            strOutputParam = strCmd.Parameters.Add("@IsEnrolled", SqlDbType.Bit)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam.Value = PostLeadPaymentInfo.IsEnrolled

            strOutputParam = strCmd.Parameters.Add("@TransDate", SqlDbType.DateTime)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam.Value = PostLeadPaymentInfo.LeadTransactionDate

            strOutputParam = strCmd.Parameters.Add("@IsVoided", SqlDbType.Bit)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam.Value = PostLeadPaymentInfo.IsVoided

            strOutputParam = strCmd.Parameters.Add("@CampusId", SqlDbType.UniqueIdentifier)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam.Value = New Guid(PostLeadPaymentInfo.CampusId.ToString)

            strOutputParam = strCmd.Parameters.Add("@PaymentTransactionId", SqlDbType.UniqueIdentifier)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam.Value = New Guid(PostLeadPaymentInfo.PaymentTransactionId.ToString)

            strOutputParam = strCmd.Parameters.Add("@PaymentTypeId", SqlDbType.Int)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam.Value = PostLeadPaymentInfo.PaymentTypeId

            strOutputParam = strCmd.Parameters.Add("@CheckNumber", SqlDbType.VarChar)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam.Value = PostLeadPaymentInfo.CheckNumber

            strOutputParam = strCmd.Parameters.Add("@PaymentReference", SqlDbType.Int)
            strOutputParam.Direction = ParameterDirection.Output
            strOutputParam.Value = PostLeadPaymentInfo.PaymentReference

            '   create timestamp
            Dim rightNow As DateTime = Date.Now

            '   CreateDate
            strOutputParam = strCmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam.Value = rightNow

            '   ModUser
            strOutputParam = strCmd.Parameters.Add("@ModUser", SqlDbType.VarChar)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam.Value = user


            '   ModDate
            strOutputParam = strCmd.Parameters.Add("@ModDate", SqlDbType.DateTime)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam.Value = rightNow


            strCmd.ExecuteNonQuery()
            result = strCmd.Parameters("@PaymentReference").Value


        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
            Return "Error"
        Finally
            strConn.Close()
        End Try

        Return result

    End Function

    Public Function GetApplicantChargeDescriptions(ByVal showActiveOnly As String, ByVal campusid As Guid) As DataSet

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@ShowActive", showActiveOnly, SqlDbType.NVarChar, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusid, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet_SP("USP_AD_GetApplicantChargeDescriptions")
    End Function

End Class

