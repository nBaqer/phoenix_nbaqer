﻿Imports FAME.Advantage.Common

Public Class StudentPortalDB

#Region "Get AdvAppsetting object"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region

#Region "GetPortalSettingsByCampus"

    Public Function GetPortalSettingsByCampus(ByVal CampusId As String) As Boolean

        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        With sb
            .Append(" IF EXISTS (SELECT TOP 1 * FROM syConfigAppSetValues WHERE SettingId=(SELECT SettingId FROM syConfigAppSettings WHERE KeyName='PortalCustomer') AND CampusId= '" + CampusId + "')")
            .Append(" SELECT  (SELECT TOP 1 Value FROM syConfigAppSetValues  WHERE SettingId=(SELECT SettingId FROM syConfigAppSettings WHERE KeyName='PortalCustomer') AND CampusId= '" + CampusId + "') AS Value ")
            .Append(" ELSE (SELECT TOP 1 Value FROM syConfigAppSetValues  WHERE SettingId=(SELECT SettingId FROM syConfigAppSettings WHERE KeyName='PortalCustomer') AND CampusId IS NULL) ")
        End With

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        Try
            db.OpenConnection()

            Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

            If ds.Tables(0).Rows.Count > 0 Then
                If (ds.Tables(0).Rows(0)("Value").ToString() = "Yes") Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Catch ex As System.Exception
            Throw New Exception(ex.InnerException.ToString)
        Finally
            db.CloseConnection()
        End Try
    End Function

#End Region


#Region "UpdatePortalInfoByCampus"

    Public Sub UpdatePortalInfoByCampus(ByVal CampusId As String, _
                                        ByVal chkSourceCategoryAll As Boolean, ByVal SourceCategoryId As String, _
                                        ByVal chkSourceTypeAll As Boolean, ByVal SourceTypeId As String, _
                                        ByVal chkAdmRepAll As Boolean, ByVal AdmRepId As String, _
                                        ByVal chkLeadStatusAll As Boolean, ByVal LeadStatusId As String, _
                                        ByVal chkPhoneType1All As Boolean, ByVal PhoneType1Id As String, _
                                        ByVal chkPhoneType2All As Boolean, ByVal PhoneType2Id As String, _
                                        ByVal chkAddTypeAll As Boolean, ByVal AddTypeId As String, _
                                        ByVal chkGenderAll As Boolean, ByVal GenderId As String, _
                                        ByVal chkPortalCountryAll As Boolean, ByVal PortalCountryId As String, _
                                        ByVal chkPortalContactEmailAll As Boolean, ByVal PortalContactEmail As String, _
                                        ByVal chkEmailSubjectAll As Boolean, ByVal EmailSubject As String, _
                                        ByVal chkEmailBodyAll As Boolean, ByVal EmailBody As String)

        Dim db As New SQLDataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        Try
            db.OpenConnection()

            db.AddParameter("@CampusId", New Guid(CampusId), SqlDbType.UniqueIdentifier, 50, ParameterDirection.Input)

            db.AddParameter("@chkSourceCategoryAll", chkSourceCategoryAll, SqlDbType.Bit, 1, ParameterDirection.Input)
            If String.IsNullOrEmpty(SourceCategoryId) Then
                db.AddParameter("@SourceCategoryId", System.DBNull.Value, SqlDbType.VarChar, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@SourceCategoryId", SourceCategoryId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            End If

            db.AddParameter("@chkSourceTypeAll", chkSourceTypeAll, SqlDbType.Bit, 1, ParameterDirection.Input)
            If String.IsNullOrEmpty(SourceTypeId) Then
                db.AddParameter("@SourceTypeId", System.DBNull.Value, SqlDbType.VarChar, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@SourceTypeId", SourceTypeId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            End If

            db.AddParameter("@chkAdmRepAll", chkAdmRepAll, SqlDbType.Bit, 1, ParameterDirection.Input)
            If String.IsNullOrEmpty(AdmRepId) Then
                db.AddParameter("@AdmRepId", System.DBNull.Value, SqlDbType.VarChar, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@AdmRepId", AdmRepId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            End If

            db.AddParameter("@chkLeadStatusAll", chkLeadStatusAll, SqlDbType.Bit, 1, ParameterDirection.Input)
            If String.IsNullOrEmpty(LeadStatusId) Then
                db.AddParameter("@LeadStatusId", System.DBNull.Value, SqlDbType.VarChar, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@LeadStatusId", LeadStatusId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            End If

            db.AddParameter("@chkPhoneType1All", chkPhoneType1All, SqlDbType.Bit, 1, ParameterDirection.Input)
            If String.IsNullOrEmpty(PhoneType1Id) Then
                db.AddParameter("@PhoneType1Id", System.DBNull.Value, SqlDbType.VarChar, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PhoneType1Id", PhoneType1Id, SqlDbType.VarChar, 50, ParameterDirection.Input)
            End If

            db.AddParameter("@chkPhoneType2All", chkPhoneType2All, SqlDbType.Bit, 1, ParameterDirection.Input)
            If String.IsNullOrEmpty(PhoneType2Id) Then
                db.AddParameter("@PhoneType2Id", System.DBNull.Value, SqlDbType.VarChar, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PhoneType2Id", PhoneType2Id, SqlDbType.VarChar, 50, ParameterDirection.Input)
            End If

            db.AddParameter("@chkAddTypeAll", chkAddTypeAll, SqlDbType.Bit, 1, ParameterDirection.Input)
            If String.IsNullOrEmpty(AddTypeId) Then
                db.AddParameter("@AddTypeId", System.DBNull.Value, SqlDbType.VarChar, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@AddTypeId", AddTypeId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            End If
            db.AddParameter("@chkGenderAll", chkGenderAll, SqlDbType.Bit, 1, ParameterDirection.Input)
            If String.IsNullOrEmpty(GenderId) Then
                db.AddParameter("@GenderId", System.DBNull.Value, SqlDbType.VarChar, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@GenderId", GenderId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            End If

            db.AddParameter("@chkPortalCountryAll", chkPortalCountryAll, SqlDbType.Bit, 1, ParameterDirection.Input)
            If String.IsNullOrEmpty(PortalCountryId) Then
                db.AddParameter("@PortalCountryId", System.DBNull.Value, SqlDbType.VarChar, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PortalCountryId", PortalCountryId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            End If

            db.AddParameter("@chkPortalContactEmailAll", chkPortalContactEmailAll, SqlDbType.Bit, 1, ParameterDirection.Input)
            db.AddParameter("@PortalContactEmail", PortalContactEmail, SqlDbType.VarChar, 50, ParameterDirection.Input)

            db.AddParameter("@chkEmailSubjectAll", chkEmailSubjectAll, SqlDbType.Bit, 1, ParameterDirection.Input)
            db.AddParameter("@EmailSubject", EmailSubject, SqlDbType.VarChar, 250, ParameterDirection.Input)

            db.AddParameter("@chkEmailBodyAll", chkEmailBodyAll, SqlDbType.Bit, 1, ParameterDirection.Input)
            Dim emailBodyLen As Integer = Len(EmailBody)
            db.AddParameter("@EmailBody", EmailBody, SqlDbType.Text, emailBodyLen, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery_SP("usp_AdvPortalSettings")

        Catch ex As System.Exception
            Throw New Exception(ex.InnerException.ToString)
        Finally
            db.CloseConnection()
        End Try

    End Sub

#End Region

#Region "GetPortalValuesByCampus"

    Public Function GetPortalValuesByCampus(ByVal CampusId As String) As DataSet

        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        With sb
            .Append(" SELECT TOP 1 ISNULL(Cmp.SourceCategoryAll,0) AS SourceCategoryAll,Cmp.SourceCategoryId,srcCategory.SourceCatagoryDescrip," + vbCrLf)
            .Append(" ISNULL(Cmp.SourceTypeAll,0) AS SourceTypeAll,Cmp.SourceTypeId,srcType.SourceTypeDescrip, " + vbCrLf)
            .Append(" ISNULL(Cmp.AdmRepAll,0) AS AdmRepAll,Cmp.AdmRepId,Usrs.FullName, " + vbCrLf)
            .Append(" ISNULL(Cmp.LeadStatusAll,0) AS LeadStatusAll,Cmp.LeadStatusId,SC.StatusCodeDescrip AS LeadStatus, " + vbCrLf)
            .Append(" ISNULL(Cmp.PhoneType1All,0) AS PhoneType1All,Cmp.PhoneType1Id,Phone1.PhoneTypeDescrip AS PhoneType1Descrip, " + vbCrLf)
            .Append(" ISNULL(Cmp.PhoneType2All,0) AS PhoneType2All,Cmp.PhoneType2Id,Phone2.PhoneTypeDescrip AS PhoneType2Descrip, " + vbCrLf)
            .Append(" ISNULL(Cmp.AddTypeAll,0) AS AddTypeAll,Cmp.AddTypeId,AddType.AddressDescrip, " + vbCrLf)
            .Append(" ISNULL(Cmp.GenderAll,0) AS GenderAll,Cmp.GenderId,Gender.GenderDescrip, " + vbCrLf)
            .Append(" ISNULL(Cmp.PortalCountryAll,0) AS PortalCountryAll,Cmp.PortalCountryId,Country.CountryDescrip, " + vbCrLf)
            .Append(" ISNULL(Cmp.PortalContactEmailAll,0) AS PortalContactEmailAll, Cmp.PortalContactEmail, " + vbCrLf)
            .Append(" ISNULL(Cmp.EmailSubjectAll,0) AS EmailSubjectAll,Cmp.EmailSubject, " + vbCrLf)
            .Append(" ISNULL(Cmp.EmailBodyAll,0) AS EmailBodyAll,Cmp.EmailBody " + vbCrLf)
            .Append(" FROM syCampuses Cmp " + vbCrLf)
            .Append(" INNER JOIN syCmpGrpCmps CmpGrp ON CmpGrp.CampusId = Cmp.CampusId " + vbCrLf)
            .Append(" LEFT JOIN adSourceCatagory srcCategory ON srcCategory.SourceCatagoryId=Cmp.SourceCategoryId " + vbCrLf)
            .Append(" LEFT JOIN adSourceType srcType ON srcType.SourceTypeId=Cmp.SourceTypeId " + vbCrLf)
            .Append(" LEFT JOIN syUsers Usrs ON Usrs.UserId = Cmp.AdmRepId " + vbCrLf)
            .Append(" LEFT JOIN syStatusCodes SC ON SC.StatusCodeId=Cmp.LeadStatusId " + vbCrLf)
            .Append(" LEFT JOIN syPhoneType Phone1 ON Phone1.PhoneTypeId=Cmp.PhoneType1Id " + vbCrLf)
            .Append(" LEFT JOIN syPhoneType Phone2 ON Phone2.PhoneTypeId=Cmp.PhoneType2Id " + vbCrLf)
            .Append(" LEFT JOIN plAddressTypes AddType ON AddType.AddressTypeId=Cmp.AddTypeId " + vbCrLf)
            .Append(" LEFT JOIN adGenders Gender ON Gender.GenderId=Cmp.GenderId " + vbCrLf)
            .Append(" LEFT JOIN adCountries Country ON Country.CountryId=Cmp.PortalCountryId " + vbCrLf)
            .Append(" WHERE Cmp.CampusId='" + CampusId + "' ")
        End With

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        Try
            db.OpenConnection()

            Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

            Return ds

        Catch ex As System.Exception
            Throw New Exception(ex.InnerException.ToString)
        Finally
            db.CloseConnection()
        End Try

    End Function

#End Region

End Class
