Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.AdvantageV1.Common
Imports FAME.Advantage.Common

Public Class LOADB
    Public Function GetLOAStatuses() As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        With sb
            .Append("SELECT distinct a.StatusCodeId, a.StatusCode, a.StatusCodeDescrip ")
            .Append("FROM syStatusCodes a, sySysStatus b ")
            .Append("WHERE a.SysStatusId=10")
        End With


        db.OpenConnection()
        ds = db.RunSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Public Function GetStudentStatusId(ByVal StuEnrollId As String) As Guid
        Dim StatusChangeId As Guid
        '   connect to the database
        Dim db As New DataAccess
        Try
            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            '   build the sql query
            With sb
                .Append("SELECT StatusCodeId FROM dbo.arStuEnrollments WHERE StuEnrollId = ? ")
            End With
            db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            db.OpenConnection()
            StatusChangeId = CType(db.RunParamSQLScalar(sb.ToString), Guid)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            db.CloseConnection()
        Catch ex As Exception
            db.CloseConnection()
            Throw New BaseException(ex.Message)
        End Try

        Return StatusChangeId

    End Function

    Public Function GetStudentStatus(ByVal StuEnrollId As String) As StateEnum
        Dim Status As Integer
        Dim EnumType As StateEnum

        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder


            '   build the sql query
            With sb
                .Append("SELECT b.SysStatusId FROM syStatusCodes b,arStuEnrollments a ")
                .Append(" WHERE a.StatusCodeId = b.StatusCodeId and  ")
                .Append(" a.StuEnrollId = ? ")
            End With


            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)



            '   Execute the query
            'db.OpenConnection()

            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                Status = dr("SysStatusId")
            End While

            EnumType = CType(Status, StateEnum)

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return EnumType

    End Function

    'Public Function PlaceStudentOnLOA(ByVal StuEnrollId As String, ByVal LOAReasonId As String, ByVal StartDate As String, ByVal EndDate As String, ByVal Status As String, ByVal user As String)
    '    Dim db As New DataAccess
    '    Dim sb As New System.Text.StringBuilder
    '    Dim ds As DataSet


    '    'Set the connection string
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")


    '    '   we must encapsulate all DB updates in one transaction
    '    Dim groupTrans As OleDbTransaction = db.StartTransaction()

    '    Try

    '        With sb
    '            .Append("INSERT INTO arStudentLOAs(StuEnrollId,StartDate,EndDate,LOAReasonId,ModUser,ModDate) ")
    '            .Append("VALUES(?,?,?,?,?,?)")
    '        End With
    '        db.AddParameter("@courseid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@code", StartDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@descrip", EndDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        db.AddParameter("@hours", LOAReasonId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


    '        'ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'ModDate
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)

    '        With sb
    '            .Append("UPDATE arStuEnrollments Set StatusCodeId = ? ")
    '            .Append("WHERE StuEnrollId = ?")
    '        End With
    '        db.AddParameter("@grade", Status, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


    '        db.AddParameter("@stdenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


    '        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)



    '        'With sb
    '        '    .Append("UPDATE arStudent Set StudentStatus = ?  ")
    '        '    .Append("WHERE StudentId in (Select StudentId from arStuEnrollments where StuEnrollId = ?) ")
    '        'End With

    '        'db.AddParameter("@inactive", AdvantageCommonValues.InactiveGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        'db.AddParameter("@stdenrollid2", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
    '        '   commit transaction 
    '        groupTrans.Commit()

    '        '   return without errors
    '        Return ""
    '        'Catch ex As System.Exception
    '        '    'Return an Error To Client
    '        '    Return -1
    '    Catch ex As OleDbException
    '        groupTrans.Rollback()
    '        '   do not report sql lost connection
    '        If Not groupTrans.Connection Is Nothing Then
    '            ' Report the error
    '        End If
    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)

    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    'End Function
    '' Code modified by kamalesh Ahuja on 25th and 26 August to resolve mantis issue id 19485

    Public Function AreStudentLOADatesOverlapping(ByVal StuEnrollId As String, ByVal StartDate As String, ByVal EndDate As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dr As OleDbDataReader

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


        '   we must encapsulate all DB updates in one transaction
        '' Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            With sb
                .Append(" SELECT StudentLOAId,StartDate, ISNULL(LOAReturnDate,enddate) AS EndDate FROM dbo.arStudentLOAs  ")
                .Append("WHERE StuEnrollId = ?")
            End With
            ''New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19485
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            dr = db.RunParamSQLDataReader(sb.ToString)
        Catch ex As Exception

        End Try
        If dr.HasRows Then
            While dr.Read()
                If StartDate >= dr("StartDate") And StartDate <= dr("EndDate") Then
                    Return "Overlapping LOA Dates.(StartDate is between the LOA dates " & dr("StartDate") & " and " & dr("EndDate") & " ) "
                ElseIf EndDate >= dr("StartDate") And EndDate <= dr("EndDate") Then
                    Return "Overlapping LOA Dates .(EndDate is between the LOA dates " & dr("StartDate") & " and " & dr("EndDate") & " ) "
                ElseIf StartDate >= dr("StartDate") And EndDate <= dr("EndDate") Then
                    Return "Overlapping LOA Dates .(StartDate and EndDate is between the LOA dates " & dr("StartDate") & " and " & dr("EndDate") & " ) "
                ElseIf StartDate <= dr("StartDate") And EndDate >= dr("EndDate") Then
                    Return "Overlapping LOA Dates .(StartDate and EndDate is overlapping with the LOA dates " & dr("StartDate") & " and " & dr("EndDate") & " ) "
                End If
            End While
        End If

        If Not dr.IsClosed Then dr.Close()

        Return ""
    End Function


    '' US2135 01/05/2012 Janet Robinson added RequestDate
    'Public Function PlaceStudentOnLOA(ByVal StuEnrollId As String, ByVal LOAReasonId As String, ByVal StartDate As String, ByVal EndDate As String, ByVal Status As String, ByVal user As String, ByVal RequestDate As String)
    '    Dim db As New DataAccess
    '    Dim sb As New System.Text.StringBuilder
    '    Dim ds As DataSet
    '    Dim StatusChangeId As String

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If


    '    'Set the connection string
    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


    '    '   we must encapsulate all DB updates in one transaction
    '    '' Dim groupTrans As OleDbTransaction = db.StartTransaction()

    '    Try
    '        If CDate(StartDate) <= Date.Now.ToShortDateString Then
    '            With sb
    '                .Append("UPDATE arStuEnrollments Set StatusCodeId = ?,ModDate=? ")
    '                ''New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19485
    '                .Append(",ModUser=? ")
    '                ''New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19485
    '                .Append("WHERE StuEnrollId = ?")
    '            End With
    '            db.AddParameter("@grade", Status, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            ''New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19485
    '            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            ''New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19485
    '            db.AddParameter("@stdenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


    '            db.RunParamSQLExecuteNoneQuery(sb.ToString)

    '            db.ClearParameters()
    '            sb.Remove(0, sb.Length)



    '            With sb
    '                .Append("select top 1 StudentStatusChangeId from syStudentStatusChanges where StuEnrollId=? and NewStatusId=?  order by ModDate desc ")

    '            End With
    '            db.AddParameter("@stdenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.AddParameter("@grade", Status, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '            StatusChangeId = CType(db.RunParamSQLScalar(sb.ToString), Guid).ToString

    '            db.ClearParameters()
    '            sb.Remove(0, sb.Length)
    '        End If



    '        ' US2135 01/05/2012 Janet Robinson added RequestDate
    '        With sb
    '            .Append("INSERT INTO arStudentLOAs(StuEnrollId,StartDate,EndDate,LOAReasonId,ModUser,ModDate,StudentStatusChangeId,StatusCodeId,LOARequestDate) ")
    '            .Append("VALUES(?,?,?,?,?,?,?,?,?)")
    '        End With
    '        db.AddParameter("@courseid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@code", StartDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@descrip", EndDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        db.AddParameter("@hours", LOAReasonId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


    '        'ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'ModDate
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        db.AddParameter("@StatusChangeId", StatusChangeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@StatusCodeId", Status, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@LOARequestDate", RequestDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        db.RunParamSQLExecuteNoneQuery(sb.ToString)

    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)

    '        'With sb
    '        '    .Append("UPDATE arStudent Set StudentStatus = ?  ")
    '        '    .Append("WHERE StudentId in (Select StudentId from arStuEnrollments where StuEnrollId = ?) ")
    '        'End With

    '        'db.AddParameter("@inactive", AdvantageCommonValues.InactiveGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        'db.AddParameter("@stdenrollid2", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
    '        '   commit transaction 
    '        '' groupTrans.Commit()

    '        '   return without errors
    '        Return ""
    '        'Catch ex As System.Exception
    '        '    'Return an Error To Client
    '        '    Return -1
    '    Catch ex As OleDbException
    '        '' groupTrans.Rollback()
    '        '   do not report sql lost connection
    '        'If Not groupTrans.Connection Is Nothing Then
    '        '    ' Report the error
    '        'End If
    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)

    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    'End Function
    ' ''
    'Public Function UpdateLOAEndDate(ByVal EndDate As String, ByVal StudentLOAId As String, ByVal user As String) As String
    '    Dim db As New DataAccess
    '    Dim sb As New System.Text.StringBuilder
    '    Dim ds As DataSet

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If


    '    'Set the connection string
    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '    Try
    '        With sb
    '            .Append("UPDATE arStudentLOAs Set EndDate = ?, ")
    '            .Append("ModUser=?, ")
    '            .Append("ModDate=? ")
    '            .Append("WHERE StudentLOAId = ?  ")

    '            db.AddParameter("@edate", EndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '            ''ModUser
    '            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '            ''ModDate
    '            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            db.AddParameter("@clssectid", StudentLOAId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        End With

    '        db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)

    '        '   return without errors
    '        Return ""

    '    Catch ex As OleDbException
    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)

    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try

    'End Function

    Public Function UpdateLOADate(ByVal startDate As String, ByVal endDate As String, ByVal studentLOAId As String, ByVal user As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        'Set the connection string
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Try
            With sb
                .Append("UPDATE arStudentLOAs Set StartDate = ? , EndDate = ?, ")
                .Append("ModUser=?, ")
                .Append("ModDate=? ")
                .Append("WHERE StudentLOAId = ?  ")

                db.AddParameter("@sdate", startDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@edate", endDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                ''ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                ''ModDate
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@clssectid", studentLOAId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            End With

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            '   return without errors
            Return String.Empty

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetStudentStartDate(ByVal stuEnrollId As String) As String
        Dim sDate As String

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        Try
            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            db.OpenConnection()

            '   build the sql query
            With sb
                .Append("SELECT DISTINCT ")
                .Append("       A.StartDate ")
                .Append("FROM   arStuEnrollments A ")
                .Append("WHERE  A.StuEnrollId = ? ")
            End With

            db.AddParameter("@clssectid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                sDate = dr("StartDate").ToString
            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return status code Id
        Return sDate

    End Function
    Public Function GetStudentTutionEarningMethod_Sp(ByVal stuEnrollId As String) As Integer
        Dim db As New SQLDataAccess
        Dim RowCount As Integer
        db.OpenConnection()
        db.AddParameter("@stuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        RowCount = db.RunParamSQLScalar_SP("usp_GetStudentTutionEarningMethod")
        db.CloseConnection()
        Return RowCount
    End Function

    Public Function AreStudentLOADatesOverlapping_GivenLOAID(ByVal StuEnrollId As String, ByVal StartDate As String, ByVal EndDate As String, ByVal LOAID As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dr As OleDbDataReader

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If



        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


        '   we must encapsulate all DB updates in one transaction
        '' Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            With sb
                .Append(" SELECT StudentLOAId,StartDate, ISNULL(LOAReturnDate,enddate) AS EndDate FROM dbo.arStudentLOAs  ")
                .Append("WHERE StuEnrollId = ? and StudentLOAId<>? ")
            End With
            ''New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19485
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuLOAId", LOAID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            dr = db.RunParamSQLDataReader(sb.ToString)
        Catch ex As Exception

        End Try
        If dr.HasRows Then
            While dr.Read()
                If StartDate >= dr("StartDate") And StartDate <= dr("EndDate") Then
                    Return "Overlapping LOA Dates.(StartDate is between the LOA dates " & dr("StartDate") & " and " & dr("EndDate") & " ) "
                ElseIf EndDate >= dr("StartDate") And EndDate <= dr("EndDate") Then
                    Return "Overlapping LOA Dates .(EndDate is between the LOA dates " & dr("StartDate") & " and " & dr("EndDate") & " ) "
                ElseIf StartDate >= dr("StartDate") And EndDate <= dr("EndDate") Then
                    Return "Overlapping LOA Dates .(StartDate and EndDate is between the LOA dates " & dr("StartDate") & " and " & dr("EndDate") & " ) "
                ElseIf StartDate <= dr("StartDate") And EndDate >= dr("EndDate") Then
                    Return "Overlapping LOA Dates .(StartDate and EndDate is overlapping with the LOA dates " & dr("StartDate") & " and " & dr("EndDate") & " ) "
                End If
            End While
        End If

        If Not dr.IsClosed Then dr.Close()

        Return ""
    End Function
    Public Function StudentLOACount(ByVal stuEnrollId As String) As Integer
        Dim db As New SQLDataAccess
        Dim RowCount As Integer
        db.OpenConnection()
        db.AddParameter("@stuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        RowCount = db.RunParamSQLScalar_SP("dbo.usp_StudentLOACount")
        db.CloseConnection()
        Return RowCount
    End Function
    Public Function CheckAttendanceInLDADates(ByVal stuEnrollId As String, StartDate As DateTime, EndDate As DateTime) As Boolean
        Dim db As New SQLDataAccess
        Dim RowCount As Integer = 0
        db.OpenConnection()
        db.AddParameter("@stuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@StartDate", StartDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@EndDate", EndDate, SqlDbType.DateTime, , ParameterDirection.Input)
        RowCount = db.RunParamSQLScalar_SP("dbo.USP_CheckAttendanceInLDADates")
        If RowCount > 0 Then
            Return True
        Else
            Return False
        End If
        db.CloseConnection()

    End Function
    Public Function GetLoaReasons(ByVal campusid As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        With sb

            .Append("SELECT LOAReasonId,Descrip,(Case S.Status when 'Active' then 1 else 0 end) As Status ")
            .Append("FROM arLOAReasons T, syStatuses S ")
            .Append("WHERE T.StatusId=S.StatusId AND S.Status='Active' AND CampGrpId IN ")
            .Append("(SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId='" & campusid & "')")
            .Append("ORDER BY Status, Descrip")
        End With


        db.OpenConnection()
        ds = db.RunSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
End Class
