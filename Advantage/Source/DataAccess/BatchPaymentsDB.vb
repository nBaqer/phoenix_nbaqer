Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' BatchPaymentsDB.vb
'
' BatchPaymentsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class BatchPaymentsDB
    Public Function GetAllBatchPayments(ByVal isPosted As Boolean, ByVal dateFrom As Date, ByVal dateTo As Date) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   BP.BatchPaymentId, ")
            .Append("         BP.FundSourceId, ")
            .Append("         (Select FundSourceDescrip from saFundSources where FundSourceId=BP.FundSourceId) As FundSource, ")
            .Append("         BP.BatchPaymentNumber, ")
            .Append("         BP.BatchPaymentDescrip, ")
            .Append("         BP.BatchPaymentDate, ")
            .Append("         BP.UserId, ")
            .Append("         (Select FullName from syUsers where UserId=BP.UserId) As Employee ")
            .Append("FROM     saBatchPayments BP ")
            .Append("WHERE ")
            .Append("         IsPosted = ? ")

            '   add IsPosted parameter
            db.AddParameter("@IsPosted", isPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   check for Transaction Date From
            If Not dateFrom = Date.MinValue Then
                .Append(" AND ")
                .Append("BP.BatchPaymentDate >= ? ")
                db.AddParameter("@TransDateFrom", dateFrom, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            '   check for Transaction Date To
            If Not dateTo = Date.MaxValue Then
                .Append(" AND ")
                .Append("BP.BatchPaymentDate <= ? ")
                db.AddParameter("@TransDateTo", dateTo, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            .Append("ORDER BY BP.BatchPaymentDescrip ")

        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetBatchPaymentInfo(ByVal BatchPaymentId As String) As BatchPaymentInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT BP.BatchPaymentId, ")
            .Append("    BP.BatchPaymentNumber, ")
            .Append("    BP.FundSourceId, ")
            .Append("    (Select FundSourceDescrip from saFundSources where FundSourceId=BP.FundSourceId) As FundSource, ")
            .Append("    BP.BatchPaymentDate, ")
            .Append("    BP.BatchPaymentDescrip, ")
            .Append("    BP.BatchPaymentAmount, ")
            .Append("    BP.UserId, ")
            .Append("    (Select FullName from syUsers where UserId=BP.UserId) as Employee, ")
            .Append("    BP.IsPosted, ")
            .Append("    BP.ModUser, ")
            .Append("    BP.ModDate ")
            .Append("FROM  saBatchPayments BP ")
            .Append("WHERE BP.BatchPaymentId= ? ")
        End With

        ' Add the BatchPaymentId to the parameter list
        db.AddParameter("@BatchPaymentId", BatchPaymentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim BatchPaymentInfo As New BatchPaymentInfo

        While dr.Read()

            '   set properties with data from DataReader
            With BatchPaymentInfo
                .BatchPaymentId = BatchPaymentId
                .IsInDB = True
                .BatchNumber = dr("BatchPaymentNumber")
                .FundSourceId = CType(dr("FundSourceId"), Guid).ToString
                .FundSource = dr("FundSource")
                .BatchDate = dr("BatchPaymentDate")
                .BatchReference = dr("BatchPaymentDescrip")
                .BatchAmount = dr("BatchPaymentAmount")
                .UserId = CType(dr("UserId"), Guid).ToString
                .Employee = dr("Employee")
                .IsPosted = dr("IsPosted")
                If Not dr("ModUser") Is System.DBNull.Value Then .ModUser = dr("ModUser")
                If Not dr("ModDate") Is System.DBNull.Value Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BatchPaymentInfo
        Return BatchPaymentInfo

    End Function
    Public Function UpdateBatchPaymentInfo(ByVal BatchPaymentInfo As BatchPaymentInfo, ByVal user As String) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   Connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saBatchPayments Set BatchPaymentId = ?, BatchPaymentNumber = ?, ")
                .Append(" FundSourceId = ?, BatchPaymentDate = ?, BatchPaymentDescrip = ?, ")
                .Append(" BatchPaymentAmount = ?, UserId = ?, IsPosted=?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE BatchPaymentId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saBatchPayments where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   BatchPaymentId
            db.AddParameter("@BatchPaymentId", BatchPaymentInfo.BatchPaymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   BatchNumber
            db.AddParameter("@BatchPaymentNumber", BatchPaymentInfo.BatchNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   FundSourceId
            db.AddParameter("@FundSourceId", BatchPaymentInfo.FundSourceId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   BatchPaymentDate
            db.AddParameter("@BatchPaymentDate", BatchPaymentInfo.BatchDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   BatchReference
            db.AddParameter("@BatchPaymentDescrip", BatchPaymentInfo.BatchReference, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   BatchAmount
            db.AddParameter("@BatchAmount", BatchPaymentInfo.BatchAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            '   UserId
            db.AddParameter("@UserId", BatchPaymentInfo.UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   IsPosted
            db.AddParameter("@IsPosted", BatchPaymentInfo.IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   BatchPaymentId
            db.AddParameter("@BatchPaymentId1", BatchPaymentInfo.BatchPaymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", BatchPaymentInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddBatchPaymentInfo(ByVal BatchPaymentInfo As BatchPaymentInfo, ByVal user As String) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   Connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saBatchPayments (BatchPaymentId, BatchPaymentNumber, FundSourceId, ")
                .Append("   BatchPaymentDate, BatchPaymentDescrip, ")
                .Append("   BatchPaymentAmount, UserId, IsPosted, ")
                .Append("   ModUser, ModDate) ")
                .Append(" VALUES (?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   BatchPaymentId
            db.AddParameter("@BatchPaymentId", BatchPaymentInfo.BatchPaymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   BatchNumber
            db.AddParameter("@BatchPaymentNumber", BatchPaymentInfo.BatchNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   FundSourceId
            db.AddParameter("@FundSourceId", BatchPaymentInfo.FundSourceId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   BatchPaymentDate
            db.AddParameter("@BatchPaymentDate", BatchPaymentInfo.BatchDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   BatchReference
            db.AddParameter("@BatchPaymentDescrip", BatchPaymentInfo.BatchReference, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   BatchAmount
            db.AddParameter("@BatchAmount", BatchPaymentInfo.BatchAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            '   UserId
            db.AddParameter("@UserId", BatchPaymentInfo.UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   IsPosted
            db.AddParameter("@IsPosted", BatchPaymentInfo.IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteBatchPaymentInfo(ByVal BatchPaymentId As String, ByVal modDate As DateTime) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   Connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saBatchPayments ")
                .Append("WHERE BatchPaymentId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM saBatchPayments WHERE BatchPaymentId = ? ")
            End With

            '   add parameters values to the query

            '   BatchPaymentId
            db.AddParameter("@BatchPaymentId", BatchPaymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   BatchPaymentId
            db.AddParameter("@BatchPaymentId", BatchPaymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetMinAndMaxDatesFromBatchPayments() As Date()
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New System.Text.StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       Min(BP.BatchPaymentDate) As MinDate, ")
            .Append("       Max(BP.BatchPaymentDate) As MaxDate ")
            .Append("FROM   saBatchPayments BP ")
        End With

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   fill date Array 
        Dim dateArray(1) As DateTime
        While dr.Read()
            If Not dr("MinDate") Is System.DBNull.Value Then dateArray(0) = CType(dr("MinDate"), DateTime) Else dateArray(0) = Date.Now.Subtract(New TimeSpan(1824, 0, 0, 0))
            If Not dr("MaxDate") Is System.DBNull.Value Then dateArray(1) = CType(dr("MaxDate"), DateTime) Else dateArray(1) = Date.Now.Add(New TimeSpan(1824, 0, 0, 0))
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return date array
        Return dateArray

    End Function
    Public Function GetBatchPaymentsDS(ByVal isPosted As Boolean, ByVal dateFrom As Date, ByVal dateTo As Date) As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   create dataset
        Dim ds As New DataSet

        '   build select command
        Dim sc As New OleDbCommand

        '   build query to select BatchPayment records
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder
        With sb
            .Append("SELECT   BP.BatchPaymentId, ")
            .Append("         BP.FundSourceId, ")
            .Append("         (Select FundSourceDescrip from saFundSources where FundSourceId=BP.FundSourceId) As FundSource, ")
            .Append("         BP.BatchPaymentNumber, ")
            .Append("         BP.BatchPaymentDescrip, ")
            .Append("         BP.BatchPaymentDate, ")
            .Append("         BP.BatchPaymentAmount, ")
            .Append("         BP.UserId, ")
            '.Append("         (Select FullName from syUsers where UserId=BP.UserId) As Employee, ")
            .Append("         (Select UserName from syUsers where UserId=BP.UserId) As Employee, ")
            .Append("         BP.IsPosted, ")
            .Append("         BP.ModUser, ")
            .Append("         BP.ModDate ")
            .Append("FROM     saBatchPayments BP ")
            .Append("WHERE ")
            .Append("         IsPosted = ? ")

            '   add IsPosted parameter
            sc.Parameters.Add(New OleDbParameter("IsPosted", isPosted))

            '   check for Transaction Date From
            If Not dateFrom = Date.MinValue Then
                sb1.Append(" AND ")
                sb1.Append("BP.BatchPaymentDate >= ? ")
                sc.Parameters.Add(New OleDbParameter("TransDateFrom", dateFrom))
            End If

            '   check for Transaction Date To
            If Not dateTo = Date.MaxValue Then
                sb1.Append(" AND ")
                sb1.Append("BP.BatchPaymentDate <= ? ")
                sc.Parameters.Add(New OleDbParameter("TransDateTo", dateTo))
            End If

        End With

        '   build select command
        sc.CommandText = sb.ToString + sb1.ToString
        sc.Connection = New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))

        '   Create adapter to handle BatchPayments table
        Dim da As New OleDbDataAdapter(sc)

        '   Fill BatchPayments table
        da.Fill(ds, "BatchPayments")

        '   build query to select Transactions table
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       T.TransactionId, ")
            .Append("       (Select StudentId from arStuEnrollments where StuEnrollId=T.StuEnrollId) StudentId, ")
            .Append("       (Select (FirstName + ' ' + LastName) As StudentName from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=T.StuEnrollId)) StudentName, ")
            .Append("       T.StuEnrollId, ")
            .Append("       (Select PrgVerDescrip from arPrgVersions where PrgVerId=(Select PrgVerId from arStuEnrollments where StuEnrollId=T.StuEnrollId)) as Enrollment, ")
            .Append("       T.TransCodeId, ")
            .Append("       (Select TransCodeDescrip from saTransCodes where TransCodeId=T.TransCodeId) As TransCodeDescrip, ")
            .Append("       T.TransDescrip, ")
            .Append("       T.TransDate, ")
            .Append("       T.TransAmount, ")
            .Append("       T.TransReference, ")
            .Append("       T.AcademicYearId, ")
            .Append("       (Select AcademicYearDescrip from saAcademicYears where AcademicYearId=T.AcademicYearId) As AcademicYear, ")
            .Append("       T.TermId, ")
            .Append("       (Select TermDescrip from arTerm where TermId=T.TermId) As Term, ")
            .Append("       T.TransTypeId, ")
            .Append("       T.BatchPaymentId, ")
            .Append("       T.IsPosted, ")
            .Append("       T.ViewOrder, ")
            .Append("       T.ModUser, ")
            .Append("       T.ModDate ")
            .Append("FROM   saBatchPayments BP, saTransactions T ")
            .Append("WHERE ")
            .Append("         BP.IsPosted = ? ")
            .Append("AND      BP.BatchPaymentId=T.BatchPaymentId ")
            .Append("AND      T.Voided=0 ")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString + sb1.ToString

        '   fill Transactions table
        da.Fill(ds, "Transactions")

        '   create primary and foreign key constraints

        '   set primary key for BatchPayments table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("BatchPayments").Columns("BatchPaymentId")
        ds.Tables("BatchPayments").PrimaryKey = pk0

        '   set primary key for Transactions table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("Transactions").Columns("TransactionId")
        ds.Tables("Transactions").PrimaryKey = pk1

        '   set foreign key column in Transactions table
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("Transactions").Columns("BatchPaymentId")

        '   set relationships
        ds.Relations.Add("BatchPaymentsTransactions", pk0, fk0)

        '   return dataset
        Return ds

    End Function
    Public Function UpdateBatchPaymentsDS(ByVal ds As DataSet) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   all updates must be encapsulated as one transaction
        Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            '   build the sql query for the Transactions data adapter
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("       T.TransactionId, ")
                .Append("       T.StuEnrollId, ")
                .Append("       T.TransCodeId, ")
                .Append("       T.TransDescrip, ")
                .Append("       T.TransDate, ")
                .Append("       T.TransAmount, ")
                .Append("       T.TransReference, ")
                .Append("       T.AcademicYearId, ")
                .Append("       T.TermId, ")
                .Append("       T.TransTypeId, ")
                .Append("       T.BatchPaymentId, ")
                .Append("       T.ViewOrder, ")
                .Append("       T.IsPosted, ")
                .Append("       T.ModUser, ")
                .Append("       T.ModDate ")
                .Append("FROM   saTransactions T ")
                .Append("WHERE  T.Voided=0 ")

            End With

            '   build select command
            Dim TransactionsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle Transactions table
            Dim TransactionsDataAdapter As New OleDbDataAdapter(TransactionsSelectCommand)

            '   build insert, update and delete commands for Transactions table
            Dim cb As New OleDbCommandBuilder(TransactionsDataAdapter)

            '   build select query for the BatchPayments data adapter
            sb = New StringBuilder
            With sb
                '   with subqueries
                .Append("SELECT   BP.BatchPaymentId, ")
                .Append("         BP.FundSourceId, ")
                .Append("         BP.BatchPaymentNumber, ")
                .Append("         BP.BatchPaymentDescrip, ")
                .Append("         BP.BatchPaymentDate, ")
                .Append("         BP.BatchPaymentAmount, ")
                .Append("         BP.UserId, ")
                .Append("         BP.IsPosted, ")
                .Append("         BP.ModUser, ")
                .Append("         BP.ModDate ")
                .Append("FROM     saBatchPayments BP ")
            End With

            '   build select command
            Dim BatchPaymentsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle BatchPayments table
            Dim BatchPaymentsDataAdapter As New OleDbDataAdapter(BatchPaymentsSelectCommand)

            '   build insert, update and delete commands for BatchPayments table
            Dim cb1 As New OleDbCommandBuilder(BatchPaymentsDataAdapter)

            '   insert added rows in BatchPayments table
            BatchPaymentsDataAdapter.Update(ds.Tables("BatchPayments").Select(Nothing, Nothing, DataViewRowState.Added))

            '   insert added rows in Transactions table
            TransactionsDataAdapter.Update(ds.Tables("Transactions").Select(Nothing, Nothing, DataViewRowState.Added))

            '   delete rows in Transactions table
            TransactionsDataAdapter.Update(ds.Tables("Transactions").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   delete rows in BatchPayments table
            BatchPaymentsDataAdapter.Update(ds.Tables("BatchPayments").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   update rows in Transactions table
            TransactionsDataAdapter.Update(ds.Tables("Transactions").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   update rows in BatchPayments table
            BatchPaymentsDataAdapter.Update(ds.Tables("BatchPayments").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            Return ""

        Catch ex As OleDb.OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally

            '   close connection
            connection.Close()
        End Try

    End Function
End Class

