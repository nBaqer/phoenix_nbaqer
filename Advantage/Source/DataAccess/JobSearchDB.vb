
Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.Advantage.Common

Public Class JobSearchDB
    Public Function GetProgramVersions() As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   S.PrgVerId, S.PrgVerDescrip, ")
            .Append(" case when (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=S.ProgId) is null  then ")
            .Append(" S.PrgVerDescrip ")
            .Append(" else ")
            .Append(" S.PrgVerDescrip + ' (' + (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=S.ProgId) + ')'  ")
            .Append(" end as PrgVerShiftDescrip  ")
            .Append("FROM     arPrgVersions S, syStatuses ST ")
            .Append("WHERE    S.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY S.PrgVerDescrip ")
        End With
        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetSkills() As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   S.SkillId, S.SkillDescrip ")
            .Append("FROM     plSkills S, syStatuses ST ")
            .Append("WHERE    S.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY S.SkillDescrip ")
        End With
        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetCounties() As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   S.CountyId, S.CountyDescrip ")
            .Append("FROM     adCounties S, syStatuses ST ")
            .Append("WHERE    S.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY S.CountyDescrip ")
        End With
        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function

    Public Function GetCountyCodes() As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   S.CountyId, '(' + S.CountyCode + ') ' + S.CountyDescrip as CountyDescrip  ")
            .Append("FROM     adCounties S, syStatuses ST ")
            .Append("WHERE    S.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY S.CountyDescrip ")
        End With
        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function

    Public Function GetJobTitles() As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   S.TitleId, S.TitleDescrip ")
            .Append("FROM     adTitles S, syStatuses ST ")
            .Append("WHERE    S.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY S.TitleDescrip ")
        End With
        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetFullPartTime() As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   S.FullTimeId, S.FullPartTimeDescrip ")
            .Append("FROM     adFullPartTime S, syStatuses ST ")
            .Append("WHERE    S.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY S.FullPartTimeDescrip ")
        End With
        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetExpertiseLevel() As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   S.ExpertiseId, S.ExpertiseDescrip ")
            .Append("FROM     adExpertiseLevel S, syStatuses ST ")
            .Append("WHERE    S.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY S.ExpertiseDescrip ")
        End With
        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function JobSearchResults(ByVal strPrgVerID As String, ByVal strMinSalary As String, ByVal strMaxSalary As String, ByVal strCountyId As String, ByVal strAvailableDate As String, ByVal strSkillId As String, ByVal strCampus As String, ByVal strFullTimeId As String, ByVal strAvailableDateTo As String) As DataSet
        '   connect to the database
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'Dim strAvailDate As Date = CDate(strAvailableDate).ToShortDateString

        With sb
            .Append(" select distinct s1.StudentId,s1.FirstName,S1.LastName,S1.SSN ")
            .Append(" from arStudent S1,arStuEnrollments S2 ")

            If Not strSkillId = "" Then
                .Append(",plStudentSkills S3 ")
            End If

            .Append(" ,plExitInterview S4 ")
            .Append(" where s1.StudentId = s2.StudentId and s2.StuEnrollId=s4.EnrollmentId and  S4.Eligible='Yes' and S2.CampusId = ? ")
            db.AddParameter("@CampusId", strCampus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'If Not strMinSalary = "" Or Not strMaxSalary = "" Or Not strCountyId = "" Or Not strAvailableDate = "" Then
            '    .Append(",plExitInterview S4 ")
            'End If

            If Not strSkillId = "" Then
                .Append(" and s1.StudentId = s3.StudentId  ")
            End If

            If Not strPrgVerID = "" Then
                .Append(" and s2.PrgVerId = ? ")
                db.AddParameter("@PrgVerId", strPrgVerID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Not strSkillId = "" Then
                .Append(" and s3.SkillId = ? ")
                db.AddParameter("@SkillId", strSkillId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            If Not strMinSalary = "" Then
                .Append(" and s4.LowSalary = ? ")
                db.AddParameter("@MinSalary", strMinSalary, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Not strMaxSalary = "" Then
                .Append(" and s4.HighSalary = ? ")
                db.AddParameter("@MaxSalary", strMaxSalary, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Not strCountyId = "" Then
                .Append(" and s4.AreaId = ? ")
                db.AddParameter("@AreaId", strCountyId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Not strAvailableDate = "" Then
                .Append(" and s4.AvailableDate >= ? and s4.AvailableDate <= ? ")
                db.AddParameter("@AvailableDate", strAvailableDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@AvailableDate", strAvailableDateTo, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            'If Not strFullTimeId = "" Then
            '    .Append(" and s4.FullTimeId = ? ")
            '    db.AddParameter("@FullTimeId", strFullTimeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'End If
            .Append(" order by s1.LastName ")
        End With

        Return db.RunParamSQLDataSet(sb.ToString)
        sb.Remove(0, sb.Length)
    End Function
End Class
