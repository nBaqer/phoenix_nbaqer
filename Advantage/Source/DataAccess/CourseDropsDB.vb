Imports FAME.Advantage.Common

Public Class CourseDropsDB

#Region "Private Data Members"

    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")

#End Region


#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

#End Region


#Region "Public Methods"

    Public Function GetCourseDrops(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStuID As String

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If


        If StudentIdentifier = "SSN" Then
            strStuID = "arStudent.SSN AS StudentIdentifier,"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStuID = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
        ElseIf StudentIdentifier = "StudentId" Then
            strStuID = "arStudent.StudentNumber AS StudentIdentifier,"
        End If

        With sb
            .Append("SELECT distinct ")
            .Append("       arStudent.LastName,arStudent.FirstName,arStudent.MiddleName," & strStuID)
            .Append("       arGradeSystemDetails.Grade,arClassSections.ClsSection,")
            .Append("       arClassSections.TermId,(SELECT TermDescrip FROM arTerm WHERE TermId=arClassSections.TermId) AS TermDescrip,")
            .Append("       (SELECT StartDate FROM arTerm WHERE TermId=arClassSections.TermId) AS TermStartDate,")
            .Append("       (SELECT EndDate FROM arTerm WHERE TermId=arClassSections.TermId) AS TermEndDate,")
            .Append("       arClassSections.InstructorId,(SELECT FullName FROM syUsers WHERE UserId=arClassSections.InstructorId) AS InstructorName,")
            .Append("       arClassSections.StartDate,arClassSections.EndDate,arReqs.Code,arReqs.Descrip,arReqs.Credits,")
            .Append("       syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId,syCampuses.CampDescrip,")
            'code commented by Priyanka on date 27th April 2009
            '.Append("       arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StuStatusDescrip")
            'code added by Priyanka on date 27th April 2009
            .Append("       arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StuStatusDescrip, arResults.DateDetermined ")
            .Append(strOrderBy)
            .Append(" FROM   arResults,arStuEnrollments,arStudent,arGradeSystemDetails,arClassSections,arReqs,syCmpGrpCmps,syCampGrps,syCampuses,arTerm,arClassSectionTerms ")
            .Append("WHERE  arResults.StuEnrollId=arStuEnrollments.StuEnrollId")
            .Append("       AND arStuEnrollments.StudentId=arStudent.StudentId")
            .Append("       AND arResults.GrdSysDetailId=arGradeSystemDetails.GrdSysDetailId")
            .Append("       AND arGradeSystemDetails.IsDrop=1")
            .Append("       AND arResults.TestId=arClassSections.ClsSectionId")
            .Append("       AND arClassSections.ClsSectionId=arClassSectionTerms.ClsSectionId")
            .Append("       AND arClassSections.TermId=arTerm.TermId")
            .Append("       AND arClassSections.ReqId=arReqs.ReqId")
            .Append("       AND syCmpGrpCmps.CampusId=arStuEnrollments.CampusId")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId")
            .Append("       AND syCampuses.CampusId=arStuEnrollments.CampusId")
            .Append(strWhere)
            .Append(" ORDER BY syCampGrps.CampGrpDescrip,syCmpGrpCmps.CampGrpId,syCampuses.CampDescrip,")
            .Append("       arStuEnrollments.CampusId,TermStartDate,TermEndDate,TermDescrip")
            .Append(strOrderBy)
        End With

        ''With sb
        ''    .Append("SELECT arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
        ''    .Append(strStuID & "arGradeSystemDetails.Grade,arClassSections.ClsSection,")
        ''    .Append("arClassSections.TermId,(SELECT TermDescrip FROM arTerm WHERE TermId=arClassSections.TermId) AS TermDescrip,")
        ''    .Append("arClassSections.InstructorId,(SELECT FullName FROM syUsers WHERE UserId=arClassSections.InstructorId) AS InstructorName,")
        ''    .Append("arClassSections.StartDate,arClassSections.EndDate,arReqs.Code,arReqs.Descrip,arReqs.Credits,")
        ''    .Append("syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId,syCampuses.CampDescrip,")
        ''    .Append("arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StuStatusDescrip ")
        ''    .Append("FROM arResults,arStuEnrollments,arStudent,arGradeSystemDetails,arClassSections,arReqs,syCmpGrpCmps,syCampGrps,syCampuses ")
        ''    .Append("WHERE arResults.StuEnrollId=arStuEnrollments.StuEnrollId ")
        ''    .Append("AND arStuEnrollments.StudentId=arStudent.StudentId ")
        ''    .Append("AND arResults.GrdSysDetailId=arGradeSystemDetails.GrdSysDetailId ")
        ''    .Append("AND arGradeSystemDetails.IsDrop=1 ")
        ''    .Append("AND arResults.TestId=arClassSections.ClsSectionId ")
        ''    .Append("AND arClassSections.ReqId=arReqs.ReqId ")
        ''    .Append("AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId ")
        ''    .Append("AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
        ''    .Append("AND syCampuses.CampusId=arStuEnrollments.CampusId ")
        ''    .Append(strWhere)
        ''    .Append(strOrderBy)
        ''End With
        ''With sb
        ''    .Append("SELECT arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
        ''    .Append("arGradeSystemDetails.Grade,arClassSections.ClsSection,")
        ''    .Append("arClassSections.TermId,(SELECT TermDescrip FROM arTerm WHERE TermId=arClassSections.TermId) AS TermDescrip,")
        ''    .Append("arClassSections.InstructorId,(SELECT FullName FROM syUsers WHERE UserId=arClassSections.InstructorId) AS InstructorName,")
        ''    .Append("arClassSections.StartDate,arClassSections.EndDate,arReqs.Code,arReqs.Descrip,arReqs.Credits,")
        ''    .Append("syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId,syCampuses.CampDescrip,")
        ''    .Append("arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StuStatusDescrip ")
        ''    .Append("FROM arResults,arStuEnrollments,arStudent,arGradeSystemDetails,arClassSections,arReqs,syCmpGrpCmps,syCampGrps,syCampuses ")
        ''    .Append("WHERE arResults.StuEnrollId=arStuEnrollments.StuEnrollId ")
        ''    .Append("AND arStuEnrollments.StudentId=arStudent.StudentId ")
        ''    .Append("AND arResults.GrdSysDetailId=arGradeSystemDetails.GrdSysDetailId ")
        ''    .Append("AND arGradeSystemDetails.IsDrop=1 ")
        ''    .Append("AND arResults.TestId=arClassSections.ClsSectionId ")
        ''    .Append("AND arClassSections.ReqId=arReqs.ReqId ")
        ''    .Append("AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId ")
        ''    .Append("AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
        ''    .Append("AND syCampuses.CampusId=arStuEnrollments.CampusId ")
        ''    .Append(strWhere)
        ''    .Append(strOrderBy)
        ''End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "ListOfCourseDrops"
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("CourseDropCount", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("TermIdStr", System.Type.GetType("System.String")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function


    Public Function GetCoursePrereqs(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String

        If paramInfo.FilterList <> "" Then
            Dim temp As String = paramInfo.FilterList
            If temp.IndexOf("arReqs.ReqId") <> -1 Then
                temp = temp.Replace("arReqs.ReqId", "arCourseReqs.ReqId")
            End If
            strWhere &= " AND " & temp
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        Else
            strOrderBy &= ",Descrip,Code,PreReqDescrip,PreReqCode"
        End If

        With sb
            .Append("SELECT DISTINCT ")
            ''Modified By Saraswathi lakshmanan on August 06 2009
            ''For mantis case 16753 -- prereqs for program Version
            .Append("  isNull(arCourseReqs.PrgVerId, (select cast(cast(0 as binary) as uniqueidentifier)))PrgVerID ")
            .Append(" ,IsNull((Select PrgverDescrip from arPrgVersions where prgVerId=arCourseReqs.PrgVerId),'All Programs')PrgVerDescrip,")

            .Append("       arCourseReqs.PreCoReqId,arCourseReqs.ReqId,(SELECT Descrip FROM arReqs R WHERE R.ReqId=arCourseReqs.ReqId) AS Descrip,")
            .Append("       (SELECT Code FROM arReqs R WHERE R.ReqId=arCourseReqs.ReqId) AS Code,")
            .Append("       (SELECT Credits FROM arReqs R WHERE R.ReqId=arCourseReqs.ReqId) AS Credits,")
            .Append("       (SELECT Hours FROM arReqs R WHERE R.ReqId=arCourseReqs.ReqId) AS Hours,")
            .Append("       B.Descrip AS PreReqDescrip,B.Code AS PreReqCode,")
            .Append("       B.Credits AS PreReqCredits,B.Hours AS PreReqHours,")
            .Append("       (SELECT Descrip FROM arReqTypes WHERE ReqTypeId=B.ReqTypeId) AS PreReqTypeDescrip,")
            .Append("       B.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,syCampuses.CampDescrip ")
            .Append("FROM   arCourseReqs,arReqs B,syCampGrps,syCmpGrpCmps,syCampuses ")
            If strWhere.Contains("arProgVerDef.PrgVerId") Then
                .Append(" ,arProgVerDef ")
            End If

            .Append("WHERE  arCourseReqs.CourseReqTypId = 1 And arCourseReqs.PreCoReqId = B.ReqId ")
            If strWhere.Contains("arProgVerDef.PrgVerId") Then
                .Append(" And arCourseReqs.ReqId=arProgVerDef.ReqId    ")
                .Append(" And (arCourseReqs.PrgVerId=arProgVerDef.PrgVerId or arCourseReqs.PrgVerId is Null)    ")
            End If

            .Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId ")
            '.Append("       AND B.CampGrpId=syCampGrps.CampGrpId ")
            .Append(strWhere)
            .Append("ORDER BY syCampGrps.CampGrpDescrip,B.CampGrpId,syCampuses.CampDescrip,syCampuses.CampusId")
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "CoursePrerequisits"
            'ds.Tables(0).Columns.Add(New DataColumn("CoursesCount", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("ReqIdStr", System.Type.GetType("System.String")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
#End Region
    Public Function GetSummaryOfCourseDrops(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStuID As String

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        With sb
            .Append("SELECT distinct ")
            .Append("       arResults.StuEnrollId, ")
            .Append("       arReqs.Code,arReqs.Descrip,arReqs.Credits,")
            .Append("       syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId,syCampuses.CampDescrip ")
            .Append("FROM   arResults,arStuEnrollments,arGradeSystemDetails,arClassSections,arReqs,syCmpGrpCmps,syCampGrps,syCampuses,arTerm,arClassSectionTerms ")
            .Append("WHERE  arResults.StuEnrollId=arStuEnrollments.StuEnrollId")
            .Append("       AND arResults.GrdSysDetailId=arGradeSystemDetails.GrdSysDetailId")
            .Append("       AND arGradeSystemDetails.IsDrop=1")
            .Append("       AND arResults.TestId=arClassSections.ClsSectionId")
            .Append("       AND arClassSections.ClsSectionId=arClassSectionTerms.ClsSectionId")
            .Append("       AND arClassSections.TermId=arTerm.TermId")
            .Append("       AND arClassSections.ReqId=arReqs.ReqId")
            .Append("       AND syCmpGrpCmps.CampusId=arStuEnrollments.CampusId")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId")
            .Append("       AND syCampuses.CampusId=arStuEnrollments.CampusId ")
            .Append(strWhere)
            .Append(" ORDER BY syCampGrps.CampGrpDescrip,syCmpGrpCmps.CampGrpId,syCampuses.CampDescrip,")
            .Append("       arStuEnrollments.CampusId ")
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds


        'Modified by Balaji on 04/26/2007 while creating Course Dropped Summary Report

        'This is the original query thats gets a list of Courses Dropped and 
        'we build the query for summary of courses dropped from this query
        'incase if Implementation Team wants the output to change we can 
        'add it from this query

        '.Append("SELECT distinct ")
        '.Append("       arStudent.LastName,arStudent.FirstName,arStudent.MiddleName," & strStuID)
        '.Append("       arGradeSystemDetails.Grade,arClassSections.ClsSection,")
        '.Append("       arClassSections.TermId,(SELECT TermDescrip FROM arTerm WHERE TermId=arClassSections.TermId) AS TermDescrip,")
        '.Append("       (SELECT StartDate FROM arTerm WHERE TermId=arClassSections.TermId) AS TermStartDate,")
        '.Append("       (SELECT EndDate FROM arTerm WHERE TermId=arClassSections.TermId) AS TermEndDate,")
        '.Append("       arClassSections.InstructorId,(SELECT FullName FROM syUsers WHERE UserId=arClassSections.InstructorId) AS InstructorName,")
        '.Append("       arClassSections.StartDate,arClassSections.EndDate,arReqs.Code,arReqs.Descrip,arReqs.Credits,")
        '.Append("       syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId,syCampuses.CampDescrip,")
        '.Append("       arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StuStatusDescrip ")
        '.Append("FROM   arResults,arStuEnrollments,arStudent,arGradeSystemDetails,arClassSections,arReqs,syCmpGrpCmps,syCampGrps,syCampuses,arTerm,arClassSectionTerms ")
        '.Append("WHERE  arResults.StuEnrollId=arStuEnrollments.StuEnrollId")
        '.Append("       AND arStuEnrollments.StudentId=arStudent.StudentId")
        '.Append("       AND arResults.GrdSysDetailId=arGradeSystemDetails.GrdSysDetailId")
        '.Append("       AND arGradeSystemDetails.IsDrop=1")
        '.Append("       AND arResults.TestId=arClassSections.ClsSectionId")
        '.Append("       AND arClassSections.ClsSectionId=arClassSectionTerms.ClsSectionId")
        '.Append("       AND arClassSections.TermId=arTerm.TermId")
        '.Append("       AND arClassSections.ReqId=arReqs.ReqId")
        '.Append("       AND syCmpGrpCmps.CampusId=arStuEnrollments.CampusId")
        '.Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId")
        '.Append("       AND syCampuses.CampusId=arStuEnrollments.CampusId")
        '.Append(strWhere)
        '.Append(" ORDER BY syCampGrps.CampGrpDescrip,syCmpGrpCmps.CampGrpId,syCampuses.CampDescrip,")
        '.Append("       arStuEnrollments.CampusId,TermStartDate,TermEndDate,TermDescrip")
        '.Append(strOrderBy)



    End Function

#Region "Get AdvAppsetting for Manage Config entry"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region


End Class

