Imports System.Data
Imports FAME.Advantage.Common

Public Class GradebookResultsDB

#Region "Private Data Members"
    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")

#End Region


#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property


#End Region

#Region "Public Methods"

    'Public Function GradebookResults(ByVal paramInfo As ReportParamInfo) As DataSet
    '    '   connect to the database
    '    Dim db As New DataAccess
    '    Dim ds As DataSet
    '    Dim strWhere As String
    '    Dim score As Integer


    '    db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

    '    If paramInfo.FilterOther <> "" Then
    '        strWhere &= " AND " & paramInfo.FilterOther
    '    End If

    '    If paramInfo.FilterList <> "" Then
    '        strWhere &= " AND " & paramInfo.FilterList
    '    End If

    '    If paramInfo.FilterOtherString <> "" Then
    '        score = CInt(paramInfo.FilterOtherString)
    '    End If

    '    '   build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append(" select (arstudent.LastName + ' ' + arstudent.FirstName) as Studentname , ")
    '        .Append(" arstudent.SSN,   ")
    '        .Append(" ('('+arreqs.code+') - '+arreqs.descrip+' - '+arClassSections.clssection) as Class,")
    '        .Append(" (syusers.fullname) as InstructorName,  ")
    '        .Append(" (minval-maxval) as variance, (maxval) as Score,    ")
    '        .Append(" arGrdComponentTypes.descrip,syCampGrps.CampGrpId ")
    '        .Append(" from arstudent,arClsSectStudents,arClassSections , ")
    '        .Append(" arreqs,syusers,arGradeScaleDetails,arGrdComponentTypes, ")
    '        .Append(" arGrdBkWeights,arGrdBkWgtDetails,syCampGrps,arstuenrollments ")
    '        .Append(" where arstudent.studentid=arClsSectStudents.studentid and ")
    '        .Append(" arstudent.studentid=arstuenrollments.StudentId and  ")
    '        .Append(" arstuenrollments.CampusId in  ")
    '        .Append(" (select Distinct t1.CampusId from syCmpGrpCmps t1 where t1.CampGrpId in ( '")
    '        .Append(paramInfo.CampGrpId)
    '        .Append("')) and ")
    '        .Append(" arClsSectStudents.ClsSectionId=arClassSections.ClsSectionId and ")
    '        .Append(" arClassSections.ReqId =arreqs.ReqId AND arClassSections.InstructorId=syusers.userid and ")
    '        .Append(" arClassSections.GrdScaleId=arGradeScaleDetails.GrdScaleId and ")
    '        .Append(" arClassSections.ReqId =arGrdBkWeights.ReqId and ")
    '        .Append(" arClassSections.InstructorId=arGrdBkWeights.InstructorId and ")
    '        .Append(" arClassSections.InstrGrdBkWgtId=arGrdBkWeights.InstrGrdBkWgtId and ")
    '        .Append(" arGrdBkWeights.InstrGrdBkWgtId=arGrdBkWgtDetails.InstrGrdBkWgtId and ")
    '        .Append(" arGrdBkWgtDetails.GrdComponentTypeId=arGrdComponentTypes.GrdComponentTypeId ")
    '        .Append(strWhere)
    '    End With

    '    ds = db.RunParamSQLDataSet(sb.ToString)
    '    Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
    '    Dim ds1 As New DataSet
    '    Dim dt As New DataTable("GradeBookResults")
    '    If ds.Tables.Count > 0 Then
    '        dt.Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("SSN", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("Class", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("InstructorName", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("Variance", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("Score", System.Type.GetType("System.String")))
    '        Dim row As DataRow
    '        While dr.Read()
    '            row = dt.NewRow()
    '            row("Studentname") = dr("Studentname").ToString
    '            row("SSN") = dr("SSN").ToString
    '            row("Class") = dr("Class").ToString
    '            row("InstructorName") = dr("InstructorName").ToString
    '            row("Variance") = dr("Variance").ToString
    '            row("Score") = dr("Score").ToString
    '            If CInt(row("Score")) <= score Then
    '                dt.Rows.Add(row)
    '            End If
    '        End While
    '        ds1.Tables.Add(dt)
    '    End If

    '    If Not dr.IsClosed Then dr.Close()

    '    Return ds1
    'End Function
    Public Function GetGradeScaleDetails(ByVal clsSectionId As String) As DataTable
        Dim db As New DataAccess
        Dim ds As New DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        Dim sb1 As New StringBuilder
        With sb1
            .Append(" SELECT    A.GrdScaleDetailId,A.GrdScaleId,A.MinVal,A.MaxVal,A.GrdSysDetailId, ")
            .Append("           (select Distinct IsPass from arGradeSystemDetails where GrdSysDetailId=A.GrdSysDetailId) as Pass, ")
            .Append("           (Select Distinct GrdSystemId from arGradeSystemDetails where GrdSysDetailId=A.GrdSysDetailId) as GrdSystemId ")
            .Append(" FROM      arGradeScaleDetails A ")
            .Append(" WHERE     A.GrdScaleId In ")
            .Append("               (Select GrdScaleId from arClassSections where ClsSectionId = ?) ")
            .Append(" order by MinVal ")
        End With
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'execute query
        ds = db.RunParamSQLDataSet(sb1.ToString)

        Return ds.Tables(0)
    End Function
    Public Function GetWeightsByClassSectionStudentEnrollment(ByVal ClsSectionId As String) As Integer

        '   connect to the database
        Dim db As New DataAccess
        Dim sbGetStuEnrollId As New StringBuilder
        Dim strStuEnrollId, strClsSectionId As String
        Dim strIsGraded As String

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        Dim intTotalWeights As Integer
        With sb
            .Append(" select  ")
            .Append("         Count(t3.Descrip) as TotalWeights ")
            .Append(" from ")
            .Append("       arClassSections t1,arGrdBkWeights t2,arGrdBkWgtDetails t3  ")
            .Append(" where ")
            .Append("			t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId and  ")
            .Append("           t2.InstrGrdBkWgtId = t3.InstrGrdBkWgtId and  ")
            .Append("           t1.ClsSectionId =?  ")
        End With
        db.AddParameter("@ClsSectionId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Try
            Dim drPassSatisfactory As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            While drPassSatisfactory.Read
                intTotalWeights = drPassSatisfactory("TotalWeights")
            End While

            If Not drPassSatisfactory.IsClosed Then drPassSatisfactory.Close()

        Catch ex As Exception
            intTotalWeights = 0
        End Try
        Return intTotalWeights
    End Function
    Public Function GetTotalWeightsAtCourseLevel(ByVal CampusId As String, ByVal ClsSectionId As String) As Integer
        '   connect to the database
        Dim db As New DataAccess
        Dim sbGetStuEnrollId As New StringBuilder
        Dim strStuEnrollId, strClsSectionId As String
        Dim strIsGraded As String


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim maxNum As Integer = GetAdvAppSettings.AppSettings("MaxNoOfGradeBookWeightings")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb

            'Comment added by Balaji on 10/14/2005

            'Get The Maximum Number of weights for each classsection
            'Get the number of weights by class section and student enrollment
            'If the student enrollment has an incomplete grade assume the student enrollment
            'as completely graded so instead of min weights attempted put the maximum number
            'of credits for incomplete grade

            'Check if Max Number of credits is equal to min number taken,If they are equal 
            ' then Grade has been completed else NOT GRADED.
            .Append(" select TotalWeightsForClsSection ")
            .Append(" from ")
            .Append(" (  ")
            .Append(" select Distinct  ")
            .Append("              	Code,Descrip,ClsSection,IsGrades,GetMinCreditAttempted.ClsSectionId as TestCls, ")
            .Append("        	TotalWeightsForClsSection,MinimumWeightsAttempted, ")
            .Append("                	Case when (IsGrades=0 or IsGrades is Null) then 'No' else 'Yes' end as Grade ")
            .Append("                from ")
            .Append("                	( ")
            .Append("                		select Distinct Code,Descrip,ClsSectionId,ClsSection,IsGraded as IsGrades, ")
            .Append("                		( ")
            .Append(" select ")
            .Append("   Sum(coalesce(t3.Number," & maxNum & ")) ")
            .Append(" from ")
            .Append("				arGrdBkWeights t1,arClassSections t2,arGrdBkWgtDetails  t3,arGrdComponentTypes t4 ")
            .Append(" where ")
            .Append("				t1.ReqId=t2.ReqId and t1.EffectiveDate <= t2.StartDate and ")
            .Append("				t2.ClsSectionId=b2.ClsSectionId and ")
            .Append("				t3.InstrGrdBkWgtId=t1.InstrGrdBkWgtId and t3.Weight>0 and ")
            .Append("               t3.GrdComponentTypeId = t4.GrdComponentTypeId ")
            .Append("               		) as TotalWeightsForClsSection ")
            .Append("               		from ")
            .Append("                			arReqs b1,arClassSections b2,arResults b3 ")
            .Append("               		where 	")
            .Append("               			b1.ReqId = b2.ReqId and b2.ClsSectionId = b3.TestId ")
            '            .Append("               and			b2.TermId = '" & TermId & "'      ")
            .Append("                			and b2.CampusId='" & CampusId & "'  ")
            .Append("                	) ")
            .Append("                	GetMaxGrdAtt, ")
            .Append("                	( ")
            .Append("                		Select Distinct ")
            .Append("                			ClsSectionId, ")
            .Append("                			Min(MinWeightsAttempted) as MinimumWeightsAttempted ")
            .Append("                		from ")
            .Append("                			( ")
            .Append("                				select Distinct ")
            .Append("                					C2.ClsSectionId, ")
            .Append("                					c2.ClsSection, ")
            .Append("                					C1.StuEnrollId, ")
            .Append("                					Case when ")
            .Append("                						(select IsInComplete from arResults ")
            .Append("                							where TestId = C2.ClsSectionId and ")
            .Append("                							StuEnrollId = C1.StuEnrollId ")
            .Append("                						) =  1 ")
            .Append("                					then ")
            .Append("                					    (Select Sum(coalesce(t3.Number," & maxNum & ")) ")
            .Append("                					    from 	arGrdBkWeights t1,arClassSections t2,arGrdBkWgtDetails t3,arGrdComponentTypes t4 ")
            .Append("                					    where   t1.ReqId = t2.ReqId And t1.EffectiveDate <= t2.StartDate ")
            .Append("                					            and t2.ClsSectionId=c1.ClsSectionId ")
            .Append("                					            and t3.InstrGrdBkWgtId=t1.InstrGrdBkWgtId and t3.Weight>0")
            .Append("                					            and t3.GrdComponentTypeId = t4.GrdComponentTypeId) ")
            '.Append("                						(select  ")
            '.Append("                							Count(t3.Descrip)  ")
            '.Append("                						from  ")
            '.Append("                					arClassSections t1,arGrdBkWeights t2,arGrdBkWgtDetails t3 ")
            '.Append("                						where  ")
            '.Append("                						t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId and  ")
            '.Append("                						t2.InstrGrdBkWgtId = t3.InstrGrdBkWgtId and ")
            '.Append("                						t1.ClsSectionId = C2.ClsSectionId and ")
            '.Append("                						t1.TermId = '" & TermId & "' ")
            '.Append("                						and   t1.CampusId='" & CampusId & "' ")
            '.Append("                						)  ")
            .Append("                					else ")
            .Append("                					(select ")
            .Append("                							count(*) from arGrdBkResults ")
            .Append("                						where ")
            .Append("               							ClsSectionId=c2.ClsSectionId and ")
            .Append("                							StuEnrollId=C1.StuEnrollId ")
            .Append("                					) ")
            .Append("                					end ")
            .Append("                					as MinWeightsAttempted ")
            .Append("                				from ")
            .Append("                					arGrdBkResults C1,arClassSections C2,arResults C3 ")
            .Append("               				where ")
            .Append("               			c1.ClsSectionId = c2.ClsSectionId and C2.ClsSectionId = C3.TestId  ")
            '.Append("                			and C2.TermId = '" & TermId & "'  ")
            .Append("               			and    C2.CampusId='" & CampusId & "' ")
            .Append("               			) ")
            .Append("               			GetMinGrdAtt ")
            .Append("               			group by ClsSectionId ")
            .Append("              	) ")
            .Append("               	GetMinCreditAttempted ")
            .Append("               where GetMaxGrdAtt.ClsSectionId = GetMinCreditAttempted.ClsSectionId ")
            .Append("               and GetMaxGrdAtt.ClsSectionId='" & ClsSectionId & "' ")
            .Append("   ) ")
            .Append("   TTT ")
        End With
        Dim intTotalWeights As Decimal = 0.0
        Try
            Dim drPassSatisfactory As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            While drPassSatisfactory.Read
                If Not drPassSatisfactory("TotalWeightsForClsSection") Is System.DBNull.Value Then intTotalWeights = drPassSatisfactory("TotalWeightsForClsSection") Else intTotalWeights = 0
            End While

            If Not drPassSatisfactory.IsClosed Then drPassSatisfactory.Close()
            
        Catch ex As System.Exception
            intTotalWeights = 0
        End Try
        Return intTotalWeights
    End Function

    Public Function GetGradeBookResultsData(ByVal paramInfo As ReportParamInfo) As DataTable
        '   connect to the database
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strWhere As String
        Dim score As Decimal
        Dim objDB As New TransferGradeDB


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOtherString <> "" Then
            score = CInt(paramInfo.FilterOtherString)
        End If
        Dim strGrdBk As String = GetAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString()
        '     Sum(A.Score * B.Weight * .01) as FinalScore,
        'Sum(B.Weight) as TotalWeight,

        Dim sb As New StringBuilder
        Dim maxNum As Integer = GetAdvAppSettings.AppSettings("MaxNoOfGradeBookWeightings")
        With sb
            .Append(" Select Distinct " + vbCrLf)
            .Append(" A.StuEnrollId,A.ClsSectionId, " + vbCrLf)
            .Append(" Sum(A.Score * B.Weight * .01) as FinalScore,Sum(B.Weight) as TotalWeight, " + vbCrLf)
            .Append(" D.Code,D.Descrip,C.ClsSection," + vbCrLf)
            .Append(" C.StartDate,C.EndDate,G.FirstName,G.LastName,G.SSN,G.StudentNumber,F.EnrollmentId, " + vbCrLf)
            .Append(" B.InstrGrdBkWgtId,C.ReqId,C.ClsSectionId,C.InstructorId, " + vbCrLf)
            .Append(" (select Count(*) from arGrdBkResults where StuEnrollId=A.StuEnrollId " + vbCrLf)
            .Append(" and ClsSectionId=A.ClsSectionId) as TotalWeightsAttempted, " + vbCrLf)
            .Append(" GradeBkWeightLevel = '" & strGrdBk & "'," + vbCrLf)
            '.Append(" GradeBkWeightLevel = " + vbCrLf)
            '.Append(" CASE " + vbCrLf)
            '.Append(" WHEN (select COUNT(*) from arGrdBkWeights ABW where ABW.InstrGrdBkWgtId=B.InstrGrdBkWgtId and ABW.ReqId is not null) >=1 " + vbCrLf)
            '.Append(" THEN 'CourseLevel' ELSE 'InstructorLevel' " + vbCrLf)
            '.Append(" END," + vbCrLf)
            .Append(" (select Top 1 FullName from syUsers where UserId=C.InstructorId) as InstructorName, " + vbCrLf)
            .Append("(select Sum(coalesce(t3.Number," & maxNum & ")) " + vbCrLf)
            .Append("from arGrdBkWeights t1,arGrdBkWgtDetails  t3,arGrdComponentTypes t4  " + vbCrLf)
            .Append("where t1.ReqId=C.ReqId and t1.EffectiveDate <= C.StartDate and " + vbCrLf)
            .Append("		C.ClsSectionId=C.ClsSectionId and " + vbCrLf)
            .Append("		t3.InstrGrdBkWgtId=t1.InstrGrdBkWgtId and t3.Weight>0 and " + vbCrLf)
            .Append("		t3.GrdComponentTypeId = t4.GrdComponentTypeId ) as TotalWgtsSetForClass, " + vbCrLf)
            .Append(" 	C.CampusId,syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip, " + vbCrLf)
            .Append("  (SELECT Top 1 CampDescrip FROM syCampuses WHERE CampusId=C.CampusId) AS CampusDescrip " + vbCrLf)
            .Append(" from " + vbCrLf)
            .Append(" arGrdBkResults A,arGrdBkWgtDetails B, " + vbCrLf)
            .Append(" arClassSections C,arReqs D, " + vbCrLf)
            .Append(" arStuEnrollments F,arStudent G, " + vbCrLf)
            .Append(" syCampGrps ,syCmpGrpCmps I " + vbCrLf)
            .Append(" ,  arGrdComponentTypes J ")
            .Append(" where B.InstrGrdBkWgtDetailId=A.InstrGrdBkWgtDetailId and " + vbCrLf)
            .Append(" A.ClsSectionId=C.ClsSectionId and C.ReqId=D.ReqId and  " + vbCrLf)
            .Append(" A.StuEnrollId = F.StuEnrollId And F.StudentId = G.StudentId and " + vbCrLf)
            .Append(" syCampGrps.CampGrpId=I.CampGrpId AND C.CampusId=I.CampusId AND ")
            .Append(" B.GrdComponentTypeId=J.GrdComponentTypeId AND J.SysComponentTypeId NOT IN (500,503,544)  ")
            .Append(" and C.ClsSectionId in (select Distinct ClsSectionId from arClassSections where " + vbCrLf)
            .Append(paramInfo.classdate())
            .Append(")  ")
            If paramInfo.PrgVerId <> "" Then
                .Append(" and F.PrgVerId in (")
                .Append(paramInfo.PrgVerId)
                .Append(")  " + vbCrLf)
            End If
            If paramInfo.CohortStartDate <> "" Then
                .Append(" and F.CohortStartDate ")
                .Append(paramInfo.CohortStartDate)
            End If
            .Append(" and A.Score is not null " + vbCrLf)
            .Append(" and syCampGrps.CampGrpId in  " + vbCrLf)
            .Append(" (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in ('")
            .Append(paramInfo.CampGrpId)
            .Append("'))  " + vbCrLf)
            .Append("  AND F.CampusId= '" & paramInfo.CampusId & "'")
            .Append(" group by " + vbCrLf)
            .Append(" A.StuEnrollId,A.ClsSectionId, " + vbCrLf)
            .Append(" D.Code,D.Descrip,C.ClsSection, " + vbCrLf)
            .Append(" C.StartDate,C.EndDate,G.FirstName,G.LastName,G.SSN,G.StudentNumber,F.EnrollmentId, " + vbCrLf)
            .Append(" B.InstrGrdBkWgtId, C.ReqId, C.ClsSectionId,C.InstructorId, " + vbCrLf)
            .Append(" syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,C.CampusId " + vbCrLf)
            .Append(" Order by syCampGrps.CampGrpDescrip,C.CampusId, " + vbCrLf)
            .Append(" C.StartDate,C.EndDate,G.LastName,G.FirstName,D.Code, D.Descrip, C.ClsSection " + vbCrLf)
        End With
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds.Tables(0)
    End Function
#End Region

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

End Class
