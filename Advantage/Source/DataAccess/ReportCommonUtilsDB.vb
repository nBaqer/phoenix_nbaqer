Imports FAME.Advantage.Common


Public Class ReportCommonUtilsDB

#Region "Get AdvAppsetting object"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
    Public Function GetStudentNameAndPrgVersion(ByVal StuEnrollId As String) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim dt As New DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dt.TableName = "StudentNameAndPrgVersion"
        dt.Columns.Add("StudentName", System.Type.GetType("System.String"))
        dt.Columns.Add("PrgVerDescrip", System.Type.GetType("System.String"))

        If StuEnrollId <> "StuEnrollmentId" Then
            With sb
                .Append("SELECT LastName,MiddleName,FirstName,PrgVerDescrip ")
                .Append("FROM arStuEnrollments, arStudent, arPrgVersions ")
                .Append("WHERE arStuEnrollments.StuEnrollId = ? ")
                .Append("AND arStuEnrollments.StudentId = arStudent.StudentId ")
                .Append("AND arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId")
            End With

            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            ds = db.RunParamSQLDataSet(sb.ToString)

            If ds.Tables(0).Rows.Count > 0 Then
                Dim r As DataRow
                Dim stuName As String

                r = dt.NewRow

                'Set up student name as: "LastName, FirstName MI."
                stuName = ds.Tables(0).Rows(0)("LastName")
                If Not (ds.Tables(0).Rows(0)("FirstName") Is System.DBNull.Value) Then
                    If ds.Tables(0).Rows(0)("FirstName") <> "" Then
                        stuName &= ", " & ds.Tables(0).Rows(0)("FirstName")
                    End If
                End If
                If Not (ds.Tables(0).Rows(0)("MiddleName") Is System.DBNull.Value) Then
                    If ds.Tables(0).Rows(0)("MiddleName") <> "" Then
                        stuName &= " " & ds.Tables(0).Rows(0)("MiddleName") & "."
                    End If
                End If

                r("StudentName") = stuName
                r("PrgVerDescrip") = ds.Tables(0).Rows(0)("PrgVerDescrip")
                dt.Rows.Add(r)

            End If

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
            
        End If

        Return dt

    End Function

    Public Function GetStudentName(ByVal StudentId As String) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim dt As New DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT LastName,MiddleName,FirstName ")
            .Append("FROM arStudent ")
            .Append("WHERE StudentId = ? ")
        End With

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables(0).Rows.Count > 0 Then
            dt.TableName = "StudentName"
            dt.Columns.Add("StudentName", System.Type.GetType("System.String"))

            Dim r As DataRow
            Dim stuName As String
            r = dt.NewRow
            'Set up student name as: "LastName, FirstName MI."
            stuName = ds.Tables(0).Rows(0)("LastName")
            If Not (ds.Tables(0).Rows(0)("FirstName") Is System.DBNull.Value) Then
                If ds.Tables(0).Rows(0)("FirstName") <> "" Then
                    stuName &= ", " & ds.Tables(0).Rows(0)("FirstName")
                End If
            End If
            If Not (ds.Tables(0).Rows(0)("MiddleName") Is System.DBNull.Value) Then
                If ds.Tables(0).Rows(0)("MiddleName") <> "" Then
                    stuName &= " " & ds.Tables(0).Rows(0)("MiddleName") & "."
                End If
            End If
            r("StudentName") = stuName
            dt.Rows.Add(r)
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return dt
    End Function

    Public Shared Function GetRptAgencyFldValues(ByVal AgencyName As String, ByVal FldName As String) As DataTable
        Dim strSQL As String
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        strSQL = "SELECT " & _
           "syRptAgencyFldValues.RptAgencyFldValId, " & _
           "syRptAgencyFldValues.AgencyDescrip " & _
           "FROM " & _
           "syRptAgencies, syRptFields, syRptAgencyFields, syRptAgencyFldValues " & _
           "WHERE " & _
           "syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND " & _
           "syRptAgencyFields.RptFldId = syRptFields.RptFldId AND " & _
           "syRptAgencyFields.RptAgencyFldId = syRptAgencyFldValues.RptAgencyFldId AND " & _
           "syRptAgencies.Descrip LIKE '" & AgencyName & "' AND " & _
           "syRptFields.Descrip LIKE '" & FldName & "' " & _
           "ORDER BY syRptAgencyFldValues.RptAgencyFldValId"

        With New DataAccess
            .ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            .OpenConnection()
            Return .RunSQLDataSet(strSQL).Tables(0).Copy
        End With

    End Function

    Public Function GetAcademicType(ByVal StuEnrollIdList As String) As String
        Dim AcademictType As String

        Dim db As New SQLDataAccess

        Using cn As New SqlConnection(GetAdvAppSettings.AppSettings("ConnectionString"))
            Using cmd As New SqlCommand("dbo.Usp_TR_GetAcademicType")
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add(New SqlParameter("@StuEnrollIdList", SqlDbType.VarChar))
                cmd.Parameters("@StuEnrollIdList").Value = StuEnrollIdList.ToString()

                Dim ReturnParameter As SqlParameter = New SqlParameter("@AcademicType", SqlDbType.NVarChar, 50)
                ReturnParameter.Direction = ParameterDirection.Output
                cmd.Parameters.Add(ReturnParameter)


                cn.Open()
                cmd.Connection = cn
                cmd.ExecuteNonQuery()

                Dim intSysCompTypeId As Integer
                AcademictType = cmd.Parameters("@AcademicType").Value.ToString()
                cmd.Dispose()
                cn.Dispose()

                Return AcademictType
            End Using
        End Using

    End Function

    Public Sub DeleteReportSetting(ByVal strGuid As String)

        Dim db As New SQLDataAccess
        Using cn As New SqlConnection(GetAdvAppSettings.AppSettings("ConnectionString"))
            Dim sb As New StringBuilder
            sb.Append("DELETE syReportUserSettings WHERE UserSettingId = @strGuid ")
            ' Add empId to the parameter list
            db.AddParameter("@strGuid", New Guid(strGuid), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            cn.Open()
            db.RunParamSQLExecuteNoneQuery(sb.ToString())
            cn.Dispose()
        End Using

    End Sub

End Class
