Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' LetterTemplatesDB.vb
'
' LetterTemplatesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class LetterTemplatesDB
    Public Function GetAllLetterTemplates(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("	      (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("	      LT.LetterTemplateId, ")
            .Append("	      LT.StatusId, ")
            .Append("	      LT.LetterTemplateCode, ")
            .Append("	      LT.LetterTemplateContent ")
            .Append("FROM     syLetterTemplates LT, syStatuses ST ")
            .Append("WHERE    LT.StatusId = ST.StatusId ")
            .Append("AND      LT.CampGrpId In (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("ORDER BY ")
            .Append("         ST.Status, ")
            .Append("         LT.LetterTemplateCode asc ")
        End With

        ' Add the campusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetLetterTemplateInfo(ByVal LetterTemplateId As String) As LetterTemplateInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT LT.LetterTemplateId, ")
            .Append("    LT.LetterTemplateCode, ")
            .Append("    LT.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=LT.StatusId) As Status, ")
            .Append("    LT.LetterTemplateContent, ")
            .Append("    LT.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=LT.CampGrpId) As CampGrpDescrip, ")
            .Append("    LT.ModUser, ")
            .Append("    LT.ModDate ")
            .Append("FROM  syLetterTemplates LT ")
            .Append("WHERE LT.LetterTemplateId= ? ")
        End With

        ' Add the LetterTemplateId to the parameter list
        db.AddParameter("@LetterTemplateId", LetterTemplateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim LetterTemplateInfo As New LetterTemplateInfo

        While dr.Read()

            '   set properties with data from DataReader
            With LetterTemplateInfo
                .LetterTemplateId = LetterTemplateId
                .IsInDB = True
                .Code = dr("LetterTemplateCode")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Content = dr("LetterTemplateContent")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return LetterTemplateInfo
        Return LetterTemplateInfo

    End Function
    Public Function UpdateLetterTemplateInfo(ByVal LetterTemplateInfo As LetterTemplateInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syLetterTemplates Set LetterTemplateId = ?, LetterTemplateCode = ?, ")
                .Append(" StatusId = ?, LetterTemplateContent = ?, CampGrpId = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE LetterTemplateId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from syLetterTemplates where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   LetterTemplateId
            db.AddParameter("@LetterTemplateId", LetterTemplateInfo.LetterTemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   EnrollmentId
            db.AddParameter("@LetterTemplateCode", LetterTemplateInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", LetterTemplateInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   LetterTemplateContent
            db.AddParameter("@LetterTemplateContent", LetterTemplateInfo.Content, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   CampGrpId
            If LetterTemplateInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", LetterTemplateInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   LetterTemplateId
            db.AddParameter("@LetterTemplateId", LetterTemplateInfo.LetterTemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", LetterTemplateInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If


        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)


        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddLetterTemplateInfo(ByVal LetterTemplateInfo As LetterTemplateInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT syLetterTemplates (LetterTemplateId, LetterTemplateCode, StatusId, ")
                .Append("   LetterTemplateContent, CampGrpId, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   LetterTemplateId
            db.AddParameter("@LetterTemplateId", LetterTemplateInfo.LetterTemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   LetterTemplateCode
            db.AddParameter("@LetterTemplateCode", LetterTemplateInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", LetterTemplateInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   LetterTemplateContent
            db.AddParameter("@LetterTemplateContent", LetterTemplateInfo.Content, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   CampGrpId
            If LetterTemplateInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", LetterTemplateInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteLetterTemplateInfo(ByVal LetterTemplateId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syLetterTemplates ")
                .Append("WHERE LetterTemplateId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM syLetterTemplates WHERE LetterTemplateId = ? ")
            End With

            '   add parameters values to the query

            '   LetterTemplateId
            db.AddParameter("@LetterTemplateId", LetterTemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   LetterTemplateId
            db.AddParameter("@LetterTemplateId", LetterTemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If


        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
End Class
