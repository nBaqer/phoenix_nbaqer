Imports System.Data.OleDb
Imports System.Data
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.AdvantageV1.Common
Imports FAME.Advantage.Common

Public Class TerminateStudent

    ''' <summary>
    ''' Get Catalog Drop Status
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' 
    Public Function GetDropStatuses() As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        With sb
            .Append("SELECT distinct a.StatusCodeId, a.StatusCode, a.StatusCodeDescrip ")
            .Append("FROM syStatusCodes a, sySysStatus b ")
            .Append("WHERE a.SysStatusId in (12) order by a.StatusCodeDescrip ")
        End With

        db.OpenConnection()
        Try
            ds = db.RunSQLDataSet(sb.ToString) '   return dataset
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try

        Return ds
    End Function

    ''' <summary>
    ''' Get the drop reason Catalog to fill combo-box
    ''' </summary>
    ''' <param name="campusid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' 
    Public Function GetDropReasons(ByVal campusid As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        With sb

            .Append("SELECT DropReasonId,Descrip,(Case S.Status when 'Active' then 1 else 0 end) As Status ")
            .Append("FROM arDropReasons T, syStatuses S ")
            .Append("WHERE T.StatusId=S.StatusId AND S.Status='Active' AND CampGrpId IN ")
            .Append("(SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId='" & campusid & "')")
            .Append("ORDER BY Status, Descrip")
        End With


        db.OpenConnection()
        ds = db.RunSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Public Function GetStudentStatus(ByVal stuEnrollId As String) As StateEnum
        Dim status As Integer
        Dim enumType As StateEnum

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        Try
            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder


            '   build the sql query
            With sb
                .Append("SELECT b.SysStatusId FROM syStatusCodes b,arStuEnrollments a ")
                .Append(" WHERE a.StatusCodeId = b.StatusCodeId and  ")
                .Append(" a.StuEnrollId = ? ")
            End With


            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                status = dr("SysStatusId")
            End While

            enumType = CType(status, StateEnum)

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        Return enumType

    End Function

    ''' <summary>
    ''' Return the Current Student Status Id from table ArStuEnrollment
    ''' </summary>
    ''' <param name="stuEnrollId">Enrollment Id</param>
    ''' <returns>The Guid of current status as String</returns>
    ''' <remarks>Use ArStuEnrollment</remarks>
    Public Function GetStudentStatusId(ByVal stuEnrollId As String) As String
        
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim db As New DataAccess

        Try
            '   connect to the database
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT cod.StatusCodeId FROM dbo.syStatusCodes cod ")
                .Append(" JOIN dbo.arStuEnrollments ON arStuEnrollments.StatusCodeId = cod.StatusCodeId ")
                .Append(" WHERE StuEnrollId = ? ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StdId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Dim id = db.RunParamSQLScalar(sb.ToString())
            If (id Is Nothing) Then
                Return Nothing
            Else
                Return id.ToString()
            End If
         Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try
    End Function
 
    ''' <summary>
    ''' Terminate Student this should be used always to terminate a student (drop it)
    ''' </summary>
    ''' <param name="change"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    'Public Function TerminateStudent(change As StudentChangeHistoryObj)
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim count As Integer

    '    Dim myAdvAppSettings As AdvAppSettings = New AdvAppSettings()


    '    'Set the connection string
    '    db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

    '    'Check if student has multiple enrollments. If so, then don't change the status
    '    'of the student to 'Inactive'(in the arStudent table)
    '    count = DoesStdHaveMultipleEnrollments(change.StuEnrollId.ToString())

    '    '   we must encapsulate all DB updates in one transaction
    '    Dim groupTrans As OleDbTransaction = db.StartTransaction()

    '    Try

    '        'Delete from database and clear the screen as well. 
    '        'UPDATE arStuEnrollment And set the mode to "NEW" since the record has been deleted from the db.
    '        With sb
    '            .Append("UPDATE arStuEnrollments Set StatusCodeId = ?, ")
    '            .Append("LDA=?, ")
    '            .Append("DateDetermined=?, ")
    '            .Append("DropReasonId=?, ")
    '            .Append("ModUser=?, ")
    '            .Append("ModDate=? ")
    '            .Append("WHERE StuEnrollId = ?")
    '        End With
    '        db.AddParameter("@grade", change.NewStatusGuid.ToString(), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        'LDA
    '        db.AddParameter("@LDA", If(change.Lda Is Nothing, DBNull.Value, change.Lda), DataAccess.OleDbDataType.OleDbDateTime, 0, ParameterDirection.Input)

    '        'Date Determined
    '        db.AddParameter("@DateDeter", change.DateOfChange, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        'Drop Reason
    '        If change.DropReasonGuid.ToString() = "" Then
    '            db.AddParameter("@DropReason1", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@DropReason", change.DropReasonGuid.ToString(), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        ''ModUser
    '        db.AddParameter("@ModUser", change.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        ''ModDate
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        db.AddParameter("@stdenrollid", change.StuEnrollId.ToString(), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)

    '        ' INSERT Drop status change in syStudentStatusChange

    '        sb = New StringBuilder()

    '        sb.Append("INSERT INTO syStudentStatusChanges  (StuEnrollId, OrigStatusId, NewStatusId, CampusId, ModDate, ModUser, IsReversal, DropReasonId, DateOfChange, Lda)")
    '        sb.Append("VALUES(?,?,?,?,?,?,?,?,?,?)")

    '        db.AddParameter("@stdenrollid", change.StuEnrollId.ToString(), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@OrigStatusId", change.OrigStatusGuid.ToString(), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@NewStatusId", change.NewStatusGuid.ToString(), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@CampusId", change.CampusId.ToString(), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, 0, ParameterDirection.Input)
    '        db.AddParameter("@ModUser", change.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@IsRevelsal", change.IsRevelsal, DataAccess.OleDbDataType.OleDbInteger, 1, ParameterDirection.Input)
    '        db.AddParameter("@DropReasonId", change.DropReasonGuid.ToString(), DataAccess.OleDbDataType.OleDbString, 0, ParameterDirection.Input)
    '        db.AddParameter("@DateOfChange", change.DateOfChange, DataAccess.OleDbDataType.OleDbDateTime, 0, ParameterDirection.Input)
    '        db.AddParameter("@lda", If(change.Lda Is Nothing, DBNull.Value, change.Lda), DataAccess.OleDbDataType.OleDbDateTime, 0, ParameterDirection.Input)
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString(), groupTrans)

    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)

    '        If count <= 1 Then

    '            With sb
    '                .Append("UPDATE arStudent Set StudentStatus = ?, ModUser = ?, ModDate = ?  ")
    '                .Append("WHERE StudentId in (Select StudentId from arStuEnrollments where StuEnrollId = ?) ")
    '            End With

    '            db.AddParameter("@inactive", AdvantageCommonValues.InactiveGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            ''ModUser
    '            db.AddParameter("@ModUser", change.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '            ''ModDate
    '            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            db.AddParameter("@stdenrollid2", change.StuEnrollId.ToString(), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
    '        End If
    '        '   commit transaction 
    '        groupTrans.Commit()

    '        '   return without errors
    '        Return String.Empty

    '    Catch ex As OleDbException
    '        groupTrans.Rollback()
    '        '   do not report sql lost connection
    '        If Not groupTrans.Connection Is Nothing Then
    '            ' Report the error

    '        End If
    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)
    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    'End Function

    'Public Function DoesStdHaveMultipleEnrollments(ByVal stuEnrollmentId As String) As Integer

    '    Dim rowCount As Integer
    '    Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

    '    Try
    '        '   connect to the database
    '        Dim db As New DataAccess
    '        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
    '        Dim sb As New StringBuilder
    '        With sb

    '            .Append("Select count(*) as Count ")
    '            .Append("from arStuEnrollments a ")
    '            .Append("where a.StudentId in (Select StudentId from arStuEnrollments where StuEnrollId = ?) ")

    '        End With


    '        ' Add the StuEnrollId to the parameter list
    '        db.AddParameter("@cmpid", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.OpenConnection()

    '        'Execute the query
    '        rowCount = CInt(db.RunParamSQLScalar(sb.ToString))

    '        'Close Connection
    '        db.CloseConnection()

    '    Catch ex As Exception

    '        Throw New BaseException(ex.InnerException.ToString)

    '    End Try


    '    'Return the data-table in the dataset
    '    Return rowCount

    'End Function


    Public Sub DropStudentCourses(ByVal stuEnrollId As String, ByVal user As String)
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim clsSectId As String
        'Dim count As Integer

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        'Set the connection string
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        '   build the sql query
        With sb
            .Append("SELECT count(*), A.TestId ")
            .Append("FROM arResults A,arClassSections B,arReqs C ")
            .Append("WHERE A.StuEnrollId = ? ")
            .Append("AND A.TestId = B.ClsSectionId and B.ReqId = C.ReqId ")
            .Append("AND A.GrdSysDetailId IS NULL ")
            .Append("GROUP BY A.TestId ")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@stdenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        db.OpenConnection()
        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read()
            clsSectId = dr("GrdSysDetailId").ToString

            With sb
                .Append("UPDATE arResults Set GrdSysDetailId = ?, ")
                .Append("ModUser=?, ")
                .Append("ModDate=? ")
                .Append("WHERE TestId = ? and StuEnrollId = ?")
            End With

            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@clssectid", clsSectId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@stdenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.OpenConnection()
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    End Sub

 
    'Public Function GetDataGridInfo(ByVal stuEnrollId As String) As DataSet
    '    'Get the Activities DataList
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim da As OleDbDataAdapter
    '    Dim ds As New DataSet

    '    Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

    '    db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

    '    With sb
    '        .Append("SELECT Distinct A.TestId,B.ClsSectionId,C.Code,C.Descrip, ")
    '        .Append("A.StuEnrollId ")
    '        .Append("FROM arResults A,arClassSections B,arReqs C ")
    '        .Append("WHERE A.StuEnrollId = ? ")
    '        .Append("AND A.TestId = B.ClsSectionId and B.ReqId = C.ReqId ")
    '        .Append("AND A.GrdSysDetailId IS NULL ")
    '    End With

    '    ' Add the PrgVerId and ChildId to the parameter list
    '    db.AddParameter("@stuenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


    '    '   Execute the query       
    '    db.OpenConnection()
    '    da = db.RunParamSQLDataAdapter(sb.ToString)
    '    Try
    '        da.Fill(ds, "DropCourse")

    '    Catch ex As Exception

    '    End Try
    '    sb.Remove(0, sb.Length)
    '    db.ClearParameters()

    '    'Close Connection
    '    db.CloseConnection()
    '    Return ds
    'End Function

    Public Function GetDropGradeSystemDetails(ByVal clsSect As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        Try

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            With sb

                .Append("SELECT GrdSysDetailId,Grade ")
                .Append("FROM arGradeSystemDetails ")
                .Append("WHERE IsDrop = 1 ")
                .Append("AND GrdSystemId IN( ")
                .Append("SELECT GrdSystemId ")
                .Append("FROM arGradeScales ")
                .Append("WHERE GrdScaleId IN( ")
                .Append("SELECT GrdScaleId ")
                .Append("FROM arClassSections ")
                .Append("WHERE ClsSectionId = ? )) ")
            End With


            db.AddParameter("@clssectId", clsSect, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "GradeSysDetails")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try
        'Return the data-table in the dataset
        Return ds
    End Function

    ''' <summary>
    ''' Test the Query if some value is null to pass it to DB Null
    ''' </summary>
    ''' <param name="parameters"></param>
    ''' <param name="parameterName"></param>
    ''' <param name="value"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
  
End Class
