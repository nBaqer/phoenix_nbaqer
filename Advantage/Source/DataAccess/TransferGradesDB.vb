Imports System.Data.OleDb
Imports System.Data
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common.Tables


Public Class TransferGradesDB
    Public Function GetDataGridInfo(ByVal stuEnrollId As String, ByVal campusId As String) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim da As OleDbDataAdapter
        Dim ds As New DataSet

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("Select Distinct A.ReqId, A.Code, A.Descrip, C.StuEnrollId from ")
            .Append("arReqs A, arProgVerDef B, arStuEnrollments C ")
            .Append("where C.StuEnrollId = ? and C.PrgVerId = B.PrgVerId ")
            .Append("and B.ReqId = A.ReqId ")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@stuenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query       
        db.OpenConnection()
        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "TransferGrade")

        Catch ex As Exception

        End Try
        sb.Remove(0, sb.Length)
        db.ClearParameters()

        'Close Connection
        db.CloseConnection()
        Return ds
    End Function

    Public Function GetCurrentTerms() As DataSet

        'connect to the database
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim da As OleDbDataAdapter
        Dim ds As New DataSet

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Try
            With sb

                .Append("SELECT t1.TermId,t1.TermDescrip ")
                .Append("FROM arTerm t1,syStatuses t2  ")
                .Append("WHERE t1.StartDate <= ? and t1.EndDate >= ? ")
                .Append("and t2.Status = 'Active' and t1.StatusId = t2.StatusId ")
            End With

            '   Execute the query
            db.AddParameter("@edate1", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@edate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "StdTerms")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds


    End Function

    Public Function GetGradeSystemDetails(ByVal stuEnrollId As String, Optional ByVal campusid As String = "") As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            With sb

                .Append("Select a.Grade, a.GrdSysDetailId from ")
                .Append("arGradeSystemDetails a, arStuEnrollments b,arPrgVersions c ")
                .Append("where b.StuEnrollId = ? and b.PrgVerId = c.PrgVerId and ")
                .Append("c.GrdSystemId = a.GrdSystemId and  a.GrdSysDetailId not in (Select GrdSysDetailId from arGradeSystemDetails ")
                .Append("where IsDrop = 1 or IsPass = 0 or IsDefault = 1 or IsIncomplete = 1 or IsUnsatisfactory = 1) ")
                If myAdvAppSettings.AppSettings("GradesFormat", campusid).ToString.ToLower = "numeric" Then
                    .Append(" and a.IsTransferGrade=1 ")
                End If
                .Append("ORDER BY a.Grade")

            End With

            db.AddParameter("@stuenrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "GradeSysDetails")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try
        'Return the datatable in the dataset
        Return ds

    End Function



    Public Function UpdateTransferGrade(ByVal finalGrdObj As FinalGradeInfo, ByVal user As String, Optional ByVal campusid As String = "") As String
        Dim grade As String
        Dim score As Decimal
        Dim iscompleted As Boolean

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        grade = finalGrdObj.Grade
        score = finalGrdObj.Score

        If grade Is Nothing And score = 0 Then
            iscompleted = False
        Else
            iscompleted = True
        End If

        Try

            Dim myDataSet As New DataSet()
            Using myConnection As New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
                Using myCommand As New SqlCommand("USP_AR_SetTransferGradesCourseComplete", myConnection)
                    myCommand.CommandTimeout = 7200
                    myCommand.CommandType = CommandType.StoredProcedure
                    If myAdvAppSettings.AppSettings("GradesFormat", campusid).ToString.ToLower = "numeric" Then

                        If grade <> "" Then
                            Dim myGuid As Guid = Guid.NewGuid
                            myCommand.Parameters.Add(New SqlParameter("@StuEnrollId", SqlDbType.UniqueIdentifier)).Value = New Guid(finalGrdObj.StuEnrollId)
                            myCommand.Parameters.Add(New SqlParameter("@TransferID", SqlDbType.UniqueIdentifier)).Value = myGuid
                            myCommand.Parameters.Add(New SqlParameter("@ReqId", SqlDbType.UniqueIdentifier)).Value = New Guid(finalGrdObj.ReqId)
                            myCommand.Parameters.Add(New SqlParameter("@GrdSysDetailId", SqlDbType.UniqueIdentifier)).Value = New Guid(finalGrdObj.Grade)
                            myCommand.Parameters.Add(New SqlParameter("@Score", SqlDbType.Decimal)).Value = finalGrdObj.Score
                            myCommand.Parameters.Add(New SqlParameter("@TermId", SqlDbType.UniqueIdentifier)).Value = New Guid(finalGrdObj.TermId)
                            myCommand.Parameters.Add(New SqlParameter("@ModUser", SqlDbType.VarChar)).Value = New String(user)
                            myCommand.Parameters.Add(New SqlParameter("@ModDate", SqlDbType.DateTime)).Value = finalGrdObj.ModDate

                            If iscompleted Then
                                myCommand.Parameters.Add(New SqlParameter("@CompletedDate", SqlDbType.DateTime)).Value = finalGrdObj.ModDate
                            Else
                                myCommand.Parameters.Add(New SqlParameter("@CompletedDate", SqlDbType.DateTime)).Value = DBNull.Value
                            End If
                            myCommand.Parameters.Add(New SqlParameter("@IsCourseCompleted", SqlDbType.Bit)).Value = iscompleted
                            myCommand.Parameters.Add(New SqlParameter("@IsGradeOverridden", SqlDbType.Bit)).Value = 0
                            myCommand.Parameters.Add(New SqlParameter("@isTransferred", SqlDbType.Bit)).Value = 0

                        Else
                            Dim myGuid As Guid = Guid.NewGuid
                            myCommand.Parameters.Add(New SqlParameter("@StuEnrollId", SqlDbType.UniqueIdentifier)).Value = New Guid(finalGrdObj.StuEnrollId)
                            myCommand.Parameters.Add(New SqlParameter("@TransferID", SqlDbType.UniqueIdentifier)).Value = myGuid
                            myCommand.Parameters.Add(New SqlParameter("@ReqId", SqlDbType.UniqueIdentifier)).Value = New Guid(finalGrdObj.ReqId)
                            myCommand.Parameters.Add(New SqlParameter("@GrdSysDetailId", SqlDbType.UniqueIdentifier)).Value = DBNull.Value
                            myCommand.Parameters.Add(New SqlParameter("@Score", SqlDbType.Decimal)).Value = finalGrdObj.Score
                            myCommand.Parameters.Add(New SqlParameter("@TermId", SqlDbType.UniqueIdentifier)).Value = New Guid(finalGrdObj.TermId)
                            myCommand.Parameters.Add(New SqlParameter("@ModUser", SqlDbType.VarChar)).Value = New String(user)
                            myCommand.Parameters.Add(New SqlParameter("@ModDate", SqlDbType.DateTime)).Value = finalGrdObj.ModDate

                            If iscompleted Then
                                myCommand.Parameters.Add(New SqlParameter("@CompletedDate", SqlDbType.DateTime)).Value = finalGrdObj.ModDate
                            Else
                                myCommand.Parameters.Add(New SqlParameter("@CompletedDate", SqlDbType.DateTime)).Value = DBNull.Value
                            End If
                            myCommand.Parameters.Add(New SqlParameter("@IsCourseCompleted", SqlDbType.Bit)).Value = iscompleted
                            myCommand.Parameters.Add(New SqlParameter("@IsGradeOverridden", SqlDbType.Bit)).Value = 0
                            myCommand.Parameters.Add(New SqlParameter("@isTransferred", SqlDbType.Bit)).Value = 0
                        End If
                    Else
                        Dim myGuid As Guid = Guid.NewGuid
                        myCommand.Parameters.Add(New SqlParameter("@StuEnrollId", SqlDbType.UniqueIdentifier)).Value = New Guid(finalGrdObj.StuEnrollId)
                        myCommand.Parameters.Add(New SqlParameter("@TransferID", SqlDbType.UniqueIdentifier)).Value = myGuid
                        myCommand.Parameters.Add(New SqlParameter("@ReqId", SqlDbType.UniqueIdentifier)).Value = New Guid(finalGrdObj.ReqId)
                        myCommand.Parameters.Add(New SqlParameter("@GrdSysDetailId", SqlDbType.UniqueIdentifier)).Value = New Guid(finalGrdObj.Grade)
                        myCommand.Parameters.Add(New SqlParameter("@Score", SqlDbType.Decimal)).Value = DBNull.Value
                        myCommand.Parameters.Add(New SqlParameter("@TermId", SqlDbType.UniqueIdentifier)).Value = New Guid(finalGrdObj.TermId)
                        myCommand.Parameters.Add(New SqlParameter("@ModUser", SqlDbType.VarChar)).Value = New String(user)
                        myCommand.Parameters.Add(New SqlParameter("@ModDate", SqlDbType.DateTime)).Value = finalGrdObj.ModDate
                        If iscompleted Then
                            myCommand.Parameters.Add(New SqlParameter("@CompletedDate", SqlDbType.DateTime)).Value = finalGrdObj.ModDate
                        Else
                            myCommand.Parameters.Add(New SqlParameter("@CompletedDate", SqlDbType.DateTime)).Value = DBNull.Value
                        End If
                        myCommand.Parameters.Add(New SqlParameter("@IsCourseCompleted", SqlDbType.Bit)).Value = iscompleted
                        myCommand.Parameters.Add(New SqlParameter("@IsGradeOverridden", SqlDbType.Bit)).Value = 0
                        myCommand.Parameters.Add(New SqlParameter("@isTransferred", SqlDbType.Bit)).Value = 0
                    End If
                    Dim mySqlDataAdapter As New SqlDataAdapter(myCommand)
                    mySqlDataAdapter.Fill(myDataSet)
                    myCommand.Connection.Close()
                End Using
            End Using
            Return ""
        Catch ex As Exception
            If Not IsNothing(ex.InnerException) Then
                Return ex.InnerException.Message.ToString
            Else
                Return ex.Message.ToString
            End If
        Finally

        End Try
    End Function



    ''' <summary>
    ''' Set transfer for all enrollments.
    ''' </summary>
    ''' <param name="arTransferGradeses">List of enrollments to be transferred</param>
    Public Sub SetTransferTable(ByVal arTransferGradeses As IList(Of ArTransferGrades))
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        'Create the support for the query operation
        Dim conn As SqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim sqlTrans As SqlTransaction

        'Create list of command to be executed in Transaction
        Dim commandList = (From grades In arTransferGradeses Select PrepareCommandForTransferTable(grades, conn)).ToList()

        'Prepare Transactions
        conn.Open()
        sqlTrans = conn.BeginTransaction("Transfer")
        Try
            'Prepare transactions
            For Each comm As SqlCommand In commandList
                comm.Transaction = sqlTrans
            Next

            ' Execute commands
            For Each command As SqlCommand In commandList
                command.ExecuteNonQuery()
            Next

            sqlTrans.Commit()

        Catch ex As Exception
            sqlTrans.Rollback()
            Throw
        Finally
            conn.Close()
        End Try
    End Sub

    Private Function PrepareCommandForTransferTable(ByVal tran As ArTransferGrades, ByVal connection As SqlConnection) As SqlCommand
        Dim command As SqlCommand = New SqlCommand()
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = "USP_AR_SetTransferGradesCourseComplete"
        command.Connection = connection

        'Transfer parameters
        command.Parameters.AddWithValue("@StuEnrollId", tran.EnrollmentId)
        command.Parameters.AddWithValue("@TransferID", Guid.NewGuid)
        command.Parameters.AddWithValue("@ReqId", tran.ReqId)
        command.Parameters.AddWithValue("@GrdSysDetailId", If(tran.GrdSysDetailId Is Nothing Or tran.GrdSysDetailId = "", DBNull.Value, tran.GrdSysDetailId))
        command.Parameters.AddWithValue("@Score", tran.Score)
        command.Parameters.AddWithValue("@TermId", tran.TermId)
        command.Parameters.AddWithValue("@ModUser", tran.ModUser)
        command.Parameters.AddWithValue("@ModDate", tran.ModDate)
        command.Parameters.AddWithValue("@completeddate", DateTime.Now) 'Not used in the stored procedure, but it need it :-(
        command.Parameters.AddWithValue("@IsCourseCompleted", tran.IsCourseCompleted)
        command.Parameters.AddWithValue("@IsGradeOverridden", tran.IsGradeOverriden)
        command.Parameters.AddWithValue("@isTransferred", tran.IsTransferred)

        Return command
    End Function

    Public Function GetStudentEnrollments(ByVal studentId As String, ByVal campusId As String) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim da As OleDbDataAdapter
        Dim ds As New DataSet

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()


        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("Select Distinct A.StuEnrollId, B.PrgVerDescrip from ")
            .Append("arStuEnrollments A, arPrgVersions B ")
            .Append("where A.StudentId in (Select StudentId from arStuEnrollments where StuEnrollId = ?)  ")
            .Append("and A.PrgVerId = B.PrgVerId ")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@stuenrollid", studentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query       
        db.OpenConnection()
        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "StdEnrollments")

        Catch ex As Exception

        End Try
        sb.Remove(0, sb.Length)
        db.ClearParameters()

        'Close Connection
        db.CloseConnection()
        Return ds
    End Function

    Public Function TransferGradeFromOneEnrollmentToAnother(ByVal finalGrdObj As FinalGradeInfo, ByVal user As String, ByVal newEnrollId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim testId As String
        Dim stuEnrollId As String

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()


        stuEnrollId = finalGrdObj.StuEnrollId
        testId = finalGrdObj.ClsSectId

        'Set the connection string
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Try

            With sb
                .Append("insert into arResults ")
                .Append("(TestId,Score,GrdSysDetailId,Cnt,Hours,StuEnrollId, ")
                .Append("IsInComplete,DroppedInAddDrop,ModUser,ModDate) ")
                .Append("select ?,Score,GrdSysDetailId,Cnt,Hours,?, ")
                .Append("IsInComplete,DroppedInAddDrop,?,? ")
                .Append("from arResults ")
                .Append("where StuEnrollId = ? and TestId = ? ")
            End With

            db.AddParameter("TestID", testId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@newenrollid", newEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.AddParameter("@oldStuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.AddParameter("TestID2", testId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function GetRequirementTransferredRecord(stuEnrollId As String, reqId As String) As ArTransferGrades
        Dim sb = New StringBuilder()
        sb.Append(" SELECT  StuEnrollId ")
        sb.Append("       ,tg.ReqId ")
        sb.Append("       ,tg.GrdSysDetailId ")
        sb.Append("       ,tg.TermId ")
        sb.Append("       ,Score ")
        sb.Append("       ,IsTransferred ")
        sb.Append("       ,isClinicsSatisfied ")
        sb.Append("       ,IsCourseCompleted ")
        sb.Append("       ,IsGradeOverridden ")
        sb.Append("	   ,r.Code ")
        sb.Append("	   ,det.Grade  ")
        sb.Append("	   ,term.TermCode ")
        sb.Append("	   ,term.TermDescrip ")
        sb.Append("	   ,tg.ModUser ")
        sb.Append("	   ,tg.ModDate ")
        sb.Append(" FROM    dbo.arTransferGrades tg ")
        sb.Append(" JOIN    dbo.arReqs r ON r.ReqId = tg.ReqId ")
        sb.Append(" JOIN    dbo.arGradeSystemDetails det ON det.GrdSysDetailId = tg.GrdSysDetailId ")
        sb.Append(" JOIN    dbo.arTerm term ON term.TermId = tg.TermId ")
        sb.Append(" WHERE   StuEnrollId = @StuEnrollId ")
        sb.Append("        AND tg.ReqId = @ReqID ")

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim conn As SqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim command = New SqlCommand(sb.ToString(), conn)
        command.Parameters.AddWithValue("@StuEnrollId", stuEnrollId)
        command.Parameters.AddWithValue("@ReqId", reqId)
        conn.Open()
        Try
            Dim tr = New ArTransferGrades()
            tr.EnrollmentId = Guid.Empty.ToString()

            Dim reader = command.ExecuteReader()
            While reader.Read()
                tr.EnrollmentId = reader("StuEnrollId").ToString()
                tr.ReqId = reader("ReqId").ToString()
                tr.GrdSysDetailId = reader("GrdSysDetailId").ToString()
                tr.TermId = reader("TermId").ToString()
                Dim score = reader("Score")
                tr.Score = If((TypeOf score Is DBNull), Nothing, CDec(score))
                Dim istrans = reader("IsTransferred")
                tr.IsTransferred = (Not (TypeOf istrans Is DBNull) AndAlso CBool(istrans))
                Dim isclinic = reader("isClinicsSatisfied")
                tr.IsClinicsSatisfied = (Not (TypeOf isclinic Is DBNull) AndAlso CBool(isclinic))
                tr.IsCourseCompleted = CBool(reader("IsCourseCompleted"))
                tr.IsGradeOverriden = CBool(reader("IsGradeOverridden"))
                tr.CourseCode = reader("Code").ToString()
                tr.Grade = reader("Grade").ToString()
                tr.TermCode = reader("TermCode").ToString()
                tr.TermDescription = reader("TermDescrip").ToString()
                tr.ModUser = reader("ModUser").ToString()
                tr.ModDate = DirectCast(reader("ModDate"), DateTime)
            End While

            Return tr
        Finally
            conn.Close()
        End Try
    End Function

    ''' <summary>
    ''' Test if the enrollment was transferred for the requirement.
    ''' </summary>
    ''' <param name="stuEnrollId">Enroll to be tested</param>
    ''' <param name="reqId">given requirement </param>
    ''' <returns>true if was transferred false if not</returns>
    Public Function IsEnrollmentTransferredForTheReq(stuEnrollId As String, reqId As String) As Boolean
        Dim sb = New StringBuilder()
        sb.Append(" SELECT  COUNT(*) ")
        sb.Append(" FROM    dbo.arTransferGrades  ")
        sb.Append(" WHERE   StuEnrollId = @StuEnrollId ")
        sb.Append("         AND ReqId = @ReqID ")

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim conn As SqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim command = New SqlCommand(sb.ToString(), conn)
        command.Parameters.AddWithValue("@StuEnrollId", stuEnrollId)
        command.Parameters.AddWithValue("@ReqId", reqId)
        conn.Open()
        Try
            Dim val As Integer = command.ExecuteScalar()
            Return (val > 0)

        Finally
            conn.Close()
        End Try
    End Function



End Class
