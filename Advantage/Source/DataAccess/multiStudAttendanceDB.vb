Imports System.Data
Imports FAME.Advantage.Common


Public Class multiStudAttendanceDB

#Region "Private Data Members"
    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")
    Public m_AsOfDate As DateTime = Convert.ToDateTime(Date.Now)

#End Region


#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

    Public ReadOnly Property EndDate() As DateTime
        Get
            Return m_AsOfDate
        End Get
    End Property

    Public Function GetScheduledHoursForStudent(ByVal stuEnrollId As String, ByVal OffSetDate As String) As Integer
        Dim attDB As New ClsSectAttendanceDB
        Dim attDt As New DataTable
        Dim i As Integer = 0
        Dim ScheduledHours As Integer = 0
        attDt = attDB.GetAllClsSectionsForStudent(stuEnrollId)
        If attDt.Rows.Count > 0 Then
            For i = 0 To attDt.Rows.Count - 1
                ScheduledHours = ScheduledHours + GetCollectionOfMeetingDates(attDt.Rows(i)("ClsSectionId").ToString, stuEnrollId, OffSetDate)
            Next

        End If
        Return ScheduledHours

    End Function

    Public Function GetCompletedHoursForStudent(ByVal stuEnrollId As String, ByVal OffSetDate As String) As Integer
        Dim attDB As New ClsSectAttendanceDB
        Dim attDt As New DataTable
        Dim i As Integer = 0
        Dim CompletedHours As Integer = 0
        attDt = attDB.GetAllClsSectionsForStudent(stuEnrollId)
        If attDt.Rows.Count > 0 Then
            For i = 0 To attDt.Rows.Count - 1
                CompletedHours = CompletedHours + GetCollectionOfCompletedDates(attDt.Rows(i)("ClsSectionId").ToString, stuEnrollId, OffSetDate)
            Next

        End If
        Return CompletedHours

    End Function

    Public Function GetCollectionOfMeetingDates(ByVal clsSectionId As String, ByVal stuEnrollId As String, ByVal OffSetDate As String) As Integer

        'get collection of holidays
        Dim collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday) = (New HolidaysDB).GetCollectionOfHolidayDates()
        Dim totalDuration As Integer = 0
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("       0 as Type, ")
            .Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            .Append("		CSM.StartDate, ")
            .Append("       CSM.EndDate, ")
            '.Append("		P.MeetDays, ")
            .Append("       (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("		(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime ")
            .Append("FROM	arClassSections CS, arClsSectMeetings CSM, syPeriods P ")
            .Append("WHERE ")
            .Append("       CS.ClsSectionId = CSM.ClsSectionId ")
            .Append("AND	CSM.PeriodId=P.PeriodId ")
            .Append("AND    CS.ClsSectionId = ? ")
            .Append(" and  CSM.StartDate>=(select startDate from  arStuEnrollments where StuEnrollId= ? ) ")
            .Append(" and CSM.EndDate<= ? ")
            .Append("UNION ALL ")
            .Append("SELECT ")
            .Append("       1 as Type, ")
            .Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            .Append("		CSM.StartDate, ")
            .Append("       CSM.EndDate, ")
            '.Append("		P.MeetDays, ")
            .Append("       (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("		(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime ")
            .Append("FROM	arClassSections CS, arClsSectMeetings CSM, syPeriods P ")
            .Append("WHERE ")
            .Append("       CS.ClsSectionId = CSM.ClsSectionId ")
            .Append("AND	CSM.AltPeriodId=P.PeriodId ")
            .Append("AND    CS.ClsSectionId = ? ")
            .Append(" and  CSM.StartDate>=(select startDate from  arStuEnrollments where StuEnrollId= ? ) ")
            .Append(" and CSM.EndDate<= ? ")
        End With

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@EndDate", Date.Now, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@EndDate", Date.Now, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        'this is the collection 
        'Dim collectionOfClassSectionMeetings As System.Collections.Generic.List(Of AdvantageClassSectionMeeting) = New System.Collections.Generic.List(Of AdvantageClassSectionMeeting)

        While dr.Read()

            'set the type of week to process
            Dim selectWeek As Integer = 0 'all weeks
            If Not dr("IsAltPeriod") Is System.DBNull.Value Then
                If dr("Type") = 0 Then
                    selectWeek = 1  'even weeks
                Else
                    selectWeek = 2  'odd weeks
                End If
            End If

            'generate list of meeting dates
            Dim listOfDates As System.Collections.Generic.List(Of Date) = AdvantageCommonValues.GetCollectionOfDates(dr("StartDate"), dr("EndDate"), dr("MeetDays"), selectWeek)

            For Each [date] As Date In listOfDates
                'parse the start time of the meeting
                Dim meetingDateAndTime As DateTime = Date.Parse([date].Year.ToString + "-" + [date].Month.ToString + "-" + [date].Day.ToString + " " + CType(dr("StartTime"), Date).Hour.ToString + ":" + CType(dr("StartTime"), Date).Minute.ToString)

                'calculate the duration of the meeting
                Dim ts As TimeSpan = CType(dr("EndTime"), Date).Subtract(CType(dr("StartTime"), Date))
                Dim duration As Integer = ts.Hours * 60 + ts.Minutes

                'add the Class Section Meeting to the collection
                Dim acsm As AdvantageClassSectionMeeting = New AdvantageClassSectionMeeting(meetingDateAndTime, duration)

                'add it to the collection only if it is not a holiday
                If Not IsHoliday(acsm, collectionOfHolidays) Then
                    totalDuration = totalDuration + duration
                    'collectionOfClassSectionMeetings.Add(acsm)
                End If
            Next
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return collection of AdvantageClassSectionMeetings. Sort it by date
        'collectionOfClassSectionMeetings.Sort()
        Return totalDuration

    End Function

    Public Function GetCollectionOfCompletedDates(ByVal clsSectionId As String, ByVal stuEnrollId As String, ByVal OffSetDate As String) As Integer

        'get collection of holidays
        Dim collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday) = (New HolidaysDB).GetCollectionOfHolidayDates()
        Dim totalDuration As Integer = 0
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("       0 as Type, ")
            .Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            .Append("		CSM.StartDate, ")
            .Append("       CSM.EndDate, ")
            '.Append("		P.MeetDays, ")
            .Append("       (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("		(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime ")
            .Append("FROM	arClassSections CS, arClsSectMeetings CSM, syPeriods P, atclssectattendance atcls ")
            .Append("WHERE ")
            .Append("       CS.ClsSectionId = CSM.ClsSectionId ")
            .Append("AND	CSM.PeriodId=P.PeriodId")
            .Append(" and CS.ClsSectionId = atcls.ClsSectionId ")
            .Append(" and atcls.actual=1 ")
            .Append("AND    CS.ClsSectionId = ? ")
            .Append(" and  CSM.StartDate>=(select startDate from  arStuEnrollments where StuEnrollId= ? ) ")
            .Append(" and CSM.EndDate<= ? ")
            .Append("UNION ALL ")
            .Append("SELECT ")
            .Append("       1 as Type, ")
            .Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            .Append("		CSM.StartDate, ")
            .Append("       CSM.EndDate, ")
            '.Append("		P.MeetDays, ")
            .Append("       (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("		(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime ")
            .Append("FROM	arClassSections CS, arClsSectMeetings CSM, syPeriods P, atclssectattendance atcls ")
            .Append("WHERE ")
            .Append("       CS.ClsSectionId = CSM.ClsSectionId ")
            .Append(" and CS.ClsSectionId = atcls.ClsSectionId ")
            .Append(" and atcls.actual=1 ")
            .Append("AND	CSM.AltPeriodId=P.PeriodId ")
            .Append("AND    CS.ClsSectionId = ? ")
            .Append(" and  CSM.StartDate>=(select startDate from  arStuEnrollments where StuEnrollId= ? ) ")
            .Append(" and CSM.EndDate<= ? ")
        End With

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@EndDate", Date.Now, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@EndDate", Date.Now, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        'this is the collection 
        'Dim collectionOfClassSectionMeetings As System.Collections.Generic.List(Of AdvantageClassSectionMeeting) = New System.Collections.Generic.List(Of AdvantageClassSectionMeeting)

        While dr.Read()

            'set the type of week to process
            Dim selectWeek As Integer = 0 'all weeks
            If Not dr("IsAltPeriod") Is System.DBNull.Value Then
                If dr("Type") = 0 Then
                    selectWeek = 1  'even weeks
                Else
                    selectWeek = 2  'odd weeks
                End If
            End If

            'generate list of meeting dates
            Dim listOfDates As System.Collections.Generic.List(Of Date) = AdvantageCommonValues.GetCollectionOfDates(dr("StartDate"), dr("EndDate"), dr("MeetDays"), selectWeek)

            For Each [date] As Date In listOfDates
                'parse the start time of the meeting
                Dim meetingDateAndTime As DateTime = Date.Parse([date].Year.ToString + "-" + [date].Month.ToString + "-" + [date].Day.ToString + " " + CType(dr("StartTime"), Date).Hour.ToString + ":" + CType(dr("StartTime"), Date).Minute.ToString)

                'calculate the duration of the meeting
                Dim ts As TimeSpan = CType(dr("EndTime"), Date).Subtract(CType(dr("StartTime"), Date))
                Dim duration As Integer = ts.Hours * 60 + ts.Minutes

                'add the Class Section Meeting to the collection
                Dim acsm As AdvantageClassSectionMeeting = New AdvantageClassSectionMeeting(meetingDateAndTime, duration)

                'add it to the collection only if it is not a holiday
                If Not IsHoliday(acsm, collectionOfHolidays) Then
                    totalDuration = totalDuration + duration
                    'collectionOfClassSectionMeetings.Add(acsm)
                End If
            Next
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return collection of AdvantageClassSectionMeetings. Sort it by date
        'collectionOfClassSectionMeetings.Sort()
        Return totalDuration

    End Function


    Private Function IsHoliday(ByVal classSectionMeeting As AdvantageClassSectionMeeting, ByVal collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday)) As Boolean
        'calculate end of class Section meeting
        Dim endOfClassSectionMeeting As DateTime = classSectionMeeting.MeetingDateAndTime.AddMinutes(classSectionMeeting.Duration)

        For Each holiday As AdvantageHoliday In collectionOfHolidays

            'calculate end of the holiday
            Dim endOfHoliday As DateTime = holiday.HolidayDateAndTime.AddMinutes(holiday.Duration)

            'it is not a holiday if the endtime of the class section meeting is earlier than the start time of the holiday OR
            'the start time of the class section meeting is later that the start time of the holiday.
            If (endOfClassSectionMeeting.CompareTo(holiday.HolidayDateAndTime) > -1) And (classSectionMeeting.MeetingDateAndTime.CompareTo(endOfHoliday) < 1) Then Return True
        Next
        'return false
        Return False
    End Function
#End Region


#Region "Public Methods"

    Public Function GetclassSectionAttendance(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strWhere As String
        Dim strOrderBy As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Get ProgVerId and Where Clause from paramInfo.FilterList

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If
        'Get StudentId and rest of Where Clause from paramInfo.FilterOther

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        With sb
            .Append("     select distinct LastName,FirstName, arstudentclockattendance.stuEnrollid from arstudent   , ")
            .Append("      arstuEnrollments ,  arstudentclockattendance, arClassSections , syCampGrps   ")
            .Append("   where  arstudent.studentid = arstuEnrollments.studentid   ")
            .Append("   and arstuEnrollments.stuEnrollid = arstudentclockattendance.stuEnrollid ")
            .Append(strWhere)
            .Append(" order by FirstName asc ")

        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)

        Dim dr2 As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim ds2 As DataSet
        Dim i As Integer = 1

        Try
            Dim dtNames As DataTable = ds.Tables(0)
            If ds.Tables.Count > 0 Then
                dtNames.TableName = "StudentNames"

                Dim dt As New DataTable("MultiStudentAttend")

                'Added By Hepsi for School Logo, Name etc..
                'dt.Columns.Add(New DataColumn("SchoolLogo", System.Type.GetType("System.String")))
                'dt.Columns.Add(New DataColumn("SchoolName", System.Type.GetType("System.String")))
                'dt.Columns.Add(New DataColumn("ShowFilters", System.Type.GetType("System.String")))
                'dt.Columns.Add(New DataColumn("Filters", System.Type.GetType("System.String")))

                dt.Columns.Add(New DataColumn("stuEnrollid", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Date", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Hours", Type.GetType("System.String")))
                'dt.Columns.Add(New DataColumn("Remarks", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("TotalHours", Type.GetType("System.String")))
                ds.Tables.Add(dt)
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        Finally
            If Not dr2.IsClosed Then dr2.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try

        Return ds

    End Function
#End Region

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

End Class