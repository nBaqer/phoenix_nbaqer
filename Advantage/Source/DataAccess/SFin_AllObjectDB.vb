Imports FAME.AdvantageV1.Common.IPEDSCommon
Imports FAME.Advantage.Common

Public Class SFin_AllObjectDB

    Public Shared Function GetReportDatasetRaw(ByVal RptParamInfo As ReportParamInfoIPEDS) As DataSet

        ' get list of students to include, based on report parameters
        Dim StudentList As String = GetStudentListRpt(RptParamInfo)

        ' if there are no students to include, return empty dataset
        If StudentList = "" Then
            Return New DataSet
        End If

        Dim sb As New System.Text.StringBuilder
        Dim dsRaw As New DataSet

        Dim DateFilterSQL As String = _
         "saTransactions.TransDate >= '" & _
         FmtRptDateParam(RptParamInfo.CohortStartDate) & "' AND " & _
         "saTransactions.TransDate <= '" & _
         FmtRptDateParam(RptParamInfo.RptEndDate) & "'" & _
         " and saTransactions.Voided=0 "
        With sb
            ' get info for included students, for use when processing data
            .Append("SELECT DISTINCT arStudent.StudentId, ")

            ' Retrieve Student Identifier
            .Append(IPEDSDB.GetSQL_StudentIdentifier & ", ")
            ' retrieve all other needed columns
            '.Append(" arStudent.FirstName + ' ' + IsNULL(arStudent.MiddleName,'') + ' ' + arStudent.LastName as Name, ")
            .Append(" arStudent.LastName,arStudent.FirstName,arStudent.MiddleName, ")
            .Append("(SELECT COUNT(*) ")
            .Append("	FROM arStuEnrollments ")
            .Append("	WHERE arStuEnrollments.StudentId = arStudent.StudentId) AS NumEnrollments ")
            .Append("FROM ")
            .Append("arStudent ")

            ' append list of appropriate students to include
            .Append("WHERE arStudent.StudentId IN (" & StudentList & ") ")

            .Append("ORDER BY " & IPEDSDB.GetSQL_StudentSort(RptParamInfo))


            .Append(";")


            ' get list of tuition and fees for same list of students
            If RptParamInfo.ShowTuition Or RptParamInfo.ShowFees Then
                '.Append("SELECT arStudent.StudentId, ")
                '' retrieve all other needed columns
                '.Append("syRptAgencyFldValues.AgencyDescrip AS RptTransCodeDescrip, ")
                '.Append("saTransactions.TransAmount, ")
                '.Append("saTransactions.TransDate ")
                '.Append("FROM ")
                '.Append("arStudent, arStuEnrollments, saTransactions, saPayments, saTransCodes, ")
                '.Append("syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")

                '' establish necessary relationships
                '.Append("WHERE ")
                '.Append("arStudent.StudentId = arStuEnrollments.StudentId AND ")
                '.Append("arStuEnrollments.StuEnrollId = saTransactions.StuEnrollId AND ")
                '.Append("saTransactions.TransactionId = saPayments.TransactionId AND ")
                '.Append("saTransactions.TransCodeId = saTransCodes.TransCodeId AND ")
                '.Append("saTransCodes.TransCodeId = syRptAgencySchoolMapping.SchoolDescripId AND ")
                '.Append("syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
                '.Append("syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
                '.Append("syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")

                '' make sure only get records where the Fund Source maps to an IPEDS Fund Source
                '.Append("syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
                '' retrict selection to only those transactions within the report date range
                '.Append(DateFilterSQL & " AND ")
                'If Not RptParamInfo.ShowTuition Then
                '	.Append("syRptAgencyFldValues.AgencyDescrip = 'Fees' ")
                'Else
                '	.Append("syRptAgencyFldValues.AgencyDescrip = 'Tuition' ")
                'End If
                '.Append("AND ")
                '' append list of appropriate students to include
                '.Append("arStudent.StudentId IN (" & StudentList & ")")
                '.Append(";")

                '.Append("   SELECT Distinct arStudent.StudentId,RptTransCodeDescrip, saTransactions.TransAmount,saTransactions.TransDate ")
                '.Append("   FROM ")
                '.Append("   arStudent, arStuEnrollments,  ")
                '.Append("   (       ")
                '.Append("       Select TransactionId,StuEnrollId,TransAmount,TransDate,TransCodeId as TransCodeId ")
                '.Append("       from saTransactions where TransCodeId is not null ")
                '.Append("   union  ")
                '.Append("   Select TransactionId,StuEnrollId,TransAmount,TransDate,'00000000-0000-0000-0000-000000000000' as TransCodeId ")
                '.Append("   from saTransactions where TransCodeId is null ")
                '.Append("   ) saTransactions, saPayments, ")
                '.Append("   (   ")
                '.Append("       select Distinct t1.TransCodeId,t3.AgencyDescrip AS RptTransCodeDescrip  ")
                '.Append("       from ")
                '.Append("       saTransCodes t1,syRptAgencySchoolMapping t2,syRptAgencyFldValues t3,syRptAgencyFields t4,syRptAgencies t5 ")
                '.Append("       where ")
                '.Append("       t1.TransCodeId = t2.SchoolDescripId and t2.RptAgencyFldValId = t3.RptAgencyFldValId and ")
                '.Append("       t3.RptAgencyFldId = t4.RptAgencyFldId and t4.RptAgencyId = t5.RptAgencyId and  ")
                '.Append("       t5.Descrip like '" & AgencyName & "' ")
                'If Not RptParamInfo.ShowTuition Then
                '    .Append(" AND t3.AgencyDescrip = 'Fees' ")
                'Else
                '    .Append("AND t3.AgencyDescrip = 'Tuition' ")
                'End If
                '.Append("       Union ")
                '.Append("       select '00000000-0000-0000-0000-000000000000' as TransCodeId,'None' as RptTransCodeDescrip ")
                '.Append("       from saTransCodes  ")
                '.Append("   ) saTransCodes         ")
                '.Append("   WHERE arStudent.StudentId = arStuEnrollments.StudentId AND arStuEnrollments.StuEnrollId = saTransactions.StuEnrollId AND ")
                '.Append("   saTransactions.TransactionId = saPayments.TransactionId AND saTransactions.TransCodeId = saTransCodes.TransCodeId and ")
                '.Append(DateFilterSQL & " AND ")
                '.Append("arStudent.StudentId IN (" & StudentList & ")")
                '.Append(";")

                .Append("   select StudentId,RptTransCodeDescrip,TransAmount,TransDate  ")
                .Append("   from  ")
                .Append("       (   ")
                .Append("           select saTransactions.TransactionId,saTransactions.StuEnrollId,t2.StudentId,saTransactions.TransCodeId,saTransactions.TransDate,saTransactions.TransAmount,Voided from  ")
                .Append("           saTransactions,arStuEnrollments t2 ")
                .Append("           where saTransactions.StuEnrollId = t2.StuEnrollId and t2.StudentId IN (" & StudentList & ") ")
                .Append("           AND saTransactions.Voided=0 ")
                .Append("           AND ")
                .Append(DateFilterSQL)
                .Append(" ) saTransactions, ")
                .Append("   ( ")
                .Append("       select Distinct t1.TransCodeId,t3.AgencyDescrip AS RptTransCodeDescrip   ")
                .Append("       from ")
                .Append("       saTransCodes t1,syRptAgencySchoolMapping t2,syRptAgencyFldValues t3,syRptAgencyFields t4,syRptAgencies t5   ")
                .Append("       where  t1.TransCodeId = t2.SchoolDescripId and t2.RptAgencyFldValId = t3.RptAgencyFldValId and  ")
                .Append("       t3.RptAgencyFldId = t4.RptAgencyFldId and t4.RptAgencyId = t5.RptAgencyId and  ")
                .Append("       t5.Descrip like '" & AgencyName & "' ")
                If Not RptParamInfo.ShowTuition Then
                    .Append(" AND t3.AgencyDescrip = 'Fees' ")
                Else
                    .Append(" AND t3.AgencyDescrip = 'Tuition' ")
                End If
                .Append("   ) saTransCodes  ")
                .Append("   where ")
                .Append("   saTransactions.TransCodeId = saTransCodes.TransCodeId ")
                .Append("and saTransactions.Voided=0 ")
            End If


            ' get list of awards for same list of students
            If RptParamInfo.FilterFundSourceDescrips <> "" Then
                .Append("SELECT arStudent.StudentId, ")

                ' retrieve all other needed columns
                .Append("syRptAgencyFldValues.AgencyDescrip AS RptFundSourceDescrip, ")
                .Append("faStudentAwardSchedule.Amount, saTransactions.TransDate ")
                .Append("FROM ")
                .Append("arStudent, arStuEnrollments, faStudentAwards, faStudentAwardSchedule, ")
                .Append("saPmtDisbRel, saTransactions, saFundSources, ")
                .Append("syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")

                ' establish necessary relationships
                .Append("WHERE ")
                .Append("arStudent.StudentId = arStuEnrollments.StudentId AND ")
                .Append("arStuEnrollments.StuEnrollId = faStudentAwards.StuEnrollId AND ")
                .Append("faStudentAwards.StudentAwardId = faStudentAwardSchedule.StudentAwardId AND ")
                .Append("faStudentAwardSchedule.AwardScheduleId = saPmtDisbRel.AwardScheduleId AND ")
                .Append("saPmtDisbRel.TransactionId = saTransactions.TransactionId AND ")
                .Append("faStudentAwards.AwardTypeId = saFundSources.FundSourceId AND ")
                .Append("saFundSources.FundSourceId = syRptAgencySchoolMapping.SchoolDescripId AND ")
                .Append("syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
                .Append("syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
                .Append("syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")

                ' make sure only get records where the Fund Source maps to an IPEDS Fund Source
                .Append("syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")

                ' retrict selection to only those transactions within the report date range
                .Append(DateFilterSQL & " AND ")

                .Append("syRptAgencyFldValues.AgencyDescrip IN (" & PutDBQuotesList(RptParamInfo.FilterFundSourceDescrips) & ") AND ")

                ' append list of appropriate students to include
                .Append("arStudent.StudentId IN (" & StudentList & ")")
                .Append(" and saTransactions.Voided=0 ")
                .Append(";")
            End If

            ' get list of refunds for same list of students
            If RptParamInfo.FilterFundSourceDescrips <> "" Then
                .Append("SELECT arStudent.StudentId, ")

                ' retrieve all other needed columns
                .Append("saTransactions.TransAmount, saTransactions.TransDate, ")
                .Append("syRptAgencyFldValues.AgencyDescrip AS RefundFundSourceDescrip ")
                .Append("FROM ")
                .Append("arStudent, arStuEnrollments, saTransactions, saRefunds, saFundSources, ")
                .Append("syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")

                ' establish necessary relationships
                .Append("WHERE ")
                .Append("arStudent.StudentId = arStuEnrollments.StudentId AND ")
                .Append("arStuEnrollments.StuEnrollId = saTransactions.StuEnrollId AND ")
                .Append("saTransactions.TransactionId = saRefunds.TransactionId AND ")
                .Append("saRefunds.FundSourceId = saFundSources.FundSourceId AND ")
                .Append("saFundSources.FundSourceId = syRptAgencySchoolMapping.SchoolDescripId AND ")
                .Append("syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
                .Append("syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
                .Append("syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")

                ' get only records which are from selected Fund Sources
                .Append("syRptAgencyFldValues.AgencyDescrip IN (" & PutDBQuotesList(RptParamInfo.FilterFundSourceDescrips) & ") AND ")

                ' make sure only get records where the Refund Fund Source maps to an IPEDS Fund Source
                .Append("syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")

                ' retrict selection only to those refunds to Financial Aid Agencies
                .Append("saRefunds.RefundTypeId = 1 AND ")

                ' retrict selection to only those transactions within the report date range
                .Append(DateFilterSQL & " AND ")

                ' append list of appropriate students to include
                .Append("arStudent.StudentId IN (" & StudentList & ")")
                .Append(" and saTransactions.Voided=0 ")
            End If
        End With

        ' run query, add returned data to raw dataset
        dsRaw = IPEDSDB.DataAccessIPEDS().RunSQLDataSet(sb.ToString)

        ' set table names for each part of returned data
        With dsRaw
            .Tables(0).TableName = TblNameStudents
            If (RptParamInfo.ShowTuition Or RptParamInfo.ShowFees) And RptParamInfo.FilterFundSourceDescrips <> "" Then
                .Tables(1).TableName = "TuitionFees"
                .Tables(2).TableName = "Awards"
                .Tables(3).TableName = "Refunds"
            ElseIf RptParamInfo.ShowTuition Or RptParamInfo.ShowFees Then
                .Tables(1).TableName = "TuitionFees"
                .Tables.Add("Awards")
                .Tables.Add("Refunds")
            ElseIf RptParamInfo.FilterFundSourceDescrips <> "" Then
                .Tables.Add("TuitionFees")
                .Tables(1).TableName = "Awards"
                .Tables(2).TableName = "Refunds"
            End If
        End With

        ' return raw dataset
        Return dsRaw

    End Function

    Private Shared Function GetStudentListRpt(ByVal RptParamInfo As ReportParamInfoIPEDS) As String
        Dim dtStudents As New DataTable
        Dim drStudent As DataRow
        Dim sb As New System.Text.StringBuilder
        Dim objDA As New DataAccess

        Dim dvAuditHist As New DataView

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With objDA
            .ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            .OpenConnection()
        End With

        ' get list of students based on passed in report param info
        With sb
            ' get needed columns
            .Append("SELECT DISTINCT " & _
              "arStudent.StudentId " & _
              "FROM " & _
              "arStudent, arStuEnrollments, saTransactions ")

            ' establish necessary relationships
            .Append("WHERE " & _
              "arStudent.StudentId = arStuEnrollments.StudentId AND " & _
              "arStuEnrollments.StuEnrollId = saTransactions.StuEnrollId AND ")

            ' only get students who have transactions within the report date range
            .Append("saTransactions.TransDate >= '" & _
              FmtRptDateParam(RptParamInfo.CohortStartDate) & "' AND " & _
              "saTransactions.TransDate <= '" & _
              FmtRptDateParam(RptParamInfo.RptEndDate) & "'")
            .Append(" and saTransactions.Voided=0 ")

            ' apply sort on student ids
            .Append("ORDER BY arStudent.StudentId")
        End With

        ' run SQL to get DataTable with list of students
        dtStudents = objDA.RunSQLDataSet(sb.ToString).Tables(0).Copy

        With sb
            .Remove(0, .Length)
            For Each drStudent In dtStudents.Rows
                If .ToString <> "" Then
                    .Append(",'" & drStudent("StudentId").ToString & "'")
                Else
                    .Append("'" & drStudent("StudentId").ToString & "'")
                End If
            Next

            Return .ToString
        End With

    End Function



End Class
