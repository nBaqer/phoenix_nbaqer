Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class TermProgressDB
    Public Function GetStudentProgress(ByVal StuEnrollmentId As String) As DataSet

        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb4 As New StringBuilder
        Dim sb0 As New StringBuilder
        Dim sb1 As New StringBuilder
        Dim sb2 As New StringBuilder
        Dim sb3 As New StringBuilder
        Dim StuEnrollInfo As New StudentProgressInfo
        Dim sb As New System.Text.StringBuilder
        Dim da1 As New OleDbDataAdapter
        Dim ds As New DataSet

        'With sb4
        '    .Append("SELECT distinct(t1.TermId), t2.TermDescrip as Term, t2.StartDate from arResults t1, arTerm t2 ")
        '    .Append("WHERE t1.StudentId = ? and t1.TermId = t2.TermId ")
        'End With

        With sb4
            .Append("SELECT distinct(t3.TermId), t2.TermDescrip as Term, t2.StartDate ")
            .Append("from arResults t1, arTerm t2, arClassSections t3  ")
            .Append("WHERE t1.StuEnrollId = ? and t1.TestId = t3.ClsSectionId and t3.TermId = t2.TermId ")
        End With


        db.AddParameter("@StdId", StuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query       
        db.OpenConnection()
        da1 = db.RunParamSQLDataAdapter(sb4.ToString)
        Try
            da1.Fill(ds, "StdProgress")

        Catch ex As System.Exception

        End Try
        sb4.Remove(0, sb4.Length)
        db.ClearParameters()

        'With sb0
        '    .Append("SELECT t1.TermId, t1.ReqId, t1.Grade, t2.Credits, t3.StartDate, t3.EndDate ")
        '    .Append("FROM arResults t1, arReqs t2, arTerm t3 ")
        '    .Append("WHERE t1.ReqId = t2.ReqId ")
        '    .Append("AND StudentId = ? and t1.TermId = t3.TermId ")
        'End With

        With sb0
            .Append("SELECT distinct t4.TermId, t4.ReqId, t2.Credits, t3.StartDate, t3.EndDate, t1.TestId, t1.GrdSysDetailId, ")
            .Append("(Select Grade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId)as Grade, ")
            .Append("(Select IsPass from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsPass, ")
            .Append("(Select GPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as GPA, ")
            .Append("(Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsAttempted, ")
            .Append("(Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsEarned, ")
            .Append("(Select IsInGPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInGPA ")
            .Append("FROM arResults t1, arReqs t2, arTerm t3, arClassSections t4 ")
            .Append("WHERE t1.TestId = t4.ClsSectionId And t4.TermId = t3.TermId And t4.ReqId = t2.ReqId ")
            .Append("AND t1.GrdSysDetailId is not null ")
            .Append("AND EXISTS(")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t400.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t400.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t700.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t700.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append(" SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t800.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t800.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t900.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t900.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t1000.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t1000.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t1100.ReqId as ReqId, t3.Descrip As Req ")
            .Append(" FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000, arReqGrpDef t1100 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t1000.ReqId = t1100.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t1100.ReqId=t4.ReqId ")
            .Append(") ")

        End With
        db.AddParameter("@StdId", StuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query       
        db.OpenConnection()
        da1 = db.RunParamSQLDataAdapter(sb0.ToString)
        Try
            da1.Fill(ds, "StuResults")

        Catch ex As System.Exception

        End Try
        sb0.Remove(0, sb0.Length)
        db.ClearParameters()
        'With sb1
        '    .Append("SELECT sum(t4.Credits) as CredsAttmptd ")
        '    .Append("FROM arPrgVersions t1, arStuProgVer t2, arResults t3, arReqs t4, arGradeSystemDetails t5, arTerm t6 WHERE ")
        '    .Append("t2.PrgVerId = t1.PrgVerId And t2.StudentId = t3.StudentId and ")
        '    .Append("t3.ReqId = t4.ReqId and t1.GrdSystemId = t5.GrdSystemId and t3.TermId = t6.TermId and ")
        '    .Append("t2.PrgVerId = ? and t2.StudentId = ?  and t3.Grade = t5.Grade and t5.IsCreditsAttempted = 1 ")
        'End With

        ''Add the parameter list
        'db.AddParameter("@StdId", StuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        ''   Execute the query       
        'db.OpenConnection()
        'da1 = db.RunParamSQLDataAdapter(sb1.ToString)
        'Try
        '    da1.Fill(ds, "CredsAttmptd")

        'Catch ex As System.Exception

        'End Try
        'sb1.Remove(0, sb1.Length)
        'db.ClearParameters()


        'With sb2
        '    .Append("SELECT sum(t4.Credits) as CredsCompltd ")
        '    .Append("FROM arPrgVersions t1, arStuProgVer t2, arResults t3, arReqs t4, arGradeSystemDetails t5, arTerm t6 WHERE ")
        '    .Append("t2.PrgVerId = t1.PrgVerId And t2.StudentId = t3.StudentId and ")
        '    .Append("t3.ReqId = t4.ReqId and t1.GrdSystemId = t5.GrdSystemId and ")
        '    .Append("t2.PrgVerId = ? and t2.StudentId = ?  and t3.Grade = t5.Grade and t5.IsCreditsEarned = 1 and ")
        '    .Append("t6.TermId = t3.TermId ")
        'End With

        ''Add the parameter list
        'db.AddParameter("@StdId", StuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Dim da2 As New OleDbDataAdapter
        'da2 = db.RunParamSQLDataAdapter(sb2.ToString)
        'Try
        '    da2.Fill(ds, "CredsEarned")
        'Catch ex As System.Exception

        'End Try
        'sb2.Remove(0, sb2.Length)
        'db.ClearParameters()


        'With sb3
        '    .Append("Select Sum(t4.Credits) as TotalCreds, Sum(t5.GPA * t4.Credits)as GradeWeight ")
        '    .Append("FROM arPrgVersions t1, arStuProgVer t2, arResults t3, arReqs t4, arGradeSystemDetails t5, arTerm t6 WHERE ")
        '    .Append("t2.PrgVerId = t1.PrgVerId And t2.StudentId = t3.StudentId and ")
        '    .Append("t3.ReqId = t4.ReqId and t1.GrdSystemId = t5.GrdSystemId and ")
        '    .Append("t2.PrgVerId = ? and t2.StudentId = ?  and t3.Grade = t5.Grade and t5.IsInGPA = 1 ")
        '    .Append("and t6.TermId = t3.TermId ")
        'End With
        ''Add the parameter list
        'db.AddParameter("@StdId", StuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ''Execute the query
        'Dim da3 As New OleDbDataAdapter
        'da3 = db.RunParamSQLDataAdapter(sb3.ToString)
        'Try
        '    da3.Fill(ds, "CumGPA")
        'Catch ex As System.Exception

        'End Try
        'sb3.Remove(0, sb3.Length)
        'db.ClearParameters()


        'Close(Connection)
        db.CloseConnection()

        'Return BankInfo
        Return ds
    End Function

    Public Function GetStudentResultsForClassesInEnrollment(ByVal stuEnrollmentId As String, Optional ByVal bGetNumGrade As Boolean = False, Optional ByVal bNumGradeRnd As Boolean = False) As DataTable
        'connect to the database
        Dim db As New DataAccess
        Dim sb0 As New StringBuilder
        Dim StuEnrollInfo As New StudentProgressInfo

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb0
            .Append("SELECT * FROM ")
            .Append("(SELECT distinct t4.TermId,t3.TermDescrip,t4.ReqId,t2.Code,t2.Descrip,t2.Credits,t2.Hours,t2.CourseCategoryId,t3.StartDate,t3.EndDate,t1.GrdSysDetailId, t1.TestId, ")
            If bGetNumGrade Then
                '.Append("t1.Score, ")
                If bNumGradeRnd Then
                    .Append(" Round(t1.Score,0) as NumGrade, ")
                Else
                    .Append(" t1.Score as NumGrade, ")
                End If
            End If
            .Append("(Select Grade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId)as Grade, ")
            .Append("(Select IsPass from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsPass, ")
            .Append("(Select GPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as GPA, ")
            .Append("(Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsAttempted, ")
            .Append("(Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsEarned, ")
            .Append("(Select IsInGPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInGPA, ")
            .Append("(Select IsDrop from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsDrop, ")
            .Append("(Select Descrip from arCourseCategories where CourseCategoryId = t2.CourseCategoryId) as CourseCategory ")
            .Append("FROM arResults t1, arReqs t2, arTerm t3, arClassSections t4, arClassSectionTerms t5, arStuEnrollments t6 ")
            .Append("WHERE t1.TestId = t4.ClsSectionId ")
            .Append("AND t4.ClsSectionId = t5.ClsSectionId ")
            .Append("AND t5.TermId = t3.TermId ")
            .Append("AND t4.ReqId = t2.ReqId ")
            .Append("AND t1.StuEnrollId = t6.StuEnrollId ")
            .Append("AND t1.GrdSysDetailId is not null ")
            .Append("AND t1.StuEnrollId = ? ")
            'If we are dealing with a module start school the class might be shared between starts.
            'In this case the class would show up multiple times on the student's transcript - once
            'for each start that it is related to. As a temporary fix we are addding a filter to only
            'return the results for the start of the student. Remember that this is just a temporary 
            'fix. In the long term we will have to store the PK from arClassSectionTerms table that
            'tells us exactly which class and start the student is doing th class for. This is
            'important because the student might fail a course and has to retake it in a different 
            'start from the one that he started out with.
            If MyAdvAppSettings.AppSettings("SchedulingMethod", Nothing) = "ModuleStart" Then
                .Append("   AND t6.ExpStartDate = t3.StartDate ")
            End If
            .Append("UNION ")
            .Append("SELECT distinct t10.TermId,t30.TermDescrip,t10.ReqId,t20.Code,t20.Descrip,t20.Credits,t20.Hours,t20.CourseCategoryId,t30.StartDate,t30.EndDate,t10.GrdSysDetailId,'{00000000-0000-0000-0000-000000000000}' as TestId, ")
            If bGetNumGrade Then
                '.Append("t10.Score,  ")
                If bNumGradeRnd Then
                    .Append(" Round(t10.Score,0) as NumGrade, ")
                Else
                    .Append(" t10.Score as NumGrade, ")
                End If
            End If
            .Append("(Select Grade from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId)as Grade, ")
            .Append("(Select IsPass from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsPass, ")
            .Append("(Select GPA from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as GPA, ")
            .Append("(Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsCreditsAttempted, ")
            .Append("(Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsCreditsEarned, ")
            .Append("(Select IsInGPA from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsInGPA, ")
            .Append("(Select IsDrop from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsDrop, ")
            .Append("(Select Descrip from arCourseCategories where CourseCategoryId = t20.CourseCategoryId) as CourseCategory ")
            .Append("FROM arTransferGrades t10, arReqs t20, arTerm t30 ")
            .Append("WHERE t10.TermId = t30.TermId And t10.ReqId = t20.ReqId ")
            .Append("AND t10.GrdSysDetailId is not null ")
            .Append("AND t10.StuEnrollId = ?) A ")
            .Append("ORDER BY StartDate ")
        End With
        db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query       
        db.OpenConnection()

        Try
            Return db.RunParamSQLDataSet(sb0.ToString, "StudentResults").Tables(0)
        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw ex
            Else
                Throw ex.InnerException
            End If
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try

    End Function

    Public Function GetCoursesForStudentEnrollment(ByVal stuEnrollmentId As String) As DataTable
        'connect to the database
        Dim db As New DataAccess
        Dim sb0 As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb0
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t400.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t100.StuEnrollId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t700.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t100.StuEnrollId = ? ")
            .Append("UNION ")
            .Append(" SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t800.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t100.StuEnrollId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t900.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t100.StuEnrollId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t1000.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t100.StuEnrollId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t1100.ReqId as ReqId, t3.Descrip As Req ")
            .Append(" FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000, arReqGrpDef t1100 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t1000.ReqId = t1100.GrpId AND ")
            .Append("t100.StuEnrollId = ? ")
        End With
        db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query       
        db.OpenConnection()

        Try
            Return db.RunParamSQLDataSet(sb0.ToString, "StudentResults").Tables(0)
        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw ex
            Else
                Throw ex.InnerException
            End If
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try

    End Function

    Public Function GetCoursesForProgramVersion(ByVal programVersionId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            'Retrieve all courses in Program Version Definition disregarding the level
            'Retrieve all direct children, meaning only courses (ReqTypeId=1)
            .Append("SELECT ")
            .Append("       R.ReqId,")
            .Append("       R.Descrip,")
            .Append("       R.Code,")
            .Append("       R.Credits,")
            .Append("       R.Hours ")
            .Append("FROM   arReqs R,arProgVerDef PVD ")
            .Append("WHERE  R.ReqId=PVD.ReqId")
            .Append("       AND PVD.PrgVerId=?")
            .Append("       AND R.ReqTypeId=1 ")
            .Append("UNION ")
            'Retrieve direct children of nested course groups 
            .Append("SELECT ")
            .Append("       rg.ReqId,")
            .Append("       ar.Descrip,")
            .Append("       ar.Code,")
            .Append("       ar.Credits,")
            .Append("       ar.Hours ")
            .Append("FROM   arReqGrpDef rg,arReqs ar ")
            .Append("WHERE  rg.ReqId=ar.ReqId")
            .Append("       AND ar.ReqTypeId=1")
            .Append("       AND EXISTS(")
            .Append("               SELECT  DISTINCT t4.ReqId")
            .Append("               FROM    arReqs t4")
            .Append("               WHERE   t4.ReqTypeId=2")
            .Append("                       AND t4.ReqId=rg.GrpId")
            .Append("                       AND EXISTS(")
            .Append("                                   SELECT  DISTINCT t400.ReqId")
            .Append("                                   FROM    arReqs t3,arProgVerDef t400,arPrgVersions t600 ")
            .Append("                                   WHERE   t3.ReqId=t400.ReqId")
            .Append("                                           AND t400.PrgVerId=?")
            .Append("                                           AND t400.PrgVerId=t600.PrgVerId")
            .Append("                                           AND t400.ReqId=t4.ReqId")
            .Append("                                   UNION")
            .Append("                                   SELECT  DISTINCT t700.ReqId")
            .Append("                                   FROM    arReqs t3,arProgVerDef t400,arPrgVersions t600,arReqGrpDef t700 ")
            .Append("                                   WHERE   t3.ReqId=t400.ReqId")
            .Append("                                           AND t400.PrgVerId=?")
            .Append("                                           AND t400.PrgVerId=t600.PrgVerId")
            .Append("                                           AND t3.ReqTypeId=2")
            .Append("                                           AND t400.ReqId=t700.GrpId")
            .Append("                                           AND t700.ReqId=t4.ReqId")
            .Append("                                   UNION")
            .Append("                                   SELECT  DISTINCT t800.ReqId")
            .Append("                                   FROM    arReqs t3,arProgVerDef t400,arPrgVersions t600,arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("                                   WHERE   t3.ReqId=t400.ReqId")
            .Append("                                           AND t400.PrgVerId=?")
            .Append("                                           AND t400.PrgVerId=t600.PrgVerId")
            .Append("                                           AND t3.ReqTypeId=2")
            .Append("                                           AND t400.ReqId=t700.GrpId")
            .Append("                                           AND t700.ReqId=t800.GrpId")
            .Append("                                           AND t800.ReqId=t4.ReqId")
            .Append("                                   UNION")
            .Append("                                   SELECT  DISTINCT t900.ReqId")
            .Append("                                   FROM    arReqs t3,arProgVerDef t400, arPrgVersions t600,arReqGrpDef t700,arReqGrpDef t800,arReqGrpDef t900 ")
            .Append("                                   WHERE   t3.ReqId = t400.ReqId")
            .Append("                                           AND t400.PrgVerId=?")
            .Append("                                           AND t400.PrgVerId=t600.PrgVerId")
            .Append("                                           AND t3.ReqTypeId=2")
            .Append("                                           AND t400.ReqId=t700.GrpId")
            .Append("                                           AND t700.ReqId=t800.GrpId")
            .Append("                                           AND t800.ReqId=t900.GrpId")
            .Append("                                           AND t900.ReqId=t4.ReqId")
            .Append("                                   UNION")
            .Append("                                   SELECT  DISTINCT t1000.ReqId")
            .Append("                                   FROM    arReqs t3,arProgVerDef t400,arPrgVersions t600,arReqGrpDef t700,arReqGrpDef t800,arReqGrpDef t900,arReqGrpDef t1000 ")
            .Append("                                   WHERE   t3.ReqId= t400.ReqId")
            .Append("                                           AND t400.PrgVerId=?")
            .Append("                                           AND t400.PrgVerId=t600.PrgVerId")
            .Append("                                           AND t3.ReqTypeId=2")
            .Append("                                           AND t400.ReqId=t700.GrpId")
            .Append("                                           AND t700.ReqId=t800.GrpId")
            .Append("                                           AND t800.ReqId=t900.GrpId")
            .Append("                                           AND t900.ReqId=t1000.GrpId")
            .Append("                                           AND t1000.ReqId=t4.ReqId")
            .Append("                                   UNION")
            .Append("                                   SELECT  DISTINCT t1100.ReqId")
            .Append("                                   FROM    arReqs t3,arProgVerDef t400,arPrgVersions t600,arReqGrpDef t700,arReqGrpDef t800,arReqGrpDef t900,arReqGrpDef t1000,arReqGrpDef t1100 ")
            .Append("                                   WHERE   t3.ReqId=t400.ReqId")
            .Append("                                           AND t400.PrgVerId=?")
            .Append("                                           AND t400.PrgVerId=t600.PrgVerId")
            .Append("                                           AND t3.ReqTypeId=2")
            .Append("                                           AND t400.ReqId=t700.GrpId")
            .Append("                                           AND t700.ReqId=t800.GrpId")
            .Append("                                           AND t800.ReqId=t900.GrpId")
            .Append("                                           AND t900.ReqId=t1000.GrpId")
            .Append("                                           AND t1000.ReqId=t1100.GrpId")
            .Append("                                           AND t1100.ReqId=t4.ReqId")
            .Append("                                   )")
            .Append("                   ) ")
            .Append("ORDER BY Code,Descrip")

            '.Append("SELECT distinct t400.ReqId as ReqId, t3.Descrip, t3.Code ")
            '.Append("FROM arReqs t3,arProgVerDef t400 ")
            '.Append("WHERE t3.ReqId = t400.ReqId AND ")
            '.Append("t400.PrgVerId = ? ")
            '.Append("UNION ")
            '.Append("SELECT distinct t700.ReqId as ReqId, t3.Descrip, t3.Code ")
            '.Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700 ")
            '.Append("WHERE t3.ReqId = t400.ReqId AND ")
            '.Append("t3.ReqTypeId = 2 AND ")
            '.Append("t400.ReqId = t700.GrpId AND ")
            '.Append("t400.PrgVerId = ? ")
            '.Append("UNION ")
            '.Append("SELECT distinct t800.ReqId as ReqId, t3.Descrip, t3.Code ")
            '.Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800 ")
            '.Append("WHERE t3.ReqId = t400.ReqId AND ")
            '.Append("t3.ReqTypeId = 2 AND ")
            '.Append("t400.ReqId = t700.GrpId AND ")
            '.Append("t700.ReqId = t800.GrpId AND ")
            '.Append("t400.PrgVerId = ? ")
            '.Append("UNION ")
            '.Append("SELECT distinct t900.ReqId as ReqId, t3.Descrip, t3.Code ")
            '.Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900 ")
            '.Append("WHERE t3.ReqId = t400.ReqId AND ")
            '.Append("t3.ReqTypeId = 2 AND ")
            '.Append("t400.ReqId = t700.GrpId AND ")
            '.Append(" t700.ReqId = t800.GrpId AND ")
            '.Append("t800.ReqId = t900.GrpId AND ")
            '.Append("t400.PrgVerId = ? ")
            '.Append("UNION ")
            '.Append("SELECT distinct t1000.ReqId as ReqId, t3.Descrip, t3.Code ")
            '.Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000 ")
            '.Append("WHERE t3.ReqId = t400.ReqId AND ")
            '.Append("t3.ReqTypeId = 2 AND ")
            '.Append("t400.ReqId = t700.GrpId AND ")
            '.Append("t700.ReqId = t800.GrpId AND ")
            '.Append("t800.ReqId = t900.GrpId AND ")
            '.Append("t900.ReqId = t1000.GrpId AND ")
            '.Append("t400.PrgVerId = ? ")
            '.Append("UNION ")
            '.Append("SELECT distinct t1100.ReqId as ReqId, t3.Descrip, t3.Code ")
            '.Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000, arReqGrpDef t1100 ")
            '.Append("WHERE t3.ReqId = t400.ReqId AND ")
            '.Append("t3.ReqTypeId = 2 AND ")
            '.Append("t400.ReqId = t700.GrpId AND ")
            '.Append("t700.ReqId = t800.GrpId AND ")
            '.Append("t800.ReqId = t900.GrpId AND ")
            '.Append("t900.ReqId = t1000.GrpId AND ")
            '.Append("t1000.ReqId = t1100.GrpId AND ")
            '.Append("t400.PrgVerId = ? ")
            '.Append("ORDER BY t3.Code, t3.Descrip")
        End With

        db.AddParameter("@PrgVerId", programVersionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@PrgVerId", programVersionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@PrgVerId", programVersionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@PrgVerId", programVersionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@PrgVerId", programVersionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@PrgVerId", programVersionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@PrgVerId", programVersionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        '   Execute the query       
        db.OpenConnection()

        Try
            Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw ex
            Else
                Throw ex.InnerException
            End If
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try
    End Function


    Public Function GetClassSectionsForStudentEnrollmentWhenEnrolling(ByVal stuEnrollmentId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT t1.TermId,t1.StartDate,t1.EndDate,t3.Descrip,t2.ClsSectionId,t2.CampusId,t2.ShiftId,t2.ClsSection,t2.ReqId ")
            .Append("FROM arTerm t1, arClassSections t2, arReqs t3, ")
            .Append("(SELECT distinct t100.ExpStartDate,t100.CampusId,t100.ShiftId,t400.ReqId as ReqId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.StuEnrollId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t100.ExpStartDate,t100.CampusId,t100.ShiftId,t700.ReqId as ReqId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arReqGrpDef t700 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t100.StuEnrollId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t100.ExpStartDate,t100.CampusId,t100.ShiftId,t800.ReqId as ReqId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t100.StuEnrollId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t100.ExpStartDate,t100.CampusId,t100.ShiftId,t900.ReqId as ReqId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t100.StuEnrollId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t100.ExpStartDate,t100.CampusId,t100.ShiftId,t1000.ReqId as ReqId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t100.StuEnrollId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t100.ExpStartDate,t100.CampusId,t100.ShiftId,t1100.ReqId as ReqId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000, arReqGrpDef t1100 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t1000.ReqId = t1100.GrpId AND ")
            .Append("t100.StuEnrollId = ? ")
            .Append(")  t4 ")
            .Append("WHERE t1.TermId = t2.TermId ")
            .Append("AND t2.ReqId = t3.ReqId ")
            .Append("AND t1.IsModule = 1 ")
            .Append("AND t2.ReqId = t4.ReqId ")
            .Append("AND t1.StartDate >= t4.ExpStartDate ")
            .Append("AND t2.CampusId = t4.CampusId ")
            .Append("AND ((t2.ShiftId = t4.ShiftId) OR (t2.ShiftId IS NULL AND t4.ShiftId IS NULL)) ")
            .Append("AND t2.MaxStud > (SELECT COUNT(*) ")
            .Append("FROM arResults ar ")
            .Append("WHERE ar.TestId = t2.ClsSectionId) ")
            .Append("ORDER BY t1.StartDate ")
        End With

        db.AddParameter("@sid", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function

    'Public Function GetCoursesWithoutPassingGradeForStudentEnrollment(ByVal stuEnrollmentId As String, Optional ByVal excludeNullGrades As Boolean = False) As DataTable
    '    'connect to the database
    '    Dim db As New DataAccess
    '    Dim sb0 As New StringBuilder

    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

    '    With sb0
    '        .Append("SELECT ReqId,Code,Req as Descrip,Credits,Hours,StuEnrollId ")
    '        .Append("FROM ")
    '        .Append("(")
    '        .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t400.ReqId as ReqId, t3.Descrip As Req,t3.Code, t3.Credits, t3.Hours  ")
    '        .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600 ")
    '        .Append("WHERE t100.StudentId=t500.StudentId AND ")
    '        .Append("t3.ReqId = t400.ReqId AND ")
    '        .Append("t100.PrgVerId = t400.PrgVerId AND ")
    '        .Append("t100.PrgVerId = t600.PrgVerId AND ")
    '        .Append("t3.ReqTypeId = 1 AND ")
    '        .Append("t100.StuEnrollId = ? ")
    '        .Append("UNION ")
    '        .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t700.ReqId as ReqId, t701.Descrip As Req,t701.Code, t701.Credits, t701.Hours ")
    '        .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqs t701 ")
    '        .Append("WHERE t100.StudentId=t500.StudentId AND ")
    '        .Append("t3.ReqId = t400.ReqId AND ")
    '        .Append("t100.PrgVerId = t400.PrgVerId AND ")
    '        .Append("t100.PrgVerId = t600.PrgVerId AND ")
    '        .Append("t3.ReqTypeId = 2 AND ")
    '        .Append("t400.ReqId = t700.GrpId AND ")
    '        .Append("t700.ReqId = t701.ReqId AND ")
    '        .Append("t701.ReqTypeId = 1 AND ")
    '        .Append("t100.StuEnrollId = ? ")
    '        .Append(") A ")
    '        .Append("WHERE ReqId NOT IN ")
    '        .Append("(")
    '        .Append("SELECT ac.ReqId ")
    '        .Append("FROM arResults ar, arClassSections ac, arGradeSystemDetails gs ")
    '        .Append("WHERE ar.StuEnrollId = ? ")
    '        .Append("AND ar.TestId = ac.ClsSectionId ")
    '        If excludeNullGrades = False Then
    '            .Append("AND ar.GrdSysDetailId = gs.GrdSysDetailId AND gs.IsPass = 1 ")
    '        Else
    '            .Append("AND ((ar.GrdSysDetailId = gs.GrdSysDetailId AND gs.IsPass = 1) OR (ar.GrdSysDetailId IS NULL)) ")
    '        End If

    '        .Append("UNION ")
    '        .Append("SELECT tg.ReqId ")
    '        .Append("FROM arTransferGrades tg, arGradeSystemDetails gsd ")
    '        .Append("WHERE tg.StuEnrollId = ? ")
    '        .Append("AND tg.GrdSysDetailId = gsd.GrdSysDetailId ")
    '        .Append("AND gsd.IsPass = 1 ")
    '        .Append(") ")
    '        '.Append(" group by ReqId,Code,Req,Credits,Hours,StuEnrollId  ")
    '        '.Append(" having (select      count(*) as TheoryCount ")
    '        '.Append(" from     arGrdComponentTypes GC, arGrdBkWgtDetails GD ")
    '        '.Append(" where      GC.GrdComponentTypeId = GD.GrdComponentTypeId ")
    '        '' and SysComponentTypeid not in(500,503) ")
    '        '.Append(" and number is not null    and weight is not null  and GD.InstrGrdBkWgtId in(select  GBW.InstrGrdBkWgtId as ID ")
    '        '.Append(" from arGrdBkWeights GBW   where  GBW.ReqId = A.Reqid ")
    '        '.Append(" and     GBW.StatusId = (select StatusId from syStatuses where Status='Active') ))>0 ")
    '        .Append(" ORDER BY Code ")

    '    End With
    '    db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    'db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    'db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    'db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    'db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    '   Execute the query       
    '    db.OpenConnection()

    '    Try
    '        Return db.RunParamSQLDataSet(sb0.ToString, "StudentResults").Tables(0)
    '    Catch ex As System.Exception
    '        If ex.InnerException Is Nothing Then
    '            Throw ex
    '        Else
    '            Throw ex.InnerException
    '        End If
    '    End Try

    'End Function
    Public Function GetCoursesWithoutPassingGradeForStudentEnrollment(ByVal stuEnrollmentId As String, Optional ByVal excludeNullGrades As Boolean = False) As DataTable
        'connect to the database
        Dim db As New DataAccess
        Dim sb0 As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb0
            .Append("SELECT ReqId,Code,Req as Descrip,Credits,Hours,StuEnrollId,Grd ")
            .Append("FROM ")
            .Append("(")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t400.ReqId as ReqId, t3.Descrip As Req,t3.Code, t3.Credits, t3.Hours,  ")
            .Append("(CASE WHEN ")
            .Append("(select  count(*)  from arGrdBkWeights GBW,arGrdBkWgtDetails GD,arGrdComponentTypes GC   where ")
            .Append(" GBW.ReqId = t400.ReqId and GBW.InstrGrdBkWgtId=GD.InstrGrdBkWgtId and GC.GrdComponentTypeId = GD.GrdComponentTypeId) = 0 THEN 1 ")
            .Append(" ELSE ")
            .Append(" (CASE WHEN ")
            .Append(" (select  count(*)  from arGrdBkWeights GBW,arGrdBkWgtDetails GD,arGrdComponentTypes GC   where ")
            .Append(" GBW.ReqId = t400.ReqId And GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId And GC.GrdComponentTypeId = GD.GrdComponentTypeId And (number Is Not null ")
            .Append(" OR weight is not null)) >0 then 1 ")
            .Append(" else ")
            .Append(" 0 ")
            .Append(" end)")
            .Append(" END) ")
            .Append(" AS Grd ,t3.StatusId  ")
            .Append(" FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 1 AND ")
            .Append("t100.StuEnrollId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t700.ReqId as ReqId, t701.Descrip As Req,t701.Code, t701.Credits, t701.Hours, ")
            .Append("(CASE WHEN ")
            .Append("(select  count(*)  from arGrdBkWeights GBW,arGrdBkWgtDetails GD,arGrdComponentTypes GC   where ")
            .Append(" GBW.ReqId = t400.ReqId and GBW.InstrGrdBkWgtId=GD.InstrGrdBkWgtId and GC.GrdComponentTypeId = GD.GrdComponentTypeId) = 0 THEN 1 ")
            .Append(" ELSE ")
            .Append(" (CASE WHEN ")
            .Append(" (select  count(*)  from arGrdBkWeights GBW,arGrdBkWgtDetails GD,arGrdComponentTypes GC   where ")
            .Append(" GBW.ReqId = t400.ReqId And GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId And GC.GrdComponentTypeId = GD.GrdComponentTypeId And (number Is Not null ")
            .Append(" OR weight is not null)) >0 then 1 ")
            .Append(" else ")
            .Append(" 0 ")
            .Append(" end)")
            .Append(" END) ")
            .Append(" AS Grd,t701.StatusId  ")
            .Append(" FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqs t701 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t701.ReqId AND ")
            .Append("t701.ReqTypeId = 1 AND ")
            .Append("t100.StuEnrollId = ? ")
            .Append(") A ,syStatuses WHERE A.StatusId=syStatuses.StatusId and syStatuses.Status='Active' and ")
            .Append(" ReqId NOT IN ")
            .Append("(")
            .Append("SELECT ac.ReqId ")
            .Append("FROM arResults ar, arClassSections ac, arGradeSystemDetails gs ")
            .Append("WHERE ar.StuEnrollId = ? ")
            .Append("AND ar.TestId = ac.ClsSectionId ")
            If excludeNullGrades = False Then
                .Append("AND ar.GrdSysDetailId = gs.GrdSysDetailId ")   'AND gs.IsPass = 1 ") <-- this exclude the graded courses not approved
            Else
                .Append("AND ((ar.GrdSysDetailId = gs.GrdSysDetailId AND gs.IsPass = 1) OR (ar.GrdSysDetailId IS NULL)) ") 'gs.IsPass = 1) OR (ar.GrdSysDetailId IS NULL)) ")
            End If

            .Append("UNION ")
            .Append("SELECT tg.ReqId ")
            .Append("FROM arTransferGrades tg, arGradeSystemDetails gsd ")
            .Append("WHERE tg.StuEnrollId = ? ")
            .Append("AND tg.GrdSysDetailId = gsd.GrdSysDetailId ")
            .Append("AND gsd.IsPass = 1 ")
            .Append(") ")
            '.Append(" group by ReqId,Code,Req,Credits,Hours,StuEnrollId  ")
            '.Append(" having (select      count(*) as TheoryCount ")
            '.Append(" from     arGrdComponentTypes GC, arGrdBkWgtDetails GD ")
            '.Append(" where      GC.GrdComponentTypeId = GD.GrdComponentTypeId ")
            '' and SysComponentTypeid not in(500,503) ")
            '.Append(" and number is not null    and weight is not null  and GD.InstrGrdBkWgtId in(select  GBW.InstrGrdBkWgtId as ID ")
            '.Append(" from arGrdBkWeights GBW   where  GBW.ReqId = A.Reqid ")
            '.Append(" and     GBW.StatusId = (select StatusId from syStatuses where Status='Active') ))>0 ")
            .Append(" ORDER BY Code ")

        End With
        db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@StdId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query       
        db.OpenConnection()

        Try
            Return db.RunParamSQLDataSet(sb0.ToString, "StudentResults").Tables(0)
        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw ex
            Else
                Throw ex.InnerException
            End If
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try


    End Function
    Public Function GetCoursesWithoutPassingGradeForCEEnrollment(ByVal StuEnrollId As String) As DataTable
        Dim db As New DataAccess
        Dim sb0 As New StringBuilder
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb0
            .Append("Select ReqId,Code,Descrip,Credits,Hours,1 as Grd  from arReqs where ReqId not in ")
            .Append("(Select t2.ReqId from arReqs t2, arResults t3, arClassSections t4 ")
            .Append("where t3.StuEnrollId = ? ")
            .Append("and t3.GrdSysDetailId is NOT NULL ")
            .Append("and t3.TestId = t4.ClsSectionId and t4.ReqId = t2.ReqId) ")
            .Append("Order by arReqs.Code ")
        End With

        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '   Execute the query       
        db.OpenConnection()

        Try
            Return db.RunParamSQLDataSet(sb0.ToString, "StudentResults").Tables(0)
        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw ex
            Else
                Throw ex.InnerException
            End If
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try

    End Function

    Public Function DoesReqBelongToPrgVer(ByVal PrgVerId As String, ByVal ReqId As String) As Integer
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim sGrdSysDetailId As String
        Dim rowCount As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("Select count(*) as Count from arProgVerDef a where a.ReqId = ? and PrgVerId = ? ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ReqId", ReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try

        'Return the datatable in the dataset
        Return rowCount

    End Function

    Public Function GetGrdSysDetails(ByVal GrdSysDetailId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            ''   build the sql query
            'With sb
            '    .Append("Select t1.GrdSysDetailId, t1.Grade, t1.IsPass, t1.GPA, ")
            '    .Append("t1.IsCreditsAttempted, t1.IsCreditsEarned, t1.IsInGPA ")
            '    .Append("from arGradeSystemDetails t1 where t1.GrdSystemId In ")
            '    .Append("(Select t3.GrdSystemId from arPrgVersions t3 where t3.PrgVerId = ?) ")
            'End With


            '   build the sql query
            With sb
                .Append("Select t1.Grade, t1.IsPass, t1.GPA, ")
                .Append("t1.IsCreditsAttempted, t1.IsCreditsEarned, t1.IsInGPA ")
                .Append("from arGradeSystemDetails t1 where t1.GrdSystemId In ")
                .Append("(Select t3.GrdSystemId from arPrgVersions t3 where t3.PrgVerId = ?) ")
            End With
            ' Add the PrgVerId and ChildId to the parameter list
            'db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "GrdSysDetails")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function IsCredsAttmptd(ByVal Grade As String, ByVal PrgVerId As String) As Integer
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim count As String
        Dim sGrdSysDetailId As String
        Dim IsPass As Integer
        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("select IsPass from arGradeSystemDetails where Grade=? and ")
                .Append("GrdSystemID in (select GrdSystemId from arPrgVersions where PrgVerId=?) ")

            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@Grade", Grade, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)


            While dr.Read()
                IsPass = dr("IsPass")
            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()


        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return IsPass
    End Function

    'US4112 6/10/2013 Janet Robinson re-factored to stored proc
    Public Function GetGradedAndScheduledClassesForEnrollmentSP(ByVal stuEnrollId As String, Optional ByVal campusid As String = "") As DataTable
        Dim db As New DataAccess
        'Dim sb As New StringBuilder

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@CampusId", campusid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Return db.RunParamSQLDataSetUsingSP("USP_AR_GetSchAndGradedClassesForEnrollment", Nothing).Tables(0)
    End Function

    Public Function GetGradedAndScheduledClassesForEnrollment(ByVal stuEnrollId As String, Optional ByVal Campusid As String = "") As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("SELECT t1.TestId,t2.StartDate,t2.EndDate,t3.Descrip,t2.ReqId,t2.ClsSection,t4.TermDescrip, t4.TermId, ")
            If MyAdvAppSettings.AppSettings("GradesFormat", Campusid).ToLower = "numeric" Then
                'Ross Type Schools don't use the Transfer Instructor Grade Books Results Page
                'in that case we need to use the score instead of grade.
                If MyAdvAppSettings.AppSettings("GradeRounding").ToLower = "yes" Then
                    '.Append(" Round(IsNULL(t1.Score,0),0) as Grade, ")
                    .Append(" Round(t1.Score,0) as Grade, ")
                Else
                    '.Append(" IsNull(t1.Score,0) as Grade, ")
                    .Append(" t1.Score as Grade, ")
                End If
            Else
                .Append("(SELECT t6.Grade ")
                .Append("FROM arGradeSystemDetails t6 ")
                .Append("WHERE t1.GrdSysDetailId = t6.GrdSysDetailId) AS Grade, ")
            End If
            .Append("(SELECT t7.IsPass ")
            .Append("FROM arGradeSystemDetails t7 ")
            .Append("WHERE t1.GrdSysDetailId = t7.GrdSysDetailId) AS IsPass,t3.Code as Code,t4.StartDate AS TermStart ")
            .Append("FROM arResults t1, arClassSections t2, arReqs t3,arTerm t4,arStuEnrollments t8 ")
            .Append("WHERE t1.StuEnrollId = ? ")
            .Append("AND t1.TestId = t2.ClsSectionId ")
            .Append("AND t2.ReqId = t3.ReqId and t2.TermID=t4.TermID and t1.StuEnrollId=t8.StuEnrollId and t2.ShiftId = t8.ShiftId ")
            If Not MyAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                .Append(" and t3.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId=? and GrdSysDetailId is not null) ")
            Else
                .Append(" and t3.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId=? and Score is not null) ")
            End If
            .Append(" Union ")
            'Shift  is an optional field in Class Section page, so the following query
            'will get the classes with no shift available
            .Append("SELECT t1.TestId,t2.StartDate,t2.EndDate,t3.Descrip,t2.ReqId,t2.ClsSection,t4.TermDescrip, t4.TermId, ")
            If MyAdvAppSettings.AppSettings("GradesFormat", Campusid).ToLower = "numeric" Then
                'Ross Type Schools don't use the Transfer Instructor Grade Books Results Page
                'in that case we need to use the score instead of grade.
                If MyAdvAppSettings.AppSettings("GradeRounding").ToLower = "yes" Then
                    '.Append(" Round(IsNULL(t1.Score,0),0) as Grade, ")
                    .Append(" Round(t1.Score,0) as Grade, ")
                Else
                    '.Append(" IsNull(t1.Score,0) as Grade, ")
                    .Append(" t1.Score as Grade, ")
                End If
            Else
                .Append("(SELECT t6.Grade ")
                .Append("FROM arGradeSystemDetails t6 ")
                .Append("WHERE t1.GrdSysDetailId = t6.GrdSysDetailId) AS Grade, ")
            End If
            .Append("(SELECT t7.IsPass ")
            .Append("FROM arGradeSystemDetails t7 ")
            .Append("WHERE t1.GrdSysDetailId = t7.GrdSysDetailId) AS IsPass,t3.Code as Code,t4.StartDate AS TermStart ")
            .Append("FROM arResults t1, arClassSections t2, arReqs t3,arTerm t4,arStuEnrollments t8 ")
            .Append("WHERE t1.StuEnrollId = ? ")
            .Append("AND t1.TestId = t2.ClsSectionId ")
            .Append("AND t2.ReqId = t3.ReqId and t2.TermID=t4.TermID and t1.StuEnrollId=t8.StuEnrollId ")
            If Not MyAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                .Append(" and t3.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId=? and GrdSysDetailId is not null) ")
            Else
                .Append(" and t3.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId=? and Score is not null) ")
            End If
            .Append(" Union ")
            '.Append("SELECT t2.ClsSectionId,t2.StartDate,t2.EndDate,t3.Descrip,t2.ReqId,t2.ClsSection,t4.TermDescrip, ")
            'modified by Theresa G on 09/09/09 for mantis 17306: Bug: Allied Health - Transfer Student to Different Program Results 
            .Append("SELECT distinct '00000000-0000-0000-0000-000000000000' as ClsSectionId,t4.StartDate,t4.EndDate,t3.Descrip,t1.ReqId,'' as ClsSection,t4.TermDescrip,  t4.TermId, ")
            If MyAdvAppSettings.AppSettings("GradesFormat", Campusid).ToLower = "numeric" Then
                'Ross Type Schools don't use the Transfer Instructor Grade Books Results Page
                'in that case we need to use the score instead of grade.
                If MyAdvAppSettings.AppSettings("GradeRounding").ToLower = "yes" Then
                    '.Append(" Round(IsNULL(t1.Score,0),0) as Grade, ")
                    .Append(" Round(t1.Score,0) as Grade, ")
                Else
                    '.Append(" IsNull(t1.Score,0) as Grade, ")
                    .Append(" t1.Score as Grade, ")
                End If
            Else
                .Append("(SELECT t6.Grade ")
                .Append("FROM arGradeSystemDetails t6 ")
                .Append("WHERE t1.GrdSysDetailId = t6.GrdSysDetailId) AS Grade, ")
            End If
            .Append("(SELECT t7.IsPass ")
            .Append("FROM arGradeSystemDetails t7 ")
            .Append("WHERE t1.GrdSysDetailId = t7.GrdSysDetailId) AS IsPass,t3.Code as Code,t4.StartDate AS TermStart ")
            .Append("FROM arTransferGrades t1, arReqs t3,arTerm t4,arStuEnrollments t8 ")
            .Append("WHERE t1.StuEnrollId = ? ")
            .Append(" and t1.ReqId = t3.ReqId and t1.TermId=t4.TermId ")
            .Append(" and t1.StuEnrollId=t8.StuEnrollId ")
            .Append("ORDER BY t4.StartDate, t3.Descrip ")
        End With
        db.AddParameter("@stuid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@stuid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@stuid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@stuid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@stuid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
    End Function
    Public Function GetAvailableClassesForEnrollmentUsingRotationalSchedule(ByVal stuEnrollId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("SELECT t1.StartDate,t1.EndDate,t3.Descrip,t2.ClsSectionId,t2.ClsSection,t2.ReqId ")
            .Append("FROM arTerm t1, arClassSections t2, arReqs t3, ")
            .Append("(SELECT distinct t100.ExpStartDate,t100.CampusId,t100.ShiftId,t400.ReqId as ReqId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.StuEnrollId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t100.ExpStartDate,t100.CampusId,t100.ShiftId,t700.ReqId as ReqId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arReqGrpDef t700 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t100.StuEnrollId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t100.ExpStartDate,t100.CampusId,t100.ShiftId,t800.ReqId as ReqId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t100.StuEnrollId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t100.ExpStartDate,t100.CampusId,t100.ShiftId,t900.ReqId as ReqId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t100.StuEnrollId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t100.ExpStartDate,t100.CampusId,t100.ShiftId,t1000.ReqId as ReqId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t100.StuEnrollId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t100.ExpStartDate,t100.CampusId,t100.ShiftId,t1100.ReqId as ReqId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000, arReqGrpDef t1100 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t1000.ReqId = t1100.GrpId AND ")
            .Append("t100.StuEnrollId = ? ")
            .Append(")  t4 ")
            .Append("WHERE t1.TermId = t2.TermId ")
            .Append("AND t2.ReqId = t3.ReqId ")
            .Append("AND t1.IsModule = 1 ")
            .Append("AND t2.ReqId = t4.ReqId ")
            .Append("AND t1.StartDate >= t4.ExpStartDate ")
            '.Append("AND t1.StartDate > GetDate() ")
            .Append("AND t1.StartDate >= '" & Date.Today & "' ")
            .Append("AND t2.CampusId = t4.CampusId ")
            .Append("AND (t2.ShiftId = t4.ShiftId OR t2.ShiftId IS NULL AND t4.ShiftId IS NULL) ")
            .Append("AND t2.ClsSectionId NOT IN ")
            .Append("( ")
            .Append("SELECT t7.TestId ")
            .Append("FROM arResults t7 ")
            .Append("WHERE t7.StuEnrollId = ? ")
            .Append(") ")
            .Append("AND t2.ReqId NOT IN ")
            .Append("( ")
            .Append("SELECT t9.ReqId ")
            .Append("FROM arResults t8, arClassSections t9, arGradeSystemDetails t10 ")
            .Append("WHERE t8.StuEnrollId = ? ")
            .Append("AND t8.TestId = t9.ClsSectionId ")
            .Append("AND t8.GrdSysDetailId = t10.GrdSysDetailId ")
            .Append("AND t10.IsPass = 1 ")
            .Append(") ")
            .Append("AND t2.MaxStud > (SELECT COUNT(*) ")
            .Append("FROM arResults ar ")
            .Append("WHERE ar.TestId = t2.ClsSectionId) ")
            .Append("ORDER BY t1.StartDate ")
        End With

        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function
    ' US4112 6/10/2013 Janet Robinson refactored to stored proc
    Public Function GetAvailableClassesForEnrollmentUsingStartDateSP(ByVal stuEnrollId As String) As DataTable
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Dim ds As DataSet
        Dim stuStartDate As String
        Dim aRows() As DataRow
        ds = db.RunParamSQLDataSetUsingSP("USP_AR_GetAvailableClassesUsingStartDate", Nothing)
        ' Revised at 6/2/2015 this is not more used.
        'If myAdvAppSettings.AppSettings("SchedulingMethod", Nothing) = "ModuleStart" Then
        '    For Each row As DataRow In ds.Tables(0).Rows
        '        If (IsDuplicateClassSection(row("ClsSectionId").ToString) = True) Then
        '            stuStartDate = GetStudentStartDate(stuEnrollId)
        '            aRows = ds.Tables(0).Select("ClsSectionId='" & row("ClsSectionId").ToString & "' AND TermStart=#" & CDate(stuStartDate) & "#")
        '            If aRows.Length > 0 Then
        '                If (stuStartDate <> CDate(row("TermStart")).ToShortDateString) Then
        '                    row.Delete()
        '                End If
        '            Else
        '                'aRows = ds.Tables(0).Select("TestId='" & row("TestId").ToString & "'")
        '                aRows = ds.Tables(0).Select("ClsSectionId='" & row("ClsSectionId").ToString & "' AND TermStart <> #" & row("TermStart") & "#")
        '                If (aRows.Length > 0) Then
        '                    row.Delete()
        '                End If
        '            End If
        '        End If
        '    Next
        '    ds.AcceptChanges()
        'End If
        Return ds.Tables(0)

    End Function

    Public Function GetAvailableClassesForEnrollmentUsingStartDate(ByVal stuEnrollId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'With sb
        '    .Append("Select t0.ClsSectionId, t0.ClsSection, t0.ReqId, t0.StartDate,t0.EndDate, t9.Code, t9.Descrip ")
        '    .Append("from arClassSections t0, arStuEnrollments t8, arReqs t9 where ")
        '    .Append("t0.ReqId not in ")
        '    .Append("( ")
        '    .Append("Select t2.ReqId from arReqs t2, arResults t3,arClassSections t4, arGradeSystemDetails t5 ")
        '    .Append("where t3.StuEnrollId = ?   ")
        '    .Append("and t3.GrdSysDetailId is NOT NULL and t3.GrdSysDetailId = t5.GrdSysDetailId and t5.IsPass = 1 ")
        '    .Append("and t3.TestId = t4.ClsSectionId and t4.ReqId = t2.ReqId  ")
        '    .Append(" ) ")
        '    .Append("and t0.ReqId = t9.ReqId ")
        '    .Append("and t8.StuEnrollId = ?  ")
        '    .Append("and t0.StartDate <> t8.StartDate ")
        '    .Append("and t0.StartDate in ")
        '    .Append("( ")
        '    .Append("Select StartDate from arTerm where ProgId in (  ")
        '    .Append("Select ProgId from arPrgVersions where PrgVerId in ( ")
        '    .Append("Select PrgVerId from arStuEnrollments where StuEnrollId = ? ")
        '    .Append(" ) ")
        '    .Append(" ) ")
        '    .Append(" ) ")
        '    .Append("and t0.ClsSectionId not in ")
        '    .Append("(Select t7.TestId from arResults t7 where t7.StuEnrollId = ? ) ")
        'End With

        With sb
            .Append("Select t0.ClsSectionId, t0.ClsSection, t0.ReqId,t11.TermDescrip,t11.TermId,t11.StartDate AS TermStart, t0.StartDate,t0.EndDate, t9.Code, t9.Descrip ")
            .Append("from arClassSections t0, arStuEnrollments t8, arReqs t9, arClassSectionTerms t10, arTerm t11,arProgVerDef t12 where ")
            .Append("t0.ReqId not in ")
            .Append("( ")
            .Append("Select t2.ReqId from arReqs t2, arResults t3,arClassSections t4, arGradeSystemDetails t5 ")
            .Append("where t3.StuEnrollId = ? ")
            .Append("and t3.GrdSysDetailId is NOT NULL and t3.GrdSysDetailId = t5.GrdSysDetailId and t5.IsPass = 1 ")
            .Append("and t3.TestId = t4.ClsSectionId and t4.ReqId = t2.ReqId ")
            .Append(" ) ")
            .Append("and t0.ReqId = t9.ReqId ")
            .Append("and t0.ClsSectionId = t10.ClsSectionId ")
            .Append("and t10.TermId = t11.TermId and t0.ShiftId = t8.ShiftId ")
            .Append("and t8.StuEnrollId = ? ")
            ''progId can be null or notnull
            ''So, ProgId is null clause added
            ''Query Modified by Saraswathi Lakshmanan 4-Nov 2008
            '.Append("and t11.ProgId = (Select ProgId ")
            '.Append("                   from arPrgVersions ")
            '.Append("                   where PrgVerId in ( Select PrgVerId ")
            '.Append("                                       from arStuEnrollments ")
            '.Append("                                       where StuEnrollId = ?)) ")

            .Append(" and ( t11.ProgId = (Select ProgId ")
            .Append("                   from arPrgVersions ")
            .Append("                   where PrgVerId in ( Select PrgVerId ")
            .Append("                                       from arStuEnrollments ")
            .Append("                                       where StuEnrollId = ?)) or  t11.ProgId is Null ) ")
            .Append("and t0.ClsSectionId not in ")
            .Append("(Select t7.TestId from arResults t7 where t7.StuEnrollId = ?  ")
            'added by Theresa G on 6/11/2009 for course equivalency for mantis 16452
            '------------------------------------------------------------------------'
            .Append(" union ")
            .Append(" select ClsSectionId from arClassSections where Reqid in( ")
            .Append(" select c.EquivReqid from arResults a,arClassSections b,arCourseEquivalent  c,arGradeSystemDetails d  where ")
            .Append(" a.TestId=b.ClsSectionId and a.StuEnrollId = ? ")
            .Append(" and b.Reqid=c.Reqid and c.EquivReqid =t0.Reqid and a.GrdSysDetailId=d.GrdSysDetailId and d.IsPass=1) ")
            .Append(" ) ")
            '------------------------------------------------------------------------'
            '.Append("and t11.StartDate <= t8.StartDate ")
            '.Append("and t0.StartDate <= t8.StartDate and  ")
            'commented and modified by Theresa G on oct 29 2010 for rally issue 
            'DE1140: Student's Scheduled tab does not show the class sections if it is current. 
            '.Append("  and t0.StartDate >= t8.StartDate and  ")
            .Append("  and t0.EndDate >= t8.StartDate and  ")
            .Append("  t0.EndDate<=t8.ExpGradDate and t8.PrgVerId=t12.PrgVerId and t9.ReqId ")
            '=t12.Reqid
            .Append(" in ")
            .Append(" (select Distinct ReqId from arProgVerDef where PrgVerId=t12.PrgVerId ")
            .Append(" Union Select Distinct ReqId from arReqGrpDef where GrpId in (select Distinct ReqId ")
            .Append(" from arProgVerDef where PrgVerId=t12.PrgVerId)) ")
            .Append(" and t9.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId=?) ")
            ''Added by SAraswathi lakshmanan to show the ClassSections related to the student campus related to the student
            ''Added on mAy 12 2010
            ''To fix Mantis case : 18921 data Fix issues for Ross
            .Append(" and T0.CampusID=T8.CampusID  ")

            .Append(" Union ")
            .Append("Select t0.ClsSectionId, t0.ClsSection, t0.ReqId,t11.TermDescrip,t11.TermId,t11.StartDate AS TermStart, t0.StartDate,t0.EndDate, t9.Code, t9.Descrip ")
            .Append("from arClassSections t0, arStuEnrollments t8, ")
            .Append("(SELECT t1.ReqId,ReqSeq,IsRequired,t2.Descrip,t2.Code,t2.ReqTypeId,t2.Hours,t2.Credits,t1.ModUser,t1.ModDate  ")
            .Append(" FROM arReqGrpDef t1, arReqs t2 ")
            .Append(" WHERE t1.ReqId = t2.ReqId and t1.GrpId in ( ")
            .Append(" select distinct t0.ReqId from arReqs t0,arProgVerDef t1,arReqGrpDef t2 where PrgVerId= ")
            .Append("(select PrgVerId from arStuEnrollments where StuEnrollId = ?) ")
            .Append(" and t1.ReqId=t2.GrpId and t0.ReqId=t1.reqid)) t9, ")
            .Append(" arClassSectionTerms t10, arTerm t11 where ")
            .Append("t0.ReqId not in ")
            .Append("( ")
            .Append("Select t2.ReqId from arReqs t2, arResults t3,arClassSections t4, arGradeSystemDetails t5 ")
            .Append("where t3.StuEnrollId = ? ")
            .Append("and t3.GrdSysDetailId is NOT NULL and t3.GrdSysDetailId = t5.GrdSysDetailId and t5.IsPass = 1 ")
            .Append("and t3.TestId = t4.ClsSectionId and t4.ReqId = t2.ReqId ")
            .Append(" ) ")
            .Append("and t0.ReqId = t9.ReqId ")
            .Append("and t0.ClsSectionId = t10.ClsSectionId ")
            .Append("and t10.TermId = t11.TermId and t0.ShiftId = t8.ShiftId ")
            .Append("and t8.StuEnrollId = ? ")
            ''progId can be null or notnull
            ''So, ProgId is null clause added
            ''Query Modified by Saraswathi Lakshmanan 4-Nov 2008
            '.Append("and t11.ProgId = (Select ProgId ")
            '.Append("                   from arPrgVersions ")
            '.Append("                   where PrgVerId in ( Select PrgVerId ")
            '.Append("                                       from arStuEnrollments ")
            '.Append("                                       where StuEnrollId = ?)) ")
            .Append(" and (t11.ProgId = (Select ProgId ")
            .Append("                   from arPrgVersions ")
            .Append("                   where PrgVerId in ( Select PrgVerId ")
            .Append("                                       from arStuEnrollments ")
            .Append("                                       where StuEnrollId = ?)) or t11.ProgId is Null) ")
            .Append(" and t0.ClsSectionId not in ")
            .Append("(Select t7.TestId from arResults t7 where t7.StuEnrollId = ? ) ")
            .Append(" and t9.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId=?) ")
            '.Append("and t11.StartDate <= t8.StartDate ")
            '.Append("and t0.StartDate >= t8.StartDate and t0.EndDate<=t8.ExpGradDate ")
            'commented and modified by Theresa G on oct 29 2010 for rally issue 
            'DE1140: Student's Scheduled tab does not show the class sections if it is current. 
            .Append("and t0.EndDate >= t8.StartDate and t0.EndDate<=t8.ExpGradDate ")
            ''Added by SAraswathi lakshmanan to show the ClassSections related to the student campus related to the student
            ''Added on mAy 12 2010
            ''To fix Mantis case : 18921 data Fix issues for Ross
            .Append(" and T0.CampusID=T8.CampusID ")
            .Append("ORDER BY t11.StartDate desc,t9.Descrip,t0.StartDate ")
        End With

        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Dim ds As New DataSet
        Dim stuStartDate As String = String.Empty
        Dim aRows() As DataRow
        ds = db.RunParamSQLDataSet(sb.ToString)
        If MyAdvAppSettings.AppSettings("SchedulingMethod", Nothing) = "ModuleStart" Then
            For Each row As DataRow In ds.Tables(0).Rows
                If (IsDuplicateClassSection(row("ClsSectionId").ToString) = True) Then
                    stuStartDate = GetStudentStartDate(stuEnrollId)
                    aRows = ds.Tables(0).Select("ClsSectionId='" & row("ClsSectionId").ToString & "' AND TermStart=#" & CDate(stuStartDate) & "#")
                    If aRows.Length > 0 Then
                        If (stuStartDate <> CDate(row("TermStart")).ToShortDateString) Then
                            row.Delete()
                        End If
                    Else
                        'aRows = ds.Tables(0).Select("TestId='" & row("TestId").ToString & "'")
                        aRows = ds.Tables(0).Select("ClsSectionId='" & row("ClsSectionId").ToString & "' AND TermStart <> #" & row("TermStart") & "#")
                        If (aRows.Length > 0) Then
                            row.Delete()
                        End If
                    End If
                End If
            Next
            ds.AcceptChanges()
        End If
        Return ds.Tables(0)
    End Function
    Private Function IsDuplicateClassSection(ByVal clsSectionId As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append(" select ClsSectionId from arClassSectionTerms where ClsSectionId= ?   group by ClsSectionId having count(ClsSectionId)>1 ")
        End With
        db.AddParameter("@clsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Dim ds As New DataSet
        ds = db.RunParamSQLDataSet(sb.ToString)
        If (ds.Tables(0).Rows.Count > 0) Then
            Return True
        End If
        Return False
    End Function
    Private Function GetStudentStartDate(ByVal stuEnrollID) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT ExpStartDate FROM arStuEnrollments WHERE StuEnrollId= ? ")
        End With
        db.AddParameter("@StuEnrollId", stuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLScalar(sb.ToString)
    End Function
    Public Function GetConflictingClassSectionsUsingRotationalSchedule(ByVal ReqId As String, ByVal startDate As Date, _
            ByVal endDate As Date, ByVal campusId As String, ByVal shiftId As String, ByVal clsSectionId As String, _
            Optional ByVal IsModule As Boolean = True) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT t2.ClsSectionId,")
            ''ClsSection Added by Saraswathi lakshmanan on August 18 2009
            ''To fix mantis 06478
            '.Append(" t3.Descrip,t2.StartDate,t2.EndDate,t3.Code ")
            .Append(" (t3.Descrip + ' - '+ t2.ClsSection)  as Descrip ,t2.StartDate,t2.EndDate,t3.Code ")
            .Append("FROM arTerm t1, arClassSections t2, arReqs t3 ")
            .Append("WHERE t1.TermId = t2.TermId ")
            .Append("AND t2.ReqId = t3.ReqId ")
            If IsModule Then
                .Append("AND t1.IsModule = 1 ")
            Else
                .Append("AND t1.IsModule = 0 ")
            End If
            .Append("AND ((t2.StartDate = ? AND t2.EndDate = ?) ")
            .Append("OR ")
            .Append("(t2.StartDate BETWEEN ? AND ?) ")
            .Append("OR ")
            .Append("(t2.EndDate BETWEEN ? AND ?) ")
            .Append("OR ")
            .Append("(t2.StartDate < ? AND t2.EndDate > ?)) ")
            .Append("AND t2.CampusId = ? ")

            If shiftId <> "" Then
                'Modified by Michelle R. Rodriguez on 05/05/2006
                'Retrieve class sections with the same shift or no shift.
                .Append("AND (t2.ShiftId = ? OR t2.ShiftId IS NULL) ")
            End If

            If clsSectionId <> "" Then
                .Append("AND t2.ClsSectionId <> ? ")
            End If

            .Append("AND t2.ReqId IN(  ")
            .Append("SELECT distinct t400.ReqId as ReqId ")
            .Append("FROM arReqs t3,arProgVerDef t400 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t3.ReqTypeId = 1 AND ")
            .Append("t400.PrgVerId IN( SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arReqs t3,arProgVerDef t400, arPrgVersions t4 ")
            .Append("                   WHERE t400.PrgVerId = t4.PrgVerId ")
            .Append("                   AND t400.ReqId = t3.ReqId ")
            .Append("                   AND t3.ReqTypeId = 1 ")
            .Append("                   AND t400.ReqId = ? ")
            .Append("                   UNION ")
            .Append("                   SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqs t701 ")
            .Append("                   WHERE t3.ReqId = t400.ReqId ")
            .Append("                   AND t3.ReqTypeId = 2 ")
            .Append("                   AND t400.ReqId = t700.GrpId ")
            .Append("                   AND t700.ReqId = t701.ReqId ")
            .Append("                   AND t701.ReqTypeId = 1 ")
            .Append("                   AND t700.ReqId = ?) ")
            .Append("UNION ")
            .Append("SELECT distinct t700.ReqId as ReqId ")
            .Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqs t701 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t700.ReqId = t701.ReqId AND ")
            .Append("t701.ReqTypeId = 1 AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t400.PrgVerId IN( SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arReqs t3,arProgVerDef t400, arPrgVersions t4 ")
            .Append("                   WHERE t400.PrgVerId = t4.PrgVerId ")
            .Append("                   AND t400.ReqId = t3.ReqId ")
            .Append("                   AND t3.ReqTypeId = 1 ")
            .Append("                   AND t400.ReqId = ? ")
            .Append("                   UNION ")
            .Append("                   SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqs t701 ")
            .Append("                   WHERE t3.ReqId = t400.ReqId ")
            .Append("                   AND t3.ReqTypeId = 2 ")
            .Append("                   AND t400.ReqId = t700.GrpId ")
            .Append("                   AND t700.ReqId = t701.ReqId ")
            .Append("                   AND t701.ReqTypeId = 1 ")
            .Append("                   AND t700.ReqId = ?) ")
            .Append(") ")
            .Append("ORDER BY t1.StartDate ")
        End With

        db.AddParameter("@sdate", startDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@edate", endDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@sdate", startDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@edate", endDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@sdate", startDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@edate", endDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@sdate", startDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@edate", endDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        If shiftId <> "" Then
            db.AddParameter("@shiftid", shiftId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        If clsSectionId <> "" Then
            db.AddParameter("@csid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        db.AddParameter("@reqid", ReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", ReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", ReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", ReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function
    Public Function GetOtherCoursesInPrgVersion(ByVal reqId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT ReqId,Descrip FROM ")
            .Append("(")
            .Append("SELECT distinct t400.ReqId as ReqId,t3.Descrip ")
            .Append("FROM arReqs t3,arProgVerDef t400 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t400.PrgVerId IN( SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arProgVerDef t400, arPrgVersions t4 ")
            .Append("                   WHERE t400.PrgVerId = t4.PrgVerId ")
            .Append("                   AND t400.ReqId = ? ")
            .Append("                   UNION ")
            .Append("                   SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700 ")
            .Append("                   WHERE t3.ReqId = t400.ReqId ")
            .Append("                   AND t3.ReqTypeId = 2 ")
            .Append("                   AND t400.ReqId = t700.GrpId ")
            .Append("                   AND t400.ReqId = ? ")
            .Append("                   UNION ")
            .Append("                   SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("                   WHERE t3.ReqId = t400.ReqId ")
            .Append("                   AND t3.ReqTypeId = 2 ")
            .Append("                   AND t400.ReqId = t700.GrpId ")
            .Append("                   AND t700.ReqId = t800.GrpId ")
            .Append("                   AND t400.ReqId = ?) ")
            .Append("UNION ")
            .Append("SELECT distinct t700.ReqId as ReqId,t3.Descrip ")
            .Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t400.PrgVerId IN ( SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arProgVerDef t400, arPrgVersions t4 ")
            .Append("                   WHERE t400.PrgVerId = t4.PrgVerId ")
            .Append("                   AND t400.ReqId = ? ")
            .Append("                   UNION ")
            .Append("                   SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700 ")
            .Append("                   WHERE t3.ReqId = t400.ReqId ")
            .Append("                   AND t3.ReqTypeId = 2 ")
            .Append("                   AND t400.ReqId = t700.GrpId ")
            .Append("                   AND t400.ReqId = ? ")
            .Append("                   UNION ")
            .Append("                   SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("                   WHERE t3.ReqId = t400.ReqId ")
            .Append("                   AND t3.ReqTypeId = 2 ")
            .Append("                   AND t400.ReqId = t700.GrpId ")
            .Append("                   AND t700.ReqId = t800.GrpId ")
            .Append("                   AND t400.ReqId = ?) ")
            .Append("UNION ")
            .Append("SELECT distinct t800.ReqId as ReqId,t3.Descrip ")
            .Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t400.PrgVerId IN ( SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arProgVerDef t400, arPrgVersions t4 ")
            .Append("                   WHERE t400.PrgVerId = t4.PrgVerId ")
            .Append("                   AND t400.ReqId = ? ")
            .Append("                   UNION ")
            .Append("                   SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700 ")
            .Append("                   WHERE t3.ReqId = t400.ReqId ")
            .Append("                   AND t3.ReqTypeId = 2 ")
            .Append("                   AND t400.ReqId = t700.GrpId ")
            .Append("                   AND t400.ReqId = ? ")
            .Append("                   UNION ")
            .Append("                   SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("                   WHERE t3.ReqId = t400.ReqId ")
            .Append("                   AND t3.ReqTypeId = 2 ")
            .Append("                   AND t400.ReqId = t700.GrpId ")
            .Append("                   AND t700.ReqId = t800.GrpId ")
            .Append("                   AND t400.ReqId = ?) ")
            .Append("UNION ")
            .Append("SELECT distinct t900.ReqId as ReqId,t3.Descrip ")
            .Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t400.PrgVerId IN ( SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arProgVerDef t400, arPrgVersions t4 ")
            .Append("                   WHERE t400.PrgVerId = t4.PrgVerId ")
            .Append("                   AND t400.ReqId = ? ")
            .Append("                   UNION ")
            .Append("                   SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700 ")
            .Append("                   WHERE t3.ReqId = t400.ReqId ")
            .Append("                   AND t3.ReqTypeId = 2 ")
            .Append("                   AND t400.ReqId = t700.GrpId ")
            .Append("                   AND t400.ReqId = ? ")
            .Append("                   UNION ")
            .Append("                   SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("                   WHERE t3.ReqId = t400.ReqId ")
            .Append("                   AND t3.ReqTypeId = 2 ")
            .Append("                   AND t400.ReqId = t700.GrpId ")
            .Append("                   AND t700.ReqId = t800.GrpId ")
            .Append("                   AND t400.ReqId = ?) ")
            .Append("UNION ")
            .Append("SELECT distinct t1000.ReqId as ReqId,t3.Descrip ")
            .Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t400.PrgVerId IN ( SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arProgVerDef t400, arPrgVersions t4 ")
            .Append("                   WHERE t400.PrgVerId = t4.PrgVerId ")
            .Append("                   AND t400.ReqId = ? ")
            .Append("                   UNION ")
            .Append("                   SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700 ")
            .Append("                   WHERE t3.ReqId = t400.ReqId ")
            .Append("                   AND t3.ReqTypeId = 2 ")
            .Append("                   AND t400.ReqId = t700.GrpId ")
            .Append("                   AND t400.ReqId = ? ")
            .Append("                   UNION ")
            .Append("                   SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("                   WHERE t3.ReqId = t400.ReqId ")
            .Append("                   AND t3.ReqTypeId = 2 ")
            .Append("                   AND t400.ReqId = t700.GrpId ")
            .Append("                   AND t700.ReqId = t800.GrpId ")
            .Append("                   AND t400.ReqId = ?) ")
            .Append("UNION ")
            .Append("SELECT distinct t1100.ReqId as ReqId,t3.Descrip ")
            .Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000, arReqGrpDef t1100 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t1000.ReqId = t1100.GrpId AND ")
            .Append("t400.PrgVerId IN ( SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arProgVerDef t400, arPrgVersions t4 ")
            .Append("                   WHERE t400.PrgVerId = t4.PrgVerId ")
            .Append("                   AND t400.ReqId = ? ")
            .Append("                   UNION ")
            .Append("                   SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700 ")
            .Append("                   WHERE t3.ReqId = t400.ReqId ")
            .Append("                   AND t3.ReqTypeId = 2 ")
            .Append("                   AND t400.ReqId = t700.GrpId ")
            .Append("                   AND t400.ReqId = ? ")
            .Append("                   UNION ")
            .Append("                   SELECT distinct t400.PrgVerId ")
            .Append("                   FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("                   WHERE t3.ReqId = t400.ReqId ")
            .Append("                   AND t3.ReqTypeId = 2 ")
            .Append("                   AND t400.ReqId = t700.GrpId ")
            .Append("                   AND t700.ReqId = t800.GrpId ")
            .Append("                   AND t400.ReqId = ?) ")
            .Append(") P ")
            .Append("WHERE ReqId <> ? ")
        End With

        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)


    End Function
    'Public Function GetExpGradDateUsingRotationalSchedule(ByVal prgVerId As String, ByVal expStartDate As String, ByVal campusId As String, ByVal shiftId As String) As String
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim maxDate As String

    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

    '    'If the prgVerId, expStartDate or campusId is empty then we should return an empty string
    '    If prgVerId = "" Or expStartDate = "" Or campusId = "" Then
    '        Return ""
    '    End If

    '    With sb
    '        .Append("SELECT Max(EndDate) ")
    '        .Append("FROM ")
    '        .Append("(SELECT t1.TermId,t1.StartDate,t1.EndDate,t3.Descrip,t2.ClsSectionId,t2.CampusId,t2.ShiftId,t2.ClsSection,t2.ReqId ")
    '        .Append("FROM arTerm t1, arClassSections t2, arReqs t3, ")
    '        .Append("(SELECT distinct t400.ReqId as ReqId ")
    '        .Append("FROM arPrgVersions t100, arReqs t3,arProgVerDef t400 ")
    '        .Append("WHERE t3.ReqId = t400.ReqId AND ")
    '        .Append("t100.PrgVerId = t400.PrgVerId AND ")
    '        .Append("t100.PrgVerId = ? ")
    '        .Append("UNION ")
    '        .Append("SELECT distinct t700.ReqId as ReqId ")
    '        .Append("FROM arPrgVersions t100, arReqs t3,arProgVerDef t400, arReqGrpDef t700 ")
    '        .Append("WHERE t3.ReqId = t400.ReqId AND ")
    '        .Append("t100.PrgVerId = t400.PrgVerId AND ")
    '        .Append("t3.ReqTypeId = 2 AND ")
    '        .Append("t400.ReqId = t700.GrpId AND ")
    '        .Append("t100.PrgVerId = ? ")
    '        .Append("UNION ")
    '        .Append("SELECT distinct t800.ReqId as ReqId ")
    '        .Append("FROM arPrgVersions t100, arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800 ")
    '        .Append("WHERE t3.ReqId = t400.ReqId AND ")
    '        .Append("t100.PrgVerId = t400.PrgVerId AND ")
    '        .Append("t3.ReqTypeId = 2 AND ")
    '        .Append("t400.ReqId = t700.GrpId AND ")
    '        .Append("t700.ReqId = t800.GrpId AND ")
    '        .Append("t100.PrgVerId = ? ")
    '        .Append(")  t4 ")
    '        .Append("WHERE t1.TermId = t2.TermId ")
    '        .Append("AND t2.ReqId = t3.ReqId ")
    '        .Append("AND t1.IsModule = 1 ")
    '        .Append("AND t2.ReqId = t4.ReqId ")
    '        .Append("AND t1.StartDate >= ? ")
    '        .Append("AND t2.CampusId = ? ")

    '        If shiftId <> "" Then
    '            .Append("AND t2.ShiftId = ? ")
    '        Else
    '            .Append("AND t2.ShiftId IS NULL ")
    '        End If

    '        .Append("AND t2.MaxStud > (SELECT COUNT(*) ")
    '        .Append("FROM arResults ar ")
    '        .Append("WHERE ar.TestId = t2.ClsSectionId) ")
    '        .Append(") P ")
    '    End With

    '    db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@sdate", expStartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
    '    db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '    If shiftId <> "" Then
    '        db.AddParameter("@shiftid", shiftId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    End If

    '    Dim obj As System.Object
    '    obj = db.RunParamSQLScalar(sb.ToString)

    '    Try
    '        maxDate = CStr(db.RunParamSQLScalar(sb.ToString))
    '        Return maxDate
    '    Catch ex As Exception
    '        Return ""
    '    End Try

    'End Function

    Public Function GetClassSectionsForStudentWhenEnrolling(ByVal prgVerId As String, ByVal expStartDate As String, ByVal campusId As String, ByVal shiftId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT t1.TermId,t1.StartDate,t1.EndDate,t3.Descrip,t2.ClsSectionId,t2.CampusId,t2.ShiftId,t2.ClsSection,t2.ReqId ")
            .Append("FROM arTerm t1, arClassSections t2, arReqs t3, ")
            .Append("(SELECT distinct t400.ReqId as ReqId ")
            .Append("FROM arPrgVersions t100, arReqs t3,arProgVerDef t400 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t700.ReqId as ReqId ")
            .Append("FROM arPrgVersions t100, arReqs t3,arProgVerDef t400, arReqGrpDef t700 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t100.PrgVerId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t800.ReqId as ReqId ")
            .Append("FROM arPrgVersions t100, arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t100.PrgVerId = ? ")
            .Append(")  t4 ")
            .Append("WHERE t1.TermId = t2.TermId ")
            .Append("AND t2.ReqId = t3.ReqId ")
            .Append("AND t1.IsModule = 1 ")
            .Append("AND t2.ReqId = t4.ReqId ")
            .Append("AND t1.StartDate >= ? ")
            .Append("AND t2.CampusId = ? ")

            If shiftId <> "" Then
                .Append("AND t2.ShiftId = ? ")
            Else
                .Append("AND t2.ShiftId IS NULL ")
            End If

            .Append("AND t2.MaxStud > (SELECT COUNT(*) ")
            .Append("FROM arResults ar ")
            .Append("WHERE ar.TestId = t2.ClsSectionId) ")
            .Append("ORDER BY t1.StartDate ")
        End With

        db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sdate", expStartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        If shiftId <> "" Then
            db.AddParameter("@shiftid", shiftId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)


    End Function

    Public Sub UpdateExpectedGradDateUsingRotationalSchedule(ByVal stuEnrollId As String)
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("UPDATE arStuEnrollments ")
            .Append("SET ExpGradDate = (SELECT MAX(t2.EndDate) ")
            .Append("                   FROM arResults t1, arClassSections t2 ")
            .Append("                   WHERE t1.StuEnrollId = arStuEnrollments.StuEnrollId ")
            .Append("                   AND t1.TestId = t2.ClsSectionId) ")
            .Append("WHERE arStuEnrollments.StuEnrollId = ? ")
        End With

        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)


    End Sub

    Public Function GetCoursesForProgram(ByVal programId As String, Optional ByVal campusId As String = "", Optional ByVal activeOnly As Boolean = False) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT * FROM ")
            .Append("( ")
            .Append("SELECT distinct t400.ReqId as ReqId, t3.Descrip As Req,'(' + t3.Code  + ')' + ' ' + t3.Descrip AS FullDescrip ")
            .Append("FROM arReqs t3,arProgVerDef t400, systatuses s ")
            .Append("WHERE t3.ReqId = t400.ReqId AND t3.ReqTypeId = 1 and ")
            .Append("t3.StatusId=s.StatusId and ")

            If activeOnly = True Then
                .Append("s.Status = 'Active' and ")
            End If

            If campusId <> "" Then
                'We only want courses that belong to the all campus group or campus groups that have this campus
                .Append(" ( ")
                .Append("       t3.CampGrpId = (SELECT CampGrpId FROM dbo.syCampGrps WHERE CampGrpCode='All') ")
                .Append("                                           OR                                        ")
                .Append("       t3.CampGrpId IN(SELECT CampGrpId FROM dbo.syCmpGrpCmps WHERE CampusId='" & campusId & "') ")
                .Append("    ) and ")
            End If

            .Append("t400.PrgVerId IN(SELECT PrgVerId ")
            .Append("                 FROM arPrgVersions ")
            .Append("                 WHERE ProgId = ?) ")
            .Append("UNION ")
            .Append("SELECT distinct t700.ReqId as ReqId, (select Descrip from arReqs where ReqId=t700.ReqId) As Req,'(' + (select Code from arReqs where ReqId=t700.ReqId)  + ')' + ' ' + (select Descrip from arReqs where ReqId=t700.ReqId) AS FullDescrip ")
            .Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t400.PrgVerId IN(SELECT PrgVerId ")
            .Append("                 FROM arPrgVersions ")
            .Append("                 WHERE ProgId = ?) ")
            .Append(") R ")
            .Append("ORDER BY Req ")
        End With

        db.AddParameter("@prgverid", programId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prgverid", programId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   Execute the query       
        db.OpenConnection()

        Try
            Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw ex
            Else
                Throw ex.InnerException
            End If
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try
    End Function

    Public Function GetCoursesForTerm(ByVal programId As String, ByVal TermID As String, ByVal CampusId As String, Optional ByVal activeOnly As Boolean = False) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT distinct R.ReqId,FullDescrip,Req  FROM ")
            .Append("( ")
            .Append("SELECT distinct t400.ReqId as ReqId, t3.Descrip As Req,'(' + t3.Code  + ')' + ' ' + t3.Descrip AS FullDescrip ")
            .Append("FROM arReqs t3,arProgVerDef t400,syCampGrps, syStatuses s ")
            .Append("WHERE t3.ReqId = t400.ReqId AND t3.ReqTypeId = 1 and t3.CampGrpId=syCampGrps.CampGrpId ")
            .Append(" and (syCampGrps.CampGrpId in (Select Distinct CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
            .Append(" or (syCampGrps.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL'))) ")
            .Append(" and t400.PrgVerId IN(SELECT PrgVerId ")
            .Append("                 FROM arPrgVersions ")
            .Append("                 WHERE ProgId = ?) ")
            .Append("and t3.StatusId=s.StatusId ")

            If activeOnly = True Then
                .Append("and s.Status = 'Active' ")
            End If

            .Append("UNION ")
            .Append("SELECT distinct t700.ReqId as ReqId, (select Descrip from arReqs where ReqId=t700.ReqId) As Req,'(' + (select Code from arReqs where ReqId=t700.ReqId)  + ')' + ' ' + (select Descrip from arReqs where ReqId=t700.ReqId) AS FullDescrip ")
            .Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700,syCampGrps ")
            ''ReqTypeId cannot be 1 and 2
            ''So, the condition is removed
            ''Modified by saraswathi Lakshmanan on Nov 3rd 2008
            ''t3.ReqTypeId = 1 and t3.ReqTypeId = 2 and 
            .Append("WHERE t3.ReqId = t400.ReqId AND t3.CampGrpId=syCampGrps.CampGrpId  ")
            .Append(" and (syCampGrps.CampGrpId in (Select Distinct CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
            .Append(" or (syCampGrps.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL'))) ")
            .Append("  AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t400.PrgVerId IN(SELECT PrgVerId ")
            .Append("                 FROM arPrgVersions ")
            .Append("                 WHERE ProgId = ?) ")
            .Append(") R ,arClassSections T where  R.ReqID=T.ReqID and T.TermId= ? ")
            .Append(" ORDER BY Req ")
        End With

        db.AddParameter("@campusid", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prgverid", programId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@campusid", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prgverid", programId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@termId", TermID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   Execute the query       
        db.OpenConnection()

        Try
            Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw ex
            Else
                Throw ex.InnerException
            End If
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try
    End Function
    Public Function GetProgramForTerm(ByVal TermId As String) As String

        Dim ProgramID As String
        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("select ProgId from arTerm where TermID=? ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@TermID", TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)


            While dr.Read()
                If Not (dr("ProgId") Is System.DBNull.Value) Then
                    ProgramID = dr("ProgId").ToString()
                Else
                    ProgramID = ""
                End If
            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ProgramID

    End Function
    Public Function GetCourseGroup(ByVal ReqId As String) As Integer

        Dim intGetTotalcount As Integer = 0
        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("select Count(*) as GetTotalCount from arReqs where ReqTypeId=2 and ReqId=? ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ReqID", ReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()
            'Execute the query
            intGetTotalcount = db.RunParamSQLScalar(sb.ToString)
            'Close Connection
            db.CloseConnection()
        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try
        Return intGetTotalcount
    End Function
End Class
