Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Text
Imports System.Web.UI
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.AdvantageV1.Common
Imports FAME.Advantage.Common

Public Class ProbationDB
    Enum eProbationType
        AcademicProb = 1
        DisciplinaryProb = 2
        Warning = 3
    End Enum
    ''' <summary>
    ''' Store the connection string for this class
    ''' </summary>
    ''' <remarks></remarks>
    Private ReadOnly conString As String

    ''' <summary>
    ''' Application Setting for get some other different values from config.
    ''' </summary>
    ''' <remarks></remarks>                                                                                                             
    Private ReadOnly myAdvAppSettings As AdvAppSettings
    Sub New()
        myAdvAppSettings = AdvAppSettings.GetAppSettings()
        conString = myAdvAppSettings.AppSettings("ConString")
    End Sub
    Public Function GetProbationStatuses() As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        With sb
            .Append("SELECT distinct a.ProbWarningTypeId, a.Descrip ")
            .Append("FROM arProbWarningTypes a ")
            .Append("WHERE a.ProbWarningTypeId in (2,3) ")

        End With


        db.OpenConnection()
        ds = db.RunSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    Public Function GetWarningsOnly() As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        With sb
            .Append("SELECT distinct a.ProbWarningTypeId, a.Descrip ")
            .Append("FROM arProbWarningTypes a ")
            .Append("WHERE a.ProbWarningTypeId = 3 ")

        End With


        db.OpenConnection()
        ds = db.RunSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    Public Function GetStudentStatus(ByVal StuEnrollId As String) As StateEnum
        Dim da As OleDbDataAdapter
        Dim Status As Integer
        Dim EnumType As StateEnum

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder


            '   build the sql query
            With sb
                .Append("SELECT b.SysStatusId FROM syStatusCodes b,arStuEnrollments a ")
                .Append(" WHERE a.StatusCodeId = b.StatusCodeId and  ")
                .Append(" a.StuEnrollId = ? ")
            End With


            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)



            '   Execute the query
            'db.OpenConnection()

            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                Status = dr("SysStatusId")
            End While

            EnumType = CType(Status, StateEnum)

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return EnumType

    End Function

    'Public Function PlaceStdonProbation(ByVal StuEnrollId As String, ByVal Reason As String, ByVal StartDate As String, ByVal EndDate As String, ByVal ProbType As String, ByVal user As String) As String
    '    Dim db As New DataAccess
    '    Dim sb As New System.Text.StringBuilder
    '    Dim ds As DataSet
    '    Dim status As String

    '    db.OpenConnection()
    '    'Set the connection string
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

    '    '   we must encapsulate all DB updates in one transaction
    '    Dim groupTrans As OleDbTransaction = db.StartTransaction()

    '    Try

    '        With sb
    '            .Append("INSERT INTO arStuProbWarnings(StuProbWarningId,StuEnrollId,ProbWarningTypeId,StartDate,EndDate,Reason,ModUser,ModDate) ")
    '            .Append("VALUES(?,?,?,?,?,?,?,?)")
    '        End With

    '        db.AddParameter("@TransferID", System.Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.AddParameter("@courseid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@ProbWarning", ProbType, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

    '        db.AddParameter("@code", StartDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        If EndDate = "" Then
    '            db.AddParameter("@enddate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@enddate", EndDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        db.AddParameter("@hours", Reason, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'ModDate
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)


    '        '   Execute the query
    '        'db.OpenConnection()
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)


    '        If ProbType = 1 Then
    '            'ProbType = 1 ===> Academic Probation.
    '            'Disciplinary Probation is treated as a warning. There is not need to update the enrollment status code.
    '            status = GetAcademicProbationStatus()
    '            ''''Change status to 'Currently Attending - Probation Type
    '            ''''status = GetCurrAttendingProbationStatus(ProbType)
    '            If status <> "" Then
    '                With sb
    '                    .Append("UPDATE arStuEnrollments Set StatusCodeId = ? ")
    '                    .Append("WHERE StuEnrollId = ?")
    '                End With

    '                db.AddParameter("@StatusCodeId", status, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

    '                db.ClearParameters()
    '                sb.Remove(0, sb.Length)
    '            End If
    '        End If

    '        groupTrans.Commit()

    '        '   return without errors
    '        Return ""
    '        'Catch ex As System.Exception
    '        '    'Return an Error To Client
    '        '    Return -1
    '    Catch ex As OleDbException
    '        groupTrans.Rollback()
    '        '   do not report sql lost connection
    '        If Not groupTrans.Connection Is Nothing Then
    '            ' Report the error
    '        End If
    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)

    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    'End Function
    'Public Function PlaceStdonSuspension(ByVal StuEnrollId As String, ByVal SuspensionStatusId As String, ByVal Reason As String, ByVal StartDate As String, ByVal EndDate As String, ByVal user As String) As String
    '    Dim db As New DataAccess
    '    Dim sb As New System.Text.StringBuilder
    '    Dim ds As DataSet
    '    Dim status As String

    '    db.OpenConnection()
    '    'Set the connection string
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

    '    '   we must encapsulate all DB updates in one transaction
    '    Dim groupTrans As OleDbTransaction = db.StartTransaction()

    '    Try

    '        With sb
    '            .Append("INSERT INTO arStdSuspensions(StuSuspensionId,StuEnrollId,StartDate,EndDate,Reason,ModUser,ModDate) ")
    '            .Append("VALUES(?,?,?,?,?,?,?)")
    '        End With

    '        db.AddParameter("@TransferID", System.Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.AddParameter("@courseid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        db.AddParameter("@code", StartDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        If EndDate = "" Then
    '            db.AddParameter("@enddate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@enddate", EndDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        db.AddParameter("@hours", Reason, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'ModDate
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)


    '        '   Execute the query
    '        'db.OpenConnection()
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)


    '        ''''Change status to 'Currently Attending - Probation Type
    '        ''''status = GetCurrAttendingProbationStatus(ProbType)
    '        If SuspensionStatusId <> "" Then
    '            With sb
    '                .Append("UPDATE arStuEnrollments Set StatusCodeId = ? ")
    '                .Append("WHERE StuEnrollId = ?")
    '            End With

    '            db.AddParameter("@StatusCodeId", SuspensionStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

    '            db.ClearParameters()
    '            sb.Remove(0, sb.Length)
    '        End If

    '        groupTrans.Commit()

    '        '   return without errors
    '        Return ""
    '        'Catch ex As System.Exception
    '        '    'Return an Error To Client
    '        '    Return -1
    '    Catch ex As OleDbException
    '        groupTrans.Rollback()
    '        '   do not report sql lost connection
    '        If Not groupTrans.Connection Is Nothing Then
    '            ' Report the error
    '        End If
    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)

    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    'End Function

    ' '' Code modified by kamalesh Ahuja on 25th and 26 August to resolve mantis issue id 19485
    'Public Function PlaceStudentOnProbation(ByVal StuEnrollId As String, ByVal Reason As String, ByVal StartDate As String, ByVal EndDate As String, ByVal ProbType As String, ByVal user As String) As String
    '    Dim db As New DataAccess
    '    Dim sb As New System.Text.StringBuilder
    '    Dim ds As DataSet
    '    Dim status As String

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If


    '    db.OpenConnection()
    '    'Set the connection string
    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   we must encapsulate all DB updates in one transaction
    '    '' Dim groupTrans As OleDbTransaction = db.StartTransaction()

    '    Try


    '        If ProbType = 1 Then
    '            'ProbType = 1 ===> Academic Probation.
    '            'Disciplinary Probation is treated as a warning. There is not need to update the enrollment status code.
    '            status = GetAcademicProbationStatus()
    '            ''''Change status to 'Currently Attending - Probation Type
    '            ''''status = GetCurrAttendingProbationStatus(ProbType)
    '            If status <> "" Then
    '                With sb
    '                    .Append("UPDATE arStuEnrollments Set StatusCodeId = ?, ModDate=? ")
    '                    ''New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19485
    '                    .Append(",ModUser=? ")
    '                    ''New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19485
    '                    .Append("WHERE StuEnrollId = ?")
    '                End With

    '                db.AddParameter("@StatusCodeId", status, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '                ''New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19485
    '                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '                ''New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19485
    '                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '                db.RunParamSQLExecuteNoneQuery(sb.ToString)

    '                db.ClearParameters()
    '                sb.Remove(0, sb.Length)
    '            End If

    '        End If


    '        Dim StatusChangeId As String

    '        With sb
    '            .Append("select top 1 StudentStatusChangeId from syStudentStatusChanges where StuEnrollId=? and NewStatusId=?  order by ModDate desc ")

    '        End With
    '        db.AddParameter("@stdenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@StatusCodeId", status, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        StatusChangeId = CType(db.RunParamSQLScalar(sb.ToString), Guid).ToString

    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)

    '        With sb
    '            .Append("INSERT INTO arStuProbWarnings(StuProbWarningId,StuEnrollId,ProbWarningTypeId,StartDate,EndDate,Reason,ModUser,ModDate,StudentStatusChangeId) ")
    '            .Append("VALUES(?,?,?,?,?,?,?,?,?)")
    '        End With

    '        db.AddParameter("@TransferID", System.Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.AddParameter("@courseid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@ProbWarning", ProbType, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

    '        db.AddParameter("@code", StartDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        If EndDate = "" Then
    '            db.AddParameter("@enddate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@enddate", EndDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        db.AddParameter("@hours", Reason, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'ModDate
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        db.AddParameter("@StatusChangeId", StatusChangeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        '   Execute the query
    '        'db.OpenConnection()
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)


    '        ''groupTrans.Commit()

    '        '   return without errors
    '        Return ""
    '        'Catch ex As System.Exception
    '        '    'Return an Error To Client
    '        '    Return -1
    '    Catch ex As OleDbException
    '        'groupTrans.Rollback()
    '        ''   do not report sql lost connection
    '        'If Not groupTrans.Connection Is Nothing Then
    '        '    ' Report the error
    '        'End If
    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)

    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    'End Function
    ' ''
    ' '' Code modified by kamalesh Ahuja on 25th and 26 August to resolve mantis issue id 19485
    'Public Function ChangeStatusToSuspension(ByVal StuEnrollId As String, ByVal SuspensionStatusId As String, ByVal Reason As String, ByVal StartDate As String, ByVal EndDate As String, ByVal user As String) As String
    '    Dim db As New DataAccess
    '    Dim sb As New System.Text.StringBuilder
    '    Dim ds As DataSet
    '    Dim status As String

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If


    '    db.OpenConnection()
    '    'Set the connection string
    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   we must encapsulate all DB updates in one transaction
    '    '' Dim groupTrans As OleDbTransaction = db.StartTransaction()

    '    Try

    '        ''''Change status to 'Currently Attending - Probation Type
    '        ''''status = GetCurrAttendingProbationStatus(ProbType)
    '        If SuspensionStatusId <> "" Then
    '            With sb
    '                .Append("UPDATE arStuEnrollments Set StatusCodeId = ?,ModDate=? ")
    '                ''New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19485
    '                .Append(",ModUser=? ")
    '                ''New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19485
    '                .Append("WHERE StuEnrollId = ?")
    '            End With

    '            db.AddParameter("@StatusCodeId", SuspensionStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            ''New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19485
    '            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            ''New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19485
    '            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '            db.RunParamSQLExecuteNoneQuery(sb.ToString)

    '            db.ClearParameters()
    '            sb.Remove(0, sb.Length)
    '        End If

    '        Dim StatusChangeId As String

    '        With sb
    '            .Append("select top 1 StudentStatusChangeId from syStudentStatusChanges where StuEnrollId=? and NewStatusId=?  order by ModDate desc ")

    '        End With
    '        db.AddParameter("@stdenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@StatusCodeId", SuspensionStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        StatusChangeId = CType(db.RunParamSQLScalar(sb.ToString), Guid).ToString

    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)


    '        With sb
    '            .Append("INSERT INTO arStdSuspensions(StuSuspensionId,StuEnrollId,StartDate,EndDate,Reason,ModUser,ModDate,StudentStatusChangeId) ")
    '            .Append("VALUES(?,?,?,?,?,?,?,?)")
    '        End With

    '        db.AddParameter("@TransferID", System.Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.AddParameter("@courseid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        db.AddParameter("@code", StartDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        If EndDate = "" Then
    '            db.AddParameter("@enddate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@enddate", EndDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        db.AddParameter("@hours", Reason, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'ModDate
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        db.AddParameter("@StatusChangeId", StatusChangeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        '   Execute the query
    '        'db.OpenConnection()
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)


    '        '' groupTrans.Commit()

    '        '   return without errors
    '        Return ""
    '        'Catch ex As System.Exception
    '        '    'Return an Error To Client
    '        '    Return -1
    '    Catch ex As OleDbException
    '        '' groupTrans.Rollback()
    '        '   do not report sql lost connection
    '        'If Not groupTrans.Connection Is Nothing Then
    '        '    ' Report the error
    '        'End If
    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)

    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    'End Function
    ' ''

    Public Function GetStudentLOAs(ByVal StuEnrollId As String) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim da As New OleDbDataAdapter
        Dim ds As New DataSet
        Dim count As Integer


        With sb
            .Append("SELECT a.StudentLOAId, a.StartDate,a.EndDate,a.LOAReturnDate, b.Descrip, a.LOARequestDate ")
            .Append("from arStudentLOAs a, arLOAReasons b ")
            .Append("where a.StuEnrollId = ?  and a.LOAReasonId = b.LOAReasonId ")
        End With

        ' Add the ClsSectId to the parameter list
        db.AddParameter("@termId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        Try
            ds = db.RunParamSQLDataSet(sb.ToString)
        Catch ex As Exception
            'Redirect to error page.
            Throw New Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        db.ClearParameters()
        Return ds
    End Function
    Public Function GetStudentProbations(ByVal StuEnrollId As String, Optional ByVal probationType As Integer = 0) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim da As New OleDbDataAdapter
        Dim ds As New DataSet
        Dim count As Integer



        With sb
            '.Append("Select a.StuProbWarningId, a.StartDate, a.EndDate, a.Reason, b.Descrip ")
            '.Append("from arStuProbWarnings a, arProbWarningTypes b ")
            '.Append("where a.StuEnrollId = ?")
            .Append("Select a.StuProbWarningId, a.StartDate, a.EndDate, a.Reason, b.Descrip, ")
            .Append("statusCode.StatusCodeDescrip as StatusDescription, a.StudentStatusChangeId  ")
            .Append("from arStuProbWarnings a  ")
            .Append("inner join arProbWarningTypes b on a.ProbWarningTypeId = b.ProbWarningTypeId  ")
            .Append("left join syStudentStatusChanges statusChange on statusChange.StudentStatusChangeId = a.StudentStatusChangeId  ")
            .Append("left join syStatusCodes statusCode on statusCode.StatusCodeId = statusChange.NewStatusId  ")
            .Append("where a.StuEnrollId = ? ")

        End With

        If (probationType > 0) Then
            sb.Append(" and a.ProbWarningTypeId = " + probationType.ToString())
        End If


        ' Add the ClsSectId to the parameter list
        db.AddParameter("@termId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        Try
            ds = db.RunParamSQLDataSet(sb.ToString)
        Catch ex As Exception
            'Redirect to error page.
            Throw New Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        db.ClearParameters()
        Return ds
    End Function
    Public Function GetStudentWarnings(ByVal StuEnrollId As String) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim da As New OleDbDataAdapter
        Dim ds As New DataSet
        Dim count As Integer

        With sb
            .Append("Select a.StuProbWarningId, a.StartDate, a.EndDate, a.Reason ")
            .Append("from arStuProbWarnings a ")
            .Append("where a.StuEnrollId = ? and a.ProbWarningTypeId = 3  ")
        End With

        ' Add the ClsSectId to the parameter list
        db.AddParameter("@termId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        Try
            ds = db.RunParamSQLDataSet(sb.ToString)
        Catch ex As Exception
            'Redirect to error page.
            Throw New Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        db.ClearParameters()
        Return ds
    End Function
    Public Function GetStudentSuspensions(ByVal StuEnrollId As String) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim da As New OleDbDataAdapter
        Dim ds As New DataSet
        Dim count As Integer



        With sb
            .Append("Select a.StuSuspensionId, a.StartDate, a.EndDate, a.Reason ")
            .Append("from arStdSuspensions a ")
            .Append("where a.StuEnrollId = ?  ")
        End With

        ' Add the ClsSectId to the parameter list
        db.AddParameter("@termId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        Try
            ds = db.RunParamSQLDataSet(sb.ToString)
        Catch ex As Exception
            'Redirect to error page.
            Throw New Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        db.ClearParameters()
        Return ds
    End Function
    Public Function GetCurrAttendingProbationStatus(Optional ByVal probationType As String = "2") As String
        Dim sStatusCodeId As String
        Try

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT DISTINCT A.StatusCodeID as StatusCodeID,A.StatusCodeDescrip as StatusCodeDescrip ")
                .Append("FROM syStatusCodes A ")
                .Append("WHERE A.sysStatusId = 9 ")
                If probationType = "2" Then
                    .Append("and A.DiscProbation = 1 ")
                ElseIf probationType = "1" Then
                    .Append("and A.AcadProbation = 1 ")
                End If

            End With


            db.OpenConnection()
            'Execute the query
            Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString)


            While dr.Read()
                sStatusCodeId = dr("StatusCodeID").ToString
            End While


            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return sStatusCodeId

    End Function

    Public Function GetAcademicProbationStatus() As String
        Dim sStatusCodeId As String = String.Empty
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        '   connect to the database
        Dim db As New DataAccess
        Try
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT DISTINCT ")
                .Append("       A.StatusCodeID AS StatusCodeID,")
                .Append("       A.StatusCodeDescrip AS StatusCodeDescrip ")
                .Append("FROM   syStatusCodes A ")
                .Append("WHERE  A.sysStatusId = 20 ")
            End With

            db.OpenConnection()

            'Execute the query
            Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString)

            While dr.Read()
                sStatusCodeId = dr("StatusCodeID").ToString
            End While

            If Not dr.IsClosed Then dr.Close()
            'Return status code Id
            Return sStatusCodeId

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try
    End Function

    Public Function GetSuspensionStatus() As String
        Dim sStatusCodeId As String = String.Empty

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        '   connect to the database
        Dim db As New DataAccess
        Try
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT DISTINCT ")
                .Append("       A.StatusCodeID AS StatusCodeID,")
                .Append("       A.StatusCodeDescrip AS StatusCodeDescrip ")
                .Append("FROM   syStatusCodes A ")
                .Append("WHERE  A.sysStatusId = 11 ")
            End With

            db.OpenConnection()

            'Execute the query
            Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString)

            While dr.Read()
                sStatusCodeId = dr("StatusCodeID").ToString
            End While

            If Not dr.IsClosed Then dr.Close()
            'Return status code Id
            Return sStatusCodeId


        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try


    End Function
    Public Function GetSuspensionStatuses() As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        With sb
            .Append("SELECT SSC.StatusCodeId, SSC.StatusCode, SSC.StatusCodeDescrip " + vbCrLf)
            .Append("FROM dbo.syStatusCodes AS SSC " + vbCrLf)
            .Append("INNER Join dbo.syStatuses AS SS ON SS.StatusId = SSC.StatusId " + vbCrLf)
            .Append("WHERE SSC.SysStatusId = 11 " + vbCrLf)
            .Append("And SS.Status = 'Active' " + vbCrLf)
        End With

        db.OpenConnection()
        ds = db.RunSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    '' Call fill status
    '' Parameters sysStatusList  String  "11" --> suspension
    ''            Optional ShowActiveOnly = True filter only active, Default false. If is Omited or false should not filter
    ''            Optional CampusId  Current Campus Id, Defalut Null, if is ommited, then get all status for the corp campus group.
    ''            .DataSource = facade.GetStatusList( "11", "Active", campusid) 
    Public Function GetStatusCodeForAGivenList(sysStatusIdList As String, Optional showActiveOnly As Boolean = False, Optional campusId As String = Nothing) As DataSet
        '   Instantiate DAL component
        Dim ds As New DataSet()
        Dim guidCampusId As Guid
        If Not Guid.TryParse(campusId, guidCampusId) Then
            guidCampusId = Nothing
        End If
        Using myConnection As New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
            Using myCommand As New SqlCommand("USP_GetStatusCodeForAGivenList", myConnection)
                myCommand.CommandType = CommandType.StoredProcedure
                myCommand.Parameters.Add(New SqlParameter("@sysStatusIdList", SqlDbType.NVarChar)).Value = sysStatusIdList
                myCommand.Parameters.Add(New SqlParameter("@ShowActiveOnly", SqlDbType.Bit)).Value = showActiveOnly
                myCommand.Parameters.Add(New SqlParameter("@CampusId", SqlDbType.UniqueIdentifier)).Value = guidCampusId
                Dim mySqlDataAdapter As New SqlDataAdapter(myCommand)
                mySqlDataAdapter.Fill(ds)
            End Using
        End Using
        Return ds
    End Function
    Public Function GetProbationTypes() As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        With sb
            .Append("SELECT distinct a.ProbWarningTypeId, a.Descrip ")
            .Append("FROM arProbWarningTypes a ")
            .Append("WHERE a.ProbWarningTypeId in (" + CType(eProbationType.DisciplinaryProb, Integer).ToString() + "," + CType(eProbationType.Warning, Integer).ToString() + ") ")
        End With


        db.OpenConnection()
        ds = db.RunSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    Public Function GetProbationTypeFromStatusCode(ByVal statusCodeGuid As String) As Integer
        Dim sStatusCodeId As String = String.Empty
        Try

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT ProbWarningTypeId FROM arProbWarningTypes types ")
                .Append("INNER JOIN sySysStatus AS systemStatus ON systemStatus.SysStatusId = types.SysStatusId  ")
                .Append("INNER JOIN syStatusCodes AS syStatusCodes on syStatusCodes.SysStatusId = systemStatus.SysStatusId ")
                .Append("WHERE syStatusCodes.StatusCodeId=?")
            End With

            db.AddParameter("@StatusCodeId", statusCodeGuid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)


            While dr.Read()
                sStatusCodeId = dr("ProbWarningTypeId").ToString
            End While


            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            If (Not String.IsNullOrEmpty(sStatusCodeId)) Then
                Return CType(sStatusCodeId, Integer)
            Else
                Return 0
            End If

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset

    End Function
End Class
