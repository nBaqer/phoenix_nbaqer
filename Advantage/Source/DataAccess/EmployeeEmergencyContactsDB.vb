Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' EmployeeEmergencyContactsDB.vb
'
' EmployeeEmergencyContactsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class EmployeeEmergencyContactsDB
    Public Function GetAllEmployeeEmergencyContacts(ByVal empId As String, ByVal statusSelectIndex As Integer) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        ''   instantiate names and values for the relationShip
        'Dim names() As String = System.Enum.GetNames(GetType(Relationship))
        'Dim values() As Integer = System.Enum.GetValues(GetType(Relationship))

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   EEC.EmployeeEmergencyContactId, ")
            .Append("         EEC.EmployeeEmergencyContactName, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As StatusSelect, ")
            .Append("         EEC.RelationId, ")
            .Append("         (Select RelationDescrip from syRelations where RelationId=EEC.RelationId) As Relation ")
            '.Append("         (Case EEC.Relation ")
            'For i As Integer = 0 To names.Length - 1
            '    .Append("   when " + values(i).ToString + " then '" + names(i) + "' ")
            'Next
            '.Append("         end ) as RelationShip ")
            .Append("FROM     hrEmployeeEmergencyContacts EEC, syStatuses ST ")
            .Append("WHERE    EEC.EmpId = ? ")
            .Append("AND      EEC.StatusId = ST.StatusId ")
            '   Conditionally include only Active, Inactive or All Items 
            Select Case statusSelectIndex
                Case 0
                    .Append("AND    ST.Status = 'Active' ")
                    .Append("ORDER BY EEC.EmployeeEmergencyContactName ")
                Case 1
                    .Append("AND    ST.Status = 'Inactive' ")
                    .Append("ORDER BY EEC.EmployeeEmergencyContactName ")
                Case Else
                    .Append("ORDER BY ST.Status, EEC.EmployeeEmergencyContactName ")
            End Select

        End With

        ' Add the EmployeeId to the parameter list
        db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetEmployeeEmergencyContactInfo(ByVal employeeEmergencyContactId As String) As EmployeeEmergencyContactInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT EEC.EmployeeEmergencyContactId, ")
            .Append("    EEC.EmployeeEmergencyContactName, ")
            .Append("    EEC.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=EEC.StatusId) As Status, ")
            .Append("    EEC.EmpId, ")
            .Append("    EEC.RelationId, ")
            .Append("    (Select RelationDescrip from syRelations where RelationId=EEC.RelationId) As Relation, ")
            .Append("    EEC.WorkPhone, ")
            .Append("    EEC.HomePhone, ")
            .Append("    EEC.CellPhone, ")
            .Append("    EEC.Beeper, ")
            .Append("    EEC.WorkEmail, ")
            .Append("    EEC.HomeEmail, ")
            .Append("    EEC.ModUser, ")
            .Append("    EEC.ModDate,EEC.ForeignHomePhone,EEC.ForeignWorkPhone,EEC.ForeignCellPhone ")
            .Append("FROM  hrEmployeeEmergencyContacts EEC ")
            .Append("WHERE EEC.EmployeeEmergencyContactId = ? ")
        End With

        ' Add the EmployeeEmergencyContactId to the parameter list
        db.AddParameter("@EmployeeEmergencyContactId", employeeEmergencyContactId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim EmployeeEmergencyContactInfo As New EmployeeEmergencyContactInfo

        While dr.Read()

            '   set properties with data from DataReader
            With EmployeeEmergencyContactInfo
                .EmployeeEmergencyContactId = CType(dr("EmployeeEmergencyContactId"), Guid).ToString
                .IsInDB = True
                .Name = dr("EmployeeEmergencyContactName")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .EmpId = CType(dr("EmpId"), Guid).ToString
                .RelationId = CType(dr("RelationId"), Guid).ToString
                If Not dr("Relation") Is System.DBNull.Value Then .Relation = dr("Relation")
                If Not (dr("WorkPhone") Is System.DBNull.Value) Then .WorkPhone = dr("WorkPhone") Else .WorkPhone = ""
                If Not (dr("HomePhone") Is System.DBNull.Value) Then .HomePhone = dr("HomePhone") Else .HomePhone = ""
                If Not (dr("CellPhone") Is System.DBNull.Value) Then .CellPhone = dr("CellPhone") Else .CellPhone = ""
                If Not (dr("Beeper") Is System.DBNull.Value) Then .Beeper = dr("Beeper") Else .Beeper = ""
                If Not (dr("WorkEmail") Is System.DBNull.Value) Then .WorkEmail = dr("WorkEmail")
                If Not (dr("HomeEmail") Is System.DBNull.Value) Then .HomeEmail = dr("HomeEmail")
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
                .ForeignHomePhone = dr("ForeignHomePhone")
                .ForeignWorkPhone = dr("ForeignWorkPhone")
                .ForeignCellPhone = dr("ForeignCellPhone")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return EmployeeEmergencyContactInfo
        Return EmployeeEmergencyContactInfo

    End Function
    Public Function UpdateEmployeeEmergencyContactInfo(ByVal EmployeeEmergencyContactInfo As EmployeeEmergencyContactInfo, ByVal user As String) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   Connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'do an update
        Try
            'build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE hrEmployeeEmergencyContacts Set EmployeeEmergencyContactId = ?, EmployeeEmergencyContactName = ?, ")
                .Append(" StatusId = ?, RelationId = ?, ")
                .Append(" WorkPhone = ?, HomePhone = ?,  CellPhone = ?, ")
                .Append(" Beeper = ?, WorkEmail = ?, HomeEmail = ?, ")
                .Append(" ModUser = ?, ModDate = ?,ForeignHomePhone=?,ForeignCellPhone=?,ForeignWorkPhone=? ")
                .Append("WHERE EmployeeEmergencyContactId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from hrEmployeeEmergencyContacts where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   EmployeeEmergencyContactId
            db.AddParameter("@EmployeeEmergencyContactId", EmployeeEmergencyContactInfo.EmployeeEmergencyContactId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   EnrollmentId
            db.AddParameter("@EmployeeEmergencyContactName", EmployeeEmergencyContactInfo.Name, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", EmployeeEmergencyContactInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   RelationId
            db.AddParameter("@RelationId", EmployeeEmergencyContactInfo.RelationId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   WorkPhone
            db.AddParameter("@WorkPhone", EmployeeEmergencyContactInfo.WorkPhone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   HomePhone
            db.AddParameter("@HomePhone", EmployeeEmergencyContactInfo.HomePhone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CellPhone
            db.AddParameter("@CellPhone", EmployeeEmergencyContactInfo.CellPhone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Beeper
            db.AddParameter("@Beeper", EmployeeEmergencyContactInfo.Beeper, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   WorkEmail
            db.AddParameter("@WorkEmail", EmployeeEmergencyContactInfo.WorkEmail, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   HomeEmail
            db.AddParameter("@HomeEmail", EmployeeEmergencyContactInfo.HomeEmail, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   Foreign HomePhone
            db.AddParameter("@ForeignHomePhone", EmployeeEmergencyContactInfo.ForeignHomePhone, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   Foreign CellPhone
            db.AddParameter("@ForeignCellPhone", EmployeeEmergencyContactInfo.ForeignCellPhone, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   Foreign Work Phone
            db.AddParameter("@ForeignWorkPhone", EmployeeEmergencyContactInfo.ForeignWorkPhone, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   EmployeeEmergencyContactId
            db.AddParameter("@AdmDepositId", EmployeeEmergencyContactInfo.EmployeeEmergencyContactId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", EmployeeEmergencyContactInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddEmployeeEmergencyContactInfo(ByVal EmployeeEmergencyContactInfo As EmployeeEmergencyContactInfo, ByVal user As String) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   Connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT hrEmployeeEmergencyContacts (EmployeeEmergencyContactId, EmployeeEmergencyContactName, StatusId, ")
                .Append("   EmpId, RelationId, ")
                .Append("   WorkPhone, HomePhone, CellPhone, ")
                .Append("   Beeper, WorkEmail, HomeEmail, ")
                .Append("   ModUser, ModDate,ForeignHomePhone,ForeignCellPhone,ForeignWorkPhone) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   EmployeeEmergencyContactId
            db.AddParameter("@EmployeeEmergencyContactId", EmployeeEmergencyContactInfo.EmployeeEmergencyContactId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   EmployeeEmergencyContactName
            db.AddParameter("@EmployeeEmergencyContactName", EmployeeEmergencyContactInfo.Name, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", EmployeeEmergencyContactInfo.StatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   EmpId
            db.AddParameter("@EmpId", EmployeeEmergencyContactInfo.EmpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   RelationId
            db.AddParameter("@RelationId", EmployeeEmergencyContactInfo.RelationId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   WorkPhone
            db.AddParameter("@WorkPhone", EmployeeEmergencyContactInfo.WorkPhone, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   HomePhone
            db.AddParameter("@HomePhone", EmployeeEmergencyContactInfo.HomePhone, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   CellPhone
            db.AddParameter("@CellPhone", EmployeeEmergencyContactInfo.CellPhone, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Beeper
            db.AddParameter("@Beeper", EmployeeEmergencyContactInfo.Beeper, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   WorkEmail
            db.AddParameter("@WorkEmail", EmployeeEmergencyContactInfo.WorkEmail, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   HomeEmail
            db.AddParameter("@HomeEmail", EmployeeEmergencyContactInfo.HomeEmail, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   Foreign HomePhone
            db.AddParameter("@ForeignHomePhone", EmployeeEmergencyContactInfo.ForeignHomePhone, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   Foreign CellPhone
            db.AddParameter("@ForeignCellPhone", EmployeeEmergencyContactInfo.ForeignCellPhone, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   Foreign Work Phone
            db.AddParameter("@ForeignWorkPhone", EmployeeEmergencyContactInfo.ForeignWorkPhone, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteEmployeeEmergencyContactInfo(ByVal EmployeeEmergencyContactId As String, ByVal modDate As DateTime) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   Connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM hrEmployeeEmergencyContacts ")
                .Append("WHERE EmployeeEmergencyContactId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM hrEmployeeEmergencyContacts WHERE EmployeeEmergencyContactId = ? ")
            End With

            '   add parameters values to the query

            '   EmployeeEmergencyContactId
            db.AddParameter("@EmployeeEmergencyContactId", EmployeeEmergencyContactId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   EmployeeEmergencyContactId
            db.AddParameter("@EmployeeEmergencyContactId", EmployeeEmergencyContactId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
End Class
