' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' CampusGroupsDB.vb
'
' CampusGroupsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================

Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class CampusGroupsDB
    Public Function GetAllCampusGroups() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   CG.CampGrpId, CG.CampGrpDescrip, CG.StatusId ")
            .Append("FROM     syCampGrps CG, syStatuses ST ")
            .Append("WHERE    CG.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY CG.CampGrpDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllCampusGroupsByUser(ByVal userId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT DISTINCT A.CampGrpId, B.CampGrpDescrip,A.CampGrpId, B.IsAllCampusGrp ")
            .Append("FROM syUsersRolesCampGrps A, syCampGrps B ")
            .Append("WHERE A.CampGrpId = B.CampGrpId ")
            .Append("AND A.UserId=? ")
            .Append("ORDER BY B.IsAllCampusGrp desc, B.CampGrpDescrip")
        End With
        db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllIndividualCampusGroupsByUserAndResourceID(ByVal userId As String, resourceId As Integer) As DataSet

        ' connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' build the sql query
        With sb
            .Append("SELECT * FROM dbo.UserAccessPerResource ( ? ,? ) ")
        End With
        db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@resourceId", resourceId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)

        Return db.RunParamSQLDataSet(sb.ToString)

    End Function

    Public Function GetCampusGroupsByUserAndResourceIDWithAccess(ByVal userId As String, resourceId As Integer, CampusId As String) As DataSet

        ' connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' build the sql query
        With sb
            .Append("SELECT * FROM dbo.UserAccessPerResourceForItems ( ? ,? ,?) ")
        End With
        db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@resourceId", resourceId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)

        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetCampusGroupsItems(ByVal userId As String, resourceId As Integer, CampusId As String, originalDataSet As DataSet) As DataSet
        Dim dataSetAccess = GetCampusGroupsByUserAndResourceIDWithAccess(userId, resourceId, CampusId)
        If originalDataSet.Tables(0).Rows.Count > 0 Then
            If Not originalDataSet.Tables(0).Columns.Contains("AccessLevel") Then
                originalDataSet.Tables(0).Columns.Add("AccessLevel")
            End If

            For Each dr As DataRow In originalDataSet.Tables(0).Rows
                    Dim hasCampGrp = dataSetAccess.Tables(0).Select("CampGrpId = '" + dr("CampGrpId").ToString() + "'")
                    If hasCampGrp.Any Then
                        dr("AccessLevel") = hasCampGrp.First()("AccessLevel")
                    End If

                Next
                originalDataSet.EnforceConstraints = False
                If originalDataSet.Tables(0).Select("AccessLevel IS NOT NULL").Count > 0 Then
                    Dim RV = originalDataSet.Tables(0).Select("AccessLevel IS NOT NULL").CopyToDataTable
                    originalDataSet.Tables(0).Clear()

                    originalDataSet.Tables(0).Merge(RV)
                    RV.Dispose()
                Else
                    originalDataSet.Tables(0).Clear()

                End If

            End If

            Return originalDataSet
    End Function


    Public Function GetAllTermsByUser(ByVal UserId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT DISTINCT A.CampGrpId, B.CampGrpDescrip,A.CampGrpId ")
            .Append("FROM syUsersRolesCampGrps A, syCampGrps B ")
            .Append("WHERE A.CampGrpId = B.CampGrpId ")
            .Append("AND A.UserId=? ")
            .Append("ORDER BY B.CampGrpDescrip")
        End With
        db.AddParameter("@UserId", UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllCampusesUsedByLeads() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT DISTINCT ")
            .Append("         C.CampusId, C.StatusId, C.CampDescrip ")
            .Append("FROM     adLeads L, syCampuses C, syStatuses ST ")
            .Append("WHERE ")
            .Append("         L.CampusId=C.CampusId ")
            .Append(" AND     C.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY C.CampDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllCampusesUsedByStudents() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT DISTINCT ")
            .Append("         C.CampusId, C.StatusId, C.CampDescrip ")
            .Append("FROM     arStuEnrollments SE, syCampuses C, syStatuses ST ")
            .Append("WHERE ")
            .Append("         SE.CampusId=C.CampusId ")
            .Append(" AND     C.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY C.CampDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllCampusEnrollment() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   Distinct CG.CampusId, CG.StatusId, CG.CampDescrip ")
            .Append("FROM     syCampuses CG, syStatuses ST ")
            .Append("WHERE    CG.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY CG.CampDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllCampusesByCampusId(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   Distinct CG.CampusId, CG.StatusId, CG.CampDescrip ")
            .Append("FROM     syCampuses CG, syStatuses ST ")
            .Append("WHERE    CG.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append(" AND     CampusId IN (Select CampusId from syCmpGrpCmps where CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? )) ")
            .Append("ORDER BY CG.CampDescrip ")
        End With

        '   add CampusId parameter
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetCampusDetailsByCampusId(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   Distinct CG.CampusId, CG.StatusId, CG.CampDescrip, CG.Email ")
            .Append("FROM     syCampuses CG, syStatuses ST ")
            .Append("WHERE    CG.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append(" AND     CampusId = ?  ")
            .Append("ORDER BY CG.CampDescrip ")
        End With

        '   add CampusId parameter
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        '''
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllCampusEnrollmentByCampus(ByVal CampusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   Distinct CG.CampusId, CG.StatusId, CG.CampDescrip ")
            .Append("FROM     syCampuses CG, syStatuses ST ")
            .Append("WHERE    CG.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' and CG.CampusId = ? ")
            .Append("ORDER BY CG.CampDescrip ")
        End With

        '   add CampusId parameter
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetTermByCampus(ByVal CampusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append(" select t1.TermId,t1.TermDescrip from arTerm t1,syStatuses t2 ")
            .Append(" where t1.StatusId = t2.StatusId and t2.Status='Active' and t1.CampGrpId in ")
            .Append(" (select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
        End With

        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetCampusesByUser(ByVal userId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT ")
            .Append("        C.CampusId, ")
            .Append("        C.CampDescrip ")
            .Append("FROM syCampuses C, syStatuses S ")
            .Append("WHERE ")
            .Append("        C.StatusId = S.StatusId ")
            .Append("AND ")
            .Append("        S.Status = 'Active' ")
            .Append("AND ")
            .Append("       CampusId in ")
            .Append("           (Select CampusId from syCmpGrpCmps where CampGrpId in ")
            .Append("               (select CampgrpId from syUsersRolesCampGrps where UserId = ? )) ")
            .Append("ORDER BY C.CampDescrip ")
        End With

        ' Add empId to the parameter list
        db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    Public Function GetCampusDescrip(ByVal campusId As String) As String
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT CampDescrip ")
            .Append("FROM syCampuses ")
            .Append("WHERE CampusId = ?")
        End With

        ' Add empId to the parameter list
        db.AddParameter("@cmpdid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Return db.RunParamSQLScalar(sb.ToString).ToString()
    End Function

    ''' <summary>
    ''' Get the campus Grp Description
    ''' </summary>
    ''' <param name="campusGrpId"></param>
    ''' <returns>Campus Group Description String</returns>
    Public Function GetCampusGroupDescrip(ByVal campusGrpId As String) As String
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = GetAdvAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT CampGrpDescrip ")
            .Append("FROM syCampGrps ")
            .Append("WHERE CampGrpId = ?")
        End With

        ' Add empId to the parameter list
        db.AddParameter("@campusGrpId", campusGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Return db.RunParamSQLScalar(sb.ToString).ToString()
    End Function

    Public Function GetCampusDataAsReceiptAddress(ByVal campusId As String) As ReceiptAddress
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT CampDescrip, ")
            .Append("       Address1, ")
            .Append("       Address2, ")
            .Append("       City, ")
            .Append("       (select StateDescrip from syStates where StateId=C.StateId) As State, ")
            .Append("       Zip ")
            .Append("FROM syCampuses C ")
            .Append("WHERE CampusId = ?")
        End With

        ' Add empId to the parameter list
        db.AddParameter("@cmpdid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim receiptAddress As New ReceiptAddress()

        While dr.Read()

            '   set properties with data from DataReader
            With receiptAddress
                If Not (dr("CampDescrip") Is DBNull.Value) Then .SchoolName = dr("CampDescrip")
                If Not (dr("Address1") Is DBNull.Value) Then .SchoolAddress1 = dr("Address1")
                If Not (dr("Address2") Is DBNull.Value) Then .SchoolAddress2 = dr("Address2")
                If Not (dr("City") Is DBNull.Value) Then .City = dr("City")
                If Not (dr("State") Is DBNull.Value) Then .State = dr("State")
                If Not (dr("Zip") Is DBNull.Value) Then .Zip = dr("Zip")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'return receiptAddress
        Return receiptAddress
    End Function

    ''Added by SAraswathi Lakshmanan on April 13 2010
    ''Converted to stored Procedure
    Public Function GetCampusDataAsReceiptAddress_sp(ByVal campusId As String) As ReceiptAddress
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' Add empId to the parameter list
        db.AddParameter("@CAmpusID", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As SqlDataReader = db.RunParamSQLDataReader_SP("USP_SA_GetCampusDataAsReceiptAddress")

        Dim receiptAddress As New ReceiptAddress()

        While dr.Read()

            '   set properties with data from DataReader
            With receiptAddress
                If Not (dr("CampDescrip") Is DBNull.Value) Then .SchoolName = dr("CampDescrip")
                If Not (dr("Address1") Is DBNull.Value) Then .SchoolAddress1 = dr("Address1")
                If Not (dr("Address2") Is DBNull.Value) Then .SchoolAddress2 = dr("Address2")
                If Not (dr("City") Is DBNull.Value) Then .City = dr("City")
                If Not (dr("State") Is DBNull.Value) Then .State = dr("State")
                If Not (dr("Zip") Is DBNull.Value) Then .Zip = dr("Zip")
                If Not (dr("Country") Is DBNull.Value) Then .Country = dr("Country")
            End With

        End While

        'Close Connection
        db.CloseConnection()

        'return receiptAddress
        Return receiptAddress
    End Function


    Public Function InsertCampusIntoAllCampusGroup(ByVal campusid As String, ByVal Code As String, ByVal sStatusId As String, ByVal Description As String, ByVal user As String)
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim AllCampGrpId As String = GetAllCampGrpId()
        Dim NewCampGrpId As String = Guid.NewGuid.ToString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("INSERT INTO syCmpGrpCmps(CampGrpId,CampusId,ModUser,ModDate) ")
            .Append("VALUES(?,?,?,?)")
        End With

        db.AddParameter("@campgrpid", AllCampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@campusid", campusid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        With sb
            .Append("INSERT INTO syCampGrps(CampGrpId,CampusId,CampGrpCode,StatusId,CampGrpDescrip,ModUser,ModDate) ")
            .Append("VALUES(?,?,?,?,?,?,?)")
        End With

        db.AddParameter("@campgrpidnew", NewCampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@campusid2", campusid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@campgrpcode", Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@statusid", sStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@campgrpdescrip", Description, DataAccess.OleDbDataType.OleDbString, 80, ParameterDirection.Input)
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        With sb
            .Append("INSERT INTO syCmpGrpCmps(CampGrpId,CampusId,ModUser,ModDate) ")
            .Append("VALUES(?,?,?,?)")
        End With

        db.AddParameter("@campgrpid2", NewCampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@campusid3", campusid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModUser2", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ModDate2", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

    End Function


    Public Function UpdateCampusBranch(ByVal campusid As String, ByVal isBranch As Boolean, Optional parentCampusId As Guid? = Nothing) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("UPDATE syCampuses SET IsBranch = ?, ParentCampusId = ? where CampusId = ? ")

        End With

        db.AddParameter("@isBranch", isBranch, DataAccess.OleDbDataType.OleDbBoolean)
        db.AddParameter("@parentCampusId", If(parentCampusId Is Nothing, DBNull.Value, parentCampusId), DataAccess.OleDbDataType.OleDbGuid)
        db.AddParameter("@campusid", campusid, DataAccess.OleDbDataType.OleDbString)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)


    End Function
    Public Function GetAllCampGrpId() As String
        Dim sCampGrpId As String = String.Empty

        '   connect to the database
        Dim db As New DataAccess
        Dim myAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       A.CampGrpId ")
            .Append("FROM   syCampGrps A ")
            .Append("WHERE  A.IsAllCampusGrp = 1 ")
        End With

        db.OpenConnection()
        Try
            'Execute the query
            Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString)
            While dr.Read()
                sCampGrpId = dr("CampGrpId").ToString
            End While
            If Not dr.IsClosed Then dr.Close()

        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try

        'Return status code Id
        Return sCampGrpId
    End Function

    Public Function DeleteCampusFrmCampGrps(ByVal sCampusId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try
            'Delete from database and clear the screen as well. 
            'And set the mode to "NEW" since the record has been deleted from the db.
            With sb
                .Append("DELETE FROM syCmpGrpCmps ")
                .Append("WHERE CampusId =  ? ")
            End With
            db.AddParameter("@campusid", sCampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            db.ClearParameters()
            sb.Remove(0, sb.Length)

            With sb
                .Append("DELETE FROM syCampGrps ")
                .Append("WHERE CampusId =  ? ")
            End With
            db.AddParameter("@campid", sCampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
            '   commit transaction 
            groupTrans.Commit()

            '   return without errors
            Return ""
            'Catch ex As System.Exception
            '    'Return an Error To Client
            '    Return -1
        Catch ex As OleDbException
            groupTrans.Rollback()
            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            End If
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetAllCampGroupsByCampusId(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid

        '   build the sql query
        With sb
            .Append("  SELECT Distinct t2.CampGrpId, t2.CampGrpDescrip FROM syCmpGrpCmps t1,syCampGrps t2 WHERE t1.CampGrpId=t2.CampGrpId and t1.CampusId=? and t2.StatusId=? ")
            .Append(" Order By t2.CampGrpDescrip ")
        End With

        '   add CampusId parameter
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StatusId", strActiveGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetStatusCodesExceptEnrolledStatus(ByVal strCampGrpId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid

        '   build the sql query
        With sb
            .Append("  SELECT Distinct StatusCodeId from syStatusCodes where sysStatusId = 6 and StatusId=? and CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
        End With

        '   add CampusId parameter
        db.AddParameter("@StatusId", strActiveGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetCorporateInfoFromCampusId(ByVal campusId As String) As CorporateInfo
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim CorpInfo As New CorporateInfo

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        With sb
            .Append("SELECT ")
            .Append("       CampDescrip AS CorporateName,Address1,Address2,City,Zip,TranscriptAuthZnTitle,TranscriptAuthZnName,Website,CampDescrip,")
            .Append("       (SELECT StateDescrip FROM syStates WHERE StateId=C.StateId) AS State,")
            .Append("       (SELECT CountryDescrip FROM adCountries WHERE CountryId=C.CountryId) AS Country,Fax,")
            .Append("       Phone = CASE WHEN (Phone1 IS NULL) THEN (CASE WHEN (Phone2 IS NULL) THEN (CASE WHEN (Phone3 IS NULL) THEN '' ELSE Phone3 END) ELSE Phone2 END) ELSE Phone1 END ")
            .Append("FROM   syCampuses C ")
            .Append("WHERE  CampusId = ? ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        db.AddParameter("campusid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read
            With CorpInfo
                If Not (dr("CorporateName") Is DBNull.Value) Then .CorporateName = dr("CorporateName")
                If Not (dr("Address1") Is DBNull.Value) Then .Address1 = dr("Address1")
                If Not (dr("Address2") Is DBNull.Value) Then .Address2 = dr("Address2")
                If Not (dr("City") Is DBNull.Value) Then .City = dr("City")
                If Not (dr("Zip") Is DBNull.Value) Then .Zip = dr("Zip")
                If Not (dr("State") Is DBNull.Value) Then .State = dr("State")
                If Not (dr("Country") Is DBNull.Value) Then .Country = dr("Country")
                If Not (dr("Phone") Is DBNull.Value) Then .Phone = dr("Phone")
                If Not (dr("Fax") Is DBNull.Value) Then .Fax = dr("Fax")
                If Not (dr("TranscriptAuthznTitle") Is DBNull.Value) Then .TranscriptAuthznTitle = dr("TranscriptAuthznTitle")
                If Not (dr("TranscriptAuthznName") Is DBNull.Value) Then .TranscriptAuthznName = dr("TranscriptAuthznName")
                If Not (dr("Website") Is DBNull.Value) Then .Website = dr("Website")
            End With
        End While


        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return CorporateInfo
        Return CorpInfo

    End Function
    Public Function GetCorporateInfoFromCampusId_SP(ByVal campusId As String) As CorporateInfo


        Dim CorpInfo As New CorporateInfo
        Dim dr As SqlDataReader
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("campusid", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)





        'ds = db.RunParamSQLDataSet_SP("dbo.usp_GetEnrollmentListWithNoLeadGrps")

        '   Execute the query
        Try
            dr = db.RunParamSQLDataReader_SP("dbo.usp_GetCorporateInfoFromCampusId")

            While dr.Read
                With CorpInfo
                    If Not (dr("CorporateName") Is DBNull.Value) Then .CorporateName = dr("CorporateName")
                    If Not (dr("Address1") Is DBNull.Value) Then .Address1 = dr("Address1")
                    If Not (dr("Address2") Is DBNull.Value) Then .Address2 = dr("Address2")
                    If Not (dr("City") Is DBNull.Value) Then .City = dr("City")
                    If Not (dr("Zip") Is DBNull.Value) Then .Zip = dr("Zip")
                    If Not (dr("State") Is DBNull.Value) Then .State = dr("State")
                    If Not (dr("Country") Is DBNull.Value) Then .Country = dr("Country")
                    If Not (dr("Phone") Is DBNull.Value) Then .Phone = dr("Phone")
                    If Not (dr("Fax") Is DBNull.Value) Then .Fax = dr("Fax")
                    If Not (dr("TranscriptAuthznTitle") Is DBNull.Value) Then .TranscriptAuthznTitle = dr("TranscriptAuthznTitle")
                    If Not (dr("TranscriptAuthznName") Is DBNull.Value) Then .TranscriptAuthznName = dr("TranscriptAuthznName")
                    If Not (dr("Website") Is DBNull.Value) Then .Website = dr("Website")
                    If CInt(dr("clockHour")) > 0 Then
                        .IsClockHour = True
                    Else
                        .IsClockHour = False
                    End If


                End With
            End While


            'Close Connection
        Catch ex As Exception
        Finally
            dr.Close()
            db.CloseConnection()
        End Try

        '   Return CorporateInfo
        Return CorpInfo

    End Function
    Public Function CampusHasDeferredRevenuePostings(ByVal campusid) As Boolean
        Dim sb As New StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("select count(*) ")
            .Append("from saTransactions tr, saTransCodes tc ")
            .Append("where tr.TransCodeId=tc.TransCodeId ")
            .Append("and tr.CampusId=? ")
            .Append("and tr.Voided=0 ")
            .Append("and tc.DefEarnings=1 ")
        End With

        db.AddParameter("@cmpdid", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        If CInt(db.RunParamSQLScalar(sb.ToString)) > 0 Then
            Return True
        Else
            Return False
        End If


    End Function
    Public Function GetAllCampus_SP(ByVal showActiveOnly As Boolean, ByVal campusId As String) As DataTable
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@showActiveOnly", showActiveOnly, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet("dbo.usp_GetAllCampus", Nothing, "SP").Tables(0)

    End Function
    '' Code Added by kamalesh Ahuja on 10 June 2010 to Resolve mantis issue id 18411
    Public Function GetAllCampuses() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT ")
            .Append("        C.CampusId, ")
            .Append("        C.CampDescrip ")
            .Append("FROM syCampuses C, syStatuses S ")
            .Append("WHERE ")
            .Append("        C.StatusId = S.StatusId ")
            .Append("AND ")
            .Append("        S.Status = 'Active' ")
            .Append("AND ")
            .Append("       CampusId in ")
            .Append("           (Select CampusId from syCmpGrpCmps where CampGrpId in ")
            .Append("               (select CampgrpId from syUsersRolesCampGrps )) ")
            .Append("ORDER BY C.CampDescrip ")
        End With

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds
    End Function

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim myAdvApp As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvApp = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvApp = New AdvAppSettings
        End If
        Return myAdvApp
    End Function
End Class
