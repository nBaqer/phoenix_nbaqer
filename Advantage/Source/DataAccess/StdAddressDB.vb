Imports System.Data
Imports System.Data.OleDb
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class StdAddressDB
    Public Function GetDefaultStudentAddress(ByVal transactionId As String) As StuAddressInfo
        Return GetStudentAddressInfo(GetDefaultAddressId(transactionId))
    End Function
    Private Function GetDefaultAddressId(ByVal transactionId As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("select top 1 ")
            .Append("		StdAddressId ")
            .Append("from	 ")
            .Append("		saTransactions T, arStuEnrollments SE, arStudAddresses SA, syStatuses ST ")
            .Append("where  ")
            .Append("		T.StuEnrollId=SE.StuEnrollId ")
            .Append("and		SE.StudentId=SA.StudentId ")
            .Append("and		SA.StatusId=ST.StatusId ")
            .Append("AND		ST.Status='Active'		 ")
            .Append("AND		TransactionId = ? ")
            .Append("order by  ")
            .Append("		Default1 desc ")
        End With

        db.OpenConnection()

        db.AddParameter("@TransactionId", transactionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Dim s As String
        s = CType(db.RunParamSQLScalar(sb.ToString), Guid).ToString

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return s


    End Function
    Public Function GetStudentAddresses(ByVal studentId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim dr As OleDbDataReader

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            'With sb
            '    .Append("SELECT t1.StdAddressId,Address1,ST.StatusId,ST.Status, t1.default1,t1.AddressTypeId, ")
            '    .Append(" (select AddressDescrip from plAddressTypes where AddressTypeId=t1.AddressTypeId) as AddressType ")
            '    .Append("FROM arStudAddresses t1,syStatuses ST ")
            '    .Append("WHERE StudentId = ? and t1.StatusId = ST.StatusId and Address1 is not null ")
            '    If statusId = "True" Then
            '        .Append("AND    ST.Status = 'Active' ")
            '        .Append(" Order By t1.Address1 ")
            '    ElseIf statusId = "False" Then
            '        .Append("AND    ST.Status = 'InActive' ")
            '        .Append(" Order By t1.Address1 ")
            '    Else
            '        .Append("ORDER BY ST.Status,t1.Address1 asc")
            '    End If
            'End With

            With sb
                .Append("SELECT t1.StdAddressId,Address1,ST.StatusId,t1.default1,t1.AddressTypeId, ")
                .Append(" (select AddressDescrip from plAddressTypes where AddressTypeId=t1.AddressTypeId) as AddressType, ")
                .Append(" (Case ST.Status when 'Active' then 1 else 0 end) As Status ")
                .Append("FROM arStudAddresses t1,syStatuses ST ")
                .Append("WHERE StudentId = ? and t1.StatusId = ST.StatusId and Address1 is not null ")
                .Append("ORDER BY ST.Status,t1.Address1 asc")
            End With

            db.OpenConnection()

            db.AddParameter("@studid", studentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '            db.AddParameter("@statusid", statusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            Return ds

        Catch ex As Exception
            Throw New BaseException("Error retrieving student addresses - " & ex.InnerException.Message)
        Finally
            'Close Connection
            db.CloseConnection()

        End Try

    End Function
    Public Function DoesDefaultAddExist(ByVal StudentId As String, ByVal StdAddressId As String) As Integer
        Dim ds As New DataSet
        'Dim da As OleDbDataAdapter
        'Dim sGrdSysDetailId As String
        Dim rowCount As Integer
        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            With sb
                .Append(" delete from arStudAddresses where StudentId = ? ")
                .Append(" and default1 = 1 And Address1 Is null And Address2 Is null ")
                .Append(" and city is null and stateid is null and zip is null and countryid is null ")
            End With
            db.AddParameter("@stdId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            '   build the sql query
            With sb
                .Append("Select count(*) as Count from arStudAddresses a where a.StudentId = ? and default1 = 1  ")
                .Append("and StdAddressId <> ?")
            End With

            ' Add the studentid and stdaddressid to the parameter list
            db.AddParameter("@stdId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@stdaddId", StdAddressId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try

        'Return the datatable in the dataset
        Return rowCount

    End Function
    Public Function GetStudentAddressInfo(ByVal StudentAddressId As String) As StuAddressInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT CCT.StdAddressId, ")
            .Append("    CCT.Address1, ")
            .Append("    CCT.Address2, ")
            .Append("    CCT.city, ")
            .Append("    CCT.StateId, ")
            .Append("    (Select StateDescrip from syStates where StateId=CCT.StateId) As State, ")
            .Append("    CCT.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=CCT.StatusId) As AddStatus, ")
            .Append("    CCT.AddressTypeId, ")
            .Append("    (Select AddressDescrip from plAddressTypes where AddressTypeId=CCT.AddressTypeId) As AddType, ")
            .Append("    CCT.Zip, ")
            .Append("    CCT.CountryId, ")
            .Append("    (Select CountryDescrip from adCountries where CountryId=CCT.CountryId) As Country, ")
            .Append("    CCT.ForeignZip, ")
            .Append("    CCT.default1, ")
            .Append("    CCT.OtherState, ")
            .Append("    CCT.ModUser, ")
            .Append("    CCT.ModDate ")
            .Append("FROM  arStudAddresses CCT ")
            .Append("WHERE CCT.StdAddressId= ? ")
        End With

        ' Add the AcademicYearId to the parameter list
        db.AddParameter("@AcademicYearId", StudentAddressId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim StudentAddressInfo As New StuAddressInfo

        While dr.Read()

            '   set properties with data from DataReader
            With StudentAddressInfo
                'get IsInDB
                .IsInDB = True
                '.StudentID = CType(dr("StudentId"), Guid).ToString()

                'get AcademicYearId
                .StdAddressId = CType(dr("StdAddressId"), Guid).ToString()

                If Not (dr("Address1") Is System.DBNull.Value) Then .Address1 = CType(dr("Address1"), String).ToString Else .Address1 = ""
                If Not (dr("Address2") Is System.DBNull.Value) Then .Address2 = CType(dr("Address2"), String).ToString Else .Address2 = ""
                If Not (dr("City") Is System.DBNull.Value) Then .City = CType(dr("City"), String).ToString Else .City = ""
                If Not (dr("StateId") Is System.DBNull.Value) Then .State = CType(dr("StateId"), Guid).ToString Else .State = ""
                If Not (dr("State") Is System.DBNull.Value) Then .StateText = dr("State")
                If Not (dr("AddStatus") Is System.DBNull.Value) Then .AddStatusText = dr("AddStatus")
                If Not (dr("AddType") Is System.DBNull.Value) Then .AddTypeText = dr("AddType")
                If Not (dr("Country") Is System.DBNull.Value) Then .CountryText = dr("Country")

                If Not (dr("zip") Is System.DBNull.Value) Then .Zip = CType(dr("zip"), String).ToString Else .Zip = ""

                If Not (dr("CountryId") Is System.DBNull.Value) Then .Country = CType(dr("CountryId"), Guid).ToString Else .Country = ""

                If Not (dr("ForeignZip") Is System.DBNull.Value) Then .ForeignZip = dr("ForeignZip") Else .ForeignZip = 0

                If Not (dr("default1") Is System.DBNull.Value) Then .Default1 = dr("default1") Else .Default1 = 0

                If Not (dr("OtherState") Is System.DBNull.Value) Then .OtherState = CType(dr("OtherState"), String).ToString Else .OtherState = ""

                If Not (dr("StatusId") Is System.DBNull.Value) Then .AddressStatus = CType(dr("StatusId"), Guid).ToString Else .AddressStatus = ""

                If Not (dr("AddressTypeId") Is System.DBNull.Value) Then .AddressType = CType(dr("AddressTypeId"), Guid).ToString Else .AddressType = ""

                'get ModUser
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate") Else .ModDate = Date.MinValue

                .ModUser = dr("ModUser")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return StudentAddressInfo

    End Function
    Public Function UpdateStudentAddress(ByVal StudentAddressInfo As StuAddressInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim intForeignZip As Integer
        'Dim intForeignPhone As Integer
        Dim intDefault As Integer

        If StudentAddressInfo.ForeignZip = 1 Then
            intForeignZip = 1
        Else
            intForeignZip = 0
        End If
        If StudentAddressInfo.Default1 = 1 Then
            intDefault = 1
        Else
            intDefault = 0
        End If

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("Update arStudAddresses set Address1=?, ")
                .Append(" Address2=?,City=?,StateId=?,Zip=?,CountryId=?, ")
                .Append("AddressTypeId=?,ModUser=?,ModDate=?,StatusId=?,ForeignZip=?,default1=?,OtherState=? ")
                .Append(" where StdAddressID = ? ")
            End With

            '   add parameters values to the query

            'Address
            If StudentAddressInfo.Address1 = "" Then
                db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address1", StudentAddressInfo.Address1, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Address2
            If StudentAddressInfo.Address2 = "" Then
                db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", StudentAddressInfo.Address2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'city
            If StudentAddressInfo.City = "" Then
                db.AddParameter("@city", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@city", StudentAddressInfo.City, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'StateId
            If StudentAddressInfo.State = "" Or StudentAddressInfo.State = Guid.Empty.ToString Then
                db.AddParameter("@State", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@State", StudentAddressInfo.State, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Zip
            If StudentAddressInfo.Zip = "" Then
                db.AddParameter("@zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@zip", StudentAddressInfo.Zip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Country
            If StudentAddressInfo.Country = "" Or StudentAddressInfo.Country = Guid.Empty.ToString Then
                db.AddParameter("@Country", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Country", StudentAddressInfo.Country, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'AddressType
            If StudentAddressInfo.AddressType = "" Or StudentAddressInfo.AddressType = Guid.Empty.ToString Then
                db.AddParameter("@AddressType", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AddressType", StudentAddressInfo.AddressType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'AddressStatus
            If StudentAddressInfo.AddressStatus = "" Or StudentAddressInfo.AddressStatus = Guid.Empty.ToString Then
                db.AddParameter("@AddressStatus", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AddressStatus", StudentAddressInfo.AddressStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Foreign Zip
            db.AddParameter("@ForeignZip", StudentAddressInfo.ForeignZip, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@Default1", intDefault, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)


            'Other State
            If StudentAddressInfo.OtherState = "" Then
                db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@OtherState", StudentAddressInfo.OtherState, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'StudentId
            db.AddParameter("@StdaddressId", StudentAddressInfo.StdAddressId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            'Insert Only If Phone is not empty
            If Not StudentAddressInfo.Address1 = "" Or Not StudentAddressInfo.Address2 = "" Then
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
            End If
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Insert data into regent xml 
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                Dim regDB As New regentDB
                Dim strFilename As String = AdvantageCommonValues.getStudentBatchFileName()
                regDB.AddAddressXML(StudentAddressInfo.StudentID, strFilename, StudentAddressInfo.StdAddressId)
            End If

            StudentAddressInfo.IsInDB = True
            Return ""
        Catch ex As OleDbException
            '   return an error to the client
            Return ex.Message
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        ''   execute the query
        'Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

        ''   If there were no updated rows then there was a concurrency problem
        'If rowCount = 1 Then
        '    '   return without errors
        '    Return ""
        'Else
        '    Return DALExceptions.BuildConcurrencyExceptionMessage()
        'End If

        'Catch ex As OleDbException
        '    '   return an error to the client
        '    Return DALExceptions.BuildErrorMessage(ex)

        'Finally
        '    'Close Connection
        '    db.CloseConnection()
        'End Try

    End Function
    Public Function AddStudentAddress(ByVal StudentAddressInfo As StuAddressInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim intForeignZip As Integer
        Dim intDefault As Integer

        If StudentAddressInfo.ForeignZip = 1 Then
            intForeignZip = 1
        Else
            intForeignZip = 0
        End If
        If StudentAddressInfo.Default1 = 1 Then
            intDefault = 1
        Else
            intDefault = 0
        End If

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            Dim sb4 As New StringBuilder
            With sb4
                .Append(" Insert into arStudAddresses(StdAddressId,StudentId,Address1,Address2,")
                .Append(" City,StateId,Zip,CountryId,AddressTypeId,OtherState,ModUser,ModDate,StatusId,default1,ForeignZip)")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")

            End With

            db.AddParameter("@StdAddressId", StudentAddressInfo.StdAddressId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@StudentId", StudentAddressInfo.StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'Address
            If StudentAddressInfo.Address1 = "" Then
                db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address1", StudentAddressInfo.Address1, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Address2
            If StudentAddressInfo.Address2 = "" Then
                db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", StudentAddressInfo.Address2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'city
            If StudentAddressInfo.City = "" Then
                db.AddParameter("@city", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@city", StudentAddressInfo.City, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'StateId
            If StudentAddressInfo.State = "" Then
                db.AddParameter("@State", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@State", StudentAddressInfo.State, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If StudentAddressInfo.Zip = "" Then
                db.AddParameter("@zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@zip", StudentAddressInfo.Zip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If StudentAddressInfo.Country = "" Then
                db.AddParameter("@Country", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Country", StudentAddressInfo.Country, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            'Zip



            'AddressType
            If StudentAddressInfo.AddressType = "" Or StudentAddressInfo.AddressType = Guid.Empty.ToString Then
                db.AddParameter("@AddressType", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AddressType", StudentAddressInfo.AddressType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            If StudentAddressInfo.OtherState = "" Then
                db.AddParameter("@OState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@OState", StudentAddressInfo.OtherState, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@StatusId", StudentAddressInfo.AddressStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Default1", intDefault, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.AddParameter("@ForeignZip", intForeignZip, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            If Not StudentAddressInfo.Address1 = "" Or Not StudentAddressInfo.Address2 = "" Then
                db.RunParamSQLExecuteNoneQuery(sb4.ToString)
            End If
            db.ClearParameters()
            sb4.Remove(0, sb4.Length)

            'Insert data into regent xml 
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                Dim regDB As New regentDB
                Dim strFilename As String = AdvantageCommonValues.getStudentBatchFileName()
                regDB.AddAddressXML(StudentAddressInfo.StudentID, strFilename, StudentAddressInfo.StdAddressId)
            End If

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteStudentAddress(ByVal StudentAddressId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM arStudAddresses ")
                .Append("WHERE StdAddressId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM arStudAddresses WHERE StdAddressId = ? ")
            End With

            '   add parameters values to the query

            '   AcademicYearId
            db.AddParameter("@StdAddressId", StudentAddressId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   AcademicYearId
            db.AddParameter("@StdAddressId2", StudentAddressId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

End Class
