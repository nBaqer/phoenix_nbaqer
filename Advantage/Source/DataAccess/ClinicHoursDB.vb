Imports System.Data
Imports FAME.Advantage.Common

Public Class ClinicHoursDB

    Private ReadOnly myAdvAppSettings As AdvAppSettings

    Public Sub New ()
          myAdvAppSettings = AdvAppSettings.GetAppSettings()
    End Sub
    

    Public Function GetAllCoursesByEnrollment(ByVal strStuEnrollId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        With sb
            .Append(" Select Distinct t4.ReqId,t4.Descrip  as Descrip,t4.Code as Code ")
            .Append(" from arStuEnrollments t1,arPrgVersions t2,arProgVerDef t3,arReqs t4 ")
            .Append(" where t1.StuEnrollId=? and t1.PrgVerId = t2.PrgVerId And ")
            .Append(" t2.PrgVerId = t3.PrgVerId And t3.ReqId = t4.ReqId And t4.ReqTypeId = 1 ")
            .Append(" Union ")
            .Append(" Select Distinct t6.ReqId,t6.Descrip as Descrip,t6.Code as Code ")
            .Append(" from arStuEnrollments t1,arPrgVersions t2,arProgVerDef t3,arReqs t4,arReqGrpDef t5,arReqs t6 ")
            .Append(" where t1.StuEnrollId=? and t1.PrgVerId = t2.PrgVerId And t2.PrgVerId = t3.PrgVerId And t3.ReqId = t4.ReqId And t4.ReqTypeId = 2 ")
            .Append(" and t3.ReqId = t5.GrpId and t5.ReqId=t6.ReqId  ")
            .Append(" Order by t4.Descrip, t4.Code  ")
        End With
        db.ClearParameters()
        db.AddParameter("@StuEnrollId", strStuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", strStuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllCoursesByEnrollmentByTerm(ByVal strStuEnrollId As String, ByVal termId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        With sb
            .Append(" select distinct t2.ReqId from arResults t1,arClassSections t2,arReqs t3 ")
            .Append(" where t1.TestId=t2.ClsSectionId and t2.reqId=t3.reqId and t2.TermId=? and ")
            .Append(" StuEnrollId= ? and IsClinicsSatisfied=1 ")
        End With
        db.ClearParameters()
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", strStuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllClinicServices(ByVal strStuEnrollId As String, Optional ByVal courseId As String = "", Optional ByVal courses As String = "All", Optional ByVal clinicType As String = "LabHours") As DataSet

        Dim sb As New StringBuilder
        Dim dsGetCourses As  DataSet
        Dim strCourses As String = ""
        Dim db As New DataAccess

       db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        If courses = "All" Then
            dsGetCourses = GetAllCoursesByEnrollment(strStuEnrollId)
            If dsGetCourses.Tables(0).Rows.Count >= 1 Then
                For Each row As DataRow In dsGetCourses.Tables(0).Rows
                    strCourses &= row("ReqId").ToString & "','"
                Next
                strCourses = Mid(strCourses, 1, InStrRev(strCourses, "'") - 2)
            End If
        End If

        With sb
            .Append(" select GD.InstrGrdBkWgtDetailId,isnull(GD.Code,GC.Code) as Code,GC.Descrip as Descrip,  ")
            .Append(" GD.Number as Required,(Select IsNULL(Sum(GBR.Score),0) from arGrdBkResults  GBR where GBR.StuEnrollId=? ")
            .Append(" and GBR.InstrGrdBkWgtDetailId=GD.InstrGrdBkWgtDetailId) as Completed,GC.SysComponentTypeId,'' as Remaining ")
            .Append(" from arGrdComponentTypes GC, arGrdBkWgtDetails GD where ")
            .Append(" GC.GrdComponentTypeId = GD.GrdComponentTypeId and GD.InstrGrdBkWgtId  in ")
            .Append(" (select Distinct InstrGrdBkWgtId from arGrdBkWeights where ")
            If courses = "All" Then
                .Append(" ReqId in ('")
                .Append(strCourses)
                .Append(") ")
            Else
                .Append(" ReqId = '" & courseId & "' ")
            End If
            .Append(" ) and GD.Number >= 1 ")
            .Append(" and GC.SysComponentTypeId in (select Distinct ResourceId from syResources where ")
            .Append(" Resource=? and ResourceTypeId=10) ")
            'For clinic services set the Resource to 'Lab Work'
            .Append(" union ")
            .Append(" select Distinct null,t1.Code as Code,t1.Descrip as Descrip,t2.MinResult as Required, ")
            .Append(" (Select IsNULL(Sum(GBR.Score),0) from arGrdBkConversionResults  GBR ")
            .Append("  where GBR.StuEnrollId=t2.StuEnrollId and GBR.GrdComponentTypeId=t1.GrdComponentTypeId) as Completed, ")
            .Append("  t1.SysComponentTypeId,'' as Remaining from arGrdComponentTypes t1,arGrdBkConversionResults t2 ")
            .Append("  where t1.GrdComponentTypeId = t2.GrdComponentTypeId ")
            .Append("  And t1.SysComponentTypeId in (select Distinct ResourceId from syResources where ")
            .Append("  Resource=? and ResourceTypeId=10) and ")
            .Append("  t2.StuEnrollId=? ")
            If Courses = "All" Then
                .Append(" and t2.ReqId in ('")
                .Append(strCourses)
                .Append(") ")
            Else
                .Append(" and t2.ReqId = '" & CourseId & "' ")
            End If
            .Append(" order by GC.Descrip ")
        End With
        db.ClearParameters()
        db.AddParameter("@StuEnrollId", strStuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@Resource", clinicType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@Resource", clinicType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", strStuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllEnrollments(ByVal studentid As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        With sb
            .Append(" select StuEnrollId,(select Distinct PrgVerDescrip from arPrgVersions ")
            .Append(" where PrgVerId= SE.PrgVerId) as Descrip from arStuEnrollments SE ")
            .Append(" where SE.StudentId=? ")
            .Append(" order by StartDate desc ")
        End With
        db.ClearParameters()
        db.AddParameter("@Studentid", studentid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
End Class
