Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' CoursesDB.vb
'
' CoursesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class CoursesDB
    Public Function GetAllCourses(ByVal showActiveOnly As Boolean) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   R.ReqId, R.StatusId, R.Code, R.Descrip,'(' + R.Code  + ')' + ' ' + R.Descrip AS FullDescrip  ")
            .Append(" FROM     arReqs R, syStatuses ST ")
            .Append("WHERE    R.StatusId = ST.StatusId ")
            If showActiveOnly Then
                .Append(" AND     ST.Status = 'Active' ")
            End If
            .Append("ORDER BY R.Descrip ")
        End With
        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllCoursesNotCourseGroups(ByVal showActiveOnly As Boolean, Optional ByVal campusId As String = "") As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   R.ReqId, R.StatusId, R.Code, R.Descrip,'(' + R.Code  + ')' + ' ' + R.Descrip AS FullDescrip  ")
            .Append(" FROM     arReqs R, syStatuses ST ")
            .Append("WHERE    R.StatusId = ST.StatusId  and R.ReqTypeId = 1 ")

            If showActiveOnly Then
                .Append(" AND     ST.Status = 'Active' ")
            End If

            If campusId <> "" Then
                'We only want terms that belong to the all campus group or campus groups that have this campus
                .Append("AND ( ")
                .Append("       R.CampGrpId = (SELECT CampGrpId FROM dbo.syCampGrps WHERE CampGrpCode='All') ")
                .Append("                                           OR                                        ")
                .Append("       R.CampGrpId IN(SELECT CampGrpId FROM dbo.syCmpGrpCmps WHERE CampusId='" & campusId & "') ")
                .Append("    ) ")
            End If

            .Append("ORDER BY R.Descrip, R.Code ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetCourses(ByVal CampusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT t1.ReqId,t1.Descrip,t1.Code, t2.ReqTypeId, t1.StatusId  ")
            .Append(" ,(select Distinct Status from syStatuses where StatusId=t1.StatusId) as Status ")
            .Append(" ,t1.TrackTardies,t1.TardiesMakingAbsence ")
            .Append("FROM arReqs t1, arReqTypes t2 ")
            .Append("WHERE t1.ReqTypeId = t2.ReqTypeId And t2.IsGroup = 0 And t2.ReqTypeId = 1 ")
            .Append("AND t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("ORDER BY t1.Descrip")
        End With
        db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)


    End Function

    Public Function GetCourseDetails(ByVal CourseId As String) As CourseInfo

        'connect to the database
        Dim db As New DataAccess



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select ReqId,Code,Descrip,Hours,Credits,FinAidCredits,StatusId,GrdLvlId,UnitTypeId, ")
            .Append(" CampGrpId,ReqTypeId,CourseCategoryId,CourseCatalog,CourseComments, ")
            .Append(" AreStdsReg = (CASE WHEN (SELECT COUNT(*) FROM arResults WHERE TestId in ")
            .Append("                (Select ClsSectionId from arClassSections where ReqId = ?)) > 0 THEN 'True' ELSE 'False' END), ")
            .Append(" IsCourseInPrgVerDef = (CASE WHEN (SELECT COUNT(*) FROM arProgVerDef WHERE ReqId = ?) > 0 THEN 'True' ELSE 'False' END), ")
            .Append("ModDate,ModUser,SU,PF,TrackTardies,IsOnLine,TardiesMakingAbsence,DeptId,IsExternship,UseTimeClock,IsAttendanceOnly,AllowCompletedCourseRetake from arReqs where ReqId=? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@CourseID1", CourseId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CourseID", CourseId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CourseID3", CourseId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim CourseObj As New CourseInfo

        While dr.Read()
            With CourseObj
                '.StudentId = dr("StudentId")
                CourseObj.CourseId = dr("ReqId")
                CourseObj.CourseCode = dr("Code").ToString
                CourseObj.CourseDescrip = dr("Descrip").ToString
                CourseObj.CourseHours = dr("Hours").ToString
                If Not dr("Credits") Is System.DBNull.Value Then CourseObj.CourseCredits = dr("Credits").ToString
                If Not dr("FinAidCredits") Is System.DBNull.Value Then
                    CourseObj.FinAidCredits = dr("FinAidCredits").ToString
                Else
                    CourseObj.FinAidCredits = 0
                End If
                CourseObj.CampGrpId = dr("CampGrpId")
                If Not (dr("GrdLvlId") Is System.DBNull.Value) Then CourseObj.GrdLvlId = CType(dr("GrdLvlId"), Guid).ToString Else CourseObj.GrdLvlId = ""
                'CourseObj.GrdLvlId = dr("GrdLvlId")
                If Not (dr("CourseCategoryId") Is System.DBNull.Value) Then CourseObj.CourseCatId = CType(dr("CourseCategoryId"), Guid).ToString Else CourseObj.CourseCatId = ""
                CourseObj.StatusId = dr("StatusId")
                CourseObj.UnitTypeId = dr("UnitTypeId")
                CourseObj.CourseCatalog = dr("CourseCatalog").ToString
                CourseObj.CourseComments = dr("CourseComments").ToString
                CourseObj.AreStdsReg = dr("AreStdsReg")
                CourseObj.IsCourseInPrgVerDef = dr("IsCourseInPrgVerDef")
                CourseObj.TrackTardies = dr("TrackTardies")
                CourseObj.IsOnLine = dr("IsOnLine")
                CourseObj.Externship = dr("IsExternship")
                If Not (dr("TardiesMakingAbsence") Is System.DBNull.Value) Then
                    CourseObj.TardiesMakingAbsence = dr("TardiesMakingAbsence")
                Else
                    CourseObj.TardiesMakingAbsence = 0
                End If
                If Not (dr("DeptId") Is System.DBNull.Value) Then
                    CourseObj.DeptId = dr("DeptId")
                End If
                If dr("SU").ToString <> "" Then
                    CourseObj.SU = dr("SU")
                End If
                If dr("PF").ToString <> "" Then
                    CourseObj.PF = dr("PF")
                End If
                'get ModUser
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate") Else .ModDate = Date.MinValue

                .ModUser = dr("ModUser")
                If Not dr("UseTimeClock") Is DBNull.Value Then
                    CourseObj.IsUseTimeClock = dr("UseTimeClock")
                Else
                    CourseObj.IsUseTimeClock = False
                End If
                If Not dr("IsAttendanceOnly") Is DBNull.Value Then
                    CourseObj.IsAttendanceOnly = dr("IsAttendanceOnly")
                Else
                    CourseObj.IsAttendanceOnly = False
                End If

                CourseObj.AllowCompletedCourseRetake = dr("AllowCompletedCourseRetake")


            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return CourseObj
    End Function
    Public Function InsertCourse(ByVal CourseObj As CourseInfo, ByVal user As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim clssectid As String
        Dim StudentId As String
        Dim GrdBkWgtDetailId As String
        Dim score As Integer
        Dim Comments As String



        'clssectid = StdGrdObj.ClsSectId
        'StudentId = StdGrdObj.StudentId
        'GrdBkWgtDetailId = StdGrdObj.GrdBkWgtDetailId
        'score = StdGrdObj.Score
        'Comments = StdGrdObj.Comments

        'Set the connection string
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Try
            With sb
                .Append("INSERT INTO arReqs(ReqId,Code,Descrip,Hours,Credits,FinAidCredits,StatusId,GrdLvlId,UnitTypeId,CampGrpId,ReqTypeId,CourseCategoryId,CourseCatalog,CourseComments,SU,PF,TrackTardies,TardiesMakingAbsence,IsOnLine,ModUser,ModDate,DeptId,IsExternship,UseTimeClock,IsAttendanceOnly,AllowCompletedCourseRetake) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")

                db.AddParameter("@courseid", CourseObj.CourseId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                db.AddParameter("@code", CourseObj.CourseCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@descrip", CourseObj.CourseDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@hours", CourseObj.CourseHours, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@credits", CourseObj.CourseCredits, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@finAidCredits", CourseObj.FinAidCredits, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@statusid", CourseObj.StatusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                'db.AddParameter("@GrdLvlId", CourseObj.GrdLvlId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                If CourseObj.GrdLvlId = "" Then
                    db.AddParameter("@grdlvlid", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@grdlvlid", CourseObj.GrdLvlId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                db.AddParameter("@UnitTypeId", CourseObj.UnitTypeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                db.AddParameter("@CampGrpId", CourseObj.CampGrpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                db.AddParameter("@ReqTypeId", CourseObj.ReqTypeId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
                If CourseObj.CourseCatId = "" Then
                    db.AddParameter("@coursecatid", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@coursecatid", CourseObj.CourseCatId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                db.AddParameter("@catalog", CourseObj.CourseCatalog, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@comments", CourseObj.CourseComments, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
                db.AddParameter("@SU", Convert.ToBoolean(CourseObj.SU), DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
                db.AddParameter("@PF", Convert.ToBoolean(CourseObj.PF), DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
                db.AddParameter("@TrackTardies", CourseObj.TrackTardies, DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
                db.AddParameter("@TardiesMakingAbsence", CourseObj.TardiesMakingAbsence, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
                db.AddParameter("@IsOnLine", CourseObj.IsOnLine, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If (CourseObj.DeptId.ToString <> "00000000-0000-0000-0000-000000000000") Then
                    db.AddParameter("@deptId", CourseObj.DeptId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                Else
                    db.AddParameter("@deptId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                End If
                db.AddParameter("@Externship", CourseObj.Externship, DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
                db.AddParameter("@UseTimeClock", CourseObj.IsUseTimeClock, DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
                db.AddParameter("@IsAttendanceOnly", CourseObj.IsAttendanceOnly, DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
                db.AddParameter("@AllowCompletedCourseRetake", CourseObj.AllowCompletedCourseRetake, DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
            End With

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Function UpdateCourse(ByVal CourseObj As CourseInfo, ByVal user As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim clssectid As String
        Dim StudentId As String
        Dim GrdBkWgtDetailId As String
        Dim score As Integer
        Dim Comments As String


        'clssectid = StdGrdObj.ClsSectId
        'StudentId = StdGrdObj.StudentId
        'GrdBkWgtDetailId = StdGrdObj.GrdBkWgtDetailId
        'score = StdGrdObj.Score
        'Comments = StdGrdObj.Comments

        'Set the connection string
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Try
            With sb
                .Append("UPDATE arReqs Set Code = ?,Descrip = ?,Hours = ?,Credits = ?,FinAidCredits=?,StatusId = ?,GrdLvlId = ?,CourseCategoryId = ?,UnitTypeId = ? ,CampGrpId = ?,ReqTypeId = ?,CourseCatalog = ?,CourseComments = ?, SU = ?, PF = ?, TrackTardies = ?, TardiesMakingAbsence = ?, IsOnLine = ?, ModUser=?,ModDate=?,DeptId=?,IsExternship=?,USeTimeClock=?,IsAttendanceOnly=?,AllowCompletedCourseRetake=? ")
                .Append("WHERE  ReqId = ? ")

                db.AddParameter("@code", CourseObj.CourseCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@descrip", CourseObj.CourseDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@hours", CourseObj.CourseHours, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@credits", CourseObj.CourseCredits, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@finAidCredits", CourseObj.FinAidCredits, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@statusid", CourseObj.StatusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                'db.AddParameter("@GrdLvlId", CourseObj.GrdLvlId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                If CourseObj.GrdLvlId = "" Then
                    db.AddParameter("@grdlvlid", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@grdlvlid", CourseObj.GrdLvlId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                If CourseObj.CourseCatId = "" Then
                    db.AddParameter("@coursecatid", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@coursecatid", CourseObj.CourseCatId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                db.AddParameter("@UnitTypeId", CourseObj.UnitTypeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                db.AddParameter("@CampGrpId", CourseObj.CampGrpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                db.AddParameter("@ReqTypeId", CourseObj.ReqTypeId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
                db.AddParameter("@catalog", CourseObj.CourseCatalog, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@comments", CourseObj.CourseComments, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
                db.AddParameter("@SU", Convert.ToBoolean(CourseObj.SU), DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
                db.AddParameter("@PF", Convert.ToBoolean(CourseObj.PF), DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
                db.AddParameter("@TrackTardies", CourseObj.TrackTardies, DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
                db.AddParameter("@TardiesMakingAbsence", CourseObj.TardiesMakingAbsence, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
                db.AddParameter("@IsOnLine", CourseObj.IsOnLine, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If (CourseObj.DeptId.ToString <> "00000000-0000-0000-0000-000000000000") Then
                    db.AddParameter("@deptId", CourseObj.DeptId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                Else
                    db.AddParameter("@deptId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                End If
                db.AddParameter("@Externship", CourseObj.Externship, DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
                db.AddParameter("@UseTimeClock", CourseObj.IsUseTimeClock, DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
                db.AddParameter("@IsAttendanceOnly", CourseObj.IsAttendanceOnly, DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
                db.AddParameter("@AllowCompletedCourseRetake", CourseObj.AllowCompletedCourseRetake, DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)

                db.AddParameter("@courseid", CourseObj.CourseId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            End With

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Function DeleteCourse(ByVal CourseId As String, ByVal modDate As DateTime) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim clssectid As String
        Dim StudentId As String
        Dim GrdBkWgtDetailId As String
        Dim score As Integer
        Dim Comments As String


        'clssectid = StdGrdObj.ClsSectId
        'StudentId = StdGrdObj.StudentId
        'GrdBkWgtDetailId = StdGrdObj.GrdBkWgtDetailId


        'Set the connection string
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Try
            With sb
                .Append("DELETE FROM arReqs ")
                .Append("WHERE ReqId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM arReqs WHERE ReqId = ? ")

            End With

            db.AddParameter("@courseid", CourseId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@courseid2", CourseId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'db.RunParamSQLExecuteNoneQuery(sb.ToString)
            'db.ClearParameters()
            'sb.Remove(0, sb.Length)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function DoesReqHasAttendanceRecords(ByVal reqId As String) As Boolean
        'connect to the database
        Dim db As New DataAccess
        Dim numRecords As Integer


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("select count(*) ")
            .Append("from atClsSectAttendance t1 ")
            .Append("where exists ")
            .Append("(select t2.ClsSectionId from atClsSectAttendance t2, arClassSections t3, arReqs t4 ")
            .Append("where t1.ClsSectionId=t2.ClsSectionId and t2.ClsSectionId=t3.ClsSectionId and t3.ReqId=t4.ReqId and t4.ReqId = ?)")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        numRecords = CInt(db.RunParamSQLScalar(sb.ToString))

        If numRecords > 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function CheckCourseWithProgramAttendanceType(ByVal reqId As String, ByVal attType As String) As Boolean
        'connect to the database
        Dim db As New DataAccess
        Dim numRecords As Integer

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select count(UnitTypeId) from arProgVerDef  ,arPrgVersions  where Reqid= ? and arProgVerDef.PrgVerId=arPrgVersions.PrgVerId ")
            .Append(" and arPrgVersions.UnitTypeId IN (SELECT UnitTypeId FROM arAttUnitType WHERE UnitTypeDescrip  LIKE '%Present%' or unitTypeDescrip  LIKE '%Minute%') ")
        End With
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        numRecords = CInt(db.RunParamSQLScalar(sb.ToString))
        sb.Remove(0, sb.Length)
        db.ClearParameters()
        If numRecords = 0 Then
            Return True
        Else
            With sb
                .Append(" select count(A.UnitTypeId) from ")
                .Append("(select UnitTypeId from arProgVerDef ,arPrgVersions where arProgVerDef.Reqid= ? ")
                .Append(" and arProgVerDef.PrgVerId=arPrgVersions.PrgVerId ")
                .Append(" union ")
                .Append(" select UnitTypeId from arAttUnitType where UnitTypeDescrip='None') A where A.UnitTypeId= ? ")
            End With
            db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@unitTypeId", attType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute the query
            numRecords = CInt(db.RunParamSQLScalar(sb.ToString))
            sb.Remove(0, sb.Length)
            db.ClearParameters()
            If (numRecords >= 1) Then
                Return True
            Else
                Return False
            End If
        End If
        Return True
    End Function
    Public Function GetCourses(ByVal CampusId As String, ByVal ParentReqId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT t1.ReqId,t1.Descrip,t1.Code, t2.ReqTypeId, t1.StatusId  ")
            .Append(" ,(select Distinct Status from syStatuses where StatusId=t1.StatusId) as Status, ")
            .Append("(select Count(*) from arCourseEquivalent where ReqId=? and EquivReqId=t1.ReqId) as SelectedEquivReqId ")
            .Append("FROM arReqs t1, arReqTypes t2 ")
            .Append("WHERE t1.ReqTypeId = t2.ReqTypeId And t2.IsGroup = 0 And t2.ReqTypeId = 1 ")
            .Append("AND t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("AND t1.ReqId <> ? ")
            .Append("ORDER BY SelectedEquivReqId desc,t1.Descrip")
        End With
        db.AddParameter("@ReqId", ParentReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ReqId", ParentReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function AddCourseEquivalent(ByVal ParentReqId As String, ByVal EquivalentReqId() As String, ByVal user As String)
        Dim i As Integer
        '   connect to the database
        Dim db As New DataAccess
        Dim sb As New StringBuilder



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        With sb
            .Append(" Delete from arCourseEquivalent where ReqId=? ")
        End With
        db.AddParameter("@ReqId", ParentReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try

        For i = 0 To EquivalentReqId.Length - 1
            '   build query
            With sb
                .Append("INSERT INTO arCourseEquivalent(ReqEquivalentId,ReqId,EquivReqId,ModDate, ModUser) ")
                .Append("VALUES(?,?,?,?,?)")
            End With

            '   add parameters
            db.AddParameter("@ReqEquivalentId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ReqId", ParentReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@EquivReqId", DirectCast(EquivalentReqId.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute query

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        Next

        '   return without errors
        Return ""

        'Catch ex As OleDbException
        '    '   return an error to the client
        '    Return DALExceptions.BuildErrorMessage(ex)
        'Finally
        '    '   Close Connection
        '    db.CloseConnection()
        'End Try

    End Function
    Public Function DeleteCourseEquivalent(ByVal ParentReqId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        With sb
            .Append(" Delete from arCourseEquivalent where ReqId=? ")
        End With
        db.AddParameter("@ReqId", ParentReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try
        Return ""
    End Function

    Public Function IsExternship(ByVal courseId As String) As Boolean
        Dim db As New DataAccess
        Dim numRecords As Integer



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("select count(*) ")
            .Append("from arReqs  ")
            .Append("where ReqId = ? ")
            .Append("and IsExternship = 1 ")
            .Append("and IsExternship is not null ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@reqid", courseId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        numRecords = CInt(db.RunParamSQLScalar(sb.ToString))

        If numRecords > 0 Then
            Return True
        Else
            Return False
        End If

    End Function
    Public Function GetAllCourses_SP(ByVal showActiveOnly As Boolean, Optional ByVal campusId As String = "") As DataTable

        Dim db As New DataAccess



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@showActiveOnly", showActiveOnly, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet("dbo.usp_GetAllCourses", Nothing, "SP").Tables(0)

    End Function
    Public Function GetCoursesForTerm_SP(ByVal TermId As String, ByVal campusid As String, Optional ByVal showActiveOnly As Boolean = True) As DataSet
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@showActiveOnly", showActiveOnly, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

        Return db.RunParamSQLDataSet("dbo.usp_GetCoursesForTerm", Nothing, "SP")
    End Function

    Public Function GetCoursesFromClassSectionsForTerm_SP(ByVal TermId As String) As DataTable
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet("dbo.usp_GetCoursesFromClassSectionsForTerm", Nothing, "SP").Tables(0)
    End Function
    Public Function GetClassSections(ByVal StartDate As Date, ByVal EndDate As Date, ByVal CampusId As String, _
                                     ByVal InstructorId As String, ByVal CourseId As String, ByVal TermId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As New DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@StartDate", StartDate, SqlDbType.Date, 50, ParameterDirection.Input)
            db.AddParameter("@EndDate", EndDate, SqlDbType.Date, 50, ParameterDirection.Input)
            db.AddParameter("@CampusId", CampusId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            If InstructorId.ToString.Trim = "" Then
                db.AddParameter("@InstructorId", System.DBNull.Value, SqlDbType.VarChar, 50, ParameterDirection.Input)

            Else
                db.AddParameter("@InstructorId", InstructorId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            End If
            If CourseId.ToString.Trim = "" Then
                db.AddParameter("@CourseId", System.DBNull.Value, SqlDbType.VarChar, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CourseId", CourseId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            End If
            If TermId.ToString.Trim = "" Then
                db.AddParameter("@TermId", System.DBNull.Value, SqlDbType.VarChar, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@TermId", TermId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            End If
            ds = db.RunParamSQLDataSet_SP("dbo.USP_Classes_GetList", "ClassSections")
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function GetClassSectionMeetings(ByVal ClsSectionId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As New DataSet



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@ClsSectionId", ClsSectionId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_ClassSectionMeetings_GetList", "ClassSectionMeetings")
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function GetClassSectionsAndMeetings(ByVal StartDate As Date, ByVal EndDate As Date, ByVal CampusId As String,
                                    ByVal InstructorId As String, ByVal CourseId As String, ByVal TermId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As New DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@StartDate", StartDate, SqlDbType.Date, 50, ParameterDirection.Input)
            db.AddParameter("@EndDate", EndDate, SqlDbType.Date, 50, ParameterDirection.Input)
            db.AddParameter("@CampusId", CampusId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            If InstructorId.ToString.Trim = "" Then
                db.AddParameter("@InstructorId", System.DBNull.Value, SqlDbType.VarChar, 50, ParameterDirection.Input)

            Else
                db.AddParameter("@InstructorId", InstructorId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            End If
            If CourseId.ToString.Trim = "" Then
                db.AddParameter("@CourseId", System.DBNull.Value, SqlDbType.VarChar, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CourseId", CourseId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            End If
            If TermId.ToString.Trim = "" Then
                db.AddParameter("@TermId", System.DBNull.Value, SqlDbType.VarChar, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@TermId", TermId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            End If
            ds = db.RunParamSQLDataSet_SP("dbo.USP_ClassesAndMeetings_GetList", "ClassSections")
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function

    Public Function GetCourseStatus(ByVal reqId As String) As String
        Dim db As New SQLDataAccess
        Dim ds As New DataSet

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")

        Try
            db.OpenConnection()
            db.AddParameter("@ReqId", reqId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GetCourseStatus", "CourseStatus")

            If ds.Tables("CourseStatus").Rows.Count = 0 Then
                Return "Inactive"
            Else
                Return ds.Tables("CourseStatus").Rows(0)("Status").ToString
            End If
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try

    End Function
#Region "Get AdvAppsetting for Manage Config entry"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
End Class
