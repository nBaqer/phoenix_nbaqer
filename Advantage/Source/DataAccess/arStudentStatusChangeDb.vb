﻿Imports FAME.Advantage.Common

Public Class ArStudentStatusChangeDb

    ''' <summary>
    ''' Get the previous student Status Change
    ''' This function is prepared to use DateOfChange but it is 
    ''' compatible with the false old field ModDate.
    ''' </summary>
    ''' <param name="stuEnrollmentId">Student Enrollment Id</param>
    ''' <returns>The previous status, this is a GUID</returns>
    ''' <remarks>Use this all the time that you need get the previous status </remarks>
    Function GetPreviousStatusChange(stuEnrollmentId As String) As String

        Dim myAdvAppSettings As AdvAppSettings = New AdvAppSettings()

        'Set the connection string
        Dim constring As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim conn = New SqlConnection(constring)

        'Create the query
        Dim sb As New StringBuilder

        sb.Append("SELECT TOP 1 OrigStatusId FROM syStudentStatusChanges ")
        sb.Append("	WHERE StuEnrollId = @StuEnrollId ")
        sb.Append("	ORDER BY")
        sb.Append("	(SELECT (CASE WHEN DateOfChange IS NULL THEN ModDate ELSE DateOfChange END) ) DESC")

        'Enter parameter
        Dim command As SqlCommand = New SqlCommand(sb.ToString(), conn)
        command.Parameters.AddWithValue("@StuEnrollId", stuEnrollmentId)

        Dim result As String
        conn.Open()
        Try
            Dim status = command.ExecuteScalar()
            If status = Nothing Then
                result = Nothing
            Else
                result = status.ToString()
            End If

        Finally
            conn.Close()
        End Try

        Return result
    End Function

End Class
