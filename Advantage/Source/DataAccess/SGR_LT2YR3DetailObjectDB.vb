Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SGR_LT2YR3DetailObjectDB

	Public Shared Function GetReportDatasetRaw(ByVal RptParamInfo As ReportParamInfoIPEDS) As DataSet

		' get list of students to include, based on report parameters
		Dim StudentList As String = IPEDSDB.GetStudentList(RptParamInfo)

		' if there are no students to include, return empty dataset
		If StudentList = "" Then
			Return New DataSet
		End If

		Dim sb As New System.Text.StringBuilder
		Dim dsRaw As New DataSet

		With sb
			' get Students
            .Append("SELECT DISTINCT arStudent.StudentId, " + vbCrLf)

			' retrieve Student Identifier
            .Append(IPEDSDB.GetSQL_StudentIdentifier & ", " + vbCrLf)

			' retrieve all other needed columns
            .Append("arStudent.LastName, arStudent.FirstName, arStudent.MiddleName, " + vbCrLf)
            .Append("(SELECT syRptAgencyFldValues.RptAgencyFldValId " + vbCrLf)
            .Append("	FROM adGenders, " + vbCrLf)
            .Append("        syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies " + vbCrLf)
            .Append("	WHERE adGenders.GenderId = syRptAgencySchoolMapping.SchoolDescripId AND " + vbCrLf)
            .Append("         syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND " + vbCrLf)
            .Append("         syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND " + vbCrLf)
            .Append("         syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND " + vbCrLf)
            .Append("         syRptAgencies.Descrip LIKE '" & AgencyName & "' AND " + vbCrLf)
            .Append("	      adGenders.GenderId = arStudent.Gender) As RptAgencyFldValId_Gender, " + vbCrLf)
            .Append("(SELECT syRptAgencyFldValues.AgencyDescrip " + vbCrLf)
            .Append("	FROM adGenders, " + vbCrLf)
            .Append("        syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies " + vbCrLf)
            .Append("	WHERE adGenders.GenderId = syRptAgencySchoolMapping.SchoolDescripId AND " + vbCrLf)
            .Append("         syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND " + vbCrLf)
            .Append("         syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND " + vbCrLf)
            .Append("         syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND " + vbCrLf)
            .Append("         syRptAgencies.Descrip LIKE '" & AgencyName & "' AND " + vbCrLf)
            .Append("	      adGenders.GenderId = arStudent.Gender) As GenderDescrip, " + vbCrLf)
            .Append("(SELECT syRptAgencyFldValues.RptAgencyFldValId " + vbCrLf)
            .Append("	FROM adEthCodes, " + vbCrLf)
            .Append("        syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies " + vbCrLf)
            .Append("	WHERE adEthCodes.EthCodeId = syRptAgencySchoolMapping.SchoolDescripId AND " + vbCrLf)
            .Append("         syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND " + vbCrLf)
            .Append("         syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND " + vbCrLf)
            .Append("         syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND " + vbCrLf)
            .Append("         syRptAgencies.Descrip LIKE '" & AgencyName & "' AND " + vbCrLf)
            .Append("	      adEthCodes.EthCodeId = arStudent.Race) As RptAgencyFldValId_EthCode, " + vbCrLf)
            .Append("(SELECT syRptAgencyFldValues.AgencyDescrip " + vbCrLf)
            .Append("	FROM adEthCodes, " + vbCrLf)
            .Append("        syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies " + vbCrLf)
            .Append("	WHERE adEthCodes.EthCodeId = syRptAgencySchoolMapping.SchoolDescripId AND " + vbCrLf)
            .Append("         syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND " + vbCrLf)
            .Append("         syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND " + vbCrLf)
            .Append("         syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND " + vbCrLf)
            .Append("         syRptAgencies.Descrip LIKE '" & AgencyName & "' AND " + vbCrLf)
            .Append("	      adEthCodes.EthCodeId = arStudent.Race) As EthCodeDescrip " + vbCrLf)
            .Append("FROM " + vbCrLf)
            .Append("arStudent " + vbCrLf)

			' append list of appropriate students to include
            .Append("WHERE arStudent.StudentId IN (" & StudentList & ") " + vbCrLf)

			' apply primary sort on Gender and Race
            .Append("ORDER BY RptAgencyFldValId_Gender, RptAgencyFldValId_EthCode, " + vbCrLf)

			' apply secondary sort from report param info - either Student Identifier or Last Name
            .Append(IPEDSDB.GetSQL_StudentSort(RptParamInfo) + vbCrLf)


            .Append(";" + vbCrLf)


			' get enrollments for same set of students
            .Append("SELECT " + vbCrLf)
            .Append("arStuEnrollments.StuEnrollId, arStuEnrollments.StudentId, " + vbCrLf)
            .Append("arStuEnrollments.EnrollDate, arStuEnrollments.ExpGradDate, " + vbCrLf)
            .Append("arPrgVersions.Weeks, " + vbCrLf)
            .Append("dbo.sySysStatus.SysStatusDescrip, " + vbCrLf)
            .Append("(SELECT syRptAgencyFldValues.AgencyDescrip " + vbCrLf)
            .Append("	FROM arDropReasons, " + vbCrLf)
            .Append("        syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies " + vbCrLf)
            .Append("	WHERE arDropReasons.DropReasonId = syRptAgencySchoolMapping.SchoolDescripId AND " + vbCrLf)
            .Append("         syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND " + vbCrLf)
            .Append("         syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND " + vbCrLf)
            .Append("         syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND " + vbCrLf)
            .Append("         syRptAgencies.Descrip LIKE '" & AgencyName & "' AND " + vbCrLf)
            .Append("	      arDropReasons.DropReasonId = arStuEnrollments.DropReasonId) As DropReasonDescrip " + vbCrLf)
            .Append("FROM " + vbCrLf)
            .Append("arStuEnrollments, arPrgVersions, arPrograms, syStatusCodes, sySysStatus " + vbCrLf)
            .Append("WHERE " + vbCrLf)
            .Append("arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId AND " + vbCrLf)
            .Append("arPrgVersions.ProgId = arPrograms.ProgId AND " + vbCrLf)
            .Append("arStuEnrollments.StatusCodeId = syStatusCodes.StatusCodeId AND " + vbCrLf)
            .Append("syStatusCodes.SysStatusId = sySysStatus.SysStatusId AND " + vbCrLf)

            ' filter on passed-in list of programs as selected by user
            .Append("arPrograms.ProgId IN (" & RptParamInfo.FilterProgramIDs & ") AND " + vbCrLf)

            ' append list of appropriate students to include
            .Append("arStuEnrollments.StudentId IN (" & StudentList & ") " + vbCrLf)
		End With

		' run queries, get raw DataSet
		dsRaw = IPEDSDB.DataAccessIPEDS().RunSQLDataSet(sb.ToString)

		' set table names for returned data
		With dsRaw
			.Tables(0).TableName = TblNameStudents
			.Tables(1).TableName = TblNameEnrollments
		End With

		' return the raw DataSet
		Return dsRaw

	End Function

End Class
