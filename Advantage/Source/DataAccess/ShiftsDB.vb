
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common


Public Class ShiftsDB
    Public Function GetAllShifts() As DataSet

        '   connect to the database
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   S.ShiftId, S.StatusId, S.ShiftDescrip ")
            .Append("FROM     arShifts S, syStatuses ST ")
            .Append("WHERE    S.StatusId = ST.StatusId ")
            .Append("ORDER BY S.ShiftDescrip ")
        End With

        Try
            ds = db.RunSQLDataSet(sb.ToString)
            Return ds
        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

    End Function
    Public Function GetAllShifts_SP(ByVal showActiveOnly As Boolean) As DataTable

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@showActiveOnly", showActiveOnly, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet("dbo.usp_GetAllShifts", Nothing, "SP").Tables(0)

    End Function
End Class
