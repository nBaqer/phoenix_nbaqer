Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' CertificationsDB.vb
'
' CertificationsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class CertificationsDB

    Public Function GetAllCertifications() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT ")
            .Append("	      (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("	      C.CertificationId, ")
            .Append("	      C.StatusId, ")
            .Append("	      C.CertificationCode, ")
            .Append("	      C.CertificationDescrip ")
            .Append("FROM     syCertifications C, syStatuses ST ")
            .Append("WHERE    C.StatusId = ST.StatusId ")
            .Append("ORDER BY ST.Status,C.CertificationDescrip")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetCertificationInfo(ByVal CertificationId As String) As CertificationInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT C.CertificationId, ")
            .Append("    C.CertificationCode, ")
            .Append("    C.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=C.StatusId) As Status, ")
            .Append("    C.CertificationDescrip, ")
            .Append("    C.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=C.CampGrpId) As CampGrpDescrip, ")
            .Append("    C.ModUser, ")
            .Append("    C.ModDate ")
            .Append("FROM  syCertifications C ")
            .Append("WHERE C.CertificationId= ? ")
        End With

        ' Add the CertificationId to the parameter list
        db.AddParameter("@CertificationId", CertificationId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim CertificationInfo As New CertificationInfo

        While dr.Read()

            '   set properties with data from DataReader
            With CertificationInfo
                .CertificationId = CertificationId
                .IsInDB = True
                .Code = dr("CertificationCode")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Description = dr("CertificationDescrip")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return CertificationInfo
        Return CertificationInfo

    End Function
    Public Function UpdateCertificationInfo(ByVal CertificationInfo As CertificationInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syCertifications Set CertificationId = ?, CertificationCode = ?, ")
                .Append(" StatusId = ?, CertificationDescrip = ?, CampGrpId = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE CertificationId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from syCertifications where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   CertificationId
            db.AddParameter("@CertificationId", CertificationInfo.CertificationId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   EnrollmentId
            db.AddParameter("@CertificationCode", CertificationInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", CertificationInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CertificationDescrip
            db.AddParameter("@CertificationDescrip", CertificationInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If CertificationInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", CertificationInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   CertificationId
            db.AddParameter("@CertificationId", CertificationInfo.CertificationId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", CertificationInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If


        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)


        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddCertificationInfo(ByVal CertificationInfo As CertificationInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT syCertifications (CertificationId, CertificationCode, StatusId, ")
                .Append("   CertificationDescrip, CampGrpId, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   CertificationId
            db.AddParameter("@CertificationId", CertificationInfo.CertificationId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CertificationCode
            db.AddParameter("@CertificationCode", CertificationInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", CertificationInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CertificationDescrip
            db.AddParameter("@CertificationDescrip", CertificationInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If CertificationInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", CertificationInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteCertificationInfo(ByVal CertificationId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syCertifications ")
                .Append("WHERE CertificationId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM syCertifications WHERE CertificationId = ? ")
            End With

            '   add parameters values to the query

            '   CertificationId
            db.AddParameter("@CertificationId", CertificationId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   CertificationId
            db.AddParameter("@CertificationId", CertificationId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If


        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
End Class

