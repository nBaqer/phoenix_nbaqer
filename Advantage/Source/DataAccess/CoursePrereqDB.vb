Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class CoursePrereqDB
    Public Function GetAllCourses(ByVal CampusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess
        Dim da As New OleDbDataAdapter
        Dim ds As New DataSet

   

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        Try
            With sb
                .Append("SELECT t1.ReqId,t1.Descrip,t1.Code, t2.ReqTypeId, t1.StatusId  ")
                .Append("FROM arReqs t1, arReqTypes t2 ")
                .Append("WHERE(t1.ReqTypeId = t2.ReqTypeId And t2.IsGroup = 0 And t2.ReqTypeId = 1) ")
                .Append("AND t1.CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ?)  ")
                If GetAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                    .Append(" and t1.Code not like 'SH%' ")
                End If
                .Append("ORDER BY t1.Descrip,t1.Code")
            End With

            db.AddParameter("@cmpid2", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "ReqDT")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception

        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    ''modified by Saraswathi Lakshmanan on August 4 2009
    ''For prereqs- mantis 16753
    Public Function GetAvailSelectedCourses(ByVal Course As String, ByVal CampusId As String, ByVal PrgverId As String) As DataSet
        Dim ds As New DataSet
        Dim dsTemp As New DataSet
        Dim da As OleDbDataAdapter
        Dim db2 As New DataAccess
        Dim dstemp2 As New DataSet

        Try

       

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder


            db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Reqid", Course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PrgVerId", PrgverId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            dsTemp = db.RunParamSQLDataSetUsingSP("Sp_getPreCoReqsandReqsforReqs", Nothing)

            Dim dsChildParentreqs As New DataSet
            dsChildParentreqs.Tables.Add(dsTemp.Tables(0).Copy)
            Dim IParentandChildrowcountforGivenReq As Integer
            If dsTemp.Tables.Count > 0 Then
                If dsTemp.Tables(0).Rows.Count > 0 Then
                    For IParentandChildrowcountforGivenReq = 0 To dsTemp.Tables(0).Rows.Count - 1
                        Dim db1 As New DataAccess
                        Dim dstemp1 As New DataSet
                        db1.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

                        db1.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db1.AddParameter("@Reqid", dsTemp.Tables(0).Rows(IParentandChildrowcountforGivenReq)("REQID").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db1.AddParameter("@PrgVerId", PrgverId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                        dstemp1 = db1.RunParamSQLDataSetUsingSP("Sp_getPreCoReqsandReqsforReqs", Nothing)
                        If dstemp1.Tables.Count > 0 Then
                            If dstemp1.Tables(0).Rows.Count > 0 Then
                                For Each drSource As DataRow In dstemp1.Tables(0).Rows
                                    dsChildParentreqs.Tables(0).ImportRow(drSource)
                                Next

                                'dstemp1.Tables(0).TableName = dsTemp.Tables(0).Rows(IParentandChildrowcountforGivenReq)("Code")
                                'dsChildParentreqs.Tables(0).Rows.Add(dstemp1.Tables(0).Rows.Copy)
                            End If
                        End If
                        dstemp1.Clear()
                    Next

                End If
            End If
            ''Append all the reqids as one string



            db2.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

            db2.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db2.AddParameter("@PrgVerId", PrgverId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            If GetAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                dstemp2 = db2.RunParamSQLDataSetUsingSP("USP_GetReqsForProgramVersion_CourtReporting", Nothing)
            Else
                dstemp2 = db2.RunParamSQLDataSetUsingSP("Sp_GetReqsForProgramVersion", Nothing)
            End If




            If dstemp2.Tables.Count > 0 Then
                If dstemp2.Tables(0).Rows.Count > 0 Then
                    With dstemp2.Tables(0)
                        .PrimaryKey = New DataColumn() {.Columns("ReqId")}
                    End With
                    If dsChildParentreqs.Tables.Count > 0 Then
                        If dsChildParentreqs.Tables(0).Rows.Count > 0 Then
                            'With dsChildParentreqs.Tables(0)
                            '    .PrimaryKey = New DataColumn() {.Columns("ReqId")}
                            'End With
                            For Each dr As DataRow In dsChildParentreqs.Tables(0).Rows
                                If dstemp2.Tables(0).Rows.Find(dr("ReqID")) IsNot Nothing Then
                                    Dim drfoundRow As DataRow
                                    drfoundRow = dstemp2.Tables(0).Rows.Find(dr("ReqID"))
                                    dstemp2.Tables(0).Rows.Remove(drfoundRow)
                                End If
                            Next
                        End If
                    End If
                End If
            End If
            If dstemp2.Tables(0).Rows.Count > 0 Then
                Dim dr1 As DataRow
                dr1 = dstemp2.Tables(0).Rows.Find(Course)
                If dr1 IsNot Nothing Then
                    dstemp2.Tables(0).Rows.Remove(dr1)
                End If
            End If
            dstemp2.AcceptChanges()

            ''   Execute the query
            'db.OpenConnection()
            'da = db.RunParamSQLDataAdapter(sb.ToString)
            'da.Fill(ds, "AvailCourses")
            sb.Remove(0, sb.Length)
            db.ClearParameters()
            Dim da2 As New OleDbDataAdapter

            If PrgverId = Guid.Empty.ToString Then
                With sb
                    .Append("SELECT DISTINCT a.PreCoReqId, b.Descrip,b.Code ")
                    .Append("FROM arCourseReqs a, arReqs b ")
                    .Append("WHERE a.ReqId = ? and a.CourseReqTypId = 1 and a.PreCoReqId = b.ReqId ")
                    .Append("and b.CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    .Append(" and (a.PrgVerId is Null) ")
                    If GetAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                        '.Append(" and b.Code not like 'SH%' ")
                        'Code modified to assume that all courses mapped to Dictation/Speed Test Component will be SH Courses
                        .Append(" and b.ReqId not in (Select Distinct ReqId from arBridge_GradeComponentTypes_Courses) ")
                    End If
                    .Append("ORDER BY b.Descrip,b.Code ")
                End With

            Else
                With sb
                    .Append("SELECT DISTINCT a.PreCoReqId, b.Descrip,b.Code ")
                    .Append("FROM arCourseReqs a, arReqs b ")
                    .Append("WHERE a.ReqId = ? and a.CourseReqTypId = 1 and a.PreCoReqId = b.ReqId ")
                    .Append("and b.CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    .Append(" and (a.PrgVerId =?) ")
                    If GetAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                        '.Append(" and b.Code not like 'SH%' ")
                        'Code modified to assume that all courses mapped to Dictation/Speed Test Component will be SH Courses
                        .Append(" and b.ReqId not in (Select Distinct ReqId from arBridge_GradeComponentTypes_Courses) ")
                    End If
                    .Append(" Union ")
                    .Append("SELECT DISTINCT a.PreCoReqId, b.Descrip+' (All Programs)',b.Code ")
                    .Append("FROM arCourseReqs a, arReqs b ")
                    .Append("WHERE a.ReqId = ? and a.CourseReqTypId = 1 and a.PreCoReqId = b.ReqId ")
                    .Append("and b.CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    .Append(" and (a.PrgVerId is Null) ")
                    If GetAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                        '.Append(" and b.Code not like 'SH%' ")
                        'Code modified to assume that all courses mapped to Dictation/Speed Test Component will be SH Courses
                        .Append(" and b.ReqId not in (Select Distinct ReqId from arBridge_GradeComponentTypes_Courses) ")
                    End If
                    .Append("ORDER BY b.Descrip,b.Code ")
                End With


            End If
            'db.OpenConnection()
            If PrgverId = Guid.Empty.ToString Then
                db.AddParameter("@course", Course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@campusid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@course", Course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@campusid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@PrgVerId", PrgverId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@course", Course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@campusid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            da2 = db.RunParamSQLDataAdapter(sb.ToString)
            Try
                da2.Fill(ds, "SelectedCourses")
            Catch ex As System.Exception
                Throw New BaseException(ex.Message)
            End Try
            sb.Remove(0, sb.Length)


            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        dsTemp.Tables(0).TableName = "AvailCourses"

        ds.Tables.Add(dstemp2.Tables(0).Copy)
        ds.Tables(1).TableName = "AvailCourses"


        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function GetAvailSelectedCoreqs(ByVal Course As String, ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            'With sb
            '    .Append("SELECT DISTINCT a.ReqId, a.Descrip ")
            '    .Append("FROM arReqs a ")
            '    .Append("WHERE a.ReqId <> ? ")
            'End With

            With sb
                .Append("select * from arReqs where ReqId not in ( ")

                .Append("select PreCoReqId from arCourseReqs where PreCoReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId=?) ")

                .Append(" union ")


                .Append("select PreCoReqId from arCourseReqs where PreCoReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId=?)) ")

                .Append("union ")


                .Append("select PreCoReqId from arCourseReqs where PreCoReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId=?))) ")

                .Append("union ")


                .Append("select PreCoReqId from arCourseReqs where PreCoReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId=?)))) ")

                .Append("union ")


                .Append("select PreCoReqId from arCourseReqs where PreCoReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId=?))))) ")

                .Append("union ")


                .Append("select PreCoReqId from arCourseReqs where PreCoReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")
                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId=?)))))) ")

                .Append("union ")


                .Append("select PreCoReqId from arCourseReqs where PreCoReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")
                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")
                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")
                .Append("select PreCoReqId from arCourseReqs where ReqId=?))))))) ")

                .Append("union ")

                .Append("select PreCoReqId from arCourseReqs where PreCoReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")
                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")
                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")
                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")
                .Append("select PreCoReqId from arCourseReqs where ReqId=?)))))))) ")

                .Append("union ")

                .Append("select PreCoReqId from arCourseReqs where PreCoReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")
                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")
                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")
                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")
                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")
                .Append("select PreCoReqId from arCourseReqs where ReqId=?))))))))) ")

                .Append("union ")

                .Append("select PreCoReqId from arCourseReqs where PreCoReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append(" select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId in ( ")

                .Append("select PreCoReqId from arCourseReqs where ReqId=?))))))))))) ")

                '.Append("and arReqs.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' ")

                .Append("and arReqs.ReqId <> ? ")

                .Append("AND arReqs.CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ?)  ")

                .Append("ORDER BY arReqs.Descrip ")
            End With
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@course1", Course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@course2", Course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@course3", Course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@course4", Course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@course5", Course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@course6", Course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@course7", Course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@course8", Course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@course9", Course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@course0", Course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@courseid", Course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@campus", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "AvailCourses")
            sb.Remove(0, sb.Length)
            db.ClearParameters()


            Dim da2 As New OleDbDataAdapter
            With sb
                .Append("SELECT DISTINCT a.PreCoReqId,b.Code, b.Descrip ")
                .Append("FROM arCourseReqs a, arReqs b ")
                .Append("WHERE a.ReqId = ? and a.CourseReqTypId = 2 and a.PreCoReqId = b.ReqId ")
                .Append("and b.CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                .Append("ORDER BY b.Descrip,b.Code ")
            End With

            'db.OpenConnection()
            db.AddParameter("@course", Course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@campusid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            da2 = db.RunParamSQLDataAdapter(sb.ToString)
            Try
                da2.Fill(ds, "SelectedCourses")
            Catch ex As System.Exception
                Throw New BaseException(ex.Message)
            End Try
            sb.Remove(0, sb.Length)


            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    ''Modified by Saraswathi on July 29 2009
    ''to fix issue 16753: BUG: Cannot apply pre-req to just one program. 
    Public Function DeleteCoursePreCoreq(ByVal sReqId As String, ByVal PrereqId As String, ByVal Progverid As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

    

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        'Try
        With sb
            .Append("DELETE FROM arCourseReqs ")
            .Append("WHERE ReqId = ? ")
            .Append("AND PreCoReqId = ? ")
            If Progverid = Guid.Empty.ToString Then
                .Append("AND PrgVerid is Null ")
            Else
                .Append("AND PrgVerid = ? ")
            End If


        End With

        db.AddParameter("@courseid", sReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prereqid", PrereqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        If Not Progverid = Guid.Empty.ToString Then
            db.AddParameter("@Progverid", Progverid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If




        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        '    '   execute the query
        '    Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

        '    '   If the row was not deleted then there was a concurrency problem
        '    If rowCount = 0 Then
        '        '   return without errors
        '        Return ""
        '    Else
        '        Return DALExceptions.BuildConcurrencyExceptionMessage()
        '    End If

        'Catch ex As OleDbException
        '    '   return an error to the client
        '    Return DALExceptions.BuildErrorMessage(ex)
        'Finally
        '    'Close Connection
        '    db.CloseConnection()
        'End Try
    End Function
    ''Modified by Saraswathi on July 29 2009
    ''to fix issue 16753: BUG: Cannot apply pre-req to just one program. 
    Public Function InsertCoursePreCoreq(ByVal Progverid As String, ByVal sReqId As String, ByVal PrereqId As String, ByVal CourseReqTyp As Integer, ByVal user As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        'Insert row into arCourseReqs table
        With sb
            .Append("INSERT INTO arCourseReqs(ReqId,PreCoReqId,CourseReqTypId,ModUser,ModDate,PrgVerId) ")
            .Append("VALUES(?,?,?,?,?,?)")
        End With

        db.AddParameter("@courseid", sReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@precoreqid", PrereqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqtypid", CourseReqTyp, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        ''ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ''ModDate
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        If Progverid = Guid.Empty.ToString Then
            db.AddParameter("@ProgVerId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@ProgVerId", Progverid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)
    End Function
    ''Added by Saraswathi Laksshmanan on July 29 2009
    ''to fix issue 16753: BUG: Cannot apply pre-req to just one program. 
    Public Function GetTheCoursesforProgverid(ByVal ProgverId As String, ByVal CampusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess
        Dim da As New OleDbDataAdapter
        Dim ds As New DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        ''Modified by Saraswathi lakshmanan on Sept 22 2009
        ''Course group added to fix mantis issue 
        ''17611: QA: Courses belonging to a course group are not shown on the enter prerequisites page. 
        '   build the sql query
        Dim sb As New StringBuilder
        Try
            With sb
                .Append("SELECT t1.ReqId,t1.Descrip Descrip,t1.Code, t3.ReqTypeId, t1.StatusId  ")
                .Append("FROM arReqs t1,arProgVerDef t2 ,arReqTypes t3 ")
                .Append("WHERE t1.ReqId=t2.ReqId and t1.ReqTypeId = 1 and PrgVerId = ? ")
                .Append("And t1.ReqTypeId = t3.ReqTypeId And t3.IsGroup = 0   ")
                .Append("AND t1.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId=?)  ")
                If GetAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                    .Append(" and t1.Code not like 'SH%' ")
                End If
                .Append(" Union ")
                .Append(" SELECT t5.ReqId,t5.Descrip Descrip,t5.Code, t5.ReqTypeId, t5.StatusId ")
                .Append(" FROM arReqs t1,arProgVerDef t2 ,arReqTypes t3,arReqGrpDef t4 ,arReqs T5 ")
                .Append("  WHERE t1.ReqId=t4.GrpID and t1.ReqId=t2.ReqId and t1.ReqTypeId = 2 and PrgVerId =? ")
                .Append("  And t1.ReqTypeId = t3.ReqTypeId And t3.IsGroup = 1  ")
                .Append(" AND t1.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId=?)")
                If GetAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                    .Append(" and t1.Code not like 'SH%' ")
                End If
                .Append("  and t4.ReqId=t5.ReqId order by Descrip")
            End With

            db.AddParameter("@Prgverid", ProgverId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@cmpid2", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.AddParameter("@Prgverid", ProgverId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@cmpid2", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "Courses")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception

        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    ''Addded by Saraswathi lakshmanan on August 4 2009
    ''For prereqs mantis 16753
    Public Function getPrereqsandReqsForSelectediteminAvailListBox(ByVal CampusId As String, ByVal ReqId As String, ByVal PrgverId As String) As DataSet
        Dim dsTemp As New DataSet
        Dim dsChildParentreqs As New DataSet
        Try


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            'db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'db.AddParameter("@Reqid", ReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'db.AddParameter("@PrgVerId", PrgverId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'dsTemp = db.RunParamSQLDataSetUsingSP("Sp_getPreCoReqsandReqsforReqs", Nothing)
            db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Reqid", ReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PrgVerId", PrgverId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            dsTemp = db.RunParamSQLDataSetUsingSP("Sp_getPreCoReqsandReqsforReqs", Nothing)

            dsChildParentreqs.Tables.Add(dsTemp.Tables(0).Copy)
            Dim IParentandChildrowcountforGivenReq As Integer
            If dsTemp.Tables.Count > 0 Then
                If dsTemp.Tables(0).Rows.Count > 0 Then
                    For IParentandChildrowcountforGivenReq = 0 To dsTemp.Tables(0).Rows.Count - 1
                        Dim db1 As New DataAccess
                        Dim dstemp1 As New DataSet
                        db1.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

                        db1.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db1.AddParameter("@Reqid", dsTemp.Tables(0).Rows(IParentandChildrowcountforGivenReq)("REQID").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db1.AddParameter("@PrgVerId", PrgverId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                        dstemp1 = db1.RunParamSQLDataSetUsingSP("Sp_getPreCoReqsandReqsforReqs", Nothing)
                        If dstemp1.Tables.Count > 0 Then
                            If dstemp1.Tables(0).Rows.Count > 0 Then
                                For Each drSource As DataRow In dstemp1.Tables(0).Rows
                                    dsChildParentreqs.Tables(0).ImportRow(drSource)
                                Next

                                'dstemp1.Tables(0).TableName = dsTemp.Tables(0).Rows(IParentandChildrowcountforGivenReq)("Code")
                                'dsChildParentreqs.Tables(0).Rows.Add(dstemp1.Tables(0).Rows.Copy)
                            End If
                        End If
                        dstemp1.Clear()
                    Next

                End If
            End If
            ''Append all the reqids as one string


            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return dsChildParentreqs

    End Function
#Region "Get AdvAppsetting for Manage Config entry"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
End Class
