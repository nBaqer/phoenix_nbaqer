' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' TuitionEarningsDB.vb
'
' TuitionEarningsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Imports System.Collections.Generic
Imports FAME.Advantage.Common

Public Class TuitionEarningsDB
    Public Function GetAllTuitionEarnings() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   TEBMC.TuitionEarningId, ")
            .Append("       (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("         TEBMC.TuitionEarningsCode, ")
            .Append("         TEBMC.TuitionEarningsDescrip ")
            .Append("FROM     saTuitionEarnings TEBMC, syStatuses ST ")
            .Append("WHERE    TEBMC.StatusId = ST.StatusId ")
            .Append(" ORDER BY ST.StatusId,TEBMC.TuitionEarningsDescrip asc ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetTuitionEarningsInfo(ByVal TuitionEarningId As String) As TuitionEarningsInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT TEBMC.TuitionEarningId, ")
            .Append("    TEBMC.TuitionEarningsCode, ")
            .Append("    TEBMC.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=TEBMC.StatusId) As Status, ")
            .Append("    TEBMC.TuitionEarningsDescrip, ")
            .Append("    TEBMC.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=TEBMC.CampGrpId) As CampGrpDescrip, ")
            .Append("    TEBMC.RevenueBasisIdx, ")
            .Append("    TEBMC.PercentageRangeBasisIdx, ")
            .Append("    TEBMC.PercentToEarnIdx, ")
            .Append("    TEBMC.FullMonthBeforeDay, ")
            .Append("    TEBMC.HalfMonthBeforeDay, ")
            .Append("    TEBMC.IsAttendanceRequired, ")
            .Append("    TEBMC.RequiredAttendancePercent, ")
            .Append("    TEBMC.RequiredCumulativeHoursAttended, ")
            .Append("    TEBMC.ModUser, ")
            .Append("    TEBMC.ModDate ")
            .Append("FROM  saTuitionEarnings TEBMC ")
            .Append("WHERE TuitionEarningId= ? ")
        End With

        ' Add the TuitionEarningId to the parameter list
        db.AddParameter("@TuitionEarningId", TuitionEarningId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim TuitionEarningsInfo As New TuitionEarningsInfo

        While dr.Read()

            '   set properties with data from DataReader
            With TuitionEarningsInfo
                .TuitionEarningId = TuitionEarningId
                .IsInDB = True
                .Code = dr("TuitionEarningsCode")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Description = dr("TuitionEarningsDescrip")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                .RevenueBasis = dr("RevenueBasisIdx")
                .PercentageRangeBasis = dr("PercentageRangeBasisIdx")
                .PercentToEarn = dr("PercentToEarnIdx")
                If Not (dr("FullMonthBeforeDay") Is System.DBNull.Value) Then .FullMonthBeforeDay = dr("FullMonthBeforeDay")
                If Not (dr("HalfMonthBeforeDay") Is System.DBNull.Value) Then .HalfMonthBeforeDay = dr("HalfMonthBeforeDay")
                If Not (dr("IsAttendanceRequired") Is System.DBNull.Value) Then .IsAttendanceRequired = dr("IsAttendanceRequired")
                If Not (dr("RequiredAttendancePercent") Is System.DBNull.Value) Then .RequiredAttendancePercent = dr("RequiredAttendancePercent")
                If Not (dr("RequiredCumulativeHoursAttended") Is System.DBNull.Value) Then .RequiredCumulativeHoursAttended = dr("RequiredCumulativeHoursAttended")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return TuitionEarningsInfo
        Return TuitionEarningsInfo

    End Function
    Public Function UpdateTuitionEarningsInfo(ByVal TuitionEarningsInfo As TuitionEarningsInfo, ByVal user As String) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   Connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saTuitionEarnings Set TuitionEarningId = ?, TuitionEarningsCode = ?, ")
                .Append(" StatusId = ?, TuitionEarningsDescrip = ?, CampGrpId = ?, ")
                .Append(" RevenueBasisIdx = ?, PercentageRangeBasisIdx = ?, PercentToEarnIdx = ?, FullMonthBeforeDay = ?, ")
                .Append(" HalfMonthBeforeDay = ?, IsAttendanceRequired = ?, RequiredAttendancePercent = ?, ")
                .Append(" RequiredCumulativeHoursAttended = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE TuitionEarningId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saTuitionEarnings where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   TuitionEarningId
            db.AddParameter("@TuitionEarningId", TuitionEarningsInfo.TuitionEarningId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TuitionEarningsCode
            db.AddParameter("@TuitionEarningsCode", TuitionEarningsInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", TuitionEarningsInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TuitionEarningsDescrip
            db.AddParameter("@TuitionEarningsDescrip", TuitionEarningsInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If TuitionEarningsInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", TuitionEarningsInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   RevenueBasis
            db.AddParameter("@RevenueBasisIdx", TuitionEarningsInfo.RevenueBasis, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   PercentageRangeBasis
            db.AddParameter("@PercentageRangeBasisIdx", TuitionEarningsInfo.PercentageRangeBasis, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   PercentToEarn
            db.AddParameter("@PercentToEarnIdx", TuitionEarningsInfo.PercentToEarn, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   FullMonthBeforeDay
            If TuitionEarningsInfo.FullMonthBeforeDay = 0 Then
                db.AddParameter("@FullMonthBeforeDay", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Else
                db.AddParameter("@FullMonthBeforeDay", TuitionEarningsInfo.FullMonthBeforeDay, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End If

            '   HalfMonthBeforeDay
            If TuitionEarningsInfo.HalfMonthBeforeDay = 0 Then
                db.AddParameter("@HalfMonthBeforeDay", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Else
                db.AddParameter("@HalfMonthBeforeDay", TuitionEarningsInfo.HalfMonthBeforeDay, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End If

            '   IsAttendanceRequired
            If TuitionEarningsInfo.IsAttendanceRequired = False Then
                db.AddParameter("@IsAttendanceRequired", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Else
                db.AddParameter("@IsAttendanceRequired", TuitionEarningsInfo.IsAttendanceRequired, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If

            '   RequiredAttendancePercent
            If TuitionEarningsInfo.RequiredAttendancePercent = 0 Then
                db.AddParameter("@RequiredAttendancePercent", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Else
                db.AddParameter("@RequiredAttendancePercent", TuitionEarningsInfo.RequiredAttendancePercent, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End If

            '   RequiredCumulativeHoursAttended
            If TuitionEarningsInfo.RequiredAttendancePercent = 0 Then
                db.AddParameter("@RequiredCumulativeHoursAttended", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Else
                db.AddParameter("@RequiredCumulativeHoursAttended", TuitionEarningsInfo.RequiredCumulativeHoursAttended, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TuitionEarningId
            db.AddParameter("@AdmDepositId", TuitionEarningsInfo.TuitionEarningId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", TuitionEarningsInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddTuitionEarningsInfo(ByVal TuitionEarningsInfo As TuitionEarningsInfo, ByVal user As String) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   Connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saTuitionEarnings (TuitionEarningId, TuitionEarningsCode, StatusId, ")
                .Append("   TuitionEarningsDescrip, CampGrpId, ")
                .Append("   RevenueBasisIdx, PercentageRangeBasisIdx, PercentToEarnIdx, FullMonthBeforeDay, ")
                .Append("   HalfMonthBeforeDay, IsAttendanceRequired, RequiredAttendancePercent, ")
                .Append("   RequiredCumulativeHoursAttended, ")
                .Append("   ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   TuitionEarningId
            db.AddParameter("@TuitionEarningId", TuitionEarningsInfo.TuitionEarningId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TuitionEarningsCode
            db.AddParameter("@TuitionEarningsCode", TuitionEarningsInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", TuitionEarningsInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TuitionEarningsDescrip
            db.AddParameter("@TuitionEarningsDescrip", TuitionEarningsInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If TuitionEarningsInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", TuitionEarningsInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   RevenueBasis
            db.AddParameter("@RevenueBasisIdx", TuitionEarningsInfo.RevenueBasis, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   PercentageRangeBasis
            db.AddParameter("@PercentageRangeBasisIdx", TuitionEarningsInfo.PercentageRangeBasis, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   PercentToEarn
            db.AddParameter("@PercentToEarnIdx", TuitionEarningsInfo.PercentToEarn, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   FullMonthBeforeDay
            If TuitionEarningsInfo.FullMonthBeforeDay = 0 Then
                db.AddParameter("@FullMonthBeforeDay", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Else
                db.AddParameter("@FullMonthBeforeDay", TuitionEarningsInfo.FullMonthBeforeDay, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End If

            '   HalfMonthBeforeDay
            If TuitionEarningsInfo.HalfMonthBeforeDay = 0 Then
                db.AddParameter("@HalfMonthBeforeDay", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Else
                db.AddParameter("@HalfMonthBeforeDay", TuitionEarningsInfo.HalfMonthBeforeDay, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End If

            '   IsAttendanceRequired
            If TuitionEarningsInfo.IsAttendanceRequired = False Then
                db.AddParameter("@IsAttendanceRequired", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Else
                db.AddParameter("@IsAttendanceRequired", TuitionEarningsInfo.IsAttendanceRequired, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If

            '   RequiredAttendancePercent
            If TuitionEarningsInfo.RequiredAttendancePercent = 0 Then
                db.AddParameter("@RequiredAttendancePercent", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Else
                db.AddParameter("@RequiredAttendancePercent", TuitionEarningsInfo.RequiredAttendancePercent, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End If

            '   RequiredCumulativeHoursAttended
            If TuitionEarningsInfo.RequiredAttendancePercent = 0 Then
                db.AddParameter("@RequiredCumulativeHoursAttended", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Else
                db.AddParameter("@RequiredCumulativeHoursAttended", TuitionEarningsInfo.RequiredCumulativeHoursAttended, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteTuitionEarningsInfo(ByVal TuitionEarningId As String, ByVal modDate As DateTime) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   Connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saTuitionEarnings ")
                .Append("WHERE TuitionEarningId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM saTuitionEarnings WHERE TuitionEarningId = ? ")
            End With

            '   add parameters values to the query

            '   TuitionEarningId
            db.AddParameter("@TuitionEarningId", TuitionEarningId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TuitionEarningId
            db.AddParameter("@TuitionEarningId", TuitionEarningId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetTuitionEarningsDS() As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   create dataset
        Dim ds As New DataSet

        '   build the sql query for the TuitionEarningsPercentages data adapter
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       TEPR.TuitionEarningsPercentageRangeId, ")
            .Append("       TEPR.TuitionEarningId, ")
            .Append("       TEPR.UpTo, ")
            .Append("       TEPR.EarnPercent, ")
            .Append("       TEPR.ViewOrder, ")
            .Append("       TEPR.ModUser, ")
            .Append("       TEPR.ModDate ")
            .Append("FROM   saTuitionEarningsPercentageRanges TEPR, saTuitionEarnings TEBMC, syStatuses ST ")
            .Append("WHERE  TEPR.TuitionEarningId=TEBMC.TuitionEarningId ")
            .Append(" AND   TEBMC.StatusId = ST.StatusId ")
            .Append("ORDER BY ST.Status, TEBMC.TuitionEarningsDescrip asc")
        End With

        '   build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

        '   Create adapter to handle TuitionEarningsPercentageRanges table
        Dim da As New OleDbDataAdapter(sc)

        '   Fill TuitionEarningsPercentageRanges table
        da.Fill(ds, "TuitionEarningsPercentageRanges")

        '   build select query for the TuitionEarnings data adapter
        '   build the sql query
        sb = New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT TEBMC.TuitionEarningId, ")
            .Append("    TEBMC.TuitionEarningsCode, ")
            .Append("    (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("    TEBMC.StatusId, ")
            '.Append("    (Select Status from syStatuses where StatusId=TEBMC.StatusId) As Status, ")
            .Append("    TEBMC.TuitionEarningsDescrip, ")
            .Append("    TEBMC.CampGrpId, ")
            '.Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=TEBMC.CampGrpId) As CampGrpDescrip, ")
            .Append("    TEBMC.RevenueBasisIdx, ")
            .Append("    TEBMC.PercentageRangeBasisIdx, ")
            .Append("    TEBMC.PercentToEarnIdx, ")
            ''Added by Saraswathi On July 1 2009
            ''To Take holidays into account or not
            .Append("    TEBMC.TakeHolidays, ")
            ''Added by Saraswathi On May 19 2010
            .Append("    TEBMC.RemainingMethod, ")

            .Append("    TEBMC.FullMonthBeforeDay, ")
            .Append("    TEBMC.HalfMonthBeforeDay, ")
            .Append("    TEBMC.IsAttendanceRequired, ")
            .Append("    TEBMC.RequiredAttendancePercent, ")
            .Append("    TEBMC.RequiredCumulativeHoursAttended, ")
            .Append("    TEBMC.ModUser, ")
            .Append("    TEBMC.ModDate ")
            .Append("FROM  saTuitionEarnings TEBMC, syStatuses ST ")
            .Append("WHERE  TEBMC.StatusId = ST.StatusId ")
            .Append("ORDER BY ST.Status,TEBMC.TuitionEarningsDescrip asc")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   fill saTuitionEarnings table
        da.Fill(ds, "TuitionEarnings")

        '   create primary and foreign key constraints

        '   set primary key for saTuitionEarningsPercentageRanges table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("TuitionEarningsPercentageRanges").Columns("TuitionEarningsPercentageRangeId")
        ds.Tables("TuitionEarningsPercentageRanges").PrimaryKey = pk0

        '   set primary key for saTuitionEarnings table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("TuitionEarnings").Columns("TuitionEarningId")
        ds.Tables("TuitionEarnings").PrimaryKey = pk1

        '   get foreign key column in saTuitionEarningsPercentageRanges
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("TuitionEarningsPercentageRanges").Columns("TuitionEarningId")

        '   add relationship
        ds.Relations.Add("TuitionEarningsTuitionEarningsPercentageRanges", pk1, fk0)

        '   return dataset
        Return ds

    End Function
    Public Function UpdateTuitionEarningsDS(ByVal ds As DataSet) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   all updates must be encapsulated as one transaction
        Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            '   build the sql query for the TuitionEarningsPercentages data adapter
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("       TEPR.TuitionEarningsPercentageRangeId, ")
                .Append("       TEPR.TuitionEarningId, ")
                .Append("       TEPR.UpTo, ")
                .Append("       TEPR.EarnPercent, ")
                .Append("       TEPR.ViewOrder, ")
                .Append("       TEPR.ModUser, ")
                .Append("       TEPR.ModDate ")
                .Append("FROM   saTuitionEarningsPercentageRanges TEPR ")
            End With

            '   build select command
            Dim TuitionEarningsPercentageRangesSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle TuitionEarningsPercentageRanges table
            Dim TuitionEarningsPercentageRangesDataAdapter As New OleDbDataAdapter(TuitionEarningsPercentageRangesSelectCommand)

            '   build insert, update and delete commands for TuitionEarningsPercentageRanges table
            Dim cb As New OleDbCommandBuilder(TuitionEarningsPercentageRangesDataAdapter)

            '   build select query for the TuitionEarnings data adapter
            sb = New StringBuilder
            With sb
                '   with subqueries
                .Append("SELECT TEBMC.TuitionEarningId, ")
                .Append("    TEBMC.TuitionEarningsCode, ")
                .Append("    TEBMC.StatusId, ")
                '.Append("    (Select Status from syStatuses where StatusId=TEBMC.StatusId) As Status, ")
                .Append("    TEBMC.TuitionEarningsDescrip, ")
                .Append("    TEBMC.CampGrpId, ")
                '.Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=TEBMC.CampGrpId) As CampGrpDescrip, ")
                .Append("    TEBMC.RevenueBasisIdx, ")
                ''Added by Saraswathi lakshmanan on July 1 2009
                .Append("    TEBMC.TakeHolidays, ")
                ''Added by Saraswathi lakshmanan on May 19 2010
                .Append("    TEBMC.RemainingMethod, ")

                .Append("    TEBMC.PercentageRangeBasisIdx, ")
                .Append("    TEBMC.PercentToEarnIdx, ")
                .Append("    TEBMC.FullMonthBeforeDay, ")
                .Append("    TEBMC.HalfMonthBeforeDay, ")
                .Append("    TEBMC.IsAttendanceRequired, ")
                .Append("    TEBMC.RequiredAttendancePercent, ")
                .Append("    TEBMC.RequiredCumulativeHoursAttended, ")
                .Append("    TEBMC.ModUser, ")
                .Append("    TEBMC.ModDate ")
                .Append("FROM  saTuitionEarnings TEBMC")
            End With

            '   build select command
            Dim TuitionEarningsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle TuitionEarnings table
            Dim TuitionEarningsDataAdapter As New OleDbDataAdapter(TuitionEarningsSelectCommand)

            '   build insert, update and delete commands for TuitionEarnings table
            Dim cb1 As New OleDbCommandBuilder(TuitionEarningsDataAdapter)

            '   insert added rows in TuitionEarnings table
            TuitionEarningsDataAdapter.Update(ds.Tables("TuitionEarnings").Select(Nothing, Nothing, DataViewRowState.Added))

            '   insert added rows in TuitionEarningsPercentageRanges table
            TuitionEarningsPercentageRangesDataAdapter.Update(ds.Tables("TuitionEarningsPercentageRanges").Select(Nothing, Nothing, DataViewRowState.Added))

            '   delete rows in TuitionEarningsPercentageRanges table
            TuitionEarningsPercentageRangesDataAdapter.Update(ds.Tables("TuitionEarningsPercentageRanges").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   delete rows in TuitionEarnings table
            TuitionEarningsDataAdapter.Update(ds.Tables("TuitionEarnings").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   update rows in TuitionEarningsPercentageRanges table
            TuitionEarningsPercentageRangesDataAdapter.Update(ds.Tables("TuitionEarningsPercentageRanges").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   update rows in TuitionEarnings table
            TuitionEarningsDataAdapter.Update(ds.Tables("TuitionEarnings").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            Return ""

        Catch ex As OleDb.OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally

            '   close connection
            connection.Close()
        End Try

    End Function
    Public Function GetAllDeferredRevenuesByTransCode(ByVal reportDate As Date) As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("Select ")
            .Append("       T.TransCodeId, ")
            .Append("       (Select TransCodeDescrip from saTransCodes where TransCodeId=T.TransCodeId) As TransCodeDescrip, ")
            .Append("       sum(T.TransAmount) TotalAmount, ")
            .Append("       Coalesce(dbo.GetDeferredRevenueByTransCode(T.TransCodeId, ?) - Coalesce((Select sum(Amount) from saDeferredRevenues where TransactionId in (select TransactionId from saTransactions where TransCodeId=T.TransCodeId and saTransactions.Voided=0)), 0),0) DeferredRevenueAmount, ")
            .Append("       Coalesce((Select sum(Amount) from saDeferredRevenues where TransactionId in (Select transactionId from saTransactions where TransCodeId=T.TransCodeId and saTransactions.Voided=0)),0) AccruedAmount ")
            .Append("From saTransactions T, saTransCodes TC ")
            .Append("Where ")
            .Append("       (T.TransCodeId = TC.TransCodeId) ")
            .Append("And    TC.DefEarnings = 1 ")
            .Append("AND    T.Voided=0 ")
            .Append("       Group By T.TransCodeId, TransCodeDescrip ")
        End With

        ' Add ReportDate to the parameter list
        db.AddParameter("@ReportDate", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    'Public Function GetAllDeferredRevenuesByTransCodeDBI(ByVal reportDate As Date, ByVal campusId As String) As DataTable

    '    '   build DeferredRevenues dataset
    '    Return BuildDatasetForDeferredRevenueCalculations(reportDate, campusId).Tables("TransCodes")

    'End Function
    ''Modified by Saraswathi lakshmanan on June 29 2009
    ''The return type is changed from datatable to datatset
    ''Modified by Saraswathi on May 18n 2010
    ''Deferred revenue Calculation method is added
    ''18962: Deferred Revenue Posting issue:Negative Tuition Amounts 
    ''Optional parameter added bvy Saraswathi Lakshmanan On June 30 2010
    ''Tofix deferred revenue 19313: Need a Earliest Transaction Date when Processing Deferred Revenue 
    Public Function GetAllDeferredRevenuesByTransCodeDBI(ByVal reportDate As Date, ByVal campusId As String, Optional ByVal TransDate As String = "") As DataSet

        Return BuildDatasetForDeferredRevenueCalculationsTraditionalAndRemainingMethod(reportDate, campusId, TransDate)

    End Function
    Public Function GetDeferredRevenueDetailsByTransCode(ByVal transCodeId As String, ByVal reportDate As Date) As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("Select ")
            .Append("       DR.DefRevenueId, ")
            .Append("       (Select FirstName + ' ' + LastName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=(Select StuEnrollId from saTransactions where TransactionId=DR.TransactionId and saTransactions.Voided=0))) StudentName, ")
            .Append("       DR.DefRevenueDate, ")
            .Append("       DR.Source, ")
            .Append("       DR.Amount Accrued, ")
            .Append("       0.00 ToAccrue, ")
            .Append("       0.00 Total, ")
            .Append("       DR.IsPosted ")
            .Append("From	saDeferredRevenues DR, saTransactions T, saTransCodes TC ")
            .Append("Where ")
            .Append("       (DR.TransactionId=T.TransactionId) ")
            .Append("And    (T.TransCodeId = TC.TransCodeId) ")
            .Append("And    TC.DefEarnings = 1 ")
            .Append("AND    T.Voided=0 ")
            .Append("And    T.TransCodeId=? ")
            .Append("UNION ")
            .Append("Select ")
            .Append("       T.TransactionId, ")
            .Append("       (Select FirstName + ' ' + LastName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=T.StuEnrollId)), ")
            .Append("       ?, ")
            .Append("       1, ")
            .Append("       0.00, ")
            .Append("       Coalesce(dbo.GetDeferredRevenue(TransactionId, ?) - Coalesce((Select sum(Amount) from saDeferredRevenues Where TransactionId=T.TransactionId), 0),0) ToAccrue, ")
            .Append("       TransAmount, ")
            .Append("       0 IsPosted ")
            .Append("From	saTransactions T, saTransCodes TC ")
            .Append("Where ")
            .Append("       (T.TransCodeId = TC.TransCodeId) ")
            .Append("And    TC.DefEarnings = 1 ")
            .Append("AND    T.Voided=0 ")
            .Append("And    T.TransCodeId=? ")
            .Append("Order By ")
            .Append("       StudentName, DefRevenueDate ")
        End With

        ' Add TransCodeId to the parameter list
        db.AddParameter("@TransCodeId", transCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ' Add ReportDate to the parameter list
        db.AddParameter("@ReportDate", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add ReportDate to the parameter list
        db.AddParameter("@ReportDate1", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add TransCodeId to the parameter list
        db.AddParameter("@TransCodeId1", transCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function

    Public Function GetDeferredRevenueDetailsForTransCode(ByVal transCodeId As String, ByVal reportDate As Date, ByVal campusId As String, ByVal studentIdentifier As String, _
                                                          ByVal mode As Integer, ByVal user As String, ByVal transCutOffDate As String) As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("Select ")
            .Append("       DR.DefRevenueId, ")
            .Append("       (Select FirstName + ' ' + LastName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=(Select StuEnrollId from saTransactions where TransactionId=DR.TransactionId and saTransactions.Voided=0))) StudentName, ")

            'include SSN or student identifier
            If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
                .Append("       (Select Coalesce(Case Len(S.SSN) When 9 then '***-**-' + SUBSTRING(SSN,6,4) else ' ' end, ' ') ")
            ElseIf CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToLower = "studentid" Then
                .Append("       (Select Coalesce(StudentNumber, ' ') ")
            Else
                .Append("       (Select Coalesce(" + CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToString + ", ' ') ")
            End If
            .Append("       from arStudent S ")
            .Append("       where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=(Select StuEnrollId from saTransactions where TransactionId=T.TransactionId and saTransactions.Voided=0))) As StudentIdentifier, ")

            .Append("       DR.DefRevenueDate, ")
            .Append("       DR.Source, ")
            .Append("       DR.Amount Accrued, ")
            .Append("       0.00 ToAccrue, ")
            .Append("       0.00 Total, ")
            .Append("       DR.IsPosted ")
            .Append("From	saDeferredRevenues DR, saTransactions T, saTransCodes TC ")
            .Append("Where ")
            .Append("       (DR.TransactionId=T.TransactionId) ")
            .Append("And    (T.TransCodeId = TC.TransCodeId) ")
            .Append("And    TC.DefEarnings = 1 ")
            .Append("and    T.Voided=0 ")
            .Append("And    T.TransCodeId=? ")
            .Append("Order By ")
            .Append("       StudentName, DefRevenueDate ;")


        End With

        ' Add TransCodeId to the parameter list
        db.AddParameter("@TransCodeId", transCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   get datatable
        Dim newDT As DataTable = db.RunParamSQLDataSet(sb.ToString).Tables(0)

        '   build DeferredRevenues dataset and select the Transactions table
        Dim dt As New DataTable
        If transCutOffDate = "" Then
            dt = GetDeferredRevenueResults(reportDate, campusId, studentIdentifier, mode, user).Tables("DeferredRevenueResultsTable")
        Else
            dt = GetDeferredRevenueResults(reportDate, campusId, studentIdentifier, mode, user, transCutOffDate).Tables("DeferredRevenueResultsTable")
        End If



        '   select only the rows for the corresponding TransCodeId
        Dim rows() As DataRow = dt.Select("TransCodeId=" + "'" + transCodeId + "'")

        '   add those rows to the cloned table
        If rows.Length > 0 Then
            For i As Integer = 0 To rows.Length - 1
                Dim newRow As DataRow = newDT.NewRow()
                newRow("DefRevenueId") = rows(i)("TransactionId")
                newRow("StudentName") = rows(i)("StudentName")
                newRow("StudentIdentifier") = rows(i)("StudentIdentifier")
                newRow("DefRevenueDate") = reportDate
                newRow("Source") = 1
                newRow("Accrued") = 0.0
                If rows(i)("DeferredRevenue") Is System.DBNull.Value Then rows(i)("DeferredRevenue") = 0.0
                If rows(i)("Earned") Is System.DBNull.Value Then rows(i)("Earned") = 0.0
                If rows(i)("RemainingMethod") = 1 Then
                    newRow("ToAccrue") = rows(i)("DeferredRevenue")
                Else
                    If Not rows(i)("DeferredRevenue") = 0.0 Then
                        newRow("ToAccrue") = rows(i)("DeferredRevenue") - rows(i)("Earned")
                    Else
                        newRow("ToAccrue") = 0.0
                    End If

                End If


                newRow("Total") = rows(i)("TransAmount")
                newRow("IsPosted") = 0
                '   add row to the table
                newDT.Rows.Add(newRow)
            Next
        End If

        '   return the created table
        Return newDT


    End Function

    Public Function GetDeferredRevenueDetailsByTransCodeDBI(ByVal transCodeId As String, ByVal reportDate As Date, ByVal campusId As String, Optional ByVal TransDate As String = "") As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("Select ")
            .Append("       DR.DefRevenueId, ")
            .Append("       (Select FirstName + ' ' + LastName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=(Select StuEnrollId from saTransactions where TransactionId=DR.TransactionId and saTransactions.Voided=0))) StudentName, ")

            'include SSN or student identifier
            If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
                .Append("       (Select Coalesce(Case Len(S.SSN) When 9 then '***-**-' + SUBSTRING(SSN,6,4) else ' ' end, ' ') ")
            ElseIf CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToLower = "studentid" Then
                .Append("       (Select Coalesce(StudentNumber, ' ') ")
            Else
                .Append("       (Select Coalesce(" + CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToString + ", ' ') ")
            End If
            .Append("       from arStudent S ")
            .Append("       where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=(Select StuEnrollId from saTransactions where TransactionId=T.TransactionId and saTransactions.Voided=0))) As StudentIdentifier, ")

            .Append("       DR.DefRevenueDate, ")
            .Append("       DR.Source, ")
            .Append("       DR.Amount Accrued, ")
            .Append("       0.00 ToAccrue, ")
            .Append("       0.00 Total, ")
            .Append("       DR.IsPosted ")
            .Append("From	saDeferredRevenues DR, saTransactions T, saTransCodes TC ")
            .Append("Where ")
            .Append("       (DR.TransactionId=T.TransactionId) ")
            .Append("And    (T.TransCodeId = TC.TransCodeId) ")
            .Append("And    TC.DefEarnings = 1 ")
            .Append("and    T.Voided=0 ")
            .Append("And    T.TransCodeId=? ")
            .Append("Order By ")
            .Append("       StudentName, DefRevenueDate ;")


        End With

        ' Add TransCodeId to the parameter list
        db.AddParameter("@TransCodeId", transCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   get datatable
        Dim newDT As DataTable = db.RunParamSQLDataSet(sb.ToString).Tables(0)

        '   build DeferredRevenues dataset and select the Transactions table
        Dim dt As DataTable = BuildDatasetForDeferredRevenueCalculationsTraditionalAndRemainingMethod(reportDate, campusId, TransDate).Tables("Transactions")


        '   select only the rows for the corresponding TransCodeId
        Dim rows() As DataRow = dt.Select("TransCodeId=" + "'" + transCodeId + "'")

        '   add those rows to the cloned table
        If rows.Length > 0 Then
            For i As Integer = 0 To rows.Length - 1
                Dim newRow As DataRow = newDT.NewRow()
                newRow("DefRevenueId") = rows(i)("TransactionId")
                newRow("StudentName") = rows(i)("StudentName")
                newRow("StudentIdentifier") = rows(i)("StudentIdentifier")
                newRow("DefRevenueDate") = reportDate
                newRow("Source") = 1
                newRow("Accrued") = 0.0
                If rows(i)("DeferredRevenue") Is System.DBNull.Value Then rows(i)("DeferredRevenue") = 0.0
                If rows(i)("Earned") Is System.DBNull.Value Then rows(i)("Earned") = 0.0
                If rows(i)("RemainingMethod") = 1 Then
                    newRow("ToAccrue") = rows(i)("DeferredRevenue")
                Else
                    If Not rows(i)("DeferredRevenue") = 0.0 Then
                        newRow("ToAccrue") = rows(i)("DeferredRevenue") - rows(i)("Earned")
                    Else
                        newRow("ToAccrue") = 0.0
                    End If

                End If


                newRow("Total") = rows(i)("TransAmount")
                newRow("IsPosted") = 0
                '   add row to the table
                newDT.Rows.Add(newRow)
            Next
        End If

        '   return the created table
        Return newDT

    End Function
    Public Function GetDeferredRevenueDetailsByEnrollment(ByVal stuEnrollId As String, ByVal transCodeId As String) As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("Select ")
            .Append("       DR.DefRevenueId, ")
            .Append("       DR.DefRevenueDate, ")
            .Append("       DR.Source, ")
            .Append("       DR.Amount, ")
            .Append("       DR.IsPosted, ")
            .Append("       TC.TransCodeDescrip, ")
            .Append("       T.TransDescrip, ")
            .Append("       FL.Description as FeeLevel, ")
            .Append("       T.TransactionId, ")
            .Append("       0 as Earned, ")
            .Append("       0 AS Unearned ")
            .Append("From	saDeferredRevenues DR, saTransactions T, saTransCodes TC, saFeeLevels FL ")
            .Append("Where ")
            .Append("       (DR.TransactionId=T.TransactionId) ")
            .Append("And    (T.TransCodeId = TC.TransCodeId) ")
            .Append("And    TC.DefEarnings = 1 ")
            .Append("And    T.Voided=0 ")
            .Append("And    T.FeeLevelId=FL.FeeLevelId ")
            .Append("And    T.TransTypeId in(0,1) ")
            .Append("And    T.StuEnrollId=? ")
            .Append("And    T.TransCodeId=? ")
            .Append("Union ")
            .Append("Select ")
            .Append("       T.TransactionId, ")
            .Append("       T.TransDate, ")
            .Append("       0, ")
            .Append("       T.TransAmount, ")
            .Append("       T.IsPosted, ")
            .Append("       TC.TransCodeDescrip, ")
            .Append("       T.TransDescrip, ")
            .Append("       FL.Description as FeeLevel, ")
            .Append("       T.TransactionId, ")
            .Append("       ISNULL((SELECT SUM(Amount) FROM dbo.saDeferredRevenues WHERE TransactionId=T.TransactionId),0.0) AS Earned, ")
            .Append("       T.TransAmount - ISNULL((SELECT SUM(Amount) FROM dbo.saDeferredRevenues WHERE TransactionId=T.TransactionId),0.0) AS Unearned ")
            .Append("From   saTransactions T, saTransCodes TC, saFeeLevels FL ")
            .Append("Where ")
            .Append("       (T.TransCodeId = TC.TransCodeId) ")
            .Append("And    TC.DefEarnings = 1 ")
            .Append("And    T.Voided=0 ")
            .Append("And    T.FeeLevelId=FL.FeeLevelId ")
            .Append("And    T.TransTypeId in(0,1) ")
            .Append("And    T.StuEnrollId=? ")
            .Append("And    T.TransCodeId=? ")
            .Append("Order By ")
            .Append("       DefRevenueDate ")
        End With

        '   if StuEnrollId is empty replace it with empty.Guid
        If stuEnrollId = "" Then stuEnrollId = Guid.Empty.ToString

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ' Add TransCodeId to the parameter list
        db.AddParameter("@TransCodeId", transCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId1", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ' Add TransCodeId to the parameter list
        db.AddParameter("@TransCodeId1", transCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString, "RevenueLedgerDS")

    End Function
    Public Function GetDeferredRevenueTotalsByEnrollment(ByVal stuEnrollId As String) As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("Select ")
            .Append("       T.TransCodeId, ")
            .Append("       (Select TransCodeDescrip from saTransCodes where TransCodeId=T.TransCodeId) TransCodeDescrip, ")
            '.Append("       sum(T.TransAmount) - Coalesce((Select sum(Amount) from saDeferredRevenues where TransactionId in (Select TransactionId from saTransactions where TransCodeId=T.TransCodeId)),0) UnEarned, ")
            '.Append("       (Select sum(Amount) from saDeferredRevenues where TransactionId in (Select TransactionId from saTransactions where TransCodeId=T.TransCodeId)) Earned ")
            .Append("       Coalesce(sum(T.TransAmount) - Coalesce((Select sum(Amount) from saDeferredRevenues where TransactionId in (Select TransactionId from saTransactions where TransCodeId=T.TransCodeId and StuEnrollId=? and saTransactions.Voided=0)),0),0) UnEarned, ")
            .Append("       Coalesce((Select sum(Amount) from saDeferredRevenues where TransactionId in (Select TransactionId from saTransactions where TransCodeId=T.TransCodeId and StuEnrollId=? and saTransactions.Voided=0)),0) Earned,  ")
            .Append("       SUM(T.TransAmount) AS TransAmount ")
            .Append("From	saTransactions T, saTransCodes TC ")
            .Append("Where ")
            .Append("       (T.TransCodeId = TC.TransCodeId) ")
            .Append("And    TC.DefEarnings = 1 ")
            .Append("And    T.StuEnrollId=? ")
            .Append("And    T.Voided=0 ")
            .Append("And    T.TransTypeId IN(0,1) ")
            .Append("Group By ")
            .Append("       T.TransCodeId, TransCodeDescrip ")
        End With

        '   if StuEnrollId is empty replace it with empty.Guid
        If stuEnrollId = "" Then stuEnrollId = Guid.Empty.ToString

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId1", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId2", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function

    Public Function PostDeferredRevenues(ByVal reportDate As Date, ByVal user As String) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   Connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("Insert Into saDeferredRevenues ")
                .Append("Select ")
                .Append("       NEWID() DefRevenueId, ")
                .Append("       ? DefRevenueDate, ")
                .Append("       T.TransactionId TransactionId, ")
                .Append("       0 Type, ")
                .Append("       1 Source, ")
                .Append("       dbo.GetDeferredRevenue(TransactionId, ?) - Coalesce((Select sum(Amount) from saDeferredRevenues Where TransactionId=T.TransactionId), 0) Amount, ")
                .Append("       1 IsPosted, ")
                .Append("       ? ModUser, ")
                .Append("       ? ModDate ")
                .Append("From ")
                .Append("       saTransactions T, saTransCodes TC ")
                .Append("Where ")
                .Append("       (T.TransCodeId = TC.TransCodeId) ")
                .Append("And    TC.DefEarnings = 1 ")
                .Append("And    T.Voided=0 ")
                .Append("And    (dbo.GetDeferredRevenue(TransactionId, ?) - Coalesce((Select sum(Amount) from saDeferredRevenues Where TransactionId=T.TransactionId), 0) ) <> 0 ")
            End With

            '   add parameters values to the query

            '   ReportDate
            db.AddParameter("@ReportDate", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ReportDate
            db.AddParameter("@ReportDate1", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ReportDate
            db.AddParameter("@ReportDate2", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   delete all transactions with zero amount
            sb = New StringBuilder
            sb.Append("Delete from saDeferredRevenues Where Amount=0")
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function PostDeferredRevenuesDBI(ByVal reportDate As Date, ByVal user As String, ByVal campusId As String, Optional ByVal TransDAte As String = "") As DataSet
        'Troy: 5/29/2012 I modified this function to return a DataSet that the UI could use to bind to. The  BindDataList() function in the UI uses the same DataSet that is generated here
        'but it had to call BuildDatasetForDeferredRevenueCalculationsTraditionalAndRemainingMethod again and go through all the loops to generate the DataSet when it is already generated here.

        '   Connect to the database
        'Dim db As New DataAccess
        Dim dsToReturn As DataSet
        Dim dt As DataTable
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   get a dataset with an empty DeferredRevenues table
        Dim sb As New StringBuilder
        With sb
            .Append("Select ")
            .Append("       DefRevenueId, ")
            .Append("       DefRevenueDate, ")
            .Append("       TransactionId, ")
            .Append("       Type, ")
            .Append("       Source, ")
            .Append("       Amount, ")
            .Append("       IsPosted, ")
            .Append("       ModUser, ")
            .Append("       ModDate ")
            .Append("From	saDeferredRevenues ")
            .Append("Where ")
            .Append("       (1=2) ")
        End With

        '   get the empty dataset
        '   Troy: 5/29/2012 added the name of the datatable
        Dim ds As New DataSet("dsDeferredRevenues")
        ds = db.RunSQLDataSet(sb.ToString, "dtDeferredRevenues")

        ds.DataSetName = "dsDeferredRevenues"
        Dim newDT As DataTable = ds.Tables(0)

        '   build DeferredRevenues dataset and select the Transactions table
        ''MOdified by Saraswathi Lakshmanan on June 14 2010
        ''CAlling the new remaining method
        'Dim dt As DataTable = BuildDatasetForDeferredRevenueCalculations(reportDate, campusId).Tables("Transactions")
        'Dim dt As DataTable = BuildDatasetForDeferredRevenueCalculationsTraditionalAndRemainingMethod(reportDate, campusId, TransDAte).Tables("Transactions")
        dsToReturn = BuildDatasetForDeferredRevenueCalculationsTraditionalAndRemainingMethod(reportDate, campusId, TransDAte)
        dt = dsToReturn.Tables("Transactions")

        '   insert all new rows in the empty table
        Dim modDate As DateTime = Date.Now
        '   add those rows to the table
        Dim amount As Decimal
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim newRow As DataRow = newDT.NewRow()
            Dim oldRow As DataRow = dt.Rows(i)
            ''Added By Saraswathi lakshmanan on June 14 2010
            If dt.Rows(i)("RemainingMethod") = 1 Then
                amount = dt.Rows(i)("DeferredRevenue")
            Else
                If Not dt.Rows(i)("DeferredRevenue") = 0.0 Then
                    amount = dt.Rows(i)("DeferredRevenue") - dt.Rows(i)("Earned")
                Else
                    amount = 0.0
                End If

            End If


            'Try
            '    amount = oldRow("DeferredRevenue") - oldRow("Earned")
            'Catch ex As Exception
            '    amount = 0
            'End Try
            'If amount > 0 Then
            newRow("DefRevenueId") = Guid.NewGuid
            newRow("DefRevenueDate") = reportDate
            newRow("TransactionId") = oldRow("TransactionId")
            newRow("Type") = 0
            newRow("Source") = 1
            newRow("Amount") = amount
            newRow("IsPosted") = 1
            newRow("ModUser") = user
            newRow("ModDate") = modDate
            '   add row to the table only if amount is <> 0
            'If amount <> 0 Then newDT.Rows.Add(newRow)
            If Not (amount = 0 Or (amount > 0 And amount < 0.01) Or (amount < 0 And amount > -0.01)) Then newDT.Rows.Add(newRow)
            'End If
        Next

        '   all updates must be encapsulated as one transaction
        Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            'Troy: 5/29/2012 The following code was commented out as the performance from the DataAdapter.Update was not acceptable.
            'We are now trying to pass the DataSet as XML to a SP and have the SP use OPENXML to process the batch. This should avoid the penalty
            'of sending the records (or several batches) across the net work.
            ''   build sql query for the DeferredRevenues data adapter
            'sb = New StringBuilder
            'With sb
            '    .Append("Select ")
            '    .Append("       DefRevenueId, ")
            '    .Append("       DefRevenueDate, ")
            '    .Append("       TransactionId, ")
            '    .Append("       Type, ")
            '    .Append("       Source, ")
            '    .Append("       Amount, ")
            '    .Append("       IsPosted, ")
            '    .Append("       ModUser, ")
            '    .Append("       ModDate ")
            '    .Append("From	saDeferredRevenues ")
            'End With

            ''   build select command
            'Dim DeferredRevenuesSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            ''   Create adapter to handle DeferredRevenues table
            'Dim DeferredRevenuesDataAdapter As New OleDbDataAdapter(DeferredRevenuesSelectCommand)

            ''   build insert, update and delete commands for DeferredRevenues table
            'Dim cb As New OleDbCommandBuilder(DeferredRevenuesDataAdapter)

            ''   insert added rows in DeferredRevenues table
            'DeferredRevenuesDataAdapter.Update(ds.Tables(0).Select(Nothing, Nothing, DataViewRowState.Added))
            'Call the procedure to insert more than one record all at once
            For Each lcol As DataColumn In ds.Tables(0).Columns
                lcol.ColumnMapping = System.Data.MappingType.Attribute
            Next
            Dim strXML As String = ds.GetXml
            db.AddParameter("@StrXML", strXML, SqlDbType.VarChar, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("USP_PostDeferredRevenues", Nothing)

            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            'Return ""
            Return dsToReturn

        Catch ex As OleDb.OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            'Return DALExceptions.BuildErrorMessage(ex)
            Throw ex

        Finally

            '   close connection
            connection.Close()
        End Try

    End Function

    Public Function GetDeferredRevenueResults(ByVal reportDate As Date, ByVal campusId As String, ByVal studentIdentifierType As String, ByVal mode As Integer, ByVal user As String, Optional ByVal CutOffTransDate As Date = Nothing) As DataSet
        Dim ds As New DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.CommandTimeout = 1800
        Try
            db.OpenConnection()
            db.AddParameter("@ReportDate", reportDate, SqlDbType.Date, , ParameterDirection.Input)
            db.AddParameter("@CampusId", campusId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            db.AddParameter("@StudentIdentifierType", studentIdentifierType, SqlDbType.VarChar, 50, ParameterDirection.Input)
            db.AddParameter("@Mode", mode, SqlDbType.Int, , ParameterDirection.Input)
            db.AddParameter("@user", user, SqlDbType.VarChar, 50, ParameterDirection.Input)

            If (IsNothing(CutOffTransDate) Or CutOffTransDate = Date.MinValue Or CutOffTransDate = "#12:00:00 AM#") Then
                db.AddParameter("@CutOffTransDate", System.DBNull.Value, SqlDbType.Date, , ParameterDirection.Input)
            Else
                db.AddParameter("@CutOffTransDate", CutOffTransDate, SqlDbType.Date, , ParameterDirection.Input)
            End If


            ds = db.RunParamSQLDataSet_SP("dbo.USP_SA_GetDeferredRevenueResults", "DeferredRevenueResultsTable")
            Return ds
        Catch ex As System.Exception
            Throw ex
        Finally
            db.CloseConnection()
        End Try


        Return ds
    End Function


    ''Modified by saraswathi lakshmanan on May 19 2009
    ''The deferred revenue dates are fetched based on the campusID
    Public Function GetEndOfMonthDatesForPostingDeferredRevenueCampus(ByVal CampusId As String) As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '.Append("Select ")
            '.Append("       Min(DefRevenueDate) MinDate, ")
            '.Append("       Max(DefRevenueDate) MaxDate ")
            '.Append("From	saDeferredRevenues DR  ")

            .Append("Select ")
            .Append("       Min(DefRevenueDate) MinDate, ")
            .Append("       Max(DefRevenueDate) MaxDate ")
            .Append("From	saDeferredRevenues DR  where TransactionId in (Select TransactionId from saTransactions where CampusID ='" + CampusId + "' )")
        End With

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   get the min and max dates from the DB table
        Dim maxDate, minDate, currDate As Date
        While dr.Read()
            If Not (dr("MinDate") Is System.DBNull.Value) Then
                'minDate = dr("MaxDate")
                minDate = CType(dr("MaxDate"), Date)
            Else
                minDate = Date.Now.AddMonths(-7)
                maxDate = minDate
            End If
        End While

        If Not dr.IsClosed Then dr.Close()

        '   round dates to the end of the month
        minDate = ConvertDateToEndOfMonthDate(minDate)
        maxDate = ConvertDateToEndOfMonthDate(maxDate)
        currDate = minDate

        '   create twelve records starting with the minDate
        Dim table As New DataTable("DeferredRevenuePostingPeriods")
        table.Columns.Add("EndOfPeriodDate", GetType(System.String))
        ''Modified by Saraswathi lakshmanan on Sept 23 2009
        ''Do not add future dates to the deferred revenue posting page,
        ''Add the max date and the dates till the current month
        For i As Integer = 0 To 11
            If ConvertDateToEndOfMonthDate(currDate).ToShortDateString < System.DateTime.Now Then
                Dim r As DataRow = table.NewRow()

                currDate = currDate.AddMonths(1)
                r(0) = ConvertDateToEndOfMonthDate(currDate).ToShortDateString
                table.Rows.Add(r)
            Else
                Exit For
            End If
        Next

        '   return table
        If table.Rows.Count = 0 Then
            Dim r As DataRow = table.NewRow()
            r(0) = ConvertDateToEndOfMonthDate(minDate).ToShortDateString
            table.Rows.Add(r)
        End If

        Return table


    End Function

    ''Modified by saraswathi lakshmanan on May 19 2009
    ''The deferred revenue dates are fetched based on the campusID
    Public Function GetEndOfMonthDatesForPostingDeferredRevenue() As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("Select ")
            .Append("       Min(DefRevenueDate) MinDate, ")
            .Append("       Max(DefRevenueDate) MaxDate ")
            .Append("From	saDeferredRevenues DR  ")

        End With

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   get the min and max dates from the DB table
        Dim maxDate, minDate, currDate As Date
        While dr.Read()
            If Not (dr("MinDate") Is System.DBNull.Value) Then
                'minDate = dr("MaxDate")
                minDate = CType(dr("MaxDate"), Date)
            Else
                minDate = Date.Now.AddMonths(-7)
                maxDate = minDate
            End If
        End While

        If Not dr.IsClosed Then dr.Close()
        
        '   round dates to the end of the month
        minDate = ConvertDateToEndOfMonthDate(minDate)
        maxDate = ConvertDateToEndOfMonthDate(maxDate)
        currDate = minDate

        '   create twelve records starting with the minDate
        Dim table As New DataTable("DeferredRevenuePostingPeriods")
        table.Columns.Add("EndOfPeriodDate", GetType(System.String))
        For i As Integer = 0 To 11
            Dim r As DataRow = table.NewRow()
            r(0) = ConvertDateToEndOfMonthDate(currDate).ToShortDateString
            table.Rows.Add(r)
            currDate = currDate.AddMonths(1)
        Next

        '   return table
        Return table

    End Function


    Private Function ConvertDateToEndOfMonthDate(ByVal dt As Date) As Date
        Return dt.AddDays(Date.DaysInMonth(dt.Year, dt.Month) - dt.Day)
    End Function
    Private Function BuildDatasetForDeferredRevenueCalculations(ByVal reportDate As Date, ByVal campusId As String) As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   Instantiate SelectCommands class
        Dim scs As New SelectCommands(reportDate, campusId, MyAdvAppSettings.AppSettings("ConString"))

        '   create instance of the dataset
        Dim ds As New DataSet

        '   fill Transactions table
        Dim da As New OleDbDataAdapter(scs.Transactions)
        da.Fill(ds, "Transactions")

        '   fill TransCodes table
        da.SelectCommand = scs.TransCodes
        da.Fill(ds, "TransCodes")

        '   fill StuEnrollments table
        da.SelectCommand = scs.StuEnrollments
        da.Fill(ds, "StuEnrollments")

        '   fill PrgVersions table
        da.SelectCommand = scs.PrgVersions
        da.Fill(ds, "PrgVersions")

        '   fill TuitionEarnings table
        da.SelectCommand = scs.TuitionEarnings
        da.Fill(ds, "TuitionEarnings")

        '   fill TuitionEarningsPercentageRanges table
        da.SelectCommand = scs.TuitionEarningsPercentageRanges
        da.Fill(ds, "TuitionEarningsPercentageRanges")

        '   fill Results table
        da.SelectCommand = scs.Results
        da.Fill(ds, "Results")

        '   fill Reqs table
        da.SelectCommand = scs.Reqs
        da.Fill(ds, "Reqs")

        '   set primary key for Transactions table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("Transactions").Columns("TransactionId")
        ds.Tables("Transactions").PrimaryKey = pk0

        '   set primary key for TransCodes table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("TransCodes").Columns("TransCodeId")
        ds.Tables("TransCodes").PrimaryKey = pk1

        '   set primary key for StuEnrollments table
        Dim pk2(0) As DataColumn
        pk2(0) = ds.Tables("StuEnrollments").Columns("StuEnrollId")
        ds.Tables("StuEnrollments").PrimaryKey = pk2

        '   set primary key for PrgVersions table
        Dim pk3(0) As DataColumn
        pk3(0) = ds.Tables("PrgVersions").Columns("PrgVerId")
        ds.Tables("PrgVersions").PrimaryKey = pk3

        '   set primary key for TuitionEarnings table
        Dim pk4(0) As DataColumn
        pk4(0) = ds.Tables("TuitionEarnings").Columns("TuitionEarningId")
        ds.Tables("TuitionEarnings").PrimaryKey = pk4

        '   set primary key for TuitionEarningsPercentageRanges table
        Dim pk5(0) As DataColumn
        pk5(0) = ds.Tables("TuitionEarningsPercentageRanges").Columns("TuitionEarningsPercentageRangeId")
        ds.Tables("TuitionEarningsPercentageRanges").PrimaryKey = pk5

        '   set primary key for Results table
        Dim pk6(0) As DataColumn
        pk6(0) = ds.Tables("Results").Columns("ResultId")
        ds.Tables("Results").PrimaryKey = pk6

        '   set primary key for Reqs table
        Dim pk7(0) As DataColumn
        pk7(0) = ds.Tables("Reqs").Columns("ReqId")
        ds.Tables("Reqs").PrimaryKey = pk7

        '   set foreign key column for TransCodeId in Transactions table
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("Transactions").Columns("TransCodeId")

        '   set foreign key column for StuEnrollId in Transactions table
        Dim fk1(0) As DataColumn
        fk1(0) = ds.Tables("Transactions").Columns("StuEnrollId")

        '   set foreign key column for PrgVerId in StuEnrollments table
        Dim fk2(0) As DataColumn
        fk2(0) = ds.Tables("StuEnrollments").Columns("PrgVerId")

        '   set foreign key column for TuitionEarningId in PrgVersions table
        Dim fk3(0) As DataColumn
        fk3(0) = ds.Tables("PrgVersions").Columns("TuitionEarningId")

        '   set foreign key column for TuitionEarningId in TuitionEarningsPercentageRanges table
        Dim fk4(0) As DataColumn
        fk4(0) = ds.Tables("TuitionEarningsPercentageRanges").Columns("TuitionEarningId")

        '   set foreign key column for StuEnrollId in Results table
        Dim fk5(0) As DataColumn
        fk5(0) = ds.Tables("Results").Columns("StuEnrollId")

        '   set foreign key column for PrgVerId in Reqs table
        Dim fk6(0) As DataColumn
        fk6(0) = ds.Tables("Reqs").Columns("PrgVerId")

        '   add relationship TransCodesTransactions
        ds.Relations.Add("TransCodesTransactions", pk1, fk0)

        '   add relationship StuEnrollmentsTransactions
        ds.Relations.Add("StuEnrollmentsTransactions", pk2, fk1)

        '   add relationship PrgVersionsStuEnrollments
        ds.Relations.Add("PrgVersionsStuEnrollments", pk3, fk2)

        '   add relationship TuitionEarningsPrgVersions
        ds.Relations.Add("TuitionEarningsPrgVersions", pk4, fk3)

        '   add relationship TuitionEarningsTuitionEarningsPercentageRanges
        ds.Relations.Add("TuitionEarningsTuitionEarningsPercentageRanges", pk4, fk4)

        '   add relationship StuEnrollmentsResults
        ds.Relations.Add("StuEnrollmentsResults", pk2, fk5)

        '   add relationship PrgVersionsReqs
        ds.Relations.Add("PrgVersionsReqs", pk3, fk6)

        '   add the column that will hold the DeferredRevenue for each transaction 
        Dim transactionsTable As DataTable = ds.Tables("Transactions")
        transactionsTable.Columns.Add("DeferredRevenue", GetType(System.Decimal))

        '   add the column that will hold the TotalDeferredRevenue for each type of transaction
        Dim transCodesTable As DataTable = ds.Tables("TransCodes")
        transCodesTable.Columns.Add("TotalDeferredRevenue", GetType(System.Decimal))

        '   add the column that will hold the AccruedAmount for each type of transaction
        transCodesTable.Columns.Add("AccruedAmount", GetType(System.Decimal))

        '   add the column that will hold the DeferredRevenueAmount for each type of transaction
        transCodesTable.Columns.Add("DeferredRevenueAmount", GetType(System.Decimal), "TotalDeferredRevenue - AccruedAmount")

        ' ''Added by saraswathi on june 26 2009
        Dim dtStudentException As New DataTable("StudentExceptions")
        dtStudentException.Columns.Add("StudentName", Type.GetType("System.String"))
        dtStudentException.Columns.Add("StuEnrollId", Type.GetType("System.String"))
        dtStudentException.Columns.Add("FeeLevelId", Type.GetType("System.Int32"))
        dtStudentException.Columns.Add("TermStart", Type.GetType("System.DateTime"))
        dtStudentException.Columns.Add("TermEnd", Type.GetType("System.DateTime"))
        dtStudentException.Columns.Add("TransCode", Type.GetType("System.String"))
        dtStudentException.Columns.Add("TransAmount", Type.GetType("System.String"))
        dtStudentException.Columns.Add("Exception", Type.GetType("System.String"))
        ds.Tables.Add(dtStudentException)


        '   loop throghout the Transactions table in order to update the "DeferredRevenue" column
        For i As Integer = 0 To transactionsTable.Rows.Count - 1
            For l As Integer = 0 To 0

                Dim Exception As String = ""
                Dim ExceptionNo As Integer = 0

                Dim r As DataRow = transactionsTable.Rows(i)
                r("DeferredRevenue") = 0.0
                Dim str As String = CType(r("StuEnrollId"), Guid).ToString


                ''Get the status of the student, if the student is of nostart status and it was changed from currentlya attending
                ''Then get the date they modified the status
                ''Post deferred revenue for the student for the remaining amount in the next month and then donot post deferred revenue at all, once they have earned all the revenue
                ''Modified on july 27 2009
                Dim nostartdate As Date = Date.MinValue
                If Not r.GetParentRow("StuEnrollmentsTransactions")("SysStatusId") Is DBNull.Value Then
                    If r.GetParentRow("StuEnrollmentsTransactions")("SysStatusId") = 8 Then
                        nostartdate = GetStudentNoStartStatusDate(str)
                    End If
                End If



                '   this is the calculated duration in months of the term
                'Dim termDuration As Decimal = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments")("Weeks") * 12.0 / 52.0
                Dim termStart As Date = Date.MinValue
                Dim termEnd As Date = Date.MinValue

                'calculate last date of attendance
                Dim lda As Date = GetLDA(r)
                If lda = Date.MinValue Then
                    ''Exception date determined is null for this student
                    ExceptionNo = 3
                    Exception = "The student has a drop reason and no date determined. "
                    Dim dr As DataRow
                    dr = dtStudentException.NewRow
                    dr("StudentName") = r("StudentName")
                    dr("StuEnrollId") = str
                    dr("FeeLevelId") = r("FeeLevelId")
                    dr("TermStart") = r("TermStart")
                    dr("TermEnd") = r("TermEnd")
                    Dim rtc1 As DataRow = r.GetParentRow("TransCodesTransactions")
                    dr("TransCode") = rtc1("TransCodeDescrip")
                    dr("TransAmount") = FormatCurrency(r("TransAmount"), , , TriState.True, TriState.True)

                    dr("Exception") = Exception
                    dtStudentException.Rows.Add(dr)
                    Exit For
                End If
                If lda > reportDate Then lda = Date.MaxValue

                Dim feeLevelId As Integer = 0
                If Not r.IsNull("FeeLevelId") Then feeLevelId = r("FeeLevelId")
                Select Case feeLevelId
                    ''Modified by Saraswathi On June 03 2009
                    ''1- Term and 3- Course 2- Program level
                    ''Course added to function as term.
                    Case 1, 3
                        termStart = r("TermStart")
                        termEnd = MinDate(r("TermEnd"), lda)
                        If nostartdate <> Date.MinValue Then
                            termEnd = MinDate(termEnd, nostartdate)
                        End If
                        ''Case 2 is program version
                        ''Modified by Saraswathi On June 3 2009
                    Case 2
                        '  Case 0, 2
                        termStart = CType(r.GetParentRow("StuEnrollmentsTransactions")("StartDate"), Date)
                        If r.GetParentRow("StuEnrollmentsTransactions")("ExpGradDate") Is System.DBNull.Value Then
                            Dim programDurationInWeeks As Integer = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments")("Weeks")
                            termEnd = MinDate(termStart.AddDays(programDurationInWeeks * 7), lda)
                        Else
                            termEnd = MinDate(r.GetParentRow("StuEnrollmentsTransactions")("ExpGradDate"), lda)
                        End If
                        If nostartdate <> Date.MinValue Then
                            termEnd = MinDate(termEnd, nostartdate)
                        End If

                    Case Else
                        ExceptionNo = 1
                        Exception = "There is no Fee Level Id defined for the student"
                        Dim dr As DataRow
                        dr = dtStudentException.NewRow
                        dr("StudentName") = r("StudentName")
                        dr("StuEnrollId") = str
                        dr("FeeLevelId") = r("FeeLevelId")
                        dr("TermStart") = r("TermStart")
                        dr("TermEnd") = r("TermEnd")
                        Dim rtc2 As DataRow = r.GetParentRow("TransCodesTransactions")
                        dr("TransCode") = rtc2("TransCodeDescrip")

                        dr("TransAmount") = FormatCurrency(r("TransAmount"), , , TriState.True, TriState.True)
                        dr("Exception") = Exception
                        dtStudentException.Rows.Add(dr)
                        Exit For
                End Select
                Dim termDuration As Decimal = 0

                If termStart.Date = termEnd.Date Then
                    ExceptionNo = 2
                    Exception = "Term Start Date and Term End Date/Date Determined/LDA are the same"
                    Dim dr As DataRow
                    dr = dtStudentException.NewRow
                    dr("StudentName") = r("StudentName")
                    dr("StuEnrollId") = str
                    dr("FeeLevelId") = feeLevelId
                    dr("TermStart") = termStart
                    dr("TermEnd") = termEnd
                    Dim rtc3 As DataRow = r.GetParentRow("TransCodesTransactions")
                    dr("TransCode") = rtc3("TransCodeDescrip")

                    dr("TransAmount") = FormatCurrency(r("TransAmount"), , , TriState.True, TriState.True)
                    dr("Exception") = Exception
                    dtStudentException.Rows.Add(dr)
                    Exit For
                End If
                ''Added by Saraswathi lakshmanan On July 1 2009

                '   check whether to apply a straight percentage or a conversion table
                Dim DeferredRevenuePostingBasedonHolidays As String
                If Not r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments").GetParentRow("TuitionEarningsPrgVersions")("TakeHolidays") Is DBNull.Value Then
                    If r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments").GetParentRow("TuitionEarningsPrgVersions")("TakeHolidays").ToString = "True" Then
                        DeferredRevenuePostingBasedonHolidays = "yes"
                    Else
                        DeferredRevenuePostingBasedonHolidays = "no"
                    End If
                Else
                    DeferredRevenuePostingBasedonHolidays = "no"
                End If

                '   check whether to apply a straight percentage or a conversion table
                Dim percentToEarnIdx As Integer = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments").GetParentRow("TuitionEarningsPrgVersions")("PercentToEarnIdx")
                ''Added by Saraswathi lakshmanan on July 1 2009
                ''If percentToEarnIdx=0 -- Percentage baseed on Calendar days in a month
                ''if percentToEarnIdx=1 -- Equal percentage 
                ''If percentToEarnIdx=2 -- Specify percentage


                If termStart <> Date.MinValue And termEnd <> Date.MinValue Then
                    If percentToEarnIdx = 1 Then
                        ''If it is a equal percentage every month  Then there is no holidays into considereation
                        ''termDuration = (termEnd.Subtract(termStart).TotalDays) / 30
                        termDuration = DateDiff(DateInterval.Month, termStart, termEnd) + 1
                        DeferredRevenuePostingBasedonHolidays = "no"
                    Else
                        termDuration = (termEnd.Subtract(termStart).TotalDays) / 30
                    End If
                End If





                '   ignore all calculations if the termDuration is zero or negative
                ''If percentToEarnIdx = 0 And Not (termDuration > 0) Then Exit For
                ''Modified by Saraswathi on July 1 2009
                If (percentToEarnIdx = 0 Or percentToEarnIdx = 1) And Not (termDuration > 0) Then Exit For

                '   this is the start date of this students term
                ''''modified by saraswathi Lakshmanan
                ''April 3 rd 2009,  mantis Case  15752
                ''elapsed time is based on the student term start date and report Date
                ''so, modifying the student start date to term start date

                'Dim startDate As Date = r.GetParentRow("StuEnrollmentsTransactions")("StartDate")

                Dim startDate As Date = termStart
                '' 

                '   this is the elapsed time in months from the beginning of the period to the report date
                Dim elapsedTime As Decimal = 0
                Dim LoaStartDate As Date = Date.MinValue
                Dim LOAEndDate As Date = Date.MinValue

                If Not r("LOAStartDate") Is System.DBNull.Value Then
                    LoaStartDate = r("LOAStartDate")
                End If
                If Not r("LOAEndDate") Is System.DBNull.Value Then
                    LOAEndDate = r("LOAEndDate")
                End If

                elapsedTime = CalculateElaspedTime(LoaStartDate, LOAEndDate, startDate, reportDate, termStart, campusId, DeferredRevenuePostingBasedonHolidays)

                '   elapsed time can not be negative
                If elapsedTime < 0 Then elapsedTime = 0.0

                '   if the student started before this day of the month .. round elapsed time to a half month
                Dim roundedElapsedTime As Decimal = elapsedTime

                '   round elapsed time only if the elapsed time is > 0
                If elapsedTime > 0 Then
                    ''Modified by saraswathi lakshmanan
                    ''Only the first month, the deferred revenue is calculated based on full month before day or half month before day(based on rounded elasped time). otherwise, it is calculated   based on the elasped time 
                    ''Added on July 1st 2009
                    ''based on the webConfig Entry the Equal Percentage is calculated everymonth

                    If percentToEarnIdx = 1 Then
                        '' roundedElapsedTime = elapsedTime.Truncate(elapsedTime + 0.99999)
                        ''Modified by saraswathi lakshmanan on may 04 2009
                        ''Since the rounded elasped time is not giving the correct number of months, when rounded.
                        ''Eg: When the student start date is 8/4/2008 and when the deferedrevenue is run for january, for program level fees.
                        ''It is expecting the student to pay 2 months revenue. i.e for january and for february.
                        ''Since the rounded elasped time is 7. But it is supposed to be only 6.
                        ''The calculation is because we are truncating the elasped time.
                        ''And the new formula derived is Round the elasped time 
                        ' roundedElapsedTime = elapsedTime.Round(elapsedTime)
                        ''new frm 
                        roundedElapsedTime = DateDiff(DateInterval.Month, startDate, reportDate) + 1

                    Else
                        If reportDate.Month = startDate.Month And reportDate.Year = startDate.Year Then

                            Dim halfMonthBeforeDay As Integer = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments").GetParentRow("TuitionEarningsPrgVersions")("HalfMonthBeforeDay")
                            If startDate.Day <= halfMonthBeforeDay And halfMonthBeforeDay > 0 Then
                                ''roundedElapsedTime = (elapsedTime.Truncate(elapsedTime) * 2 + 1.0) / 2.0
                                ''Modified by saraswathi lakshmanan on may 04 2009
                                ''Since the rounded elasped time is not giving the correct number of months, when rounded.
                                ''Eg: When the student start date is 8/4/2008 and when the deferedrevenue is run for january, for program level fees.
                                ''It is expecting the student to pay 2 months revenue. i.e for january and for february.
                                ''Since the rounded elasped time is 7. But it is supposed to be only 6.
                                ''The calculation is because we are truncating the elasped time.
                                ''this was found for full month.
                                ''For half month: For december the Rounded elasped time  is : 5.5, but it is supposed to be 4.5
                                ''So the new formula derived is Round the number ans subtract by 0.5
                                'roundedElapsedTime = (elapsedTime.Round(elapsedTime) - 0.5)
                                'If roundedElapsedTime < 0 Then
                                '    roundedElapsedTime = 0
                                'End If

                                '' or first time the rounded elasped time shuld be 0.5
                                roundedElapsedTime = 0.5

                            End If


                            '   if the student started before this day of the month .. round elapsed time to a whole month
                            Dim fullMonthBeforeDay As Integer = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments").GetParentRow("TuitionEarningsPrgVersions")("FullMonthBeforeDay")
                            If startDate.Day <= fullMonthBeforeDay And fullMonthBeforeDay > 0 Then
                                '' roundedElapsedTime = elapsedTime.Truncate(elapsedTime + 0.99999)
                                ''Modified by saraswathi lakshmanan on may 04 2009
                                ''Since the rounded elasped time is not giving the correct number of months, when rounded.
                                ''Eg: When the student start date is 8/4/2008 and when the deferedrevenue is run for january, for program level fees.
                                ''It is expecting the student to pay 2 months revenue. i.e for january and for february.
                                ''Since the rounded elasped time is 7. But it is supposed to be only 6.
                                ''The calculation is because we are truncating the elasped time.
                                ''And the new formula derived is Round the elasped time 
                                ' roundedElapsedTime = elapsedTime.Round(elapsedTime)

                                '' or first time the rounded elasped time shuld be 0.5
                                roundedElapsedTime = 1

                            End If
                        End If
                    End If
                End If

                '   check that rounded elapsed time does not exceed term duration
                If roundedElapsedTime > termDuration Then roundedElapsedTime = termDuration

                If (percentToEarnIdx = 0 Or percentToEarnIdx = 1) Then
                    If termDuration <> 0 Then
                        '   apply straight line percentage
                        r("DeferredRevenue") = r("TransAmount") * roundedElapsedTime / termDuration
                    Else
                        ''Exception
                        ''CAse 1: feelevelid is not set, 
                        'Case 2:term duration =0 ,  term Start and term End is the same or term start and LDA or Datedetermined is the same..
                        r("DeferredRevenue") = 0
                        r("Earned") = 0

                    End If

                ElseIf (percentToEarnIdx = 2) Then
                    '   apply table. 
                    Dim PercentageRangeBasisIdx As Integer = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments").GetParentRow("TuitionEarningsPrgVersions")("PercentageRangeBasisIdx")
                    Dim percentArgument As Integer

                    Select Case PercentageRangeBasisIdx
                        Case 0
                            '   Program Length
                            ''Modified by Saraswathi lakshmanan
                            ''When the term duration is zero the percent argument is considered to be zero . Since it is giving divide by zero error
                            percentArgument = roundedElapsedTime / termDuration * 100.0
                        Case 1
                            '   Credits Earned
                            '   add up Credits Earned
                            Dim resultRows() As DataRow = r.GetParentRow("StuEnrollmentsTransactions").GetChildRows("StuEnrollmentsResults")
                            Dim creditsEarned As Integer = 0
                            For Each resultRow As DataRow In resultRows
                                ''modified by Saraswathi lakshmanan on June 4 2009
                                ''Conversion error from dbnull to boolean, since, the student was not graded.
                                If Not resultRow("IsPass") Is DBNull.Value Then
                                    If CType(resultRow("IsPass"), Boolean) Then
                                        creditsEarned += CType(resultRow("Credits"), Integer)
                                    End If
                                End If
                            Next

                            '   this is the total number of credits for the program
                            Dim totalCredits As Integer = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments")("Credits")

                            '   Credits Earned
                            If totalCredits > 0 Then percentArgument = creditsEarned * 100.0 / totalCredits Else percentArgument = 0.0
                        Case 2
                            '   Credits Attempted
                            '   add up Credits Attempted
                            Dim resultRows() As DataRow = r.GetParentRow("StuEnrollmentsTransactions").GetChildRows("StuEnrollmentsResults")
                            Dim creditsAttempted As Integer = 0
                            For Each resultRow As DataRow In resultRows
                                ''modified by Saraswathi lakshmanan on June 4 2009
                                If Not resultRow("IsCreditsAttempted") Is DBNull.Value Then
                                    If CType(resultRow("IsCreditsAttempted"), Boolean) Then
                                        creditsAttempted += CType(resultRow("Credits"), Integer)
                                    End If
                                End If

                            Next

                            '   this is the total number of credits for the program
                            Dim totalCredits As Integer = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments")("Credits")

                            '   Credits Attempted
                            If totalCredits > 0 Then percentArgument = creditsAttempted * 100.0 / totalCredits Else percentArgument = 0.0
                        Case 3
                            '   Courses completed
                            '   get the courses for the program version
                            Dim courses As DataRow() = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments").GetChildRows("PrgVersionsReqs")
                            Dim numberOfCourses As Integer = courses.Length
                            ''calculate number of required courses
                            'Dim numberOfCourses As Integer = 0
                            'For j As Integer = 0 To courses.Length - 1
                            '    If CType(courses(j)("IsRequired"), Boolean) Then
                            '        numberOfCourses += 1
                            '    End If
                            'Next

                            'calculate number of courses taken
                            Dim numberOfCoursesTaken As Integer = 0
                            Dim resultRows() As DataRow = r.GetParentRow("StuEnrollmentsTransactions").GetChildRows("StuEnrollmentsResults")
                            For Each resultRow As DataRow In resultRows
                                ''modified by Saraswathi lakshmanan on June 4 2009
                                If Not resultRow("IsPass") Is DBNull.Value Then
                                    If CType(resultRow("IsPass"), Boolean) Then
                                        numberOfCoursesTaken += 1
                                    End If
                                End If
                            Next

                            'Percentage of courses completed
                            If numberOfCourses > 0 Then percentArgument = numberOfCoursesTaken * 100.0 / numberOfCourses Else percentArgument = 0.0
                        Case 4
                            'get number of scheduled hours
                            Dim hoursScheduled As Decimal = 0.0
                            Dim resultRows() As DataRow = r.GetParentRow("StuEnrollmentsTransactions").GetChildRows("StuEnrollmentsResults")
                            'iterate over all Class Sections
                            For Each resultRow As DataRow In resultRows
                                Dim csm As List(Of AdvantageClassSectionMeeting)
                                If CType(MyAdvAppSettings.AppSettings("TrackSapAttendance"), String).ToLower() = "byclass" Then
                                    csm = (New PeriodsDB).GetCollectionOfMeetingDates(CType(resultRow("ClsSectionId"), Guid).ToString())
                                Else
                                    csm = (New PeriodsDB).GetCollectionOfMeetingDates(r("StuEnrollId"))
                                End If
                                hoursScheduled += GetHoursScheduled(csm, reportDate)
                            Next
                            'get the total duration of the program in hours
                            Dim totalProgramHours As Decimal = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments")("Hours")

                            'Percentage of scheduled hours
                            If totalProgramHours > 0 Then percentArgument = hoursScheduled * 100.0 / totalProgramHours Else percentArgument = 0.0

                        Case 5
                            'get number of completed hours
                            Dim hoursScheduled As Decimal = 0.0
                            Dim hoursCompleted As Decimal = 0.0
                            Dim resultRows() As DataRow = r.GetParentRow("StuEnrollmentsTransactions").GetChildRows("StuEnrollmentsResults")
                            For Each resultRow As DataRow In resultRows
                                Dim csma As List(Of AdvantageClassSectionMeetingAttendance)
                                Dim csm As List(Of AdvantageClassSectionMeeting)
                                If CType(MyAdvAppSettings.AppSettings("TrackSapAttendance"), String).ToLower() = "byclass" Then
                                    csma = (New AttendanceDB).GetCollectionOfMeetingDatesAttendance(CType(resultRow("ClsSectionId"), Guid).ToString(), CType(r("StuEnrollId"), Guid).ToString())
                                    csm = (New PeriodsDB).GetCollectionOfMeetingDates(CType(resultRow("ClsSectionId"), Guid).ToString())
                                Else
                                    csma = (New AttendanceDB).GetCollectionOfMeetingDatesAttendance(CType(r("StuEnrollId"), Guid).ToString())
                                    csm = (New PeriodsDB).GetCollectionOfMeetingDates(r("StuEnrollId"))
                                End If
                                hoursScheduled += GetHoursScheduled(csm, reportDate)
                                hoursCompleted += GetHoursCompleted(csm, csma, reportDate)
                            Next

                            'get the total duration of the program in hours
                            Dim totalProgramHours As Decimal = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments")("Hours")

                            'Percentage of scheduled hours
                            If totalProgramHours > 0 Then percentArgument = hoursCompleted * 100.0 / totalProgramHours Else percentArgument = 0.0

                    End Select

                    '   percent argument should never be grater that 100
                    If percentArgument > 100.0 Then percentArgument = 100.0

                    '   loop throughout TuitionEarningsPercentageRanges table
                    Dim percentageRangesTable As DataTable = ds.Tables("TuitionEarningsPercentageRanges")

                    '   get all rows from the PercentageRanges table
                    Dim rows() As DataRow = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments").GetParentRow("TuitionEarningsPrgVersions").GetChildRows("TuitionEarningsTuitionEarningsPercentageRanges")
                    Dim percentResult As Integer = 0
                    If percentArgument > 0 Then
                        For j As Integer = 0 To rows.Length - 1
                            If CType(rows(j)("UpTo"), Double) >= percentArgument Then
                                percentResult = CType(rows(j)("EarnPercent"), Double)
                                Exit For
                            End If
                        Next
                    End If
                    '   apply percentage
                    r("DeferredRevenue") = (r("TransAmount") * percentResult) / 100.0
                End If

                '   add this value to the totalDeferredRevenue in the transCodes table
                Dim rtc As DataRow = r.GetParentRow("TransCodesTransactions")
                If Not rtc("TotalDeferredRevenue") Is System.DBNull.Value Then
                    rtc("TotalDeferredRevenue") += r("DeferredRevenue")
                Else
                    rtc("TotalDeferredRevenue") = r("DeferredRevenue")
                End If

                '   add Earned Amount from each transaction to AccruedAmount in the transCodes table
                If Not rtc("AccruedAmount") Is System.DBNull.Value Then
                    rtc("AccruedAmount") += r("Earned")
                Else
                    rtc("AccruedAmount") = r("Earned")
                End If
            Next
        Next

        '   return dataset
        Return ds

    End Function
    ''Modified by Saraswathi on May 18n 2010
    ''Deferred revenue Calculation method is added
    ''18962: Deferred Revenue Posting issue:Negative Tuition Amounts 
    ''Optional Paramteter Added by Saraswathi Lakshmanan on June 30 2010
    Private Function BuildDatasetForDeferredRevenueCalculationsTraditionalAndRemainingMethod(ByVal reportDate As Date, ByVal campusId As String, Optional ByVal TransDate As String = "") As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   Instantiate SelectCommands class
        Dim scs As New SelectCommands(reportDate, campusId, MyAdvAppSettings.AppSettings("ConString"), TransDate)

        '   create instance of the dataset
        Dim ds As New DataSet

        '   fill Transactions table
        Dim da As New OleDbDataAdapter(scs.Transactions)
        da.Fill(ds, "Transactions")

        '   fill TransCodes table
        da.SelectCommand = scs.TransCodes
        da.Fill(ds, "TransCodes")

        '   fill StuEnrollments table
        da.SelectCommand = scs.StuEnrollments
        da.Fill(ds, "StuEnrollments")

        '   fill PrgVersions table
        da.SelectCommand = scs.PrgVersions
        da.Fill(ds, "PrgVersions")

        '   fill TuitionEarnings table
        da.SelectCommand = scs.TuitionEarnings
        da.Fill(ds, "TuitionEarnings")

        '   fill TuitionEarningsPercentageRanges table
        da.SelectCommand = scs.TuitionEarningsPercentageRanges
        da.Fill(ds, "TuitionEarningsPercentageRanges")

        '   fill Results table
        da.SelectCommand = scs.Results
        da.Fill(ds, "Results")

        '   fill Reqs table
        da.SelectCommand = scs.Reqs
        da.Fill(ds, "Reqs")

        '   set primary key for Transactions table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("Transactions").Columns("TransactionId")
        ds.Tables("Transactions").PrimaryKey = pk0

        '   set primary key for TransCodes table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("TransCodes").Columns("TransCodeId")
        ds.Tables("TransCodes").PrimaryKey = pk1

        '   set primary key for StuEnrollments table
        Dim pk2(0) As DataColumn
        pk2(0) = ds.Tables("StuEnrollments").Columns("StuEnrollId")
        ds.Tables("StuEnrollments").PrimaryKey = pk2

        '   set primary key for PrgVersions table
        Dim pk3(0) As DataColumn
        pk3(0) = ds.Tables("PrgVersions").Columns("PrgVerId")
        ds.Tables("PrgVersions").PrimaryKey = pk3

        '   set primary key for TuitionEarnings table
        Dim pk4(0) As DataColumn
        pk4(0) = ds.Tables("TuitionEarnings").Columns("TuitionEarningId")
        ds.Tables("TuitionEarnings").PrimaryKey = pk4

        '   set primary key for TuitionEarningsPercentageRanges table
        Dim pk5(0) As DataColumn
        pk5(0) = ds.Tables("TuitionEarningsPercentageRanges").Columns("TuitionEarningsPercentageRangeId")
        ds.Tables("TuitionEarningsPercentageRanges").PrimaryKey = pk5

        '   set primary key for Results table
        Dim pk6(0) As DataColumn
        pk6(0) = ds.Tables("Results").Columns("ResultId")
        ds.Tables("Results").PrimaryKey = pk6

        '   set primary key for Reqs table
        Dim pk7(0) As DataColumn
        pk7(0) = ds.Tables("Reqs").Columns("ReqId")
        ds.Tables("Reqs").PrimaryKey = pk7

        '   set foreign key column for TransCodeId in Transactions table
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("Transactions").Columns("TransCodeId")

        '   set foreign key column for StuEnrollId in Transactions table
        Dim fk1(0) As DataColumn
        fk1(0) = ds.Tables("Transactions").Columns("StuEnrollId")

        '   set foreign key column for PrgVerId in StuEnrollments table
        Dim fk2(0) As DataColumn
        fk2(0) = ds.Tables("StuEnrollments").Columns("PrgVerId")

        '   set foreign key column for TuitionEarningId in PrgVersions table
        Dim fk3(0) As DataColumn
        fk3(0) = ds.Tables("PrgVersions").Columns("TuitionEarningId")

        '   set foreign key column for TuitionEarningId in TuitionEarningsPercentageRanges table
        Dim fk4(0) As DataColumn
        fk4(0) = ds.Tables("TuitionEarningsPercentageRanges").Columns("TuitionEarningId")

        '   set foreign key column for StuEnrollId in Results table
        Dim fk5(0) As DataColumn
        fk5(0) = ds.Tables("Results").Columns("StuEnrollId")

        '   set foreign key column for PrgVerId in Reqs table
        Dim fk6(0) As DataColumn
        fk6(0) = ds.Tables("Reqs").Columns("PrgVerId")

        '   add relationship TransCodesTransactions
        ds.Relations.Add("TransCodesTransactions", pk1, fk0)

        '   add relationship StuEnrollmentsTransactions
        ds.Relations.Add("StuEnrollmentsTransactions", pk2, fk1)

        '   add relationship PrgVersionsStuEnrollments
        ds.Relations.Add("PrgVersionsStuEnrollments", pk3, fk2)

        '   add relationship TuitionEarningsPrgVersions
        ds.Relations.Add("TuitionEarningsPrgVersions", pk4, fk3)

        '   add relationship TuitionEarningsTuitionEarningsPercentageRanges
        ds.Relations.Add("TuitionEarningsTuitionEarningsPercentageRanges", pk4, fk4)

        '   add relationship StuEnrollmentsResults
        ds.Relations.Add("StuEnrollmentsResults", pk2, fk5)

        '   add relationship PrgVersionsReqs
        ds.Relations.Add("PrgVersionsReqs", pk3, fk6)

        '   add the column that will hold the DeferredRevenue for each transaction 
        Dim transactionsTable As DataTable = ds.Tables("Transactions")
        transactionsTable.Columns.Add("DeferredRevenue", GetType(System.Decimal))

        '   add the column that will hold the TotalDeferredRevenue for each type of transaction
        Dim transCodesTable As DataTable = ds.Tables("TransCodes")
        transCodesTable.Columns.Add("TotalDeferredRevenue", GetType(System.Decimal))

        '   add the column that will hold the AccruedAmount for each type of transaction
        transCodesTable.Columns.Add("AccruedAmount", GetType(System.Decimal))

        '   add the column that will hold the DeferredRevenueAmount for each type of transaction
        'transCodesTable.Columns.Add("DeferredRevenueAmount", GetType(System.Decimal), "TotalDeferredRevenue - AccruedAmount")
        transCodesTable.Columns.Add("DeferredRevenueAmount", GetType(System.Decimal))
        ' ''Added by saraswathi on june 26 2009
        Dim dtStudentException As New DataTable("StudentExceptions")
        dtStudentException.Columns.Add("StudentName", Type.GetType("System.String"))
        dtStudentException.Columns.Add("StuEnrollId", Type.GetType("System.String"))
        dtStudentException.Columns.Add("FeeLevelId", Type.GetType("System.Int32"))
        dtStudentException.Columns.Add("TermStart", Type.GetType("System.DateTime"))
        dtStudentException.Columns.Add("TermEnd", Type.GetType("System.DateTime"))
        dtStudentException.Columns.Add("TransCode", Type.GetType("System.String"))
        dtStudentException.Columns.Add("TransAmount", Type.GetType("System.String"))
        dtStudentException.Columns.Add("Exception", Type.GetType("System.String"))
        ds.Tables.Add(dtStudentException)
        Dim SpecialCAseUnearnedRevExistsandRemainingDurationNotExists As Boolean = False

        '   loop throghout the Transactions table in order to update the "DeferredRevenue" column
        For i As Integer = 0 To transactionsTable.Rows.Count - 1
            For l As Integer = 0 To 0

                Dim Exception As String = ""
                Dim ExceptionNo As Integer = 0

                Dim r As DataRow = transactionsTable.Rows(i)
                r("DeferredRevenue") = 0.0
                Dim str As String = CType(r("StuEnrollId"), Guid).ToString


                ''Get the status of the student, if the student is of nostart status and it was changed from currentlya attending
                ''Then get the date they modified the status
                ''Post deferred revenue for the student for the remaining amount in the next month and then donot post deferred revenue at all, once they have earned all the revenue
                ''Modified on july 27 2009
                Dim nostartdate As Date = Date.MinValue
                If Not r.GetParentRow("StuEnrollmentsTransactions")("SysStatusId") Is DBNull.Value Then
                    If r.GetParentRow("StuEnrollmentsTransactions")("SysStatusId") = 8 Then
                        nostartdate = GetStudentNoStartStatusDate(str)
                    End If
                End If



                '   this is the calculated duration in months of the term
                'Dim termDuration As Decimal = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments")("Weeks") * 12.0 / 52.0
                Dim termStart As Date = Date.MinValue
                Dim termEnd As Date = Date.MinValue

                'calculate last date of attendance
                Dim lda As Date = GetLDA(r)
                If lda = Date.MinValue Then
                    ''Exception date determined is null for this student
                    ExceptionNo = 3
                    Exception = "The student has a drop reason and no date determined. "
                    Dim dr As DataRow
                    dr = dtStudentException.NewRow
                    dr("StudentName") = r("StudentName")
                    dr("StuEnrollId") = str
                    dr("FeeLevelId") = r("FeeLevelId")
                    dr("TermStart") = r("TermStart")
                    dr("TermEnd") = r("TermEnd")
                    Dim rtc1 As DataRow = r.GetParentRow("TransCodesTransactions")
                    dr("TransCode") = rtc1("TransCodeDescrip")
                    dr("TransAmount") = FormatCurrency(r("TransAmount"), , , TriState.True, TriState.True)

                    dr("Exception") = Exception
                    dtStudentException.Rows.Add(dr)
                    Exit For
                End If
                If lda > reportDate Then lda = Date.MaxValue

                Dim feeLevelId As Integer = 0
                If Not r.IsNull("FeeLevelId") Then feeLevelId = r("FeeLevelId")

                Dim strTransactionID As String = CType(r("TransactionID"), Guid).ToString
                Dim defrevPostedDate As Date
                defrevPostedDate = GetMaxDeferredRevPostedDateforStudent(strTransactionID)

                Select Case feeLevelId
                    ''Modified by Saraswathi On June 03 2009
                    ''1- Term and 3- Course 2- Program level
                    ''Course added to function as term.

                    Case 1, 3
                        termStart = r("TermStart")
                        termEnd = MinDate(r("TermEnd"), lda)
                        If nostartdate <> Date.MinValue Then
                            termEnd = MinDate(termEnd, nostartdate)
                        End If


                        ''Case 2 is program version
                        ''Modified by Saraswathi On June 3 2009
                    Case 2
                        '  Case 0, 2
                        termStart = CType(r.GetParentRow("StuEnrollmentsTransactions")("StartDate"), Date)
                        If r.GetParentRow("StuEnrollmentsTransactions")("ExpGradDate") Is System.DBNull.Value Then
                            Dim programDurationInWeeks As Integer = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments")("Weeks")
                            termEnd = MinDate(termStart.AddDays(programDurationInWeeks * 7), lda)
                        Else
                            termEnd = MinDate(r.GetParentRow("StuEnrollmentsTransactions")("ExpGradDate"), lda)
                        End If
                        If nostartdate <> Date.MinValue Then
                            termEnd = MinDate(termEnd, nostartdate)
                        End If

                    Case Else
                        ExceptionNo = 1
                        Exception = "There is no Fee Level Id defined for the student"
                        Dim dr As DataRow
                        dr = dtStudentException.NewRow
                        dr("StudentName") = r("StudentName")
                        dr("StuEnrollId") = str
                        dr("FeeLevelId") = r("FeeLevelId")
                        dr("TermStart") = r("TermStart")
                        dr("TermEnd") = r("TermEnd")
                        Dim rtc2 As DataRow = r.GetParentRow("TransCodesTransactions")
                        dr("TransCode") = rtc2("TransCodeDescrip")

                        dr("TransAmount") = FormatCurrency(r("TransAmount"), , , TriState.True, TriState.True)
                        dr("Exception") = Exception
                        dtStudentException.Rows.Add(dr)
                        Exit For
                End Select
                Dim termDuration As Decimal = 0
                Dim NewtermDuration As Decimal = 0

                Dim RemainingMethod As Boolean = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments").GetParentRow("TuitionEarningsPrgVersions")("RemainingMethod")
                'If RemainingMethod = True Then
                '    If defrevPostedDate >= termEnd Then
                '        Exit For
                '    End If
                'End If


                If termStart.Date = termEnd.Date Then
                    ExceptionNo = 2
                    Exception = "Term Start Date and Term End Date/Date Determined/LDA are the same"
                    Dim dr As DataRow
                    dr = dtStudentException.NewRow
                    dr("StudentName") = r("StudentName")
                    dr("StuEnrollId") = str
                    dr("FeeLevelId") = feeLevelId
                    dr("TermStart") = termStart
                    dr("TermEnd") = termEnd
                    Dim rtc3 As DataRow = r.GetParentRow("TransCodesTransactions")
                    dr("TransCode") = rtc3("TransCodeDescrip")

                    dr("TransAmount") = FormatCurrency(r("TransAmount"), , , TriState.True, TriState.True)
                    dr("Exception") = Exception
                    dtStudentException.Rows.Add(dr)
                    Exit For
                End If
                ''Added by Saraswathi lakshmanan On July 1 2009

                '   check whether to apply a straight percentage or a conversion table
                Dim DeferredRevenuePostingBasedonHolidays As String
                If Not r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments").GetParentRow("TuitionEarningsPrgVersions")("TakeHolidays") Is DBNull.Value Then
                    If r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments").GetParentRow("TuitionEarningsPrgVersions")("TakeHolidays").ToString = "True" Then
                        DeferredRevenuePostingBasedonHolidays = "yes"
                    Else
                        DeferredRevenuePostingBasedonHolidays = "no"
                    End If
                Else
                    DeferredRevenuePostingBasedonHolidays = "no"
                End If

                '   check whether to apply a straight percentage or a conversion table
                Dim percentToEarnIdx As Integer = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments").GetParentRow("TuitionEarningsPrgVersions")("PercentToEarnIdx")
                ''Added by Saraswathi lakshmanan on July 1 2009
                ''If percentToEarnIdx=0 -- Percentage baseed on Calendar days in a month
                ''if percentToEarnIdx=1 -- Equal percentage 
                ''If percentToEarnIdx=2 -- Specify percentage
                r("PercentToEarnIdx") = percentToEarnIdx
                'Dim LoaStartDate As Date = Date.MinValue
                'Dim LOAEndDate As Date = Date.MinValue

                'If Not r("LOAStartDate") Is System.DBNull.Value Then
                '    LoaStartDate = r("LOAStartDate")
                'End If
                'If Not r("LOAEndDate") Is System.DBNull.Value Then
                '    LOAEndDate = r("LOAEndDate")
                'End If

                Dim newStartDate As Date
                'If percentToEarnIdx = 1 Then
                '    newStartDate = MaxDate(termStart, DateAdd(DateInterval.Day, 1, defrevPostedDate))
                'Else
                newStartDate = MaxDate(termStart, DateAdd(DateInterval.Day, 1, defrevPostedDate))
                ' End If


                Dim startDate As Date = termStart
                ''Code modified by Saraswathi Lakshmanan on Sept 10 2010
                ''INclude LOA to elapsed time and term duration only if LOA is between the elapsed time period.

                Dim Trad_numberofHOlidaysbetweentheGivenPeriod As Integer
                Trad_numberofHOlidaysbetweentheGivenPeriod = NumberofHolidaysBetweentheGivenPeriod(reportDate, startDate, campusId)
                Dim Rem_numberofHOlidaysbetweentheGivenPeriod As Integer
                Rem_numberofHOlidaysbetweentheGivenPeriod = NumberofHolidaysBetweentheGivenPeriod(reportDate, newStartDate, campusId)


                Dim Trad_numberofLOAdaysbetweentheGivenPeriod As Integer
                Trad_numberofLOAdaysbetweentheGivenPeriod = NumberofLOADaysBetweentheGivenPeriod(reportDate, startDate, str)
                Dim Rem_numberofLOAdaysbetweentheGivenPeriod As Integer
                Rem_numberofLOAdaysbetweentheGivenPeriod = NumberofLOADaysBetweentheGivenPeriod(reportDate, newStartDate, str)


                If termStart <> Date.MinValue And termEnd <> Date.MinValue Then
                    If percentToEarnIdx = 1 Then
                        ''If it is a equal percentage every month  Then there is no holidays into considereation
                        ''termDuration = (termEnd.Subtract(termStart).TotalDays) / 30
                        termDuration = DateDiff(DateInterval.Month, startDate, termEnd) + 1
                        NewtermDuration = DateDiff(DateInterval.Month, newStartDate, termEnd) + 1

                        DeferredRevenuePostingBasedonHolidays = "no"
                    Else
                        If (DateDiff(DateInterval.DayOfYear, startDate, termEnd) + 1 - Trad_numberofLOAdaysbetweentheGivenPeriod) <= 0 Then
                            termDuration = 0
                        Else
                            termDuration = (DateDiff(DateInterval.DayOfYear, startDate, termEnd) + 1 - Trad_numberofLOAdaysbetweentheGivenPeriod) / 30
                        End If
                        If (DateDiff(DateInterval.DayOfYear, newStartDate, termEnd) + 1 - Rem_numberofLOAdaysbetweentheGivenPeriod) <= 0 Then
                            NewtermDuration = 0
                        Else
                            NewtermDuration = (DateDiff(DateInterval.DayOfYear, newStartDate, termEnd) + 1 - Rem_numberofLOAdaysbetweentheGivenPeriod) / 30
                        End If


                        If DeferredRevenuePostingBasedonHolidays.ToLower() = "yes" Then
                            If (DateDiff(DateInterval.DayOfYear, startDate, termEnd) + 1 - Trad_numberofLOAdaysbetweentheGivenPeriod - Trad_numberofHOlidaysbetweentheGivenPeriod) <= 0 Then
                                termDuration = 0
                            Else
                                termDuration = (DateDiff(DateInterval.DayOfYear, startDate, termEnd) + 1 - Trad_numberofLOAdaysbetweentheGivenPeriod - Trad_numberofHOlidaysbetweentheGivenPeriod) / 30
                            End If
                            If (DateDiff(DateInterval.DayOfYear, newStartDate, termEnd) + 1 - Rem_numberofLOAdaysbetweentheGivenPeriod - Rem_numberofHOlidaysbetweentheGivenPeriod) <= 0 Then
                                NewtermDuration = 0
                            Else
                                NewtermDuration = (DateDiff(DateInterval.DayOfYear, newStartDate, termEnd) + 1 - Rem_numberofLOAdaysbetweentheGivenPeriod - Rem_numberofHOlidaysbetweentheGivenPeriod) / 30
                            End If
                        End If


                        ''Modified by Saraswathi lakshmanan on sept 10 2010
                        ''
                        'termDuration = CalculateTermDuration(LoaStartDate, LOAEndDate, startDate, termEnd, campusId, DeferredRevenuePostingBasedonHolidays)
                        'NewtermDuration = CalculateTermDuration(LoaStartDate, LOAEndDate, newStartDate.AddDays(1), termEnd, campusId, DeferredRevenuePostingBasedonHolidays)
                        '  termDuration = (termEnd.Subtract(termStart).TotalDays) / 30
                    End If
                End If

                Dim EarnedRevenue As Decimal
                Dim UnearnedRevenue As Decimal

                EarnedRevenue = GetUnearnedRevenueForStudentTransaction(strTransactionID)
                UnearnedRevenue = r("TransAmount") - EarnedRevenue




                '   ignore all calculations if the termDuration is zero or negative
                ''If percentToEarnIdx = 0 And Not (termDuration > 0) Then Exit For
                ''Modified by Saraswathi on July 1 2009

                If RemainingMethod = False Then
                    If (percentToEarnIdx = 0 Or percentToEarnIdx = 1) And (termDuration <= 0 And UnearnedRevenue > 0) Then
                        SpecialCAseUnearnedRevExistsandRemainingDurationNotExists = True
                    ElseIf (percentToEarnIdx = 0 Or percentToEarnIdx = 1) And Not (termDuration > 0) And (UnearnedRevenue < 0) Then
                        Exit For
                    End If
                End If
                If RemainingMethod = True Then
                    If (percentToEarnIdx = 0 Or percentToEarnIdx = 1) And (NewtermDuration <= 0 And UnearnedRevenue > 0) Then
                        SpecialCAseUnearnedRevExistsandRemainingDurationNotExists = True
                    ElseIf (percentToEarnIdx = 0 Or percentToEarnIdx = 1) And Not (NewtermDuration > 0) And (UnearnedRevenue < 0) Then
                        Exit For
                    End If


                    '' If (percentToEarnIdx = 0 Or percentToEarnIdx = 1) And Not (NewtermDuration > 0) Then Exit For
                End If


                '   this is the start date of this students term
                ''''modified by saraswathi Lakshmanan
                ''April 3 rd 2009,  mantis Case  15752
                ''elapsed time is based on the student term start date and report Date
                ''so, modifying the student start date to term start date

                'Dim startDate As Date = r.GetParentRow("StuEnrollmentsTransactions")("StartDate")


                '' 

                '   this is the elapsed time in months from the beginning of the period to the report date
                Dim elapsedTime As Decimal = 0
                Dim newElapsedTime As Decimal = 0

                ' elapsedTime = CalculateElaspedTime(LoaStartDate, LOAEndDate, startDate, reportDate, termStart, campusId, DeferredRevenuePostingBasedonHolidays)

                If DeferredRevenuePostingBasedonHolidays.ToLower() = "yes" Then
                    If (DateDiff(DateInterval.DayOfYear, startDate, reportDate) + 1 - Trad_numberofLOAdaysbetweentheGivenPeriod - Trad_numberofHOlidaysbetweentheGivenPeriod) <= 0 Then
                        elapsedTime = 0
                    Else
                        elapsedTime = (DateDiff(DateInterval.DayOfYear, startDate, reportDate) + 1 - Trad_numberofLOAdaysbetweentheGivenPeriod - Trad_numberofHOlidaysbetweentheGivenPeriod) / 30
                    End If

                    If RemainingMethod = True Then
                        If (DateDiff(DateInterval.DayOfYear, newStartDate, reportDate) + 1 - Rem_numberofLOAdaysbetweentheGivenPeriod - Rem_numberofHOlidaysbetweentheGivenPeriod) <= 0 Then
                            newElapsedTime = 0
                        Else
                            newElapsedTime = (DateDiff(DateInterval.DayOfYear, newStartDate, reportDate) + 1 - Rem_numberofLOAdaysbetweentheGivenPeriod - Rem_numberofHOlidaysbetweentheGivenPeriod) / 30
                        End If

                    End If
                Else
                    If (DateDiff(DateInterval.DayOfYear, startDate, reportDate) + 1 - Trad_numberofLOAdaysbetweentheGivenPeriod) <= 0 Then
                        elapsedTime = 0
                    Else
                        elapsedTime = (DateDiff(DateInterval.DayOfYear, startDate, reportDate) + 1 - Trad_numberofLOAdaysbetweentheGivenPeriod) / 30
                    End If

                    If RemainingMethod = True Then
                        If (DateDiff(DateInterval.DayOfYear, newStartDate, reportDate) + 1 - Rem_numberofLOAdaysbetweentheGivenPeriod) <= 0 Then
                            newElapsedTime = 0
                        Else
                            newElapsedTime = (DateDiff(DateInterval.DayOfYear, newStartDate, reportDate) + 1 - Rem_numberofLOAdaysbetweentheGivenPeriod) / 30
                        End If

                    End If
                End If


                ''Modified by Saraswathi lakshmanan on September 10 2010
                ''elapsedTime = CalculateElaspedTime(LoaStartDate, LOAEndDate, startDate, reportDate, termStart, campusId, DeferredRevenuePostingBasedonHolidays)
                ''If RemainingMethod = True Then
                ''    newElapsedTime = CalculateElaspedTime(LoaStartDate, LOAEndDate, newStartDate.AddDays(1), reportDate, newStartDate.AddDays(1), campusId, DeferredRevenuePostingBasedonHolidays)
                ''End If

                '   elapsed time can not be negative
                If elapsedTime < 0 Then elapsedTime = 0.0
                If newElapsedTime < 0 Then newElapsedTime = 0.0

                '   if the student started before this day of the month .. round elapsed time to a half month
                Dim roundedElapsedTime As Decimal = elapsedTime
                Dim NewroundedElapsedTime As Decimal = newElapsedTime

                '   round elapsed time only if the elapsed time is > 0
                If elapsedTime > 0 Then
                    If percentToEarnIdx = 1 Then
                        roundedElapsedTime = DateDiff(DateInterval.Month, startDate, reportDate) + 1

                    Else
                        If reportDate.Month = startDate.Month And reportDate.Year = startDate.Year Then

                            Dim halfMonthBeforeDay As Integer = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments").GetParentRow("TuitionEarningsPrgVersions")("HalfMonthBeforeDay")
                            If startDate.Day <= halfMonthBeforeDay And halfMonthBeforeDay > 0 Then
                                roundedElapsedTime = 0.5

                            End If
                            Dim fullMonthBeforeDay As Integer = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments").GetParentRow("TuitionEarningsPrgVersions")("FullMonthBeforeDay")
                            If startDate.Day <= fullMonthBeforeDay And fullMonthBeforeDay > 0 Then
                                roundedElapsedTime = 1
                            End If
                        End If
                    End If
                End If

                If newElapsedTime > 0 Then
                    If percentToEarnIdx = 1 Then
                        NewroundedElapsedTime = DateDiff(DateInterval.Month, newStartDate, reportDate) + 1

                    Else
                        If reportDate.Month = startDate.Month And reportDate.Year = startDate.Year Then
                            Dim halfMonthBeforeDay As Integer = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments").GetParentRow("TuitionEarningsPrgVersions")("HalfMonthBeforeDay")
                            If startDate.Day <= halfMonthBeforeDay And halfMonthBeforeDay > 0 Then
                                NewroundedElapsedTime = 0.5

                            End If
                            Dim fullMonthBeforeDay As Integer = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments").GetParentRow("TuitionEarningsPrgVersions")("FullMonthBeforeDay")
                            If startDate.Day <= fullMonthBeforeDay And fullMonthBeforeDay > 0 Then
                                NewroundedElapsedTime = 1
                            End If
                        End If
                    End If
                End If



                '   check that rounded elapsed time does not exceed term duration
                If roundedElapsedTime > termDuration Then roundedElapsedTime = termDuration
                If NewroundedElapsedTime > NewtermDuration Then NewroundedElapsedTime = NewtermDuration


                r("RemainingMethod") = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments").GetParentRow("TuitionEarningsPrgVersions")("RemainingMethod")
                If (percentToEarnIdx = 0 Or percentToEarnIdx = 1) Then

                    If RemainingMethod = True Then

                        If NewtermDuration <= 0 And SpecialCAseUnearnedRevExistsandRemainingDurationNotExists = True Then
                            r("DeferredRevenue") = UnearnedRevenue
                        ElseIf NewtermDuration <> 0 Then
                            '   apply straight line percentage
                            r("DeferredRevenue") = UnearnedRevenue * NewroundedElapsedTime / NewtermDuration
                        Else
                            ''Exception
                            ''CAse 1: feelevelid is not set, 
                            'Case 2:term duration =0 ,  term Start and term End is the same or term start and LDA or Datedetermined is the same..
                            r("DeferredRevenue") = 0
                            r("Earned") = 0

                        End If
                    Else
                        If termDuration <= 0 And SpecialCAseUnearnedRevExistsandRemainingDurationNotExists = True Then
                            r("DeferredRevenue") = r("TransAmount")
                        ElseIf termDuration <> 0 Then
                            '   apply straight line percentage
                            r("DeferredRevenue") = r("TransAmount") * roundedElapsedTime / termDuration
                        Else
                            ''Exception
                            ''CAse 1: feelevelid is not set, 
                            'Case 2:term duration =0 ,  term Start and term End is the same or term start and LDA or Datedetermined is the same..
                            r("DeferredRevenue") = 0
                            r("Earned") = 0

                        End If
                    End If



                ElseIf (percentToEarnIdx = 2) Then
                    '   apply table. 
                    Dim PercentageRangeBasisIdx As Integer = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments").GetParentRow("TuitionEarningsPrgVersions")("PercentageRangeBasisIdx")
                    Dim percentArgument As Integer

                    Select Case PercentageRangeBasisIdx
                        Case 0
                            '   Program Length
                            ''Modified by Saraswathi lakshmanan
                            ''When the term duration is zero the percent argument is considered to be zero . Since it is giving divide by zero error
                            If termDuration > 0 Then percentArgument = roundedElapsedTime / termDuration * 100.0 Else percentArgument = 0.0
                        Case 1
                            '   Credits Earned
                            '   add up Credits Earned
                            Dim resultRows() As DataRow = r.GetParentRow("StuEnrollmentsTransactions").GetChildRows("StuEnrollmentsResults")
                            Dim creditsEarned As Integer = 0
                            For Each resultRow As DataRow In resultRows
                                ''modified by Saraswathi lakshmanan on June 4 2009
                                ''Conversion error from dbnull to boolean, since, the student was not graded.
                                If Not resultRow("IsPass") Is DBNull.Value Then
                                    If CType(resultRow("IsPass"), Boolean) Then
                                        creditsEarned += CType(resultRow("Credits"), Integer)
                                    End If
                                End If
                            Next

                            '   this is the total number of credits for the program
                            Dim totalCredits As Integer = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments")("Credits")

                            '   Credits Earned
                            If totalCredits > 0 Then percentArgument = creditsEarned * 100.0 / totalCredits Else percentArgument = 0.0
                        Case 2
                            '   Credits Attempted
                            '   add up Credits Attempted
                            Dim resultRows() As DataRow = r.GetParentRow("StuEnrollmentsTransactions").GetChildRows("StuEnrollmentsResults")
                            Dim creditsAttempted As Integer = 0
                            For Each resultRow As DataRow In resultRows
                                ''modified by Saraswathi lakshmanan on June 4 2009
                                If Not resultRow("IsCreditsAttempted") Is DBNull.Value Then
                                    If CType(resultRow("IsCreditsAttempted"), Boolean) Then
                                        creditsAttempted += CType(resultRow("Credits"), Integer)
                                    End If
                                End If

                            Next

                            '   this is the total number of credits for the program
                            Dim totalCredits As Integer = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments")("Credits")

                            '   Credits Attempted
                            If totalCredits > 0 Then percentArgument = creditsAttempted * 100.0 / totalCredits Else percentArgument = 0.0
                        Case 3
                            '   Courses completed
                            '   get the courses for the program version
                            Dim courses As DataRow() = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments").GetChildRows("PrgVersionsReqs")
                            Dim numberOfCourses As Integer = courses.Length
                            ''calculate number of required courses
                            'Dim numberOfCourses As Integer = 0
                            'For j As Integer = 0 To courses.Length - 1
                            '    If CType(courses(j)("IsRequired"), Boolean) Then
                            '        numberOfCourses += 1
                            '    End If
                            'Next

                            'calculate number of courses taken
                            Dim numberOfCoursesTaken As Integer = 0
                            Dim resultRows() As DataRow = r.GetParentRow("StuEnrollmentsTransactions").GetChildRows("StuEnrollmentsResults")
                            For Each resultRow As DataRow In resultRows
                                ''modified by Saraswathi lakshmanan on June 4 2009
                                If Not resultRow("IsPass") Is DBNull.Value Then
                                    If CType(resultRow("IsPass"), Boolean) Then
                                        numberOfCoursesTaken += 1
                                    End If
                                End If
                            Next

                            'Percentage of courses completed
                            If numberOfCourses > 0 Then percentArgument = numberOfCoursesTaken * 100.0 / numberOfCourses Else percentArgument = 0.0
                        Case 4
                            'get number of scheduled hours
                            Dim hoursScheduled As Decimal = 0.0
                            Dim resultRows() As DataRow = r.GetParentRow("StuEnrollmentsTransactions").GetChildRows("StuEnrollmentsResults")
                            'iterate over all Class Sections
                            For Each resultRow As DataRow In resultRows
                                Dim csm As List(Of AdvantageClassSectionMeeting)
                                If CType(MyAdvAppSettings.AppSettings("TrackSapAttendance"), String).ToLower() = "byclass" Then
                                    csm = (New PeriodsDB).GetCollectionOfMeetingDates(CType(resultRow("ClsSectionId"), Guid).ToString())
                                Else
                                    csm = (New PeriodsDB).GetCollectionOfMeetingDates(r("StuEnrollId"))
                                End If
                                hoursScheduled += GetHoursScheduled(csm, reportDate)
                            Next
                            'get the total duration of the program in hours
                            Dim totalProgramHours As Decimal = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments")("Hours")

                            'Percentage of scheduled hours
                            If totalProgramHours > 0 Then percentArgument = hoursScheduled * 100.0 / totalProgramHours Else percentArgument = 0.0

                        Case 5
                            'get number of completed hours
                            Dim hoursScheduled As Decimal = 0.0
                            Dim hoursCompleted As Decimal = 0.0
                            Dim resultRows() As DataRow = r.GetParentRow("StuEnrollmentsTransactions").GetChildRows("StuEnrollmentsResults")
                            For Each resultRow As DataRow In resultRows
                                Dim csma As List(Of AdvantageClassSectionMeetingAttendance)
                                Dim csm As List(Of AdvantageClassSectionMeeting)
                                If CType(MyAdvAppSettings.AppSettings("TrackSapAttendance"), String).ToLower() = "byclass" Then
                                    csma = (New AttendanceDB).GetCollectionOfMeetingDatesAttendance(CType(resultRow("ClsSectionId"), Guid).ToString(), CType(r("StuEnrollId"), Guid).ToString())
                                    csm = (New PeriodsDB).GetCollectionOfMeetingDates(CType(resultRow("ClsSectionId"), Guid).ToString())
                                Else
                                    csma = (New AttendanceDB).GetCollectionOfMeetingDatesAttendance(CType(r("StuEnrollId"), Guid).ToString())
                                    csm = (New PeriodsDB).GetCollectionOfMeetingDates(r("StuEnrollId"))
                                End If
                                hoursScheduled += GetHoursScheduled(csm, reportDate)
                                hoursCompleted += GetHoursCompleted(csm, csma, reportDate)
                            Next

                            'get the total duration of the program in hours
                            Dim totalProgramHours As Decimal = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments")("Hours")

                            'Percentage of scheduled hours
                            If totalProgramHours > 0 Then percentArgument = hoursCompleted * 100.0 / totalProgramHours Else percentArgument = 0.0

                    End Select

                    '   percent argument should never be grater that 100
                    If percentArgument > 100.0 Then percentArgument = 100.0

                    '   loop throughout TuitionEarningsPercentageRanges table
                    Dim percentageRangesTable As DataTable = ds.Tables("TuitionEarningsPercentageRanges")

                    '   get all rows from the PercentageRanges table
                    Dim rows() As DataRow = r.GetParentRow("StuEnrollmentsTransactions").GetParentRow("PrgVersionsStuEnrollments").GetParentRow("TuitionEarningsPrgVersions").GetChildRows("TuitionEarningsTuitionEarningsPercentageRanges")
                    Dim percentResult As Integer = 0
                    If percentArgument > 0 Then
                        For j As Integer = 0 To rows.Length - 1
                            If CType(rows(j)("UpTo"), Double) >= percentArgument Then
                                percentResult = CType(rows(j)("EarnPercent"), Double)
                                Exit For
                            End If
                        Next
                    End If
                    '   apply percentage
                    r("DeferredRevenue") = (r("TransAmount") * percentResult) / 100.0
                End If

                '   add this value to the totalDeferredRevenue in the transCodes table
                Dim rtc As DataRow = r.GetParentRow("TransCodesTransactions")
                If Not rtc("TotalDeferredRevenue") Is System.DBNull.Value Then
                    rtc("TotalDeferredRevenue") += r("DeferredRevenue")
                Else
                    rtc("TotalDeferredRevenue") = r("DeferredRevenue")
                End If

                '   add Earned Amount from each transaction to AccruedAmount in the transCodes table
                If Not rtc("AccruedAmount") Is System.DBNull.Value Then
                    rtc("AccruedAmount") += r("Earned")
                Else
                    rtc("AccruedAmount") = r("Earned")
                End If

                If RemainingMethod = True Then
                    rtc("DeferredRevenueAmount") = rtc("TotalDeferredRevenue")
                Else
                    rtc("DeferredRevenueAmount") = rtc("TotalDeferredRevenue") - rtc("AccruedAmount")
                End If




            Next
        Next

        '   return dataset
        Return ds

    End Function

    Private Function ComputeElapsedTime(ByVal reportDate As Date, ByVal StartDate As Date, ByVal intActualDaysFromStartDate As Integer, Optional ByVal CampusId As String = "") As Integer
        '   connect to the database
        Dim db As New DataAccess
        Dim intTotalNumberOfHolidays As Integer = -1

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder

        'Check to see if the holiday falls outside the range of Student StartDate (the StartDate parameter)
        'and the date deferred revenue is posted (reportDate parameter)
        'If holiday falls outside the range, then no need to calculate, return 0
        With sb
            .Append(" select Distinct Count(*) as NumberOfHolidays from syHolidays H")
            .Append(" where H.HolidayStartDate <= ?  and H.HolidayEndDate >= ? ")
            If CampusId <> "" Then
                .Append("AND (H.CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId = '")
                .Append(CampusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR     H.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
        End With
        db.AddParameter("@HolidayStartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@HolidayEndDate", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        Try
            intTotalNumberOfHolidays = CType(db.RunParamSQLScalar(sb.ToString), Integer)
            If intTotalNumberOfHolidays >= 1 Then
                Return 0
                Exit Function
            Else
            End If
        Catch ex As System.Exception
            intTotalNumberOfHolidays = 0
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try

        With sb
            .Append(" Select Sum(NumberOfHolidays) as TotalNumberofHolidays from " + vbCrLf)
            .Append("   ( " + vbCrLf)
            ' if Holiday start date and end date falls with in the range
            .Append(" Select Distinct H.HolidayStartDate,H.HolidayEndDate, " + vbCrLf)
            .Append(" DateDiff(day,H.HolidayStartDate,H.HolidayEndDate)+1 as NumberOfHolidays " + vbCrLf)
            .Append(" from syHolidays H where H.HolidayStartDate>=? and H.HolidayEndDate<=? " + vbCrLf)
            If CampusId <> "" Then
                .Append("AND (H.CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId = '")
                .Append(CampusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR     H.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
            .Append(" Union " + vbCrLf)
            ' if Holiday start date is with in the range but holiday end date falls after report date
            ' when enddate falls after report date
            .Append("   select Distinct H.HolidayStartDate,H.HolidayEndDate, " + vbCrLf)
            .Append("   DateDiff(day,H.HolidayStartDate,'" & reportDate & "')+1 as NumberOfHolidays " + vbCrLf)
            .Append("   from syHolidays H where " + vbCrLf)
            .Append("   (H.HolidayStartDate>=? and H.HolidayStartDate<=?) and H.HolidayEndDate >=? " + vbCrLf)
            If CampusId <> "" Then
                .Append("AND (H.CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId = '")
                .Append(CampusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR     H.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
            .Append(" Union " + vbCrLf)
            'if holiday start date falls before student startdate and 
            'holiday end date is before the report date.
            .Append(" select Distinct H.HolidayStartDate,H.HolidayEndDate, " + vbCrLf)
            .Append(" DateDiff(day,'" & StartDate & "',H.HolidayEndDate)+1 as NumberOfHolidays " + vbCrLf)
            .Append(" from syHolidays H where H.HolidayStartDate <=? and H.HolidayEndDate <=? " + vbCrLf)
            .Append(" and DateDiff(day,'" & StartDate & "',H.HolidayEndDate)+1 >=1 " + vbCrLf)
            If CampusId <> "" Then
                .Append("AND (H.CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId = '")
                .Append(CampusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR     H.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
            .Append("   ) Holidays  ")

        End With
        db.AddParameter("@HolidayStartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@HolidayEndDate", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@HolidayStartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@HolidayEndDate", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@HolidayEndDate", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@HolidayStartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@HolidayEndDate", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        Dim intActualDays As Integer = 0
        Try
            intTotalNumberOfHolidays = CType(db.RunParamSQLScalar(sb.ToString), Integer)
        Catch ex As System.Exception
            intTotalNumberOfHolidays = 0
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try
        intActualDays = intActualDaysFromStartDate - intTotalNumberOfHolidays
        Return intActualDays
    End Function
    Private Function GetLDA(ByVal r As DataRow) As Date
        If r.GetParentRow("StuEnrollmentsTransactions")("DropReasonId") Is System.DBNull.Value Then
            Return Date.MaxValue
        Else
            If r.GetParentRow("StuEnrollmentsTransactions")("DateDetermined") Is System.DBNull.Value Then
                If Not r.GetParentRow("StuEnrollmentsTransactions")("LDA") Is System.DBNull.Value Then
                    Return CType(r.GetParentRow("StuEnrollmentsTransactions")("LDA"), Date)
                Else
                    Return Date.MinValue
                End If
            Else
                Return CType(r.GetParentRow("StuEnrollmentsTransactions")("DateDetermined"), Date)
            End If
        End If
    End Function
    Private Function MinDate(ByVal date1 As Date, ByVal date2 As Date) As Date
        If date1 < date2 Then Return date1
        Return date2
    End Function
    Private Function MaxDate(ByVal date1 As Date, ByVal date2 As Date) As Date
        If date1 < date2 Then Return date2
        Return date1
    End Function

    Private Function GetHoursScheduled(ByVal csm As List(Of AdvantageClassSectionMeeting), ByVal reportDate As Date) As Decimal

        'update hoursScheduled
        Dim hoursScheduled As Decimal = 0.0
        For Each meeting As AdvantageClassSectionMeeting In csm
            If meeting.MeetingDateAndTime <= reportDate.Add(New TimeSpan(23, 59, 59)) Then
                hoursScheduled += meeting.Duration / 60.0
            End If
        Next

        Return hoursScheduled

    End Function
    Private Function GetHoursCompleted(ByVal csm As List(Of AdvantageClassSectionMeeting), ByVal csma As List(Of AdvantageClassSectionMeetingAttendance), ByVal reportDate As Date) As Decimal

        'update hoursCompleted
        Dim hoursCompleted As Decimal = 0.0
        For Each meeting As AdvantageClassSectionMeetingAttendance In csma
            If meeting.MeetingDateAndTime <= reportDate.Add(New TimeSpan(23, 59, 59)) Then
                'hoursCompleted += meeting.Actual * GetMeetingDuration(csm, meeting.MeetingDateAndTime)
                hoursCompleted += meeting.Actual
            End If
        Next

        Return hoursCompleted

    End Function
    Private Function GetMeetingDuration(ByVal csm As List(Of AdvantageClassSectionMeeting), ByVal meetingDate As DateTime) As Decimal
        For Each meeting As AdvantageClassSectionMeeting In csm
            If meeting.MeetingDateAndTime = meetingDate Then
                Return meeting.Duration
            End If
        Next
    End Function
    Public Function GetDeferredRevenueDates() As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("           DefRevenueDate ")
            .Append("FROM	    saDeferredRevenues DR ")
            .Append("ORDER BY   DefRevenueDate DESC")
        End With

        '   Execute the query
        Dim ds As New DataSet
        ds = db.RunParamSQLDataSet(sb.ToString)

        ds.Tables(0).Columns.Add(New DataColumn("DefRevenueDateDescrip", System.Type.GetType("System.String")))

        For Each dr As DataRow In ds.Tables(0).Rows
            dr("DefRevenueDateDescrip") = Convert.ToDateTime(dr("DefRevenueDate")).ToShortDateString
        Next

        '   return table
        Return ds.Tables(0)

    End Function
    'Private Function AddstudenttoExceptionTable(ByVal ds As DataSet) As DataTable
    '    Dim dtexception As New DataTable
    '    dtexception.Columns.Add("StudentName", System.Type.GetType("System.String"))
    '    dtexception.Columns.Add("StuEnrollId", System.Type.GetType("System.String"))
    '    dtexception.Columns.Add("FeeLevelId", System.Type.GetType("System.Int32"))
    '    dtexception.Columns.Add("TermStart", System.Type.GetType("System.Datetime"))
    '    dtexception.Columns.Add("TermEnd", System.Type.GetType("System.Datetime"))
    '    dtexception.Columns.Add("Exception", System.Type.GetType("System.String"))

    'End Function


    ''Added to calculate the Elasped Time
    Public Function CalculateElaspedTime(ByVal LOAStartDate As Date, ByVal LOAEndDate As Date, ByVal StartDate As Date, ByVal ReportDate As Date, ByVal TermStart As Date, ByVal Campusid As String, ByVal DeferredRevenuePostingBasedonHolidays As String) As Decimal



        If DeferredRevenuePostingBasedonHolidays.ToLower() = "yes" Then
            ''Calculate Elasped Time with Holidays

            If Not LOAStartDate = Date.MinValue Then
                Dim sLOA As Date = LOAStartDate
                Dim eLOA As Date = LOAEndDate
                Dim loaCase As Integer = 0
                'there are six cases
                If eLOA <= StartDate Then loaCase = 1
                If sLOA < StartDate And eLOA > StartDate And eLOA <= ReportDate Then loaCase = 2
                If sLOA >= StartDate And eLOA <= ReportDate Then loaCase = 3
                If sLOA >= StartDate And eLOA > ReportDate Then loaCase = 4
                If sLOA >= ReportDate Then loaCase = 5
                If sLOA <= StartDate And eLOA >= ReportDate Then loaCase = 6
                'commented by balaji on 02/04/2009 to take the holidays into consideration
                '    Select Case loaCase
                '        Case 1, 5   'out of range
                '            elapsedTime = (reportDate.Subtract(startDate).TotalDays + 1.0) / 30.0
                '        Case 2  'loa starts before startDate and ends after startDate but before reportDate
                '            elapsedTime = (reportDate.Subtract(eLOA).TotalDays + 1.0) / 30.0
                '        Case 3 'loa inside range
                '            elapsedTime = (sLOA.Subtract(startDate).TotalDays + reportDate.Subtract(eLOA).TotalDays + 1.0) / 30.0
                '        Case 4 'loa starts in range but ends after reportDate
                '            elapsedTime = (sLOA.Subtract(startDate).TotalDays + 1.0) / 30.0
                '        Case 6 'loa starts before startDate and ends after reportDate
                '            elapsedTime = 0
                '    End Select
                'Else
                '    elapsedTime = (reportDate.Subtract(startDate).TotalDays + 1.0) / 30.0
                'End If
                Dim intComputedDays As Integer = 0
                Dim intCountOfDaysMarkedAsHoliday As Integer = 0
                Dim intDaysStudentAttended As Integer = 0
                Dim intDaysFromLOAStartToStudentStartDate As Integer = 0
                Dim intDaysFromLOAEndToReportDate As Integer = 0
                Select Case loaCase
                    Case 1, 5   'out of range
                        intComputedDays = ReportDate.Subtract(StartDate).TotalDays + 1.0
                        intCountOfDaysMarkedAsHoliday = (New ClassSchedulesDB).GetHolidaysAfterLOA(StartDate, ReportDate, intComputedDays, 1, Campusid)
                        intDaysStudentAttended = intComputedDays - intCountOfDaysMarkedAsHoliday
                        '' elapsedTime = intComputedDays / 30.0
                        Return (intComputedDays / 30.0)
                    Case 2  'loa starts before startDate and ends after startDate but before reportDate
                        'intComputedDays = ReportDate.Subtract(eLOA).TotalDays + 1.0
                        intComputedDays = ReportDate.Subtract(eLOA).TotalDays
                        intCountOfDaysMarkedAsHoliday = (New ClassSchedulesDB).GetHolidaysAfterLOA(eLOA, ReportDate, intComputedDays, 2, Campusid)
                        intDaysStudentAttended = intComputedDays - intCountOfDaysMarkedAsHoliday
                        'elapsedTime = intDaysStudentAttended / 30.0
                        Return (intDaysStudentAttended / 30.0)
                    Case 3 'loa inside range
                        intDaysFromLOAStartToStudentStartDate = sLOA.Subtract(StartDate).TotalDays
                        intDaysFromLOAEndToReportDate = ReportDate.Subtract(eLOA).TotalDays
                        ' intComputedDays = intDaysFromLOAStartToStudentStartDate + intDaysFromLOAEndToReportDate + 1.0
                        intComputedDays = intDaysFromLOAStartToStudentStartDate + intDaysFromLOAEndToReportDate
                        intCountOfDaysMarkedAsHoliday = (New ClassSchedulesDB).GetHolidaysAfterLOAStartDateAndLOAEndDate(sLOA, StartDate, intDaysFromLOAStartToStudentStartDate, eLOA, ReportDate, intDaysFromLOAEndToReportDate, Campusid)
                        intDaysStudentAttended = intComputedDays - intCountOfDaysMarkedAsHoliday
                        'elapsedTime = intDaysStudentAttended / 30.0
                        Return (intDaysStudentAttended / 30.0)

                    Case 4 'loa starts in range but ends after reportDate
                        ' intComputedDays = sLOA.Subtract(StartDate).TotalDays + 1.0
                        intComputedDays = sLOA.Subtract(StartDate).TotalDays
                        intCountOfDaysMarkedAsHoliday = (New ClassSchedulesDB).GetHolidaysAfterLOA(sLOA, StartDate, intComputedDays, 4, Campusid)
                        intDaysStudentAttended = intComputedDays - intCountOfDaysMarkedAsHoliday
                        ' elapsedTime = intDaysStudentAttended / 30.0
                        Return (intDaysStudentAttended / 30.0)
                    Case 6 'loa starts before startDate and ends after reportDate
                        ' elapsedTime = 0
                        Return 0
                End Select
            Else
                ''''Modified by Saraswathi lakshmanan
                ''April 3rd 2009
                ''The Elaspsed time is the Report Date - term Start Date
                ''hence modified from Student StartDate to term Start Date

                'Dim intDaysFromStartDate As Integer = 0
                'Dim intComputedElapsedTime As Integer = 0
                'intDaysFromStartDate = reportDate.Subtract(startDate).TotalDays + 1.0
                'intComputedElapsedTime = ComputeElapsedTime(reportDate, startDate, intDaysFromStartDate, campusId)
                ''elapsedTime = (reportDate.Subtract(startDate).TotalDays + 1.0) / 30.0
                'elapsedTime = intComputedElapsedTime / 30.0
                Dim intDaysFromTermStartDate As Integer = 0
                Dim intComputedElapsedTime As Integer = 0
                intDaysFromTermStartDate = ReportDate.Subtract(TermStart).TotalDays + 1.0
                intComputedElapsedTime = ComputeElapsedTime(ReportDate, TermStart, intDaysFromTermStartDate, Campusid)
                'elapsedTime = (reportDate.Subtract(startDate).TotalDays + 1.0) / 30.0
                'elapsedTime = intComputedElapsedTime / 30.0
                Return (intComputedElapsedTime / 30.0)
            End If
        Else
            ''Calculate Elasped Time With out holidays
            ' commented by balaji on 02/04/2009 to take the holidays into consideration
            If Not LOAStartDate = Date.MinValue Then
                Dim sLOA As Date = LOAStartDate
                Dim eLOA As Date = LOAEndDate
                Dim loaCase As Integer = 0
                'there are six cases
                If eLOA <= StartDate Then loaCase = 1
                If sLOA < StartDate And eLOA > StartDate And eLOA <= ReportDate Then loaCase = 2
                If sLOA >= StartDate And eLOA <= ReportDate Then loaCase = 3
                If sLOA >= StartDate And eLOA > ReportDate Then loaCase = 4
                If sLOA >= ReportDate Then loaCase = 5
                If sLOA <= StartDate And eLOA >= ReportDate Then loaCase = 6
                Select Case loaCase
                    Case 1, 5   'out of range
                        'elapsedTime = (ReportDate.Subtract(StartDate).TotalDays + 1.0) / 30.0
                        Return ((ReportDate.Subtract(StartDate).TotalDays + 1.0) / 30.0)
                    Case 2  'loa starts before startDate and ends after startDate but before reportDate
                        ' elapsedTime = (ReportDate.Subtract(eLOA).TotalDays + 1.0) / 30.0
                        '  Return ((ReportDate.Subtract(eLOA).TotalDays + 1.0) / 30.0)
                        Return ((ReportDate.Subtract(eLOA).TotalDays) / 30.0)
                    Case 3 'loa inside range
                        'elapsedTime = (sLOA.Subtract(StartDate).TotalDays + ReportDate.Subtract(eLOA).TotalDays + 1.0) / 30.0
                        ' Return ((sLOA.Subtract(StartDate).TotalDays + ReportDate.Subtract(eLOA).TotalDays + 1.0) / 30.0)
                        Return ((sLOA.Subtract(StartDate).TotalDays + ReportDate.Subtract(eLOA).TotalDays) / 30.0)
                    Case 4 'loa starts in range but ends after reportDate
                        ' elapsedTime = (sLOA.Subtract(StartDate).TotalDays + 1.0) / 30.0
                        '  Return ((sLOA.Subtract(StartDate).TotalDays + 1.0) / 30.0)
                        Return ((sLOA.Subtract(StartDate).TotalDays) / 30.0)
                    Case 6 'loa starts before startDate and ends after reportDate
                        'elapsedTime = 0
                        Return 0
                End Select
            Else
                'elapsedTime = (ReportDate.Subtract(StartDate).TotalDays + 1.0) / 30.0
                Return ((ReportDate.Subtract(StartDate).TotalDays + 1.0) / 30.0)
            End If
        End If




    End Function

    ''Added by Saraswathi lakshmanan on May 18 2010
    ''Compute the term Durtion
    ''18962: Deferred Revenue Posting issue:Negative Tuition Amounts 
    Public Function CalculateTermDuration(ByVal LOAStartDate As Date, ByVal LOAEndDate As Date, ByVal PrevDefRevenueDate As Date, ByVal TermEndDate As Date, ByVal Campusid As String, ByVal DeferredRevenuePostingBasedonHolidays As String) As Decimal



        If DeferredRevenuePostingBasedonHolidays.ToLower() = "yes" Then
            ''Calculate Elasped Time with Holidays

            If Not LOAStartDate = Date.MinValue Then
                Dim sLOA As Date = LOAStartDate
                Dim eLOA As Date = LOAEndDate
                Dim loaCase As Integer = 0
                'there are six cases
                If eLOA <= PrevDefRevenueDate Then loaCase = 1
                If sLOA < PrevDefRevenueDate And eLOA > PrevDefRevenueDate And eLOA <= TermEndDate Then loaCase = 2
                If sLOA >= PrevDefRevenueDate And eLOA <= TermEndDate Then loaCase = 3
                If sLOA >= PrevDefRevenueDate And eLOA > TermEndDate Then loaCase = 4
                If sLOA >= PrevDefRevenueDate Then loaCase = 5
                If sLOA <= PrevDefRevenueDate And eLOA >= TermEndDate Then loaCase = 6
                'commented by balaji on 02/04/2009 to take the holidays into consideration
                '    Select Case loaCase
                '        Case 1, 5   'out of range
                '            elapsedTime = (reportDate.Subtract(startDate).TotalDays + 1.0) / 30.0
                '        Case 2  'loa starts before startDate and ends after startDate but before reportDate
                '            elapsedTime = (reportDate.Subtract(eLOA).TotalDays + 1.0) / 30.0
                '        Case 3 'loa inside range
                '            elapsedTime = (sLOA.Subtract(startDate).TotalDays + reportDate.Subtract(eLOA).TotalDays + 1.0) / 30.0
                '        Case 4 'loa starts in range but ends after reportDate
                '            elapsedTime = (sLOA.Subtract(startDate).TotalDays + 1.0) / 30.0
                '        Case 6 'loa starts before startDate and ends after reportDate
                '            elapsedTime = 0
                '    End Select
                'Else
                '    elapsedTime = (reportDate.Subtract(startDate).TotalDays + 1.0) / 30.0
                'End If
                Dim intComputedDays As Integer = 0
                Dim intCountOfDaysMarkedAsHoliday As Integer = 0
                Dim intDaysStudentAttended As Integer = 0
                Dim intDaysFromLOAStartToStudentStartDate As Integer = 0
                Dim intDaysFromLOAEndToReportDate As Integer = 0
                Select Case loaCase
                    Case 1, 5   'out of range
                        intComputedDays = TermEndDate.Subtract(PrevDefRevenueDate).TotalDays
                        intCountOfDaysMarkedAsHoliday = (New ClassSchedulesDB).GetHolidaysAfterLOA(PrevDefRevenueDate, TermEndDate, intComputedDays, 1, Campusid)
                        intDaysStudentAttended = intComputedDays - intCountOfDaysMarkedAsHoliday
                        '' elapsedTime = intComputedDays / 30.0
                        Return (intComputedDays / 30.0)
                    Case 2  'loa starts before startDate and ends after startDate but before reportDate
                        intComputedDays = TermEndDate.Subtract(eLOA).TotalDays
                        intCountOfDaysMarkedAsHoliday = (New ClassSchedulesDB).GetHolidaysAfterLOA(eLOA, TermEndDate, intComputedDays, 2, Campusid)
                        intDaysStudentAttended = intComputedDays - intCountOfDaysMarkedAsHoliday
                        'elapsedTime = intDaysStudentAttended / 30.0
                        Return (intDaysStudentAttended / 30.0)
                    Case 3 'loa inside range
                        intDaysFromLOAStartToStudentStartDate = sLOA.Subtract(PrevDefRevenueDate).TotalDays
                        intDaysFromLOAEndToReportDate = TermEndDate.Subtract(eLOA).TotalDays
                        intComputedDays = intDaysFromLOAStartToStudentStartDate + intDaysFromLOAEndToReportDate
                        intCountOfDaysMarkedAsHoliday = (New ClassSchedulesDB).GetHolidaysAfterLOAStartDateAndLOAEndDate(sLOA, PrevDefRevenueDate, intDaysFromLOAStartToStudentStartDate, eLOA, TermEndDate, intDaysFromLOAEndToReportDate, Campusid)
                        intDaysStudentAttended = intComputedDays - intCountOfDaysMarkedAsHoliday
                        'elapsedTime = intDaysStudentAttended / 30.0
                        Return (intDaysStudentAttended / 30.0)

                    Case 4 'loa starts in range but ends after reportDate
                        intComputedDays = sLOA.Subtract(PrevDefRevenueDate).TotalDays
                        intCountOfDaysMarkedAsHoliday = (New ClassSchedulesDB).GetHolidaysAfterLOA(sLOA, PrevDefRevenueDate, intComputedDays, 4, Campusid)
                        intDaysStudentAttended = intComputedDays - intCountOfDaysMarkedAsHoliday
                        ' elapsedTime = intDaysStudentAttended / 30.0
                        Return (intDaysStudentAttended / 30.0)
                    Case 6 'loa starts before startDate and ends after reportDate
                        ' elapsedTime = 0
                        Return 0
                End Select
            Else
                ''''Modified by Saraswathi lakshmanan
                ''April 3rd 2009
                ''The Elaspsed time is the Report Date - term Start Date
                ''hence modified from Student StartDate to term Start Date

                'Dim intDaysFromStartDate As Integer = 0
                'Dim intComputedElapsedTime As Integer = 0
                'intDaysFromStartDate = reportDate.Subtract(startDate).TotalDays + 1.0
                'intComputedElapsedTime = ComputeElapsedTime(reportDate, startDate, intDaysFromStartDate, campusId)
                ''elapsedTime = (reportDate.Subtract(startDate).TotalDays + 1.0) / 30.0
                'elapsedTime = intComputedElapsedTime / 30.0
                Dim intDaysFromTermStartDate As Integer = 0
                Dim intComputedtermDuration As Integer = 0
                intDaysFromTermStartDate = TermEndDate.Subtract(PrevDefRevenueDate).TotalDays
                intComputedtermDuration = ComputeElapsedTime(TermEndDate, PrevDefRevenueDate, intDaysFromTermStartDate, Campusid)
                'elapsedTime = (reportDate.Subtract(startDate).TotalDays + 1.0) / 30.0
                'elapsedTime = intComputedElapsedTime / 30.0
                Return (intComputedtermDuration / 30.0)
            End If
        Else
            ''Calculate Elasped Time With out holidays
            ' commented by balaji on 02/04/2009 to take the holidays into consideration
            If Not LOAStartDate = Date.MinValue Then
                Dim sLOA As Date = LOAStartDate
                Dim eLOA As Date = LOAEndDate
                Dim loaCase As Integer = 0
                'there are six cases
                If eLOA <= PrevDefRevenueDate Then loaCase = 1
                If sLOA < PrevDefRevenueDate And eLOA > PrevDefRevenueDate And eLOA <= TermEndDate Then loaCase = 2
                If sLOA >= PrevDefRevenueDate And eLOA <= TermEndDate Then loaCase = 3
                If sLOA >= PrevDefRevenueDate And eLOA > TermEndDate Then loaCase = 4
                If sLOA >= TermEndDate Then loaCase = 5
                If sLOA <= PrevDefRevenueDate And eLOA >= TermEndDate Then loaCase = 6
                Select Case loaCase
                    Case 1, 5   'out of range
                        'elapsedTime = (ReportDate.Subtract(StartDate).TotalDays + 1.0) / 30.0
                        Return ((TermEndDate.Subtract(PrevDefRevenueDate).TotalDays) / 30.0)
                    Case 2  'loa starts before startDate and ends after startDate but before reportDate
                        ' elapsedTime = (ReportDate.Subtract(eLOA).TotalDays + 1.0) / 30.0
                        Return ((TermEndDate.Subtract(eLOA).TotalDays) / 30.0)
                    Case 3 'loa inside range
                        'elapsedTime = (sLOA.Subtract(StartDate).TotalDays + ReportDate.Subtract(eLOA).TotalDays + 1.0) / 30.0
                        Return ((sLOA.Subtract(PrevDefRevenueDate).TotalDays + TermEndDate.Subtract(eLOA).TotalDays) / 30.0)
                    Case 4 'loa starts in range but ends after reportDate
                        ' elapsedTime = (sLOA.Subtract(StartDate).TotalDays + 1.0) / 30.0
                        Return ((sLOA.Subtract(PrevDefRevenueDate).TotalDays) / 30.0)
                    Case 6 'loa starts before startDate and ends after reportDate
                        'elapsedTime = 0
                        Return 0
                End Select
            Else
                'elapsedTime = (ReportDate.Subtract(StartDate).TotalDays + 1.0) / 30.0
                Return ((TermEndDate.Subtract(PrevDefRevenueDate).TotalDays) / 30.0)
            End If
        End If




    End Function


    ''Added by SAraswathi Lakshmanan on July 27 2009

    ''If the student is in nostart status then get the date ythe student changed the ststus
    Private Function GetStudentNoStartStatusDate(ByVal StuEnrollId As String) As Date

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" Select Top 1 MOdDate from syStudentStatusChanges where StuEnrollId= ?")
            .Append(" and NewStatusID in (Select StatusCodeId from systatusCodes where SysStatusId=8) ")
            .Append(" and OrigStatusID in (Select StatusCodeId from systatusCodes where SysStatusId=9 ) ")
        End With

        '   Execute the query
        Dim ds As New DataSet
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)(0)
            Else
                Return Date.MinValue
            End If
        Else
            Return Date.MinValue
        End If




    End Function


    ''Added by Saraswathi lakshmanan on May 18 2010
    ''Added to find the max deferred revenue posted date for the given transactionid 
    ''18962: Deferred Revenue Posting issue:Negative Tuition Amounts 
    Private Function GetMaxDeferredRevPostedDateforStudent(ByVal TransactionID As String) As Date

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" Select MAX(DefrevenueDate) from saDeferredRevenues where TransactionId= ? ")
        End With

        '   Execute the query
        Dim ds As New DataSet
        db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)(0) Is DBNull.Value Then
                    Return Date.MinValue
                Else
                    Return ds.Tables(0).Rows(0)(0)
                End If

            Else
                Return Date.MinValue
            End If
        Else
            Return Date.MinValue
        End If

    End Function


    Private Function GetUnearnedRevenueForStudentTransaction(ByVal TransactionID As String) As Decimal

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" Select SUM(Amount) from saDeferredRevenues where TransactionId= ? ")
        End With

        '   Execute the query
        Dim ds As New DataSet
        db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)(0) Is DBNull.Value Then
                    Return 0
                Else
                    Return ds.Tables(0).Rows(0)(0)
                End If

            Else
                Return 0
            End If
        Else
            Return 0
        End If

    End Function
    ''new Function Added by Saraswathi Lakshmanan to find the NUmber of Holidays between the deferred run report Start date and report end date
    ''The parameters are students report start and end dates and CampusID
    ''Function added on Sept 10 2010
    Private Function NumberofHolidaysBetweentheGivenPeriod(ByVal reportDate As Date, ByVal StartDate As Date, Optional ByVal CampusId As String = "") As Integer
        '   connect to the database
        Dim db As New DataAccess
        Dim intTotalNumberOfHolidays As Integer = -1

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder

        'Check to see if the holiday falls outside the range of Student StartDate (the StartDate parameter)
        'and the date deferred revenue is posted (reportDate parameter)
        'If holiday falls outside the range, then no need to calculate, return 0
        With sb
            .Append(" select Distinct Count(*) as NumberOfHolidays from syHolidays H")
            .Append(" where H.HolidayStartDate <= ?  and H.HolidayEndDate >= ? ")
            If CampusId <> "" Then
                .Append("AND (H.CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId = '")
                .Append(CampusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR     H.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
        End With
        db.AddParameter("@HolidayStartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@HolidayEndDate", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        Try
            intTotalNumberOfHolidays = CType(db.RunParamSQLScalar(sb.ToString), Integer)
            If intTotalNumberOfHolidays >= 1 Then
                Return DateDiff(DateInterval.DayOfYear, StartDate, reportDate) + 1
                ' Return 0
                Exit Function
            Else
            End If
        Catch ex As System.Exception
            intTotalNumberOfHolidays = 0
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try

        With sb
            .Append(" Select Sum(NumberOfHolidays) as TotalNumberofHolidays from " + vbCrLf)
            .Append("   ( " + vbCrLf)
            ' if Holiday start date and end date falls with in the range
            .Append(" Select Distinct H.HolidayStartDate,H.HolidayEndDate, " + vbCrLf)
            .Append(" DateDiff(day,H.HolidayStartDate,H.HolidayEndDate)+1 as NumberOfHolidays " + vbCrLf)
            .Append(" from syHolidays H where H.HolidayStartDate>=? and H.HolidayEndDate<=? " + vbCrLf)
            If CampusId <> "" Then
                .Append("AND (H.CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId = '")
                .Append(CampusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR     H.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
            .Append(" Union " + vbCrLf)
            ' if Holiday start date is with in the range but holiday end date falls after report date
            ' when enddate falls after report date
            .Append("   select Distinct H.HolidayStartDate,H.HolidayEndDate, " + vbCrLf)
            .Append("   DateDiff(day,H.HolidayStartDate,'" & reportDate & "')+1 as NumberOfHolidays " + vbCrLf)
            .Append("   from syHolidays H where " + vbCrLf)
            .Append("   (H.HolidayStartDate>=? and H.HolidayStartDate<=?) and H.HolidayEndDate >=? " + vbCrLf)
            If CampusId <> "" Then
                .Append("AND (H.CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId = '")
                .Append(CampusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR     H.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
            .Append(" Union " + vbCrLf)
            'if holiday start date falls before student startdate and 
            'holiday end date is before the report date.
            .Append(" select Distinct H.HolidayStartDate,H.HolidayEndDate, " + vbCrLf)
            .Append(" DateDiff(day,'" & StartDate & "',H.HolidayEndDate)+1 as NumberOfHolidays " + vbCrLf)
            .Append(" from syHolidays H where H.HolidayStartDate <? and H.HolidayEndDate <=? " + vbCrLf)
            .Append(" and DateDiff(day,'" & StartDate & "',H.HolidayEndDate)+1 >=1 " + vbCrLf)
            If CampusId <> "" Then
                .Append("AND (H.CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId = '")
                .Append(CampusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR     H.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
            .Append("   ) Holidays  ")

        End With
        db.AddParameter("@HolidayStartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@HolidayEndDate", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@HolidayStartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@HolidayEndDate", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@HolidayEndDate", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@HolidayStartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@HolidayEndDate", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        Dim intActualDays As Integer = 0
        Try
            intTotalNumberOfHolidays = CType(db.RunParamSQLScalar(sb.ToString), Integer)
        Catch ex As System.Exception
            intTotalNumberOfHolidays = 0
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try
        Return intTotalNumberOfHolidays
    End Function


    ''NUmber of LOA Days between the given period

    ''new Function Added by Saraswathi Lakshmanan to find the NUmber of LOA between the deferred run report Start date and report end date
    ''The parameters are students report start and end dates and CampusID
    ''Function added on Sept 10 2010
    Private Function NumberofLOADaysBetweentheGivenPeriod(ByVal reportDate As Date, ByVal StartDate As Date, ByVal StuEnrollId As String) As Integer
        '   connect to the database
        Dim db As New DataAccess
        Dim intTotalNumberOfLOADays As Integer = -1

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder

        'Check to see if the holiday falls outside the range of Student StartDate (the StartDate parameter)
        'and the date deferred revenue is posted (reportDate parameter)
        'If holiday falls outside the range, then no need to calculate, return 0
        With sb
            .Append(" select Distinct Count(*) as NumberOfLOADays from arStudentLOAs")
            .Append(" where StartDate <= ?  and ISNULL(LOAReturnDate,EndDate) >= ? ")
            .Append(" and stuenrollId= '" & StuEnrollId & "' ")
        End With
        db.AddParameter("@StartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@EndDate", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        Try
            intTotalNumberOfLOADays = CType(db.RunParamSQLScalar(sb.ToString), Integer)
            If intTotalNumberOfLOADays >= 1 Then
                Return DateDiff(DateInterval.DayOfYear, StartDate, reportDate) + 1
                '  Return 0
                Exit Function
            Else
            End If
        Catch ex As System.Exception
            intTotalNumberOfLOADays = 0
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try

        With sb
            .Append(" Select Sum(NumberOfLOADays) as TotalNumberofLOAdays from " + vbCrLf)
            .Append("   ( " + vbCrLf)
            ' if LOA start date and end date falls with in the range
            .Append(" Select Distinct StartDate,ISNULL(LOAReturnDate,EndDate) AS LOAEndDate, " + vbCrLf)
            .Append(" DateDiff(day,StartDate,ISNULL(LOAReturnDate,EndDate))+1 as NumberOfLOAdays " + vbCrLf)
            .Append(" from arStudentLOAs where StartDate>=? and ISNULL(LOAReturnDate,EndDate)<=? " + vbCrLf)
            .Append(" and stuenrollId= '" & StuEnrollId & "' ")
            .Append(" Union " + vbCrLf)
            ' if LOA start date is with in the range but holiday end date falls after report date
            ' when enddate falls after report date
            .Append("   select Distinct StartDate,ISNULL(LOAReturnDate,EndDate) AS LOAEndDate, " + vbCrLf)
            .Append("   DateDiff(day,StartDate,'" & reportDate & "')+1 as NumberOfLOAdays " + vbCrLf)
            .Append("   from arStudentLOAs where " + vbCrLf)
            .Append("   (StartDate>=? and StartDate<=?) and ISNULL(LOAReturnDate,EndDate) >=? " + vbCrLf)
            .Append(" and stuenrollId= '" & StuEnrollId & "' ")
            .Append(" Union " + vbCrLf)
            'if holiday start date falls before student startdate and 
            'holiday end date is before the report date.
            .Append(" select Distinct StartDate,ISNULL(LOAReturnDate,EndDate) AS LOAEndDate, " + vbCrLf)
            .Append(" DateDiff(day,'" & StartDate & "',ISNULL(LOAReturnDate,EndDate))+1 as NumberOfLOAdays " + vbCrLf)
            .Append(" from arStudentLOAs where StartDate <? and ISNULL(LOAReturnDate,EndDate) <=? " + vbCrLf)
            .Append(" and DateDiff(day,'" & StartDate & "',ISNULL(LOAReturnDate,EndDate))+1 >=1 " + vbCrLf)
            .Append(" and stuenrollId= '" & StuEnrollId & "' ")
            .Append("   ) LOAdays  ")

        End With
        db.AddParameter("@StartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@EndDate", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@StartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@EndDate", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@EndDate", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@StartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@EndDate", reportDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        Dim intActualDays As Integer = 0
        Try
            intTotalNumberOfLOADays = CType(db.RunParamSQLScalar(sb.ToString), Integer)
        Catch ex As System.Exception
            intTotalNumberOfLOADays = 0
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try
        Return intTotalNumberOfLOADays
    End Function


    Private Class SelectCommands
        Dim _campusId As String
        Dim _reportDate As Date
        Dim _connectionString As String
        Dim _TransDate As String
        Sub New(ByVal reportDate As Date, ByVal campusId As String, ByVal connectionString As String, Optional ByVal TransDate As String = "")
            _campusId = campusId
            _reportDate = reportDate
            _connectionString = connectionString
            _TransDate = TransDate
        End Sub
        ReadOnly Property Transactions() As OleDbCommand
            Get

                Dim MyAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    MyAdvAppSettings = New AdvAppSettings
                End If

                Dim sc As New OleDbCommand()
                '   select only records marked as Deferred Revenue
                Dim sb As New StringBuilder
                With sb
                    .Append("SELECT ")
                    .Append("       T.TransactionId, ")
                    .Append("       (Select FirstName + ' ' + LastName from arStudent S where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=(Select StuEnrollId from saTransactions where TransactionId=T.TransactionId and saTransactions.Voided=0))) As StudentName, ")
                    'include SSN or student identifier
                    If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
                        .Append("       (Select Coalesce(Case Len(S.SSN) When 9 then '***-**-' + SUBSTRING(SSN,6,4) else ' ' end, ' ') ")
                    ElseIf CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToLower = "studentid" Then
                        .Append("       (Select Coalesce(StudentNumber, ' ') ")
                    Else
                        .Append("       (Select Coalesce(" + CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToString + ", ' ') ")
                    End If
                    .Append("       from arStudent S ")
                    .Append("       where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=(Select StuEnrollId from saTransactions where TransactionId=T.TransactionId and saTransactions.Voided=0))) As StudentIdentifier, ")
                    .Append("       T.StuEnrollId, ")
                    .Append("       T.TransCodeId, ")
                    .Append("       T.TransAmount, ")
                    .Append("       (Select Top 1 StartDate from arStudentLOAs where StuEnrollId=T.StuEnrollId order by StartDate desc) AS LOAStartDate, ")
                    .Append("       (Select Top 1 EndDate from arStudentLOAs where StuEnrollId=T.StuEnrollId order by EndDate desc) AS LOAEndDate, ")
                    .Append("       Coalesce((Select sum(Amount) from saDeferredRevenues where TransactionId=T.TransactionId),0) Earned, ")
                    .Append("       T.FeeLevelId, ")
                    .Append("       (Select StartDate from arTerm tm where tm.TermId=T.TermId) as TermStart, ")
                    .Append("       (Select EndDate from arTerm tm where tm.TermId=T.TermId) as TermEnd ")
                    ''PercentRangeIndex added by Saraswathi Lakshmanan on May 18 2010
                    ''To find the percentagetoearnIndex
                    .Append(" , '' as percentToEarnIdx ,0 as RemainingMethod   ")
                    .Append("From   saTransactions T, saTransCodes TC, arStuEnrollments SE ")
                    ''Added by saraswathi on march 16 2009
                    'to fix issue 15412 and 15413
                    ''negative values show up in deferred revenue posting
                    ''So trnastype of type 2 is not shown (i.e payments)
                    .Append(" ,saTransTypes S ")
                    .Append("Where ")

                    .Append("       (T.TransCodeId = TC.TransCodeId) ")
                    .Append("And    TC.DefEarnings = 1 ")
                    .Append("And    SE.StuEnrollId=T.StuEnrollId ")
                    '.Append(" And SE.StuEnrollId='28A29F2B-47D1-4CBB-9128-7FA0DC70697B' ")
                    '.Append(" And TC.TransCodeID='51161D98-F66A-4D3C-BE12-7335B571F0FD' ")
                    .Append("And    T.Voided=0 ")
                    ''Added by saraswathi on march 16 2009
                    'to fix issue 15412 and 15413
                    ''negative values show up in deferred revenue posting
                    ''So trnastype of type 2 is not shown (i.e payments)
                    .Append(" and  T.TransTypeId=S.TransTypeId and T.TransTypeId<>2 ")
                    .Append("And    SE.StartDate <= ? ")
                    sc.Parameters.Add(New OleDbParameter("@ReportDate", _reportDate))
                    .Append("And    T.TransDate <= ? ")
                    sc.Parameters.Add(New OleDbParameter("@ReportDate", _reportDate))

                    '.Append("And    T.TransDate >= '01/01/2010' ")

                    If Not _campusId Is Nothing Then
                        .Append("And    T.CampusId = ? ")
                        sc.Parameters.Add(New OleDbParameter("@CampusId", _campusId))
                    End If

                    ''Added by Saraswathi Lakshmanan on June 30 2009
                    ''To run the deferred revenue staring from the given transdate
                    If Not _TransDate = "" Then
                        .Append("And    T.TransDate >= ? ")
                        sc.Parameters.Add(New OleDbParameter("@TransDate", _TransDate))
                    End If

                    '.Append("And T.StuEnrollId='67887763-86CC-4829-90BA-B01C056191A0' ")
                    '.Append("And T.TransactionId='4805ED14-748D-4660-B60B-D16228BFF180' ")

                End With
                sc.CommandText = sb.ToString()
                'Mantis Case: 17635 - Time out Error
                'Comment By Balaji modification starts here
                sc.CommandTimeout = 600
                'Modification ends here
                sc.Connection = New OleDbConnection(_connectionString)
                Return sc
            End Get
        End Property
        ReadOnly Property TransCodes() As OleDbCommand
            Get
                Dim sc As New OleDbCommand()
                '   build the sql query for TransCodes data adapter
                Dim sb As New StringBuilder
                With sb
                    .Append("SELECT ")
                    .Append("       TC.TransCodeId, ")
                    .Append("       TC.TransCodeDescrip, ")
                    .Append("       SUM(T.TransAmount) TotalAmount  ")
                    .Append("From   saTransCodes TC, saTransactions T ")
                    ''Added by saraswathi on march 16 2009
                    'to fix issue 15412 and 15413
                    ''negative values show up in deferred revenue posting
                    ''So trnastype of type 2 is not shown (i.e payments)
                    .Append(" ,saTransTypes S ")
                    .Append("Where ")
                    .Append("       (T.TransCodeId = TC.TransCodeId) ")
                    .Append("And    (TC.DefEarnings = 1) ")
                    If Not _campusId Is Nothing Then
                        .Append("And    T.CampusId = ? ")
                        sc.Parameters.Add(New OleDbParameter("@CampusId", _campusId))
                    End If
                    .Append("And    T.Voided=0 ")
                    '.Append(" And TC.TransCodeID='51161D98-F66A-4D3C-BE12-7335B571F0FD' ")
                    ''Added by saraswathi on march 16 2009
                    'to fix issue 15412 and 15413
                    ''negative values show up in deferred revenue posting
                    ''So trnastype of type 2 is not shown (i.e payments)
                    .Append(" and T.TransTypeId=S.TransTypeId and T.TransTypeId<>2 ")
                    .Append("Group By ")
                    .Append("       TC.TransCodeId, TC.TransCodeDescrip ")
                End With
                sc.CommandText = sb.ToString()
                'Mantis Case: 17635 - Time out Error
                'Comment By Balaji modification starts here
                sc.CommandTimeout = 600
                'Modification ends here
                sc.Connection = New OleDbConnection(_connectionString)
                Return sc
            End Get
        End Property
        ReadOnly Property StuEnrollments() As OleDbCommand
            Get
                Dim sc As New OleDbCommand()
                '   build the sql query for TransCodes data adapter
                Dim sb As New StringBuilder
                With sb
                    '   select only records marked as Deferred Revenue
                    .Append("SELECT DISTINCT")
                    .Append("       SE.StuEnrollId, ")
                    .Append("       SE.StudentId, ")
                    .Append("       SE.PrgVerId, ")
                    .Append("       SE.StartDate, ")
                    .Append("       SE.ExpGradDate, ")
                    .Append("       SE.DropReasonId, ")
                    .Append("       SE.DateDetermined, ")
                    ''Added by Saraswathi to find the sysstatusCodeId of the studenty
                    .Append("(Select SysStatusId from SySTatusCodes where StatusCodeId in (Select StatusCodeId  from arStuEnrollments where StuENrollId=SE.StuEnrollId) ) SysSTatusID ,")
                    ''Added by saraswathi To find the LDA of the student.
                    ''Changed on 19 May 2009
                    .Append("(select Max(LDA) from ")
                    .Append("( ")
                    .Append("	select max(AttendedDate)as LDA from arExternshipAttendance where StuEnrollId=SE.StuEnrollID ")
                    .Append("	union all ")
                    .Append("	select max(MeetDate) as LDA from atClsSectAttendance where StuEnrollId=SE.StuEnrollID and Actual >= 1 ")
                    .Append("	union all ")
                    .Append("	select max(AttendanceDate) as LDA from atAttendance where EnrollId=SE.StuEnrollID and Actual >=1 ")
                    .Append("	union all ")
                    .Append("	select max(RecordDate) as LDA from arStudentClockAttendance where StuEnrollId=SE.StuEnrollID and (ActualHours >=1.00 and  ActualHours <> 99.00 and ActualHours <> 999.00 and ActualHours <> 9999.00) ")
                    .Append("	union all ")
                    .Append("	select max(MeetDate) as LDA from atConversionAttendance where StuEnrollId=SE.StuEnrollID and (Actual >=1.00 and  Actual <> 99.00 and Actual <> 999.00 and Actual <> 9999.00) ")
                    .Append(" Union All select LDA from arStuEnrollments where StuEnrollId=SE.StuEnrollID  ")
                    .Append(")T1 )as LDA	")
                    ' .Append("       SE.LDA ")
                    .Append("From   arStuEnrollments SE, saTransactions T, saTransCodes TC ")
                    .Append("Where ")
                    .Append("       (SE.StuEnrollId = T.StuEnrollId) ")
                    .Append("And    (T.TransCodeId=TC.TransCodeId) ")
                    .Append("And    TC.DefEarnings = 1 ")
                    .Append("And    T.Voided=0 ")
                    If Not _campusId Is Nothing Then
                        .Append("And    T.CampusId = ? ")
                        sc.Parameters.Add(New OleDbParameter("@CampusId", _campusId))
                    End If
                End With
                sc.CommandText = sb.ToString()
                'Mantis Case: 17635 - Time out Error
                'Comment By Balaji modification starts here
                sc.CommandTimeout = 600
                'Modification ends here
                sc.Connection = New OleDbConnection(_connectionString)
                Return sc
            End Get
        End Property
        ReadOnly Property PrgVersions() As OleDbCommand
            Get
                Dim sc As New OleDbCommand()
                '   build the sql query for TransCodes data adapter
                Dim sb As New StringBuilder
                With sb
                    .Append("SELECT DISTINCT")
                    .Append("       PV.PrgVerId, ")
                    .Append("       PV.TuitionEarningId, ")
                    .Append("       PV.Weeks, ")
                    .Append("       PV.Hours, ")
                    .Append("       PV.Credits ")
                    .Append("From   arPrgVersions PV, arStuEnrollments SE, saTransactions T, saTransCodes TC ")
                    .Append("Where ")
                    .Append("       (PV.PrgVerId=SE.PrgVerId) ")
                    .Append("And    (SE.StuEnrollId = T.StuEnrollId) ")
                    .Append("And    (T.TransCodeId=TC.TransCodeId) ")
                    .Append("And    (TC.DefEarnings = 1) ")
                    .Append("And    T.Voided=0 ")
                    If Not _campusId Is Nothing Then
                        .Append("And    T.CampusId = ? ")
                        sc.Parameters.Add(New OleDbParameter("@CampusId", _campusId))
                    End If
                End With
                sc.CommandText = sb.ToString()
                'Mantis Case: 17635 - Time out Error
                'Comment By Balaji modification starts here
                sc.CommandTimeout = 600
                'Modification ends here
                sc.Connection = New OleDbConnection(_connectionString)
                Return sc
            End Get
        End Property
        ReadOnly Property TuitionEarnings() As OleDbCommand
            Get
                Dim sc As New OleDbCommand()
                '   build the sql query for TransCodes data adapter
                Dim sb As New StringBuilder
                With sb
                    .Append("SELECT Distinct")
                    .Append("       TE.TuitionEarningId, ")
                    .Append("       TE.FullMonthBeforeDay, ")
                    .Append("       TE.HalfMonthBeforeDay, ")
                    .Append("       TE.RevenueBasisIdx, ")
                    .Append("       TE.PercentageRangeBasisIdx, ")
                    .Append("       TE.PercentToEarnIdx ,")
                    ''Added by Saraswathi on July 1 2009
                    .Append("       TE.TakeHolidays ")
                    ''Added on May 18 2010 By Saraswathi lakshmanan 

                    .Append("     ,  TE.RemainingMethod ")

                    .Append("From   saTuitionEarnings TE, arPrgVersions PV, arStuEnrollments SE, saTransactions T, saTransCodes TC ")
                    .Append("Where ")
                    .Append("       (TE.TuitionEarningId=PV.TuitionEarningId) ")
                    .Append("And    (PV.PrgVerId=SE.PrgVerId) ")
                    .Append("And    (SE.StuEnrollId=T.StuEnrollId) ")
                    .Append("And    (T.TransCodeId=TC.TransCodeId) ")
                    .Append("And    (TC.DefEarnings = 1) ")
                    .Append("And    T.Voided=0 ")
                    If Not _campusId Is Nothing Then
                        .Append("And    T.CampusId = ? ")
                        sc.Parameters.Add(New OleDbParameter("@CampusId", _campusId))
                    End If
                End With

                sc.CommandText = sb.ToString()
                'Mantis Case: 17635 - Time out Error
                'Comment By Balaji modification starts here
                sc.CommandTimeout = 600
                'Modification ends here
                sc.Connection = New OleDbConnection(_connectionString)
                Return sc
            End Get
        End Property
        ReadOnly Property TuitionEarningsPercentageRanges() As OleDbCommand
            Get
                Dim sc As New OleDbCommand()
                '   build the sql query for TransCodes data adapter
                Dim sb As New StringBuilder
                sb = New StringBuilder
                With sb
                    .Append("SELECT Distinct")
                    .Append("       TEPR.TuitionEarningsPercentageRangeId, ")
                    .Append("       TE.TuitionEarningId, ")
                    .Append("       TEPR.UpTo, ")
                    .Append("       TEPR.EarnPercent ")
                    .Append("From   saTuitionEarningsPercentageRanges TEPR, saTuitionEarnings TE, arPrgVersions PV, arStuEnrollments SE, saTransactions T, saTransCodes TC ")
                    .Append("Where ")
                    .Append("       (TEPR.TuitionEarningId=TE.TuitionEarningId) ")
                    .Append("And    (TE.TuitionEarningId=PV.TuitionEarningId) ")
                    .Append("And    (PV.PrgVerId=SE.PrgVerId) ")
                    .Append("And    (SE.StuEnrollId=T.StuEnrollId) ")
                    .Append("And    (T.TransCodeId=TC.TransCodeId) ")
                    .Append("And    (TC.DefEarnings = 1) ")
                    If Not _campusId Is Nothing Then
                        .Append("And    T.CampusId = ? ")
                        sc.Parameters.Add(New OleDbParameter("@CampusId", _campusId))
                    End If
                    .Append("Order by TEPR.UpTo ")
                End With

                sc.CommandText = sb.ToString()
                'Mantis Case: 17635 - Time out Error
                'Comment By Balaji modification starts here
                sc.CommandTimeout = 600
                'Modification ends here
                sc.Connection = New OleDbConnection(_connectionString)
                Return sc
            End Get
        End Property
        ReadOnly Property Results() As OleDbCommand
            Get
                Dim sc As New OleDbCommand()
                '   build the sql query for Results data adapter

                Dim sb As New StringBuilder
                sb = New StringBuilder
                With sb
                    '.Append("SELECT DISTINCT ")
                    '.Append("       R.ResultId, ")
                    '.Append("       CS.ClsSectionId, ")
                    '.Append("       SE.StuEnrollId, ")
                    '.Append("       RQ.Credits, ")
                    '.Append("       GSD.IsPass, ")
                    '.Append("       GSD.IsCreditsEarned, ")
                    '.Append("       GSD.IsCreditsAttempted ")
                    '.Append("FROM    arStuEnrollments SE, saTransactions T, saTransCodes TC, arResults R, arGradeSystemDetails GSD, arClassSections CS, arReqs RQ ")
                    '.Append("WHERE ")
                    '.Append("        SE.StuEnrollId = T.StuEnrollId ")
                    '.Append("AND     T.TransCodeId = TC.TransCodeId ")
                    '.Append("AND     TC.DefEarnings = 1 ")
                    '.Append("AND     T.Voided=0 ")
                    '.Append("AND     T.StuEnrollId = R.StuEnrollId ")
                    '.Append("AND	 R.TestId = CS.ClsSectionId ")
                    '.Append("AND 	 CS.ReqId = RQ.ReqId ")
                    '.Append("AND	 (R.GrdSysDetailId = GSD.GrdSysDetailId) ")
                    .Append("       SELECT DISTINCT  ")
                    .Append("               R.ResultId,  ")
                    .Append("               CS.ClsSectionId,  ")
                    .Append("               R.StuEnrollId,  ")
                    .Append("               RQ.Credits,  ")
                    .Append("			   (Select IsPass from arGradeSystemDetails where GrdSysDetailId = R.GrdSysDetailId) as IsPass,  ")
                    .Append("			   (Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = R.GrdSysDetailId) as IsCreditsEarned,  ")
                    .Append("			   (Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = R.GrdSysDetailId) as IsCreditsAttempted  ")
                    .Append("        FROM   saTransactions T, saTransCodes TC, arResults R, arClassSections CS, arReqs RQ  ")
                    .Append("        WHERE  ")
                    .Append("			    T.TransCodeId = TC.TransCodeId  ")
                    .Append("        AND     TC.DefEarnings = 1  ")
                    .Append("        AND     T.Voided=0  ")
                    .Append("        AND     T.StuEnrollId = R.StuEnrollId  ")
                    .Append("        AND		R.TestId = CS.ClsSectionId  ")
                    .Append("        AND 	CS.ReqId = RQ.ReqId ")
                    If Not _campusId Is Nothing Then
                        .Append("And    T.CampusId = ? ")
                        sc.Parameters.Add(New OleDbParameter("@CampusId", _campusId))
                    End If
                End With

                sc.CommandText = sb.ToString()
                'Mantis Case: 17635 - Time out Error
                'Comment By Balaji modification starts here
                sc.CommandTimeout = 600
                'Modification ends here
                sc.Connection = New OleDbConnection(_connectionString)
                Return sc
            End Get
        End Property
        ReadOnly Property Reqs() As OleDbCommand
            Get
                Dim sc As New OleDbCommand()
                '   build the sql query for Reqs data adapter
                Dim sb As New StringBuilder
                With sb
                    .Append("SELECT DISTINCT ")
                    .Append("       PV.PrgVerId, ")
                    .Append("       PV.TuitionEarningId, ")
                    .Append("       R.Descrip, ")
                    .Append("       IsRequired ")
                    .Append("FROM   arPrgVersions PV, arStuEnrollments SE, saTransactions T, saTransCodes TC, arProgVerDef PVD, arReqs R ")
                    .Append("WHERE ")
                    .Append("       PV.PrgVerId = SE.PrgVerId ")
                    .Append("And    SE.PrgVerId=PVD.PrgVerId ")
                    .Append("And    SE.StuEnrollId = T.StuEnrollId ")
                    .Append("And    T.TransCodeId=TC.TransCodeId ")
                    .Append("And    TC.DefEarnings = 1 ")
                    .Append("And    T.Voided=0 ")
                    .Append("And    PVD.ReqId=R.ReqId  ")
                    If Not _campusId Is Nothing Then
                        .Append("And    T.CampusId = ? ")
                        sc.Parameters.Add(New OleDbParameter("@CampusId", _campusId))
                    End If
                End With

                sc.CommandText = sb.ToString()
                'Mantis Case: 17635 - Time out Error
                'Comment By Balaji modification starts here
                sc.CommandTimeout = 600
                'Modification ends here
                sc.Connection = New OleDbConnection(_connectionString)
                Return sc
            End Get
        End Property

    End Class
End Class



