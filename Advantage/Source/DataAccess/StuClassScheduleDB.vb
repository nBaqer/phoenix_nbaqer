Imports FAME.Advantage.Common

Public Class StuClassScheduleDB

#Region "Private Data Members"

    Private ReadOnly myAdvAppSettings As AdvAppSettings
    Private ReadOnly mStudentIdentifier As String

#End Region

        

    Sub New ()
        myAdvAppSettings = AdvAppSettings.GetAppSettings()
        mStudentIdentifier = myAdvAppSettings.AppSettings("StudentIdentifier")
    End Sub

#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return mStudentIdentifier
        End Get
    End Property

#End Region

#Region "Public Methods"

    Public Function GetClassSchedule(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As  DataSet
        Dim strWhere As String = String.Empty
        Dim strOrderBy As String
        Dim strStuID As String

       If paramInfo.FilterList <> "" Then
            strWhere &= paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            If paramInfo.FilterList <> "" Then
                strWhere &= " AND "
            End If
            strWhere &= paramInfo.FilterOther
        End If

        'For default sorting by StartDate, StartTime
        'strWhere &= " AND arClsSectMeetings.ClsSectionId = A.TestId " _
        '& "AND syPeriods.PeriodId = arClsSectMeetings.PeriodId " _
        '& "AND cmTimeInterval.TimeIntervalId = syPeriods.StartTimeId"

        If paramInfo.OrderBy <> "" Then
            If paramInfo.OrderBy.IndexOf("arReqs.Descrip", StringComparison.Ordinal) = -1 Then
                strOrderBy &= "," & paramInfo.OrderBy & ",TimeIn,arClassSections.StartDate,TestId,arReqs.Descrip,arReqs.Code,arClassSections.ClsSection"
            Else
                strOrderBy &= "," & paramInfo.OrderBy
            End If
        Else
            ' strOrderBy = ",arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,arTerm.StartDate,arTerm.TermDescrip, arClassSections.StartDate,cmTimeInterval.TimeIntervalDescrip, arReqs.Descrip,arReqs.Code,arClassSections.ClsSection"
            strOrderBy = ",arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,TimeIn,arTerm.StartDate,arTerm.TermDescrip, arClassSections.StartDate,TestId,arReqs.Descrip,arReqs.Code,arClassSections.ClsSection"
        End If



        If StudentIdentifier = "SSN" Then
            strStuID = "arStudent.SSN AS StudentIdentifier,"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStuID = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
        ElseIf StudentIdentifier = "StudentId" Then
            strStuID = "arStudent.StudentNumber AS StudentIdentifier,"
        End If
        With sb

            'check id the user selected "ALL" campus groups
            If paramInfo.FilterList.IndexOf("CampGrpId", StringComparison.Ordinal) < 0 Then

                'Code modified by Balaji to resolve issue 17527
                'The Class Schedule - Single student report was not pulling classes with no meetings scheduled.
                'Modification starts here for Mantis 17527
                .Append(" SELECT DISTINCT arResults.TestId,arStudent.LastName, 		arStudent.FirstName, 		arStudent.MiddleName,  arStudent.SSN AS StudentIdentifier, " + vbCrLf)
                .Append(" (SELECT arPrograms.ProgDescrip FROM arPrgVersions INNER JOIN arPrograms ON arPrograms.ProgId=arPrgVersions.ProgId " + vbCrLf)
                .Append("  WHERE PrgVerId=arStuEnrollments.PrgVerId) AS ProgDescrip, " + vbCrLf)
                .Append(" (SELECT PrgVerDescrip FROM arPrgVersions  WHERE PrgVerId=arStuEnrollments.PrgVerId) AS PrgVerDescrip, " + vbCrLf)
                .Append("  arTerm.TermDescrip,arReqs.Descrip,arReqs.Code,arClassSections.ClsSection,arReqs.Credits, arReqs.Hours, 	" + vbCrLf)
                .Append(" (SELECT FullName FROM syUsers WHERE UserId=arClassSections.InstructorId) AS InstructorName, " + vbCrLf)
                .Append("  arClassSections.StartDate,arClassSections.EndDate,  'All' as CampGrpDescrip, " + vbCrLf)
                .Append(" (SELECT CampDescrip FROM syCampuses WHERE CampusId=arClassSections.CampusId) AS CampusDescrip, " + vbCrLf)
                .Append(" (SELECT COUNT(*) FROM arClsSectMeetings WHERE ClsSectionId=arResults.TestId) AS MeetingsCount, " + vbCrLf)
                .Append(" arTerm.StartDate as TermStart,arStuEnrollments.StuEnrollId, " + vbCrLf)
                .Append(" TimeIn=(CASE WHEN  (arClsSectMeetings.TimeIntervalId IS NOT NULL) THEN (SELECT TimeIntervalDescrip FROM cmTimeInterval WHERE TimeIntervalId=arClsSectMeetings.TimeIntervalId)	" + vbCrLf)
                .Append(" ELSE (SELECT TimeIntervalDescrip FROM syPeriods,cmTimeInterval WHERE PeriodId=arClsSectMeetings.PeriodId AND StartTimeId=TimeIntervalId)	END) " + vbCrLf)
                '.Append(" ,cmTimeInterval.TimeIntervalDescrip " + vbCrLf)
                .Append(" FROM " + vbCrLf)
                .Append(" 		arResults  INNER JOIN arStuEnrollments  ON arResults.StuEnrollId=arStuEnrollments.StuEnrollId " + vbCrLf)
                .Append(" 		INNER JOIN arStudent  ON arStuEnrollments.StudentId=arStudent.StudentId " + vbCrLf)
                .Append(" 		INNER JOIN arClassSections  ON arResults.TestId=arClassSections.ClsSectionId " + vbCrLf)
                .Append(" 		INNER JOIN arClassSectionTerms  ON arClassSections.ClsSectionId=arClassSectionTerms.ClsSectionId  " + vbCrLf)
                .Append(" 		INNER JOIN arTerm  ON arClassSectionTerms.TermId=arTerm.TermId  " + vbCrLf)
                .Append(" 				INNER JOIN arReqs  ON arClassSections.ReqId=arReqs.ReqId  " + vbCrLf)
                .Append(" 				INNER JOIN syCmpGrpCmps  ON arClassSections.CampusId=syCmpGrpCmps.CampusId  " + vbCrLf)
                .Append(" 				LEFT OUTER JOIN arClsSectMeetings  ON arResults.TestId = arClsSectMeetings.ClsSectionId " + vbCrLf)
                 .Append(" WHERE  " + vbCrLf)
  
                .Append(strWhere.Replace(";", ""))


                If paramInfo.ResId = 295 Then
                    If paramInfo.CampGrpId <> Nothing Then
                        .Append(" ORDER BY syCampGrps.CampGrpDescrip,CampusDescrip  " + vbCrLf)
                    Else
                        .Append(" ORDER BY CampusDescrip  " + vbCrLf)
                    End If
                Else
                    .Append(" ORDER BY syCampGrps.CampGrpDescrip,CampusDescrip  " + vbCrLf)
                End If

                .Append(strOrderBy)
                'Modification ends here for Mantis 17527
            Else
                'Code modified by Balaji to resolve issue 17527
                'The Class Schedule - Single student report was not pulling classes with no meetings scheduled.
                'Modification starts here for Mantis 17527

                .Append(" SELECT DISTINCT    " + vbCrLf)
                .Append("                		arClassSections.CampusId,  " + vbCrLf)
                .Append("                		A.TestId,  " + vbCrLf)
                .Append("                		arStudent.LastName,  " + vbCrLf)
                .Append("                		arStudent.FirstName,  " + vbCrLf)
                .Append("                		arStudent.MiddleName,   " & strStuID)
                .Append("                       (SELECT arPrograms.ProgDescrip FROM arPrgVersions INNER JOIN arPrograms ON arPrograms.ProgId=arPrgVersions.ProgId WHERE PrgVerId=arStuEnrollments.PrgVerId) AS ProgDescrip,  " + vbCrLf)
                .Append(" (SELECT PrgVerDescrip FROM arPrgVersions  WHERE PrgVerId=arStuEnrollments.PrgVerId) AS PrgVerDescrip, " + vbCrLf)
                .Append("                       arTerm.TermDescrip,arReqs.Descrip,arReqs.Code,arClassSections.ClsSection,  " + vbCrLf)
                .Append("                     arReqs.Credits,  " + vbCrLf)
                .Append("                	   arReqs.Hours,  " + vbCrLf)
                .Append("                	   (SELECT FullName FROM syUsers WHERE UserId=arClassSections.InstructorId) AS InstructorName, " + vbCrLf)
                .Append("                       arClassSections.StartDate,  " + vbCrLf)
                .Append("                	   arClassSections.EndDate,  " + vbCrLf)
                .Append("                       syCampGrps.CampGrpDescrip as CampGrpDescrip,  " + vbCrLf)
                .Append("                       (SELECT CampDescrip FROM syCampuses WHERE CampusId=arClassSections.CampusId) AS CampusDescrip, " + vbCrLf)
                .Append("                      (SELECT COUNT(*) FROM arClsSectMeetings WHERE ClsSectionId=A.TestId) AS MeetingsCount,arTerm.StartDate as TermStart,arStuEnrollments.StuEnrollId,      " + vbCrLf)
                .Append(" TimeIn = '' " + vbCrLf)
                '.Append(" TimeIn=(CASE WHEN  (arClsSectMeetings.TimeIntervalId IS NOT NULL) THEN (SELECT TimeIntervalDescrip FROM cmTimeInterval WHERE TimeIntervalId=arClsSectMeetings.TimeIntervalId)	" + vbCrLf)
                '.Append(" ELSE (SELECT TimeIntervalDescrip FROM syPeriods,cmTimeInterval WHERE PeriodId=arClsSectMeetings.PeriodId AND StartTimeId=TimeIntervalId)	END) " + vbCrLf)
                ' .Append(" ,cmTimeInterval.TimeIntervalDescrip " + vbCrLf)
                .Append(" FROM  " + vbCrLf)
                .Append("  		arResults  A INNER JOIN arStuEnrollments  ON A.StuEnrollId=arStuEnrollments.StuEnrollId  " + vbCrLf)
                .Append("  		INNER JOIN arStudent  ON arStuEnrollments.StudentId=arStudent.StudentId  " + vbCrLf)
                .Append("  		INNER JOIN arClassSections  ON A.TestId=arClassSections.ClsSectionId  " + vbCrLf)
                .Append("  		INNER JOIN arClassSectionTerms  ON arClassSections.ClsSectionId=arClassSectionTerms.ClsSectionId   " + vbCrLf)
                .Append("  		INNER JOIN arTerm  ON arClassSectionTerms.TermId=arTerm.TermId   " + vbCrLf)
                .Append("  		INNER JOIN arReqs  ON arClassSections.ReqId=arReqs.ReqId   " + vbCrLf)
                .Append(" 		INNER JOIN syCmpGrpCmps B ON arClassSections.CampusId=B.CampusId    " + vbCrLf)
                .Append(" 		INNER JOIN syCampGrps ON syCampGrps.CampGrpId = B.CampGrpId  " + vbCrLf)
                '.Append(" 		INNER JOIN arGradeSystemDetails C ON A.GrdSysDetailId=C.GrdSysDetailId " + vbCrLf)
                .Append(" 		LEFT OUTER JOIN arClsSectMeetings  ON A.TestId = arClsSectMeetings.ClsSectionId " + vbCrLf)
                '.Append(" 		LEFT OUTER JOIN syPeriods K ON J.PeriodId=K.PeriodId " + vbCrLf)
                '.Append(" 		LEFT OUTER JOIN cmTimeInterval L ON K.StartTimeId=L.TimeIntervalId " + vbCrLf)
                .Append(" WHERE             " + vbCrLf)
                '.Append(" 		((A.GrdSysDetailId IS NULL) OR (C.IsDrop=0))   " + vbCrLf)
                .Append(strWhere.Replace(";", ""))

                ' jagg Correct defect when you try to run report individual
                If paramInfo.ResId = 295 Then
                    If paramInfo.CampGrpId <> Nothing Then
                        .Append(" and syCampGrps.CampGrpId in(" + paramInfo.CampGrpId + ")")
                        .Append(" ORDER BY syCampGrps.CampGrpDescrip,CampusDescrip  " + vbCrLf)
                    Else
                        .Append(" ORDER BY CampusDescrip  " + vbCrLf)
                    End If
                Else
                    .Append(" ORDER BY syCampGrps.CampGrpDescrip,CampusDescrip  " + vbCrLf)
                End If
    
                .Append(strOrderBy)
            End If
        End With

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        Dim stuStartDate As String 
        Dim aRows() As DataRow
        If myAdvAppSettings.AppSettings("SchedulingMethod", paramInfo.CampusId) = "ModuleStart" Then
            For Each row As DataRow In ds.Tables(0).Rows
                If (IsDuplicateClassSection(row("TestId").ToString) = True) Then
                    stuStartDate = GetStudentStartDate(row("stuEnrollId").ToString)
                    aRows = ds.Tables(0).Select("TestId='" & row("TestId").ToString & "' AND TermStart=#" & CDate(stuStartDate) & "#")
                    If aRows.Length > 0 Then
                        If (stuStartDate <> CDate(row("TermStart")).ToShortDateString) Then
                            row.Delete()
                        End If
                    End If
                End If
            Next
            ds.AcceptChanges()
        End If

        'Mantis case #17762
        If paramInfo.ResId = "295" Then
           For Each row As DataRow In ds.Tables(0).Rows
                Dim aRowsWithTimeIn() As DataRow
                aRowsWithTimeIn = ds.Tables(0).Select("TestId='" & row("TestId").ToString & "'")
                If aRowsWithTimeIn.Length > 0 Then
                    For i = 1 To aRowsWithTimeIn.Length - 1
                        row.Delete()
                    Next
                End If
            Next
            ds.AcceptChanges()
        End If
        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "StuClassSchedule"
            'Add new columns
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("TestIdStr", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("SuppressDate", Type.GetType("System.String")))

            If ds.Tables(0).Rows.Count > 0 Then
                Dim clsSectList As String = GetClsSectionList(ds.Tables(0))
                Dim dt As  DataTable
                dt = GetClassMeetings(clsSectList).Copy
                ds.Tables.Add(dt)
                ds.Tables(1).TableName = "ClassMeetings"
            End If
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Private Function IsDuplicateClassSection(ByVal clsSectionId As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder

     db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        With sb
            .Append(" select ClsSectionId,Count(*) from arClassSectionTerms where ClsSectionId= ? ")
            .Append(" group by ClsSectionId having count(*) > 1 ")
        End With
        db.AddParameter("@clsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Dim ds As  DataSet
        ds = db.RunParamSQLDataSet(sb.ToString)
        If (ds.Tables(0).Rows.Count > 0) Then
            Return True
        End If
        Return False
    End Function
    Private Function GetStudentStartDate(ByVal stuEnrollID) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder

      db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT ExpStartDate FROM arStuEnrollments WHERE StuEnrollId= ? ")
        End With
        db.AddParameter("@StuEnrollId", stuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLScalar(sb.ToString)
    End Function

    Public Function GetClassMeetings(ByVal clsSectList As String) As DataTable
        Dim sb As New StringBuilder
        Dim ds As  DataSet
        Dim db As New DataAccess

     With sb
            .Append("SELECT ")
            .Append("       A.ClsSectionId,")
            .Append("       B.Descrip AS RoomDescrip,")
            .Append("       WorkDaysDescrip=(	CASE WHEN  (A.TimeIntervalId IS NOT NULL)")
            .Append("					        THEN (SELECT WorkDaysDescrip FROM plWorkDays WHERE WorkDaysId=A.WorkDaysId)")
            .Append("					        ELSE (SELECT PeriodDescrip FROM syPeriods WHERE PeriodId=A.PeriodId)")
            .Append("					        END),")
            .Append("       TimeIn=(	CASE WHEN  (A.TimeIntervalId IS NOT NULL)")
            .Append("					THEN (SELECT TimeIntervalDescrip FROM cmTimeInterval WHERE TimeIntervalId=A.TimeIntervalId)")
            .Append("					ELSE (SELECT TimeIntervalDescrip FROM syPeriods,cmTimeInterval WHERE PeriodId=A.PeriodId AND StartTimeId=TimeIntervalId)")
            .Append("					END),")
            .Append("       TimeOut=(	CASE WHEN  (A.TimeIntervalId IS NOT NULL)")
            .Append("					THEN (SELECT TimeIntervalDescrip FROM cmTimeInterval WHERE TimeIntervalId=A.EndIntervalId)")
            .Append("					ELSE (SELECT TimeIntervalDescrip FROM syPeriods,cmTimeInterval WHERE PeriodId=A.PeriodId AND EndTimeId=TimeIntervalId)")
            .Append("					END),")
            .Append("       MeetingType=(   CASE WHEN  (A.TimeIntervalId IS NOT NULL)")
            .Append("                       THEN (0)")
            .Append("                       ELSE (1) END),")
            .Append("       ViewOrder=(	CASE WHEN  (A.TimeIntervalId is not NULL)")
            .Append("					THEN (SELECT ViewOrder FROM plWorkDays WHERE WorkDaysId=A.WorkDaysId)")
            .Append("					ELSE (0)")
            .Append("					END) ,A.StartDate,A.EndDate ")
            .Append("FROM	arClsSectMeetings A,arRooms B ")
            .Append("WHERE  A.RoomId=B.RoomID")
            .Append("       AND A.ClsSectionId IN (" & clsSectList & ")")
            .Append("UNION ")
            .Append("SELECT ")
            .Append("       A.ClsSectionId,")
            .Append("       B.Descrip AS RoomDescrip,")
            .Append("       WorkDaysDescrip=(SELECT PeriodDescrip FROM syPeriods WHERE PeriodId=A.AltPeriodId),")
            .Append("       TimeIn=(SELECT TimeIntervalDescrip FROM syPeriods,cmTimeInterval WHERE PeriodId=A.AltPeriodId AND StartTimeId=TimeIntervalId),")
            .Append("       TimeOut=(SELECT TimeIntervalDescrip FROM syPeriods,cmTimeInterval WHERE PeriodId=A.AltPeriodId AND EndTimeId=TimeIntervalId),")
            .Append("       MeetingType=(1),")
            .Append("       ViewOrder=(	CASE WHEN  (A.TimeIntervalId is not NULL)")
            .Append("					THEN (SELECT ViewOrder FROM plWorkDays WHERE WorkDaysId=A.WorkDaysId)")
            .Append("					ELSE (0)")
            .Append("					END) ,A.StartDate,A.EndDate ")
            .Append("FROM	arClsSectMeetings A,arRooms B ")
            .Append("WHERE  A.RoomId=B.RoomID AND A.AltPeriodId IS NOT NULL")
            .Append("       AND A.ClsSectionId IN (" & clsSectList & ")")
            .Append("       and exists (SELECT * FROM syPeriods WHERE PeriodId=A.AltPeriodId) ")
            .Append("ORDER BY ViewOrder,StartDate,TimeIn;")
          
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            'Add new columns
            ds.Tables(0).Columns.Add(New DataColumn("ClsSectionStr", Type.GetType("System.String")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function

#End Region

#Region "Private Methods"

    Private Function GetClsSectionList(ByVal dt As DataTable) As String
        Dim clsSectList As String = ""
        For Each dr As DataRow In dt.Rows
            clsSectList &= "'" & dr("TestId").ToString & "',"
        Next
        If clsSectList <> "" Then
            'remove the last comma
            clsSectList = clsSectList.Substring(0, clsSectList.Length - 1)
        End If
        Return clsSectList
    End Function


#End Region

End Class
