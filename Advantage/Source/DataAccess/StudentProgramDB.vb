﻿Imports FAME.Advantage.Common
Public Class StudentProgramDB

#Region "Private Data Members"
    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")
#End Region

#Region "Public Properties"
    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property
#End Region
#Region "Public Methods"

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

    Public Function GetAllStudentByProgram(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim strWhere As String
        Dim strTempWhere As String = ""
        Dim db As New SQLDataAccess
        Dim ds As New DataSet
        Dim campGrpId As String = String.Empty
        Dim prgVerId As String = String.Empty
        Dim dtStudentByProgram As New DataTable
        Dim dtStudentByProgramNewStartAndReenrolls As New DataTable
        Dim dtStudentByProgramCountByStatus As New DataTable
        Dim dtStudentByProgramNewStartAndReenrollsTotal As New DataTable
        Dim dtStudentByProgramCountByStatusTotal As New DataTable
        Dim dtStudentByProgramCountBySourceTotal As New DataTable
        Dim dtStartDate As DateTime
        Dim myCulture As New System.Globalization.CultureInfo("en-US", True)
        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If


        If paramInfo.FilterOther <> "" Then
            'strWhere &= " AND " & paramInfo.FilterOther
            Dim idx As Integer = paramInfo.FilterOther.IndexOf("BETWEEN")
            If idx > -1 Then
                Dim str As String = paramInfo.FilterOther.Substring(idx + 9)
                Dim lidx As Integer = str.IndexOf("'")
                dtStartDate = Date.Parse(str.Substring(0, lidx), myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
                idx = str.IndexOf("AND") + 5
                lidx = str.LastIndexOf("'")
            End If
        End If

        strTempWhere = strWhere
        strTempWhere = strTempWhere.ToLower.Replace("and", ";")
        strWhere = ""
        Dim strArr As String() = strTempWhere.Remove(strTempWhere.IndexOf(";"), 1).Split(";")

        For i As Integer = 0 To strArr.Length - 1
            strWhere &= " and " & strArr(i)
        Next
        If strArr.Length = 1 Then
            strWhere &= " and "
        End If
        campGrpId = strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in (")).ToLower.IndexOf(" and ")).Replace("t1.campgrpid in", "").Replace(")", "").Replace("(", "").Replace("'", "")

        'get the prgverid
        If strWhere.ToLower.Contains("arprgversions.prgverid") Then
            If strWhere.ToLower.Contains("arprgversions.prgverid in ") Then
                If strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid in ")).ToLower.IndexOf(" and ") >= 0 Then
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid in (")).ToLower.IndexOf(" and ")).Replace("arprgversions.prgverid in", "").Replace(")", "").Replace("(", "").Replace("'", "")
                Else
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid in (")).Replace("arprgversions.prgverid in", "").Replace(")", "").Replace("(", "").Replace("'", "")
                End If
            Else
                If strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid = ")).ToLower.IndexOf(" and ") >= 0 Then
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid = "), strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid = ")).ToLower.IndexOf(" and ")).Replace("arprgversions.prgverid =", "").Replace(")", "").Replace("(", "").Replace("'", "")
                Else
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid = ")).Replace("arprgversions.prgverid =", "").Replace(")", "").Replace("(", "").Replace("'", "")
                End If
            End If
        End If
        'end the prgverid
        db.ClearParameters()
        db.AddParameter("@StartDate", dtStartDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@CampGrpId", campGrpId.Trim(), SqlDbType.VarChar, , ParameterDirection.Input)
        db.AddParameter("@ProgVerId", IIf(Not prgVerId = "", prgVerId.Trim(), DBNull.Value), SqlDbType.VarChar, , ParameterDirection.Input)
        db.AddParameter("@StudentIdentifier", StudentIdentifier, SqlDbType.VarChar, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet_SP("usp_AR_GetAllStudentByStartDateAndProgram", "StudentByProgram")

        dtStudentByProgram.TableName = "StudentByProgram"
        dtStudentByProgram = ds.Tables("StudentByProgram").Copy()
        ds.Tables.RemoveAt(0)

        db.ClearParameters()
        db.AddParameter("@StartDate", dtStartDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@CampGrpId", campGrpId.Trim(), SqlDbType.VarChar, , ParameterDirection.Input)
        db.AddParameter("@ProgVerId", IIf(Not prgVerId = "", prgVerId.Trim(), DBNull.Value), SqlDbType.VarChar, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet_SP("usp_AR_GetAllStudentByStartDateAndProgramCountNewStartAndReEnrolled", "StudentByProgramAndByNewStartAndReenrolls")

        dtStudentByProgramNewStartAndReenrolls.TableName = "StudentByProgramAndByNewStartAndReenrolls"
        dtStudentByProgramNewStartAndReenrolls = ds.Tables("StudentByProgramAndByNewStartAndReenrolls").Copy()
        ds.Tables.RemoveAt(0)

        db.ClearParameters()
        db.AddParameter("@StartDate", dtStartDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@CampGrpId", campGrpId.Trim(), SqlDbType.VarChar, , ParameterDirection.Input)
        db.AddParameter("@ProgVerId", IIf(Not prgVerId = "", prgVerId.Trim(), DBNull.Value), SqlDbType.VarChar, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet_SP("usp_AR_GetAllStudentByStartDateAndProgramCountByStatus", "StudentByProgramAndByStatus")

        dtStudentByProgramCountByStatus.TableName = "StudentByProgramAndByStatus"
        dtStudentByProgramCountByStatus = ds.Tables("StudentByProgramAndByStatus").Copy()
        ds.Tables.RemoveAt(0)

        db.ClearParameters()
        db.AddParameter("@StartDate", dtStartDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@CampGrpId", campGrpId.Trim(), SqlDbType.VarChar, , ParameterDirection.Input)
        db.AddParameter("@ProgVerId", IIf(Not prgVerId = "", prgVerId.Trim(), DBNull.Value), SqlDbType.VarChar, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet_SP("usp_AR_GetAllStudentByStartDateAndProgramCountNewStartAndReEnrolledTotal", "StudentByProgramAndByNewStartAndReenrollsTotal")

        dtStudentByProgramNewStartAndReenrollsTotal.TableName = "StudentByProgramAndByNewStartAndReenrollsTotal"
        dtStudentByProgramNewStartAndReenrollsTotal = ds.Tables("StudentByProgramAndByNewStartAndReenrollsTotal").Copy()
        ds.Tables.RemoveAt(0)

        db.ClearParameters()
        db.AddParameter("@StartDate", dtStartDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@CampGrpId", campGrpId.Trim(), SqlDbType.VarChar, , ParameterDirection.Input)
        db.AddParameter("@ProgVerId", IIf(Not prgVerId = "", prgVerId.Trim(), DBNull.Value), SqlDbType.VarChar, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet_SP("usp_AR_GetAllStudentByStartDateAndProgramCountByStatusTotal", "StudentByProgramAndByStatusTotal")

        dtStudentByProgramCountByStatusTotal.TableName = "StudentByProgramAndByStatusTotal"
        dtStudentByProgramCountByStatusTotal = ds.Tables("StudentByProgramAndByStatusTotal").Copy()
        ds.Tables.RemoveAt(0)

        db.ClearParameters()
        db.AddParameter("@StartDate", dtStartDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@CampGrpId", campGrpId.Trim(), SqlDbType.VarChar, , ParameterDirection.Input)
        db.AddParameter("@ProgVerId", IIf(Not prgVerId = "", prgVerId.Trim(), DBNull.Value), SqlDbType.VarChar, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet_SP("usp_AR_GetAllStudentByStartDateAndProgramBySource", "StudentByProgramAndBySourceTotal")

        dtStudentByProgramCountBySourceTotal.TableName = "StudentByProgramAndBySourceTotal"
        dtStudentByProgramCountBySourceTotal = ds.Tables("StudentByProgramAndBySourceTotal").Copy()
        ds.Tables.RemoveAt(0)

        ds.Tables.Add(dtStudentByProgram)
        ds.Tables.Add(dtStudentByProgramNewStartAndReenrolls)
        ds.Tables.Add(dtStudentByProgramCountByStatus)
        ds.Tables.Add(dtStudentByProgramNewStartAndReenrollsTotal)
        ds.Tables.Add(dtStudentByProgramCountByStatusTotal)
        ds.Tables.Add(dtStudentByProgramCountBySourceTotal)

        Return ds
    End Function
#End Region
End Class
