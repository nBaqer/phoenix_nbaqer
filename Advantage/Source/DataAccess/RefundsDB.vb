Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.Advantage.Common

Public Class RefundsDB
    Public Function GetAppliedPaymentsDS(ByVal stuEnrollId As String) As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   create dataset
        Dim ds As New DataSet

        '   build select query for the saAppliedPayments data adapter
        Dim sb As New StringBuilder
        With sb
            '.Append("--Applied Payments ")
            .Append("select  ")
            .Append("		(case when not exists(select * from saPmtDisbRel where TransactionId=T1.TransactionId) then 1 else 0 end) as Cash, ")
            .Append("		AP.AppliedPmtId, ")
            .Append("		AP.TransactionId, ")
            .Append("		AP.ApplyToTransId, ")
            .Append("		AP.Amount, ")
            .Append("		AP.ModDate, ")
            .Append("		AP.ModUser ")
            .Append("from	saAppliedPayments AP, saTransactions T1, saTransactions T2  ")
            .Append("where ")
            .Append("		AP.TransactionId=T1.TransactionId ")
            .Append("and	AP.ApplyToTransId=T2.TransactionId ")
            .Append("and	T1.StuEnrollId=T2.StuEnrollId ")
            .Append("and	T1.StuEnrollId = ? ")
        End With

        '   build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
        sc.Parameters.Add(New OleDbParameter("StuEnrollId", stuEnrollId))

        '   Create adapter to handle AppliedPayments table
        Dim da As New OleDbDataAdapter(sc)

        '   Fill Applied Payments table
        da.Fill(ds, "AppliedPayments")

        '   build select query for the FinancialAidPayments data adapter
        sb = New StringBuilder
        With sb
            '.Append("--Finacial Aid Payments ")
            .Append("select  distinct ")
            .Append("		AP.TransactionId, ")
            .Append("		PDR.Amount, ")
            .Append("		PDR.AwardScheduleId ")
            .Append("from	saAppliedPayments AP, saTransactions T1, saTransactions T2, saPmtDisbRel PDR  ")
            .Append("where ")
            .Append("		AP.TransactionId=T1.TransactionId ")
            .Append("and	AP.ApplyToTransId=T2.TransactionId ")
            .Append("and	T1.StuEnrollId=T2.StuEnrollId ")
            .Append("and	PDR.TransactionId=T1.TransactionId ")
            .Append("and	PDR.AwardScheduleId is not null ")
            .Append("and	T1.StuEnrollId = ? ")
        End With

        '   build select command
        sc = New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
        sc.Parameters.Add(New OleDbParameter("StuEnrollId", stuEnrollId))

        '   Create adapter to handle FinancialAidPayments table
        da = New OleDbDataAdapter(sc)

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   fill FinancialAidPayments table
        da.Fill(ds, "FinancialAidPayments")

        '   build select query for the CashPayments data adapter
        sb = New StringBuilder
        With sb
            '.Append("--Cash Payments ")
            .Append("select  distinct ")
            .Append("		AP.TransactionId, ")
            .Append("		T1.TransAmount ")
            .Append("from	saAppliedPayments AP, saTransactions T1, saTransactions T2  ")
            .Append("where ")
            .Append("		AP.TransactionId=T1.TransactionId ")
            .Append("and	AP.ApplyToTransId=T2.TransactionId ")
            .Append("and	T1.StuEnrollId=T2.StuEnrollId ")
            .Append("and	not exists(select * from saPmtDisbRel where TransactionId=T1.TransactionId) ")
            .Append("and	T1.StuEnrollId = ? ")
        End With

        '   build select command
        sc = New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
        sc.Parameters.Add(New OleDbParameter("StuEnrollId", stuEnrollId))

        '   Create adapter to handle CashPayments table
        da = New OleDbDataAdapter(sc)

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   fill CashPayments table
        da.Fill(ds, "CashPayments")

        '   build select query for the Charges data adapter
        sb = New StringBuilder
        With sb
            '.Append("--Charges ")
            .Append("select	distinct ")
            .Append("       T1.TransactionId, ")
            .Append("		T2.TransactionId as ApplyToTransId, ")
            .Append("		T2.TransAmount, ")
            .Append("       T2.TransReference ")
            .Append("from	saAppliedPayments AP, saTransactions T1, saTransactions T2  ")
            .Append("where ")
            .Append("		AP.TransactionId=T1.TransactionId ")
            .Append("and	AP.ApplyToTransId=T2.TransactionId ")
            .Append("and	T1.StuEnrollId=T2.StuEnrollId ")
            .Append("and	T1.StuEnrollId = ? ")
        End With

        '   build select command
        sc = New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
        sc.Parameters.Add(New OleDbParameter("StuEnrollId", stuEnrollId))

        '   Create adapter to handle Charges table
        da = New OleDbDataAdapter(sc)

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   fill Charges table
        da.Fill(ds, "Charges")

        '   build select query for the StudentAwardSchedule data adapter
        sb = New StringBuilder
        With sb
            '.Append("--StudentAwardSchedule ")
            .Append("select  distinct ")
            .Append("		SAS.AwardScheduleId, ")
            .Append("       (select FundSourceDescrip from saFundSources where FundSourceId = (select AwardTypeId from faStudentAwards where StudentAwardId=SAS.StudentAwardId)) as AwardTypeDescrip, ")
            .Append("       (select AwardStartDate from faStudentAwards where StudentAwardId=SAS.StudentAwardId) as AwardStartDate, ")
            .Append("		SAS.Amount ")
            .Append("from	saAppliedPayments AP, saTransactions T1, saTransactions T2, saPmtDisbRel PDR, faStudentAwardSchedule SAS  ")
            .Append("where ")
            .Append("		AP.TransactionId=T1.TransactionId ")
            .Append("and	AP.ApplyToTransId=T2.TransactionId ")
            .Append("and	T1.StuEnrollId=T2.StuEnrollId ")
            .Append("and	PDR.TransactionId=T1.TransactionId ")
            .Append("and	PDR.AwardScheduleId=SAS.AwardScheduleId ")
            .Append("and	T1.StuEnrollId = ? ")
        End With

        '   build select command
        sc = New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
        sc.Parameters.Add(New OleDbParameter("StuEnrollId", stuEnrollId))

        '   Create adapter to handle StudentAwardSchedule table
        da = New OleDbDataAdapter(sc)

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   fill StudentAwardSchedule table
        da.Fill(ds, "StudentAwardSchedules")

        '   build select query for the Refunds data adapter
        sb = New StringBuilder
        With sb
            '.Append("--Refunds ")
            .Append("select ")
            .Append("		R.TransactionId, ")
            .Append("		R.RefundTypeId, ")
            .Append("		R.ModUser, ")
            .Append("		R.ModDate ")
            .Append("from	saRefunds R ")
            .Append("where	1=2 ")
        End With

        '   build select command
        sc = New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

        '   Create adapter to handle Refunds table
        da = New OleDbDataAdapter(sc)

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   fill Refunds table
        da.Fill(ds, "Refunds")

        '   build select query for the Transactions data adapter
        sb = New StringBuilder
        With sb
            '.Append("--Transactions ")
            .Append("select ")
            .Append("		T.TransactionId, ")
            .Append("		T.StuEnrollId, ")
            .Append("		T.TransDate, ")
            .Append("		T.TransCodeId, ")
            .Append("		T.TransReference, ")
            .Append("		T.TransDescrip, ")
            .Append("		T.TransAmount, ")
            .Append("		T.ModUser, ")
            .Append("		T.ModDate ")
            .Append("from	saTransactions T ")
            .Append("where	1=2 ")
        End With

        '   build select command
        sc = New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

        '   Create adapter to handle Refunds table
        da = New OleDbDataAdapter(sc)

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   fill Transactions table
        da.Fill(ds, "Transactions")

        '   create primary and foreign key constraints

        '   set primary key for AppliedPayments table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("AppliedPayments").Columns("AppliedPmtId")
        ds.Tables("AppliedPayments").PrimaryKey = pk0

        '   set primary key for FinancialAidPayments table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("FinancialAidPayments").Columns("TransactionId")
        ds.Tables("FinancialAidPayments").PrimaryKey = pk1

        '   set primary key for CashPayments table
        Dim pk2(0) As DataColumn
        pk2(0) = ds.Tables("CashPayments").Columns("TransactionId")
        ds.Tables("CashPayments").PrimaryKey = pk2

        '   set primary key for Charges table
        Dim pk3(1) As DataColumn
        pk3(0) = ds.Tables("Charges").Columns("TransactionId")
        pk3(1) = ds.Tables("Charges").Columns("ApplyToTransId")
        ds.Tables("Charges").PrimaryKey = pk3

        '   set primary key for StudentAwardSchedule table
        Dim pk4(0) As DataColumn
        pk4(0) = ds.Tables("StudentAwardSchedules").Columns("AwardScheduleId")
        ds.Tables("StudentAwardSchedules").PrimaryKey = pk4

        '   set primary key for Refunds table
        Dim pk5(0) As DataColumn
        pk5(0) = ds.Tables("Refunds").Columns("TransactionId")
        ds.Tables("Refunds").PrimaryKey = pk5

        '   set primary key for Transactions table
        Dim pk6(0) As DataColumn
        pk6(0) = ds.Tables("Transactions").Columns("TransactionId")
        ds.Tables("Transactions").PrimaryKey = pk6

        '   set foreign key column in AppliedPayments
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("AppliedPayments").Columns("TransactionId")

        '   set foreign key column in AppliedPayments
        Dim fk1(1) As DataColumn
        fk1(0) = ds.Tables("AppliedPayments").Columns("TransactionId")
        fk1(1) = ds.Tables("AppliedPayments").Columns("ApplyToTransId")

        '   set foreign key column in FinancialAidPayments
        Dim fk2(0) As DataColumn
        fk2(0) = ds.Tables("FinancialAidPayments").Columns("AwardScheduleId")

        '   set relationship in AppliedPayments table
        ds.Relations.Add("ChargesAppliedPayments", pk3, fk1)

        '   set relationship in AppliedPayments table
        ds.Relations.Add("FinancialAidPaymentsAppliedPayments", pk1, fk0, False)

        '   set relationship in AppliedPayments table
        ds.Relations.Add("CashPaymentsAppliedPayments", pk2, fk0, False)

        '   set relationship in FinancialAidPayments table
        ds.Relations.Add("StudentAwardSchedulesFinancialAidPayments", pk4, fk2)

        '   return dataset
        Return ds

    End Function
    Public Function UpdateAppliedPaymentsDS(ByVal ds As DataSet) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   all updates must be encapsulated as one transaction
        Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            '   build select query for the AppliedPayments data adapter
            Dim sb As New StringBuilder
            With sb
                '.Append("--Applied Payments ")
                .Append("select  ")
                .Append("		AP.AppliedPmtId, ")
                .Append("		AP.TransactionId, ")
                .Append("		AP.ApplyToTransId, ")
                .Append("		AP.Amount, ")
                .Append("		AP.ModDate, ")
                .Append("		AP.ModUser ")
                .Append("from	saAppliedPayments AP ")
            End With

            '   build select command
            Dim AppliedPaymentsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle AppliedPayments table
            Dim AppliedPaymentsDataAdapter As New OleDbDataAdapter(AppliedPaymentsSelectCommand)

            '   build insert, update and delete commands for AppliedPayments table
            Dim cb As New OleDbCommandBuilder(AppliedPaymentsDataAdapter)

            '   build select query for the refunds data adapter
            sb = New StringBuilder
            With sb
                .Append("select ")
                .Append("		R.TransactionId, ")
                .Append("		R.RefundTypeId, ")
                .Append("		R.ModUser, ")
                .Append("		R.ModDate ")
                .Append("from	saRefunds R ")
            End With

            '   build select command
            Dim refundsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle refunds table
            Dim refundsDataAdapter As New OleDbDataAdapter(refundsSelectCommand)

            '   build insert, update and delete commands for refunds table
            Dim cb1 As New OleDbCommandBuilder(refundsDataAdapter)


            '   build select query for the refunds data adapter
            sb = New StringBuilder
            With sb
                .Append("select ")
                .Append("		T.TransactionId, ")
                .Append("		T.StuEnrollId, ")
                .Append("		T.TransDate, ")
                .Append("		T.TransCodeId, ")
                .Append("		T.TransReference, ")
                .Append("		T.TransDescrip, ")
                .Append("		T.TransAmount, ")
                .Append("		T.ModUser, ")
                .Append("		T.ModDate ")
                .Append("from	saTransactions T ")
            End With

            '   build select command
            Dim transactionsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle refunds table
            Dim transactionsDataAdapter As New OleDbDataAdapter(transactionsSelectCommand)

            '   build insert, update and delete commands for refunds table
            Dim cb2 As New OleDbCommandBuilder(transactionsDataAdapter)

            '   insert added rows in refunds table
            refundsDataAdapter.Update(ds.Tables("Refunds").Select(Nothing, Nothing, DataViewRowState.Added))

            '   insert added rows in transactions table
            refundsDataAdapter.Update(ds.Tables("Transactions").Select(Nothing, Nothing, DataViewRowState.Added))

            '   insert added rows in AppliedPayments table
            AppliedPaymentsDataAdapter.Update(ds.Tables("AppliedPayments").Select(Nothing, Nothing, DataViewRowState.Added))

            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            Return ""

        Catch ex As OleDb.OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally

            '   close connection
            connection.Close()
        End Try

    End Function


    ''Function added by saraswathi Lakshmanan on June 2010
    ''To show the TransCodes of type refunds
    Public Function GetTransCodesofTypeRefundsByCampus(ByVal campusId As String) As DataSet
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@ShowActive", "true", SqlDbType.VarChar, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, SqlDbType.VarChar, , ParameterDirection.Input)

        Try
            ds = db.RunParamSQLDataSet_SP("dbo.USP_AR_GetSystemTransCodesRefunds_GetList")
        Catch ex As System.Exception

        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return ds
    End Function



End Class
