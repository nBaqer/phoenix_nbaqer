Imports FAME.Advantage.Common

Public Class TestViewStateDB
    Public Sub InsertRowsIntoTitles(ByVal numRows As Integer)
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim iCounter As Integer
        Dim statusId As String
        Dim campGrpId As String
        Dim titleCode As String
        Dim titleDescrip As String
        Dim strSQL As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        iCounter = 0
        statusId = "F23DE1E2-D90A-4720-B4C7-0F6FB09C9965"
        campGrpId = "9C156B21-7A77-44CA-A013-39F708C875E3"
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        For iCounter = 0 To numRows
            titleCode = "Code" & iCounter.ToString
            titleDescrip = "Long Title Description " & iCounter.ToString

            strSQL = "INSERT INTO adTitles (TitleCode,StatusId,TitleDescrip,CampGrpId) " & _
                     "VALUES('" & titleCode & "','" & statusId & "','" & titleDescrip & "','" & campGrpId & "')"

            db.RunParamSQLExecuteNoneQuery(strSQL)
        Next

    End Sub


End Class
