Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.DataAccessLayer
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.DataAccess.AR

Public Class StuEnrollDB
    Public Function GetStudentName(ByVal StudentID As String) As String

        'connect to the database
        Dim db As New DataAccess
        Dim strFirstName, strLastName, strMiddleName As String

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("SELECT FirstName,LastName,MiddleName from arStudent where StudentId = ? ")

        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@StudentId", StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim Extracurricularinfo As New PlacementInfo

        While dr.Read()

            'set properties with data from DataReader
            strFirstName = dr("FirstName").ToString()
            strLastName = dr("LastName").ToString()
            strMiddleName = dr("MiddleName").ToString()

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return strFirstName & " " & strLastName & " " & strMiddleName
    End Function

    Public Function GetLeadName(ByVal LeadID As String) As String

        'connect to the database
        Dim db As New DataAccess
        Dim strFirstName, strLastName, strMiddleName As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("SELECT FirstName,LastName,MiddleName from adLeads where LeadId = ? ")

        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@LeadId", LeadID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        'Dim Extracurricularinfo As New PlacementInfo
        While dr.Read()

            'set properties with data from DataReader
            strFirstName = dr("FirstName").ToString()
            strLastName = dr("LastName").ToString()
            strMiddleName = dr("MiddleName").ToString()

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        If String.IsNullOrEmpty(strMiddleName) Then
            Return strFirstName & " " & strLastName
        Else
            Return strFirstName & " " & strLastName & " " & strMiddleName
        End If
    End Function

    Public Function GetAllProgVersions() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   PrgVerId, PrgVerDescrip ")
            .Append("FROM     arPrgVersions  ")
            .Append("ORDER BY PrgVerDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function

    Public Function GetAllProgVersionsByProgramId(ByVal ProgId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   Distinct PrgVerId, PrgVerDescrip ")
            .Append("FROM     arPrgVersions,syStatuses  ")
            .Append(" where arPrgVersions.StatusId = syStatuses.StatusId and syStatuses.Status='Active' and ProgId = ? ")
            .Append("ORDER BY PrgVerDescrip ")
        End With

        db.AddParameter("@ProgId", ProgId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function

    Public Function GetAllPrgVersions() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT DISTINCT ")
            .Append("           PrgVerId,")
            .Append("           PrgVerDescrip,")
            .Append(" case when (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=arPrgVersions.ProgId) is null  then ")
            .Append(" arPrgVersions.PrgVerDescrip ")
            .Append(" else ")
            .Append(" arPrgVersions.PrgVerDescrip + ' (' + (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=arPrgVersions.ProgId) + ')'  ")
            .Append(" end as PrgVerShiftDescrip,  ")
            .Append("           ProgId,")
            .Append("           (SELECT ProgDescrip FROM arPrograms WHERE ProgId=arPrgVersions.ProgId) AS Program ")
            .Append("FROM       arPrgVersions,syStatuses  ")
            .Append("WHERE      arPrgVersions.StatusId = syStatuses.StatusId and syStatuses.Status='Active'  ")
            .Append("ORDER BY   PrgVerDescrip ")
        End With

        'db.AddParameter("@ProgId", ProgId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function

    Public Function GetAllActiveInActivePrgVersions(ByVal CampusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT DISTINCT ")
            .Append("           PrgVerId,")
            .Append("           PrgVerDescrip,")
            .Append(" case when (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=arPrgVersions.ProgId) is null  then ")
            .Append(" arPrgVersions.PrgVerDescrip ")
            .Append(" else ")
            .Append(" arPrgVersions.PrgVerDescrip + ' (' + (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=arPrgVersions.ProgId) + ')'  ")
            .Append(" end as PrgVerShiftDescrip,  ")
            .Append("           ProgId,")
            .Append("           (SELECT ProgDescrip FROM arPrograms WHERE ProgId=arPrgVersions.ProgId) AS Program ")
            .Append("FROM       arPrgVersions,syStatuses  ")
            .Append("WHERE      arPrgVersions.StatusId = syStatuses.StatusId ")
            .Append("       AND arPrgVersions.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("ORDER BY   PrgVerDescrip ")
        End With

        db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@ProgId", ProgId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function

    Public Function GetCampGrpId(ByVal CampID As String) As String
        Dim db As New DataAccess
        Dim strSQL As String = "Select CampGrpId from syCmpGrpCmps where CampusId ='" + CampID + "'"
        Dim reader As OleDbDataReader = db.RunParamSQLDataReader(strSQL)
        Dim CampGrpId As String
        While reader.Read()

            CampGrpId &= "'" + reader("CampGrpId").ToString + "',"
        End While

        If Not reader.IsClosed Then reader.Close()

        Return CampGrpId.Substring(0, CampGrpId.Length - 1)
    End Function

    Public Function GetAcademicAdvisors(ByVal CampId As String, Optional ByVal advisorID As String = "") As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append(" SELECT DISTINCT t1.UserId,t1.FullName ")
            .Append(" FROM syUsers t1,syUsersRolesCampGrps t2,syRoles t3,syStatuses t4,sySysRoles t5  ")
            If advisorID = "" Then
                .Append(" WHERE t1.UserId=t2.UserId AND t2.RoleId = t3.RoleId AND t3.SysRoleId = t5.SysRoleId ")
            Else
                .Append(" WHERE (t1.UserId = ? OR (t2.RoleId = t3.RoleId AND t3.SysRoleId = t5.SysRoleId)) ")
            End If
            .Append(" AND	CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
            .Append(" AND t5.SysRoleId = 4 ")
            .Append(" AND t3.StatusId = t4.StatusId ")
            .Append(" ORDER BY FullName")
        End With
        If advisorID <> "" Then
            db.AddParameter("@advisorid", advisorID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.AddParameter("@CampusId", CampId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    'Public Function GetAcademicAdvisors(ByVal CampId As String, Optional ByVal advisorID As String = "") As DataSet

    '    '   connect to the database
    '    Try

    '        Dim CampGrpId As String = GetCampGrpId(CampId)
    '        Dim db As New DataAccess
    '        db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '        Dim sb As New StringBuilder

    '        '   build the sql query
    '        With sb
    '            .Append(" SELECT DISTINCT t1.UserId,t1.FullName ")
    '            .Append(" FROM syUsers t1,syUsersRolesCampGrps t2,syRoles t3,syStatuses t4,sySysRoles t5  ")
    '            If advisorID = "" Then
    '                .Append(" WHERE t1.UserId=t2.UserId AND t2.RoleId = t3.RoleId AND t3.SysRoleId = t5.SysRoleId ")
    '            Else
    '                .Append(" WHERE t1.UserId = ? OR (t2.RoleId = t3.RoleId AND t3.SysRoleId = t5.SysRoleId) ")
    '            End If
    '            '.Append(" AND	CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
    '            .Append(" AND	CampGrpId IN ( " + CampGrpId + " ) ")
    '            .Append(" AND t5.SysRoleId = 4 ")
    '            .Append(" AND t3.StatusId = t4.StatusId ")
    '            .Append(" ORDER BY FullName")
    '        End With

    '        If advisorID <> "" Then
    '            db.AddParameter("@advisorid", advisorID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If
    '        ' Add the CampusId to the parameter list
    '        'db.AddParameter("@CampusId", CampId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   return dataset

    '        Return db.RunParamSQLDataSet(sb.ToString)
    '    Catch ex As Exception
    '        Dim ss As String = ex.Message
    '    End Try
    'End Function

    Public Function GetFinancialAidAdvisorsPerCampus(ByVal campusId As String, Optional ByVal advisorID As String = "") As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" SELECT DISTINCT t1.UserId,t1.FullName ")
            .Append(" FROM syUsers t1,syUsersRolesCampGrps t2,syRoles t3,syStatuses t4,sySysRoles t5  ")
            If advisorID = "" Then
                .Append(" WHERE t1.UserId=t2.UserId AND t2.RoleId = t3.RoleId AND t3.SysRoleId = t5.SysRoleId ")
            Else
                .Append(" WHERE (t1.UserId = ? OR (t2.RoleId = t3.RoleId AND t3.SysRoleId = t5.SysRoleId)) ")
            End If
            .Append(" AND	CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
            .Append(" AND t5.SysRoleId = 7 ")
            .Append(" AND t3.StatusId = t4.StatusId ")
            .Append(" ORDER BY FullName")
        End With

        If advisorID <> "" Then
            db.AddParameter("@advisorid", advisorID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        'With sb
        '    .Append("SELECT ")
        '    .Append("       U.UserId as AcademicAdvisorId, ")
        '    .Append("       U.FullName as AcademicAdvisorName ")
        '    .Append("FROM ")
        '    .Append("       syUsersRolesCampGrps URCG, syRoles R, syUsers U ")
        '    .Append("WHERE ")
        '    .Append("       R.RoleId=URCG.RoleId ")
        '    .Append("AND	R.SysRoleId=7 ")
        '    .Append("AND	URCG.UserId=U.UserId ")
        '    .Append("AND	CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
        '    .Append("ORDER BY U.FullName ")
        'End With



        ' Add the CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function

    Public Function GetBillingMethodForChargingMethods(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")



        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" SELECT bm.BillingMethodId, bm.BillingMethod FROM dbo.saBillingMethods bm ")
            .Append(" INNER JOIN dbo.syCampGrps cg ON	cg.CampGrpId = bm.CampGrpId  ")
            .Append(" INNER JOIN dbo.syCmpGrpCmps cgc ON cgc.CampGrpId = cg.CampGrpId INNER JOIN dbo.syStatuses stat ON stat.StatusId = bm.StatusId  ")
            .Append(" WHERE cgc.CampusId = ? AND stat.StatusCode = 'A' ")

        End With

        ' Add the CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    Public Function GetAdmissionReps(ByVal CampId As String, Optional ByVal repID As String = "") As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'build the sql query
        With sb
            .Append(" SELECT DISTINCT t1.UserId,t1.FullName ")
            .Append(" FROM syUsers t1,syUsersRolesCampGrps t2,syRoles t3,syStatuses t4,sySysRoles t5  ")
            If repID = "" Then
                .Append(" WHERE t1.UserId=t2.UserId AND t2.RoleId = t3.RoleId ")
            Else
                .Append(" WHERE (t1.UserId = ? OR (t1.UserId=t2.UserId AND t2.RoleId = t3.RoleId)) ")
            End If
            .Append(" AND t3.SysRoleId = t5.SysRoleId ")
            .Append(" AND t5.SysRoleId = 3 ")
            .Append(" AND t3.StatusId = t4.StatusId ")
            .Append("AND	CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
            .Append(" ORDER BY FullName")
        End With
        If repID <> "" Then
            db.AddParameter("@RepId", repID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.AddParameter("@CampusId", CampId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    'Public Function GetAdmissionReps(ByVal CampId As String, Optional ByVal repID As String = "") As DataSet
    '    '   connect to the database
    '    Dim db As New DataAccess
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder
    '    Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
    '    'build the sql query
    '    With sb
    '        '.Append(" SELECT DISTINCT t1.UserId,t1.FullName ")
    '        '.Append(" FROM syUsers t1,syUsersRolesCampGrps t2,syRoles t3,syStatuses t4,sySysRoles t5  ")
    '        'If repID = "" Then
    '        '    .Append(" WHERE t1.UserId=t2.UserId AND t2.RoleId = t3.RoleId ")
    '        'Else
    '        '    .Append(" WHERE t1.UserId = ? OR (t1.UserId=t2.UserId AND t2.RoleId = t3.RoleId) ")
    '        'End If
    '        '.Append(" AND t3.SysRoleId = t5.SysRoleId ")
    '        '.Append(" AND t5.SysRoleId = 3 ")
    '        '.Append(" AND t3.StatusId = t4.StatusId ")
    '        ''following line is fix for issue 3465
    '        '.Append("AND	CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
    '        '.Append(" ORDER BY FullName")

    '        'Query Modified to get rid of Time out expired error in Build 1932
    '        .Append(" SELECT DISTINCT t1.UserId,t1.FullName  ")
    '        .Append(" FROM syUsers t1,syUsersRolesCampGrps t2,syRoles t3 ")
    '        .Append(" WHERE ")
    '        .Append(" t1.UserId = t2.UserId And t2.RoleId = t3.RoleId And t3.SysRoleId = 3 ")
    '        If repID <> "" Then
    '            .Append(" AND t1.UserId = ?  ")
    '        End If
    '        ' .Append(" AND t3.StatusId = ? ")
    '        .Append(" AND t2.CampGrpId IN (Select Distinct CampGrpId from syCmpGrpCmps where CampusId = ?) ")
    '        .Append(" Union ")
    '        .Append(" SELECT DISTINCT t1.UserId,t1.FullName  ")
    '        .Append(" FROM syUsers t1,syUsersRolesCampGrps t2,syRoles t3 ")
    '        .Append(" WHERE ")
    '        .Append(" t1.UserId = t2.UserId And t2.RoleId = t3.RoleId And t3.SysRoleId = 3 ")
    '        If repID <> "" Then
    '            .Append(" AND t1.UserId NOT IN (?)  ")
    '        End If
    '        ' .Append(" AND t3.StatusId = ? ")
    '        .Append(" AND t2.CampGrpId IN (Select Distinct CampGrpId from syCmpGrpCmps where CampusId = ?) ")
    '        .Append(" Order by t1.FullName ")
    '    End With

    '    If repID <> "" Then
    '        db.AddParameter("@RepId", repID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    End If
    '    'db.AddParameter("@StatusId", strActiveGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@CampusId", CampId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    If repID <> "" Then
    '        db.AddParameter("@RepId", repID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    End If
    '    'db.AddParameter("@StatusId", strActiveGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@CampusId", CampId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function

    Public Function GetEnrollStatuses() As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append(" SELECT Distinct  A.StatusCodeID as StatusCodeID,A.StatusCodeDescrip as StatusCodeDescrip FROM  ")
            .Append(" syStatusCodes A,sySysStatus B,syStatuses C,syStatusLevels D ")
            .Append(" where A.sysStatusID = B.sysStatusID And A.StatusID = C.StatusID and B.StatusLevelId = D.StatusLevelId ")
            .Append(" and C.Status = 'Active' and D.StatusLevelId = 2 ")
            .Append(" ORDER BY A.StatusCodeDescrip  ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function

    ''' <summary>
    ''' Get student Enrollments. can be filter by active inactive or by campus.
    ''' </summary>
    ''' <param name="studentId">Student Id </param>
    ''' <param name="showActiveOnly">Is true only the active are listed</param>
    ''' <param name="campusId">If it is specified list only of the specified campus.</param>
    ''' <returns>A data set wit the enrollment.</returns>
    ''' <remarks></remarks>
    Public Function GetStudentEnrollments(ByVal studentId As String, ByVal showStatus As AdvantageCommonValues.IncludeStatus, Optional ByVal campusId As String = Nothing) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try
            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            With sb
                .Append("SELECT t1.StuEnrollId,t1.PrgVerId, t2.PrgVerDescrip + ' (' + SC.StatusCodeDescrip + ')' AS PrgVerDescrip,ST.StatusId,ST.Status ")
                .Append(" ,t3.DescriptionProgramVersionType, SS.InSchool ")
                .Append("FROM arStuEnrollments t1, arPrgVersions t2,syStatuses ST, syStatusCodes SC, dbo.arProgramVersionType t3, dbo.sySysStatus SS ")
                .Append("WHERE t1.PrgVerId = t2.PrgVerId and t1.StudentId = ? and t2.StatusId = ST.StatusId and t1.StatusCodeId=SC.StatusCodeId ")
                .Append(" AND t3.ProgramVersionTypeId = t1.PrgVersionTypeId ")
                .Append(" AND SC.SysStatusId=SS.SysStatusId ")
                If Not campusId Is Nothing Then
                    .Append("AND t1.CampusId = ? ")
                End If

                Select Case showStatus
                    Case AdvantageCommonValues.IncludeStatus.ActiveOnly
                        .Append("AND    ST.Status = 'Active' ")
                    Case AdvantageCommonValues.IncludeStatus.InactiveOnly
                        .Append("AND    ST.Status = 'Inactive' ")
                End Select
                .Append("ORDER BY SS.InSchool desc, t1.StartDate desc, t1.ExpGradDate DESC ")

            End With
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            If Not campusId Is Nothing Then
                db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "StdEnrollments")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Sub InsertStudentEnrollment(ByVal StuEnrollmentID As String, ByVal StuEnrollObj As StudentEnrollmentInfo, ByVal user As String)
        Dim strSQL As New StringBuilder
        Dim myTrans As OleDbTransaction
        Dim mycmd As OleDbCommand

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim myconn As New OleDbConnection(myAdvAppSettings.AppSettings("ConString"))
        myconn.Open()

        myTrans = myconn.BeginTransaction()

        Try
            With strSQL
                If (StuEnrollObj.EnrollmentId = "") Then
                    .Append("INSERT INTO arStuEnrollments ")
                    .Append(" (StuEnrollId, StudentId, PrgVerId, AcademicAdvisor, AdmissionsRep, BillingMethodId, CampusId, EdLvlId, ShiftId, StatusCodeId, EnrollDate, ExpGradDate, ExpStartDate, LDA, MidPtDate, StartDate,TransferDate,DateDetermined,TuitionCategoryId,DropReasonId,FAAdvisorId,AttendTypeId,DegCertSeekingId,ModUser,ModDate,cohortStartDate,SAPId,ContractedGradDate,TransferHours,GraduatedOrReceivedDate,BadgeNumber,DistanceEdStatus, PrgVersionTypeId,  ")
                    .Append(" IsDisabled, EntranceInterviewDate, IsFirstTimeInSchool, IsFirstTimePostSecSchool, DisableAutoCharge, LeadId, ThirdPartyContract,LicensureLastPartWrittenOn ,LicensureWrittenAllParts ,LicensurePassedAllParts ) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")

                Else
                    .Append("INSERT INTO arStuEnrollments ")
                    .Append(" (StuEnrollId, StudentId, PrgVerId, AcademicAdvisor, AdmissionsRep, EnrollmentId, BillingMethodId, CampusId, EdLvlId, ShiftId, StatusCodeId, EnrollDate, ExpGradDate, ExpStartDate, LDA, MidPtDate, StartDate,TransferDate,DateDetermined,TuitionCategoryId,DropReasonId,FAAdvisorId,AttendTypeId,DegCertSeekingId,ModUser,ModDate,cohortStartDate,SAPId,ContractedGradDate,TransferHours,GraduatedOrReceivedDate,BadgeNumber,DistanceEdStatus, PrgVersionTypeId,  ")
                    .Append(" IsDisabled, EntranceInterviewDate, IsFirstTimeInSchool, IsFirstTimePostSecSchool, DisableAutoCharge, LeadId, ThirdPartyContract,LicensureLastPartWrittenOn
      ,LicensureWrittenAllParts
      ,LicensurePassedAllParts ) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
                End If
            End With

            Dim shadowLeadId = HttpContext.Current.Session("ShadowLeadId")
            If (String.IsNullOrWhiteSpace(shadowLeadId)) Then
                Dim dbMRU As New MRUDB
                shadowLeadId = dbMRU.GetShadowLead(StuEnrollObj.StudentID)
            End If
            StuEnrollObj.LeadId = shadowLeadId

            mycmd = New OleDbCommand(strSQL.ToString, myconn, myTrans)

            mycmd.Parameters.AddWithValue("@StuEnrollID", StuEnrollmentID)
            mycmd.Parameters.AddWithValue("@StudentID", StuEnrollObj.StudentID)
            mycmd.Parameters.AddWithValue("@PrgVerID", StuEnrollObj.ProgramVersionID)
            If StuEnrollObj.AcadamicAdvisor = "" Then
                mycmd.Parameters.AddWithValue("@EmpID", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@EmpID", StuEnrollObj.AcadamicAdvisor)
            End If
            If StuEnrollObj.AdmissionsRep = "" Then
                mycmd.Parameters.AddWithValue("@AdmRep", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@AdmRep", StuEnrollObj.AdmissionsRep)
            End If
            If (StuEnrollObj.EnrollmentId <> "") Then
                mycmd.Parameters.AddWithValue("@EnrollID", StuEnrollObj.EnrollmentId)
            End If
            If StuEnrollObj.BillingMethodId = "" Then
                mycmd.Parameters.AddWithValue("@Billingmthd", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@Billingmthd", StuEnrollObj.BillingMethodId)
            End If
            If StuEnrollObj.CampusId = "" Then
                mycmd.Parameters.AddWithValue("@campusid", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@campusid", StuEnrollObj.CampusId)
            End If
            If StuEnrollObj.GrdLvlId = "" Then
                mycmd.Parameters.AddWithValue("@grdlvlid", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@grdlvlid", StuEnrollObj.GrdLvlId)
            End If
            If StuEnrollObj.ShiftId = "" Then
                mycmd.Parameters.AddWithValue("@shiftid", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@shiftid", StuEnrollObj.ShiftId)
            End If
            If StuEnrollObj.StatusCodeId = "" Then
                mycmd.Parameters.AddWithValue("@statuscodeid", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@statuscodeid", StuEnrollObj.StatusCodeId)
            End If



            mycmd.Parameters.AddWithValue("@enrolldate", StuEnrollObj.EnrollDate)
            
            mycmd.Parameters.AddWithValue("@expgraddate", StuEnrollObj.ExpGradDate)
            mycmd.Parameters.AddWithValue("@expstartdate", StuEnrollObj.ExpStartDate)
            If StuEnrollObj.LDA = "" Then
                mycmd.Parameters.AddWithValue("@lda", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@lda", StuEnrollObj.LDA)
            End If
            If StuEnrollObj.MidPtDate = "" Then
                mycmd.Parameters.AddWithValue("@midptdate", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@midptdate", StuEnrollObj.MidPtDate)
            End If
            If StuEnrollObj.StartDate = "" Then
                mycmd.Parameters.AddWithValue("@startdate", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@startdate", StuEnrollObj.StartDate)
            End If
            If StuEnrollObj.TransferDate = "" Then
                mycmd.Parameters.AddWithValue("@transferdate", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@transferdate", StuEnrollObj.TransferDate)
            End If
            If StuEnrollObj.DateDetermined = "" Then
                mycmd.Parameters.AddWithValue("@datedetermined", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@datedetermined", StuEnrollObj.DateDetermined)
            End If
            If StuEnrollObj.TuitionCategory = Guid.Empty.ToString Or StuEnrollObj.TuitionCategory = "" Then
                mycmd.Parameters.AddWithValue("@TuitionCategory", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@TuitionCategory", StuEnrollObj.TuitionCategory)
            End If

            If StuEnrollObj.DropReasonId = Guid.Empty.ToString Or StuEnrollObj.DropReasonId = "" Then
                mycmd.Parameters.AddWithValue("@DropReason", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@DropReason", StuEnrollObj.DropReasonId)
            End If

            If StuEnrollObj.FaAdvisor() = "" Then
                mycmd.Parameters.AddWithValue("@faadvisorid", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@faadvisorid", StuEnrollObj.FaAdvisor())
            End If

            If StuEnrollObj.AttendTypeId = "" Then
                mycmd.Parameters.AddWithValue("@AttendTypeId", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@AttendTypeId", StuEnrollObj.AttendTypeId)
            End If

            If StuEnrollObj.DegCertSeekingId = "" Then
                mycmd.Parameters.AddWithValue("@DegCertSeekingId", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@DegCertSeekingId", StuEnrollObj.DegCertSeekingId)
            End If

            ''ModUser
            mycmd.Parameters.AddWithValue("@ModUser", user)

            ''ModDate
            mycmd.Parameters.AddWithValue("@ModDate", Date.Now)

            mycmd.Parameters.AddWithValue("@cohortStartDate", StuEnrollObj.CohortStartDate)

            If StuEnrollObj.SAPId = "" Or StuEnrollObj.SAPId = "00000000-0000-0000-0000-000000000000" Then
                mycmd.Parameters.AddWithValue("@SAPId", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@SAPId", StuEnrollObj.SAPId)
            End If
            mycmd.Parameters.AddWithValue("@contractedgraddate", StuEnrollObj.ContractedGradDate)

            mycmd.Parameters.AddWithValue("@TransferHours", StuEnrollObj.TransferHours)
            If StuEnrollObj.GraduatedOrReceivedDate = "" Then
                mycmd.Parameters.AddWithValue("@GraduatedOrReceivedDate", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@GraduatedOrReceivedDate", StuEnrollObj.GraduatedOrReceivedDate)
            End If

            If StuEnrollObj.BadgeNumber = "" Then
                mycmd.Parameters.AddWithValue("@BadgeNumber", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@BadgeNumber", StuEnrollObj.BadgeNumber)
            End If
            If StuEnrollObj.DistanceEdStatus = "" Then
                mycmd.Parameters.AddWithValue("@DistanceEdStatus", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@DistanceEdStatus", StuEnrollObj.DistanceEdStatus)
            End If

            mycmd.Parameters.AddWithValue("@PrgVersionTypeId", StuEnrollObj.ProgramVersionTypeId)

            'Add the new 4 fields .........
            If StuEnrollObj.IsDisabled = -1 Then
                mycmd.Parameters.AddWithValue("@IsDisabled", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@IsDisabled", StuEnrollObj.IsDisabled)
            End If

            If StuEnrollObj.EntranceInterviewDate Is Nothing Then
                mycmd.Parameters.AddWithValue("@EntranceInterviewDate", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@EntranceInterviewDate", StuEnrollObj.EntranceInterviewDate)
            End If
            
            mycmd.Parameters.AddWithValue("@IsFirstTimeInSchool", StuEnrollObj.IsFirstTimeInSchool)
            mycmd.Parameters.AddWithValue("@IsFirstTimePostSecSchool", StuEnrollObj.IsFirstTimePostSecSchool)
            mycmd.Parameters.AddWithValue("@DisableAutoCharge", StuEnrollObj.DisableAutoCharge)
            mycmd.Parameters.AddWithValue("@LeadId", StuEnrollObj.LeadId)
            mycmd.Parameters.AddWithValue("@ThirdPartyContract", StuEnrollObj.ThirdPartyContract)
            
            If stuEnrollObj.LicensureLastPartWrittenOn Is Nothing Then
                mycmd.Parameters.AddWithValue("@LicensureLastPartWrittenOn", DBNull.Value)
            Else
                mycmd.Parameters.AddWithValue("@LicensureLastPartWrittenOn", stuEnrollObj.LicensureLastPartWrittenOn)
            End If

            mycmd.Parameters.AddWithValue("@LicensureWrittenAllParts", stuEnrollObj.LicensureWrittenAllParts)
            mycmd.Parameters.AddWithValue("@LicensurePassedAllParts", StuEnrollObj.LicensurePassedAllParts)
            mycmd.ExecuteNonQuery()
            mycmd.Parameters.Clear()
            strSQL.Remove(0, strSQL.Length)

            '   charge Program Fees if applicable.
            Dim result As String = (New TransactionsDB).ApplyFeesByProgramVersion(StuEnrollmentID, user, StuEnrollObj.CampusId, Date.Today)

            'Insert syStudentStatusChanges for a new enrollment
            Dim sbStatusChange As New StringBuilder

            With sbStatusChange
                .Append("INSERT INTO syStudentStatusChanges ")
                .Append("(StudentStatusChangeId, StuEnrollId, OrigStatusId, NewStatusId, CampusId, ModDate, ModUser, IsReversal, DropReasonId, DateOfChange, Lda) ")
                .Append("VALUES (NEWID(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ")
            End With
            mycmd.CommandText = sbStatusChange.ToString
            mycmd.Parameters.AddWithValue("@StuEnrollId", StuEnrollmentID)

            mycmd.Parameters.AddWithValue("@OrigStatusId", DBNull.Value)
            mycmd.Parameters.AddWithValue("@NewStatusId", StuEnrollObj.StatusCodeId)
            mycmd.Parameters.AddWithValue("@CampusId", StuEnrollObj.CampusId)
            mycmd.Parameters.AddWithValue("@ModDate", Date.Now)
            mycmd.Parameters.AddWithValue("@ModUser", user)
            mycmd.Parameters.AddWithValue("@IsReversal", False)
            mycmd.Parameters.AddWithValue("@DropReasonId", DBNull.Value)
            mycmd.Parameters.AddWithValue("@DateOfChange", Date.Now)
            mycmd.Parameters.AddWithValue("@Lda", DBNull.Value)
            mycmd.ExecuteNonQuery()
            mycmd.Parameters.Clear()

            mycmd.Parameters.Clear()
            mycmd.CommandType = CommandType.StoredProcedure
            mycmd.CommandText = "USP_REGISTER_STUDENT_AUTOMATICALLY"
            mycmd.Parameters.AddWithValue("@ProgramVersionId", StuEnrollObj.ProgramVersionID)
            mycmd.Parameters.AddWithValue("@StudentEnrollmentId", StuEnrollmentID)
            mycmd.Parameters.AddWithValue("@ModifiedUser", user)
            myCmd.ExecuteNonQuery()

            myTrans.Commit()
            myconn.Close()
        Catch ex As OleDbException
            myTrans.Rollback()
            Throw ex 

        Finally
            'Close Connection
            myconn.Close()
        End Try

    End Sub

    ''' <summary>
    ''' check if the studentID format is generated by badgeId then upadte the student number in the adleads with the badgeId 
    ''' </summary>
    ''' <param name="stuEnrollmentId"></param>
    ''' <param name="badgeNum"></param>
    Public sub updateLeadBadgeNumber(ByVal stuEnrollId As String,ByVal badgeNum As String) 
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        Dim formatType As Integer
        Dim myAdvAppSettings As AdvAppSettings = New AdvAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        'checking teh studentID format
        With strSQL
            .Append("SELECT top 1 FormatType FROM dbo.syStudentFormat")
        End With
        formatType = CInt(db.RunParamSQLScalar(strSQL.ToString))

        If formatType = 4 Then
            'update the student number with the badge Id columns in adleads
            strSQL.Remove(0, strSQL.Length)
            with strSQL
                .Append("UPDATE adleads SET StudentNumber = ?  WHERE LeadId = (select LeadId from arStuEnrollments where StuEnrollId = ?)")
                db.AddParameter("@StudentNumber", badgeNum, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End With
            Try
                db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
            Finally
                db.CloseConnection()
            End Try

            db.ClearParameters()
            strSQL.Remove(0, strSQL.Length)
        End If
    End sub

    ''' <summary>
    ''' Please change or not used.
    ''' </summary>
    ''' <param name="StuEnrollmentID"></param>
    ''' <param name="StuEnrollObj"></param>
    ''' <param name="user"></param>
    ''' <remarks>This procedure must be upgrade because now does not change the student status history!!!!!! This unsynchronized the Student Enrollment Page !!!!!!!! </remarks>
    Public Sub UpdateStudentEnrollment(ByVal stuEnrollmentID As String, ByVal stuEnrollObj As StudentEnrollmentInfo, ByVal user As String)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder

        Dim myAdvAppSettings As AdvAppSettings = New AdvAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")


        With strSQL
            .Append("UPDATE arStuEnrollments ")
            .Append("SET StudentId = ?, PrgVerId = ?, AcademicAdvisor = ? , AdmissionsRep = ?, EnrollmentId = ?, BillingMethodId = ? , CampusId = ?, EdLvlId = ?, ShiftId = ?, StatusCodeId = ?, EnrollDate = ?, ExpGradDate = ?, ExpStartDate = ?, LDA = ? , MidPtDate = ?, StartDate = ?,TransferDate = ?,DateDetermined = ?, FAAdvisorId = ?,TuitionCategoryId = ?,DropReasonId = ?,AttendTypeId = ?,DegCertSeekingId = ?, ModUser = ?, ModDate = ?, ReEnrollmentDate = ? , CohortStartDate = ? , SAPId = ?,ContractedGradDate = ? ")
            .Append(" , TransferHours=?,GraduatedorReceivedDate=?,BadgeNumber=? ")
            .Append(", DistanceEdStatus=?, PrgVersionTypeId=?, IsDisabled = ?, EntranceInterviewDate=?, IsFirstTimeInSchool=?, IsFirstTimePostSecSchool=?, DisableAutoCharge=?, ThirdPartyContract=?  , LicensureLastPartWrittenOn=?, LicensureWrittenAllParts=?, LicensurePassedAllParts=? ")
            .Append("WHERE StuEnrollId= ?")
            'End If

        End With
        db.AddParameter("@StudentID", stuEnrollObj.StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerID", stuEnrollObj.ProgramVersionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If stuEnrollObj.AcadamicAdvisor = "" Then
            db.AddParameter("@EmpID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@EmpID", stuEnrollObj.AcadamicAdvisor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If stuEnrollObj.AdmissionsRep = "" Then
            db.AddParameter("@AdmRep", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@AdmRep", stuEnrollObj.AdmissionsRep, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.AddParameter("@EnrollID", stuEnrollObj.EnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If stuEnrollObj.BillingMethodId = "" Then
            db.AddParameter("@Billingmthd", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@Billingmthd", stuEnrollObj.BillingMethodId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If stuEnrollObj.CampusId = "" Then
            db.AddParameter("@campusid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@campusid", stuEnrollObj.CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If stuEnrollObj.GrdLvlId = "" Then
            db.AddParameter("@grdlvlid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@grdlvlid", stuEnrollObj.GrdLvlId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If stuEnrollObj.ShiftId = "" Then
            db.AddParameter("@shiftid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@shiftid", stuEnrollObj.ShiftId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If stuEnrollObj.StatusCodeId = "" Then
            db.AddParameter("@statuscodeid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@statuscodeid", stuEnrollObj.StatusCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.AddParameter("@EnrollDate", stuEnrollObj.EnrollDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ExpGradDate", stuEnrollObj.ExpGradDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ExpStartDate", stuEnrollObj.ExpStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If stuEnrollObj.LDA = "" Then
            db.AddParameter("@lda", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@lda", stuEnrollObj.LDA, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        'db.AddParameter("@lda", StuEnrollObj.LDA, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If stuEnrollObj.MidPtDate = "" Then
            db.AddParameter("@midptdate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@midptdate", stuEnrollObj.MidPtDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If stuEnrollObj.StartDate = "" Then
            db.AddParameter("@startdate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@startdate", stuEnrollObj.StartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If        'db.AddParameter("@TransDate", StuEnrollObj.TransferDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If stuEnrollObj.TransferDate = "" Then
            db.AddParameter("@transferdate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@transferdate", stuEnrollObj.TransferDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If stuEnrollObj.DateDetermined = "" Then
            db.AddParameter("@datedetermined", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@datedetermined", stuEnrollObj.DateDetermined, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        If stuEnrollObj.FaAdvisor() = "" Then
            db.AddParameter("@faadvisorid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@faadvisorid", stuEnrollObj.FaAdvisor(), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If stuEnrollObj.TuitionCategory = Guid.Empty.ToString Or stuEnrollObj.TuitionCategory = "" Then
            db.AddParameter("@TuitionCategory", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@TuitionCategory", stuEnrollObj.TuitionCategory, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        If stuEnrollObj.DropReasonId = Guid.Empty.ToString Or stuEnrollObj.DropReasonId = "" Then
            db.AddParameter("@DropReason", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@DropReason", stuEnrollObj.DropReasonId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        If stuEnrollObj.AttendTypeId = Guid.Empty.ToString Or stuEnrollObj.AttendTypeId = "" Then
            db.AddParameter("@AttendTypeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@AttendTypeId", stuEnrollObj.AttendTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        If stuEnrollObj.DegCertSeekingId = Guid.Empty.ToString Or stuEnrollObj.DegCertSeekingId = "" Then
            db.AddParameter("@DegCertSeekingId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@DegCertSeekingId", stuEnrollObj.DegCertSeekingId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        ''ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ''ModDate
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        'ReEnroll Date

        If stuEnrollObj.ReenrollDate = "" Then
            db.AddParameter("@ReenrollDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@ReenrollDate", stuEnrollObj.ReenrollDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.AddParameter("@CohortStartDate", stuEnrollObj.CohortStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If stuEnrollObj.SAPId = "" Or stuEnrollObj.SAPId = "00000000-0000-0000-0000-000000000000" Then
            db.AddParameter("@SAPId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@SAPId", stuEnrollObj.SAPId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.AddParameter("@ContractedGradDate", stuEnrollObj.ContractedGradDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@TransferHours", stuEnrollObj.TransferHours, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
        If stuEnrollObj.GraduatedOrReceivedDate = "" Then
            db.AddParameter("@GraduatedOrReceivedDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@GraduatedOrReceivedDate", stuEnrollObj.GraduatedOrReceivedDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        If stuEnrollObj.BadgeNumber = "" Then
            db.AddParameter("@BadgeNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@BadgeNumber", stuEnrollObj.BadgeNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If stuEnrollObj.DistanceEdStatus = "" Then
            db.AddParameter("@DistanceEdStatus", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@DistanceEdStatus", stuEnrollObj.DistanceEdStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.AddParameter("@PrgVersionTypeId", stuEnrollObj.ProgramVersionTypeId, DataAccess.OleDbDataType.OleDbInteger, ParameterDirection.Input)

        'Add the new 4 fields .........
        If stuEnrollObj.IsDisabled = -1 Then
            db.AddParameter("@IsDisabled", DBNull.Value)
        Else
            db.AddParameter("@IsDisabled", stuEnrollObj.IsDisabled)
        End If

        If stuEnrollObj.EntranceInterviewDate Is Nothing Then
            db.AddParameter("@EntranceInterviewDate", DBNull.Value)
        Else
            db.AddParameter("@EntranceInterviewDate", stuEnrollObj.EntranceInterviewDate)
        End If

        db.AddParameter("@IsFirstTimeInSchool", stuEnrollObj.IsFirstTimeInSchool)
        db.AddParameter("@IsFirstTimePostSecSchool", stuEnrollObj.IsFirstTimePostSecSchool)

        db.AddParameter("@DisableAutoCharge", stuEnrollObj.DisableAutoCharge)
        db.AddParameter("@ThirdPartyContract", StuEnrollObj.ThirdPartyContract)

        If stuEnrollObj.LicensureLastPartWrittenOn Is Nothing Then
            db.AddParameter("@LicensureLastPartWrittenOn", DBNull.Value)
        Else
            db.AddParameter("@LicensureLastPartWrittenOn", stuEnrollObj.LicensureLastPartWrittenOn)
        End If

        db.AddParameter("@LicensureWrittenAllParts", stuEnrollObj.LicensureWrittenAllParts)
        db.AddParameter("@LicensurePassedAllParts", StuEnrollObj.LicensurePassedAllParts)


        db.AddParameter("@StuEnrollmentID", stuEnrollmentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Finally
            db.CloseConnection()
        End Try

        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Sub
    Public Function OKToAddEnrollmentWithSameProgramVersion(ByVal stuEnrollId As String, ByVal PrgVerId As String) As Boolean
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        Dim numFound As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


        With strSQL
            .Append("SELECT COUNT(*) ")
            .Append("FROM arStuEnrollments t1, syStatusCodes t3 ")
            .Append("WHERE StudentId = (SELECT StudentId ")
            .Append("                   FROM arStuEnrollments t2 ")
            .Append("                   WHERE t2.StuEnrollId = ?) ")
            .Append("AND PrgVerId = ? ")
            .Append("AND t1.StatusCodeId = t3.StatusCodeId ")
            .Append("AND t3.SysStatusId IN(7,9,10,11,13,14) ")
        End With

        db.AddParameter("stuenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("prgverid", PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        numFound = CInt(db.RunParamSQLScalar(strSQL.ToString))

        If numFound = 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function GetStudentByEnrollID(ByVal stuEnrollID As String) As StudentEnrollmentInfo

        'connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        ''Transfer Hours added to the query
        With sb
            'With subqueries
            .Append(" select CCT.PrgVerId, ")
            .Append(" CCT.AcademicAdvisor,  ")
            .Append(" CCT.AdmissionsRep, CCT.EnrollmentId, ")
            .Append(" CCT.BillingMethodId, CCT.EnrollDate, CCT.LDA, CCT.ExpStartDate, CCT.TransferDate, ")
            .Append(" CCT.StatusCodeId, CCT.MidPtDate, CCT.ExpGradDate, CCT.ShiftId, CCT.StartDate, CCT.EdLvlId,  ")
            .Append(" CCT.CampusId,CCT.TuitionCategoryId,CCT.DropReasonId,CCT.FAAdvisorId,CCT.AttendTypeId,CCT.DegCertSeekingId, ")
            .Append(" CCT.DateDetermined, CCT.ModUser, CCT.ModDate, CCT.PrgVersionTypeId, ")
            .Append("    (Select PrgVerDescrip from arPrgVersions where PrgVerId=CCT.PrgVerId) As PrgVerDescrip, ")
            .Append("    (Select BillingMethodDescrip from saBillingMethods where BillingMethodId=CCT.BillingMethodId) As BillingMethodDescrip, ")
            .Append("    (Select TuitionCategoryDescrip from saTuitionCategories where TuitionCategoryId=CCT.TuitionCategoryId) As TuitionCategoryDescrip, ")
            .Append("    (Select ShiftDescrip from arShifts where ShiftId=CCT.ShiftId) As ShiftDescrip, ")
            .Append("    (Select EdLvlDescrip from adEdLvls where EdLvlId=CCT.EdLvlId) As EdLvlDescrip, ")
            .Append("    (Select Descrip from arDropReasons where DropReasonId=CCT.DropReasonId) As DropReason, ")
            .Append("    (Select StatusCodeDescrip from syStatusCodes where StatusCodeId=CCT.StatusCodeId) As StatusCodeDescrip, ")
            .Append("    (Select CampDescrip from syCampuses where CampusId=CCT.CampusId) As CampDescrip, ")
            .Append("    (Select Descrip from arAttendTypes where AttendTypeId=CCT.AttendTypeId) As AttType, ")
            .Append("    (Select Descrip from adDegCertSeeking where DegCertSeekingId=CCT.DegCertSeekingId) As DegCertSeeking,CCT.ReEnrollmentDate,CCT.CohortStartDate,CCT.SAPId,CCT.ContractedGradDate  ")
            ''Transfer Hours added to the query
            .Append(" ,CCT.TransferHours, CCT.GraduatedOrReceivedDate,CCT.BadgeNumber,  ")
            .Append(" (SELECT UseTimeClock FROM arPrgVersions WHERE PrgVerId=CCT.PrgVerId) AS UseTimeClock  ")
            .Append(" ,(SELECT CASE WHEN 1 in (SELECT 1 FROM dbo.arTrackTransfer TT WHERE TT.StuEnrollId = CCT.TransferHoursFromThisSchoolEnrollmentId) THEN 1 ELSE 0 END) AS TracksTransfer")
            .Append(" ,CCT.DistanceEdStatus ")
            .Append(" ,IsDisabled, EntranceInterviewDate, IsFirstTimeInSchool, IsFirstTimePostSecSchool, DisableAutoCharge, ThirdPartyContract, TotalTransferHoursFromThisSchool, TransferHoursFromThisSchoolEnrollmentId  ,LicensureLastPartWrittenOn    ,LicensureWrittenAllParts   ,LicensurePassedAllParts	")
            .Append(" FROM arStuEnrollments CCT where CCT.StuEnrollID=? ")
        End With
        'Add the EmployerContactId the parameter list
        '    db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollID", stuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim stuEnrollInfo As New StudentEnrollmentInfo

        While dr.Read()
            With stuEnrollInfo
                '.StudentId = dr("StudentId")
                .ProgramVersionID = dr("PrgVerId").ToString
                .ProgramVersionTypeId = dr("PrgVersionTypeId")
                .AcadamicAdvisor = dr("AcademicAdvisor").ToString
                .AdmissionsRep = dr("AdmissionsRep").ToString
                ' .EnrollmentId = dr("EnrollmentId")
                If Not (dr("EnrollmentId") Is DBNull.Value) Then
                    .EnrollmentId = dr("EnrollmentId").ToString()
                Else
                    .EnrollmentId = ""
                End If
                .BillingMethodId = dr("BillingMethodId").ToString
                .CampusId = dr("CampusId").ToString
                .TuitionCategory = dr("TuitionCategoryId").ToString
                If Not (dr("EnrollDate") Is DBNull.Value) Then
                    .EnrollDate = dr("EnrollDate")
                Else
                    .EnrollDate = ""
                End If
                If Not (dr("ExpGradDate") Is DBNull.Value) Then
                    .ExpGradDate = dr("ExpGradDate")
                Else
                    .ExpGradDate = ""
                End If
                If Not (dr("ExpStartDate") Is DBNull.Value) Then
                    .ExpStartDate = dr("ExpStartDate")
                Else
                    .ExpStartDate = ""
                End If
                If Not (dr("ContractedGradDate") Is DBNull.Value) Then
                    .ContractedGradDate = dr("ContractedGradDate")
                Else
                    .ContractedGradDate = ""
                End If
                If Not (dr("MidPtDate") Is DBNull.Value) Then
                    .MidPtDate = dr("MidPtDate")
                Else
                    .MidPtDate = ""
                End If
                If Not (dr("StartDate") Is DBNull.Value) Then
                    .StartDate = dr("StartDate")
                Else
                    .StartDate = ""
                End If
                .ShiftId = dr("ShiftId").ToString
                .GrdLvlId = dr("EdLvlId").ToString
                If Not (dr("LDA") Is DBNull.Value) Then
                    .LDA = dr("LDA").ToShortDateString()
                Else
                    .LDA = ""
                End If
                .StatusCodeId = dr("StatusCodeId").ToString
                .DropReasonId = dr("DropReasonId").ToString
                If Not (dr("AttendTypeId") Is DBNull.Value) Then
                    .AttendTypeId = dr("AttendTypeId").ToString
                Else
                    .AttendTypeId = ""
                End If
                If Not (dr("DegCertSeekingId") Is DBNull.Value) Then
                    .DegCertSeekingId = dr("DegCertSeekingId").ToString
                Else
                    .DegCertSeekingId = ""
                End If
                .DegCertSeekingId = dr("DegCertSeekingId").ToString
                If Not (dr("TransferDate") Is DBNull.Value) Then
                    .TransferDate = dr("TransferDate")
                Else
                    .TransferDate = ""
                End If
                If Not (dr("DateDetermined") Is DBNull.Value) Then
                    .DateDetermined = dr("DateDetermined")
                Else
                    .DateDetermined = ""
                End If
                If Not (dr("FAAdvisorId") Is DBNull.Value) Then .FaAdvisor() = dr("FAAdvisorId").ToString Else .FaAdvisor() = ""
                'get ModUser
                If Not (dr("ModDate") Is DBNull.Value) Then .ModDate = dr("ModDate") Else .ModDate = Date.MinValue

                If Not (dr("ModUser") Is DBNull.Value) Then .ModUser = dr("ModUser").ToString Else .ModUser = ""
                If Not (dr("PrgVerDescrip") Is DBNull.Value) Then .ProgramVersion = dr("PrgVerDescrip")
                If Not (dr("BillingMethodDescrip") Is DBNull.Value) Then .BillingMethod = dr("BillingMethodDescrip")
                If Not (dr("TuitionCategoryDescrip") Is DBNull.Value) Then .TuitionCatText = dr("TuitionCategoryDescrip")
                If Not (dr("ShiftDescrip") Is DBNull.Value) Then .Shift = dr("ShiftDescrip")
                If Not (dr("EdLvlDescrip") Is DBNull.Value) Then .GradeLevel = dr("EdLvlDescrip")
                If Not (dr("DropReason") Is DBNull.Value) Then .DropReason = dr("DropReason")
                If Not (dr("StatusCodeDescrip") Is DBNull.Value) Then .StatusCode = dr("StatusCodeDescrip")
                If Not (dr("CampDescrip") Is DBNull.Value) Then .Campus = dr("CampDescrip")
                If Not (dr("AttType") Is DBNull.Value) Then .AttendType = dr("AttType")
                If Not (dr("DegCertSeeking") Is DBNull.Value) Then .DegCertSeekingText = dr("DegCertSeeking")
                If Not (dr("ReEnrollmentDate") Is DBNull.Value) Then .ReenrollDate = dr("ReEnrollmentDate")
                If Not (dr("CohortStartDate") Is DBNull.Value) Then .CohortStartDate = dr("CohortStartDate")
                If Not (dr("SAPId") Is DBNull.Value) Then .SAPId = dr("SAPId").ToString

                ''Transfer Hours added to the query
                If Not (dr("TransferHours") Is DBNull.Value) Then
                    .TransferHours = dr("TransferHours")
                Else
                    .TransferHours = 0
                End If
                If Not (dr("GraduatedOrReceivedDate") Is DBNull.Value) Then
                    .GraduatedOrReceivedDate = dr("GraduatedOrReceivedDate")
                Else
                    .GraduatedOrReceivedDate = ""
                End If
                If Not (dr("BadgeNumber") Is DBNull.Value) Then
                    .BadgeNumber = dr("BadgeNumber")
                Else
                    .BadgeNumber = ""
                End If
                Try
                    If (IsDBNull(dr("UseTimeClock")) OrElse dr("UseTimeClock") = 0) Then
                        .UseTimeClock = False
                    Else
                        .UseTimeClock = True
                    End If
                Catch ex As Exception
                    .UseTimeClock = False
                End Try
                'US3153
                .DistanceEdStatus = dr("DistanceEdStatus").ToString
                .StuEnrollmentId = stuEnrollID

                'Is Disabled
                Dim isdi = dr("IsDisabled")
                If (isdi Is DBNull.Value) Then
                    .IsDisabled = -1
                Else
                    .IsDisabled = If(isdi, 1, 0)
                End If

                'EntranceInterviewDate
                Dim eid = dr("EntranceInterviewDate")
                If Not (eid Is DBNull.Value) Then .EntranceInterviewDate = eid Else .EntranceInterviewDate = Nothing

                'IsFirstTimeInSchool
                .IsFirstTimeInSchool = dr("IsFirstTimeInSchool")

                'IsFirstTimePostSecSchool
                .IsFirstTimePostSecSchool = dr("IsFirstTimePostSecSchool")

                .DisableAutoCharge = dr("DisableAutoCharge")

                .ThirdPartyContract = dr("ThirdPartyContract")

                .TotalTransferHoursFromThisSchool = dr("TotalTransferHoursFromThisSchool")

                .TracksTransfer = dr("TracksTransfer")

                .LicensureLastPartWrittenOn = if ( not (dr("LicensureLastPartWrittenOn") Is dbnull.value), dr("LicensureLastPartWrittenOn") , nothing)
                .LicensureWrittenAllParts = dr("LicensureWrittenAllParts")
        .LicensurePassedAllParts	= dr("LicensurePassedAllParts")

                Dim transferHoursFromThisSchoolProgramId = dr("TransferHoursFromThisSchoolEnrollmentId")
                .TransferHoursFromProgramId = If (Not (transferHoursFromThisSchoolProgramId Is dbnull.value), transferHoursFromThisSchoolProgramId.ToString(), Guid.Empty.ToString())
            End With
        End While

        If Not dr.IsClosed Then dr.Close()

        db.ClearParameters()
        sb.Remove(0, sb.Length)
        With sb.Append("SELECT ScheduleId FROM dbo.arStudentSchedules WHERE StuEnrollId = ?")
        End With

        db.AddParameter("@StuEnrollId", stuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'execute the query
        Dim reader = db.RunParamSQLDataReader(sb.ToString)
        If reader.HasRows Then

            While reader.Read()
                stuEnrollInfo.ScheduleId = reader("ScheduleId").ToString()
            End While
        End If

        If Not reader.IsClosed Then reader.Close()

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return stuEnrollInfo
    End Function

    Public Function GetStudentByEnrollID_UI(ByVal StuEnrollID As String) As StudentEnrollmentInfo

        'connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder

        ''Transfer Hours added to the query
        With sb
            'With subqueries
            .Append(" select CCT.PrgVerId, ")
            .Append(" CCT.AcademicAdvisor,  ")
            .Append(" CCT.AdmissionsRep, CCT.EnrollmentId, ")
            .Append(" CCT.BillingMethodId, CCT.EnrollDate, CCT.LDA, CCT.ExpStartDate, CCT.TransferDate, ")
            .Append(" CCT.StatusCodeId, CCT.MidPtDate, CCT.ExpGradDate, CCT.ShiftId, CCT.StartDate, CCT.EdLvlId,  ")
            .Append(" CCT.CampusId,CCT.TuitionCategoryId,CCT.DropReasonId,CCT.FAAdvisorId,CCT.AttendTypeId,CCT.DegCertSeekingId, ")
            .Append(" CCT.DateDetermined, CCT.ModUser, CCT.ModDate, ")
            .Append("    (SELECT ProgDescrip	FROM dbo.arPrograms WHERE ProgId = (SELECT ProgId FROM dbo.arPrgVersions WHERE PrgVerId = CCT.PrgVerId)) AS Program, ")
            .Append("    (Select PrgVerDescrip from arPrgVersions where PrgVerId=CCT.PrgVerId) As PrgVerDescrip, ")
            .Append("    (Select ProgDescrip from arPrograms where ProgId = (Select ProgId FROM arPrgVersions WHERE PrgVerId=CCT.PrgVerId)) As ProgDescrip, ")
            .Append("    (Select BillingMethodDescrip from saBillingMethods where BillingMethodId=CCT.BillingMethodId) As BillingMethodDescrip, ")
            .Append("    (Select TuitionCategoryDescrip from saTuitionCategories where TuitionCategoryId=CCT.TuitionCategoryId) As TuitionCategoryDescrip, ")
            .Append("    (Select ShiftDescrip from arShifts where ShiftId=CCT.ShiftId) As ShiftDescrip, ")
            .Append("    (Select EdLvlDescrip from adEdLvls where EdLvlId=CCT.EdLvlId) As EdLvlDescrip, ")
            .Append("    (Select Descrip from arDropReasons where DropReasonId=CCT.DropReasonId) As DropReason, ")
            .Append("    (Select StatusCodeDescrip from syStatusCodes where StatusCodeId=CCT.StatusCodeId) As StatusCodeDescrip, ")
            .Append("    (Select CampDescrip from syCampuses where CampusId=CCT.CampusId) As CampDescrip, ")
            .Append("    (Select Descrip from arAttendTypes where AttendTypeId=CCT.AttendTypeId) As AttType, ")
            .Append("    (Select Descrip from adDegCertSeeking where DegCertSeekingId=CCT.DegCertSeekingId) As DegCertSeeking,CCT.ReEnrollmentDate,CCT.CohortStartDate,CCT.SAPId,CCT.ContractedGradDate  ")
            ''Modified by Saraswathi lakshmanan on June 1 2010
            ''Transfer Hours added to the query
            .Append(" ,CCT.TransferHours, CCT.GraduatedOrReceivedDate,CCT.StudentID ")
            .Append(" from arStuEnrollments CCT where CCT.StuEnrollID=? ")
        End With
        'Add the EmployerContactId the parameter list
        '    db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim stuEnrollInfo As New StudentEnrollmentInfo

        While dr.Read()
            With stuEnrollInfo
                .StudentID = dr("StudentId").ToString
                .ProgramVersionID = dr("PrgVerId").ToString
                .AcadamicAdvisor = dr("AcademicAdvisor").ToString
                .AdmissionsRep = dr("AdmissionsRep").ToString
                .Program = dr("ProgDescrip").ToString

                ' .EnrollmentId = dr("EnrollmentId")
                If Not (dr("EnrollmentId") Is DBNull.Value) Then
                    .EnrollmentId = dr("EnrollmentId").ToString()
                Else
                    .EnrollmentId = ""
                End If
                .BillingMethodId = dr("BillingMethodId").ToString
                .CampusId = dr("CampusId").ToString
                .TuitionCategory = dr("TuitionCategoryId").ToString
                If Not (dr("EnrollDate") Is DBNull.Value) Then
                    .EnrollDate = dr("EnrollDate")
                Else
                    .EnrollDate = ""
                End If
                If Not (dr("ExpGradDate") Is DBNull.Value) Then
                    .ExpGradDate = dr("ExpGradDate")
                Else
                    .ExpGradDate = ""
                End If
                If Not (dr("ExpStartDate") Is DBNull.Value) Then
                    .ExpStartDate = dr("ExpStartDate")
                Else
                    .ExpStartDate = ""
                End If
                If Not (dr("ContractedGradDate") Is DBNull.Value) Then
                    .ContractedGradDate = dr("ContractedGradDate")
                Else
                    .ContractedGradDate = ""
                End If
                If Not (dr("MidPtDate") Is DBNull.Value) Then
                    .MidPtDate = dr("MidPtDate")
                Else
                    .MidPtDate = ""
                End If
                If Not (dr("StartDate") Is DBNull.Value) Then
                    .StartDate = dr("StartDate")
                Else
                    .StartDate = ""
                End If
                .ShiftId = dr("ShiftId").ToString
                .GrdLvlId = dr("EdLvlId").ToString
                If Not (dr("LDA") Is DBNull.Value) Then
                    .LDA = dr("LDA")
                Else
                    .LDA = ""
                End If
                .StatusCodeId = dr("StatusCodeId").ToString
                .DropReasonId = dr("DropReasonId").ToString
                If Not (dr("AttendTypeId") Is DBNull.Value) Then
                    .AttendTypeId = dr("AttendTypeId").ToString
                Else
                    .AttendTypeId = ""
                End If
                If Not (dr("DegCertSeekingId") Is DBNull.Value) Then
                    .DegCertSeekingId = dr("DegCertSeekingId").ToString
                Else
                    .DegCertSeekingId = ""
                End If
                .DegCertSeekingId = dr("DegCertSeekingId").ToString
                If Not (dr("TransferDate") Is DBNull.Value) Then
                    .TransferDate = dr("TransferDate")
                Else
                    .TransferDate = ""
                End If
                If Not (dr("DateDetermined") Is DBNull.Value) Then
                    .DateDetermined = dr("DateDetermined")
                Else
                    .DateDetermined = ""
                End If
                If Not (dr("FAAdvisorId") Is DBNull.Value) Then .FaAdvisor() = dr("FAAdvisorId").ToString Else .FaAdvisor() = ""
                'get ModUser
                If Not (dr("ModDate") Is DBNull.Value) Then .ModDate = dr("ModDate") Else .ModDate = Date.MinValue

                If Not (dr("ModUser") Is DBNull.Value) Then .ModUser = dr("ModUser").ToString Else .ModUser = ""
                If Not (dr("Program") Is DBNull.Value) Then .Program = dr("Program")
                If Not (dr("PrgVerDescrip") Is DBNull.Value) Then .ProgramVersion = dr("PrgVerDescrip")
                If Not (dr("BillingMethodDescrip") Is DBNull.Value) Then .BillingMethod = dr("BillingMethodDescrip")
                If Not (dr("TuitionCategoryDescrip") Is DBNull.Value) Then .TuitionCatText = dr("TuitionCategoryDescrip")
                If Not (dr("ShiftDescrip") Is DBNull.Value) Then .Shift = dr("ShiftDescrip")
                If Not (dr("EdLvlDescrip") Is DBNull.Value) Then .GradeLevel = dr("EdLvlDescrip")
                If Not (dr("DropReason") Is DBNull.Value) Then .DropReason = dr("DropReason")
                If Not (dr("StatusCodeDescrip") Is DBNull.Value) Then .StatusCode = dr("StatusCodeDescrip")
                If Not (dr("CampDescrip") Is DBNull.Value) Then .Campus = dr("CampDescrip")
                If Not (dr("AttType") Is DBNull.Value) Then .AttendType = dr("AttType")
                If Not (dr("DegCertSeeking") Is DBNull.Value) Then .DegCertSeekingText = dr("DegCertSeeking")
                If Not (dr("ReEnrollmentDate") Is DBNull.Value) Then .ReenrollDate = dr("ReEnrollmentDate")
                If Not (dr("CohortStartDate") Is DBNull.Value) Then .CohortStartDate = dr("CohortStartDate")
                If Not (dr("SAPId") Is DBNull.Value) Then .SAPId = dr("SAPId").ToString

                'Added by Saraswathi lakshmanan on June 3 2010
                ''Transfer Hours added to the query
                If Not (dr("TransferHours") Is DBNull.Value) Then
                    .TransferHours = dr("TransferHours")
                Else
                    .TransferHours = 0
                End If
                If Not (dr("GraduatedOrReceivedDate") Is DBNull.Value) Then
                    .GraduatedOrReceivedDate = dr("GraduatedOrReceivedDate")
                Else
                    .GraduatedOrReceivedDate = ""
                End If
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return stuEnrollInfo
    End Function
    Public Function GetStatusCount(ByVal StuEnrollId As String) As Integer
        'Dim ds As New DataSet
        'Dim da As OleDbDataAdapter
        'Dim sGrdSysDetailId As String
        Dim rowCount As Integer
        'rowCount = 0
        'Dim da1 As New OleDbDataAdapter
        'Dim strSQLString As New StringBuilder
        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder


            '   build the sql query
            With sb
                .Append("Select count(*) as Count from syStudentStatusChanges a where a.StuEnrollId = ? ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@PrgVerId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try

        'Return the datatable in the dataset
        Return rowCount

    End Function
    Public Function DeleteStudentEnrollment(ByVal StuEnrollId As String, ByVal modDate As DateTime) As String
        If GetStatusCount(StuEnrollId) > 1 Then
            Return "Enrollment cannot be deleted because it is being referenced in another place."

        Else
            Dim db As New DataAccess
            Dim strSQL As New StringBuilder

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()


            'Set the connection string

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Try
                With strSQL
                    .Append("DELETE FROM arStuEnrollments WHERE StuEnrollId = ? ")
                    .Append(" AND ModDate = ? ; DELETE FROM syStudentStatusChanges WHERE StuEnrollId = ? ; ")
                    .Append("SELECT count(*) FROM arStuEnrollments WHERE StuEnrollId = ? ")
                End With
                db.AddParameter("@StudentID", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                '   ModDate
                db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                db.AddParameter("@StudentID", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                db.AddParameter("@StudentID2", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
                '   execute the query
                Dim rowCount As Integer = db.RunParamSQLScalar(strSQL.ToString)

                '   If the row was not deleted then there was a concurrency problem
                If rowCount = 0 Then
                    '   return without errors
                    Return ""
                Else
                    Return DALExceptions.BuildConcurrencyExceptionMessage()
                End If

            Catch ex As OleDbException
                '   return an error to the client
                Return DALExceptions.BuildErrorMessage(ex)
            Finally
                'Close Connection
                db.CloseConnection()
            End Try
        End If

        'db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        'db.ClearParameters()
        'strSQL.Remove(0, strSQL.Length)
        'db.CloseConnection()
    End Function
    Public Function GetPrgVersionBillingMethod(ByVal PrgVerId As String) As String
        'Dim ds As New DataSet
        'Dim da As OleDbDataAdapter
        'Dim count As String
        Dim sBillingMethodId As String

        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("Select BillingMethodId from arPrgVersions where PrgVerId = ?")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            db.OpenConnection()
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                sBillingMethodId = dr("BillingMethodId").ToString
            End While

            'ds = db.RunParamSQLDataSet(sb.ToString)
            'sb.Remove(0, sb.Length)


            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return sBillingMethodId

    End Function

    Public Function GetPrgVersionBillingMethod(ByVal PrgVerId As String, ByVal campusId As String) As String
        'Dim ds As New DataSet
        'Dim da As OleDbDataAdapter
        'Dim count As String
        Dim sBillingMethodId As String

        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append(" Select * from arPrgVersions,saBillingMethods where PrgVerId = ? ")
                .Append(" and arPrgVersions.BillingMethodId=saBillingMethods.BillingMethodId ")
                .Append(" and saBillingMethods.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ? )")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            db.OpenConnection()
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                sBillingMethodId = dr("BillingMethodId").ToString
            End While

            'ds = db.RunParamSQLDataSet(sb.ToString)
            'sb.Remove(0, sb.Length)


            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return sBillingMethodId

    End Function
    Public Function GetScheduleByProgramVersion(ByVal PrgVerId As String) As DataSet
        'Dim ds As New DataSet
        'Dim da As OleDbDataAdapter
        '   connect to the database
        Dim db As New DataAccess
        Dim ds as DataSet

        Try
            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append(" Select ScheduleId, Descrip from arProgSchedules ps where ps.PrgVerId = ? And Active = 1")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            db.OpenConnection()
            'Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)


        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        Return ds

    End Function
    Public Function SaveScheduleForEnrollment(ByVal StuEnrollId As String, ByVal ScheduleId As String, ByVal User As String,
                                              ByVal PrgVerId As String) As String
        '   connect to the database
        Dim db As New DataAccess
        'Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        Dim noSchedule = ScheduleId = ""

        Try
            With sb.Append("SELECT Count(*)
                                FROM    dbo.arStudentSchedules
                                WHERE   StuEnrollId = ?")
            End With

            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            sb.Remove(0, sb.Length)
            db.ClearParameters()

            If rowCount > 0 Then
                If noSchedule Then
                    Dim isPrgVerUsingTC As Boolean = SchedulesDB.IsPrgVerUsingTimeClockSchedule(PrgVerId)

                    If isPrgVerUsingTC Then
                        Return "Time clock programs must have a schedule."
                    Else
                        With sb
                            .Append("Delete From arStudentSchedules
                        WHERE   StuEnrollId = ?")
                        End With
                        '   StuEnrollId
                        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                        db.RunParamSQLExecuteNoneQuery(sb.ToString)
                    End If

                Else
                    With sb
                        .Append("UPDATE arStudentSchedules
                        SET     ScheduleId = ?
                               ,Source = 'attendance'
                               ,Active = 1
                               ,ModUser = ?
                               ,ModDate = GETDATE()
                        WHERE   StuEnrollId = ?")
                    End With
                    '   Add params to the parameter list
                    db.AddParameter("@ScheduleId", ScheduleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                    '   StuEnrollId
                    db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                End If

            Else
                If noSchedule Then
                    Return ""
                End If
                With sb
                    .Append("INSERT  INTO arStudentSchedules
                                (
                                 StuEnrollId
                                ,ScheduleId
                                ,Source
                                ,Active
                                ,ModUser
                                ,ModDate
								)
                        VALUES  (
                                 ?
                                ,?
                                ,'attendance'
                                ,1
                                ,?
                                ,GETDATE()
                                )")
                End With
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ScheduleId", ScheduleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString)
            End If


            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetStdProbations(ByVal StuEnrollId As String) As Integer
        'Dim ds As New DataSet
        'Dim da As OleDbDataAdapter
        'Dim sGrdSysDetailId As String
        Dim rowCount As Integer
        'Dim da1 As New OleDbDataAdapter
        'Dim strSQLString As New StringBuilder
        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder


            '   build the sql query
            With sb
                .Append("Select count(*) as Count from arStuProbWarnings a where a.StuEnrollId = ? and a.ProbWarningTypeId in (1,2) ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@PrgVerId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try

        'Return the datatable in the dataset
        Return rowCount

    End Function

    Public Function GetCurrentProbations(ByVal StuEnrollId As String) As List(Of String)
        ' Dim ds As New DataSet
        Dim dr As OleDbDataReader
        'Dim sGrdSysDetailId As String
        'Dim rowCount As Integer
        'Dim da1 As New OleDbDataAdapter
        'Dim strSQLString As New StringBuilder
        Dim rtn As New List(Of String)
        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder


            '   build the sql query
            With sb
                .Append("Select StartDate,EndDate from arStuProbWarnings a where a.StuEnrollId = ? and a.ProbWarningTypeId in (1,2) and EndDate >= ? ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StuEnID", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@DateNow", DateTime.Now, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query
            dr = db.RunParamSQLDataReader(sb.ToString())
            While dr.Read()
                rtn.Add(dr(0))
                rtn.Add(dr(1))
            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try

        'Return the datatable in the dataset
        Return rtn

    End Function

    Public Function GetStdLOAs(ByVal StuEnrollId As String) As Integer
        'Dim ds As New DataSet
        'Dim da As OleDbDataAdapter
        'Dim sGrdSysDetailId As String
        Dim rowCount As Integer
        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("Select count(*) as Count from arStudentLOAs a where a.StuEnrollId = ? ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@PrgVerId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try

        'Return the datatable in the dataset
        Return rowCount

    End Function
    Public Function GetStdWarnings(ByVal StuEnrollId As String) As Integer
        'Dim ds As New DataSet
        'Dim da As OleDbDataAdapter
        'Dim sGrdSysDetailId As String
        Dim rowCount As Integer
        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("Select count(*) as Count from arStuProbWarnings a where a.StuEnrollId = ? and a.ProbWarningTypeId = 3 ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@PrgVerId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try

        'Return the datatable in the dataset
        Return rowCount

    End Function
    Public Function GetStdSuspensionCount(ByVal StuEnrollId As String) As Integer
        'Dim ds As New DataSet
        'Dim da As OleDbDataAdapter
        'Dim sGrdSysDetailId As String
        Dim rowCount As Integer
        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("Select count(*) as Count from arStdSuspensions a where a.StuEnrollId = ? ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@PrgVerId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try

        'Return the datatable in the dataset
        Return rowCount

    End Function
    Public Function GetStudentsInPrgVersion(ByVal prgVerId As String, ByVal campusId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            'Pull all the statuses except for 'Dropped', 'Graduated' and 'No starts' and 'Transfer out' 
            'since these students are no longer in the school
            '   Dropped         => SysStatusId = 12
            '   Graduated       => SysStatusId = 14
            '   No starts       => SysStatusId = 8
            '   Transfer out    => SysStatusId = 19
            .Append("SELECT ")
            .Append("       E.StuEnrollId,S.LastName,S.FirstName,S.SSN,")
            .Append("       E.PrgVerId,(SELECT PrgVerDescrip FROM arPrgVersions WHERE PrgVerId=E.PrgVerId) AS PrgVerDescrip,")
            .Append("       E.StatusCodeId,C.StatusCodeDescrip,E.StartDate,E.ExpGradDate ")
            .Append("FROM   arStuEnrollments E, arStudent S, syStatusCodes C, sySysStatus B ")
            .Append("WHERE  E.StudentId=S.StudentId AND E.PrgVerId=? AND E.CampusId=? ")
            .Append("       AND E.StatusCodeId=C.StatusCodeId ")
            .Append("       AND C.SysStatusId=B.SysStatusId AND C.SysStatusId NOT IN (12,14,8,19) ")
            .Append("ORDER BY S.LastName,S.FirstName,S.SSN")
        End With

        '   Add params to the parameter list
        db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        '   return datatable
        Return ds
    End Function

    Public Function UpdatePrgVersionForStuEnrollments(ByVal arrStuEnrollId As ArrayList, ByVal PrgVerId As String, ByVal user As String) As String
        '   connect to the database
        Dim db As New DataAccess
        'Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try
            '   build the sql query to update arStuEnrollments
            With sb
                .Append("UPDATE     arStuEnrollments ")
                .Append("SET        PrgVerId=?, ModDate=?, ModUser=? ")
                .Append("WHERE      StuEnrollId=? ")
            End With

            For i As Integer = 0 To arrStuEnrollId.Count - 1
                '   Add params to the parameter list
                '   StatusCodeId
                db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                '   ModDate
                Dim now As DateTime = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                '   StuEnrollId
                db.AddParameter("@StuEnrollId", arrStuEnrollId.Item(i).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                db.ClearParameters()
            Next

            '   commit transaction 
            groupTrans.Commit()

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()
            '   do not report sql lost connection
            'If Not groupTrans.Connection Is Nothing Then
            ' Report the error
            'End If
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function UpdateCampusAndPrgVersionForStuEnrollments(ByVal arrStuEnrollId As ArrayList, ByVal NewCampusId As String, ByVal PrgVerId As String, ByVal user As String) As String
        '   connect to the database
        Dim db As New DataAccess
        'Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try
            '   build the sql query to update arStuEnrollments
            With sb
                .AppendLine("UPDATE     arStuEnrollments ")
                .AppendLine("SET        PrgVerId=?, CampusId =?, ModDate=?, ModUser=?")
                .AppendLine("WHERE      StuEnrollId=? ")
            End With

            For i As Integer = 0 To arrStuEnrollId.Count - 1
                '   Add params to the parameter list
                '   StatusCodeId

                db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@CampId", NewCampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                '   ModDate
                Dim now As DateTime = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                '   StuEnrollId
                db.AddParameter("@StuEnrollId", arrStuEnrollId.Item(i).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                db.ClearParameters()
            Next

            '   commit transaction 
            groupTrans.Commit()

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()
            '   do not report sql lost connection
            'If Not groupTrans.Connection Is Nothing Then
            ' Report the error
            'End If
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetAllPrograms() As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        With sb
            .Append("SELECT ")
            .Append("       P.ProgId,P.ProgCode,P.ProgDescrip,P.StatusId ")
            .Append("FROM   arPrograms P, syStatuses ST ")
            .Append("WHERE  P.StatusId=ST.StatusId AND ST.Status='Active' ")
            .Append("ORDER BY P.ProgDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetStudentsForIPEDSHelper(ByVal genderId As String, ByVal programId As String,
                                                ByVal EthCodeId As String, ByVal maritalStatusId As String,
                                                ByVal statusCodeId As String, ByVal minAge As String,
                                                ByVal maxAge As String, ByVal minStartDate As String,
                                                ByVal maxStartDate As String, ByVal zipCode As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        With sb
            .Append("SELECT ")
            .Append("       E.StuEnrollId,E.StudentId,S.FirstName,S.LastName,S.MiddleName,")
            .Append("       S.SSN,E.StartDate,E.ExpGradDate,E.StatusCodeId,E.PrgVerId,")
            .Append("       (SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=E.StatusCodeId) AS StatusCodeDescrip, ")
            .Append("       (SELECT PrgVerDescrip FROM arPrgVersions WHERE PrgVerId=E.PrgVerId) AS PrgVerDescrip ")
            .Append("FROM   arStuEnrollments E, arStudent S ")
            .Append("WHERE  E.StudentId = S.StudentId ")
            '   Append filters
            '   Gender filter
            If genderId <> Guid.Empty.ToString Then
                .Append("   AND S.Gender=? ")
                db.AddParameter("@Gender", genderId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            '   Program filter
            If programId <> Guid.Empty.ToString Then
                .Append("   AND EXISTS (SELECT *  ")
                .Append("               FROM arPrgVersions PV,arPrograms P  ")
                .Append("               WHERE PV.ProgId=P.ProgId ")
                .Append("		        AND P.ProgId=? ")
                .Append("		        AND PV.PrgVerId=E.PrgVerId ) ")
                db.AddParameter("@ProgId", programId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            '   Ethnicity filter
            If EthCodeId <> Guid.Empty.ToString Then
                .Append("   AND S.Race=? ")
                db.AddParameter("@Race", EthCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            '   Marital Status filter
            If maritalStatusId <> Guid.Empty.ToString Then
                .Append("   AND S.MaritalStatus=? ")
                db.AddParameter("@MaritalStatus", maritalStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            '   Zip Code filter
            If zipCode <> "" Then
                .Append("   AND EXISTS (SELECT *  ")
                .Append("		        FROM arStudAddresses A  ")
                .Append("               WHERE A.StudentId=E.StudentId  ")
                .Append("		        AND A.Zip=?)  ")
                db.AddParameter("@Zip", zipCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            '   Enrollment Status filter
            If statusCodeId <> Guid.Empty.ToString Then
                .Append("   AND E.StatusCodeId=?   ")
                db.AddParameter("@StatusCodeId", statusCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            '   Start Date filter
            If minStartDate <> "" And maxStartDate <> "" Then
                .Append("   AND E.StartDate >= ? AND E.StartDate <= ? ")
                db.AddParameter("@StartDate", minStartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
                db.AddParameter("@StartDate", maxStartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            End If
            '   Age filter
            If minAge <> "" And maxAge <> "" Then
                .Append("   AND (DATEDIFF(year, S.DOB, getdate()))>=? AND (DATEDIFF(year, S.DOB, getdate()))<=? ")
                db.AddParameter("@MinAge", minAge, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
                db.AddParameter("@MaxAge", maxAge, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
            End If

            .Append("ORDER BY S.LastName,S.FirstName,S.MiddleName ")
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllPrgVersionsForCampus(ByVal NewCampusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT DISTINCT ")
            .Append("           PrgVerId,")
            .Append("           PrgVerDescrip,")
            .Append(" case when (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=arPrgVersions.ProgId) is null  then ")
            .Append(" arPrgVersions.PrgVerDescrip ")
            .Append(" else ")
            .Append(" arPrgVersions.PrgVerDescrip + ' (' + (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=arPrgVersions.ProgId) + ')'  ")
            .Append(" end as PrgVerShiftDescrip,  ")
            .Append("           ProgId,")
            .Append("           (SELECT ProgDescrip FROM arPrograms WHERE ProgId=arPrgVersions.ProgId) AS Program ")
            .Append("FROM       arPrgVersions,syStatuses  ")
            .Append("WHERE      arPrgVersions.StatusId = syStatuses.StatusId and syStatuses.Status='Active'  ")
            .Append("           and arPrgVersions.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("ORDER BY   PrgVerDescrip ")
        End With

        db.AddParameter("@CampId", NewCampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetCampuses(ByVal PrgVersion As String, ByVal CurrentCampus As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            If PrgVersion = "" Then
                .Append("Select DISTINCT ")
                .Append("t1.CampusId, ")
                .Append("t2.CampDescrip ")
                .Append("FROM    syCmpGrpCmps t1 ")
                .Append("JOIN    syCampuses t2 ON t2.CampusId = t1.CampusId ")
                .Append("WHERE   t1.CampusId = ?  ")
                .Append("ORDER BY   t2.CampDescrip ")

                db.AddParameter("@CampId", CurrentCampus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else 
                .Append("Select DISTINCT ")
                .Append("t1.CampusId, ")
                .Append("t2.CampDescrip ")
                .Append("FROM       syCmpGrpCmps t1,syCampuses t2 ")
                .Append("WHERE      t1.CampGrpId in (Select CampGrpId from arPrgVersions where PrgVerId = ?)  ")
                .Append(" and t1.CampusId = t2.CampusId ")
                .Append("ORDER BY   t2.CampDescrip ")

                db.AddParameter("@CampId", PrgVersion, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
        End With
       
        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetStudentsInSameProgram(ByVal prgVerId As String, ByVal campusId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            'Pull all the statuses except for 'Dropped', 'Graduated' and 'No starts' and 'Transfer out' 
            'since these students are no longer in the school
            '   Dropped         => SysStatusId = 12
            '   Graduated       => SysStatusId = 14
            '   No starts       => SysStatusId = 8
            '   Transfer out    => SysStatusId = 19
            .Append("SELECT ")
            .Append("       E.StuEnrollId,S.LastName,S.FirstName,S.SSN,")
            .Append("       E.PrgVerId,(SELECT PrgVerDescrip FROM arPrgVersions WHERE PrgVerId=E.PrgVerId) AS PrgVerDescrip,")
            .Append("       E.StatusCodeId,C.StatusCodeDescrip,E.StartDate,E.ExpGradDate ")
            .Append("FROM   arStuEnrollments E, arStudent S, syStatusCodes C, sySysStatus B ")
            .Append("WHERE  E.StudentId=S.StudentId AND E.PrgVerId in (Select PrgVerId from arPrgVersions where ProgId in(Select ProgId from arPrgVersions where PrgVerId = ?)) ")
            .Append("       AND E.CampusId=? ")
            .Append("       AND E.StatusCodeId=C.StatusCodeId ")
            .Append("       AND C.SysStatusId=B.SysStatusId AND C.SysStatusId NOT IN (12,8,19) ")
            .Append("ORDER BY S.LastName,S.FirstName,S.SSN")
        End With

        '   Add params to the parameter list
        db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        '   return datatable
        Return ds
    End Function
    Public Function GetAllAdmissionRepsByCampusAndUserId(ByVal strCampusId As String, ByVal strUserId As String, Optional ByVal StuEnrollId As String = "") As DataSet
        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Dim da6 As New OleDbDataAdapter
        'Dim ds As New DataSet
        'Build the sql query
        With sb
            If Not StuEnrollId = "" Then
                .Append(" Select distinct '(X) ' + U.fullname as FullName,U.UserId as UserId from arStuEnrollments L,syUsers U ")
                .Append(" where L.AdmissionsRep=U.UserId  ")
                .Append(" AND L.StuEnrollId='" & StuEnrollId & "' ")
                .Append(" AND U.UserId not in ")
                'Subquery brings in all the Admission Reps that belong to campus user is logged in
                .Append(" ( ")
                .Append(" select distinct s1.UserId from ")
                .Append(" syUsers s1,syUsersRolesCampGrps s2,syRoles s3,sySysRoles s4,syCmpGrpCmps s5,syStatuses s6 ")
                .Append(" where s1.UserId = s2.UserId and s2.RoleId = s3.RoleId ")
                .Append(" and s3.SysRoleId = s4.SysRoleId and s2.CampGrpId = s5.CampGrpId and s4.StatusId = s6.StatusId ")
                .Append(" and s5.CampusId=? and s1.AccountActive=1 and s4.SysRoleId = 3 and s6.Status='Active' ")
                .Append(" and s1.UserId=? ")
                .Append(" ) ")
                .Append(" Union ")
            End If
            .Append(" select distinct s1.fullname,s1.UserId from ")
            .Append(" syUsers s1,syUsersRolesCampGrps s2,syRoles s3,sySysRoles s4,syCmpGrpCmps s5,syStatuses s6 ")
            .Append(" where s1.UserId = s2.UserId and s2.RoleId = s3.RoleId ")
            .Append(" and s3.SysRoleId = s4.SysRoleId and s2.CampGrpId = s5.CampGrpId and s4.StatusId = s6.StatusId ")
            .Append(" and s5.CampusId=? and s4.SysRoleId = 3 and s1.AccountActive=1 and s6.Status='Active' ")
            .Append(" and s1.UserId=? ")
            .Append(" order by fullname")
        End With
        db.OpenConnection()
        db.AddParameter("@CampusId", strCampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@UserId", strUserId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", strCampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@UserId", strUserId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        Try
            Return db.RunParamSQLDataSet(sb.ToString)
        Catch ex As Exception
            Throw New Exception(ex.InnerException.Message)
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function GetAllAdmissionRepsByCampusAndUserIdAndUserRoles(ByVal strCampusId As String, ByVal strUserId As String, ByVal RoleId As Integer, Optional ByVal StuEnrollId As String = "") As DataSet
        'connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Dim da6 As New OleDbDataAdapter
        'Dim strInActiveStatus As String = AdvantageCommonValues.InactiveGuid
        'Dim ds As New DataSet
        'Build the sql query
        With sb
            If Not StuEnrollId = "" Then
                .Append(" Select distinct '(X) ' + U.fullname as FullName,U.UserId as UserId from arStuEnrollments L,syUsers U ")
                .Append(" where L.AdmissionsRep=U.UserId  ")
                .Append(" AND L.StuEnrollId='" & StuEnrollId & "' ")
                .Append(" AND U.UserId not in ")
                'Subquery brings in all the Admission Reps that belong to campus user is logged in
                .Append(" ( ")
                .Append(" select distinct s1.UserId from ")
                .Append(" syUsers s1,syUsersRolesCampGrps s2,syRoles s3,sySysRoles s4,syCmpGrpCmps s5,syStatuses s6 ")
                .Append(" where s1.UserId = s2.UserId and s2.RoleId = s3.RoleId ")
                .Append(" and s3.SysRoleId = s4.SysRoleId and s2.CampGrpId = s5.CampGrpId and s4.StatusId = s6.StatusId ")
                .Append(" and s5.CampusId=? and s1.AccountActive=1 and s4.SysRoleId = 3 and s6.Status='Active' ")
                If Not RoleId = 8 Then
                    .Append(" and s1.UserId=? ")
                End If
                .Append(" ) ")
                .Append(" Union ")
            End If
            .Append(" select distinct s1.fullname as FullName,s1.UserId as UserId from ")
            .Append(" syUsers s1,syUsersRolesCampGrps s2,syRoles s3,sySysRoles s4,syCmpGrpCmps s5,syStatuses s6 ")
            .Append(" where s1.UserId = s2.UserId and s2.RoleId = s3.RoleId ")
            .Append(" and s3.SysRoleId = s4.SysRoleId and s2.CampGrpId = s5.CampGrpId and s4.StatusId = s6.StatusId ")
            .Append(" and s5.CampusId=? and s1.AccountActive=1 and s4.SysRoleId = 3 and s6.Status='Active' ")
            If Not RoleId = 8 Then
                .Append(" and s1.UserId=? ")
            End If
            .Append(" order by fullname")
        End With
        db.OpenConnection()
        db.AddParameter("@CampusId", strCampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If Not RoleId = 8 Then
            db.AddParameter("@UserId", strUserId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.AddParameter("@CampusId", strCampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If Not RoleId = 8 Then
            db.AddParameter("@UserId", strUserId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        Try
            Return db.RunParamSQLDataSet(sb.ToString)
        Catch ex As Exception
            Throw New Exception(ex.InnerException.Message)
        Finally
            db.CloseConnection()
        End Try
    End Function
    ''Function Added by Saraswathi lakshmanan on 1-Oct-2008
    ''To get the Reschedule Reasons for the student
    Public Function GetRescheduleReason(ByVal stuEnrollId As String, ByVal CampusId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        With sb
            .Append("Select Descrip,ModDate,ModUser from arStuReschReason where stuEnrollId=  ?")
            .Append("  and CampusId = ? ")
        End With
        db.OpenConnection()
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            Return db.RunParamSQLDataSet(sb.ToString)
        Catch ex As Exception
            Throw New Exception(ex.InnerException.Message)
        Finally
            db.CloseConnection()
        End Try
    End Function

    ''To get the Count of Reschedule Reasons for the student
    'Public Function GetRescheduleReasonCount(ByVal stuEnrollId As String, ByVal campusId As String) As Integer
    Public Function GetRescheduleReasonCount(ByVal stuEnrollId As String) As Integer
        Dim rowCount As Integer
        Try

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            '   build the sql query
            With sb
                '.Append("Select count(ReschReasonTypeId) as Count from arStuReschReason  where StuEnrollId = ? and CampusId=? ")
                .Append("Select count(ReschReasonTypeId) as Count from arStuReschReason  where StuEnrollId = ? ")
            End With
            db.OpenConnection()
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            ' db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)
            'Close Connection
            db.CloseConnection()
        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try
        Return rowCount
    End Function

    '' update the cohort start Date
    Public Sub UpdateCohortStartDateinStudentEnrollment(ByVal StuEnrollmentID As String, ByVal CohortStartDate As String, ByVal user As String)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


        With strSQL
            'If (StuEnrollObj.EnrollmentId = "") Then
            '    .Append("INSERT INTO arStuEnrollments ")
            '    .Append(" (StudentId, PrgVerId, AcademicAdvisor, AdmissionsRep, BillingMethodId, CampusId, GrdLvlId, ShiftId, StatusCodeId, EnrollDate, ExpGradDate, ExpStartDate, LDA, MidPtDate, StartDate,TransferDate)  ")
            '    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")

            'Else
            .Append("UPDATE arStuEnrollments ")
            .Append("SET  CohortStartDate = ? ,ModUser = ?, ModDate = ? ")
            .Append("WHERE StuEnrollId= ?")
            'End If

        End With
        db.AddParameter("@CohortStartDate", CohortStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ''ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ''ModDate
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        db.AddParameter("@StuEnrollmentID", StuEnrollmentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

        'Close(Connection)
        db.CloseConnection()
    End Sub
    ''Added by Saraswathi Lakshmanan on Oct-2008-3rd
    Public Function GetShiftIdforProgramVersion(ByVal PrgVerId As String) As String
        Dim shiftID As String

        Try

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            '   build the sql query
            With sb
                .Append("Select ShiftId from arPrograms t1,arPrgVersions t2 ")
                .Append(" where t1.ProgId=t2.ProgId and t2.PrgVerId= ?")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                shiftID = dr("ShiftId").ToString
            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return shiftID

    End Function
    Public Function GetSchedLeadGrpCnt_SP(ByVal LeadGrpId As String) As Integer
        'Dim sb As New StringBuilder
        Dim db As New SQLDataAccess
        Dim rowCount As Integer
        db.OpenConnection()
        db.AddParameter("@LeadGrpId", LeadGrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        rowCount = db.RunParamSQLScalar_SP("usp_GetSchedLeadGrpCnt")
        db.CloseConnection()
        Return rowCount
    End Function
    Public Sub UpdateLDAForManuallySetLDAAsNO(ByVal stuEnrollId As String)
        'Dim sb As New StringBuilder
        Dim db As New SQLDataAccess
        'Dim RowCount As Integer = 0
        db.OpenConnection()
        db.AddParameter("@StuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        'RowCount = db.RunParamSQLScalar_SP("usp_GetSchedLeadGrpCnt")
        db.RunParamSQLExecuteNoneQuery_SP("usp_UpdateLDA")


    End Sub

    ''To get the Count of Status Change History for the student
    Public Function GetStatusChangeHistoryCount(ByVal StuEnrollId As String) As Integer
        Dim rowCount As Integer
        Try

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            '   build the sql query
            With sb
                .Append("Select count(StuEnrollId) as Count from syStudentStatusChanges  where StuEnrollId = ? ")
            End With
            db.OpenConnection()
            db.AddParameter("@stuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)
            'Close Connection
            db.CloseConnection()
        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try
        Return rowCount
    End Function

    Public Function IsStudentProgramClockHourType(ByVal stuEnrollId As String) As Boolean

        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@StuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Try
            ds = db.RunParamSQLDataSet_SP("dbo.USP_IsStudentProgramClockHourType")
        Catch ex As Exception
            Return False
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False

            End If
        Else
            Return False
        End If
    End Function
    Public Function HasStudentMetGraduationHoursRequirement(ByVal StuEnrollId As String) As Boolean

        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Try
            ds = db.RunParamSQLDataSet_SP("dbo.USP_HasStudentMetGraduationHoursRequirement")
        Catch ex As Exception
            Return False
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False

            End If
        Else
            Return False
        End If
    End Function
    Public Function HasStudentMetGraduationHoursRequirementByClass(ByVal stuEnrollId As String, ByVal campusid As String) As Boolean

        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@StuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@CampusId", New Guid(campusid), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Try
            ds = db.RunParamSQLDataSet_SP("dbo.USP_HasStudentMetGraduationHoursRequirementByClass")
        Catch ex As Exception
            Return False
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Return False
            Else
                Return True

            End If
        Else
            Return True
        End If
    End Function


    Public Function IsStudentLOA(ByVal StuEnrollmentId As String) As Boolean
        Try
            Dim StatusCodeId As String
            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            '   build the sql query
            With sb
                .Append("SELECT codes.StatusCodeId FROM [DBO].arStuEnrollments AS enrollments ")
                .Append("INNER JOIN syStatusCodes AS codes ON codes.StatusCodeId = enrollments.StatusCodeId ")
                .Append("WHERE enrollments.StuEnrollId = ? AND codes.SysStatusId IN (" + CType(StatusCodeDB.eStatusCodes.LeaveOfAbsence, Integer).ToString() + ") ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StuEnrollId", StuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                StatusCodeId = dr("StatusCodeId").ToString
            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return Not String.IsNullOrEmpty(StatusCodeId)

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try
    End Function

    ' ''' <summary>
    ' ''' Get the enrollment info from one enrollment
    ' ''' 
    ' ''' </summary>
    ' ''' <param name="stuEnrollID"></param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    'Public Function GetEnrollmentInfo(ByVal stuEnrollID As String) As EnrollmentInfo
    '    Const strSQL As String = "SELECT IsDisabled, EntranceInterviewDate, IsFirstTimeInSchool, IsFirstTimePostSecSchool FROM dbo.arStuEnrollments WHERE StuEnrollId = @StuEnrollId "
    '    Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings '.GetAppSettings()
    '    Dim connectionString As String = myAdvAppSettings.AppSettings("ConnectionString")
    '    Dim conn As New SqlConnection(connectionString)
    '    Dim command As SqlCommand = New SqlCommand(strSQL, conn)
    '    command.Parameters.AddWithValue("@StuEnrollId", stuEnrollID)
    '    conn.Open()
    '    Try
    '        Dim reader As SqlDataReader = command.ExecuteReader()
    '        Dim result As EnrollmentInfo = New EnrollmentInfo()
    '        While reader.Read()
    '            Dim eid = reader("EntranceInterviewDate")
    '            result.EntranceInterviewDate = If(eid Is DBNull.Value, Nothing, eid)
    '            Dim idd = reader("IsDisabled")
    '            result.IsDisabled = If(idd Is DBNull.Value, Nothing, idd)
    '            result.IsFirstTimeInSchool = reader("IsFirstTimeInSchool")
    '            result.IsFirstTimePostSecSchool = reader("IsFirstTimePostSecSchool")
    '        End While
    '        Return result
    '    Finally
    '        conn.Close()
    '    End Try
    'End Function
End Class
