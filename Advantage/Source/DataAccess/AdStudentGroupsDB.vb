﻿Imports FAME.Advantage.Common

Public Class AdStudentGroupsDB
    ''New Code Added By Vijay Ramteke On June 20, 2010 For Mantis Id 19197
    Public Function GetStudentGroups(ByVal UserId As String, ByVal CampusId As String, ByVal isUserSa As Boolean) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("select SG.StuGrpId,SG.Descrip,SG.StatusId,(select Distinct Status from syStatuses where StatusId=SG.StatusId) as Status ")
            ''New Code Added By Vijay Ramteke On June 29, 2010 For Mantis Id 19241
            ''.Append(" from adStudentGroups SG where (OwnerId=? or IsPublic=1) ")
            .Append(" from adStudentGroups SG  ")
                .Append(" where SG.CampGrpId in ( Select CampGrpId From syCampuses C, syCmpGrpCmps CGC Where CGC.CampusId=C.CampusId And C.CampusId=? ) ")
            If Not isUserSa Then
                .Append(" and (OwnerId=? or IsPublic=1) ")
                ''New COde Added By Vijay Ramteke On June 20, 2010 For Mantis Id 19197
                ''New COde Added By Vijay Ramteke On June 20, 2010 For Mantis Id 19197
            End If
            ''New Code Added By Vijay Ramteke On June 29, 2010 For Mantis Id 19241
            .Append("ORDER BY Descrip")
        End With
        ''New Code Added By Vijay Ramteke On June 29, 2010 For Mantis Id 19241
            db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        If Not isUserSa Then
            db.AddParameter("@Ownerid", UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ''New COde Added By Vijay Ramteke On June 20, 2010 For Mantis Id 19197
            ''New COde Added By Vijay Ramteke On June 20, 2010 For Mantis Id 19197
        End If
        ''New Code Added By Vijay Ramteke On June 29, 2010 For Mantis Id 19241
        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds
    End Function
    Public Function StatusIdGenerator() As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        With sb
            .Append("SELECT StatusId, Status ")
            .Append("FROM syStatuses")

        End With

        db.ConnectionString = "ConString"
        ds = db.RunParamSQLDataSet(sb.ToString(), "Status")
        sb.Remove(0, sb.Length)

        Return ds
    End Function
    Public Function GetGroupTypes() As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT GrpTypeID, Descrip ")
            .Append(" FROM adStuGrpTypes ")
        End With

        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString, "GroupTypes")

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds

    End Function
    Public Function GetStudentGroupInfo(ByVal StuGrpId As String) As AdReqGrpInfo
        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            '' Code Modified by Kamalesh Ahuja on June 09 2010 to resolve Mantis Issue Id 18141
            '.Append("SELECT StuGrpId, code,Descrip,StatusId,CampGrpId,OwnerId,TypeId,IsPublic,ModDate,ModUser ")
            '.Append(" FROM adStudentGroups where StuGrpId=?")
            .Append("SELECT StuGrpId, code,Descrip,StatusId,CampGrpId,OwnerId,TypeId,IsPublic,IsRegHold,IsTransHold,ModDate,ModUser ")
            .Append(" FROM adStudentGroups where StuGrpId=?")
            '''''''''''
        End With

        db.AddParameter("@StuGrpid", StuGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim ReqInfo As New AdReqGrpInfo
        While dr.Read()
            'set properties with data from DataReader
            With ReqInfo

                ''StuGrpId
                .StuGrpId = StuGrpId

                'Code
                If Not (dr("Code") Is System.DBNull.Value) Then .Code = dr("Code") Else .Code = ""

                'Descrip
                If Not (dr("Descrip") Is System.DBNull.Value) Then .Descrip = dr("Descrip") Else .Descrip = ""

                'CampusGrpId
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString Else .CampGrpId = ""

                'StatusId
                If Not (dr("StatusId") Is System.DBNull.Value) Then .StatusId = CType(dr("StatusId"), Guid).ToString Else .StatusId = ""

                'OwnerId
                If Not (dr("OwnerId") Is System.DBNull.Value) Then .OwnerId = CType(dr("OwnerId"), Guid).ToString Else .OwnerId = ""

                'GroupTypeId
                If Not (dr("TypeId") Is System.DBNull.Value) Then .GrpTypeId = CType(dr("TypeId"), Guid).ToString Else .GrpTypeId = ""

                'Is Public
                If Not (dr("IsPublic") Is System.DBNull.Value) Then .IsPublic = CType(dr("IsPublic"), Boolean) Else .IsPublic = False

                'ModDate 
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate") Else .ModDate = ""

                'ModDate 
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser") Else .ModUser = ""

                '' Code Added by Kamalesh Ahuja on June 09 2010 to resolve Mantis Issue Id 18141
                'Is Public
                If Not (dr("IsRegHold") Is System.DBNull.Value) Then .IsRegHold = CType(dr("IsRegHold"), Boolean) Else .IsRegHold = False

                'Is Public
                If Not (dr("IsTransHold") Is System.DBNull.Value) Then .IsTransHold = CType(dr("IsTransHold"), Boolean) Else .IsTransHold = False
                '''''''''''


            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return ReqInfo

    End Function
    Public Function ValidateStudentGroup(ByVal StuGrpName As String) As String

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT     Descrip ")
            .Append("FROM       adStudentGroups ")
            .Append("WHERE      Descrip =?")
        End With

        db.AddParameter("@StuGrpName", StuGrpName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Dim obj As Object = db.RunParamSQLScalar(sb.ToString)
        Dim result As String = ""

        '   return string
        If Not obj Is Nothing Then
            result = "The Student Group already exists."
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return result
    End Function
    Public Function AddStudentGroup(ByVal StuGroupInfo As AdReqGrpInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                '' Code Modified by Kamalesh Ahuja on June 09 2010 to resolve Mantis Issue Id 18141
                '.Append(" INSERT adStudentGroups(StuGrpId,code,Descrip,StatusId,CampGrpId,OwnerId,TypeId,ModDate,ModUser,IsPublic) ")
                '.Append(" VALUES (?,?,?,?,?,?,?,?,?,?) ")
                .Append(" INSERT adStudentGroups(StuGrpId,code,Descrip,StatusId,CampGrpId,OwnerId,TypeId,ModDate,ModUser,IsPublic,IsRegHold,IsTransHold) ")
                .Append(" VALUES (?,?,?,?,?,?,?,?,?,?,?,?) ")
                ''
            End With

            '  Stu Group Id
            db.AddParameter("@StuGrpId", StuGroupInfo.StuGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Code
            db.AddParameter("@Code", StuGroupInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Descrip
            db.AddParameter("@Descrip", StuGroupInfo.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", StuGroupInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId         
            db.AddParameter("@CampusGrpId", StuGroupInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   OwnerId         
            db.AddParameter("@OwnerId", StuGroupInfo.OwnerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   GrpTypeId         
            db.AddParameter("@GrpTypeId", StuGroupInfo.GrpTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'IsPublic
            If StuGroupInfo.IsPublic = True Then
                db.AddParameter("@IsPublic", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            Else
                db.AddParameter("@IsPublic", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If

            '' Code Added by Kamalesh Ahuja on June 09 2010 to resolve Mantis Issue Id 18141
            'IsRegHold
            If StuGroupInfo.IsRegHold = True Then
                db.AddParameter("@IsRegHold", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            Else
                db.AddParameter("@IsRegHold", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If

            'IsTransHold
            If StuGroupInfo.IsTransHold = True Then
                db.AddParameter("@IsTransHold", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            Else
                db.AddParameter("@IsTransHold", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If
            ''''''''''''

            'execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            ' return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function UpdateStudentGroup(ByVal StuGroupInfo As AdReqGrpInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                '' Code Modified by Kamalesh Ahuja on June 09 2010 to resolve Mantis Issue Id 18141
                '.Append(" update adStudentGroups set code=?,Descrip=?,StatusId=?,CampGrpId=?,OwnerId=?,TypeId=?,ModDate=?,ModUser=?,IsPublic=? ")
                '.Append(" where StuGrpId=? ")
                .Append(" update adStudentGroups set code=?,Descrip=?,StatusId=?,CampGrpId=?,OwnerId=?,TypeId=?,ModDate=?,ModUser=?,IsPublic=?,IsRegHold=?,IsTransHold=? ")
                .Append(" where StuGrpId=? ")
                ''''
            End With

            '   Code
            db.AddParameter("@Code", StuGroupInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Descrip
            db.AddParameter("@Descrip", StuGroupInfo.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", StuGroupInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId         
            db.AddParameter("@CampusGrpId", StuGroupInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   OwnerId         
            db.AddParameter("@OwnerId", StuGroupInfo.OwnerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   GrpTypeId         
            db.AddParameter("@GrpTypeId", StuGroupInfo.GrpTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'OtherState
            If StuGroupInfo.IsPublic = True Then
                db.AddParameter("@IsPublic", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            Else
                db.AddParameter("@IsPublic", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If

            '' Code Added by Kamalesh Ahuja on June 09 2010 to resolve Mantis Issue Id 18141
            'IsRegHold
            If StuGroupInfo.IsRegHold = True Then
                db.AddParameter("@IsRegHold", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            Else
                db.AddParameter("@IsRegHold", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If

            'IsTransHold
            If StuGroupInfo.IsTransHold = True Then
                db.AddParameter("@IsTransHold", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            Else
                db.AddParameter("@IsTransHold", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If
            ''''''''''''

            '  Student Group Id
            db.AddParameter("@StuGrpId", StuGroupInfo.StuGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            ' return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteStudentGroup(ByVal StuGrpId As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'do an insert
        Try
            '   build the query
            Dim sb5 As New StringBuilder
            With sb5
                .Append(" Delete from adStudentGroups  where StuGrpId=? ")
            End With

            '  Req Group Id
            db.AddParameter("@StuGrpId", StuGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb5.ToString)

            '   return without errors
            Return ""
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetGroupStudents(ByVal StuGrpId As String) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim sb As New StringBuilder

        With sb
            .Append("select SG.StuGrpStuId,  ST.StudentId, ST.FirstName , ST.LastName ,PV.PrgVerDescrip as Program,SC.StatusCodeDescrip as Status,CA.CampDescrip as CampusName,'False' as chkDelete ")
            .Append(" from arStudent ST,adStuGrpStudents SG, arStuEnrollments SE,  arPrgVersions PV,syStatusCodes SC,syCampuses CA ")
            .Append(" where ST.StudentId=SG.studentid  and SE.StudentId =ST.StudentId and SE.PrgVerId =PV.PrgVerId ")
            .Append(" and SE.StatusCodeId=SC.StatuscodeId and SE.CampusId=CA.CampusId and SG.StuGrpId=? And SG.IsDeleted=0 ")
            ''New Code Added By Vijay Ramteke On June 19, 2010 For Mantis Id 19199
            .Append(" AND SE.StuEnrollId=SG.StuEnrollId ")
            ''New Code Added By Vijay Ramteke On June 19, 2010 For Mantis Id 19199
        End With

        db.OpenConnection()
        db.AddParameter("@StuGrpId", StuGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString, "GroupStudents")

        Return ds

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    End Function
    Public Function DeleteGroupStudents(ByVal StuGrpStuId As String, ByVal User As String) As String
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        Dim msg As String

        Try
            With sb
                .Append("Update adStuGrpStudents Set ISDeleted=1, DateRemoved=?, ModUser=? where StuGrpStuId=? ")

            End With

            db.OpenConnection()
            db.AddParameter("@DateRemoved", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuGrpStuId", StuGrpStuId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString)

        Catch ex As Exception
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try
        Return msg
    End Function
    Public Function GetAllStudentGroup(ByVal StudentId As String) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        With sb
            ''New Code Added By Vijay Ramteke On June 20, 2010
            ''.Append("Select SG.Descrip, SGS.DateAdded, (Select U.FullName From syUsers U Where U.UserId=SG.OwnerId ) As OwnerName, SGS.DateRemoved,SGS.ModUser from arStudent S, adStuGrpStudents SGS, adStudentGroups SG Where SGS.StuGrpId=SG.StuGrpId AND S.StudentId=SGS.StudentId And SGS.StudentId=?")
            .Append("Select SG.Descrip, SGS.DateAdded, (Select U.FullName From syUsers U Where U.UserId=SG.OwnerId ) As OwnerName, SGS.DateRemoved,SGS.ModUser,PV.PrgVerDescrip as EnrollMents")
            .Append(" from arStuEnrollments SE,  arPrgVersions PV, adStuGrpStudents SGS, adStudentGroups SG ")
            .Append(" Where SGS.StuGrpId=SG.StuGrpId AND SE.StuEnrollId=SGS.StuEnrollId and SE.PrgVerId =PV.PrgVerId And SGS.StudentId=? ")
            ''New Code Added By Vijay Ramteke On June 20, 2010
            .Append(" Order by SG.Descrip ")
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.ConnectionString = "ConString"
        ds = db.RunParamSQLDataSet(sb.ToString(), "StudentGroup")
        sb.Remove(0, sb.Length)
        Return ds
    End Function
    ''New Code Added By Vijay Ramteke On June 19, 2010 For Mantis Id 19199
    Public Function ValidateStudentGroupStudent(ByVal StudentId As String, ByVal StuGroupId As String, ByVal StuEnrollId As String) As String

        '   connect to the database
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" SELECT     Count(*) ")
            .Append(" FROM       adStuGrpStudents ")
            .Append(" WHERE      StudentId =? ")
            .Append(" AND         StuGrpId=? ")
            ''New Code Added By Vijay Ramteke On June 19, 2010 For Mantis Id 19199
            .Append(" AND         StuEnrollId=? And IsDeleted=0")
            ''New Code Added By Vijay Ramteke On June 19, 2010 For Mantis Id 19199
        End With

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuGroupId", StuGroupId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)
        Dim result As String = ""

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   return string
        If Not ds.Tables(0).Rows(0)(0).ToString = "0" Then
            result = "The Student already exists in the Selected Group."
        End If

        Return result
    End Function
    ''New Code Added By Vijay Ramteke On June 19, 2010 For Mantis Id 19199
    Public Function AddStudentGroupStudent(ByRef StuGroupInfo As AdReqGrpInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                ''New Code Added By Vijay Ramteke On June 19, 2010 For Mantis Id 19199
                '.Append(" INSERT adStuGrpStudents(StuGrpStuId, StuGrpId, StudentId, DateAdded, DateRemoved, ModUser, ModDate, IsDeleted ) ")
                '.Append(" VALUES (?,?,?,?,?,?,?,?) ")
                .Append(" INSERT adStuGrpStudents(StuGrpStuId, StuGrpId, StudentId, DateAdded, DateRemoved, ModUser, ModDate, IsDeleted, StuEnrollId ) ")
                .Append(" VALUES (?,?,?,?,?,?,?,?,?) ")
                ''New Code Added By Vijay Ramteke On June 19, 2010 For Mantis Id 19199
            End With

            db.AddParameter("@StuGrpStuId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuGrpId", StuGroupInfo.StuGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StudentId", StuGroupInfo.StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@DateAdded", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@DateRemoved", DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@IsDeleted", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            ''New Code Added By Vijay Ramteke On June 19, 2010 For Mantis Id 19199
            db.AddParameter("@StuEnrollId", StuGroupInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ''New Code Added By Vijay Ramteke On June 19, 2010 For Mantis Id 19199
            'execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            ' return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    ''New Code Added By Vijay Ramteke On June 20, 2010 For Mantis Id 19197
    Public Function CheckForStudentGroupOwner(ByVal StuGrpId As String, ByVal UserId As String) As String

        '   Connect to the database
        Dim ds As New DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append(" Select Count(*) AS IsCheck From adStudentGroups ")
                .Append(" Where StuGrpId=? And OwnerId=? ")
            End With

            db.AddParameter("@StuGrpId", StuGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@UserId", UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'execute the query
            ds = db.RunParamSQLDataSet(sb.ToString, "CheckStudentGroupOwner")
            ' return without errors
            Return ds.Tables("CheckStudentGroupOwner").Rows(0)("IsCheck").ToString

        Catch ex As OleDbException
            '   return an error to the client
            Return "0"

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetStudentGroupOwner(ByVal OwnerId As String) As DataSet
        '   Connect to the database
        Dim ds As New DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                With sb
                    .Append("SELECT UserId,FullName,UserName ")
                    .Append("FROM syUsers ")
                    .Append(" WHERE UserId =? ")
                    .Append(" ORDER BY FullName")
                End With
            End With
            db.AddParameter("@OwnerId", OwnerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'execute the query
            ds = db.RunParamSQLDataSet(sb.ToString, "CheckStudentGroupOwner")
            ' return without errors
            Return ds

        Catch ex As OleDbException
            '   return an error to the client
            Return ds

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetAllStudentEnrollments(ByVal StudentId As String) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        With sb

            .Append("Select SE.StuEnrollId, PV.PrgVerDescrip")
            .Append(" from arStuEnrollments SE,  arPrgVersions PV ")
            .Append(" Where SE.PrgVerId =PV.PrgVerId And SE.StudentId=? ")
            .Append(" Order by PV.PrgVerDescrip ")
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.ConnectionString = "ConString"
        ds = db.RunParamSQLDataSet(sb.ToString(), "StudentEnrollments")
        sb.Remove(0, sb.Length)
        Return ds
    End Function

    Public Function GetStudentGroupsByEnrollment(ByVal StuEnrollId As String) As DataSet
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Return db.RunParamSQLDataSetUsingSP("usp_adStuGrpStudents_SELECT_ByEnrollment")

    End Function

End Class
