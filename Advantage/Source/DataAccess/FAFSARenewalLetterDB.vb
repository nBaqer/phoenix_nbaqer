Imports FAME.Advantage.Common

Public Class FAFSARenewalLetterDB
    Public Function GetFAFSADataGridDS(ByVal startDate As Date, ByVal cutOffDate As Date, ByVal campusId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("select distinct (S.studentId),firstname,middlename,lastname,ssn,AwardEndDate,")
            .Append("Case when (select count(*) from arStudaddresses where StudentId=S.studentId and default1=1)>=1 then 'True' else 'False' end as DefaultAddress ")
            .Append("from faStudentAwards SA,arStuEnrollments SE,syStatusCodes SC,syStatuses Stat,arStudent S ")
            .Append("where ")
            .Append("SA.StuEnrollId = SE.StuEnrollId ")
            .Append("and ")
            .Append("SE.StatusCodeID = SC.StatusCodeID ")
            .Append("and ")
            .Append("SC.StatusID = Stat.StatusID ")
            .Append("and ")
            .Append("Stat.Status ='Active' ")
            .Append("and ")
            .Append("SC.SysStatusID = 9 ")
            .Append("and ")
            .Append("se.studentid = s.studentid ")
            .Append("and AwardEndDate <= ? ")
            .Append("and AwardEndDate >= ? ")
            .Append("and SE.CampusId=? ")
        End With

        ' Add the cutOffDate to the parameter list
        db.AddParameter("@CutOffDate", cutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add the startDate to the parameter list
        db.AddParameter("@StartDate", startDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add campusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Return db.RunParamSQLDataSet(sb.ToString)

    End Function

    Public Function GetFAFSALetterDS(ByVal StudentId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Try
            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            With sb
                .Append("select S.studentId,firstname,middlename,lastname,Address1,Address2,City,")
                .Append("(Select statedescrip from syStates St where St.stateid = SAdd.StateId) AS Statedescrip,")
                .Append("Zip ")
                .Append("from arStudaddresses SAdd,arStudent S ")
                .Append("where ")
                .Append("sadd.StudentId = S.studentId And default1 = 1 ")
                .Append("and s.studentid in(" & StudentId & ")")
            End With

            Dim ds As DataSet = db.RunSQLDataSet(sb.ToString)

            db.CloseConnection()

            Return ds

        Catch ExceptionObject As System.Exception
            'Throw New Exception(m_ExceptionMessage, ExceptionObject)
            'Return "this is the error:" & sb.ToString
        Finally
            db.CloseConnection()
        End Try
    End Function
End Class
