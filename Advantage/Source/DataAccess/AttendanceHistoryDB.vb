Imports System.Text
Imports System.Data
Imports System.Data.OleDb
Imports FAME.AdvantageV1.Common.AR
Imports FAME.Advantage.Common

Public Class AttendanceHistoryDB

    Private Property CampusId As Object

    Public Function GetAttendanceData(ByVal paramInfo As ReportParamInfo) As DataTable
        '   connect to the database
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strWhere As String
        Dim score As Decimal
        Dim objDB As New TransferGradeDB

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'If paramInfo.FilterOther <> "" Then
        '    strWhere &= " AND " & paramInfo.FilterOther
        'End If

        'If paramInfo.FilterList <> "" Then
        '    strWhere &= " AND " & paramInfo.FilterList
        'End If

        'If paramInfo.FilterOtherString <> "" Then
        '    score = CInt(paramInfo.FilterOtherString)
        'End If

        Dim sb As New StringBuilder
        With sb
            .Append(" select Distinct " + vbCrLf)
            .Append(" t3.FirstName,t3.LastName,t1.StuEnrollId, " + vbCrLf)
            .Append(" Convert(char(10),t1.MeetDate,101) as MeetDate, " + vbCrLf)
            .Append(" NULL as Schedule,t1.Actual,Case when t1.Actual > 0 then t1.Actual/60 else 0 end as ActualHours, " + vbCrLf)
            .Append(" NULL as dw1,NULL as dw,t1.Comments,NULL as StudentIsTardy, " + vbCrLf)
            .Append(" t4.TrackTardies, t4.tardiesMakingAbsence, t4.PrgVerId, t4.PrgVerDescrip, " + vbCrLf)
            .Append(" 	C.CampusId,syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip, " + vbCrLf)
            .Append("  (SELECT Top 1 CampDescrip FROM syCampuses WHERE CampusId=C.CampusId) AS CampusDescrip " + vbCrLf)
            .Append(" from " + vbCrLf)
            .Append(" atClsSectAttendance t1,arStuEnrollments t2,arStudent t3,arPrgVersions t4, " + vbCrLf)
            .Append(" syCampGrps ,syCmpGrpCmps I " + vbCrLf)
            .Append(" where ")
            .Append("   t1.StuEnrollId=t2.StuEnrollId and t2.StudentId = t3.StudentId and " + vbCrLf)
            .Append("   t2.PrgVerId = t4.PrgVerId and " + vbCrLf)
            .Append(" syCampGrps.CampGrpId=I.CampGrpId AND t2.CampusId=I.CampusId " + vbCrLf)
            If paramInfo.PrgVerId <> "" Then
                .Append(" and t2.PrgVerId in (")
                .Append(paramInfo.PrgVerId)
                .Append(")  " + vbCrLf)
            End If
            .Append(" and syCampGrps.CampGrpId in  " + vbCrLf)
            .Append(" (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in ('")
            .Append(paramInfo.CampGrpId)
            .Append("'))  " + vbCrLf)
            .Append(" group by " + vbCrLf)
            .Append(" Order by syCampGrps.CampGrpDescrip,C.CampusId,t3.LastName,t3.FirstName " + vbCrLf)
        End With
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds.Tables(0)
    End Function
    Public Function GetAttendanceSummary(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim getTardyTrackingDetailsInfo As New TardyAttendanceUnitInfo
        Dim getAttendanceSummaryInfo As New ClockHourAttendanceInfo
        Dim strUnitDescription As String = ""
        Dim intTardiesForProgramVersion As Integer = 0
        Dim strUnitTypeId As String = ""
        Dim intPresentUnitFromTardyCalculation As Decimal = 0.0
        Dim intAbsentUnitFromTardyCalculation As Decimal = 0.0

        Dim decActualTardyHours As Decimal = 0
        Dim decNumberofTardy As Decimal = 0
        Dim decAdjustedActual As Decimal = 0
        Dim decAdjustedAbsent As Decimal = 0
        Dim decAdjustedTardy As Decimal = 0
        Dim decTardiesMakingAbsence As Decimal = 0
        Dim isUseTimeClock As Boolean = False
        Dim ds1 As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Dim ds As New DataSet
        ds = GetStudentSummaryDetails(paramInfo, "", "", False)


        Dim dt As New DataTable("AttendanceHistory")
        If ds.Tables.Count > 0 Then
            dt.Columns.Add(New DataColumn("LastName", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("FirstName", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("MeetDate", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Hours", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("Comments", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusId", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampGrpId", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("StudentId", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("PrgVerDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("SuppressDate", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Type", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ExternshipHoursCount", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("RegularHoursCount", System.Type.GetType("System.Decimal")))

            Dim row As DataRow
            Dim strType, strStudentId As String
            For Each dr As DataRow In ds.Tables(0).Rows
                row = dt.NewRow()
                row("LastName") = dr("LastName").ToString
                row("FirstName") = dr("FirstName").ToString
                row("MeetDate") = dr("MeetDate").ToString
                If Not dr("ActualHours") Is System.DBNull.Value Then
                    row("Hours") = dr("ActualHours").ToString
                Else
                    row("Hours") = 0.0
                End If
                Try
                    row("SuppressDate") = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
                Catch ex As System.Exception
                    row("SuppressDate") = "no"
                End Try
                row("Type") = "Regular"
                row("Comments") = dr("Comments").ToString
                row("CampusId") = dr("CampusId").ToString
                row("CampGrpId") = dr("CampGrpId").ToString
                row("CampusDescrip") = dr("CampusDescrip").ToString
                row("CampGrpDescrip") = dr("CampGrpDescrip").ToString
                row("StudentId") = dr("StudentId").ToString
                row("PrgVerDescrip") = paramInfo.PrgVerDescrip.ToString
                If strStudentId = dr("StudentId").ToString Then
                    If Not dr("ActualHours") Is System.DBNull.Value Then
                        row("RegularHoursCount") += dr("ActualHours").ToString
                    End If
                Else
                    If Not dr("ActualHours") Is System.DBNull.Value Then
                        row("RegularHoursCount") = dr("ActualHours").ToString
                    End If
                End If
                dt.Rows.Add(row)
                strStudentId = dr("StudentId").ToString
            Next
            ds = GetExternshipDetails(paramInfo, "", "", False)
            For Each dr As DataRow In ds.Tables(0).Rows
                row = dt.NewRow()
                row("LastName") = dr("LastName").ToString
                row("FirstName") = dr("FirstName").ToString
                row("MeetDate") = dr("MeetDate").ToString
                If Not dr("ActualHours") Is System.DBNull.Value Then
                    row("Hours") = dr("ActualHours").ToString
                Else
                    row("Hours") = 0.0
                End If
                Try
                    row("SuppressDate") = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
                Catch ex As System.Exception
                    row("SuppressDate") = "no"
                End Try
                row("Type") = "Externship"
                row("Comments") = dr("Comments").ToString
                row("CampusId") = dr("CampusId").ToString
                row("CampGrpId") = dr("CampGrpId").ToString
                row("CampusDescrip") = dr("CampusDescrip").ToString
                row("CampGrpDescrip") = dr("CampGrpDescrip").ToString
                row("StudentId") = dr("StudentId").ToString
                row("PrgVerDescrip") = paramInfo.PrgVerDescrip.ToString
                If strStudentId = dr("StudentId").ToString Then
                    If Not dr("ActualHours") Is System.DBNull.Value Then
                        row("ExternshipHoursCount") += dr("ActualHours").ToString
                    End If
                Else
                    If Not dr("ActualHours") Is System.DBNull.Value Then
                        row("ExternshipHoursCount") = dr("ActualHours").ToString
                    End If
                End If
                dt.Rows.Add(row)
                strStudentId = dr("StudentId").ToString
            Next
            ds1.Tables.Add(dt)
        End If
        Return ds1
    End Function
    Public Function GetExternshipSummary(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim getTardyTrackingDetailsInfo As New TardyAttendanceUnitInfo
        Dim getAttendanceSummaryInfo As New ClockHourAttendanceInfo
        Dim strUnitDescription As String = ""
        Dim intTardiesForProgramVersion As Integer = 0
        Dim strUnitTypeId As String = ""
        Dim intPresentUnitFromTardyCalculation As Decimal = 0.0
        Dim intAbsentUnitFromTardyCalculation As Decimal = 0.0

        Dim decActualTardyHours As Decimal = 0
        Dim decNumberofTardy As Decimal = 0
        Dim decAdjustedActual As Decimal = 0
        Dim decAdjustedAbsent As Decimal = 0
        Dim decAdjustedTardy As Decimal = 0
        Dim decTardiesMakingAbsence As Decimal = 0
        Dim isUseTimeClock As Boolean = False

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Get Tardy Information Based on Program Version
        'If Not paramInfo.PrgVerId = "" Then
        '    getTardyTrackingDetailsInfo = getTardyByProgramVersion(paramInfo.PrgVerId)
        '    With getTardyTrackingDetailsInfo
        '        intTardiesForProgramVersion = .TardiesMakingAbsence
        '        strUnitTypeId = .UnitTypeId
        '        isUseTimeClock = .UseTimeClock
        '    End With
        'End If
        'If strUnitTypeId.ToString.Trim = AttendanceUnitTypes.PresentAbsentGuid Then
        '    strUnitDescription = "pa"
        'ElseIf strUnitTypeId.ToString.Trim = AttendanceUnitTypes.MinutesGuid Then
        '    strUnitDescription = "minutes"
        'Else
        '    strUnitDescription = "hrs"
        'End If
        Dim ds As New DataSet
        ds = GetExternshipDetails(paramInfo, "", "", False)

        Dim ds1 As New DataSet
        Dim dt As New DataTable("AttendanceHistory")
        If ds.Tables.Count > 0 Then
            dt.Columns.Add(New DataColumn("LastName", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("FirstName", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("MeetDate", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Hours", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("Comments", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusId", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampGrpId", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("StudentId", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("PrgVerDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("SuppressDate", System.Type.GetType("System.String")))

            Dim row As DataRow
            For Each dr As DataRow In ds.Tables(0).Rows
                row = dt.NewRow()
                row("LastName") = dr("LastName").ToString
                row("FirstName") = dr("FirstName").ToString
                row("MeetDate") = dr("MeetDate").ToString
                If Not dr("ActualHours") Is System.DBNull.Value Then
                    row("Hours") = dr("ActualHours").ToString
                Else
                    row("Hours") = 0.0
                End If
                row("Comments") = dr("Comments").ToString

                Try
                    row("SuppressDate") = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
                Catch ex As System.Exception
                    row("SuppressDate") = "no"
                End Try
                'If Not dr("Schedule") Is System.DBNull.Value Then
                '    If dr("Schedule") = 0.0 Then
                '        If Not dr("ActualHours") Is System.DBNull.Value Then
                '            If dr("ActualHours") >= 1.0 Then
                '                row("Comments") = "Make up Day"
                '            End If
                '        End If
                '    End If
                'Else
                '    row("Comments") = dr("Comments").ToString
                'End If
                'If CType(dr("StudentIsTardy"), Boolean) = True Then
                '    Dim strTardy As String = "Tardy"
                '    row("Comments") = strTardy.ToString
                'Else
                '    row("Comments") = dr("Comments").ToString
                'End If
                'If Not dr("Schedule") Is System.DBNull.Value Then
                '    If dr("Schedule") = 0.0 Then
                '        If Not dr("ActualHours") Is System.DBNull.Value Then
                '            If dr("ActualHours") >= 1.0 Then
                '                row("Comments") = "Make up Day"
                '            End If
                '        End If
                '    End If
                'Else
                '    row("Comments") = dr("Comments").ToString
                'End If
                'If CType(dr("StudentIsTardy"), Boolean) = True Then
                '    Dim strTardy As String = "Tardy"
                '    row("Comments") = strTardy.ToString
                'Else
                '    row("Comments") = dr("Comments").ToString
                'End If
                row("CampusId") = dr("CampusId").ToString
                row("CampGrpId") = dr("CampGrpId").ToString
                row("CampusDescrip") = dr("CampusDescrip").ToString
                row("CampGrpDescrip") = dr("CampGrpDescrip").ToString
                row("StudentId") = dr("StudentId").ToString
                row("PrgVerDescrip") = paramInfo.PrgVerDescrip.ToString
                dt.Rows.Add(row)
            Next
            ds1.Tables.Add(dt)
        End If
        Return ds1
    End Function
    Private Function getTardyByProgramVersion(ByVal PrgVerId As String) As TardyAttendanceUnitInfo
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dr As OleDbDataReader

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim getTardyTrackDetails As New TardyAttendanceUnitInfo
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("   Select Distinct TardiesMakingAbsence,UnitTypeId,UseTimeClock ")
            .Append("   from    arPrgVersions PV ")
            .Append("   where PV.PrgVerId = ? ")
        End With
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            dr = db.RunParamSQLDataReader(sb.ToString)
            Do While dr.Read
                With getTardyTrackDetails
                    If Not dr("TardiesMakingAbsence") Is System.DBNull.Value Then .TardiesMakingAbsence = CType(dr("TardiesMakingAbsence"), Integer) Else .TardiesMakingAbsence = 0
                    If Not dr("UnitTypeId") Is System.DBNull.Value Then .UnitTypeId = dr("UnitTypeId").ToString Else .UnitTypeId = ""
                    .UseTimeClock = CType(dr("UseTimeClock"), Boolean)
                End With
            Loop
            If getTardyTrackDetails.UnitTypeId <> AdvantageCommonValues.PresentAbsentGuid And getTardyTrackDetails.UnitTypeId <> AdvantageCommonValues.MinutesGuid Then
                getTardyTrackDetails = getTardyByCourse()
            End If
            Return getTardyTrackDetails
        Catch ex As System.Exception
        Finally
            dr.Close()
            db.CloseConnection()
        End Try
    End Function
    Public Function getPrograms(ByVal progid As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@ProgId", progid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet("dbo.usp_Programs", Nothing, "SP")
    End Function
    Private Function getTardyByCourse() As TardyAttendanceUnitInfo
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dr As OleDbDataReader
        Dim getTardyTrackDetails As New TardyAttendanceUnitInfo

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("   Select Distinct TardiesMakingAbsence,UnitTypeId ")
            .Append("   from    arReqs  where UnitTypeid is not null ")

        End With

        Try
            dr = db.RunParamSQLDataReader(sb.ToString)
            Do While dr.Read
                With getTardyTrackDetails
                    If Not dr("TardiesMakingAbsence") Is System.DBNull.Value Then .TardiesMakingAbsence = CType(dr("TardiesMakingAbsence"), Integer) Else .TardiesMakingAbsence = 0
                    If Not dr("UnitTypeId") Is System.DBNull.Value Then .UnitTypeId = CType(dr("UnitTypeId"), Guid).ToString Else .UnitTypeId = ""
                End With
            Loop
            Return getTardyTrackDetails
        Catch ex As System.Exception
        Finally
            dr.Close()
            db.CloseConnection()
        End Try
    End Function
    'Private Function GetStudentSummaryDetails(ByVal paramInfo As ReportParamInfo, _
    '                                        Optional ByVal AttUnitType As String = "", _
    '                                        Optional ByVal PrgVerId As String = "", _
    '                                        Optional ByVal UseTimeClock As Boolean = False) As DataSet
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '    'If Attendance Unit Type Is Present Absent
    '    'If AttUnitType.ToString.ToLower = "pa" Or AttUnitType.ToString.ToLower = "minutes" Then
    '    With sb
    '        .Append(" select Distinct " + vbCrLf)
    '        .Append(" t4.FirstName,t4.LastName,t1.StuEnrollId,t4.StudentId, " + vbCrLf)
    '        .Append(" Convert(char(10),RecordDate,101) as MeetDate,SchedHours as Schedule, " + vbCrLf)
    '        .Append(" Case when t6.UnitTypeDescrip='Present Absent' and ActualHours=1.00 Then t2.Total else ActualHours end as ActualHours, " + vbCrLf)
    '        .Append(" (DatePart(dw,RecordDate)-1) as dw1,t2.dw,t1.Comments as comments, " + vbCrLf)
    '        .Append(" IsTardy as StudentIsTardy,t5.TrackTardies, t5.tardiesMakingAbsence,t5.PrgVerId, t5.PrgVerDescrip, " + vbCrLf)
    '        .Append(" t3.CampusId,syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip, " + vbCrLf)
    '        .Append(" (SELECT Top 1 CampDescrip FROM syCampuses WHERE CampusId=t3.CampusId) AS CampusDescrip " + vbCrLf)
    '        .Append(" from " + vbCrLf)
    '        .Append(" arStudentClockAttendance t1,arProgScheduleDetails t2, " + vbCrLf)
    '        If Not paramInfo.TermId.ToString.Trim = "" Then
    '            .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
    '            .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
    '            .Append("   arResults, arClassSections, arStuEnrollments " + vbCrLf)
    '            .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
    '            .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
    '            .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
    '            If paramInfo.PrgVerId <> "" Then
    '                .Append(" and arStuEnrollments.PrgVerId in ('")
    '                .Append(paramInfo.PrgVerId)
    '                .Append("')  " + vbCrLf)
    '            End If
    '            .Append(" Union " + vbCrLf)
    '            .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
    '            .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
    '            .Append("   from arTransferGrades,arStuEnrollments where  " + vbCrLf)
    '            .Append("   arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId   " + vbCrLf)
    '            .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
    '            If paramInfo.PrgVerId.ToString.Trim <> "" Then
    '                .Append(" and arStuEnrollments.PrgVerId in ('")
    '                .Append(paramInfo.PrgVerId)
    '                .Append("')  " + vbCrLf)
    '            End If
    '            .Append(") t3, " + vbCrLf)
    '        ElseIf Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
    '            .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
    '            .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
    '            .Append("   arResults, arClassSections, arStuEnrollments,arTerm " + vbCrLf)
    '            .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
    '            .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
    '            .Append("   and arClassSections.TermId=arTerm.TermId " + vbCrLf)
    '            If Not paramInfo.TermStartDate = "01/01/1900" Then
    '                .Append(" and arTerm.StartDate >= ? ")
    '            End If
    '            If Not paramInfo.TermEndDate = "01/01/1900" Then
    '                .Append(" and arTerm.StartDate <= ? ")
    '            End If
    '            If paramInfo.PrgVerId <> "" Then
    '                .Append(" and arStuEnrollments.PrgVerId in ('")
    '                .Append(paramInfo.PrgVerId)
    '                .Append("')  " + vbCrLf)
    '            End If
    '            .Append(" Union " + vbCrLf)
    '            .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
    '            .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
    '            .Append("   from arTransferGrades,arStuEnrollments,arTerm where  " + vbCrLf)
    '            .Append("   arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId   " + vbCrLf)
    '            .Append("   and arTransferGrades.TermId=arTerm.TermId " + vbCrLf)
    '            If Not paramInfo.TermStartDate = "01/01/1900" Then
    '                .Append(" and arTerm.StartDate >= ? ")
    '            End If
    '            If Not paramInfo.TermEndDate = "01/01/1900" Then
    '                .Append(" and arTerm.StartDate <= ? ")
    '            End If
    '            If paramInfo.PrgVerId.ToString.Trim <> "" Then
    '                .Append(" and arStuEnrollments.PrgVerId in ('")
    '                .Append(paramInfo.PrgVerId)
    '                .Append("')  " + vbCrLf)
    '            End If
    '            .Append(") t3, " + vbCrLf)
    '        Else
    '            .Append(" (Select * from arStuEnrollments " + vbCrLf)
    '            If paramInfo.PrgVerId.ToString.Trim <> "" Then
    '                .Append(" where arStuEnrollments.PrgVerId in ('")
    '                .Append(paramInfo.PrgVerId)
    '                .Append("')  " + vbCrLf)
    '            End If
    '            .Append(") t3, " + vbCrLf)
    '        End If
    '        .Append(" arStudent t4,arPrgVersions t5,arAttUnitType t6,  " + vbCrLf)
    '        .Append(" syCampGrps ,syCmpGrpCmps I " + vbCrLf)
    '        .Append(" where ")
    '        .Append("   t1.ScheduleId=t2.ScheduleId and t1.StuEnrollId=t3.StuEnrollId and " + vbCrLf)
    '        .Append("   t3.StudentId=t4.StudentId and t3.PrgVerId = t5.PrgVerId and t5.UnitTypeId=t6.UnitTypeId and " + vbCrLf)
    '        .Append("   syCampGrps.CampGrpId=I.CampGrpId AND t3.CampusId=I.CampusId " + vbCrLf)
    '        .Append("  and (DatePart(dw,RecordDate)-1)=t2.dw and " + vbCrLf)
    '        .Append("   (t1.ActualHours is not null AND t1.ActualHours <> 999.00 AND t1.ActualHours <> 9999.00) " + vbCrLf)
    '        If Not paramInfo.FirstName.ToString.Trim = "" Then
    '            .Append(" and t4.FirstName like  + ? + '%'")
    '        End If
    '        If Not paramInfo.LastName.ToString.Trim = "" Then
    '            .Append(" and t4.LastName like  + ? + '%'")
    '        End If
    '        If Not paramInfo.StartDate = "01/01/1900" Then
    '            .Append(" and RecordDate >= ? ")
    '        End If
    '        If Not paramInfo.EndDate = "01/01/1900" Then
    '            .Append(" and RecordDate <= ? ")
    '        End If
    '        .Append(" and syCampGrps.CampGrpId in  " + vbCrLf)
    '        .Append(" (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in ('")
    '        .Append(paramInfo.CampGrpId)
    '        .Append("))  " + vbCrLf)
    '        .Append(" Group by syCampGrps.CampGrpId,t3.CampusId,t4.StudentId,t1.StuEnrollId,LastName, " + vbCrLf)
    '        .Append(" FirstName,RecordDate,SchedHours,ActualHours,t2.Total,dw,t1.Comments,IsTardy,TrackTardies,tardiesMakingAbsence, " + vbCrLf)
    '        .Append(" t5.PrgVerId, t5.PrgVerDescrip, syCampGrps.CampGrpDescrip,UnitTypeDescrip ")
    '        If SingletonAppSettings.AppSettings("GradesFormat",CampusId).ToString.ToLower = "numeric" Then
    '            .Append(" union ")
    '            .Append(" select Distinct " + vbCrLf)
    '            .Append(" t4.FirstName,t4.LastName,t1.StuEnrollId,t4.StudentId, " + vbCrLf)
    '            .Append(" Convert(char(10),MeetDate,101) as MeetDate,Schedule as Schedule, " + vbCrLf)
    '            .Append(" Actual as ActualHours, " + vbCrLf)
    '            .Append(" NULL,NULL,t1.Comments as comments, " + vbCrLf)
    '            .Append(" Tardy as StudentIsTardy,t5.TrackTardies, t5.tardiesMakingAbsence,t5.PrgVerId, t5.PrgVerDescrip, " + vbCrLf)
    '            .Append(" t3.CampusId,syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip, " + vbCrLf)
    '            .Append(" (SELECT Top 1 CampDescrip FROM syCampuses WHERE CampusId=t3.CampusId) AS CampusDescrip " + vbCrLf)
    '            .Append(" from " + vbCrLf)
    '            .Append(" atConversionAttendance t1,arProgScheduleDetails t2, " + vbCrLf)
    '            If Not paramInfo.TermId.ToString.Trim = "" Then
    '                .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
    '                .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
    '                .Append("   arResults, arClassSections, arStuEnrollments " + vbCrLf)
    '                .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
    '                .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
    '                .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
    '                If paramInfo.PrgVerId.ToString.Trim <> "" Then
    '                    .Append(" and arStuEnrollments.PrgVerId in ('")
    '                    .Append(paramInfo.PrgVerId)
    '                    .Append("')  " + vbCrLf)
    '                End If
    '                .Append(" Union " + vbCrLf)
    '                .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
    '                .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
    '                .Append("   from arTransferGrades,arStuEnrollments where  " + vbCrLf)
    '                .Append("   arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId  " + vbCrLf)
    '                .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
    '                If paramInfo.PrgVerId.ToString.Trim <> "" Then
    '                    .Append(" and arStuEnrollments.PrgVerId in ('")
    '                    .Append(paramInfo.PrgVerId)
    '                    .Append("')  " + vbCrLf)
    '                End If
    '                .Append(") t3, " + vbCrLf)
    '            ElseIf Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
    '                .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
    '                .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
    '                .Append("   arResults, arClassSections, arStuEnrollments,arTerm " + vbCrLf)
    '                .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
    '                .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
    '                .Append("   and arClassSections.TermId=arTerm.TermId " + vbCrLf)
    '                If Not paramInfo.TermStartDate = "01/01/1900" Then
    '                    .Append(" and arTerm.StartDate >= ? ")
    '                End If
    '                If Not paramInfo.TermEndDate = "01/01/1900" Then
    '                    .Append(" and arTerm.StartDate <= ? ")
    '                End If
    '                If paramInfo.PrgVerId <> "" Then
    '                    .Append(" and arStuEnrollments.PrgVerId in ('")
    '                    .Append(paramInfo.PrgVerId)
    '                    .Append("')  " + vbCrLf)
    '                End If
    '                .Append(" Union " + vbCrLf)
    '                .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
    '                .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
    '                .Append("   from arTransferGrades,arStuEnrollments,arTerm where  " + vbCrLf)
    '                .Append("   arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId   " + vbCrLf)
    '                .Append("   and arTransferGrades.TermId=arTerm.TermId " + vbCrLf)
    '                If Not paramInfo.TermStartDate = "01/01/1900" Then
    '                    .Append(" and arTerm.StartDate >= ? ")
    '                End If
    '                If Not paramInfo.TermEndDate = "01/01/1900" Then
    '                    .Append(" and arTerm.StartDate <= ? ")
    '                End If
    '                If paramInfo.PrgVerId.ToString.Trim <> "" Then
    '                    .Append(" and arStuEnrollments.PrgVerId in ('")
    '                    .Append(paramInfo.PrgVerId)
    '                    .Append("')  " + vbCrLf)
    '                End If
    '                .Append(") t3, " + vbCrLf)
    '            Else
    '                .Append(" (Select * from arStuEnrollments " + vbCrLf)
    '                If paramInfo.PrgVerId.ToString.Trim <> "" Then
    '                    .Append(" where arStuEnrollments.PrgVerId in ('")
    '                    .Append(paramInfo.PrgVerId)
    '                    .Append("')  " + vbCrLf)
    '                End If
    '                .Append(") t3, " + vbCrLf)
    '            End If
    '            .Append("   arStudent t4,arPrgVersions t5,  " + vbCrLf)
    '            .Append(" syCampGrps ,syCmpGrpCmps I " + vbCrLf)
    '            .Append(" where ")
    '            .Append("   t1.StuEnrollId=t3.StuEnrollId and " + vbCrLf)
    '            .Append("   t3.StudentId=t4.StudentId and t3.PrgVerId = t5.PrgVerId and " + vbCrLf)
    '            .Append("   syCampGrps.CampGrpId=I.CampGrpId AND t3.CampusId=I.CampusId " + vbCrLf)
    '            .Append("   and (DatePart(dw,MeetDate)-1)=t2.dw and " + vbCrLf)
    '            .Append("   (t1.Actual is not null AND t1.Actual <> 999.00 AND t1.Actual <> 9999.00) " + vbCrLf)
    '            If Not paramInfo.FirstName.ToString.Trim = "" Then
    '                .Append(" and t4.FirstName like  + ? + '%'")
    '            End If
    '            If Not paramInfo.LastName.ToString.Trim = "" Then
    '                .Append(" and t4.LastName like  + ? + '%'")
    '            End If
    '            If Not paramInfo.StartDate = "01/01/1900" Then
    '                .Append(" and MeetDate >= ? ")
    '            End If
    '            If Not paramInfo.EndDate = "01/01/1900" Then
    '                .Append(" and MeetDate <= ? ")
    '            End If
    '            .Append(" and syCampGrps.CampGrpId in  " + vbCrLf)
    '            .Append(" (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in ('")
    '            .Append(paramInfo.CampGrpId)
    '            .Append("))  " + vbCrLf)
    '            .Append(" Group by syCampGrps.CampGrpId,t3.CampusId,t4.StudentId,t1.StuEnrollId,LastName, " + vbCrLf)
    '            .Append(" FirstName,MeetDate,Schedule,Actual,Tardy,t1.comments,TrackTardies,tardiesMakingAbsence, " + vbCrLf)
    '            .Append(" t5.PrgVerId, t5.PrgVerDescrip, syCampGrps.CampGrpDescrip " + vbCrLf)
    '        End If
    '        .Append(" Order by syCampGrps.CampGrpDescrip,t3.CampusId,t4.LastName,t4.FirstName,MeetDate " + vbCrLf)
    '    End With
    '    If Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
    '        If Not paramInfo.TermStartDate = "01/01/1900" Then
    '            db.AddParameter("@TermStartDate", paramInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        End If
    '        If Not paramInfo.TermEndDate = "01/01/1900" Then
    '            db.AddParameter("@TermEndDate", paramInfo.TermEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        End If
    '        If Not paramInfo.TermStartDate = "01/01/1900" Then
    '            db.AddParameter("@TermStartDate", paramInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        End If
    '        If Not paramInfo.TermEndDate = "01/01/1900" Then
    '            db.AddParameter("@TermEndDate", paramInfo.TermEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        End If
    '    End If
    '    If Not paramInfo.FirstName.ToString.Trim = "" Then
    '        db.AddParameter("@FirstName", paramInfo.FirstName.ToString.Trim, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    End If
    '    If Not paramInfo.LastName.ToString.Trim = "" Then
    '        db.AddParameter("@LastName", paramInfo.LastName.ToString.Trim, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    End If
    '    If Not paramInfo.StartDate = "01/01/1900" Then
    '        db.AddParameter("@StartDate", paramInfo.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '    End If
    '    If Not paramInfo.EndDate = "01/01/1900" Then
    '        db.AddParameter("@EndDate", paramInfo.EndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '    End If
    '    If SingletonAppSettings.AppSettings("GradesFormat",CampusId).ToString.ToLower = "numeric" Then
    '        If Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
    '            If Not paramInfo.TermStartDate = "01/01/1900" Then
    '                db.AddParameter("@TermStartDate", paramInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            End If
    '            If Not paramInfo.TermEndDate = "01/01/1900" Then
    '                db.AddParameter("@TermEndDate", paramInfo.TermEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            End If
    '            If Not paramInfo.TermStartDate = "01/01/1900" Then
    '                db.AddParameter("@TermStartDate", paramInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            End If
    '            If Not paramInfo.TermEndDate = "01/01/1900" Then
    '                db.AddParameter("@TermEndDate", paramInfo.TermEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            End If
    '        End If
    '        If Not paramInfo.FirstName.ToString.Trim = "" Then
    '            db.AddParameter("@FirstName", paramInfo.FirstName.ToString.Trim, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If
    '        If Not paramInfo.LastName.ToString.Trim = "" Then
    '            db.AddParameter("@LastName", paramInfo.LastName.ToString.Trim, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If
    '        If Not paramInfo.StartDate = "01/01/1900" Then
    '            db.AddParameter("@StartDate", paramInfo.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        End If
    '        If Not paramInfo.EndDate = "01/01/1900" Then
    '            db.AddParameter("@EndDate", paramInfo.EndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        End If
    '    End If
    '    Try
    '        Return db.RunParamSQLDataSet(sb.ToString)
    '    Catch ex As System.Exception
    '    Finally
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)
    '    End Try


    '    'If school is using timeclock then use time clock will be set to 1 in Program Version page
    '    'ElseIf UseTimeClock = True Then
    '    '    With sb
    '    '        .Append(" select Distinct " + vbCrLf)
    '    '        .Append(" t4.FirstName,t4.LastName,t1.StuEnrollId,t4.StudentId, " + vbCrLf)
    '    '        .Append(" Convert(char(10),RecordDate,101) as MeetDate,SchedHours as Schedule, " + vbCrLf)
    '    '        .Append(" ActualHours, " + vbCrLf)
    '    '        .Append(" (DatePart(dw,RecordDate)-1) as dw1,t2.dw,NULL as comments, " + vbCrLf)
    '    '        .Append(" Case When t2.Check_Tardyin=1 and Convert(char(10),t6.PunchTime,101) = Convert(char(10),RecordDate,101) and " + vbCrLf)
    '    '        .Append(" Convert(char(8),t6.PunchTime,108) < Convert(char(8),t2.max_beforetardy,108) " + vbCrLf)
    '    '        .Append(" Then  1 else 0 End as StudentIsTardy, " + vbCrLf)
    '    '        .Append(" t5.TrackTardies, t5.tardiesMakingAbsence,t5.PrgVerId, t5.PrgVerDescrip, " + vbCrLf)
    '    '        .Append(" t3.CampusId,syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip, " + vbCrLf)
    '    '        .Append(" (SELECT Top 1 CampDescrip FROM syCampuses WHERE CampusId=t3.CampusId) AS CampusDescrip " + vbCrLf)
    '    '        .Append(" from " + vbCrLf)
    '    '        .Append(" arStudentClockAttendance t1,arProgScheduleDetails t2, " + vbCrLf)
    '    '        If Not paramInfo.TermId.ToString.Trim = "" Then
    '    '            .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
    '    '            .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
    '    '            .Append("   arResults, arClassSections, arStuEnrollments " + vbCrLf)
    '    '            .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
    '    '            .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
    '    '            .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
    '    '            If paramInfo.PrgVerId <> "" Then
    '    '                .Append(" and arStuEnrollments.PrgVerId in ('")
    '    '                .Append(paramInfo.PrgVerId)
    '    '                .Append("')  " + vbCrLf)
    '    '            End If
    '    '            .Append(" Union " + vbCrLf)
    '    '            .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
    '    '            .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
    '    '            .Append("   from arTransferGrades,arStuEnrollments where  " + vbCrLf)
    '    '            .Append("   arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId   " + vbCrLf)
    '    '            .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
    '    '            If paramInfo.PrgVerId.ToString.Trim <> "" Then
    '    '                .Append(" and arStuEnrollments.PrgVerId in ('")
    '    '                .Append(paramInfo.PrgVerId)
    '    '                .Append("')  " + vbCrLf)
    '    '            End If
    '    '            .Append(") t3, " + vbCrLf)
    '    '        Else
    '    '            .Append(" (Select * from arStuEnrollments " + vbCrLf)
    '    '            If paramInfo.PrgVerId.ToString.Trim <> "" Then
    '    '                .Append(" where arStuEnrollments.PrgVerId in ('")
    '    '                .Append(paramInfo.PrgVerId)
    '    '                .Append("')  " + vbCrLf)
    '    '            End If
    '    '            .Append(") t3, " + vbCrLf)
    '    '        End If
    '    '        .Append(" arStudent t4,arPrgVersions t5,  " + vbCrLf)
    '    '        .Append(" arStudentTimeClockPunches t6, " + vbCrLf)
    '    '        .Append(" syCampGrps ,syCmpGrpCmps I " + vbCrLf)
    '    '        .Append(" where ")
    '    '        .Append("   t1.ScheduleId=t2.ScheduleId and t1.StuEnrollId=t3.StuEnrollId and " + vbCrLf)
    '    '        .Append("   t3.StudentId=t4.StudentId and t3.PrgVerId = t5.PrgVerId and " + vbCrLf)
    '    '        .Append("   t6.StuEnrollID = t3.StuEnrollId and t6.PunchType=1 and " + vbCrLf)
    '    '        .Append("   syCampGrps.CampGrpId=I.CampGrpId AND t3.CampusId=I.CampusId " + vbCrLf)
    '    '        .Append("  and (DatePart(dw,RecordDate)-1)=t2.dw and " + vbCrLf)
    '    '        .Append("   (t1.ActualHours is not null AND t1.ActualHours <> 999.00 AND t1.ActualHours <> 9999.00) " + vbCrLf)
    '    '        If Not paramInfo.FirstName.ToString.Trim = "" Then
    '    '            .Append(" and t4.FirstName like  + ? + '%'")
    '    '        End If
    '    '        If Not paramInfo.LastName.ToString.Trim = "" Then
    '    '            .Append(" and t4.LastName like  + ? + '%'")
    '    '        End If
    '    '        If Not paramInfo.StartDate = "01/01/1900" Then
    '    '            .Append(" and RecordDate >= ? ")
    '    '        End If
    '    '        If Not paramInfo.EndDate = "01/01/1900" Then
    '    '            .Append(" and RecordDate <= ? ")
    '    '        End If
    '    '        .Append(" and syCampGrps.CampGrpId in  " + vbCrLf)
    '    '        .Append(" (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in ('")
    '    '        .Append(paramInfo.CampGrpId)
    '    '        .Append("'))  " + vbCrLf)
    '    '        .Append(" Group by syCampGrps.CampGrpId,t3.CampusId,t4.StudentId,t1.StuEnrollId,LastName, " + vbCrLf)
    '    '        .Append(" FirstName,RecordDate,SchedHours,ActualHours,t2.Total,dw,IsTardy,TrackTardies,tardiesMakingAbsence, " + vbCrLf)
    '    '        .Append(" t5.PrgVerId, t5.PrgVerDescrip, syCampGrps.CampGrpDescrip ")
    '    '        If SingletonAppSettings.AppSettings("GradesFormat",CampusId).ToString.ToLower = "numeric" Then
    '    '            .Append(" union ")
    '    '            .Append(" select Distinct " + vbCrLf)
    '    '            .Append(" t4.FirstName,t4.LastName,t1.StuEnrollId,t4.StudentId, " + vbCrLf)
    '    '            .Append(" Convert(char(10),MeetDate,101) as MeetDate,Schedule as Schedule, " + vbCrLf)
    '    '            .Append(" Actual as ActualHours, " + vbCrLf)
    '    '            .Append(" NULL,NULL,NULL as comments, " + vbCrLf)
    '    '            .Append(" Case When t2.Check_Tardyin=1 and Convert(char(10),t6.PunchTime,101) = Convert(char(10),MeetDate,101) and " + vbCrLf)
    '    '            .Append(" Convert(char(8),t6.PunchTime,108) < Convert(char(8),t2.max_beforetardy,108) " + vbCrLf)
    '    '            .Append(" Then  1 else 0 End as StudentIsTardy, " + vbCrLf)
    '    '            .Append(" t5.TrackTardies, t5.tardiesMakingAbsence,t5.PrgVerId, t5.PrgVerDescrip, " + vbCrLf)
    '    '            .Append(" t3.CampusId,syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip, " + vbCrLf)
    '    '            .Append(" (SELECT Top 1 CampDescrip FROM syCampuses WHERE CampusId=t3.CampusId) AS CampusDescrip " + vbCrLf)
    '    '            .Append(" from " + vbCrLf)
    '    '            .Append(" atConversionAttendance t1,arProgScheduleDetails t2, " + vbCrLf)
    '    '            If Not paramInfo.TermId.ToString.Trim = "" Then
    '    '                .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
    '    '                .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
    '    '                .Append("   arResults, arClassSections, arStuEnrollments " + vbCrLf)
    '    '                .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
    '    '                .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
    '    '                .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
    '    '                If paramInfo.PrgVerId.ToString.Trim <> "" Then
    '    '                    .Append(" and arStuEnrollments.PrgVerId in ('")
    '    '                    .Append(paramInfo.PrgVerId)
    '    '                    .Append("')  " + vbCrLf)
    '    '                End If
    '    '                .Append(" Union " + vbCrLf)
    '    '                .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
    '    '                .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
    '    '                .Append("   from arTransferGrades,arStuEnrollments where  " + vbCrLf)
    '    '                .Append("   arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId  " + vbCrLf)
    '    '                .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
    '    '                If paramInfo.PrgVerId.ToString.Trim <> "" Then
    '    '                    .Append(" and arStuEnrollments.PrgVerId in ('")
    '    '                    .Append(paramInfo.PrgVerId)
    '    '                    .Append("')  " + vbCrLf)
    '    '                End If
    '    '                .Append(") t3, " + vbCrLf)
    '    '            Else
    '    '                .Append(" (Select * from arStuEnrollments " + vbCrLf)
    '    '                If paramInfo.PrgVerId.ToString.Trim <> "" Then
    '    '                    .Append(" where arStuEnrollments.PrgVerId in ('")
    '    '                    .Append(paramInfo.PrgVerId)
    '    '                    .Append("')  " + vbCrLf)
    '    '                End If
    '    '                .Append(") t3, " + vbCrLf)
    '    '            End If
    '    '            .Append("   arStudent t4,arPrgVersions t5,  " + vbCrLf)
    '    '            .Append(" arStudentTimeClockPunches t6, " + vbCrLf)
    '    '            .Append(" syCampGrps ,syCmpGrpCmps I " + vbCrLf)
    '    '            .Append(" where ")
    '    '            .Append("   t1.StuEnrollId=t3.StuEnrollId and " + vbCrLf)
    '    '            .Append("   t3.StudentId=t4.StudentId and t3.PrgVerId = t5.PrgVerId and " + vbCrLf)
    '    '            .Append("   t6.StuEnrollID = t3.StuEnrollId and t6.PunchType=1 and " + vbCrLf)
    '    '            .Append("   syCampGrps.CampGrpId=I.CampGrpId AND t3.CampusId=I.CampusId " + vbCrLf)
    '    '            .Append("   and (DatePart(dw,MeetDate)-1)=t2.dw and " + vbCrLf)
    '    '            .Append("   (t1.Actual is not null AND t1.Actual <> 999.00 AND t1.Actual <> 9999.00) " + vbCrLf)
    '    '            If Not paramInfo.FirstName.ToString.Trim = "" Then
    '    '                .Append(" and t4.FirstName like  + ? + '%'")
    '    '            End If
    '    '            If Not paramInfo.LastName.ToString.Trim = "" Then
    '    '                .Append(" and t4.LastName like  + ? + '%'")
    '    '            End If
    '    '            If Not paramInfo.StartDate = "01/01/1900" Then
    '    '                .Append(" and MeetDate >= ? ")
    '    '            End If
    '    '            If Not paramInfo.EndDate = "01/01/1900" Then
    '    '                .Append(" and MeetDate <= ? ")
    '    '            End If
    '    '            .Append(" and syCampGrps.CampGrpId in  " + vbCrLf)
    '    '            .Append(" (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in ('")
    '    '            .Append(paramInfo.CampGrpId)
    '    '            .Append("'))  " + vbCrLf)
    '    '            .Append(" Group by syCampGrps.CampGrpId,t3.CampusId,t4.StudentId,t1.StuEnrollId,LastName, " + vbCrLf)
    '    '            .Append(" FirstName,MeetDate,Schedule,Actual,Tardy,TrackTardies,tardiesMakingAbsence, " + vbCrLf)
    '    '            .Append(" t5.PrgVerId, t5.PrgVerDescrip, syCampGrps.CampGrpDescrip " + vbCrLf)
    '    '        End If
    '    '        .Append(" Order by syCampGrps.CampGrpDescrip,t3.CampusId,t4.LastName,t4.FirstName,MeetDate " + vbCrLf)
    '    '    End With
    '    '    Return db.RunParamSQLDataSet(sb.ToString)
    '    ' End If
    'End Function
    Private Function GetExternshipDetails(ByVal paramInfo As ReportParamInfo, _
                                            Optional ByVal AttUnitType As String = "", _
                                            Optional ByVal PrgVerId As String = "", _
                                            Optional ByVal UseTimeClock As Boolean = False) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb

            .Append(" select Distinct " + vbCrLf)
            .Append(" t4.FirstName,t4.LastName,t1.StuEnrollId,t4.StudentId, " + vbCrLf)
            .Append(" Convert(char(10),AttendedDate,101) as MeetDate, " + vbCrLf)

            .Append(" HoursAttended as  ActualHours, " + vbCrLf)
            .Append(" (DatePart(dw,AttendedDate)-1) as dw1,t2.dw,t1.Comments as comments, " + vbCrLf)
            .Append(" t5.PrgVerId, t5.PrgVerDescrip, " + vbCrLf)
            .Append(" t3.CampusId,syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip, " + vbCrLf)
            .Append(" (SELECT Top 1 CampDescrip FROM syCampuses WHERE CampusId=t3.CampusId) AS CampusDescrip " + vbCrLf)
            .Append("  from " + vbCrLf)
            .Append(" arExternshipAttendance t1,arProgScheduleDetails t2, " + vbCrLf)
            If Not paramInfo.TermId.ToString.Trim = "" Then
                .Append("  (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
                .Append("    arResults, arClassSections, arStuEnrollments " + vbCrLf)
                If Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(",arTerm " + vbCrLf)
                End If
                .Append("    where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
                .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
                If Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(" and arClassSections.termId=arTerm.termid " + vbCrLf)
                End If
                .Append("    and arClassSections.TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
                If Not paramInfo.TermStartDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate >='" & paramInfo.TermStartDate & "'" + vbCrLf)
                End If
                If Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate <='" & paramInfo.TermEndDate & "'" + vbCrLf)
                End If
                If paramInfo.PrgVerId.ToString.Trim <> "" Then
                    .Append(" and arStuEnrollments.PrgVerId in ('")
                    .Append(paramInfo.PrgVerId)
                    .Append("')  " + vbCrLf)
                End If


                .Append(" Union " + vbCrLf)
                .Append("    Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                .Append("    arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
                .Append("   from arTransferGrades,arStuEnrollments   " + vbCrLf)
                If Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(",arTerm " + vbCrLf)
                End If
                .Append("  where  arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId   " + vbCrLf)
                If Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(" and arTransferGrades.termId=arTerm.termid " + vbCrLf)
                End If
                .Append("   and arTransferGrades.TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
                If Not paramInfo.TermStartDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate >='" & paramInfo.TermStartDate & "'" + vbCrLf)
                End If
                If Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate <= '" & paramInfo.TermEndDate & "'" + vbCrLf)
                End If
                If paramInfo.PrgVerId.ToString.Trim <> "" Then
                    .Append(" and arStuEnrollments.PrgVerId in ('")
                    .Append(paramInfo.PrgVerId)
                    .Append("')  " + vbCrLf)
                End If


                .Append(") t3, " + vbCrLf)
            ElseIf Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
                .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                .Append("  arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
                .Append("  arResults, arClassSections, arStuEnrollments,arTerm " + vbCrLf)
                .Append("  where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
                .Append("  arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
                .Append("  and arClassSections.TermId=arTerm.TermId " + vbCrLf)
                If Not paramInfo.TermStartDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate >='" & paramInfo.TermStartDate & "'")
                End If
                If Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate <= '" & paramInfo.TermEndDate & "'")
                End If
                If Not paramInfo.TermId.ToString.Trim = "" Then
                    .Append("   and arClassSections.TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
                End If
                If paramInfo.PrgVerId.ToString.Trim <> "" Then
                    .Append(" and arStuEnrollments.PrgVerId in ('")
                    .Append(paramInfo.PrgVerId)
                    .Append("')  " + vbCrLf)
                End If
                .Append(" Union  " + vbCrLf)
                .Append(" Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId,  " + vbCrLf)
                .Append(" arStuEnrollments.StudentId, arStuEnrollments.CampusId  " + vbCrLf)
                .Append(" from arTransferGrades,arStuEnrollments,arTerm where   " + vbCrLf)
                .Append(" arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId    " + vbCrLf)
                .Append(" and arTransferGrades.TermId=arTerm.TermId  " + vbCrLf)
                If Not paramInfo.TermStartDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate >='" & paramInfo.TermStartDate & "'")
                End If
                If Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate <= '" & paramInfo.TermEndDate & "'")
                End If
                If Not paramInfo.TermId.ToString.Trim = "" Then
                    .Append("   and arTransferGrades.TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
                End If
                If paramInfo.PrgVerId.ToString.Trim <> "" Then
                    .Append(" and arStuEnrollments.PrgVerId in ('")
                    .Append(paramInfo.PrgVerId)
                    .Append("')  " + vbCrLf)
                End If
                .Append(" ) t3,  " + vbCrLf)
            Else
                .Append(" (Select * from arStuEnrollments " + vbCrLf)
                If paramInfo.PrgVerId.ToString.Trim <> "" Then

                    .Append("    where arStuEnrollments.PrgVerId in ('")
                    .Append(paramInfo.PrgVerId)
                    .Append("')  " + vbCrLf)
                End If
                .Append("       ) t3,  " + vbCrLf)
            End If
            .Append(" arStudent t4,arPrgVersions t5,   " + vbCrLf)
            .Append(" syCampGrps ,syCmpGrpCmps I  " + vbCrLf)
            .Append(" where  " + vbCrLf)
            .Append(" t1.StuEnrollId=t3.StuEnrollId and  " + vbCrLf)
            .Append(" t3.StudentId=t4.StudentId and t3.PrgVerId = t5.PrgVerId and  " + vbCrLf)
            .Append("  syCampGrps.CampGrpId=I.CampGrpId AND t3.CampusId=I.CampusId  " + vbCrLf)
            .Append(" and (DatePart(dw,AttendedDate)-1)=t2.dw and  " + vbCrLf)
            .Append("  (t1.HoursAttended is not null )  " + vbCrLf)
            If Not paramInfo.FirstName.ToString.Trim = "" Then
                .Append(" and t4.FirstName like '" + paramInfo.FirstName.ToString.Trim + "%'" + vbCrLf)
            End If
            If Not paramInfo.LastName.ToString.Trim = "" Then
                .Append(" and t4.LastName like  '" + paramInfo.LastName.ToString.Trim + "%'" + vbCrLf)
            End If
            If Not paramInfo.StartDate = "01/01/1900" Then
                .Append(" and AttendedDate >='" & paramInfo.StartDate & "'" + vbCrLf)
            End If
            If Not paramInfo.EndDate = "01/01/1900" Then
                .Append(" and AttendedDate <='" & paramInfo.EndDate & "'" + vbCrLf)
            End If
            .Append(" and syCampGrps.CampGrpId in  " + vbCrLf)

            .Append(" (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in ('")
            .Append(paramInfo.CampGrpId)
            .Append("))  " + vbCrLf)
            .Append(" Group by syCampGrps.CampGrpId,t3.CampusId,t4.StudentId,t1.StuEnrollId,LastName, " + vbCrLf)
            .Append(" FirstName,AttendedDate,HoursAttended,t2.Total,dw,t1.Comments, " + vbCrLf)
            .Append(" t5.PrgVerId, t5.PrgVerDescrip, syCampGrps.CampGrpDescrip " + vbCrLf)

            .Append(" Order by syCampGrps.CampGrpDescrip,t3.CampusId,t4.LastName,t4.FirstName,MeetDate " + vbCrLf)
        End With

        Try
            Return db.RunParamSQLDataSet(sb.ToString)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try


        'If school is using timeclock then use time clock will be set to 1 in Program Version page
        'ElseIf UseTimeClock = True Then
        '    With sb
        '        .Append(" select Distinct " + vbCrLf)
        '        .Append(" t4.FirstName,t4.LastName,t1.StuEnrollId,t4.StudentId, " + vbCrLf)
        '        .Append(" Convert(char(10),RecordDate,101) as MeetDate,SchedHours as Schedule, " + vbCrLf)
        '        .Append(" ActualHours, " + vbCrLf)
        '        .Append(" (DatePart(dw,RecordDate)-1) as dw1,t2.dw,NULL as comments, " + vbCrLf)
        '        .Append(" Case When t2.Check_Tardyin=1 and Convert(char(10),t6.PunchTime,101) = Convert(char(10),RecordDate,101) and " + vbCrLf)
        '        .Append(" Convert(char(8),t6.PunchTime,108) < Convert(char(8),t2.max_beforetardy,108) " + vbCrLf)
        '        .Append(" Then  1 else 0 End as StudentIsTardy, " + vbCrLf)
        '        .Append(" t5.TrackTardies, t5.tardiesMakingAbsence,t5.PrgVerId, t5.PrgVerDescrip, " + vbCrLf)
        '        .Append(" t3.CampusId,syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip, " + vbCrLf)
        '        .Append(" (SELECT Top 1 CampDescrip FROM syCampuses WHERE CampusId=t3.CampusId) AS CampusDescrip " + vbCrLf)
        '        .Append(" from " + vbCrLf)
        '        .Append(" arStudentClockAttendance t1,arProgScheduleDetails t2, " + vbCrLf)
        '        If Not paramInfo.TermId.ToString.Trim = "" Then
        '            .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
        '            .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
        '            .Append("   arResults, arClassSections, arStuEnrollments " + vbCrLf)
        '            .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
        '            .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
        '            .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
        '            If paramInfo.PrgVerId <> "" Then
        '                .Append(" and arStuEnrollments.PrgVerId in ('")
        '                .Append(paramInfo.PrgVerId)
        '                .Append("')  " + vbCrLf)
        '            End If
        '            .Append(" Union " + vbCrLf)
        '            .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
        '            .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
        '            .Append("   from arTransferGrades,arStuEnrollments where  " + vbCrLf)
        '            .Append("   arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId   " + vbCrLf)
        '            .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
        '            If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '                .Append(" and arStuEnrollments.PrgVerId in ('")
        '                .Append(paramInfo.PrgVerId)
        '                .Append("')  " + vbCrLf)
        '            End If
        '            .Append(") t3, " + vbCrLf)
        '        Else
        '            .Append(" (Select * from arStuEnrollments " + vbCrLf)
        '            If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '                .Append(" where arStuEnrollments.PrgVerId in ('")
        '                .Append(paramInfo.PrgVerId)
        '                .Append("')  " + vbCrLf)
        '            End If
        '            .Append(") t3, " + vbCrLf)
        '        End If
        '        .Append(" arStudent t4,arPrgVersions t5,  " + vbCrLf)
        '        .Append(" arStudentTimeClockPunches t6, " + vbCrLf)
        '        .Append(" syCampGrps ,syCmpGrpCmps I " + vbCrLf)
        '        .Append(" where ")
        '        .Append("   t1.ScheduleId=t2.ScheduleId and t1.StuEnrollId=t3.StuEnrollId and " + vbCrLf)
        '        .Append("   t3.StudentId=t4.StudentId and t3.PrgVerId = t5.PrgVerId and " + vbCrLf)
        '        .Append("   t6.StuEnrollID = t3.StuEnrollId and t6.PunchType=1 and " + vbCrLf)
        '        .Append("   syCampGrps.CampGrpId=I.CampGrpId AND t3.CampusId=I.CampusId " + vbCrLf)
        '        .Append("  and (DatePart(dw,RecordDate)-1)=t2.dw and " + vbCrLf)
        '        .Append("   (t1.ActualHours is not null AND t1.ActualHours <> 999.00 AND t1.ActualHours <> 9999.00) " + vbCrLf)
        '        If Not paramInfo.FirstName.ToString.Trim = "" Then
        '            .Append(" and t4.FirstName like  + ? + '%'")
        '        End If
        '        If Not paramInfo.LastName.ToString.Trim = "" Then
        '            .Append(" and t4.LastName like  + ? + '%'")
        '        End If
        '        If Not paramInfo.StartDate = "01/01/1900" Then
        '            .Append(" and RecordDate >= ? ")
        '        End If
        '        If Not paramInfo.EndDate = "01/01/1900" Then
        '            .Append(" and RecordDate <= ? ")
        '        End If
        '        .Append(" and syCampGrps.CampGrpId in  " + vbCrLf)
        '        .Append(" (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in ('")
        '        .Append(paramInfo.CampGrpId)
        '        .Append("'))  " + vbCrLf)
        '        .Append(" Group by syCampGrps.CampGrpId,t3.CampusId,t4.StudentId,t1.StuEnrollId,LastName, " + vbCrLf)
        '        .Append(" FirstName,RecordDate,SchedHours,ActualHours,t2.Total,dw,IsTardy,TrackTardies,tardiesMakingAbsence, " + vbCrLf)
        '        .Append(" t5.PrgVerId, t5.PrgVerDescrip, syCampGrps.CampGrpDescrip ")
        '        If SingletonAppSettings.AppSettings("GradesFormat",CampusId).ToString.ToLower = "numeric" Then
        '            .Append(" union ")
        '            .Append(" select Distinct " + vbCrLf)
        '            .Append(" t4.FirstName,t4.LastName,t1.StuEnrollId,t4.StudentId, " + vbCrLf)
        '            .Append(" Convert(char(10),MeetDate,101) as MeetDate,Schedule as Schedule, " + vbCrLf)
        '            .Append(" Actual as ActualHours, " + vbCrLf)
        '            .Append(" NULL,NULL,NULL as comments, " + vbCrLf)
        '            .Append(" Case When t2.Check_Tardyin=1 and Convert(char(10),t6.PunchTime,101) = Convert(char(10),MeetDate,101) and " + vbCrLf)
        '            .Append(" Convert(char(8),t6.PunchTime,108) < Convert(char(8),t2.max_beforetardy,108) " + vbCrLf)
        '            .Append(" Then  1 else 0 End as StudentIsTardy, " + vbCrLf)
        '            .Append(" t5.TrackTardies, t5.tardiesMakingAbsence,t5.PrgVerId, t5.PrgVerDescrip, " + vbCrLf)
        '            .Append(" t3.CampusId,syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip, " + vbCrLf)
        '            .Append(" (SELECT Top 1 CampDescrip FROM syCampuses WHERE CampusId=t3.CampusId) AS CampusDescrip " + vbCrLf)
        '            .Append(" from " + vbCrLf)
        '            .Append(" atConversionAttendance t1,arProgScheduleDetails t2, " + vbCrLf)
        '            If Not paramInfo.TermId.ToString.Trim = "" Then
        '                .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
        '                .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
        '                .Append("   arResults, arClassSections, arStuEnrollments " + vbCrLf)
        '                .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
        '                .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
        '                .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
        '                If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '                    .Append(" and arStuEnrollments.PrgVerId in ('")
        '                    .Append(paramInfo.PrgVerId)
        '                    .Append("')  " + vbCrLf)
        '                End If
        '                .Append(" Union " + vbCrLf)
        '                .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
        '                .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
        '                .Append("   from arTransferGrades,arStuEnrollments where  " + vbCrLf)
        '                .Append("   arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId  " + vbCrLf)
        '                .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
        '                If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '                    .Append(" and arStuEnrollments.PrgVerId in ('")
        '                    .Append(paramInfo.PrgVerId)
        '                    .Append("')  " + vbCrLf)
        '                End If
        '                .Append(") t3, " + vbCrLf)
        '            Else
        '                .Append(" (Select * from arStuEnrollments " + vbCrLf)
        '                If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '                    .Append(" where arStuEnrollments.PrgVerId in ('")
        '                    .Append(paramInfo.PrgVerId)
        '                    .Append("')  " + vbCrLf)
        '                End If
        '                .Append(") t3, " + vbCrLf)
        '            End If
        '            .Append("   arStudent t4,arPrgVersions t5,  " + vbCrLf)
        '            .Append(" arStudentTimeClockPunches t6, " + vbCrLf)
        '            .Append(" syCampGrps ,syCmpGrpCmps I " + vbCrLf)
        '            .Append(" where ")
        '            .Append("   t1.StuEnrollId=t3.StuEnrollId and " + vbCrLf)
        '            .Append("   t3.StudentId=t4.StudentId and t3.PrgVerId = t5.PrgVerId and " + vbCrLf)
        '            .Append("   t6.StuEnrollID = t3.StuEnrollId and t6.PunchType=1 and " + vbCrLf)
        '            .Append("   syCampGrps.CampGrpId=I.CampGrpId AND t3.CampusId=I.CampusId " + vbCrLf)
        '            .Append("   and (DatePart(dw,MeetDate)-1)=t2.dw and " + vbCrLf)
        '            .Append("   (t1.Actual is not null AND t1.Actual <> 999.00 AND t1.Actual <> 9999.00) " + vbCrLf)
        '            If Not paramInfo.FirstName.ToString.Trim = "" Then
        '                .Append(" and t4.FirstName like  + ? + '%'")
        '            End If
        '            If Not paramInfo.LastName.ToString.Trim = "" Then
        '                .Append(" and t4.LastName like  + ? + '%'")
        '            End If
        '            If Not paramInfo.StartDate = "01/01/1900" Then
        '                .Append(" and MeetDate >= ? ")
        '            End If
        '            If Not paramInfo.EndDate = "01/01/1900" Then
        '                .Append(" and MeetDate <= ? ")
        '            End If
        '            .Append(" and syCampGrps.CampGrpId in  " + vbCrLf)
        '            .Append(" (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in ('")
        '            .Append(paramInfo.CampGrpId)
        '            .Append("'))  " + vbCrLf)
        '            .Append(" Group by syCampGrps.CampGrpId,t3.CampusId,t4.StudentId,t1.StuEnrollId,LastName, " + vbCrLf)
        '            .Append(" FirstName,MeetDate,Schedule,Actual,Tardy,TrackTardies,tardiesMakingAbsence, " + vbCrLf)
        '            .Append(" t5.PrgVerId, t5.PrgVerDescrip, syCampGrps.CampGrpDescrip " + vbCrLf)
        '        End If
        '        .Append(" Order by syCampGrps.CampGrpDescrip,t3.CampusId,t4.LastName,t4.FirstName,MeetDate " + vbCrLf)
        '    End With
        '    Return db.RunParamSQLDataSet(sb.ToString)
        ' End If
    End Function
    Private Function GetSAPCheckResults(ByVal paramInfo As ReportParamInfo, Optional ByVal StuEnrollId As String = "") As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim strWhere As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'Get StudentId and rest of Where Clause from paramInfo.FilterOther
        'Code Added by Vijay Ramteke On May 14,2009
        If StuEnrollId <> "" Then
            If paramInfo.FilterOther <> "" Then
                strWhere &= "t2.StuEnrollId='" & StuEnrollId & "' AND " & paramInfo.FilterOther
            Else
                strWhere &= "t2.StuEnrollId='" & StuEnrollId & "'"
            End If
        Else
            If paramInfo.FilterOther <> "" Then
                strWhere &= paramInfo.FilterOther
            End If

        End If
        strWhere = Replace(strWhere, "arStuEnrollments", "t2")
        strWhere = Replace(strWhere, "arSAPChkResults", "t7")

        With sb
            '.Append(" select Distinct t1.FirstName,t1.LastName,t1.SSN,t3.SAPID, " + vbCrLf)
            '.Append(" Convert(char(10),t2.StartDate,101) as ModuleStart, " + vbCrLf)
            '.Append(" (select Top 1 GPA from arSAPChkResults where StuEnrollId=t2.StuEnrollId AND " + strWhere + " order by Period desc) " + vbCrLf)
            '.Append(" as GPA, " + vbCrLf)
            '.Append(" Case when t4.TrigOffSetTypId=1 Then 'N/A' else t4.MinTermGPA end as GPARequired, " + vbCrLf)
            '.Append(" NULL as CGPA, " + vbCrLf)
            '.Append("   (Select Top 1 QualMinValue from arSAPDetails where SAPDetailId in " + vbCrLf)
            '.Append("   (select Top 1 SAPDetailId from arSAPChkResults where StuEnrollId=t2.StuEnrollId AND " + strWhere + "" + vbCrLf)
            '.Append("   order by Period desc)) as CGPARequired, " + vbCrLf)
            '.Append("   (Select Top 1 QuantMinValue from arSAPDetails where SAPDetailId in " + vbCrLf)
            '.Append("   (select Top 1 SAPDetailId from arSAPChkResults where StuEnrollId=t2.StuEnrollId AND " + strWhere + "" + vbCrLf)
            '.Append("   order by Period desc)) as CourseCompletion, " + vbCrLf)
            '.Append(" (Select Distinct Top 1 ConseqTypDesc from arConsequenceTyps where ConsequenceTypId=t5.ConsequenceTypId) as ConsequenceDescrip, " + vbCrLf)
            '.Append(" (select Top 1 PercentCompleted from arSAPChkResults where StuEnrollId=t2.StuEnrollId AND " + strWhere + " order by Period desc) " + vbCrLf)
            '.Append(" as CourseCompletionPercentage, " + vbCrLf)
            '.Append(" (select Top 1 Period from arSAPChkResults where StuEnrollId=t2.StuEnrollId AND " + strWhere + " order by Period desc) as Period, " + vbCrLf)
            '.Append("   (Select Top 1 MinAttendanceValue from arSAPDetails where SAPDetailId in " + vbCrLf)
            '.Append("   (select Top 1 SAPDetailId from arSAPChkResults where StuEnrollId=t2.StuEnrollId AND " + strWhere + " " + vbCrLf)
            '.Append("   order by Period desc)) as Attendance, " + vbCrLf)
            '.Append(" (select Top 1 Attendance from arSAPChkResults where StuEnrollId=t2.StuEnrollId AND " + strWhere + " order by Period desc) " + vbCrLf)
            '.Append(" as AttendancePercentage, " + vbCrLf)
            '.Append(" (select Top 1 CumFinAidCredits from arSAPChkResults where StuEnrollId=t2.StuEnrollId AND " + strWhere + " order by Period desc) " + vbCrLf)
            '.Append(" as CumFinAidCredits, " + vbCrLf)
            '.Append(" (select Top 1 IsMakingSAP from arSAPChkResults where StuEnrollId=t2.StuEnrollId AND " + strWhere + " order by Period desc) as IsMakingSAP,t6.ProgDescrip as Program " + vbCrLf)
            ''-code added by Priyanka on date 21st of May 2009
            '.Append(" , " + vbCrLf)
            '.Append(" (select Top 1 ModDate from arSAPChkResults where StuEnrollId=t2.StuEnrollId AND " + strWhere + " order by Period desc) as ModDate " + vbCrLf)
            ''.Append(" (select Top 1 CheckPointDate from arSAPChkResults where StuEnrollId=t2.StuEnrollId AND " + strWhere + " order by Period desc) as CheckPointDate " + vbCrLf)
            ''-
            '.Append("  from " + vbCrLf)
            '.Append(" arStudent t1,arStuEnrollments t2,arPrgVersions t3,arSAP t4,arSAPDetails t5,arPrograms t6 " + vbCrLf)
            'If strWhere <> "" Then
            '    If strWhere.Contains("arSAPChkResults.CheckPointDate") Then
            '        .Append(" ,arSAPChkResults " + vbCrLf)
            '    End If
            'End If
            '.Append("   where  " + vbCrLf)

            '.Append(" t1.StudentId=t2.StudentId and t2.PrgVerId=t3.PrgVerId and t3.SAPID=t4.SAPID and t3.ProgId=t6.ProgId and " + vbCrLf)
            '.Append(" t4.SAPID = t5.SAPID  and " + vbCrLf)

            'If strWhere <> "" Then
            '    If strWhere.Contains("arSAPChkResults.CheckPointDate") Then
            '        .Append(" arSAPChkResults.StuEnrollId=t2.StuEnrollId  AND " + vbCrLf)
            '    End If
            'End If

            '.Append(strWhere)

            .Append(" select Distinct t2.StuEnrollId,t1.FirstName,t1.LastName,t1.SSN,t4.SAPID,t7.SapDetailid, " + vbCrLf)
            .Append(" Convert(char(10),t2.StartDate,101) as ModuleStart, " + vbCrLf)
            .Append(" t7.GPA as GPA, " + vbCrLf)
            .Append(" Case when t4.TrigOffSetTypId=1 Then 'N/A' else t4.MinTermGPA end as GPARequired, " + vbCrLf)
            .Append(" NULL as CGPA, " + vbCrLf)
            .Append(" t5.QualMinValue  as CGPARequired,  " + vbCrLf)
            .Append(" t11.QuantMinValue as CourseCompletion, " + vbCrLf)
            .Append("  t8.ConseqTypDesc as ConsequenceDescrip, " + vbCrLf)
            .Append("  t12.PercentCompleted as CourseCompletionPercentage, " + vbCrLf)
            .Append("  t7.Period as Period, " + vbCrLf)
            .Append("  t5.MinAttendanceValue as Attendance, " + vbCrLf)
            .Append("  t7.Attendance as AttendancePercentage, " + vbCrLf)
            .Append("  t7.CumFinAidCredits as CumFinAidCredits, " + vbCrLf)

            .Append(" t7.IsMakingSAP as IsMakingSAP,t6.ProgDescrip as Program " + vbCrLf)
            '-code added by Priyanka on date 21st of May 2009
            .Append(" , " + vbCrLf)
            .Append(" t7.ModDate as ModDate, " + vbCrLf)
            .Append(" t7.CheckPointDate as CheckPointDate, " + vbCrLf)
            .Append(" t7.TermStartDate as TermStartDate " + vbCrLf)
            '-
            '-code added by Vijay on date Auguest 26, 2009
            .Append(" , Convert(varchar(10),t7.Period) +' : '+ Convert(varchar(10),cast(t5.TrigValue as float)/100) + ' ' + t10.TrigUnitTypDescrip +' after the ' + t9.TrigOffTypDescrip+ ' ' + ISNULL(Convert(varchar(10),TrigOffsetSeq),'')  as IncDesc,  " + vbCrLf)
            '.Append("(SELECT QuantMinTypDesc FROM dbo.arQuantMinUnitTyps WHERE QuantMinUnitTypId=t5.QuantMinUnitTypId) as QuantMinTypDesc
            .Append(" 'Percent of Scheduled ' + (SELECT InstructionTypeDescrip FROM dbo.arInstructionType WHERE InstructionTypeId=t11.InstructionTypeId) + ' Hours/Days' AS QuantMinTypDesc, ")
            .Append(" t5.mincredsCompltd, t7.CredsEarned, ")
            .Append(" t11.InstructionTypeId ")
            '-
            .Append("  from " + vbCrLf)
            .Append(" arStudent t1,arStuEnrollments t2,arPrgVersions t3,arSAP t4,arSAPDetails t5,arPrograms t6, arSAPChkResults t7, arConsequenceTyps t8 " + vbCrLf)
            'New code added by Vijay Ramteke on August 26, 2009
            .Append(" , arTrigOffsetTyps t9, arTrigUnitTyps t10,arSAPQuantByInstruction t11,arSAPQuantResults t12  " + vbCrLf)
            'If strWhere <> "" Then
            '    If strWhere.Contains("arSAPChkResults.CheckPointDate") Then
            '        .Append(" ,arSAPChkResults " + vbCrLf)
            '    End If
            'End If
            .Append("   where  " + vbCrLf)
            'modified by Theresa G on 11/18/2010 for Rally DE1115  QA: SAP: SAP report is not returning any results for some students.  
            '.Append(" t1.StudentId=t2.StudentId and t2.PrgVerId=t3.PrgVerId and t3.SAPID=t4.SAPID and t3.ProgId=t6.ProgId and " + vbCrLf)
            .Append(" t5.SAPDetailId=t11.SAPDetailId  ")
            .Append(" AND t11.InstructionTypeId=t12.InstructionTypeId AND t12.StuEnrollId=t2.StuEnrollId AND t12.SAPDetailId=t5.SAPDetailId and ")

            .Append(" t1.StudentId=t2.StudentId and t2.PrgVerId=t3.PrgVerId and t3.ProgId=t6.ProgId and " + vbCrLf)
            .Append(" t4.SAPID = t5.SAPID and t7.StuEnrollId=t2.StuEnrollId and t8.ConsequenceTypId=t5.ConsequenceTypId and t5.SAPDetailId=t7.SAPDetailId and " + vbCrLf)
            'New code added by Vijay Ramteke on August 26, 2009
            .Append(" t5.TrigOffsetTypId=t9.TrigOffsetTypId and t5.TrigUnitTypId=t10.TrigUnitTypId and t7.PreviewSapCheck=0 and " + vbCrLf)
            'If strWhere <> "" Then
            '    If strWhere.Contains("arSAPChkResults.CheckPointDate") Then
            '        .Append(" arSAPChkResults.StuEnrollId=t2.StuEnrollId  AND " + vbCrLf)
            '    End If
            'End If

            .Append(strWhere)
            .Append(" UNION ")
            .Append(" select Distinct t2.StuEnrollId,t1.FirstName,t1.LastName,t1.SSN,t4.SAPID,t7.SapDetailid, " + vbCrLf)
            .Append(" Convert(char(10),t2.StartDate,101) as ModuleStart, " + vbCrLf)
            .Append(" t7.GPA as GPA, " + vbCrLf)
            .Append(" Case when t4.TrigOffSetTypId=1 Then 'N/A' else t4.MinTermGPA end as GPARequired, " + vbCrLf)
            .Append(" NULL as CGPA, " + vbCrLf)
            .Append(" t5.QualMinValue  as CGPARequired,  " + vbCrLf)
            .Append(" t5.QuantMinValue as CourseCompletion, " + vbCrLf)
            .Append("  t8.ConseqTypDesc as ConsequenceDescrip, " + vbCrLf)
            .Append("  t7.PercentCompleted as CourseCompletionPercentage, " + vbCrLf)
            .Append("  t7.Period as Period, " + vbCrLf)
            .Append("  t5.MinAttendanceValue as Attendance, " + vbCrLf)
            .Append("  t7.Attendance as AttendancePercentage, " + vbCrLf)
            .Append("  t7.CumFinAidCredits as CumFinAidCredits, " + vbCrLf)

            .Append(" t7.IsMakingSAP as IsMakingSAP,t6.ProgDescrip as Program " + vbCrLf)
            '-code added by Priyanka on date 21st of May 2009
            .Append(" , " + vbCrLf)
            .Append(" t7.ModDate as ModDate, " + vbCrLf)
            .Append(" t7.CheckPointDate as CheckPointDate, " + vbCrLf)
            .Append(" t7.TermStartDate as TermStartDate " + vbCrLf)
            '-
            '-code added by Vijay on date Auguest 26, 2009
            .Append(" , Convert(varchar(10),t7.Period) +' : '+ Convert(varchar(10),cast(t5.TrigValue as float)/100) + ' ' + t10.TrigUnitTypDescrip +' after the ' + t9.TrigOffTypDescrip+ ' ' + ISNULL(Convert(varchar(10),TrigOffsetSeq),'')  as IncDesc,  " + vbCrLf)
            .Append("(SELECT QuantMinTypDesc FROM dbo.arQuantMinUnitTyps WHERE QuantMinUnitTypId=t5.QuantMinUnitTypId) as QuantMinTypDesc,t5.mincredsCompltd,t7.CredsEarned, ")
            .Append(" (SELECT InstructionTypeId FROM dbo.arInstructionType WHERE IsDefault=1) AS InstructionTypeId ")
            '-
            .Append("  from " + vbCrLf)
            .Append(" arStudent t1,arStuEnrollments t2,arPrgVersions t3,arSAP t4,arSAPDetails t5,arPrograms t6, arSAPChkResults t7, arConsequenceTyps t8 " + vbCrLf)
            'New code added by Vijay Ramteke on August 26, 2009
            .Append(" , arTrigOffsetTyps t9, arTrigUnitTyps t10  " + vbCrLf)
            'If strWhere <> "" Then
            '    If strWhere.Contains("arSAPChkResults.CheckPointDate") Then
            '        .Append(" ,arSAPChkResults " + vbCrLf)
            '    End If
            'End If
            .Append("   where  " + vbCrLf)
            'modified by Theresa G on 11/18/2010 for Rally DE1115  QA: SAP: SAP report is not returning any results for some students.  
            '.Append(" t1.StudentId=t2.StudentId and t2.PrgVerId=t3.PrgVerId and t3.SAPID=t4.SAPID and t3.ProgId=t6.ProgId and " + vbCrLf)

            .Append("   t7.SAPDetailId NOT IN (SELECT SAPDetailId FROM dbo.arSAPQuantByInstruction) AND ")

            .Append(" t1.StudentId=t2.StudentId and t2.PrgVerId=t3.PrgVerId and t3.ProgId=t6.ProgId and " + vbCrLf)
            .Append(" t4.SAPID = t5.SAPID and t7.StuEnrollId=t2.StuEnrollId and t8.ConsequenceTypId=t5.ConsequenceTypId and t5.SAPDetailId=t7.SAPDetailId and " + vbCrLf)
            'New code added by Vijay Ramteke on August 26, 2009
            .Append(" t5.TrigOffsetTypId=t9.TrigOffsetTypId and t5.TrigUnitTypId=t10.TrigUnitTypId and t7.PreviewSapCheck=0 and " + vbCrLf)
            'If strWhere <> "" Then
            '    If strWhere.Contains("arSAPChkResults.CheckPointDate") Then
            '        .Append(" arSAPChkResults.StuEnrollId=t2.StuEnrollId  AND " + vbCrLf)
            '    End If
            'End If

            .Append(strWhere)


            .Append(" order by t7.Period; ")
            '.Append(" SELECT A.SAPDetailId,A.StuEnrollId,'Percent of Scheduled ' + B.InstructionTypeDescrip + ' Hours/Days' AS QuantMinTypDesc,C.QuantMinValue AS ")
            '.Append("CourseCompletion,A.PercentCompleted AS CourseCompletionPercentage FROM arSAPQuantResults A,arInstructionType B,arSAPQuantByInstruction C WHERE ")
            '.Append(" A.InstructionTypeId=B.InstructionTypeId AND A.SAPDetailId=C.SAPDetailId AND C.InstructionTypeId=B.InstructionTypeId ")
            '.Append("  and A.StuEnrollid= ? ")



        End With
        Try
            'db.AddParameter("@StudentId", paramInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Return db.RunParamSQLDataSet(sb.ToString)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try
    End Function
    Public Function GetInvidualSAPDetails(ByVal paramInfo As ReportParamInfo, Optional ByVal dtGetGPAandCGPA As DataTable = Nothing, Optional ByVal StuEnrollID As String = "") As DataSet

        Dim ds As New DataSet
        ds = GetSAPCheckResults(paramInfo, StuEnrollID)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim ds1 As New DataSet
        Dim dt As New DataTable("SAPDetails")
        If ds.Tables.Count > 0 Then
            dt.Columns.Add(New DataColumn("StuEnrollId", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("LastName", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("FirstName", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("SSN", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ModuleStart", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("GPA", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("GPARequired", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CGPA", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CGPARequired", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CourseCompletion", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CourseCompletionPercentage", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("Attendance", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("AttendancePercentage", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CumFinAidCredits", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("IsMakingSAP", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Program", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ConsequenceDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("SuppressDate", System.Type.GetType("System.String")))
            '-code added by Priyanka on date 21st of May 2009
            dt.Columns.Add(New DataColumn("ModDate", System.Type.GetType("System.DateTime")))
            dt.Columns.Add(New DataColumn("CheckPointDate", System.Type.GetType("System.DateTime")))
            dt.Columns.Add(New DataColumn("TermStartDate", System.Type.GetType("System.DateTime")))
            '-
            '-code added by Vijay Ramteke on August 26, 2009
            dt.Columns.Add(New DataColumn("IncDesc", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("QuantMinTypDesc", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("MinCredsCompltd", System.Type.GetType("System.Int16")))
            dt.Columns.Add(New DataColumn("CredsEarned", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("SapDetailId", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("InstructionTypeId", System.Type.GetType("System.String")))
            '-

            Dim row As DataRow
            Dim GPA As Decimal = 0.0
            Dim CGPA As Decimal = 0.0
            Dim intMaxIndexCount As Integer = 0
            Dim intPeriod As Integer = 0
            Dim intIndex As Integer

            For Each dr As DataRow In ds.Tables(0).Rows
                intIndex = 0
                row = dt.NewRow()
                row("StuEnrollId") = dr("StuEnrollId").ToString
                row("LastName") = dr("LastName").ToString
                row("FirstName") = dr("FirstName").ToString
                If Not dr("SSN") Is System.DBNull.Value Then row("SSN") = dr("SSN").ToString Else row("SSN") = ""
                If Not dr("ModuleStart") Is System.DBNull.Value Then
                    row("ModuleStart") = dr("ModuleStart").ToString
                Else
                    row("ModuleStart") = ""
                End If

                Try
                    row("SuppressDate") = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
                Catch ex As System.Exception
                    row("SuppressDate") = "no"
                End Try

                'need to get the increment and get the TermGPA based on the period for
                'which the SAP check was done
                Try
                    intPeriod = CType(dr("Period"), Integer)
                    intIndex = intPeriod - 1
                    Try
                        If dtGetGPAandCGPA.Rows.Count >= 1 Then
                            GPA = dtGetGPAandCGPA.Rows(intIndex)("TermGPA")
                            'CGPA = dtGetGPAandCGPA.Rows(intIndex)("GPA")
                        End If
                    Catch ex As System.Exception
                    End Try
                Catch ex As System.Exception
                    intPeriod = 0
                    Return Nothing
                End Try

                Try
                    CGPA = dr("GPA").ToString
                Catch ex As System.Exception
                    CGPA = 0.0
                End Try
                row("CGPA") = CGPA
                Try
                    row("GPARequired") = dr("GPARequired").ToString
                Catch ex As System.Exception
                    row("GPARequired") = "0.00"
                End Try
                If Not dr("CGPARequired") Is System.DBNull.Value Then
                    row("CGPARequired") = dr("CGPARequired").ToString
                Else
                    row("CGPARequired") = "2.0"
                End If
                row("GPA") = GPA
                If Not dr("CourseCompletion") Is System.DBNull.Value Then
                    row("CourseCompletion") = dr("CourseCompletion").ToString
                Else
                    row("CourseCompletion") = "0"
                End If
                If Not dr("CourseCompletionPercentage") Is System.DBNull.Value Then
                    row("CourseCompletionPercentage") = dr("CourseCompletionPercentage").ToString
                Else
                    row("CourseCompletionPercentage") = "0"
                End If
                If Not dr("Attendance") Is System.DBNull.Value Then
                    row("Attendance") = dr("Attendance").ToString
                Else
                    row("Attendance") = "0"
                End If
                If Not dr("AttendancePercentage") Is System.DBNull.Value Then
                    row("AttendancePercentage") = dr("AttendancePercentage").ToString
                Else
                    row("AttendancePercentage") = "0"
                End If
                If Not dr("CumFinAidCredits") Is System.DBNull.Value Then
                    row("CumFinAidCredits") = dr("CumFinAidCredits").ToString
                Else
                    row("CumFinAidCredits") = "0"
                End If
                If Not dr("IsMakingSAP") Is System.DBNull.Value Then
                    If CType(dr("IsMakingSAP"), Boolean) = True Then
                        row("IsMakingSAP") = "yes"
                    Else
                        row("IsMakingSAP") = "no"
                    End If
                Else
                    row("IsMakingSAP") = "no"
                End If
                row("Program") = dr("Program").ToString
                If Not dr("ConsequenceDescrip") Is System.DBNull.Value Then
                    row("ConsequenceDescrip") = dr("ConsequenceDescrip").ToString
                Else
                    row("ConsequenceDescrip") = ""
                End If
                '-code added by priyanka on date 21st of May 2009
                row("ModDate") = dr("ModDate").ToString()
                If Not dr("CheckPointDate") Is System.DBNull.Value Then
                    row("CheckPointDate") = dr("CheckPointDate").ToString
                Else
                    row("CheckPointDate") = "1/1/1000"
                End If

                If Not dr("TermStartDate") Is System.DBNull.Value Then
                    row("TermStartDate") = CDate(dr("TermStartDate")).Date.ToString
                Else
                    row("TermStartDate") = "1/1/1000"
                End If
                '-
                '-code added by Vijay Ramteke on August 26, 2009
                row("IncDesc") = dr("IncDesc").ToString
                row("QuantMinTypDesc") = dr("QuantMinTypDesc").ToString
                row("MinCredsCompltd") = dr("MinCredsCompltd")
                row("CredsEarned") = dr("CredsEarned")
                row("SapDetailId") = dr("SapDetailId")
                row("InstructionTypeId") = dr("InstructionTypeId")
                '-
                dt.Rows.Add(row)
            Next
            ds1.Tables.Add(dt)
            'ds1.Tables.Add(ds.Tables(1).Copy())
        End If
        Return ds1
    End Function
    Public Function GetAttendanceDates(ByVal StuEnrollId As String, ByVal dtInputDate As Date) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim dtEndDate As DateTime
        dtEndDate = DateAdd(DateInterval.Day, 7, dtInputDate)

        'build the sql query
        With sb
            .Append(" select Distinct Convert(Char(10),RecordDate,101) as RecordDate from arStudentClockAttendance ")
            .Append(" where StuEnrollId=? and RecordDate>=? and RecordDate<=? ")
            .Append(" Union ")
            .Append(" select Distinct Convert(Char(10),MeetDate,101) as RecordDate from atConversionAttendance ")
            .Append(" where StuEnrollId=? and MeetDate>=? and MeetDate<=? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StartDate", dtInputDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@EndDate", dtEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StartDate", dtInputDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@EndDate", dtEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds.Tables(0)
    End Function
    Public Function GetCommentsByDate(ByVal StuEnrollId As String, ByVal dtDate As String) As String
        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        Dim strComments As String
        With sb
            .Append("SELECT Distinct Comments from arStudentClockAttendance where StuEnrollId=? and RecordDate=? ")
            .Append("Union ")
            .Append("SELECT Distinct Comments from atConversionAttendance where StuEnrollId=? and MeetDate=? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@RecordDate", dtDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@MeetDate", dtDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            strComments = dr("Comments").ToString
        End While

        dr.Close()
        db.ClearParameters()
        sb.Remove(0, sb.Length)



        'Close(Connection)
        db.CloseConnection()

        'Return BankInfo
        Return strComments
    End Function
    Public Function UpdateComments(ByVal StuEnrollId As String, ByVal RecordDate As DateTime, ByVal Comments As String) As String
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            Dim sb As New StringBuilder
            'Dim intRowCount As Integer = 0
            'With sb
            '    .Append("Select Count(*) as RowCount from arStudentClockAttendance where StuEnrollId=? and RecordDate=? ")
            'End With
            'db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'db.AddParameter("@RecordDate", RecordDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Try
            '    intRowCount = db.RunParamSQLScalar(sb.ToString)
            '    If intRowCount >= 1 Then
            '    Else
            '        Return "Please post the attendance in Post Attendance Page, before posting comments"
            '        Exit Function
            '    End If
            'Catch ex As System.Exception
            '    intRowCount = 0
            'Finally
            '    sb.Remove(0, sb.Length)
            '    db.ClearParameters()
            'End Try
            With sb
                .Append(" Update arStudentClockAttendance set Comments=? where StuEnrollId=? and RecordDate=? ")
            End With
            db.AddParameter("@Comments", Comments, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@RecordDate", RecordDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            With sb
                .Append(" Update atConversionAttendance set Comments=? where StuEnrollId=? and MeetDate=? ")
            End With
            db.AddParameter("@Comments", Comments, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@RecordDate", RecordDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            Return ""
        Catch ex As OleDbException
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function GetStudentNames() As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'build the sql query
        With sb
            .Append(" select Distinct t2.StuEnrollId,t3.LastName,t3.LastName+ ',' + t3.FirstName + ' - ' + t4.PrgVerDescrip as FullName,t3.StudentId from arStudentClockAttendance t1,arStuEnrollments t2,arStudent t3,arPrgVersions t4 ")
            .Append(" where t1.StuEnrollId = t2.StuEnrollId And t2.StudentId = t3.Studentid and t2.PrgVerId=t4.PrgVerId  order by t3.LastName ")
        End With
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds.Tables(0)
    End Function
    Public Function GetAttendanceDateByStudent(ByVal StuEnrollId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            .Append(" select Distinct Convert(Char(10),t1.RecordDate,101) as RecordDate from arStudentClockAttendance t1 ")
            .Append(" where t1.StuEnrollId=? ")
            .Append(" Union ")
            .Append(" select Distinct Convert(Char(10),t1.MeetDate,101) as RecordDate from atConversionAttendance t1 ")
            .Append(" where t1.StuEnrollId=? ")
        End With
        db.ClearParameters()
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds.Tables(0)
    End Function

    Public Function GetExternshipAttendanceDays(ByVal StuEnrollId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            .Append(" select Distinct Convert(Char(10),t1.AttendedDate,101) as RecordDate from arExternshipAttendance t1 ")
            .Append(" where t1.StuEnrollId=? ")

        End With
        db.ClearParameters()
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds.Tables(0)
    End Function
    Private Function GetDailyCompletedAndExternshipDetails(ByVal paramInfo As ReportParamInfo, _
                                          Optional ByVal AttUnitType As String = "", _
                                          Optional ByVal PrgVerId As String = "", _
                                          Optional ByVal UseTimeClock As Boolean = False) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'If Attendance Unit Type Is Present Absent
        'If AttUnitType.ToString.ToLower = "pa" Or AttUnitType.ToString.ToLower = "minutes" Then
        'With sb
        '    .Append(" select Distinct " + vbCrLf)
        '    .Append(" t4.FirstName,t4.LastName,t1.StuEnrollId,t4.StudentId, " + vbCrLf)
        '    .Append(" Convert(char(10),RecordDate,101) as MeetDate,SchedHours as Schedule, " + vbCrLf)
        '    .Append(" Case when t6.UnitTypeDescrip='Present Absent' and ActualHours=1.00 Then t2.Total else ActualHours end as ActualHours, " + vbCrLf)
        '    .Append(" (DatePart(dw,RecordDate)-1) as dw1,t2.dw,t1.Comments as comments, " + vbCrLf)
        '    .Append(" IsTardy as StudentIsTardy,t5.TrackTardies, t5.tardiesMakingAbsence,t5.PrgVerId, t5.PrgVerDescrip, " + vbCrLf)
        '    .Append(" t3.CampusId,syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip, " + vbCrLf)
        '    .Append(" (SELECT Top 1 CampDescrip FROM syCampuses WHERE CampusId=t3.CampusId) AS CampusDescrip,'Regular' as Type " + vbCrLf)
        '    .Append(" from " + vbCrLf)
        '    .Append(" arStudentClockAttendance t1,arProgScheduleDetails t2, " + vbCrLf)
        '    If Not paramInfo.TermId.ToString.Trim = "" Then
        '        .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
        '        .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
        '        .Append("   arResults, arClassSections, arStuEnrollments " + vbCrLf)
        '        .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
        '        .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
        '        .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
        '        If paramInfo.PrgVerId <> "" Then
        '            .Append(" and arStuEnrollments.PrgVerId in ('")
        '            .Append(paramInfo.PrgVerId)
        '            .Append("')  " + vbCrLf)
        '        End If
        '        .Append(" Union " + vbCrLf)
        '        .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
        '        .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
        '        .Append("   from arTransferGrades,arStuEnrollments where  " + vbCrLf)
        '        .Append("   arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId   " + vbCrLf)
        '        .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
        '        If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '            .Append(" and arStuEnrollments.PrgVerId in ('")
        '            .Append(paramInfo.PrgVerId)
        '            .Append("')  " + vbCrLf)
        '        End If
        '        .Append(") t3, " + vbCrLf)
        '    ElseIf Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
        '        .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
        '        .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
        '        .Append("   arResults, arClassSections, arStuEnrollments,arTerm " + vbCrLf)
        '        .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
        '        .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
        '        .Append("   and arClassSections.TermId=arTerm.TermId " + vbCrLf)
        '        If Not paramInfo.TermStartDate = "01/01/1900" Then
        '            .Append(" and arTerm.StartDate >= ? ")
        '        End If
        '        If Not paramInfo.TermEndDate = "01/01/1900" Then
        '            .Append(" and arTerm.StartDate <= ? ")
        '        End If
        '        If paramInfo.PrgVerId <> "" Then
        '            .Append(" and arStuEnrollments.PrgVerId in ('")
        '            .Append(paramInfo.PrgVerId)
        '            .Append("')  " + vbCrLf)
        '        End If
        '        .Append(" Union " + vbCrLf)
        '        .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
        '        .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
        '        .Append("   from arTransferGrades,arStuEnrollments,arTerm where  " + vbCrLf)
        '        .Append("   arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId   " + vbCrLf)
        '        .Append("   and arTransferGrades.TermId=arTerm.TermId " + vbCrLf)
        '        If Not paramInfo.TermStartDate = "01/01/1900" Then
        '            .Append(" and arTerm.StartDate >= ? ")
        '        End If
        '        If Not paramInfo.TermEndDate = "01/01/1900" Then
        '            .Append(" and arTerm.StartDate <= ? ")
        '        End If
        '        If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '            .Append(" and arStuEnrollments.PrgVerId in ('")
        '            .Append(paramInfo.PrgVerId)
        '            .Append("')  " + vbCrLf)
        '        End If
        '        .Append(") t3, " + vbCrLf)
        '    Else
        '        .Append(" (Select * from arStuEnrollments " + vbCrLf)
        '        If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '            .Append(" where arStuEnrollments.PrgVerId in ('")
        '            .Append(paramInfo.PrgVerId)
        '            .Append("')  " + vbCrLf)
        '        End If
        '        .Append(") t3, " + vbCrLf)
        '    End If
        '    .Append(" arStudent t4,arPrgVersions t5,arAttUnitType t6,  " + vbCrLf)
        '    .Append(" syCampGrps ,syCmpGrpCmps I " + vbCrLf)
        '    .Append(" where ")
        '    .Append("   t1.ScheduleId=t2.ScheduleId and t1.StuEnrollId=t3.StuEnrollId and " + vbCrLf)
        '    .Append("   t3.StudentId=t4.StudentId and t3.PrgVerId = t5.PrgVerId and t5.UnitTypeId=t6.UnitTypeId and " + vbCrLf)
        '    .Append("   syCampGrps.CampGrpId=I.CampGrpId AND t3.CampusId=I.CampusId " + vbCrLf)
        '    .Append("  and (DatePart(dw,RecordDate)-1)=t2.dw and " + vbCrLf)
        '    .Append("   (t1.ActualHours is not null AND t1.ActualHours <> 999.00 AND t1.ActualHours <> 9999.00) " + vbCrLf)
        '    If Not paramInfo.FirstName.ToString.Trim = "" Then
        '        .Append(" and t4.FirstName like  + ? + '%'")
        '    End If
        '    If Not paramInfo.LastName.ToString.Trim = "" Then
        '        .Append(" and t4.LastName like  + ? + '%'")
        '    End If
        '    If Not paramInfo.StartDate = "01/01/1900" Then
        '        .Append(" and RecordDate >= ? ")
        '    End If
        '    If Not paramInfo.EndDate = "01/01/1900" Then
        '        .Append(" and RecordDate <= ? ")
        '    End If
        '    If Not paramInfo.FirstName.ToString.Trim = "" Then
        '        db.AddParameter("@FirstName", paramInfo.FirstName.ToString.Trim, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '    End If
        '    If Not paramInfo.LastName.ToString.Trim = "" Then
        '        db.AddParameter("@LastName", paramInfo.LastName.ToString.Trim, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '    End If
        '    If Not paramInfo.StartDate = "01/01/1900" Then
        '        db.AddParameter("@StartDate", paramInfo.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '    End If
        '    If Not paramInfo.EndDate = "01/01/1900" Then
        '        db.AddParameter("@EndDate", paramInfo.EndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '    End If
        '    .Append(" and syCampGrps.CampGrpId in  " + vbCrLf)
        '    .Append(" (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in ('")
        '    .Append(paramInfo.CampGrpId)
        '    .Append("))  " + vbCrLf)
        '    .Append(" Group by syCampGrps.CampGrpId,t3.CampusId,t4.StudentId,t1.StuEnrollId,LastName, " + vbCrLf)
        '    .Append(" FirstName,RecordDate,SchedHours,ActualHours,t2.Total,dw,t1.Comments,IsTardy,TrackTardies,tardiesMakingAbsence, " + vbCrLf)
        '    .Append(" t5.PrgVerId, t5.PrgVerDescrip, syCampGrps.CampGrpDescrip,UnitTypeDescrip ")
        '    If SingletonAppSettings.AppSettings("GradesFormat",CampusId).ToString.ToLower = "numeric" Then
        '        .Append(" union ")
        '        .Append(" select Distinct " + vbCrLf)
        '        .Append(" t4.FirstName,t4.LastName,t1.StuEnrollId,t4.StudentId, " + vbCrLf)
        '        .Append(" Convert(char(10),MeetDate,101) as MeetDate,Schedule as Schedule, " + vbCrLf)
        '        .Append(" Actual as ActualHours, " + vbCrLf)
        '        .Append(" NULL,NULL,t1.Comments as comments, " + vbCrLf)
        '        .Append(" Tardy as StudentIsTardy,t5.TrackTardies, t5.tardiesMakingAbsence,t5.PrgVerId, t5.PrgVerDescrip, " + vbCrLf)
        '        .Append(" t3.CampusId,syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip, " + vbCrLf)
        '        .Append(" (SELECT Top 1 CampDescrip FROM syCampuses WHERE CampusId=t3.CampusId) AS CampusDescrip,'Daily' as Type " + vbCrLf)
        '        .Append(" from " + vbCrLf)
        '        .Append(" atConversionAttendance t1,arProgScheduleDetails t2, " + vbCrLf)
        '        If Not paramInfo.TermId.ToString.Trim = "" Then
        '            .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
        '            .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
        '            .Append("   arResults, arClassSections, arStuEnrollments " + vbCrLf)
        '            .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
        '            .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
        '            .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
        '            If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '                .Append(" and arStuEnrollments.PrgVerId in ('")
        '                .Append(paramInfo.PrgVerId)
        '                .Append("')  " + vbCrLf)
        '            End If
        '            .Append(" Union " + vbCrLf)
        '            .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
        '            .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
        '            .Append("   from arTransferGrades,arStuEnrollments where  " + vbCrLf)
        '            .Append("   arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId  " + vbCrLf)
        '            .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
        '            If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '                .Append(" and arStuEnrollments.PrgVerId in ('")
        '                .Append(paramInfo.PrgVerId)
        '                .Append("')  " + vbCrLf)
        '            End If
        '            .Append(") t3, " + vbCrLf)
        '        ElseIf Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
        '            .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
        '            .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
        '            .Append("   arResults, arClassSections, arStuEnrollments,arTerm " + vbCrLf)
        '            .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
        '            .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
        '            .Append("   and arClassSections.TermId=arTerm.TermId " + vbCrLf)
        '            If Not paramInfo.TermStartDate = "01/01/1900" Then
        '                .Append(" and arTerm.StartDate >= ? ")
        '            End If
        '            If Not paramInfo.TermEndDate = "01/01/1900" Then
        '                .Append(" and arTerm.StartDate <= ? ")
        '            End If
        '            If paramInfo.PrgVerId <> "" Then
        '                .Append(" and arStuEnrollments.PrgVerId in ('")
        '                .Append(paramInfo.PrgVerId)
        '                .Append("')  " + vbCrLf)
        '            End If
        '            .Append(" Union " + vbCrLf)
        '            .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
        '            .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
        '            .Append("   from arTransferGrades,arStuEnrollments,arTerm where  " + vbCrLf)
        '            .Append("   arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId   " + vbCrLf)
        '            .Append("   and arTransferGrades.TermId=arTerm.TermId " + vbCrLf)
        '            If Not paramInfo.TermStartDate = "01/01/1900" Then
        '                .Append(" and arTerm.StartDate >= ? ")
        '            End If
        '            If Not paramInfo.TermEndDate = "01/01/1900" Then
        '                .Append(" and arTerm.StartDate <= ? ")
        '            End If
        '            If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '                .Append(" and arStuEnrollments.PrgVerId in ('")
        '                .Append(paramInfo.PrgVerId)
        '                .Append("')  " + vbCrLf)
        '            End If
        '            .Append(") t3, " + vbCrLf)
        '        Else
        '            .Append(" (Select * from arStuEnrollments " + vbCrLf)
        '            If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '                .Append(" where arStuEnrollments.PrgVerId in ('")
        '                .Append(paramInfo.PrgVerId)
        '                .Append("')  " + vbCrLf)
        '            End If
        '            .Append(") t3, " + vbCrLf)
        '        End If
        '        .Append("   arStudent t4,arPrgVersions t5,  " + vbCrLf)
        '        .Append(" syCampGrps ,syCmpGrpCmps I " + vbCrLf)
        '        .Append(" where ")
        '        .Append("   t1.StuEnrollId=t3.StuEnrollId and " + vbCrLf)
        '        .Append("   t3.StudentId=t4.StudentId and t3.PrgVerId = t5.PrgVerId and " + vbCrLf)
        '        .Append("   syCampGrps.CampGrpId=I.CampGrpId AND t3.CampusId=I.CampusId " + vbCrLf)
        '        .Append("   and (DatePart(dw,MeetDate)-1)=t2.dw and " + vbCrLf)
        '        .Append("   (t1.Actual is not null AND t1.Actual <> 999.00 AND t1.Actual <> 9999.00) " + vbCrLf)
        '        If Not paramInfo.FirstName.ToString.Trim = "" Then
        '            .Append(" and t4.FirstName like  + ? + '%'")
        '        End If
        '        If Not paramInfo.LastName.ToString.Trim = "" Then
        '            .Append(" and t4.LastName like  + ? + '%'")
        '        End If
        '        If Not paramInfo.StartDate = "01/01/1900" Then
        '            .Append(" and MeetDate >= ? ")
        '        End If
        '        If Not paramInfo.EndDate = "01/01/1900" Then
        '            .Append(" and MeetDate <= ? ")
        '        End If
        '        .Append(" and syCampGrps.CampGrpId in  " + vbCrLf)
        '        .Append(" (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in ('")
        '        .Append(paramInfo.CampGrpId)
        '        .Append("))  " + vbCrLf)


        '        If Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
        '            If Not paramInfo.TermStartDate = "01/01/1900" Then
        '                db.AddParameter("@TermStartDate", paramInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '            End If
        '            If Not paramInfo.TermEndDate = "01/01/1900" Then
        '                db.AddParameter("@TermEndDate", paramInfo.TermEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '            End If
        '            If Not paramInfo.TermStartDate = "01/01/1900" Then
        '                db.AddParameter("@TermStartDate", paramInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '            End If
        '            If Not paramInfo.TermEndDate = "01/01/1900" Then
        '                db.AddParameter("@TermEndDate", paramInfo.TermEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '            End If
        '        End If
        '        If Not paramInfo.FirstName.ToString.Trim = "" Then
        '            db.AddParameter("@FirstName", paramInfo.FirstName.ToString.Trim, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '        End If
        '        If Not paramInfo.LastName.ToString.Trim = "" Then
        '            db.AddParameter("@LastName", paramInfo.LastName.ToString.Trim, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '        End If
        '        If Not paramInfo.StartDate = "01/01/1900" Then
        '            db.AddParameter("@StartDate", paramInfo.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '        End If
        '        If Not paramInfo.EndDate = "01/01/1900" Then
        '            db.AddParameter("@EndDate", paramInfo.EndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '        End If
        '        If SingletonAppSettings.AppSettings("GradesFormat",CampusId).ToString.ToLower = "numeric" Then
        '            If Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
        '                If Not paramInfo.TermStartDate = "01/01/1900" Then
        '                    db.AddParameter("@TermStartDate", paramInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '                End If
        '                If Not paramInfo.TermEndDate = "01/01/1900" Then
        '                    db.AddParameter("@TermEndDate", paramInfo.TermEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '                End If
        '                If Not paramInfo.TermStartDate = "01/01/1900" Then
        '                    db.AddParameter("@TermStartDate", paramInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '                End If
        '                If Not paramInfo.TermEndDate = "01/01/1900" Then
        '                    db.AddParameter("@TermEndDate", paramInfo.TermEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '                End If
        '            End If
        '            If Not paramInfo.FirstName.ToString.Trim = "" Then
        '                db.AddParameter("@FirstName", paramInfo.FirstName.ToString.Trim, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '            End If
        '            If Not paramInfo.LastName.ToString.Trim = "" Then
        '                db.AddParameter("@LastName", paramInfo.LastName.ToString.Trim, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '            End If
        '            If Not paramInfo.StartDate = "01/01/1900" Then
        '                db.AddParameter("@StartDate", paramInfo.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '            End If
        '            If Not paramInfo.EndDate = "01/01/1900" Then
        '                db.AddParameter("@EndDate", paramInfo.EndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '            End If
        '        End If
        '        .Append(" Group by syCampGrps.CampGrpId,t3.CampusId,t4.StudentId,t1.StuEnrollId,LastName, " + vbCrLf)
        '        .Append(" FirstName,MeetDate,Schedule,Actual,Tardy,t1.comments,TrackTardies,tardiesMakingAbsence, " + vbCrLf)
        '        .Append(" t5.PrgVerId, t5.PrgVerDescrip, syCampGrps.CampGrpDescrip " + vbCrLf)
        '    End If
        '    'Externship query starts here
        '    .Append(" Union ")
        '    .Append(" select Distinct " + vbCrLf)
        '    .Append(" t4.FirstName,t4.LastName,t1.StuEnrollId,t4.StudentId, " + vbCrLf)
        '    .Append(" Convert(char(10),AttendedDate,101) as MeetDate, " + vbCrLf)
        '    .Append(" HoursAttended as  ActualHours, " + vbCrLf)
        '    .Append(" (DatePart(dw,AttendedDate)-1) as dw1,t2.dw,t1.Comments as comments, " + vbCrLf)
        '    .Append(" t5.PrgVerId, t5.PrgVerDescrip, " + vbCrLf)
        '    .Append(" t3.CampusId,syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip, " + vbCrLf)
        '    .Append(" (SELECT Top 1 CampDescrip FROM syCampuses WHERE CampusId=t3.CampusId) AS CampusDescrip,'Externship' as Type " + vbCrLf)
        '    .Append("  from " + vbCrLf)
        '    .Append(" arExternshipAttendance t1,arProgScheduleDetails t2, " + vbCrLf)
        '    If Not paramInfo.TermId.ToString.Trim = "" Then
        '        .Append("  (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
        '        .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
        '        .Append("    arResults, arClassSections, arStuEnrollments " + vbCrLf)
        '        .Append("    where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
        '        .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
        '        .Append("    and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)

        '        If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '            .Append(" and arStuEnrollments.PrgVerId in ('")
        '            .Append(paramInfo.PrgVerId)
        '            .Append("')  " + vbCrLf)
        '        End If


        '        .Append(" Union " + vbCrLf)
        '        .Append("    Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
        '        .Append("    arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
        '        .Append("   from arTransferGrades,arStuEnrollments where  " + vbCrLf)
        '        .Append("    arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId   " + vbCrLf)
        '        .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)

        '        If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '            .Append(" and arStuEnrollments.PrgVerId in ('")
        '            .Append(paramInfo.PrgVerId)
        '            .Append("')  " + vbCrLf)
        '        End If


        '        .Append(") t3, " + vbCrLf)
        '    ElseIf Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
        '        .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
        '        .Append("  arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
        '        .Append("  arResults, arClassSections, arStuEnrollments,arTerm " + vbCrLf)
        '        .Append("  where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
        '        .Append("  arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
        '        .Append("  and arClassSections.TermId=arTerm.TermId " + vbCrLf)
        '        If Not paramInfo.TermStartDate = "01/01/1900" Then
        '            .Append("   and arTerm.StartDate >= ? " + vbCrLf)
        '        End If
        '        If Not paramInfo.TermEndDate = "01/01/1900" Then
        '            .Append("  and arTerm.StartDate <= ? " + vbCrLf)
        '        End If
        '        If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '            .Append(" and arStuEnrollments.PrgVerId in ('")
        '            .Append(paramInfo.PrgVerId)
        '            .Append("')  " + vbCrLf)
        '        End If
        '        .Append(" Union  " + vbCrLf)
        '        .Append(" Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId,  " + vbCrLf)
        '        .Append(" arStuEnrollments.StudentId, arStuEnrollments.CampusId  " + vbCrLf)
        '        .Append(" from arTransferGrades,arStuEnrollments,arTerm where   " + vbCrLf)
        '        .Append(" arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId    " + vbCrLf)
        '        .Append(" and arTransferGrades.TermId=arTerm.TermId  " + vbCrLf)
        '        If Not paramInfo.TermStartDate = "01/01/1900" Then
        '            .Append(" and arTerm.StartDate >= ? " + vbCrLf)
        '        End If
        '        If Not paramInfo.TermEndDate = "01/01/1900" Then
        '            .Append(" and arTerm.StartDate <= ? " + vbCrLf)
        '        End If

        '        If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '            .Append(" and arStuEnrollments.PrgVerId in ('")
        '            .Append(paramInfo.PrgVerId)
        '            .Append("')  " + vbCrLf)
        '        End If
        '        .Append(" ) t3,  " + vbCrLf)
        '    Else
        '        .Append(" (Select * from arStuEnrollments " + vbCrLf)
        '        If paramInfo.PrgVerId.ToString.Trim <> "" Then

        '            .Append("    where arStuEnrollments.PrgVerId in ('")
        '            .Append(paramInfo.PrgVerId)
        '            .Append("')  " + vbCrLf)
        '        End If
        '        .Append("       ) t3,  " + vbCrLf)
        '    End If
        '    .Append(" arStudent t4,arPrgVersions t5,   " + vbCrLf)
        '    .Append(" syCampGrps ,syCmpGrpCmps I  " + vbCrLf)
        '    .Append(" where  " + vbCrLf)
        '    .Append(" t1.StuEnrollId=t3.StuEnrollId and  " + vbCrLf)
        '    .Append(" t3.StudentId=t4.StudentId and t3.PrgVerId = t5.PrgVerId and  " + vbCrLf)
        '    .Append("  syCampGrps.CampGrpId=I.CampGrpId AND t3.CampusId=I.CampusId  " + vbCrLf)
        '    .Append(" and (DatePart(dw,AttendedDate)-1)=t2.dw and  " + vbCrLf)
        '    .Append("  (t1.HoursAttended is not null )  " + vbCrLf)
        '    If Not paramInfo.FirstName.ToString.Trim = "" Then
        '        .Append(" and t4.FirstName like  + ? + '%'" + vbCrLf)
        '    End If
        '    If Not paramInfo.LastName.ToString.Trim = "" Then
        '        .Append("  and t4.LastName like  + ? + '%'" + vbCrLf)
        '    End If
        '    If Not paramInfo.StartDate = "01/01/1900" Then
        '        .Append(" and AttendedDate >= ? " + vbCrLf)
        '    End If
        '    If Not paramInfo.EndDate = "01/01/1900" Then
        '        .Append(" and AttendedDate <= ? " + vbCrLf)
        '    End If
        '    .Append(" and syCampGrps.CampGrpId in  " + vbCrLf)

        '    .Append(" (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in ('")
        '    .Append(paramInfo.CampGrpId)
        '    .Append("))  " + vbCrLf)

        '    If Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
        '        If Not paramInfo.TermStartDate = "01/01/1900" Then
        '            db.AddParameter("@TermStartDate", paramInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '        End If
        '        If Not paramInfo.TermEndDate = "01/01/1900" Then
        '            db.AddParameter("@TermEndDate", paramInfo.TermEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '        End If
        '        If Not paramInfo.TermStartDate = "01/01/1900" Then
        '            db.AddParameter("@TermStartDate", paramInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '        End If
        '        If Not paramInfo.TermEndDate = "01/01/1900" Then
        '            db.AddParameter("@TermEndDate", paramInfo.TermEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '        End If
        '    End If
        '    If Not paramInfo.FirstName.ToString.Trim = "" Then
        '        db.AddParameter("@FirstName", paramInfo.FirstName.ToString.Trim, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '    End If
        '    If Not paramInfo.LastName.ToString.Trim = "" Then
        '        db.AddParameter("@LastName", paramInfo.LastName.ToString.Trim, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '    End If
        '    If Not paramInfo.StartDate = "01/01/1900" Then
        '        db.AddParameter("@StartDate", paramInfo.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '    End If
        '    If Not paramInfo.EndDate = "01/01/1900" Then
        '        db.AddParameter("@EndDate", paramInfo.EndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '    End If
        '    .Append(" Group by syCampGrps.CampGrpId,t3.CampusId,t4.StudentId,t1.StuEnrollId,LastName, " + vbCrLf)
        '    .Append(" FirstName,AttendedDate,HoursAttended,t2.Total,dw,t1.Comments, " + vbCrLf)
        '    .Append(" t5.PrgVerId, t5.PrgVerDescrip, syCampGrps.CampGrpDescrip " + vbCrLf)
        '    .Append(" Order by syCampGrps.CampGrpDescrip,t3.CampusId,t4.LastName,t4.FirstName,Type " + vbCrLf)
        'End With

        'Try
        '    Return db.RunParamSQLDataSet(sb.ToString)
        'Catch ex As System.Exception
        'Finally
        '    db.ClearParameters()
        '    sb.Remove(0, sb.Length)
        'End Try
        With sb
            .Append(" select Distinct " + vbCrLf)
            .Append(" t4.FirstName,t4.LastName,t1.StuEnrollId,t4.StudentId, " + vbCrLf)
            .Append(" Convert(char(10),RecordDate,101) as MeetDate,SchedHours as Schedule, " + vbCrLf)
            .Append(" Case when t6.UnitTypeDescrip='Present Absent' and ActualHours=1.00 Then t2.Total else ActualHours end as ActualHours, " + vbCrLf)
            .Append(" (DatePart(dw,RecordDate)-1) as dw1,t2.dw,t1.Comments as comments, " + vbCrLf)
            .Append(" IsTardy as StudentIsTardy,t5.TrackTardies, t5.tardiesMakingAbsence,t5.PrgVerId, t5.PrgVerDescrip, " + vbCrLf)
            .Append(" t3.CampusId,syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip, " + vbCrLf)
            .Append(" (SELECT Top 1 CampDescrip FROM syCampuses WHERE CampusId=t3.CampusId) AS CampusDescrip,'Regular' as Type " + vbCrLf)
            .Append(" from " + vbCrLf)
            .Append(" arStudentClockAttendance t1,arProgScheduleDetails t2, " + vbCrLf)
            If Not paramInfo.TermId.ToString.Trim = "" Then
                .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
                .Append("   arResults, arClassSections, arStuEnrollments " + vbCrLf)
                .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
                .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
                .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
                If paramInfo.PrgVerId <> "" Then
                    .Append(" and arStuEnrollments.PrgVerId in ('")
                    .Append(paramInfo.PrgVerId)
                    .Append("')  " + vbCrLf)
                End If
                .Append(" Union " + vbCrLf)
                .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
                .Append("   from arTransferGrades,arStuEnrollments where  " + vbCrLf)
                .Append("   arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId   " + vbCrLf)
                .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
                If paramInfo.PrgVerId.ToString.Trim <> "" Then
                    .Append(" and arStuEnrollments.PrgVerId in ('")
                    .Append(paramInfo.PrgVerId)
                    .Append("')  " + vbCrLf)
                End If
                .Append(") t3, " + vbCrLf)
            ElseIf Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
                .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
                .Append("   arResults, arClassSections, arStuEnrollments,arTerm " + vbCrLf)
                .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
                .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
                .Append("   and arClassSections.TermId=arTerm.TermId " + vbCrLf)
                If Not paramInfo.TermStartDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate >= ? ")
                End If
                If Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate <= ? ")
                End If
                If paramInfo.PrgVerId <> "" Then
                    .Append(" and arStuEnrollments.PrgVerId in ('")
                    .Append(paramInfo.PrgVerId)
                    .Append("')  " + vbCrLf)
                End If
                .Append(" Union " + vbCrLf)
                .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
                .Append("   from arTransferGrades,arStuEnrollments,arTerm where  " + vbCrLf)
                .Append("   arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId   " + vbCrLf)
                .Append("   and arTransferGrades.TermId=arTerm.TermId " + vbCrLf)
                If Not paramInfo.TermStartDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate >= ? ")
                End If
                If Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate <= ? ")
                End If
                If paramInfo.PrgVerId.ToString.Trim <> "" Then
                    .Append(" and arStuEnrollments.PrgVerId in ('")
                    .Append(paramInfo.PrgVerId)
                    .Append("')  " + vbCrLf)
                End If
                .Append(") t3, " + vbCrLf)
            Else
                .Append(" (Select * from arStuEnrollments " + vbCrLf)
                If paramInfo.PrgVerId.ToString.Trim <> "" Then
                    .Append(" where arStuEnrollments.PrgVerId in ('")
                    .Append(paramInfo.PrgVerId)
                    .Append("')  " + vbCrLf)
                End If
                .Append(") t3, " + vbCrLf)
            End If
            .Append(" arStudent t4,arPrgVersions t5,arAttUnitType t6,  " + vbCrLf)
            .Append(" syCampGrps ,syCmpGrpCmps I " + vbCrLf)
            .Append(" where ")
            .Append("   t1.ScheduleId=t2.ScheduleId and t1.StuEnrollId=t3.StuEnrollId and " + vbCrLf)
            .Append("   t3.StudentId=t4.StudentId and t3.PrgVerId = t5.PrgVerId and t5.UnitTypeId=t6.UnitTypeId and " + vbCrLf)
            .Append("   syCampGrps.CampGrpId=I.CampGrpId AND t3.CampusId=I.CampusId " + vbCrLf)
            .Append("  and (DatePart(dw,RecordDate)-1)=t2.dw and " + vbCrLf)
            .Append("   (t1.ActualHours is not null AND t1.ActualHours <> 999.00 AND t1.ActualHours <> 9999.00) " + vbCrLf)
            If Not paramInfo.FirstName.ToString.Trim = "" Then
                .Append(" and t4.FirstName like  + ? + '%'")
            End If
            If Not paramInfo.LastName.ToString.Trim = "" Then
                .Append(" and t4.LastName like  + ? + '%'")
            End If
            If Not paramInfo.StartDate = "01/01/1900" Then
                .Append(" and RecordDate >= ? ")
            End If
            If Not paramInfo.EndDate = "01/01/1900" Then
                .Append(" and RecordDate <= ? ")
            End If
            .Append(" and syCampGrps.CampGrpId in  " + vbCrLf)
            .Append(" (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in ('")
            .Append(paramInfo.CampGrpId)
            .Append("))  " + vbCrLf)
            .Append(" Group by syCampGrps.CampGrpId,t3.CampusId,t4.StudentId,t1.StuEnrollId,LastName, " + vbCrLf)
            .Append(" FirstName,RecordDate,SchedHours,ActualHours,t2.Total,dw,t1.Comments,IsTardy,TrackTardies,tardiesMakingAbsence, " + vbCrLf)
            .Append(" t5.PrgVerId, t5.PrgVerDescrip, syCampGrps.CampGrpDescrip,UnitTypeDescrip ")
            If MyAdvAppSettings.AppSettings("GradesFormat", paramInfo.CampusId).ToString.ToLower = "numeric" Then
                .Append(" union ")
                .Append(" select Distinct " + vbCrLf)
                .Append(" t4.FirstName,t4.LastName,t1.StuEnrollId,t4.StudentId, " + vbCrLf)
                .Append(" Convert(char(10),MeetDate,101) as MeetDate,Schedule as Schedule, " + vbCrLf)
                .Append(" Actual as ActualHours, " + vbCrLf)
                .Append(" NULL,NULL,t1.Comments as comments, " + vbCrLf)
                .Append(" Tardy as StudentIsTardy,t5.TrackTardies, t5.tardiesMakingAbsence,t5.PrgVerId, t5.PrgVerDescrip, " + vbCrLf)
                .Append(" t3.CampusId,syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip, " + vbCrLf)
                .Append(" (SELECT Top 1 CampDescrip FROM syCampuses WHERE CampusId=t3.CampusId) AS CampusDescrip " + vbCrLf)
                .Append(" from " + vbCrLf)
                .Append(" atConversionAttendance t1,arProgScheduleDetails t2, " + vbCrLf)
                If Not paramInfo.TermId.ToString.Trim = "" Then
                    .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                    .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
                    .Append("   arResults, arClassSections, arStuEnrollments " + vbCrLf)
                    .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
                    .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
                    .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
                    If paramInfo.PrgVerId.ToString.Trim <> "" Then
                        .Append(" and arStuEnrollments.PrgVerId in ('")
                        .Append(paramInfo.PrgVerId)
                        .Append("')  " + vbCrLf)
                    End If
                    .Append(" Union " + vbCrLf)
                    .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                    .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
                    .Append("   from arTransferGrades,arStuEnrollments where  " + vbCrLf)
                    .Append("   arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId  " + vbCrLf)
                    .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
                    If paramInfo.PrgVerId.ToString.Trim <> "" Then
                        .Append(" and arStuEnrollments.PrgVerId in ('")
                        .Append(paramInfo.PrgVerId)
                        .Append("')  " + vbCrLf)
                    End If
                    .Append(") t3, " + vbCrLf)
                ElseIf Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                    .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
                    .Append("   arResults, arClassSections, arStuEnrollments,arTerm " + vbCrLf)
                    .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
                    .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
                    .Append("   and arClassSections.TermId=arTerm.TermId " + vbCrLf)
                    If Not paramInfo.TermStartDate = "01/01/1900" Then
                        .Append(" and arTerm.StartDate >= ? ")
                    End If
                    If Not paramInfo.TermEndDate = "01/01/1900" Then
                        .Append(" and arTerm.StartDate <= ? ")
                    End If
                    If paramInfo.PrgVerId <> "" Then
                        .Append(" and arStuEnrollments.PrgVerId in ('")
                        .Append(paramInfo.PrgVerId)
                        .Append("')  " + vbCrLf)
                    End If
                    .Append(" Union " + vbCrLf)
                    .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                    .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
                    .Append("   from arTransferGrades,arStuEnrollments,arTerm where  " + vbCrLf)
                    .Append("   arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId   " + vbCrLf)
                    .Append("   and arTransferGrades.TermId=arTerm.TermId " + vbCrLf)
                    If Not paramInfo.TermStartDate = "01/01/1900" Then
                        .Append(" and arTerm.StartDate >= ? ")
                    End If
                    If Not paramInfo.TermEndDate = "01/01/1900" Then
                        .Append(" and arTerm.StartDate <= ? ")
                    End If
                    If paramInfo.PrgVerId.ToString.Trim <> "" Then
                        .Append(" and arStuEnrollments.PrgVerId in ('")
                        .Append(paramInfo.PrgVerId)
                        .Append("')  " + vbCrLf)
                    End If
                    .Append(") t3, " + vbCrLf)
                Else
                    .Append(" (Select * from arStuEnrollments " + vbCrLf)
                    If paramInfo.PrgVerId.ToString.Trim <> "" Then
                        .Append(" where arStuEnrollments.PrgVerId in ('")
                        .Append(paramInfo.PrgVerId)
                        .Append("')  " + vbCrLf)
                    End If
                    .Append(") t3, " + vbCrLf)
                End If
                .Append("   arStudent t4,arPrgVersions t5,  " + vbCrLf)
                .Append(" syCampGrps ,syCmpGrpCmps I " + vbCrLf)
                .Append(" where ")
                .Append("   t1.StuEnrollId=t3.StuEnrollId and " + vbCrLf)
                .Append("   t3.StudentId=t4.StudentId and t3.PrgVerId = t5.PrgVerId and " + vbCrLf)
                .Append("   syCampGrps.CampGrpId=I.CampGrpId AND t3.CampusId=I.CampusId " + vbCrLf)
                .Append("   and (DatePart(dw,MeetDate)-1)=t2.dw and " + vbCrLf)
                .Append("   (t1.Actual is not null AND t1.Actual <> 999.00 AND t1.Actual <> 9999.00) " + vbCrLf)
                If Not paramInfo.FirstName.ToString.Trim = "" Then
                    .Append(" and t4.FirstName like  + ? + '%'")
                End If
                If Not paramInfo.LastName.ToString.Trim = "" Then
                    .Append(" and t4.LastName like  + ? + '%'")
                End If
                If Not paramInfo.StartDate = "01/01/1900" Then
                    .Append(" and MeetDate >= ? ")
                End If
                If Not paramInfo.EndDate = "01/01/1900" Then
                    .Append(" and MeetDate <= ? ")
                End If
                .Append(" and syCampGrps.CampGrpId in  " + vbCrLf)
                .Append(" (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in ('")
                .Append(paramInfo.CampGrpId)
                .Append("))  " + vbCrLf)
                .Append(" Group by syCampGrps.CampGrpId,t3.CampusId,t4.StudentId,t1.StuEnrollId,LastName, " + vbCrLf)
                .Append(" FirstName,MeetDate,Schedule,Actual,Tardy,t1.comments,TrackTardies,tardiesMakingAbsence, " + vbCrLf)
                .Append(" t5.PrgVerId, t5.PrgVerDescrip, syCampGrps.CampGrpDescrip " + vbCrLf)
            End If
            'Externship query starts here
            .Append(" Union ")
            .Append(" select Distinct " + vbCrLf)
            .Append(" t4.FirstName,t4.LastName,t1.StuEnrollId,t4.StudentId, " + vbCrLf)
            .Append(" Convert(char(10),AttendedDate,101) as MeetDate, " + vbCrLf)

            .Append(" HoursAttended as  ActualHours, " + vbCrLf)
            .Append(" (DatePart(dw,AttendedDate)-1) as dw1,t2.dw,t1.Comments as comments, " + vbCrLf)
            .Append(" t5.PrgVerId, t5.PrgVerDescrip, " + vbCrLf)
            .Append(" t3.CampusId,syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip, " + vbCrLf)
            .Append(" (SELECT Top 1 CampDescrip FROM syCampuses WHERE CampusId=t3.CampusId) AS CampusDescrip,'Externship' as Type " + vbCrLf)
            .Append("  from " + vbCrLf)
            .Append(" arExternshipAttendance t1,arProgScheduleDetails t2, " + vbCrLf)
            If Not paramInfo.TermId.ToString.Trim = "" Then
                .Append("  (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
                .Append("    arResults, arClassSections, arStuEnrollments " + vbCrLf)
                .Append("    where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
                .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
                .Append("    and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)

                If paramInfo.PrgVerId.ToString.Trim <> "" Then
                    .Append(" and arStuEnrollments.PrgVerId in ('")
                    .Append(paramInfo.PrgVerId)
                    .Append("')  " + vbCrLf)
                End If


                .Append(" Union " + vbCrLf)
                .Append("    Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                .Append("    arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
                .Append("   from arTransferGrades,arStuEnrollments where  " + vbCrLf)
                .Append("    arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId   " + vbCrLf)
                .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)

                If paramInfo.PrgVerId.ToString.Trim <> "" Then
                    .Append(" and arStuEnrollments.PrgVerId in ('")
                    .Append(paramInfo.PrgVerId)
                    .Append("')  " + vbCrLf)
                End If
                .Append(") t3, " + vbCrLf)
            ElseIf Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
                .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                .Append("  arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
                .Append("  arResults, arClassSections, arStuEnrollments,arTerm " + vbCrLf)
                .Append("  where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
                .Append("  arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
                .Append("  and arClassSections.TermId=arTerm.TermId " + vbCrLf)
                If Not paramInfo.TermStartDate = "01/01/1900" Then
                    .Append("   and arTerm.StartDate >= ? " + vbCrLf)
                End If
                If Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append("  and arTerm.StartDate <= ? " + vbCrLf)
                End If
                If paramInfo.PrgVerId.ToString.Trim <> "" Then
                    .Append(" and arStuEnrollments.PrgVerId in ('")
                    .Append(paramInfo.PrgVerId)
                    .Append("')  " + vbCrLf)
                End If
                .Append(" Union  " + vbCrLf)
                .Append(" Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId,  " + vbCrLf)
                .Append(" arStuEnrollments.StudentId, arStuEnrollments.CampusId  " + vbCrLf)
                .Append(" from arTransferGrades,arStuEnrollments,arTerm where   " + vbCrLf)
                .Append(" arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId    " + vbCrLf)
                .Append(" and arTransferGrades.TermId=arTerm.TermId  " + vbCrLf)
                If Not paramInfo.TermStartDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate >= ? " + vbCrLf)
                End If
                If Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate <= ? " + vbCrLf)
                End If

                If paramInfo.PrgVerId.ToString.Trim <> "" Then
                    .Append(" and arStuEnrollments.PrgVerId in ('")
                    .Append(paramInfo.PrgVerId)
                    .Append("')  " + vbCrLf)
                End If
                .Append(" ) t3,  " + vbCrLf)
            Else
                .Append(" (Select * from arStuEnrollments " + vbCrLf)
                If paramInfo.PrgVerId.ToString.Trim <> "" Then

                    .Append("    where arStuEnrollments.PrgVerId in ('")
                    .Append(paramInfo.PrgVerId)
                    .Append("')  " + vbCrLf)
                End If
                .Append("       ) t3,  " + vbCrLf)
            End If
            .Append(" arStudent t4,arPrgVersions t5,   " + vbCrLf)
            .Append(" syCampGrps ,syCmpGrpCmps I  " + vbCrLf)
            .Append(" where  " + vbCrLf)
            .Append(" t1.StuEnrollId=t3.StuEnrollId and  " + vbCrLf)
            .Append(" t3.StudentId=t4.StudentId and t3.PrgVerId = t5.PrgVerId and  " + vbCrLf)
            .Append("  syCampGrps.CampGrpId=I.CampGrpId AND t3.CampusId=I.CampusId  " + vbCrLf)
            .Append(" and (DatePart(dw,AttendedDate)-1)=t2.dw and  " + vbCrLf)
            .Append("  (t1.HoursAttended is not null )  " + vbCrLf)
            If Not paramInfo.FirstName.ToString.Trim = "" Then
                .Append(" and t4.FirstName like  + ? + '%'" + vbCrLf)
            End If
            If Not paramInfo.LastName.ToString.Trim = "" Then
                .Append("  and t4.LastName like  + ? + '%'" + vbCrLf)
            End If
            If Not paramInfo.StartDate = "01/01/1900" Then
                .Append(" and AttendedDate >= ? " + vbCrLf)
            End If
            If Not paramInfo.EndDate = "01/01/1900" Then
                .Append(" and AttendedDate <= ? " + vbCrLf)
            End If
            .Append(" and syCampGrps.CampGrpId in  " + vbCrLf)

            .Append(" (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in ('")
            .Append(paramInfo.CampGrpId)
            .Append("))  " + vbCrLf)
            .Append(" Group by syCampGrps.CampGrpId,t3.CampusId,t4.StudentId,t1.StuEnrollId,LastName, " + vbCrLf)
            .Append(" FirstName,AttendedDate,HoursAttended,t2.Total,dw,t1.Comments, " + vbCrLf)
            .Append(" t5.PrgVerId, t5.PrgVerDescrip, syCampGrps.CampGrpDescrip " + vbCrLf)
            .Append(" Order by syCampGrps.CampGrpDescrip,t3.CampusId,t4.LastName,t4.FirstName,Type " + vbCrLf)
        End With
        If Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
            If Not paramInfo.TermStartDate = "01/01/1900" Then
                db.AddParameter("@TermStartDate", paramInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            If Not paramInfo.TermEndDate = "01/01/1900" Then
                db.AddParameter("@TermEndDate", paramInfo.TermEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            If Not paramInfo.TermStartDate = "01/01/1900" Then
                db.AddParameter("@TermStartDate", paramInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            If Not paramInfo.TermEndDate = "01/01/1900" Then
                db.AddParameter("@TermEndDate", paramInfo.TermEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
        End If
        If Not paramInfo.FirstName.ToString.Trim = "" Then
            db.AddParameter("@FirstName", paramInfo.FirstName.ToString.Trim, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        If Not paramInfo.LastName.ToString.Trim = "" Then
            db.AddParameter("@LastName", paramInfo.LastName.ToString.Trim, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        If Not paramInfo.StartDate = "01/01/1900" Then
            db.AddParameter("@StartDate", paramInfo.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If
        If Not paramInfo.EndDate = "01/01/1900" Then
            db.AddParameter("@EndDate", paramInfo.EndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If
        If MyAdvAppSettings.AppSettings("GradesFormat", paramInfo.CampusId).ToString.ToLower = "numeric" Then
            If Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
                If Not paramInfo.TermStartDate = "01/01/1900" Then
                    db.AddParameter("@TermStartDate", paramInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                If Not paramInfo.TermEndDate = "01/01/1900" Then
                    db.AddParameter("@TermEndDate", paramInfo.TermEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                If Not paramInfo.TermStartDate = "01/01/1900" Then
                    db.AddParameter("@TermStartDate", paramInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                If Not paramInfo.TermEndDate = "01/01/1900" Then
                    db.AddParameter("@TermEndDate", paramInfo.TermEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
            End If
            If Not paramInfo.FirstName.ToString.Trim = "" Then
                db.AddParameter("@FirstName", paramInfo.FirstName.ToString.Trim, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            If Not paramInfo.LastName.ToString.Trim = "" Then
                db.AddParameter("@LastName", paramInfo.LastName.ToString.Trim, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            If Not paramInfo.StartDate = "01/01/1900" Then
                db.AddParameter("@StartDate", paramInfo.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            If Not paramInfo.EndDate = "01/01/1900" Then
                db.AddParameter("@EndDate", paramInfo.EndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
        End If
        If Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
            If Not paramInfo.TermStartDate = "01/01/1900" Then
                db.AddParameter("@TermStartDate", paramInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            If Not paramInfo.TermEndDate = "01/01/1900" Then
                db.AddParameter("@TermEndDate", paramInfo.TermEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            If Not paramInfo.TermStartDate = "01/01/1900" Then
                db.AddParameter("@TermStartDate", paramInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            If Not paramInfo.TermEndDate = "01/01/1900" Then
                db.AddParameter("@TermEndDate", paramInfo.TermEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
        End If
        If Not paramInfo.FirstName.ToString.Trim = "" Then
            db.AddParameter("@FirstName", paramInfo.FirstName.ToString.Trim, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        If Not paramInfo.LastName.ToString.Trim = "" Then
            db.AddParameter("@LastName", paramInfo.LastName.ToString.Trim, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        If Not paramInfo.StartDate = "01/01/1900" Then
            db.AddParameter("@StartDate", paramInfo.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If
        If Not paramInfo.EndDate = "01/01/1900" Then
            db.AddParameter("@EndDate", paramInfo.EndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If
        Try
            Return db.RunParamSQLDataSet(sb.ToString)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try
    End Function
    Public Function GetCompletedAndExternshipSummary(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim getTardyTrackingDetailsInfo As New TardyAttendanceUnitInfo
        Dim getAttendanceSummaryInfo As New ClockHourAttendanceInfo
        Dim strUnitDescription As String = ""
        Dim intTardiesForProgramVersion As Integer = 0
        Dim strUnitTypeId As String = ""
        Dim intPresentUnitFromTardyCalculation As Decimal = 0.0
        Dim intAbsentUnitFromTardyCalculation As Decimal = 0.0

        Dim decActualTardyHours As Decimal = 0
        Dim decNumberofTardy As Decimal = 0
        Dim decAdjustedActual As Decimal = 0
        Dim decAdjustedAbsent As Decimal = 0
        Dim decAdjustedTardy As Decimal = 0
        Dim decTardiesMakingAbsence As Decimal = 0
        Dim isUseTimeClock As Boolean = False

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim ds As New DataSet
        ds = GetDailyCompletedAndExternshipDetails(paramInfo, "", "", False)

        Dim ds1 As New DataSet
        Dim dt As New DataTable("AttendanceHistory")
        If ds.Tables.Count > 0 Then
            dt.Columns.Add(New DataColumn("LastName", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("FirstName", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("MeetDate", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Hours", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("Comments", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusId", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampGrpId", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("StudentId", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("PrgVerDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("SuppressDate", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Type", System.Type.GetType("System.String")))

            Dim row As DataRow
            For Each dr As DataRow In ds.Tables(0).Rows
                row = dt.NewRow()
                row("LastName") = dr("LastName").ToString
                row("FirstName") = dr("FirstName").ToString
                row("MeetDate") = dr("MeetDate").ToString
                row("Type") = dr("Type").ToString
                If Not dr("ActualHours") Is System.DBNull.Value Then
                    row("Hours") = dr("ActualHours").ToString
                Else
                    row("Hours") = 0.0
                End If
                Try
                    row("SuppressDate") = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
                Catch ex As System.Exception
                    row("SuppressDate") = "no"
                End Try
                row("Comments") = dr("Comments").ToString
                row("CampusId") = dr("CampusId").ToString
                row("CampGrpId") = dr("CampGrpId").ToString
                row("CampusDescrip") = dr("CampusDescrip").ToString
                row("CampGrpDescrip") = dr("CampGrpDescrip").ToString
                row("StudentId") = dr("StudentId").ToString
                row("PrgVerDescrip") = paramInfo.PrgVerDescrip.ToString
                dt.Rows.Add(row)
            Next
            ds1.Tables.Add(dt)
        End If
        Return ds1
    End Function
    Private Function GetStudentSummaryDetails(ByVal paramInfo As ReportParamInfo, _
                                          Optional ByVal AttUnitType As String = "", _
                                          Optional ByVal PrgVerId As String = "", _
                                          Optional ByVal UseTimeClock As Boolean = False) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append(" select Distinct " + vbCrLf)
            .Append(" t4.FirstName,t4.LastName,t1.StuEnrollId,t4.StudentId, " + vbCrLf)
            .Append(" Convert(char(10),RecordDate,101) as MeetDate,SchedHours as Schedule, " + vbCrLf)
            .Append(" Case when t6.UnitTypeDescrip='Present Absent' and ActualHours=1.00 Then t2.Total else ActualHours end as ActualHours, " + vbCrLf)
            .Append(" (DatePart(dw,RecordDate)-1) as dw1,t2.dw,t1.Comments as comments, " + vbCrLf)
            .Append(" IsTardy as StudentIsTardy,t5.TrackTardies, t5.tardiesMakingAbsence,t5.PrgVerId, t5.PrgVerDescrip, " + vbCrLf)
            .Append(" t3.CampusId,syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip, " + vbCrLf)
            .Append(" (SELECT Top 1 CampDescrip FROM syCampuses WHERE CampusId=t3.CampusId) AS CampusDescrip " + vbCrLf)
            .Append(" from " + vbCrLf)
            .Append(" arStudentClockAttendance t1,arProgScheduleDetails t2, " + vbCrLf)
            If Not paramInfo.TermId.ToString.Trim = "" Then
                .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
                .Append("   arResults, arClassSections, arStuEnrollments " + vbCrLf)
                If Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(",arTerm " + vbCrLf)
                End If
                .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
                .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
                If Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(" and arClassSections.termId=arTerm.termid " + vbCrLf)
                End If
                .Append("   and arClassSections.termId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
                If Not paramInfo.TermStartDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate >='" & paramInfo.TermStartDate & "'" + vbCrLf)
                End If
                If Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate <= '" & paramInfo.TermEndDate & "'" + vbCrLf)
                End If
                If paramInfo.PrgVerId <> "" Then
                    .Append(" and arStuEnrollments.PrgVerId in ('")
                    .Append(paramInfo.PrgVerId)
                    .Append("')  " + vbCrLf)
                End If
                .Append(" Union " + vbCrLf)
                .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
                .Append("   from arTransferGrades,arStuEnrollments   " + vbCrLf)
                If Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(",arTerm " + vbCrLf)
                End If
                .Append("   where arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId   " + vbCrLf)
                If Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(" and arTransferGrades.termId=arTerm.termid " + vbCrLf)
                End If
                .Append("   and arTransferGrades.TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
                If Not paramInfo.TermStartDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate >='" & paramInfo.TermStartDate & "'" + vbCrLf)
                End If
                If Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate <= '" & paramInfo.TermEndDate & "'" + vbCrLf)
                End If
                If paramInfo.PrgVerId.ToString.Trim <> "" Then
                    .Append(" and arStuEnrollments.PrgVerId in ('")
                    .Append(paramInfo.PrgVerId)
                    .Append("')  " + vbCrLf)
                End If
                .Append(") t3, " + vbCrLf)
            ElseIf Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
                .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
                .Append("   arResults, arClassSections, arStuEnrollments,arTerm " + vbCrLf)
                .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
                .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
                .Append("   and arClassSections.TermId=arTerm.TermId " + vbCrLf)
                If Not paramInfo.TermStartDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate >='" & paramInfo.TermStartDate & "'" + vbCrLf)
                End If
                If Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate <='" & paramInfo.TermEndDate & "'" + vbCrLf)
                End If
                If Not paramInfo.TermId.ToString.Trim = "" Then
                    .Append("   and arClassSections.TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
                End If
                If paramInfo.PrgVerId <> "" Then
                    .Append(" and arStuEnrollments.PrgVerId in ('")
                    .Append(paramInfo.PrgVerId)
                    .Append("')  " + vbCrLf)
                End If
                .Append(" Union " + vbCrLf)
                .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
                .Append("   from arTransferGrades,arStuEnrollments,arTerm where  " + vbCrLf)
                .Append("   arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId   " + vbCrLf)
                .Append("   and arTransferGrades.TermId=arTerm.TermId " + vbCrLf)
                If Not paramInfo.TermStartDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate >='" & paramInfo.TermStartDate & "'")
                End If
                If Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(" and arTerm.StartDate <='" & paramInfo.TermEndDate & "'")
                End If
                If Not paramInfo.TermId.ToString.Trim = "" Then
                    .Append("   and arTransferGrades.TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
                End If
                If paramInfo.PrgVerId.ToString.Trim <> "" Then
                    .Append(" and arStuEnrollments.PrgVerId in ('")
                    .Append(paramInfo.PrgVerId)
                    .Append("')  " + vbCrLf)
                End If
                .Append(") t3, " + vbCrLf)
            Else
                .Append(" (Select * from arStuEnrollments " + vbCrLf)
                If paramInfo.PrgVerId.ToString.Trim <> "" Then
                    .Append(" where arStuEnrollments.PrgVerId in ('")
                    .Append(paramInfo.PrgVerId)
                    .Append("')  " + vbCrLf)
                End If
                .Append(") t3, " + vbCrLf)
            End If
            .Append(" arStudent t4,arPrgVersions t5,arAttUnitType t6,  " + vbCrLf)
            .Append(" syCampGrps ,syCmpGrpCmps I " + vbCrLf)
            .Append(" where ")
            .Append("   t1.ScheduleId=t2.ScheduleId and t1.StuEnrollId=t3.StuEnrollId and " + vbCrLf)
            .Append("   t3.StudentId=t4.StudentId and t3.PrgVerId = t5.PrgVerId and t5.UnitTypeId=t6.UnitTypeId and " + vbCrLf)
            .Append("   syCampGrps.CampGrpId=I.CampGrpId AND t3.CampusId=I.CampusId " + vbCrLf)
            .Append("  and (DatePart(dw,RecordDate)-1)=t2.dw and " + vbCrLf)
            .Append("   (t1.ActualHours is not null AND t1.ActualHours <> 999.00 AND t1.ActualHours <> 9999.00) " + vbCrLf)
            If Not paramInfo.FirstName.ToString.Trim = "" Then
                .Append(" and t4.FirstName like '" + paramInfo.FirstName.ToString.Trim + "%'" + vbCrLf)
            End If
            If Not paramInfo.LastName.ToString.Trim = "" Then
                .Append(" and t4.LastName like  '" + paramInfo.LastName.ToString.Trim + "%'" + vbCrLf)
            End If
            If Not paramInfo.StartDate = "01/01/1900" Then
                .Append(" and RecordDate >='" & paramInfo.StartDate & "'" + vbCrLf)
            End If
            If Not paramInfo.EndDate = "01/01/1900" Then
                .Append(" and RecordDate <='" & paramInfo.EndDate & "'" + vbCrLf)
            End If
            .Append(" and syCampGrps.CampGrpId in  " + vbCrLf)
            .Append(" (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in ('")
            .Append(paramInfo.CampGrpId)
            .Append("))  " + vbCrLf)
            .Append(" Group by syCampGrps.CampGrpId,t3.CampusId,t4.StudentId,t1.StuEnrollId,LastName, " + vbCrLf)
            .Append(" FirstName,RecordDate,SchedHours,ActualHours,t2.Total,dw,t1.Comments,IsTardy,TrackTardies,tardiesMakingAbsence, " + vbCrLf)
            .Append(" t5.PrgVerId, t5.PrgVerDescrip, syCampGrps.CampGrpDescrip,UnitTypeDescrip ")
            If MyAdvAppSettings.AppSettings("GradesFormat", paramInfo.CampusId).ToString.ToLower = "numeric" Then
                .Append(" union ")
                .Append(" select Distinct " + vbCrLf)
                .Append(" t4.FirstName,t4.LastName,t1.StuEnrollId,t4.StudentId, " + vbCrLf)
                .Append(" Convert(char(10),MeetDate,101) as MeetDate,Schedule as Schedule, " + vbCrLf)
                .Append(" Actual as ActualHours, " + vbCrLf)
                .Append(" NULL,NULL,t1.Comments as comments, " + vbCrLf)
                .Append(" Tardy as StudentIsTardy,t5.TrackTardies, t5.tardiesMakingAbsence,t5.PrgVerId, t5.PrgVerDescrip, " + vbCrLf)
                .Append(" t3.CampusId,syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip, " + vbCrLf)
                .Append(" (SELECT Top 1 CampDescrip FROM syCampuses WHERE CampusId=t3.CampusId) AS CampusDescrip " + vbCrLf)
                .Append(" from " + vbCrLf)
                .Append(" atConversionAttendance t1,arProgScheduleDetails t2, " + vbCrLf)
                If Not paramInfo.TermId.ToString.Trim = "" Then
                    .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                    .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
                    .Append("   arResults, arClassSections, arStuEnrollments " + vbCrLf)
                    If Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
                        .Append(",arTerm ")
                    End If
                    .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
                    .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
                    If Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
                        .Append(" and arClassSections.termId=arTerm.termid ")
                    End If
                    .Append("   and arClassSections.TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
                    If Not paramInfo.TermStartDate = "01/01/1900" Then
                        .Append(" and arTerm.StartDate >='" & paramInfo.TermStartDate & "'")
                    End If
                    If Not paramInfo.TermEndDate = "01/01/1900" Then
                        .Append(" and arTerm.StartDate <= '" & paramInfo.TermEndDate & "'" + vbCrLf)
                    End If
                    If paramInfo.PrgVerId.ToString.Trim <> "" Then
                        .Append(" and arStuEnrollments.PrgVerId in ('")
                        .Append(paramInfo.PrgVerId)
                        .Append("')  " + vbCrLf)
                    End If
                    .Append(" Union " + vbCrLf)
                    .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                    .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
                    .Append("   from arTransferGrades,arStuEnrollments   " + vbCrLf)
                    If Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
                        .Append(",arTerm ")
                    End If
                    .Append("  where arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId  " + vbCrLf)
                    If Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
                        .Append(" and arTransferGrades.termId=arTerm.termid ")
                    End If
                    .Append("   and arTransferGrades.TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
                    If Not paramInfo.TermStartDate = "01/01/1900" Then
                        .Append(" and arTerm.StartDate >='" & paramInfo.TermStartDate & "'")
                    End If
                    If Not paramInfo.TermEndDate = "01/01/1900" Then
                        .Append(" and arTerm.StartDate <= '" & paramInfo.TermEndDate & "'" + vbCrLf)
                    End If
                    If paramInfo.PrgVerId.ToString.Trim <> "" Then
                        .Append(" and arStuEnrollments.PrgVerId in ('")
                        .Append(paramInfo.PrgVerId)
                        .Append("')  " + vbCrLf)
                    End If
                    .Append(") t3, " + vbCrLf)
                ElseIf Not paramInfo.TermStartDate = "01/01/1900" Or Not paramInfo.TermEndDate = "01/01/1900" Then
                    .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                    .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
                    .Append("   arResults, arClassSections, arStuEnrollments,arTerm " + vbCrLf)
                    .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
                    .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
                    .Append("   and arClassSections.TermId=arTerm.TermId " + vbCrLf)
                    If Not paramInfo.TermStartDate = "01/01/1900" Then
                        .Append(" and arTerm.StartDate >='" & paramInfo.TermStartDate & "'")
                    End If
                    If Not paramInfo.TermEndDate = "01/01/1900" Then
                        .Append(" and arTerm.StartDate <= '" & paramInfo.TermEndDate & "'" + vbCrLf)
                    End If
                    If Not paramInfo.TermId.ToString.Trim = "" Then
                        .Append("   and arClassSections.TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
                    End If
                    If paramInfo.PrgVerId <> "" Then
                        .Append(" and arStuEnrollments.PrgVerId in ('")
                        .Append(paramInfo.PrgVerId)
                        .Append("')  " + vbCrLf)
                    End If
                    .Append(" Union " + vbCrLf)
                    .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
                    .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
                    .Append("   from arTransferGrades,arStuEnrollments,arTerm where  " + vbCrLf)
                    .Append("   arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId   " + vbCrLf)
                    .Append("   and arTransferGrades.TermId=arTerm.TermId " + vbCrLf)
                    If Not paramInfo.TermStartDate = "01/01/1900" Then
                        .Append(" and arTerm.StartDate >='" & paramInfo.TermStartDate & "'")
                    End If
                    If Not paramInfo.TermEndDate = "01/01/1900" Then
                        .Append(" and arTerm.StartDate <= '" & paramInfo.TermEndDate & "'" + vbCrLf)
                    End If
                    If Not paramInfo.TermId.ToString.Trim = "" Then
                        .Append("   and arTransferGrades.TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
                    End If
                    If paramInfo.PrgVerId.ToString.Trim <> "" Then
                        .Append(" and arStuEnrollments.PrgVerId in ('")
                        .Append(paramInfo.PrgVerId)
                        .Append("')  " + vbCrLf)
                    End If
                    .Append(") t3, " + vbCrLf)
                Else
                    .Append(" (Select * from arStuEnrollments " + vbCrLf)
                    If paramInfo.PrgVerId.ToString.Trim <> "" Then
                        .Append(" where arStuEnrollments.PrgVerId in ('")
                        .Append(paramInfo.PrgVerId)
                        .Append("')  " + vbCrLf)
                    End If
                    .Append(") t3, " + vbCrLf)
                End If
                .Append("   arStudent t4,arPrgVersions t5,  " + vbCrLf)
                .Append(" syCampGrps ,syCmpGrpCmps I " + vbCrLf)
                .Append(" where ")
                .Append("   t1.StuEnrollId=t3.StuEnrollId and " + vbCrLf)
                .Append("   t3.StudentId=t4.StudentId and t3.PrgVerId = t5.PrgVerId and " + vbCrLf)
                .Append("   syCampGrps.CampGrpId=I.CampGrpId AND t3.CampusId=I.CampusId " + vbCrLf)
                .Append("   and (DatePart(dw,MeetDate)-1)=t2.dw and " + vbCrLf)
                .Append("   (t1.Actual is not null AND t1.Actual <> 999.00 AND t1.Actual <> 9999.00) " + vbCrLf)
                If Not paramInfo.FirstName.ToString.Trim = "" Then
                    .Append(" and t4.FirstName like '" + paramInfo.FirstName.ToString.Trim + "%'" + vbCrLf)
                End If
                If Not paramInfo.LastName.ToString.Trim = "" Then
                    .Append(" and t4.LastName like  '" + paramInfo.LastName.ToString.Trim + "%'" + vbCrLf)
                End If
                'If Not paramInfo.StartDate = "01/01/1900" Then
                '    .Append(" and RecordDate >='" & paramInfo.StartDate & "'" + vbCrLf)
                'End If
                'If Not paramInfo.EndDate = "01/01/1900" Then
                '    .Append(" and RecordDate <='" & paramInfo.EndDate & "'" + vbCrLf)
                'End If
                'Mantis 17610 code modified by balaji on 09/29/2009
                'changed from RecordDate to MeetDate
                If Not paramInfo.StartDate = "01/01/1900" Then
                    .Append(" and MeetDate >='" & paramInfo.StartDate & "'" + vbCrLf)
                End If
                If Not paramInfo.EndDate = "01/01/1900" Then
                    .Append(" and MeetDate <='" & paramInfo.EndDate & "'" + vbCrLf)
                End If
                'Mantis 17610 modification ends here
                .Append(" and syCampGrps.CampGrpId in  " + vbCrLf)
                .Append(" (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in ('")
                .Append(paramInfo.CampGrpId)
                .Append("))  " + vbCrLf)
                .Append(" Group by syCampGrps.CampGrpId,t3.CampusId,t4.StudentId,t1.StuEnrollId,LastName, " + vbCrLf)
                .Append(" FirstName,MeetDate,Schedule,Actual,Tardy,t1.comments,TrackTardies,tardiesMakingAbsence, " + vbCrLf)
                .Append(" t5.PrgVerId, t5.PrgVerDescrip, syCampGrps.CampGrpDescrip " + vbCrLf)
            End If
            .Append(" Order by syCampGrps.CampGrpDescrip,t3.CampusId,t4.LastName,t4.FirstName,MeetDate " + vbCrLf)
        End With

        Try
            Return db.RunParamSQLDataSet(sb.ToString)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try


        'If school is using timeclock then use time clock will be set to 1 in Program Version page
        'ElseIf UseTimeClock = True Then
        '    With sb
        '        .Append(" select Distinct " + vbCrLf)
        '        .Append(" t4.FirstName,t4.LastName,t1.StuEnrollId,t4.StudentId, " + vbCrLf)
        '        .Append(" Convert(char(10),RecordDate,101) as MeetDate,SchedHours as Schedule, " + vbCrLf)
        '        .Append(" ActualHours, " + vbCrLf)
        '        .Append(" (DatePart(dw,RecordDate)-1) as dw1,t2.dw,NULL as comments, " + vbCrLf)
        '        .Append(" Case When t2.Check_Tardyin=1 and Convert(char(10),t6.PunchTime,101) = Convert(char(10),RecordDate,101) and " + vbCrLf)
        '        .Append(" Convert(char(8),t6.PunchTime,108) < Convert(char(8),t2.max_beforetardy,108) " + vbCrLf)
        '        .Append(" Then  1 else 0 End as StudentIsTardy, " + vbCrLf)
        '        .Append(" t5.TrackTardies, t5.tardiesMakingAbsence,t5.PrgVerId, t5.PrgVerDescrip, " + vbCrLf)
        '        .Append(" t3.CampusId,syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip, " + vbCrLf)
        '        .Append(" (SELECT Top 1 CampDescrip FROM syCampuses WHERE CampusId=t3.CampusId) AS CampusDescrip " + vbCrLf)
        '        .Append(" from " + vbCrLf)
        '        .Append(" arStudentClockAttendance t1,arProgScheduleDetails t2, " + vbCrLf)
        '        If Not paramInfo.TermId.ToString.Trim = "" Then
        '            .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
        '            .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
        '            .Append("   arResults, arClassSections, arStuEnrollments " + vbCrLf)
        '            .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
        '            .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
        '            .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
        '            If paramInfo.PrgVerId <> "" Then
        '                .Append(" and arStuEnrollments.PrgVerId in ('")
        '                .Append(paramInfo.PrgVerId)
        '                .Append("')  " + vbCrLf)
        '            End If
        '            .Append(" Union " + vbCrLf)
        '            .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
        '            .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
        '            .Append("   from arTransferGrades,arStuEnrollments where  " + vbCrLf)
        '            .Append("   arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId   " + vbCrLf)
        '            .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
        '            If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '                .Append(" and arStuEnrollments.PrgVerId in ('")
        '                .Append(paramInfo.PrgVerId)
        '                .Append("')  " + vbCrLf)
        '            End If
        '            .Append(") t3, " + vbCrLf)
        '        Else
        '            .Append(" (Select * from arStuEnrollments " + vbCrLf)
        '            If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '                .Append(" where arStuEnrollments.PrgVerId in ('")
        '                .Append(paramInfo.PrgVerId)
        '                .Append("')  " + vbCrLf)
        '            End If
        '            .Append(") t3, " + vbCrLf)
        '        End If
        '        .Append(" arStudent t4,arPrgVersions t5,  " + vbCrLf)
        '        .Append(" arStudentTimeClockPunches t6, " + vbCrLf)
        '        .Append(" syCampGrps ,syCmpGrpCmps I " + vbCrLf)
        '        .Append(" where ")
        '        .Append("   t1.ScheduleId=t2.ScheduleId and t1.StuEnrollId=t3.StuEnrollId and " + vbCrLf)
        '        .Append("   t3.StudentId=t4.StudentId and t3.PrgVerId = t5.PrgVerId and " + vbCrLf)
        '        .Append("   t6.StuEnrollID = t3.StuEnrollId and t6.PunchType=1 and " + vbCrLf)
        '        .Append("   syCampGrps.CampGrpId=I.CampGrpId AND t3.CampusId=I.CampusId " + vbCrLf)
        '        .Append("  and (DatePart(dw,RecordDate)-1)=t2.dw and " + vbCrLf)
        '        .Append("   (t1.ActualHours is not null AND t1.ActualHours <> 999.00 AND t1.ActualHours <> 9999.00) " + vbCrLf)
        '        If Not paramInfo.FirstName.ToString.Trim = "" Then
        '            .Append(" and t4.FirstName like  + ? + '%'")
        '        End If
        '        If Not paramInfo.LastName.ToString.Trim = "" Then
        '            .Append(" and t4.LastName like  + ? + '%'")
        '        End If
        '        If Not paramInfo.StartDate = "01/01/1900" Then
        '            .Append(" and RecordDate >= ? ")
        '        End If
        '        If Not paramInfo.EndDate = "01/01/1900" Then
        '            .Append(" and RecordDate <= ? ")
        '        End If
        '        .Append(" and syCampGrps.CampGrpId in  " + vbCrLf)
        '        .Append(" (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in ('")
        '        .Append(paramInfo.CampGrpId)
        '        .Append("'))  " + vbCrLf)
        '        .Append(" Group by syCampGrps.CampGrpId,t3.CampusId,t4.StudentId,t1.StuEnrollId,LastName, " + vbCrLf)
        '        .Append(" FirstName,RecordDate,SchedHours,ActualHours,t2.Total,dw,IsTardy,TrackTardies,tardiesMakingAbsence, " + vbCrLf)
        '        .Append(" t5.PrgVerId, t5.PrgVerDescrip, syCampGrps.CampGrpDescrip ")
        '        If SingletonAppSettings.AppSettings("GradesFormat",CampusId).ToString.ToLower = "numeric" Then
        '            .Append(" union ")
        '            .Append(" select Distinct " + vbCrLf)
        '            .Append(" t4.FirstName,t4.LastName,t1.StuEnrollId,t4.StudentId, " + vbCrLf)
        '            .Append(" Convert(char(10),MeetDate,101) as MeetDate,Schedule as Schedule, " + vbCrLf)
        '            .Append(" Actual as ActualHours, " + vbCrLf)
        '            .Append(" NULL,NULL,NULL as comments, " + vbCrLf)
        '            .Append(" Case When t2.Check_Tardyin=1 and Convert(char(10),t6.PunchTime,101) = Convert(char(10),MeetDate,101) and " + vbCrLf)
        '            .Append(" Convert(char(8),t6.PunchTime,108) < Convert(char(8),t2.max_beforetardy,108) " + vbCrLf)
        '            .Append(" Then  1 else 0 End as StudentIsTardy, " + vbCrLf)
        '            .Append(" t5.TrackTardies, t5.tardiesMakingAbsence,t5.PrgVerId, t5.PrgVerDescrip, " + vbCrLf)
        '            .Append(" t3.CampusId,syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip, " + vbCrLf)
        '            .Append(" (SELECT Top 1 CampDescrip FROM syCampuses WHERE CampusId=t3.CampusId) AS CampusDescrip " + vbCrLf)
        '            .Append(" from " + vbCrLf)
        '            .Append(" atConversionAttendance t1,arProgScheduleDetails t2, " + vbCrLf)
        '            If Not paramInfo.TermId.ToString.Trim = "" Then
        '                .Append(" (select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
        '                .Append("   arStuEnrollments.StudentId,arStuEnrollments.CampusId from " + vbCrLf)
        '                .Append("   arResults, arClassSections, arStuEnrollments " + vbCrLf)
        '                .Append("   where   arResults.TestId = arClassSections.ClsSectionId and " + vbCrLf)
        '                .Append("   arResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
        '                .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
        '                If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '                    .Append(" and arStuEnrollments.PrgVerId in ('")
        '                    .Append(paramInfo.PrgVerId)
        '                    .Append("')  " + vbCrLf)
        '                End If
        '                .Append(" Union " + vbCrLf)
        '                .Append("   Select Distinct arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId, " + vbCrLf)
        '                .Append("   arStuEnrollments.StudentId, arStuEnrollments.CampusId " + vbCrLf)
        '                .Append("   from arTransferGrades,arStuEnrollments where  " + vbCrLf)
        '                .Append("   arTransferGrades.StuEnrollId=arStuEnrollments.StuEnrollId  " + vbCrLf)
        '                .Append("   and TermId='" & paramInfo.TermId.ToString.Trim & "'" + vbCrLf)
        '                If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '                    .Append(" and arStuEnrollments.PrgVerId in ('")
        '                    .Append(paramInfo.PrgVerId)
        '                    .Append("')  " + vbCrLf)
        '                End If
        '                .Append(") t3, " + vbCrLf)
        '            Else
        '                .Append(" (Select * from arStuEnrollments " + vbCrLf)
        '                If paramInfo.PrgVerId.ToString.Trim <> "" Then
        '                    .Append(" where arStuEnrollments.PrgVerId in ('")
        '                    .Append(paramInfo.PrgVerId)
        '                    .Append("')  " + vbCrLf)
        '                End If
        '                .Append(") t3, " + vbCrLf)
        '            End If
        '            .Append("   arStudent t4,arPrgVersions t5,  " + vbCrLf)
        '            .Append(" arStudentTimeClockPunches t6, " + vbCrLf)
        '            .Append(" syCampGrps ,syCmpGrpCmps I " + vbCrLf)
        '            .Append(" where ")
        '            .Append("   t1.StuEnrollId=t3.StuEnrollId and " + vbCrLf)
        '            .Append("   t3.StudentId=t4.StudentId and t3.PrgVerId = t5.PrgVerId and " + vbCrLf)
        '            .Append("   t6.StuEnrollID = t3.StuEnrollId and t6.PunchType=1 and " + vbCrLf)
        '            .Append("   syCampGrps.CampGrpId=I.CampGrpId AND t3.CampusId=I.CampusId " + vbCrLf)
        '            .Append("   and (DatePart(dw,MeetDate)-1)=t2.dw and " + vbCrLf)
        '            .Append("   (t1.Actual is not null AND t1.Actual <> 999.00 AND t1.Actual <> 9999.00) " + vbCrLf)
        '            If Not paramInfo.FirstName.ToString.Trim = "" Then
        '                .Append(" and t4.FirstName like  + ? + '%'")
        '            End If
        '            If Not paramInfo.LastName.ToString.Trim = "" Then
        '                .Append(" and t4.LastName like  + ? + '%'")
        '            End If
        '            If Not paramInfo.StartDate = "01/01/1900" Then
        '                .Append(" and MeetDate >= ? ")
        '            End If
        '            If Not paramInfo.EndDate = "01/01/1900" Then
        '                .Append(" and MeetDate <= ? ")
        '            End If
        '            .Append(" and syCampGrps.CampGrpId in  " + vbCrLf)
        '            .Append(" (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in ('")
        '            .Append(paramInfo.CampGrpId)
        '            .Append("'))  " + vbCrLf)
        '            .Append(" Group by syCampGrps.CampGrpId,t3.CampusId,t4.StudentId,t1.StuEnrollId,LastName, " + vbCrLf)
        '            .Append(" FirstName,MeetDate,Schedule,Actual,Tardy,TrackTardies,tardiesMakingAbsence, " + vbCrLf)
        '            .Append(" t5.PrgVerId, t5.PrgVerDescrip, syCampGrps.CampGrpDescrip " + vbCrLf)
        '        End If
        '        .Append(" Order by syCampGrps.CampGrpDescrip,t3.CampusId,t4.LastName,t4.FirstName,MeetDate " + vbCrLf)
        '    End With
        '    Return db.RunParamSQLDataSet(sb.ToString)
        ' End If
    End Function
    ''New Code Added By Vijay Ramteke On October 22, 2010 For Rally Id DE1007
    Public Function GetStudentNames(ByVal ProgId As String, ByVal PrgVerId As String, ByVal StuName As String, ByVal StuStatus As String, ByVal StartDate As String, ByVal EndDate As String, ByVal UseTimeClock As String, ByVal BadgeNum As String, Optional ByVal campusId As String = "", Optional ByVal Studentgrpid As String = "", Optional ByVal CohortStartDate As String = "", Optional ByVal outofschoolfilter As String = "") As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If StuStatus <> "" And outofschoolfilter <> "" Then
            StuStatus = StuStatus & ",'" & outofschoolfilter
        ElseIf StuStatus = "" And outofschoolfilter <> "" Then
            StuStatus = outofschoolfilter
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'build the sql query
        With sb
            .Append(" select Distinct t2.StuEnrollId,t3.LastName,t3.LastName+ ',' + t3.FirstName + ' - ' + t4.PrgVerDescrip as FullName,t3.StudentId ")
            .Append(" from arStudentClockAttendance t1,arStuEnrollments t2,arStudent t3,arPrgVersions t4, arStudentSchedules t5 ")
            If Studentgrpid IsNot Nothing AndAlso Studentgrpid <> "" Then
                sb.Append(" ,adLeadByLeadGroups t6 " + vbCrLf)
            End If
            .Append(" where t1.StuEnrollId = t2.StuEnrollId And t2.StudentId = t3.Studentid and t2.PrgVerId=t4.PrgVerId  ")
            .Append(" and t5.StuEnrollId = t2.StuEnrollId and t5.Active=1 ")
            If Studentgrpid IsNot Nothing AndAlso Studentgrpid <> "" Then
                sb.Append(" and t2.StuEnrollId = t6.StuEnrollId " + vbCrLf)
                sb.AppendFormat(" and t6.LeadGrpId = '{0}' {1}", Studentgrpid, vbCrLf)
            End If
            If campusId <> "" Then
                sb.Append("     and t2.CampusId = '" & campusId & "'" + vbCrLf)
            End If
            If StuName IsNot Nothing AndAlso StuName <> "" Then
                Dim i As Integer = StuName.IndexOf(" ")
                Dim LastName As String = StuName.Trim()
                Dim FirstName As String = ""
                If i > 0 AndAlso i < StuName.Length - 1 Then
                    LastName = StuName.Substring(i, StuName.Length - i).Trim()
                    FirstName = StuName.Substring(0, i).Trim()
                End If
                If LastName <> "" And FirstName <> "" Then
                    sb.AppendFormat("       And(( t3.LastName like '{0}%'  and t3.FirstName like '{1}%') or ( t3.LastName like '{1}%'  and t3.FirstName like '{0}%') )", LastName, FirstName)
                ElseIf LastName <> "" Then
                    sb.AppendFormat("       and( t3.LastName like '{0}%' or t3.FirstName like '{0}%'){1}", LastName, vbCrLf)
                ElseIf FirstName <> "" Then
                    sb.AppendFormat("       and( t3.FirstName like '{0}%' or t3.LastName like '{0}%'){1}", FirstName, vbCrLf)
                End If
            End If
            If StuStatus IsNot Nothing AndAlso StuStatus <> "" Then
                sb.AppendFormat(" and t2.StatusCodeId in ('")
                sb.AppendFormat(StuStatus)
                sb.AppendFormat(") ")
            End If
            If StartDate IsNot Nothing AndAlso StartDate <> "" Then
                sb.AppendFormat("       and (t5.StartDate is null or t5.StartDate >= '{0}') {1}", StartDate, vbCrLf)
            End If
            If EndDate IsNot Nothing AndAlso EndDate <> "" Then
                sb.AppendFormat("       and (t5.EndDate is null or t5.EndDate <= '{0}') {1}", StartDate, vbCrLf)
            End If
            If UseTimeClock IsNot Nothing AndAlso UseTimeClock <> "" Then
                If UseTimeClock.ToLower = "true" Or UseTimeClock = "1" Then
                    sb.Append("       and t4.UseTimeClock = 1" + vbCrLf)
                Else
                    sb.Append("       and (t4.UseTimeClock = 0 or t4.UseTimeClock is null)" + vbCrLf)
                End If
            End If
            If ProgId IsNot Nothing AndAlso ProgId <> "" Then
                sb.AppendFormat("       and t4.ProgId = '{0}' {1}", ProgId, vbCrLf)
            End If
            If PrgVerId IsNot Nothing AndAlso PrgVerId <> "" Then
                sb.AppendFormat("       and t4.PrgVerId = '{0}' {1}", PrgVerId, vbCrLf)
            End If
            If BadgeNum IsNot Nothing AndAlso BadgeNum <> "" Then
                sb.AppendFormat("       and t2.BadgeNumber = '{0}' {1}", BadgeNum, vbCrLf)
            End If
            If CohortStartDate IsNot Nothing AndAlso CohortStartDate <> "" Then
                sb.AppendFormat(" and t2.CohortStartDate = '{0}' {1}", CohortStartDate, vbCrLf)
            End If
            .Append(" order by t3.LastName ")
        End With
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds.Tables(0)
    End Function
    ''New Code Added By Vijay Ramteke On October 22, 2010 For Rally Id DE1007
End Class
