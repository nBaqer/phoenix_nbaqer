Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' CountyDB.vb
'
' CountyDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class SDFModulePageDB
    Public Function GetAllSDF() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("select SDFID, CASE WHEN sysdf.isrequired=1 THEN SDFDescrip + ' (REQUIRED)' ELSE SDFDescrip END as SDFDescrip from sySDF,syStatuses where sySDF.StatusId=syStatuses.StatusId and Status='Active' order by SDFDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    'Public Function GetAllSDFVisibilty() As DataSet

    '    'connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder

    '    'build the sql query
    '    With sb
    '        .Append("select VisId,SDFVisibility from sySDFVisiblity order by VisID ")
    '    End With

    '    'return dataset
    '    Return db.RunSQLDataSet(sb.ToString)

    'End Function

    Public Function GetAllModuleBySDF() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select Distinct ResourceId,Resource from syResources where ResourceTypeId = 1 ")
            .Append(" and ResourceId not in (195,300) ")
            .Append(" order by Resource ")
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllModuleByResourceID(ByVal ResourceID As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append(" select ResourceURL from syResources where ResourceId = ? ")
        End With
        db.AddParameter("@ResourceId", ResourceID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllModulePages(ByVal ModuleCode As String) As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        '' Dim rsURL As String

        'build the sql query
        With sb
            .Append(" select ResourceID,Resource from syResources ")
            .Append(" where  ResourceURL LIke ?")
        End With
        db.AddParameter("@ModuleCode", ModuleCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
        'Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllModulePagesBySDF1(ByVal SDF As String) As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        '' Dim rsURL As String

        'build the sql query
        With sb
            .Append(" SELECT Distinct PkId, (Select Resource from syResources where ResourceID=syResourceSdf.ResourceID) as Resource ")
            .Append(" FROM   syResourceSdf where SDFID= ? ")
        End With

        db.AddParameter("@SDFID", SDF, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllModulePagesBySDF(ByVal SDF As String) As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        ''   Dim rsURL As String

        'build the sql query




        With sb
            .Append(" SELECT Distinct PkId, (Select Resource from syResources where ResourceID=syResourceSdf.ResourceID) as Resource ")
            .Append(" FROM   syResourceSdf where SDFID= ? ")
        End With

        db.AddParameter("@SDFID", SDF, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    'Public Function GetAllVisibilityByModSDF(ByVal SDF As String, ByVal SDFText As String, ByVal ModuleCode As String) As DataSet

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    'connect to the database
    '    Dim db As New DataAccess
    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder
    '    '' Dim rsURL As String

    '    'build the sql query

    '    'If this is a required field, do not send back the "View" visibility option
    '    Dim indexOfRequried As Int32
    '    indexOfRequried = SDFText.IndexOf("REQUIRED", 0, System.StringComparison.Ordinal)


    '    With sb
    '        '.Append(" select (select SDFVisibility from sySDFVisiblity where VisID = sySDFModVis.VisID)  ")
    '        '.Append(" as Visibility from sySDFModVis where SDFID = ? and ModuleID = ")
    '        '.Append(" (select ModuleID from syModules where ModuleCode=?) ")
    '        If indexOfRequried > 0 Then
    '            .Append(" select Distinct SDFVisibility from sySDFVisiblity WHERE SDFVisibility <> 'View' ")
    '        Else
    '            .Append(" select Distinct SDFVisibility from sySDFVisiblity ")
    '        End If

    '    End With

    '    'db.AddParameter("@SDFID", SDF, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    'db.AddParameter("@ModuleCode", ModuleCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    'return dataset
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function



    'Public Function AddModulePage(ByVal ModulePage As StudentDocsInfo, ByVal PKID As String, ByVal user As String) As Integer

    '    ''Connect To The Database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '    'Do an Update
    '    'Try
    '    '    'Build The Query
    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append("Insert syResourceSDF(PKID,ResourceID,ModuleID,SDFID, ")
    '        .Append(" ModUser,ModDate) ")
    '        .Append(" Values(?,?,?,?,?,?) ")
    '    End With

    '    '    'Add Parameters To Query

    '    db.AddParameter("@PKId", PKID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '    'ResourceID
    '    db.AddParameter("@ResourceId", ModulePage.Pages, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '    'ResourceID
    '    db.AddParameter("@ModuleId", ModulePage.ModuleName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '    'SDFID
    '    db.AddParameter("@SDFId", ModulePage.SDF, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


    '    '    '   ModUser
    '    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '    '    '   ModDate
    '    db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '    '    'Execute The Query
    '    db.RunParamSQLExecuteNoneQuery(sb.ToString)

    '    ModulePage.IsInDb = True

    '    'Retun Without Errors
    '    Return 0

    '    'Catch ex As System.Exception
    '    'Return an Error To Client
    '    '   Return -1

    '    '  Finally
    '    'Close Connection
    '    db.CloseConnection()
    '    '     End Try
    'End Function
    ''Public Function UpdateModulePage(ByVal ModulePage As StudentDocsInfo, ByVal PKID As String, ByVal user As String) As Integer

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    ''Connect To The Database
    '    Dim db As New DataAccess
    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '    'Do an Update
    '    'Try
    '    '    'Build The Query
    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append("Update syResourceSDF set ResourceID=?,ModuleID=?,SDFID=?, ")
    '        .Append(" ModUser=?,ModDate= ?  ")
    '        .Append("where PkID = ? ")
    '    End With

    '    '    'Add Parameters To Query

    '    'ResourceID
    '    db.AddParameter("@ResourceId", ModulePage.Pages, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '    'ResourceID
    '    db.AddParameter("@ModuleId", ModulePage.ModuleName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '    'SDFID
    '    db.AddParameter("@SDFId", ModulePage.SDF, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


    '    '    '   ModUser
    '    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '    '    '   ModDate
    '    db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '    db.AddParameter("@PKID", PKID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


    '    '    'Execute The Query
    '    db.RunParamSQLExecuteNoneQuery(sb.ToString)

    '    ModulePage.IsInDb = True

    '    'Retun Without Errors
    '    Return 0

    '    'Catch ex As System.Exception
    '    'Return an Error To Client
    '    '   Return -1

    '    '  Finally
    '    'Close Connection
    '    db.CloseConnection()
    '    '     End Try
    'End Function

    'Public Function GetModuleDetails(ByVal PKID As String) As StudentDocsInfo
    '    'connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    'build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        'With subqueries
    '        .Append(" select ResourceID,ModuleID,SDFID ")
    '        .Append(" from syResourceSDF where PKId= ? ")
    '    End With

    '    'Add the EmployerContactId the parameter list
    '    db.AddParameter("@PKID", PKID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    'Execute the query
    '    Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

    '    Dim StudentDocs As New StudentDocsInfo

    '    While dr.Read()
    '        With StudentDocs
    '            .IsInDb = True
    '            .ModuleName = dr("ModuleID")
    '            .Pages = dr("ResourceID")
    '            .SDF = dr("sdfid")
    '        End With
    '    End While

    '    If Not dr.IsClosed Then dr.Close()
    '    If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    '    'Return BankInfo
    '    Return StudentDocs
    'End Function
    'Public Function UpdateModules(ByVal SDFID As String, ByVal intModuleResId As Integer, ByVal user As String, ByVal selectedDegrees() As String, _
    '                              ByVal selectedVisibilty() As String, _
    '                              ByVal entityId As Integer)
    '    '   connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   First we have to delete all existing selections
    '    '   build the query

    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append("DELETE FROM syResourceSDF WHERE SDFID = ? ")
    '        '.Append(" and ModuleId = ? ")
    '        .Append(" and EntityId=? ")
    '    End With

    '    '   delete all selected items
    '    db.AddParameter("@SDFId", SDFID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@EntityId", entityId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)

    '    db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '    db.ClearParameters()
    '    sb.Remove(0, sb.Length)

    '    '   Insert one record per each Item in the Selected Group
    '    If selectedDegrees.Length >= 1 Then
    '        Dim i As Integer
    '        For i = 0 To selectedDegrees.Length - 1

    '            '   build query
    '            With sb
    '                .Append("INSERT INTO syResourceSDF (SDFID, ModuleId,ResourceID,SDFVisibilty,ModDate, ModUser, EntityId) ")
    '                .Append("VALUES(?,?,?,?,?,?,?)")
    '            End With

    '            'add parameters
    '            db.AddParameter("@SDFId", SDFID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.AddParameter("@ModuleId", intModuleResId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
    '            db.AddParameter("@ResourceId", DirectCast(selectedDegrees.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.AddParameter("@SDFVisibility", DirectCast(selectedVisibilty.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.AddParameter("@EntityId", entityId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)

    '            'execute query
    '            db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '            db.ClearParameters()
    '            sb.Remove(0, sb.Length)
    '        Next
    '    End If
    '    'Close Connection
    '    db.CloseConnection()
    '    Return 1
    'End Function
    'Public Function GetAllResourceSelected(ByVal SDFID As String, ByVal intModuleResourceId As Integer, Optional ByVal EntityId As Integer = 0) As DataSet
    '    '   connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder

    '    '   build the sql query
    '    With sb
    '        '.Append(" Select ResourceID,(select Resource from syResources where ResourceId=syResourceSDF.ResourceID) + ' - ' +  SDFVisibilty as Resource ")
    '        '.Append(" from syResourceSDF  ")
    '        ''.Append(" where SDFID = ? and ModuleID = ? ")
    '        '.Append(" where SDFID = ? ")
    '        .Append(" Select distinct t1.ResourceID,(select Resource from syResources where ResourceId=t1.ResourceID) + ' - ' +  ")
    '        .Append(" SDFVisibilty as Resource  ")
    '        .Append(" from syResourceSDF t1 inner join syAdvantageResourceRelations t2 on t1.ResourceId=t2.RelatedResourceId ")
    '        .Append(" where t2.ResourceId IN (?)  and SDFID = ? ")
    '    End With

    '    'Add the EmployerContactId the parameter list
    '    db.AddParameter("@EntityId", EntityId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
    '    db.AddParameter("@SDFID", SDFID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


    '    '   return dataset
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function
    'Public Function GetAllResourceNotSelected(ByVal SDFID As String, ByVal ModuleID As String, ByVal URL As String) As DataSet
    '    '   connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder

    '    Dim strModExp As String
    '    Dim strExStudentRes As Integer
    '    Dim strExEmployerRes As Integer

    '    Select Case ModuleID.ToString
    '        Case Is = "AD"
    '            strModExp = "Admissions"
    '            strExStudentRes = 153
    '        Case Is = "AR"
    '            strModExp = "Academic Records"
    '            strExStudentRes = 108
    '        Case Is = "PL"
    '            strModExp = "Placement"
    '            strExStudentRes = 198
    '            strExEmployerRes = 197
    '        Case Is = "SA"
    '            strModExp = "Student Accounts"
    '            strExStudentRes = 231
    '        Case Is = "HR"
    '            strModExp = "Human Resources"
    '            strExStudentRes = 136
    '        Case Is = "FA"
    '            strModExp = "Financial Aid"
    '            strExStudentRes = 308
    '    End Select


    '    '   build the sql query
    '    With sb
    '        '.Append(" select ResourceID,Resource,ResourceURL from syResources where ResourceId not in ")
    '        '.Append(" (Select ResourceID from syResourceSDF where SDFID=? and ModuleID=?)   ")
    '        '.Append(" and ResourceURL like ? and ResourceTypeId=3 and AllowSchlReqFlds=1 ")

    '        'All Common Task Nodes
    '        .Append(" select t1.HierarchyId,t1.HierarchyName as Resource,t1.ResourceId from  ")
    '        .Append(" syNavigationnodes t1,syResources t2 ")
    '        .Append(" where t1.ResourceId = t2.resourceId and t2.AllowSchlReqFlds = 1 and t1.ResourceId not in ")
    '        .Append(" (Select ResourceID from syResourceSDF where SDFID=? and ModuleID=?) ")
    '        .Append(" and ParentId in (select HierarchyId from synavigationnodes where ParentId in ")
    '        .Append(" (select HierarchyId from synavigationnodes where HierarchyName=?)) ")
    '        .Append(" union ")
    '        'Student Nodes
    '        .Append(" select t1.HierarchyId,t1.HierarchyName as Resource,t1.ResourceId from ")
    '        .Append(" syNavigationNodes t1,syResources t2 ")
    '        .Append(" where t1.ResourceId = t2.resourceId And t2.AllowSchlReqFlds = 1 ")
    '        .Append(" and t1.ResourceId not in ")
    '        .Append(" (Select ResourceID from syResourceSDF where SDFID=? and ModuleID=?) ")
    '        .Append(" and t1.ParentId in ")
    '        .Append(" (select HierarchyId from syNavigationNodes where ParentId in ")
    '        .Append(" (select HierarchyId from syNavigationNodes where  ParentId in ")
    '        .Append(" (select HierarchyId from synavigationnodes where HierarchyName=?)) ")
    '        .Append(" and ResourceId=?) ")
    '        If ModuleID.ToString = "PL" Then
    '            .Append(" union ")
    '            .Append(" select t1.HierarchyId,t1.HierarchyName as Resource,t1.ResourceId from ")
    '            .Append(" syNavigationNodes t1,syResources t2 ")
    '            .Append(" where t1.ResourceId = t2.resourceId And t2.AllowSchlReqFlds = 1 ")
    '            .Append(" and t1.ResourceId not in ")
    '            .Append(" (Select ResourceID from syResourceSDF where SDFID=? and ModuleID=?) ")
    '            .Append(" and t1.ParentId in ")
    '            .Append(" (select HierarchyId from syNavigationNodes where ParentId in ")
    '            .Append(" (select HierarchyId from syNavigationNodes where  ParentId in ")
    '            .Append(" (select HierarchyId from synavigationnodes where HierarchyName=?)) ")
    '            .Append(" and ResourceId=?) ")
    '        End If
    '        .Append(" Order By HierarchyName ")
    '    End With



    '    'Add the EmployerContactId the parameter list
    '    db.AddParameter("@SDFID", SDFID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@ModuleID", ModuleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@ModuleDescrip", strModExp, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@SDFID", SDFID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@ModuleID", ModuleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@ModuleDescrip", strModExp, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    'View Existing Student
    '    db.AddParameter("@strExistStudRes", strExStudentRes, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
    '    If ModuleID.ToString = "PL" Then
    '        'View Existing Employer
    '        'In Placement we have Existing Students and Existing Employers so we need to do this for existing employer
    '        db.AddParameter("@SDFID", SDFID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@ModuleID", ModuleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@ModuleDescrip", strModExp, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@strExEmployerRes", strExEmployerRes, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
    '    End If

    '    '   return dataset
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function
    Public Function AddUDF(ByVal UDFInformations As UDFInfo, ByVal user As String) As String

        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            '    'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("Insert sySDF(SDFId,SDFDescrip,DTypeId,Len,Decimals,HelpText,StatusId,valTypeId,ModUser,ModDate,IsRequired) ")
                .Append(" Values(?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            ''Add Parameters To Query

            'SDFId
            db.AddParameter("@SDFId", UDFInformations.SDFId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'SDF Descrip
            db.AddParameter("@SDFDescrip", UDFInformations.SDFDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Data Type
            db.AddParameter("@DtypeId", UDFInformations.DtypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'Len
            db.AddParameter("@Len", UDFInformations.Len, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'Decimals
            db.AddParameter("@Decimals", UDFInformations.Decimals, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'Help Text
            db.AddParameter("@Comments", UDFInformations.Comments, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)

            'StatusId
            db.AddParameter("@StatusId", UDFInformations.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.AddParameter("@ValTypeId", UDFInformations.ValidationType, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)


            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'IsRequired
            db.AddParameter("@IsRequired", UDFInformations.IsRequired, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Dim sb2 As New StringBuilder
            'With sb2
            '    .Append(" insert sySDFModVis(SdfModVisId,SDFId,ModuleId,VisId,ModUser,ModDate) values(?,?,?,?,?,?) ")
            'End With
            'db.AddParameter("@sdfModVisId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'db.AddParameter("@sdfId", UDFInformations.SDFId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'db.AddParameter("@ModuleId", intModuleId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)



            Dim sb1 As New StringBuilder
            If UDFInformations.ValidationType = 1 Then
                With sb1
                    .Append("Insert sySDFRange(SdfRangeId,SdfId,MinVal,MaxVal,ModUser,ModDate) ")
                    .Append(" Values(?,?,?,?,?,?) ")
                End With
                db.AddParameter("@sdfRangeId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@sdfId", UDFInformations.SDFId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Minval", UDFInformations.MinValue, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@Maxval", UDFInformations.MaxValue, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                'Execute The Query
                db.RunParamSQLExecuteNoneQuery(sb1.ToString)
                db.ClearParameters()
                sb1.Remove(0, sb1.Length)

                With sb1
                    .Append("DELETE FROM sySDFValList WHERE SDFId = ? ")
                End With
                db.AddParameter("@SDFId", UDFInformations.SDFId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb1.ToString)
                db.ClearParameters()
                sb1.Remove(0, sb1.Length)
            ElseIf UDFInformations.ValidationType = 0 Then
                With sb1
                    .Append("DELETE FROM sySDFRange WHERE SDFId = ? ")
                End With
                db.AddParameter("@SDFId", UDFInformations.SDFId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb1.ToString)
                db.ClearParameters()
                sb1.Remove(0, sb1.Length)

                With sb1
                    .Append("DELETE FROM sySDFValList WHERE SDFId = ? ")
                End With
                db.AddParameter("@SDFId", UDFInformations.SDFId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb1.ToString)
                db.ClearParameters()
                sb1.Remove(0, sb1.Length)
            End If
            UDFInformations.IsInDB = True

            Return ""
            'Catch ex As System.Exception
            '    'Return an Error To Client
            '    Return -1
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function UpdateUDF(ByVal UDFInformations As UDFInfo, ByVal user As String) As String

        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            '    'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("update sySDF set SDFDescrip=?,DTypeId=?,Len=?,Decimals=?,HelpText=?,StatusId=?,ModUser=?,ModDate=?,valTypeId=?,IsRequired=? ")
                .Append(" where sdfid=? ")
            End With

            ''Add Parameters To Query

            'SDF Descrip
            db.AddParameter("@SDFDescrip", UDFInformations.SDFDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Data Type
            db.AddParameter("@DtypeId", UDFInformations.DtypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'Len
            db.AddParameter("@Len", UDFInformations.Len, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'Decimals
            db.AddParameter("@Decimals", UDFInformations.Decimals, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'Help Text
            db.AddParameter("@Comments", UDFInformations.Comments, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)

            'StatusId
            db.AddParameter("@StatusId", UDFInformations.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.AddParameter("@ValTypeId", UDFInformations.ValidationType, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.AddParameter("@IsRequired", UDFInformations.IsRequired, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            'SDFId
            db.AddParameter("@SDFId", UDFInformations.SDFId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)



            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()

            Dim sb1 As New StringBuilder
            If UDFInformations.ValidationType = 1 Then
                With sb1
                    .Append(" delete from sySDFRange where sdfId=? ")
                End With
                db.AddParameter("@sdfId", UDFInformations.SDFId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb1.ToString)
                db.ClearParameters()
                sb1.Remove(0, sb1.Length)
                With sb1
                    .Append("Insert sySDFRange(SdfRangeId,SdfId,MinVal,MaxVal,ModUser,ModDate) ")
                    .Append(" Values(?,?,?,?,?,?) ")
                End With
                db.AddParameter("@sdfRangeId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@sdfId", UDFInformations.SDFId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Minval", UDFInformations.MinValue, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@Maxval", UDFInformations.MaxValue, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb1.ToString)
            ElseIf UDFInformations.ValidationType = 0 Then
                With sb1
                    .Append("DELETE FROM sySDFRange WHERE SDFId = ? ")
                End With
                db.AddParameter("@SDFId", UDFInformations.SDFId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb1.ToString)
                db.ClearParameters()
                sb1.Remove(0, sb1.Length)

                With sb1
                    .Append("DELETE FROM sySDFValList WHERE SDFId = ? ")
                End With
                db.AddParameter("@SDFId", UDFInformations.SDFId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb1.ToString)
                db.ClearParameters()
                sb1.Remove(0, sb1.Length)
            End If

            UDFInformations.IsInDB = True

            Return ""
            'Catch ex As System.Exception
            '    'Return an Error To Client
            '    Return -1
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetAllUDFDatas(ByVal boolStatus As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append(" Select SD.SDFId,SD.SDFDescrip,ST.Status from sySDF SD,syStatuses ST ")
            .Append(" where SD.StatusId = ST.StatusId ")
            If boolStatus = "True" Then
                .Append(" and ST.Status = 'Active' ")
                .Append(" order by SD.SDFDescrip ")
            ElseIf boolStatus = "False" Then
                .Append(" and ST.Status = 'Inactive' ")
                .Append(" order by SD.SDFDescrip ")
            Else
                .Append(" order by ST.Status,SD.SDFDescrip ")
            End If
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function GetUDFDetails(ByVal UDFId As String) As UDFInfo
        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("SELECT   B.SDFId,B.SDFDescrip,B.DtypeId,B.Len,B.Decimals,B.HelpText,B.StatusId,B.ValTypeId,B.IsRequired, ")
            .Append(" (select Distinct MinVal from sySDFRange where sdfId=B.sdfId) as MinVal, ")
            .Append(" (select Distinct MaxVal from sySDFRange where sdfId=B.sdfId) as MaxVal, ")
            .Append(" (select Distinct ValList from sySDFValList where sdfId=B.sdfId) as ValueList, ")
            .Append(" (select Convert(bit,Count(PkId)) from syResourceSdf where sdfId=B.sdfId) as InUse, ")
            .Append(" (select Convert(bit, Count(PgPKID)) from sySDFModuleValue smv where SDFID = B.SDFID) as HasData, ")
            .Append(" (select StatusCode from syStatuses where StatusId=B.StatusId) as StatusCode,B.ModDate  ")
            .Append(" from sySDF B ")
            .Append(" where B.SDFId = ? ")
        End With

        'Add the PlacementId the parameter list
        db.AddParameter("@SdfId", UDFId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim info As New UDFInfo
        While dr.Read()

            'set properties with data from DataReader
            With info
                .IsInDB = True
                .SDFId = CType(dr("SDFId"), Guid).ToString
                .SDFDescrip = dr("SDFDescrip")
                .DtypeId = dr("DtypeId")
                .Len = dr("Len")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .IsRequired = CType(dr("IsRequired"), Boolean)
                .InUse = CType(dr("inUse"), Boolean)
                .HasData = CType(dr("hasData"), Boolean)
                .StatusCode = dr("StatusCode")

                If Not (dr("Decimals") Is System.DBNull.Value) Then .Decimals = dr("Decimals") Else .Decimals = 0
                If Not (dr("HelpText") Is System.DBNull.Value) Then .Comments = dr("HelpText") Else .Comments = ""

                .ValidationType = CInt(dr("ValTypeId"))

                If Not (dr("MinVal") Is System.DBNull.Value) Then .MinValue = dr("MinVal") Else .MinValue = ""
                If Not (dr("MaxVal") Is System.DBNull.Value) Then .MaxValue = dr("MaxVal") Else .MaxValue = ""

                If Not (dr("ValueList") Is System.DBNull.Value) Then .ValueList = dr("ValueList") Else .ValueList = ""

            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return info
    End Function
    Public Function updateListValue(ByVal UserName As String, ByVal selectedList() As String, ByVal sdfId As String) As String

        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


        '   First we have to delete all existing selections
        '   build the query

        Dim sb As New StringBuilder
        With sb
            .Append("DELETE FROM sySDFValList WHERE SDFId = ? ")
        End With
        '   delete all selected items
        db.AddParameter("@SDFId", sdfId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        '   Insert one record per each Item in the Selected Group
        Dim i As Integer
        Dim s As String
        Dim strListValue As String
        For i = 0 To selectedList.Length - 1
            s &= DirectCast(selectedList.GetValue(i), String)
            s &= ","
        Next
        strListValue = Mid(s, 1, InStrRev(s, ",", -1) - 1)

        'Do an Update
        Try
            '    'Build The Query
            With sb
                .Append(" insert into sySDFValList(sdfListId,SDFId,ValList,ModUser,ModDate) ")
                .Append(" values(?,?,?,?,?) ")
            End With

            ''Add Parameters To Query

            'SDFId
            db.AddParameter("@SDFListId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'SDF Descrip
            db.AddParameter("@SDFId", sdfId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Data Type
            db.AddParameter("@ValList", strListValue, DataAccess.OleDbDataType.OleDbString, 5000, ParameterDirection.Input)

            'ModUser
            db.AddParameter("@ModUser", UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            With sb
                .Append("DELETE FROM sySDFRange WHERE SDFId = ? ")
            End With
            db.AddParameter("@SDFId", sdfId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            Return ""
            'Catch ex As System.Exception
            '    'Return an Error To Client
            '    Return -1
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function DeleteSDF(ByVal SDFID As String, ByVal ModDate As DateTime) As String
        'Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        'Try
        'Build The Query
        Dim sb As New StringBuilder
        Try
            With sb
                .Append("DELETE FROM sySDFRange WHERE SDFId = ? ")
            End With
            db.AddParameter("@SDFId", SDFID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            With sb
                .Append("DELETE FROM sySDFValList WHERE SDFId = ? ")
            End With
            db.AddParameter("@SDFId", SDFID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            With sb
                .Append("DELETE FROM syResourceSdf WHERE SDFId = ? ")
            End With
            db.AddParameter("@SDFId", SDFID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            With sb
                .Append("DELETE FROM sySDF WHERE SDFId = ? ")
            End With
            db.AddParameter("@SDFId", SDFID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function


    'Public Function GetAvailableUDFPagesByResource(ByVal SDFID As String, ByVal intModuleResId As Integer, Optional ByVal intEntityId As Integer = 0) As DataSet
    '    '   connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder
    '    ''  Dim strSQL As String
    '    Dim ds As New DataSet
    '    'Dim strModExp As String
    '    'Dim strExStudentRes As Integer
    '    'Dim strExEmployerRes As Integer
    '    Dim intEntityResId As String
    '    Dim entityIDS As String
    '    Dim iCounter As Integer

    '    'We need to get the ResourceId for the entity associated with this page. This code assumes that UDFs are only setup
    '    'on pages dealing with entitities such as student, lead, employee and employers. For any given module we can use
    '    'the metadata to get the relevant entities. Note that a module could have more than one entity - eg. Placement that
    '    'that has student and employers.
    '    'Dim intEntityResId As Integer = 394  'ResourceId for Student Entity
    '    'ds = TM.ModulesDB.GetModuleEntities(intModuleResId)

    '    'For Each dr As DataRow In ds.Tables(0).Rows
    '    '    iCounter += 1
    '    '    If iCounter = ds.Tables(0).Rows.Count Then
    '    '        entityIDS &= dr("EntityID").ToString
    '    '    Else
    '    '        entityIDS &= dr("EntityID").ToString & ","
    '    '    End If
    '    'Next

    '    'intEntityResId = entityIDS

    '    With sb
    '        .Append(" select Distinct t1.RelatedResourceId as ResourceId,")
    '        .Append(" t2.Resource as Resource ")
    '        .Append("  from syAdvantageResourceRelations t1,syResources t2 ")
    '        '.Append(" where t1.ResourceId IN (" & entityIDS & ") And ")
    '        .Append(" where t1.ResourceId=? And ")
    '        .Append(" t1.RelatedResourceId = t2.ResourceId And t2.AllowSchlReqFlds=1 and t2.ResourceTypeId <> 1 ")
    '        '.Append(" and t1.RelatedResourceId not in (select ResourceId from syResourceSDF where SDFId=? and ModuleId=?) ")
    '        .Append(" and t1.RelatedResourceId not in (select ResourceId from syResourceSDF where SDFId=?) ")
    '        .Append(" union ")
    '        .Append("   select Distinct t1.ResourceId,t1.Resource as resource from ")
    '        .Append("   syResources t1,syNavigationNodes t2,syResources t3,syNavigationNodes t4 ")
    '        .Append("   where ")
    '        .Append("       t1.ResourceId = t2.ResourceId and ")
    '        .Append("       t2.ResourceId = t3.ResourceId and ")
    '        .Append("       t3.ResourceTypeId = 4 And t1.AllowSchlReqFlds=1 And ")
    '        .Append("       t2.ParentId = t4.HierarchyId  ")
    '        .Append(" And t4.ResourceId=?  ")
    '        '.Append(" and t1.ResourceId not in (select ResourceId from syResourceSDF where SDFId=? and ModuleId=?) ")
    '        .Append(" and t1.ResourceId not in (select ResourceId from syResourceSDF where SDFId=?) ")
    '        'The Bottom Query returns all maintenance pages that belongs to a module
    '        'but resides under a submenu for Ex:Placement -- Jobs ---- Job Titles
    '        .Append("       union ")
    '        .Append("   select A.ResourceId,B.Resource as resource from syNavigationNodes A,syResources B where ")
    '        .Append("   A.ResourceId = B.ResourceId and B.ResourceTypeId = 4 and ")
    '        .Append("   ParentId in ")
    '        .Append("       (       ")
    '        .Append("           Select Distinct    ")
    '        .Append("                       t2.HierarchyId      ")
    '        .Append("           from                    ")
    '        .Append("               syResources t1,syNavigationNodes t2 ")
    '        .Append("           where ")
    '        .Append("               t1.ResourceId = t2.ResourceId And t1.AllowSchlReqFlds=1 ")
    '        .Append("               and t1.ResourceTypeId in (2) ")
    '        '.Append("               and t2.ParentId in (select HierarchyId from syNavigationNodes where ResourceId IN (" & entityIDS & ") ) ")
    '        .Append("               and t2.ParentId in (select HierarchyId from syNavigationNodes where ResourceId=?) ")
    '        .Append(" ) ")
    '        '.Append(" and A.ResourceId not in (select ResourceId from syResourceSDF where SDFId=? and ModuleId=?) ")
    '        .Append(" and A.ResourceId not in (select ResourceId from syResourceSDF where SDFId=?) ")
    '        .Append(" order by resource ")
    '    End With
    '    db.AddParameter("@intEntityResId", intEntityId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
    '    db.AddParameter("@SDFID", SDFID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    'db.AddParameter("@ModuleResourceId", intModuleResId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

    '    db.AddParameter("@intEntityResId", intEntityId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
    '    db.AddParameter("@SDFID", SDFID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    ' db.AddParameter("@ModuleResourceId", intModuleResId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

    '    db.AddParameter("@intEntityResId", intEntityId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
    '    db.AddParameter("@SDFID", SDFID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    'db.AddParameter("@ModuleResourceId", intModuleResId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

    '    'return dataset
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function

    Public Function GetEntityForResource(ByVal pageId As Integer) As Integer
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT ResourceId ")
            .Append("FROM syAdvantageResourceRelations ")
            .Append("WHERE RelatedResourceId = ? ")
        End With

        db.AddParameter("@pageid", pageId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        Return CInt(db.RunParamSQLScalar(sb.ToString))

    End Function

    Public Function GetModulesForEntity(ByVal entityId As Integer) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        ' build the sql query
        Dim sb As New StringBuilder
        sb.Append("SELECT " & vbCrLf)
        sb.Append("		   E.ResourceID AS EntityID, e.Resource AS EntityName , R.RelatedResourceId AS ResourceID, m.resource AS [Resource]		" & vbCrLf)
        sb.Append("FROM " + vbCrLf)
        sb.Append("        syAdvantageResourceRelations R INNER JOIN syResources E ON R.ResourceId = E.ResourceId " + vbCrLf)
        sb.Append("        INNER JOIN syResources M ON R.RelatedResourceId = M.ResourceId " + vbCrLf)
        sb.Append("WHERE " + vbCrLf)
        sb.Append("        E.ResourceTypeId = 8 AND M.ResourceTypeId = 1 AND E.ResourceId = ? " + vbCrLf)
        db.AddParameter("Id", entityId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
End Class


