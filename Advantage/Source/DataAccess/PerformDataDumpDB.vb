Public Class PerformDataDumpDB
#Region "Public Methods"

    Public Function GetPerformDataDump(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim sbUnion As New System.Text.StringBuilder
        Dim sbTempTable As New System.Text.StringBuilder
        Dim sbTempTableInsert As New System.Text.StringBuilder


        Dim CampusId As String = ""
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhereAS As String = ""
        Dim strWherePS As String = ""
        Dim strConditon As String = ""
        Dim strConditonUnion As String = ""
        Dim strOrderBy As String = ""
        Dim refDateStart As String = ""
        Dim refDateEnd As String = ""
        Dim RunDateTime As String = ""
        Dim FirstWeekEnded As String = ""
        Dim LastWeekEnded As String = ""
        Dim WkCount As Integer = 1
        Dim WkCountInner As Integer = 1
        Dim UserName As String = ""
        Dim MaxCountNew As Integer = 0
        Dim MaxCountOld As Integer = 0
        Dim MaxRecordTable As Integer = 1
        Dim strColumnNames As String = ""
        CampusId = paramInfo.CampusId

        If paramInfo.FilterList <> "" Then
            Dim strWC As String = paramInfo.FilterList
            strWhereAS &= " AND " & paramInfo.FilterList
            Dim lst As String
            While strWC.Length > 1
                Dim strWCA As String
                If strWC.IndexOf("AND") <> -1 Then
                    strWCA = strWC.Substring(0, strWC.IndexOf("AND"))
                Else
                    strWCA = strWC
                End If
                If strWCA.IndexOf("saFundSources") = -1 Then
                    strWherePS &= " AND " & strWCA
                End If
                If strWC.IndexOf("AND") <> -1 Then
                    strWC = strWC.Remove(0, strWC.IndexOf("AND") + 3)
                Else
                    strWC = ""
                End If

            End While

        End If
        'strOrderBy = " ORDER BY arStudent.LastName , arStudent.FirstName, arStudent.SSN, arStuEnrollments.StartDate, syStatusCodes.StatusCodeDescrip, arStuEnrollments.LDA, arPrograms.ProgDescrip, FundSource "
        strOrderBy = "  "
        If paramInfo.FilterOther <> "" Then
            'Parse FilterOther in order to get Reference Start And End Date

            Dim tempDate() As String = paramInfo.FilterOther.Split(" ")
            If tempDate.GetLength(0) > 0 Then
                refDateStart = tempDate(2).Replace("'", "").Trim
                refDateEnd = tempDate(4).Replace("'", "").Trim
            End If
        End If

        Dim dtRefDateStart As Date = Date.Parse(refDateStart)
        Dim dtRefDateEnd As Date = Date.Parse(refDateEnd)

        If dtRefDateStart.DayOfWeek() = DayOfWeek.Sunday Then
            dtRefDateStart = dtRefDateStart.AddDays(0)
        ElseIf dtRefDateStart.DayOfWeek() = DayOfWeek.Monday Then
            dtRefDateStart = dtRefDateStart.AddDays(-1)
        ElseIf dtRefDateStart.DayOfWeek() = DayOfWeek.Tuesday Then
            dtRefDateStart = dtRefDateStart.AddDays(-2)
        ElseIf dtRefDateStart.DayOfWeek() = DayOfWeek.Wednesday Then
            dtRefDateStart = dtRefDateStart.AddDays(-3)
        ElseIf dtRefDateStart.DayOfWeek() = DayOfWeek.Thursday Then
            dtRefDateStart = dtRefDateStart.AddDays(-4)
        ElseIf dtRefDateStart.DayOfWeek() = DayOfWeek.Friday Then
            dtRefDateStart = dtRefDateStart.AddDays(-5)
        Else
            dtRefDateStart = dtRefDateStart.AddDays(-6)
        End If

        If dtRefDateEnd.DayOfWeek() = DayOfWeek.Sunday Then
            dtRefDateEnd = dtRefDateEnd.AddDays(6)
        ElseIf dtRefDateEnd.DayOfWeek() = DayOfWeek.Monday Then
            dtRefDateEnd = dtRefDateEnd.AddDays(5)
        ElseIf dtRefDateEnd.DayOfWeek() = DayOfWeek.Tuesday Then
            dtRefDateEnd = dtRefDateEnd.AddDays(4)
        ElseIf dtRefDateEnd.DayOfWeek() = DayOfWeek.Wednesday Then
            dtRefDateEnd = dtRefDateEnd.AddDays(3)
        ElseIf dtRefDateEnd.DayOfWeek() = DayOfWeek.Thursday Then
            dtRefDateEnd = dtRefDateEnd.AddDays(2)
        ElseIf dtRefDateEnd.DayOfWeek() = DayOfWeek.Friday Then
            dtRefDateEnd = dtRefDateEnd.AddDays(1)
        Else
            dtRefDateEnd = dtRefDateEnd.AddDays(0)
        End If

        Dim NumberOfWeek As Integer = 0
        Dim MaxYear As Integer = 1
        Dim CurrentYear As Integer = 1

        NumberOfWeek = DateDiff(DateInterval.Day, dtRefDateStart, dtRefDateEnd) / 7

        If NumberOfWeek > 265 Then
            NumberOfWeek = 265
        End If

        Dim CreateQuery As Boolean = False
        Dim CompleteQuery As Boolean = False

        Try

            UserName = IIf(Not HttpContext.Current.Session("UserName") Is Nothing, HttpContext.Current.Session("UserName"), "")

            '' New Code Added By VIjay Ramteke on April 23, 2009
            With sbTempTableInsert
                .Append("IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tempASPS_" + UserName + "]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1) ")
                .Append("DROP TABLE [tempASPS_" + UserName + "] ")
                .Append(" SELECT * INTO [tempASPS_" + UserName + "] FROM( ")
                'Code Added By Vijay Ramteke on May 13,2009
                If strWhereAS.IndexOf("saFundSources") = -1 Then
                    ''.Append(" SELECT DISTINCT NEWID() AS ID, arStudent.LastName, arStudent.FirstName, arStudent.SSN, CONVERT(VARCHAR(10),arStuEnrollments.StartDate,101) AS StartDate, syStatusCodes.StatusCodeDescrip AS Status , CONVERT(VARCHAR(10),arStuEnrollments.LDA,101) AS LDA, arPrograms.ProgDescrip AS Program, 'Payment Plan' AS FundSource, adLeadGroups.Descrip AS LeadGroup, arStuEnrollments.StuEnrollId, '{00000000-0000-0000-0000-000000000000}' AS AwardScheduleId, faStuPaymentPlanSchedule.PayPlanScheduleId, faStuPaymentPlanSchedule.ExpectedDate, faStuPaymentPlanSchedule.Amount AS Amount, saPmtDisbRel.Amount AS DisbAmount, saTransactions.Voided ")
                    .Append(" SELECT DISTINCT NEWID() AS ID, arStudent.LastName, arStudent.FirstName, arStudent.SSN, CONVERT(VARCHAR(10),arStuEnrollments.StartDate,101) AS StartDate, syStatusCodes.StatusCodeDescrip AS Status , CONVERT(VARCHAR(10),arStuEnrollments.LDA,101) AS LDA, arPrograms.ProgDescrip AS Program, 'Payment Plan' AS FundSource, adLeadGroups.Descrip AS LeadGroup, arStuEnrollments.StuEnrollId, '{00000000-0000-0000-0000-000000000000}' AS AwardScheduleId, faStuPaymentPlanSchedule.PayPlanScheduleId, faStuPaymentPlanSchedule.ExpectedDate, faStuPaymentPlanSchedule.Amount AS Amount, ")
                    .Append("(select sum(saPmtDisbRel.Amount) FROM  saPmtDisbRel RIGHT OUTER JOIN saTransactions on saTransactions.TransactionId = saPmtDisbRel.TransactionId where [faStuPaymentPlanSchedule].[PayPlanScheduleId] = saPmtDisbRel.[PayPlanScheduleId] and saTransactions.Voided=0) AS DisbAmount ")
                    .Append(" FROM ")
                    .Append(" saTransactions INNER JOIN saPmtDisbRel ON saTransactions.TransactionId = saPmtDisbRel.TransactionId RIGHT OUTER JOIN ")
                    .Append(" syCmpGrpCmps INNER JOIN syCampuses ON syCmpGrpCmps.CampusId = syCampuses.CampusId AND syCmpGrpCmps.CampusId = syCampuses.CampusId INNER JOIN ")
                    .Append(" syCampGrps ON syCmpGrpCmps.CampGrpId = syCampGrps.CampGrpId INNER JOIN arPrograms INNER JOIN arStudent INNER JOIN ")
                    .Append(" arStuEnrollments ON arStudent.StudentId = arStuEnrollments.StudentId INNER JOIN syStatusCodes ON arStuEnrollments.StatusCodeId = syStatusCodes.StatusCodeId INNER JOIN ")
                    .Append(" arPrgVersions ON arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId ON arPrograms.ProgId = arPrgVersions.ProgId ON ")
                    .Append(" syCmpGrpCmps.CampusId = arStuEnrollments.CampusId INNER JOIN adLeadByLeadGroups ON arStuEnrollments.StuEnrollId = adLeadByLeadGroups.StuEnrollId INNER JOIN ")
                    .Append(" adLeadGroups ON adLeadByLeadGroups.LeadGrpId = adLeadGroups.LeadGrpId AND adLeadByLeadGroups.LeadGrpId = adLeadGroups.LeadGrpId INNER JOIN ")
                    .Append(" faStudentPaymentPlans ON arStuEnrollments.StuEnrollId = faStudentPaymentPlans.StuEnrollId INNER JOIN faStuPaymentPlanSchedule ON faStudentPaymentPlans.PaymentPlanId = faStuPaymentPlanSchedule.PaymentPlanId ON saPmtDisbRel.PayPlanScheduleId = faStuPaymentPlanSchedule.PayPlanScheduleId ")
                    .Append(" WHERE ")
                    .Append("adLeadGroups.[UseForScheduling]=1 and (arStuEnrollments.CampusId = '" + CampusId + "') AND (faStuPaymentPlanSchedule.ExpectedDate>='" + dtRefDateStart.ToString("MM/dd/yyyy") + "' AND faStuPaymentPlanSchedule.ExpectedDate<='" + dtRefDateEnd.ToString("MM/dd/yyyy") + "') ")
                    .Append(" " + strWherePS + " ")
                    .Append(" UNION ")
                End If
                ''.Append(" SELECT DISTINCT NEWID() AS ID, arStudent.LastName, arStudent.FirstName, arStudent.SSN, CONVERT(VARCHAR(10),arStuEnrollments.StartDate,101) AS StartDate, syStatusCodes.StatusCodeDescrip AS Status, CONVERT(VARCHAR(10),arStuEnrollments.LDA,101) AS LDA, arPrograms.ProgDescrip AS Program, saFundSources.FundSourceDescrip AS FundSource, adLeadGroups.Descrip AS LeadGroup, arStuEnrollments.StuEnrollId, faStudentAwardSchedule.AwardScheduleId, '{00000000-0000-0000-0000-000000000000}' AS PayPlanScheduleId, faStudentAwardSchedule.ExpectedDate, faStudentAwardSchedule.Amount AS Amount, saPmtDisbRel.Amount AS DisbAmount, saTransactions.Voided ")
                .Append(" SELECT DISTINCT NEWID() AS ID, arStudent.LastName, arStudent.FirstName, arStudent.SSN, CONVERT(VARCHAR(10),arStuEnrollments.StartDate,101) AS StartDate, syStatusCodes.StatusCodeDescrip AS Status, CONVERT(VARCHAR(10),arStuEnrollments.LDA,101) AS LDA, arPrograms.ProgDescrip AS Program, saFundSources.FundSourceDescrip AS FundSource, adLeadGroups.Descrip AS LeadGroup, arStuEnrollments.StuEnrollId, faStudentAwardSchedule.AwardScheduleId, '{00000000-0000-0000-0000-000000000000}' AS PayPlanScheduleId, faStudentAwardSchedule.ExpectedDate, faStudentAwardSchedule.Amount AS Amount, ")
                .Append("(select sum(saPmtDisbRel.Amount) FROM  saPmtDisbRel RIGHT OUTER JOIN saTransactions on saTransactions.TransactionId = saPmtDisbRel.TransactionId where faStudentAwardSchedule.AwardScheduleId = saPmtDisbRel.AwardScheduleId and saTransactions.Voided=0) AS DisbAmount ")
                .Append(" FROM ")
                ''.Append(" saTransactions RIGHT OUTER JOIN saPmtDisbRel RIGHT OUTER JOIN syCmpGrpCmps INNER JOIN syCampuses ON syCmpGrpCmps.CampusId = syCampuses.CampusId AND ")
                .Append(" syCmpGrpCmps INNER JOIN syCampuses ON syCmpGrpCmps.CampusId = syCampuses.CampusId AND ")
                .Append(" syCmpGrpCmps.CampusId = syCampuses.CampusId INNER JOIN syCampGrps ON syCmpGrpCmps.CampGrpId = syCampGrps.CampGrpId INNER JOIN arPrograms INNER JOIN ")
                .Append(" faStudentAwards INNER JOIN arStudent INNER JOIN arStuEnrollments ON arStudent.StudentId = arStuEnrollments.StudentId ON ")
                .Append(" faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId INNER JOIN syStatusCodes ON arStuEnrollments.StatusCodeId = syStatusCodes.StatusCodeId INNER JOIN ")
                .Append(" arPrgVersions ON arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId ON arPrograms.ProgId = arPrgVersions.ProgId INNER JOIN ")
                .Append(" saFundSources ON faStudentAwards.AwardTypeId = saFundSources.FundSourceId ON syCmpGrpCmps.CampusId = arStuEnrollments.CampusId INNER JOIN adLeadByLeadGroups ON arStuEnrollments.StuEnrollId = adLeadByLeadGroups.StuEnrollId INNER JOIN ")
                .Append(" adLeadGroups ON adLeadByLeadGroups.LeadGrpId = adLeadGroups.LeadGrpId AND adLeadByLeadGroups.LeadGrpId = adLeadGroups.LeadGrpId INNER JOIN ")
                ''.Append(" faStudentAwardSchedule ON faStudentAwards.StudentAwardId = faStudentAwardSchedule.StudentAwardId ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId ON saTransactions.TransactionId = saPmtDisbRel.TransactionId ")
                .Append(" faStudentAwardSchedule ON faStudentAwards.StudentAwardId = faStudentAwardSchedule.StudentAwardId  ")
                .Append(" WHERE ")
                .Append(" adLeadGroups.[UseForScheduling]=1 and (arStuEnrollments.CampusId = '" + CampusId + "') AND (faStudentAwardSchedule.ExpectedDate>='" + dtRefDateStart.ToString("MM/dd/yyyy") + "' AND faStudentAwardSchedule.ExpectedDate<='" + dtRefDateEnd.ToString("MM/dd/yyyy") + "') ")
                .Append(" " + strWhereAS + " ")
                .Append(" ) AS tempTable; ")

                ''.Append(" UPDATE [tempASPS_" + UserName + "] SET Amount=ISNULL(Amount,0)-ISNULL(DisbAmount,0) WHERE ID=ID AND Voided=0 ;")
                .Append(" UPDATE [tempASPS_" + UserName + "] SET Amount=ISNULL(Amount,0)-ISNULL(DisbAmount,0) WHERE ID=ID;")
                .Append(" DELETE FROM [tempASPS_" + UserName + "] WHERE Amount=0 ")

            End With
            db.RunParamSQLExecuteNoneQuery(sbTempTableInsert.ToString)
            '' New Code Added By VIjay Ramteke on April 23, 2009

            While (dtRefDateStart <= dtRefDateEnd And WkCount <= NumberOfWeek And MaxYear <= 5)

                If CreateQuery = False Then

                    sbTempTable.Append("IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tbl_TempFAFSAStudentData_" + UserName + "_" + CurrentYear.ToString + "]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1) ")
                    sbTempTable.Append("DROP TABLE [tbl_TempFAFSAStudentData_" + UserName + "_" + CurrentYear.ToString + "] ")
                    sbTempTable.Append("CREATE TABLE [tbl_TempFAFSAStudentData_" + UserName + "_" + CurrentYear.ToString + "] ( ")
                    sbTempTable.Append("[AID] [int] IDENTITY (1, 1) NOT NULL ,")
                    sbTempTable.Append("[Last Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,")
                    sbTempTable.Append("[First Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,")
                    sbTempTable.Append("[SSN] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,")
                    sbTempTable.Append("[Start Date] [datetime] NULL ,")
                    sbTempTable.Append("[Status] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,")
                    sbTempTable.Append("[LDA] [datetime] NULL ,")
                    sbTempTable.Append("[Program] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,")
                    sbTempTable.Append("[Fund Source/Payment Plan] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,")
                    sbTempTable.Append("[Student Group] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,")
                    sb.Append("INSERT INTO tbl_TempFAFSAStudentData_" + UserName + "_" + CurrentYear.ToString + " SELECT M.LastName, M.FirstName, M.SSN, M.StartDate, M.Status, M.LDA, M.Program, M.FundSource, M.LeadGroup, ")

                    CreateQuery = True
                End If

                Dim ExpectedDateStart As String = ""
                Dim ExpectedDateEnd As String = ""
                ExpectedDateStart = dtRefDateStart.ToString("MM/dd/yyyy")
                ExpectedDateEnd = dtRefDateStart.AddDays(6).ToString("MM/dd/yyyy")

                If dtRefDateStart.AddDays(7) <= dtRefDateEnd And WkCountInner < 52 Then
                    sb.Append(" ISNULL((SELECT SUM(t.Amount) FROM tempASPS_" + UserName + " t WHERE t.StuEnrollId=M.StuEnrollId AND t.FundSource=M.FundSource AND t.ExpectedDate>='" + ExpectedDateStart + "' AND t.ExpectedDate<='" + ExpectedDateEnd + "'),0) AS '" + ExpectedDateEnd + "', ")
                    If WkCount = 1 Then
                        sbTempTable.Append("[" + ExpectedDateEnd + "] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,")
                        strConditon += " ISNULL((SELECT SUM(t.Amount) FROM tempASPS_" + UserName + " t WHERE t.StuEnrollId=M.StuEnrollId AND t.FundSource=M.FundSource AND t.ExpectedDate>='" + ExpectedDateStart + "' AND t.ExpectedDate<='" + ExpectedDateEnd + "'),0) > 0 OR "
                        strColumnNames += " [" + ExpectedDateEnd + "], "
                    Else
                        sbTempTable.Append("[" + ExpectedDateEnd + "] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,")
                        strConditon += " ISNULL((SELECT SUM(t.Amount) FROM tempASPS_" + UserName + " t WHERE t.StuEnrollId=M.StuEnrollId AND t.FundSource=M.FundSource AND t.ExpectedDate>='" + ExpectedDateStart + "' AND t.ExpectedDate<='" + ExpectedDateEnd + "'),0) > 0 OR "
                        strColumnNames += " [" + ExpectedDateEnd + "], "
                    End If
                Else
                    sbTempTable.Append("[" + ExpectedDateEnd + "] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ")
                    sb.Append(" ISNULL((SELECT SUM(t.Amount) FROM tempASPS_" + UserName + " t WHERE t.StuEnrollId=M.StuEnrollId AND t.FundSource=M.FundSource AND t.ExpectedDate>='" + ExpectedDateStart + "' AND t.ExpectedDate<='" + ExpectedDateEnd + "'),0) AS '" + ExpectedDateEnd + "' ")
                    strConditon += " ISNULL((SELECT SUM(t.Amount) FROM tempASPS_" + UserName + " t WHERE t.StuEnrollId=M.StuEnrollId AND t.FundSource=M.FundSource AND t.ExpectedDate>='" + ExpectedDateStart + "' AND t.ExpectedDate<='" + ExpectedDateEnd + "''),0) > 0  "
                    strColumnNames += " [" + ExpectedDateEnd + "], "
                    CompleteQuery = True
                End If
                dtRefDateStart = dtRefDateStart.AddDays(7)
                WkCount += 1
                WkCountInner += 1
                If CompleteQuery = True Then
                    sb.Append(" FROM tempASPS_" + UserName + " M ")
                    sbTempTable.Append(") ON [PRIMARY]")
                End If

                If CompleteQuery = True Then

                    CompleteQuery = False
                    db.RunParamSQLExecuteNoneQuery(sbTempTable.ToString)
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)

                    sbTempTable = sbTempTable.Remove(0, sbTempTable.Length)
                    sb = sb.Remove(0, sb.Length)
                    sbUnion = sbUnion.Remove(0, sbUnion.Length)

                    sb.Append("SELECT COUNT(*) FROM [tbl_TempFAFSAStudentData_" + UserName + "_" + CurrentYear.ToString + "]")

                    MaxCountNew = DirectCast(db.RunSQLScalar(sb.ToString), Integer)
                    If MaxCountNew > MaxCountOld Then
                        MaxCountOld = MaxCountNew
                        MaxRecordTable = CurrentYear
                    End If

                    sb = sb.Remove(0, sb.Length)
                    WkCountInner = 1
                    MaxYear += 1
                    CreateQuery = False
                    strConditon = ""
                    strConditonUnion = ""
                    CurrentYear += 1
                End If

            End While

            sb = sb.Remove(0, sb.Length)
            Dim i As Integer = 1
            Dim strSQLSelect As String = ""
            Dim strWhere As String = ""
            Dim strSQLDelete As String = ""
            Dim strOrder As String = ""
            strSQLSelect += " Select DISTINCT tbl_TempFAFSAStudentData_" + UserName + "_" + MaxRecordTable.ToString + ".[Last Name], tbl_TempFAFSAStudentData_" + UserName + "_" + MaxRecordTable.ToString + ".[First Name],tbl_TempFAFSAStudentData_" + UserName + "_" + MaxRecordTable.ToString + ".[SSN],CONVERT(VARCHAR(10),tbl_TempFAFSAStudentData_" + UserName + "_" + MaxRecordTable.ToString + ".[Start Date],101) AS [Start Date],tbl_TempFAFSAStudentData_" + UserName + "_" + MaxRecordTable.ToString + ".[Status],CONVERT(VARCHAR(10),tbl_TempFAFSAStudentData_" + UserName + "_" + MaxRecordTable.ToString + ".[LDA],101) AS LDA,tbl_TempFAFSAStudentData_" + UserName + "_" + MaxRecordTable.ToString + ".[Program],tbl_TempFAFSAStudentData_" + UserName + "_" + MaxRecordTable.ToString + ".[Fund Source/Payment Plan],tbl_TempFAFSAStudentData_" + UserName + "_" + MaxRecordTable.ToString + ".[Student Group],"
            strSQLSelect += strColumnNames.Remove(strColumnNames.LastIndexOf(","), 1)
            strSQLSelect += " From tbl_TempFAFSAStudentData_" + UserName + "_" + MaxRecordTable.ToString + " "

            For i = 1 To CurrentYear - 1
                If MaxRecordTable <> i Then
                    'strSQLSelect += " LEFT OUTER JOIN tbl_TempFAFSAStudentData_" + UserName + "_" + i.ToString + " on tbl_TempFAFSAStudentData_" + UserName + "_" + MaxRecordTable.ToString + ".SSN=tbl_TempFAFSAStudentData_" + UserName + "_" + i.ToString + ".SSN "
                    ''Modified by Saraswathi lakshmanan on Sept 09 2009
                    ''To fix issues related to 17400, 17401, 17403
                    ''The left Outer Join was performed only on the SSN fields, they should be performed based on the SSn and the FundSource.
                    ''The left outer Join is removed and Normal Jion of these tables are performed with a where condition of SSN and FundSource
                    strSQLSelect += " , tbl_TempFAFSAStudentData_" + UserName + "_" + i.ToString
                    strWhere += "and tbl_TempFAFSAStudentData_" + UserName + "_" + MaxRecordTable.ToString + ".SSN=tbl_TempFAFSAStudentData_" + UserName + "_" + i.ToString + ".SSN  and tbl_TempFAFSAStudentData_" + UserName + "_" + MaxRecordTable.ToString + ".[Fund Source/Payment Plan]=tbl_TempFAFSAStudentData_" + UserName + "_" + i.ToString + ".[Fund Source/Payment Plan] "
                End If
                strSQLDelete += "DROP TABLE tbl_TempFAFSAStudentData_" + UserName + "_" + i.ToString + ";"
            Next
            strOrder += " Order By tbl_TempFAFSAStudentData_" + UserName + "_" + MaxRecordTable.ToString + ".[Fund Source/Payment Plan] "

            If strWhere <> "" Then
                strSQLSelect = strSQLSelect + " Where " + strWhere.Remove(0, 3)
            End If

            ' Execute The Query To Fill Dataset with SQL Created
            ds = db.RunSQLDataSet(strSQLSelect + strOrder)
            strSQLDelete += " DROP TABLE tempASPS_" + UserName + ";"
            db.RunSQLDataSet(strSQLDelete)

            '' New Code By Vijay Ramteke on 03/04/2009

            Dim dtMain As New DataTable
            Dim dtTemp As New DataTable
            dtTemp = ds.Tables(0)
            dtMain = ds.Tables(0).Clone()
            dtMain.Clear()

            Dim strFundSource As String = ""
            Dim strFundSourceNext As String = ""
            Dim drFST As DataRow
            Dim drFSTA As DataRow = dtMain.NewRow()
            Dim ColumnCount As Integer = 1
            Dim RowCount As Integer = 0

            For Each dr As DataRow In dtTemp.Rows
                If strFundSource = "" Then
                    drFST = dtMain.NewRow()
                    drFST = dtMain.NewRow()
                End If
                If strFundSource <> dr("Fund Source/Payment Plan").ToString Then
                    strFundSource = dr("Fund Source/Payment Plan").ToString
                    If RowCount < dtTemp.Rows.Count - 1 Then
                        strFundSourceNext = dtTemp.Rows(RowCount + 1)("Fund Source/Payment Plan").ToString
                    Else
                        strFundSourceNext = ""
                    End If

                    For Each dc As DataColumn In dtTemp.Columns
                        If ColumnCount > 9 Then
                            Dim strColumnName As String = dc.ColumnName.ToString
                            Dim dbTotCurrentRow As Double = 0
                            Dim dbTotal As Double = 0
                            Dim dbAllTotal As Double = 0
                            Dim str As String = dc.DataType.ToString
                            If str = "System.String" Then
                                Try
                                    dbTotCurrentRow = Convert.ToDouble(dr(strColumnName).ToString)
                                Catch ex As Exception
                                    dbTotCurrentRow = 0
                                End Try

                                Try
                                    dbTotal = Convert.ToDouble(drFST(strColumnName).ToString)
                                Catch ex As Exception
                                    dbTotal = 0
                                End Try

                                Try
                                    dbAllTotal = Convert.ToDouble(drFSTA(strColumnName).ToString)
                                Catch ex As Exception
                                    dbAllTotal = 0
                                End Try

                                dbTotal = dbTotal + dbTotCurrentRow
                                dbAllTotal = dbAllTotal + dbTotCurrentRow
                                drFST(strColumnName) = dbTotal.ToString
                                drFSTA(strColumnName) = dbAllTotal.ToString
                            End If
                        Else
                            ColumnCount += 1
                        End If
                    Next

                Else
                    If RowCount < dtTemp.Rows.Count - 1 Then
                        strFundSourceNext = dtTemp.Rows(RowCount + 1)("Fund Source/Payment Plan").ToString
                    Else
                        strFundSourceNext = ""
                    End If
                    For Each dc As DataColumn In dtTemp.Columns
                        If ColumnCount > 9 Then
                            Dim strColumnName As String = dc.ColumnName.ToString
                            Dim dbTotCurrentRow As Double = 0
                            Dim dbTotal As Double = 0
                            Dim dbAllTotal As Double = 0
                            Dim str As String = dc.DataType.ToString
                            If str = "System.String" Then
                                Try
                                    dbTotCurrentRow = Convert.ToDouble(dr(strColumnName).ToString)
                                Catch ex As Exception
                                    dbTotCurrentRow = 0
                                End Try

                                Try
                                    dbTotal = Convert.ToDouble(drFST(strColumnName).ToString)
                                Catch ex As Exception
                                    dbTotal = 0
                                End Try

                                Try
                                    dbAllTotal = Convert.ToDouble(drFSTA(strColumnName).ToString)
                                Catch ex As Exception
                                    dbAllTotal = 0
                                End Try

                                dbTotal = dbTotal + dbTotCurrentRow
                                dbAllTotal = dbAllTotal + dbTotCurrentRow
                                drFST(strColumnName) = dbTotal.ToString
                                drFSTA(strColumnName) = dbAllTotal.ToString
                            End If
                        Else
                            ColumnCount += 1
                        End If
                    Next

                End If
                dtMain.ImportRow(dr)
                If strFundSource <> strFundSourceNext Then
                    drFST("Last Name") = strFundSource + " Total"
                    dtMain.Rows.Add(drFST)
                    drFST = dtMain.NewRow()
                End If
                ColumnCount = 1
                RowCount += 1
            Next
            If dtMain.Rows.Count > 0 Then
                drFSTA("Last Name") = "Total ( All Sources )"
                dtMain.Rows.Add(drFSTA)
            End If

            ds.Tables.RemoveAt(0)
            ds.Tables.Add(dtMain)
            '' New Code By Vijay Ramteke on 03/04/2009

        Catch ex As OleDbException
            '   Rollback transaction
            'dumpTrans.Rollback()
        Finally
            'Close Connection
            'db.CloseConnection()
        End Try
        ''Code added by Saraswathi lakshmanan on Sept 09 2009
        ''If the projected Amount is a negative amount then make it Zero.
        ''To fix issue 17401 and 17403
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Columns.Count > 9 Then
                    Dim iColumn As Integer
                    Dim Jrow As Integer
                    For Jrow = 0 To ds.Tables(0).Rows.Count - 1
                        For iColumn = 9 To ds.Tables(0).Columns.Count - 1
                            If ds.Tables(0).Rows(Jrow)(iColumn) < 0 Then
                                ds.Tables(0).Rows(Jrow)(iColumn) = 0
                            End If
                        Next
                    Next

                End If
            End If
        End If
        ds.AcceptChanges()


        Return ds
    End Function
#End Region
End Class