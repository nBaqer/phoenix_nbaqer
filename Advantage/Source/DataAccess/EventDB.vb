Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' EventsDB.vb
'
' EventsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class EventDB
    Public Function GetAllEvents(ByVal showActiveOnly As String, ByVal ModuleName As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         SE.EventID, ")
            .Append("         SE.CampGrpId, ")
            .Append("         SE.ModuleID, ")
            .Append("         SE.EventDate, ")
            .Append("         SE.ShowDate, ")
            .Append("         SE.Title, ")
            .Append("         SE.WhereWhen, ")
            .Append("         SE.Description, ")
            .Append("         SE.ModDate, ")
            .Append("         SE.ModUser, ")
            .Append("         ST.StatusId, ")
            .Append("         ST.Status ")
            .Append("FROM     syEvents SE, syStatuses ST, syModules SM ")
            .Append("WHERE    SE.ModuleID = SM.ModuleID ")
            .Append("AND      SE.StatusId = ST.StatusId ")
            If Not ModuleName Is Nothing Then
                .Append("AND   SM.ModuleName = ? ")
                db.AddParameter("@ModuleName", ModuleName, DataAccess.OleDbDataType.OleDbString, 70, ParameterDirection.Input)
            End If
            If showActiveOnly = "True" Then
                .Append("AND    ST.Status = 'Active' ")
                '.Append("ORDER BY Description ")
            ElseIf showActiveOnly = "False" Then
                .Append("AND    ST.Status = 'Inactive' ")
                '.Append("ORDER BY Description ")
            Else
                '.Append("ORDER BY ST.Status,Description asc")
                .Append("ORDER BY ST.Status ")
            End If
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetCurrentEvents(ByVal showActiveOnly As Boolean, ByVal ModuleName As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   SE.EventID, SE.ModuleID, SE.EventDate, ")
            .Append("         SE.ShowDate, SE.Title, SE.WhereWhen,")
            .Append("         SE.Description, SE.ModDate, SE.ModUser ")
            .Append("FROM     syEvents SE, syStatuses ST, syModules SM ")
            .Append("WHERE    SE.ModuleID = SM.ModuleID ")
            .Append("AND      SE.StatusId = ST.StatusId ")
            If Not ModuleName Is Nothing Then
                .Append(" AND   SM.ModuleName = ? ")
                db.AddParameter("@ModuleName", ModuleName, DataAccess.OleDbDataType.OleDbString, 70, ParameterDirection.Input)
            End If
            If showActiveOnly Then
                .Append("AND     ST.Status = 'Active' ")
            End If
            .Append("AND     SE.EventDate >= ? ")
            db.AddParameter("@EventDate", Date.Today.AddDays(1), DataAccess.OleDbDataType.OleDbDateTime, ParameterDirection.Input)
            .Append("AND     SE.ShowDate <= ? ")
            db.AddParameter("@ShowDate", Date.Today, DataAccess.OleDbDataType.OleDbDateTime, ParameterDirection.Input)
            .Append("ORDER BY SE.EventDate ")
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetEventInfo(ByVal EventID As String) As EventInfo

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT SE.EventID, ")
            .Append("    SE.StatusId, ")
            .Append("    SE.CampGrpId, ")
            .Append("    SE.ModuleID, ")
            .Append("    SE.EventDate, ")
            .Append("    SE.ShowDate, ")
            .Append("    SE.Title, ")
            .Append("    SE.WhereWhen, ")
            .Append("    SE.Description, ")
            .Append("    SE.ModDate, ")
            .Append("    SE.ModUser ")
            .Append("FROM  syEvents SE ")
            .Append("WHERE SE.EventID= ? ")
        End With

        ' Add the EventId to the parameter list
        db.AddParameter("@EventID", EventID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim EventInfo As New EventInfo

        While dr.Read()

            '   set properties with data from DataReader
            With EventInfo
                .IsInDB = True
                .EventID = EventID
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                .ModuleID = dr("ModuleID")
                .EventDate = dr("EventDate")
                .ShowDate = dr("ShowDate")
                .Title = dr("Title")
                .WhereWhen = dr("WhereWhen")
                .Description = dr("Description")
                .ModDate = dr("ModDate")
                .ModUser = dr("ModUser")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return EventInfo
        Return EventInfo

    End Function
    Public Function UpdateEventInfo(ByVal EventInfo As EventInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syEvents ")
                .Append("   Set StatusId = ?, ModuleID = ?, ")
                .Append("   CampGrpId = ?, ")
                .Append("   EventDate = ?, ShowDate = ?, ")
                .Append("   Title = ?, WhereWhen = ?, Description = ?, ")
                .Append("   ModUser = ?, ModDate = ? ")
                .Append("WHERE EventID = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from syEvents where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   StatusID
            db.AddParameter("@StatusID", EventInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModuleID
            db.AddParameter("@ModuleID", EventInfo.ModuleID, DataAccess.OleDbDataType.OleDbInteger, ParameterDirection.Input)

            '   CampGrpId
            If EventInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", EventInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   EventDate
            db.AddParameter("@EventDate", EventInfo.EventDate, DataAccess.OleDbDataType.OleDbDateTime, ParameterDirection.Input)

            '   ShowDate
            db.AddParameter("@EventDate", EventInfo.ShowDate, DataAccess.OleDbDataType.OleDbDateTime, ParameterDirection.Input)

            '   Title
            db.AddParameter("@Title", EventInfo.Title, DataAccess.OleDbDataType.OleDbString, 150, ParameterDirection.Input)

            '   WhereWhen
            db.AddParameter("@WhereWhen", EventInfo.WhereWhen, DataAccess.OleDbDataType.OleDbString, 150, ParameterDirection.Input)

            '   Description
            db.AddParameter("@Description", EventInfo.Description, DataAccess.OleDbDataType.OleDbString, 2000, ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   EventID
            db.AddParameter("@EventID", EventInfo.EventID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", EventInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddEventInfo(ByVal EventInfo As EventInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT syEvents (EventID, StatusId, ModuleID, CampGrpId, EventDate, ShowDate, ")
                .Append("   Title, WhereWhen, Description, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   EventID
            db.AddParameter("@EventID", EventInfo.EventID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusID
            db.AddParameter("@StatusID", EventInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModuleID
            db.AddParameter("@ModuleID", EventInfo.ModuleID, DataAccess.OleDbDataType.OleDbInteger, ParameterDirection.Input)

            '   CampGrpId
            If EventInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", EventInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   EventDate
            db.AddParameter("@EventDate", EventInfo.EventDate, DataAccess.OleDbDataType.OleDbDateTime, ParameterDirection.Input)

            '   ShowDate
            db.AddParameter("@EventDate", EventInfo.ShowDate, DataAccess.OleDbDataType.OleDbDateTime, ParameterDirection.Input)

            '   Title
            db.AddParameter("@Title", EventInfo.Title, DataAccess.OleDbDataType.OleDbString, 150, ParameterDirection.Input)

            '   WhereWhen
            db.AddParameter("@WhereWhen", EventInfo.WhereWhen, DataAccess.OleDbDataType.OleDbString, 150, ParameterDirection.Input)

            '   Description
            db.AddParameter("@Description", EventInfo.Description, DataAccess.OleDbDataType.OleDbString, 2000, ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteEventInfo(ByVal EventID As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syEvents ")
                .Append("WHERE EventID = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM syEvents WHERE EventID = ? ")
            End With

            '   add parameters values to the query

            '   EventID
            db.AddParameter("@EventID", EventID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   EventId
            db.AddParameter("@EventID", EventID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
#Region "Get AdvAppsetting for Manage Config entry"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
End Class
