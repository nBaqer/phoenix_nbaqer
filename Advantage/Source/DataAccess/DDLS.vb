Imports FAME.Advantage.Common

Public Class DDLS
    Public Function GetDDLValue(ByVal ddlId As Integer, ByVal idVal As String) As String
        Dim db As New DataAccess
        Dim db2 As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim strTemplate As String
        Dim strDDLValue As String
        Dim dr As DataRow
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim DisplayField As String = "%DisplayField%"
        Dim ValueField As String = "%ValueField%"
        Dim TableName As String = "%TableName%"

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'Modified by Michelle R. Rodriguez on 03/30/2005
        Select Case ddlId
            Case 51
                'Special case: Student Enrollment
                With sb
                    .Append("SELECT B.PrgVerDescrip As DispText ")
                    .Append("FROM arStuEnrollments A, arPrgVersions B ")
                    .Append("WHERE A.StuEnrollId = ? AND A.PrgVerId = B.PrgVerId")
                End With

                db.AddParameter("@idval", idVal, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                Return db.RunParamSQLScalar(sb.ToString).ToString

            Case 123
                'Special case: Employer Contact
                With sb
                    .Append("SELECT A.FirstName,A.MiddleName,A.LastName ")
                    .Append("FROM plEmployerContact A ")
                    .Append("WHERE A.EmployerContactId = ? ")
                End With

                db.AddParameter("@idval", idVal, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                ds = db.RunParamSQLDataSet(sb.ToString)

                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim row As DataRow = ds.Tables(0).Rows(0)
                        If Not row.IsNull("FirstName") Then
                            strDDLValue = row("FirstName")
                        End If
                        If Not row.IsNull("MiddleName") Then
                            If row("MiddleName") <> "" Then strDDLValue &= " " & row("MiddleName")
                        End If
                        If Not row.IsNull("LastName") Then
                            If row("LastName") <> "" Then strDDLValue &= " " & row("LastName")
                        End If
                    End If
                End If

                Return strDDLValue

            Case Else
                'Get the metadata for the DDLId passed in
                With sb
                    .Append("SELECT t2.TblName,t3.FldName as DispText,t4.FldName as DispValue ")
                    .Append("FROM syDDLS t1, syTables t2,syFields t3,syFields t4 ")
                    .Append("WHERE t1.TblId=t2.TblId ")
                    .Append("AND t1.DispFldId=t3.FldId ")
                    .Append("AND t1.ValFldId=t4.FldId ")
                    .Append("AND t1.DDLId=?")
                End With

                db.AddParameter("@ddlid", ddlId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)

                ds = db.RunParamSQLDataSet(sb.ToString)

                db2.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                dr = ds.Tables(0).Rows(0)
                strTemplate = "SELECT %DisplayField% As DispText " & _
                                "FROM %TableName% " & _
                                "WHERE %ValueField% = ?"

                'Replace the TableName token
                strTemplate = strTemplate.Replace(TableName, dr("TblName").ToString())
                'Replace the DisplayField token
                strTemplate = strTemplate.Replace(DisplayField, dr("DispText").ToString())
                'Replace the ValueField token
                strTemplate = strTemplate.Replace(ValueField, dr("DispValue").ToString())

                db2.AddParameter("@idval", idVal, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                ds2 = db2.RunParamSQLDataSet(strTemplate)

                If ds2.Tables.Count > 0 Then
                    If ds2.Tables(0).Rows.Count > 0 Then
                        strDDLValue = ds2.Tables(0).Rows(0)("DispText")
                    End If
                End If

                Return strDDLValue
        End Select

    End Function

End Class
