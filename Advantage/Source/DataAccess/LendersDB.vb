Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' LendersDB.vb
'
' LendersDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class LendersDB
    Public Function GetAllLenders(ByVal strStatus As String, ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         L.LenderId, ")
            .Append("         L.StatusId, ")
            .Append("         ST.Status AS StatusDescrip, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("         L.Code, ")
            .Append("         L.LenderDescrip, ")
            .Append("         L.Address1, ")
            .Append("         L.Address2, ")
            .Append("         L.City, ")
            .Append("         L.StateId, ")
            .Append("         L.OtherState, ")
            .Append("         L.Zip, ")
            .Append("         L.ForeignAddress, ")
            .Append("         L.CountryId, ")
            .Append("         L.Email, ")
            .Append("         L.PrimaryContact, ")
            .Append("         L.OtherContact, ")
            .Append("         L.IsLender, ")
            .Append("         L.IsServicer, ")
            .Append("         L.IsGuarantor, ")
            .Append("         L.PayAddress1, ")
            .Append("         L.PayAddress2, ")
            .Append("         L.PayCity, ")
            .Append("         L.PayStateId, ")
            .Append("         L.OtherPayState, ")
            .Append("         L.PayZip, ")
            .Append("         L.PayCountryId, ")
            .Append("         L.CustService, ")
            .Append("         L.ForeignCustService, ")
            .Append("         L.Fax, ")
            .Append("         L.ForeignFax, ")
            .Append("         L.PreClaim, ")
            .Append("         L.ForeignPreClaim, ")
            .Append("         L.PostClaim, ")
            .Append("         L.ForeignPostClaim, ")
            .Append("         L.Comments, ")
            .Append("         IsUsedAsLender = (CASE WHEN (SELECT COUNT(*) FROM faStudentAwards WHERE LenderId=L.LenderId) > 0 THEN 'True' ELSE 'False' END), ")
            .Append("         IsUsedAsServicer = (CASE WHEN (SELECT COUNT(*) FROM faStudentAwards WHERE ServicerId=L.LenderId) > 0 THEN 'True' ELSE 'False' END), ")
            .Append("         IsUsedAsGuarantor = (CASE WHEN (SELECT COUNT(*) FROM faStudentAwards WHERE GuarantorId=L.LenderId) > 0 THEN 'True' ELSE 'False' END), ")
            .Append("         L.ModUser, ")
            .Append("         L.ModDate ")
            .Append("FROM     faLenders L, syStatuses ST ")
            .Append("WHERE    L.StatusId = ST.StatusId ")
            .Append("         AND L.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            If strStatus = "True" Then
                .Append("AND    ST.Status = 'Active' ")
                .Append("ORDER BY L.LenderDescrip ")
            ElseIf strStatus = "False" Then
                .Append("AND    ST.Status = 'Inactive' ")
                .Append("ORDER BY L.LenderDescrip ")
            Else
                .Append("ORDER BY StatusDescrip, L.LenderDescrip ")
            End If
        End With
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetLendersOnly(ByVal strStatus As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         L.LenderId, ")
            .Append("         L.StatusId, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("         L.Code, ")
            .Append("         L.LenderDescrip, ")
            .Append("         L.Address1, ")
            .Append("         L.Address2, ")
            .Append("         L.City, ")
            .Append("         L.StateId, ")
            .Append("         L.OtherState, ")
            .Append("         L.Zip, ")
            .Append("         L.ForeignAddress, ")
            .Append("         L.CountryId, ")
            .Append("         L.Email, ")
            .Append("         L.PrimaryContact, ")
            .Append("         L.OtherContact, ")
            .Append("         L.IsLender, ")
            .Append("         L.IsServicer, ")
            .Append("         L.IsGuarantor, ")
            .Append("         L.PayAddress1, ")
            .Append("         L.PayAddress2, ")
            .Append("         L.PayCity, ")
            .Append("         L.PayStateId, ")
            .Append("         L.OtherPayState, ")
            .Append("         L.PayZip, ")
            .Append("         L.PayCountryId, ")
            .Append("         L.CustService, ")
            .Append("         L.ForeignCustService, ")
            .Append("         L.Fax, ")
            .Append("         L.ForeignFax, ")
            .Append("         L.PreClaim, ")
            .Append("         L.ForeignPreClaim, ")
            .Append("         L.PostClaim, ")
            .Append("         L.ForeignPostClaim, ")
            .Append("         L.Comments, ")
            .Append("         L.ModUser, ")
            .Append("         L.ModDate ")
            .Append("FROM     faLenders L, syStatuses ST ")
            .Append("WHERE    L.StatusId = ST.StatusId AND L.IsLender = 1 ")
            If strStatus = "True" Then
                .Append("AND    ST.Status = 'Active' ")
                .Append("ORDER BY L.LenderDescrip ")
            ElseIf strStatus = "False" Then
                .Append("AND    ST.Status = 'Inactive' ")
                .Append("ORDER BY L.LenderDescrip ")
            Else
                .Append("ORDER BY ST.Status,L.LenderDescrip asc")
            End If
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetServicersOnly(ByVal strStatus As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         L.LenderId, ")
            .Append("         L.StatusId, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("         L.Code, ")
            .Append("         L.LenderDescrip, ")
            .Append("         L.Address1, ")
            .Append("         L.Address2, ")
            .Append("         L.City, ")
            .Append("         L.StateId, ")
            .Append("         L.OtherState, ")
            .Append("         L.Zip, ")
            .Append("         L.ForeignAddress, ")
            .Append("         L.CountryId, ")
            .Append("         L.Email, ")
            .Append("         L.PrimaryContact, ")
            .Append("         L.OtherContact, ")
            .Append("         L.IsLender, ")
            .Append("         L.IsServicer, ")
            .Append("         L.IsGuarantor, ")
            .Append("         L.PayAddress1, ")
            .Append("         L.PayAddress2, ")
            .Append("         L.PayCity, ")
            .Append("         L.PayStateId, ")
            .Append("         L.OtherPayState, ")
            .Append("         L.PayZip, ")
            .Append("         L.PayCountryId, ")
            .Append("         L.CustService, ")
            .Append("         L.ForeignCustService, ")
            .Append("         L.Fax, ")
            .Append("         L.ForeignFax, ")
            .Append("         L.PreClaim, ")
            .Append("         L.ForeignPreClaim, ")
            .Append("         L.PostClaim, ")
            .Append("         L.ForeignPostClaim, ")
            .Append("         L.Comments, ")
            .Append("         L.ModUser, ")
            .Append("         L.ModDate ")
            .Append("FROM     faLenders L, syStatuses ST ")
            .Append("WHERE    L.StatusId = ST.StatusId AND L.IsServicer = 1 ")
            If strStatus = "True" Then
                .Append("AND    ST.Status = 'Active' ")
                .Append("ORDER BY L.LenderDescrip ")
            ElseIf strStatus = "False" Then
                .Append("AND    ST.Status = 'Inactive' ")
                .Append("ORDER BY L.LenderDescrip ")
            Else
                .Append("ORDER BY ST.Status,L.LenderDescrip asc")
            End If
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetGuarantorsOnly(ByVal strStatus As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         L.LenderId, ")
            .Append("         L.StatusId, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("         L.Code, ")
            .Append("         L.LenderDescrip, ")
            .Append("         L.Address1, ")
            .Append("         L.Address2, ")
            .Append("         L.City, ")
            .Append("         L.StateId, ")
            .Append("         L.OtherState, ")
            .Append("         L.Zip, ")
            .Append("         L.ForeignAddress, ")
            .Append("         L.CountryId, ")
            .Append("         L.Email, ")
            .Append("         L.PrimaryContact, ")
            .Append("         L.OtherContact, ")
            .Append("         L.IsLender, ")
            .Append("         L.IsServicer, ")
            .Append("         L.IsGuarantor, ")
            .Append("         L.PayAddress1, ")
            .Append("         L.PayAddress2, ")
            .Append("         L.PayCity, ")
            .Append("         L.PayStateId, ")
            .Append("         L.OtherPayState, ")
            .Append("         L.PayZip, ")
            .Append("         L.PayCountryId, ")
            .Append("         L.CustService, ")
            .Append("         L.ForeignCustService, ")
            .Append("         L.Fax, ")
            .Append("         L.ForeignFax, ")
            .Append("         L.PreClaim, ")
            .Append("         L.ForeignPreClaim, ")
            .Append("         L.PostClaim, ")
            .Append("         L.ForeignPostClaim, ")
            .Append("         L.Comments, ")
            .Append("         L.ModUser, ")
            .Append("         L.ModDate ")
            .Append("FROM     faLenders L, syStatuses ST ")
            .Append("WHERE    L.StatusId = ST.StatusId AND L.IsGuarantor = 1 ")
            If strStatus = "True" Then
                .Append("AND    ST.Status = 'Active' ")
                .Append("ORDER BY L.LenderDescrip ")
            ElseIf strStatus = "False" Then
                .Append("AND    ST.Status = 'Inactive' ")
                .Append("ORDER BY L.LenderDescrip ")
            Else
                .Append("ORDER BY ST.Status,L.LenderDescrip asc")
            End If
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetLenderInfo(ByVal LenderId As String) As LenderInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("         L.LenderId, ")
            .Append("         L.StatusId, ")
            .Append("         L.CampGrpId, ")
            .Append("         L.Code, ")
            .Append("         L.LenderDescrip, ")
            .Append("         L.Address1, ")
            .Append("         L.Address2, ")
            .Append("         L.City, ")
            .Append("         L.StateId, ")
            .Append("         (Select StateDescrip from syStates where StateId=L.StateId) as State, ")
            .Append("         L.OtherState, ")
            .Append("         L.Zip, ")
            .Append("         L.ForeignAddress, ")
            .Append("         L.CountryId, ")
            .Append("         (Select CountryDescrip from adCountries where CountryId=L.CountryId) As Country, ")
            .Append("         L.Email, ")
            .Append("         L.PrimaryContact, ")
            .Append("         L.OtherContact, ")
            .Append("         L.IsLender, ")
            .Append("         L.IsServicer, ")
            .Append("         L.IsGuarantor, ")
            .Append("         L.PayAddress1, ")
            .Append("         L.PayAddress2, ")
            .Append("         L.PayCity, ")
            .Append("         L.PayStateId, ")
            .Append("         (Select StateDescrip from syStates where StateId=L.PayStateId) as PayState, ")
            .Append("         L.OtherPayState, ")
            .Append("         L.PayZip, ")
            .Append("         L.ForeignPayAddress, ")
            .Append("         L.PayCountryId, ")
            .Append("         (Select CountryDescrip from adCountries where CountryId=L.PayCountryId) As PayCountry, ")
            .Append("         L.CustService, ")
            .Append("         L.ForeignCustService, ")
            .Append("         L.Fax, ")
            .Append("         L.ForeignFax, ")
            .Append("         L.PreClaim, ")
            .Append("         L.ForeignPreClaim, ")
            .Append("         L.PostClaim, ")
            .Append("         L.ForeignPostClaim, ")
            .Append("         L.Comments, ")
            .Append("         IsUsedAsLender = (CASE WHEN (SELECT COUNT(*) FROM faStudentAwards WHERE LenderId=L.LenderId) > 0 THEN 'True' ELSE 'False' END), ")
            .Append("         IsUsedAsServicer = (CASE WHEN (SELECT COUNT(*) FROM faStudentAwards WHERE ServicerId=L.LenderId) > 0 THEN 'True' ELSE 'False' END), ")
            .Append("         IsUsedAsGuarantor = (CASE WHEN (SELECT COUNT(*) FROM faStudentAwards WHERE GuarantorId=L.LenderId) > 0 THEN 'True' ELSE 'False' END), ")
            .Append("         L.ModUser, ")
            .Append("         L.ModDate ")
            .Append("FROM  faLenders L ")
            .Append("WHERE L.LenderId = ? ")
        End With

        ' Add the LenderId to the parameter list
        db.AddParameter("@LenderId", LenderId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim LenderInfo As New LenderInfo

        While dr.Read()

            '   set properties with data from DataReader
            With LenderInfo
                .IsInDB = True
                .LenderId = LenderId
                .StatusId = CType(dr("StatusId"), Guid).ToString
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampusGroupId = CType(dr("CampGrpId"), Guid).ToString
                .Code = dr("Code")
                .LenderDescrip = dr("LenderDescrip")
                If Not (dr("Address1") Is System.DBNull.Value) Then .Address1 = dr("Address1")
                If Not (dr("Address2") Is System.DBNull.Value) Then .Address2 = dr("Address2")
                If Not (dr("City") Is System.DBNull.Value) Then .City = dr("City")
                If Not (dr("StateId") Is System.DBNull.Value) Then .StateId = CType(dr("StateId"), Guid).ToString
                If Not (dr("State") Is System.DBNull.Value) Then .State = dr("State")
                If Not (dr("OtherState") Is System.DBNull.Value) Then .OtherState = dr("OtherState")
                If Not (dr("Zip") Is System.DBNull.Value) Then .Zip = dr("Zip")
                .ForeignAddress = dr("ForeignAddress")
                If Not (dr("CountryId") Is System.DBNull.Value) Then .CountryId = CType(dr("CountryId"), Guid).ToString
                If Not (dr("Country") Is System.DBNull.Value) Then .Country = dr("Country")
                If Not (dr("Email") Is System.DBNull.Value) Then .Email = dr("Email")
                If Not (dr("PrimaryContact") Is System.DBNull.Value) Then .PrimaryContact = dr("PrimaryContact")
                If Not (dr("OtherContact") Is System.DBNull.Value) Then .OtherContact = dr("OtherContact")
                .IsLender = dr("IsLender")
                .IsServicer = dr("IsServicer")
                .IsGuarantor = dr("IsGuarantor")
                If Not (dr("PayAddress1") Is System.DBNull.Value) Then .PayAddress1 = dr("PayAddress1")
                If Not (dr("PayAddress2") Is System.DBNull.Value) Then .PayAddress2 = dr("PayAddress2")
                If Not (dr("PayCity") Is System.DBNull.Value) Then .PayCity = dr("PayCity")
                If Not (dr("PayStateId") Is System.DBNull.Value) Then .PayStateId = CType(dr("PayStateId"), Guid).ToString
                If Not (dr("PayState") Is System.DBNull.Value) Then .PayState = dr("PayState")
                If Not (dr("OtherPayState") Is System.DBNull.Value) Then .OtherPayState = dr("OtherPayState")
                If Not (dr("PayZip") Is System.DBNull.Value) Then .PayZip = dr("PayZip")
                .ForeignPayAddress = dr("ForeignPayAddress")
                If Not (dr("PayCountryId") Is System.DBNull.Value) Then .PayCountryId = CType(dr("PayCountryId"), Guid).ToString
                If Not (dr("PayCountry") Is System.DBNull.Value) Then .PayCountry = dr("PayCountry")
                If Not (dr("CustService") Is System.DBNull.Value) Then .CustService = dr("CustService")
                .ForeignCustService = dr("ForeignCustService")
                If Not (dr("Fax") Is System.DBNull.Value) Then .Fax = dr("Fax")
                .ForeignFax = dr("ForeignFax")
                If Not (dr("PreClaim") Is System.DBNull.Value) Then .PreClaim = dr("PreClaim")
                .ForeignPreClaim = dr("ForeignPreClaim")
                If Not (dr("PostClaim") Is System.DBNull.Value) Then .PostClaim = dr("PostClaim")
                .ForeignPostClaim = dr("ForeignPostClaim")
                If Not (dr("Comments") Is System.DBNull.Value) Then .Comments = dr("Comments")
                .IsUsedAsLender = dr("IsUsedAsLender")
                .IsUsedAsServicer = dr("IsUsedAsServicer")
                .IsUsedAsGuarantor = dr("IsUsedAsGuarantor")
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return LenderInfo
        Return LenderInfo
    End Function
    Public Function UpdateLenderInfo(ByVal LenderInfo As LenderInfo, ByVal user As String) As ResultInfo
        Dim resInfo As New ResultInfo

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE faLenders ")
                .Append("   Set LenderId = ?, StatusId = ?, CampGrpId = ?, Code =?, LenderDescrip = ?, ")
                .Append("       Address1 = ?, Address2 = ?, City = ?, StateId = ?, OtherState = ?, Zip = ?, ForeignAddress = ?, CountryId = ?, ")
                .Append("       Email = ?, PrimaryContact = ?, OtherContact = ?, ")
                .Append("       IsLender = ?, IsServicer = ?, IsGuarantor = ?, ")
                .Append("       PayAddress1 = ?, PayAddress2 = ?, PayCity = ?, PayStateId = ?, OtherPayState = ?, PayZip = ?, ForeignPayAddress = ?, PayCountryId = ?, ")
                .Append("       CustService = ?, ForeignCustService = ?, Fax = ?, ForeignFax = ?, PreClaim = ?, ForeignPreClaim = ?, PostClaim = ?, ForeignPostClaim = ?, ")
                .Append("       Comments = ?, ModUser = ?, ModDate = ? ")
                .Append("WHERE LenderId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from faLenders where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   LenderId
            db.AddParameter("@LenderId", LenderInfo.LenderId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", LenderInfo.StatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   CampusGroupId
            db.AddParameter("@CampGrpId", LenderInfo.CampusGroupId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Code
            db.AddParameter("@Code", LenderInfo.Code, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   LenderDescrip
            db.AddParameter("@LenderDescrip", LenderInfo.LenderDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Address1
            If LenderInfo.Address1 = "" Then
                db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Address1", LenderInfo.Address1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Address2
            If LenderInfo.Address2 = "" Then
                db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", LenderInfo.Address2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   City
            If LenderInfo.City = "" Then
                db.AddParameter("@City", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@City", LenderInfo.City, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   StateId
            If LenderInfo.StateId = Guid.Empty.ToString Then
                db.AddParameter("@StateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@StateId", LenderInfo.StateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   OtherState
            If LenderInfo.OtherState = "" Then
                db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@OtherState", LenderInfo.OtherState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Zip
            If LenderInfo.Zip = "" Then
                db.AddParameter("@Zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Zip", LenderInfo.Zip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ForeignAddress
            db.AddParameter("@ForeignAddress", LenderInfo.ForeignAddress, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   CountryId
            If LenderInfo.CountryId = Guid.Empty.ToString Then
                db.AddParameter("@CountryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CountryId", LenderInfo.CountryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Email
            If LenderInfo.Email = "" Then
                db.AddParameter("@Email", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Email", LenderInfo.Email, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   PrimaryContact
            If LenderInfo.PrimaryContact = "" Then
                db.AddParameter("@PrimaryContact", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PrimaryContact", LenderInfo.PrimaryContact, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   OtherContact
            If LenderInfo.OtherContact = "" Then
                db.AddParameter("@OtherContact", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@OtherContact", LenderInfo.OtherContact, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   IsLender
            db.AddParameter("@IsLender", LenderInfo.IsLender, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   IsServicer
            db.AddParameter("@IsServicer", LenderInfo.IsServicer, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   IsGuarantor
            db.AddParameter("@IsGuarantor", LenderInfo.IsGuarantor, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   PayAddress1
            If LenderInfo.PayAddress1 = "" Then
                db.AddParameter("@PayAddress1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PayAddress1", LenderInfo.PayAddress1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   PayAddress2
            If LenderInfo.PayAddress2 = "" Then
                db.AddParameter("@PayAddress2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PayAddress2", LenderInfo.PayAddress2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   PayCity
            If LenderInfo.PayCity = "" Then
                db.AddParameter("@PayCity", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PayCity", LenderInfo.PayCity, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   PayStateId
            If LenderInfo.PayStateId = Guid.Empty.ToString Then
                db.AddParameter("@PayStateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PayStateId", LenderInfo.PayStateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   OtherPayState
            If LenderInfo.OtherPayState = "" Then
                db.AddParameter("@PayOtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PayOtherState", LenderInfo.OtherPayState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   PayZip
            If LenderInfo.PayZip = "" Then
                db.AddParameter("@PayZip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PayZip", LenderInfo.PayZip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ForeignPayAddress
            db.AddParameter("@ForeignPayAddress", LenderInfo.ForeignPayAddress, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   PayCountryId
            If LenderInfo.PayCountryId = Guid.Empty.ToString Then
                db.AddParameter("@PayCountryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PayCountryId", LenderInfo.PayCountryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   CustService
            If LenderInfo.CustService = "" Then
                db.AddParameter("@CustService", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CustService", LenderInfo.CustService, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ForeignCustService
            db.AddParameter("@ForeignCustService", LenderInfo.ForeignCustService, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   Fax
            If LenderInfo.Fax = "" Then
                db.AddParameter("@Fax", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Fax", LenderInfo.Fax, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ForeignFax
            db.AddParameter("@ForeignFax", LenderInfo.ForeignFax, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   PreClaim
            If LenderInfo.PreClaim = "" Then
                db.AddParameter("@PreClaim", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PreClaim", LenderInfo.PreClaim, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ForeignPreClaim
            db.AddParameter("@ForeignPreClaim", LenderInfo.ForeignPreClaim, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   PostClaim
            If LenderInfo.PostClaim = "" Then
                db.AddParameter("@PostClaim", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PostClaim", LenderInfo.PostClaim, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ForeignPostClaim
            db.AddParameter("@ForeignPostClaim", LenderInfo.ForeignPostClaim, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   Comments
            If LenderInfo.Comments = "" Then
                db.AddParameter("@Comments", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Comments", LenderInfo.Comments, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            'Dim now As Date = Date.Now
            Dim strnow As Date = Utilities.GetAdvantageDBDateTime(Date.Now)
            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   LenderId
            db.AddParameter("@LenderId", LenderInfo.LenderId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", LenderInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                'Return ""
                LenderInfo.ModUser = user
                LenderInfo.ModDate = strnow
                resInfo.UpdatedObject = LenderInfo
                Return resInfo
            Else
                'Return DALExceptions.BuildConcurrencyExceptionMessage()
                resInfo.ErrorString = DALExceptions.BuildConcurrencyExceptionMessage()
                Return resInfo
            End If

        Catch ex As OleDbException

            '   return an error to the client
            'DisplayOleDbErrorCollection(ex)
            resInfo.ErrorString = DALExceptions.BuildErrorMessage(ex)
            Return resInfo

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddLenderInfo(ByVal LenderInfo As LenderInfo, ByVal user As String) As ResultInfo
        Dim resInfo As New ResultInfo

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT faLenders ( ")
                .Append("       LenderId, StatusId, CampGrpId, Code, LenderDescrip, ")
                .Append("       Address1, Address2, City, StateId, OtherState, Zip, ForeignAddress, CountryId, ")
                .Append("       Email, PrimaryContact, OtherContact, ")
                .Append("       IsLender, IsServicer, IsGuarantor, ")
                .Append("       PayAddress1, PayAddress2, PayCity, PayStateId, OtherPayState, PayZip, ForeignPayAddress, PayCountryId, ")
                .Append("       CustService, ForeignCustService, Fax, ForeignFax, PreClaim, ForeignPreClaim, PostClaim, ForeignPostClaim, ")
                .Append("       Comments, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   LenderId
            db.AddParameter("@LenderId", LenderInfo.LenderId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", LenderInfo.StatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   CampusGroupId
            db.AddParameter("@CampGrpId", LenderInfo.CampusGroupId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Code
            db.AddParameter("@Code", LenderInfo.Code, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   LenderDescrip
            db.AddParameter("@LenderDescrip", LenderInfo.LenderDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Address1
            If LenderInfo.Address1 = "" Then
                db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Address1", LenderInfo.Address1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Address2
            If LenderInfo.Address2 = "" Then
                db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", LenderInfo.Address2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   City
            If LenderInfo.City = "" Then
                db.AddParameter("@City", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@City", LenderInfo.City, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   StateId
            If LenderInfo.StateId = Guid.Empty.ToString Then
                db.AddParameter("@StateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@StateId", LenderInfo.StateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   OtherState
            If LenderInfo.OtherState = "" Then
                db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@OtherState", LenderInfo.OtherState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Zip
            If LenderInfo.Zip = "" Then
                db.AddParameter("@Zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Zip", LenderInfo.Zip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ForeignAddress
            db.AddParameter("@ForeignAddress", LenderInfo.ForeignAddress, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   CountryId
            If LenderInfo.CountryId = Guid.Empty.ToString Then
                db.AddParameter("@CountryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CountryId", LenderInfo.CountryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Email
            If LenderInfo.Email = "" Then
                db.AddParameter("@Email", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Email", LenderInfo.Email, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   PrimaryContact
            If LenderInfo.PrimaryContact = "" Then
                db.AddParameter("@PrimaryContact", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PrimaryContact", LenderInfo.PrimaryContact, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   OtherContact
            If LenderInfo.OtherContact = "" Then
                db.AddParameter("@OtherContact", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@OtherContact", LenderInfo.OtherContact, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   IsLender
            db.AddParameter("@IsLender", LenderInfo.IsLender, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   IsServicer
            db.AddParameter("@IsServicer", LenderInfo.IsServicer, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   IsGuarantor
            db.AddParameter("@IsGuarantor", LenderInfo.IsGuarantor, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   PayAddress1
            If LenderInfo.PayAddress1 = "" Then
                db.AddParameter("@PayAddress1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PayAddress1", LenderInfo.PayAddress1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   PayAddress2
            If LenderInfo.PayAddress2 = "" Then
                db.AddParameter("@PayAddress2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PayAddress2", LenderInfo.PayAddress2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   PayCity
            If LenderInfo.PayCity = "" Then
                db.AddParameter("@PayCity", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PayCity", LenderInfo.PayCity, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   PayStateId
            If LenderInfo.PayStateId = Guid.Empty.ToString Then
                db.AddParameter("@PayStateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PayStateId", LenderInfo.PayStateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   OtherPayState
            If LenderInfo.OtherPayState = "" Then
                db.AddParameter("@OtherPayState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@OtherPayState", LenderInfo.OtherPayState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   PayZip
            If LenderInfo.PayZip = "" Then
                db.AddParameter("@PayZip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PayZip", LenderInfo.PayZip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ForeignPayAddress
            db.AddParameter("@ForeignPayAddress", LenderInfo.ForeignPayAddress, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   PayCountryId
            If LenderInfo.PayCountryId = Guid.Empty.ToString Then
                db.AddParameter("@PayCountryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PayCountryId", LenderInfo.PayCountryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   CustService
            If LenderInfo.CustService = "" Then
                db.AddParameter("@CustService", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CustService", LenderInfo.CustService, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ForeignCustService
            db.AddParameter("@ForeignCustService", LenderInfo.ForeignCustService, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   Fax
            If LenderInfo.Fax = "" Then
                db.AddParameter("@Fax", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Fax", LenderInfo.Fax, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ForeignFax
            db.AddParameter("@ForeignFax", LenderInfo.ForeignFax, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   PreClaim
            If LenderInfo.PreClaim = "" Then
                db.AddParameter("@PreClaim", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PreClaim", LenderInfo.PreClaim, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ForeignPreClaim
            db.AddParameter("@ForeignPreClaim", LenderInfo.ForeignPreClaim, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   PostClaim
            If LenderInfo.PostClaim = "" Then
                db.AddParameter("@PostClaim", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PostClaim", LenderInfo.PostClaim, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ForeignPostClaim
            db.AddParameter("@ForeignPostClaim", LenderInfo.ForeignPostClaim, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   Comments
            If LenderInfo.Comments = "" Then
                db.AddParameter("@Comments", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Comments", LenderInfo.Comments, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModDate
            'Dim now As Date = Date.Now
            Dim strnow As Date = Utilities.GetAdvantageDBDateTime(Date.Now)
            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            'Return ""
            LenderInfo.IsInDB = True
            LenderInfo.ModUser = user
            LenderInfo.ModDate = strnow
            resInfo.UpdatedObject = LenderInfo
            Return resInfo

        Catch ex As OleDbException
            '   return an error to the client
            resInfo.ErrorString = DALExceptions.BuildErrorMessage(ex)
            Return resInfo

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteLenderInfo(ByVal LenderId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM faLenders ")
                .Append("WHERE LenderId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM faLenders WHERE LenderId = ? ")
            End With

            '   add parameters values to the query

            '   LenderId
            db.AddParameter("@LenderId", LenderId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   LenderId
            db.AddParameter("@LenderId", LenderId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
End Class