Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class StdGrdBkDB
    Public Function GetAllTerms() As DataSet
        Dim ds As DataSet

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.TermId,t1.TermDescrip ")
                .Append("FROM arTerm t1  ")
                '.Append("WHERE t2.Status = 'Active' and t1.StatusId = t2.StatusId ")
            End With

            '   Execute the query
            ds = db.RunSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetClsSects(ByVal Term As String, ByVal Instr As String, ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT a.ClsSectionId, a.ReqId, a.ClsSection, b.Descrip ")
                .Append("FROM arClassSections a, arReqs b ")
                .Append("WHERE a.TermId = ?  and a.ReqId = b.ReqId and a.InstructorId = ? ")
                .Append("AND a.CampusId = ? and a.StartDate <= ? ")
                .Append("ORDER BY b.Descrip")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@TermId", Term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@instructor", Instr, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@cmpid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Sdate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "TermClsSects")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetClsSects(ByVal Term As String, ByVal Instr As String, ByVal CampusId As String, ByVal UserName As String) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT ")
                .Append("a.ClsSectionId, ")
                .Append("c.ReqId, ")
                .Append("c.ClsSection, ")
                .Append("b.Descrip ")
                .Append("FROM   arClassSectionTerms a,arReqs b, arClassSections c ")
                .Append("WHERE  a.TermId=? ")
                .Append("AND a.ClsSectionId = c.ClsSectionId  ")
                .Append(" AND c.ReqId=b.ReqId ")
                If Not (UserName = "sa") Then
                    'retrieve all class sections where the instructor teaches in.
                    '.Append("AND c.InstructorId=? ")
                    .Append("AND		c.InstructorId in (select InstructorId from arInstructorsSupervisors where SupervisorId = ? ")
                    .Append("							    union all ")
                    .Append("							    select ? ) ")
                End If
                .Append("AND c.CampusId=? ")
                .Append("AND c.StartDate<=?  ")
                .Append("ORDER BY b.Descrip ")
            End With

            'With sb
            '    .Append("SELECT ")
            '    .Append("       a.ClsSectionId,")
            '    .Append("       a.ReqId,")
            '    .Append("       a.ClsSection,")
            '    .Append("       b.Descrip ")
            '    .Append("FROM   arClassSections a,arReqs b ")
            '    .Append("WHERE  a.TermId=?")
            '    .Append("       AND a.ReqId=b.ReqId")
            '    If Not (UserName = "sa") Then
            '        'retrieve all class sections where the instructor teaches in.
            '        .Append("       AND a.InstructorId=?")
            '    End If
            '    .Append("       AND a.CampusId=?")
            '    .Append("       AND a.StartDate<=? ")
            '    .Append("ORDER BY b.Descrip")
            'End With

            db.AddParameter("@TermId", Term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Not (UserName = "sa") Then
                db.AddParameter("@instructor", Instr, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@instructor", Instr, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            db.AddParameter("@cmpid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Sdate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "TermClsSects")

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        Return ds
    End Function

    Public Function PopulateStdDataList(ByVal clsSect As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        da = GetStudentDataList(clsSect)
        da.Fill(ds, "ClsSectStds")
        Return ds
    End Function

    Public Function GetStudentDataList(ByVal clsSectId As String, Optional ByVal campusId As String = "") As OleDbDataAdapter
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim da As OleDbDataAdapter

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        With sb
            .Append("SELECT a.StuEnrollId, b.LastName, b.StudentId, ")
            .Append("    Coalesce(Case Len(b.SSN) When 9 then '*******' + SUBSTRING(SSN,6,4) else ' ' end, ' ') SSN, ")
            .Append("b.StudentNumber, b.FirstName, a.GrdSysDetailId, a.IsInComplete, ")
            If myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                .Append(" a.Score as Grade ")
            Else
                .Append("(Select Grade from arGradeSystemDetails where  ")
                .Append("GrdSysDetailId = a.GrdSysDetailId) as Grade ")
            End If
            .Append("FROM arResults a, arStudent b, arStuEnrollments c ")
            .Append(" WHERE a.TestId = ? and a.StuEnrollId = c.StuEnrollId and c.StudentId = b.StudentId  ")
            .Append("ORDER BY b.LastName")
        End With

        ' Add the ClsSectId to the parameter list
        db.AddParameter("@termId", clsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(sb.ToString)
        Finally
            db.CloseConnection()
            db.ClearParameters()
        End Try

        Return da
    End Function
    ''Optional Parameter isAcademicAdvisor added by Saraswathi Lakshmanan
    ''To allow the Academic advisor to post and modify grade

    Public Function GetDataGridInfoOnItmCmd(ByVal StuEnrollId As String, ByVal ClsSectId As String, ByVal Instr As String, ByVal userName As String, Optional ByVal isAcademicAdvisor As Boolean = False) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim da1 As OleDbDataAdapter
        Dim ds As New DataSet
        Dim count As Integer

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("Select a.StuEnrollId, a.InstrGrdBkWgtDetailId, a.Score,a.DateCompleted,a.Comments, b.Descrip, c.LastName, c.FirstName, e.IsInComplete, a.GrdBkResultId from arGrdBkResults a, ")
            .Append("arGrdBkWgtDetails b, arStudent c, arStuEnrollments d, arResults e, arGrdComponentTypes f  where a.InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId and b.GrdComponentTypeId=f.GrdComponentTypeId ")
            .Append(" and a.StuEnrollId = ? and a.StuEnrollId = d.StuEnrollId and d.StudentId = c.StudentId and a.StuEnrollId = e.StuEnrollId  and a.ClsSectionId = e.TestId  ")
            .Append("and a.ClsSectionId = ? ")
            .Append("and f.SysComponentTypeId not in(500,503,544) ")
        End With

        ' Add the StdId and ClsSectId to the parameter list
        db.AddParameter("@StudentId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@clssectionId", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '   Execute the query       
        db.OpenConnection()
        da1 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da1.Fill(ds, "StudentGrdBk")


            sb.Remove(0, sb.Length)
            db.ClearParameters()

            Dim da2 As OleDbDataAdapter

            With sb
                .Append("SELECT distinct a.InstrGrdBkWgtDetailId, a.Descrip, a.Weight, a.Seq  ")
                .Append("FROM arGrdBkWgtDetails a, arClassSections b, arGrdBkWeights c, arGrdComponentTypes d ")
                .Append("WHERE a.InstrGrdBkWgtId = b.InstrGrdBkWgtId  and b.ClsSectionId = ? ")
                .Append("and b.InstructorId=C.InstructorId ")
                .Append("and a.GrdComponentTypeId=d.GrdComponentTypeId ")
                .Append("and d.SysComponentTypeId not in(500,503,544) ")
                If (userName <> "sa" And userName.ToLower <> "support") And isAcademicAdvisor = False Then
                    .Append("AND		c.InstructorId in (select InstructorId from arInstructorsSupervisors where SupervisorId = ? ")
                    .Append("							    union all ")
                    .Append("							    select ? ) ")
                End If
                .Append("ORDER BY a.Seq ")
            End With

            'db.OpenConnection()
            db.AddParameter("@clssectId", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If (userName <> "sa" And userName.ToLower <> "support") And isAcademicAdvisor = False Then
                db.AddParameter("@instr", Instr, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@instr", Instr, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            da2 = db.RunParamSQLDataAdapter(sb.ToString)
            da2.Fill(ds, "GrdBkCriteria")

        Finally
            db.CloseConnection()    'Close Connection
        End Try
        sb.Remove(0, sb.Length)
        Return ds
    End Function

    Public Function InsertStudentGrade(ByVal StdGrdObj As StdGrdBkInfo, ByVal user As String) As String

        Try
            Dim myAdvAppSettings = AdvAppSettings.GetAppSettings()

            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            db.AddParameter("@GrdBkResultId", StdGrdObj.GrdBkResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ClsSectionId", StdGrdObj.ClassId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StdGrdObj.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@InstrGrdBkWgtDetailId", StdGrdObj.GrdBkWgtDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@Score", StdGrdObj.Score, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
            db.AddParameter("@Comments", StdGrdObj.Comments, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ResNum", StdGrdObj.ResNum, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@IsCompGraded", StdGrdObj.IsCompGraded, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@DateCompleted", StdGrdObj.DateCompleted, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery("usp_AR_ArGrdBkResults_Insert", Nothing, "SP")

            db.ClearParameters()
            db.CloseConnection()

        Catch ex As Exception
            Return "Error inserting score - " & ex.Message
        End Try

    End Function
    Public Function UpdateStudentGrade(ByVal StdGrdObj As StdGrdBkInfo, ByVal user As String) As String
        Try
            Dim myAdvAppSettings = AdvAppSettings.GetAppSettings()

            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            db.AddParameter("@ClsSectionId", StdGrdObj.ClassId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StdGrdObj.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@InstrGrdBkWgtDetailId", StdGrdObj.GrdBkWgtDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@Score", StdGrdObj.Score, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
            db.AddParameter("@Comments", StdGrdObj.Comments, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ResNum", StdGrdObj.ResNum, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
            db.AddParameter("@IsCompGraded", StdGrdObj.IsCompGraded, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@DateCompleted", StdGrdObj.DateCompleted, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery("usp_AR_ArGrdBkResults_Update", Nothing, "SP")

            db.ClearParameters()
            db.CloseConnection()

        Catch ex As Exception
            Return "Error updating score - " & ex.Message
        End Try

    End Function
    Public Function UpdateIncompleteGrade(ByVal StdGrdObj As StdGrdBkInfo, ByVal user As String)
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        'Dim ds As DataSet
        Dim clssectid As String
        Dim StuEnrollId As String
        ' Dim GrdBkWgtDetailId As String
        Dim incomplete As Integer
        'Dim Comments As String

        Dim myAdvAppSettings = AdvAppSettings.GetAppSettings()
      
        clssectid = StdGrdObj.ClassId
        StuEnrollId = StdGrdObj.StuEnrollId
        If StdGrdObj.IsIncomplete = 1 Then
            incomplete = 1
        Else
            incomplete = 0
        End If
        'Set the connection string
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("UPDATE arResults Set IsInComplete = ?, ModUser = ?, ModDate =?  ")
            .Append("WHERE  TestId = ? and StuEnrollId = ? ")

        End With
        db.AddParameter("@incomplete", incomplete, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@clsectid", clssectid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@stuenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function
    Public Function UpdateIncompleteGrade(ByVal stuEnrollId As String, ByVal clsSectionId As String, ByVal isIncomplete As Boolean, ByVal user As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Try

            Dim myAdvAppSettings = AdvAppSettings.GetAppSettings()

            'Set the connection string
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            With sb
                .Append("UPDATE arResults ")
                .Append("SET    IsInComplete = ?,ModUser = ?,ModDate =?  ")
                .Append("WHERE  TestId = ? AND StuEnrollId = ?")
            End With
            db.AddParameter("@IsInComplete", isIncomplete, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@TestId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            Return ""
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function DeleteStudentGrade(ByVal StdGrdObj As StdGrdBkInfo) As String
        Try
            Dim myAdvAppSettings = AdvAppSettings.GetAppSettings()

            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            db.AddParameter("@GrdBkResultId", StdGrdObj.GrdBkResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery("USP_AR_ArGrdBkResults_Delete", Nothing, "SP")

            db.ClearParameters()
            db.CloseConnection()

        Catch ex As Exception
            Return "Error deleting score - " & ex.Message
        End Try

    End Function
    Public Function GetGradeScaleSystemDetails(ByVal ClsSectId As String, ByVal empId As String, ByVal termId As String) As DataSet
        Dim db As New DataAccess
        '   create dataset
        Dim ds As New DataSet
        '   build query to select GradeBookResults from a given Teacher
        Dim sb As New StringBuilder
        '   build query to select GradeScales used by teacher in those terms

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        With sb
            .Append("SELECT ")
            .Append("       GrdScaleDetailId, ")
            .Append("       GrdScaleId, ")
            .Append("       MinVal, ")
            .Append("       MaxVal, ")
            .Append("       GrdSysDetailId ")
            .Append("FROM ")
            .Append("       arGradeScaleDetails ")
            .Append("WHERE ")
            ' .Append("       GrdScaleId In (Select GrdScaleId from arClassSections where TermId = ? and ClsSectionId = ?) ")
            .Append("       GrdScaleId In (Select GrdScaleId from arClassSections where ClsSectionId = ?) ")
        End With

        '   build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
        '   add EmpId parameter
        'sc.Parameters.Add(New OleDbParameter("EmpId", empId))

        ''   add TermId parameter   
        'sc.Parameters.Add(New OleDbParameter("TermId", termId))

        '   add TermId parameter   
        sc.Parameters.Add(New OleDbParameter("ClsSectionId", ClsSectId))

        '   Create adapter to handle GradeSystems table
        Dim da As New OleDbDataAdapter(sc)

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString
        'da.SelectCommand.Parameters.Clear()
        'da.SelectCommand.Parameters.Add(New OleDbParameter("EmpId", empId))
        'da.SelectCommand.Parameters.Add(New OleDbParameter("TermId", termId))

        '   fill ClassSectionsStudents table
        da.Fill(ds, "GradeScaleDetails")

        '   build query to select GradeSystemDetails used by teacher in those terms
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       GrdSysDetailId, ")
            .Append("       Grade ")
            .Append("FROM ")
            .Append("       arGradeSystemDetails ")
            .Append("WHERE ")
            '.Append("       GrdSystemId In (Select GrdSystemId from arGradeScales where GrdScaleId in (select GrdScaleId from arClassSections where TermId = ? and ClsSectionId = ?)) ")
            .Append("       GrdSystemId In (Select GrdSystemId from arGradeScales where GrdScaleId in (select GrdScaleId from arClassSections where ClsSectionId = ?)) ")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString
        'da.SelectCommand.Parameters.Clear()
        'da.SelectCommand.Parameters.Add(New OleDbParameter("EmpId", empId))
        'da.SelectCommand.Parameters.Add(New OleDbParameter("TermId", termId))

        '   fill GradeSystemDetails table
        da.Fill(ds, "GradeSystemDetails")

        '   create primary and foreign key constraints


        '   set primary key for GradeSystemDetails table
        Dim pk3(0) As DataColumn
        pk3(0) = ds.Tables("GradeSystemDetails").Columns("GrdSysDetailId")
        ds.Tables("GradeSystemDetails").PrimaryKey = pk3


        '   set foreign key column in GradeBookResults
        Dim fk3(0) As DataColumn
        fk3(0) = ds.Tables("GradeScaleDetails").Columns("GrdSysDetailId")

        '   set foreign keys in GradeBookResults table
        ds.Relations.Add("GradeSystemDetailsGradeScaleDetails", pk3, fk3)

        '   return dataset
        Return ds
    End Function
    Public Function GetGradeScaleId(ByVal ClsSectId As String) As DataSet
        Dim ds As New DataSet
        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            '   build the sql query
            With sb
                .Append("SELECT t1.GrdScaleId ")
                .Append("FROM arClassSections t1 ")
                .Append("WHERE t1.ClsSectionId = ? ")

            End With
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ClsSectId", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function boolIsScorePosted(ByVal ClsSectId As String, ByVal StuEnrollId As String) As Boolean
        Dim ds As New DataSet
        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            Dim intRowCount As Integer = 0
            '   build the sql query
            With sb
                '.Append("SELECT Count(*) ")
                '.Append("FROM arGrdBkResults t1 ")
                '.Append("WHERE t1.ClsSectionId = ? and t1.StuEnrollId=? and Score is not NULL")

                'Look if score is posted for any grade book components apart from 
                'Lab work,Lab Hours and Externship
                .Append("   Select Distinct Count(*) ")
                .Append("   from	arGrdBkResults GBR, arGrdComponentTypes GCT, arGrdBkWgtDetails GBWD   ")
                .Append("   where 	GBR.ClsSectionId=? and GBR.StuEnrollId=? ")
                .Append("   and	GBR.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
                .Append("   and	GBWD.GrdComponentTypeId = GCT.GrdComponentTypeId  and Score is NOT NULL ")
                .Append("   and	GCT.SysComponentTypeId Not in (500,503,544)   ")
            End With
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ClsSectId", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            Try
                intRowCount = db.RunParamSQLScalar(sb.ToString)
            Catch ex As Exception
                intRowCount = 0
            Finally
                db.ClearParameters()
                sb.Remove(0, sb.Length)
                db.CloseConnection()
            End Try
            If intRowCount >= 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try
    End Function
    Public Function GetGrdScaleScoreLimit(ByVal StuEnrollID As String) As String

        'connect to the database
        Dim db As New DataAccess
        Dim strMaxScore, strMinScore As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT  arGradeScaleDetails.GrdScaleId, MIN(arGradeScaleDetails.MinVal) AS MinScore, MAX(arGradeScaleDetails.MaxVal) AS MaxScore ")
            .Append("FROM  arPrgVersions, arStuEnrollments,arGradeScales, arGradeScaleDetails ")
            .Append("WHERE  arPrgVersions.PrgVerId =  arStuEnrollments.PrgVerId ")
            .Append("AND arGradeScaleDetails.GrdScaleId =  arGradeScales.GrdScaleId  ")
            .Append("AND arPrgVersions.ThGrdScaleId =  arGradeScales.GrdScaleId ")
            .Append("AND arStuEnrollments.StuEnrollId = ? ")
            .Append("GROUP BY arGradeScaleDetails.GrdScaleId ")

        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@LeadId", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)


        While dr.Read()

            'set properties with data from DataReader
            strMaxScore = dr("MaxScore").ToString()
            strMinScore = dr("MinScore").ToString()

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return strMaxScore & " " & strMinScore
    End Function
    Public Function GetDefaultGrade(ByVal StuEnrollId As String) As String
        Dim da As OleDbDataAdapter
        Dim sGrade As String

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            With sb
                .Append("Select arGradeSystemDetails.Grade ")
                .Append("FROM arStuEnrollments, arPrgVersions, arGradeSystems, arGradeSystemDetails ")
                .Append("WHERE arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId ")
                .Append("AND  arPrgVersions.GrdSystemId =  arGradeSystems.GrdSystemId ")
                .Append("AND  arGradeSystems.GrdSystemId =  arGradeSystemDetails.GrdSystemId ")
                .Append("AND arStuEnrollments.StuEnrollId = ? ")
                .Append("AND arGradeSystemDetails.IsDefault = 1 ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   build the sql query

            '   Execute the query
            db.OpenConnection()

            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                sGrade = dr("Grade")
            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return sGrade

    End Function
    Public Function GetGradeBookWeightingLevel(ByVal clsSectionId As String, ByVal stuEnrollId As String) As String
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Dim rtn As String = MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString()


        'db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

        ''build the sql query
        'Dim sb As New StringBuilder
        'With sb
        '    'With subqueries
        '    .Append("select distinct ReqId from arGrdBkResults a,arGrdBkWgtDetails b,arGrdBkWeights c ")
        '    .Append(" where a.InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId And b.InstrGrdBkWgtId = c.InstrGrdBkWgtId ")
        '    .Append(" and a.ClsSectionId = ? and a.StuEnrollId = ? ")
        'End With

        ''Add the EmployerContactId the parameter list
        'db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'db.OpenConnection()
        'Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        'While dr.Read()
        '    'set properties with data from DataReader
        '    If Not (dr("ReqId") Is System.DBNull.Value) Then
        '        rtn = "CourseLevel"
        '    Else
        '        rtn = "InstructorLevel"
        '    End If
        'End While
        'db.CloseConnection()
        Return rtn
    End Function

    ''Modified by Saraswathi lakshmanan
    ''on march 30 2009
    ''To find if the School is a course level or Instructor level
    ''if the count of the Reqid in arGrdBkWeights is greater than 0 then it is a Course level School
    ''Else if it is zero it is a instructor Level School


    Public Function GetGradeBookWeightingLevel(ByVal clsSectionId As String) As String
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Dim rtn As String = MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString()


        'db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        ''build the sql query
        'Dim sb As New StringBuilder
        'With sb
        '    'With subqueries
        '    ''Modified by saraswathi to fix issue related to sql swerver 2000
        '    ''Modified on april 28 2008
        '    .Append(" select count(*) from ( ")
        '    .Append("select Reqid from arGrdBkWeights   ")
        '    .Append(" where ReqId in(Select ReqId from arClassSections where ClsSectionid= ?) ")
        '    .Append(" group by ReqId ")
        '    .Append("  ) GradeWeights  ")


        '    ''.Append(" Select Count(Reqid) from arGrdBkWeights where ReqId in( ")
        '    ''.Append(" Select ReqId from arClassSections where ClsSectionid= ?) ")
        '    ''.Append("select distinct ReqId from arGrdBkResults a,arGrdBkWgtDetails b,arGrdBkWeights c ")
        '    ''.Append(" where a.InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId And b.InstrGrdBkWgtId = c.InstrGrdBkWgtId ")
        '    ''.Append(" and a.ClsSectionId = ? ")
        'End With

        ''Add the EmployerContactId the parameter list
        'db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ''db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'db.OpenConnection()
        'Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        'While dr.Read()
        '    'set properties with data from DataReader
        '    If dr(0) > 0 Then
        '        rtn = "CourseLevel"
        '    Else
        '        rtn = "InstructorLevel"
        '    End If
        'End While
        'db.CloseConnection()
        Return rtn
    End Function
    Public Function GetGradeBookWeightingLevel_SP(ByVal clsSectionId As String) As String
        Dim ds As DataSet
        Dim db As New SQLDataAccess
        Dim dr As SqlDataReader

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Dim rtn As String = MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString()
        'db.ConnectionString = SingletonAppSettings.AppSettings("ConnectionString")


        'db.AddParameter("@clsSectionId", New Guid(clsSectionId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)


        'Try
        '    dr = db.RunParamSQLDataReader_SP("dbo.usp_GetGradeBookWeightingLevel")
        '    While dr.Read()
        '        'set properties with data from DataReader
        '        If dr(0) > 0 Then
        '            rtn = "CourseLevel"
        '        Else
        '            rtn = "InstructorLevel"
        '        End If
        '    End While
        'Catch ex As Exception
        'Finally
        '    db.CloseConnection()
        'End Try
        Return rtn
    End Function
    Public Function GetStdGrdBkPopupInfo(ByVal ClsSectId As String, ByVal StudentId As String) As String

        'connect to the database
        Dim db As New DataAccess
        Dim strInstructor, strCourse, strTerm As String
        Dim strFirstName, strLastName, strMiddleName As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("Select a.*, b.Descrip from arClassSections a, arReqs b where a.ClsSectionId = ? and a.ReqId = b.ReqId ")

        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@StudentId", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim Extracurricularinfo As New PlacementInfo

        While dr.Read()
            'set properties with data from DataReader
            strInstructor = dr("InstructorId").ToString()
            strCourse = dr("Descrip") & " - " & dr("ClsSection")
            strTerm = dr("TermId").ToString
        End While

        db.ClearParameters()
        sb.Remove(0, sb.Length)

        With sb
            'With subqueries
            .Append("SELECT FirstName,LastName,MiddleName from arStudent where StudentId = ? ")

        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        dr = db.RunParamSQLDataReader(sb.ToString)


        While dr.Read()

            'set properties with data from DataReader
            strFirstName = dr("FirstName").ToString()
            strLastName = dr("LastName").ToString()
            strMiddleName = dr("MiddleName").ToString()

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return strFirstName & ";" & strLastName & ";" & strMiddleName & ";" & strInstructor & ";" & strCourse & ";" & strTerm
    End Function

    Public Function GetStdGrdBkPopupInfo(ByVal clsSectId As String, ByVal studentId As String, _
                                            ByVal stuEnrollId As String) As String
        Dim db As New DataAccess
        Dim strInstructor, strCourse, strTerm As String
        Dim strFirstName, strLastName, strMiddleName As String
        Dim sbRes As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            'Class section info
            .Append("SELECT a.*, b.Descrip ")
            .Append("FROM arClassSections a, arReqs b ")
            .Append("WHERE a.ClsSectionId=? AND a.ReqId=b.ReqId ")
        End With
        db.AddParameter("@StudentId", clsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read()
            With sbRes
                .Append(dr("InstructorId").ToString())
                .Append(";")
                .Append(dr("Descrip") & " - " & dr("ClsSection"))
                .Append(";")
                .Append(dr("TermId").ToString)
            End With
        End While


        db.ClearParameters()
        sb.Remove(0, sb.Length)


        With sb
            'Student name
            .Append("SELECT FirstName,LastName,MiddleName ")
            .Append("FROM arStudent ")
            .Append("WHERE StudentId=? ")
        End With
        db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        dr = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read()
            With sbRes
                If .Length > 0 Then
                    .Append(";")
                End If
                .Append(dr("FirstName").ToString())
                .Append(";")
                .Append(dr("LastName").ToString())
                .Append(";")
                .Append(dr("MiddleName").ToString())
            End With
        End While


        db.ClearParameters()
        sb.Remove(0, sb.Length)


        With sb
            'IsIncomple
            .Append("SELECT IsInComplete=CASE WHEN (IsInComplete=1) THEN 'True' ELSE 'False' END ")
            .Append("FROM arResults ")
            .Append("WHERE StuEnrollId=? AND TestId=?")
        End With
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@TestId", clsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        dr = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read()
            With sbRes
                If .Length > 0 Then
                    .Append(";")
                End If
                .Append(dr("IsInComplete").ToString())
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return sbRes.ToString
    End Function


    Public Function GetGrdBkWgtDetailsAndResultsDS(ByVal clsSectionId As String, ByVal stuEnrollId As String) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        db.OpenConnection()

        '   build query to select all Grade Component Types, Weight and MinNumber 
        '   for this particular course
        With sb
            .Append("SELECT ")
            .Append("       C.InstrGrdBkWgtDetailId,")
            .Append("       D.Code,")
            .Append("       D.Descrip,")
            .Append("      IsNull( C.Weight,0)as Weight,")
            .Append("       C.Number AS MinNumber,")
            .Append("       C.GrdPolicyId,")
            .Append("       C.Parameter AS Param,")
            .Append("       X.GrdScaleId ")
            '.Append("FROM   (SELECT TOP 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId ")
            .Append("FROM   (SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId ")
            .Append("       FROM          arGrdBkWeights A,arClassSections B ")
            .Append("       WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=? ")
            .Append(" ORDER BY      A.EffectiveDate DESC ")
            '.Append("       ORDER BY      A.EffectiveDate DESC) X,arGrdBkWgtDetails C,arGrdComponentTypes D ")
            .Append("       ) X,arGrdBkWgtDetails C,arGrdComponentTypes D ")
            .Append("WHERE  X.InstrGrdBkWgtId=C.InstrGrdBkWgtId")
            'Commented by Balaji on 04/17/2009 to fix issue 13924
            'modification starts here
            '.Append("       AND C.Weight>0")
            'modification ends here
            .Append("       AND C.GrdComponentTypeId=D.GrdComponentTypeId ")
            'code added by balaji,clinic services should not be displayed in Post Grade By Student (Course Level)
            'code modified by Troy for DE9754:QA: Externship type components should not be shown on the Post grade pages under the Faculty module.
            .Append(" and D.SysComponentTypeID not in (500,503,544) ")
            .Append("ORDER BY C.Seq")
        End With

        'Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim da As OleDbDataAdapter = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "GrdComponentTypesDT")
        Catch ex As Exception

        End Try

        db.ClearParameters()
        sb.Remove(0, sb.Length)


        '   build query to retrieve results per grade component type
        '   for this particular student and course
        With sb
            .Append("SELECT")
            .Append("       GrdBkResultId,")
            .Append("       ClsSectionId,")
            .Append("       InstrGrdBkWgtDetailId,")
            .Append("       Score,")
            .Append("       Comments,")
            .Append("       StuEnrollId,")
            .Append("       ModUser,")
            .Append("       ModDate,")
            .Append("       DateCompleted,")
            .Append("       ResNum,IsCompGraded,PostDate ")
            .Append("FROM   arGrdBkResults ")
            .Append("WHERE  StuEnrollId=? AND ClsSectionId=?")
        End With

        'Add parameters to parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim da1 As OleDbDataAdapter = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da1.Fill(ds, "StuGrdBkResultsDT")
        Catch ex As Exception

        End Try

        'Close connection
        db.CloseConnection()

        Return ds
    End Function

    Public Function GetGrdBkWgtDetailsAndResultsGradedDS(ByVal clsSectionId As String, ByVal stuEnrollId As String) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        db.OpenConnection()

        '   build query to select all Grade Component Types, Weight and MinNumber 
        '   for this particular course
        With sb
            .Append("SELECT ")
            .Append("       C.InstrGrdBkWgtDetailId,")
            .Append("       D.Code,")
            .Append("       D.Descrip,")
            .Append("       C.Weight,")
            .Append("       C.Number AS MinNumber,")
            .Append("       C.GrdPolicyId,")
            .Append("       C.Parameter AS Param,")
            .Append("       X.GrdScaleId ")
            '.Append("FROM   (SELECT TOP 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId ")
            .Append("FROM   (SELECT Distinct A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId ")
            .Append("       FROM          arGrdBkWeights A,arClassSections B ")
            .Append("       WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=? ")
            '.Append("       ORDER BY      A.EffectiveDate DESC) X,arGrdBkWgtDetails C,arGrdComponentTypes D ")
            .Append("       ) X,arGrdBkWgtDetails C,arGrdComponentTypes D ")
            .Append("WHERE  X.InstrGrdBkWgtId=C.InstrGrdBkWgtId")
            .Append("       AND C.Weight>0")
            .Append("       AND C.GrdComponentTypeId=D.GrdComponentTypeId ")
            .Append("ORDER BY C.Seq")
        End With

        'Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim da As OleDbDataAdapter = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "GrdComponentTypesDT")
        Catch ex As Exception

        End Try

        db.ClearParameters()
        sb.Remove(0, sb.Length)


        '   build query to retrieve results per grade component type
        '   for this particular student and course
        With sb
            .Append("SELECT")
            .Append("       GrdBkResultId,")
            .Append("       ClsSectionId,")
            .Append("       InstrGrdBkWgtDetailId,")
            .Append("       Score,")
            .Append("       Comments,")
            .Append("       StuEnrollId,")
            .Append("       ModUser,")
            .Append("       ModDate,")
            .Append("       ResNum,IsCompGraded ")
            .Append("FROM   arGrdBkResults ")
            .Append("WHERE  StuEnrollId=? AND ClsSectionId=?")
        End With

        'Add parameters to parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim da1 As OleDbDataAdapter = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da1.Fill(ds, "StuGrdBkResultsDT")
        Catch ex As Exception

        End Try

        'Close connection
        db.CloseConnection()

        Return ds
    End Function

    Public Function GetGradeBookComponentsAndEffectiveDatesDS() As DataSet

        '   create dataset
        Dim ds As New DataSet
        Dim strEnrollDate As Date = Date.Now.ToShortDateString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        '   build the sql query for the ReqGroups data adapter
        Dim sb As New StringBuilder
        With sb
            .Append(" select t1.*,t2.StatusId,(CASE t2.Status WHEN 'Active' THEN 1 ELSE 0 END) As Status from arGrdComponentTypes t1,syStatuses t2 where  ")
            .Append(" t1.StatusId = t2.StatusId ")
        End With

        '   build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

        '   Create adapter 
        Dim da As New OleDbDataAdapter(sc)

        '   Fill Requirements Table table
        da.Fill(ds, "GradeBookComponents")

        '   build the sql query for the Effective Dates
        sb = New StringBuilder
        With sb
            .Append(" Select * from arCreditsPerService ")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   Fill LeadGroups table
        da.Fill(ds, "GradeBookComponentsByEffectiveDates")

        '   set primary key for Requirements table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("GradeBookComponents").Columns("GrdComponentTypeId")
        ds.Tables("GradeBookComponents").PrimaryKey = pk0

        '   set foreign key column in LeadGrpReqGrps
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("GradeBookComponentsByEffectiveDates").Columns("GrdComponentTypeId")

        '   add relationship
        ds.Relations.Add("GrdComponentsAndEffectiveDates", pk0, fk0)

        '   return dataset
        Return ds

    End Function
    Public Function UpdateGradeBookComponentsandEffectiveDatesDS(ByVal ds As DataSet) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        '   all updates must be encapsulated as one transaction
        Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            '   build the sql query for the adLeadGrpReqGroups data adapter
            Dim sb As New StringBuilder
            With sb
                .Append(" Select * from arCreditsPerService ")
            End With

            '   build select command
            Dim LeadGrpReqGroupsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle LeadGrpReqGroups table
            Dim LeadGrpReqGroupsDataAdapter As New OleDbDataAdapter(LeadGrpReqGroupsSelectCommand)

            '   build insert, update and delete commands for LeadGrpReqGroups table
            Dim cb As New OleDbCommandBuilder(LeadGrpReqGroupsDataAdapter)

            '   build select query for the ReqGroups data adapter
            sb = New StringBuilder
            With sb
                '   with subqueries
                .Append(" select * from arGrdComponentTypes ")
            End With

            '   build select command
            Dim ReqGroupsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle StudentAwards table
            Dim ReqGroupsDataAdapter As New OleDbDataAdapter(ReqGroupsSelectCommand)

            '   build insert, update and delete commands for ReqGroups table
            Dim cb1 As New OleDbCommandBuilder(ReqGroupsDataAdapter)

            '   insert added rows in adReqs table
            ReqGroupsDataAdapter.Update(ds.Tables("GradeBookComponents").Select(Nothing, Nothing, DataViewRowState.Added))

            '   insert added rows in adReqEffectiveDates table
            LeadGrpReqGroupsDataAdapter.Update(ds.Tables("GradeBookComponentsByEffectiveDates").Select(Nothing, Nothing, DataViewRowState.Added))

            '   delete rows in ReqGroups table
            LeadGrpReqGroupsDataAdapter.Update(ds.Tables("GradeBookComponentsByEffectiveDates").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   delete rows in LeadGrpReqGrps table
            ReqGroupsDataAdapter.Update(ds.Tables("GradeBookComponents").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   update rows in ReqGroups table
            LeadGrpReqGroupsDataAdapter.Update(ds.Tables("GradeBookComponentsByEffectiveDates").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   update rows in LeadGrpReqGrps table
            ReqGroupsDataAdapter.Update(ds.Tables("GradeBookComponents").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))


            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            Return ""

        Catch ex As OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message`
            Return DALExceptions.BuildErrorMessage(ex)

        Catch ex As Exception
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return ex.Message

        Finally

            '   close connection
            connection.Close()
        End Try

    End Function
    Public Function GetSysGradeBookComponents() As DataTable
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        sb.Append("SELECT " + vbCrLf)
        sb.Append("     R.ResourceId as ID, " + vbCrLf)
        sb.Append("     R.Resource as Descrip " + vbCrLf)
        sb.Append("FROM " + vbCrLf)
        sb.Append("     syResources R " + vbCrLf)
        sb.Append("WHERE " + vbCrLf)
        sb.Append("     R.ResourceTypeId = 10 " + vbCrLf) ' 10 is the resourcetype for System GradeBook Components
        sb.Append("ORDER BY " + vbCrLf)
        sb.Append("     R.Resource ")
        Return db.RunSQLDataSet(sb.ToString).Tables(0)
    End Function

    Public Function GetClsSectsbyUserandRoles(ByVal Term As String, ByVal userId As String, ByVal CampusId As String, ByVal ShiftId As String, ByVal MinimumClassStartDate As Date, ByVal UserName As String, ByVal isAcademicAdvisor As Boolean) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            'US3580 
            With sb
                .Append("SELECT DISTINCT " & vbCrLf)
                .Append("a.ClsSectionId, " & vbCrLf)
                .Append("c.ReqId, " & vbCrLf)
                .Append("c.ClsSection, " & vbCrLf)
                .Append("'(' + b.code + ') ' + b.Descrip as Descrip, b.code, b.Descrip AS ShortDesc, c.InstrGrdBkWgtId  " & vbCrLf)
                .Append("FROM   arClassSectionTerms a,arReqs b, arClassSections c " & vbCrLf)
                .Append("WHERE  a.TermId=? " & vbCrLf)
                .Append("AND a.ClsSectionId = c.ClsSectionId  " & vbCrLf)
                .Append(" AND c.ReqId=b.ReqId " & vbCrLf)
                If Not ShiftId = "00000000-0000-0000-0000-000000000000" Then
                    .Append(" AND c.ShiftId=? " & vbCrLf)
                End If

                If Not MinimumClassStartDate = Nothing Then
                    .Append(" AND c.StartDate >= ? " & vbCrLf)
                End If
                'Code modified to fix issue 15545
                'The class section dropdown in Post Grade By Student should not show Externship classes
                'modification starts here
                '.Append(" and b.IsExternship <> 1 ")
                'modification ends here 
                If UserName <> "sa" And UserName.ToLower <> "support" And isAcademicAdvisor = False Then

                    .Append(" AND (c.InstructorId in (Select InstructorId " & vbCrLf)
                    .Append(" from arInstructorsSupervisors where" & vbCrLf)
                    .Append(" SupervisorId= ? )" & vbCrLf)
                    .Append(" OR c.InstructorId= ? )" & vbCrLf)

                End If

                .Append("AND c.CampusId=? " & vbCrLf)
                .Append("AND c.StartDate<=?  " & vbCrLf)
                .Append("ORDER BY b.Descrip, b.code " & vbCrLf)
            End With

            db.AddParameter("@TermId", Term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Not ShiftId = "00000000-0000-0000-0000-000000000000" Then
                db.AddParameter("@ShiftId", ShiftId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            If Not MinimumClassStartDate = Nothing Then
                db.AddParameter("@MinimumClassStartDate", MinimumClassStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            If UserName <> "sa" And UserName.ToLower <> "support" And isAcademicAdvisor = False Then

                db.AddParameter("@userId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@userId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            db.AddParameter("@cmpid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Sdate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "TermClsSects")

            Try
                Dim dtCourses As DataTable = ds.Tables(0)
                Dim dtTransferGrade As New TransferGradeDB
                If dtCourses.Rows.Count > 0 Then
                    For Each row As DataRow In dtCourses.Rows
                        If (dtTransferGrade.IsCourseALab(row("ReqId").ToString, row("InstrGrdBkWgtId").ToString) = True) Then
                            row.Delete()
                        End If
                    Next
                End If
                ds.AcceptChanges()
            Catch ex As Exception
            End Try
        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        Return ds
    End Function

    public Function IsCourseALab(ByVal reqid As String, ByVal instrGrdBkWgtId As String) As Boolean
        Dim dtTransferGrade As New TransferGradeDB
        return (dtTransferGrade.IsCourseALab(reqid, instrGrdBkWgtId))
    End Function

    Public Function GetClsSectsbyUserandRoles(ByVal Term As String, ByVal userId As String, ByVal CampusId As String, ByVal UserName As String, ByVal isAcademicAdvisor As Boolean) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            'US3580 
            With sb
                .Append("SELECT DISTINCT ")
                .Append("a.ClsSectionId, ")
                .Append("c.ReqId, ")
                .Append("c.ClsSection, ")
                .Append("'(' + b.code + ') ' + b.Descrip as Descrip, b.code, b.Descrip AS ShortDesc, c.InstrGrdBkWgtId  ")
                .Append("FROM   arClassSectionTerms a,arReqs b, arClassSections c ")
                .Append("WHERE  a.TermId=? ")
                .Append("AND a.ClsSectionId = c.ClsSectionId  ")
                .Append(" AND c.ReqId=b.ReqId ")
                'Code modified to fix issue 15545
                'The class section dropdown in Post Grade By Student should not show Externship classes
                'modification starts here
                '.Append(" and b.IsExternship <> 1 ")
                'modification ends here 
                If UserName <> "sa" And UserName.ToLower <> "support" And isAcademicAdvisor = False Then

                    .Append(" AND (c.InstructorId in (Select InstructorId ")
                    .Append(" from arInstructorsSupervisors where")
                    .Append(" SupervisorId= ? )")
                    .Append(" OR c.InstructorId= ? )")

                End If

                .Append("AND c.CampusId=? ")
                .Append("AND c.StartDate<=?  ")
                .Append("ORDER BY b.Descrip, b.code ")
            End With

            db.AddParameter("@TermId", Term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If UserName <> "sa" And UserName.ToLower <> "support" And isAcademicAdvisor = False Then

                db.AddParameter("@userId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@userId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            db.AddParameter("@cmpid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Sdate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "TermClsSects")

            Try
                Dim dtCourses As DataTable = ds.Tables(0)
                Dim dtTransferGrade As New TransferGradeDB
                If dtCourses.Rows.Count > 0 Then
                    For Each row As DataRow In dtCourses.Rows
                        If (dtTransferGrade.IsCourseALab(row("ReqId").ToString, row("InstrGrdBkWgtId").ToString) = True) Then
                            row.Delete()
                        End If
                    Next
                End If
                ds.AcceptChanges()
            Catch ex As Exception
            End Try
        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        Return ds
    End Function
    ''Added by Saraswathi lakshmanan on Oct- 3- 2008
    ''To select the Class Sections based on CohortStart Date

    Public Function GetClsSectsbyUserandRolesandCohortStDt(ByVal CohortStDate As String, ByVal userId As String, ByVal CampusId As String, ByVal UserName As String, ByVal isAcademicAdvisor As Boolean) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT DISTINCT ")
                .Append("a.ClsSectionId, ")
                .Append("c.ReqId, ")
                .Append("c.ClsSection, ")
                .Append("b.Descrip ")
                .Append("FROM   arClassSectionTerms a,arReqs b, arClassSections c ")
                .Append("WHERE  c.CohortStartDate= ? ")
                .Append("AND a.ClsSectionId = c.ClsSectionId  ")
                .Append(" AND c.ReqId=b.ReqId ")
                If UserName <> "sa" And isAcademicAdvisor = False Then

                    .Append(" AND (c.InstructorId in (Select InstructorId ")
                    .Append(" from arInstructorsSupervisors where")
                    .Append(" SupervisorId= ? )")
                    .Append(" OR c.InstructorId= ? )")

                End If

                .Append("AND c.CampusId=? ")
                .Append("AND c.StartDate<=?  ")
                .Append("ORDER BY b.Descrip ")
            End With

            db.AddParameter("@CohortStDate", CohortStDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If UserName <> "sa" And isAcademicAdvisor = False Then

                db.AddParameter("@userId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@userId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            db.AddParameter("@cmpid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Sdate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "TermClsSects")

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        Return ds
    End Function

    Public Function GetCohortStartDate(ByVal UserName As String, ByVal Instr As String, ByVal isAcademicAdvisor As Boolean, ByVal CampusId As String) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                ''
                .Append("Select distinct Convert(varchar(10),CohortStartDate,101)as CohortStartDate ")
                .Append(" from arClassSections CS where (EndDate>CohortStartDate)  and ")
                .Append("((EndDate>GetDate()) or (EndDate>Dateadd(month,-2,Getdate())))  ")
                If UserName <> "sa" And isAcademicAdvisor = False Then

                    .Append(" AND (CS.InstructorId in (Select InstructorId ")
                    .Append(" from arInstructorsSupervisors where")
                    .Append(" SupervisorId= ? )")
                    .Append(" OR CS.InstructorId= ? )")

                End If

                .Append(" AND CS.CampusId=? ")
                .Append(" order by  CohortStartDate ")

            End With
            If UserName <> "sa" And isAcademicAdvisor = False Then

                db.AddParameter("@userId", Instr, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@userId", Instr, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            db.AddParameter("@cmpid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "CohortStDate")

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        Return ds
    End Function

    Public Function GetCohortStartDatebyPrgVersion(ByVal PrgVersion As String) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                ''
                .Append("Select distinct Convert(varchar(10),CohortStartDate,101)as CohortStartDate,year(CohortStartDate) ")
                .Append(" from arStuEnrollments where    PrgVerId= ? ")
                .Append(" order by  year(CohortStartDate) ")

            End With
            db.AddParameter("@PrgVersion", PrgVersion, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "CohortStDate")

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        Return ds
    End Function
End Class
