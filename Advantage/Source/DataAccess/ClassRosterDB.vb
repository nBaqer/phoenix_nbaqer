Imports FAME.Advantage.Common


Public Class ClassRosterDB

#Region "Private Data Members"

    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")

#End Region


#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

#End Region


#Region "Public Methods"

    Public Function GetClassRoster(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            If paramInfo.ResId = 551 Then
                strWhere &= " AND '" & paramInfo.FilterOtherString.Replace("Start Date Range Equal To ", "") & "' between arClassSections.StartDate and arClassSections.EndDate "
            Else
                strWhere &= " AND " & paramInfo.FilterOther
            End If

        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        With sb
            .Append("SELECT ")
            .Append("       arClassSections.ClsSectionId,arClassSections.TermId,arTerm.TermDescrip,")
            .Append("       arClassSections.ReqId,arReqs.Descrip,arReqs.Code,")
            .Append("       arReqs.Credits,arClassSections.ClsSection,")
            .Append("       arClassSections.InstructorId,(SELECT FullName FROM syUsers WHERE UserId=arClassSections.InstructorId) AS InstructorName,")
            .Append("       arClassSections.StartDate,arClassSections.EndDate,arClassSections.MaxStud,")
            .Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,arClassSections.CampusId,")
            .Append("       (SELECT CampDescrip FROM syCampuses WHERE CampusId=arClassSections.CampusId) AS CampusDescrip,")
            .Append("       (SELECT COUNT(DISTINCT e.StudentId) FROM arResults r INNER JOIN dbo.arStuEnrollments e ON r.StuEnrollId=e.StuEnrollId ")
            .Append("       INNER JOIN dbo.syStatusCodes sc ON e.StatusCodeId=sc.StatusCodeId INNER JOIN dbo.sySysStatus s ON sc.SysStatusId= s.SysStatusId ")
            .Append("       WHERE r.TestId = arClassSections.ClsSectionId AND s.SysStatusId IN (6,7,9,20,22) ")
            .Append("       AND (SELECT CASE ")
            .Append("       WHEN r.GrdSysDetailId IS NULL THEN 1 ")
            .Append("       WHEN (r.GrdSysDetailId IS NOT NULL AND (SELECT TOP 1 isDrop FROM arGradeSystemDetails WHERE GrdSysDetailId=r.GrdSysDetailId) = 1) THEN 0 ")
            .Append("       ELSE 1 END) <> 0 ) AS EnrollmentCount ")
            .Append("       FROM   arClassSections,arClassSectionTerms,arTerm,arReqs,syCampGrps,syCmpGrpCmps F ")
            .Append("       WHERE  arClassSectionTerms.ClsSectionId=arClassSections.ClsSectionId")
            .Append("       AND arClassSectionTerms.TermId=arTerm.TermId")
            .Append("       AND arClassSections.ReqId=arReqs.ReqId")
            .Append("       AND syCampGrps.CampGrpId=F.CampGrpId")
            .Append("       AND arClassSections.CampusId=F.CampusId")
            .Append(strWhere)
            .Append(" ORDER BY syCampGrps.CampGrpDescrip,CampusDescrip,arTerm.TermDescrip")
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            'Add new column
            ds.Tables(0).Columns.Add(New DataColumn("ClsSectionStr", System.Type.GetType("System.String")))
            ds.Tables(0).TableName = "ClassRosterMain"

            ds.Merge(GetStuInSection(strWhere))
            ds.Tables(1).TableName = "ClassRosterSub"
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Public Function GetStuInSection(ByVal WhereClause As String, Optional ByVal Campusid As String = "") As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim stuId As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If StudentIdentifier = "EnrollmentId" Then
            stuId = "B.EnrollmentId"
        ElseIf StudentIdentifier = "StudentId" Then
            stuId = "C.StudentNumber"
        Else
            'If StudentIdentifier = "SSN" Then
            stuId = "C.SSN"
        End If

        With sb
            'query with subqueries
            .Append("SELECT DISTINCT ")
            .Append("       A.TestId,")
            .Append("       C.LastName,C.FirstName,C.MiddleName," & stuId & " AS StudentIdentifier,")
            .Append(" dbo.GetProgramVersions(C.StudentId, A.TestId) AS PrgVerDescrip, ")
            .Append("       (SELECT Grade FROM arGradeSystemDetails GSD WHERE GSD.GrdSysDetailId=A.GrdSysDetailId) AS Grade, ")
            .Append("       A.GrdSysDetailId,'")
            .Append(MyAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower & "' as GradeType,")
            .Append("       A.score as Score, ")
            '.Append("       (SELECT Grade FROM arGradeSystemDetails WHERE GrdSysDetailId=A.GrdSysDetailId) AS GradeDescrip,")
            .Append("       B.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes k WHERE B.StatusCodeId=k.StatusCodeId ) AS StatusCodeDescrip ")
            .Append("FROM    arResults A, arStuEnrollments B,arStudent C,arPrgVersions D ")
            .Append("WHERE  A.StuEnrollId=B.StuEnrollId ")
            .Append("       AND B.StudentId=C.StudentId AND B.PrgVerId=D.PrgVerId ")
            .Append("       AND (SELECT CASE ")
            .Append("	    WHEN A.GrdSysDetailId IS NULL THEN 1 ")
            .Append("	    WHEN (A.GrdSysDetailId IS NOT NULL AND (SELECT TOP 1 isDrop FROM arGradeSystemDetails WHERE GrdSysDetailId=A.GrdSysDetailId) = 1) THEN 0 ")
            .Append("	    ELSE 1 END) <> 0 ")
            .Append("       AND EXISTS  (SELECT arClassSections.ClsSectionId ")
            .Append("                    FROM   arClassSections,arClassSectionTerms,arTerm,arReqs,syCampGrps,syCmpGrpCmps F ")
            .Append("                    WHERE  A.TestId=arClassSections.ClsSectionId ")
            .Append("                           AND arClassSectionTerms.ClsSectionId=arClassSections.ClsSectionId")
            .Append("                           AND arClassSectionTerms.TermId=arTerm.TermId")
            .Append("                           AND arClassSections.ReqId=arReqs.ReqId")
            .Append(WhereClause)
            .Append("                           AND syCampGrps.CampGrpId=F.CampGrpId")
            .Append("                           AND arClassSections.CampusId=F.CampusId) ")
            .Append(" AND  B.StatusCodeID in (SELECT k.StatusCodeID  FROM syStatusCodes k WHERE  B.StatusCodeId=k.StatusCodeId AND  k.SysStatusId in ( ")
            .Append(" SELECT k.SysStatusId FROM syStatusCodes k INNER JOIN sySysStatus M ON k.SysStatusId = M.SysStatusId ")
            .Append(" INNER JOIN syCampGrps cg ON k.CampGrpId = cg.CampGrpId  Where M.SysStatusId IN (6,7,9,20,22)")
            .Append(") )")
            .Append("ORDER BY A.TestId,C.LastName,C.FirstName,C.MiddleName")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            'Add new columns
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("TestIdStr", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("StuIdentifierField", System.Type.GetType("System.String")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function

#End Region

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
End Class
