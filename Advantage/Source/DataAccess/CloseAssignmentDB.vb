Public Class CloseAssignmentDB
    'this is a test
    'Public Function UpdateActivityAssignment(ByVal CloseAssignment As CloseAssignmentInfo)
    '    Dim db As New DataAccess
    '    Dim strSQL As New StringBuilder
    '    With strSQL
    '        .Append("UPDATE cmActivityAssignment SET ")
    '        .Append("ActivityResultId = ?")
    '        .Append(",DateCompleted = ?")
    '        .Append(",Comments = ?")
    '        .Append(",ActivityStatusId = ?")
    '        .Append(" WHERE ActivityAssignmentId = ? ")
    '    End With
    '    db.AddParameter("@activityresult", CloseAssignment.ActivityResult, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@datecompleted", CloseAssignment.DateCompleted, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '    db.AddParameter("@comments", CloseAssignment.Comments, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@activitystatus", 2, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
    '    db.AddParameter("@activityassignmentid", CloseAssignment.ActivityAssignmentId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    Try
    '        db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    db.ClearParameters()
    '    strSQL.Remove(0, strSQL.Length)
    'End Function
    'Public Function GetActivityResults()
    '    Dim db As New DataAccess
    '    Dim ds As New DataSet
    '    Dim strSQLString As New StringBuilder

    '    strSQLString.Remove(0, strSQLString.Length)

    '    'New dropdown table - Activity Results
    '    Dim da7 As New OleDbDataAdapter

    '    With strSQLString
    '        .Append("Select ActivityResultId, ActivityResultDescrip from cmActivityResults order by ActivityResultDescrip")
    '    End With
    '    db.OpenConnection()
    '    Try
    '        da7 = db.RunParamSQLDataAdapter(strSQLString.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Try
    '        da7.Fill(ds, "ActivityResult")
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    strSQLString.Remove(0, strSQLString.Length)
    '    db.CloseConnection()
    '    Return ds
    'End Function
End Class
