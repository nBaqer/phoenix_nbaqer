Imports FAME.Advantage.Common

Public Class JobListingDB

    Public Function GetJobListing(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String = ""
        Dim strOrderBy As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= " ORDER BY " & paramInfo.OrderBy
        End If

        With sb
            .Append("SELECT ")
            .Append("       plEmployers.EmployerDescrip,plEmployers.Code AS EmployerCode,plEmployerJobs.EmployerID,")
            .Append("       plEmployerJobs.Code AS JobCode,plEmployerJobs.EmployerJobTitle AS TitleDescrip,")       'adTitles.TitleDescrip,
            .Append("       (SELECT JobScheduleDescrip FROM plJobSchedule WHERE JobScheduleID=plEmployerJobs.ScheduleID) AS ScheduleDescrip,")
            .Append("       plEmployerJobs.OpenedFrom,plEmployerJobs.OpenedTo,plEmployerJobs.NumberOpen,plEmployerJobs.NumberFilled,plEmployerJobs.ContactID,")
            .Append("       (SELECT FirstName FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS FirstName,")
            .Append("       (SELECT MiddleName FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS MiddleName,")
            .Append("       (SELECT LastName FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS LastName,")
            .Append("       (SELECT Address1 FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS Address1,")
            .Append("       (SELECT Address2 FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS Address2,")
            .Append("       (SELECT City FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS City,")
            .Append("       (SELECT B.StateDescrip FROM plEmployerContact A,syStates B WHERE A.EmployerContactID=plEmployerJobs.ContactID AND A.State=B.StateId) AS State,")
            .Append("       (SELECT Zip FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS Zip,")
            .Append("       (SELECT WorkPhone FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS WorkPhone,")
            .Append("       (SELECT HomePhone FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS HomePhone,")
            .Append("       (SELECT CellPhone FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS CellPhone,")
            .Append("       (SELECT Beeper FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS Beeper,")
            .Append("       (SELECT WorkEmail FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS WorkEmail,")
            .Append("       (SELECT HomeEmail FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS HomeEmail,")
            .Append("       (SELECT ForeignZip FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS ForeignZip,")
            .Append("       (SELECT OtherState FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS OtherState,")
            .Append("       (SELECT ForeignWorkPhone FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS ForeignWorkPhone,")
            .Append("       (SELECT ForeignHomePhone FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS ForeignHomePhone,")
            .Append("       (SELECT ForeignCellPhone FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS ForeignCellPhone ")
            .Append("FROM   plEmployerJobs,syStatuses,adTitles,plEmployers,syCampGrps ")
            .Append("WHERE  plEmployerJobs.StatusId=syStatuses.StatusID ")
            .Append("       AND adTitles.TitleId=plEmployerJobs.JobTitleID ")
            .Append("       AND plEmployers.EmployerID=plEmployerJobs.EmployerID and plEmployers.CampGrpId=syCampGrps.CampGrpId ")
            .Append(strWhere)
            .Append(strOrderBy)

            '.Append("SELECT (SELECT EmployerDescrip FROM plEmployers WHERE EmployerID=plEmployerJobs.EmployerID) AS EmployerDescrip,")
            '.Append("plEmployers.Code AS Code,plEmployerJobs.EmployerID,")
            '.Append("adTitles.TitleDescrip,")
            '.Append("(SELECT Address1 FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS Address1,")
            '.Append("(SELECT Address2 FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS Address2,")
            '.Append("(SELECT City FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS City,")
            '.Append("(SELECT B.StateDescrip FROM plEmployerContact A,syStates B WHERE A.EmployerContactID=plEmployerJobs.ContactID AND A.State=B.StateId) AS State,")
            '.Append("(SELECT Zip FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS Zip,")
            '.Append("(SELECT FirstName FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS FirstName,")
            '.Append("(SELECT MiddleName FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS MiddleName,")
            '.Append("(SELECT LastName FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS LastName,")
            '.Append("(SELECT WorkPhone FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS WorkPhone,")
            '.Append("(SELECT HomePhone FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS HomePhone,")
            '.Append("(SELECT CellPhone FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS CellPhone,")
            '.Append("(SELECT Beeper FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS Beeper,")
            '.Append("(SELECT WorkEmail FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS WorkEmail,")
            '.Append("(SELECT HomeEmail FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS HomeEmail,")
            '.Append("plEmployerJobs.ContactID ")
            '.Append("FROM plEmployerJobs,syStatuses,adTitles,plEmployers ")
            '.Append("WHERE plEmployerJobs.StatusId=syStatuses.StatusID ")
            '.Append("AND adTitles.TitleId = plEmployerJobs.JobTitleID ")
            '.Append("AND plEmployers.EmployerID=plEmployerJobs.EmployerID ")


            ''.Append("SELECT DISTINCT plEmployers.EmployerDescrip,plEmployers.Code,plEmployerJobs.EmployerID,")
            ''.Append("adTitles.TitleDescrip,")
            ''.Append("(SELECT Address1 FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS Address1,")
            ''.Append("(SELECT Address2 FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS Address2,")
            ''.Append("(SELECT City FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS City,")
            ''.Append("(SELECT State FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS State,")
            ''.Append("(SELECT Zip FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS Zip,")
            ''.Append("(SELECT FirstName FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS FirstName,")
            ''.Append("(SELECT MiddleName FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS MiddleName,")
            ''.Append("(SELECT LastName FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS LastName,")
            ''.Append("(SELECT WorkPhone FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS WorkPhone,")
            ''.Append("(SELECT HomePhone FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS HomePhone,")
            ''.Append("(SELECT CellPhone FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS CellPhone,")
            ''.Append("(SELECT Beeper FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS Beeper,")
            ''.Append("(SELECT WorkEmail FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS WorkEmail,")
            ''.Append("(SELECT HomeEmail FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS HomeEmail,")
            ''.Append("plEmployerJobs.ContactID ")
            ''.Append("FROM plEmployerJobs,plEmployers,adTitles ")
            ''.Append("WHERE plEmployerJobs.EmployerID=plEmployers.EmployerID ")
            ''.Append("AND plEmployerJobs.JobTitleID=adTitles.TitleID ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function


    Public Function GetJobListSummary(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String = ""
        Dim strOrderBy As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= " ORDER BY " & paramInfo.OrderBy
        End If

        With sb
            .Append("SELECT ")
            .Append("       plEmployerJobs.StatusId,plEmployerJobs.EmployerJobID,")
            .Append("       (SELECT Status FROM syStatuses WHERE StatusId=plEmployerJobs.StatusId) AS Status,")
            .Append("       plEmployerJobs.Code,plEmployerJobs.EmployerJobTitle AS TitleDescrip,plEmployerJobs.JobTitleId,")    'adTitles.TitleDescrip,
            .Append("       plEmployerJobs.JobDescription,plEmployers.Code AS EmployerCode,plEmployers.EmployerDescrip,")
            .Append("       (SELECT JobCatDescrip FROM plJobCats where JobCatID=plEmployerJobs.JobGroupID) AS JobCatDescrip,")
            .Append("       plEmployerJobs.JobGroupId,(SELECT FirstName FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS FirstName,")
            .Append("       (SELECT MiddleName FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS MiddleName,")
            .Append("       (SELECT LastName FROM plEmployerContact WHERE EmployerContactID=plEmployerJobs.ContactID) AS LastName,")
            .Append("       plEmployerJobs.ContactId,plEmployerJobs.TypeId,plEmployerJobs.AreaId,plEmployerJobs.WorkDays,plEmployerJobs.NumberOpen,")
            .Append("       plEmployerJobs.NumberFilled,plEmployerJobs.OpenedFrom,plEmployerJobs.OpenedTo,plEmployerJobs.SalaryFrom,")
            .Append("       plEmployerJobs.SalaryTo,plEmployerJobs.HoursFrom,plEmployerJobs.HoursTo,plEmployerJobs.Notes,plEmployerJobs.Start,")
            .Append("       (SELECT FeeDescrip FROM plFee WHERE FeeID=plEmployerJobs.FeeID) AS FeeDescrip,plEmployerJobs.FeeId,plEmployerJobs.BenefitsId,")
            .Append("       (SELECT JobScheduleDescrip FROM plJobSchedule WHERE JobScheduleID=plEmployerJobs.ScheduleID) AS ScheduleDescrip,")
            .Append("       plEmployerJobs.ScheduleId,plEmployerJobs.EmployerId,plEmployerJobs.CampGrpId,")
            .Append("       (SELECT CampGrpDescrip FROM syCampGrps WHERE CampGrpId=plEmployerJobs.CampGrpId) AS CampGrpDescrip,")
            .Append("       plEmployerJobs.JobRequirements,plEmployerJobs.SalaryTypeID ")
            .Append("FROM   plEmployerJobs,syStatuses,adTitles,plEmployers,syCampGrps ")
            .Append("WHERE  plEmployerJobs.StatusId=syStatuses.StatusID ")
            .Append("       AND adTitles.TitleId=plEmployerJobs.JobTitleID ")
            .Append("       AND plEmployers.EmployerID=plEmployerJobs.EmployerID and plEmployers.CampGrpId=syCampGrps.CampGrpId ")
            .Append(strWhere)
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

End Class
