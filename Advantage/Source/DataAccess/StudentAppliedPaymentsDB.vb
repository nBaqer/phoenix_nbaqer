﻿Imports FAME.Advantage.Common
Public Class StudentAppliedPaymentsDB

#Region "Private Data Members"
    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")

#End Region

#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

#End Region

#Region "Public Methods"



    Public Function GetAppliedPaymentsForStudents(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New SQLDataAccess
        Dim ds As New DataSet
        Dim strWhere As String = ""
        Dim strTempWhere As String = ""
        Dim strOrderBy As String = ""
        Dim myCulture As New System.Globalization.CultureInfo("en-US", True)

        Dim CampGrpId As String = ""
        Dim PrgVerId As String = ""
        Dim ChargeCode As String = ""
        Dim EnrollmentStatus As String = ""
        Dim dtStartDate As Date
        Dim dtEndDate As Date


        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            Dim idx As Integer = paramInfo.FilterOther.IndexOf("BETWEEN")
            If idx > -1 Then
                Dim str As String = paramInfo.FilterOther.Substring(idx + 9)
                Dim lidx As Integer = str.IndexOf("'")
                dtStartDate = Date.Parse(str.Substring(0, lidx), myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
                idx = str.IndexOf("AND") + 5
                lidx = str.LastIndexOf("'")
                dtEndDate = Date.Parse(str.Substring(idx, lidx - idx), myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
            End If
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If
        strTempWhere = strWhere
        strTempWhere = strTempWhere.ToLower.Replace("and", ";")
        strWhere = ""
        Dim strArr As String() = strTempWhere.Remove(strTempWhere.IndexOf(";"), 1).Split(";")

        For i As Integer = 0 To strArr.Length - 1
            strWhere &= " and " & strArr(i)
        Next
        If strArr.Length = 1 Then
            strWhere &= " and "
        End If
        CampGrpId = strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in (")).ToLower.IndexOf(" and ")).Replace("t1.campgrpid in", "").Replace(")", "").Replace("(", "").Replace("'", "")

        'get the prgverid
        If strWhere.ToLower.Contains("arprgversions.prgverid") Then
            If strWhere.ToLower.Contains("arprgversions.prgverid in ") Then
                If strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid in ")).ToLower.IndexOf(" and ") >= 0 Then
                    PrgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid in (")).ToLower.IndexOf(" and ")).Replace("arprgversions.prgverid in", "").Replace(")", "").Replace("(", "").Replace("'", "")
                Else
                    PrgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid in (")).Replace("arprgversions.prgverid in", "").Replace(")", "").Replace("(", "").Replace("'", "")
                End If
            Else
                If strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid = ")).ToLower.IndexOf(" and ") >= 0 Then
                    PrgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid = "), strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid = ")).ToLower.IndexOf(" and ")).Replace("arprgversions.prgverid =", "").Replace(")", "").Replace("(", "").Replace("'", "")
                Else
                    PrgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid = ")).Replace("arprgversions.prgverid =", "").Replace(")", "").Replace("(", "").Replace("'", "")
                End If
            End If
        End If
        'end the prgverid

        'get the charge code
        If strWhere.ToLower.Contains("satranscodes.transcodeid") Then
            If strWhere.ToLower.Contains("satranscodes.transcodeid in ") Then
                If strWhere.Substring(strWhere.ToLower.IndexOf("satranscodes.transcodeid in ")).ToLower.IndexOf(" and ") >= 0 Then
                    ChargeCode = strWhere.Substring(strWhere.ToLower.IndexOf("satranscodes.transcodeid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("satranscodes.transcodeid in (")).ToLower.IndexOf(" and ")).Replace("satranscodes.transcodeid in", "").Replace(")", "").Replace("(", "").Replace("'", "")
                Else
                    ChargeCode = strWhere.Substring(strWhere.ToLower.IndexOf("satranscodes.transcodeid in (")).Replace("satranscodes.transcodeid in", "").Replace(")", "").Replace("(", "").Replace("'", "")
                End If
            Else
                If strWhere.Substring(strWhere.ToLower.IndexOf("satranscodes.transcodeid = ")).ToLower.IndexOf(" and ") >= 0 Then
                    ChargeCode = strWhere.Substring(strWhere.ToLower.IndexOf("satranscodes.transcodeid = "), strWhere.Substring(strWhere.ToLower.IndexOf("satranscodes.transcodeid = ")).ToLower.IndexOf(" and ")).Replace("satranscodes.transcodeid =", "").Replace(")", "").Replace("(", "").Replace("'", "")
                Else
                    ChargeCode = strWhere.Substring(strWhere.ToLower.IndexOf("satranscodes.transcodeid = ")).Replace("satranscodes.transcodeid =", "").Replace(")", "").Replace("(", "").Replace("'", "")
                End If
            End If
        End If
        'end charge code

        'get the enrollment status
        If strWhere.ToLower.Contains("systatuscodes.statuscodeid") Then
            If strWhere.ToLower.Contains("systatuscodes.statuscodeid in ") Then
                If strWhere.Substring(strWhere.ToLower.IndexOf("systatuscodes.statuscodeid in ")).ToLower.IndexOf(" and ") >= 0 Then
                    EnrollmentStatus = strWhere.Substring(strWhere.ToLower.IndexOf("systatuscodes.statuscodeid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("systatuscodes.statuscodeid in (")).ToLower.IndexOf(" and ")).Replace("systatuscodes.statuscodeid in", "").Replace(")", "").Replace("(", "").Replace("'", "")
                Else
                    EnrollmentStatus = strWhere.Substring(strWhere.ToLower.IndexOf("systatuscodes.statuscodeid in (")).Replace("systatuscodes.statuscodeid in", "").Replace(")", "").Replace("(", "").Replace("'", "")
                End If
            Else
                If strWhere.Substring(strWhere.ToLower.IndexOf("systatuscodes.statuscodeid = ")).ToLower.IndexOf(" and ") >= 0 Then
                    EnrollmentStatus = strWhere.Substring(strWhere.ToLower.IndexOf("systatuscodes.statuscodeid = "), strWhere.Substring(strWhere.ToLower.IndexOf("systatuscodes.statuscodeid = ")).ToLower.IndexOf(" and ")).Replace("systatuscodes.statuscodeid =", "").Replace(")", "").Replace("(", "").Replace("'", "")
                Else
                    EnrollmentStatus = strWhere.Substring(strWhere.ToLower.IndexOf("systatuscodes.statuscodeid = ")).Replace("systatuscodes.statuscodeid =", "").Replace(")", "").Replace("(", "").Replace("'", "")
                End If
            End If
        End If
        'end enrollment status

        db.AddParameter("@CampGrpId", CampGrpId.Trim(), SqlDbType.VarChar, 8000, ParameterDirection.Input)
        db.AddParameter("@PrgVerId", IIf(Not PrgVerId = "", PrgVerId.Trim(), DBNull.Value), SqlDbType.VarChar, 8000, ParameterDirection.Input)
        '' New Code Added By Vijay Ramteke On October 21, 2010 For Rally Id 1051
        ''db.AddParameter("@ChargeCode",ChargeCode.Trim(), SqlDbType.VarChar, 8000, ParameterDirection.Input)
        db.AddParameter("@ChargeCode", IIf(Not ChargeCode = "", ChargeCode.Trim(), DBNull.Value), SqlDbType.VarChar, 8000, ParameterDirection.Input)
        '' New Code Added By Vijay Ramteke On October 21, 2010 For Rally Id 1051
        db.AddParameter("@EnrollmentStatus", IIf(Not EnrollmentStatus = "", EnrollmentStatus.Trim(), DBNull.Value), SqlDbType.VarChar, 8000, ParameterDirection.Input)
        db.AddParameter("@StartDate", dtStartDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@EndDate", dtEndDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@StudentIdentifier", StudentIdentifier, SqlDbType.VarChar, , ParameterDirection.Input)
        ''''''''''''''''''
        db.CommandTimeout = 0
        ds = db.RunParamSQLDataSet_SP("dbo.usp_SA_GetAppliedPaymentsReport", "StudentAppliedPayments")

        Return ds
    End Function
#End Region


    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
End Class