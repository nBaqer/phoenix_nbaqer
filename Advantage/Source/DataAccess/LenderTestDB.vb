'Public Class LenderTestDB
'    Public Function GetAllLenders(ByVal strStatus As String) As DataSet

'        '   connect to the database
'        Dim db As New DataAccess
'        db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

'        '   build the sql query
'        Dim sb As New StringBuilder
'        With sb
'            .Append("SELECT ")
'            .Append("         L.LenderId, ")
'            .Append("         L.StatusId, ")
'            .Append("         ST.Status AS StatusDescrip, ")
'            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
'            .Append("         L.Code, ")
'            .Append("         L.LenderDescrip, ")
'            .Append("         L.Address1, ")
'            .Append("         L.Address2, ")
'            .Append("         L.City, ")
'            .Append("         L.StateId, ")
'            .Append("         L.OtherState, ")
'            .Append("         L.Zip, ")
'            .Append("         L.ForeignAddress, ")
'            .Append("         L.CountryId, ")
'            .Append("         L.Email, ")
'            .Append("         L.PrimaryContact, ")
'            .Append("         L.OtherContact, ")
'            .Append("         L.IsLender, ")
'            .Append("         L.IsServicer, ")
'            .Append("         L.IsGuarantor, ")
'            .Append("         L.PayAddress1, ")
'            .Append("         L.PayAddress2, ")
'            .Append("         L.PayCity, ")
'            .Append("         L.PayStateId, ")
'            .Append("         L.OtherPayState, ")
'            .Append("         L.PayZip, ")
'            .Append("         L.PayCountryId, ")
'            .Append("         L.CustService, ")
'            .Append("         L.ForeignCustService, ")
'            .Append("         L.Fax, ")
'            .Append("         L.ForeignFax, ")
'            .Append("         L.PreClaim, ")
'            .Append("         L.ForeignPreClaim, ")
'            .Append("         L.PostClaim, ")
'            .Append("         L.ForeignPostClaim, ")
'            .Append("         L.Comments, ")
'            .Append("         IsUsedAsLender = (CASE WHEN (SELECT COUNT(*) FROM faStudentAwards WHERE LenderId=L.LenderId) > 0 THEN 'True' ELSE 'False' END), ")
'            .Append("         IsUsedAsServicer = (CASE WHEN (SELECT COUNT(*) FROM faStudentAwards WHERE ServicerId=L.LenderId) > 0 THEN 'True' ELSE 'False' END), ")
'            .Append("         IsUsedAsGuarantor = (CASE WHEN (SELECT COUNT(*) FROM faStudentAwards WHERE GuarantorId=L.LenderId) > 0 THEN 'True' ELSE 'False' END), ")
'            .Append("         L.ModUser, ")
'            .Append("         L.ModDate ")
'            .Append("FROM     faLenders L, syStatuses ST ")
'            .Append("WHERE    L.StatusId = ST.StatusId ")
'            If strStatus = "True" Then
'                .Append("AND    ST.Status = 'Active' ")
'                .Append("ORDER BY L.LenderDescrip ")
'            ElseIf strStatus = "False" Then
'                .Append("AND    ST.Status = 'InActive' ")
'                .Append("ORDER BY L.LenderDescrip ")
'            Else
'                .Append("ORDER BY StatusDescrip, L.LenderDescrip ")
'            End If
'        End With
'        '   return dataset
'        Return db.RunParamSQLDataSet(sb.ToString)
'    End Function
'    Public Function GetLendersOnly(ByVal strStatus As String) As DataSet

'        '   connect to the database
'        Dim db As New DataAccess
'        db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

'        '   build the sql query
'        Dim sb As New StringBuilder
'        With sb
'            .Append("SELECT ")
'            .Append("         L.LenderId, ")
'            .Append("         L.StatusId, ")
'            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
'            .Append("         L.Code, ")
'            .Append("         L.LenderDescrip, ")
'            .Append("         L.Address1, ")
'            .Append("         L.Address2, ")
'            .Append("         L.City, ")
'            .Append("         L.StateId, ")
'            .Append("         L.OtherState, ")
'            .Append("         L.Zip, ")
'            .Append("         L.ForeignAddress, ")
'            .Append("         L.CountryId, ")
'            .Append("         L.Email, ")
'            .Append("         L.PrimaryContact, ")
'            .Append("         L.OtherContact, ")
'            .Append("         L.IsLender, ")
'            .Append("         L.IsServicer, ")
'            .Append("         L.IsGuarantor, ")
'            .Append("         L.PayAddress1, ")
'            .Append("         L.PayAddress2, ")
'            .Append("         L.PayCity, ")
'            .Append("         L.PayStateId, ")
'            .Append("         L.OtherPayState, ")
'            .Append("         L.PayZip, ")
'            .Append("         L.PayCountryId, ")
'            .Append("         L.CustService, ")
'            .Append("         L.ForeignCustService, ")
'            .Append("         L.Fax, ")
'            .Append("         L.ForeignFax, ")
'            .Append("         L.PreClaim, ")
'            .Append("         L.ForeignPreClaim, ")
'            .Append("         L.PostClaim, ")
'            .Append("         L.ForeignPostClaim, ")
'            .Append("         L.Comments, ")
'            .Append("         L.ModUser, ")
'            .Append("         L.ModDate ")
'            .Append("FROM     faLenders L, syStatuses ST ")
'            .Append("WHERE    L.StatusId = ST.StatusId AND L.IsLender = 1 ")
'            If strStatus = "True" Then
'                .Append("AND    ST.Status = 'Active' ")
'                .Append("ORDER BY L.LenderDescrip ")
'            ElseIf strStatus = "False" Then
'                .Append("AND    ST.Status = 'InActive' ")
'                .Append("ORDER BY L.LenderDescrip ")
'            Else
'                .Append("ORDER BY ST.Status,L.LenderDescrip asc")
'            End If
'        End With

'        '   return dataset
'        Return db.RunParamSQLDataSet(sb.ToString)
'    End Function
'    Public Function GetServicersOnly(ByVal strStatus As String) As DataSet

'        '   connect to the database
'        Dim db As New DataAccess
'        db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

'        '   build the sql query
'        Dim sb As New StringBuilder
'        With sb
'            .Append("SELECT ")
'            .Append("         L.LenderId, ")
'            .Append("         L.StatusId, ")
'            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
'            .Append("         L.Code, ")
'            .Append("         L.LenderDescrip, ")
'            .Append("         L.Address1, ")
'            .Append("         L.Address2, ")
'            .Append("         L.City, ")
'            .Append("         L.StateId, ")
'            .Append("         L.OtherState, ")
'            .Append("         L.Zip, ")
'            .Append("         L.ForeignAddress, ")
'            .Append("         L.CountryId, ")
'            .Append("         L.Email, ")
'            .Append("         L.PrimaryContact, ")
'            .Append("         L.OtherContact, ")
'            .Append("         L.IsLender, ")
'            .Append("         L.IsServicer, ")
'            .Append("         L.IsGuarantor, ")
'            .Append("         L.PayAddress1, ")
'            .Append("         L.PayAddress2, ")
'            .Append("         L.PayCity, ")
'            .Append("         L.PayStateId, ")
'            .Append("         L.OtherPayState, ")
'            .Append("         L.PayZip, ")
'            .Append("         L.PayCountryId, ")
'            .Append("         L.CustService, ")
'            .Append("         L.ForeignCustService, ")
'            .Append("         L.Fax, ")
'            .Append("         L.ForeignFax, ")
'            .Append("         L.PreClaim, ")
'            .Append("         L.ForeignPreClaim, ")
'            .Append("         L.PostClaim, ")
'            .Append("         L.ForeignPostClaim, ")
'            .Append("         L.Comments, ")
'            .Append("         L.ModUser, ")
'            .Append("         L.ModDate ")
'            .Append("FROM     faLenders L, syStatuses ST ")
'            .Append("WHERE    L.StatusId = ST.StatusId AND L.IsServicer = 1 ")
'            If strStatus = "True" Then
'                .Append("AND    ST.Status = 'Active' ")
'                .Append("ORDER BY L.LenderDescrip ")
'            ElseIf strStatus = "False" Then
'                .Append("AND    ST.Status = 'InActive' ")
'                .Append("ORDER BY L.LenderDescrip ")
'            Else
'                .Append("ORDER BY ST.Status,L.LenderDescrip asc")
'            End If
'        End With

'        '   return dataset
'        Return db.RunParamSQLDataSet(sb.ToString)
'    End Function
'    Public Function GetGuarantorsOnly(ByVal strStatus As String) As DataSet

'        '   connect to the database
'        Dim db As New DataAccess
'        db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

'        '   build the sql query
'        Dim sb As New StringBuilder
'        With sb
'            .Append("SELECT ")
'            .Append("         L.LenderId, ")
'            .Append("         L.StatusId, ")
'            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
'            .Append("         L.Code, ")
'            .Append("         L.LenderDescrip, ")
'            .Append("         L.Address1, ")
'            .Append("         L.Address2, ")
'            .Append("         L.City, ")
'            .Append("         L.StateId, ")
'            .Append("         L.OtherState, ")
'            .Append("         L.Zip, ")
'            .Append("         L.ForeignAddress, ")
'            .Append("         L.CountryId, ")
'            .Append("         L.Email, ")
'            .Append("         L.PrimaryContact, ")
'            .Append("         L.OtherContact, ")
'            .Append("         L.IsLender, ")
'            .Append("         L.IsServicer, ")
'            .Append("         L.IsGuarantor, ")
'            .Append("         L.PayAddress1, ")
'            .Append("         L.PayAddress2, ")
'            .Append("         L.PayCity, ")
'            .Append("         L.PayStateId, ")
'            .Append("         L.OtherPayState, ")
'            .Append("         L.PayZip, ")
'            .Append("         L.PayCountryId, ")
'            .Append("         L.CustService, ")
'            .Append("         L.ForeignCustService, ")
'            .Append("         L.Fax, ")
'            .Append("         L.ForeignFax, ")
'            .Append("         L.PreClaim, ")
'            .Append("         L.ForeignPreClaim, ")
'            .Append("         L.PostClaim, ")
'            .Append("         L.ForeignPostClaim, ")
'            .Append("         L.Comments, ")
'            .Append("         L.ModUser, ")
'            .Append("         L.ModDate ")
'            .Append("FROM     faLenders L, syStatuses ST ")
'            .Append("WHERE    L.StatusId = ST.StatusId AND L.IsGuarantor = 1 ")
'            If strStatus = "True" Then
'                .Append("AND    ST.Status = 'Active' ")
'                .Append("ORDER BY L.LenderDescrip ")
'            ElseIf strStatus = "False" Then
'                .Append("AND    ST.Status = 'InActive' ")
'                .Append("ORDER BY L.LenderDescrip ")
'            Else
'                .Append("ORDER BY ST.Status,L.LenderDescrip asc")
'            End If
'        End With

'        '   return dataset
'        Return db.RunParamSQLDataSet(sb.ToString)
'    End Function
'    Public Function GetLenderInfo(ByVal LenderId As String) As LenderTestInfo

'        '   connect to the database
'        Dim db As New DataAccess
'        db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

'        '   build the sql query
'        Dim sb As New StringBuilder
'        With sb
'            '   with subqueries
'            .Append("SELECT ")
'            .Append("         L.LenderId, ")
'            .Append("         L.StatusId, ")
'            .Append("         L.Code, ")
'            .Append("         L.LenderDescrip, ")
'            .Append("         L.Address1, ")
'            .Append("         L.Address2, ")
'            .Append("         L.City, ")
'            .Append("         L.StateId, ")
'            .Append("         (Select StateDescrip from syStates where StateId=L.StateId) as State, ")
'            .Append("         L.OtherState, ")
'            .Append("         L.Zip, ")
'            .Append("         L.ForeignAddress, ")
'            .Append("         L.CountryId, ")
'            .Append("         (Select CountryDescrip from adCountries where CountryId=L.CountryId) As Country, ")
'            .Append("         L.Email, ")
'            .Append("         L.PrimaryContact, ")
'            .Append("         L.OtherContact, ")
'            .Append("         L.IsLender, ")
'            .Append("         L.IsServicer, ")
'            .Append("         L.IsGuarantor, ")
'            .Append("         L.PayAddress1, ")
'            .Append("         L.PayAddress2, ")
'            .Append("         L.PayCity, ")
'            .Append("         L.PayStateId, ")
'            .Append("         (Select StateDescrip from syStates where StateId=L.PayStateId) as PayState, ")
'            .Append("         L.OtherPayState, ")
'            .Append("         L.PayZip, ")
'            .Append("         L.ForeignPayAddress, ")
'            .Append("         L.PayCountryId, ")
'            .Append("         (Select CountryDescrip from adCountries where CountryId=L.PayCountryId) As PayCountry, ")
'            .Append("         L.CustService, ")
'            .Append("         L.ForeignCustService, ")
'            .Append("         L.Fax, ")
'            .Append("         L.ForeignFax, ")
'            .Append("         L.PreClaim, ")
'            .Append("         L.ForeignPreClaim, ")
'            .Append("         L.PostClaim, ")
'            .Append("         L.ForeignPostClaim, ")
'            .Append("         L.Comments, ")
'            .Append("         IsUsedAsLender = (CASE WHEN (SELECT COUNT(*) FROM faStudentAwards WHERE LenderId=L.LenderId) > 0 THEN 'True' ELSE 'False' END), ")
'            .Append("         IsUsedAsServicer = (CASE WHEN (SELECT COUNT(*) FROM faStudentAwards WHERE ServicerId=L.LenderId) > 0 THEN 'True' ELSE 'False' END), ")
'            .Append("         IsUsedAsGuarantor = (CASE WHEN (SELECT COUNT(*) FROM faStudentAwards WHERE GuarantorId=L.LenderId) > 0 THEN 'True' ELSE 'False' END), ")
'            .Append("         L.ModUser, ")
'            .Append("         L.ModDate ")
'            .Append("FROM  faLenders L ")
'            .Append("WHERE L.LenderId = ? ")
'        End With

'        ' Add the LenderId to the parameter list
'        db.AddParameter("@LenderId", LenderId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'        '   Execute the query
'        Dim dr As New SafeDataReader(db.RunParamSQLDataReader(sb.ToString))

'        ' Call GetOrdinal and assign value to variable.
'        Dim iAddress1 As Integer = dr.GetOrdinal("Address1")
'        Dim iAddress2 As Integer = dr.GetOrdinal("Address2")

'        Dim LenderTestInfo As New LenderTestInfo

'        While dr.Read()

'            '   set properties with data from DataReader
'            With LenderTestInfo
'                .IsInDB = True
'                .LenderId = LenderId
'                .StatusId = CType(dr("StatusId"), Guid).ToString
'                .Code = dr("Code")
'                .LenderDescrip = dr("LenderDescrip")
'                .Address1 = dr.GetString(dr.GetOrdinal("Address1"))
'                .Address2 = dr.GetString(dr.GetOrdinal("Address2"))
'                .City = dr("City")
'                '.StateId = CType(dr("StateId"), Guid).ToString
'                .StateId = dr.GetGuid(dr.GetOrdinal("StateId")).ToString
'                .State = dr(dr.GetOrdinal("State"))
'                .OtherState = dr("OtherState")
'                .Zip = dr("Zip")
'                .ForeignAddress = dr("ForeignAddress")
'                .CountryId = dr.GetGuid(dr.GetOrdinal("CountryId")).ToString
'                .Country = dr("Country")
'                .Email = dr("Email")
'                .PrimaryContact = dr("PrimaryContact")
'                .OtherContact = dr("OtherContact")
'                .IsLender = dr("IsLender")
'                .IsServicer = dr("IsServicer")
'                .IsGuarantor = dr("IsGuarantor")
'                .PayAddress1 = dr("PayAddress1")
'                .PayAddress2 = dr("PayAddress2")
'                .PayCity = dr("PayCity")
'                .PayStateId = dr.GetGuid(dr.GetOrdinal("PayStateId")).ToString
'                .PayState = dr("PayState")
'                .OtherPayState = dr("OtherPayState")
'                .PayZip = dr("PayZip")
'                .ForeignPayAddress = dr("ForeignPayAddress")
'                .PayCountryId = dr.GetGuid(dr.GetOrdinal("PayCountryId")).ToString
'                .PayCountry = dr("PayCountry")
'                .CustService = dr("CustService")
'                .ForeignCustService = dr("ForeignCustService")
'                .Fax = dr("Fax")
'                .ForeignFax = dr("ForeignFax")
'                .PreClaim = dr("PreClaim")
'                .ForeignPreClaim = dr("ForeignPreClaim")
'                .PostClaim = dr("PostClaim")
'                .ForeignPostClaim = dr("ForeignPostClaim")
'                .Comments = dr("Comments")
'                .IsUsedAsLender = dr("IsUsedAsLender")
'                .IsUsedAsServicer = dr("IsUsedAsServicer")
'                .IsUsedAsGuarantor = dr("IsUsedAsGuarantor")
'                .ModUser = dr("ModUser")
'                .ModDate = dr.GetSmartDate(dr.GetOrdinal("ModDate"))
'            End With
'        End While

'        'Close Connection
'        db.CloseConnection()

'        '   Return LenderTestInfo
'        Return LenderTestInfo
'    End Function
'    Public Function UpdateLenderInfo(ByVal LenderTestInfo As LenderTestInfo, ByVal user As String) As TestResultInfo
'        Dim resInfo As New TestResultInfo

'        '   Connect to the database
'        Dim db As New DataAccess
'        db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

'        '   do an update
'        Try
'            '   build the query
'            Dim sb As New StringBuilder
'            With sb
'                .Append("UPDATE faLenders ")
'                .Append("   Set LenderId = ?, StatusId = ?, Code =?, LenderDescrip = ?, ")
'                .Append("       Address1 = ?, Address2 = ?, City = ?, StateId = ?, OtherState = ?, Zip = ?, ForeignAddress = ?, CountryId = ?, ")
'                .Append("       Email = ?, PrimaryContact = ?, OtherContact = ?, ")
'                .Append("       IsLender = ?, IsServicer = ?, IsGuarantor = ?, ")
'                .Append("       PayAddress1 = ?, PayAddress2 = ?, PayCity = ?, PayStateId = ?, OtherPayState = ?, PayZip = ?, ForeignPayAddress = ?, PayCountryId = ?, ")
'                .Append("       CustService = ?, ForeignCustService = ?, Fax = ?, ForeignFax = ?, PreClaim = ?, ForeignPreClaim = ?, PostClaim = ?, ForeignPostClaim = ?, ")
'                .Append("       Comments = ?, ModUser = ?, ModDate = ? ")
'                .Append("WHERE LenderId = ? ")
'                .Append("AND ModDate = ? ")
'                '.Append("Select count(*) from faLenders where ModDate = ? ")
'            End With

'            '   add parameters values to the query

'            '   LenderId
'            db.AddParameter("@LenderId", LenderTestInfo.LenderId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   StatusId
'            db.AddParameter("@StatusId", LenderTestInfo.StatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   Code
'            db.AddParameter("@Code", LenderTestInfo.Code, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   LenderDescrip
'            db.AddParameter("@LenderDescrip", LenderTestInfo.LenderDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   Address1
'            If LenderTestInfo.Address1 = "" Then
'                db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@Address1", LenderTestInfo.Address1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   Address2
'            If LenderTestInfo.Address2 = "" Then
'                db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@Address2", LenderTestInfo.Address2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   City
'            If LenderTestInfo.City = "" Then
'                db.AddParameter("@City", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@City", LenderTestInfo.City, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   StateId
'            If LenderTestInfo.StateId = Guid.Empty.ToString Then
'                db.AddParameter("@StateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@StateId", LenderTestInfo.StateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   OtherState
'            If LenderTestInfo.OtherState = "" Then
'                db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@OtherState", LenderTestInfo.OtherState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   Zip
'            If LenderTestInfo.Zip = "" Then
'                db.AddParameter("@Zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@Zip", LenderTestInfo.Zip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   ForeignAddress
'            db.AddParameter("@ForeignAddress", LenderTestInfo.ForeignAddress, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   CountryId
'            If LenderTestInfo.CountryId = Guid.Empty.ToString Then
'                db.AddParameter("@CountryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@CountryId", LenderTestInfo.CountryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   Email
'            If LenderTestInfo.Email = "" Then
'                db.AddParameter("@Email", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@Email", LenderTestInfo.Email, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   PrimaryContact
'            If LenderTestInfo.PrimaryContact = "" Then
'                db.AddParameter("@PrimaryContact", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@PrimaryContact", LenderTestInfo.PrimaryContact, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   OtherContact
'            If LenderTestInfo.OtherContact = "" Then
'                db.AddParameter("@OtherContact", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@OtherContact", LenderTestInfo.OtherContact, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   IsLender
'            db.AddParameter("@IsLender", LenderTestInfo.IsLender, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   IsServicer
'            db.AddParameter("@IsServicer", LenderTestInfo.IsServicer, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   IsGuarantor
'            db.AddParameter("@IsGuarantor", LenderTestInfo.IsGuarantor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   PayAddress1
'            If LenderTestInfo.PayAddress1 = "" Then
'                db.AddParameter("@PayAddress1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@PayAddress1", LenderTestInfo.PayAddress1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   PayAddress2
'            If LenderTestInfo.PayAddress2 = "" Then
'                db.AddParameter("@PayAddress2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@PayAddress2", LenderTestInfo.PayAddress2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   PayCity
'            If LenderTestInfo.PayCity = "" Then
'                db.AddParameter("@PayCity", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@PayCity", LenderTestInfo.PayCity, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   PayStateId
'            If LenderTestInfo.PayStateId = Guid.Empty.ToString Then
'                db.AddParameter("@PayStateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@PayStateId", LenderTestInfo.PayStateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   OtherPayState
'            If LenderTestInfo.OtherPayState = "" Then
'                db.AddParameter("@PayOtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@PayOtherState", LenderTestInfo.OtherPayState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   PayZip
'            If LenderTestInfo.PayZip = "" Then
'                db.AddParameter("@PayZip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@PayZip", LenderTestInfo.PayZip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   ForeignPayAddress
'            db.AddParameter("@ForeignPayAddress", LenderTestInfo.ForeignPayAddress, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   PayCountryId
'            If LenderTestInfo.PayCountryId = Guid.Empty.ToString Then
'                db.AddParameter("@PayCountryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@PayCountryId", LenderTestInfo.PayCountryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   CustService
'            If LenderTestInfo.CustService = "" Then
'                db.AddParameter("@CustService", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@CustService", LenderTestInfo.CustService, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   ForeignCustService
'            db.AddParameter("@ForeignCustService", LenderTestInfo.ForeignCustService, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   Fax
'            If LenderTestInfo.Fax = "" Then
'                db.AddParameter("@Fax", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@Fax", LenderTestInfo.Fax, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   ForeignFax
'            db.AddParameter("@ForeignFax", LenderTestInfo.ForeignFax, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   PreClaim
'            If LenderTestInfo.PreClaim = "" Then
'                db.AddParameter("@PreClaim", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@PreClaim", LenderTestInfo.PreClaim, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   ForeignPreClaim
'            db.AddParameter("@ForeignPreClaim", LenderTestInfo.ForeignPreClaim, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   PostClaim
'            If LenderTestInfo.PostClaim = "" Then
'                db.AddParameter("@PostClaim", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@PostClaim", LenderTestInfo.PostClaim, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   ForeignPostClaim
'            db.AddParameter("@ForeignPostClaim", LenderTestInfo.ForeignPostClaim, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   Comments
'            If LenderTestInfo.Comments = "" Then
'                db.AddParameter("@Comments", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@Comments", LenderTestInfo.Comments, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   ModUser
'            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

'            '   ModDate
'            'Dim now As Date = Date.Now
'            Dim strnow As Date = Utilities.GetAdvantageDBDateTime(Date.Now)
'            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

'            '   LenderId
'            db.AddParameter("@LenderId", LenderTestInfo.LenderId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

'            '   ModDate
'            db.AddParameter("@Original_ModDate", LenderTestInfo.ModDate.Date, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

'            '   ModDate
'            'db.AddParameter("@Updated_ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

'            '   execute the query
'            'Dim rowCount As Integer = db.RunParamSQLExecuteNoneQuery(sb.ToString)
'            db.RunParamSQLExecuteNoneQuery(sb.ToString)
'            '   If there were no updated rows then there was a concurrency problem
'            If rowCount > 0 Then
'                '   return without errors
'                'Return ""
'                LenderTestInfo.ModUser = user
'                LenderTestInfo.ModDate = New SmartDate(strnow)
'                resInfo.UpdatedObject = LenderTestInfo
'                Return resInfo
'            Else
'                'Return DALExceptions.BuildConcurrencyExceptionMessage()
'                resInfo.ErrorString = DALExceptions.BuildConcurrencyExceptionMessage()
'                Return resInfo
'            End If

'        Catch ex As OleDbException

'            '   return an error to the client
'            'DisplayOleDbErrorCollection(ex)
'            resInfo.ErrorString = DALExceptions.BuildErrorMessage(ex)
'            Return resInfo

'        Finally
'            'Close Connection
'            db.CloseConnection()
'        End Try

'    End Function
'    Public Function AddLenderInfo(ByVal LenderTestInfo As LenderTestInfo, ByVal user As String) As TestResultInfo
'        Dim resInfo As New TestResultInfo

'        '   Connect to the database
'        Dim db As New DataAccess
'        db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

'        '   do an insert
'        Try
'            '   build the query
'            Dim sb As New StringBuilder
'            With sb
'                .Append("INSERT faLenders ( ")
'                .Append("       LenderId, StatusId, Code, LenderDescrip, ")
'                .Append("       Address1, Address2, City, StateId, OtherState, Zip, ForeignAddress, CountryId, ")
'                .Append("       Email, PrimaryContact, OtherContact, ")
'                .Append("       IsLender, IsServicer, IsGuarantor, ")
'                .Append("       PayAddress1, PayAddress2, PayCity, PayStateId, OtherPayState, PayZip, ForeignPayAddress, PayCountryId, ")
'                .Append("       CustService, ForeignCustService, Fax, ForeignFax, PreClaim, ForeignPreClaim, PostClaim, ForeignPostClaim, ")
'                .Append("       Comments, ModUser, ModDate) ")
'                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
'            End With

'            '   add parameters values to the query

'            '   LenderId
'            db.AddParameter("@LenderId", LenderTestInfo.LenderId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

'            '   StatusId
'            db.AddParameter("@StatusId", LenderTestInfo.StatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   Code
'            db.AddParameter("@Code", LenderTestInfo.Code, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   LenderDescrip
'            db.AddParameter("@LenderDescrip", LenderTestInfo.LenderDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   Address1
'            If LenderTestInfo.Address1 = "" Then
'                db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@Address1", LenderTestInfo.Address1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   Address2
'            If LenderTestInfo.Address2 = "" Then
'                db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@Address2", LenderTestInfo.Address2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   City
'            If LenderTestInfo.City = "" Then
'                db.AddParameter("@City", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@City", LenderTestInfo.City, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   StateId
'            If LenderTestInfo.StateId = Guid.Empty.ToString Then
'                db.AddParameter("@StateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@StateId", LenderTestInfo.StateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   OtherState
'            If LenderTestInfo.OtherState = "" Then
'                db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@OtherState", LenderTestInfo.OtherState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   Zip
'            If LenderTestInfo.Zip = "" Then
'                db.AddParameter("@Zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@Zip", LenderTestInfo.Zip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   ForeignAddress
'            db.AddParameter("@ForeignAddress", LenderTestInfo.ForeignAddress, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   CountryId
'            If LenderTestInfo.CountryId = Guid.Empty.ToString Then
'                db.AddParameter("@CountryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@CountryId", LenderTestInfo.CountryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   Email
'            If LenderTestInfo.Email = "" Then
'                db.AddParameter("@Email", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@Email", LenderTestInfo.Email, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   PrimaryContact
'            If LenderTestInfo.PrimaryContact = "" Then
'                db.AddParameter("@PrimaryContact", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@PrimaryContact", LenderTestInfo.PrimaryContact, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   OtherContact
'            If LenderTestInfo.OtherContact = "" Then
'                db.AddParameter("@OtherContact", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@OtherContact", LenderTestInfo.OtherContact, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   IsLender
'            db.AddParameter("@IsLender", LenderTestInfo.IsLender, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   IsServicer
'            db.AddParameter("@IsServicer", LenderTestInfo.IsServicer, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   IsGuarantor
'            db.AddParameter("@IsGuarantor", LenderTestInfo.IsGuarantor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   PayAddress1
'            If LenderTestInfo.PayAddress1 = "" Then
'                db.AddParameter("@PayAddress1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@PayAddress1", LenderTestInfo.PayAddress1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   PayAddress2
'            If LenderTestInfo.PayAddress2 = "" Then
'                db.AddParameter("@PayAddress2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@PayAddress2", LenderTestInfo.PayAddress2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   PayCity
'            If LenderTestInfo.PayCity = "" Then
'                db.AddParameter("@PayCity", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@PayCity", LenderTestInfo.PayCity, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   PayStateId
'            If LenderTestInfo.PayStateId = Guid.Empty.ToString Then
'                db.AddParameter("@PayStateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@PayStateId", LenderTestInfo.PayStateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   OtherPayState
'            If LenderTestInfo.OtherPayState = "" Then
'                db.AddParameter("@OtherPayState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@OtherPayState", LenderTestInfo.OtherPayState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   PayZip
'            If LenderTestInfo.PayZip = "" Then
'                db.AddParameter("@PayZip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@PayZip", LenderTestInfo.PayZip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   ForeignPayAddress
'            db.AddParameter("@ForeignPayAddress", LenderTestInfo.ForeignPayAddress, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   PayCountryId
'            If LenderTestInfo.PayCountryId = Guid.Empty.ToString Then
'                db.AddParameter("@PayCountryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@PayCountryId", LenderTestInfo.PayCountryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   CustService
'            If LenderTestInfo.CustService = "" Then
'                db.AddParameter("@CustService", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@CustService", LenderTestInfo.CustService, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   ForeignCustService
'            db.AddParameter("@ForeignCustService", LenderTestInfo.ForeignCustService, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   Fax
'            If LenderTestInfo.Fax = "" Then
'                db.AddParameter("@Fax", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@Fax", LenderTestInfo.Fax, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   ForeignFax
'            db.AddParameter("@ForeignFax", LenderTestInfo.ForeignFax, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   PreClaim
'            If LenderTestInfo.PreClaim = "" Then
'                db.AddParameter("@PreClaim", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@PreClaim", LenderTestInfo.PreClaim, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   ForeignPreClaim
'            db.AddParameter("@ForeignPreClaim", LenderTestInfo.ForeignPreClaim, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   PostClaim
'            If LenderTestInfo.PostClaim = "" Then
'                db.AddParameter("@PostClaim", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@PostClaim", LenderTestInfo.PostClaim, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   ForeignPostClaim
'            db.AddParameter("@ForeignPostClaim", LenderTestInfo.ForeignPostClaim, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   Comments
'            If LenderTestInfo.Comments = "" Then
'                db.AddParameter("@Comments", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            Else
'                db.AddParameter("@Comments", LenderTestInfo.Comments, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
'            End If

'            '   ModUser
'            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

'            '   ModDate
'            'Dim now As Date = Date.Now
'            Dim strnow As Date = Utilities.GetAdvantageDBDateTime(Date.Now)
'            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

'            '   execute the query
'            'Dim rowCount As Integer = db.RunParamSQLExecuteNoneQuery(sb.ToString)
'            db.RunParamSQLExecuteNoneQuery(sb.ToString)
'            '   If there were no updated rows then there was a concurrency problem
'            If rowCount > 0 Then
'                '   return without errors
'                'Return ""
'                LenderTestInfo.IsInDB = True
'                LenderTestInfo.ModUser = user
'                LenderTestInfo.ModDate = New SmartDate(strnow)
'                resInfo.UpdatedObject = LenderTestInfo
'                Return resInfo
'            Else
'                'Return DALExceptions.BuildConcurrencyExceptionMessage()
'                resInfo.ErrorString = DALExceptions.BuildConcurrencyExceptionMessage()
'                Return resInfo
'            End If

'        Catch ex As OleDbException
'            '   return an error to the client
'            resInfo.ErrorString = DALExceptions.BuildErrorMessage(ex)
'            Return resInfo

'        Finally
'            'Close Connection
'            db.CloseConnection()
'        End Try

'    End Function
'    Public Function DeleteLenderInfo(ByVal LenderId As String, ByVal modDate As DateTime) As String

'        '   Connect to the database
'        Dim db As New DataAccess
'        db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

'        '   do a delete
'        Try
'            '   build the query
'            Dim sb As New StringBuilder
'            With sb
'                .Append("DELETE FROM faLenders ")
'                .Append("WHERE LenderId = ? ")
'                .Append(" AND ModDate = ? ;")
'                .Append("SELECT count(*) FROM faLenders WHERE LenderId = ? ")
'            End With

'            '   add parameters values to the query

'            '   LenderId
'            db.AddParameter("@LenderId", LenderId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

'            '   ModDate
'            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

'            '   LenderId
'            db.AddParameter("@LenderId", LenderId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

'            '   execute the query
'            'Dim rowCount As Integer = db.RunParamSQLExecuteNoneQuery(sb.ToString)
'            db.RunParamSQLExecuteNoneQuery(sb.ToString)
'            '   If there were no updated rows then there was a concurrency problem
'            If rowCount > 0 Then
'                '   return without errors
'                Return ""
'            Else
'                Return DALExceptions.BuildConcurrencyExceptionMessage()
'            End If

'        Catch ex As OleDbException
'            '   return error message
'            Return DALExceptions.BuildErrorMessage(ex)
'        Finally
'            'Close Connection
'            db.CloseConnection()
'        End Try
'    End Function
'End Class
