Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SGR_4YR1DetailObjectDB

	Public Shared Function GetReportDatasetRaw(ByVal RptParamInfo As ReportParamInfoIPEDS) As DataSet

		' get list of students to include, based on report parameters
		Dim StudentList As String = IPEDSDB.GetStudentList(RptParamInfo)

		' if there are no students to include, return empty dataset
		If StudentList = "" Then
			Return New DataSet
		End If

		Dim sb As New System.Text.StringBuilder
		Dim dsRaw As New DataSet

		With sb
			' get Students
			.Append("SELECT DISTINCT arStudent.StudentId, ")

			' retrieve Student Identifier
			.Append(IPEDSDB.GetSQL_StudentIdentifier & ", ")

			' retrieve all other needed columns
			.Append("arStudent.LastName, arStudent.FirstName, arStudent.MiddleName, ")
			.Append("(SELECT syRptAgencyFldValues.RptAgencyFldValId ")
			.Append("	FROM adGenders, ")
			.Append("        syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
			.Append("	WHERE adGenders.GenderId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("         syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("         syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("         syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("         syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
			.Append("	      adGenders.GenderId = arStudent.Gender) As RptAgencyFldValId_Gender, ")
			.Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
			.Append("	FROM adGenders, ")
			.Append("        syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
			.Append("	WHERE adGenders.GenderId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("         syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("         syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("         syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("         syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
			.Append("	      adGenders.GenderId = arStudent.Gender) As GenderDescrip, ")
			.Append("(SELECT syRptAgencyFldValues.RptAgencyFldValId ")
			.Append("	FROM adEthCodes, ")
			.Append("        syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
			.Append("	WHERE adEthCodes.EthCodeId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("         syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("         syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("         syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("         syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
			.Append("	      adEthCodes.EthCodeId = arStudent.Race) As RptAgencyFldValId_EthCode, ")
			.Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
			.Append("	FROM adEthCodes, ")
			.Append("        syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
			.Append("	WHERE adEthCodes.EthCodeId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("         syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("         syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("         syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("         syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
			.Append("	      adEthCodes.EthCodeId = arStudent.Race) As EthCodeDescrip ")
			.Append("FROM ")
			.Append("arStudent ")

			' append list of appropriate students to include
			.Append("WHERE arStudent.StudentId IN (" & StudentList & ") ")

			' apply primary sort on Gender and Race
			.Append("ORDER BY RptAgencyFldValId_Gender, RptAgencyFldValId_EthCode, ")

			' apply secondary sort from report param info - either Student Identifier or Last Name
			.Append(IPEDSDB.GetSQL_StudentSort(RptParamInfo))

			' get list of enrollments and relevant info for same list of students
            .Append(";" & IPEDSDB.GetSQL_EnrollmentInfo(StudentList, RptParamInfo.FilterProgramIDs))
		End With

		' run queries, get raw DataSet
		dsRaw = IPEDSDB.DataAccessIPEDS().RunSQLDataSet(sb.ToString)

		' set table names for returned data
		With dsRaw
			.Tables(0).TableName = TblNameStudents
			.Tables(1).TableName = TblNameEnrollments
		End With

		' return the raw DataSet
		Return dsRaw

	End Function

End Class
