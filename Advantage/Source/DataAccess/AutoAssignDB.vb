Imports System
Imports System.Reflection
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OleDb
Imports System.Text
Imports System.Xml

Public Class AutoAssignDB

    Public Sub AddAutoAssign(ByVal AutoAssign As AutoAssignInfo)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("INSERT INTO cmAutoAssignRule")
            .Append("(AutoAssignRuleId, AutoAssignRuleDescrip, BasisId, TaskId, StatusId, CampusGroupId,")
            .Append(" BasisWhenId, BasisChangeId, ActivityId, Subject, Note, DueDateDays, DueDateCriteriaId, AssignToId, EmpId, WhenQuantity, DateCriteriaId)")
            .Append(" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
        End With
        db.AddParameter("@Param1", AutoAssign.AutoAssignRuleId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param2", AutoAssign.AutoAssignRuleDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@Param3", AutoAssign.BasisId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param4", AutoAssign.TaskId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        db.AddParameter("@Param5", AutoAssign.StatusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param6", AutoAssign.CampusGroupId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param7", AutoAssign.BasisWhenId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param8", AutoAssign.BasisChangeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param9", AutoAssign.ActivityId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param10", AutoAssign.Subject, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@Param11", AutoAssign.Note, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
        db.AddParameter("@Param12", AutoAssign.DueDateDays, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        db.AddParameter("@Param13", AutoAssign.DueDateCriteriaId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param14", AutoAssign.AssignToId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param15", AutoAssign.EmpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param16", AutoAssign.WhenQuantity, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        db.AddParameter("@Param17", AutoAssign.DateCriteriaId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex.InnerException)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Sub

    Public Sub UpdateAutoAssign(ByVal AutoAssign As AutoAssignInfo)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("UPDATE cmAutoAssignRule SET ")
            .Append("AutoAssignRuleDescrip = ?, BasisId = ?, TaskId = ?, StatusId = ?, CampusGroupId = ?, ")
            .Append(" BasisWhenId = ?, BasisChangeId = ?, ActivityId = ?, Subject = ?, Note = ?, DueDateDays = ?, DueDateCriteriaId = ?, AssignToId = ?, EmpId = ?, WhenQuantity = ?, DateCriteriaId = ?")
            .Append(" WHERE AutoAssignRuleId = ? ")
        End With
        Dim b As String = strSQL.ToString
        db.AddParameter("@Param2", AutoAssign.AutoAssignRuleDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@Param3", AutoAssign.BasisId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param4", AutoAssign.TaskId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        db.AddParameter("@Param5", AutoAssign.StatusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param6", AutoAssign.CampusGroupId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param7", AutoAssign.BasisWhenId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param8", AutoAssign.BasisChangeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param9", AutoAssign.ActivityId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param10", AutoAssign.Subject, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@Param11", AutoAssign.Note, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
        db.AddParameter("@Param12", AutoAssign.DueDateDays, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        db.AddParameter("@Param13", AutoAssign.DueDateCriteriaId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param14", AutoAssign.AssignToId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param15", AutoAssign.EmpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param16", AutoAssign.WhenQuantity, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        db.AddParameter("@Param17", AutoAssign.DateCriteriaId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param1", AutoAssign.AutoAssignRuleId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Dim a As String = ex.InnerException.Message
            Throw New Exception(ex.Message, ex)

        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
    End Sub

    Public Sub DeleteAutoAssign(ByVal AutoAssignRuleID As Guid)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("DELETE FROM cmAutoAssignRule WHERE AutoAssignRuleId = ?")
        End With
        db.AddParameter("@AutoAssignRuleId", AutoAssignRuleID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Sub

    Public Function GetDropDownList() As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da1 As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder
        With strSQLString
            .Append("SELECT a.BasisID, b.TblDescrip FROM cmBasis a, syTables b")
            .Append(" WHERE a.TblId = b.TblId")
        End With
        db.OpenConnection()
        Try
            da1 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da1.Fill(ds, "BasisList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Dim da2 As New OleDbDataAdapter
        strSQLString.Remove(0, strSQLString.Length)

        With strSQLString
            .Append("SELECT TaskId, TaskDescrip FROM cmTask")
        End With
        'db.OpenConnection()
        Try
            da2 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da2.Fill(ds, "TaskList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)

        Dim da3 As New OleDbDataAdapter

        With strSQLString
            .Append("SELECT StatusID, Status from syStatuses")
        End With
        Try
            da3 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da3.Fill(ds, "StatusList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)

        Dim da4 As New OleDbDataAdapter

        With strSQLString
            .Append("SELECT CampGrpId, CampGrpDescrip from syCampGrps")
        End With
        Try
            da4 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da4.Fill(ds, "CampGrpList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)

        Dim da5 As New OleDbDataAdapter

        With strSQLString
            .Append("SELECT a.BasisWhenId, a.BasisId, b.FldName from cmBasisWhen a, syFields b")
            .Append(" WHERE a.FldId = b.FldId")
        End With
        Try
            da5 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da5.Fill(ds, "BasisWhenList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)

        Dim da6 As New OleDbDataAdapter

        With strSQLString
            .Append("SELECT BasisDetailId, BasisWhenId, BasisDetailValue FROM cmBasisDetail ")
        End With
        Try
            da6 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da6.Fill(ds, "BasisDetailList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)

        'New dropdown table - Activity Results
        Dim da7 As New OleDbDataAdapter

        With strSQLString
            .Append("Select ActivityId, ActivityDescrip from cmActivities")
        End With
        Try
            da7 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da7.Fill(ds, "ActivityList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)

        Dim da8 As New OleDbDataAdapter

        With strSQLString
            .Append("Select AssignToId, AssignToDescrip from cmAssignTo")
        End With
        Try
            da8 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da8.Fill(ds, "AssignToList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)

        Dim da9 As New OleDbDataAdapter

        With strSQLString
            .Append("Select EmpId, FirstName, LastName, MI from hrEmployees")
        End With
        Try
            da9 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da9.Fill(ds, "EmployeeList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)

        Dim da10 As New OleDbDataAdapter

        With strSQLString
            .Append("Select DateCriteriaId, DateCriteriaDescrip from cmDateCriteria")
        End With
        Try
            da10 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da10.Fill(ds, "DateCriteriaList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)
        db.CloseConnection()
        Return ds
    End Function

    
    Public Function PopulateAutoAssignDataList() As DataSet
        Dim da As New OleDbDataAdapter
        Dim ds As New DataSet
        Try
            da = GetActivitiesDataList(ds)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex.InnerException)
        End Try
        da.Fill(ds, "AutoAssignList")
        Return ds
    End Function

    Public Function GetActivitiesDataList(ByVal ds As DataSet) As OleDbDataAdapter
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        Dim objAutoAssignList As New OleDbDataAdapter
        With strSQL
            .Append("SELECT a.*, b.Status FROM cmAutoAssignRule a, syStatuses b")
            .Append(" WHERE a.StatusId = b.StatusId")
        End With
        db.OpenConnection()
        Try
            objAutoAssignList = db.RunParamSQLDataAdapter(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.CloseConnection()
        db.ClearParameters()
        Return objAutoAssignList
    End Function
    ' CheckRule Function that accepts a Basis Table Name
    ' In the case that we are doing an Insert, we have no old object to compare to so it is optional
    Public Function CheckRule(ByVal BasisTableName As String, ByVal NewObject As Object, Optional ByVal OldObject As Object = Nothing)
        Dim RuleList As New SortedList
        Dim ObjectName As String
        Dim ObjectPk As String
        ObjectName = NewObject.GetType.Name.ToString
        If OldObject <> Nothing Then
            ' We have performed an Update
            Dim NewObjectProperty As PropertyInfo()
            Dim OldObjectProperty As PropertyInfo()
            NewObjectProperty = NewObject.GetType.GetProperties
            OldObjectProperty = OldObject.GetType.GetProperties
            Dim i As Integer
            For i = 0 To NewObjectProperty.Length - 1
                Dim NewPropInfo As PropertyInfo = CType(NewObjectProperty(i), PropertyInfo)
                Dim OldPropInfo As PropertyInfo = CType(OldObjectProperty(i), PropertyInfo)
                Dim NewPropertyName As String = NewPropInfo.Name.ToString()
                Dim NewPropertyType As String = NewPropInfo.PropertyType.ToString()
                Dim NewPropertyValue As String = NewPropInfo.GetValue(NewObject, Nothing).ToString
                Dim OldPropertyName As String = OldPropInfo.Name.ToString()
                Dim OldPropertyType As String = OldPropInfo.PropertyType.ToString()
                Dim OldPropertyValue As String = OldPropInfo.GetValue(OldObject, Nothing).ToString
                If i = 0 Then
                    ObjectPk = NewPropertyValue
                End If
                If NewPropertyValue <> OldPropertyValue Then
                    'write the property name, type and value to sorted list
                    RuleList.Add(NewPropertyName, NewPropertyValue)
                End If
            Next i
        Else
            ' We have performed an Insert Get all the property values
            Dim NewObjectProperty As PropertyInfo()
            NewObjectProperty = NewObject.GetType.GetProperties
            Dim i As Integer
            For i = 0 To NewObjectProperty.Length - 1
                Dim NewPropInfo As PropertyInfo = CType(NewObjectProperty(i), PropertyInfo)
                Dim NewPropertyName As String = NewPropInfo.Name.ToString()
                Dim NewPropertyType As String = NewPropInfo.PropertyType.ToString()
                Dim NewPropertyValue As String = NewPropInfo.GetValue(NewObject, Nothing).ToString
                RuleList.Add(NewPropertyName, NewPropertyValue)
                If i = 0 Then
                    ObjectPk = NewPropertyValue
                End If
            Next i
        End If
        'Now that we have the properties that have changed, get all the rules that apply 
        Dim tbl As DataTable
        Dim row As DataRow
        Dim ruleds As New DataSet
        ruleds = GetRules(BasisTableName)
        tbl = ruleds.Tables("RuleList")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("AutoAssignRuleId")}
        End With
        If ruleds.Tables(0).Rows.Count > 0 Then
            Dim o As String
            For Each o In RuleList.Keys
                For Each row In tbl.Rows
                    If o = row("FldName") Then
                        If row("BasisDetailValue") = "*" Then
                            ' Apply the rule - As any change to the field kicks off the rule
                            ApplyRule(row, ObjectName, ObjectPk)
                        Else
                            If RuleList(o) = row("BasisDetailValue") Then
                                ' Apply the rule
                                ApplyRule(row, ObjectName, ObjectPk)
                            End If
                        End If
                    End If
                Next
            Next
        End If
    End Function
    ' CheckRule Function that accepts a TaskId (This is hard coded into the app)
    ' In the case that we are doing an Insert, we have no old object to compare to so it is optional
    Public Function CheckRule(ByVal TaskId As Int32, ByVal NewObject As Object)
        Dim ds As New DataSet
        Dim tbl As New DataTable
        Dim row As DataRow
        Dim ObjectName As String
        Dim ObjectPk As String
        ObjectName = NewObject.GetType.Name.ToString
        Dim NewObjectProperty As PropertyInfo()
        NewObjectProperty = NewObject.GetType.GetProperties
        Dim NewPropInfo As PropertyInfo = CType(NewObjectProperty(0), PropertyInfo)
        Dim NewPropertyValue As String = NewPropInfo.GetValue(NewObject, Nothing).ToString
        ObjectPk = NewPropertyValue
        ds = GetRules(TaskId)
        tbl = ds.Tables("RuleList")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("AutoAssignRuleId")}
        End With
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row In tbl.Rows
                ApplyRule(row, ObjectName, ObjectPk)
            Next
        End If
    End Function
    Public Function ApplyRule(ByVal row As DataRow, ByVal ObjectName As String, ByVal ObjectPk As String)
        'Calculate the Creation Date for the Rule
        Dim CreationDate As DateTime
        CreationDate = CalculateDateCreate(row("WhenQuantity"), row(22))
        If DateDiff(DateInterval.Day, Now, CreationDate) <> 0 Then
            ' Put the activity in the queue
            Dim AutoAssignQueueInfo As New AutoAssignQueueInfo
            AutoAssignQueueInfo.AutoAssignQueueId = System.Guid.NewGuid
            AutoAssignQueueInfo.AutoAssignRuleId = row("AutoAssignRuleId")
            AutoAssignQueueInfo.BasisValuePKId = XmlConvert.ToGuid(ObjectPk)
            AutoAssignQueueInfo.ObjectName = ObjectName
            AutoAssignQueueInfo.DateToCreate = CreationDate.Date
            Dim AutoAssignQueueDB As New AutoAssignQueueDB
            AutoAssignQueueDB.AddAutoAssignQueue(AutoAssignQueueInfo)
        Else
            ' Create the activity immediately
            Dim ActivityAssignmentInfo As ActivityAssignmentInfo
            ActivityAssignmentInfo.ActivityId = row("ActivityId")
            ActivityAssignmentInfo.Comments = row("Note")
            ActivityAssignmentInfo.Subject = row("Subject")
            ActivityAssignmentInfo.EmployeeId = row("EmpId")
            ActivityAssignmentInfo.DateCreated = Now
            ActivityAssignmentInfo.ActivityStatusId = 1
            ActivityAssignmentInfo.DueDate = CalculateDueDate(row("DueDateDays"), row(21), CreationDate)
            If ObjectName = "StudentInfo" Then
                ActivityAssignmentInfo.StudentId = XmlConvert.ToGuid(ObjectPk)
            Else
                ActivityAssignmentInfo.LeadId = XmlConvert.ToGuid(ObjectPk)
            End If
            Dim db As ActivityAssignmentDB
            db.AddActivityAssignment(ActivityAssignmentInfo)
        End If

    End Function
    Public Function GetRules(ByVal BasisTableName As String) As DataSet
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("select a.*, d.TblName, e.FldName ,c.BasisDetailValue, c.BasisDetailGuid, g.DateCriteriaDescrip, h.DateCriteriaDescrip from cmAutoAssignRule a, cmBasisWhen b, cmBasisDetail c, syTables d, syFields e, cmBasis f, cmDateCriteria g, cmDateCriteria h")
            .Append(" where d.tblDescrip = ? and ")
            .Append(" a.BasisId = f.BasisId and ")
            .Append(" f.TblId = d.TblId and ")
            .Append(" a.BasisWhenId = b.BasisWhenId and ")
            .Append(" b.FldId = e.FldId and ")
            .Append(" a.BasisChangeId = c.BasisDetailId and ")
            .Append(" a.DueDateCriteriaId = g.DateCriteriaId and ")
            .Append(" a.DateCriteriaId = h.DateCriteriaId")
        End With
        Dim objRules As New OleDbDataAdapter
        db.OpenConnection()
        db.AddParameter("@BasisTableName", BasisTableName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Try
            objRules = db.RunParamSQLDataAdapter(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        objRules.Fill(ds, "RuleList")
        db.ClearParameters()
        db.CloseConnection()
        Return ds

    End Function
    Public Function GetRules(ByVal TaskId As Int32) As DataSet
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("select a.*, b.DateCriteriaDescrip, c.DateCriteriaDescrip from cmAutoAssignRule a, cmDateCriteria b, cmDateCriteria c")
            .Append(" where a.TaskId = ? and ")
            .Append(" a.DueDateCriteriaId = b.DateCriteriaId and ")
            .Append(" a.DateCriteriaId = c.DateCriteriaId")
        End With
        Dim objRules As New OleDbDataAdapter
        db.OpenConnection()
        db.AddParameter("@TaskId", TaskId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        Try
            objRules = db.RunParamSQLDataAdapter(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        objRules.Fill(ds, "RuleList")
        db.ClearParameters()
        db.CloseConnection()
        Return ds

    End Function
    Private Function CalculateDateCreate(ByVal Value As Int32, ByVal CriteriaType As String)
        Dim DateCreate As DateTime
        Dim NumberOfDays As Int32
        Select Case CriteriaType
            Case "Days"
                DateCreate = DateAdd(DateInterval.Day, Value, Now)
            Case "Weeks"
                DateCreate = DateAdd(DateInterval.WeekOfYear, Value, Now)
            Case "Months"
                DateCreate = DateAdd(DateInterval.Month, Value, Now)
            Case "Years"
                DateCreate = DateAdd(DateInterval.Year, Value, Now)
        End Select
        Return DateCreate
    End Function
    Private Function CalculateDueDate(ByVal Value As Int32, ByVal CriteriaType As String, ByVal CreationDate As DateTime)
        Dim DueDate As DateTime
        Dim NumberOfDays As Int32
        Select Case CriteriaType
            Case "Days"
                DueDate = DateAdd(DateInterval.Day, Value, CreationDate)
            Case "Weeks"
                DueDate = DateAdd(DateInterval.WeekOfYear, Value, CreationDate)
            Case "Months"
                DueDate = DateAdd(DateInterval.Month, Value, CreationDate)
            Case "Years"
                DueDate = DateAdd(DateInterval.Year, Value, CreationDate)
        End Select
        Return DueDate
    End Function
End Class
