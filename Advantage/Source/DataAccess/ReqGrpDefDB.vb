Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class ReqGrpDefDB

    Public Function GetReqType(ByVal GrpID As String) As DataTable
        Dim ds As DataSet

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.ReqTypeId ")
                .Append("FROM arReqs t1 ")
                .Append("WHERE ReqId=? ")
            End With

            'Add Parameter
            db.AddParameter("@GrpId", GrpID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

            'Return the dataset
            Return ds.Tables(0)

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

    End Function
    Public Function GetReqsNotAssigned(ByVal ReqGrpID As String) As DataSet
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim da As New OleDbDataAdapter
        Dim sb As New System.Text.StringBuilder
        Dim reccount As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        With sb
            .Append("Select count(*) from arReqGrpDef")
        End With

        db.OpenConnection()
        reccount = db.RunParamSQLScalar(sb.ToString)
        sb.Remove(0, sb.Length)

        If reccount <> 0 Then

            With sb
                .Append("SELECT t1.ReqId,t1.Descrip,t1.ReqTypeId ")
                .Append("FROM arReqs t1, arReqTypes t2 ")
                .Append("WHERE(t1.ReqTypeId = t2.ReqTypeId And t2.IsGroup = 0) ")
                .Append("AND t1.ReqTypeId = ")
                .Append("(SELECT DISTINCT ChildType FROM arReqTypes t3,arReqs t4 ")
                .Append("WHERE t3.ReqTypeId=t4.ReqTypeId AND t4.ReqId = ? ) ")
                .Append("AND  t1.ReqId not in (Select ReqId from arReqGrpDef t3 where GrpId = ? ) ")
                .Append("ORDER BY t1.Descrip")
            End With
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ReqGrpId", ReqGrpID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ReqId", ReqGrpID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Else
            With sb
                .Append("SELECT t1.ReqId,t1.Descrip,t1.ReqTypeId ")
                .Append("FROM arReqs t1, arReqTypes t2 ")
                .Append("WHERE(t1.ReqTypeId = t2.ReqTypeId And t2.IsGroup = 0) ")
                .Append("AND t1.ReqTypeId = ")
                .Append("(SELECT DISTINCT ChildType FROM arReqTypes t3,arReqs t4 ")
                .Append("WHERE t3.ReqTypeId=t4.ReqTypeId AND t4.ReqId = ? ) ")
                .Append("ORDER BY t1.Descrip")
            End With
            db.AddParameter("@ReqGrpId", ReqGrpID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        End If

        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds
    End Function

    Public Function GetReqGrpsNotAssigned(ByVal ReqGrpID As String) As DataSet
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim reccount As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        With sb
            .Append("Select count(*) from arReqGrpDef")
        End With

        db.OpenConnection()
        reccount = db.RunParamSQLScalar(sb.ToString)
        sb.Remove(0, sb.Length)

        If reccount <> 0 Then

            With sb
                .Append("SELECT t1.ReqId,t1.Descrip,t1.ReqTypeId ")
                .Append("FROM arReqs t1, arReqTypes t2 ")
                .Append("WHERE(t1.ReqTypeId = t2.ReqTypeId And t2.IsGroup = 1) ")
                .Append("AND t1.ReqTypeId = ")
                .Append("(SELECT DISTINCT t3.ReqTypeId FROM arReqTypes t3,arReqs t4 ")
                .Append("WHERE t3.ReqTypeId=t4.ReqTypeId AND t4.ReqId = ? ) ")
                .Append("and  t1.ReqId not in (Select ReqId from arReqGrpDef t3 where GrpId = ?) ")
                .Append("and t1.ReqId <> ? ")
                .Append("ORDER BY t1.Descrip")
            End With
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ReqGrpId", ReqGrpID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PrgVerId", ReqGrpID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ReqGrpId", ReqGrpID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Else
            With sb
                .Append("SELECT t1.ReqId,t1.Descrip,t1.ReqTypeId ")
                .Append("FROM arReqs t1, arReqTypes t2 ")
                .Append("WHERE(t1.ReqTypeId = t2.ReqTypeId And t2.IsGroup = 1) ")
                .Append("AND t1.ReqTypeId = ")
                .Append("(SELECT DISTINCT t3.ReqTypeId FROM arReqTypes t3,arReqs t4 ")
                .Append("WHERE t3.ReqTypeId=t4.ReqTypeId AND t4.ReqId = ? ) ")
                .Append("and t1.ReqId <> ? ")
                .Append("ORDER BY t1.Descrip")
            End With
            db.AddParameter("@ReqGrpId", ReqGrpID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ReqGrpId", ReqGrpID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        End If

        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds
    End Function

    Public Function GetReqsReqGrpsAssgd(ByVal GrpID As String) As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'Build query to obtain course/coursegrpids and course/coursegrpdescrips assgd to a prog ver.
        With sb

            .Append("SELECT t1.ReqId,ReqSeq,IsRequired,t2.Descrip,t1.Hours,t1.Cnt,t3.IsGroup  ")
            .Append("FROM arReqGrpDef t1, arReqs t2, arReqTypes t3 ")
            .Append("WHERE t1.ReqId = t2.ReqId and t1.GrpId = ? and t2.ReqTypeId = t3.ReqTypeId ")
            .Append("ORDER BY t1.ReqSeq")

        End With
        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@GrpId", GrpID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds.Tables(0)
    End Function

    Public Function GetTrackHrsCnt(ByVal ReqTypeId As Integer) As DataSet
        Dim ds As DataSet
        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.TrkHours,TrkCount ")
                .Append("FROM arReqTypes t1 ")
                .Append("WHERE t1.ReqTypeId = ? ")
            End With
            db.AddParameter("@ReqTypeId", ReqTypeId, DataAccess.OleDbDataType.OleDbInteger, 16, ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()
        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function



    Public Function GetReqs() As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim sb As New System.Text.StringBuilder

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            'Set the connection string
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            'Build query to obtain courseids and coursedescrips from the arCourses table.
            With sb
                .Append("SELECT t1.ReqId,Descrip,Hours,Credits ")
                .Append("FROM arReqs t1 ")
                .Append("WHERE t1.ReqTypeId = 1 ")
            End With

            '   Execute the query
            ds = db.RunSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds.Tables(0)

    End Function
    Public Function GetReqGrps() As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim da As OleDbDataAdapter

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            'Set the connection string
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            'Build query to obtain coursegrpids and coursegrpdescrips from the arCourseGrps table.
            With sb
                .Append("SELECT t1.ReqId,Descrip,Hours,Credits ")
                .Append("FROM arReqs t1 ")
                .Append("WHERE t1.ReqTypeId = 2 ")
            End With

            '   Execute the query
            ds = db.RunSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable
        Return ds.Tables(0)
    End Function

    Public Function LoadChildDS(ByVal ReqGrpID As String) As DataSet

        Dim LoadedChildDS As New DataSet
        Dim dt1 As New DataTable
        Dim dt2 As New DataTable
        Dim dt3 As New DataTable
        Dim row As DataRow
        Dim isreq As Boolean

        dt1 = GetReqs().Copy
        dt1.TableName = "Reqs"
        LoadedChildDS.Tables.Add(dt1)


        dt2 = GetReqGrps().Copy
        dt2.TableName = "ReqGrps"
        LoadedChildDS.Tables.Add(dt2)


        dt3 = GetReqsReqGrpsAssgd(ReqGrpID).Copy
        dt3.TableName = "FldsSelected"
        LoadedChildDS.Tables.Add(dt3)

        'return dataset to facade
        Return LoadedChildDS

    End Function

    Public Function InsertChildToDB(ByVal ChildInfoObj As ChildInfo, ByVal ID As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim childID As String
        Dim childTyp As String
        Dim childSeq As Integer
        Dim req As Integer
        Dim trk As Integer
        Dim hours As Decimal
        Dim cnt As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        childID = ChildInfoObj.ChildId
        ' childTyp = ChildInfoObj.ChildType
        childSeq = ChildInfoObj.ChildSeq
        req = ChildInfoObj.Req
        hours = ChildInfoObj.Hours
        cnt = ChildInfoObj.Cnt

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("INSERT INTO arReqGrpDef(GrpId,ReqId,ReqSeq,IsRequired,Hours,Cnt) ")
            .Append("VALUES(?,?,?,?,?,?)")

            db.AddParameter("@grpid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@childid", childID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@childseq", childSeq, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@req", req, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@hours", hours, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@credits", cnt, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        End With

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function

    Public Function UpdateChildInDB(ByVal ChildInfoObj As ChildInfo, ByVal ID As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim childID As String
        Dim childTyp As String
        Dim childSeq As Integer
        Dim req As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        childID = ChildInfoObj.ChildId
        childSeq = ChildInfoObj.ChildSeq

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("UPDATE arReqGrpDef Set ReqSeq = ? ")
            .Append("WHERE  ReqId = ? and GrpId = ?")

            db.AddParameter("@childseq", childSeq, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@childid", childID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@grpid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        End With

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function

    Public Function DeleteChildFrmDB(ByVal ChildInfoObj As ChildInfo, ByVal ID As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("DELETE FROM arReqGrpDef ")
            .Append("WHERE GrpId = ? ")
            .Append("AND ReqId = ? ")
        End With

        db.AddParameter("@grpid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", ChildInfoObj.ChildId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function
End Class
