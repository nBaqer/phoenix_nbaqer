' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' TransactionTypesDB.vb
'
' TransactionTypesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Imports System.Xml
Imports System.Xml.Schema
Imports System.IO
Imports FAME.Advantage.Common

Public Class TransactionTypesDB
    Private errorMessage As String

    'Public Function GetAllTransactionTypes(ByVal showActiveOnly As Boolean) As DataSet

    '    '   connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append("SELECT   TC.TransactionTypeId, ")
    '        .Append("         TC.StatusId, ")
    '        .Append("         TC.TransactionTypeDescrip, ")
    '        .Append("         TC.XmlGLDistributions ")
    '        .Append("FROM     saTransactionTypes TC, syStatuses ST ")
    '        .Append("WHERE    TC.StatusId = ST.StatusId ")
    '        If showActiveOnly Then
    '            .Append(" AND     ST.Status = 'Active' ")
    '        End If
    '        .Append("ORDER BY TC.TransactionTypeDescrip ")
    '    End With

    '    '   return dataset
    '    Return db.RunSQLDataSet(sb.ToString)

    'End Function
    'Public Function GetAllTransactionTypesXmlDoc(ByVal showActiveOnly As Boolean) As XmlDocument

    '    '   connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append("SELECT   TC.TransactionTypeId, ")
    '        .Append("         TC.StatusId, ")
    '        .Append("         TC.TransactionTypeDescrip, ")
    '        .Append("         TC.XmlGLDistributions ")
    '        .Append("FROM     saTransactionTypes TC, syStatuses ST ")
    '        .Append("WHERE    TC.StatusId = ST.StatusId ")
    '        If showActiveOnly Then
    '            .Append(" AND     ST.Status = 'Active' ")
    '        End If
    '        .Append("ORDER BY TC.TransactionTypeDescrip ")
    '    End With

    '    '   Execute the query
    '    Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString)

    '    Dim xmlTransactionTypes As New XmlDocument
    '    Dim root As System.Xml.XmlNode = xmlTransactionTypes.CreateElement("glDistributions")
    '    xmlTransactionTypes.AppendChild(root)

    '    While dr.Read()

    '        Dim xmlInstance As New XmlDocument
    '        Dim newNode As XmlNode

    '        If Not dr("XmlGLDistributions") Is System.DBNull.Value Then
    '            '   instantiate XmlGlDistributions

    '            xmlInstance.LoadXml(dr("XmlGLDistributions"))

    '            '   validate xmlDistributions for this TransactionType
    '            ValidateXmlInstance(xmlInstance)

    '            '    if any of the nodes is invalid return an error message
    '            If Not errorMessage = "" Then
    '                xmlTransactionTypes.RemoveAll()
    '                xmlTransactionTypes.InnerText = "This is a serious error that should be reported to the System Administrator: " + errorMessage
    '                Return xmlTransactionTypes
    '            End If

    '            '   get the node from the TransactionType
    '            newNode = xmlTransactionTypes.ImportNode(xmlInstance.SelectSingleNode("*"), True)

    '        Else
    '            '   create an empty glTransactionType
    '            newNode = xmlTransactionTypes.CreateElement("fame", "glTransactionTypes", "http://localhost/AdvantageV1App/localhost/Xml/Schemas/GLDistributionsSchema.xsd")

    '        End If

    '        '   add TransactionTypeId as attribute
    '        Dim TransactionTypeIdAttribute As XmlAttribute = xmlTransactionTypes.CreateAttribute("TransactionTypeId")
    '        TransactionTypeIdAttribute.Value = CType(dr("TransactionTypeId"), Guid).ToString
    '        newNode.Attributes.Append(TransactionTypeIdAttribute)

    '        '   add TransactionTypeDescrip as attribute
    '        Dim TransactionTypeDescripAttribute As XmlAttribute = xmlTransactionTypes.CreateAttribute("TransactionTypeDescrip")
    '        TransactionTypeDescripAttribute.Value = dr("TransactionTypeDescrip")
    '        newNode.Attributes.Append(TransactionTypeDescripAttribute)

    '        '   append this transaction
    '        root.AppendChild(newNode)

    '    End While

    '    If Not dr.IsClosed Then dr.Close()
    '    If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    '    '   Return xmlTransactionTypes
    '    Return xmlTransactionTypes

    'End Function
    Private Function ValidateXmlInstance(ByVal instance As XmlDocument) As String

        ' Initially errorMessage is empty
        errorMessage = ""

        ' Instantiate Validating Reader
        Dim myXmlValidatingReader As XmlValidatingReader = New XmlValidatingReader(New XmlTextReader(New StringReader(instance.OuterXml)))

        myXmlValidatingReader.ValidationType = ValidationType.Schema

        ' Set the validation event handler
        AddHandler myXmlValidatingReader.ValidationEventHandler, New ValidationEventHandler(AddressOf ValidationEvent)

        ' Read XML data to validate
        While myXmlValidatingReader.Read()
        End While

        ' Return error message (if any)
        Return errorMessage

    End Function
    Public Sub ValidationEvent(ByVal errorid As Object, ByVal args As ValidationEventArgs)

        '   concatenate error messages
        errorMessage &= (Strings.Chr(9) & "Validation error: " & args.Message)

        If (args.Severity = XmlSeverityType.Warning) Then
            errorMessage &= "No schema found to enforce validation."
        ElseIf (args.Severity = XmlSeverityType.Error) Then
            errorMessage &= "validation error occurred when validating the instance document."
        End If

        If Not (args.Exception Is Nothing) Then ' XSD schema validation error
            errorMessage &= args.Exception.SourceUri & "," & args.Exception.LinePosition & "," & args.Exception.LineNumber
        End If

    End Sub
    'Public Function GetTransactionTypeInfo(ByVal TransactionTypeId As String) As TransactionTypeInfo

    '    '   connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        '   with subqueries
    '        .Append("SELECT TC.TransactionTypeId, ")
    '        .Append("    TC.TransactionTypeCode, ")
    '        .Append("    TC.StatusId, ")
    '        .Append("    (Select Status from syStatuses where StatusId=TC.StatusId) As Status, ")
    '        .Append("    TC.TransactionTypeDescrip, ")
    '        .Append("    TC.CampGrpId, ")
    '        .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=TC.CampGrpId) As CampGrpDescrip ")
    '        .Append("FROM  saTransactionTypes TC ")
    '        .Append("WHERE TC.TransactionTypeId= ? ")
    '    End With

    '    ' Add the TransactionTypeId to the parameter list
    '    db.AddParameter("@TransactionTypeId", TransactionTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    '   Execute the query
    '    Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

    '    Dim TransactionTypeInfo As New TransactionTypeInfo

    '    While dr.Read()

    '        '   set properties with data from DataReader
    '        With TransactionTypeInfo
    '            .transactionTypeId = TransactionTypeId
    '            .IsInDB = True
    '            .StatusId = CType(dr("StatusId"), Guid).ToString
    '            .Status = dr("Status")
    '            .Description = dr("TransactionTypeDescrip")
    '            If Not (dr("XmlGLDistributions") Is System.DBNull.Value) Then .XmlGLDistributions = dr("XmlGLDistributions")
    '            If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
    '            If Not (dr("CampGrpdescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
    '        End With

    '    End While

    '    If Not dr.IsClosed Then dr.Close()
    '    If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    '    '   Return TransactionType
    '    Return TransactionTypeInfo

    'End Function
    'Public Function UpdateTransactionTypeInfo(ByVal TransactionTypeInfo As TransactionTypeInfo, ByVal user As String) As String

    '    '   Connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   do an update
    '    Try
    '        '   build the query
    '        Dim sb As New StringBuilder
    '        With sb
    '            .Append("UPDATE saTransactionTypes Set TransactionTypeId = ?, ")
    '            .Append(" StatusId = ?, TransactionTypeDescrip = ?, CampGrpId = ?, ")
    '            .Append(" XmlGLDistributions = ?, ")
    '            .Append(" ModUser = ?, ModDate = ? ")
    '            .Append("WHERE TransactionTypeId = ? ")
    '        End With

    '        '   add parameters values to the query

    '        '   TransactionTypeId
    '        db.AddParameter("@TransactionTypeId", TransactionTypeInfo.transactionTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   StatusId
    '        db.AddParameter("@StatusId", TransactionTypeInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   TransactionTypeDescrip
    '        db.AddParameter("@TransactionTypeDescrip", TransactionTypeInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   CampGrpId
    '        If TransactionTypeInfo.CampGrpId = Guid.Empty.ToString Then
    '            db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@CampGrpId", TransactionTypeInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        '   XmlGLDistributions
    '        If TransactionTypeInfo.XmlGLDistributions = "" Then
    '            db.AddParameter("@XmlGLDistributions", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@XmlGLDistributions", TransactionTypeInfo.XmlGLDistributions, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        '   ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   ModDate
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   TransactionTypeId
    '        db.AddParameter("@TransactionTypeId", TransactionTypeInfo.transactionTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   execute the query
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString)

    '        '   return without errors
    '        Return 0

    '    Catch ex As OleDbException
    '        '   return an error to the client
    '        Return -1
    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try

    'End Function
    'Public Function AddTransactionTypeInfo(ByVal TransactionTypeInfo As TransactionTypeInfo, ByVal user As String) As String

    '    '   Connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   do an insert
    '    Try
    '        '   build the query
    '        Dim sb As New StringBuilder
    '        With sb
    '            .Append("INSERT saTransactionTypes (TransactionTypeId, StatusId, ")
    '            .Append("   TransactionTypeDescrip, CampGrpId, XMLGLDistributions, ")
    '            .Append("   ModUser, ModDate) ")
    '            .Append("VALUES (?,?,?,?,?,?,?) ")
    '        End With

    '        '   add parameters values to the query

    '        '   TransactionTypeId
    '        db.AddParameter("@TransactionTypeId", TransactionTypeInfo.transactionTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   StatusId
    '        db.AddParameter("@StatusId", TransactionTypeInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   TransactionTypeDescrip
    '        db.AddParameter("@TransactionTypeDescrip", TransactionTypeInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   CampGrpId
    '        If TransactionTypeInfo.CampGrpId = Guid.Empty.ToString Then
    '            db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@CampGrpId", TransactionTypeInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        '   XmlGLDistributions
    '        If TransactionTypeInfo.XmlGLDistributions = "" Then
    '            db.AddParameter("@XmlGLDistributions", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@XmlGLDistributions", TransactionTypeInfo.XmlGLDistributions, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        '   ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   ModDate
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   execute the query
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString)

    '        '   return without errors
    '        Return 0

    '    Catch ex As OleDbException
    '        '   return an error to the client
    '        Return -1
    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try

    'End Function
    'Public Function DeleteTransactionTypeInfo(ByVal TransactionTypeId As String) As String

    '    '   Connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   do a delete
    '    Try
    '        '   build the query
    '        Dim sb As New StringBuilder
    '        With sb
    '            .Append("DELETE FROM saTransactionTypes ")
    '            .Append("WHERE TransactionTypeId = ? ")
    '        End With

    '        '   add parameters values to the query

    '        '   BankId
    '        db.AddParameter("@TransactionTypeId", TransactionTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   execute the query
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString)

    '        '   return without errors
    '        Return ""

    '    Catch ex As OleDbException
    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)

    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    'End Function
End Class
