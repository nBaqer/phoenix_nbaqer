Imports FAME.Advantage.Common

'=================================================
'FAME.AdvantageV1.BusinessFacade
'
'plEmployerInfoDB.vb
'
'Placement Employer Info DataAccess Interface
'
'=================================================
'CopyRight (c) 2003-2004 FAME Inc.
'
'All Rights Reserved
'=================================================

Public Class PlEmployerInfoDB
    Public Function UpdateEmployerInfo(ByVal employerinfo As plEmployerInfo, ByVal User As String) As String

        'Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE plEmployers Set Code=?,StatusId=?,EmployerDescrip=?,")
                .Append(" CampGrpId=?,Address1=?,Address2=?,")
                .Append(" City=?,StateId=?,Zip=?,CountyId=?,")
                .Append(" LocationId=?,Phone=?,Fax=?,")
                .Append(" Email=?,FeeId=?,IndustryId=?,ModUser=?,ModDate=?,ParentId=?,GroupName=?,ForeignPhone=?,ForeignFax=?,ForeignZip=?,OtherState=?,CountryId=?")
                .Append(" Where EmployerId=? ")
            End With

            'Add Parameters To Query

            'Code
            If employerinfo.Code = "" Then
                db.AddParameter("@code", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@code", employerinfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'StatusId
            If employerinfo.StatusId = Guid.Empty.ToString Or employerinfo.StatusId = "" Then
                db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StatusId", employerinfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'EmployerDescrip
            If employerinfo.Description = "" Then
                db.AddParameter("@Description", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Description", employerinfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'CampGroupId
            If employerinfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", employerinfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Address1
            db.AddParameter("@Address1", employerinfo.Address1, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Address2
            'db.AddParameter("@Address2", employerinfo.Address2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            If employerinfo.Address2 = "" Then
                db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", employerinfo.Address2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'City
            If employerinfo.City = "" Then
                db.AddParameter("@City", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@City", employerinfo.City, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'StateId
            If employerinfo.StateId = Guid.Empty.ToString Then
                db.AddParameter("@StateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StateId", employerinfo.StateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Zip
            If employerinfo.Zip = "" Then
                db.AddParameter("@zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@zip", employerinfo.Zip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'CountyId
            If employerinfo.County = "" Or employerinfo.County = Guid.Empty.ToString Then
                db.AddParameter("@CountyId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CountyId", employerinfo.County, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'LocationId
            If employerinfo.Location = "" Or employerinfo.Location = Guid.Empty.ToString Then
                db.AddParameter("@LocationId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@LocationId", employerinfo.Location, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Phone
            If employerinfo.Phone = "" Then
                db.AddParameter("@Phone", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Phone", employerinfo.Phone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'Fax
            If employerinfo.Fax = "" Then
                db.AddParameter("@Fax", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Fax", employerinfo.Fax, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'Email
            If employerinfo.Email = "" Then
                db.AddParameter("@Email", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Email", employerinfo.Email, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'FeeId
            If employerinfo.FeeId = Guid.Empty.ToString Or employerinfo.FeeId = "" Then
                db.AddParameter("@FeeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@FeeId", employerinfo.FeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'IndustryId
            If employerinfo.IndustryId = Guid.Empty.ToString Or employerinfo.IndustryId = "" Then
                db.AddParameter("@IndustryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@IndustryId", employerinfo.IndustryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", employerinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'Employer Group
            'If Group CheckBox Is Checked Then Add This Employer as Group
            'If not Checked then Insert Null in to Employer Group
            'If employerinfo.Group = True Then
            If employerinfo.ParentId = Guid.Empty.ToString Or employerinfo.ParentId = "" Then
                db.AddParameter("@ParentId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ParentId", employerinfo.ParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            ' Else
            'db.AddParameter("@ParentId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'End If

            'IsGroup
            db.AddParameter("@Group", employerinfo.Group, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            'Foreign Phone
            db.AddParameter("@ForeignPhone", employerinfo.ForeignPhone, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            'Foreign Fax
            db.AddParameter("@ForeignFax", employerinfo.ForeignFax, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            'Foreign Zip
            db.AddParameter("@ForeignZip", employerinfo.ForeignZip, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            'Other State
            If employerinfo.OtherState = "" Then
                db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@OtherState", employerinfo.OtherState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'CountyId
            If employerinfo.CountryId = "" Or employerinfo.CountryId = Guid.Empty.ToString Then
                db.AddParameter("@CountryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CountryId", employerinfo.CountryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'EmployerId
            db.AddParameter("@EmployerId", employerinfo.EmployerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            'Retun Without Errors
            Return ""
        Catch ex As System.Exception
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Function AddEmployerInfo(ByVal employerinfo As plEmployerInfo, ByVal user As String) As String

        'Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("Insert plEmployers(EmployerId,Code,StatusId,EmployerDescrip,")
                .Append(" CampGrpId,Address1,Address2,")
                .Append(" City,StateId,Zip,CountyId,")
                .Append(" LocationId,Phone,Fax,")
                .Append(" Email,FeeId,IndustryId,ModUser,ModDate,ParentId,GroupName,ForeignPhone,ForeignFax,ForeignZip,OtherState,CountryId) ")
                .Append(" Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            'Add Parameters To Query

            'EmployerId
            db.AddParameter("@EmployerId", employerinfo.EmployerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Code
            If employerinfo.Code = "" Then
                db.AddParameter("@code", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@code", employerinfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'StatusId
            If employerinfo.StatusId = Guid.Empty.ToString Or employerinfo.StatusId = "" Then
                db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StatusId", employerinfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'EmployerDescrip
            If employerinfo.Description = "" Then
                db.AddParameter("@Description", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Description", employerinfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'CampGroupId
            If employerinfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", employerinfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Address1
            db.AddParameter("@Address1", employerinfo.Address1, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Address2
            'db.AddParameter("@Address2", employerinfo.Address2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            If employerinfo.Address2 = "" Then
                db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", employerinfo.Address2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'City
            If employerinfo.City = "" Then
                db.AddParameter("@City", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@City", employerinfo.City, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'StateId
            If employerinfo.StateId = Guid.Empty.ToString Then
                db.AddParameter("@StateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StateId", employerinfo.StateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Zip
            If employerinfo.Zip = "" Then
                db.AddParameter("@zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@zip", employerinfo.Zip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'CountyId
            If employerinfo.County = "" Or employerinfo.County = Guid.Empty.ToString Then
                db.AddParameter("@CountyId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CountyId", employerinfo.County, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'LocationId
            If employerinfo.Location = "" Or employerinfo.Location = Guid.Empty.ToString Then
                db.AddParameter("@LocationId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@LocationId", employerinfo.Location, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Phone
            If employerinfo.Phone = "" Then
                db.AddParameter("@Phone", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Phone", employerinfo.Phone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'Fax
            If employerinfo.Fax = "" Then
                db.AddParameter("@Fax", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Fax", employerinfo.Fax, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'Email
            If employerinfo.Email = "" Then
                db.AddParameter("@Email", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Email", employerinfo.Email, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'FeeId
            If employerinfo.FeeId = Guid.Empty.ToString Or employerinfo.FeeId = "" Then
                db.AddParameter("@FeeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@FeeId", employerinfo.FeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'IndustryId
            If employerinfo.IndustryId = Guid.Empty.ToString Or employerinfo.IndustryId = "" Then
                db.AddParameter("@IndustryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@IndustryId", employerinfo.IndustryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", employerinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'Employer Group
            'If Group CheckBox Is Checked Then Add This Employer as Group
            'If not Checked then Insert Null in to Employer Group
            ' If employerinfo.Group = True Then
            If employerinfo.ParentId = Guid.Empty.ToString Or employerinfo.ParentId = "" Then
                db.AddParameter("@ParentId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ParentId", employerinfo.ParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            'Else
            ' db.AddParameter("@ParentId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ' End If

            'IsGroup
            db.AddParameter("@Group", employerinfo.Group, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            'Foreign Phone
            db.AddParameter("@ForeignPhone", employerinfo.ForeignPhone, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            'Foreign Fax
            db.AddParameter("@ForeignFax", employerinfo.ForeignFax, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            'Foreign Zip
            db.AddParameter("@ForeignZip", employerinfo.ForeignZip, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            'Other State
            If employerinfo.OtherState = "" Then
                db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@OtherState", employerinfo.OtherState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'CountyId
            If employerinfo.CountryId = "" Or employerinfo.CountryId = Guid.Empty.ToString Then
                db.AddParameter("@CountryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CountryId", employerinfo.CountryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            employerinfo.IsInDB = True
            'Retun Without Errors
            Return ""

        Catch ex As System.Exception
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Private Function GetEmployerInfo(ByVal EmployerInfo As plEmployerInfo) As Integer

    End Function

    Public Function UpdateEmployerJobCats(ByVal empId As String, ByVal user As String, ByVal selectedDegrees() As String)
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   First we have to delete all existing selections
        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("DELETE FROM plEmployerJobCats WHERE EmployerId = ?")
        End With

        '   delete all selected items
        db.AddParameter("@EmployerId", empId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        '   Insert one record per each Item in the Selected Group
        Dim i As Integer
        For i = 0 To selectedDegrees.Length - 1

            '   build query
            With sb
                .Append("INSERT INTO plEmployerJobCats (EmployerId, JobCatId, ModDate, ModUser) ")
                .Append("VALUES(?,?,?,?)")
            End With

            '   add parameters
            db.AddParameter("@EmployerId", empId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@JobCatId", DirectCast(selectedDegrees.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        Next

        'Close Connection
        db.CloseConnection()

    End Function

    Public Function DeleteEmployerInfo(ByVal EmployerId As String, ByVal moddate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder

            With sb
                .Append(" Delete from plEmployerJobCats where EmployerId=? ")
            End With
            db.AddParameter("@EmployerId", EmployerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)


            With sb
                .Append("DELETE FROM plEmployers ")
                .Append("WHERE EmployerId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM plEmployers WHERE EmployerId = ? ")
            End With

            '   add parameters values to the query

            'EmployerId
            db.AddParameter("@EmployerId", EmployerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", moddate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'EmployerId
            db.AddParameter("@EmployerId", EmployerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            'If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        ''   Connect to the database
        'Dim db As New DataAccess
        'db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

        ''   do a delete
        'Try
        '    '   build the query
        '    Dim sb As New StringBuilder
        '    With sb
        '        .Append("DELETE FROM saAcademicYears ")
        '        .Append("WHERE AcademicYearId = ? ")
        '        .Append(" AND ModDate = ? ;")
        '        .Append("SELECT count(*) FROM saAcademicYears WHERE AcademicYearId = ? ")
        '    End With

        '    '   add parameters values to the query

        '    '   AcademicYearId
        '    db.AddParameter("@AcademicYearId", AcademicYearId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '    '   ModDate
        '    db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        '    '   AcademicYearId
        '    db.AddParameter("@AcademicYearId", AcademicYearId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '    '   execute the query
        '    Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

        '    '   If the row was not deleted then there was a concurrency problem
        '    If rowCount = 0 Then
        '        '   return without errors
        '        Return ""
        '    Else
        '        Return DALExceptions.BuildConcurrencyExceptionMessage()
        '    End If

        'Catch ex As OleDbException
        '    '   return an error to the client
        '    Return DALExceptions.BuildErrorMessage(ex)
        'Finally
        '    'Close Connection
        '    db.CloseConnection()
        'End Try
    End Function

    Public Function GetEmployerInfo(ByVal EmployerId As String) As plEmployerInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT     B.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=B.StatusId) as Status, ")
            .Append("    B.Code, ")
            .Append("    B.EmployerDescrip, ")
            .Append("    B.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=B.CampGrpId) As CampGrpDescrip, ")
            .Append("    B.Address1, ")
            .Append("    B.Address2, ")
            .Append("    B.City, ")
            .Append("    B.StateId, ")
            .Append("    (Select StateDescrip from syStates where StateId=B.StateId) As State, ")
            .Append("    B.Zip, ")
            .Append("    B.Phone, ")
            .Append("    B.Fax, ")
            .Append("    B.Email, ")
            .Append("    B.ParentId, ")
            .Append("    (Select EmployerDescrip from plEmployers where EmployerId=B.ParentId) As Parent, ")
            .Append("    B.LocationId, ")
            .Append("    (Select LocationDescrip from plLocations where LocationId=B.LocationId) As Location, ")
            .Append("    B.FeeId, ")
            .Append("    (Select FeeDescrip from plFee where FeeId=B.FeeId) As Fee, ")
            .Append("    B.IndustryId, ")
            .Append("    (Select IndustryDescrip from plIndustries where IndustryId=B.IndustryId) As Industry, ")
            .Append("    B.CountyId, ")
            .Append("    (Select CountyDescrip from adCounties where CountyId=B.CountyId) As County, ")
            .Append("    B.GroupName, ")
            .Append(" B.ForeignPhone, ")
            .Append(" B.ForeignFax, ")
            .Append(" B.ForeignZip, B.OtherState,B.CountryId,B.ModDate ")
            .Append("FROM  plEmployers B ")
            .Append("WHERE B.EmployerId= ? ")
        End With

        ' Add the bankIdto the parameter list
        db.AddParameter("@EmployerId", EmployerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim employerinfo As New plEmployerInfo

        While dr.Read()

            '   set properties with data from DataReader
            With employerinfo
                .IsInDB = True
                .EmployerId = EmployerId
                If Not (dr("Code") Is System.DBNull.Value) Then .Code = dr("Code")
                If Not (dr("StatusId") Is System.DBNull.Value) Then .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Description = dr("EmployerDescrip")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                If Not (dr("Address1") Is System.DBNull.Value) Then .Address1 = dr("Address1")
                If Not (dr("Address2") Is System.DBNull.Value) Then .Address2 = dr("Address2")
                If Not (dr("City") Is System.DBNull.Value) Then .City = dr("City")
                If Not (dr("StateId") Is System.DBNull.Value) Then .StateId = CType(dr("StateId"), Guid).ToString Else .StateId = ""
                If Not (dr("State") Is System.DBNull.Value) Then .State = dr("State")
                If Not (dr("Zip") Is System.DBNull.Value) Then .Zip = dr("Zip")
                If Not (dr("Phone") Is System.DBNull.Value) Then .Phone = dr("Phone")
                If Not (dr("Fax") Is System.DBNull.Value) Then .Fax = dr("Fax")
                If Not (dr("Email") Is System.DBNull.Value) Then .Email = dr("Email")
                If Not (dr("ParentId") Is System.DBNull.Value) Then .ParentId = CType(dr("ParentId"), Guid).ToString
                If Not (dr("LocationId") Is System.DBNull.Value) Then .LocationId = CType(dr("LocationId"), Guid).ToString
                If Not (dr("Location") Is System.DBNull.Value) Then .Location = dr("Location")
                If Not (dr("FeeId") Is System.DBNull.Value) Then .FeeId = CType(dr("FeeId"), Guid).ToString
                If Not (dr("Fee") Is System.DBNull.Value) Then .Fee = dr("Fee")
                If Not (dr("CountyId") Is System.DBNull.Value) Then .CountyId = CType(dr("CountyId"), Guid).ToString
                If Not (dr("County") Is System.DBNull.Value) Then .County = dr("County")
                If Not (dr("CountryId") Is System.DBNull.Value) Then .CountryId = CType(dr("CountryId"), Guid).ToString
                If Not (dr("IndustryId") Is System.DBNull.Value) Then .IndustryId = CType(dr("IndustryId"), Guid).ToString
                .Group = IIf(dr("GroupName") Is System.DBNull.Value, False, dr("GroupName"))
                .ForeignPhone = IIf(dr("ForeignPhone") Is System.DBNull.Value, False, dr("ForeignPhone"))
                .ForeignFax = IIf(dr("ForeignFax") Is System.DBNull.Value, False, dr("ForeignFax"))
                .ForeignZip = IIf(dr("ForeignZip") Is System.DBNull.Value, False, dr("ForeignZip"))
                '.Group = dr("GroupName")
                'If Not (dr("GroupName") Is System.DBNull.Value) Then
                '    .Group = 1 'dr("GroupName")
                'Else
                '    .Group = 0
                'End If
                'If dr("ForeignPhone") = True Then .ForeignPhone = True Else .ForeignPhone = False
                'If dr("ForeignFax") = True Then .ForeignFax = True Else .ForeignFax = False
                'If dr("ForeignZip") = True Then .ForeignZip = 1 Else .ForeignZip = 0

                If Not (dr("OtherState") Is System.DBNull.Value) Then .OtherState = CType(dr("OtherState"), String).ToString
                .ModDate = dr("ModDate")
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return employerinfo
    End Function
    Public Function GetJobsOffered(ByVal empId As String) As DataSet

        '   get the connection to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT EmployerJobId, JobCatId ")
            .Append("FROM   plEmployerJobCats ")
            .Append("WHERE  EmployerId = ?")
        End With

        ' Add the parameter
        db.AddParameter("@EmployerId", empId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return the dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function

    Public Function GetValidJobsOffered(ByVal empId As String) As Integer
        '   get the connection to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder



        '   build the sql query
        With sb
            .Append("SELECT Count(*) ")
            .Append("FROM   plEmployerJobCats ")
            .Append("WHERE  EmployerId = ? ")
            .Append("and  JobCatId is Not Null")
        End With

        ' Add the parameter
        db.AddParameter("@EmployerId", empId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim JobCount As Integer

        '   Return the dataset
        JobCount = db.RunParamSQLScalar(sb.ToString)
        Return JobCount
    End Function
    Public Function GetAllTitles() As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append("SELECT   PR.TitleId, PR.TitleDescrip ")
            .Append("FROM     adTitles PR, syStatuses ST ")
            .Append("WHERE    PR.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY PR.TitleDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
End Class
