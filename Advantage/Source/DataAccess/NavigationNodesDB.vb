' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' NavigationNodesDB.vb
'
' NavigationNodesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Imports System.Xml
Imports FAME.Advantage.Common

Public Class NavigationNodesDB
    Protected navTable As DataTable
    Protected rolesTable As DataTable
    Protected navXmlDoc As XmlDocument

    Public Function GetNavigationTree(ByVal filter As String, ByVal schedulingMethods As Integer, campusID As String) As XmlDocument

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT NN.HierarchyId, ")
            '.Append("       NN.HierarchyName, ")
            '.Append("       NN.HierarchyIndex, ")
            .Append("       (Case ResourceTypeId    when 2 then ")
            .Append("										(Case ChildTypeId ")
            .Append("											when 4 then ")
            .Append("												Soundex(R.Resource) + Soundex(Substring(R.Resource,CharIndex(' ', R.Resource)+1,4)) ")
            .Append("											when 5 then ")
            .Append("												Soundex(R.Resource) + Soundex(Substring(R.Resource,CharIndex(' ', R.Resource)+1,4))  ")
            .Append(" 											else Cast(NN.HierarchyIndex as varchar)  ")
            .Append("											end) ")
            .Append("                               when 4 then ")
            .Append("				                    Soundex(R.Resource) + Soundex(Substring(R.Resource,CharIndex(' ', R.Resource)+1,4)) ")
            .Append("							    when 5 then ")
            .Append("								    Soundex(R.Resource) + Soundex(Substring(R.Resource,CharIndex(' ', R.Resource)+1,4)) ")
            .Append("							    else Cast(NN.HierarchyIndex as varchar) ")
            .Append("							    end) As HierarchyIndex, ")
            .Append("       NN.ResourceId, ")
            '.Append("       (Select Resource from syResources where ResourceId=NN.ResourceId) As HierarchyName, ")
            '.Append("       (Select ResourceTypeId from syResources where ResourceId=NN.ResourceId) As ResourceTypeId, ")
            '.Append("       (Select ChildTypeId from syResources where ResourceId=NN.ResourceId) As ChildTypeId, ")
            '.Append("       (Select ResourceURL from syResources where ResourceId=NN.ResourceId) As URL, ")
            '.Append("       (Select AllowSchlReqFlds from syResources where ResourceId=NN.ResourceId) As AllowSchlReqFlds, ")
            '.Append("       (Select MRUTypeId from syResources where ResourceId=NN.ResourceId) As MRUTypeId, ")
            .Append("       R.Resource As HierarchyName, ")
            .Append("       R.ResourceTypeId, ")
            .Append("       R.ChildTypeId, ")
            .Append("       R.ResourceURL As URL, ")
            .Append("       R.AllowSchlReqFlds, ")
            .Append("       R.UsedIn, ")
            .Append("       NN.IsPopupWindow, ")
            .Append("       NN.ParentId ")
            .Append("FROM   syNavigationNodes NN, syResources R ")
            .Append("WHERE ")
            .Append("       NN.ResourceId=R.ResourceId ")
            .Append("AND    R.UsedIn & " + schedulingMethods.ToString + " > 0 ")
            If MyAdvAppSettings.AppSettings("FAMEEdExp").ToUpper = "NO" Then
                .Append(" AND NN.ResourceId NOT IN (603,604,605,619) ")
            End If
            If MyAdvAppSettings.AppSettings("FAMEESP").ToUpper = "NO" Then
                .Append(" AND NN.ResourceId NOT IN (517,523,525) ")
            End If
            ''Code Added by Kamalesh Ahuja on 22 April 2010 to resolve Mantis Issue Id 18047
            If Not (HttpContext.Current.Session("UserName").ToString.ToUpper = "SUPER") And Not (HttpContext.Current.Session("UserName").ToString.ToUpper = "SUPPORT") Then
                .Append(" AND NN.ResourceId NOT IN (616) ")
            End If
            '''''''''''''''''''''''''''
            If Not MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages") Is Nothing Then
                If MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "no" Then
                    .Append(" and NN.ResourceId not in (614,615) ")
                End If
            End If
            ''Code Added By Vijay Ramteke  On July 10, 2010 Form Mantis Id 11856
            If Not MyAdvAppSettings.AppSettings("TrackSapAttendance", campusID).ToUpper = "BYDAY" Then
                .Append(" AND NN.ResourceId NOT IN (633) ")
            End If
            ''Code Added By Vijay Ramteke  On July 10, 2010 Form Mantis Id 11856
            .Append(" Order by ")
            ' .Append(" Case When (R.ResourceTypeID=5 and R.ResourceId in (444,445,452,458,459,460,639,640,641,642,643,644,645)) Then NN.SortSequence End, ")
            .Append(" R.Resource ")
        End With

        '   get navigation table
        navTable = db.RunSQLDataSet(sb.ToString).Tables(0)

        '   build the sql query
        sb = New StringBuilder
        With sb
            .Append("SELECT NN.ResourceId, ")
            '.Append("       (Select ResourceURL from syResources where ResourceId=NN.ResourceId) As URL, ")
            .Append("       R.RoleId ")
            .Append("FROM     syNavigationNodes NN, syRlsResLvls RR, syRoles R ")
            .Append("WHERE ")
            .Append("       NN.ResourceId=RR.ResourceId ")
            .Append("AND    RR.RoleId=R.RoleId")
        End With

        '   get navigation table
        rolesTable = db.RunSQLDataSet(sb.ToString).Tables(0)

        '   get HierarchyId of the root node. This must be the "Advantage" node.
        '   ParentId of the root node (Advantage node) must be an Guid.Empty value.
        Dim row() As DataRow = navTable.Select("ParentId='" + Guid.Empty.ToString + "'", "HierarchyId desc")
        Dim parentNodeId As String = CType(row(0)("HierarchyId"), Guid).ToString

        '   build navigation tree
        navXmlDoc = New XmlDocument
        BuildNavigationTree(navXmlDoc, parentNodeId, filter)

        '   return XmlDocument
        Return navXmlDoc

    End Function
    Private Sub BuildNavigationTree(ByVal node As XmlNode, ByVal parentNodeId As String, ByVal filter As String)

        '   get children to be created from the DB table
        Dim rows() As DataRow = navTable.Select("ParentId=" + "'" + parentNodeId + "' " + filter, "HierarchyIndex asc")

        '   add children only if there are children
        If rows.Length > 0 Then

            '   add "iTems" element
            Dim itemsElement As XmlNode = navXmlDoc.CreateElement("Nodes")
            node.AppendChild(itemsElement)

            '   add one child per row of the table
            For i As Integer = 0 To rows.Length - 1
                '   add "Node" element
                Dim newNode As XmlNode = navXmlDoc.CreateElement("Node")
                itemsElement.AppendChild(newNode)

                '   add HierarchyName as "Text" element
                Dim textElement As XmlElement = navXmlDoc.CreateElement("Text")
                textElement.InnerText = CType(rows(i)("HierarchyName"), String).ToString.Trim
                newNode.AppendChild(textElement)

                '   add ResourceTypeId as "ResourceTypeId" element
                Dim resourceTypeIdElement As XmlElement = navXmlDoc.CreateElement("ResourceTypeId")
                resourceTypeIdElement.InnerText = CType(rows(i)("ResourceTypeId"), String).ToString.Trim
                newNode.AppendChild(resourceTypeIdElement)

                '   add AllowSchlReqFlds as "AllowSchlReqFlds" element
                If Not rows(i)("AllowSchlReqFlds") Is System.DBNull.Value Then
                    Dim allowSchlReqFldsElement As XmlElement = navXmlDoc.CreateElement("AllowSchlReqFlds")
                    If rows(i)("AllowSchlReqFlds") Then
                        allowSchlReqFldsElement.InnerText = "true"
                    Else
                        allowSchlReqFldsElement.InnerText = "false"
                    End If
                    newNode.AppendChild(allowSchlReqFldsElement)
                End If

                '   add ChildTypeId as "ChildTypeId" element only if its not null
                If Not rows(i)("ChildTypeId") Is System.DBNull.Value Then
                    Dim childTypeIdElement As XmlElement = navXmlDoc.CreateElement("ChildTypeId")
                    childTypeIdElement.InnerText = CType(rows(i)("ChildTypeId"), String).ToString.Trim
                    newNode.AppendChild(childTypeIdElement)
                End If

                '   if the URL exists... add it as "URL" element
                If Not (rows(i)("URL") Is System.DBNull.Value) Then
                    '   add "URL" element
                    Dim URLElement As XmlElement = navXmlDoc.CreateElement("Url")
                    'add RESID=XXX to the query string
                    URLElement.InnerText = ("../" + CType(rows(i)("URL"), String).Substring(2) + "?resid=" + CType(rows(i)("ResourceId"), String)).ToLower
                    newNode.AppendChild(URLElement)

                    'add "Roles" to the node
                    Dim newRoles As XmlElement = newNode.AppendChild(navXmlDoc.CreateElement("Roles"))

                    'select rows with same URL
                    Dim roleRows() As DataRow = rolesTable.Select("ResourceId=" + "'" + CType(rows(i)("ResourceId"), String) + "'")

                    'add one "Role" per row
                    If roleRows.Length > 0 Then
                        For j As Integer = 0 To roleRows.Length - 1
                            Dim roleElement As XmlElement = navXmlDoc.CreateElement("Role")
                            roleElement.InnerText = CType(roleRows(j)("RoleId"), Guid).ToString
                            newRoles.AppendChild(roleElement)
                        Next
                    End If

                End If

                'iterate
                BuildNavigationTree(newNode, CType(rows(i)("HierarchyId"), Guid).ToString, filter)
            Next

        End If

    End Sub
    Public Function GetRolesResourcesPermissionsDS() As DataSet
        '   connect to the database
        Dim db As New DataAccess

      Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       RRLId, ")
            .Append("       ResourceId, ")
            .Append("       RoleId, ")
            .Append("       AccessLevel, ")
            .Append("       ModUser, ")
            .Append("       ModDate ")
            .Append("FROM   syRlsResLvls RRL ")
        End With

        'create data adapter for syRlsResLvls table
        Dim da As OleDbDataAdapter = db.RunParamSQLDataAdapter(sb.ToString())

        Dim ds As New DataSet
        da.Fill(ds, "RlsResLvls")

        sb = New StringBuilder()
        With sb
            .Append("SELECT NN.HierarchyId, ")
            .Append("       NN.HierarchyIndex, ")
            .Append("       NN.ResourceId, ")
            .Append("       NN.ParentId ")
            .Append("FROM   syNavigationNodes NN ")
        End With

        'create data adapter for syNavigationNodes table
        da = db.RunParamSQLDataAdapter(sb.ToString())

        'fill NavigationNodesTable
        da.Fill(ds, "NavigationNodes")

        '   build the sql query
        sb = New StringBuilder
        With sb
            .Append("SELECT ResourceId, ")
            .Append("       Resource, ")
            .Append("       ResourceTypeId, ")
            .Append("       ChildTypeId, ")
            .Append("       ResourceURL ")
            .Append("FROM   syResources ")
        End With

        'create data adapter for syNavigationNodes table
        da = db.RunParamSQLDataAdapter(sb.ToString())

        'fill NavigationNodesTable
        da.Fill(ds, "Resources")


        '   create primary and foreign key constraints

        '   set primary key for Resources table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("Resources").Columns("ResourceId")
        ds.Tables("Resources").PrimaryKey = pk0

        '   set primary key for RlsResLvls table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("RlsResLvls").Columns("RRLId")
        ds.Tables("RlsResLvls").PrimaryKey = pk1

        '   set foreign key column in NavigationNodes
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("NavigationNodes").Columns("ResourceId")

        '   set foreign key column in RlsResLvls table
        Dim fk1(0) As DataColumn
        fk1(0) = ds.Tables("RlsResLvls").Columns("ResourceId")

        '   set relationship in ResourcesNavigationNodes
        ds.Relations.Add("ResourcesNavigationNodes", pk0, fk0)

        '   set relationship in ResourcesRlsResLvls
        ds.Relations.Add("ResourcesRlsResLvls", pk0, fk1)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   return dataset
        Return ds

    End Function
    Public Function UpdateRolesResourcesPermissionsDS(ByVal rolesResourcesPermissionsDS As DataSet) As String

        '   all updates must be encapsulated as one transaction

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            '   build select query for the RptAgencyFldValues data adapter
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("       RRLId, ")
                .Append("       ResourceId, ")
                .Append("       RoleId, ")
                .Append("       AccessLevel, ")
                .Append("       ModUser, ")
                .Append("       ModDate ")
                .Append("FROM   syRlsResLvls RRL ")
            End With

            '   build select command
            Dim rolesResourcesPermissionsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle rolesResourcesPermissions table
            Dim rolesResourcesPermissionsDataAdapter As New OleDbDataAdapter(rolesResourcesPermissionsSelectCommand)

            '   build insert, update and delete commands for rolesResourcesPermissions table
            Dim cb As New OleDbCommandBuilder(rolesResourcesPermissionsDataAdapter)

            '   delete rows in RelResLvls table
            rolesResourcesPermissionsDataAdapter.Update(rolesResourcesPermissionsDS.Tables("RlsResLvls").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   update rows in RelResLvls table
            rolesResourcesPermissionsDataAdapter.Update(rolesResourcesPermissionsDS.Tables("RlsResLvls").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   add rows in RelResLvls table
            rolesResourcesPermissionsDataAdapter.Update(rolesResourcesPermissionsDS.Tables("RlsResLvls").Select(Nothing, Nothing, DataViewRowState.Added))

            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            Return ""

        Catch ex As OleDb.OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            '   close connection
            connection.Close()
        End Try
    End Function
    Public Function GetPagesByModulePageNameSP(ByVal ResourceId As Integer, ByVal PageName As String) As DataSet
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@ModuleId", ResourceId, SqlDbType.Int, , ParameterDirection.Input)
        db.AddParameter("@PageName", PageName, SqlDbType.VarChar, , ParameterDirection.Input)
        Try
            ds = db.RunParamSQLDataSet_SP("dbo.USP_PagesByModuleAndPageName_List")
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function GetModulesByResource(ByVal ResourceId As Integer) As DataSet
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@MaintenanePageId", ResourceId, SqlDbType.Int, , ParameterDirection.Input)

        Try
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GetModulesByResource_List")
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function GetPagesByModulePageName(ByVal ResourceId As Integer, ByVal PageName As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim intModuleResId As Integer = ResourceId

        '   build the sql query
        Dim sb As New StringBuilder
        If ResourceId = 0 And PageName = "" Then
            With sb
                .Append(" Select Distinct t1.ResourceId,t1.Resource from syResources t1,syNavigationNodes t2 where t1.ResourceId=t2.ResourceId and t1.ResourceTypeId=4 and t2.IsPopupWindow = 0 order by t1.Resource ")
            End With
        Else
            With sb
                'Top Query Returns all maintenance pages that belongs to Module Passed
                'For ex:AR
                .Append(" select Distinct ResourceId,Resource ")
                .Append(" from ")
                .Append(" ( ")
                .Append("   select t1.ResourceId,t1.Resource as resource from ")
                .Append("   syResources t1,syNavigationNodes t2,syResources t3,syNavigationNodes t4 ")
                .Append("   where ")
                .Append("       t1.ResourceId = t2.ResourceId and ")
                .Append("       t2.ResourceId = t3.ResourceId and ")
                .Append("       t3.ResourceTypeId = 4 and         ")
                .Append("       t2.ParentId = t4.HierarchyId  and t2.IsPopupWindow = 0 ")
                If Not ResourceId = 0 Then
                    .Append(" And t4.ResourceId = ? ")
                End If
                If Not PageName = "" Then
                    .Append(" and t1.Resource like  + ? + '%'")
                End If
                'The Bottom Query returns all maintenance pages that belongs to a module
                'but resides under a submenu for Ex:Placement -- Jobs ---- Job Titles
                .Append("       union ")
                .Append("   select A.ResourceId,B.Resource as resource from syNavigationNodes A,syResources B where ")
                .Append("   A.ResourceId = B.ResourceId and B.ResourceTypeId = 4 and A.IsPopupWindow=0 and ")
                .Append("   ParentId in ")
                .Append("       (       ")
                .Append("           Select Distinct    ")
                .Append("                       t2.HierarchyId      ")
                .Append("           from                    ")
                .Append("               syResources t1,syNavigationNodes t2 ")
                .Append("           where ")
                .Append("               t1.ResourceId = t2.ResourceId ")
                .Append("               and t1.ResourceTypeId in (2) ")
                If Not ResourceId = 0 Then
                    .Append("               and t2.ParentId in (select HierarchyId from syNavigationNodes where ResourceId=?) ")
                End If
                .Append(" ) ")
                If Not PageName = "" Then
                    .Append(" and B.Resource like  + ? + '%'")
                End If
                .Append(" ) GetAllMaintenancePages ")
                .Append(" order by resource ")
            End With
            If Not ResourceId = 0 Then
                db.AddParameter("@ModuleResourceId", intModuleResId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End If
            If Not PageName = "" Then
                db.AddParameter("@Resource", PageName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If Not ResourceId = 0 Then
                db.AddParameter("@ModuleResourceId", intModuleResId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End If
            If Not PageName = "" Then
                db.AddParameter("@Resource", PageName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
        End If
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetStudentPagesByModulePageName(ByVal ResourceId As Integer, ByVal PageName As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'Dim strModExp As String
        'Dim strExStudentRes As Integer
        'Dim strExEmployerRes As Integer
        Dim intModuleResId, intEntityResId As Integer

        intEntityResId = 394
        intModuleResId = ResourceId

        'Build the sql query
        If ResourceId = 0 And PageName = "" Then
            With sb
                .Append(" select Distinct t1.RelatedResourceId as ResourceId,")
                .Append(" t2.Resource as Resource ")
                .Append("  from syAdvantageResourceRelations t1,syResources t2 ")
                .Append(" where t1.ResourceId = 394 And ")
                .Append(" t1.RelatedResourceId = t2.ResourceId And t2.ResourceTypeId <> 1 ")
                .Append(" order by t2.Resource ")
            End With
        Else
            With sb
                .Append(" select Distinct t1.ResourceId,t1.Resource ")
                .Append(" from syResources t1,syNavigationNodes t2,syNavigationNodes t3,syNavigationNodes t4 ")
                .Append(" where t1.ResourceId = t2.ResourceId and t2.ParentId = t3.HierarchyId and ")
                .Append(" t3.ParentId = t4.HierarchyId  And t3.ResourceId = ? ")
                If Not ResourceId = 0 Then
                    .Append(" and t4.ResourceId = ? ")
                End If
                If Not PageName = "" Then
                    .Append(" and t1.Resource like  + ? + '%'")
                End If
                .Append(" order by t1.Resource ")
            End With
            db.AddParameter("@intEntityResId", intEntityResId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            If Not ResourceId = 0 Then
                db.AddParameter("@intModuleResId", intModuleResId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End If
            If Not PageName = "" Then
                db.AddParameter("@ResourceName", PageName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
        End If
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllNodeDetails(ByVal ResourceId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        Dim intReturnResId As Integer
        Dim boolModuleResource As Boolean
        With sb
            .Append("   select Distinct t1.ResourceId from syNavigationNodes t1,syNavigationNodes t2 ")
            .Append("   where t1.HierarchyId = t2.ParentId And t2.ResourceId = ? ")
        End With
        db.AddParameter("@ResourceId", ResourceId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
        intReturnResId = db.RunParamSQLScalar(sb.ToString)

        'The Expected Output is the resourceid of AR,FA,PL,SA,AD,HR,System,Faculty
        'If the Output is not one of the above mentioned we need to look up the node
        'and get the parent resource of the submenu
        If Not intReturnResId = 26 And Not intReturnResId = 191 And Not intReturnResId = 193 And Not intReturnResId = 194 And Not intReturnResId = 189 And Not intReturnResId = 192 And Not intReturnResId = 195 And Not intReturnResId = 300 Then
            boolModuleResource = False
        Else
            boolModuleResource = True
            Return db.RunParamSQLDataSet(sb.ToString)
        End If
        sb.Remove(0, sb.Length)
        db.ClearParameters()

        If boolModuleResource = False Then
            With sb
                .Append("   				select Distinct    ")
                .Append("                       A.ResourceId    ")
                .Append("                   from ")
                .Append("                       syNavigationNodes A,syNavigationNodes B,syNavigationNodes C ")
                .Append("                   where ")
                .Append("                       A.HierarchyId = B.ParentId and ")
                .Append("                       B.HierarchyId = C.ParentId And C.ResourceId = ? ")
            End With
            db.AddParameter("@ResourceId", ResourceId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Return db.RunParamSQLDataSet(sb.ToString)
        End If
        Return Nothing
    End Function
    Public Function GetStudentNodeDetails(ByVal ResourceId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select ")
            .Append("       t1.Resource, ")
            .Append("       t1.ResourceId ")
            .Append(" from ")
            .Append("       syResources t1, ")
            .Append("       syNavigationNodes t2,")
            .Append("       syNavigationNodes t3, ")
            .Append("       syNavigationNodes t4  ")
            .Append("   where ")
            .Append("       t1.ResourceId = t2.ResourceId and ")
            .Append("       t2.HierarchyId = t3.ParentId  and ")
            .Append("       t3.HierarchyId = t4.ParentId  and ")
            .Append("       t4.ResourceId = ?  ")
        End With
        db.AddParameter("@ResourceId", ResourceId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetHierarchyIndex(ByVal ResourceId As String) As Integer
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select Distinct HierarchyIndex from syNavigationNodes where ResourceId=? ")
        End With
        db.AddParameter("@ResourceId", ResourceId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Dim intIndex As Integer = db.RunParamSQLScalar(sb.ToString)

        'return dataset
        Return intIndex
    End Function
    Public Function GetAllParentNodes() As String
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select HierarchyId from syNavigationNodes where ResourceId=189 ")
        End With

        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim strADParentId As String = ""
        While dr.Read()
            strADParentId = CType(dr("HierarchyId"), Guid).ToString
        End While
        sb.Remove(0, sb.Length)

        With sb
            .Append(" select HierarchyId from syNavigationNodes where ResourceId=26 ")
        End With
        dr = db.RunParamSQLDataReader(sb.ToString)
        Dim strARParentId As String
        While dr.Read()
            strARParentId = CType(dr("HierarchyId"), Guid).ToString
        End While
        sb.Remove(0, sb.Length)

        With sb
            .Append(" select HierarchyId from syNavigationNodes where ResourceId=193 ")
        End With

        dr = db.RunParamSQLDataReader(sb.ToString)
        Dim strPLParentId As String
        While dr.Read()
            strPLParentId = CType(dr("HierarchyId"), Guid).ToString
        End While
        sb.Remove(0, sb.Length)

        With sb
            .Append(" select HierarchyId from syNavigationNodes where ResourceId=194 ")
        End With

        dr = db.RunParamSQLDataReader(sb.ToString)
        Dim strSAParentId As String
        While dr.Read()
            strSAParentId = CType(dr("HierarchyId"), Guid).ToString
        End While
        sb.Remove(0, sb.Length)

        With sb
            .Append(" select HierarchyId from syNavigationNodes where ResourceId=192 ")
        End With

        dr = db.RunParamSQLDataReader(sb.ToString)
        Dim strHRParentId As String
        While dr.Read()
            strHRParentId = CType(dr("HierarchyId"), Guid).ToString
        End While
        sb.Remove(0, sb.Length)

        With sb
            .Append(" select HierarchyId from syNavigationNodes where ResourceId=191 ")
        End With
        dr = db.RunParamSQLDataReader(sb.ToString)
        Dim strFinAidParentId As String
        While dr.Read()
            strFinAidParentId = CType(dr("HierarchyId"), Guid).ToString
        End While
        sb.Remove(0, sb.Length)

        With sb
            .Append(" select HierarchyId from syNavigationNodes where ResourceId=300 ")
        End With
        dr = db.RunParamSQLDataReader(sb.ToString)
        Dim strFacParentId As String
        While dr.Read()
            strFacParentId = CType(dr("HierarchyId"), Guid).ToString
        End While
        sb.Remove(0, sb.Length)

        With sb
            .Append(" select HierarchyId from syNavigationNodes where ResourceId=195 ")
        End With
        dr = db.RunParamSQLDataReader(sb.ToString)
        Dim strSysParentId As String
        While dr.Read()
            strSysParentId = CType(dr("HierarchyId"), Guid).ToString
        End While
        sb.Remove(0, sb.Length)

        If Not dr.IsClosed Then dr.Close()

        'return dataset
        Return strADParentId & "," & strARParentId & "," & strPLParentId & "," & strSAParentId & "," & strHRParentId & "," & strFinAidParentId & "," & strFacParentId & "," & strSysParentId
    End Function
    Public Function GetAllCommonTaskNodes() As String
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        'With sb
        '    .Append(" select HierarchyId from syNavigationNodes where ")
        '    .Append(" HierarchyName like  'Common%' and ParentId in  ")
        '    .Append(" (select HierarchyId from syNavigationNodes where ")
        '    .Append(" HierarchyName = 'Admissions') ")
        'End With


        'Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        'Dim strADParentId As String
        'While dr.Read()
        '    strADParentId = CType(dr("HierarchyId"), Guid).ToString
        'End While
        'sb.Remove(0, sb.Length)

        With sb
            .Append("   select ")
            .Append("       t1.HierarchyId ")
            .Append("   from ")
            .Append("       syNavigationNodes t1,syNavigationNodes t2 ")
            .Append("   where ")
            .Append("       t1.ResourceId=394 and t1.ParentId = t2.HierarchyId and ")
            .Append("       t2.ResourceId = 26 ")
        End With
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim strARParentId As String
        While dr.Read()
            strARParentId = CType(dr("HierarchyId"), Guid).ToString
        End While
        sb.Remove(0, sb.Length)

        With sb
            .Append("   select ")
            .Append("       t1.HierarchyId ")
            .Append("   from ")
            .Append("       syNavigationNodes t1,syNavigationNodes t2 ")
            .Append("   where ")
            .Append("       t1.ResourceId=394 and t1.ParentId = t2.HierarchyId and ")
            .Append("       t2.ResourceId = 193 ")
        End With

        dr = db.RunParamSQLDataReader(sb.ToString)
        Dim strPLParentId As String
        While dr.Read()
            strPLParentId = CType(dr("HierarchyId"), Guid).ToString
        End While
        sb.Remove(0, sb.Length)

        With sb
            .Append("   select ")
            .Append("       t1.HierarchyId ")
            .Append("   from ")
            .Append("       syNavigationNodes t1,syNavigationNodes t2 ")
            .Append("   where ")
            .Append("       t1.ResourceId=394 and t1.ParentId = t2.HierarchyId and ")
            .Append("       t2.ResourceId = 194 ")
        End With

        dr = db.RunParamSQLDataReader(sb.ToString)
        Dim strSAParentId As String
        While dr.Read()
            strSAParentId = CType(dr("HierarchyId"), Guid).ToString
        End While
        sb.Remove(0, sb.Length)

        With sb
            .Append("   select ")
            .Append("       t1.HierarchyId ")
            .Append("   from ")
            .Append("       syNavigationNodes t1,syNavigationNodes t2 ")
            .Append("   where ")
            .Append("       t1.ResourceId=394 and t1.ParentId = t2.HierarchyId and ")
            .Append("       t2.ResourceId = 191 ")
        End With
        dr = db.RunParamSQLDataReader(sb.ToString)
        Dim strFinAidParentId As String
        While dr.Read()
            strFinAidParentId = CType(dr("HierarchyId"), Guid).ToString
        End While
        sb.Remove(0, sb.Length)

        With sb
            .Append("   select ")
            .Append("       t1.HierarchyId ")
            .Append("   from ")
            .Append("       syNavigationNodes t1,syNavigationNodes t2 ")
            .Append("   where ")
            .Append("       t1.ResourceId=394 and t1.ParentId = t2.HierarchyId and ")
            .Append("       t2.ResourceId = 300 ")
        End With
        dr = db.RunParamSQLDataReader(sb.ToString)
        Dim strFACParentId As String
        While dr.Read()
            strFACParentId = CType(dr("HierarchyId"), Guid).ToString
        End While
        sb.Remove(0, sb.Length)

        If Not dr.IsClosed Then dr.Close()

        'return dataset
        Return strARParentId & "," & strPLParentId & "," & strSAParentId & "," & strFinAidParentId & "," & strFACParentId
    End Function
    Public Sub InsertNodeSP(ByVal AD_ModuleId As Boolean, ByVal AR_ModuleId As Boolean, _
                                ByVal SA_ModuleId As Boolean, ByVal PL_ModuleId As Boolean, _
                                ByVal FA_ModuleId As Boolean, ByVal HR_ModuleId As Boolean, _
                                ByVal FAC_ModuleId As Boolean, ByVal SY_ModuleId As Boolean, _
                                ByVal MaintenanePageId As Integer, ByVal userName As String)
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@AD_ModuleId", AD_ModuleId, SqlDbType.Bit, , ParameterDirection.Input)
        db.AddParameter("@AR_ModuleId", AR_ModuleId, SqlDbType.Bit, , ParameterDirection.Input)
        db.AddParameter("@SA_ModuleId", SA_ModuleId, SqlDbType.Bit, , ParameterDirection.Input)
        db.AddParameter("@PL_ModuleId", PL_ModuleId, SqlDbType.Bit, , ParameterDirection.Input)
        db.AddParameter("@FA_ModuleId", FA_ModuleId, SqlDbType.Bit, , ParameterDirection.Input)
        db.AddParameter("@HR_ModuleId", HR_ModuleId, SqlDbType.Bit, , ParameterDirection.Input)
        db.AddParameter("@FAC_ModuleId", FAC_ModuleId, SqlDbType.Bit, , ParameterDirection.Input)
        db.AddParameter("@SY_ModuleId", SY_ModuleId, SqlDbType.Bit, , ParameterDirection.Input)
        db.AddParameter("@MaintenanePageId", MaintenanePageId, SqlDbType.Int, , ParameterDirection.Input)
        db.AddParameter("@userName", userName, SqlDbType.VarChar, , ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery_SP("dbo.Usp_MoveMaintenancePage_InModules_General")
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Sub
    Public Function InsertNode(ByVal NodeInfo As NavigationNodeInfo, ByVal User As String) As String
        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim Values As New AdvantageCommonValues
        'Dim dr1 As OleDbDataReader
        'Dim intResRelValue As Integer
        Dim ds As New DataSet
        Dim intResId As Integer
        'Dim dr As DataRow
        Dim boolSubMenu As Boolean

        With NodeInfo
            Select Case Mid(.strHierarchyName, 1, 1).ToString.ToLower
                Case Is = "a"
                    .HierarchyIndex = 1
                Case Is = "b"
                    .HierarchyIndex = 2
                Case Is = "c"
                    .HierarchyIndex = 3
                Case Is = "d"
                    .HierarchyIndex = 4
                Case Is = "e"
                    .HierarchyIndex = 5
                Case Is = "f"
                    .HierarchyIndex = 6
                Case Is = "g"
                    .HierarchyIndex = 7
                Case Is = "h"
                    .HierarchyIndex = 8
                Case Is = "i"
                    .HierarchyIndex = 9
                Case Is = "j"
                    .HierarchyIndex = 10
                Case Is = "k"
                    .HierarchyIndex = 11
                Case Is = "l"
                    .HierarchyIndex = 12
                Case Is = "m"
                    .HierarchyIndex = 13
                Case Is = "n"
                    .HierarchyIndex = 14
                Case Is = "o"
                    .HierarchyIndex = 15
                Case Is = "p"
                    .HierarchyIndex = 16
                Case Is = "q"
                    .HierarchyIndex = 17
                Case Is = "r"
                    .HierarchyIndex = 18
                Case Is = "s"
                    .HierarchyIndex = 19
                Case Is = "t"
                    .HierarchyIndex = 20
                Case Is = "u"
                    .HierarchyIndex = 21
                Case Is = "v"
                    .HierarchyIndex = 22
                Case Is = "w"
                    .HierarchyIndex = 23
                Case Is = "x"
                    .HierarchyIndex = 24
                Case Is = "y"
                    .HierarchyIndex = 25
                Case Is = "z"
                    .HierarchyIndex = 26
            End Select

        End With

        Try
            intResId = GetResourceId(NodeInfo.strResourceId)

            If Not intResId = 26 And Not intResId = 191 And Not intResId = 193 And Not intResId = 194 And Not intResId = 189 And Not intResId = 192 And Not intResId = 195 And Not intResId = 300 Then
                boolSubMenu = True
            Else
                boolSubMenu = False
            End If


            'If AD  Is Deleted
            If NodeInfo.StrADDeleted = True Then
                If NodeInfo.strResourceId = 44 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 189, 469)
                ElseIf NodeInfo.strResourceId = 38 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 189, 469)
                ElseIf NodeInfo.strResourceId = 336 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 189, 400)
                ElseIf NodeInfo.strResourceId = 40 Or NodeInfo.strResourceId = 43 Or NodeInfo.strResourceId = 6 Then
                    DeleteChildPopupWindow(NodeInfo.strResourceId, 189)
                ElseIf NodeInfo.strResourceId = 41 Then
                    DeleteProgramPopupWindow(NodeInfo.strResourceId, 189)
                End If
                AddDeleteSubMenuAndSubMenuGroup(boolSubMenu, NodeInfo.strResourceId, 189, NodeInfo.strADParentId)
            End If

            'If AR  Is Deleted
            If NodeInfo.StrARDeleted = True Then
                If NodeInfo.strResourceId = 44 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 26, 469)
                ElseIf NodeInfo.strResourceId = 38 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 26, 469)
                ElseIf NodeInfo.strResourceId = 336 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 26, 400)
                ElseIf NodeInfo.strResourceId = 40 Or NodeInfo.strResourceId = 43 Or NodeInfo.strResourceId = 6 Then
                    DeleteChildPopupWindow(NodeInfo.strResourceId, 26)
                ElseIf NodeInfo.strResourceId = 41 Then
                    DeleteProgramPopupWindow(NodeInfo.strResourceId, 26)
                End If
                AddDeleteSubMenuAndSubMenuGroup(boolSubMenu, NodeInfo.strResourceId, 26, NodeInfo.strARParentId)
            End If

            'If PL  Is Deleted
            If NodeInfo.StrPLDeleted = True Then
                If NodeInfo.strResourceId = 44 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 193, 469)
                ElseIf NodeInfo.strResourceId = 38 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 193, 469)
                ElseIf NodeInfo.strResourceId = 336 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 193, 400)
                ElseIf NodeInfo.strResourceId = 40 Or NodeInfo.strResourceId = 43 Or NodeInfo.strResourceId = 6 Then
                    DeleteChildPopupWindow(NodeInfo.strResourceId, 193)
                ElseIf NodeInfo.strResourceId = 41 Then
                    DeleteProgramPopupWindow(NodeInfo.strResourceId, 193)
                End If
                AddDeleteSubMenuAndSubMenuGroup(boolSubMenu, NodeInfo.strResourceId, 193, NodeInfo.strPLParentId)
            End If

            'If SA  Is Deleted
            If NodeInfo.StrSADeleted = True Then
                If NodeInfo.strResourceId = 44 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 194, 469)
                ElseIf NodeInfo.strResourceId = 38 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 194, 469)
                ElseIf NodeInfo.strResourceId = 336 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 194, 400)
                ElseIf NodeInfo.strResourceId = 40 Or NodeInfo.strResourceId = 43 Or NodeInfo.strResourceId = 6 Then
                    DeleteChildPopupWindow(NodeInfo.strResourceId, 194)
                ElseIf NodeInfo.strResourceId = 41 Then
                    DeleteProgramPopupWindow(NodeInfo.strResourceId, 194)
                End If
                AddDeleteSubMenuAndSubMenuGroup(boolSubMenu, NodeInfo.strResourceId, 194, NodeInfo.strSAParentId)
            End If

            'If HR  Is Deleted
            If NodeInfo.StrHRDeleted = True Then
                If NodeInfo.strResourceId = 44 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 192, 469)
                ElseIf NodeInfo.strResourceId = 38 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 192, 469)
                ElseIf NodeInfo.strResourceId = 336 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 192, 400)
                ElseIf NodeInfo.strResourceId = 40 Or NodeInfo.strResourceId = 43 Or NodeInfo.strResourceId = 6 Then
                    DeleteChildPopupWindow(NodeInfo.strResourceId, 192)
                ElseIf NodeInfo.strResourceId = 41 Then
                    DeleteProgramPopupWindow(NodeInfo.strResourceId, 192)
                End If
                AddDeleteSubMenuAndSubMenuGroup(boolSubMenu, NodeInfo.strResourceId, 192, NodeInfo.strHRParentId)
            End If

            'If FAC  Is Deleted
            If NodeInfo.StrFACDeleted = True Then
                If NodeInfo.strResourceId = 44 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 300, 469)
                ElseIf NodeInfo.strResourceId = 38 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 300, 469)
                ElseIf NodeInfo.strResourceId = 336 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 300, 400)
                ElseIf NodeInfo.strResourceId = 40 Or NodeInfo.strResourceId = 43 Or NodeInfo.strResourceId = 6 Then
                    DeleteChildPopupWindow(NodeInfo.strResourceId, 300)
                ElseIf NodeInfo.strResourceId = 41 Then
                    DeleteProgramPopupWindow(NodeInfo.strResourceId, 300)
                End If
                AddDeleteSubMenuAndSubMenuGroup(boolSubMenu, NodeInfo.strResourceId, 300, NodeInfo.strFACParentId)
            End If

            'If FinAid  Is Deleted
            If NodeInfo.StrFinAidDeleted = True Then
                If NodeInfo.strResourceId = 44 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 191, 469)
                ElseIf NodeInfo.strResourceId = 38 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 191, 469)
                ElseIf NodeInfo.strResourceId = 336 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 191, 400)
                ElseIf NodeInfo.strResourceId = 40 Or NodeInfo.strResourceId = 43 Or NodeInfo.strResourceId = 6 Then
                    DeleteChildPopupWindow(NodeInfo.strResourceId, 191)
                ElseIf NodeInfo.strResourceId = 41 Then
                    DeleteProgramPopupWindow(NodeInfo.strResourceId, 191)
                End If
                AddDeleteSubMenuAndSubMenuGroup(boolSubMenu, NodeInfo.strResourceId, 191, NodeInfo.strFinAidParentId)
            End If

            'If a menu in system is deleted
            If NodeInfo.StrSysDeleted = True Then
                If NodeInfo.strResourceId = 44 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 195, 469)
                ElseIf NodeInfo.strResourceId = 38 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 195, 469)
                ElseIf NodeInfo.strResourceId = 336 Then
                    DeleteSpecialChildPopupWindow(NodeInfo.strResourceId, 195, 400)
                ElseIf NodeInfo.strResourceId = 40 Or NodeInfo.strResourceId = 43 Or NodeInfo.strResourceId = 6 Then
                    DeleteChildPopupWindow(NodeInfo.strResourceId, 195)
                ElseIf NodeInfo.strResourceId = 41 Then
                    DeleteProgramPopupWindow(NodeInfo.strResourceId, 195)
                End If
                AddDeleteSubMenuAndSubMenuGroup(boolSubMenu, NodeInfo.strResourceId, 195, NodeInfo.strSysParentId)
            End If

            'If AD Is Modified
            If NodeInfo.StrADModified = True Then
                InsertMaintenanceMenuInModules(boolSubMenu, NodeInfo.strResourceId, NodeInfo.strADParentId, NodeInfo.HierarchyIndex, intResId, User, NodeInfo.strModDate, 189)
            End If

            If NodeInfo.StrARModified = True Then
                InsertMaintenanceMenuInModules(boolSubMenu, NodeInfo.strResourceId, NodeInfo.strARParentId, NodeInfo.HierarchyIndex, intResId, User, NodeInfo.strModDate, 26)
            End If

            If NodeInfo.StrPLModified = True Then
                InsertMaintenanceMenuInModules(boolSubMenu, NodeInfo.strResourceId, NodeInfo.strPLParentId, NodeInfo.HierarchyIndex, intResId, User, NodeInfo.strModDate, 193)
            End If

            If NodeInfo.StrHRModified = True Then
                InsertMaintenanceMenuInModules(boolSubMenu, NodeInfo.strResourceId, NodeInfo.strHRParentId, NodeInfo.HierarchyIndex, intResId, User, NodeInfo.strModDate, 192)
            End If

            If NodeInfo.StrSAModified = True Then
                InsertMaintenanceMenuInModules(boolSubMenu, NodeInfo.strResourceId, NodeInfo.strSAParentId, NodeInfo.HierarchyIndex, intResId, User, NodeInfo.strModDate, 194)
            End If

            If NodeInfo.StrFACModified = True Then
                InsertMaintenanceMenuInModules(boolSubMenu, NodeInfo.strResourceId, NodeInfo.strFACParentId, NodeInfo.HierarchyIndex, intResId, User, NodeInfo.strModDate, 300)
            End If

            If NodeInfo.StrFinAidModified = True Then
                InsertMaintenanceMenuInModules(boolSubMenu, NodeInfo.strResourceId, NodeInfo.strFinAidParentId, NodeInfo.HierarchyIndex, intResId, User, NodeInfo.strModDate, 191)
            End If

            If NodeInfo.StrSysModified = True Then
                InsertMaintenanceMenuInModules(boolSubMenu, NodeInfo.strResourceId, NodeInfo.strSysParentId, NodeInfo.HierarchyIndex, intResId, User, NodeInfo.strModDate, 195)
            End If
            Return ""
        Finally
        End Try
    End Function
    Public Function InsertStudentNode(ByVal NodeInfo As NavigationNodeInfo, ByVal User As String) As String
        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim Values As New AdvantageCommonValues
        'Dim dr As OleDbDataReader
        'Dim intResRelValue As Integer
        Dim sMessage As String = ""

        With NodeInfo
            Select Case Mid(.strHierarchyName, 1, 1).ToString.ToLower
                Case Is = "a"
                    .HierarchyIndex = 1
                Case Is = "b"
                    .HierarchyIndex = 2
                Case Is = "c"
                    .HierarchyIndex = 3
                Case Is = "d"
                    .HierarchyIndex = 4
                Case Is = "e"
                    .HierarchyIndex = 5
                Case Is = "f"
                    .HierarchyIndex = 6
                Case Is = "g"
                    .HierarchyIndex = 7
                Case Is = "h"
                    .HierarchyIndex = 8
                Case Is = "i"
                    .HierarchyIndex = 9
                Case Is = "j"
                    .HierarchyIndex = 10
                Case Is = "k"
                    .HierarchyIndex = 11
                Case Is = "l"
                    .HierarchyIndex = 12
                Case Is = "m"
                    .HierarchyIndex = 13
                Case Is = "n"
                    .HierarchyIndex = 14
                Case Is = "o"
                    .HierarchyIndex = 15
                Case Is = "p"
                    .HierarchyIndex = 16
                Case Is = "q"
                    .HierarchyIndex = 17
                Case Is = "r"
                    .HierarchyIndex = 18
                Case Is = "s"
                    .HierarchyIndex = 19
                Case Is = "t"
                    .HierarchyIndex = 20
                Case Is = "u"
                    .HierarchyIndex = 21
                Case Is = "v"
                    .HierarchyIndex = 22
                Case Is = "w"
                    .HierarchyIndex = 23
                Case Is = "x"
                    .HierarchyIndex = 24
                Case Is = "y"
                    .HierarchyIndex = 25
                Case Is = "z"
                    .HierarchyIndex = 26
            End Select
        End With

        Try
            'Build The Query
            Dim sb As New StringBuilder
            Try
                'If AR  Is Deleted
                If NodeInfo.StrARDeleted = True Then
                    With sb
                        .Append(" select count(*) from syNavigationNodes where ParentId=?  ")
                    End With
                    'ParentId
                    db.AddParameter("@ParentId", NodeInfo.strARParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    Dim intRecordsPresent As Integer
                    intRecordsPresent = db.RunParamSQLScalar(sb.ToString)
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)

                    If intRecordsPresent = 1 Then
                        sMessage &= "Academic Records" & vbLf
                        Exit Try
                    End If

                    If NodeInfo.strResourceId = 169 Or NodeInfo.strResourceId = 213 Then
                        DeleteStudentChildPopupWindow(NodeInfo.strResourceId, 26)
                    End If

                    With sb
                        .Append(" Delete from syNavigationNodes where ResourceId=? and ParentId=?  ")
                    End With
                    'ResourceId
                    db.AddParameter("@ResourceId", NodeInfo.strResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                    'ParentId
                    db.AddParameter("@ParentId", NodeInfo.strARParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    ''Execute The Query
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)
                End If


                'If AR Is Modified
                If NodeInfo.StrARModified = True Then
                    With sb
                        .Append("Insert syNavigationNodes(HierarchyId,HierarchyIndex,ResourceId,ParentId,ModUser,ModDate)   ")
                        .Append(" Values(?,?,?,?,?,?) ")
                    End With
                    'Hierarchy Id
                    db.AddParameter("@HierarchyId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    'HierarchyIndex
                    db.AddParameter("@HierarchyIndex", NodeInfo.HierarchyIndex, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    ' ResourceId
                    db.AddParameter("@ResourceId", NodeInfo.strResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    'ParentId
                    db.AddParameter("@ParentId", NodeInfo.strARParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    'ModUser
                    db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    'ModDate
                    db.AddParameter("@ModDate", NodeInfo.strModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    ''Execute The Query
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)

                    If NodeInfo.strResourceId = 169 Then
                        InsertStudentPopupChildWindow(NodeInfo.strResourceId, NodeInfo.strARParentId, 345, 346, 347, 376, 0, User, NodeInfo.strModDate, 26)
                    ElseIf NodeInfo.strResourceId = 213 Then
                        InsertStudentPopupChildWindow(NodeInfo.strResourceId, NodeInfo.strARParentId, 287, 288, 0, 0, 0, User, NodeInfo.strModDate, 26)
                    End If
                End If

            Catch ex As Exception
            End Try

            'If PL  Is Deleted
            Try
                If NodeInfo.StrPLDeleted = True Then

                    With sb
                        .Append(" select count(*) from syNavigationNodes where ParentId=?  ")
                    End With
                    'ParentId
                    db.AddParameter("@ParentId", NodeInfo.strPLParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    Dim intRecordsPresent As Integer
                    intRecordsPresent = db.RunParamSQLScalar(sb.ToString)
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)

                    If intRecordsPresent = 1 Then
                        sMessage &= "Placement" & vbLf
                        Exit Try
                    End If

                    If NodeInfo.strResourceId = 169 Or NodeInfo.strResourceId = 213 Then
                        DeleteStudentChildPopupWindow(NodeInfo.strResourceId, 193)
                    End If


                    With sb
                        .Append(" Delete from syNavigationNodes where ResourceId=? and ParentId=?  ")
                    End With
                    'ResourceId
                    db.AddParameter("@ResourceId", NodeInfo.strResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                    'ParentId
                    db.AddParameter("@ParentId", NodeInfo.strPLParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    ''Execute The Query
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)
                End If

                'If PL Is Modified
                If NodeInfo.StrPLModified = True Then
                    With sb
                        .Append("Insert syNavigationNodes(HierarchyId,HierarchyIndex,ResourceId,ParentId,ModUser,ModDate)  ")
                        .Append(" Values(?,?,?,?,?,?) ")
                    End With
                    'Hierarchy Id
                    db.AddParameter("@HierarchyId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    'HierarchyIndex
                    db.AddParameter("@HierarchyIndex", NodeInfo.HierarchyIndex, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    ' ResourceId
                    db.AddParameter("@ResourceId", NodeInfo.strResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    'ParentId
                    db.AddParameter("@ParentId", NodeInfo.strPLParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    'ModUser
                    db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    'ModDate
                    db.AddParameter("@ModDate", NodeInfo.strModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    ''Execute The Query
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)

                    If NodeInfo.strResourceId = 169 Then
                        InsertStudentPopupChildWindow(NodeInfo.strResourceId, NodeInfo.strARParentId, 345, 346, 347, 376, 0, User, NodeInfo.strModDate, 193)
                    ElseIf NodeInfo.strResourceId = 213 Then
                        InsertStudentPopupChildWindow(NodeInfo.strResourceId, NodeInfo.strARParentId, 287, 288, 0, 0, 0, User, NodeInfo.strModDate, 193)
                    End If
                End If
            Catch ex As Exception
            End Try

            'If SA  Is Deleted
            Try
                If NodeInfo.StrSADeleted = True Then

                    With sb
                        .Append(" select count(*) from syNavigationNodes where ParentId=?  ")
                    End With
                    'ParentId
                    db.AddParameter("@ParentId", NodeInfo.strSAParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    Dim intRecordsPresent As Integer
                    intRecordsPresent = db.RunParamSQLScalar(sb.ToString)
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)

                    If intRecordsPresent = 1 Then
                        sMessage &= "Student Accounts" & vbLf
                        Exit Try
                    End If

                    If NodeInfo.strResourceId = 169 Or NodeInfo.strResourceId = 213 Then
                        DeleteStudentChildPopupWindow(NodeInfo.strResourceId, 194)
                    End If


                    With sb
                        .Append(" Delete from syNavigationNodes where ResourceId=? and ParentId=?  ")
                    End With
                    'ResourceId
                    db.AddParameter("@ResourceId", NodeInfo.strResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    'ParentId
                    db.AddParameter("@ParentId", NodeInfo.strSAParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    ''Execute The Query
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)
                End If

                'If SA Is Modified
                If NodeInfo.StrSAModified = True Then
                    With sb
                        .Append("Insert syNavigationNodes(HierarchyId,HierarchyIndex,ResourceId,ParentId,ModUser,ModDate)  ")
                        .Append(" Values(?,?,?,?,?,?) ")
                    End With
                    'Hierarchy Id
                    db.AddParameter("@HierarchyId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    'HierarchyIndex
                    db.AddParameter("@HierarchyIndex", NodeInfo.HierarchyIndex, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    ' ResourceId
                    db.AddParameter("@ResourceId", NodeInfo.strResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    'ParentId
                    db.AddParameter("@ParentId", NodeInfo.strSAParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    'ModUser
                    db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    'ModDate
                    db.AddParameter("@ModDate", NodeInfo.strModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                    ''Execute The Query
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)

                    If NodeInfo.strResourceId = 169 Then
                        InsertStudentPopupChildWindow(NodeInfo.strResourceId, NodeInfo.strARParentId, 345, 346, 347, 376, 0, User, NodeInfo.strModDate, 194)
                    ElseIf NodeInfo.strResourceId = 213 Then
                        InsertStudentPopupChildWindow(NodeInfo.strResourceId, NodeInfo.strARParentId, 287, 288, 0, 0, 0, User, NodeInfo.strModDate, 194)
                    End If
                End If
            Catch ex As Exception
            End Try

            Try
                'If FinAid  Is Deleted
                If NodeInfo.StrFinAidDeleted = True Then

                    With sb
                        .Append(" select count(*) from syNavigationNodes where ParentId=?  ")
                    End With
                    'ParentId
                    db.AddParameter("@ParentId", NodeInfo.strFinAidParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    Dim intRecordsPresent As Integer
                    intRecordsPresent = db.RunParamSQLScalar(sb.ToString)
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)

                    If intRecordsPresent = 1 Then
                        sMessage &= "Financial Aid" & vbLf
                        Exit Try
                    End If

                    If NodeInfo.strResourceId = 169 Or NodeInfo.strResourceId = 213 Then
                        DeleteStudentChildPopupWindow(NodeInfo.strResourceId, 191)
                    End If

                    With sb
                        .Append(" Delete from syNavigationNodes where ResourceId=? and ParentId=?  ")
                    End With
                    'ResourceId
                    db.AddParameter("@ResourceId", NodeInfo.strResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    'ParentId
                    db.AddParameter("@ParentId", NodeInfo.strFinAidParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    ''Execute The Query
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)


                End If

                'If FinAid Is Modified
                If NodeInfo.StrFinAidModified = True Then
                    With sb
                        .Append("Insert syNavigationNodes(HierarchyId,HierarchyIndex,ResourceId,ParentId,ModUser,ModDate)  ")
                        .Append(" Values(?,?,?,?,?,?) ")
                    End With
                    'Hierarchy Id
                    db.AddParameter("@HierarchyId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    'HierarchyIndex
                    db.AddParameter("@HierarchyIndex", NodeInfo.HierarchyIndex, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    ' ResourceId
                    db.AddParameter("@ResourceId", NodeInfo.strResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    'ParentId
                    db.AddParameter("@ParentId", NodeInfo.strFinAidParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    'ModUser
                    db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    'ModDate
                    db.AddParameter("@ModDate", NodeInfo.strModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    ''Execute The Query
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)

                    db.ClearParameters()
                    sb.Remove(0, sb.Length)

                    If NodeInfo.strResourceId = 169 Then
                        InsertStudentPopupChildWindow(NodeInfo.strResourceId, NodeInfo.strARParentId, 345, 346, 347, 376, 0, User, NodeInfo.strModDate, 191)
                    ElseIf NodeInfo.strResourceId = 213 Then
                        InsertStudentPopupChildWindow(NodeInfo.strResourceId, NodeInfo.strARParentId, 287, 288, 0, 0, 0, User, NodeInfo.strModDate, 191)
                    End If
                End If
            Catch ex As Exception
            End Try

            'If Faculty is modified
            Try
                If NodeInfo.StrFACDeleted = True Then
                    With sb
                        .Append(" select count(*) from syNavigationNodes where ParentId=?  ")
                    End With
                    'ParentId
                    db.AddParameter("@ParentId", NodeInfo.strFACParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    Dim intRecordsPresent As Integer
                    intRecordsPresent = db.RunParamSQLScalar(sb.ToString)
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)

                    If intRecordsPresent = 1 Then
                        sMessage &= "Faculty" & vbLf
                        Exit Try
                    End If

                    If NodeInfo.strResourceId = 169 Or NodeInfo.strResourceId = 213 Then
                        DeleteStudentChildPopupWindow(NodeInfo.strResourceId, 300)
                    End If


                    With sb
                        .Append(" Delete from syNavigationNodes where ResourceId=? and ParentId=?  ")
                    End With
                    'ResourceId
                    db.AddParameter("@ResourceId", NodeInfo.strResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    'ParentId
                    db.AddParameter("@ParentId", NodeInfo.strFACParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    ''Execute The Query
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)
                End If

                'If SA Is Modified
                If NodeInfo.StrFACModified = True Then
                    With sb
                        .Append("Insert syNavigationNodes(HierarchyId,HierarchyIndex,ResourceId,ParentId,ModUser,ModDate)  ")
                        .Append(" Values(?,?,?,?,?,?) ")
                    End With
                    'Hierarchy Id
                    db.AddParameter("@HierarchyId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    'HierarchyIndex
                    db.AddParameter("@HierarchyIndex", NodeInfo.HierarchyIndex, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    ' ResourceId
                    db.AddParameter("@ResourceId", NodeInfo.strResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    'ParentId
                    db.AddParameter("@ParentId", NodeInfo.strFACParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    'ModUser
                    db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    'ModDate
                    db.AddParameter("@ModDate", NodeInfo.strModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                    ''Execute The Query
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)

                    If NodeInfo.strResourceId = 169 Then
                        InsertStudentPopupChildWindow(NodeInfo.strResourceId, NodeInfo.strARParentId, 345, 346, 347, 376, 0, User, NodeInfo.strModDate, 300)
                    ElseIf NodeInfo.strResourceId = 213 Then
                        InsertStudentPopupChildWindow(NodeInfo.strResourceId, NodeInfo.strARParentId, 287, 288, 0, 0, 0, User, NodeInfo.strModDate, 300)
                    End If
                End If
            Catch ex As Exception
            End Try

            If Not sMessage = "" Then
                Return sMessage
            End If
        Finally
        End Try
        Return ""
    End Function
    Private Sub AddDeleteSubMenuAndSubMenuGroup(ByVal boolIsSubMenu As Boolean, ByVal intPageResourceId As Integer, ByVal intModuleResourceId As Integer, ByVal strModuleParentId As String)
        Dim sb As New StringBuilder
        Dim guidDelHierarchyId As String
        Dim dsHierarchy As New DataSet
        Dim drHierarchy As DataRow
        Dim intHierarchyCount As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        If boolIsSubMenu = True Then
            'This is to identifiy the group of sub menu and 
            'delete those groups
            With sb
                .Append("   select Distinct t1.HierarchyId from syNavigationNodes t1,syNavigationNodes t2  ")
                .Append("   where t1.HierarchyId = t2.ParentId And t2.ResourceId = ? ")
                .Append("   and t1.ParentId in (select HierarchyId from syNavigationNodes where ResourceId=?) ")
            End With
            db.AddParameter("@ResourceId", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ResourceId", intModuleResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            dsHierarchy = db.RunParamSQLDataSet(sb.ToString)
            For Each drHierarchy In dsHierarchy.Tables(0).Rows
                guidDelHierarchyId = drHierarchy("HierarchyId").ToString
            Next
            sb.Remove(0, sb.Length)
            db.ClearParameters()

            With sb
                .Append("select count(*) from syNavigationNodes where ParentId=? ")
            End With
            db.AddParameter("@ParentId", guidDelHierarchyId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            intHierarchyCount = db.RunParamSQLScalar(sb.ToString)
            sb.Remove(0, sb.Length)
            db.ClearParameters()

            'Delete the child menu for example Job List
            With sb
                .Append(" Delete from syNavigationNodes where ResourceId=? and ParentId ")
                .Append(" in (select ParentId from syNavigationNodes where ResourceId = ? and ParentId in ")
                .Append(" (select t1.HierarchyId from syNavigationNodes t1,syNavigationNodes t2 ")
                .Append("   where t1.ParentId = t2.HierarchyId and t2.ResourceId=?)) ")
            End With
            db.AddParameter("@ResourceId", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ResourceId", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ResourceId", intModuleResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'If there is only once child for this submenu then delete the group for example
            'if jobs list is the only child of jobs in Admissions,then delete jobs list and
            'delete jobs
            If intHierarchyCount = 1 Then
                With sb
                    .Append("Delete from syNavigationNodes where HierarchyId=?")
                End With
                db.AddParameter("@HierarchyId", guidDelHierarchyId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                sb.Remove(0, sb.Length)
                db.ClearParameters()
            End If
        Else
            With sb
                .Append(" Delete from syNavigationNodes where ResourceId=? and ParentId=?  ")
            End With
            db.AddParameter("@ResourceId", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ParentId", strModuleParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End If
    End Sub
    Private Sub InsertMaintenanceMenuInModules(ByVal boolIsSubMenu As Boolean, ByVal intPageResourceId As Integer, ByVal strModuleParentId As String, ByVal strHierarchyIndex As String, ByVal intResId As Integer, ByVal user As String, ByVal ModDate As DateTime, ByVal intModuleResId As Integer)
        Dim guidHierarchyId As String = Guid.NewGuid.ToString
        Dim intSubMenuAlreadyExists As Integer
        Dim sb As New StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        If boolIsSubMenu = True Then
            With sb
                .Append("select count(*) from syNavigationNodes where resourceid=? and ParentId=? ")
            End With
            db.AddParameter("@resourceid", intResId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ParentId", strModuleParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            intSubMenuAlreadyExists = db.RunParamSQLScalar(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            If intSubMenuAlreadyExists < 1 Then
                With sb
                    .Append("Insert into syNavigationNodes(HierarchyId,HierarchyIndex,ResourceId,ParentId,ModUser,ModDate)   ")
                    .Append(" Values(?,?,?,?,?,?) ")
                End With
                'Hierarchy Id
                db.AddParameter("@HierarchyId", guidHierarchyId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'HierarchyIndex
                db.AddParameter("@HierarchyIndex", strHierarchyIndex, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                ' ResourceId
                db.AddParameter("@ResourceId", intResId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                'ParentId
                db.AddParameter("@ParentId", strModuleParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'ModDate
                db.AddParameter("@ModDate", ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                ''Execute The Query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Else
                With sb
                    .Append("select Distinct HierarchyId from syNavigationNodes where resourceid=? and ParentId=? ")
                End With
                db.AddParameter("@resourceid", intResId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ParentId", strModuleParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Dim dr2 As DataRow
                Dim ds2 As New DataSet
                ds2 = db.RunParamSQLDataSet(sb.ToString)
                For Each dr2 In ds2.Tables(0).Rows
                    guidHierarchyId = dr2("Hierarchyid").ToString
                Next
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            End If

            ' The ParentId of the inserted resource will be the HierarchyId of the previously
            ' inserted menu
            With sb
                .Append("Insert into syNavigationNodes(HierarchyId,HierarchyIndex,ResourceId,ParentId,ModUser,ModDate)   ")
                .Append(" Values(?,?,?,?,?,?) ")
            End With
            'Hierarchy Id
            db.AddParameter("@HierarchyId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'HierarchyIndex
            db.AddParameter("@HierarchyIndex", strHierarchyIndex, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ' ResourceId
            db.AddParameter("@ResourceId", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            'ParentId
            db.AddParameter("@ParentId", guidHierarchyId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'ModDate
            db.AddParameter("@ModDate", ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            ''Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        Else
            With sb
                .Append("Insert into syNavigationNodes(HierarchyId,HierarchyIndex,ResourceId,ParentId,ModUser,ModDate)   ")
                .Append(" Values(?,?,?,?,?,?) ")
            End With
            'Hierarchy Id
            db.AddParameter("@HierarchyId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'HierarchyIndex
            db.AddParameter("@HierarchyIndex", strHierarchyIndex, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ' ResourceId
            db.AddParameter("@ResourceId", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            'ParentId
            db.AddParameter("@ParentId", strModuleParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'ModDate
            db.AddParameter("@ModDate", ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            ''Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End If

        'Insert Pop up Windows
        If intPageResourceId = 44 Then
            InsertPopupChildWindow(intPageResourceId, strModuleParentId, 45, 0, 0, 0, 0, user, ModDate, intModuleResId)
        ElseIf intPageResourceId = 38 Then
            InsertPopupChildWindow(intPageResourceId, strModuleParentId, 64, 475, 0, 0, 0, user, ModDate, intModuleResId)
        ElseIf intPageResourceId = 169 Then
            InsertPopupChildWindow(intPageResourceId, strModuleParentId, 345, 346, 347, 376, 0, user, ModDate, intModuleResId)
        ElseIf intPageResourceId = 213 Then
            InsertPopupChildWindow(intPageResourceId, strModuleParentId, 287, 288, 0, 0, 0, user, ModDate, intModuleResId)
        ElseIf intPageResourceId = 40 Then
            InsertPopupChildWindow(intPageResourceId, strModuleParentId, 284, 0, 0, 0, 0, user, ModDate, intModuleResId)
        ElseIf intPageResourceId = 336 Then
            InsertPopupChildWindow(intPageResourceId, strModuleParentId, 341, 0, 0, 0, 0, user, ModDate, intModuleResId)
        ElseIf intPageResourceId = 43 Then
            InsertPopupChildWindow(intPageResourceId, strModuleParentId, 109, 0, 0, 0, 0, user, ModDate, intModuleResId)
        ElseIf intPageResourceId = 6 Then
            InsertPopupChildWindow(intPageResourceId, strModuleParentId, 163, 0, 0, 0, 0, user, ModDate, intModuleResId)
        ElseIf intPageResourceId = 41 Then
            InsertProgramPopupChildWindow(intPageResourceId, strModuleParentId, 61, 42, 68, 224, 379, 520, user, ModDate, intModuleResId)
        End If
    End Sub
    Private Function GetResourceId(ByVal intPageResourceId As Integer) As Integer
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim intResId As Integer
        Dim dr As DataRow

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("       select Distinct t1.ResourceId from syNavigationNodes t1,syNavigationNodes t2 ")
            .Append("       where t1.HierarchyId = t2.ParentId And t2.ResourceId = ? ")
        End With
        db.AddParameter("@resourceid", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)
        For Each dr In ds.Tables(0).Rows
            intResId = dr("ResourceId")
        Next
        sb.Remove(0, sb.Length)
        db.ClearParameters()
        Return intResId
    End Function
    Private Function DeleteChildPopupWindowWhenParentisDeleted(ByVal intPageResourceId As Integer, ByVal intModuleResourceId As Integer) As String
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        'Dim intResId As Integer
        'Dim dr As DataRow

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")



        With sb
            .Append(" Delete from syNavigationNodes where HierarchyId in ")
            'This Top query brings in all resources that are pop ups and direct child of 
            'deleted parent -- for example Programs -- Program Versions -- Setup Start Dates
            '                                                           -- Program Version Definition
            '                                                           -- Set up Fees

            'The Top query will return the HierarchyId of Program Versions
            .Append(" ( ")
            .Append(" select HierarchyId from syNavigationNodes where ParentId in ")
            .Append(" (Select Distinct HierarchyId from syNavigationNodes where ResourceId=? and ParentId ")
            .Append("  in (select ParentId from syNavigationNodes where ResourceId = ? and ParentId in ")
            .Append("   (select t1.HierarchyId from syNavigationNodes t1,syNavigationNodes t2 ")
            .Append(" where t1.ParentId = t2.HierarchyId and t2.ResourceId=?)) ")
            .Append(" ) ")
            .Append(" union  ")
            'The Bottom query will return the HierarchyId of Setup Start Dates,Program Version Definition,
            'Setup Fees
            .Append(" Select HierarchyId from syNavigationNodes where ParentId in ")
            .Append(" ( ")
            .Append(" select Distinct t1.HierarchyId from syNavigationNodes t1 where t1.ParentId = ")
            .Append(" (Select Distinct HierarchyId from syNavigationNodes where ResourceId=? and ParentId ")
            .Append(" in (select ParentId from syNavigationNodes where ResourceId = ? and ParentId in  ")
            .Append("   (select t1.HierarchyId from syNavigationNodes t1,syNavigationNodes t2 ")
            .Append(" where t1.ParentId = t2.HierarchyId and t2.ResourceId=?))  ")
            .Append("  ) ")
            .Append("   ) ")
            .Append(" ) ")


        End With
        db.AddParameter("@resourceid", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@resourceid", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@resourceid", intModuleResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        db.AddParameter("@resourceid", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@resourceid", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@resourceid", intModuleResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)
        db.CloseConnection()
        Return ""
    End Function
    Private Sub InsertPopupChildWindow(ByVal intPageResourceId As Integer, ByVal strModuleParentId As String, ByVal intFirstChild As Integer, ByVal intSecondChild As Integer, ByVal intThirdChild As Integer, ByVal intFourthChild As Integer, ByVal intFifthChild As Integer, ByVal User As String, ByVal ModDate As String, ByVal intModuleResId As Integer)
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim guidParentId As String
        Dim guidHierarchyId As String
        Dim dr As DataRow
        'Dim intHierarchyIndex As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '.Append(" Select Distinct t1.HierarchyId as HierarchyId from syNavigationNodes where ParentId in ")
        '.Append(" (Select Distinct HierarchyId from syNavigationNodes where ResourceId=? and ParentId ")
        '.Append("   in (select ParentId from syNavigationNodes where ResourceId = ? and ParentId in ('")
        '.Append(strModuleParentId)
        '.Append("')")
        If intPageResourceId = 44 Or intPageResourceId = 38 Then
            With sb
                .Append(" select  Distinct HierarchyId from syNavigationNodes where ResourceId=? and ParentId in ")
                .Append(" (select Distinct HierarchyId from syNavigationNodes where ResourceId=469 and ParentId in ( ")
                .Append(" select Top 1 HierarchyId from syNavigationNodes where ResourceId=?)) ")
            End With
            db.AddParameter("@resourceid", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@resourceid", intModuleResId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet(sb.ToString)
            For Each dr In ds.Tables(0).Rows
                guidParentId = dr("HierarchyId").ToString
            Next
            sb.Remove(0, sb.Length)
            db.ClearParameters()
            db.CloseConnection()
        ElseIf intPageResourceId = 336 Then
            With sb
                .Append(" select  Distinct HierarchyId from syNavigationNodes where ResourceId=? and ParentId in ")
                .Append(" (select Distinct HierarchyId from syNavigationNodes where ResourceId=400 and ParentId in ( ")
                .Append(" select Top 1 HierarchyId from syNavigationNodes where ResourceId=?)) ")
            End With
            db.AddParameter("@resourceid", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@resourceid", intModuleResId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet(sb.ToString)
            For Each dr In ds.Tables(0).Rows
                guidParentId = dr("HierarchyId").ToString
            Next
            sb.Remove(0, sb.Length)
            db.ClearParameters()
            db.CloseConnection()
        Else
            With sb
                .Append(" select Distinct HierarchyId from syNavigationNodes where ResourceId=? and ParentId in ")
                .Append(" (select Top 1 HierarchyId from syNavigationNodes where ResourceId=?) ")
            End With
            db.AddParameter("@resourceid", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@resourceid", intModuleResId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet(sb.ToString)
            For Each dr In ds.Tables(0).Rows
                guidParentId = dr("HierarchyId").ToString
            Next
            sb.Remove(0, sb.Length)
            db.ClearParameters()
            db.CloseConnection()
        End If


        guidHierarchyId = Guid.NewGuid.ToString
        If intFirstChild <> 0 Then
            InsertChild(guidHierarchyId, 1, intFirstChild, guidParentId, User, ModDate)
        End If
        guidHierarchyId = Guid.NewGuid.ToString
        If intSecondChild <> 0 Then
            InsertChild(guidHierarchyId, 2, intSecondChild, guidParentId, User, ModDate)
        End If
        guidHierarchyId = Guid.NewGuid.ToString
        If intThirdChild <> 0 Then
            InsertChild(guidHierarchyId, 3, intThirdChild, guidParentId, User, ModDate)
        End If
        guidHierarchyId = Guid.NewGuid.ToString
        If intFourthChild <> 0 Then
            InsertChild(guidHierarchyId, 4, intFourthChild, guidParentId, User, ModDate)
        End If
        guidHierarchyId = Guid.NewGuid.ToString
        If intFifthChild <> 0 Then
            InsertChild(guidHierarchyId, 5, intFifthChild, guidParentId, User, ModDate)
        End If
    End Sub
    Private Sub InsertChild(ByVal strHierarchyId As String, ByVal strHierarchyIndex As String, ByVal intPageResourceId As Integer, ByVal strParentId As String, ByVal ModUser As String, ByVal ModDate As Date)
        Dim sb As New StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("Insert into syNavigationNodes(HierarchyId,HierarchyIndex,ResourceId,ParentId,ModUser,ModDate,IsPopupWindow)   ")
            .Append(" Values(?,?,?,?,?,?,?) ")
        End With
        'Hierarchy Id
        db.AddParameter("@HierarchyId", strHierarchyId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'HierarchyIndex
        db.AddParameter("@HierarchyIndex", strHierarchyIndex, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ' ResourceId
        db.AddParameter("@ResourceId", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        'ParentId
        db.AddParameter("@ParentId", strParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'ModUser
        db.AddParameter("@ModUser", ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.AddParameter("@ModDate", ModDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

        db.AddParameter("@IsPopupWindow", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        ''Execute The Query
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)
        db.CloseConnection()
    End Sub
    Private Sub InsertProgramPopupChildWindow(ByVal intPageResourceId As Integer, ByVal strModuleParentId As String, ByVal intFirstChild As Integer, ByVal intSecondChild As Integer, ByVal intThirdChild As Integer, ByVal intFourthChild As Integer, ByVal intFifthChild As Integer, ByVal intSixthChild As Integer, ByVal User As String, ByVal ModDate As String, ByVal intModuleResId As Integer)
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim guidParentId As String
        Dim guidHierarchyId As String
        Dim dr As DataRow

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Dim intHierarchyIndex As Integer
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            '.Append(" Select Distinct t1.HierarchyId as HierarchyId from syNavigationNodes where ParentId in ")
            '.Append(" (Select Distinct HierarchyId from syNavigationNodes where ResourceId=? and ParentId ")
            '.Append("   in (select ParentId from syNavigationNodes where ResourceId = ? and ParentId in ('")
            '.Append(strModuleParentId)
            '.Append("')")
            '.Append(" )) ")
            .Append(" select Distinct HierarchyId from syNavigationNodes where ResourceId=41 and ParentId in ")
            .Append(" (select Distinct HierarchyId from syNavigationNodes where ResourceId=470 and ParentId in ")
            .Append(" (select Top 1 HierarchyId from syNavigationNodes where ResourceId=?) ")
            .Append(" ) ")
        End With
        db.AddParameter("@resourceid", intModuleResId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)
        For Each dr In ds.Tables(0).Rows
            guidParentId = dr("HierarchyId").ToString
        Next
        sb.Remove(0, sb.Length)
        db.ClearParameters()
        db.CloseConnection()

        guidHierarchyId = Guid.NewGuid.ToString
        If intFirstChild <> 0 Then
            InsertChild(guidHierarchyId, 1, intFirstChild, guidParentId, User, ModDate)
        End If

        guidParentId = guidHierarchyId
        guidHierarchyId = Guid.NewGuid.ToString
        If intSecondChild <> 0 Then
            InsertChild(guidHierarchyId, 2, intSecondChild, guidParentId, User, ModDate)
        End If
        guidHierarchyId = Guid.NewGuid.ToString
        If intThirdChild <> 0 Then
            InsertChild(guidHierarchyId, 3, intThirdChild, guidParentId, User, ModDate)
        End If
        guidHierarchyId = Guid.NewGuid.ToString
        If intFourthChild <> 0 Then
            InsertChild(guidHierarchyId, 4, intFourthChild, guidParentId, User, ModDate)
        End If
        guidHierarchyId = Guid.NewGuid.ToString
        If intFifthChild <> 0 Then
            InsertChild(guidHierarchyId, 5, intFifthChild, guidParentId, User, ModDate)
        End If
        guidHierarchyId = Guid.NewGuid.ToString
        If intSixthChild <> 0 Then
            InsertChild(guidHierarchyId, 6, intSixthChild, guidParentId, User, ModDate)
        End If
    End Sub
    Private Sub InsertStudentPopupChildWindow(ByVal intPageResourceId As Integer, ByVal strModuleParentId As String, ByVal intFirstChild As Integer, ByVal intSecondChild As Integer, ByVal intThirdChild As Integer, ByVal intFourthChild As Integer, ByVal intFifthChild As Integer, ByVal User As String, ByVal ModDate As String, ByVal intModuleResId As Integer)
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim guidParentId As String
        Dim guidHierarchyId As String
        Dim dr As DataRow
        Dim intHierarchyIndex As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            '.Append(" Select Distinct t1.HierarchyId as HierarchyId from syNavigationNodes where ParentId in ")
            '.Append(" (Select Distinct HierarchyId from syNavigationNodes where ResourceId=? and ParentId ")
            '.Append("   in (select ParentId from syNavigationNodes where ResourceId = ? and ParentId in ('")
            '.Append(strModuleParentId)
            '.Append("')")
            '.Append(" )) ")
            .Append(" select Distinct HierarchyId from syNavigationNodes where ResourceId=? and ParentId in ")
            .Append(" (select Distinct HierarchyId from syNavigationNodes where ResourceId=394 and ParentId in ")
            .Append(" (select Top 1 HierarchyId from syNavigationNodes where ResourceId=?) ")
            .Append(" ) ")
        End With
        db.AddParameter("@resourceid", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@resourceid", intModuleResId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)
        For Each dr In ds.Tables(0).Rows
            guidParentId = dr("HierarchyId").ToString
        Next
        sb.Remove(0, sb.Length)
        db.ClearParameters()
        db.CloseConnection()

        guidHierarchyId = Guid.NewGuid.ToString
        If intFirstChild <> 0 Then
            InsertChild(guidHierarchyId, 1, intFirstChild, guidParentId, User, ModDate)
        End If

        guidHierarchyId = Guid.NewGuid.ToString
        If intSecondChild <> 0 Then
            InsertChild(guidHierarchyId, 2, intSecondChild, guidParentId, User, ModDate)
        End If
        guidHierarchyId = Guid.NewGuid.ToString
        If intThirdChild <> 0 Then
            InsertChild(guidHierarchyId, 3, intThirdChild, guidParentId, User, ModDate)
        End If
        guidHierarchyId = Guid.NewGuid.ToString
        If intFourthChild <> 0 Then
            InsertChild(guidHierarchyId, 4, intFourthChild, guidParentId, User, ModDate)
        End If
        guidHierarchyId = Guid.NewGuid.ToString
        If intFifthChild <> 0 Then
            InsertChild(guidHierarchyId, 5, intFifthChild, guidParentId, User, ModDate)
        End If
    End Sub
    Private Function DeleteSpecialChildPopupWindow(ByVal intPageResourceId As Integer, ByVal intModuleResourceId As Integer, ByVal intGroupResourceId As Integer) As String
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        'Dim intResId As Integer
        'Dim dr As DataRow

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'Delete Child Windows
        With sb
            .Append("Delete from syNavigationNodes where HierarchyId in ")
            .Append(" ( ")
            .Append(" Select Distinct HierarchyId from syNavigationNodes where ParentId in ")
            .Append(" (select Distinct HierarchyId from syNavigationNodes where ResourceId = ? and  ParentId in ")
            .Append(" (select Distinct  t2.HierarchyId from syNavigationNodes t1,syNavigationNodes t2 where t1.ResourceId=? and ")
            .Append(" t1.HierarchyId = t2.ParentId and t2.ResourceId=?)) ")
            .Append(" ) ")
        End With
        db.AddParameter("@resourceid", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@resourceid", intModuleResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@resourceid", intGroupResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        Catch ex As System.Exception
        End Try
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Delete Parent
        'With sb
        '    .Append("Delete from syNavigationNodes where HierarchyId in ")
        '    .Append(" ( ")
        '    .Append(" select Distinct HierarchyId from syNavigationNodes where ResourceId = ? and  ParentId in ")
        '    .Append(" (select Distinct  t2.HierarchyId from syNavigationNodes t1,syNavigationNodes t2 where t1.ResourceId=? and  ")
        '    .Append(" t1.HierarchyId = t2.ParentId and t2.ResourceId=?) ")
        '    .Append(" ) ")
        'End With
        'db.AddParameter("@resourceid", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        'db.AddParameter("@resourceid", intModuleResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        'db.AddParameter("@resourceid", intGroupResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        'db.RunParamSQLExecuteNoneQuery(sb.ToString)
        'db.ClearParameters()
        'sb.Remove(0, sb.Length)

        db.CloseConnection()
        Return ""
    End Function
    Private Function DeleteChildPopupWindow(ByVal intPageResourceId As Integer, ByVal intModuleResourceId As Integer) As String
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        'Dim intResId As Integer
        'Dim dr As DataRow

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'Delete Child Windows
        With sb
            .Append("Delete from syNavigationNodes where HierarchyId in ")
            .Append(" ( ")
            .Append(" (select Distinct HierarchyId from syNavigationNodes where ParentId in ")
            .Append(" (select Distinct  t2.HierarchyId from syNavigationNodes t1,syNavigationNodes t2 where t1.ResourceId=? and ")
            .Append(" t1.HierarchyId = t2.ParentId and t2.ResourceId=?)) ")
            .Append(" ) ")
        End With
        db.AddParameter("@resourceid", intModuleResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@resourceid", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        Catch ex As System.Exception
        End Try

        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Delete Parent
        'With sb
        '    .Append("Delete from syNavigationNodes where HierarchyId in ")
        '    .Append(" ( ")
        '    .Append(" (select Distinct  t2.HierarchyId from syNavigationNodes t1,syNavigationNodes t2 where t1.ResourceId=? and  ")
        '    .Append(" t1.HierarchyId = t2.ParentId and t2.ResourceId=?) ")
        '    .Append(" ) ")
        'End With
        'db.AddParameter("@resourceid", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        'db.AddParameter("@resourceid", intModuleResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        'db.RunParamSQLExecuteNoneQuery(sb.ToString)
        'db.ClearParameters()
        'sb.Remove(0, sb.Length)

        db.CloseConnection()
        Return ""
    End Function
    Private Function DeleteStudentChildPopupWindow(ByVal intPageResourceId As Integer, ByVal intModuleResourceId As Integer) As String
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        'Dim intResId As Integer
        'Dim dr As DataRow

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'Delete Child Windows
        With sb
            .Append("Delete from syNavigationNodes where HierarchyId in ")
            .Append(" ( ")
            .Append(" (select Distinct HierarchyId from syNavigationNodes where ParentId in ")
            .Append(" (select Distinct  t3.HierarchyId from syNavigationNodes t1,syNavigationNodes t2,syNavigationNodes t3 where t1.ResourceId=? and ")
            .Append(" t1.HierarchyId = t2.ParentId and t2.ResourceId=394 and t2.HierarchyId=t3.ParentId and t3.ResourceId=?)) ")
            .Append(" ) ")
        End With
        db.AddParameter("@resourceid", intModuleResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@resourceid", intPageResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        Catch ex As System.Exception
        End Try

        db.ClearParameters()
        sb.Remove(0, sb.Length)


        db.CloseConnection()
        Return ""
    End Function
    Private Function DeleteProgramPopupWindow(ByVal intPageResourceId As Integer, ByVal intModuleResourceId As Integer) As String
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        'Dim intResId As Integer
        'Dim dr As DataRow

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("Delete from syNavigationNodes where HierarchyId in ")
            .Append(" ( ")
            .Append(" (select Distinct HierarchyId from syNavigationNodes where ParentId in ")
            .Append(" (select Distinct  t4.HierarchyId from syNavigationNodes t1,syNavigationNodes t2,syNavigationNodes t3,syNavigationNodes t4 where t1.ResourceId=? and ")
            .Append(" t1.HierarchyId = t2.ParentId and t2.ResourceId=470 and t2.HierarchyId=t3.ParentId and t3.ResourceId=41 and t3.HierarchyId=t4.ParentId and t4.ResourceId=61)) ")
            .Append(" ) ")
        End With
        db.AddParameter("@resourceid", intModuleResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        Catch ex As System.Exception
        End Try

        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Delete Child Windows
        With sb
            .Append("Delete from syNavigationNodes where HierarchyId in ")
            .Append(" ( ")
            .Append(" (select Distinct HierarchyId from syNavigationNodes where ParentId in ")
            .Append(" (select Distinct  t3.HierarchyId from syNavigationNodes t1,syNavigationNodes t2,syNavigationNodes t3 where t1.ResourceId=? and ")
            .Append(" t1.HierarchyId = t2.ParentId and t2.ResourceId=470 and t2.HierarchyId=t3.ParentId and t3.ResourceId=41)) ")
            .Append(" ) ")
        End With
        db.AddParameter("@resourceid", intModuleResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        Catch ex As System.Exception
        End Try

        db.ClearParameters()
        sb.Remove(0, sb.Length)


        db.CloseConnection()
        Return ""
    End Function
    Public Function GetNumberOfAdHocReportsUsedByModule(ByVal modul As String, ByVal campusId As String) As Integer
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       Count(*) as NumberOfAdHocReports ")
            .Append("FROM   syUserResources UR, syUserResPermissions URP, ")
            .Append("       (select Distinct ResourceId,Resource from syResources where ResourceTypeId=1) as Modules, syCmpGrpCmps CGC, syCampGrps CG, syCampuses C ")
            .Append("WHERE ")
            .Append("       UR.ResourceId = URP.UserResourceId ")
            .Append("AND    URP.ResourceId = Modules.ResourceId ")
            .Append("AND    CGC.CampGrpId = CG.CampGrpId ")
            .Append("AND    CGC.CampusId = C.CampusId ")
            .Append("AND    URP.Permission = 1 ")
            .Append("AND    Modules.Resource = '" + modul + "' ")
            .Append("AND    C.CampusId='" + campusId + "'")
        End With

        'return number of AdHoc Reports for this module
        Return CType(db.RunParamSQLScalar(sb.ToString), Integer)
    End Function

End Class
