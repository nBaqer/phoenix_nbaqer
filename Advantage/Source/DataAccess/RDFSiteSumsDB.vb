Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' RDFSiteSumsDB.vb
'
' RDFSiteSumsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class RDFSiteSumsDB
    Public Function GetAllRDFSiteSums(ByVal statusSelectIndex As Integer, ByVal moduleId As Integer) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   RSS.RDFSiteSumId, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As StatusSelect, ")
            .Append("         RSS.RDFSiteSumCode, ")
            .Append("         RSS.RDFSiteSumDescrip ")
            .Append("FROM     syRDFSiteSums RSS, syStatuses ST ")
            .Append("WHERE    RSS.StatusId = ST.StatusId ")
            .Append("AND      RSS.ModuleId = ? ")
            '   Conditionally include only Active, Inactive or All Items 
            Select Case statusSelectIndex
                Case 0
                    .Append("AND    ST.Status = 'Active' ")
                    .Append("ORDER BY RSS.RDFSiteSumCode ")
                Case 1
                    .Append("AND    ST.Status = 'Inactive' ")
                    .Append("ORDER BY RSS.RDFSiteSumCode ")
                Case Else
                    .Append("ORDER BY ST.Status, RSS.RDFSiteSumCode ")
            End Select
        End With

        ' Add the moduleId to the parameter list
        db.AddParameter("@ModuleId", moduleId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetRDFSiteSum(ByVal moduleId As Integer, ByVal campusId As String) As String

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   RSS.RDFSiteSumDescrip ")
            .Append("FROM     syRDFSiteSums RSS, syCmpGrpCmps CGC ")
            .Append("WHERE    RSS.CampGrpId = CGC.CampGrpId ")
            .Append("AND      RSS.ModuleId = ? ")
            .Append("AND      ? IN (Select CampusId from syCmpGrpCmps where CampGrpId=RSS.CampGrpId) ")
        End With

        ' Add the moduleId to the parameter list
        db.AddParameter("@ModuleId", moduleId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)

        ' Add the campusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return string
        Dim result As String = db.RunParamSQLScalar(sb.ToString)
        If result Is Nothing Then Return "" Else Return result

    End Function
    Public Function GetRDFSiteSumInfo(ByVal RDFSiteSumId As String) As RDFSiteSumInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT RSS.RDFSiteSumId, ")
            .Append("    RSS.RDFSiteSumCode, ")
            .Append("    RSS.ModuleId, ")
            .Append("    RSS.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=RSS.StatusId) As Status, ")
            .Append("    RSS.RDFSiteSumDescrip, ")
            .Append("    RSS.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=RSS.CampGrpId) As CampGrpDescrip, ")
            .Append("    RSS.ModUser, ")
            .Append("    RSS.ModDate ")
            .Append("FROM  syRDFSiteSums RSS ")
            .Append("WHERE RSS.RDFSiteSumId= ? ")
        End With

        ' Add the RDFSiteSumId to the parameter list
        db.AddParameter("@RDFSiteSumId", RDFSiteSumId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim RDFSiteSumInfo As New RDFSiteSumInfo

        While dr.Read()

            '   set properties with data from DataReader
            With RDFSiteSumInfo
                .RDFSiteSumId = RDFSiteSumId
                .IsInDB = True
                .Code = dr("RDFSiteSumCode")
                .ModuleId = dr("ModuleId")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Description = dr("RDFSiteSumDescrip")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                If Not dr("ModUser") Is System.DBNull.Value Then .ModUser = dr("ModUser")
                If Not dr("ModDate") Is System.DBNull.Value Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return RDFSiteSumInfo
        Return RDFSiteSumInfo

    End Function
    Public Function UpdateRDFSiteSumInfo(ByVal RDFSiteSumInfo As RDFSiteSumInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syRDFSiteSums Set RDFSiteSumId = ?, ModuleId = ?, RDFSiteSumCode = ?, ")
                .Append(" StatusId = ?, RDFSiteSumDescrip = ?, CampGrpId = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE RDFSiteSumId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from syRDFSiteSums where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   RDFSiteSumId
            db.AddParameter("@RDFSiteSumId", RDFSiteSumInfo.RDFSiteSumId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModuleId
            db.AddParameter("@ModuleId", RDFSiteSumInfo.ModuleId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   EnrollmentId
            db.AddParameter("@RDFSiteSumCode", RDFSiteSumInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", RDFSiteSumInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   RDFSiteSumDescrip
            db.AddParameter("@RDFSiteSumDescrip", RDFSiteSumInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If RDFSiteSumInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", RDFSiteSumInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   RDFSiteSumId
            db.AddParameter("@AdmDepositId", RDFSiteSumInfo.RDFSiteSumId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", RDFSiteSumInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddRDFSiteSumInfo(ByVal RDFSiteSumInfo As RDFSiteSumInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT syRDFSiteSums (RDFSiteSumId, ModuleId, RDFSiteSumCode, StatusId, ")
                .Append("   RDFSiteSumDescrip, CampGrpId, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query


            '   RDFSiteSumId
            db.AddParameter("@RDFSiteSumId", RDFSiteSumInfo.RDFSiteSumId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModuleId
            db.AddParameter("@ModuleId", RDFSiteSumInfo.ModuleId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   RDFSiteSumCode
            db.AddParameter("@RDFSiteSumCode", RDFSiteSumInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", RDFSiteSumInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   RDFSiteSumDescrip
            db.AddParameter("@RDFSiteSumDescrip", RDFSiteSumInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If RDFSiteSumInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", RDFSiteSumInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteRDFSiteSumInfo(ByVal RDFSiteSumId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syRDFSiteSums ")
                .Append("WHERE RDFSiteSumId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM syRDFSiteSums WHERE RDFSiteSumId = ? ")
            End With

            '   add parameters values to the query

            '   RDFSiteSumId
            db.AddParameter("@RDFSiteSumId", RDFSiteSumId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   RDFSiteSumId
            db.AddParameter("@RDFSiteSumId", RDFSiteSumId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
End Class