Imports System.Data
Imports System.Data.OleDb
Imports System.Text
Imports System.Xml

Public Class MessageDB

    Public Function PopulateMessageDataList(ByVal ToEmployeeId As Guid, ByVal FromEmployeeId As Guid, ByVal FromStartDate As String, ByVal ToStartDate As String, ByVal MessageStatus As String, Optional ByVal SentDataset As DataSet = Nothing)
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Try
            da = GetMessagesDataList(ToEmployeeId, FromEmployeeId, FromStartDate, ToStartDate, MessageStatus)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        If SentDataset Is Nothing Then
            da.Fill(ds, "Messages")
            Return ds
        Else
            da.Fill(SentDataset, "Messages")
            Return SentDataset
        End If
    End Function
    Public Function PopulateMessageDataList(ByVal ToEmployeeId As Guid, ByVal FromStartDate As String, ByVal ToStartDate As String, ByVal MessageStatus As String, Optional ByVal SentDataset As DataSet = Nothing)
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Try
            da = GetMessagesDataList(ToEmployeeId, FromStartDate, ToStartDate, MessageStatus)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        If SentDataset Is Nothing Then
            da.Fill(ds, "Messages")
            Return ds
        Else
            da.Fill(SentDataset, "Messages")
            Return SentDataset
        End If
    End Function
    Public Function GetMessagesDataList(ByRef ToEmployeeId As Guid, ByVal FromEmployeeId As Guid, ByRef FromStartDate As String, ByRef ToStartDate As String, ByVal MessageStatus As String) As OleDbDataAdapter
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim strMessageListString As New StringBuilder
        Dim objMessageList As New OleDbDataAdapter
        With strMessageListString
            .Append("select a.*, b.FirstName as ToFirstName, b.LastName as ToLastName, c.FirstName as FromFirstName, c.LastName as FromLastName from cmMessage a, hrEmployees b, hrEmployees c")
            .Append(" where (a.ToId = b.EmpId)")
            .Append(" and (a.FromId = c.EmpId)")
            .Append(" and (a.ToId = ?) ")
            .Append(" and (a.FromId = ?) ")
            ' Added by Corey - Do not get records that are logically marked as delete
            .Append(" and (a.DeletedByFrom = ?) ")
            If FromStartDate <> "" Then
                .Append(" and (SentDate >= ?) ")
            End If
            If ToStartDate <> "" Then
                .Append(" and (SentDate <= ?) ")
            End If
            Select Case MessageStatus
                Case "Unread"
                    .Append(" and (MessageRead = ?)")
                Case "Read"
                    .Append(" and (MessageRead = ?)")
                Case "All"
                    ' Do no filtering
            End Select
        End With
        db.AddParameter("@ToEmployeeId", ToEmployeeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@FromEmployeeId", FromEmployeeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@DeletedByFrom", False, DataAccess.OleDbDataType.OleDbBoolean, 1, ParameterDirection.Input)
        If FromStartDate <> "" Then
            db.AddParameter("@StartDate", CDate(FromStartDate), DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
        End If
        If ToStartDate <> "" Then
            db.AddParameter("@StartDate2", CDate(ToStartDate), DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
        End If
        Select Case MessageStatus
            Case "Unread"
                db.AddParameter("@MessageRead", False, DataAccess.OleDbDataType.OleDbBoolean, 1, ParameterDirection.Input)
            Case "Read"
                db.AddParameter("@MessageRead", True, DataAccess.OleDbDataType.OleDbBoolean, 1, ParameterDirection.Input)
            Case "All"
                ' do nothing
        End Select
        db.OpenConnection()
        Try
            objMessageList = db.RunParamSQLDataAdapter(strMessageListString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.CloseConnection()
        db.ClearParameters()
        Return objMessageList
    End Function


    Public Function GetMessagesDataList(ByRef ToEmployeeId As Guid, ByRef FromStartDate As String, ByRef ToStartDate As String, ByVal MessageStatus As String) As OleDbDataAdapter
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim strMessageListString As New StringBuilder
        Dim objMessageList As New OleDbDataAdapter
        With strMessageListString
            .Append("select a.*, b.FirstName as ToFirstName, b.LastName as ToLastName, c.FirstName as FromFirstName, c.LastName as FromLastName from cmMessage a, hrEmployees b, hrEmployees c")
            .Append(" where (a.ToId = b.EmpId)")
            .Append(" and (a.FromId = c.EmpId)")
            .Append(" and (a.ToId = ?) ")
            ' Added by Corey - Do not get records that are logically marked as delete
            .Append(" and (a.DeletedByTo = ?) ")
            If FromStartDate <> "" Then
                .Append(" and (SentDate >= ?) ")
            End If
            If ToStartDate <> "" Then
                .Append(" and (SentDate <= ?) ")
            End If
            Select Case MessageStatus
                Case "Unread"
                    .Append(" and (MessageRead = ?)")
                Case "Read"
                    .Append(" and (MessageRead = ?)")
                Case "All"
                    ' Do no filtering
            End Select
        End With

        db.AddParameter("@ToEmployeeId", ToEmployeeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@DeletedByTo", False, DataAccess.OleDbDataType.OleDbBoolean, 1, ParameterDirection.Input)
        If FromStartDate <> "" Then
            db.AddParameter("@StartDate", CDate(FromStartDate), DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
        End If
        If ToStartDate <> "" Then
            db.AddParameter("@StartDate2", CDate(ToStartDate), DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
        End If
        Select Case MessageStatus
            Case "Unread"
                db.AddParameter("@MessageRead", False, DataAccess.OleDbDataType.OleDbBoolean, 1, ParameterDirection.Input)
            Case "Read"
                db.AddParameter("@MessageRead", True, DataAccess.OleDbDataType.OleDbBoolean, 1, ParameterDirection.Input)
            Case "All"
                ' do nothing
        End Select
        db.OpenConnection()
        Try
            objMessageList = db.RunParamSQLDataAdapter(strMessageListString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.CloseConnection()
        db.ClearParameters()
        Return objMessageList
    End Function
    Public Function AddMessage(ByVal Message As MessageInfo)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("INSERT INTO cmMessage")
            .Append("(MessageId, ToId, FromId, SentDate, Subject, Message, ParentMessageId, ChildMessageId, DeletedByTo, DeletedByFrom) ")
            .Append("VALUES(?,?,?,?,?,?,?,?,?,?)")
        End With
        db.AddParameter("@messageid", Message.MessageId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@toid", Message.ToId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@fromid", Message.FromId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@sentdate", Now, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@subject", Message.Subject, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@message", Message.Message, DataAccess.OleDbDataType.OleDbString, 250, ParameterDirection.Input)
        db.AddParameter("@parentmessageid", Message.ParentMessageId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@childmessageid", Message.ChildMessageId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@todeleted", False, DataAccess.OleDbDataType.OleDbBoolean, 1, ParameterDirection.Input)
        db.AddParameter("@fromdeleted", False, DataAccess.OleDbDataType.OleDbBoolean, 1, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
    End Function
    Public Function UpdateMessage(ByVal Message As MessageInfo)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("UPDATE cmMessage SET ")
            .Append("ToId = ?")
            .Append(",FromId = ?")
            .Append(",SentDate = ?")
            .Append(",Subject = ?")
            .Append(",Message = ?")
            .Append(",ParentMessageId = ?")
            .Append(",ChildMessageId = ?")
            .Append(" WHERE MessageId = ? ")
        End With

        db.AddParameter("@toid", Message.ToId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@fromid", Message.FromId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@sentdate", Now, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@subject", Message.Subject, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@message", Message.Message, DataAccess.OleDbDataType.OleDbString, 250, ParameterDirection.Input)
        db.AddParameter("@parentmessageid", Message.ParentMessageId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@childmessageid", Message.ChildMessageId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@messageid", Message.MessageId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Function
    Public Function UpdateDeleteMessage(ByVal MessageId As Guid, ByVal WhoDeleted As String)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("UPDATE cmMessage SET ")
            If WhoDeleted = "To" Then
                .Append("DeletedByTo = ?")
            Else
                .Append("DeletedByFrom = ?")
            End If
            .Append(" WHERE MessageId = ? ")
        End With
        db.AddParameter("@tofromdeleted", True, DataAccess.OleDbDataType.OleDbBoolean, 1, ParameterDirection.Input)
        db.AddParameter("@messageid", MessageId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Function
    Public Function UpdateReadMessage(ByVal MessageId As Guid, Optional ByVal MessageStatus As Boolean = True)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("UPDATE cmMessage SET ")
            .Append("MessageRead = ?")
            .Append(" WHERE MessageId = ? ")
        End With
        db.AddParameter("@messageread", MessageStatus, DataAccess.OleDbDataType.OleDbBoolean, 1, ParameterDirection.Input)
        db.AddParameter("@messageid", MessageId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Function
    Public Function UpdateChildMessage(ByVal ParentMessageId As Guid, ByVal ChildMessageId As Guid)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("UPDATE cmMessage SET ")
            .Append(" ChildMessageId = ?")
            .Append(" WHERE MessageId = ? ")
        End With
        db.AddParameter("@childmessageid", ChildMessageId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@messageid", ParentMessageId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Function

    Public Function ChangeReadStatus(ByVal MessageId As Guid)
        ' Read the message
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder
        With strSQLString
            .Append("select MessageId, MessageRead From cmMessage")
            .Append(" where (MessageId = ?) ")
        End With
        db.AddParameter("@MessageId", MessageId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
            da.Fill(ds, "DatabaseMessage")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)
        db.ClearParameters()

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("DatabaseMessage")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("MessageId")}
        End With
        If ds.Tables("DatabaseMessage").Rows.Count > 0 Then
            For Each row In tbl.Rows
                If row("MessageRead") = True Then
                    UpdateReadMessage(MessageId, False)
                Else
                    UpdateReadMessage(MessageId, True)
                End If
            Next
        Else
            'throw an error -- we did not find the message
        End If
    End Function

    Public Function CheckDeleteMessage(ByVal MessageId As Guid, ByVal WhoDeleted As String)
        ' Read the message
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder
        With strSQLString
            .Append("select MessageId, DeletedByTo, DeletedByFrom From cmMessage")
            .Append(" where (MessageId = ?) ")
        End With
        Dim d As String = XmlConvert.ToString(MessageId)
        db.AddParameter("@MessageId", MessageId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
            da.Fill(ds, "DatabaseMessage")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)
        db.ClearParameters()

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Dim tbl As DataTable
        Dim row As DataRow
        If WhoDeleted = "To" Then
            tbl = ds.Tables("DatabaseMessage")
            With tbl
                .PrimaryKey = New DataColumn() {.Columns("MessageId")}
            End With
            If ds.Tables("DatabaseMessage").Rows.Count > 0 Then
                For Each row In tbl.Rows
                    If row("DeletedByFrom") = True Then
                        'Not sure yet if we want to physically delete records -- if in the future we do remove comment
                        'DeleteMessage(MessageId)
                    Else
                        UpdateDeleteMessage(MessageId, "To")
                    End If
                Next
            Else
                'throw an error -- we did not find the message
            End If
        Else
            tbl = ds.Tables("DatabaseMessage")
            With tbl
                .PrimaryKey = New DataColumn() {.Columns("MessageId")}
            End With
            If ds.Tables("DatabaseMessage").Rows.Count > 0 Then
                For Each row In tbl.Rows
                    If row("DeletedByTo") = True Then
                        'Not sure yet if we want to physically delete records -- if in the future we do remove comment
                        'DeleteMessage(MessageId)
                    Else
                        UpdateDeleteMessage(MessageId, "From")
                    End If
                Next
            Else
                'throw an error -- we did not find the message
            End If
        End If
    End Function
    Public Function DeleteMessage(ByVal MessageId As Guid)

        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("DELETE FROM cmMessage WHERE MessageId = ?")
        End With
        db.AddParameter("@messageid", MessageId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Function
    Public Function GetDropDownList(ByVal EmployeeId As Guid) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strSQLString As New StringBuilder
        Dim da As New OleDbDataAdapter
        db.OpenConnection()
        With strSQLString
            .Append("SELECT UserId, FullName ")
            .Append("FROM syUsers ")
        End With

        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "EmployeeList")
        Catch ex As System.Exception

        End Try
        strSQLString.Remove(0, strSQLString.Length)
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetOneMessage(ByVal MessageId As Guid)
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Dim da2 As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder
        With strSQLString
            .Append("select a.*, b.FirstName as ToFirstName, b.LastName as ToLastName, c.FirstName as FromFirstName, c.LastName as FromLastName from cmMessage a, hrEmployees b, hrEmployees c")
            .Append(" where (a.ToId = b.EmpId)")
            .Append(" and (a.FromId = c.EmpId)")
            .Append(" and (a.MessageId = ?) ")
        End With

        db.AddParameter("@MessageId", MessageId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
            da.Fill(ds, "OriginalMessage")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)
        db.ClearParameters()
        'We need to read the record found to see if there is a parent Id - if so then we need to get the previous message
        Dim tbl As DataTable
        Dim row As DataRow
        Dim ParentGuid As Guid
        tbl = ds.Tables("OriginalMessage")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("MessageId")}
        End With
        If ds.Tables("OriginalMessage").Rows.Count > 0 Then
            For Each row In tbl.Rows
                If Left(System.Convert.ToString(row("ParentMessageId")), 6) <> "000000" Then
                    ParentGuid = row("ParentMessageId")
                    With strSQLString
                        .Append("select a.*, b.FirstName as ToFirstName, b.LastName as ToLastName, c.FirstName as FromFirstName, c.LastName as FromLastName from cmMessage a, hrEmployees b, hrEmployees c")
                        .Append(" where (a.ToId = b.EmpId)")
                        .Append(" and (a.FromId = c.EmpId)")
                        .Append(" and (a.MessageId = ?) ")
                    End With

                    db.AddParameter("@MessageId", ParentGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                    db.OpenConnection()
                    Try
                        da = db.RunParamSQLDataAdapter(strSQLString.ToString)
                        da.Fill(ds, "PreviousMessage")
                    Catch ex As System.Exception
                        'Redirect to error page.
                        Throw New Exception(ex.Message, ex)
                    End Try
                    strSQLString.Remove(0, strSQLString.Length)
                End If
            Next
        Else
            'No Original message was found - error
        End If
        ' end read
        db.CloseConnection()
        db.ClearParameters()
        Return ds
    End Function
End Class
