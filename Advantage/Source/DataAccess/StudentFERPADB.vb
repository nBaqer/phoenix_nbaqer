Imports FAME.Advantage.Common

Public Class StudentFERPADB

    Public Function GetFERPAcategories(ByVal showActiveOnly As String, ByVal campusId As String) As DataTable

        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
        Dim intActive As Integer = -1
        If showActiveOnly = "True" Then
            intActive = 1
        ElseIf showActiveOnly = "False" Then
            intActive = 0
        End If
        db.AddParameter("@showActiveOnly", intActive, SqlDbType.Int, , ParameterDirection.Input)
        db.AddParameter("@campusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        'add cutOff date parameter


        ds = db.RunParamSQLDataSet_SP("dbo.usp_GetFERPACategories")

        Try
            Return ds.Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try



    End Function
    Public Function GetFERPAEntities(ByVal showActiveOnly As String, ByVal campusId As String) As DataTable

        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
        Dim intActive As Integer = -1
        If showActiveOnly = "True" Then
            intActive = 1
        ElseIf showActiveOnly = "False" Then
            intActive = 0
        End If
        db.AddParameter("@showActiveOnly", intActive, SqlDbType.Int, , ParameterDirection.Input)
        db.AddParameter("@campusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        'add cutOff date parameter


        ds = db.RunParamSQLDataSet_SP("dbo.usp_GetFERPAEntities")

        Try
            Return ds.Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try



    End Function

    Public Function GetFERPAInfo(ByVal ferpaId As String) As FERPAInfo

        '   connect to the database
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")


        db.AddParameter("@FERPACategoryID", New Guid(ferpaId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        'add cutOff date parameter


        Dim dr As SqlDataReader = db.RunParamSQLDataReader_SP("usp_GetFERPAInfo")

        Dim ferpaInfo As New FERPAInfo

        While dr.Read()

            '   set properties with data from DataReader
            With ferpaInfo
                .IsInDB = True
                .FERPAId = ferpaId
                If Not (dr("FERPACategoryCode") Is System.DBNull.Value) Then .Code = dr("FERPACategoryCode")
                If Not (dr("StatusId") Is System.DBNull.Value) Then .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Name = dr("FERPACategoryDescrip")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")

            End With

        End While

        'Close Connection
        db.CloseConnection()

        '   Return BankInfo
        Return ferpaInfo

    End Function
    Public Function GetFERPAEntityInfo(ByVal ferpaId As String) As FERPAInfo

        '   connect to the database
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")


        db.AddParameter("@FERPAEntityID", New Guid(ferpaId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        'add cutOff date parameter


        Dim dr As SqlDataReader = db.RunParamSQLDataReader_SP("usp_GetFERPAEntityInfo")

        Dim ferpaInfo As New FERPAInfo

        While dr.Read()

            '   set properties with data from DataReader
            With ferpaInfo
                .IsInDB = True
                .FERPAId = ferpaId
                If Not (dr("FERPAEntityCode") Is System.DBNull.Value) Then .Code = dr("FERPAEntityCode")
                If Not (dr("StatusId") Is System.DBNull.Value) Then .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Name = dr("FERPAEntityDescrip")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")

            End With

        End While

        'Close Connection
        db.CloseConnection()

        '   Return BankInfo
        Return ferpaInfo

    End Function

    Public Function AddFERPAInfo(ByVal ferpa As FERPAInfo, ByVal FERPAPages As String()) As String

        Dim rtn As String = String.Empty
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            db.AddParameter("@FERPACategoryID", New Guid(ferpa.FERPAId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@FERPACategoryCode", ferpa.Code, SqlDbType.VarChar, 12, ParameterDirection.Input)
            db.AddParameter("@StatusId", New Guid(ferpa.StatusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@FERPACategoryDescrip", ferpa.Name, SqlDbType.VarChar, 50, ParameterDirection.Input)
            db.AddParameter("@CampGrpId", New Guid(ferpa.CampGrpId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ModUser", ferpa.ModUser, SqlDbType.VarChar, 50, ParameterDirection.Input)
            db.AddParameter("@moddate", DateTime.Now.ToShortDateString, SqlDbType.DateTime, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("dbo.usp_FERPAInfo_Insert", Nothing)
        Catch ex As System.Exception
            Return "Some error Occured"
        Finally
            db.ClearParameters()
            db.CloseConnection()
            AddFERPAPages(ferpa.FERPAId, FERPAPages)
        End Try
        Return rtn

    End Function
    Public Function AddFERPAEntityInfo(ByVal ferpa As FERPAInfo) As String

        Dim rtn As String = String.Empty
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            db.AddParameter("@FERPAEntityID", New Guid(ferpa.FERPAId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@FERPAEntityCode", ferpa.Code, SqlDbType.VarChar, 12, ParameterDirection.Input)
            db.AddParameter("@StatusId", New Guid(ferpa.StatusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@FERPAEntityDescrip", ferpa.Name, SqlDbType.VarChar, 50, ParameterDirection.Input)
            db.AddParameter("@CampGrpId", New Guid(ferpa.CampGrpId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ModUser", ferpa.ModUser, SqlDbType.VarChar, 50, ParameterDirection.Input)
            db.AddParameter("@moddate", DateTime.Now.ToShortDateString, SqlDbType.DateTime, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("dbo.usp_FERPAEntityInfo_Insert", Nothing)
        Catch ex As System.Exception
            Return "Some error Occured"
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return rtn

    End Function

    Public Sub AddFERPAPages(ByVal FERPACategoryID As String, ByVal FERPAPages As String())


        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            Dim i As Integer
            For i = 0 To FERPAPages.Length - 1
                db.AddParameter("@FERPACategoryID", New Guid(FERPACategoryID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@ResourceID", CType(FERPAPages.GetValue(i), Integer), SqlDbType.SmallInt, , ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery_SP("dbo.usp_FERPAPage_Insert", Nothing)
                db.ClearParameters()
            Next
        Catch ex As System.Exception

        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try



    End Sub

    Public Function UpdateFERPAInfo(ByVal ferpa As FERPAInfo, ByVal FERPAPages As String()) As String


        Dim rtn As String = String.Empty
        DeleteFERPAPage(ferpa.FERPAId)
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            db.AddParameter("@FERPACategoryID", New Guid(ferpa.FERPAId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@FERPACategoryCode", ferpa.Code, SqlDbType.VarChar, 12, ParameterDirection.Input)
            db.AddParameter("@StatusId", New Guid(ferpa.StatusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@FERPACategoryDescrip", ferpa.Name, SqlDbType.VarChar, 50, ParameterDirection.Input)
            db.AddParameter("@CampGrpId", New Guid(ferpa.CampGrpId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ModUser", ferpa.ModUser, SqlDbType.VarChar, 50, ParameterDirection.Input)
            db.AddParameter("@moddate", DateTime.Now.ToShortDateString, SqlDbType.DateTime, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("dbo.usp_FERPAInfo_Update", Nothing)
        Catch ex As System.Exception
            Return "Some error Occured"
        Finally
            db.ClearParameters()
            db.CloseConnection()
            AddFERPAPages(ferpa.FERPAId, FERPAPages)
        End Try
        Return rtn

    End Function
    Public Function UpdateFERPAEntityInfo(ByVal ferpa As FERPAInfo) As String


        Dim rtn As String = String.Empty
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            db.AddParameter("@FERPAEntityID", New Guid(ferpa.FERPAId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@FERPAEntityCode", ferpa.Code, SqlDbType.VarChar, 12, ParameterDirection.Input)
            db.AddParameter("@StatusId", New Guid(ferpa.StatusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@FERPAEntityDescrip", ferpa.Name, SqlDbType.VarChar, 50, ParameterDirection.Input)
            db.AddParameter("@CampGrpId", New Guid(ferpa.CampGrpId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ModUser", ferpa.ModUser, SqlDbType.VarChar, 50, ParameterDirection.Input)
            db.AddParameter("@moddate", DateTime.Now.ToShortDateString, SqlDbType.DateTime, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("dbo.usp_FERPAEntityInfo_Update", Nothing)
        Catch ex As System.Exception
            Return "Some error Occured"
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return rtn

    End Function
    Public Function AddFERPAPolicyInfo(ByVal ferpa As FERPAPolicyInfo) As String


        Dim rtn As String = String.Empty
        DeleteFERPAPolicyPage(ferpa.FERPAEntityId, ferpa.StudentId)

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        If Not ferpa.FERPACategoryId Is Nothing Then
            Try
                Dim i As Integer
                For i = 0 To ferpa.FERPACategoryId.Length - 1

                    db.AddParameter("@FERPAEntityID", New Guid(ferpa.FERPAEntityId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                    db.AddParameter("@FERPACategoryID", New Guid(ferpa.FERPACategoryId(i)), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                    db.AddParameter("@StudentId", New Guid(ferpa.StudentId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                    db.AddParameter("@ModUser", ferpa.ModUser, SqlDbType.VarChar, 50, ParameterDirection.Input)
                    db.AddParameter("@moddate", DateTime.Now.ToShortDateString, SqlDbType.DateTime, , ParameterDirection.Input)
                    db.RunParamSQLExecuteNoneQuery_SP("dbo.usp_FERPAPolicyInfo_Insert", Nothing)
                    db.ClearParameters()

                Next
            Catch ex As System.Exception
                Return "Some error Occured"
            Finally
                db.ClearParameters()
                db.CloseConnection()
            End Try
        End If
        Return rtn


    End Function

    Public Function DeleteFERPAInfo(ByVal ferpaID As String) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        DeleteFERPAPage(ferpaID)
        Dim rtn As String = String.Empty
        Dim db As New SQLDataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            db.AddParameter("@FERPACategoryID", New Guid(ferpaID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("dbo.usp_FERPAInfo_Delete", Nothing)
        Catch ex As System.Exception
            Return "Some error Occured"
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return rtn

    End Function
    Public Function DeleteFERPAEntityInfo(ByVal ferpaID As String) As String

        Dim rtn As String = String.Empty
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            db.AddParameter("@FERPAEntityID", New Guid(ferpaID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("dbo.usp_FERPAEntityInfo_Delete", Nothing)
        Catch ex As System.Exception
            Return "Some error Occured"
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return rtn

    End Function
    Public Sub DeleteFERPAPage(ByVal ferpaID As String)


        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            db.AddParameter("@FERPACategoryID", New Guid(ferpaID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("dbo.usp_FERPAPage_Delete", Nothing)
        Catch ex As System.Exception

        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try


    End Sub
    Public Function DeleteFERPAPolicyPage(ByVal ferpaEntityID As String, ByVal studentID As String) As String

        Dim rtn As String = String.Empty
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            db.AddParameter("@FERPAEntityID", New Guid(ferpaEntityID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@studentID", New Guid(studentID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("dbo.usp_FERPAPolicyInfo_Delete", Nothing)
        Catch ex As System.Exception
            Return "Some error Occured"
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return rtn

    End Function
    Public Function GetFERPAPages(ByVal FERPACategoryID As String) As DataTable

        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@FERPACategoryID", New Guid(FERPACategoryID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        'add cutOff date parameter


        ds = db.RunParamSQLDataSet_SP("dbo.usp_GetFERPAPages")

        Try
            Return ds.Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try



    End Function
    Public Function GetStudentPagesByModulePageName() As DataTable
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" select Distinct t1.RelatedResourceId as ResourceId,")
            .Append(" t2.Resource as Resource ")
            .Append("  from syAdvantageResourceRelations t1,syResources t2 ")
            .Append(" where t1.ResourceId = 394 And ")
            .Append(" t1.RelatedResourceId = t2.ResourceId And t2.ResourceTypeId <> 1 and t2.ResourceId <> 625 ")
            .Append(" order by t2.Resource ")
        End With

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
    End Function
    Public Function IsStudentPage(ByVal ResourceId As String) As DataTable
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" select Distinct t1.RelatedResourceId as ResourceId,")
            .Append(" t2.Resource as Resource ")
            .Append("  from syAdvantageResourceRelations t1,syResources t2 ")
            .Append(" where t1.ResourceId = 394 And ")
            .Append(" t1.RelatedResourceId = t2.ResourceId And t2.ResourceTypeId <> 1 and t2.ResourceId <> 625 ")
            .Append(" and t1.RelatedResourceId = ? ")
            .Append(" order by t2.Resource ")
        End With

        'return dataset
        db.AddParameter("@ResourceId", ResourceId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
    End Function
    Public Function GetFERPAPolicy(ByVal FERPAEntityID As String, ByVal StudentId As String) As DataTable
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@FERPAEntityID", New Guid(FERPAEntityID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@StudentId", New Guid(StudentId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        'add cutOff date parameter


        ds = db.RunParamSQLDataSet_SP("dbo.usp_GetFERPAPolicy")

        Try
            Return ds.Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try



    End Function
    Public Function GetFERPAPermission(ByVal resourceId As Integer, ByVal StudentId As String) As DataTable
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@ResourceID", resourceId, SqlDbType.SmallInt, , ParameterDirection.Input)
        db.AddParameter("@StudentId", New Guid(StudentId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        'add cutOff date parameter


        ds = db.RunParamSQLDataSet_SP("dbo.usp_GetFERPAPermissions")

        Try

            If ds is Nothing OrElse ds.Tables is Nothing Then
                Return Nothing
            End If
             
            If ds.Tables.Count = 0 Then
                Return Nothing
            End If

            Return ds.Tables(0)

        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try



    End Function
End Class

