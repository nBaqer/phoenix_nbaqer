'Option Strict On

Imports System.Data
Imports System.Data.OleDb
Imports System.Text
Imports System.Xml
Imports FAME.Advantage.Common

Public Class ActivityAssignmentDB
    Inherits System.ComponentModel.Component

#Region "Activity Assignment Database Section"

    Public Sub AddActivityAssignment(ByVal ActivityAssignment As ActivityAssignmentInfo)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("INSERT INTO cmActivityAssignment")
            .Append("(ActivityAssignmentId, EmployeeId, StudentId, ActivityId, Subject, DueDate,")
            .Append(" StartTime, EndTime, ActivityStatusId, PriorityId, Comments, DateCreated, ModUser, ModDate)")
            .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
        End With
        ' Set the DateCreated time to now
        ActivityAssignment.DateCreated = Now
        db.AddParameter("@activityassignmentid", ActivityAssignment.ActivityAssignmentId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@employeeid", ActivityAssignment.EmployeeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@studentid", ActivityAssignment.StudentId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@activityid", ActivityAssignment.ActivityId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@subject", ActivityAssignment.Subject, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        If Year(ActivityAssignment.DueDate) <> 1900 Then
            db.AddParameter("@duedate", ActivityAssignment.DueDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@duedate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        End If
        If ActivityAssignment.StartTime.ToString <> System.Guid.Empty.ToString Then
            db.AddParameter("@starttime", ActivityAssignment.StartTime, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Else
            db.AddParameter("@starttime", System.DBNull.Value, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        If ActivityAssignment.EndTime.ToString <> System.Guid.Empty.ToString Then
            db.AddParameter("@endtime", ActivityAssignment.EndTime, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Else
            db.AddParameter("@endtime", System.DBNull.Value, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        db.AddParameter("@activitystatusid", ActivityAssignment.ActivityStatusId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        If ActivityAssignment.PriorityId.ToString <> System.Guid.Empty.ToString Then
            db.AddParameter("@priorityid", ActivityAssignment.PriorityId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Else
            db.AddParameter("@priorityid", System.DBNull.Value, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        db.AddParameter("@comments", ActivityAssignment.Comments, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@datecreated", ActivityAssignment.DateCreated, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@ModUser", ActivityAssignment.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModDate", Now, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Sub

    Public Sub UpdateActivityAssignment(ByVal ActivityAssignment As ActivityAssignmentInfo)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("UPDATE cmActivityAssignment SET ")
            .Append("EmployeeId = ?")
            .Append(",StudentId = ?")
            .Append(",ActivityId = ?")
            .Append(",Subject = ?")
            .Append(",DueDate = ?")
            .Append(",StartTime = ?")
            .Append(",EndTime = ?")
            .Append(",ActivityStatusId = ?")
            .Append(",PriorityId = ?")
            .Append(",Comments = ?")
            .Append(",ModUser = ?")
            .Append(",ModDate = ?")
            .Append(" WHERE ActivityAssignmentId = ? ")
        End With

        db.AddParameter("@employeeid", ActivityAssignment.EmployeeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@studentid", ActivityAssignment.StudentId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@activityid", ActivityAssignment.ActivityId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@subject", ActivityAssignment.Subject, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@duedate", ActivityAssignment.DueDate, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
        If ActivityAssignment.StartTime.ToString <> System.Guid.Empty.ToString Then
            db.AddParameter("@starttime", ActivityAssignment.StartTime, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Else
            db.AddParameter("@starttime", System.DBNull.Value, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        If ActivityAssignment.EndTime.ToString <> System.Guid.Empty.ToString Then
            db.AddParameter("@endtime", ActivityAssignment.EndTime, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Else
            db.AddParameter("@endtime", System.DBNull.Value, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        db.AddParameter("@activitystatusid", ActivityAssignment.ActivityStatusId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        If ActivityAssignment.PriorityId.ToString <> System.Guid.Empty.ToString Then
            db.AddParameter("@priorityid", ActivityAssignment.PriorityId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Else
            db.AddParameter("@priorityid", System.DBNull.Value, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        db.AddParameter("@comments", ActivityAssignment.Comments, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModUser", ActivityAssignment.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModDate", Now, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
        db.AddParameter("@activityassignmentid", ActivityAssignment.ActivityAssignmentId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Sub

    Public Sub DeleteActivityAssignment(ByVal ActivityAssignmentID As Guid)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("DELETE FROM cmActivityAssignment WHERE ActivityAssignmentId = ?")
        End With
        db.AddParameter("@activityassignmentid", ActivityAssignmentID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Sub

    Public Function GetOneActivityAssignment(ByVal ActivityAssignmentID As Guid) As ActivityAssignmentInfo
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        Dim strSQL As StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With strSQL
            .Append("Select * from cmActivityAssignment")
            .Append(" where ActivityAssignmentId = ? ")
        End With
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@activityassignmentid", ActivityAssignmentID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Dim ActivityAssignment As New ActivityAssignmentInfo
        Try
            ds = db.RunParamSQLDataSet(strSQL.ToString, "SelectedAssignment")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        If ds.Tables("SelectedAssignment").Rows.Count > 0 Then
            For Each row In tbl.Rows
                ActivityAssignment.ActivityAssignmentId = DirectCast(row("ActivityAssignmentId"), Guid)
                ActivityAssignment.EmployeeId = DirectCast(row("EmployeeId"), Guid)
                ActivityAssignment.StudentId = DirectCast(row("StudentId"), Guid)
                ActivityAssignment.ActivityId = DirectCast(row("ActivityId"), Guid)
                ActivityAssignment.Subject = DirectCast(row("Subject"), String)
                ActivityAssignment.DueDate = DirectCast(row("DueDate"), Date)
                ActivityAssignment.StartTime = DirectCast(row("StartTime"), Guid)
                ActivityAssignment.EndTime = DirectCast(row("EndTime"), Guid)
                ActivityAssignment.ActivityStatusId = DirectCast(row("ActivityStatusId"), Int32)
                ActivityAssignment.PriorityId = DirectCast(row("PriorityId"), Guid)
                ActivityAssignment.Comments = DirectCast(row("Comments"), String)
                ActivityAssignment.ActivityResultId = DirectCast(row("ActivityResultId"), Guid)
                ActivityAssignment.DateCompleted = DirectCast(row("DateCompleted"), Date)
                ActivityAssignment.DateCreated = DirectCast(row("DateCreated"), Date)
                ActivityAssignment.ModUser = DirectCast(row("ModUser"), String)
                ActivityAssignment.ModDate = DirectCast(row("ModDate"), Date)
            Next
        End If
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
        Return ActivityAssignment

    End Function

    Public Function GetMoreThanOneActivityAssignment(ByVal EmployeeId As String, ByVal FromStartDate As String, ByVal ToStartDate As String, ByVal ActivityStatusId As String) As ActivityAssignmentInfo()
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        Dim strSQL As StringBuilder
        With strSQL
            .Append("Select a.ActivityAssignmentId, a.EmployeeId, a.studentid, a.ActivityId, a.ActivityStatusId,a.PriorityId, a.Comments,b.LastName, b.FirstName, a.Subject, a.DueDate, a.StartTime, a.EndTime, a.DateCompleted, a.DateCreated, a.ChildId, a.ParentId, a.ActivityResultId, c.TimeIntervalDescrip as StartTimeDescrip, d.TimeIntervalDescrip as EndTimeDescrip from cmActivityAssignment a, arStudent b, cmTimeInterval c, cmTimeInterval d")
            .Append(" where (a.EmployeeId = ?) ")
            .Append(" and (a.StudentId = b.StudentId) ")
            .Append(" and (a.StartTime = c.TimeIntervalId) ")
            .Append(" and (a.EndTime = d.TimeIntervalId) ")
            If FromStartDate <> "" Then
                .Append(" and (a.DueDate >= ?) ")
            End If
            If ToStartDate <> "" Then
                .Append(" and (a.DueDate <= ?) ")
            End If
            If ActivityStatusId <> "" Then
                .Append(" and (a.ActivityStatusId = ?) ")
            End If
        End With

        Dim reader As OleDbDataReader
        db.AddParameter("@employeeid", EmployeeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@startdate", FromStartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@enddate", ToStartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@activitystatusid", ActivityStatusId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        Dim ActivityList As New ArrayList
        Try
            ds = db.RunParamSQLDataSet(strSQL.ToString, "SeveralAssignments")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        If ds.Tables("SeveralAssignments").Rows.Count > 0 Then
            For Each row In tbl.Rows
                Dim ActivityAssignment As New ActivityAssignmentInfo
                ActivityAssignment.ActivityAssignmentId = DirectCast(row("ActivityAssignmentId"), Guid)
                ActivityAssignment.EmployeeId = DirectCast(row("EmployeeId"), Guid)
                ActivityAssignment.StudentId = DirectCast(row("StudentId"), Guid)
                ActivityAssignment.ActivityId = DirectCast(row("ActivityId"), Guid)
                ActivityAssignment.Subject = DirectCast(row("Subject"), String)
                ActivityAssignment.DueDate = DirectCast(row("DueDate"), Date)
                ActivityAssignment.StartTime = DirectCast(row("StartTime"), Guid)
                ActivityAssignment.EndTime = DirectCast(row("EndTime"), Guid)
                ActivityAssignment.ActivityStatusId = DirectCast(row("ActivityStatusId"), Int32)
                ActivityAssignment.PriorityId = DirectCast(row("PriorityId"), Guid)
                ActivityAssignment.Comments = DirectCast(row("Comments"), String)
                ActivityAssignment.ActivityResultId = DirectCast(row("ActivityResultId"), Guid)
                ActivityAssignment.DateCompleted = DirectCast(row("DateCompleted"), Date)
                ActivityAssignment.DateCreated = DirectCast(row("DateCreated"), Date)
                ActivityAssignment.ModUser = DirectCast(row("ModUser"), String)
                ActivityAssignment.ModDate = DirectCast(row("ModDate"), Date)
            Next
        End If
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
        ' Now we convert the ArrayList to a strongly-typed array,
        ' which is easier for the client to deal with.
        ' The ArrayList makes this possible through a handy ToArray() method.
        Return CType(ActivityList.ToArray(GetType(ActivityAssignmentInfo)), ActivityAssignmentInfo())

    End Function

    Public Function GetDropDownList(ByVal EmployeeId As Guid) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da1 As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder
        With strSQLString
            .Append("Select StudentID, LastName, FirstName from arStudent")
        End With
        db.OpenConnection()
        Try
            da1 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da1.Fill(ds, "StudentList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Dim da2 As New OleDbDataAdapter
        strSQLString.Remove(0, strSQLString.Length)

        With strSQLString
            .Append("Select PriorityID, PriorityDescrip from cmPriority")
        End With
        'db.OpenConnection()
        Try
            da2 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da2.Fill(ds, "PriorityList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)

        Dim da3 As New OleDbDataAdapter

        With strSQLString
            .Append("Select ActivityID, ActivityDescrip from cmActivities")
        End With
        Try
            da3 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da3.Fill(ds, "ActivityList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)

        Dim da4 As New OleDbDataAdapter

        With strSQLString
            .Append("Select ActivityStatusID, ActivityStatusDescrip from cmActivityStatus")
        End With
        Try
            da4 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da4.Fill(ds, "ActivityStatusList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)

        Dim da5 As New OleDbDataAdapter

        With strSQLString
            .Append("Select TimeIntervalID, TimeIntervalDescrip from cmTimeInterval order by TimeIntervalDescrip")
        End With
        Try
            da5 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da5.Fill(ds, "TimeIntervalList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)

        Dim da6 As New OleDbDataAdapter

        With strSQLString
            .Append("SELECT UserId, FullName ")
            .Append("FROM syUsers  ")
        End With

        Try
            da6 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da6.Fill(ds, "ActivityEmpList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)

        'New dropdown table - Activity Results
        Dim da7 As New OleDbDataAdapter

        With strSQLString
            .Append("Select ActivityResultId, ActivityResultDescrip from cmActivityResults order by ActivityResultDescrip")
        End With
        Try
            da7 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da7.Fill(ds, "ActivityResult")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)

        db.CloseConnection()
        Return ds
    End Function

    Public Function GetSingleAssignment(ByRef AssignmentId As Guid) As DataSet
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim strSQLString As New StringBuilder
        With strSQLString
            .Append("Select * from cmActivityAssignment")
            .Append(" where ActivityAssignmentId = ? ")
        End With
        Dim objSingleAssignment As New OleDbDataAdapter
        db.OpenConnection()
        db.AddParameter("@AssignmentId", AssignmentId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            objSingleAssignment = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        objSingleAssignment.Fill(ds, "SingleAssignment")
        db.ClearParameters()
        db.CloseConnection()
        Return ds
    End Function

    Public Function PopulateActivitiesDataList(ByVal EmployeeId As Guid, ByVal FromStartDate As String, ByVal ToStartDate As String, ByVal ActivityStatusId As String, ByVal Activity As String, Optional ByVal dataset As DataSet = Nothing) As DataSet
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Try
            da = GetActivitiesDataList(EmployeeId, FromStartDate, ToStartDate, ActivityStatusId, Activity)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        If dataset Is Nothing Then
            da.Fill(ds, "ActivityAssignment")
            Return ds
        Else
            da.Fill(dataset, "ActivityAssignment")
            Return dataset
        End If
    End Function

    Public Function GetActivitiesDataList(ByRef EmployeeId As Guid, ByRef FromStartDate As String, ByRef ToStartDate As String, ByRef ActivityStatusId As String, ByVal ActivityId As String) As OleDbDataAdapter
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim strEmployeeListString As New StringBuilder
        Dim objEmployeeList As New OleDbDataAdapter
        With strEmployeeListString
            .Append(" SELECT a.ActivityAssignmentId, a.EmployeeId, a.studentid, a.ActivityId, a.ActivityStatusId,a.PriorityId, a.Comments,b.LastName,b.FirstName, a.Subject, a.DueDate, a.StartTime, a.EndTime,a.ActivityResultId, a.ChildId, a.ParentId, a.DateCompleted, a.DateCreated, c.TimeIntervalDescrip as StartTimeDescrip, d.TimeIntervalDescrip as EndTimeDescrip FROM cmActivityAssignment a INNER JOIN arStudent b ON a.StudentId = b.StudentId LEFT JOIN cmTimeInterval c ON a.StartTime = c.TimeIntervalId LEFT JOIN cmTimeInterval d ON a.EndTime = d.TimeIntervalId ")
            .Append(" WHERE (a.EmployeeId = ?) ")

            If FromStartDate <> "" Then
                .Append(" and (a.DueDate >= ?) ")
            End If
            If ToStartDate <> "" Then
                .Append(" and (a.DueDate <= ?) ")
            End If
            If ActivityStatusId.ToString <> "" Then
                .Append(" and (a.ActivityStatusId = ?) ")
            End If
            ' added by Corey Masson on July 23 2004 to filter by Activity
            If ActivityId.ToString <> "" Then
                .Append(" and (a.ActivityId = ?) ")
            End If
        End With
        db.AddParameter("@EmployeeId", EmployeeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        If FromStartDate <> "" Then
            db.AddParameter("@StartDate", CDate(FromStartDate), DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
        End If
        If ToStartDate <> "" Then
            db.AddParameter("@StartDate2", CDate(ToStartDate), DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
        End If
        If ActivityStatusId <> "" Then
            db.AddParameter("@ActivityStatusId", XmlConvert.ToInt32(ActivityStatusId), DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        End If
        ' Added by Corey Masson on July 23 2004 to filter by Activity
        If ActivityId <> "" Then
            db.AddParameter("@ActivityId", XmlConvert.ToGuid(ActivityId), DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        db.OpenConnection()
        Try
            objEmployeeList = db.RunParamSQLDataAdapter(strEmployeeListString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.CloseConnection()
        db.ClearParameters()
        Return objEmployeeList
    End Function

#End Region

End Class

