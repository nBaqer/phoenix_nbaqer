Imports FAME.Advantage.Common


Public Class DataDictionaryDB
    Public Function GetModulesListing() As DataTable
        Dim db As New DataAccess
        Dim sSQL As String
        Dim ds As New DataSet

     
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        sSQL = "SELECT ModuleCode,ModuleName FROM syModules"

        ds = db.RunSQLDataSet(sSQL)

        Return ds.Tables(0)

    End Function

    Public Function GetTablesForModule(ByVal modulePrefix As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet

   

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT TblId, TblName ")
            .Append("FROM syTables ")
            .Append("WHERE LEFT(TblName,2) = ?")
        End With

        db.AddParameter("@val", modulePrefix, DataAccess.OleDbDataType.OleDbString, 2, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)

    End Function

    Public Function GetFieldsForTableExclID(ByVal tableId As Integer) As DataTable
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT t1.FldId, FldName ")
            .Append("FROM syTblFlds t1, syFields t2, syTables t3 ")
            .Append("WHERE t1.FldId=t2.FldId ")
            .Append("AND t1.TblId=t3.TblId ")
            .Append("AND t1.TblId = ? ")
            .Append("AND t2.FldName NOT IN('ModDate','ModUser') ")
            .Append("AND RIGHT(t2.FldName,2) <> 'Id'")
        End With

        db.AddParameter("@tblid", tableId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Function GetFieldProperties(ByVal fldId As Integer) As DataTable
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT t1.*, t2.FldType, ")
            .Append("(SELECT t4.Caption ")
            .Append(" FROM syFldCaptions t4 ")
            .Append(" WHERE t4.FldId = t1.FldId ")
            .Append(" AND t4.LangId = 1 ) AS Caption ")
            .Append("FROM syFields t1, syFieldTypes t2 ")
            .Append("WHERE t1.FldTypeId=t2.FldTypeId ")
            .Append("AND t1.FldId = ? ")
        End With

        db.AddParameter("@fldid", fldId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Sub UpdateFieldProperties(ByVal fldId As Integer, ByVal fldReq As Boolean, ByVal fldCaption As String, ByVal captionIsInDB As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim recId As String
        Dim maxFldCapId As Integer


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'Since this page is rarely used we will simply delete the field from the data dictionary
        'required table. This table stores the fields that the school wants to be required throughout
        'the system
        With sb
            .Append("DELETE FROM syInstFldsDDReq ")
            .Append("WHERE FldId =?")
        End With

        db.AddParameter("@fldid", fldId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)

        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'If the field is required (fldReq = true) then we will insert the field. This means that if the
        'field is no longer required the previous delete operation would have taken care of it.
        If fldReq = True Then
            recId = Guid.NewGuid.ToString

            With sb
                .Append("INSERT INTO syInstFldsDDReq (DDFldReqId,FldId)")
                .Append("VALUES (?,?)")
            End With

            db.AddParameter("@recid", recId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@fldid", fldId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            db.ClearParameters()
            sb.Remove(0, sb.Length)

        End If

        'Update the caption if the caption is already in the db.
        If captionIsInDB = "TRUE" Then
            With sb
                .Append("UPDATE syFldCaptions ")
                .Append("SET Caption = ? ")
                .Append("WHERE FldId = ?")
            End With

            db.AddParameter("@caption", fldCaption, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@fldid", fldId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        Else
            If fldCaption <> "" Then 'We need to insert a new caption
                'First get the max FldCapId
                maxFldCapId = GetMaxFldCapId() + 1

                With sb
                    .Append("INSERT INTO syFldCaptions(FldCapId,FldId,LangId,Caption) ")
                    .Append("VALUES(?,?,?,?)")
                End With

                db.AddParameter("@fldcapid", maxFldCapId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@fldid", fldId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@langid", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@caption", fldCaption, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString)

                db.ClearParameters()
                sb.Remove(0, sb.Length)

            End If
        End If

    End Sub

    Public Function GetFields() As DataTable
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT FldId,FldName ")
            .Append("FROM syFields ")
            .Append("ORDER BY FldName")
        End With

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Function IsFieldDataDictionaryRequired(ByVal fldId As Integer) As Boolean
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim count As Integer


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT COUNT(*) ")
            .Append("FROM syInstFldsDDReq ")
            .Append("WHERE FldId = ?")
        End With

        db.AddParameter("@fldid", fldId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        count = CInt(db.RunParamSQLScalar(sb.ToString))

        If count = 0 Then
            Return False
        Else
            Return True
        End If


    End Function

    Public Function GetMaxFldCapId() As Integer
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim count As Integer

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT MAX(FldCapId) ")
            .Append("FROM syFldCaptions ")
        End With

        count = CInt(db.RunParamSQLScalar(sb.ToString))

        Return count

    End Function
#Region "Get AdvAppsetting for Manage Config entry"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
End Class
