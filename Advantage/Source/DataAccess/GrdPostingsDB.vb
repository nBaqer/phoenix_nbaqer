Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class GrdPostingsDB
#Region "Get AdvAppsetting object"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
    Public Function GetAllTerms() As DataSet
        Dim ds As DataSet

        Try
            '   connect to the database
            Dim db As New DataAccess

            db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.TermId,t1.TermDescrip ")
                .Append("FROM arTerm t1  ")
                '.Append("WHERE t2.Status = 'Active' and t1.StatusId = t2.StatusId ")
            End With

            '   Execute the query
            ds = db.RunSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetCurrentAndFutureTerms(Optional ByVal campusId As String = "", Optional ByVal activeOnly As Boolean = False) As DataSet
        Dim ds As DataSet

        Try


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.TermId,t1.TermDescrip ")
                .Append("FROM arTerm t1, syStatuses t2  ")
                .Append("WHERE t1.EndDate >= ? ")
                .Append("AND t1.StatusId = t2.StatusId AND t1.ProgramVersionId IS NULL ")

                If activeOnly = True Then
                    .Append("AND t2.Status = 'Active' ")
                End If

                If campusId <> "" Then
                    'We only want terms that belong to the all campus group or campus groups that have this campus
                    .Append("AND ( ")
                    .Append("       t1.CampGrpId = (SELECT CampGrpId FROM dbo.syCampGrps WHERE CampGrpCode='All') ")
                    .Append("                                           OR                                        ")
                    .Append("       t1.CampGrpId IN(SELECT CampGrpId FROM dbo.syCmpGrpCmps WHERE CampusId='" & campusId & "') ")
                    .Append("    ) ")
                End If

                .Append("ORDER BY t1.StartDate ")


            End With

            db.AddParameter("@edate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            '   Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

            Return ds

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.Message)
        End Try

        'Return the 
    End Function
    Public Function GetCurrentAndFutureTermsRegStds() As DataSet
        Dim ds As DataSet

        Try

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.TermId,t1.TermDescrip ")
                .Append("FROM arTerm t1  ")
                .Append("WHERE t1.EndDate >= ? and t1.IsModule = 0 ")
                .Append("ORDER BY t1.StartDate ")
                '.Append("WHERE t2.Status = 'Active' and t1.StatusId = t2.StatusId ")
            End With

            db.AddParameter("@edate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            '   Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

            Return ds

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.Message)
        End Try

        'Return the 
    End Function
    Public Function GetCurrentAndPastTerms(Optional ByVal instructorId As String = "", Optional ByVal campusId As String = "") As DataSet
        Dim ds As DataSet

        Try


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT Distinct t1.TermId,t1.TermDescrip,t1.StartDate,programVersions.ProgramRegistrationType ")
                .Append("FROM arTerm t1  ")
                .Append("INNER JOIN syStatuses t2 ON t1.StatusId = t2.StatusId ")
                .Append("LEFT JOIN arPrgVersions programVersions on t1.ProgramVersionId = programVersions.PrgVerId  ")
                .Append("WHERE (t1.StartDate <= ? or (t1.StartDate IS NULL and t1.EndDate IS NULL AND t1.ProgramVersionId IS NOT NULL))  ")
                .Append("and t2.Status = 'Active' ")
                If instructorId <> "" Then
                    'this condition will make the query retrieve all current and future terms where the instructor teaches in.
                    .Append("AND EXISTS (SELECT * FROM arClassSections WHERE InstructorId=? AND TermId=t1.TermId) ")
                End If
                If campusId <> "" Then
                    .Append("AND (t1.CampGrpId IN(SELECT CampGrpId ")

                    .Append("FROM syCmpGrpCmps ")

                    .Append("WHERE CampusId = ? ")
                    .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                    .Append("OR t1.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                End If
                .Append("ORDER BY t1.StartDate,t1.TermDescrip")
            End With

            db.AddParameter("@Sdate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            If instructorId <> "" Then
                db.AddParameter("@InstructorId", instructorId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If campusId <> "" Then
                db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

            Return ds

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.Message)
        End Try

        'Return the 
    End Function
    Public Function GetCurrentAndPastTermsForAnyUser(ByVal instructorId As String, ByVal userName As String, Optional ByVal campusId As String = "") As DataSet
        Dim ds As DataSet
        Dim isAcademicAdvisorORInstructorSuperVisor As Boolean
        isAcademicAdvisorORInstructorSuperVisor = (New ClsSectAttendanceDB).GetIsRoleAcademicAdvisorORInstructorSuperVisor(instructorId)
        Try


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT	DISTINCT  ")
                .Append("		T.TermId, ")
                .Append("		T.TermDescrip, ")
                .Append("		T.StartDate  ")
                .Append("FROM	arTerm T, syStatuses ST, arClassSectionTerms CST, arClassSections CS   ")
                .Append("WHERE  ")
                .Append("		T.StartDate <= ? ")
                .Append("AND		T.StatusId = ST.StatusId  ")
                .Append("AND		ST.Status = 'Active'   ")
                .Append("AND		T.TermId=CST.TermId ")
                .Append("AND		CST.ClsSectionId=CS.ClsSectionId ")
                '.Append("AND		CS.InstrGrdBkWgtId IS NOT NULL ")
                'If instructorId <> "" Then
                '    'this condition will make the query retrieve all current and future terms where the instructor teaches in.
                '    .Append(" AND EXISTS (SELECT * FROM arClassSections WHERE InstructorId=? AND TermId=t1.TermId) ")
                'End If
                If Not (userName = "sa" Or isAcademicAdvisorORInstructorSuperVisor) Then
                    .Append("AND		CS.InstructorId in (select InstructorId from arInstructorsSupervisors where SupervisorId = ? ")
                    .Append("							    union all ")
                    .Append("							    select ? ) ")
                End If
                If campusId <> "" Then
                    .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")
                    .Append("FROM syCmpGrpCmps ")
                    .Append("WHERE CampusId = ? ")
                    .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                    .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                End If
                .Append("ORDER BY  ")
                .Append("		T.StartDate,T.TermDescrip ")
            End With

            db.AddParameter("@Sdate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            If Not (userName = "sa" Or isAcademicAdvisorORInstructorSuperVisor) Then
                db.AddParameter("@InstructorId", instructorId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@InstructorId", instructorId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If campusId <> "" Then
                db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)

            'Close Connection
            db.CloseConnection()

            Return ds

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.Message)
        End Try

        'Return the 
    End Function
    Public Function GetTermsFromStartDate(ByVal stuEnrollId As String, Optional ByVal campusId As String = "") As DataSet
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Try


            '   connect to the database
            db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

            '   build the sql query
            With sb
                .Append("SELECT ")
                .Append("           t1.TermId,t1.TermDescrip,t1.StartDate,t1.EndDate ")
                .Append("FROM       arTerm t1, (SELECT StartDate FROM arStuEnrollments WHERE StuEnrollId=?) t2 ,syStatuses  t3 ")
                .Append("WHERE    t1.Statusid=t3.Statusid and t3.Status='Active' and  ((t1.StartDate <= t2.StartDate And t1.EndDate >= t2.StartDate) ")
                .Append("           OR (t1.StartDate > t2.StartDate AND t1.StartDate <= ?)) ")

                If campusId <> "" Then
                    .Append("AND (t1.CampGrpId IN(SELECT CampGrpId ")
                    .Append("FROM syCmpGrpCmps ")
                    .Append("WHERE CampusId = ? ")
                    .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                    .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                End If

                .Append("ORDER BY   t1.StartDate,t1.EndDate,t1.TermDescrip ")
            End With

            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@TodaysDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            If campusId <> "" Then
                db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)

        Catch ex As Exception

            Throw New BaseException(ex.InnerException.Message)

        Finally

            'Close Connection
            db.CloseConnection()

        End Try

        'Return dataset 
        Return ds
    End Function

    Public Function GetClsSects(ByVal Term As String, ByVal Instr As String, ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT a.ClsSectionId, a.ReqId, a.ClsSection, b.Descrip ")
                .Append("FROM arClassSections a, arReqs b ")
                .Append("WHERE a.TermId = ? and a.ReqId = b.ReqId and a.InstructorId = ? ")
                .Append("AND a.CampusId = ? and a.StartDate <= ? ")
                .Append("ORDER BY b.Descrip")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@TermId", Term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@instructor", Instr, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@cmpid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Sdate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "TermClsSects")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetClsSects(ByVal Term As String, ByVal Instr As String, ByVal CampusId As String, ByVal UserName As String) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try


            db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT ")
                .Append("a.ClsSectionId, ")
                .Append("c.ReqId, ")
                .Append("c.ClsSection, ")
                .Append("b.Descrip ")
                .Append("FROM   arClassSectionTerms a,arReqs b, arClassSections c ")
                .Append("WHERE  a.TermId=? ")
                .Append("AND a.ClsSectionId = c.ClsSectionId  ")
                .Append(" AND c.ReqId=b.ReqId ")
                If Not (UserName = "sa") Then
                    'retrieve all class sections where the instructor teaches in.
                    '.Append("AND c.InstructorId=? ")
                    .Append("AND		C.InstructorId in (select InstructorId from arInstructorsSupervisors where SupervisorId = ? ")
                    .Append("							    union all ")
                    .Append("							    select ? ) ")
                End If
                .Append("AND c.CampusId=? ")
                .Append("AND c.StartDate<=?  ")
                .Append("ORDER BY b.Descrip ")
            End With

            'With sb
            '    .Append("SELECT ")
            '    .Append("       a.ClsSectionId,")
            '    .Append("       a.ReqId,")
            '    .Append("       a.ClsSection,")
            '    .Append("       b.Descrip ")
            '    .Append("FROM   arClassSections a,arReqs b ")
            '    .Append("WHERE  a.TermId=?")
            '    .Append("       AND a.ReqId=b.ReqId")
            '    If Not (UserName = "sa") Then
            '        'retrieve all class sections where the instructor teaches in.
            '        .Append("       AND a.InstructorId=?")
            '    End If
            '    .Append("       AND a.CampusId=?")
            '    .Append("       AND a.StartDate<=? ")
            '    .Append("ORDER BY b.Descrip")
            'End With

            db.AddParameter("@TermId", Term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Not (UserName = "sa") Then
                db.AddParameter("@instructor", Instr, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@instructor1", Instr, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            db.AddParameter("@cmpid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Sdate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "TermClsSects")

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        Return ds
    End Function
    ''Optional Patameter added by Saraswathi Lakshmanan
    ''To filter based on academic Advisor
    Public Function GetGrdCriteria(ByVal ClsSect As String, ByVal Instr As String, ByVal username As String, Optional ByVal isAcademicAdvisor As Boolean = False) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT distinct a.InstrGrdBkWgtDetailId, a.Descrip, a.Seq ")
                .Append("FROM arGrdBkWgtDetails a, arClassSections b, arGrdBkWeights c, arGrdComponentTypes d ")
                .Append("WHERE b.ClsSectionId = ? and a.InstrGrdBkWgtId = b.InstrGrdBkWgtId ")
                .Append("and b.InstructorId=C.InstructorId ")
                .Append("and a.GrdComponentTypeId=d.GrdComponentTypeId ")
                .Append("and d.SysComponentTypeId not in(500,503,544) ")
                ''Modified by Saraswathi
                If (username <> "sa" And username.ToLower <> "support") And isAcademicAdvisor = False Then
                    '.Append("and c.InstructorId = ?")
                    .Append("and    C.InstructorId in (select InstructorId from arInstructorsSupervisors where SupervisorId = ? ")
                    .Append("							    union all ")
                    .Append("							    select ? ) ")
                End If
                .Append("ORDER BY a.Descrip")

            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@TermId", ClsSect, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If (username <> "sa" And username.ToLower <> "support") And isAcademicAdvisor = False Then
                db.AddParameter("@instr", Instr, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@instr1", Instr, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "TermClsSects")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function GetClsSectDataGrid(ByVal term As String, ByVal clsSect As String, ByVal grdCriteria As String, Optional ByVal resNum As Integer = 0) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim da As OleDbDataAdapter
        Dim ds As New DataSet
        'Dim count As Integer


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        With sb
            .Append(" SELECT   ")
            .Append(" 	a.StuEnrollId  ")
            .Append(" 	, b.LastName  ")
            .Append(" 	,     Coalesce(Case Len(b.SSN) When 9 then '*******' + SUBSTRING(SSN,6,4) else ' ' end, ' ') SSN  ")
            .Append(" 	, b.StudentNumber  ")
            .Append(" 	, b.FirstName  ")
            .Append(" 	, a.GrdSysDetailId  ")
            .Append(" 	, b.StudentId  ")
            .Append(" 	, (Select Grade from arGradeSystemDetails where  GrdSysDetailId = a.GrdSysDetailId) as Grade  ")
            .Append(" 	,0 AS IscompGraded   ")
            .Append(" 	,a.DateCompleted  ")
            .Append(" FROM arResults a  ")
            .Append(" JOIN dbo.arStuEnrollments c ON c.StuEnrollId = a.StuEnrollId  ")
            .Append(" JOIN dbo.arStudent b ON b.StudentId = c.StudentId  ")
            .Append(" JOIN dbo.syStatusCodes codes ON codes.StatusCodeId = c.StatusCodeId  ")
            .Append(" JOIN dbo.sySysStatus stat ON stat.SysStatusId = codes.SysStatusId  ")
            .Append(" WHERE   ")
            .Append(" 	a.TestId = ?  ")
            .Append(" 	AND (stat.PostAcademics = 1 OR stat.SysStatusId = 7)  ")
            .Append(" ORDER BY b.LastName  ")


            '.Append("SELECT a.StuEnrollId, b.LastName, ")
            '.Append("    Coalesce(Case Len(b.SSN) When 9 then '*******' + SUBSTRING(SSN,6,4) else ' ' end, ' ') SSN, ")
            '.Append("b.StudentNumber, b.FirstName, a.GrdSysDetailId, b.StudentId, ")
            '.Append("(Select Grade from arGradeSystemDetails where  ")
            '.Append("GrdSysDetailId = a.GrdSysDetailId) as Grade,0 AS IscompGraded ")
            '.Append("FROM arResults a, arStudent b, arStuEnrollments c ")
            '.Append(" WHERE a.TestId = ? and a.StuEnrollId = c.StuEnrollId and c.StudentId = b.StudentId  ")
            '.Append("ORDER BY b.LastName")

        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@clsId", clsSect, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query       
        db.OpenConnection()
        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "ClsSectStdGrds")

            sb.Remove(0, sb.Length)
            db.ClearParameters()

            Dim da2 As OleDbDataAdapter

            With sb
                .Append("SELECT  a.StuEnrollId, a.Score,a.DateCompleted, a.Comments, a.GrdBkResultId,a.IsCompGraded ")
                .Append("FROM arGrdBkResults a ")
                .Append("WHERE a.ClsSectionId = ? and a.InstrGrdBkWgtDetailId = ? ")
                If ResNum <> 0 Then
                    .Append("AND a.ResNum = ?")
                End If
            End With
            'db.OpenConnection()

            db.AddParameter("@clssectId", clsSect, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@grdcritId", grdCriteria, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If ResNum <> 0 Then
                db.AddParameter("@resnum", resNum, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
            End If
            da2 = db.RunParamSQLDataAdapter(sb.ToString)

            da2.Fill(ds, "ClsSectGrds")
            'count = ds.Tables("ClsSectGrds").Rows.Count
        Finally
            db.CloseConnection()
        End Try

        Return ds
    End Function

    Public Function InsertGrdPosting(ByVal GrdPostingObj As GrdPostingsInfo, ByVal user As String) As String
        Try
            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()


            Dim db As New DataAccess

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.AddParameter("@GrdBkResultId", GrdPostingObj.GrdBkResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ClsSectionId", GrdPostingObj.ClsSectId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", GrdPostingObj.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@InstrGrdBkWgtDetailId", GrdPostingObj.GrdBkWgtDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@Score", GrdPostingObj.Score, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
            db.AddParameter("@Comments", GrdPostingObj.Comments, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ResNum", GrdPostingObj.ResNum, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@IsCompGraded", GrdPostingObj.IsCompGraded, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery("usp_AR_ArGrdBkResults_Insert", Nothing, "SP")

            db.ClearParameters()
            db.CloseConnection()

        Catch ex As Exception
            Return "Error Insert score - " & ex.Message
        End Try

    End Function

    Public Function UpdateGrdPosting(ByVal GrdPostingObj As GrdPostingsInfo, ByVal user As String) As String
        Try
            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            db.AddParameter("@ClsSectionId", GrdPostingObj.ClsSectId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", GrdPostingObj.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@InstrGrdBkWgtDetailId", GrdPostingObj.GrdBkWgtDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@Score", GrdPostingObj.Score, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
            db.AddParameter("@Comments", GrdPostingObj.Comments, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ResNum", GrdPostingObj.ResNum, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
            db.AddParameter("@IsCompGraded", GrdPostingObj.IsCompGraded, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery("usp_AR_ArGrdBkResults_Update", Nothing, "SP")

            db.ClearParameters()
            db.CloseConnection()

        Catch ex As Exception
            Return "Error updating score - " & ex.Message
        End Try

    End Function

    Public Function DeleteGrdPosting(ByVal GrdPostingObj As GrdPostingsInfo) As String
        Try
            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            db.AddParameter("@GrdBkResultId", GrdPostingObj.GrdBkResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery("USP_AR_ArGrdBkResults_Delete", Nothing, "SP")

            db.ClearParameters()
            db.CloseConnection()

        Catch ex As Exception
            Return "Error deleting score - " & ex.Message
        End Try

    End Function

    Public Function GetTermsByInstructor(ByVal InstructorId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       T.TermId, ")
            .Append("       T.TermDescrip ")
            .Append("FROM ")
            .Append("       arTerm T, ")
            .Append("       arClassSections CS, ")
            .Append("       arClassSectionTerms CST ")
            .Append("WHERE ")
            .Append("       T.TermId=CST.TermId ")
            .Append("AND    CST.ClsSectionId=CS.ClsSectionId ")
            .Append("AND    CS.EmpId = ? ")
        End With

        ' Add InstructorId parameter 
        db.AddParameter("@EmpId", InstructorId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetClsSectInstructor(ByVal ClsSect As String) As String
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim count As String
        Dim sGrdSysDetailId As String
        Dim Instructor As String


        Try


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("Select a.InstructorId, b.FullName from arClassSections a, syUsers b where a.ClsSectionId = ? and ")
                .Append("a.InstructorId = b.UserId ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@GrdSysDetailId", ClsSect, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            db.OpenConnection()
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)


            While dr.Read()
                Instructor = dr("FullName")
            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return Instructor

    End Function
    Public Function GetCourseLevelGrdCriteria(ByVal ClsSect As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try


            '   connect to the database
            Dim maxDate As String = (New FA.GrdPostingsDB).GetRecentDate(ClsSect)
            Dim db As New DataAccess
            db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT distinct a.InstrGrdBkWgtDetailId, d.Descrip,a.Number, a.Seq  ")
                .Append("FROM arGrdBkWgtDetails a, arClassSections b, arGrdBkWeights c, arGrdComponentTypes d ")
                .Append("WHERE(a.InstrGrdBkWgtId = c.InstrGrdBkWgtId) ")
                .Append("and c.ReqId in  ")
                .Append("(Select ReqId from arClassSections where ClsSectionId = ?) ")
                .Append("and c.EffectiveDate <= b.StartDate and a.GrdComponentTypeId = d.GrdComponentTypeId ")
                .Append("and c.ReqId=b.Reqid ")
                .Append("and c.EffectiveDate= ? ")
                .Append("and d.SysComponentTypeId not in(500,503,544) ")
                .Append("order by d.Descrip")
            End With
            
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@clssect", ClsSect, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@EffectiveDate", maxDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "TermClsSects")

            'Close Connection
            db.CloseConnection()
        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    ''Added by Saraswathi lakshmanan to fix issue 14296
    ''Academic advisors and Instructor supervisor able to 
    ''post and modify attendance and grade
    ''The term is populated based on the Roles  the User has
    ''New Parameters are added to the existing function
    ''Changed on 26-Sept -2008
    Public Function GetCurrentAndPastTermsForAnyUserbyRoles(ByVal userId As String, ByVal userName As String, ByVal isAcademicAdvisor As Boolean, Optional ByVal campusId As String = "") As DataSet
        Dim ds As DataSet

        Try

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT	DISTINCT  ")
                .Append("		T.TermId, ")
                .Append("		T.TermDescrip, ")
                .Append("		T.StartDate  ")
                .Append("FROM	arTerm T, syStatuses ST, arClassSectionTerms CST, arClassSections CS   ")
                .Append("WHERE  ")
                .Append("		T.StartDate <= ? ")
                .Append("AND		T.StatusId = ST.StatusId  ")
                .Append("AND		ST.Status = 'Active'   ")
                .Append("AND		T.TermId=CST.TermId ")
                .Append("AND		CST.ClsSectionId=CS.ClsSectionId ")
                '.Append("AND		CS.InstrGrdBkWgtId IS NOT NULL ")
                'If instructorId <> "" Then
                '    'this condition will make the query retrieve all current and future terms where the instructor teaches in.
                '    .Append(" AND EXISTS (SELECT * FROM arClassSections WHERE InstructorId=? AND TermId=t1.TermId) ")
                'End If

                'Modified here  by Saraswathi Lakshmanan
                If userName <> "sa" And userName.ToLower <> "support" And isAcademicAdvisor = False Then

                    '' .Append("AND		CS.InstructorId in (select InstructorId from arInstructorsSupervisors where SupervisorId = ? ")
                    '.Append("							    union all ")
                    '.Append("							    select ? ) ")

                    .Append(" AND T.TermId=CS.TermId  ")
                    .Append(" AND (CS.InstructorId in (Select InstructorId ")
                    .Append(" from arInstructorsSupervisors where")
                    .Append(" SupervisorId= ? )")
                    .Append(" OR CS.InstructorId= ? ) ")

                End If

                If campusId <> "" Then
                    .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")
                    .Append("FROM syCmpGrpCmps ")
                    .Append("WHERE CampusId = ? ")
                    .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                    .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                End If
                .Append(" ORDER BY  ")
                .Append("		T.StartDate,T.TermDescrip ")
            End With

            db.AddParameter("@Sdate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            If userName <> "sa" And userName.ToLower <> "support" And isAcademicAdvisor = False Then
                db.AddParameter("@userId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@userId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            If campusId <> "" Then
                db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)

            'Close Connection
            db.CloseConnection()

            Return ds

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.Message)
        End Try

        'Return the 
    End Function
    ''Get CohortStartDate for the current and  Past terms which are active
    ''GetCohortStartDateforCurrentAndPastTermsForAnyUserbyRoles
    ''Added by Saraswathi lakshmanan on October 14 2008
    Public Function GetCohortStartDateforCurrentAndPastTermsForAnyUserbyRoles(ByVal userId As String, ByVal userName As String, ByVal isAcademicAdvisor As Boolean, Optional ByVal campusId As String = "") As DataSet
        Dim ds As DataSet

        Try


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT	DISTINCT  ")
                .Append("	Convert(varchar(10),CS.CohortStartDate,101)as CohortStartDate,year(CS.CohortStartDate) ")
                .Append("FROM	arTerm T, syStatuses ST, arClassSectionTerms CST, arClassSections CS   ")
                .Append("WHERE  ")
                .Append("		T.StartDate <= ? ")
                .Append("AND		T.StatusId = ST.StatusId  ")
                .Append("AND		ST.Status = 'Active'   ")
                .Append("AND		T.TermId=CST.TermId ")
                .Append("AND		CST.ClsSectionId=CS.ClsSectionId ")
                .Append(" AND CS.CohortStartDate is not null ")
                '.Append("AND		CS.InstrGrdBkWgtId IS NOT NULL ")
                'If instructorId <> "" Then
                '    'this condition will make the query retrieve all current and future terms where the instructor teaches in.
                '    .Append(" AND EXISTS (SELECT * FROM arClassSections WHERE InstructorId=? AND TermId=t1.TermId) ")
                'End If

                ''Modified here  by Saraswathi Lakshmanan
                If userName <> "sa" And isAcademicAdvisor = False Then

                    '' .Append("AND		CS.InstructorId in (select InstructorId from arInstructorsSupervisors where SupervisorId = ? ")
                    '.Append("							    union all ")
                    '.Append("							    select ? ) ")

                    .Append(" AND T.TermId=CS.TermId  ")
                    .Append(" AND (CS.InstructorId in (Select InstructorId ")
                    .Append(" from arInstructorsSupervisors where")
                    .Append(" SupervisorId= ? )")
                    .Append(" OR CS.InstructorId= ? ) ")

                End If

                If campusId <> "" Then
                    .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")
                    .Append("FROM syCmpGrpCmps ")
                    .Append("WHERE CampusId = ? ")
                    .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                    .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                End If
                .Append(" ORDER BY  ")
                .Append("	year(CS.CohortStartDate) ")
            End With

            db.AddParameter("@Sdate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            If userName <> "sa" And isAcademicAdvisor = False Then
                db.AddParameter("@userId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@userId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            If campusId <> "" Then
                db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)

            'Close Connection
            db.CloseConnection()

            Return ds

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.Message)
        End Try

        'Return the 
    End Function
    ''Added by saraswathi lakshmanan on July 07 2009
    ''The terms dropdown list should show the terms related to the students prgverid and those mapped to all programs

    Public Function GetTermsFromStudentsProgramandStartDate(ByVal stuEnrollId As String, Optional ByVal campusId As String = "") As DataSet
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Try


            '   connect to the database
            db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

            '   build the sql query
            With sb
                .Append("SELECT ")
                .Append("           t1.TermId,t1.TermDescrip,t1.StartDate,t1.EndDate ")
                .Append("FROM       arTerm t1, (SELECT StartDate FROM arStuEnrollments WHERE StuEnrollId=?) t2 ,syStatuses  t3 ")
                .Append("WHERE    t1.Statusid=t3.Statusid and t3.Status='Active' ")
                .Append(" and (t1.progId in (Select Progid from arprgVersions PV, arStuEnrollments SE where ")
                .Append(" PV.prgverid=SE.PrgverId and SE.StuEnrollid=?) or t1.progid is null) ")
                .Append("  and  ((t1.StartDate <= t2.StartDate And t1.EndDate >= t2.StartDate) ")
                .Append("           OR (t1.StartDate > t2.StartDate AND t1.StartDate <= ?)) ")

                If campusId <> "" Then
                    .Append("AND (t1.CampGrpId IN(SELECT CampGrpId ")
                    .Append("FROM syCmpGrpCmps ")
                    .Append("WHERE CampusId = ? ")
                    .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                    .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                End If

                .Append("ORDER BY   t1.StartDate,t1.EndDate,t1.TermDescrip ")
            End With

            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@TodaysDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            If campusId <> "" Then
                db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)

        Catch ex As Exception

            Throw New BaseException(ex.InnerException.Message)

        Finally

            'Close Connection
            db.CloseConnection()

        End Try

        'Return dataset 
        Return ds
    End Function
    'Public Function GetStudentsRegisteredInSelectedTerm(ByVal CampusId As String, _
    '                                                    ByVal TermId As String, _
    '                                                    ByVal FirstName As String, _
    '                                                    ByVal LastName As String) As DataSet



    '    Dim sb As New StringBuilder

    '    With sb
    '        .Append(" select Distinct t6.LastName,t6.Firstname from " + vbCrLf)
    '        .Append(" @DictationTestComponent t1 inner join arBridge_GradeComponentTypes_Courses t2 on t1.GrdComponentTypeId = t2.GrdComponentTypeId " + vbCrLf)
    '        .Append(" inner join arClassSections t3 on t2.ReqId = t3.ReqId " + vbCrLf)
    '        .Append(" inner join arResults t4 on t3.ClsSectionId = t4.TestId " + vbCrLf)
    '        .Append(" inner join arStuEnrollments t5 on t4.StuEnrollId=t5.StuEnrollId " + vbCrLf)
    '        .Append(" inner join arStudent t6 on t5.StudentId = t6.StudentId " + vbCrLf)
    '        If Not TermId = "" Then
    '            .Append(" and t4.TermId = '" & TermId & "' " + vbCrLf)
    '        End If
    '        If Not CampusId = "" Then
    '            .Append(" and t5.CampusId = '" & CampusId & "' " + vbCrLf)
    '        End If
    '        If Not FirstName = "" Then
    '            .Append(" and t6.FirstName like '" + FirstName + "%' " + vbCrLf)
    '        End If
    '        If Not LastName = "" Then
    '            .Append(" and t6.LastName like '" + LastName + "%' " + vbCrLf)
    '        End If
    '    End With

    '    Dim db As New SQLDataAccess
    '    Dim ds As New DataSet
    '    db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
    '    Try
    '        db.OpenConnection()
    '        db.AddParameter("@strSQL", sb.ToString, SqlDbType.VarChar, , ParameterDirection.Input)
    '        ds = db.RunParamSQLDataSet_SP("dbo.Usp_StudentsRegisteredInTermsWithCoursesMappedToDictationTest_GetList", "GetStudentsRegisteredInTerm")
    '        Return ds
    '    Catch ex As Exception
    '        Return Nothing
    '    Finally
    '        db.CloseConnection()
    '    End Try

    'End Function

    'Added by : Balaji
    'Modified Date : 10/18/2009
    Public Function CurrentAndPastTermsWithCoursesMappedToDictationSpeedTestComponent(ByVal CampusId As String, _
                                                 ByVal UserId As String, _
                                                 ByVal isAcademicAdvisor As Boolean, _
                                                 ByVal UserName As String) As DataSet

        Dim db As New SQLDataAccess
        Dim ds As New DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@TermStartDate", Date.Now.ToShortDateString, SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@SupervisorId", New Guid(UserId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@InstructorId", New Guid(UserId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@CampusId", New Guid(CampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@isAcademicAdvisor", isAcademicAdvisor, SqlDbType.Bit, , ParameterDirection.Input)
            db.AddParameter("@UserName", UserName, SqlDbType.VarChar, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.usp_CurrentAndPastTermsForAnyUserbyRoles_GetList", "CurrentAndPastTerms")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Create Procedure usp_CurrentAndPastTermsForAnyUserbyRoles_GetList  
        '@TermStartDate datetime,
        '@SupervisorId uniqueidentifier,
        '@InstructorId uniqueidentifier,
        '@CampusId uniqueidentifier,
        '@isAcademicAdvisor bit,
        '@userName varchar(50)
        'as
        '	   declare @DictationTestComponent table(GrdComponentTypeId uniqueidentifier)
        '	   insert into @DictationTestComponent select GrdComponentTypeId from arGrdComponentTypes where SysComponentTypeId=612
        '	   If @userName <> 'sa' And @isAcademicAdvisor = 0 
        '            begin()

        '     Select DISTINCT
        '                		T.TermId, 
        '                		T.TermDescrip, 
        '                		T.StartDate  
        '			        FROM	arTerm T, syStatuses ST, arClassSectionTerms CST, arClassSections CS, 
        '							arBridge_GradeComponentTypes_Courses BGC,@DictationTestComponent DTC

        '				    WHERE  
        '							T.StartDate <= @TermStartDate 
        '							AND		T.StatusId = ST.StatusId  
        '							AND		ST.Status = 'Active'
        '							AND		T.TermId=CST.TermId 
        '							AND		CST.ClsSectionId=CS.ClsSectionId
        '							AND T.TermId=CS.TermId  and CS.ReqId = BGC.ReqId and BGC.GrdComponentTypeId=DTC.GrdComponentTypeId
        '							AND (CS.InstructorId in (Select InstructorId 
        '							from arInstructorsSupervisors where
        '							SupervisorId=@SupervisorId) OR CS.InstructorId=@InstructorId)
        '							AND (T.CampGrpId IN (
        '													SELECT CampGrpId FROM syCmpGrpCmps 
        '													WHERE CampusId =@CampusId
        '													AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip ='All')
        '												) 
        '												OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip ='All')
        '								) 
        '					ORDER BY T.StartDate,T.TermDescrip
        '                    End
        '		else
        '			begin
        '					SELECT	DISTINCT  
        '                		T.TermId, 
        '                		T.TermDescrip, 
        '                		T.StartDate  
        '			        FROM	arTerm T, syStatuses ST, arClassSectionTerms CST, arClassSections CS,
        '					arBridge_GradeComponentTypes_Courses BGC,@DictationTestComponent DTC   
        '				    WHERE  
        '							T.StartDate <= @TermStartDate 
        '							AND		T.StatusId = ST.StatusId  
        '							AND		ST.Status = 'Active'
        '							AND		T.TermId=CST.TermId 
        '							AND		CST.ClsSectionId=CS.ClsSectionId 
        '							AND CS.ReqId = BGC.ReqId and BGC.GrdComponentTypeId=DTC.GrdComponentTypeId
        '							AND (T.CampGrpId IN (
        '													SELECT CampGrpId FROM syCmpGrpCmps 
        '													WHERE CampusId =@CampusId
        '													AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip ='All')
        '												) 
        '												OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip ='All')
        '								) 
        '					ORDER BY T.StartDate,T.TermDescrip
        '                            End

        'go
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Function
    Public Function GetProgramVersionWithCoursesMappedToDictationSpeedTest(ByVal StatusId As String, _
                                                ByVal CampusId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As New DataSet

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@StatusId", New Guid(StatusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@CampusId", New Guid(CampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_ProgramVersionWithCoursesMappedToDictationSpeedTest_GetList", "ProgramVersion")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Create Proc USP_ProgramVersionWithCoursesMappedToDictationSpeedTest_GetList
        '@StatusId uniqueidentifier,
        '@CampusId uniqueidentifier
        'as
        'Select distinct
        '		PV.PrgVerDescrip,PV.PrgVerId
        '		--,R.ReqDescrip
        'from 
        '		arPrgVersions  PV inner join arPrograms P on PV.ProgId = P.ProgId
        '		inner join arProgVerDef PVD on PV.PrgVerId = PVD.PrgVerId 
        '		inner join arReqs R on R.ReqId = PVD.ReqId 
        '		inner join arBridge_GradeComponentTypes_Courses BGC on BGC.ReqId = R.ReqId
        'where 
        '		R.StatusId = @StatusId AND
        '		(
        '								PV.CampGrpId IN (
        '													SELECT CampGrpId FROM syCmpGrpCmps 
        '													WHERE CampusId =@CampusId
        '													AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip ='All')
        '												) 
        '								OR 
        '								PV.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip ='All')
        '		) 
        'go
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Function
    Public Function GetTermStudentIsCurrentlyRegisteredIn(ByVal StudentId As String, _
                                                          ByVal campusId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As New DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@StudentId", New Guid(StudentId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@campusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_TermStudentIsCurrentlyRegisteredIn_GetList", "Terms")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Create Procedure USP_TermStudentIsCurrentlyRegisteredIn_GetList
        '@StudentId uniqueidentifier
        'as
        ' Select distinct
        '			T.TermId,T.TermDescrip,Convert(char(10),T.StartDate,101) + ' - ' + Convert(char(10),T.EndDate,101) as TermPeriod,T.ProgId
        '	from 
        '			arPrgVersions PV,
        '			arPrograms P,
        '			arTerm T,
        '			arStuEnrollments SE
        '	where 
        '			PV.ProgId = P.ProgId and 
        '			(T.ProgId = P.ProgId or T.ProgId is NULL) and
        '			PV.PrgVerId = SE.PrgVerId and
        '			SE.StudentId=@StudentId
        '	order by 
        '			T.TermDescrip, T.StartDate
        'go
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Function
    Public Function GetGradeComponentsMappedToSystemComponent(ByVal SysComponentTypeId As Integer) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As New DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@SysComponentTypeId", SysComponentTypeId, SqlDbType.Int, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GradeComponentsMappedToDictationSpeedTestComponent_GetList", "GradeComponentTypes")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Create Procedure USP_GradeComponentsMappedToDictationSpeedTestComponent_GetList
        '@SysComponentTypeId int
        'as
        ' Select Distinct
        '				GrdComponentTypeId,Descrip 
        '	from 
        '				arGrdComponentTypes 
        '	where 
        '				SysComponentTypeId=@SysComponentTypeId
        '	order by
        '				Descrip
        'go
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Function
    Public Function GetGrades(ByVal StuEnrollId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As New DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GradeSystemDetails_GetList", "Grades")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Create Procedure USP_GradeSystemDetails_GetList
        '@StuEnrollId uniqueidentifier
        'as
        'Select Distinct
        '				t5.GrdSysDetailId,
        '				t5.Grade
        'from
        '	arStuEnrollments t2 inner join arPrgVersions t3 on t2.PrgVerId = t3.PrgVerId 
        '	inner join arGradeSystems t4 on t3.GrdSystemId = t4.GrdSystemId 
        '	inner join arGradeSystemDetails t5 on t4.GrdSystemId = t5.GrdSystemId 
        'where 	
        '	t2.StuEnrollId = @StuEnrollId
        'order by 
        '	t5.Grade 
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Function
    Public Function GetDictationScorePostedByStudentEnrollment(ByVal StuEnrollId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As New DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GetScoresPosted_GetList", "StudentScores")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Create Procedure USP_GetScoresPosted_GetList
        '@StuEnrollId uniqueidentifier
        'as
        'Select Distinct
        '				t6.TermId,	
        '				t6.TermDescrip,
        '				t6.StartDate,
        '				t6.EndDate,
        '				t1.DatePassed,
        '				t1.grdcomponenttypeid,
        '				t7.Descrip,
        '				t1.accuracy,
        '				t1.speed,
        '				t1.GrdSysDetailId,
        '				t5.Grade,
        '				t1.InstructorId,
        '				t8.UserName
        'from
        '	arPostScoreDictationSpeedTest  t1 inner join arStuEnrollments t2 on t1.StuEnrollId = t2.StuEnrollId 
        '	inner join arPrgVersions t3 on t2.PrgVerId = t3.PrgVerId 
        '	inner join arGradeSystems t4 on t3.GrdSystemId = t4.GrdSystemId 
        '	inner join arGradeSystemDetails t5 on t4.GrdSystemId = t5.GrdSystemId 
        '	inner join arTerm t6 on t1.termId = t6.TermId
        '	inner join (select distinct GrdComponentTypeId,Descrip from arGrdComponentTypes where SysComponentTypeId=612) t7 on t1.grdcomponenttypeid=t7.GrdComponentTypeId
        '	inner join syUsers t8 on t1.InstructorId = t8.UserId
        'where 	
        '	t1.StuEnrollId = @StuEnrollId
        'order by 
        '	t1.DatePassed, t6.startdate,t6.enddate
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Function
    Public Function GetInstructorsByCampusId(ByVal CampusId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As New DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@CampusId", New Guid(CampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_Instructors_GetList", "Instructors")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Create Procedure USP_Instructors_GetList
        '@CampusId uniqueidentifier
        'as
        'Select distinct
        '		s1.fullname as FullName,s1.UserId as UserId 
        'from 
        '        syUsers s1,syUsersRolesCampGrps s2,syRoles s3,sySysRoles s4,syCmpGrpCmps s5,syStatuses s6 
        'where 
        '		s1.UserId = s2.UserId and s2.RoleId = s3.RoleId 
        '        and s3.SysRoleId = s4.SysRoleId and s2.CampGrpId = s5.CampGrpId and s4.StatusId = s6.StatusId 
        '        and s5.CampusId=@CampusId and s1.AccountActive=1 and s4.SysRoleId = 2 and s6.Status='Active' 
        'order by 
        '		fullname
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Function
    Public Function GetEnrollmentsByStudent(ByVal StudentId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As New DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@StudentId", New Guid(StudentId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_EnrollmentByStudentId_GetList", "StudentEnrollments")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Create Procedure USP_EnrollmentByStudentId_GetList
        '@StudentId uniqueidentifier
        'as
        ' Select Distinct
        '					t2.StuEnrollId,
        '					t3.PrgVerDescrip
        '					t4.StatusCodeDescrip
        '	from
        '					arStudent t1 inner join arStuEnrollments t2 on t1.StudentId = t2.StudentId
        '					inner join arPrgVersions t3 on t2.PrgVerId = t3.PrgVerId 
        '					inner join syStatusCodes t4 on t2.StatusCodeId = t4.StatusCodeId
        '	where 	
        '					t1.StudentId = @StudentId
        '	order by 
        '					t2.ModDate desc
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Function
    Public Sub PostScoresForDictationSpeedTest(ByVal dtEffectiveDate As DateTime, _
                              ByVal xmlRules As String)
        Dim db As New SQLDataAccess
        Dim ds As New DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@EffectiveDate", dtEffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@RuleValues", xmlRules, SqlDbType.VarChar, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_PostScores_Insert", Nothing)
        Catch ex As Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Sub
    Public Sub UpdatePostedScoresForDictationSpeedTest(ByVal dtEffectiveDate As DateTime, _
                              ByVal xmlRules As String)
        Dim db As New SQLDataAccess
        Dim ds As New DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@EffectiveDate", dtEffectiveDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@RuleValues", xmlRules, SqlDbType.VarChar, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_PostScores_Update", Nothing)
        Catch ex As Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Sub
    Public Sub DeletePostedScoresForDictationSpeedTest(ByVal PKValue As String)
        Dim db As New SQLDataAccess
        Dim ds As New DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@id", New Guid(PKValue), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_PostScores_Delete", Nothing)
        Catch ex As Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Sub
End Class
