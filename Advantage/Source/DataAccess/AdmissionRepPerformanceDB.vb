Imports FAME.Advantage.Common

Public Class AdmissionRepPerformanceDB

#Region "Private Data Members"

    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")
    Private m_CutOffDate As DateTime

#End Region


#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

    Public ReadOnly Property CutOffDate() As DateTime
        Get
            Return m_CutOffDate
        End Get
    End Property

#End Region


#Region "Public Methods"
    ''This function is used by the Lead Conversion totals report or MTD/WTD Admission Rep Performance Report
    ''The total number of leads gone through different status in the given period is found.
    ''For eg: Newlead has entered, lead is interviewed , lead is enrolled, etc.
    ''So the count of all the new leads, Interviewed and the count for all the lead statuses are obtained
    ''And thier conversion ratio is also found.
    ''



    Public Function GetWeekToDateMTDRepPerformance(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String = ""
        Dim strOrderBy As String = ""
        Dim idx As Integer
        Dim idx2 As Integer
        Dim endDate As String = Date.Today.ToShortDateString
        Dim StrDateRange As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim strCampWhere As String
        ''The filter conditions Campus and program is moved to strwhere 
        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
            strCampWhere &= " AND " & paramInfo.FilterList
        End If

        ''Filter other has the date ranges
        If paramInfo.FilterOther <> "" Then
            If paramInfo.FilterOther.Contains("syLeadStatusesChanges.ModDate") = True Then
                StrDateRange = paramInfo.FilterOther.Replace("syLeadStatusesChanges.ModDate", " ")
            End If
            ''Parse FilterOther in order to get End Date
            'idx = paramInfo.FilterOther.IndexOf("BETWEEN")
            'If idx <> -1 Then
            '    idx2 = paramInfo.FilterOther.IndexOf("AM")
            '    If idx2 <> -1 Then
            '        endDate = paramInfo.FilterOther.Substring(idx + 9, idx2 + 2 - (idx + 8)).Trim
            '    Else
            '        endDate = paramInfo.FilterOther.Substring(idx + 8).Trim
            '    End If
            '    Dim idx3 As Integer
            '    idx3 = endDate.IndexOf(" ")
            '    If idx3 > 0 Then
            '        endDate = endDate.Substring(0, idx3).Trim
            '    End If
            'End If


        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        With sb
            'WTDDetail table
            ''WTDDetail has all the leads  and their status changes details, Program, Rep and Source details
            .Append("SELECT DISTINCT ")
            .Append("       syCampGrps.CampGrpId,syCampuses.CampusId,B.AdmissionsRep,A.OrigStatusId,A.NewStatusId,A.LeadId,")
            ''Modified by Saraswathi lakshmanan on 13 Oct 2009
            ''Program Id and Source Category Id is Added
            .Append(" B.ProgramID,B.SourceCategoryID,  ")

            .Append("       (SELECT SysStatusId FROM syStatusCodes WHERE StatusCodeId=A.OrigStatusId) AS SysOrigStatusId, ")
            .Append("       (SELECT SysStatusId FROM syStatusCodes WHERE StatusCodeId=A.NewStatusId) AS SysNewStatusId, ")
            '.Append("       (SELECT IsReversal FROM syLeadStatusChanges X WHERE X.OrigStatusId=A.OrigStatusId AND X.NewStatusId=A.NewStatusId AND X.CampGrpId=syCampGrps.CampGrpId) AS IsReversal ")
            .Append("       (SELECT IsReversal FROM syLeadStatusChanges X WHERE X.OrigStatusId=A.OrigStatusId AND X.NewStatusId=A.NewStatusId ) AS IsReversal ")
            .Append("FROM   syLeadStatusesChanges A,adLeads B,syUsers C,syCampGrps,syCmpGrpCmps,syCampuses ")
            If strWhere.Contains("arPrograms.ProgId") Then
                .Append(" ,arPrograms   ")
            End If

            .Append("WHERE  A.LeadId = B.LeadId And B.AdmissionsRep = C.UserId ")
            .Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId ")
            .Append("       AND syCampuses.CampusId=B.CampusId ")
            '.Append("       AND A.ModDate BETWEEN ? AND ? ")
            .Append("       AND A.ModDate " + StrDateRange)

            .Append(strWhere)
            If strWhere.Contains("arPrograms.ProgId") Then
                .Append(" and arPrograms.ProgId=B.ProgramID  ")
            End If
            .Append(" ORDER BY syCampGrps.CampGrpId,syCampuses.CampusId,B.AdmissionsRep,A.OrigStatusId,A.NewStatusId;")
            'Perfomance table
            ''The leads are grouped by their Admission reps
            .Append("SELECT ")
            .Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,syCampuses.CampDescrip,C.FullName,B.AdmissionsRep ")
            .Append("FROM   syLeadStatusesChanges A,adLeads B,syUsers C,syCampGrps,syCmpGrpCmps,syCampuses ")
            If strWhere.Contains("arPrograms.ProgId") Then
                .Append(" ,arPrograms   ")
            End If

            .Append("WHERE  A.LeadId=B.LeadId AND B.AdmissionsRep=C.UserId ")
            .Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId ")
            .Append("       AND syCampuses.CampusId=B.CampusId ")
            '.Append("       AND A.ModDate BETWEEN ? AND ? ")
            .Append("       AND A.ModDate " + StrDateRange)
            .Append(strWhere)
            If strWhere.Contains("arPrograms.ProgId") Then
                .Append(" and arPrograms.ProgId=B.ProgramID  ")
            End If

            '.Append(" UNION ")
            '.Append("SELECT ")
            '.Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,syCampuses.CampDescrip,C.FullName,B.AdmissionsRep ")
            '.Append("FROM   syLeadStatusesChanges A,adLeads B,syUsers C,syCampGrps,syCmpGrpCmps,syCampuses ")
            '.Append("WHERE  A.LeadId=B.LeadId AND B.AdmissionsRep=C.UserId ")
            '.Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
            '.Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId ")
            '.Append("       AND syCampuses.CampusId=B.CampusId ")
            '.Append("       AND A.ModDate BETWEEN ? AND ? ")
            '.Append(strWhere)
            .Append(" GROUP BY syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,syCampuses.CampDescrip,C.FullName,B.AdmissionsRep")
            .Append(" ORDER BY syCampGrps.CampGrpId,syCampuses.CampusId,C.FullName,B.AdmissionsRep;")
            'StatusCodeDescrip and Code table
            '.Append("SELECT (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=1 AND A.StatusId=B.StatusId AND B.Status='Active' " & strWhere & " ORDER BY A.StatusCodeDescrip) AS NewLead,")
            '.Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=1 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS NLCode,")
            '.Append("       (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=4 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS IntScheduled,")
            '.Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=4 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ISCode,")
            '.Append("       (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=5 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS Interviewed,")
            '.Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=5 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ICode,")
            '.Append("       (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=6 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS Enrolled,")
            '.Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=6 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ECode;")

            ''Added by Saraswathi lakshmanan on Oct 14 2009
            ''The Leads are grouped by their program 
            ''Group by Program
            .Append("SELECT ")
            .Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,syCampuses.CampDescrip, B.ProgramID, arPrograms.ProgDescrip ")
            .Append("FROM   syLeadStatusesChanges A,adLeads B,syUsers C,syCampGrps,syCmpGrpCmps,syCampuses,arPrograms ")
            .Append("WHERE  A.LeadId=B.LeadId AND B.AdmissionsRep=C.UserId ")
            .Append("       And arPrograms.ProgID=B.ProgramID  ")
            .Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId ")
            .Append("       AND syCampuses.CampusId=B.CampusId ")
            '.Append("       AND A.ModDate BETWEEN ? AND ? ")
            .Append("       AND A.ModDate  " + StrDateRange)
            .Append(strWhere)
            .Append(" GROUP BY syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,syCampuses.CampDescrip,B.ProgramID,arPrograms.ProgDescrip")
            .Append(" ORDER BY syCampGrps.CampGrpId,syCampuses.CampusId, B.ProgramID;")

            ''Added by Saraswathi lakshmanan on Oct 14 2009
            ''Group by Source Category
            ''The Leads are grouped by Source Category

            .Append("SELECT ")
            .Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,syCampuses.CampDescrip, B.SourceCategoryID ,SC.SourceCatagoryDescrip ")
            .Append("FROM   syLeadStatusesChanges A,adLeads B,syUsers C,syCampGrps,syCmpGrpCmps,syCampuses, adSourceCatagory SC ")
            If strWhere.Contains("arPrograms.ProgId") Then
                .Append(" ,arPrograms  ")
            End If

            .Append("WHERE  A.LeadId=B.LeadId AND B.AdmissionsRep=C.UserId ")
            .Append("       And SC.SourceCatagoryID=B.SourceCategoryID ")
            .Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId ")
            .Append("       AND syCampuses.CampusId=B.CampusId ")
            '.Append("       AND A.ModDate BETWEEN ? AND ? ")
            .Append("       AND A.ModDate  " + StrDateRange)
            .Append(strWhere)
            If strWhere.Contains("arPrograms.ProgId") Then
                .Append(" and arPrograms.ProgId=B.ProgramID  ")
            End If

            .Append(" GROUP BY syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,syCampuses.CampDescrip, B.SourceCategoryID,SC.SourceCatagoryDescrip")
            .Append(" ORDER BY syCampGrps.CampGrpId,syCampuses.CampusId,B.SourceCategoryID;")

            If strWhere.Contains("arPrograms.ProgId") Then
                Dim idx3 As Integer
                idx3 = paramInfo.FilterList.IndexOf("arPrograms.ProgId")
                If idx3 > 0 Then
                    strCampWhere = " AND " & paramInfo.FilterList.Substring(0, idx3 - 4)
                Else
                    strCampWhere = strWhere
                End If
            Else
                strCampWhere = strWhere
            End If

            ''All the Status Code and their Descriptions
            .Append("SELECT (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=1 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId  " & strCampWhere & " ORDER BY A.StatusCodeDescrip) AS NewLead,")
            .Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=1 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId   " & strCampWhere & " ORDER BY A.StatusCodeDescrip) AS NLCode,")
            .Append("       (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=4 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strCampWhere & " ORDER BY A.StatusCodeDescrip) AS IntScheduled,")
            .Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=4 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strCampWhere & " ORDER BY A.StatusCodeDescrip) AS ISCode,")
            .Append("       (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=5 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strCampWhere & " ORDER BY A.StatusCodeDescrip) AS Interviewed,")
            .Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=5 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strCampWhere & " ORDER BY A.StatusCodeDescrip) AS ICode,")
            ''Added by Saraswathi lakshmanan on Oct 13 2009
            ''''''''''''
            .Append("       (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=2 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strCampWhere & " ORDER BY A.StatusCodeDescrip) AS ApplReceived,")
            .Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=2 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strCampWhere & " ORDER BY A.StatusCodeDescrip) AS ApplReceivedCode,")

            .Append("       (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=3 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strCampWhere & " ORDER BY A.StatusCodeDescrip) AS ApplNotAccepted,")
            .Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=3 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strCampWhere & " ORDER BY A.StatusCodeDescrip) AS ApplNotAcceptedCode,")

            .Append("       (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=17 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strCampWhere & " ORDER BY A.StatusCodeDescrip) AS DeadLead,")
            .Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=17 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strCampWhere & " ORDER BY A.StatusCodeDescrip) AS DeadLeadCode,")


            .Append("       (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=18 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strCampWhere & " ORDER BY A.StatusCodeDescrip) AS WillEnrollinFuture,")
            .Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=18 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strCampWhere & " ORDER BY A.StatusCodeDescrip) AS WillEnrollinFutureCode,")

            ''''''''''


            .Append("       (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=6 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strCampWhere & " ORDER BY A.StatusCodeDescrip) AS Enrolled,")
            .Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=6 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strCampWhere & " ORDER BY A.StatusCodeDescrip) AS ECode;")

            ''gets the campuses and campgrps
            ''Get all the Campus and CampGroups
            .Append("SELECT ")
            .Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,syCampuses.CampDescrip ")
            .Append("FROM  syCampGrps,syCmpGrpCmps,syCampuses ")
            .Append("WHERE   ")
            .Append("       syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId ")
            .Append(strCampWhere + ";")


            ''--List of all the students with noStart status whose expected Start date is between the given Date range
            ''this gives a list of Students who have leadID
            .Append("  Select   Distinct syCampGrps.CampGrpId,syCampuses.CampusId,L.AdmissionsRep,")
            .Append("  L.LeadId, SE.StuEnrollId, L.ProgramID, L.SourceCategoryID")
            .Append("  from ")
            .Append("  adLeads L,arStuEnrollMents SE,syCampGrps,syCmpGrpCmps,syCampuses  ")
            If strWhere.Contains("arPrograms.ProgId") Then
                .Append("  ,arprograms ")
            End If

            .Append("  where ")
            .Append("  SE.LeadID=L.LeadID and  ")
            .Append("  SE.ExpStartDate ")
            .Append(StrDateRange + "  and SE.StatusCodeId in (Select StatusCodeId from syStatusCodes ")
            .Append("   where SysStatusID=8) and syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId        AND syCmpGrpCmps.CampusId=syCampuses.CampusId   ")
            .Append("   AND syCampuses.CampusId=L.CampusId  ")
            If strWhere.Contains("arPrograms.ProgId") Then
                .Append("  and arPrograms.ProgId=L.ProgramID ")
            End If
            .Append(strWhere + ";")

            '--List of all the students with the expected start date between the given date range

            .Append("  Select   Distinct syCampGrps.CampGrpId,syCampuses.CampusId,L.AdmissionsRep,")
            .Append("  L.LeadId, SE.StuEnrollId, L.ProgramID, L.SourceCategoryID")
            .Append("  from ")
            .Append("  adLeads L,arStuEnrollMents SE,syCampGrps,syCmpGrpCmps,syCampuses  ")
            If strWhere.Contains("arPrograms.ProgId") Then
                .Append("  ,arprograms ")
            End If
            .Append("  where ")
            .Append("  SE.LeadID=L.LeadID and  ")
            .Append("  SE.ExpStartDate ")
            .Append(StrDateRange + " and  ")
            .Append("  syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId        AND syCmpGrpCmps.CampusId=syCampuses.CampusId   ")
            .Append("   AND syCampuses.CampusId=L.CampusId  ")
            If strWhere.Contains("arPrograms.ProgId") Then
                .Append("  and arPrograms.ProgId=L.ProgramID ")
            End If
            .Append(strWhere + ";")



        End With

        'Dim myCulture As New System.Globalization.CultureInfo("en-US", True)
        'Dim dtEndDate As Date = Date.Parse(endDate, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
        'Dim plusOneDay As Date = dtEndDate.AddDays(1)
        'Dim minusSixDays As Date = dtEndDate.AddDays(-6)
        'Dim firstOfMonth As Date = Convert.ToDateTime(dtEndDate.Month & "/01/" & dtEndDate.Year)

        'db.AddParameter("@ModDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'db.AddParameter("@ModDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'db.AddParameter("@ModDate", firstOfMonth, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'db.AddParameter("@ModDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        ''db.AddParameter("@ModDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        ''db.AddParameter("@ModDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'db.AddParameter("@ModDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'db.AddParameter("@ModDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'db.AddParameter("@ModDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'db.AddParameter("@ModDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'db.AddParameter("@ModDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'db.AddParameter("@ModDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'db.AddParameter("@ModDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'db.AddParameter("@ModDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count = 8 Then
            ds.Tables(0).TableName = "WTDDetail"
            ds.Tables(1).TableName = "Perfomance"
            ds.Tables(2).TableName = "Program"
            ds.Tables(3).TableName = "SourceCategory"
            ds.Tables(4).TableName = "StatusCodeDescrip"
            ds.Tables(5).TableName = "CampusDetails"
            ds.Tables(6).TableName = "NoStartCount"
            ds.Tables(7).TableName = "StudentsCount"


            '   add additional columns to Performance table
            Dim dt As DataTable = ds.Tables("Perfomance")
            dt.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("AdmissionsRepStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WNL", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WIScheduled", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WInterviewed", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WOthers", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WEnrolled", System.Type.GetType("System.Int32")))

            'Added by Saraswathi lakshmanan on october 13 2009
            dt.Columns.Add(New DataColumn("WApplReceived", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WApplNotAccepted", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WDeadLead", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WEnrollinFuture", System.Type.GetType("System.Int32")))

            dt.Columns.Add(New DataColumn("WNLToIS", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WISToI", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WIToE", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WNLToE", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("EToS", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("LToS", System.Type.GetType("System.String")))



            dt.Columns.Add(New DataColumn("AdRepCount", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("StartsCount", System.Type.GetType("System.Int32")))


            '   add CampusTotals table
            dt = New DataTable("CampusTotalsPerf")
            dt.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WNL", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WIScheduled", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WInterviewed", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WOthers", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WEnrolled", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WApplReceived", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WApplNotAccepted", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WDeadLead", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WEnrollinFuture", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WNLToIS", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WISToI", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WIToE", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WNLToE", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("EToS", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("LToS", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("StartsCount", System.Type.GetType("System.Int32")))

            ds.Tables.Add(dt)

            '   add CampusGroupTotals table
            dt = New DataTable("CampusGroupTotalsPerf")
            dt.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WNL", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WIScheduled", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WInterviewed", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WOthers", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WEnrolled", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WApplReceived", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WApplNotAccepted", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WDeadLead", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WEnrollinFuture", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WNLToIS", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WISToI", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WIToE", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WNLToE", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("EToS", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("LToS", System.Type.GetType("System.String")))

            dt.Columns.Add(New DataColumn("StartsCount", System.Type.GetType("System.Int32")))
            ds.Tables.Add(dt)


            '   add additional columns to Program table
            dt = ds.Tables("Program")
            dt.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("FullName", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ProgramDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WNL", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WIScheduled", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WInterviewed", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WOthers", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WEnrolled", System.Type.GetType("System.Int32")))
            'Added by Saraswathi lakshmanan on october 13 2009
            dt.Columns.Add(New DataColumn("WApplReceived", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WApplNotAccepted", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WDeadLead", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WEnrollinFuture", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WNLToIS", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WISToI", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WIToE", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WNLToE", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ProgramCount", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("StartsCount", System.Type.GetType("System.Int32")))

            dt.Columns.Add(New DataColumn("EToS", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("LToS", System.Type.GetType("System.String")))

            '   add additional columns to Program table
            dt = ds.Tables("SourceCategory")
            dt.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("FullName", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("SourceDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WNL", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WIScheduled", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WInterviewed", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WOthers", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WEnrolled", System.Type.GetType("System.Int32")))
            'Added by Saraswathi lakshmanan on october 13 2009
            dt.Columns.Add(New DataColumn("WApplReceived", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WApplNotAccepted", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WDeadLead", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WEnrollinFuture", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WNLToIS", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WISToI", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WIToE", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WNLToE", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("SourceCount", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("StartsCount", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("EToS", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("LToS", System.Type.GetType("System.String")))

            '   add CampusTotals table
            dt = New DataTable("CampusTotalsProg")
            dt.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WNL", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WIScheduled", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WInterviewed", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WOthers", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WEnrolled", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WApplReceived", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WApplNotAccepted", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WDeadLead", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WEnrollinFuture", System.Type.GetType("System.Int32")))

            dt.Columns.Add(New DataColumn("WNLToIS", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WISToI", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WIToE", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WNLToE", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("EToS", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("LToS", System.Type.GetType("System.String")))

            dt.Columns.Add(New DataColumn("StartsCount", System.Type.GetType("System.Int32")))
            ds.Tables.Add(dt)

            '   add CampusTotals table
            dt = New DataTable("CampusTotalsSource")
            dt.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WNL", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WIScheduled", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WInterviewed", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WOthers", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WEnrolled", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WApplReceived", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WApplNotAccepted", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WDeadLead", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WEnrollinFuture", System.Type.GetType("System.Int32")))

            dt.Columns.Add(New DataColumn("WNLToIS", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WISToI", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WIToE", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WNLToE", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("EToS", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("LToS", System.Type.GetType("System.String")))

            dt.Columns.Add(New DataColumn("StartsCount", System.Type.GetType("System.Int32")))
            ds.Tables.Add(dt)


            '   add CampusGroupTotals table
            dt = New DataTable("CampusGroupTotalsProg")
            dt.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WNL", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WIScheduled", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WInterviewed", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WOthers", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WEnrolled", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WApplReceived", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WApplNotAccepted", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WDeadLead", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WEnrollinFuture", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WNLToIS", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WISToI", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WIToE", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WNLToE", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("EToS", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("LToS", System.Type.GetType("System.String")))

            dt.Columns.Add(New DataColumn("StartsCount", System.Type.GetType("System.Int32")))
            ds.Tables.Add(dt)

            '   add CampusGroupTotals table
            dt = New DataTable("CampusGroupTotalsSource")
            dt.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WNL", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WIScheduled", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WInterviewed", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WOthers", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WEnrolled", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WApplReceived", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WApplNotAccepted", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WDeadLead", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WEnrollinFuture", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("WNLToIS", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WISToI", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WIToE", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("WNLToE", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("EToS", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("LToS", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("StartsCount", System.Type.GetType("System.Int32")))
            ds.Tables.Add(dt)
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Public Function GetYearToDateRepPerformance(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String = ""
        Dim strOrderBy As String = ""
        Dim idx As Integer
        Dim idx2 As Integer
        Dim endDate As String = Date.Today.ToString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            'Parse FilterOther in order to get End Date
            idx = paramInfo.FilterOther.IndexOf("BETWEEN")
            If idx <> -1 Then
                idx2 = paramInfo.FilterOther.IndexOf("AM")
                If idx2 <> -1 Then
                    endDate = paramInfo.FilterOther.Substring(idx + 9, idx2 + 2 - (idx + 8))
                Else
                    endDate = paramInfo.FilterOther.Substring(idx + 8)
                End If
                Dim idx3 As Integer
                idx3 = endDate.IndexOf(" ")
                If idx3 > 0 Then
                    endDate = endDate.Substring(0, idx3).Trim
                End If
            End If
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        With sb
            'YTDDetail table
            .Append("SELECT DISTINCT ")
            .Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,syCampuses.CampDescrip,")
            .Append("       B.AdmissionsRep,C.FullName,A.OrigStatusId,A.NewStatusId,A.LeadId,A.ModDate,")
            .Append("       (SELECT SysStatusId FROM syStatusCodes WHERE StatusCodeId=A.OrigStatusId) AS SysOrigStatusId, ")
            .Append("       (SELECT SysStatusId FROM syStatusCodes WHERE StatusCodeId=A.NewStatusId) AS SysNewStatusId, ")
            .Append("       isnull((SELECT IsReversal FROM syLeadStatusChanges X WHERE X.OrigStatusId=A.OrigStatusId AND X.NewStatusId=A.NewStatusId AND X.CampGrpId=syCampGrps.CampGrpId),0) AS IsReversal ")
            .Append("FROM   syLeadStatusesChanges A,adLeads B,syUsers C,syCampGrps,syCmpGrpCmps,syCampuses ")
            .Append("WHERE  A.LeadId = B.LeadId And B.AdmissionsRep = C.UserId ")
            .Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId ")
            .Append("       AND syCampuses.CampusId=B.CampusId ")
            .Append("       AND A.ModDate BETWEEN ? AND ? ")
            .Append(strWhere)
            .Append(" ORDER BY syCampGrps.CampGrpId,syCampuses.CampusId,B.AdmissionsRep,A.OrigStatusId,A.NewStatusId,A.ModDate;")
            'StatusCodeDescrip and Code table
            'Commented by Balaji  on 12/4/2006 for mantis 8931
            '.Append("SELECT (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=1 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS NewLead,")
            '.Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=1 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS NLCode,")
            '.Append("       (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=4 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS IntScheduled,")
            '.Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=4 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ISCode,")
            '.Append("       (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=5 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS Interviewed,")
            '.Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=5 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ICode,")
            '.Append("       (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=6 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS Enrolled,")
            '.Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=6 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ECode;")
            '' ''.Append("SELECT (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=1 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS NewLead,")
            '' ''.Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=1 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId   " & strWhere & " ORDER BY A.StatusCodeDescrip) AS NLCode,")
            '' ''.Append("       (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=4 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS IntScheduled,")
            '' ''.Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=4 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ISCode,")
            '' ''.Append("       (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=5 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS Interviewed,")
            '' ''.Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=5 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ICode,")

            '' '' ''Added by Saraswathi lakshmanan on Oct 13 2009
            ''''''''''''''''
            '' ''.Append("       (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=2 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ApplicationReceived,")
            '' ''.Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=2 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ApplicationReceivedCode,")

            '' ''.Append("       (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=3 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ApplicationNotAccepted,")
            '' ''.Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=3 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ApplicationNotAcceptedCode,")

            '' ''.Append("       (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=17 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS DeadLead,")
            '' ''.Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=17 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS DeadLeadCode,")


            '' ''.Append("       (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=18 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS WillEnrollinFuture,")
            '' ''.Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=18 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS WillEnrollinFutureCode,")

            '' ''.Append("       (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=6 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS Enrolled,")
            '' ''.Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=6 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ECode;")


            ''Taking the Status From SySysStatus table instead of school defined Status

            .Append("SELECT (SELECT TOP 1 S.SysStatusDescrip FROM SySysStatus S, syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=1 and A.SysStatusID=S.SysStatusID AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS NewLead,")
            .Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=1 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId   " & strWhere & " ORDER BY A.StatusCodeDescrip) AS NLCode,")
            .Append("       (SELECT TOP 1 S.SysStatusDescrip FROM SySysStatus S, syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=4 and A.SysStatusID=S.SysStatusID AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS IntScheduled,")
            .Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=4 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ISCode,")
            .Append("       (SELECT TOP 1 S.SysStatusDescrip FROM SySysStatus S, syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=5 and A.SysStatusID=S.SysStatusID AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS Interviewed,")
            .Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=5 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ICode,")

            ''Added by Saraswathi lakshmanan on Oct 13 2009
            ''''''''''''
            .Append("       (SELECT TOP 1 S.SysStatusDescrip FROM SySysStatus S, syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=2 and A.SysStatusID=S.SysStatusID AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ApplicationReceived,")
            .Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=2 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ApplicationReceivedCode,")

            .Append("       (SELECT TOP 1 S.SysStatusDescrip FROM SySysStatus S, syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=3 and A.SysStatusID=S.SysStatusID AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ApplicationNotAccepted,")
            .Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=3 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ApplicationNotAcceptedCode,")

            .Append("       (SELECT TOP 1 S.SysStatusDescrip FROM SySysStatus S, syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=17 and A.SysStatusID=S.SysStatusID AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS DeadLead,")
            .Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=17 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS DeadLeadCode,")


            .Append("       (SELECT TOP 1 S.SysStatusDescrip FROM SySysStatus S, syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=18 and A.SysStatusID=S.SysStatusID AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS WillEnrollinFuture,")
            .Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=18 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS WillEnrollinFutureCode,")

            .Append("       (SELECT TOP 1 S.SysStatusDescrip FROM SySysStatus S, syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=6 and A.SysStatusID=S.SysStatusID AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS Enrolled,")
            .Append("       (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=6 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId    " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ECode;")



        End With

        Dim myCulture As New System.Globalization.CultureInfo("en-US", True)
        Dim dtEndDate As Date = Date.Parse(endDate, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
        Dim add1Day As Date = dtEndDate.AddDays(1)
        Dim firstDayOfYear As DateTime = New DateTime(dtEndDate.Year, 1, 1)

        Dim ToDate As Date
        Dim FromDate As Date

        ToDate = dtEndDate.AddMonths(1)
        ToDate = New Date(ToDate.Year, ToDate.Month, 1)
        ToDate = ToDate.AddDays(-1)

        FromDate = dtEndDate.AddMonths(-11)
        FromDate = New Date(FromDate.Year, FromDate.Month, 1)

        'db.AddParameter("@ModDate", firstDayOfYear, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'db.AddParameter("@ModDate", add1Day, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

        db.AddParameter("@ModDate", FromDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@ModDate", ToDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count = 2 Then
            ds.Tables(0).TableName = "YTDDetail"
            ds.Tables(1).TableName = "StatusCodeDescrip"
            Dim dt As New DataTable
            dt.TableName = "YTDTemp"

            '   add additional columns to YTDTemp table
            dt.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("AdmissionsRepStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("FullName", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("LeadStatus", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("LeadStatusDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Count", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("Month", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("AdRepCount", System.Type.GetType("System.Int32")))
            ds.Tables.Add(dt)


            dt = New DataTable
            dt.TableName = "YTDCampusTotals"

            '   add additional columns to YTDCampusTotals table
            dt.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("LeadStatusOrCriteria", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("Descrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Jan", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Feb", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Mar", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Apr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("May", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Jun", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Jul", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Aug", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Sep", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Oct", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Nov", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Dec", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Total", System.Type.GetType("System.String")))
            ds.Tables.Add(dt)


            dt = New DataTable
            dt.TableName = "YTDCampusGroupTotals"

            '   add additional columns to YTDCampusGroupTotals table
            dt.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("LeadStatusOrCriteria", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("Descrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Jan", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Feb", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Mar", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Apr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("May", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Jun", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Jul", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Aug", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Sep", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Oct", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Nov", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Dec", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Total", System.Type.GetType("System.String")))
            ds.Tables.Add(dt)

        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function


    'Public Function GetStartDateRepPerformance(ByVal paramInfo As ReportParamInfo) As DataSet
    '    Dim sb As New System.Text.StringBuilder
    '    Dim db As New DataAccess
    '    Dim ds As New DataSet
    '    Dim strWhere As String = ""
    '    Dim strOrderBy As String = ""
    '    Dim strTerm As String = ""

    '    If paramInfo.FilterList <> "" Then
    '        Dim idx As Integer
    '        Dim temp As String = paramInfo.FilterList
    '        idx = temp.IndexOf(" AND arTerm.TermId")
    '        If idx > -1 Then
    '            strTerm = temp.Substring(idx)
    '            strWhere &= " AND " & temp.Substring(0, idx)
    '        End If
    '        idx = temp.IndexOf("adLeads.AdmissionsRep")
    '        If idx > -1 Then
    '            strWhere = strWhere.Replace("adLeads.AdmissionsRep", "URCG.UserId")
    '        End If
    '    End If

    '    If paramInfo.FilterOther <> "" Then
    '        strWhere &= " AND " & paramInfo.FilterOther
    '    End If

    '    If paramInfo.OrderBy <> "" Then
    '        strOrderBy &= "," & paramInfo.OrderBy
    '    End If

    '    With sb
    '        'StartDateRepPerformance table
    '        .Append("SELECT ")
    '        .Append("       CampGrpId,")
    '        .Append("       CampGrpDescrip,")
    '        .Append("       CampusId,")
    '        .Append("       CampDescrip,")
    '        .Append("       UserId AS AdmissionsRep,")
    '        .Append("       (SELECT FullName FROM syUsers WHERE UserId=Y.UserId) AS FullName,")
    '        .Append("       (SELECT StartDate FROM arTerm WHERE TermId=Y.TermId) AS StartDate,")
    '        .Append("       (SELECT EndDate FROM arTerm WHERE TermId=Y.TermId) AS EndDate,")
    '        .Append("       (SELECT TermDescrip FROM arTerm WHERE TermId=Y.TermId) AS TermDescrip,")
    '        .Append("       EnrolledCount AS Enrolled,")
    '        .Append("       CancelledCount AS Cancelled,")
    '        .Append("       Starts=(CASE WHEN (EnrolledCount-CancelledCount>0)")
    '        .Append("				        THEN (EnrolledCount-CancelledCount)")
    '        .Append("				        ELSE (0)")
    '        .Append("				        END),")
    '        .Append("       StartRate=(CASE WHEN (EnrolledCount<>0 AND (EnrolledCount-CancelledCount)>0)")
    '        '.Append("				        THEN (((EnrolledCount-CancelledCount)/EnrolledCount)*100)")
    '        ''Commented and modified by Saraswathi lakshmanan on oct 7 2009
    '        ''Integer division in Sql gives either intger or Zero
    '        .Append("				        THEN CONVERT(DECIMAL(6,2),(((EnrolledCount-CancelledCount)*1.0/EnrolledCount)*100))	")

    '        .Append("				        ELSE (0)")
    '        .Append("				        END)")
    '        .Append("FROM   (")
    '        .Append("       SELECT")
    '        .Append("	        CampGrpId,")
    '        .Append("	        CampGrpDescrip,")
    '        .Append("	        CampusId,")
    '        .Append("	        CampDescrip,")
    '        .Append("	        TermId,")
    '        .Append("	        UserId,")
    '        .Append("	        SUM(Enrollments) AS EnrolledCount,")
    '        .Append("	        SUM(Cancels) AS CancelledCount")
    '        .Append("       FROM	(")
    '        .Append("               SELECT DISTINCT")
    '        .Append("			                syCampGrps.CampGrpId,")
    '        .Append("			                syCampGrps.CampGrpDescrip,")
    '        .Append("			                B.CampusId,")
    '        .Append("			                B.CampDescrip,")
    '        .Append("			                URCG.UserId,")
    '        .Append("			                W.TermId,")
    '        .Append("			                W.Enrollments,")
    '        .Append("			                0 AS Cancels")
    '        .Append("		        FROM   syCampGrps,syCampuses B,syCmpGrpCmps C,syUsersRolesCampGrps URCG,")
    '        .Append("				        (	SELECT  AdmissionsRep,CampusId,TermId,COUNT(*) AS Enrollments")
    '        .Append("					        FROM	adLeads ,syStatusCodes SC,arTerm")
    '        If strTerm.Contains("arPrograms") Then
    '            .Append(" ,arPrograms ")
    '        End If

    '        ''Modified by Saraswathi lakshmanan on oct 07 2009
    '        ''The expected Start Date should be equal to the term Start Date of the Student
    '        '.Append("                           WHERE   L.ExpectedStart>=arTerm.StartDate And L.ExpectedStart<arTerm.EndDate")
    '        .Append("                           WHERE   adLeads.ExpectedStart=arTerm.StartDate ")
    '        ''Modified by Saraswathi lakshmanan on oct 07 2009
    '        ''To fix mantis issue 14513: ENH: Global: Need an Admissions conversion report or Admissions Rep performance report 
    '        ''The enrolled Lead Should be compared against the Program, term and startdates.
    '        .Append("                                   and adLeads.ProgramId=arTerm.ProgID  ")
    '        If strTerm.Contains("arPrograms") Then
    '            .Append("                               and arprograms.ProgId=adLeads.ProgramID  ")
    '        End If

    '        .Append("							        AND adLeads.LeadStatus=SC.StatusCodeId")
    '        .Append("							        AND SC.SysStatusId=6 " & strTerm)
    '        .Append("					        GROUP BY adLeads.AdmissionsRep,adLeads.CampusId,TermId) W")
    '        .Append("               WHERE   syCampGrps.CampGrpId=C.CampGrpId")
    '        .Append("			            AND B.CampusId=C.CampusId")
    '        .Append("			            AND syCampGrps.CampGrpId=URCG.CampGrpId")
    '        .Append("			            AND URCG.RoleId IN (SELECT RoleId FROM syRoles WHERE SysRoleId IN (3,8))")
    '        .Append("			            AND W.AdmissionsRep=URCG.UserId AND W.CampusId=B.CampusId " & strWhere)
    '        .Append("               UNION ALL")
    '        .Append("               SELECT DISTINCT")
    '        .Append("			                syCampGrps.CampGrpId,")
    '        .Append("			                syCampGrps.CampGrpDescrip,")
    '        .Append("			                B.CampusId,")
    '        .Append("			                B.CampDescrip,")
    '        .Append("			                URCG.UserId,")
    '        .Append("			                W.TermId,")
    '        .Append("			                0 AS Enrollments,")
    '        .Append("                           W.Cancels")
    '        .Append("		        FROM   syCampGrps,syCampuses B,syCmpGrpCmps C,syUsersRolesCampGrps URCG,")
    '        .Append("				        (	SELECT E.AdmissionsRep,E.CampusId,TermId,COUNT(*) AS Cancels")
    '        .Append("					        FROM    arStuEnrollments E,syStatusCodes A, arTerm")
    '        ''Modified by Saraswathi lakshmanan on oct 07 2009
    '        ''To fix mantis issue 14513: ENH: Global: Need an Admissions conversion report or Admissions Rep performance report 
    '        ''The Student Should be compared against the Program, term and startdates.
    '        .Append("                               ,arPrgVersions PV  ")
    '        If strTerm.Contains("adLeads") Then
    '            .Append(" ,adleads ")
    '        End If

    '        If strTerm.Contains("arPrograms") Then
    '            .Append(" ,arPrograms ")
    '        End If

    '        .Append("                           WHERE   E.StatusCodeId=A.StatusCodeId")
    '        ''Modified by Saraswathi lakshmanan on oct 07 2009
    '        ''To fix mantis issue 14513: ENH: Global: Need an Admissions conversion report or Admissions Rep performance report 
    '        ''The Student Should be compared against the Program, term and startdates.
    '        .Append("                                   and PV.PrgverId=E.prgverid and PV.ProgId=arTerm.ProgID ")

    '        If strTerm.Contains("adLeads") Then
    '            .Append("                               and  adleads.LeadId=E.LeadID and adLeads.AdmissionsRep=E.AdmissionsRep  ")
    '        End If

    '        If strTerm.Contains("arPrograms") Then
    '            .Append("                               and arprograms.ProgID=PV.ProgID ")
    '        End If

    '        .Append("							        AND A.sysStatusID=8 " & strTerm)
    '        .Append("							        AND (EXISTS (SELECT * FROM adLeads L1 ")
    '        .Append("                                               WHERE   E.LeadId=L1.LeadId")
    '        .Append("												        AND L1.ExpectedStart=arTerm.StartDate")
    '        '.Append("												        AND L1.AdmissionsRep=E.AdmissionsRep))")
    '        .Append("												        AND L1.AdmissionsRep=E.AdmissionsRep)")
    '        ''Modified by Saraswathi lakshmanan Oct 8 2009
    '        ''Expected Start Date should be equal to the term Start Date
    '        '.Append("								        OR (E.ExpStartDate>=arTerm.StartDate AND E.ExpStartDate<arTerm.EndDate))")
    '        .Append("								        OR (E.ExpStartDate=arTerm.StartDate))")

    '        .Append("					        GROUP BY E.AdmissionsRep,E.CampusId,TermId) W")
    '        .Append("               WHERE   syCampGrps.CampGrpId=C.CampGrpId")
    '        .Append("			            AND B.CampusId=C.CampusId")
    '        .Append("			            AND syCampGrps.CampGrpId=URCG.CampGrpId")
    '        .Append("			            AND URCG.RoleId IN (SELECT RoleId FROM syRoles WHERE SysRoleId IN (3,8))")
    '        .Append("			            AND W.AdmissionsRep=URCG.UserId AND W.CampusId=B.CampusId " & strWhere)
    '        .Append("               ) Q ")
    '        .Append("       GROUP BY CampGrpId,CampGrpDescrip,CampusId,CampDescrip,TermId,UserId")
    '        .Append("       ) Y ")
    '        .Append("WHERE  Y.EnrolledCount>0 OR Y.CancelledCount>0 ")
    '        .Append("ORDER BY CampGrpDescrip,CampGrpId,CampDescrip,CampusId,StartDate,EndDate,TermDescrip,TermId,FullName,AdmissionsRep;")
    '        'StartDateRepPerformance table
    '        ' ''.Append("SELECT ")
    '        ' ''.Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,adLeads.CampusId,syCampuses.CampDescrip,")
    '        ' ''.Append("       adLeads.AdmissionsRep,C.FullName,arTerm.StartDate,arTerm.EndDate,arTerm.TermDescrip,")
    '        ' ''.Append("       (SELECT COUNT(*)")
    '        ' ''.Append("           FROM adLeads L1,syStatusCodes A,sySysStatus B ")
    '        ' ''.Append("           WHERE L1.LeadStatus=A.StatusCodeId ")
    '        ' ''.Append("           AND A.sysStatusID = B.sysStatusID ")
    '        ' ''.Append("           AND B.SysStatusDescrip='Enrolled'")
    '        ' ''.Append("           AND arTerm.StartDate<=L1.ExpectedStart AND L1.ExpectedStart<=arTerm.EndDate ")
    '        ' ''.Append("           AND L1.AdmissionsRep=adLeads.AdmissionsRep ")
    '        ' ''.Append("           AND L1.CampusId=adLeads.CampusId) AS Enrolled, ")
    '        ' ''.Append("       (SELECT COUNT(*) ")
    '        ' ''.Append("           FROM arStuEnrollments E,syStatusCodes A,sySysStatus B ")
    '        ' ''.Append("           WHERE E.StatusCodeId = A.StatusCodeId ")
    '        ' ''.Append("           AND A.sysStatusID = B.sysStatusID ")
    '        ' ''.Append("           AND B.SysStatusDescrip='No Start'")
    '        ' ''.Append("           AND EXISTS (SELECT * FROM adLeads L1 ")
    '        ' ''.Append("                           WHERE E.LeadId = L1.LeadId ")
    '        ' ''.Append("                           AND arTerm.StartDate<=L1.ExpectedStart AND L1.ExpectedStart<=arTerm.EndDate ")
    '        ' ''.Append("                           AND L1.AdmissionsRep=adLeads.AdmissionsRep)) AS Cancelled ")
    '        ' ''.Append("FROM       adLeads,syCampGrps,syCmpGrpCmps,syCampuses,syUsers C,arTerm ")
    '        ' ''.Append("WHERE      syCmpGrpCmps.CampGrpId = syCampGrps.CampGrpId ")
    '        ' ''.Append("           AND syCmpGrpCmps.CampusId=syCampuses.CampusId ")
    '        ' ''.Append("           AND syCampuses.CampusId=adLeads.CampusId ")
    '        ' ''.Append("           AND adLeads.AdmissionsRep = C.UserId ")
    '        ' ''.Append("           AND arTerm.CampGrpId=syCampGrps.CampGrpId ")
    '        ' ''.Append(strWhere)
    '        ' ''.Append("GROUP BY   syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,adLeads.CampusId,syCampuses.CampDescrip,adLeads.AdmissionsRep,C.FullName,arTerm.StartDate,arTerm.EndDate,arTerm.TermDescrip ")
    '        ' ''.Append("ORDER BY   syCampGrps.CampGrpDescrip,syCampGrps.CampGrpId,syCampuses.CampDescrip,adLeads.CampusId,C.FullName,adLeads.AdmissionsRep,arTerm.StartDate,arTerm.EndDate,arTerm.TermDescrip;")
    '        'StatusCodes table
    '        '.Append("SELECT     (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=8 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS NoStart,")
    '        '.Append("           (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=8 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS NSCode,")
    '        '.Append("           (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=6 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS Enrolled,")
    '        '.Append("           (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=6 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ECode;")
    '        .Append("SELECT     (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=8 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS NoStart,")
    '        .Append("           (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=8 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS NSCode,")
    '        .Append("           (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=6 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS Enrolled,")
    '        .Append("           (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=6 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ECode;")
    '    End With

    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '    db.OpenConnection()

    '    ds = db.RunParamSQLDataSet(sb.ToString)

    '    If ds.Tables.Count = 2 Then
    '        Dim dt As DataTable = ds.Tables(0)
    '        dt.TableName = "StartDateRepPerformance"
    '        ds.Tables(1).TableName = "StatusCodeDescrip"

    '        'Dim dc As DataColumn = dt.Columns.Add("Starts", Type.GetType("System.Int32"))
    '        'dc.AllowDBNull = True
    '        'dc.Expression = "Enrolled - Cancelled"

    '        'dt.Columns.Add("StartRate", Type.GetType("System.String"))
    '        dt.Columns.Add("CampGrpIdStr", Type.GetType("System.String"))
    '        dt.Columns.Add("CampusIdStr", Type.GetType("System.String"))
    '        dt.Columns.Add("AdRepCount", Type.GetType("System.Int32"))


    '        dt = New DataTable("CampusGroupTotals")
    '        dt.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("Enrolled", System.Type.GetType("System.Int32")))
    '        dt.Columns.Add(New DataColumn("Cancelled", System.Type.GetType("System.Int32")))
    '        dt.Columns.Add(New DataColumn("Starts", System.Type.GetType("System.Int32")))
    '        dt.Columns.Add(New DataColumn("StartRate", System.Type.GetType("System.String")))
    '        ds.Tables.Add(dt)

    '        dt = New DataTable("CampusTotals")
    '        dt.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("CampusIdStr", System.Type.GetType("System.String")))
    '        dt.Columns.Add(New DataColumn("Enrolled", System.Type.GetType("System.Int32")))
    '        dt.Columns.Add(New DataColumn("Cancelled", System.Type.GetType("System.Int32")))
    '        dt.Columns.Add(New DataColumn("Starts", System.Type.GetType("System.Int32")))
    '        dt.Columns.Add(New DataColumn("StartRate", System.Type.GetType("System.String")))
    '        ds.Tables.Add(dt)

    '    End If

    '    Return ds
    'End Function

    Public Function GetPackagingListByRep(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String = ""
        Dim strOrderBy As String = ""
        Dim strStudentId As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        If StudentIdentifier = "SSN" Then
            strStudentId = "arStudent.SSN AS StudentIdentifier, "
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStudentId = "arStuEnrollments.EnrollmentId AS StudentIdentifier, "
        ElseIf StudentIdentifier = "StudentId" Then
            strStudentId = "arStudent.StudentNumber AS StudentIdentifier, "
        End If
        'build query with subqueries
        With sb
            'PackagingListByRep table
            .Append("SELECT ")
            .Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId,syCampuses.CampDescrip,")
            .Append("       arStuEnrollments.StuEnrollId,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName," & strStudentId)
            .Append("       (SELECT StatusCodeDescrip FROM syStatusCodes WHERE arStuEnrollments.StatusCodeId=StatusCodeId) AS StatusCodeDescrip,")
            .Append("       (SELECT U.FullName FROM syUsers U WHERE arStuEnrollments.AdmissionsRep=U.UserId) AS AdmissionRep,")
            .Append("       (SELECT U.FullName FROM syUsers U WHERE arStuEnrollments.FAAdvisorId=U.UserId) AS FAAdvisor,")
            .Append("       arStuEnrollments.PrgVerId,arPrgVersions.PrgVerDescrip,arStuEnrollments.EnrollDate,")
            .Append("       (SELECT TOP 1 AwardStartDate ")
            .Append("           FROM faStudentAwards ")
            .Append("           WHERE StuEnrollId = arStuEnrollments.StuEnrollId ")
            .Append("           ORDER BY AwardStartDate, AwardEndDate) AS TopAwardDate,")
            .Append("       (SELECT TOP 1 PayPlanStartDate ")
            .Append("           FROM faStudentPaymentPlans ")
            .Append("           WHERE StuEnrollId = arStuEnrollments.StuEnrollId ")
            .Append("           ORDER BY PayPlanStartDate, PayPlanEndDate) AS TopPayPlanDate,")
            .Append("       (SELECT TOP 1 SUM((GrossAmount - LoanFees)) ")
            .Append("           FROM faStudentAwards ")
            .Append("           WHERE StuEnrollId = arStuEnrollments.StuEnrollId ")
            .Append("           GROUP BY AwardStartDate, AwardEndDate ")
            .Append("           ORDER BY AwardStartDate, AwardEndDate) AS TopAwardAmount, ")
            .Append("       (SELECT TOP 1 SUM(TotalAmountDue) ")
            .Append("           FROM faStudentPaymentPlans ")
            .Append("           WHERE StuEnrollId = arStuEnrollments.StuEnrollId ")
            .Append("           GROUP BY PayPlanStartDate,PayPlanEndDate ")
            .Append("           ORDER BY PayPlanStartDate, PayPlanEndDate) AS TopPayPlanAmount ")
            .Append("FROM   arStuEnrollments,arStudent,arPrgVersions,syCmpGrpCmps,syCampGrps,syCampuses ")
            .Append("WHERE  syCmpGrpCmps.CampGrpId = syCampGrps.CampGrpId ")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId ")
            .Append("       AND syCampuses.CampusId=arStuEnrollments.CampusId ")
            .Append("       AND arStuEnrollments.StudentId=arStudent.StudentId ")
            .Append("       AND arStuEnrollments.PrgVerId=arPrgVersions.PrgVerId ")
            .Append(strWhere)
            .Append("ORDER BY syCampGrps.CampGrpDescrip,syCampGrps.CampGrpId,syCampuses.CampDescrip,arStuEnrollments.CampusId,AdmissionRep,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count = 1 Then
            Dim dt As DataTable = ds.Tables(0)
            dt.TableName = "PackagingListByRep"

            dt.Columns.Add("FirstProjectedDate", Type.GetType("System.DateTime"))
            dt.Columns.Add("Days", Type.GetType("System.Int32"))
            dt.Columns.Add("FirstProjectedAmount", Type.GetType("System.Decimal"))
            dt.Columns.Add("StudentName", Type.GetType("System.String"))
            dt.Columns.Add("StudentCount", Type.GetType("System.Int32"))
            dt.Columns.Add("CampGrpIdStr", Type.GetType("System.String"))
            dt.Columns.Add("CampusIdStr", Type.GetType("System.String"))

            dt = New DataTable("CampusGroupTotals")
            dt.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("StudentTotal", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("ProjectedStudents", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("ProjectedRate", System.Type.GetType("System.String")))
            ds.Tables.Add(dt)

            dt = New DataTable("CampusTotals")
            dt.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("StudentTotal", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("ProjectedStudents", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("ProjectedRate", System.Type.GetType("System.String")))
            ds.Tables.Add(dt)

            dt = New DataTable("AdmissionRepTotals")
            dt.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("AdmissionRep", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("StudentTotal", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("ProjectedStudents", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("ProjectedRate", System.Type.GetType("System.String")))
            ds.Tables.Add(dt)

        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Public Function GetAdmissionsWeeklyReport(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim idx As Integer
        Dim idx2 As Integer
        Dim strWhere As String = ""
        Dim strOrderBy As String = ""
        Dim refDate As String = Date.Today.ToShortDateString

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            'Parse FilterOther in order to get End Date
            idx = paramInfo.FilterOther.IndexOf("BETWEEN")
            If idx <> -1 Then
                idx2 = paramInfo.FilterOther.IndexOf("AM")
                If idx2 <> -1 Then
                    refDate = paramInfo.FilterOther.Substring(idx + 9, idx2 + 2 - (idx + 8)).Trim
                Else
                    refDate = paramInfo.FilterOther.Substring(idx + 8).Trim
                End If
                Dim idx3 As Integer
                idx3 = refDate.IndexOf(" ")
                If idx3 > 0 Then
                    refDate = refDate.Substring(0, idx3).Trim
                End If
            End If
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        With sb
            '-- All campus groups and their campuses
            .Append("SELECT ")
            .Append("		syCampGrps.CampGrpId,")
            .Append("		syCampGrps.CampGrpDescrip,")
            .Append("		syCampuses.CampusId,")
            .Append("		syCampuses.CampDescrip ")
            .Append("FROM   syCmpGrpCmps,syCampGrps,syCampuses ")
            .Append("WHERE  syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId")
            .Append("		AND syCampuses.CampusId=syCmpGrpCmps.CampusId ")
            .Append(strWhere)
            .Append(" ORDER BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip;")
            '-- Inquiries
            .Append("SELECT")
            .Append("       CampGrpId,")
            .Append("       CampusId,")
            .Append("		SourceCategoryID,")
            .Append("       Category,")
            .Append("       WeeklyCount,")
            .Append("       CummCount ")
            .Append("FROM (	SELECT DISTINCT")
            .Append("		        syCampGrps.CampGrpId,")
            .Append("		        adLeads.CampusId,")
            .Append("		        SourceCategoryID,")
            .Append("		        SourceCatagoryDescrip AS Category,")
            .Append("		        (SELECT COUNT(*)")
            .Append("                   FROM adLeads X ")
            .Append("                   WHERE X.SourceCategoryID=adLeads.SourceCategoryID")
            .Append("			        AND SourceDate>=? AND SourceDate<?")
            .Append("			        AND X.CampusId=adLeads.CampusId) AS WeeklyCount,")
            .Append("		        (SELECT COUNT(*)")
            .Append("                   FROM adLeads X ")
            .Append("                   WHERE X.SourceCategoryID=adLeads.SourceCategoryID")
            .Append("			        AND SourceDate>=? AND SourceDate<?")
            .Append("			        AND X.CampusId=adLeads.CampusId) AS CummCount ")
            .Append("       FROM    adLeads,syCmpGrpCmps,syCampGrps,syCampuses,adSourceCatagory ")
            .Append("       WHERE   adLeads.SourceCategoryID=adSourceCatagory.SourceCatagoryId")
            .Append("               AND syCmpGrpCmps.CampusId=adLeads.CampusId")
            .Append("               AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId")
            .Append("               AND syCampuses.CampusId=adLeads.CampusId")
            .Append(strWhere)
            .Append("       GROUP BY syCampGrps.CampGrpId,adLeads.CampusId,SourceCategoryID,SourceCatagoryDescrip")
            .Append(") X ")
            .Append("WHERE WeeklyCount> 0 OR CummCount > 0 ")
            .Append("ORDER BY Category;")
            '-- Lead Statuses
            .Append("SELECT	")
            .Append("       syCampGrps.CampGrpId,")
            .Append("       B.CampusId,")
            .Append("       S.SysStatusId,")
            .Append("       S.StatusCodeId,")
            .Append("       S.StatusCode,")
            .Append("       S.StatusCodeDescrip,")
            .Append("       WeeklyCount=(SELECT	COUNT(*)")
            .Append("                   FROM	syLeadStatusesChanges X,adLeads L")
            .Append("                   WHERE	X.ModDate>=? AND X.ModDate<?")
            .Append("		                    AND X.NewStatusId=S.StatusCodeId")
            .Append("		                    AND X.LeadId=L.LeadId AND L.CampusId=B.CampusId")
            .Append("		                    AND EXISTS (SELECT  * FROM syUsers U, syUsersRolesCampGrps Q")
            .Append("		                                WHERE   U.UserId=Q.UserId")
            .Append("		                                        AND U.userid=L.AdmissionsRep")
            .Append("		                                        AND Q.RoleId IN (SELECT RoleId FROM syRoles WHERE SysRoleId IN (3,8))")
            .Append("		                                        AND Q.CampGrpId=syCampGrps.CampGrpId)")
            .Append("       ),")
            .Append("       CummCount=(SELECT	COUNT(*)")
            .Append("                   FROM	syLeadStatusesChanges X,adLeads L")
            .Append("                   WHERE	X.ModDate>=? AND X.ModDate<?")
            .Append("		                    AND X.NewStatusId=S.StatusCodeId")
            .Append("		                    AND X.LeadId=L.LeadId AND L.CampusId=B.CampusId")
            .Append("		                    AND EXISTS (SELECT  * FROM syUsers U, syUsersRolesCampGrps Q")
            .Append("		                                WHERE   U.UserId=Q.UserId")
            .Append("		                                        AND U.userid=L.AdmissionsRep")
            .Append("		                                        AND Q.RoleId IN (SELECT RoleId FROM syRoles WHERE SysRoleId IN (3,8))")
            .Append("		                                        AND Q.CampGrpId=syCampGrps.CampGrpId)")
            .Append("       ),")
            .Append("       Depth=(SELECT   COUNT(*)")
            .Append("               FROM    syLeadStatusChanges Y")
            .Append("               WHERE   Y.newStatusId=S.StatusCodeId")
            .Append("                       AND Y.campGrpId=syCampGrps.CampGrpId) ")
            .Append("FROM	syCampGrps,syCampuses B,syCmpGrpCmps C,syStatusCodes S ")
            .Append("WHERE  syCampGrps.CampGrpId=C.CampGrpId AND B.CampusId=C.CampusId")
            .Append("       AND syCampGrps.CampGrpId=S.CampGrpId")
            .Append("       AND S.SysStatusId IN (SELECT SysStatusId FROM sySysStatus WHERE StatusLevelId=(SELECT StatusLevelId FROM syStatusLevels WHERE StatusLevelDescrip='Lead'))")
            .Append(strWhere)
            .Append(" GROUP BY syCampGrps.CampGrpId,B.CampusId,S.SysStatusId,S.StatusCodeId,S.StatusCode,S.StatusCodeDescrip")
            .Append(" UNION ")
            .Append("SELECT ")
            .Append("       syCampGrps.CampGrpId,")
            .Append("       B.CampusId,")
            .Append("       SysStatusId=(8),")
            .Append("       StatusCodeId=(SELECT TOP 1 A.StatusCodeId FROM syStatusCodes A,syStatuses B,syCampGrps Q WHERE A.SysStatusId=8 AND A.StatusId=B.StatusId AND B.Status='Active' AND Q.CampGrpId=A.CampGrpId AND Q.CampGrpId=syCampGrps.CampGrpId),")
            .Append("       StatusCode=(SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B,syCampGrps Q WHERE A.SysStatusId=8 AND A.StatusId=B.StatusId AND B.Status='Active' AND Q.CampGrpId=A.CampGrpId AND Q.CampGrpId=syCampGrps.CampGrpId),")
            .Append("       StatusCodeDescrip=(SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B,syCampGrps Q WHERE A.SysStatusId=8 AND A.StatusId=B.StatusId AND B.Status='Active' AND Q.CampGrpId=A.CampGrpId AND Q.CampGrpId=syCampGrps.CampGrpId),")
            .Append("       WeeklyCount=(SELECT COUNT(*)")
            .Append(" 				    FROM    arStuEnrollments E,syStatusCodes A,sySysStatus W")
            .Append("                   WHERE   E.StatusCodeId=A.StatusCodeId AND A.sysStatusID=W.sysStatusID")
            .Append(" 				            AND W.SysStatusId=8 AND E.CampusId=B.CampusId")
            .Append(" 				            AND (EXISTS (SELECT * FROM adLeads L1 ")
            .Append("                                       WHERE   E.LeadId = L1.LeadId")
            .Append(" 								                AND L1.ExpectedStart>=? AND L1.ExpectedStart<?")
            .Append(" 								                AND L1.CampusId=B.CampusId)")
            .Append(" 					             OR (E.StartDate>=? AND E.StartDate<?))")
            .Append(" 		),")
            .Append("       CummCount=(SELECT   COUNT(*)")
            .Append(" 				    FROM    arStuEnrollments E,syStatusCodes A,sySysStatus W")
            .Append("                   WHERE   E.StatusCodeId=A.StatusCodeId AND A.sysStatusID=W.sysStatusID")
            .Append(" 				            AND W.SysStatusId=8 AND E.CampusId=B.CampusId")
            .Append(" 				            AND (EXISTS (SELECT * FROM adLeads L1")
            .Append("                                       WHERE   E.LeadId=L1.LeadId")
            .Append(" 								                AND L1.ExpectedStart>=? AND L1.ExpectedStart<?")
            .Append(" 								                AND L1.CampusId=B.CampusId)")
            .Append(" 					            OR (E.StartDate>=? AND E.StartDate<?))")
            .Append("       ),")
            .Append("       Depth=9999 ")
            .Append("FROM   syCampGrps,syCampuses B,syCmpGrpCmps C ")
            .Append("WHERE  syCampGrps.CampGrpId=C.CampGrpId AND B.CampusId=C.CampusId ")
            .Append(strWhere)
            .Append(" GROUP BY syCampGrps.CampGrpId,B.CampusId")
            .Append(" ORDER BY syCampGrps.CampGrpId,B.CampusId,Depth,S.StatusCodeDescrip;")
            '-- Documents and Tests: six queries in a UNION
            '--     Passed tests
            .Append("SELECT DISTINCT")
            .Append("       syCampGrps.CampGrpId,")
            .Append("       B.CampusId,")
            .Append("       'Tests' AS ReqType,")
            .Append("       1 AS ViewOrder,")
            .Append("       'Passed' AS Descrip,")
            .Append("       (SELECT	COUNT(*)")
            .Append("       FROM	adLeadEntranceTest X,adLeads L")
            .Append("       WHERE	X.ModDate>=? AND X.ModDate<?")
            .Append("               AND X.Pass=1")
            .Append("               AND X.LeadId=L.LeadId AND L.CampusId=B.CampusId) AS WeeklyCount,")
            .Append("       (SELECT	COUNT(*)")
            .Append("       FROM	adLeadEntranceTest X,adLeads L")
            .Append("       WHERE	X.ModDate>=? AND X.ModDate<?")
            .Append("               AND X.Pass=1")
            .Append("               AND X.LeadId=L.LeadId AND L.CampusId=B.CampusId) AS CummCount ")
            .Append("FROM	syCampGrps,syCampuses B,syCmpGrpCmps C,adReqs R ")
            .Append("WHERE  syCampGrps.CampGrpId=C.CampGrpId AND B.CampusId=C.CampusId ")
            .Append("       AND syCampGrps.CampGrpId=R.CampGrpId")
            .Append("       AND R.adReqTypeId=1")
            .Append(strWhere)
            .Append(" UNION ")
            '--     Overriden tests
            .Append("Select DISTINCT")
            .Append("       syCampGrps.CampGrpId,")
            .Append("       B.CampusId,")
            .Append("       'Tests' AS ReqType,")
            .Append("       2 AS ViewOrder,")
            .Append("       'Overriden' AS Descrip,")
            .Append("       (SELECT	COUNT(*)")
            .Append("       FROM	adLeadEntranceTest X,adLeads L")
            .Append("       WHERE	X.ModDate>=? AND X.ModDate<?")
            .Append("               AND X.Override=1")
            .Append("               AND X.LeadId=L.LeadId AND L.CampusId=B.CampusId) AS WeeklyCount,")
            .Append("       (SELECT	COUNT(*)")
            .Append("       FROM	adLeadEntranceTest X,adLeads L")
            .Append("       WHERE	X.ModDate>=? AND X.ModDate<?")
            .Append("               and X.Override=1")
            .Append("               and X.LeadId=L.LeadId and L.CampusId=B.CampusId) AS CummCount ")
            .Append("FROM	syCampGrps,syCampuses B,syCmpGrpCmps C,adReqs R ")
            .Append("WHERE  syCampGrps.CampGrpId=C.CampGrpId AND B.CampusId=C.CampusId")
            .Append("       AND syCampGrps.CampGrpId=R.CampGrpId")
            .Append("       AND R.adReqTypeId=1")
            .Append(strWhere)
            .Append(" UNION ")
            '--     Completed means passed or overriden tests
            .Append("Select DISTINCT")
            .Append("       syCampGrps.CampGrpId,")
            .Append("       B.CampusId,")
            .Append("       'Tests' AS ReqType,")
            .Append("       4 AS ViewOrder,")
            .Append("       'Completed' AS Descrip,")
            .Append("       (SELECT	COUNT(*)")
            .Append("       FROM	adLeadEntranceTest X,adLeads L")
            .Append("       WHERE	X.ModDate>=? AND X.ModDate<?")
            .Append("               AND (X.Pass=1 OR X.Override=1)")
            .Append("               AND X.LeadId=L.LeadId AND L.CampusId=B.CampusId) AS WeeklyCount,")
            .Append("       (SELECT	COUNT(*)")
            .Append("       FROM	adLeadEntranceTest X,adLeads L")
            .Append("       WHERE	X.ModDate>=? AND X.ModDate<?")
            .Append("               AND (X.Pass=1 or X.Override=1)")
            .Append("               AND X.LeadId=L.LeadId AND L.CampusId=B.CampusId) AS CummCount ")
            .Append("FROM	syCampGrps,syCampuses B,syCmpGrpCmps C,adReqs R ")
            .Append("WHERE  syCampGrps.CampGrpId=C.CampGrpId AND B.CampusId=C.CampusId")
            .Append("       AND syCampGrps.CampGrpId=R.CampGrpId")
            .Append("       AND R.adReqTypeId=1")
            .Append(strWhere)
            .Append(" UNION ")
            '--     Pending tests means not passed and not overriden
            .Append("Select DISTINCT")
            .Append("       syCampGrps.CampGrpId,")
            .Append("       B.CampusId,")
            .Append("       'Tests' AS ReqType,")
            .Append("       0 AS ViewOrder,")
            .Append("       'Pending' AS Descrip,")
            .Append("       (SELECT	COUNT(*)")
            .Append("       FROM	adLeadEntranceTest X,adLeads L")
            .Append("       WHERE	X.ModDate>=? AND X.ModDate<?")
            .Append("               AND X.Pass=0 AND X.Override=0")
            .Append("               AND X.LeadId=L.LeadId AND L.CampusId=B.CampusId) AS WeeklyCount,")
            .Append("       (SELECT	COUNT(*)")
            .Append("       FROM	adLeadEntranceTest X,adLeads L")
            .Append("       WHERE	X.ModDate>=? AND X.ModDate<?")
            .Append("               AND X.Pass=0 AND X.Override=0")
            .Append("               AND X.LeadId=L.LeadId AND L.CampusId=B.CampusId) AS CummCount ")
            .Append("FROM	syCampGrps,syCampuses B,syCmpGrpCmps C,adReqs R ")
            .Append("WHERE  syCampGrps.CampGrpId=C.CampGrpId AND B.CampusId=C.CampusId")
            .Append("       AND syCampGrps.CampGrpId=R.CampGrpId")
            .Append("       AND R.adReqTypeId=1")
            .Append(strWhere)
            .Append(" UNION ")
            '--     Pending Documents (mapping to the 'Not Approved' system document status)
            .Append("Select DISTINCT")
            .Append("       syCampGrps.CampGrpId,")
            .Append("       B.CampusId,")
            .Append("       'Documents' AS ReqType,")
            .Append("       0 AS ViewOrder,")
            .Append("       'Pending' AS Descrip,")
            .Append("       (SELECT	COUNT(*)")
            .Append("       FROM	adLeadDocsReceived X,adLeads L")
            .Append("       WHERE	X.ModDate>=? AND X.ModDate<?")
            .Append("               AND X.DocStatusId IN (SELECT DocStatusId FROM syDocStatuses WHERE sysDocStatusId=2)")
            .Append("               AND X.LeadId=L.LeadId AND L.CampusId=B.CampusId) AS WeeklyCount,")
            .Append("       (SELECT	COUNT(*)")
            .Append("       FROM	adLeadDocsReceived X,adLeads L")
            .Append("       WHERE	X.ModDate>=? AND X.ModDate<?")
            .Append("               AND X.DocStatusId IN (SELECT DocStatusId FROM syDocStatuses WHERE sysDocStatusId=2)")
            .Append("               AND X.LeadId=L.LeadId and L.CampusId=B.CampusId) AS CummCount ")
            .Append("FROM	syCampGrps,syCampuses B,syCmpGrpCmps C,adReqs R ")
            .Append("WHERE  syCampGrps.CampGrpId=C.CampGrpId AND B.CampusId=C.CampusId")
            .Append("       AND syCampGrps.CampGrpId=R.CampGrpId")
            .Append("       AND R.adReqTypeId=3")
            .Append(strWhere)
            .Append(" UNION ")
            '--     Overriden documents
            .Append("Select DISTINCT")
            .Append("       syCampGrps.CampGrpId,")
            .Append("       B.CampusId,")
            .Append("       'Documents' AS ReqType,")
            .Append("       2 AS ViewOrder,")
            .Append("       'Overriden' AS Descrip,")
            .Append("       (SELECT	COUNT(*)")
            .Append("       FROM	adLeadDocsReceived X,adLeads L")
            .Append("       WHERE	X.ModDate>=? AND X.ModDate<?")
            .Append("               AND X.Override=1")
            .Append("               AND X.LeadId=L.LeadId AND L.CampusId=B.CampusId) AS WeeklyCount,")
            .Append("       (SELECT	COUNT(*)")
            .Append("       FROM	adLeadDocsReceived X,adLeads L")
            .Append("       WHERE	X.ModDate>=? AND X.ModDate<?")
            .Append("               AND X.Override=1")
            .Append("               AND X.LeadId=L.LeadId AND L.CampusId=B.CampusId) AS CummCount ")
            .Append("FROM	syCampGrps,syCampuses B,syCmpGrpCmps C,adReqs R ")
            .Append("WHERE  syCampGrps.CampGrpId=C.CampGrpId AND B.CampusId=C.CampusId")
            .Append("       AND syCampGrps.CampGrpId=R.CampGrpId")
            .Append("       AND R.adReqTypeId=3")
            .Append(strWhere)
            .Append(" UNION ")
            '--     Received means approved or overriden documents
            .Append("Select DISTINCT")
            .Append("       syCampGrps.CampGrpId,")
            .Append("       B.CampusId,")
            .Append("       'Documents' AS ReqType,")
            .Append("       4 AS ViewOrder,")
            .Append("       'Received' AS Descrip,")
            .Append("       (SELECT	COUNT(*)")
            .Append("       FROM	adLeadDocsReceived X,adLeads L")
            .Append("       WHERE	X.ModDate>=? AND X.ModDate<?")
            .Append("               AND (X.DocStatusId IN (SELECT DocStatusId FROM syDocStatuses WHERE sysDocStatusId=1) OR X.Override=1)")
            .Append("               AND X.LeadId=L.LeadId AND L.CampusId=B.CampusId) AS WeeklyCount,")
            .Append("       (SELECT	COUNT(*)")
            .Append("       FROM	adLeadDocsReceived X,adLeads L")
            .Append("       WHERE	X.ModDate>=? AND X.ModDate<?")
            .Append("               AND (X.DocStatusId IN (SELECT DocStatusId FROM syDocStatuses WHERE sysDocStatusId=1) OR X.Override=1)")
            .Append("               AND X.LeadId=L.LeadId and L.CampusId=B.CampusId) AS CummCount ")
            .Append("FROM	syCampGrps,syCampuses B,syCmpGrpCmps C,adReqs R ")
            .Append("WHERE  syCampGrps.CampGrpId=C.CampGrpId AND B.CampusId=C.CampusId")
            .Append("       AND syCampGrps.CampGrpId=R.CampGrpId")
            .Append("       AND R.adReqTypeId=3")
            .Append(strWhere)
            .Append(" ORDER BY ReqType,ViewOrder;")
            '-- Placement Information
            .Append("SELECT DISTINCT")
            .Append("       syCampGrps.CampGrpId,")
            .Append("       E.CampusId,")
            .Append("       S.LastName,")
            .Append("       S.FirstName,")
            .Append("       S.MiddleName,")
            .Append("       E.ExpGradDate,")
            .Append("       (SELECT TOP 1 EmployerDescrip FROM plEmployers E, plEmployerJobs EJ WHERE EJ.EmployerJobId=P.EmployerJobId AND EJ.EmployerId=E.EmployerId) AS Employer,")
            .Append("       (SELECT TOP 1 Address1 FROM plEmployers E,plEmployerJobs EJ WHERE EJ.EmployerJobId=P.EmployerJobId AND EJ.EmployerId=E.EmployerId) AS Address1,")
            .Append("       (SELECT TOP 1 Address2 FROM plEmployers E,plEmployerJobs EJ WHERE EJ.EmployerJobId=P.EmployerJobId AND EJ.EmployerId=E.EmployerId) AS Address2,")
            .Append("       (SELECT TOP 1 City FROM plEmployers E,plEmployerJobs EJ WHERE EJ.EmployerJobId=P.EmployerJobId AND EJ.EmployerId=E.EmployerId) AS City,")
            .Append("       (SELECT TOP 1 StateDescrip FROM plEmployers E,plEmployerJobs EJ,syStates S WHERE EJ.EmployerJobId=P.EmployerJobId AND EJ.EmployerId=E.EmployerId AND E.StateId=S.StateId) AS State,")
            .Append("       (SELECT TOP 1 OtherState FROM plEmployers E,plEmployerJobs EJ,syStates S WHERE EJ.EmployerJobId=P.EmployerJobId AND EJ.EmployerId=E.EmployerId) AS OtherState,")
            .Append("       (SELECT TOP 1 ForeignZip FROM plEmployers E,plEmployerJobs EJ,syStates S WHERE EJ.EmployerJobId=P.EmployerJobId AND EJ.EmployerId=E.EmployerId) AS ForeignZip,")
            .Append("       (SELECT TOP 1 Zip FROM plEmployers E,plEmployerJobs EJ,syStates S WHERE EJ.EmployerJobId=P.EmployerJobId AND EJ.EmployerId=E.EmployerId) AS Zip,")
            .Append("       (SELECT TOP 1 CountryDescrip FROM plEmployers E,plEmployerJobs EJ,adCountries C WHERE EJ.EmployerJobId=P.EmployerJobId AND EJ.EmployerId=E.EmployerId AND E.CountryId=C.CountryId) AS Country ")
            .Append("FROM	plStudentsPlaced P,arStuEnrollments E,arStudent S,")
            .Append("       syCmpGrpCmps CC,syCampGrps,syCampuses B ")
            .Append("WHERE  P.StuEnrollId=E.StuEnrollId")
            .Append("       AND E.StudentId=S.StudentId")
            .Append("       AND E.CampusId=B.CampusId")
            .Append("       AND CC.CampusId=E.CampusId ")
            .Append("       AND syCampGrps.CampGrpId=CC.CampGrpId ")
            .Append("       AND P.PlacedDate>=? AND P.PlacedDate<?")
            .Append(strWhere)
            .Append(" ORDER BY S.LastName,S.FirstName,S.MiddleName,E.ExpGradDate;")
            '-- Admissions Rep Performance
            '--		Roles: Admissions Reps and Director of Admissions
            '--		Statuses: New Lead(1), Scheduled Interview(4), Interviewed(5), Enrolled(6), No Start(8)
            .Append("SELECT DISTINCT")
            .Append("       syCampGrps.CampGrpId,")
            .Append("       B.CampusId,")
            .Append("       URCG.UserId,")
            .Append("       (SELECT FullName FROM syUsers WHERE UserId=URCG.UserId) AS AdmissionsRep,")
            .Append("       S.SysStatusId,")
            .Append("       StatusCodeDescription=(SELECT TOP 1 A.StatusCodeDescrip")
            .Append("						        FROM    syStatusCodes A,syStatuses B")
            .Append("                               WHERE   A.SysStatusId= S.SysStatusId")
            .Append("						                AND A.StatusId=B.StatusId ")
            .Append("						                AND B.Status='Active'")
            .Append("						        ORDER BY A.StatusCodeDescrip),")
            .Append("       WeeklyCount=(CASE WHEN S.SysStatusId IN (1,4,5,6)")
            .Append("                       THEN")
            .Append("					        (SELECT COUNT(*)")
            .Append("					        FROM	adLeads L,syLeadStatusesChanges LSC,syStatusCodes SC")
            .Append("                           WHERE   L.LeadId=LSC.LeadId")
            .Append("							        AND LSC.ModDate>=? AND LSC.ModDate<?")
            .Append("							        AND L.AdmissionsRep=URCG.UserId")
            .Append("							        AND LSC.NewStatusId=SC.StatusCodeId")
            .Append("							        AND SC.SysStatusId=S.SysStatusId")
            .Append("							        AND L.CampusId=B.CampusId)")
            .Append("				        ELSE")
            .Append("					        (SELECT COUNT(*)")
            .Append("					        FROM    arStuEnrollments E,syStatusCodes A,sySysStatus B")
            .Append("                           WHERE   E.StatusCodeId=A.StatusCodeId")
            .Append("					                AND A.sysStatusID=B.sysStatusID ")
            .Append("					                AND B.SysStatusId=8")
            .Append("					                AND (EXISTS (SELECT * FROM adLeads L1 ")
            .Append("                                               WHERE   E.LeadId=L1.LeadId")
            .Append("									                    AND L1.ExpectedStart>=? AND L1.ExpectedStart<? ")
            .Append("									                    AND L1.AdmissionsRep=URCG.UserId)")
            .Append("						                OR (E.AdmissionsRep=URCG.UserId AND E.StartDate>=? AND E.StartDate<?))")
            .Append("					        )")
            .Append("				        END),")
            .Append("       CummCount=(CASE WHEN S.SysStatusId IN (1,4,5,6)")
            .Append("                       THEN")
            .Append("				            (SELECT COUNT(*)")
            .Append("				            FROM	adLeads L,syLeadStatusesChanges LSC,syStatusCodes SC")
            .Append("                           WHERE   L.LeadId=LSC.LeadId")
            .Append("						            AND LSC.ModDate>=? AND LSC.ModDate<?")
            .Append("						            AND L.AdmissionsRep=URCG.UserId")
            .Append("						            AND LSC.NewStatusId=SC.StatusCodeId")
            .Append("						            AND SC.SysStatusId=S.SysStatusId")
            .Append("						            AND L.CampusId=B.CampusId)")
            .Append("			            ELSE")
            .Append("				            (SELECT COUNT(*)")
            .Append("				            FROM    arStuEnrollments E,syStatusCodes A,sySysStatus B")
            .Append("                           WHERE   E.StatusCodeId=A.StatusCodeId")
            .Append("				                    AND A.sysStatusID=B.sysStatusID")
            .Append("				                    AND B.SysStatusId=8")
            .Append("				                    AND (EXISTS (SELECT * FROM adLeads L1")
            .Append("                                                   WHERE   E.LeadId=L1.LeadId")
            .Append("								                            AND L1.ExpectedStart>=? AND L1.ExpectedStart<?")
            .Append("								                            AND L1.AdmissionsRep=URCG.UserId)")
            .Append("					                    OR (E.AdmissionsRep=URCG.UserId AND E.StartDate>=? AND E.StartDate<?))")
            .Append("				            )				")
            .Append("			            END)")
            .Append("FROM   syCampGrps,syCampuses B,syCmpGrpCmps C,syUsersRolesCampGrps URCG,syStatusCodes S ")
            .Append("WHERE  syCampGrps.CampGrpId=C.CampGrpId")
            .Append("       AND B.CampusId=C.CampusId")
            .Append("		AND syCampGrps.CampGrpId=URCG.CampGrpId")
            .Append("		AND URCG.RoleId IN (SELECT RoleId FROM syRoles WHERE SysRoleId IN (3,8))")
            .Append("		AND syCampGrps.CampGrpId=S.CampGrpId")
            .Append("		AND S.SysStatusId IN (1,4,5,6,8)")
            .Append(strWhere)
            .Append(" ORDER BY syCampGrps.CampGrpId,B.CampusId,AdmissionsRep;")
        End With

        Dim myCulture As New System.Globalization.CultureInfo("en-US", True)
        Dim dtEndDate As Date = Date.Parse(refDate, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
        '
        m_CutOffDate = dtEndDate
        '
        Dim plusOneDay As Date = dtEndDate.AddDays(1)
        Dim minusSixDays As Date = dtEndDate.AddDays(-6)
        Dim firstOfYear As Date = Convert.ToDateTime("01/01/" & dtEndDate.Year)

        'Inquiry parameters
        db.AddParameter("@SourceDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", firstOfYear, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'Admission Statuses parameters
        db.AddParameter("@SourceDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", firstOfYear, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", firstOfYear, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", firstOfYear, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'Documents and Tests parameters
        db.AddParameter("@SourceDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", firstOfYear, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", firstOfYear, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", firstOfYear, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", firstOfYear, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", firstOfYear, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", firstOfYear, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", firstOfYear, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'Placement Information parameters
        db.AddParameter("@SourceDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'Admissions Rep Performance parameters
        db.AddParameter("@SourceDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", minusSixDays, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", firstOfYear, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", firstOfYear, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", firstOfYear, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@SourceDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count = 6 Then
            ds.Tables(0).TableName = "CampGrpsCampuses"
            '
            ds.Tables(1).TableName = "Inquiries"
            '
            ds.Tables(2).TableName = "LeadStatuses"
            '
            ds.Tables(3).TableName = "DocumentsAndTests"
            '
            ds.Tables(4).TableName = "Placements"
            ds.Tables(4).Columns.Add(New DataColumn("LeadName", System.Type.GetType("System.String")))
            ds.Tables(4).Columns.Add(New DataColumn("PlaceOfEmployment", System.Type.GetType("System.String")))

            ds.Tables(5).TableName = "AdRepPerformance"

            'Build the Appointments table and use the TM component to get data to populate it
            Dim dt As New DataTable("Appointments")
            With dt
                .Columns.Add(New DataColumn("CampGrpId", System.Type.GetType("System.Guid")))
                .Columns.Add(New DataColumn("CampusId", System.Type.GetType("System.Guid")))
                .Columns.Add(New DataColumn("Descrip", System.Type.GetType("System.String")))
                .Columns.Add(New DataColumn("ViewOrder", System.Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("WeeklyCount", System.Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("CummCount", System.Type.GetType("System.Int32")))
            End With
            ds.Tables.Add(dt)
            '
            'Build the Appointment Rates table to be populated off the Appointments DT
            dt = New DataTable("ApptRates")
            With dt
                .Columns.Add(New DataColumn("CampGrpId", System.Type.GetType("System.Guid")))
                .Columns.Add(New DataColumn("CampusId", System.Type.GetType("System.Guid")))
                .Columns.Add(New DataColumn("WeeklyCount", System.Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("CummCount", System.Type.GetType("System.Int32")))
            End With
            ds.Tables.Add(dt)
            '
            'Build the DocTest Rates table to be populated off the DocumentsAndTests DT
            dt = New DataTable("DocTestRates")
            With dt
                .Columns.Add(New DataColumn("CampGrpId", System.Type.GetType("System.Guid")))
                .Columns.Add(New DataColumn("CampusId", System.Type.GetType("System.Guid")))
                .Columns.Add(New DataColumn("ReqType", System.Type.GetType("System.String")))
                .Columns.Add(New DataColumn("WeeklyCount", System.Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("CummCount", System.Type.GetType("System.Int32")))
            End With
            ds.Tables.Add(dt)
        End If

        Return ds

    End Function

    ''Modified by Saraswathi lakshmanan
    ''join with arterm removed
    ''the students expected start date range is compared

    Public Function GetStartDateRepPerformance(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String = ""
        Dim strOrderBy As String = ""
        Dim strTerm As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim strCamp As String = ""
        If paramInfo.FilterList <> "" Then
            Dim idx As Integer
            Dim temp As String = paramInfo.FilterList
            idx = temp.IndexOf(" AND")
            If idx > -1 Then
                strCamp &= " AND " & temp.Substring(0, idx)
                strWhere &= temp.Substring(idx)
            Else
                strCamp &= " AND " & temp
                strWhere &= " "
            End If
        End If
        Dim strExpStartDate As String = ""
        Dim strLeadModDate As String = ""

        If paramInfo.FilterOther <> "" Then
            Dim idx As Integer
            Dim temp As String = paramInfo.FilterOther
            idx = temp.IndexOf("AND adLeads")
            If idx > -1 Then
                strLeadModDate = temp.Substring(idx)
                strExpStartDate &= " AND " & temp.Substring(0, idx)
            Else
                strLeadModDate = " AND " & temp
                strExpStartDate = " AND " & temp
            End If
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        If paramInfo.FilterList.Contains("arTerm.TermId") Then
            'For term Selected
            With sb

                'START datatable StartDateRepPerformance

                .Append("SELECT ")
                .Append("       CampGrpId,")
                .Append("       CampGrpDescrip,")
                .Append("       CampusId,")
                .Append("       CampDescrip,")
                .Append("       UserId AS AdmissionsRep,")
                .Append("       (SELECT FullName FROM syUsers WHERE UserId=Y.UserId) AS FullName,")
                .Append("       (SELECT StartDate FROM arTerm WHERE TermId=Y.TermId) AS StartDate,")
                .Append("       (SELECT EndDate FROM arTerm WHERE TermId=Y.TermId) AS EndDate,")
                .Append("       (SELECT TermDescrip FROM arTerm WHERE TermId=Y.TermId) AS TermDescrip,")
                .Append("       EnrolledCount AS Enrolled,")
                .Append("       CancelledCount AS Cancelled,")
                .Append("       Starts=(CASE WHEN (EnrolledCount-CancelledCount>0)")
                .Append("				        THEN (EnrolledCount-CancelledCount)")
                .Append("				        ELSE (0)")
                .Append("				        END),")
                .Append("       StartRate=(CASE WHEN (EnrolledCount<>0 AND (EnrolledCount-CancelledCount)>0)")
                .Append("				        THEN CONVERT(DECIMAL(6,2),(((EnrolledCount-CancelledCount)*1.0/EnrolledCount)*100))	")

                .Append("				        ELSE (0)")
                .Append("				        END)")
                .Append("FROM   (")
                .Append("       SELECT")
                .Append("	        CampGrpId,")
                .Append("	        CampGrpDescrip,")
                .Append("	        CampusId,")
                .Append("	        CampDescrip,")
                .Append("	        TermId,")
                .Append("	        UserId,")
                .Append("	        SUM(Enrollments) AS EnrolledCount,")
                .Append("	        SUM(Cancels) AS CancelledCount")
                .Append("       FROM	(")
                .Append("               SELECT DISTINCT")
                .Append("			                syCampGrps.CampGrpId,")
                .Append("			                syCampGrps.CampGrpDescrip,")
                .Append("			                B.CampusId,")
                .Append("			                B.CampDescrip,")
                .Append("			                URCG.UserId,")
                .Append("			                W.TermId,")
                .Append("			                W.Enrollments,")
                .Append("			                0 AS Cancels")
                .Append("		        FROM   syCampGrps,syCampuses B,syCmpGrpCmps C,syUsersRolesCampGrps URCG,")
                .Append("				        (	SELECT  AdmissionsRep,CampusId,TermId,COUNT(*) AS Enrollments")
                .Append("					        FROM	adLeads ,syStatusCodes SC,arTerm")
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms ")
                End If
                .Append("                           WHERE   adLeads.ExpectedStart=arTerm.StartDate ")
                .Append("                                   and adLeads.ProgramId=arTerm.ProgID  ")
                If strWhere.Contains("arPrograms") Then
                    .Append("                               and arprograms.ProgId=adLeads.ProgramID  ")
                End If
                .Append("							        AND adLeads.LeadStatus=SC.StatusCodeId")
                .Append("							        AND SC.SysStatusId=6 ")

                .Append(strWhere & strLeadModDate)
                .Append("					        GROUP BY adLeads.AdmissionsRep,adLeads.CampusId,TermId) W")
                .Append("               WHERE   syCampGrps.CampGrpId=C.CampGrpId")
                '.Append("			            AND B.CampusId=C.CampusId")
                '.Append("			            AND syCampGrps.CampGrpId=URCG.CampGrpId")
                .Append("			            AND URCG.RoleId IN (SELECT RoleId FROM syRoles WHERE SysRoleId IN (3,8))")
                .Append("			            AND W.AdmissionsRep=URCG.UserId AND W.CampusId=B.CampusId " & strCamp)
                '           leads --- ^
                .Append("               UNION ALL")
                '           students --- v
                .Append("               SELECT DISTINCT")
                .Append("			                syCampGrps.CampGrpId,")
                .Append("			                syCampGrps.CampGrpDescrip,")
                .Append("			                B.CampusId,")
                .Append("			                B.CampDescrip,")
                .Append("			                URCG.UserId,")
                .Append("			                W.TermId,")
                .Append("			                0 AS Enrollments,")
                .Append("                           W.Cancels")
                .Append("		        FROM   syCampGrps,syCampuses B,syCmpGrpCmps C,syUsersRolesCampGrps URCG,")
                .Append("				        (	SELECT E.AdmissionsRep,E.CampusId,TermId,COUNT(*) AS Cancels")
                .Append("					        FROM    arStuEnrollments E,syStatusCodes A, arTerm")
                .Append("                               ,arPrgVersions PV  ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append(" ,adleads ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms ")
                End If
                .Append("                           WHERE   E.StatusCodeId=A.StatusCodeId")
                .Append("                                   and PV.PrgverId=E.prgverid and PV.ProgId=arTerm.ProgID ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append("                               and  adleads.LeadId=E.LeadID  ")
                End If
                If strWhere.Contains("adLeads") Then
                    .Append("                               and adLeads.AdmissionsRep=E.AdmissionsRep  ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append("                               and arprograms.ProgID=PV.ProgID ")
                End If

                .Append("							        AND A.sysStatusID=8 ")
                .Append(strWhere & strLeadModDate)
                .Append("							        AND (EXISTS (SELECT * FROM adLeads L1 ")
                .Append("                                               WHERE   E.LeadId=L1.LeadId")
                .Append("												        AND L1.ExpectedStart=arTerm.StartDate")
                .Append("												        AND L1.AdmissionsRep=E.AdmissionsRep)")
                .Append("								        OR (E.ExpStartDate=arTerm.StartDate))")
                .Append("					        GROUP BY E.AdmissionsRep,E.CampusId,TermId) W")
                .Append("               WHERE   syCampGrps.CampGrpId=C.CampGrpId")
                .Append("			            AND B.CampusId=C.CampusId")
                .Append("			            AND syCampGrps.CampGrpId=URCG.CampGrpId")
                .Append("			            AND URCG.RoleId IN (SELECT RoleId FROM syRoles WHERE SysRoleId IN (3,8))")
                .Append("			            AND W.AdmissionsRep=URCG.UserId AND W.CampusId=B.CampusId " & strCamp)
                .Append("               ) Q ")
                .Append("       GROUP BY CampGrpId,CampGrpDescrip,CampusId,CampDescrip,TermId,UserId")
                .Append("       ) Y ")
                .Append("WHERE  Y.EnrolledCount>0 OR Y.CancelledCount>0 ")
                .Append("ORDER BY CampGrpDescrip,CampGrpId,CampDescrip,CampusId,StartDate,EndDate,TermDescrip,TermId,FullName,AdmissionsRep;")

                'END datatable StartDateRepPerformance
                'START datatable StatusCodeDescrip

                .Append("SELECT     (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=8 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId  " & strCamp & " ORDER BY A.StatusCodeDescrip) AS NoStart,")
                .Append("           (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=8 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId  " & strCamp & " ORDER BY A.StatusCodeDescrip) AS NSCode,")
                .Append("           (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=6 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId  " & strCamp & " ORDER BY A.StatusCodeDescrip) AS Enrolled,")
                .Append("           (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=6 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId  " & strCamp & " ORDER BY A.StatusCodeDescrip) AS ECode;")

                'END datatable StatusCodeDescrip
                'START datatable ProgProjectedAmount

                .Append(" Select ADmissionsRep,CampusID,CampGrpID,CampGrpDescrip,CampDescrip,")
                .Append(" (Select TermDescrip from arterm where termid=Res.termID)as TermDescrip, ")
                .Append(" Sum(TransAmount) TotTransAmount,Count(StuEnrollId) from  ")
                .Append(" (SELECT       SE.StuEnrollId,  SE.AdmissionsRep ,SE.CampusID,         ")
                .Append(" syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,B.CampDescrip, ")
                .Append(" TermID, ")
                .Append(" (Case LEN(PVF.RateScheduleId)             when 36 then              ")
                .Append(" (SELECT                Case RSD.FlatAmount                    When 0.00                        Then case RSD.UnitId          ")
                .Append(" when 0 then Rate*PV.Credits                                when 1 then Rate*PV.Hours                      ")
                .Append(" End 				    Else                            FlatAmount                    End             ")
                .Append(" FROM saRateSchedules RS, saRateScheduleDetails RSD                WHERE                      ")
                .Append(" RS.RateScheduleId = RSD.RateScheduleId And RS.RateScheduleId = PVF.RateScheduleId")
                .Append(" AND     (RSD.TuitionCategoryId = SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and ")
                .Append(" SE.TuitionCategoryId is null))                AND     ((RSD.FlatAmount=0.00 AND RSD.UnitId=0 ")
                .Append(" and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=PV.Credits ")
                .Append(" and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR ")
                .Append(" (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) or (RSD.FlatAmount=0.00 AND ")
                .Append(" RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=PV.Hours and RateScheduleId=RSD.RateScheduleId ")
                .Append(" and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) OR (RSD.FlatAmount>0.00 ")
                .Append(" and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=PV.Credits and RateScheduleId=RSD.RateScheduleId and ")
                .Append(" (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null))))))             else  ")
                .Append(" (Case PVF.UnitId                    when 0 Then PVF.Amount*PV.Credits                    when 1 then PVF.Amount*PV.Hours   ")
                .Append(" when 2 then PVF.Amount End)                End )TransAmount")
                .Append(", Convert(smalldatetime,getdate()) As TransDate")
                .Append(" FROM arStuEnrollments SE, arPrgVersions PV,")
                .Append(" saProgramVersionFees PVF ")
                .Append(" , syCampGrps,syCampuses B,syCmpGrpCmps C,syUsersRolesCampGrps URCG")
                .Append(" ,arTerm ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append(" ,adleads ")
                End If
                If strWhere.Contains("adLeads") Then
                    .Append(" ,SyStatusCodes SC")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms ")
                End If
                .Append("                 WHERE ")
                .Append(" arTerm.StartDate=SE.ExpStartDate ")
                .Append("  and PV.ProgId=arTerm.ProgID  ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append("                               and  adleads.LeadId=SE.LeadID  ")
                End If
                If strWhere.Contains("adLeads") Then
                    .Append("    and adLeads.AdmissionsRep=SE.AdmissionsRep and adLeads.ExpectedStart=arTerm.StartDate and adLeads.ProgramId=arTerm.ProgID  AND adLeads.LeadStatus=SC.StatusCodeId	   AND SC.SysStatusId=6    ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" and arprograms.ProgID=PV.ProgID ")
                End If
                .Append(strWhere & strLeadModDate)
                .Append(" AND SE.StuEnrollId in (	SELECT  SE1.StuEnrollId					 ")
                .Append(" FROM	arStuEnrollMents SE1, arTerm ")
                .Append("                               ,arPrgVersions PV  ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append(" ,adleads ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms ")
                End If
                .Append("       WHERE   ")
                .Append("       PV.PrgverId=SE1.prgverid and PV.ProgId=arTerm.ProgID ")


                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append("                               and  adleads.LeadId=SE1.LeadID  ")
                End If
                If strWhere.Contains("adLeads") Then
                    .Append("                               and adLeads.AdmissionsRep=SE1.AdmissionsRep  ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" and arprograms.ProgID=PV.ProgID ")
                End If
                .Append(strWhere & strLeadModDate & " ) ")
                .Append(" AND SE.PrgVerId = PV.PrgVerId And PV.PrgVerId = PVF.PrgVerId")
                .Append(" AND	(((PVF.RateScheduleId Is Null AND SE.TuitionCategoryId Is NULL AND PVF.TuitionCategoryId Is NULL) OR (PVF.RateScheduleId Is Null AND")
                .Append(" SE.TuitionCategoryId=PVF.TuitionCategoryId)) OR PVF.RateScheduleId Is Not Null)")
                .Append(" and")
                .Append(" syCampGrps.CampGrpId = C.CampGrpId")
                .Append(" AND B.CampusId=C.CampusId			            AND syCampGrps.CampGrpId=URCG.CampGrpId			          ")
                .Append(" AND URCG.RoleId IN (SELECT RoleId FROM syRoles WHERE SysRoleId IN (3,8))			            ")
                .Append(" AND SE.AdmissionsRep=URCG.UserId AND SE.CampusId=B.CampusId  " & strCamp)
                .Append("  ) Res  Group by AdmissionsRep,CampusID,CampGrpID,CampGrpDescrip,CampDescrip,TermID;")

                'END datatable ProgProjectedAmount
                'START datatable TermProjectedAmount

                .Append("    Select ADmissionsRep,CampusID,CampGrpID,CampGrpDescrip,CampDescrip,")
                .Append(" (Select TermDescrip from arterm where termid=Res.termID)as TermDescrip, ")
                .Append(" isNUll(Sum(TransAmount),0) TotTransAmount,Count(StuEnrollId) from   (SELECT  ")
                .Append("  SE.StuEnrollId,  SE.AdmissionsRep ,SE.CampusID,  syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,B.CampDescrip, ")
                .Append(" arterm.TermID,")
                .Append("  (Case LEN(PF.RateScheduleId)            when 36 then                (SELECT                    Case RSD.FlatAmount  ")
                .Append("  When 0.00        					Then case RSD.UnitId     when 0 then (SELECT   Coalesce(SUM(RQ.Credits*SRSD.Rate),0) ")
                .Append("  FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ, saRateSchedules SRS, saRateScheduleDetails SRSD, saPeriodicFees SPF WHERE R.TestId = CS.ClsSectionId  AND CS.ReqId=RQ.ReqId AND SRS.RateScheduleId=SRSD.RateScheduleId AND SRS.RateScheduleId=SPF.RateScheduleId AND ")
                .Append("  R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId and CST.TermId in ")
                .Append("  (Select Distinct TermID from arResults ,arClassSections where arClassSections.ClsSectionId=arResults.TestID and arResults.StuEnrollid in(")
                .Append("  Select SE1.StuEnrollId")
                .Append(" FROM	arStuEnrollMents SE1, arTerm ")
                .Append("                               ,arPrgVersions PV  ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append(" ,adleads ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms ")
                End If
                .Append("       WHERE ")
                .Append("      PV.PrgverId=SE1.prgverid and PV.ProgId=arTerm.ProgID ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append("                               and  adleads.LeadId=SE1.LeadID  ")
                End If
                If strWhere.Contains("adLeads") Then
                    .Append("                               and adLeads.AdmissionsRep=SE1.AdmissionsRep  ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" and arprograms.ProgID=PV.ProgID ")
                End If
                .Append(strWhere & strLeadModDate & " )) 	")
                .Append("  )                         ")
                .Append("  when 1 then (SELECT Coalesce(SUM(RQ.Hours*SRSD.Rate),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, ")
                .Append("  arReqs RQ , saRateSchedules SRS, saRateScheduleDetails SRSD, saPeriodicFees SPF WHERE  R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND SRS.RateScheduleId=SRSD.RateScheduleId AND SRS.RateScheduleId=SPF.RateScheduleId AND R.StuEnrollId=SE.StuEnrollId AND ")
                .Append("  CST.ClsSectionId=CS.ClsSectionId and CST.TermId in (Select Distinct TermID from arResults ,arClassSections  where")
                .Append("  arClassSections.ClsSectionId=arResults.TestID and arResults.StuEnrollid in")
                .Append("  (	SELECT  SE1.StuEnrollId		")
                .Append(" FROM	arStuEnrollMents SE1, arTerm ")
                .Append("                               ,arPrgVersions PV  ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append(" ,adleads ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms ")
                End If
                .Append("       WHERE  ")
                .Append("      PV.PrgverId=SE1.prgverid and PV.ProgId=arTerm.ProgID ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append("                               and  adleads.LeadId=SE1.LeadID  ")
                End If
                If strWhere.Contains("adLeads") Then
                    .Append("                               and adLeads.AdmissionsRep=SE1.AdmissionsRep  ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" and arprograms.ProgID=PV.ProgID ")
                End If
                .Append(strWhere & strLeadModDate & " )) 	")
                .Append("  )                                 End    	")
                .Append("  Else                      FlatAmount                    End ")
                .Append("  FROM saRateSchedules RS, saRateScheduleDetails RSD                WHERE                     RS.RateScheduleId = RSD.RateScheduleId        ")
                .Append("  AND	RS.RateScheduleId=PF.RateScheduleId                AND   (RSD.TuitionCategoryId = SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null ")
                .Append("  AND SE.TuitionCategoryId is null))      AND  ((RSD.FlatAmount=0.00 AND RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails ")
                .Append("  Where   MinUnits<=(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ ")
                .Append("  WHERE(R.TestId = CS.ClsSectionId And CS.ReqId = RQ.ReqId And R.StuEnrollId = SE.StuEnrollId And CST.ClsSectionId = CS.ClsSectionId)")
                .Append("  AND CST.TermId in (Select Distinct TermID from arResults,arClassSections  where arClassSections.ClsSectionId=arResults.TestID and arResults.StuEnrollid in(")
                .Append("  Select SE1.StuEnrollId")
                .Append(" FROM	arStuEnrollMents SE1, arTerm ")
                .Append("                               ,arPrgVersions PV  ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append(" ,adleads ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms ")
                End If
                .Append("       WHERE  ")
                .Append("     PV.PrgverId=SE1.prgverid and PV.ProgId=arTerm.ProgID ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append("                               and  adleads.LeadId=SE1.LeadID  ")
                End If
                If strWhere.Contains("adLeads") Then
                    .Append("                               and adLeads.AdmissionsRep=SE1.AdmissionsRep  ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" and arprograms.ProgID=PV.ProgID ")
                End If
                .Append(strWhere & strLeadModDate & " )) 	")
                .Append(" )")
                .Append("  and RateScheduleId=RSD.RateScheduleId and")
                .Append("  (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId   is null AND SE.TuitionCategoryId is null)))) or (RSD.FlatAmount=0.00 AND RSD.UnitId=1 ")
                .Append("  and RSD.MinUnits=(Select Max(MinUnits)   from saRateScheduleDetails Where MinUnits<=(SELECT Coalesce(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, ")
                .Append("  arClassSectionTerms CST, arReqs RQ WHERE   R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND")
                .Append("  CST.ClsSectionId=CS.ClsSectionId AND CST.TermId in (Select Distinct TermID from arResults,arClassSections ")
                .Append("  where arClassSections.ClsSectionId=arResults.TestID and arResults.StuEnrollid in(	SELECT  SE1.StuEnrollId	")
                .Append(" FROM	arStuEnrollMents SE1, arTerm ")
                .Append("                               ,arPrgVersions PV  ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append(" ,adleads ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms ")
                End If
                .Append("       WHERE   ")
                .Append("    PV.PrgverId=SE1.prgverid and PV.ProgId=arTerm.ProgID ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append("                               and  adleads.LeadId=SE1.LeadID  ")
                End If
                If strWhere.Contains("adLeads") Then
                    .Append("                               and adLeads.AdmissionsRep=SE1.AdmissionsRep  ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" and arprograms.ProgID=PV.ProgID ")
                End If
                .Append(strWhere & strLeadModDate & " )) 	")
                .Append(" )  and RateScheduleId=RSD.RateScheduleId and ")
                .Append("  (RSD.TuitionCategoryId=SE.TuitionCategoryId ")
                .Append("  OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null))))  OR (RSD.FlatAmount>0.00 and RSD.MinUnits=(Select Max(MinUnits) from ")
                .Append("  saRateScheduleDetails Where MinUnits<=(SELECT Coalesce(SUM(RQ.Credits),0)  FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ ")
                .Append("  WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND")
                .Append("  R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId in ")
                .Append("  (Select Distinct TermID from arResults ,arClassSections where arClassSections.ClsSectionId=arResults.TestID and arResults.StuEnrollid in(")
                .Append("  SELECT  SE1.StuEnrollId	 ")
                .Append(" FROM	arStuEnrollMents SE1, arTerm ")
                .Append("                               ,arPrgVersions PV  ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append(" ,adleads ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms ")
                End If
                .Append("       WHERE ")
                .Append("      PV.PrgverId=SE1.prgverid and PV.ProgId=arTerm.ProgID ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append("                               and  adleads.LeadId=SE1.LeadID  ")
                End If
                If strWhere.Contains("adLeads") Then
                    .Append("                               and adLeads.AdmissionsRep=SE1.AdmissionsRep  ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" and arprograms.ProgID=PV.ProgID ")
                End If
                .Append(strWhere & strLeadModDate & " )) 	")
                .Append("  ) and RateScheduleId=RSD.RateScheduleId and   (RSD.TuitionCategoryId=SE.TuitionCategoryId OR")
                .Append("  (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null))))))    	 else     (Case PF.UnitId        	    when 0 Then     ")
                .Append("  (SELECT Coalesce(SUM(RQ.Credits*SPF.Amount),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ , saPeriodicFees SPF         WHERE                  ")
                .Append("  CS.TermId=SPF.TermId And R.TestId = CS.ClsSectionId And CS.ReqId = RQ.ReqId And R.StuEnrollId = SE.StuEnrollId And CST.ClsSectionId")
                .Append("  =CS.ClsSectionId AND CST.TermId in (Select Distinct TermID from arResults,arClassSections where arClassSections.ClsSectionId=arResults.TestID")
                .Append("  and arResults.StuEnrollid in(	SELECT  SE1.StuEnrollId			")
                .Append(" FROM	arStuEnrollMents SE1, arTerm ")
                .Append("                               ,arPrgVersions PV  ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append(" ,adleads ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms ")
                End If
                .Append("       WHERE  ")
                .Append("  PV.PrgverId=SE1.prgverid and PV.ProgId=arTerm.ProgID ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append("                               and  adleads.LeadId=SE1.LeadID  ")
                End If
                If strWhere.Contains("adLeads") Then
                    .Append("                               and adLeads.AdmissionsRep=SE1.AdmissionsRep  ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" and arprograms.ProgID=PV.ProgID ")
                End If
                .Append(strWhere & strLeadModDate & " )) AND	")
                .Append("   (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) OR")
                .Append("  (SE.TuitionCategoryId=PF.TuitionCategoryId )))        	     when 1 then (SELECT Coalesce(SUM(RQ.Hours*SPF.Amount),0)")
                .Append("  FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ  , saPeriodicFees SPF                      WHERE                             ")
                .Append("  CS.TermId=SPF.TermId And  R.TestId = CS.ClsSectionId And CS.ReqId = RQ.ReqId And R.StuEnrollId = SE.StuEnrollId And CST.ClsSectionId = CS.ClsSectionId")
                .Append("  AND CST.TermId in(Select Distinct TermID from arResults ,arClassSections  where arClassSections.ClsSectionId=arResults.TestID and ")
                .Append("  arResults.StuEnrollid in(	SELECT  SE1.StuEnrollId	")
                .Append(" FROM	arStuEnrollMents SE1, arTerm ")
                .Append("                               ,arPrgVersions PV  ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append(" ,adleads ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms ")
                End If
                .Append("       WHERE   ")
                .Append("    PV.PrgverId=SE1.prgverid and PV.ProgId=arTerm.ProgID ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append("                               and  adleads.LeadId=SE1.LeadID  ")
                End If
                If strWhere.Contains("adLeads") Then
                    .Append("                               and adLeads.AdmissionsRep=SE1.AdmissionsRep  ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" and arprograms.ProgID=PV.ProgID ")
                End If
                .Append(strWhere & strLeadModDate & " )) AND	")
                .Append("      (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is   NULL) OR (SE.TuitionCategoryId=PF.TuitionCategoryId )))")
                .Append("  when 2 then PF.Amount *   (select(case when Exists(select * from arResults where StuEnrollId= SE.StuEnrollId ")
                .Append("  and TestId in (select CS1.ClsSectionId from arClassSections CS1,  arClassSectionterms CST where")
                .Append("  CST.ClsSectionId=CS1.ClsSectionId AND CST.TermId in (Select Distinct TermID from arResults ,")
                .Append("  arClassSections where arClassSections.ClsSectionId=arResults.TestID and arResults.StuEnrollid in(	SELECT  SE1.StuEnrollId				")
                .Append(" FROM	arStuEnrollMents SE1, arTerm ")
                .Append("                               ,arPrgVersions PV  ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append(" ,adleads ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms ")
                End If
                .Append("       WHERE   ")
                .Append("  PV.PrgverId=SE1.prgverid and PV.ProgId=arTerm.ProgID ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append("                               and  adleads.LeadId=SE1.LeadID  ")
                End If
                If strWhere.Contains("adLeads") Then
                    .Append("                               and adLeads.AdmissionsRep=SE1.AdmissionsRep  ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" and arprograms.ProgID=PV.ProgID ")
                End If
                .Append(strWhere & strLeadModDate & " )) AND	")
                .Append("  (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) ")
                .Append("  OR (SE.TuitionCategoryId=PF.TuitionCategoryId )))) then 1 else 0 end))             End)       ")
                .Append("  End) TransAmount,      Convert(smalldatetime, getdate())  As TransDate FROM saPeriodicFees PF,  ")
                .Append("  arTerm , arStuEnrollments SE, syStatusCodes SC, arPrgVersions PV, arProgTypes PT , syCampGrps,syCampuses B,syCmpGrpCmps C,syUsersRolesCampGrps URCG")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append(" ,adleads ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms ")
                End If
                .Append("  WHERE(  PF.TermId=arTerm.TermId")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append("                               and  adleads.LeadId=SE.LeadID  ")
                End If
                If strWhere.Contains("adLeads") Then
                    .Append("    and  adLeads.AdmissionsRep=SE.AdmissionsRep and  adLeads.ExpectedStart=arTerm.StartDate and adLeads.ProgramId=arTerm.ProgID AND adLeads.LeadStatus=SC.StatusCodeId	   AND SC.SysStatusId=6  ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" and arprograms.ProgID=PV.ProgID ")
                End If
                .Append(" and arTerm.StartDate=SE.ExpStartDate ")
                .Append(strWhere & strLeadModDate)
                .Append("  AND	se.stuenrollId in ")
                .Append("  (	SELECT  SE1.StuEnrollId	")
                .Append(" FROM	arStuEnrollMents SE1, arTerm ")
                .Append("                               ,arPrgVersions PV  ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append(" ,adleads ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms ")
                End If
                .Append("       WHERE ")
                .Append("    PV.PrgverId=SE1.prgverid and PV.ProgId=arTerm.ProgID ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append("                               and  adleads.LeadId=SE1.LeadID  ")
                End If
                If strWhere.Contains("adLeads") Then
                    .Append("                               and adLeads.AdmissionsRep=SE1.AdmissionsRep  ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" and arprograms.ProgID=PV.ProgID ")
                End If
                .Append(strWhere & strLeadModDate & " )) AND	")
                .Append("   arTerm.TermID in (Select Distinct TermID from arResults ,arClassSections where arClassSections.ClsSectionId=arResults.TestID and ")
                .Append("  arResults.StuEnrollid=SE.StuEnrollId)")
                .Append("  AND    SE.StatusCodeId=SC.StatusCodeId")
                .Append("  AND    SC.SysStatusId In (7,9,13,20) AND   SE.PrgVerId=PV.PrgVerId AND    PV.ProgTypId=PT.ProgTypId AND    ((PF.ApplyTo=0) OR (PF.ApplyTo=1 AND ")
                .Append("  PV.ProgTypId=PF.ProgTypId) OR   (PF.ApplyTo=2 AND PV.PrgVerId=PF.PrgVerId)) AND    ((PF.TermStartDate is null) or (PF.TermStartDate = SE.ExpStartDate))")
                .Append("  and syCampGrps.CampGrpId = C.CampGrpId AND B.CampusId=C.CampusId			            AND syCampGrps.CampGrpId=URCG.CampGrpId			         ")
                .Append("  AND URCG.RoleId IN (SELECT RoleId FROM syRoles WHERE SysRoleId IN (3,8))			             AND SE.AdmissionsRep=URCG.UserId AND ")
                .Append("  SE.CampusId=B.CampusId " & strCamp)
                .Append("   ) Res group by ADmissionsRep,CampusID,CampGrpID,CampGrpDescrip,CampDescrip,TermID;")

                'END datatable TermProjectedAmount
                'START datatable CourseProjectedAmount

                .Append(" Select ADmissionsRep,CampusID,CampGrpID,CampGrpDescrip,CampDescrip,")
                .Append(" (Select TermDescrip from arterm where termid=Res.termID)as TermDescrip, ")
                .Append(" Sum(TransAmount) TotTransAmount,Count(StuEnrollId) from  ")
                .Append(" (SELECT  SE.AdmissionsRep ,SE.CampusID,  SE.StuEnrollId,       ")
                .Append(" syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,B.CampDescrip, ")
                .Append(" arterm.TermID ,")
                .Append(" (Case LEN(CF.RateScheduleId) 	        when 36 then 		        (SELECT 			        Case RSD.FlatAmount 			     ")
                .Append(" When 0.00 				            Then case RSD.UnitId 						            when 0 then Rate*RQ.Credits 	")
                .Append(" when 1 then Rate*RQ.Hours                                End 			            Else   ")
                .Append(" FlatAmount                    End 		        FROM saRateSchedules RS,")
                .Append(" saRateScheduleDetails RSD                WHERE             ")
                .Append(" RS.RateScheduleId = RSD.RateScheduleId")
                .Append(" AND	    RS.RateScheduleId=CF.RateScheduleId                AND  (RSD.TuitionCategoryId = SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null ")
                .Append(" AND SE.TuitionCategoryId is null))                AND  ((RSD.FlatAmount=0.00 AND RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits)  from ")
                .Append(" saRateScheduleDetails Where MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR ")
                .Append(" (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) or (RSD.FlatAmount=0.00 AND RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) ")
                .Append(" from saRateScheduleDetails Where ")
                .Append(" MinUnits<=RQ.Hours and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null ")
                .Append(" AND SE.TuitionCategoryId is null)))) OR (RSD.FlatAmount>0.00 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where ")
                .Append(" MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null")
                .Append(" AND SE.TuitionCategoryId is null)))))) 	        else 	            (Case CF.UnitId 			        when 0 Then CF.Amount*RQ.Credits ")
                .Append(" when 1 then CF.Amount*RQ.Hours 			        when 2 then CF.Amount 		        End) 	    End) TransAmount     ")
                .Append(", Convert(smalldatetime,getdate()) As TransDate")
                .Append(" FROM   arStuEnrollments SE, arPrgVersions PV, arProgVerDef PVD,")
                .Append(" arReqs RQ, saCourseFees CF")
                .Append(" , syCampGrps,syCampuses B,syCmpGrpCmps C,syUsersRolesCampGrps URCG")
                .Append(" ,arterm ")
                .Append(" , arClassSections CS, arResults R ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append(" ,adleads ")
                End If
                If strWhere.Contains("adLeads") Then
                    .Append(" ,SyStatusCodes SC ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms ")
                End If
                .Append(" WHERE(CF.CourseId = RQ.reqId)")
                .Append(" and CS.ClsSectionId=R.TestId and arterm.TermId=CS.TermId  and R.StuEnrollId=SE.StuEnrollId and RQ.ReqId=CS.ReqId")
                .Append(" and PV.PrgVerId=PVD.PrgVerId")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append("                               and  adleads.LeadId=SE.LeadID  ")
                End If
                If strWhere.Contains("adLeads") Then
                    .Append("    and adLeads.AdmissionsRep=SE.AdmissionsRep and adLeads.ExpectedStart=arTerm.StartDate and adLeads.ProgramId=arTerm.ProgID AND adLeads.LeadStatus=SC.StatusCodeId	   AND SC.SysStatusId=6   ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" and arprograms.ProgID=PV.ProgID ")
                End If
                .Append(" and arTerm.StartDate=SE.ExpStartDate ")
                .Append(strWhere & strLeadModDate)
                .Append(" AND	CF.StartDate =	(select top 1 StartDate from saCourseFees")
                .Append(" where CourseId=CF.CourseId and StartDate <=GetDate() order by StartDate desc)  AND")
                .Append(" (((CF.RateScheduleId Is Null AND SE.TuitionCategoryId Is NULL AND CF.TuitionCategoryId Is NULL) OR")
                .Append(" (CF.RateScheduleId Is Null AND SE.TuitionCategoryId=CF.TuitionCategoryId)) OR CF.RateScheduleId Is Not Null) ")
                .Append(" and SE.StuEnrollId in(	SELECT  SE1.StuEnrollId	")
                .Append(" FROM	arStuEnrollMents SE1, arTerm ")
                .Append("                               ,arPrgVersions PV  ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append(" ,adleads ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms ")
                End If
                .Append("       WHERE  ")
                .Append("     PV.PrgverId=SE1.prgverid and PV.ProgId=arTerm.ProgID ")
                If strWhere.Contains("adLeads") Or strLeadModDate.Contains("adLeads") Then
                    .Append("                               and  adleads.LeadId=SE1.LeadID  ")
                End If
                If strWhere.Contains("adLeads") Then
                    .Append("                               and adLeads.AdmissionsRep=SE1.AdmissionsRep  ")
                End If
                If strWhere.Contains("arPrograms") Then
                    .Append(" and arprograms.ProgID=PV.ProgID ")
                End If
                .Append(strWhere & strLeadModDate & " ) AND	")
                .Append("  SE.prgVerId=PV.PrgverID and (PVD.REqID=RQ.ReqID ")
                .Append(" or RQ.ReqId in (Select arReqgrpDef.ReqId from arReqgrpDef where GrpId=PVD.ReqID))")
                .Append(" and")
                .Append(" syCampGrps.CampGrpId = C.CampGrpId")
                .Append(" AND B.CampusId=C.CampusId			            AND syCampGrps.CampGrpId=URCG.CampGrpId			          ")
                .Append(" AND URCG.RoleId IN (SELECT RoleId FROM syRoles WHERE SysRoleId IN (3,8))			            ")
                .Append(" AND SE.AdmissionsRep=URCG.UserId AND SE.CampusId=B.CampusId  " & strCamp)
                .Append(" ) Res group by ADmissionsRep,CampusID,CampGrpID,CampGrpDescrip,CampDescrip,TermID;")

                'END datatable CourseProjectedAmount

            End With
        Else

            ''For Date Range
            With sb
                'StartDateRepPerformance table
                .Append("SELECT ")
                .Append("       CampGrpId,")
                .Append("       CampGrpDescrip,")
                .Append("       CampusId,")
                .Append("       CampDescrip,")
                .Append("       UserId AS AdmissionsRep,")
                .Append("       (SELECT FullName FROM syUsers WHERE UserId=Y.UserId) AS FullName,")

                .Append(" '' AS StartDate,")
                .Append(" '' AS EndDate,")
                .Append(" '' AS TermDescrip,")

                '.Append("       (SELECT StartDate FROM arTerm WHERE TermId=Y.TermId) AS StartDate,")
                '.Append("       (SELECT EndDate FROM arTerm WHERE TermId=Y.TermId) AS EndDate,")
                '.Append("       (SELECT TermDescrip FROM arTerm WHERE TermId=Y.TermId) AS TermDescrip,")
                .Append("       EnrolledCount AS Enrolled,")
                .Append("       CancelledCount AS Cancelled,")
                .Append("       Starts=(CASE WHEN (EnrolledCount-CancelledCount>0)")
                .Append("				        THEN (EnrolledCount-CancelledCount)")
                .Append("				        ELSE (0)")
                .Append("				        END),")
                .Append("       StartRate=(CASE WHEN (EnrolledCount<>0 AND (EnrolledCount-CancelledCount)>0)")
                '.Append("				        THEN (((EnrolledCount-CancelledCount)/EnrolledCount)*100)")
                ''Commented and modified by Saraswathi lakshmanan on oct 7 2009
                ''Integer division in Sql gives either intger or Zero
                .Append("				        THEN CONVERT(DECIMAL(6,2),(((EnrolledCount-CancelledCount)*1.0/EnrolledCount)*100))	")

                .Append("				        ELSE (0)")
                .Append("				        END)")
                .Append("FROM   (")
                .Append("       SELECT")
                .Append("	        CampGrpId,")
                .Append("	        CampGrpDescrip,")
                .Append("	        CampusId,")
                .Append("	        CampDescrip,")
                .Append("	       '' TermId,")
                .Append("	        UserId,")
                .Append("	        SUM(Enrollments) AS EnrolledCount,")
                .Append("	        SUM(Cancels) AS CancelledCount")
                .Append("       FROM	(")
                .Append("               SELECT DISTINCT")
                .Append("			                syCampGrps.CampGrpId,")
                .Append("			                syCampGrps.CampGrpDescrip,")
                .Append("			                B.CampusId,")
                .Append("			                B.CampDescrip,")
                .Append("			                URCG.UserId,")
                .Append("			                '' TermId,")
                .Append("			                W.Enrollments,")
                .Append("			                0 AS Cancels")
                .Append("		        FROM   syCampGrps,syCampuses B,syCmpGrpCmps C,syUsersRolesCampGrps URCG,")
                .Append("				        (	SELECT  AdmissionsRep,CampusId,'' TermId,COUNT(*) AS Enrollments")
                .Append("					        FROM	adLeads ,syStatusCodes SC,arTerm")
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms ")
                End If

                ''Modified by Saraswathi lakshmanan on oct 07 2009
                ''The expected Start Date should be equal to the term Start Date of the Student
                '.Append("                           WHERE   L.ExpectedStart>=arTerm.StartDate And L.ExpectedStart<arTerm.EndDate")
                '   .Append("                           WHERE   adLeads.ExpectedStart=arTerm.StartDate ")

                .Append(" Where " + paramInfo.FilterOther)
                '.Append("                           WHERE   adLeads.ExpectedStart between  ")
                ''Modified by Saraswathi lakshmanan on oct 07 2009
                ''To fix mantis issue 14513: ENH: Global: Need an Admissions conversion report or Admissions Rep performance report 
                ''The enrolled Lead Should be compared against the Program, term and startdates.
                '.Append("                                   and adLeads.ProgramId=arTerm.ProgID  ")
                If strWhere.Contains("arPrograms") Then
                    .Append("                               and arprograms.ProgId=adLeads.ProgramID  ")
                End If

                .Append("							        AND adLeads.LeadStatus=SC.StatusCodeId")
                .Append("							        AND SC.SysStatusId=6 " & strWhere)
                .Append("					        GROUP BY adLeads.AdmissionsRep,adLeads.CampusId,TermId) W")
                .Append("               WHERE   syCampGrps.CampGrpId=C.CampGrpId")
                .Append("			            AND B.CampusId=C.CampusId")
                .Append("			            AND syCampGrps.CampGrpId=URCG.CampGrpId")
                .Append("			            AND URCG.RoleId IN (SELECT RoleId FROM syRoles WHERE SysRoleId IN (3,8))")
                .Append("			            AND W.AdmissionsRep=URCG.UserId AND W.CampusId=B.CampusId " & strCamp)
                .Append("               UNION ALL")
                .Append("               SELECT DISTINCT")
                .Append("			                syCampGrps.CampGrpId,")
                .Append("			                syCampGrps.CampGrpDescrip,")
                .Append("			                B.CampusId,")
                .Append("			                B.CampDescrip,")
                .Append("			                URCG.UserId,")
                .Append("			                '' TermId,")
                .Append("			                0 AS Enrollments,")
                .Append("                           W.Cancels")
                .Append("		        FROM   syCampGrps,syCampuses B,syCmpGrpCmps C,syUsersRolesCampGrps URCG,")
                '.Append("				        (	SELECT E.AdmissionsRep,E.CampusId,TermId,COUNT(*) AS Cancels")
                .Append("				        (	SELECT E.AdmissionsRep,E.CampusId,COUNT(*) AS Cancels")
                ' .Append("					        FROM    arStuEnrollments E,syStatusCodes A, arTerm")
                .Append("					        FROM    arStuEnrollments E,syStatusCodes A ")
                ''Modified by Saraswathi lakshmanan on oct 07 2009
                ''To fix mantis issue 14513: ENH: Global: Need an Admissions conversion report or Admissions Rep performance report 
                ''The Student Should be compared against the Program, term and startdates.
                .Append("                               ,arPrgVersions PV  ")
                If strWhere.Contains("adLeads") Then
                    .Append(" ,adleads ")
                End If

                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms ")
                End If

                .Append("                           WHERE   E.StatusCodeId=A.StatusCodeId")
                ''Modified by Saraswathi lakshmanan on oct 07 2009
                ''To fix mantis issue 14513: ENH: Global: Need an Admissions conversion report or Admissions Rep performance report 
                ''The Student Should be compared against the Program, term and startdates.
                '.Append("                                   and PV.PrgverId=E.prgverid and PV.ProgId=arTerm.ProgID ")

                If strWhere.Contains("adLeads") Then
                    .Append("                               and  adleads.LeadId=E.LeadID and adLeads.AdmissionsRep=E.AdmissionsRep  ")
                End If

                If strWhere.Contains("arPrograms") Then
                    .Append("                               and arprograms.ProgID=PV.ProgID  ")
                End If

                .Append("					and  E.PrgVerId=PV.PrgVerID		        AND A.sysStatusID=8 " & strWhere)
                .Append("							        AND (EXISTS (SELECT * FROM adLeads  ")
                .Append("                                               WHERE   E.LeadId=adLeads.LeadId")
                '.Append("												        AND L1.ExpectedStart=arTerm.StartDate")
                .Append(" AND " + paramInfo.FilterOther)
                '.Append("												        AND adLeads.ExpectedStart between ")
                '.Append("												        AND L1.AdmissionsRep=E.AdmissionsRep))")
                .Append("												        AND adLeads.AdmissionsRep=E.AdmissionsRep)")
                ''Modified by Saraswathi lakshmanan Oct 8 2009
                ''Expected Start Date should be equal to the term Start Date
                '.Append("								        OR (E.ExpStartDate>=arTerm.StartDate AND E.ExpStartDate<arTerm.EndDate))")
                '.Append("								        OR (E.ExpStartDate=arTerm.StartDate))")
                .Append("								)")

                '.Append("					        GROUP BY E.AdmissionsRep,E.CampusId,TermId) W")
                .Append("					        GROUP BY E.AdmissionsRep,E.CampusId) W")
                .Append("               WHERE   syCampGrps.CampGrpId=C.CampGrpId")
                .Append("			            AND B.CampusId=C.CampusId")
                .Append("			            AND syCampGrps.CampGrpId=URCG.CampGrpId")
                .Append("			            AND URCG.RoleId IN (SELECT RoleId FROM syRoles WHERE SysRoleId IN (3,8))")
                .Append("			            AND W.AdmissionsRep=URCG.UserId AND W.CampusId=B.CampusId " & strCamp)
                .Append("               ) Q ")
                ' .Append("       GROUP BY CampGrpId,CampGrpDescrip,CampusId,CampDescrip,TermId,UserId")
                .Append("       GROUP BY CampGrpId,CampGrpDescrip,CampusId,CampDescrip,UserId")
                .Append("       ) Y ")
                .Append("WHERE  Y.EnrolledCount>0 OR Y.CancelledCount>0 ")
                .Append("ORDER BY CampGrpDescrip,CampGrpId,CampDescrip,CampusId,StartDate,EndDate,TermDescrip,TermId,FullName,AdmissionsRep;")
                'StartDateRepPerformance table
                ' ''.Append("SELECT ")
                ' ''.Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,adLeads.CampusId,syCampuses.CampDescrip,")
                ' ''.Append("       adLeads.AdmissionsRep,C.FullName,arTerm.StartDate,arTerm.EndDate,arTerm.TermDescrip,")
                ' ''.Append("       (SELECT COUNT(*)")
                ' ''.Append("           FROM adLeads L1,syStatusCodes A,sySysStatus B ")
                ' ''.Append("           WHERE L1.LeadStatus=A.StatusCodeId ")
                ' ''.Append("           AND A.sysStatusID = B.sysStatusID ")
                ' ''.Append("           AND B.SysStatusDescrip='Enrolled'")
                ' ''.Append("           AND arTerm.StartDate<=L1.ExpectedStart AND L1.ExpectedStart<=arTerm.EndDate ")
                ' ''.Append("           AND L1.AdmissionsRep=adLeads.AdmissionsRep ")
                ' ''.Append("           AND L1.CampusId=adLeads.CampusId) AS Enrolled, ")
                ' ''.Append("       (SELECT COUNT(*) ")
                ' ''.Append("           FROM arStuEnrollments E,syStatusCodes A,sySysStatus B ")
                ' ''.Append("           WHERE E.StatusCodeId = A.StatusCodeId ")
                ' ''.Append("           AND A.sysStatusID = B.sysStatusID ")
                ' ''.Append("           AND B.SysStatusDescrip='No Start'")
                ' ''.Append("           AND EXISTS (SELECT * FROM adLeads L1 ")
                ' ''.Append("                           WHERE E.LeadId = L1.LeadId ")
                ' ''.Append("                           AND arTerm.StartDate<=L1.ExpectedStart AND L1.ExpectedStart<=arTerm.EndDate ")
                ' ''.Append("                           AND L1.AdmissionsRep=adLeads.AdmissionsRep)) AS Cancelled ")
                ' ''.Append("FROM       adLeads,syCampGrps,syCmpGrpCmps,syCampuses,syUsers C,arTerm ")
                ' ''.Append("WHERE      syCmpGrpCmps.CampGrpId = syCampGrps.CampGrpId ")
                ' ''.Append("           AND syCmpGrpCmps.CampusId=syCampuses.CampusId ")
                ' ''.Append("           AND syCampuses.CampusId=adLeads.CampusId ")
                ' ''.Append("           AND adLeads.AdmissionsRep = C.UserId ")
                ' ''.Append("           AND arTerm.CampGrpId=syCampGrps.CampGrpId ")
                ' ''.Append(strWhere)
                ' ''.Append("GROUP BY   syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,adLeads.CampusId,syCampuses.CampDescrip,adLeads.AdmissionsRep,C.FullName,arTerm.StartDate,arTerm.EndDate,arTerm.TermDescrip ")
                ' ''.Append("ORDER BY   syCampGrps.CampGrpDescrip,syCampGrps.CampGrpId,syCampuses.CampDescrip,adLeads.CampusId,C.FullName,adLeads.AdmissionsRep,arTerm.StartDate,arTerm.EndDate,arTerm.TermDescrip;")
                'StatusCodes table
                '.Append("SELECT     (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=8 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS NoStart,")
                '.Append("           (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=8 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS NSCode,")
                '.Append("           (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=6 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS Enrolled,")
                '.Append("           (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps WHERE A.SysStatusId=6 AND A.StatusId=B.StatusId AND B.Status='Active'  " & strWhere & " ORDER BY A.StatusCodeDescrip) AS ECode;")
                .Append("SELECT     (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=8 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId  " & strCamp & " ORDER BY A.StatusCodeDescrip) AS NoStart,")
                .Append("           (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=8 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId  " & strCamp & " ORDER BY A.StatusCodeDescrip) AS NSCode,")
                .Append("           (SELECT TOP 1 A.StatusCodeDescrip FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=6 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId  " & strCamp & " ORDER BY A.StatusCodeDescrip) AS Enrolled,")
                .Append("           (SELECT TOP 1 A.StatusCode FROM syStatusCodes A,syStatuses B, syCampGrps, syUsersRolesCampGrps URCG WHERE A.SysStatusId=6 AND A.StatusId=B.StatusId AND B.Status='Active' AND A.CampGrpId=syCampGrps.CampGrpId AND A.CampGrpId=URCG.CampGrpId  " & strCamp & " ORDER BY A.StatusCodeDescrip) AS ECode;")

                ''Query to find the Prgversion Fees for the enrolled Students
                ''Added on oct 29 2009 By Saraswathi lakshmanan
                ''Query taken from Post term Fees-( program Version ) and Modified.
                .Append(" Select ADmissionsRep,CampusID,CampGrpID,CampGrpDescrip,CampDescrip,Sum(TransAmount) TotTransAmount,Count(StuEnrollId) from  ")
                .Append(" (SELECT       SE.StuEnrollId,  SE.AdmissionsRep ,SE.CampusID,         ")
                .Append(" syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,B.CampDescrip, ")
                .Append(" (Case LEN(PVF.RateScheduleId)             when 36 then              ")
                .Append(" (SELECT                Case RSD.FlatAmount                    When 0.00                        Then case RSD.UnitId          ")
                .Append(" when 0 then Rate*PV.Credits                                when 1 then Rate*PV.Hours                      ")
                .Append(" End 				    Else                            FlatAmount                    End             ")
                .Append(" FROM saRateSchedules RS, saRateScheduleDetails RSD                WHERE                      ")
                .Append(" RS.RateScheduleId = RSD.RateScheduleId And RS.RateScheduleId = PVF.RateScheduleId")
                .Append(" AND     (RSD.TuitionCategoryId = SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and ")
                .Append(" SE.TuitionCategoryId is null))                AND     ((RSD.FlatAmount=0.00 AND RSD.UnitId=0 ")
                .Append(" and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=PV.Credits ")
                .Append(" and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR ")
                .Append(" (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) or (RSD.FlatAmount=0.00 AND ")
                .Append(" RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=PV.Hours and RateScheduleId=RSD.RateScheduleId ")
                .Append(" and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) OR (RSD.FlatAmount>0.00 ")
                .Append(" and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=PV.Credits and RateScheduleId=RSD.RateScheduleId and ")
                .Append(" (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null))))))             else  ")
                .Append(" (Case PVF.UnitId                    when 0 Then PVF.Amount*PV.Credits                    when 1 then PVF.Amount*PV.Hours   ")
                .Append(" when 2 then PVF.Amount End)                End )TransAmount")
                .Append(", Convert(smalldatetime,getdate()) As TransDate")
                .Append(" FROM arStuEnrollments SE, arPrgVersions PV,")
                .Append(" saProgramVersionFees PVF ")
                .Append(" , syCampGrps,syCampuses B,syCmpGrpCmps C,syUsersRolesCampGrps URCG")
                .Append("                 WHERE ")
                .Append(" SE.StuEnrollId in (	SELECT  SE1.StuEnrollId					 ")
                .Append(" FROM	adLeads ,syStatusCodes SC,arStuEnrollMents SE1")

                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms,arPrgVersions ")
                End If

                .Append(" Where " & paramInfo.FilterOther & strWhere)
                If strWhere.Contains("arPrograms") Then
                    .Append("    and arprograms.ProgID=arPrgVersions.ProgID 	and  SE1.PrgVerId=arPrgVersions.PrgVerID	  ")
                End If
                .Append(" AND adLeads.LeadStatus=SC.StatusCodeId	and SE1.LeadID=adLeads.LeadID  AND SC.SysStatusId=6 ) AND	")
                .Append(" SE.PrgVerId = PV.PrgVerId And PV.PrgVerId = PVF.PrgVerId")
                .Append(" AND	(((PVF.RateScheduleId Is Null AND SE.TuitionCategoryId Is NULL AND PVF.TuitionCategoryId Is NULL) OR (PVF.RateScheduleId Is Null AND")
                .Append(" SE.TuitionCategoryId=PVF.TuitionCategoryId)) OR PVF.RateScheduleId Is Not Null)")
                .Append(" and")
                .Append(" syCampGrps.CampGrpId = C.CampGrpId")
                .Append(" AND B.CampusId=C.CampusId			            AND syCampGrps.CampGrpId=URCG.CampGrpId			          ")
                .Append(" AND URCG.RoleId IN (SELECT RoleId FROM syRoles WHERE SysRoleId IN (3,8))			            ")
                .Append(" AND SE.AdmissionsRep=URCG.UserId AND SE.CampusId=B.CampusId  " & strCamp)
                .Append("  ) Res  Group by AdmissionsRep,CampusID,CampGrpID,CampGrpDescrip,CampDescrip;")

                ''''Query for term- post Term Fees
                .Append("    Select ADmissionsRep,CampusID,CampGrpID,CampGrpDescrip,CampDescrip,isNUll(Sum(TransAmount),0) TotTransAmount,Count(StuEnrollId) from   (SELECT  ")
                .Append("  SE.StuEnrollId,  SE.AdmissionsRep ,SE.CampusID,  syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,B.CampDescrip, ")
                .Append("  (Case LEN(PF.RateScheduleId)            when 36 then                (SELECT                    Case RSD.FlatAmount  ")
                '.Append("  When 0.00        					Then case RSD.UnitId     when 0 then Rate*(SELECT   Coalesce(SUM(RQ.Credits),0) ")
                '.Append("  FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ WHERE R.TestId = CS.ClsSectionId  AND CS.ReqId=RQ.ReqId AND ")
                '' Commented and new code added By Vijay Ramteke On Dec 21, 2009 Mantis Id 18214
                '.Append("  When 0.00        					Then case RSD.UnitId     when 0 then Rate*(SELECT   Coalesce(SUM(RQ.Credits),0) ")
                '.Append("  FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ WHERE R.TestId = CS.ClsSectionId  AND CS.ReqId=RQ.ReqId AND ")
                .Append("  When 0.00        					Then case RSD.UnitId     when 0 then (SELECT   Coalesce(SUM(RQ.Credits* SRSD.Rate),0) ")
                .Append("  FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ, saRateSchedules SRS, saRateScheduleDetails SRSD, saPeriodicFees SPF WHERE R.TestId = CS.ClsSectionId   AND SRS.RateScheduleId=SRSD.RateScheduleId and SRS.RateScheduleId=SPF.RateScheduleId AND CS.ReqId=RQ.ReqId AND ")
                '' Commented and new code added By Vijay Ramteke On Dec 21, 2009 Mantis Id 18214
                .Append("  R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId and CST.TermId in ")
                .Append("  (Select Distinct TermID from arResults ,arClassSections where arClassSections.ClsSectionId=arResults.TestID and arResults.StuEnrollid in(")
                .Append("  Select arStuEnrollMents.StuEnrollId")
                .Append("  FROM	adLeads ,syStatusCodes ,arStuEnrollMents  ")
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms,arPrgVersions ")
                End If

                .Append(" Where " & paramInfo.FilterOther & strWhere)
                If strWhere.Contains("arPrograms") Then
                    .Append("    and arprograms.ProgID=arPrgVersions.ProgID 	and  arStuEnrollMents.PrgVerId=arPrgVersions.PrgVerID	  ")
                End If

                .Append("  AND adLeads.LeadStatus=syStatusCodes.StatusCodeId	and arStuEnrollMents.LeadID=adLeads.LeadID  AND syStatusCodes.SysStatusId=6 )) )                         ")
                '.Append("  when 1 then Rate*(SELECT Coalesce(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, ")
                '.Append("  arReqs RQ WHERE  R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND ")
                '' Commented and new code added By Vijay Ramteke On Dec 21, 2009 Mantis Id 18214
                '.Append("  when 1 then Rate*(SELECT Coalesce(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, ")
                '.Append("  arReqs RQ WHERE  R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND ")
                .Append("  when 1 then (SELECT Coalesce(SUM(RQ.Hours* SRSD.Rate),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, ")
                .Append("  arReqs RQ , saRateSchedules SRS, saRateScheduleDetails SRSD, saPeriodicFees SPF WHERE  R.TestId = CS.ClsSectionId AND SRS.RateScheduleId=SRSD.RateScheduleId AND SRS.RateScheduleId=SPF.RateScheduleId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND ")
                '' Commented and new code added By Vijay Ramteke On Dec 21, 2009 Mantis Id 18214
                .Append("  CST.ClsSectionId=CS.ClsSectionId and CST.TermId in (Select Distinct TermID from arResults ,arClassSections  where")
                .Append("  arClassSections.ClsSectionId=arResults.TestID and arResults.StuEnrollid in")
                .Append("  (	SELECT  arStuEnrollMents.StuEnrollId				  FROM	adLeads")
                .Append("  ,syStatusCodes,arStuEnrollMents ")
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms,arPrgVersions ")
                End If

                .Append(" Where " & paramInfo.FilterOther & strWhere)
                If strWhere.Contains("arPrograms") Then
                    .Append("    and arprograms.ProgID=arPrgVersions.ProgID 	and  arStuEnrollMents.PrgVerId=arPrgVersions.PrgVerID	  ")
                End If

                .Append("  AND adLeads.LeadStatus=syStatusCodes.StatusCodeId	and arStuEnrollMents.LeadID=adLeads.LeadID  AND syStatusCodes.SysStatusId=6 )) )                                 End    	")
                .Append("  Else                      FlatAmount                    End ")
                .Append("  FROM saRateSchedules RS, saRateScheduleDetails RSD                WHERE                     RS.RateScheduleId = RSD.RateScheduleId        ")
                .Append("  AND	RS.RateScheduleId=PF.RateScheduleId                AND   (RSD.TuitionCategoryId = SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null ")
                .Append("  AND SE.TuitionCategoryId is null))      AND  ((RSD.FlatAmount=0.00 AND RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails ")
                .Append("  Where   MinUnits<=(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ ")
                .Append("  WHERE(R.TestId = CS.ClsSectionId And CS.ReqId = RQ.ReqId And R.StuEnrollId = SE.StuEnrollId And CST.ClsSectionId = CS.ClsSectionId)")
                .Append("  AND CST.TermId in (Select Distinct TermID from arResults,arClassSections  where arClassSections.ClsSectionId=arResults.TestID and arResults.StuEnrollid in(")
                .Append("  Select arStuEnrollMents.StuEnrollId")
                .Append("  FROM	adLeads ,syStatusCodes,arStuEnrollMents ")
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms,arPrgVersions ")
                End If

                .Append(" Where " & paramInfo.FilterOther & strWhere)
                If strWhere.Contains("arPrograms") Then
                    .Append("    and arprograms.ProgID=arPrgVersions.ProgID 	and  arStuEnrollMents.PrgVerId=arPrgVersions.PrgVerID	  ")
                End If

                .Append("   AND adLeads.LeadStatus=syStatusCodes.StatusCodeId	")
                .Append("  and arStuEnrollMents.LeadID=adLeads.LeadID  AND syStatusCodes.SysStatusId=6 )) )")
                .Append("  and RateScheduleId=RSD.RateScheduleId and")
                .Append("  (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId   is null AND SE.TuitionCategoryId is null)))) or (RSD.FlatAmount=0.00 AND RSD.UnitId=1 ")
                .Append("  and RSD.MinUnits=(Select Max(MinUnits)   from saRateScheduleDetails Where MinUnits<=(SELECT Coalesce(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, ")
                .Append("  arClassSectionTerms CST, arReqs RQ WHERE   R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND")
                .Append("  CST.ClsSectionId=CS.ClsSectionId AND CST.TermId in (Select Distinct TermID from arResults,arClassSections ")
                .Append("  where arClassSections.ClsSectionId=arResults.TestID and arResults.StuEnrollid in(	SELECT  arStuEnrollMents.StuEnrollId	")
                .Append("   				   FROM	adLeads ,syStatusCodes ,")
                .Append("  arStuEnrollMents ")
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms,arPrgVersions ")
                End If

                .Append(" Where " & paramInfo.FilterOther & strWhere)
                If strWhere.Contains("arPrograms") Then
                    .Append("    and arprograms.ProgID=arPrgVersions.ProgID 	and  arStuEnrollMents.PrgVerId=arPrgVersions.PrgVerID	  ")
                End If

                .Append("  AND adLeads.LeadStatus=syStatusCodes.StatusCodeId")
                .Append("  and arStuEnrollMents.LeadID=adLeads.LeadID  AND syStatusCodes.SysStatusId=6 )) )  and RateScheduleId=RSD.RateScheduleId and ")
                .Append("  (RSD.TuitionCategoryId=SE.TuitionCategoryId ")
                .Append("  OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null))))  OR (RSD.FlatAmount>0.00 and RSD.MinUnits=(Select Max(MinUnits) from ")
                .Append("  saRateScheduleDetails Where MinUnits<=(SELECT Coalesce(SUM(RQ.Credits),0)  FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ ")
                .Append("  WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND")
                .Append("  R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId in ")
                .Append("  (Select Distinct TermID from arResults ,arClassSections where arClassSections.ClsSectionId=arResults.TestID and arResults.StuEnrollid in(")
                .Append("  SELECT  arStuEnrollMents.StuEnrollId	   FROM	adLeads ,")
                .Append("  syStatusCodes ,arStuEnrollMents ")
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms,arPrgVersions ")
                End If

                .Append(" Where " & paramInfo.FilterOther & strWhere)
                If strWhere.Contains("arPrograms") Then
                    .Append("    and arprograms.ProgID=arPrgVersions.ProgID 	and  arStuEnrollMents.PrgVerId=arPrgVersions.PrgVerID	  ")
                End If
                .Append("   AND adLeads.LeadStatus=syStatusCodes.StatusCodeId")
                .Append("  and arStuEnrollMents.LeadID=adLeads.LeadID  AND syStatusCodes.SysStatusId=6 )) ) and RateScheduleId=RSD.RateScheduleId and   (RSD.TuitionCategoryId=SE.TuitionCategoryId OR")
                '.Append("  (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null))))))    	 else     (Case PF.UnitId        	    when 0 Then PF.Amount*    ")
                '.Append("  (SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ          WHERE                  ")
                '.Append("  R.TestId = CS.ClsSectionId And CS.ReqId = RQ.ReqId And R.StuEnrollId = SE.StuEnrollId And CST.ClsSectionId")
                '' Commented and new code added By Vijay Ramteke On Dec 21, 2009 Mantis Id 18214
                '.Append("  (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null))))))    	 else     (Case PF.UnitId        	    when 0 Then PF.Amount*    ")
                '.Append("  (SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ          WHERE                  ")
                '.Append("  R.TestId = CS.ClsSectionId And CS.ReqId = RQ.ReqId And R.StuEnrollId = SE.StuEnrollId And CST.ClsSectionId")
                .Append("  (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null))))))    	 else     (Case PF.UnitId        	    when 0 Then     ")
                .Append("  (SELECT Coalesce(SUM(RQ.Credits*SPF.Amount),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ , saPeriodicFees SPF         WHERE                  ")
                .Append("  CS.TermId=SPF.TermId And R.TestId = CS.ClsSectionId And CS.ReqId = RQ.ReqId And R.StuEnrollId = SE.StuEnrollId And CST.ClsSectionId")
                '' Commented and new code added By Vijay Ramteke On Dec 21, 2009 Mantis Id 18214
                .Append("  =CS.ClsSectionId AND CST.TermId in (Select Distinct TermID from arResults,arClassSections where arClassSections.ClsSectionId=arResults.TestID")
                .Append("  and arResults.StuEnrollid in(	SELECT  arStuEnrollMents.StuEnrollId					   FROM	adLeads ,syStatusCodes ,arStuEnrollMents ")
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms,arPrgVersions ")
                End If

                .Append(" Where " & paramInfo.FilterOther & strWhere)
                If strWhere.Contains("arPrograms") Then
                    .Append("    and arprograms.ProgID=arPrgVersions.ProgID 	and  arStuEnrollMents.PrgVerId=arPrgVersions.PrgVerID	  ")
                End If

                .Append("   AND adLeads.LeadStatus=syStatusCodes.StatusCodeId	and")
                .Append("  arStuEnrollMents.LeadID = adLeads.LeadID")
                .Append("  AND syStatusCodes.SysStatusId=6 ))                  		AND       (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) OR")
                '.Append("  (SE.TuitionCategoryId=PF.TuitionCategoryId )))        	     when 1 then PF.Amount*            		    (SELECT Coalesce(SUM(RQ.Hours),0)")
                '.Append("  FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ                        WHERE                             ")
                '.Append("  R.TestId = CS.ClsSectionId And CS.ReqId = RQ.ReqId And R.StuEnrollId = SE.StuEnrollId And CST.ClsSectionId = CS.ClsSectionId")
                '' Commented and new code added By Vijay Ramteke On Dec 21, 2009 Mantis Id 18214
                '.Append("  (SE.TuitionCategoryId=PF.TuitionCategoryId )))        	     when 1 then PF.Amount*            		    (SELECT Coalesce(SUM(RQ.Hours),0)")
                '.Append("  FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ                        WHERE                             ")
                '.Append("  R.TestId = CS.ClsSectionId And CS.ReqId = RQ.ReqId And R.StuEnrollId = SE.StuEnrollId And CST.ClsSectionId = CS.ClsSectionId")
                .Append("  (SE.TuitionCategoryId=PF.TuitionCategoryId )))        	     when 1 then            		    (SELECT Coalesce(SUM(RQ.Hours*SPF.Amount),0)")
                .Append("  FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ , saPeriodicFees SPF                     WHERE                             ")
                .Append("  CS.TermId=SPF.TermId And R.TestId = CS.ClsSectionId And CS.ReqId = RQ.ReqId And R.StuEnrollId = SE.StuEnrollId And CST.ClsSectionId = CS.ClsSectionId")
                '' Commented and new code added By Vijay Ramteke On Dec 21, 2009 Mantis Id 18214
                .Append("  AND CST.TermId in(Select Distinct TermID from arResults ,arClassSections  where arClassSections.ClsSectionId=arResults.TestID and ")
                .Append("  arResults.StuEnrollid in(	SELECT  arStuEnrollMents.StuEnrollId	")
                .Append("  FROM	adLeads ,syStatusCodes ,arStuEnrollMents ")
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms,arPrgVersions ")
                End If

                .Append(" Where " & paramInfo.FilterOther & strWhere)
                If strWhere.Contains("arPrograms") Then
                    .Append("    and arprograms.ProgID=arPrgVersions.ProgID 	and  arStuEnrollMents.PrgVerId=arPrgVersions.PrgVerID	  ")
                End If

                .Append("   AND adLeads.LeadStatus=syStatusCodes.StatusCodeId	and arStuEnrollMents.LeadID=adLeads.LeadID")
                .Append("  AND syStatusCodes.SysStatusId=6 ))   ")
                .Append("  AND     (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is   NULL) OR (SE.TuitionCategoryId=PF.TuitionCategoryId )))")
                .Append("  when 2 then PF.Amount *   (select(case when Exists(select * from arResults where StuEnrollId= SE.StuEnrollId ")
                .Append("  and TestId in (select CS1.ClsSectionId from arClassSections CS1,  arClassSectionterms CST where")
                .Append("  CST.ClsSectionId=CS1.ClsSectionId AND CST.TermId in (Select Distinct TermID from arResults ,")
                .Append("  arClassSections where arClassSections.ClsSectionId=arResults.TestID and arResults.StuEnrollid in(	SELECT  arStuEnrollMents.StuEnrollId				")
                .Append("  FROM	adLeads ,syStatusCodes,arStuEnrollMents  ")
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms,arPrgVersions ")
                End If

                .Append(" Where " & paramInfo.FilterOther & strWhere)
                If strWhere.Contains("arPrograms") Then
                    .Append("    and arprograms.ProgID=arPrgVersions.ProgID 	and  arStuEnrollMents.PrgVerId=arPrgVersions.PrgVerID	  ")
                End If

                .Append("   AND adLeads.LeadStatus=syStatusCodes.StatusCodeId	and arStuEnrollMents.LeadID=adLeads.LeadID ")
                .Append("  AND syStatusCodes.SysStatusId=6 )) AND (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) ")
                .Append("  OR (SE.TuitionCategoryId=PF.TuitionCategoryId )))) then 1 else 0 end))             End)       ")
                .Append("  End) TransAmount,      Convert(smalldatetime, getdate())  As TransDate FROM saPeriodicFees PF,  ")
                .Append("  arTerm T, arStuEnrollments SE, syStatusCodes SC, arPrgVersions PV, arProgTypes PT , syCampGrps,syCampuses B,syCmpGrpCmps C,syUsersRolesCampGrps URCG")
                .Append("  WHERE(  PF.TermId=T.TermId AND	se.stuenrollId in ")
                .Append("  (	SELECT  arStuEnrollMents.StuEnrollId					   FROM	adLeads ,syStatusCodes ,arStuEnrollMents ")
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms,arPrgVersions ")
                End If

                .Append(" Where " & paramInfo.FilterOther & strWhere)
                If strWhere.Contains("arPrograms") Then
                    .Append("    and arprograms.ProgID=arPrgVersions.ProgID 	and  arStuEnrollMents.PrgVerId=arPrgVersions.PrgVerID	  ")
                End If

                .Append("   AND adLeads.LeadStatus=syStatusCodes.StatusCodeId	and arStuEnrollMents.LeadID=adLeads.LeadID  AND syStatusCodes.SysStatusId=6 )) ")
                .Append("  and T.TermID in (Select Distinct TermID from arResults ,arClassSections where arClassSections.ClsSectionId=arResults.TestID and ")
                .Append("  arResults.StuEnrollid=SE.StuEnrollId)")
                .Append("  AND    SE.StatusCodeId=SC.StatusCodeId")
                .Append("  AND    SC.SysStatusId In (7,9,13,20) AND   SE.PrgVerId=PV.PrgVerId AND    PV.ProgTypId=PT.ProgTypId AND    ((PF.ApplyTo=0) OR (PF.ApplyTo=1 AND ")
                .Append("  PV.ProgTypId=PF.ProgTypId) OR   (PF.ApplyTo=2 AND PV.PrgVerId=PF.PrgVerId)) AND    ((PF.TermStartDate is null) or (PF.TermStartDate = SE.ExpStartDate))")
                .Append("  and syCampGrps.CampGrpId = C.CampGrpId AND B.CampusId=C.CampusId			            AND syCampGrps.CampGrpId=URCG.CampGrpId			         ")
                .Append("  AND URCG.RoleId IN (SELECT RoleId FROM syRoles WHERE SysRoleId IN (3,8))			             AND SE.AdmissionsRep=URCG.UserId AND ")
                .Append("  SE.CampusId=B.CampusId " & strCamp)
                .Append("   ) Res group by ADmissionsRep,CampusID,CampGrpID,CampGrpDescrip,CampDescrip;")

                ''Query for Course Fees

                .Append(" Select ADmissionsRep,CampusID,CampGrpID,CampGrpDescrip,CampDescrip,Sum(TransAmount) TotTransAmount,Count(StuEnrollId) from  ")
                .Append(" (SELECT DISTINCT  SE.AdmissionsRep ,SE.CampusID,  SE.StuEnrollId,       ")
                .Append(" syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,B.CampDescrip, ")
                .Append(" (Case LEN(CF.RateScheduleId) 	        when 36 then 		        (SELECT 			        Case RSD.FlatAmount 			     ")
                .Append(" When 0.00 				            Then case RSD.UnitId 						            when 0 then Rate*RQ.Credits 	")
                .Append(" when 1 then Rate*RQ.Hours                                End 			            Else   ")
                .Append(" FlatAmount                    End 		        FROM saRateSchedules RS,")
                .Append(" saRateScheduleDetails RSD                WHERE             ")
                .Append(" RS.RateScheduleId = RSD.RateScheduleId")
                .Append(" AND	    RS.RateScheduleId=CF.RateScheduleId                AND  (RSD.TuitionCategoryId = SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null ")
                .Append(" AND SE.TuitionCategoryId is null))                AND  ((RSD.FlatAmount=0.00 AND RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits)  from ")
                .Append(" saRateScheduleDetails Where MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR ")
                .Append(" (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) or (RSD.FlatAmount=0.00 AND RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) ")
                .Append(" from saRateScheduleDetails Where ")
                .Append(" MinUnits<=RQ.Hours and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null ")
                .Append(" AND SE.TuitionCategoryId is null)))) OR (RSD.FlatAmount>0.00 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where ")
                .Append(" MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null")
                .Append(" AND SE.TuitionCategoryId is null)))))) 	        else 	            (Case CF.UnitId 			        when 0 Then CF.Amount*RQ.Credits ")
                .Append(" when 1 then CF.Amount*RQ.Hours 			        when 2 then CF.Amount 		        End) 	    End) TransAmount     ")
                .Append(", Convert(smalldatetime,getdate()) As TransDate")
                .Append(" FROM   arStuEnrollments SE, arProgVerDef PV,")
                .Append(" arReqs RQ, saCourseFees CF")
                .Append(" , syCampGrps,syCampuses B,syCmpGrpCmps C,syUsersRolesCampGrps URCG")

                '' Code added by kamalesh Ahuja on 31 Dec 09 to fix mantis Issue Id 18214 Part 2
                .Append(" ,arterm  , arClassSections CS, arResults R  ")
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                .Append(" WHERE(CF.CourseId = RQ.reqId)")

                '' Code added by kamalesh Ahuja on 31 Dec 09 to fix mantis Issue Id 18214 Part 2
                .Append(" and CS.ClsSectionId=R.TestId and arterm.TermId=CS.TermId  and R.StuEnrollId=SE.StuEnrollId and RQ.ReqId=CS.ReqId")
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                .Append(" AND	CF.StartDate =	(select top 1 StartDate from saCourseFees")
                .Append(" where CourseId=CF.CourseId and StartDate <=GetDate() order by StartDate desc)  AND")
                .Append(" (((CF.RateScheduleId Is Null AND SE.TuitionCategoryId Is NULL AND CF.TuitionCategoryId Is NULL) OR")
                .Append(" (CF.RateScheduleId Is Null AND SE.TuitionCategoryId=CF.TuitionCategoryId)) OR CF.RateScheduleId Is Not Null) ")
                .Append(" and SE.StuEnrollId in(	SELECT  SE1.StuEnrollId					 ")
                .Append(" FROM	adLeads ,syStatusCodes SC,arStuEnrollMents SE1 ")
                If strWhere.Contains("arPrograms") Then
                    .Append(" ,arPrograms,arPrgVersions ")
                End If

                .Append(" Where " & paramInfo.FilterOther & strWhere)
                If strWhere.Contains("arPrograms") Then
                    .Append("    and arprograms.ProgID=arPrgVersions.ProgID 	and  SE1.PrgVerId=arPrgVersions.PrgVerID	  ")
                End If

                .Append(" AND adLeads.LeadStatus=SC.StatusCodeId	and SE1.LeadID=adLeads.LeadID  AND SC.SysStatusId=6 )")
                .Append(" and SE.prgVerId=PV.PrgverID and (PV.REqID=RQ.ReqID ")

                '' Code Commented and added by kamalesh Ahuja on 31 Dec 09 to fix mantis Issue Id 18214 Part 2

                '.Append(" or RQ.ReqId in (Select arReqgrpDef.ReqId from arReqgrpDef where GrpId=RQ.ReqID))")
                .Append(" or RQ.ReqId in (Select arReqgrpDef.ReqId from arReqgrpDef where GrpId=PV.ReqID))")

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                .Append(" and")
                .Append(" syCampGrps.CampGrpId = C.CampGrpId")
                .Append(" AND URCG.RoleId IN (SELECT RoleId FROM syRoles WHERE SysRoleId IN (3,8))			            ")
                .Append(" AND SE.AdmissionsRep=URCG.UserId AND SE.CampusId=B.CampusId  " & strCamp)
                .Append(" ) Res group by ADmissionsRep,CampusID,CampGrpID,CampGrpDescrip,CampDescrip;")


            End With
        End If




        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count = 5 Then
            Dim dt As DataTable = ds.Tables(0)
            dt.TableName = "StartDateRepPerformance"
            ds.Tables(1).TableName = "StatusCodeDescrip"
            ds.Tables(2).TableName = "ProgProjectedAmount"
            ds.Tables(3).TableName = "TermProjectedAmount"
            ds.Tables(4).TableName = "CourseProjectedAmount"


            'Dim dc As DataColumn = dt.Columns.Add("Starts", Type.GetType("System.Int32"))
            'dc.AllowDBNull = True
            'dc.Expression = "Enrolled - Cancelled"

            'dt.Columns.Add("StartRate", Type.GetType("System.String"))
            dt.Columns.Add("CampGrpIdStr", Type.GetType("System.String"))
            dt.Columns.Add("CampusIdStr", Type.GetType("System.String"))
            dt.Columns.Add("AdRepCount", Type.GetType("System.Int32"))
            dt.Columns.Add("ProjAmount", Type.GetType("System.Decimal"))


            dt = New DataTable("CampusGroupTotals")
            dt.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Enrolled", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("Cancelled", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("Starts", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("StartRate", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ProjAmount", System.Type.GetType("System.Decimal")))
            ds.Tables.Add(dt)

            dt = New DataTable("CampusTotals")
            dt.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusIdStr", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Enrolled", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("Cancelled", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("Starts", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("StartRate", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ProjAmount", System.Type.GetType("System.Decimal")))
            ds.Tables.Add(dt)

        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

#End Region

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

End Class
