Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' StatusesDB.vb
'
' StatusesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class StatusesDB
    Public Function GetAllStatuses() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   ST.StatusId, ST.Status ")
            .Append("FROM     syStatuses ST ")
            .Append("ORDER BY ST.Status ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllSysDocStatuses() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   ST.SysDocStatusId, ST.DocStatusDescrip ")
            .Append("FROM     sySysDocStatuses ST ")
            .Append("ORDER BY ST.DocStatusDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllPrograms() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("select JC.ProgId,JC.ProgDescrip ")
            .Append("FROM    arPrograms JC,syStatuses ST ")
            .Append("WHERE   JC.StatusId = ST.StatusId ")
            .Append(" AND    ST.Status = 'Active' ")
            .Append(" Order By JC.ProgDescrip")
        End With
        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    'Public Function GetAllPrgVerDescrip() As DataSet

    '    '   connect to the database
    '    Dim db As New DataAccess
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder

    '    '   build the sql query
    '    With sb
    '        .Append("select JC.PrgVerId,JC.PrgVerDescrip ")
    '        .Append("FROM    arPrgVersions JC,syStatuses ST ")
    '        .Append("WHERE   JC.StatusId = ST.StatusId ")
    '        .Append(" AND    ST.Status = 'Active' ")
    '        .Append(" Order By JC.PrgVerDescrip")
    '    End With
    '    '   return dataset
    '    Return db.RunSQLDataSet(sb.ToString)
    'End Function
    Public Function GetAllPrgVerDescrip(Optional ByVal campusId As String = "") As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("select JC.PrgVerId,JC.PrgVerDescrip, ")
            .Append(" case when (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=JC.ProgId) is null  then ")
            .Append(" JC.PrgVerDescrip ")
            .Append(" else ")
            .Append(" JC.PrgVerDescrip + ' (' + (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=JC.ProgId) + ')'  ")
            .Append(" end as PrgVerShiftDescrip  ")
            .Append("FROM    arPrgVersions JC,syStatuses ST ")
            .Append("WHERE   JC.StatusId = ST.StatusId ")
            .Append(" AND    ST.Status = 'Active' ")

            If campusId <> "" Then
                .Append("AND (JC.CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId = ? ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR JC.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")

            End If

            .Append(" Order By JC.PrgVerDescrip")
        End With

        If campusId <> "" Then
            db.AddParameter("cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function GetAllDataTypes() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   DTypeId, DType ")
            .Append("FROM     sySdfDtypes ST ")
            .Append("ORDER BY ST.DTypeId ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
End Class
