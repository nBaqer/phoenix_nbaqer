Imports FAME.AdvantageV1.Common.IPEDSCommon
Imports FAME.Advantage.Common

Public Class SFE_AllFObjectDB

    Public Shared Function GetReportDatasetRaw(ByVal RptParamInfo As ReportParamInfoIPEDS, ByVal MainTableName As String) As DataSet
        ' get lists of students to include for each Part, based on report parameters

        Dim StudentListPart1 As String = ""
        Dim StudentListPart2 As String = ""
        Dim StudentListPart3 As String = ""

        With RptParamInfo
            ' HACK - temp disable of Report Part F, Part 1, pending changes in Advantage 
            '	to distinguish betweeen Clock Hour vs. Credit Hour courses
            'If .FilterProgramIDsMult1 IsNot Nothing Then
            '	StudentListPart1 = IPEDSDB.GetStudentList(RptParamInfo, .FilterProgramIDsMult1)
            'End If
            If .FilterProgramIDsMult2 IsNot Nothing Then
                StudentListPart2 = IPEDSDB.GetStudentList(RptParamInfo, .FilterProgramIDsMult2)
            End If
            If .FilterProgramIDsMult3 IsNot Nothing Then
                StudentListPart3 = IPEDSDB.GetStudentList(RptParamInfo, .FilterProgramIDsMult3)
            End If
        End With

        ' if there are no students to include, return empty dataset
        If StudentListPart1 = "" And StudentListPart2 = "" And StudentListPart3 = "" Then
            Return New DataSet
        End If

        Dim sb As New System.Text.StringBuilder
        Dim SortSQLProgram As String = "" ' avoid compiler warning
        Dim StudentIdentifierSQL As String
        Dim SortSQLStudent As String
        Dim dsRaw As New DataSet

        ' set SQL for primary sort from report param info - either Program Name or Program Code
        Select Case RptParamInfo.SortByProg
            Case IPEDSCommon.SortByProgramProgCode
                SortSQLProgram = "arPrograms.ProgCode"
            Case IPEDSCommon.SortByProgramProgName
                SortSQLProgram = "arPrograms.ProgDescrip"
        End Select

        ' set SQL for Student Identifier
        StudentIdentifierSQL = IPEDSDB.GetSQL_StudentIdentifier

        ' set SQL for secondary sort from report param info - either Student Identifier or Last Name
        SortSQLStudent = IPEDSDB.GetSQL_StudentSort(RptParamInfo)

        With sb
            ' get data for report Part 1, if any
            If StudentListPart1 <> "" Then
                .Append("SELECT DISTINCT arStudent.StudentId, ")

                ' retrieve Student Identifier, as set above
                .Append(StudentIdentifierSQL & ", ")

                ' retrieve all other needed columns
                .Append("arStudent.LastName, arStudent.FirstName, arStudent.MiddleName, ")
                .Append("arPrograms.ProgCode, arPrograms.ProgDescrip, ")
                .Append("arStuEnrollments.EnrollDate AS EnrollmentStartDate, ")
                .Append("arPrgVersions.Hours ")
                .Append("FROM ")
                .Append("arStudent, arStuEnrollments, arPrgVersions, arPrograms ")

                ' establish necessary relationships
                .Append("WHERE ")
                .Append("arStudent.StudentId = arStuEnrollments.StudentId AND ")
                .Append("arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId AND ")
                .Append("arPrgVersions.ProgId = arPrograms.ProgId AND ")

                ' retrieve only records within report date range
                .Append("arStuEnrollments.EnrollDate < ? AND ")

                ' append list of appropriate students to include
                .Append("arStudent.StudentId IN (" & StudentListPart1 & ") AND ")

                ' a student within the above list may have more than one 
                '	enrollment.  Since this report retrieves Program-related 
                '	information, it's possible for the query to retrieve records
                '	for a student relating to programs other than those selected
                '	by the user.  To avoid this, in this case we must filter again 
                '	on the list of programs selected by the user.
                .Append("arPrograms.ProgId IN (" & RptParamInfo.FilterProgramIDsMult1 & ") ")

                ' apply primary and secondary sorts, as set above
                .Append("ORDER BY " & SortSQLProgram & ", " & SortSQLStudent)

                .Append(";")
            End If


            ' get data for report Part 2, if any
            If StudentListPart2 <> "" Then
                .Append("SELECT DISTINCT arStudent.StudentId, ")

                ' retrieve Student Identifier column, as set above
                .Append(StudentIdentifierSQL & ", ")

                ' retrieve all other needed columns
                .Append("arStudent.LastName, arStudent.FirstName, arStudent.MiddleName, ")
                .Append("arPrograms.ProgCode, arPrograms.ProgDescrip, ")
                .Append("arReqs.Credits, ")
                .Append("arStuEnrollments.EnrollDate AS EnrollmentStartDate, ")
                .Append("arClassSections.StartDate AS ClassSectionStartDate, ")
                .Append("arClassSections.EndDate AS ClassSectionEndDate ")
                .Append("FROM ")
                .Append("arStudent, arStuEnrollments, arPrgVersions, arPrograms, arProgTypes, ")
                .Append("syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies, ")
                .Append("arResults, arClassSections, arReqs ")

                ' establish necessary relationships
                .Append("WHERE ")
                .Append("arStudent.StudentId = arStuEnrollments.StudentId AND ")
                .Append("arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId AND ")
                .Append("arPrgVersions.ProgId = arPrograms.ProgId AND ")
                .Append("arPrgVersions.ProgTypId = arProgTypes.ProgTypId AND ")
                .Append("arProgTypes.ProgTypId = syRptAgencySchoolMapping.SchoolDescripId AND ")
                .Append("syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
                .Append("syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
                .Append("syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
                .Append("arStuEnrollments.StuEnrollId = arResults.StuEnrollId AND ")
                .Append("arResults.TestId = arClassSections.ClsSectionId AND ")
                .Append("arClassSections.ReqId = arReqs.ReqId AND ")

                ' retrieve only programs of types mapped to IPEDS "Undergraduate"
                .Append("syRptAgencyFldValues.AgencyDescrip LIKE '" & ProgTypeUndergraduate & "' AND ")
                .Append("syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")

                ' retrieve only records within report date range
                .Append("arStuEnrollments.StartDate < ? AND ")
                .Append("(")
                .Append(" (arClassSections.StartDate >= ? AND ")
                .Append("  arClassSections.StartDate <= ?) ")
                .Append(" OR ")
                .Append(" (arClassSections.EndDate >= ? AND ")
                .Append("  arClassSections.EndDate <= ?) ")
                .Append(") AND ")

                ' append list of appropriate students to include
                .Append("arStudent.StudentId IN (" & StudentListPart2 & ") AND ")

                ' a student within the above list may have more than one 
                '	enrollment.  Since this report retrieves Program-related 
                '	information, it's possible for the query to retrieve records
                '	for a student relating to programs other than those selected
                '	by the user.  To avoid this, in this case we must filter again 
                '	on the list of programs selected by the user.
                .Append("arPrograms.ProgId IN (" & RptParamInfo.FilterProgramIDsMult2 & ") ")

                ' apply primary and secondary sorts, as set above
                .Append("ORDER BY " & SortSQLProgram & ", " & SortSQLStudent)

                .Append(";")
            End If


            ' get data for report Part 3, if any
            If StudentListPart3 <> "" Then
                .Append("SELECT DISTINCT arStudent.StudentId, ")

                ' retrieve Student Identifier column, as set above
                .Append(StudentIdentifierSQL & ", ")

                ' retrieve all other needed columns
                .Append("arStudent.LastName, arStudent.FirstName, arStudent.MiddleName, ")
                .Append("arPrograms.ProgCode, arPrograms.ProgDescrip, ")
                .Append("arReqs.Credits, ")
                .Append("arStuEnrollments.EnrollDate AS EnrollmentStartDate, ")
                .Append("arClassSections.StartDate AS ClassSectionStartDate, ")
                .Append("arClassSections.EndDate AS ClassSectionEndDate ")
                .Append("FROM ")
                .Append("arStudent, arStuEnrollments, arPrgVersions, arPrograms, arProgTypes, ")
                .Append("syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies, ")
                .Append("arResults, arClassSections, arReqs ")

                ' establish necessary relationships
                .Append("WHERE ")
                .Append("arStudent.StudentId = arStuEnrollments.StudentId AND ")
                .Append("arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId AND ")
                .Append("arPrgVersions.ProgId = arPrograms.ProgId AND ")
                .Append("arPrgVersions.ProgTypId = arProgTypes.ProgTypId AND ")
                .Append("arProgTypes.ProgTypId = syRptAgencySchoolMapping.SchoolDescripId AND ")
                .Append("syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
                .Append("syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
                .Append("syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
                .Append("arStuEnrollments.StuEnrollId = arResults.StuEnrollId AND ")
                .Append("arResults.TestId = arClassSections.ClsSectionId AND ")
                .Append("arClassSections.ReqId = arReqs.ReqId AND ")

                ' retrieve only programs of types mapped to IPEDS "Graduate"
                .Append("syRptAgencyFldValues.AgencyDescrip LIKE '" & ProgTypeGraduate & "' AND ")
                .Append("syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")

                ' retrieve only records within report date range
                .Append("arStuEnrollments.StartDate < ? AND ")
                .Append("(")
                .Append(" (arClassSections.StartDate >= ? AND ")
                .Append("  arClassSections.StartDate <= ?) ")
                .Append(" OR ")
                .Append(" (arClassSections.EndDate >= ? AND ")
                .Append("  arClassSections.EndDate <= ?) ")
                .Append(") AND ")

                ' append list of appropriate students to include
                .Append("arStudent.StudentId IN (" & StudentListPart3 & ") AND ")

                ' a student within the above list may have more than one 
                '	enrollment.  Since this report retrieves Program-related 
                '	information, it's possible for the query to retrieve records
                '	for a student relating to programs other than those selected
                '	by the user.  To avoid this, in this case we must filter again 
                '	on the list of programs selected by the user.
                .Append("arPrograms.ProgId IN (" & RptParamInfo.FilterProgramIDsMult3 & ") ")

                ' apply primary and secondary sorts, as set above
                .Append("ORDER BY " & SortSQLProgram & ", " & SortSQLStudent)
            End If
        End With

        ' run query, add returned data to raw dataset
        With IPEDSDB.DataAccessIPEDS()
            ' add parameters for Part 1, if have data
            If StudentListPart1 <> "" Then
                .AddParameter("@CohortEndDateP1", RptParamInfo.CohortEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            ' add parameters for Part 2, if have data
            If StudentListPart2 <> "" Then
                .AddParameter("@CohortEndDateP2", RptParamInfo.CohortEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                .AddParameter("@CohortStartDateP2_1", RptParamInfo.CohortStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                .AddParameter("@CohortEndDateP2_1", RptParamInfo.CohortEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                .AddParameter("@CohortStartDateP2_2", RptParamInfo.CohortStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                .AddParameter("@CohortEndDateP2_2", RptParamInfo.CohortEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            ' add parameters for Part 3, if have data
            If StudentListPart3 <> "" Then
                .AddParameter("@CohortEndDateP3", RptParamInfo.CohortEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                .AddParameter("@CohortStartDateP3_1", RptParamInfo.CohortStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                .AddParameter("@CohortEndDateP3_1", RptParamInfo.CohortEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                .AddParameter("@CohortStartDateP3_2", RptParamInfo.CohortStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                .AddParameter("@CohortEndDateP3_2", RptParamInfo.CohortEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            dsRaw = .RunParamSQLDataSet(sb.ToString)
        End With

        ' set table names for each part of returned data
        With dsRaw
            Dim TableNamePart1 As String = MainTableName & "Part1"
            Dim TableNamePart2 As String = MainTableName & "Part2"
            Dim TableNamePart3 As String = MainTableName & "Part3"

            Select Case .Tables.Count
                Case 3
                    .Tables(0).TableName = TableNamePart1
                    .Tables(1).TableName = TableNamePart2
                    .Tables(2).TableName = TableNamePart3
                Case 2
                    If StudentListPart1 <> "" And _
                       StudentListPart2 <> "" Then
                        .Tables(0).TableName = TableNamePart1
                        .Tables(1).TableName = TableNamePart2
                    ElseIf StudentListPart1 <> "" And _
                        StudentListPart3 <> "" Then
                        .Tables(0).TableName = TableNamePart1
                        .Tables(1).TableName = TableNamePart3
                    ElseIf StudentListPart2 <> "" And _
                        StudentListPart3 <> "" Then
                        .Tables(0).TableName = TableNamePart2
                        .Tables(1).TableName = TableNamePart3
                    End If
                Case 1
                    If StudentListPart1 <> "" Then
                        .Tables(0).TableName = TableNamePart1
                    ElseIf StudentListPart2 <> "" Then
                        .Tables(0).TableName = TableNamePart2
                    ElseIf StudentListPart3 <> "" Then
                        .Tables(0).TableName = TableNamePart3
                    End If
            End Select
        End With

        'return the raw dataset
        Return dsRaw

    End Function

    Public Shared Function GetProgramCodeById(ByVal ProgId As String) As String
        Dim strSQL As String = "SELECT ProgCode FROM arPrograms WHERE ProgId = " & PutDBQuotes(ProgId)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With New DataAccess
            .ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            .OpenConnection()
            Return .RunSQLDataSet(strSQL).Tables(0).Rows(0)("ProgCode").ToString
        End With
    End Function

End Class