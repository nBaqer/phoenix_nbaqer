Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' PrefixesDB.vb
'
' PrefixesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class PrefixesDB
    Public Function GetAllPrefixes() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   PR.PrefixId, PR.StatusId, PR.PrefixDescrip ")
            .Append("FROM     syPrefixes PR, syStatuses ST ")
            .Append("WHERE    PR.StatusId = ST.StatusId ")
            '.Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY PR.PrefixDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllCountries() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   PR.CountryId, PR.StatusId, PR.CountryDescrip ")
            .Append("FROM     adCountries PR, syStatuses ST ")
            .Append("WHERE    PR.StatusId = ST.StatusId ")
            '.Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY PR.CountryDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllAddressTypes() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   AT.AddressTypeId, AT.AddressDescrip ")
            .Append("FROM     plAddressTypes AT,SyStatuses ST  ")
            .Append(" where AT.StatusId = St.StatusId ")
            .Append("ORDER BY AT.AddressDescrip ")
        End With
        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function

End Class
