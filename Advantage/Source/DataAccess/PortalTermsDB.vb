Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' TermsDB.vb
'
' TermsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class PortalTermsDB
    Public Function GetAllTermsByUser(ByVal showActiveOnly As Boolean, Optional ByVal campusId As String = "", Optional ByVal username As String = "", Optional ByVal userid As String = "") As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   T.TermId, ")
            .Append("         T.StatusId, ")
            .Append("         T.TermCode, ")
            .Append("         T.TermDescrip, ")
            .Append("         T.StartDate, ")
            .Append("         T.EndDate, ")
            .Append("         T.ShiftId, ")
            .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saProgramVersionFees PVF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=PVF.TransCodeId and PVF.PrgVerId=SE.PrgVerId and T1.TermId=T.TermId and Voided=0 and IsAutomatic=1) ")
            .Append("         + ")
            .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saCourseFees CF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=CF.TransCodeId and T1.TermId=T.TermId and Voided=0 and IsAutomatic=1) ")
            .Append("         + ")
            .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saPeriodicFees PF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=PF.TransCodeId and PF.TermId=T1.TermId and  T1.TermId=T.TermId  and Voided=0 and IsAutomatic=1) As NumberOfTransactionsPosted ")
            .Append("FROM     arTerm T, syStatuses ST ")
            .Append("WHERE    T.StatusId = ST.StatusId ")
            If showActiveOnly Then
                .Append(" AND     ST.Status = 'Active' ")
            End If
            If campusId <> "" Then
                .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")

                .Append("FROM syCmpGrpCmps ")

                .Append("WHERE CampusId = '")
                .Append(campusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
            If Not username = "sa" And Not userid = "" Then
                .Append(" and T.CampGrpId in ")
                .Append(" ( ")
                .Append("SELECT DISTINCT A.CampGrpId ")
                .Append("FROM syUsersRolesCampGrps A, syCampGrps B ")
                .Append("WHERE A.CampGrpId = B.CampGrpId ")
                .Append("AND A.UserId=? ")
                .Append(")")
            End If

            .Append("ORDER BY T.StartDate,T.TermDescrip")
        End With
        If Not username = "sa" And Not userid = "" Then
            db.AddParameter("@UserId", userid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllTerms(ByVal showActiveOnly As Boolean, Optional ByVal campusId As String = "") As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   T.TermId, ")
            .Append("         T.StatusId, ")
            .Append("         T.TermCode, ")
            .Append("         T.TermDescrip, ")
            .Append("         T.StartDate, ")
            .Append("         T.EndDate, ")
            .Append("         T.ShiftId, ")
            .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saProgramVersionFees PVF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=PVF.TransCodeId and PVF.PrgVerId=SE.PrgVerId and T1.TermId=T.TermId and Voided=0 and IsAutomatic=1) ")
            .Append("         + ")
            .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saCourseFees CF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=CF.TransCodeId and T1.TermId=T.TermId and Voided=0 and IsAutomatic=1) ")
            .Append("         + ")
            .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saPeriodicFees PF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=PF.TransCodeId and PF.TermId=T1.TermId and  T1.TermId=T.TermId  and Voided=0 and IsAutomatic=1) As NumberOfTransactionsPosted ")
            .Append("FROM     arTerm T, syStatuses ST ")
            .Append("WHERE    T.StatusId = ST.StatusId ")
            If showActiveOnly Then
                .Append(" AND     ST.Status = 'Active' ")
            End If
            If campusId <> "" Then
                .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")

                .Append("FROM syCmpGrpCmps ")

                .Append("WHERE CampusId = '")
                .Append(campusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If










            .Append("ORDER BY T.StartDate,T.TermDescrip")
        End With




        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllTerms_SP(ByVal showActiveOnly As Boolean, Optional ByVal campusId As String = "") As DataTable

        Dim db As New DataAccess

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@showActiveOnly", showActiveOnly, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet("dbo.usp_GetAllTerms", Nothing, "SP").Tables(0)




    End Function

    Public Function GetAllTerms(ByVal feeType As AdvantageCommonValues.TuitionFeeTypes, ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   T.TermId, ")
            .Append("         T.StatusId, ")
            .Append("         T.TermCode, ")
            .Append("         T.TermDescrip, ")
            .Append("         T.StartDate, ")
            .Append("         T.EndDate, ")
            .Append("         T.ShiftId, ")
            Select Case feeType
                Case AdvantageCommonValues.TuitionFeeTypes.Program
                    .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saProgramVersionFees PVF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=PVF.TransCodeId and PVF.PrgVerId=SE.PrgVerId and T1.TermId=T.TermId and Voided=0 and IsAutomatic=1 and T1.FeeLevelId=2 and T1.CampusId = '" + campusId + "' ) As NumberOfTransactionsPosted ")
                Case AdvantageCommonValues.TuitionFeeTypes.Course
                    .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saCourseFees CF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=CF.TransCodeId and T1.TermId=T.TermId and Voided=0 and IsAutomatic=1 and T1.FeeLevelId=3 and T1.CampusId = '" + campusId + "' ) As NumberOfTransactionsPosted ")
                Case AdvantageCommonValues.TuitionFeeTypes.Term
                    .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saPeriodicFees PF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=PF.TransCodeId and T1.TermId=T.TermId and Voided=0 and IsAutomatic=1 and T1.FeeLevelId=1 and T1.CampusId = '" + campusId + "' ) As NumberOfTransactionsPosted ")
            End Select
            .Append("FROM     arTerm T, syStatuses ST ")
            .Append("WHERE    T.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            If campusId <> "" Then
                .Append("AND T.TermId in ")
                .Append("(Select distinct CST.TermId from arStuEnrollments SE, arResults R, arClassSectionTerms CST,syStatusCodes SC  where SE.CampusId='" + campusId + "' and R.TestId=CST.ClsSectionId ")
                ''Query Modified by Saraswathi
                ''To bring the Currently Attending and Futurestart STudents only
                ' .Append(" and SE.StatusCodeId in(Select StatusCodeId from syStatusCodes where StatusCodeDescrip like 'Futur%Start%' or StatusCodeDescrip like 'Curr%Att%' or StatusCodeDescrip like 'Acad%Prob%' ))")
                'modified by Theresa G on Oct 1 2010
                .Append("AND    SE.StatusCodeId=SC.StatusCodeId ")
                .Append("AND    SC.SysStatusId In (7,9,13,20)) ")

                .Append("    AND (T.CampGrpId IN(SELECT CampGrpId  ")
                .Append("    FROM syCmpGrpCmps  ")
                .Append("    WHERE CampusId = '" + campusId + "' ")
                .Append("    AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL'))  ")
                .Append("    OR T.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL'))  ")
            End If
            .Append("ORDER BY T.StartDate,T.TermDescrip")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllCohorts(ByVal feeType As AdvantageCommonValues.TuitionFeeTypes, ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            ''Cohort Start Date Formatted
            .Append(" select  Convert(varchar(10),R.CohortStartDate,101)as CohortStartDate , ")
            Select Case feeType
                Case AdvantageCommonValues.TuitionFeeTypes.Program
                    ''Modified to fix issue De1145- The term was expecting the classsection to have the same cohort start date, so removing the validation
                    '.Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saProgramVersionFees PVF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=PVF.TransCodeId and PVF.PrgVerId=SE.PrgVerId and T1.TermId in (select TermId from arClassSections where cohortstartdate = R.CohortStartdate) and Voided=0 and IsAutomatic=1 and T1.FeeLevelId=2 and T1.CampusId = '" + campusId + "' ) As NumberOfTransactionsPosted ")
                    .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saProgramVersionFees PVF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=PVF.TransCodeId and PVF.PrgVerId=SE.PrgVerId and SE.CohortStartDate=R.CohortStartDate and Voided=0 and IsAutomatic=1 and T1.FeeLevelId=2 and T1.CampusId = '" + campusId + "' ) As NumberOfTransactionsPosted ")
                Case AdvantageCommonValues.TuitionFeeTypes.Course
                    '.Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saCourseFees CF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=CF.TransCodeId and T1.TermId in (select TermId from arClassSections where cohortstartdate = R.CohortStartdate) and Voided=0 and IsAutomatic=1 and T1.FeeLevelId=3 and T1.CampusId = '" + campusId + "' ) As NumberOfTransactionsPosted ")
                    .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saCourseFees CF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=CF.TransCodeId and SE.cohortstartdate = R.CohortStartdate  and Voided=0 and IsAutomatic=1 and T1.FeeLevelId=3 and T1.CampusId = '" + campusId + "' ) As NumberOfTransactionsPosted ")

                Case AdvantageCommonValues.TuitionFeeTypes.Term
                    ' .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saPeriodicFees PF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=PF.TransCodeId and T1.TermId in (select TermId from arClassSections where cohortstartdate = R.CohortStartdate) and Voided=0 and IsAutomatic=1 and T1.FeeLevelId=1 and PF.TermId=T1.TermId and PF.TermStartDate=R.CohortStartDAte) As NumberOfTransactionsPosted ")
                    ''Query modified by saraswathi lakshmanan to fix mantis 15281
                    '.Append("         (select count(distinct(TransactionId)) from saTransactions T1 where T1.StuEnrollId in (Select StuEnrollId from arStuEnrollments where CohortStartDate=R.CohortStartDate) and T1.TransCodeId in(Select TransCodeId from saPeriodicFees where TermStartDate= R.CohortStartdate) and  SE.cohortstartdate = R.CohortStartdate and Voided=0 and IsAutomatic=1 and T1.FeeLevelId=1  and T1.CampusId = '" + campusId + "' ) As NumberOfTransactionsPosted ")
                    ''Modifeid to fix isssue DE6223
                    ''Modified To fix de 1145  Fetching the transcode id is requiring the Term Start date to be cohort start date.
                    .Append("         (select count(distinct(TransactionId)) from saTransactions T1 where T1.StuEnrollId in (Select StuEnrollId from arStuEnrollments where CohortStartDate=R.CohortStartDate) and T1.TransCodeId in(Select TransCodeId from saPeriodicFees  WHERE TermId IN (SELECT arClassSections.TermId FROM arClassSections,arTerm WHERE arClassSections.TermId =arTerm.TermId AND  cohortstartdate =R.CohortStartDate)  )  and Voided=0 and IsAutomatic=1 and T1.FeeLevelId=1  and T1.CampusId = '" + campusId + "' ) As NumberOfTransactionsPosted ")

            End Select
            .Append("  ,year(R.CohortStartDate) ")
            .Append(" from (Select distinct SE.CohortStartdate as CohortStartDate ")
            .Append(" from arStuEnrollments SE, ")
            .Append(" arResults R, arClassSectionTerms CST,syStatusCodes SC where ")
            .Append(" R.TestId=CST.ClsSectionId and SE.CohortStartdate is not null ")
            ''Query Modified by Saraswathi
            ''To bring the Currently Attending and Futurestart STudents only
            '.Append(" and SE.StatusCodeId in(Select StatusCodeId from syStatusCodes where StatusCodeDescrip like 'Futur%Start%' or StatusCodeDescrip like 'Curr%Att%' or StatusCodeDescrip like 'Acad%Prob%' )")
            'modified by Theresa G on Oct 1 2010
            .Append("AND    SE.StatusCodeId=SC.StatusCodeId ")
            .Append("AND    SC.SysStatusId In (7,9,13,20) ")

            If campusId <> "" Then
                .Append(" and SE.CampusId='" + campusId + "' ")
            End If
            .Append(") R ")
            .Append(" ORDER BY year(R.CohortStartdate) ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetNoneFutureActiveTerms(Optional ByVal campusId As String = "") As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   T.TermId, ")
            .Append("         T.StatusId, ")
            .Append("         T.TermCode, ")
            .Append("         T.TermDescrip, ")
            .Append("         T.StartDate, ")
            .Append("         T.EndDate, ")
            .Append("         T.ShiftId ")
            .Append("FROM     arTerm T, syStatuses ST ")
            .Append("WHERE    T.StatusId = ST.StatusId ")
            .Append("AND T.StartDate <= GetDate() ")
            .Append(" AND     ST.Status = 'Active' ")
            If campusId <> "" Then
                .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")

                .Append("FROM syCmpGrpCmps ")

                .Append("WHERE CampusId = '")
                .Append(campusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
            .Append("ORDER BY T.StartDate,T.TermDescrip")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetTermInfo(ByVal TermId As String) As TermInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT T.TermId, ")
            .Append("    T.TermCode, ")
            .Append("    T.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=T.StatusId) As Status, ")
            .Append("    T.TermDescrip, ")
            .Append("    T.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=T.CampGrpId) As CampGrpdescrip, ")
            .Append("    T.StartDate, ")
            .Append("    T.EndDate, ")
            .Append("    T.ShiftId, ")
            .Append("    (Select ShiftDescrip from arShifts where ShiftId=T.ShiftId) As ShiftDescrip ")
            .Append("FROM  arTerm T ")
            .Append("WHERE T.TermId= ? ")
        End With

        ' Add the TermId to the parameter list
        db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim TermInfo As New TermInfo

        While dr.Read()

            '   set properties with data from DataReader
            With TermInfo
                .TermId = TermId
                .IsInDB = True
                .Code = dr("TermCode")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Description = dr("TermDescrip")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpdescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                If Not (dr("StartDate") Is System.DBNull.Value) Then .StartDate = CType(dr("StartDate"), Date)
                If Not (dr("EndDate") Is System.DBNull.Value) Then .EndDate = CType(dr("Enddate"), Date)
                If Not (dr("ShiftId") Is System.DBNull.Value) Then .ShiftId = CType(dr("ShiftId"), Guid).ToString
                If Not (dr("Shiftdescrip") Is System.DBNull.Value) Then .ShiftDescrip = dr("ShiftDescrip")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return TermInfo

    End Function

    Public Function GetTermEndDate_SP(ByVal TermId As String) As Date

        '   connect to the database
        Dim termEndDate As Date
        Dim db As New FAME.DataAccessLayer.SQLDataAccess

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConnectionString")

        '   build the sql query

        db.AddParameter("@TermId", New Guid(TermId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   Execute the query
        termEndDate = CDate(db.RunParamSQLScalar_SP("dbo.usp_GetTermEndDate"))
        'Close Connection


        '   Return BankInfo


        Try
            Return termEndDate
        Catch ex As Exception
        Finally
            db.CloseConnection()
        End Try



    End Function
    Public Function UpdateTermInfo(ByVal TermInfo As TermInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE arTerm Set TermId = ?, TermCode = ?, ")
                .Append(" StatusId = ?, TermDescrip = ?, CampGrpId = ?, ")
                .Append(" StartDate = ?, EndDate = ?, ShiftId = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE TermId = ? ")
            End With

            '   add parameters values to the query

            '   TermId
            db.AddParameter("@TermId", TermInfo.TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TermCode
            db.AddParameter("@TermCode", TermInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", TermInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TermDescrip
            db.AddParameter("@TermDescrip", TermInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If TermInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", TermInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   StartDate
            db.AddParameter("@StartDate", TermInfo.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   EndDate
            db.AddParameter("@EndDate", TermInfo.EndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ShiftId
            If TermInfo.ShiftId = Guid.Empty.ToString Then
                db.AddParameter("@ShiftId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ShiftId", TermInfo.ShiftId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TermId
            db.AddParameter("@TermId", TermInfo.TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return 0

        Catch ex As OleDbException
            '   return an error to the client
            Return -1
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddTermInfo(ByVal TermInfo As TermInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT arTerm (TermId, TermCode, StatusId, ")
                .Append("   TermDescrip, CampGrpId, StartDate, EndDate, ShiftId, ")
                .Append("   ModUser, ModDate, IsModule) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   TermId
            db.AddParameter("@TermId", TermInfo.TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TermCode
            db.AddParameter("@TermCode", TermInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", TermInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TermDescrip
            db.AddParameter("@TermDescrip", TermInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If TermInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", TermInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   StartDate
            db.AddParameter("@StartDate", TermInfo.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   EndDate
            db.AddParameter("@EndDate", TermInfo.EndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ShiftId
            If TermInfo.ShiftId = Guid.Empty.ToString Then
                db.AddParameter("@ShiftId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ShiftId", TermInfo.ShiftId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   IsModule
            If TermInfo.IsModule = True Then
                db.AddParameter("@ismodule", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Else
                db.AddParameter("@ismodule", 0, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End If



            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return 0

        Catch ex As OleDbException
            '   return an error to the client
            Return -1
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function


    Public Function DeleteTermInfo(ByVal TermId As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM arTerm ")
                .Append("WHERE TermId = ? ")
            End With

            '   add parameters values to the query

            '   BankId
            db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function IsThisTermBeingUsed(ByVal termId As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        With sb
            .Append("Select ")
            .Append("    	(select Count(*) from arClasssections where TermId=?) + ")
            .Append("   	(select Count(*) from arTransferGrades where TermId= ? ) + ")
            .Append("       (select Count(*) from saPeriodicFees where TermId= ? ) ")
        End With

        'add TermId to the parameters list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        'return 
        Return CType(db.RunParamSQLScalar(sb.ToString), Boolean)

    End Function
    Public Function ClsSectionForTerm(ByVal TermId As String, ByVal CampGrpId As String, Optional ByVal NewCampGrpId As String = "") As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim strCampusId As String = ""
        Dim strNewCampusId As String = ""

        strCampusId = GetCampusByCampusGroup(CampGrpId)
        strNewCampusId = GetCampusByCampusGroup(NewCampGrpId)
        With sb
            .Append("   select Distinct t1.ClsSection,t3.Descrip as Course,t2.CampDescrip as Campus ")
            .Append("   from arClassSections t1,syCampuses t2,arReqs t3 ")
            .Append("   where t1.CampusId = t2.CampusId And ")
            .Append("   t1.ReqId=t3.ReqId and ")
            .Append("   t1.TermId=? and t1.CampusId in ('")
            .Append(strCampusId)
            .Append(") ")
            .Append(" and t1.CampusId not in ('")
            .Append(strNewCampusId)
            .Append(") ")
        End With

        'add TermId to the parameters list
        db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return 
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    ''' <summary>
    ''' Function added by SAraswathi Lakshmanan on May 13 2010
    ''' ';'To fix ross DataIssue Mantis 18921
    '''
    ''' </summary>
    ''' <param name="TermId"></param>
    ''' <param name="CampGrpId"></param>
    ''' <param name="NewCampGrpId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' 

    Public Function getMinAndMaxDatesForClassSections(ByVal TermId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        With sb
            .Append("   Select MIN(StartDate) MinDate,Max(EndDate)MaxDate from arclassSections ")
            .Append("   where TermID=? ")

        End With

        'add TermId to the parameters list
        db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return 
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function IsThereAnyClsSectionForTerm(ByVal TermId As String, ByVal CampGrpId As String, Optional ByVal NewCampGrpId As String = "") As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim strCampusId As String = ""
        Dim strNewCampusId As String = ""
        strCampusId = GetCampusByCampusGroup(CampGrpId)
        strNewCampusId = GetCampusByCampusGroup(NewCampGrpId)
        With sb
            .Append("   select Count(*) ")
            .Append("   from arClassSections t1 ")
            .Append("   where ")
            .Append("   t1.TermId=? and t1.CampusId in ('")
            .Append(strCampusId)
            .Append(") ")
            .Append(" and t1.CampusId not in ('")
            .Append(strNewCampusId)
            .Append(") ")
        End With

        'add TermId to the parameters list
        db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'return 
        Return db.RunParamSQLScalar(sb.ToString)
    End Function
    Public Function IsThereAnyFERPACategory(ByVal FERPACategoryID As String, ByVal CampGrpId As String, Optional ByVal NewCampGrpId As String = "") As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim strCampusId As String = ""
        Dim strNewCampusId As String = ""
        strCampusId = GetCampusByCampusGroup(CampGrpId)
        strNewCampusId = GetCampusByCampusGroup(NewCampGrpId)
        With sb
            .Append("   select Count(*) ")
            .Append("   from arFERPAPolicy t1,arStuEnrollments t2  ")
            .Append("   where t1.StudentId=t2.StudentId and t1.FERPACategoryId = ? ")
            .Append("   and t2.CampusId in ('")
            .Append(strCampusId)
            .Append(") ")
            .Append(" and t2.CampusId not in ('")
            .Append(strNewCampusId)
            .Append(") ")
        End With

        'add TermId to the parameters list
        db.AddParameter("@FERPACategoryID", FERPACategoryID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'return 
        Return db.RunParamSQLScalar(sb.ToString)
    End Function

    Public Function IsThereAnyFERPAEntity(ByVal FERPAEntityID As String, ByVal CampGrpId As String, Optional ByVal NewCampGrpId As String = "") As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim strCampusId As String = ""
        Dim strNewCampusId As String = ""
        strCampusId = GetCampusByCampusGroup(CampGrpId)
        strNewCampusId = GetCampusByCampusGroup(NewCampGrpId)
        With sb
            .Append("   select Count(*) ")
            .Append("   from arFERPAPolicy t1,arStuEnrollments t2  ")
            .Append("   where t1.StudentId=t2.StudentId and t1.FERPAEntityID = ? ")
            .Append("   and t2.CampusId in ('")
            .Append(strCampusId)
            .Append(") ")
            .Append(" and t2.CampusId not in ('")
            .Append(strNewCampusId)
            .Append(") ")
        End With

        'add TermId to the parameters list
        db.AddParameter("@FERPACategoryID", FERPAEntityID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'return 
        Return db.RunParamSQLScalar(sb.ToString)
    End Function

    Public Function GetActiveShiftsByCampus(ByVal CampusId As String) As DataSet


        Dim db As New DataAccess

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@campusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet("dbo.USP_GetAllActiveShiftsByCampus", Nothing, "SP")





        'Dim db As New DataAccess
        'Dim sb As New StringBuilder
        'Dim ds As New DataSet
        'Dim ds1 As New DataSet
        'Dim strCampusId As String = ""
        'If Not CampGrpId = "" Then
        '    With sb
        '        .Append(" Select Distinct CampusId from syCmpGrpCmps where CampGrpId=?")
        '    End With
        '    db.AddParameter("@CampGrpId", CampGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '    ds = db.RunParamSQLDataSet(sb.ToString)
        '    If ds.Tables(0).Rows.Count >= 1 Then
        '        For Each row As DataRow In ds.Tables(0).Rows
        '            strCampusId &= row("CampusId").ToString & "','"
        '        Next
        '        strCampusId = Mid(strCampusId, 1, InStrRev(strCampusId, "'") - 2)
        '    End If
        '    db.ClearParameters()
        '    sb.Remove(0, sb.Length)
        '    ds.Dispose()
        'End If
        'Return strCampusId
    End Function



    Private Function GetCampusByCampusGroup(ByVal CampGrpId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim ds1 As New DataSet
        Dim strCampusId As String = ""
        If Not CampGrpId = "" Then
            With sb
                .Append(" Select Distinct CampusId from syCmpGrpCmps where CampGrpId=?")
            End With
            db.AddParameter("@CampGrpId", CampGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet(sb.ToString)
            If ds.Tables(0).Rows.Count >= 1 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    strCampusId &= row("CampusId").ToString & "','"
                Next
                strCampusId = Mid(strCampusId, 1, InStrRev(strCampusId, "'") - 2)
            End If
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            ds.Dispose()
        End If
        Return strCampusId
    End Function
    Public Function GetAllTermIdsWithProgIds(ByVal showActiveOnly As Boolean, Optional ByVal campusId As String = "") As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '.Append("select  distinct ")
            '.Append("		T.TermId, ")
            '.Append("		PV.PrgVerId, ")
            '.Append("       PV.PrgVerDescrip ")
            '.Append("from	arTerm T, arClassSections CS, arReqs R, arProgVerDef PVD, arPrgVersions PV, syStatuses ST  ")
            '.Append("where ")
            '.Append("		T.TermId = CS.TermId ")
            '.Append("and	CS.ReqId=R.ReqId ")
            '.Append("and	R.ReqId=PVD.ReqId ")
            '.Append("and	PVD.PrgVerId=PV.PrgVerId ")
            .Append("select  distinct ")
            .Append("		T.TermId, ")
            .Append("		T.ProgId, ")
            .Append("       (Select ProgDescrip from arPrograms where ProgId=T.ProgId) As ProgDescrip, ")
            .Append(" case when (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=T.ProgId) is null  then ")
            .Append(" (Select ProgDescrip from arPrograms where ProgId=T.ProgId) ")
            .Append(" else ")
            .Append("(Select ProgDescrip from arPrograms where ProgId=T.ProgId) + ' (' + (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=T.ProgId) + ')'  ")
            .Append(" end as ShiftDescrip  ")
            .Append("from	arTerm T, syStatuses ST ")
            .Append("where    T.StatusId = ST.StatusId ")
            If showActiveOnly Then
                .Append(" AND     ST.Status = 'Active' ")
            End If
            If campusId <> "" Then
                .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId = '")
                .Append(campusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR     T.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
            .Append("Order By ProgDescrip")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    '''  Added by Saraswathi Lakshmanan on 25-Sept-2008 to fix issue
    '''  issue 14296: Instructor Supervisors and Education Directors 
    '''  need to be able to post and modify attendance. 
    '''  The term is populated based on the role of the user 
    ''' The Academic advisors can view all the terms and 
    ''' the instructor supervisors can view their respective instructors terms only
    Public Function GetNoneFutureActiveTermsbyUserandRoles(ByVal userId As String, ByVal userName As String, ByVal isAcademicAdvisor As Boolean, Optional ByVal campusId As String = "") As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            If userName <> "sa" And isAcademicAdvisor = False Then
                .Append("SELECT distinct  T.TermId, ")
                .Append("         T.StatusId, ")
                .Append("         T.TermCode, ")
                .Append("         T.TermDescrip, ")
                .Append("         T.StartDate, ")
                .Append("         T.EndDate, ")
                .Append("         T.ShiftId ")
                .Append("FROM     arTerm T, syStatuses ST ,arClassSections t1 ")
                .Append("WHERE    T.StatusId = ST.StatusId ")
                .Append("AND T.StartDate <= GetDate() ")
                .Append(" AND     ST.Status = 'Active' ")
                If campusId <> "" Then
                    .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")

                    .Append("FROM syCmpGrpCmps ")

                    .Append("WHERE CampusId = '")
                    .Append(campusId)
                    .Append("' ")
                    'DE8605
                    '.Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                    ' .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                    .Append(")) ")
                    ''Added this Code so that
                    ''' The Academic advisors can view all the terms and 
                    ''' the instructor supervisors can view their respective instructors terms only
                End If


                .Append(" AND T.TermId=t1.TermId  ")

                .Append(" AND (t1.InstructorId in (Select InstructorId ")
                .Append(" from arInstructorsSupervisors where")
                .Append(" SupervisorId= '")
                .Append(userId)
                .Append("' )")
                .Append(" OR t1.InstructorId='")
                .Append(userId)
                .Append("') ")
                .Append("ORDER BY T.StartDate,T.TermDescrip")

                'db.AddParameter("userId", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'db.AddParameter("userId", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            Else



                .Append("SELECT distinct  T.TermId, ")
                .Append("         T.StatusId, ")
                .Append("         T.TermCode, ")
                .Append("         T.TermDescrip, ")
                .Append("         T.StartDate, ")
                .Append("         T.EndDate, ")
                .Append("         T.ShiftId ")
                .Append("FROM     arTerm T, syStatuses ST ")
                .Append("WHERE    T.StatusId = ST.StatusId ")
                .Append("AND T.StartDate <= GetDate() ")
                .Append(" AND     ST.Status = 'Active' ")
                If campusId <> "" Then
                    .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")

                    .Append("FROM syCmpGrpCmps ")

                    .Append("WHERE CampusId = '")
                    .Append(campusId)
                    .Append("' ")
                    'DE8605
                    '.Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                    ' .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                    .Append("))")
                End If


                .Append("ORDER BY T.StartDate,T.TermDescrip")
            End If

        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    ''Added by Saraswathi to get the CohortStartDAtefor the past and Current Active terms

    '''  Added by Saraswathi Lakshmanan on 25-Sept-2008 to fix issue
    '''  issue 14296: Instructor Supervisors and Education Directors 
    '''  need to be able to post and modify attendance. 
    '''  The term is populated based on the role of the user 
    ''' The Academic advisors can view all the terms and 
    ''' the instructor supervisors can view their respective instructors terms only
    Public Function GetCohortStartDateforNoneFutureActiveTermsbyUserandRoles(ByVal userId As String, ByVal userName As String, ByVal isAcademicAdvisor As Boolean, Optional ByVal campusId As String = "") As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            If userName <> "sa" And isAcademicAdvisor = False Then
                .Append("SELECT distinct  Convert(varchar(10),CS.CohortStartDate,101)as CohortStartDate,year(CS.CohortStartDate) ")
                .Append(" FROM     arTerm T, syStatuses ST ,arClassSections CS ")
                .Append("WHERE    T.StatusId = ST.StatusId ")
                .Append("AND T.StartDate <= GetDate() and CS.CohortStartDate is not null ")
                .Append(" AND     ST.Status = 'Active' ")
                If campusId <> "" Then
                    .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")

                    .Append("FROM syCmpGrpCmps ")

                    .Append("WHERE CampusId = '")
                    .Append(campusId)
                    .Append("' ")
                    .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                    .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")

                    ''Added this Code so that
                    ''' The Academic advisors can view all the terms and 
                    ''' the instructor supervisors can view their respective instructors terms only
                End If


                .Append(" AND T.TermId=CS.TermId  ")

                .Append(" AND (CS.InstructorId in (Select InstructorId ")
                .Append(" from arInstructorsSupervisors where")
                .Append(" SupervisorId= '")
                .Append(userId)
                .Append("' )")
                .Append(" OR CS.InstructorId='")
                .Append(userId)
                .Append("') ")
                .Append("ORDER BY year(CS.CohortStartDate)")

                db.AddParameter("userId", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("userId", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            Else



                .Append("SELECT Distinct Convert(varchar(10),CS.CohortStartDate,101)as CohortStartDate  ")
                .Append("FROM     arTerm T, syStatuses ST,arClassSections CS ")
                .Append("WHERE    T.StatusId = ST.StatusId ")
                .Append("AND T.StartDate <= GetDate() and CS.CohortStartDate is not null ")
                .Append(" AND     ST.Status = 'Active' ")
                .Append(" AND T.TermId=CS.TermId  ")
                If campusId <> "" Then
                    .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")

                    .Append("FROM syCmpGrpCmps ")

                    .Append("WHERE CampusId = '")
                    .Append(campusId)
                    .Append("' ")
                    .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                    .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                End If


                .Append("ORDER BY CohortStartDate")
            End If

        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function

End Class
