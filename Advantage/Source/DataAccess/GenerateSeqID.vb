Imports FAME.Advantage.Common

Public Class GenerateSeqID
    Public Function GetStudentSequence() As Integer
        '**************************************************************************************************
        'Purpose:       This Procedure Determines The Module Based On The ResourceID when page opens
        'Parameters:    ResourceID
        'Returns:       String
        'Notes:         This sub relies on the ResourceId when a request for the page is made.

        '**************************************************************************************************
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim strModuleID As String
        Dim Student_SeqID As Integer
        Dim StudentSeq As Integer
        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            With sb
                .Append(" Select Max(Student_SeqID) as Student_SeqID from syGenerateStudentID ")
            End With
            Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString)

            While dr.Read()
                If Not (dr("Student_SeqID") Is System.DBNull.Value) Then
                    Student_SeqID = dr("Student_SeqID") + 1
                Else
                    Student_SeqID = "1000"
                End If
            End While
            dr.Close()
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            With sb
                .Append("Insert into syGenerateStudentID(Student_SeqID,ModDate) values(?,?) ")
            End With

            'Student Sequence ID
            db.AddParameter("@Student_SeqID", Student_SeqID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            'ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            Dim dr1 As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            While dr.Read()
                StudentSeq = dr("Student_Seq")
            End While
            dr.Close()
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            Return StudentSeq
        Catch ex As System.Exception
        Finally
            db.CloseConnection()
        End Try
    End Function

End Class
