Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' BanksDB.vb
'
' BanksDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class BanksDB
    Public Function GetAllBanks(ByVal showActiveOnly As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   BC.BankId, BC.StatusId, BC.Code, BC.BankDescrip, ")
            .Append("         BC.CampGrpId, BC.Address1, BC.Address2, BC.City, BC.StateId, BC.Zip, ")
            .Append("         BC.Phone, BC.Fax, BC.Email, BC.FirstName, BC.LastName, BC.Title ,ST.StatusId,ST.Status ")
            .Append("FROM     saBankCodes BC, syStatuses ST ")
            .Append("WHERE    BC.StatusId = ST.StatusId ")
            '   Conditionally include only Active Items 
            If showActiveOnly = "True" Then
                .Append("AND    ST.Status = 'Active' ")
                .Append("ORDER BY BC.BankDescrip ")
            ElseIf showActiveOnly = "False" Then
                .Append("AND    ST.Status = 'Inactive' ")
                .Append("ORDER BY BC.BankDescrip ")
            Else
                .Append("ORDER BY ST.Status,BC.BankDescrip asc")
            End If
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetBankInfo(ByVal bankId As String) As BankInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT     B.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=B.StatusId) as Status, ")
            .Append("    B.Code, ")
            .Append("    B.BankDescrip, ")
            .Append("    B.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=B.CampGrpId) As CampGrpDescrip, ")
            .Append("    B.Address1, ")
            .Append("    B.Address2, ")
            .Append("    B.City, ")
            .Append("    B.StateId, ")
            .Append("    (Select StateDescrip from syStates where StateId=B.StateId) As State, ")
            .Append("    B.Zip, ")
            .Append("    B.Phone, ")
            .Append("    B.Fax, ")
            .Append("    B.Email, ")
            .Append("    B.FirstName, ")
            .Append("    B.LastName, ")
            .Append("    B.Title, ")
            .Append("    B.ForeignPhone, ")
            .Append("    B.ForeignFax, ")
            .Append("    B.ModUser, ")
            .Append("    B.ModDate,B.ForeignZip,B.OtherState ")
            .Append("FROM  saBankCodes B ")
            .Append("WHERE B.bankId= ? ")
        End With

        ' Add the bankIdto the parameter list
        db.AddParameter("@BankId", bankId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim bankInfo As New BankInfo

        While dr.Read()

            '   set properties with data from DataReader
            With bankInfo
                .IsInDB = True
                .BankId = bankId
                If Not (dr("Code") Is System.DBNull.Value) Then .Code = dr("Code")
                If Not (dr("StatusId") Is System.DBNull.Value) Then .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Name = dr("BankDescrip")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                .Address1 = dr("Address1")
                .Address2 = dr("Address2")
                .City = dr("City")
                If Not (dr("StateId") Is System.DBNull.Value) Then .StateId = CType(dr("StateId"), Guid).ToString
                If Not (dr("State") Is System.DBNull.Value) Then .State = dr("State")
                .Zip = dr("Zip")
                .Phone = dr("Phone")
                .Fax = dr("Fax")
                .Email = dr("Email")
                .ContactFirstName = dr("FirstName")
                .ContactLastName = dr("LastName")
                .ContactTitle = dr("Title")
                .ForeignPhone = dr("ForeignPhone")
                .ForeignFax = dr("ForeignFax")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
                If Not (dr("ForeignZip") Is System.DBNull.Value) Then .ForeignZip = dr("ForeignZip") Else .ForeignZip = False
                If Not (dr("OtherState") Is System.DBNull.Value) Then .OtherState = CType(dr("OtherState"), String).ToString Else .OtherState = ""
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return bankInfo

    End Function
    Public Function UpdateBankInfo(ByVal bankInfo As BankInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saBankCodes Set Code = ?, StatusId = ?, BankDescrip = ?, ")
                .Append(" CampGrpId = ?, Address1 = ?, Address2 = ?, ")
                .Append(" City = ?, StateId = ?, Zip = ?, Phone = ?, Fax = ?, Email = ?, ")
                .Append(" FirstName = ?, LastName = ?, Title = ?, ")
                .Append(" ForeignPhone = ?, ForeignFax = ?, ")
                .Append(" ForeignZip=?, OtherState=?, ModUser = ?, ModDate =? ")
                .Append("WHERE bankId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saBankCodes where ModDate=?")
            End With

            '   add parameters values to the query

            '   Code
            db.AddParameter("@Code", bankInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            If bankInfo.StatusId = Guid.Empty.ToString Then
                db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StatusId", bankInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   bank name
            db.AddParameter("@BankDescrip", bankInfo.Name, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Campus Group Id
            If bankInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", bankInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Address1
            db.AddParameter("@Address1", bankInfo.Address1, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Address2
            db.AddParameter("@Address2", bankInfo.Address2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   City
            db.AddParameter("@City", bankInfo.City, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StateId
            If bankInfo.StateId = Guid.Empty.ToString Then
                db.AddParameter("@StateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StateId", bankInfo.StateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Zip
            db.AddParameter("@Zip", bankInfo.Zip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Phone
            db.AddParameter("@Phone", bankInfo.Phone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Fax
            db.AddParameter("@Fax", bankInfo.Fax, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Email
            db.AddParameter("@Email", bankInfo.Email, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   FirstName
            db.AddParameter("@FirstName", bankInfo.ContactFirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   LastName
            db.AddParameter("@LastName", bankInfo.ContactLastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Title
            db.AddParameter("@Title", bankInfo.ContactTitle, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Foreign Phone
            db.AddParameter("@ForeignPhone", bankInfo.ForeignPhone, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   Foreign fax
            db.AddParameter("@ForeigFax", bankInfo.ForeignFax, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   Foreign zip
            db.AddParameter("@ForeignZip", bankInfo.ForeignZip, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '  Other State
            If bankInfo.OtherState = "" Then
                db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@OtherState", bankInfo.OtherState, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   BankId
            db.AddParameter("@BankId", bankInfo.BankId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", bankInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)
            'db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddBankInfo(ByVal bankInfo As BankInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saBankCodes (BankId, Code, StatusId, BankDescrip, ")
                .Append("   CampGrpId, Address1, Address2, City, StateId, Zip, Phone, ")
                .Append("   Fax, Email, FirstName, LastName, Title, ForeignPhone, ForeignFax, ForeignZip, OtherState, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   BankId
            db.AddParameter("@BankId", bankInfo.BankId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Code
            db.AddParameter("@Code", bankInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            If bankInfo.StatusId = Guid.Empty.ToString Then
                db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StatusId", bankInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   bank name
            db.AddParameter("@BankDescrip", bankInfo.Name, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Campus Group Id
            If bankInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", bankInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Address1
            db.AddParameter("@Address1", bankInfo.Address1, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Address2
            db.AddParameter("@Address2", bankInfo.Address2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   City
            db.AddParameter("@City", bankInfo.City, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StateId
            If bankInfo.StateId = Guid.Empty.ToString Then
                db.AddParameter("@StateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StateId", bankInfo.StateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Zip
            db.AddParameter("@Zip", bankInfo.Zip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Phone
            db.AddParameter("@Phone", bankInfo.Phone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Fax
            db.AddParameter("@Fax", bankInfo.Fax, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Email
            db.AddParameter("@Email", bankInfo.Email, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   FirstName
            db.AddParameter("@FirstName", bankInfo.ContactFirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   LastName
            db.AddParameter("@LastName", bankInfo.ContactLastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Title
            db.AddParameter("@Title", bankInfo.ContactTitle, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Foreign Phone
            db.AddParameter("@ForeignPhone", bankInfo.ForeignPhone, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   Foreign fax
            db.AddParameter("@ForeigFax", bankInfo.ForeignFax, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   Foreign zip
            db.AddParameter("@ForeignZip", bankInfo.ForeignZip, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '  Other State
            If bankInfo.OtherState = "" Then
                db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@OtherState", bankInfo.OtherState, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteBankInfo(ByVal bankId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saBankCodes ")
                .Append("WHERE bankId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("Select count(*) from saBankCodes where bankId = ? ")
            End With

            '   add parameters values to the query

            '   BankId
            db.AddParameter("@BankId", bankId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   BankId
            db.AddParameter("@BankId", bankId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetBankAccounts(ByVal bankId As String, ByVal showActiveOnly As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   BA.BankAcctId, BA.BankId, BA.BankAcctDescrip, BA.StatusId, BA.BankAcctNumber, ")
            .Append("         BA.BankRoutingNumber, BA.IsDefaultBankAcct, BA.CampGrpId,ST.StatusId,ST.Status ")
            .Append("FROM     saBankAccounts BA, syStatuses ST ")
            .Append("WHERE    BA.StatusId = ST.StatusId ")
            .Append("     AND     BA.BankId = ? ")
            '   Conditionally include only Active Items 
            If showActiveOnly = "True" Then
                .Append("AND    ST.Status = 'Active' ")
                .Append("ORDER BY BA.BankAcctDescrip ")
            ElseIf showActiveOnly = "False" Then
                .Append("AND    ST.Status = 'Inactive' ")
                .Append("ORDER BY BA.BankAcctDescrip ")
            Else
                .Append("ORDER BY ST.Status,BA.BankAcctDescrip asc")
            End If
        End With

        ' Add the bankId to the parameter list
        db.AddParameter("@BankId", bankId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllBankAccounts(ByVal showActiveOnly As Boolean) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   BA.BankAcctId, ")
            .Append("         BA.BankId, ")
            .Append("         BC.BankDescrip + '/' + BA.BankAcctDescrip As BankAcctDescrip, ")
            .Append("         BA.StatusId, ")
            .Append("         BA.BankAcctNumber, ")
            .Append("         BA.BankRoutingNumber, ")
            .Append("         BA.IsDefaultBankAcct, ")
            .Append("         BA.CampGrpId ")
            .Append("FROM     saBankAccounts BA, saBankCodes BC, syStatuses ST ")
            .Append("WHERE    BA.StatusId = ST.StatusId ")
            .Append("AND      BA.BankId=BC.BankId ")
            '   Conditionally include only Active Items 
            If showActiveOnly Then
                .Append(" AND     ST.Status = 'Active' ")
            End If
            .Append("ORDER BY BA.IsDefaultBankAcct desc, BA.BankAcctDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetBankAcctInfo(ByVal bankAcctId As String) As BankAcctInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT BankId, ")
            .Append("    BA.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=BA.StatusId) as Status, ")
            .Append("    BA.BankAcctDescrip, ")
            .Append("    BA.BankAcctNumber, ")
            .Append("    BA.BankRoutingNumber, ")
            .Append("    BA.IsDefaultBankAcct, ")
            .Append("    BA.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=BA.CampGrpId) As CampGrpDescrip, ")
            .Append("    BA.ModUser, ")
            .Append("    BA.ModDate ")
            .Append("FROM  saBankAccounts BA ")
            .Append("WHERE BA.bankAcctId= ? ")
        End With

        ' Add the bankAcctId to the parameter list
        db.AddParameter("@BankAcctId", bankAcctId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim bankAcctInfo As New BankAcctInfo

        While dr.Read()

            '   set properties with data from DataReader
            With bankAcctInfo
                .BankAcctId = bankAcctId
                .IsInDB = True
                .BankId = CType(dr("bankId"), Guid).ToString
                If Not (dr("StatusId") Is System.DBNull.Value) Then .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .BankAcctDescrip = dr("BankAcctDescrip")
                .BankAcctNumber = dr("BankAcctNumber")
                .BankRoutingNumber = dr("BankRoutingNumber")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                If Not (dr("IsdefaultBankAcct") Is System.DBNull.Value) Then .IsDefaultBankAcct = dr("IsDefaultBankAcct")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return bankAcctInfo

    End Function
    Public Function UpdateBankAccountInfo(ByVal bankAcctInfo As BankAcctInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saBankAccounts Set BankId = ?, StatusId = ?, BankAcctDescrip = ?, ")
                .Append(" BankAcctNumber = ?, BankRoutingNumber = ?, CampGrpId = ?, IsDefaultBankAcct = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE bankAcctId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saBankAccounts where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   bankId
            db.AddParameter("@BankId", bankAcctInfo.BankId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            If bankAcctInfo.StatusId = Guid.Empty.ToString Then
                db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StatusId", bankAcctInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   bank account description
            db.AddParameter("@BankAcctDescrip", bankAcctInfo.BankAcctDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   bank account number
            db.AddParameter("@BankAcctNumber", bankAcctInfo.BankAcctNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Routing Number
            db.AddParameter("@BankRoutingNumber", bankAcctInfo.BankRoutingNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Campus Group Id
            If bankAcctInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", bankAcctInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   IsDefaultBankAccount
            db.AddParameter("@IsDefaultBankAcct", bankAcctInfo.IsDefaultBankAcct, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   BankAcctId
            db.AddParameter("@BankAcctId", bankAcctInfo.BankAcctId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", bankAcctInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   set the default bank account
                SetDefaultBankAccount()

                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddBankAccountInfo(ByVal bankAcctInfo As BankAcctInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saBankAccounts (BankAcctId, BankId, BankAcctDescrip, StatusId, ")
                .Append("   BankAcctNumber, BankRoutingNumber, CampGrpId, IsDefaultBankAcct, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   BankAcctId
            db.AddParameter("@BankAcctId", bankAcctInfo.BankAcctId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   BankId
            db.AddParameter("@BankId", bankAcctInfo.BankId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   bank account description
            db.AddParameter("@BankAcctDescrip", bankAcctInfo.BankAcctDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            If bankAcctInfo.StatusId = Guid.Empty.ToString Then
                db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StatusId", bankAcctInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   bank account number
            db.AddParameter("@BankAcctNumber", bankAcctInfo.BankAcctNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   bank routing number
            db.AddParameter("@RoutingNumber", bankAcctInfo.BankRoutingNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Campus Group Id
            If bankAcctInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", bankAcctInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   IsDefaultBankAccount
            db.AddParameter("@IsDefaultBankAcct", bankAcctInfo.IsDefaultBankAcct, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   set the default bank account
            SetDefaultBankAccount()

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteBankAcctInfo(ByVal bankAcctId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saBankAccounts ")
                .Append("WHERE bankAcctId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("Select count(*) from saBankAccounts where bankAcctId = ? ")
            End With

            '   add parameters values to the query

            '   BankAccId
            db.AddParameter("@BankAcctId", bankAcctId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   BankAccId
            db.AddParameter("@BankAcctId", bankAcctId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   set the default bank account
                SetDefaultBankAccount()

                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Private Sub SetDefaultBankAccount()

        '   Connect to the database
        'Dim db As New DataAccess
        'db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   BA.BankAcctId, BA.IsDefaultBankAcct ")
            .Append("FROM     saBankAccounts BA, systatuses ST ")
            .Append("WHERE    BA.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY BA.IsDefaultBankAcct desc, BA.ModDate desc ")
        End With

        '   fill the data set only with required information
        Dim ds As New DataSet
        'Dim da As OleDbDataAdapter = db.RunParamSQLDataAdapter(sb.ToString)
        Dim da As OleDbDataAdapter = New OleDbDataAdapter(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
        da.Fill(ds)

        '   only the first account should be marked as Default account
        If ds.Tables(0).Rows.Count > 0 Then
            ds.Tables(0).Rows(0).Item("IsDefaultBankAcct") = True
        End If

        '   all remaining accounts are marked as "false" (non Default Accounts)
        Dim i As Integer
        If ds.Tables(0).Rows.Count > 1 Then
            For i = 1 To ds.Tables(0).Rows.Count - 1
                ds.Tables(0).Rows(i).Item("IsDefaultBankAcct") = False
            Next
        End If

        '   build automatically insert, delete and update commands
        sb = New StringBuilder()
        With sb
            .Append("SELECT   BA.BankAcctId, BA.IsDefaultBankAcct ")
            .Append("FROM     saBankAccounts BA ")
        End With

        da.SelectCommand.CommandText = sb.ToString()
        Dim cb As OleDbCommandBuilder = New OleDbCommandBuilder(da)

        '   update dataset
        da.Update(ds)

    End Sub
    Public Function GetAdmissionDepositInfo(ByVal AdmissionDepositId As String) As AdmissionDepositInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT AdmDepositId, ")
            .Append("    AD.ProspectId, ")
            '.Append("     'E324437E-9561-407F-B993-FAD78C09DF26' As ProspectId, ")
            .Append("    (Select FirstName + ' ' + LastName from arStudent where StudentId=AD.ProspectId) As ProspectDescrip, ")
            '.Append("    'Anatoly Sljussar' As ProspectDescrip, ")
            .Append("    AD.StuEnrollId, ")
            .Append("    (Select PrgVerDescrip from arPrgVersions where PrgVerId=(select PrgVerId from arStuEnrollments where StuEnrollId=AD.StuEnrollId)) as EnrollmentDescrip, ")
            .Append("    'This is an enrollment description' as EnrollmentDescrip, ")
            .Append("    AD.AdmDepositDescrip, ")
            .Append("    AD.PaidById, ")
            .Append("    (Select PaidByDescrip from saPaidBys where PaidById=AD.PaidById) As PaidByDescrip, ")
            .Append("    AD.DepositDate, ")
            .Append("    AD.Amount, ")
            .Append("    AD.PaymentType, ")
            .Append("    AD.CheckNumber, ")
            .Append("    AD.CreditCardTypeId, ")
            .Append("    (Select CreditCardTypeDescrip from saCreditCardTypes where CreditCardTypeId=AD.CreditCardTypeId) As CreditCardType, ")
            .Append("    AD.ModUser, ")
            .Append("    AD.ModDate ")
            .Append("FROM  saAdmissionDeposits AD ")
            .Append("WHERE AD.AdmDepositId= ? ")
        End With

        ' Add the AdmissionDepositId to the parameter list
        db.AddParameter("@AdmDepositId", AdmissionDepositId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim admissionDepositInfo As New AdmissionDepositInfo

        While dr.Read()

            '   set properties with data from DataReader
            With admissionDepositInfo
                .AdmissionDepositId = AdmissionDepositId
                .IsInDB = True
                If Not (dr("ProspectId") Is System.DBNull.Value) Then .ProspectId = CType(dr("ProspectId"), Guid).ToString
                'If Not (dr("ProspectId") Is System.DBNull.Value) Then .ProspectId = dr("ProspectId")
                .ProspectDescription = dr("ProspectDescrip")
                If Not (dr("StuEnrollId") Is System.DBNull.Value) Then .StuEnrollId = CType(dr("StuEnrollId"), Guid).ToString
                .EnrollmentDescription = dr("EnrollmentDescrip")
                .AdmissionDepositDescription = dr("AdmDepositDescrip")
                If Not (dr("PaidById") Is System.DBNull.Value) Then .PaidById = CType(dr("PaidById"), Guid).ToString
                If Not (dr("PaidByDescrip") Is System.DBNull.Value) Then .PaidByDescription = dr("PaidByDescrip")
                If Not (dr("DepositDate") Is System.DBNull.Value) Then .DepositDate = dr("DepositDate")
                If Not (dr("Amount") Is System.DBNull.Value) Then .Amount = dr("Amount")
                .PaymentType = dr("PaymentType")
                If Not (dr("CheckNumber") Is System.DBNull.Value) Then .CheckNumber = dr("CheckNumber")
                If Not (dr("CreditCardTypeId") Is System.DBNull.Value) Then .CreditCardTypeId = CType(dr("CreditCardTypeId"), Guid).ToString
                If Not (dr("CreditCardType") Is System.DBNull.Value) Then .CreditCardType = dr("CreditCardType")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return admissionDepositInfo

    End Function
    Public Function UpdateAdmissionDepositInfo(ByVal AdmissionDepositInfo As AdmissionDepositInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saAdmissionDeposits Set ProspectId = ?, StuEnrollId = ?, AdmDepositDescrip = ?, ")
                .Append(" PaidById = ?, DepositDate = ?, Amount = ?, PaymentType = ?, ")
                .Append(" CheckNumber = ?, CreditCardTypeId = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE AdmDepositId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saAdmissionDeposits where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   ProspectId
            db.AddParameter("@ProspectId", AdmissionDepositInfo.ProspectId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StuEnrollId
            db.AddParameter("@StuEnrollId", AdmissionDepositInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Admission Deposit Description
            db.AddParameter("@AdmDepositDescrip", AdmissionDepositInfo.AdmissionDepositDescription, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   PaidById
            db.AddParameter("@PaidById", AdmissionDepositInfo.PaidById, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Deposit Date
            db.AddParameter("@DepositDate", AdmissionDepositInfo.DepositDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Deposit Amount
            If AdmissionDepositInfo.Amount = 0.0 Then
                db.AddParameter("@Amount", System.DBNull.Value, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)
            Else
                db.AddParameter("@Amount", AdmissionDepositInfo.Amount, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)
            End If

            '   Payment Type
            db.AddParameter("@PaymentType", AdmissionDepositInfo.PaymentType, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   Check Number
            If AdmissionDepositInfo.CheckNumber = "" Then
                db.AddParameter("@CheckNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CheckNumber", AdmissionDepositInfo.CheckNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   CreditCardTypeId
            If AdmissionDepositInfo.CreditCardTypeId = Guid.Empty.ToString Then
                db.AddParameter("@CreditCardTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CreditCardTypeId", AdmissionDepositInfo.CreditCardTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   AdmissionDepositId
            db.AddParameter("@AdmDepositId", AdmissionDepositInfo.AdmissionDepositId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", AdmissionDepositInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                '   build concurrency exception message
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddAdmissionDepositInfo(ByVal AdmissionDepositInfo As AdmissionDepositInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saAdmissionDeposits (AdmDepositId, ProspectId, StuEnrollId, AdmDepositDescrip, PaidById, ")
                .Append("   DepositDate, Amount, PaymentType, CheckNumber, CreditCardTypeId, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   AdmissionDepositId
            db.AddParameter("@AdmDepositId", AdmissionDepositInfo.AdmissionDepositId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ProspectId
            db.AddParameter("@ProspectId", AdmissionDepositInfo.ProspectId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StuEnrollId
            db.AddParameter("@StuEnrollId", AdmissionDepositInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Admission deposit description
            db.AddParameter("@AdmDepositDescrip", AdmissionDepositInfo.AdmissionDepositDescription, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   PaidById
            db.AddParameter("@PaidById", AdmissionDepositInfo.PaidById, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   DepositDate
            db.AddParameter("@DepositDate", AdmissionDepositInfo.DepositDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Amount
            db.AddParameter("@Amount", AdmissionDepositInfo.Amount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            '   PaymentType
            db.AddParameter("@PaymentType", AdmissionDepositInfo.PaymentType, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   CheckNumber
            If AdmissionDepositInfo.CheckNumber = "" Then
                db.AddParameter("@CheckNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CheckNumber", AdmissionDepositInfo.CheckNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   CreditCardTypeId
            If AdmissionDepositInfo.CreditCardTypeId = Guid.Empty.ToString Then
                db.AddParameter("@CreditCardTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CreditCardTypeId", AdmissionDepositInfo.CreditCardTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteAdmissionDepositInfo(ByVal AdmissionDepositId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saAdmissionDeposits ")
                .Append("WHERE AdmDepositId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("Select count(*) from saAdmissionDeposits where AdmDepositId = ? ")
            End With

            '   add parameters values to the query

            '   AdmissionDepositId
            db.AddParameter("@AdmissionDepositId", AdmissionDepositId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   AdmissionDepositId
            db.AddParameter("@AdmissionDepositId", AdmissionDepositId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Sub DisplayOleDbErrorCollection(ByVal myException As OleDbException)
        Dim i As Integer, str As String = ""
        For i = 0 To myException.Errors.Count - 1
            str += "Index #" + i.ToString() + ControlChars.Cr _
               + "Message: " + myException.Errors(i).Message + ControlChars.Cr _
               + "Native: " + myException.Errors(i).NativeError.ToString() + ControlChars.Cr _
               + "Source: " + myException.Errors(i).Source + ControlChars.Cr _
               + "SQL: " + myException.Errors(i).SQLState + ControlChars.Cr
        Next i
    End Sub

End Class
