Imports FAME.Advantage.Common

Public Class StatusCodeDB
    Public Enum eStatusCodes
        NewLead = 1
        ApplicationReceived = 2
        ApplicationNotAccepted = 3
        InterviewScheduled = 4
        Interviewed = 5
        Enrolled = 6
        FutureStart = 7
        NoStart = 8
        CurrentlyAttending = 9
        LeaveOfAbsence = 10
        Suspension = 11
        Dropped = 12
        Graduated = 14
        Active = 15
        InActive = 16
        DeadLead = 17
        WillEnrollintheFuture = 18
        TransferOut = 19
        AcademicProbationSap = 20
        Externship = 22
        DisciplinaryProbation = 23
        WarningProbation = 24
    End Enum
    Public Function GetDropDownList() As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da1 As OleDbDataAdapter
        Dim strSQLString As New StringBuilder
        With strSQLString
            .Append("SELECT t1.StatusLevelId,t1.StatusLevelDescrip ")
            .Append("FROM syStatusLevels t1  where StatusLevelid <> 3 ")
            .Append("ORDER BY StatusLevelDescrip")
        End With
        db.OpenConnection()
        Try
            da1 = db.RunParamSQLDataAdapter(strSQLString.ToString)
            da1.Fill(ds, "StatusLevelDT")

            strSQLString.Remove(0, strSQLString.Length)

            Dim da2 As OleDbDataAdapter
            strSQLString.Remove(0, strSQLString.Length)

            With strSQLString
                .Append("Select StatusId, Status from syStatuses ")
                .Append("ORDER BY Status")
            End With
            'db.OpenConnection()
            da2 = db.RunParamSQLDataAdapter(strSQLString.ToString)

            da2.Fill(ds, "StatusDT")

            Dim da3 As OleDbDataAdapter
            strSQLString.Remove(0, strSQLString.Length)

            With strSQLString
                .Append("Select CampGrpId, CampGrpDescrip from syCampGrps ")
                .Append("ORDER BY CampGrpDescrip")
            End With
            ' db.OpenConnection()
            da3 = db.RunParamSQLDataAdapter(strSQLString.ToString)
            da3.Fill(ds, "CampGrpDT")
        Finally
            db.CloseConnection()
        End Try

        Return ds
    End Function

    Public Function GetStatusCodes(StatusLevel As String, Status As String, CampusGroup As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Try
            da = GetStatusCodesByStatus(StatusLevel, Status, CampusGroup)
            da.Fill(ds, "StatusCodes")
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

        Return ds
    End Function
    Public Function GetStatusCodesByStatus(StatusLevelId As String, StatusId As String, CampusGroupId As String) As OleDbDataAdapter
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        ' Dim da As OleDbDataAdapter

        With sb
            .Append("   SELECT Distinct a.StatusCodeId, a.StatusCode, a.StatusCodeDescrip,c.StatusId,c.Status  ")
            .Append("   FROM syStatusCodes a, sySysStatus b,syStatuses c  ")
            If Not StatusLevelId = "" Then
                .Append("   WHERE a.SysStatusId = b.SysStatusId and a.StatusId = c.StatusId and b.StatusLevelId = ?  ")
            End If
            If Not StatusId = "" Then
                .Append("   and a.StatusId = ? ")
            End If
            If Not CampusGroupId = "" Then
                .Append("   and a.CampGrpId = ?  ")
            End If
            .Append(" order by c.Status,a.StatusCodeDescrip ")
        End With

        If Not StatusLevelId = "" Then
            db.AddParameter("@statuslevelId", StatusLevelId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not StatusId = "" Then
            db.AddParameter("@statusId", StatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not CampusGroupId = "" Then
            db.AddParameter("@campgrpId", CampusGroupId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.OpenConnection()
        Try
            db.RunParamSQLDataAdapter(sb.ToString)
        Catch ex As Exception
            Throw New Exception(ex.InnerException.Message)

        Finally
            db.CloseConnection()
            db.ClearParameters()
        End Try

        Return Nothing
    End Function
    Public Function GetStatusCodesByStatusAndDefault(StatusLevelId As String, StatusId As String, CampusGroupId As String) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        With sb
            .Append("   SELECT Distinct a.StatusCodeId, a.StatusCode, ")
            .Append("   Case when a.IsDefaultLeadStatus=1 then a.StatusCodeDescrip + ' (Default for ' +  (select Distinct CASE WHEN CampGrpDescrip = 'All' THEN 
							campGrpDescrip + ' Campus Groups' 
					ELSE CampGrpDescrip END  from syCampGrps where CampGrpId=a.CampgrpId) + ')' else a.StatusCodeDescrip end	 as StatusCodeDescrip, ")
            .Append("   C.StatusId,c.Status  ")
            .Append("   FROM syStatusCodes a, sySysStatus b,syStatuses c  ")
            If Not StatusLevelId = "" Then
                .Append("   WHERE a.SysStatusId = b.SysStatusId and a.StatusId = c.StatusId and b.SysStatusId NOT IN (20) and b.StatusLevelId = ?  ")
            End If
            If Not StatusId = "" Then
                .Append("   and a.StatusId = ? ")
            End If
            If Not CampusGroupId = "" Then
                .Append("   and a.CampGrpId = ?  ")
            End If
            .Append(" order by c.Status,StatusCodeDescrip ")
        End With

        If Not StatusLevelId = "" Then
            db.AddParameter("@statuslevelId", StatusLevelId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not StatusId = "" Then
            db.AddParameter("@statusId", StatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not CampusGroupId = "" Then
            db.AddParameter("@campgrpId", CampusGroupId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        db.OpenConnection()
        Try
            Return db.RunParamSQLDataSet(sb.ToString)
        Finally
            db.CloseConnection()
        End Try
    End Function

    Public Function GetSysStatusesByStatusLevel(statusLevelId As String, includeProbation As Boolean) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim da As OleDbDataAdapter
        Dim ds As New DataSet

        With sb
            .Append("SELECT a.SysStatusId, a.SysStatusDescrip ")
            .Append(" FROM sySysStatus a ")
            .Append(" WHERE a.StatusLevelId = ? ")
            .Append(" AND a.SysStatusId <> " + CType(eStatusCodes.WarningProbation, Int32).ToString())
            .Append(" AND a.SysStatusId <> " + CType(eStatusCodes.DisciplinaryProbation, Int32).ToString())

            If includeProbation = False Then
                .Append(" AND a.SysStatusId NOT IN (" + CType(eStatusCodes.AcademicProbationSap, Int32).ToString() + ")")
            End If

        End With

        ' Add the ClsSectId to the parameter list
        db.AddParameter("@termId", statusLevelId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "StatusDT")
        Finally
            db.CloseConnection()
        End Try

        Return ds
    End Function

    Public Function GetStatusCodeDetails(statusCodeId As String) As StatusCodeInfo

        'connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With sub-queries
            .Append("SELECT a.StatusCodeId,a.StatusCode,a.StatusCodeDescrip,")
            .Append("       a.StatusId,a.CampGrpId,a.SysStatusId, b.StatusLevelId, ")
            If myAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                .Append(" a.MapId, ")
            End If
            '.Append("       a.DiscProbation,a.AcadProbation, ")
            .Append("       a.ModUser, a.ModDate, a.IsDefaultLeadStatus, ")
            .Append("       IsInUse = (CASE WHEN (  (SELECT COUNT(*) ")
            .Append("                               FROM arStuEnrollments ")
            .Append("                               WHERE StatusCodeId=a.StatusCodeId ) + ")
            .Append("                               (SELECT COUNT(*) ")
            .Append("                               FROM arStudent ")
            .Append("                               WHERE StudentStatus=a.StatusCodeId ) + ")
            .Append("                               (SELECT COUNT(*) ")
            .Append("                               FROM adLeads ")
            .Append("                               WHERE LeadStatus=a.StatusCodeId )) > 0 ")
            .Append("                       THEN 'True' ELSE 'False' END) ")
            .Append("FROM syStatusCodes a, sySysStatus b ")
            .Append("WHERE a.StatusCodeId = ? AND a.SysStatusId = b.SysStatusId")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@CourseGrpID", statusCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim obj As New StatusCodeInfo

        While dr.Read()
            With obj
                '.StudentId = dr("StudentId")
                .StatusCodeId = dr("StatusCodeId")
                .Code = dr("StatusCode").ToString
                .Descrip = dr("StatusCodeDescrip").ToString
                .StatusId = dr("StatusId")
                .CampusGroup = dr("CampGrpId")
                .SystemStatus = dr("SysStatusId")
                .StatusLevelId = dr("StatusLevelId")
                'If Not (dr("AcadProbation") Is System.DBNull.Value) Then Obj.AcadProbation = dr("AcadProbation") Else Obj.AcadProbation = 0
                'If Not (dr("DiscProbation") Is System.DBNull.Value) Then Obj.DiscProbation = dr("DiscProbation") Else Obj.DiscProbation = 0
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
                .IsInUse = dr("IsInUse")
                If Not dr("IsDefaultLeadStatus") Is DBNull.Value Then
                    .IsDefaultLeadStatus = dr("IsDefaultLeadStatus")
                Else
                    .IsDefaultLeadStatus = False
                End If
                If myAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    If Not (dr("MapId") Is DBNull.Value) Then .AcadStat = dr("MapId") Else .AcadStat = ""
                End If
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return obj
    End Function

    Public Function InsertStatusCode(obj As StatusCodeInfo, user As String) As String

        If obj.IsDefaultLeadStatus Then
            ClearDefaultStatus(obj.SystemStatus, obj.StatusLevelId, obj.CampusGroup.ToString())
        End If

        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        'Set the connection string
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Try
            With sb
                .Append("INSERT INTO syStatusCodes(StatusCodeId,StatusCode,StatusCodeDescrip,SysStatusId,StatusId,CampGrpId,IsDefaultLeadStatus,ModUser,ModDate ")
                If myAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    .Append(" ,MapId ")
                End If
                .Append(" ) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?")
                If myAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    .Append(",?")
                End If
                .Append(") ")
            End With
            db.AddParameter("@courseid", obj.StatusCodeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.AddParameter("@code", obj.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@descrip", obj.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.AddParameter("@hours", obj.SystemStatus, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@statusid", obj.StatusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.AddParameter("@CampGrpId", obj.CampusGroup, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            'db.AddParameter("@DiscProb", Obj.DiscProbation, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            'db.AddParameter("@AcadProb", Obj.AcadProbation, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)


            db.AddParameter("@defar", obj.IsDefaultLeadStatus, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)


            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'Acad Status
            If myAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                If obj.AcadStat = "" Then
                    db.AddParameter("@acadstatus", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@acadstatus", obj.AcadStat, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
            End If
            '   Execute the query
            db.OpenConnection()
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Function UpdateStatusCode(obj As StatusCodeInfo, user As String) As String
        If obj.IsDefaultLeadStatus Then
            ClearDefaultStatus(obj.SystemStatus, obj.StatusLevelId, obj.CampusGroup.ToString())
        End If

        Dim sb As New StringBuilder
        Dim conn As SqlConnection
        Dim comm As SqlCommand

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        'Set the connection string
        Dim connectionString = myAdvAppSettings.AppSettings("ConnectionString")

        With sb
            .Append("UPDATE syStatusCodes ")
            .Append("SET    StatusCode = @code, StatusCodeDescrip= @descrip, ")
            .Append("       SysStatusId = @systemStatus, StatusId = @statusid, CampGrpId = @CampGrpId, IsDefaultLeadStatus=@defar, ")
            .Append("       ModUser = @ModUser, ModDate = @ModDate ")
            .Append("WHERE  StatusCodeId = @StatusCodeId ")
        End With

        conn = New SqlConnection(connectionString)
        comm = New SqlCommand(sb.ToString(), conn)



        comm.Parameters.AddWithValue("@code", obj.Code)
        comm.Parameters.AddWithValue("@descrip", obj.Descrip)
        comm.Parameters.AddWithValue("@systemStatus", obj.SystemStatus)
        comm.Parameters.AddWithValue("@statusid", obj.StatusId)
        comm.Parameters.AddWithValue("@CampGrpId", obj.CampusGroup)
        comm.Parameters.AddWithValue("@defar", obj.IsDefaultLeadStatus)
        comm.Parameters.AddWithValue("@ModUser", user)
        Dim now As Date = Date.Now
        comm.Parameters.AddWithValue("@ModDate", now)
        comm.Parameters.AddWithValue("@StatusCodeId", obj.StatusCodeId)

        conn.Open()
        Try
            'Execute the query
            comm.ExecuteNonQuery()
        Finally
            'Close Connection
            conn.Close()
        End Try
        Return String.Empty
    End Function


    'Public Function UpdateStatusCode(Obj As StatusCodeInfo, user As String) As String
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder

    '    Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

    '    'Set the connection string
    '    db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

    '    With sb
    '        .Append("UPDATE syStatusCodes ")
    '        .Append("SET    StatusCode = ?, StatusCodeDescrip= ?, ")
    '        .Append("       SysStatusId = ?, StatusId = ?, CampGrpId = ?, IsDefaultLeadStatus=?, ")
    '        '.Append("       DiscProbation = ?, AcadProbation = ?, ")
    '        .Append("       ModUser = ?, ModDate = ? ")
    '        If myAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
    '            .Append(" ,MapId=? ")
    '        End If
    '        .Append("WHERE  StatusCodeId = ? ")
    '        '.Append("       AND ModDate = ? ;")
    '        '.Append("SELECT COUNT(*) FROM syStatusCodes WHERE ModDate = ? ")
    '    End With

    '    db.AddParameter("@code", Obj.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@descrip", Obj.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@systemStatus", Obj.SystemStatus, DataAccess.OleDbDataType.OleDbInteger, 16, ParameterDirection.Input)
    '    db.AddParameter("@statusid", Obj.StatusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@CampGrpId", Obj.CampusGroup, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

    '    'db.AddParameter("@DiscProb", Obj.DiscProbation, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
    '    'db.AddParameter("@AcadProb", Obj.AcadProbation, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)

    '    'Default Lead Status
    '    db.AddParameter("@defar", Obj.IsDefaultLeadStatus, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

    '    'ModUser
    '    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '    'Updated ModDate
    '    Dim now As Date = Date.Now
    '    db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '    'Acad Status
    '    If myAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
    '        If Obj.AcadStat = "" Then
    '            db.AddParameter("@acadstatus", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@acadstatus", Obj.AcadStat, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If
    '    End If

    '    db.AddParameter("@StatusCodeId", Obj.StatusCodeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

    '    'Original ModDate
    '    db.AddParameter("@Original_ModDate", Obj.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '    'Updated ModDate
    '    db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '    '   Execute the query
    '    db.OpenConnection()
    '    Try
    '        'Execute the query
    '        'Dim rowCount = db.RunParamSQLScalar(sb.ToString)
    '        db.RunParamSQLScalar(sb.ToString)
    '        'If there were no updated rows then there was a concurrency problem
    '        'If rowCount = 1 Then
    '        '    '   return without errors
    '        '    Return ""
    '        'Else
    '        '    Return DALExceptions.BuildConcurrencyExceptionMessage()
    '        'End If

    '    Catch ex As OleDbException
    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)

    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    '    Return String.Empty
    'End Function

    Public Function DeleteStatusCode(statusCodeId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        'Set the connection string
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Try
            With sb
                .Append("DELETE FROM syStatusCodes ")
                .Append("WHERE StatusCodeId = ? ")

            End With

            db.AddParameter("@coursegrpid", statusCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        Return String.Empty
    End Function

    Public Function ClearDefaultStatus(systemStatus As Integer, statusLevel As Integer, campusGroup As String) As Boolean
        Dim sb As New StringBuilder
        Dim conn As SqlConnection
        Dim comm As SqlCommand

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        'Set the connection string
        Dim connectionString = myAdvAppSettings.AppSettings("ConnectionString")

        With sb
            .Append("UPDATE SC ")
            .Append("SET IsDefaultLeadStatus = 0 ")
            .Append("FROM dbo.syStatusCodes SC ")
            .Append("JOIN dbo.sySysStatus SS ON SS.SysStatusId = SC.SysStatusId ")
            .Append("       WHERE SC.CampGrpId = '" + campusGroup + "' AND ")
            .Append("       SC.SysStatusId = " + systemStatus.ToString() + " AND SS.StatusLevelId = " + statusLevel.ToString() + " AND SC.IsDefaultLeadStatus =1")
        End With

        conn = New SqlConnection(connectionString)
        comm = New SqlCommand(sb.ToString(), conn)



        conn.Open()
        Try
            'Execute the query
            comm.ExecuteNonQuery()
            Return True
        Finally
            'Close Connection
            conn.Close()

        End Try
        Return False
    End Function

    Public Function GetAllStatusCodes() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   SC.StatusCodeId, ")
            .Append("         SC.StatusCodeDescrip ")
            .Append("FROM     syStatusCodes SC ")
            .Append("ORDER BY SC.StatusCodeDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    ''' <summary>
    ''' Get All Status Codes for Students. This returns only status codes that are also used in arStuEnrollments.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetAllStatusCodesForStudents() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       SC.StatusCodeId, ")
            .Append("       SC.StatusCodeDescrip ")
            .Append("FROM     arStuEnrollments SE, syStatusCodes SC, sySysStatus SS ")
            .Append("WHERE ")
            .Append("       SE.StatusCodeId=SC.StatusCodeId ")
            .Append("AND    SC.SysStatusId = SS.SysStatusId ")
            .Append("AND    SS.StatusLevelId = 2 ")
            .Append("ORDER BY SC.StatusCodeDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    ''' <summary>
    ''' Get All Status Codes for Students for a specific campus. This returns only status codes that are also used in arStuEnrollments.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetAllStatusCodesForStudents(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       SC.StatusCodeId, ")
            .Append("       SC.StatusCodeDescrip ")
            .Append("FROM     arStuEnrollments SE, syStatusCodes SC, sySysStatus SS ")
            .Append("WHERE ")
            .Append("       SE.StatusCodeId=SC.StatusCodeId ")
            .Append("AND    SC.SysStatusId = SS.SysStatusId ")
            .Append("AND    SS.StatusLevelId = 2 ")
            .Append("AND	SC.CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
            .Append("AND    SE.CampusId = ? ")
            .Append("ORDER BY SC.StatusCodeDescrip ")
        End With

        'CampusId
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    ''' <summary>
    ''' Get All Status Codes for Students for a specific campus.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetEnrollmentStatusCodes(campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       SC.StatusCodeId, ")
            .Append("       SC.StatusCodeDescrip ")
            .Append("FROM     syStatusCodes SC, sySysStatus SS ")
            .Append("WHERE ")
            .Append("   SC.SysStatusId = SS.SysStatusId ")
            .Append("AND    SS.StatusLevelId = 2 AND  SS.SysStatusId <>  20")
            .Append("AND	SC.CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
            .Append("ORDER BY SC.StatusCodeDescrip ")
        End With

        'CampusId
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllStatusCodesForLeads() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT DISTINCT ")
            .Append("         SC.StatusCodeId, ")
            .Append("         SC.StatusCodeDescrip ")
            .Append("FROM     adLeads L, syStatusCodes SC, sySysStatus SS ")
            .Append("WHERE ")
            .Append("         L.LeadStatus=SC.StatusCodeId ")
            .Append("AND      SC.SysStatusId = SS.SysStatusId ")
            .Append("AND      SS.StatusLevelId = 1 ")
            .Append("ORDER BY SC.StatusCodeDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function

    Public Function CheckIfDefaulExist(statusLevel As Integer, systemStatus As Integer, campusGroup As Guid) As Boolean
        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT ")
            .Append("       Count(*) ")
            .Append("FROM ")
            .Append("       dbo.syStatusCodes AS SC ")
            .Append("JOIN dbo.sySysStatus AS SS ON SC.SysStatusId = SS.SysStatusId ")
            .Append("       WHERE SC.CampGrpId = ? AND ")
            .Append("       SC.SysStatusId = ? AND SS.StatusLevelId = ? AND SC.IsDefaultLeadStatus =1")
        End With

        db.AddParameter("@CampusGroupId", campusGroup, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
        db.AddParameter("@SysStatusId", systemStatus, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@StatusLevelId", statusLevel, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   return dataset
        Return CType(db.RunParamSQLScalar(sb.ToString), Boolean)
    End Function


    Public Function DoesDefaultLeadExistForAllCampusGroup(statusLevel As Integer) As String
        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       Count(*) ")
            .Append("FROM ")
            .Append("       syStatusCodes U, sySysStatus T ")
            .Append("WHERE ")
            .Append("       T.StatusLevelId =1 and U.SysStatusId=T.SysStatusId AND ")
            .Append("       U.CampGrpId in (Select CampGrpId from syCampGrps where IsAllCampusGrp = 1) ")
            .Append("AND IsDefaultLeadStatus=1")
        End With

        ' Add the UserId to the parameter list
        db.AddParameter("@mod", statusLevel, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   return boolean
        Return CType(db.RunParamSQLScalar(sb.ToString), Boolean)
    End Function



    Public Function DoesDefaultLeadStatusExistForCampusGroup(statusLevel As Integer, campusGroup As String) As Boolean
        '   connect to the database
        Dim db As New DataAccess
        Dim defLeadStatusCmpGrp As String
        Dim ds As DataSet

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       U.CampGrpId ")
            .Append("FROM ")
            .Append("       syStatusCodes U, sySysStatus T ")
            .Append("WHERE ")
            .Append("       T.StatusLevelId =1 and U.SysStatusId=T.SysStatusId ")
            .Append("AND IsDefaultLeadStatus=1")
        End With
        ' Add the UserId to the parameter list
        db.AddParameter("@mod", statusLevel, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read()
            defLeadStatusCmpGrp = dr("CampGrpId").ToString
            If (defLeadStatusCmpGrp <> campusGroup) Then
                ds = DoCommonCampusesExist(campusGroup, defLeadStatusCmpGrp)
                If ds.Tables(0).Rows.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return True
            End If
        End While

        If Not dr.IsClosed Then dr.Close()

        Return False
    End Function
    Public Function DoCommonCampusesExist(campusGroup1 As String, campusGroup2 As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("Select Distinct ")

            .Append("T1.CampusId as CommonCampus, ")

            .Append("(Select CampDescrip from syCampuses where CampusId=T1.CampusId) As CampusDescription, ")

            .Append("(Select CampGrpDescrip from syCampGrps where CampGrpId=T2.CampGrpId) As CampusGroupDescription, ")

            .Append("T2.CampGrpId as DifferentCampusGroup ")

            .Append("from syCmpGrpCmps T1, syCmpGrpCmps T2 ")

            .Append("where(T1.CampusId = T2.CampusId) ")

            .Append("and T1.CmpGrpCmpId<>T2.CmpGrpCmpId ")

            .Append("and T1.CampGrpId = ? and T2.CampGrpId = ? ")

            .Append("ORDER BY CommonCampus, DifferentCampusGroup ")
        End With

        ' Add the UserId to the parameter list
        db.AddParameter("@campgrp1", campusGroup1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campgrp2", campusGroup2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetDefaultLeadStatusCode() As String

        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append(" select Distinct StatusCodeId from syStatusCodes where IsDefaultLeadStatus=1 ")
        End With

        '   return dataset
        Dim strCountryId As Guid = db.RunParamSQLScalar(sb.ToString)
        Return strCountryId.ToString
    End Function
    Public Function CheckIfDefaultStatusCodeExistForCampusGroup(ByVal campGrpId As String) As String
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append(" select Distinct Top 1  StatusCodeId from syStatusCodes where CampGrpId='" & campGrpId & "' and IsDefaultLeadStatus=1 ")
        End With

        '   return dataset
        Dim strCountryId As Guid = db.RunParamSQLScalar(sb.ToString)
        Return strCountryId.ToString
    End Function

    ''To show the Status of the student who are in School enrollment Status
    ''i.e CurrentlyAttending- 9, Future Start -7, LOA-10, Suspended, 11, Suspension-21, Academic probation-20
    Public Function GetAllStatusCodesForStudentswithInSchoolEnrollment(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       SC.StatusCodeId, ")
            .Append("       SC.StatusCodeDescrip ")
            .Append("FROM     arStuEnrollments SE, syStatusCodes SC, sySysStatus SS ")
            .Append("WHERE ")
            .Append("       SE.StatusCodeId=SC.StatusCodeId ")
            .Append("AND    SC.SysStatusId = SS.SysStatusId ")
            .Append("AND    SS.StatusLevelId = 2 ")
            .Append("AND	SC.CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
            .Append("AND    SE.CampusId = ? ")
            ''modified on June 10 2009
            ''To fix issue 14829
            ''22 added for in school status - externship
            .Append(" And  SE.StatusCodeId in (Select StatusCodeid from SyStatusCodes where SysStatusId in (7,9,10,11,20,21,22) ) ")

            .Append("ORDER BY SC.StatusCodeDescrip ")
        End With

        'CampusId
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetSystemStatusByStatusCode(statusCode As Guid) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim da As OleDbDataAdapter
        Dim ds As New DataSet

        With sb
            .Append("SELECT a.SysStatusId, a.SysStatusDescrip ")
            .Append("FROM sySysStatus a ")
            .Append(" inner join syStatusCodes b on b.SysStatusId = a.SysStatusId ")
            .Append("WHERE b.StatusCodeId = ? ")
        End With

        ' Add the ClsSectId to the parameter list
        db.AddParameter("@statusCode", statusCode, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)

        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "StatusDT")
        Finally
            db.CloseConnection()
        End Try

        Return ds
    End Function

End Class

