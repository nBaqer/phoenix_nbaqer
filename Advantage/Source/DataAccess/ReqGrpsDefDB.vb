Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class ReqGrpsDefDB
    Public Function GetAvailSelectedCourses(ByVal ReqGrpId As String) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Dim sb As New System.Text.StringBuilder

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            'Set the connection string
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")



            With sb
                .Append("SELECT t1.adReqId,t1.Descrip,t1.Code ")
                .Append("FROM adReqs t1 WHERE ")
                .Append("t1.adReqId not in (Select adReqId from adReqGrpDef t3 where ReqGrpId = ?) ")
                .Append("ORDER BY t1.Descrip")

            End With


            'db.OpenConnection()
            db.AddParameter("@reqgrpid", ReqGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "AvailCourses")
            sb.Remove(0, sb.Length)
            db.ClearParameters()


            Dim da2 As New OleDbDataAdapter
            With sb
                .Append("SELECT DISTINCT a.adReqId, b.Descrip,a.Sequence,a.ModDate ")
                .Append("FROM adReqGrpDef a, adReqs b ")
                .Append("WHERE a.ReqGrpId = ? and a.adReqId = b.adReqId ")
            End With

            'db.OpenConnection()
            db.AddParameter("@reqgrp2", ReqGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            da2 = db.RunParamSQLDataAdapter(sb.ToString)
            Try
                da2.Fill(ds, "SelectedCourses")
            Catch ex As System.Exception
                Throw New BaseException(ex.Message)
            End Try
            sb.Remove(0, sb.Length)


            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds
    End Function
    Public Function GetMandatoryReqs() As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim sb As New System.Text.StringBuilder

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            'Set the connection string
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


            With sb
                .Append("SELECT t1.adReqId,t1.Descrip,t1.Code ")
                .Append("FROM adReqs t1 WHERE ")
                .Append("t1.AppliesToAll = 1 ")
                .Append("ORDER BY t1.Descrip")

            End With


            db.OpenConnection()

            '   Execute the query
            ds = db.RunSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds
    End Function
    Public Function GetReqsForLeadGrps(ByVal LeadGrpId As String) As DataSet
        Dim ds As DataSet

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.adReqId, t1.LeadGrpId,t1.IsRequired,t2.Descrip ")
                .Append("FROM adReqLeadGroups t1, adLeadGroups t2 ")
                .Append("WHERE t1.LeadGrpId = ? and t1.LeadGrpId = t2.LeadGrpId ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@PrgVerId", LeadGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()


        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds
    End Function
    Public Function AssignReqsToReqGrp(ByVal ChildInfoObj As ChildInfo, ByVal ID As String, ByVal user As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim childID As String
        Dim childTyp As String
        Dim childSeq As Integer
        Dim req As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        childID = ChildInfoObj.ChildId
        ' childTyp = ChildInfoObj.ChildType
        childSeq = ChildInfoObj.ChildSeq
        ' req = ChildInfoObj.Req


        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


        With sb
            .Append("INSERT INTO adReqGrpDef(ReqGrpId,adReqId,Sequence,ModUser,ModDate) ")
            .Append("VALUES(?,?,?,?,?)")
        End With

        db.AddParameter("@coursegrpid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@childid", childID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@childseq", childSeq, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'db.AddParameter("@req", req, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '   ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   ModDate
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)



        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function

    'Public Function UpdateReqGrpDef(ByVal ChildInfoObj As ChildInfo, ByVal ID As String, ByVal user As String)
    '    Dim db As New DataAccess
    '    Dim sb As New System.Text.StringBuilder
    '    Dim ds As DataSet
    '    Dim childID As String
    '    Dim childTyp As String
    '    Dim childSeq As Integer
    '    Dim req As Integer

    '    childID = ChildInfoObj.ChildId
    '    childSeq = ChildInfoObj.ChildSeq

    '    'Set the connection string
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")


    '    With sb
    '        .Append("UPDATE adReqGrpDef Set Sequence = ?, ")
    '        .Append("ModUser=?, ")
    '        .Append("ModDate=? ")
    '        .Append("WHERE  adReqId = ? and ReqGrpId = ? ")
    '        .Append("AND ModDate = ? ;")
    '        .Append("Select count(*) from adReqGrpDef where adReqId = ? and ReqGrpId = ? and ModDate = ? ")

    '        db.AddParameter("@childseq", childSeq, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        '   ModDate
    '        Dim now As Date = Date.Now
    '        db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        db.AddParameter("@childid", childID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@ReqGrpId", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        '   ModDate
    '        db.AddParameter("@Original_ModDate", ChildInfoObj.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        db.AddParameter("@childid2", childID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@ReqGrpId2", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        '   ModDate
    '        db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '    End With



    '    db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '    db.ClearParameters()
    '    sb.Remove(0, sb.Length)

    '    'Close Connection
    '    db.CloseConnection()
    'End Function
    Public Function UpdateReqGrpDef(ByVal ChildInfoObj As ChildInfo, ByVal ID As String, ByVal user As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim childID As String
        Dim childTyp As String
        Dim childSeq As Integer
        Dim req As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        childID = ChildInfoObj.ChildId
        childSeq = ChildInfoObj.ChildSeq

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


        With sb
            .Append("UPDATE adReqGrpDef Set Sequence = ?, ")
            .Append("ModUser=?, ")
            .Append("ModDate=? ")
            .Append("WHERE  adReqId = ? and ReqGrpId = ? ")
            '.Append("AND ModDate = ? ;")
            '.Append("Select count(*) from adReqGrpDef where adReqId = ? and ReqGrpId = ? and ModDate = ? ")

            db.AddParameter("@childseq", childSeq, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@childid", childID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ReqGrpId", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ''   ModDate
            'db.AddParameter("@Original_ModDate", ChildInfoObj.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'db.AddParameter("@childid2", childID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'db.AddParameter("@ReqGrpId2", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ''   ModDate
            'db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End With



        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function

    Public Function DeleteReqsFrmReqGrp(ByVal ChildInfoObj As ChildInfo, ByVal ID As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


        With sb
            .Append("DELETE FROM adReqGrpDef ")
            .Append("WHERE ReqGrpId = ? ")
            .Append("AND adReqId = ? ")
        End With

        db.AddParameter("@ReqGrpId", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@childid", ChildInfoObj.ChildId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)



        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function
    'Public Function GetReqLeadGroups(ByVal ReqId As String) As String
    '    Dim ds As New DataSet
    '    Dim da As OleDbDataAdapter
    '    Dim count As String
    '    Dim sLeadGrpId As String
    '    Try
    '        '   connect to the database
    '        Dim db As New DataAccess
    '        db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '        Dim sb As New System.Text.StringBuilder

    '        '   build the sql query
    '        With sb
    '            .Append("Select LeadGrpId from adReqLeadGroups where adReqId = ? ")

    '        End With

    '        ' Add the PrgVerId and ChildId to the parameter list
    '        db.AddParameter("@adReqId", ReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


    '        db.OpenConnection()
    '        'Execute the query
    '        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)


    '        While dr.Read()
    '            sLeadGrpId = sLeadGrpId & dr("LeadGrpId").ToString & ","
    '        End While


    '        'Close Connection
    '        db.CloseConnection()

    '    Catch ex As System.Exception
    '        Throw New BaseException(ex.Message)
    '    End Try

    '    'Return the datatable in the dataset
    '    Return sLeadGrpId

    'End Function
    Public Function GetReqLeadGroups(ByVal ReqId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim count As String
        Dim sLeadGrpId As String
        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("Select a.LeadGrpId, b.Descrip from adReqLeadGroups a, adLeadGroups b where a.adReqId = ? ")
                .Append("and a.LeadGrpId = b.LeadGrpId ")

            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@adReqId", ReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            db.OpenConnection()
            '   Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()


        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
End Class
