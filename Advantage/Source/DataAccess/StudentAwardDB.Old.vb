
Imports System.Xml
Imports FAME.Advantage.Common

Public Class StudentAwardDB
    Public Sub AddStudentAward(ByVal StudentAwardInfo As StudentAwardInfo, ByVal Details As DataTable)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("INSERT INTO faStudentAwards")
            .Append("(StudentAwardId, StudentId, AwardTypeId, AwardYearId, GrossAmount, LoanFees, NetLoanAmount, LenderId, ServicerId, GuarantorId) ")
            .Append("VALUES(?,?,?,?,?,?,?,?,?,?)")
        End With
        db.AddParameter("@StudentAwardId", StudentAwardInfo.StudentAwardId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@StudentId", StudentAwardInfo.StudentId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@AwardType", StudentAwardInfo.AwardType, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@AwardYear", StudentAwardInfo.AwardYear, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@GrossAmount", StudentAwardInfo.GrossAmount, DataAccess.OleDbDataType.OleDbDecimal, 9, ParameterDirection.Input)
        db.AddParameter("@LoanFees", StudentAwardInfo.LoanFees, DataAccess.OleDbDataType.OleDbDecimal, 9, ParameterDirection.Input)
        db.AddParameter("@NetLoanAmount", StudentAwardInfo.NetLoanAmount, DataAccess.OleDbDataType.OleDbDecimal, 9, ParameterDirection.Input)
        db.AddParameter("@LenderId", StudentAwardInfo.Lender, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@ServicerId", StudentAwardInfo.Servicer, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@GuarantorId", StudentAwardInfo.Guarantor, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
        ' Then Insert/Update/Delete the detail records associated with the header
        If Not IsNothing(Details) Then
            InsertUpdateDeleteStudentAwardSchedule(StudentAwardInfo.StudentAwardId, Details)
        End If
    End Sub
    Public Sub UpdateStudentAward(ByVal StudentAwardInfo As StudentAwardInfo, ByVal Details As DataTable)
        ' First update the header record
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("UPDATE faStudentAwards SET ")
            .Append("StudentId = ?")
            .Append(",AwardTypeId = ?")
            .Append(",AwardYearId = ?")
            .Append(",GrossAmount = ?")
            .Append(",LoanFees = ?")
            .Append(",NetLoanAmount = ?")
            .Append(",LenderId = ?")
            .Append(",ServicerId = ?")
            .Append(",GuarantorId = ?")
            .Append(" WHERE StudentAwardId = ? ")
        End With

        db.AddParameter("@StudentId", StudentAwardInfo.StudentId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@AwardType", StudentAwardInfo.AwardType, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@AwardYear", StudentAwardInfo.AwardYear, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@GrossAmount", StudentAwardInfo.GrossAmount, DataAccess.OleDbDataType.OleDbDecimal, 9, ParameterDirection.Input)
        db.AddParameter("@LoanFees", StudentAwardInfo.LoanFees, DataAccess.OleDbDataType.OleDbDecimal, 9, ParameterDirection.Input)
        db.AddParameter("@NetLoanAmount", StudentAwardInfo.NetLoanAmount, DataAccess.OleDbDataType.OleDbDecimal, 9, ParameterDirection.Input)
        db.AddParameter("@LenderId", StudentAwardInfo.Lender, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@ServicerId", StudentAwardInfo.Servicer, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@GuarantorId", StudentAwardInfo.Guarantor, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@StudentAwardId", StudentAwardInfo.StudentAwardId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
        ' Then Insert/Update/Delete the detail records associated with the header
        If Not IsNothing(Details) Then
            InsertUpdateDeleteStudentAwardSchedule(StudentAwardInfo.StudentAwardId, Details)
        End If
    End Sub
    Public Sub InsertUpdateDeleteStudentAwardSchedule(ByVal StudentAwardId As Guid, ByVal Details As DataTable)
        'Get the current list of awardschedules
        ' Get the changes between the current and what the user modified
        Dim currentdt As DataTable
        Dim newdt As New DataTable
        currentdt = Details
        newdt = currentdt.GetChanges(DataRowState.Added Or DataRowState.Deleted Or DataRowState.Modified)
        Dim dr As DataRow
        If Not IsNothing(newdt) Then
            For Each dr In newdt.Rows
                Select Case dr.RowState
                    Case DataRowState.Added
                        Dim StudentAwardScheduleInfo As New StudentAwardScheduleInfo
                        StudentAwardScheduleInfo.StudentAwardScheduleId = XmlConvert.ToGuid(dr("AwardScheduleId").ToString)
                        StudentAwardScheduleInfo.StudentAwardId = StudentAwardId
                        StudentAwardScheduleInfo.ExpectedDate = CDate(dr("ExpectedDate"))
                        StudentAwardScheduleInfo.PaymentPeriod = dr("PaymentPeriod").ToString
                        StudentAwardScheduleInfo.Amount = CDbl(dr("Amount"))
                        AddStudentAwardSchedule(StudentAwardScheduleInfo)
                    Case DataRowState.Deleted
                        DeleteStudentAwardSchedule(XmlConvert.ToGuid(dr("AwardScheduleId").ToString))
                    Case DataRowState.Modified
                        Dim StudentAwardScheduleInfo As New StudentAwardScheduleInfo
                        StudentAwardScheduleInfo.StudentAwardScheduleId = XmlConvert.ToGuid(dr("AwardScheduleId").ToString)
                        StudentAwardScheduleInfo.StudentAwardId = StudentAwardId
                        StudentAwardScheduleInfo.ExpectedDate = CDate(dr("ExpectedDate"))
                        StudentAwardScheduleInfo.PaymentPeriod = dr("PaymentPeriod").ToString
                        StudentAwardScheduleInfo.Amount = CDbl(dr("Amount"))
                        UpdateStudentAwardSchedule(StudentAwardScheduleInfo)
                End Select
            Next
        End If
    End Sub
    Public Sub DeleteStudentAward(ByVal StudentAwardId As Guid)
        ' First Delete all of the detail records
        Dim ds2 As New DataSet
        ds2 = GetStudentAwardSchedule(StudentAwardId)
        Dim row As DataRow
        For Each row In ds2.Tables(0).Rows
            DeleteStudentAwardSchedule(XmlConvert.ToGuid(row("AwardScheduleId").ToString))
        Next
        ' Next delete the header record
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("DELETE FROM faStudentAwards WHERE StudentAwardId = ?")
        End With
        db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Sub
    Public Function GetCalculatedFields(ByVal StudentAwardId As Guid) As Double()
        Dim Totals(2) As Double
        Dim Payments As New Object
        Dim Refunds As New Object
        Payments = GetScalarPayments(StudentAwardId)
        Refunds = GetScalarRefunds(StudentAwardId)
        If IsDBNull(Payments) Then
            Totals(0) = 0
        Else
            Totals(0) = CDbl(Payments)
        End If
        If IsDBNull(Refunds) Then
            Totals(1) = 0
        Else
            Totals(1) = CDbl(Refunds)
        End If
        Return Totals
    End Function
    Public Function GetScalarPayments(ByVal StudentAwardId As Guid) As Object
        Dim totalpayments As Object
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("SELECT SUM(b.TransAmount) as TransactionType")
            .Append(" FROM saPayments a, saTransactions b")
            .Append(" WHERE a.StudentAwardId = ? AND")
            .Append(" a.TransactionId = b.TransactionId")
            .Append(" AND b.Voided=0 ")
        End With
        db.OpenConnection()
        db.AddParameter("@StudentAwardId1", StudentAwardId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            totalpayments = db.RunParamSQLScalar(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return totalpayments
    End Function
    Public Function GetScalarRefunds(ByVal StudentAwardId As Guid) As Object
        Dim totalrefunds As Object
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("SELECT SUM(b.TransAmount) as TransactionType")
            .Append(" FROM saRefunds a, saTransactions b")
            .Append(" WHERE a.StudentAwardId = ? AND")
            .Append(" a.TransactionId = b.TransactionId")
            .Append(" AND b.Voided=0 ")
        End With
        db.OpenConnection()
        db.AddParameter("@StudentAwardId1", StudentAwardId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            totalrefunds = db.RunParamSQLScalar(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return totalrefunds
    End Function
    Public Function GetAwardDetails(ByVal StudentAwardId As Guid) As DataSet
        ' We need to get all of the saPayments and saRefunds for this particular StudentAwardId
        ' Read the Refunds table and the payments table and merge the results together into one result set
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Dim strSQL As New StringBuilder

        With strSQL
            .Append(" SELECT a.transactionid, b.TransDate, b.TransReference, b.TransAmount, 'Payment' as TransactionType")
            .Append(" FROM saPayments a, saTransactions b")
            .Append(" WHERE a.StudentAwardId = ? and")
            .Append(" a.TransactionId = b.TransactionId")
            .Append(" union")
            .Append(" SELECT a.transactionid,b.TransDate, b.TransReference, b.TransAmount, 'Refund' as TransactionType")
            .Append(" FROM saRefunds a, saTransactions b")
            .Append(" WHERE a.StudentAwardId = ? and")
            .Append(" a.TransactionId = b.TransactionId")
            .Append(" AND b.Voided=0 ")

        End With
        db.OpenConnection()
        db.AddParameter("@StudentAwardId1", StudentAwardId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@StudentAwardId2", StudentAwardId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            da = db.RunParamSQLDataAdapter(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "TransactionList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try

        db.ClearParameters()

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        strSQL.Remove(0, strSQL.Length)

        ' Return the dataset
        Return ds
    End Function
    Public Sub AddStudentAwardSchedule(ByVal StudentAwardScheduleInfo As StudentAwardScheduleInfo)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("INSERT INTO faStudentAwardSchedule")
            .Append("(AwardScheduleId, StudentAwardId, ExpectedDate, PaymentPeriod, Amount) ")
            .Append("VALUES(?,?,?,?,?)")
        End With
        db.AddParameter("@StudentAwardScheduleId", StudentAwardScheduleInfo.StudentAwardScheduleId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@StudentAwardId", StudentAwardScheduleInfo.StudentAwardId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@ExpectedDate", StudentAwardScheduleInfo.ExpectedDate, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
        db.AddParameter("@PaymentPeriod", StudentAwardScheduleInfo.PaymentPeriod, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@Amount", StudentAwardScheduleInfo.Amount, DataAccess.OleDbDataType.OleDbDecimal, 9, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
    End Sub
    Public Sub UpdateStudentAwardSchedule(ByVal StudentAwardScheduleInfo As StudentAwardScheduleInfo)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("UPDATE faStudentAwardSchedule SET ")
            .Append("StudentAwardId = ?")
            .Append(",ExpectedDate = ?")
            .Append(",PaymentPeriod = ?")
            .Append(",Amount = ?")
            .Append(" WHERE AwardScheduleId = ? ")
        End With
        db.AddParameter("@StudentAwardId", StudentAwardScheduleInfo.StudentAwardId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@ExpectedDate", StudentAwardScheduleInfo.ExpectedDate, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
        db.AddParameter("@PaymentPeriod", StudentAwardScheduleInfo.PaymentPeriod, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@Amount", StudentAwardScheduleInfo.Amount, DataAccess.OleDbDataType.OleDbDecimal, 9, ParameterDirection.Input)
        db.AddParameter("@AwardScheduleId", StudentAwardScheduleInfo.StudentAwardScheduleId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
    End Sub
    Public Sub DeleteStudentAwardSchedule(ByVal AwardScheduleId As Guid)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("DELETE FROM faStudentAwardSchedule WHERE AwardScheduleId = ?")
        End With
        db.AddParameter("@AwardScheduleId", AwardScheduleId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
    End Sub
    Public Function GetStudentAwardSchedule(ByVal StudentAwardId As Guid) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder

        With strSQLString
            .Append("SELECT * ")
            .Append(" FROM faStudentAwardSchedule")
            .Append(" WHERE StudentAwardId = ? ")
        End With
        db.OpenConnection()
        db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "ScheduleList")
            Return ds
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        Finally

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        End Try
    End Function
    'Public Function PopulateDropDown() As DataSet
    '    Dim db As New DataAccess
    '    Dim ds As New DataSet
    '    Dim da As New OleDbDataAdapter
    '    Dim strSQLString As New StringBuilder

    '    With strSQLString
    '        .Append("SELECT * ")
    '        '.Append(" FROM faAwardTypes")
    '        'Modified by Michelle R. Rodriguez on 03/22/2005
    '        .Append(" FROM saFundSources")
    '    End With
    '    db.OpenConnection()
    '    Try
    '        da = db.RunParamSQLDataAdapter(strSQLString.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Try
    '        da.Fill(ds, "AwardTypeList")
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    db.ClearParameters()
    '    strSQLString.Remove(0, strSQLString.Length)

    '    Dim da2 As New OleDbDataAdapter

    '    With strSQLString
    '        .Append("SELECT * ")
    '        .Append(" FROM faAwardYears")
    '    End With
    '    db.OpenConnection()
    '    Try
    '        da2 = db.RunParamSQLDataAdapter(strSQLString.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Try
    '        da2.Fill(ds, "AwardYearList")
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Dim da3 As New OleDbDataAdapter

    '    db.ClearParameters()
    '    strSQLString.Remove(0, strSQLString.Length)
    '    With strSQLString
    '        .Append("SELECT * ")
    '        .Append(" FROM faLender")
    '        .Append(" WHERE IsLender = 1 ")
    '    End With
    '    db.OpenConnection()
    '    Try
    '        da3 = db.RunParamSQLDataAdapter(strSQLString.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Try
    '        da3.Fill(ds, "LenderList")
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Dim da4 As New OleDbDataAdapter

    '    db.ClearParameters()
    '    strSQLString.Remove(0, strSQLString.Length)
    '    With strSQLString
    '        .Append("SELECT * ")
    '        .Append(" FROM faLender")
    '        .Append(" WHERE IsServicer = 1 ")
    '    End With
    '    db.OpenConnection()
    '    Try
    '        da4 = db.RunParamSQLDataAdapter(strSQLString.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Try
    '        da4.Fill(ds, "ServicerList")
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Dim da5 As New OleDbDataAdapter

    '    db.ClearParameters()
    '    strSQLString.Remove(0, strSQLString.Length)
    '    With strSQLString
    '        .Append("SELECT * ")
    '        .Append(" FROM faLender")
    '        .Append(" WHERE IsGuarantor = 1 ")
    '    End With
    '    db.OpenConnection()
    '    Try
    '        da = db.RunParamSQLDataAdapter(strSQLString.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Try
    '        da.Fill(ds, "GuarantorList")

    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try

    '    If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    '    Return ds
    'End Function
    'Public Function PopulateDataList(ByVal StudentId As Guid) As DataSet
    '    Dim db As New DataAccess
    '    Dim ds As New DataSet
    '    Dim da As New OleDbDataAdapter
    '    Dim strSQLString As New StringBuilder

    '    With strSQLString
    '        .Append("SELECT * ")
    '        '.Append(" FROM faStudentAwards a, faAwardTypes b")
    '        '.Append(" WHERE a.AwardTypeId = b.AwardTypeId AND")
    '        'Modified by Michelle R. Rodriguez
    '        .Append(" FROM faStudentAwards a, saFundSources b")
    '        .Append(" WHERE a.AwardTypeId = b.FundSourceId AND")
    '        .Append(" a.StudentId = ?")
    '    End With
    '    db.OpenConnection()
    '    db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    Try
    '        da = db.RunParamSQLDataAdapter(strSQLString.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Try
    '        da.Fill(ds, "StudentAwardList")
    '        Return ds
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    Finally

    '        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    '    End Try
    'End Function
    Public Function GetLenderDropDowns() As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder

        With strSQLString
            .Append("SELECT StateId, StateCode ")
            .Append(" FROM syStates")
        End With
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "StateList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try

        db.ClearParameters()
        strSQLString.Remove(0, strSQLString.Length)

        With strSQLString
            .Append("SELECT CountryId, CountryDescrip ")
            .Append(" FROM syCountries")
        End With
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "CountryList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds

    End Function
    Public Function GetAllAwardTypes(ByVal showActiveOnly As Boolean) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            'Modified by Michelle R. Rodriguez on 03/22/2005
            .Append("SELECT   FS.FundSourceId, FS.StatusId, FS.FundSourceCode, FS.FundSourceDescrip ")
            .Append("FROM     saFundSources FS, syStatuses ST ")
            .Append("WHERE    FS.StatusId = ST.StatusId ")
            If showActiveOnly Then
                .Append(" AND     ST.Status = 'Active' ")
            End If
            .Append("ORDER BY FS.FundSourceDescrip ")

            '.Append("SELECT   AT.AwardTypeId, AT.StatusId, AT.AwardTypeCode, AT.AwardTypeDescrip ")
            '.Append("FROM     faAwardTypes AT, syStatuses ST ")
            '.Append("WHERE    AT.StatusId = ST.StatusId ")
            'If showActiveOnly Then
            '    .Append(" AND     ST.Status = 'Active' ")
            'End If
            '.Append("ORDER BY AT.AwardTypeDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllAwardsPerStudent(ByVal stuEnrollId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         SA.StudentAwardId, ")
            .Append("         SA.AwardTypeId, ")
            '.Append("         (Select AwardTypeDescrip from faAwardTypes where AwardTypeId=SA.AwardTypeId) + '/' + ")
            'Modified by Michelle R. Rodriguez on 03/22/2005
            .Append("         (Select FundSourceDescrip from saFundSources where FundSourceId=SA.AwardTypeId) + '/' + ")
            'Modified by Michelle R. Rodriguez on 03/25/2005
            '.Append("         (Select AwardYearDescrip from faAwardYears where AwardYearId=SA.AwardYearId) + '/$' + ")
            .Append("         (Select AcademicYearDescrip from saAcademicYears where AcademicYearId=SA.AcademicYearId) + '/$' + ")
            .Append("         Cast(SA.GrossAmount As Varchar(10)) As AwardDescrip ")
            .Append("FROM     faStudentAwards SA ")
            .Append("WHERE    SA.StuEnrollId= ? ")
            .Append("ORDER BY AwardDescrip ")
        End With

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    ''Added by Saraswathi lakshmanan on April 15 2010
    ''To convert to stored procedure

    Public Function GetAllAwardsPerStudent_Sp(ByVal stuEnrollId As String) As DataSet

        '   connect to the database
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   StuEnrollId
        db.AddParameter("@StuEnrollID", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet_SP("USP_SA_GetAllAwardsPerStudent")

    End Function
End Class
