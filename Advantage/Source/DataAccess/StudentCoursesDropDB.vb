﻿Imports FAME.Advantage.Common

Public Class StudentCoursesDropDB
#Region "Private Data Members"
    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")

#End Region
#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

#End Region
#Region "Public Methods"
    ''New Code Added By Vijay Ramteke On September 28, 2010 For Mantis Id 17685
    'Public Function GetDroppedCourses(ByVal paramInfo As ReportParamInfo, Optional ByVal StuEnrollID As String = "") As DataSet
    '    Dim sb As New System.Text.StringBuilder
    '    Dim db As New DataAccess
    '    Dim ds As New DataSet
    '    Dim strWhere As String
    '    Dim strOrderBy As String
    '    Dim strStuID As String


    '    If StuEnrollID <> "" Then
    '        If paramInfo.FilterOther <> "" Then
    '            strWhere &= " AND arStuEnrollments.StuEnrollId='" & StuEnrollID & "' AND " & paramInfo.FilterOther
    '        Else
    '            strWhere &= " AND arStuEnrollments.StuEnrollId='" & StuEnrollID & "'"
    '        End If
    '    Else
    '        If paramInfo.FilterOther <> "" Then
    '            strWhere &= " AND " & paramInfo.FilterOther
    '        End If
    '    End If

    '    If paramInfo.OrderBy <> "" Then
    '        strOrderBy &= "," & paramInfo.OrderBy
    '    End If


    '    If StudentIdentifier = "SSN" Then
    '        strStuID = "t6.SSN AS StudentIdentifier,"
    '    ElseIf StudentIdentifier = "EnrollmentId" Then
    '        strStuID = "t1.EnrollmentId AS StudentIdentifier,"
    '    ElseIf StudentIdentifier = "StudentId" Then
    '        strStuID = "t6.StudentNumber AS StudentIdentifier,"
    '    End If
    '    ' New code added on date May 22, 2009
    '    ' Added StatusCodeDescrip and tables syStatusCodes t7, syStatuses t8
    '    With sb
    '        .Append("SELECT distinct ")
    '        .Append("       t6.LastName,t6.FirstName, " & strStuID)
    '        .Append("       t6.StudentId,t5.PrgVerDescrip, ")
    '        .Append("       t1.StartDate,t1.LDA, ")
    '        .Append("       t1.DateDetermined,t4.CampDescrip, t7.StatusCodeDescrip ")
    '        .Append("       FROM arStuEnrollments t1,syCampGrps t2,syCmpGrpCmps t3, ")
    '        .Append("       syCampuses t4,arPrgVersions t5,arStudent t6 , syStatusCodes t7, syStatuses t8")
    '        .Append("       WHERE t3.CampusId = t1.CampusId ")
    '        .Append("       AND t2.CampGrpId = t3.CampGrpId ")
    '        .Append("       AND t4.CampusId = t1.CampusId ")
    '        .Append("       AND t5.PrgVerId=t1.PrgVerId ")
    '        .Append("       AND t1.StudentId=t6.StudentId ")
    '        .Append("       AND t7.StatusId=t8.StatusId AND SysStatusId in (12,19) AND t1.StatusCodeId=t7.StatusCodeId ")
    '        .Append(strWhere.ToLower().Replace("arstuenrollments", "t1"))
    '        .Append(" Order by t6.LastName ")
    '        .Append(strOrderBy)
    '    End With

    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '    db.OpenConnection()

    '    Try
    '        ds = db.RunParamSQLDataSet(sb.ToString)
    '        If ds.Tables.Count > 0 Then
    '            ds.Tables(0).TableName = "StudentDropCourses"
    '        End If
    '    Catch ex As System.Exception
    '    Finally
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)
    '    End Try

    '    Return ds
    'End Function
    Public Function GetDroppedCourses(ByVal paramInfo As ReportParamInfo, Optional ByVal StuEnrollID As String = "") As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String = ""
        Dim strOrderBy As String = ""
        Dim strStuID As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If StuEnrollID <> "" Then
            If paramInfo.FilterOther <> "" Then
                strWhere &= " AND arStuEnrollments.StuEnrollId='" & StuEnrollID & "' AND " & paramInfo.FilterList & " AND " & paramInfo.FilterOther
            Else
                '' New Code Added By Vijay Ramteke On November 04, 2010 For Rally Id DE1218
                strWhere &= " AND " & paramInfo.FilterList
                '' New Code Added By Vijay Ramteke On November 04, 2010 For Rally Id DE1218
            End If
        Else
            If paramInfo.FilterOther <> "" Then
                strWhere &= " AND " & paramInfo.FilterList & " AND " & paramInfo.FilterOther
            Else
                '' New Code Added By Vijay Ramteke On November 03, 2010 For Rally Id DE1218
                strWhere &= " AND " & paramInfo.FilterList
                '' New Code Added By Vijay Ramteke On November 03, 2010 For Rally Id DE1218
            End If
        End If


        If StudentIdentifier = "SSN" Then
            strStuID = "arStudent.SSN AS StudentIdentifier,"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStuID = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
        ElseIf StudentIdentifier = "StudentId" Then
            strStuID = "arStudent.StudentNumber AS StudentIdentifier,"
        End If
        ' New code added on date May 22, 2009
        ' Added StatusCodeDescrip and tables syStatusCodes t7, syStatuses t8
        With sb
            .Append("SELECT distinct ")
            .Append("       arStudent.LastName,arStudent.FirstName, " & strStuID)
            .Append("       arStudent.StudentId,arPrgVersions.PrgVerDescrip, ")
            .Append("       arStuEnrollments.StartDate,arStuEnrollments.LDA, ")
            .Append("       arStuEnrollments.DateDetermined,syCampuses.CampDescrip, syStatusCodes.StatusCodeDescrip ")
            '' New Code Added By Vijay Ramteke On September 28, 2010 For Mantis Id 17685
            .Append(" ,(Select Phone From arStudentPhone where arStudentPhone.StudentId=arStudent.StudentId and  arStudentPhone.default1=1) as Phone ")
            .Append(" ," & IIf(paramInfo.ShowLDA = True, "1", "0") & " as ShowLDA ")
            '' New Code Added By Vijay Ramteke On September 28, 2010 For Mantis Id 17685
            .Append("       FROM arStuEnrollments ,syCampGrps ,syCmpGrpCmps, ")
            .Append("       syCampuses,arPrgVersions,arStudent , syStatusCodes, syStatuses")
            .Append("       WHERE syCmpGrpCmps.CampusId = arStuEnrollments.CampusId ")
            .Append("       AND syCampGrps.CampGrpId = syCmpGrpCmps.CampGrpId ")
            .Append("       AND syCampuses.CampusId = arStuEnrollments.CampusId ")
            .Append("       AND arPrgVersions.PrgVerId=arStuEnrollments.PrgVerId ")
            .Append("       AND arStuEnrollments.StudentId=arStudent.StudentId ")
            .Append("       AND syStatusCodes.StatusId=syStatuses.StatusId AND SysStatusId in (12,19) AND arStuEnrollments.StatusCodeId=syStatusCodes.StatusCodeId ")
            '' New Code Added By Vijay Ramteke On November 03, 2010 For Rally Id DE1218
            If paramInfo.ShowTransferCampus = False And paramInfo.ShowTransferProgram = False Then
                ''.Append("       AND arStuEnrollments.StuEnrollId not in (Select Distinct StuEnrollId From arTrackTransfer) ")
                .Append(" And arStuEnrollments.DropReasonId is not null")
            ElseIf paramInfo.ShowTransferCampus = True And paramInfo.ShowTransferProgram = False Then
                ''.Append("       AND arStuEnrollments.StuEnrollId not in (Select Distinct StuEnrollId From arTrackTransfer where arTrackTransfer.SourcePrgVerId=arTrackTransfer.TargetPrgVerId) ")
                .Append("       AND arStuEnrollments.StuEnrollId not in (Select Distinct StuEnrollId From arTrackTransfer where arTrackTransfer.SourceCampusId=arTrackTransfer.TargetCampusId) ")
            ElseIf paramInfo.ShowTransferCampus = False And paramInfo.ShowTransferProgram = True Then
                ''.Append("       AND arStuEnrollments.StuEnrollId not in (Select Distinct StuEnrollId From arTrackTransfer where arTrackTransfer.SourceCampusId=arTrackTransfer.TargetCampusId) ")
                .Append("       AND arStuEnrollments.StuEnrollId not in (Select Distinct StuEnrollId From arTrackTransfer where arTrackTransfer.SourcePrgVerId=arTrackTransfer.TargetPrgVerId) ")
            End If
            '' New Code Added By Vijay Ramteke On November 03, 2010 For Rally Id DE1218
            .Append(strWhere.ToLower())
            .Append(" Order by arStudent.LastName ")
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        Try
            ds = db.RunParamSQLDataSet(sb.ToString)
            If ds.Tables.Count > 0 Then
                ds.Tables(0).TableName = "StudentDropCourses"
            End If
        Catch ex As System.Exception
            Throw ex
        Finally
            db.ClearParameters()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
            sb.Remove(0, sb.Length)
        End Try

        Return ds
    End Function
    ''New Code Added By Vijay Ramteke On September 28, 2010 For Mantis Id 17685
#End Region

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

End Class
