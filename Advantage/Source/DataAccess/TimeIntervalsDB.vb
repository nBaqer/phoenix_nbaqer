Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' AcademicYearsDB.vb
'
' AcademicYearsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2005 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class TimeIntervalsDB
    Public Function CreateTimeIntervalsTable(ByVal startHour As DateTime, ByVal endHour As DateTime, ByVal timeSpan As Integer, ByVal deleteAllExistingRecords As Boolean, ByVal user As String) As String

        '   connect to the database

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            '   build the sql query
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("       TimeIntervalId, ")
                .Append("       StatusId, ")
                .Append("       TimeIntervalDescrip, ")
                .Append("       ModDate, ")
                .Append("       ModUser ")
                .Append("FROM   cmTimeInterval ")
            End With

            '   build select command
            Dim timeIntervalsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle TimeIntervals table
            Dim timeIntervalsDataAdapter As New OleDbDataAdapter(timeIntervalsSelectCommand)

            '   build insert, update and delete commands for TimeIntrevals table
            Dim cb As New OleDbCommandBuilder(timeIntervalsDataAdapter)

            'fill TimeIntervalsDataTable
            Dim dt As New DataTable
            timeIntervalsDataAdapter.Fill(dt)

            'delete all previous existing rows
            If deleteAllExistingRecords Then
                For Each row As DataRow In dt.Rows
                    row.Delete()
                Next
            End If

            'populate the table with new values
            Dim current As DateTime = Date.Parse("1899-12-30 " + startHour.ToString("hh:mm tt"))
            endHour = Date.Parse("1899-12-30 " + endHour.ToString("hh:mm tt"))
            Dim now As DateTime = Date.Now
            now = now.Subtract(New TimeSpan(0, 0, 0, 0, now.Millisecond))
            While current <= endHour
                Dim newRow As DataRow = dt.NewRow()
                newRow("StatusId") = New Guid(CType(AdvantageCommonValues.ActiveGuid, String))
                newRow("TimeIntervalDescrip") = current
                newRow("ModDate") = now
                newRow("ModUser") = user
                dt.Rows.Add(newRow)
                current = current.AddMinutes(timeSpan)
            End While

            '   delete rows in TimeIntervals table
            timeIntervalsDataAdapter.Update(dt.Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   insert added rows in TimeIntervals table
            timeIntervalsDataAdapter.Update(dt.Select(Nothing, Nothing, DataViewRowState.Added))

            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            Return ""

        Catch ex As OleDb.OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally

            '   close connection
            connection.Close()
        End Try

    End Function
    Public Function GetAllTimeIntervals() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("	      (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("	      TI.TimeIntervalId, ")
            .Append("	      TI.StatusId, ")
            .Append("	      TI.TimeIntervalDescrip ")
            .Append("FROM     cmTimeInterval TI, syStatuses ST ")
            .Append("WHERE    TI.StatusId = ST.StatusId ")
            .Append("ORDER BY ST.Status,TI.TimeIntervalDescrip asc")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetTimeIntervalInfo(ByVal TimeIntervalId As String) As TimeIntervalInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT TI.TimeIntervalId, ")
            .Append("    TI.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=TI.StatusId) As Status, ")
            .Append("    TI.TimeIntervalDescrip, ")
            .Append("    (Select count(*) from arClsSectMeetings where TimeIntervalId = ? or EndIntervalId = ?) As IsBeingUsed, ")
            .Append("    TI.ModUser, ")
            .Append("    TI.ModDate ")
            .Append("FROM  cmTimeInterval TI ")
            .Append("WHERE TI.TimeIntervalId= ? ")
        End With

        ' Add the TimeIntervalId to the parameter list
        db.AddParameter("@TimeIntervalId", TimeIntervalId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add the TimeIntervalId to the parameter list
        db.AddParameter("@TimeIntervalId", TimeIntervalId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add the TimeIntervalId to the parameter list
        db.AddParameter("@TimeIntervalId", TimeIntervalId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim TimeIntervalInfo As New TimeIntervalInfo

        While dr.Read()

            '   set properties with data from DataReader
            With TimeIntervalInfo
                .TimeIntervalId = TimeIntervalId
                .IsInDB = True
                .Status = dr("Status")
                .TimeIntervalDescrip = dr("TimeIntervalDescrip")
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return TimeIntervalInfo
        Return TimeIntervalInfo

    End Function
    Public Function UpdateTimeIntervalInfo(ByVal TimeIntervalInfo As TimeIntervalInfo, ByVal user As String) As String


        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try

            '   TimeIntervalId
            db.AddParameter("@TimeIntervalId", New Guid(TimeIntervalInfo.TimeIntervalId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            '   TimeIntervalDescrip
            db.AddParameter("@TimeIntervalDescrip", TimeIntervalInfo.TimeIntervalDescrip, SqlDbType.DateTime, , ParameterDirection.Input)
            '   StatusId
            db.AddParameter("@StatusId", New Guid(TimeIntervalInfo.StatusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            '   ModUser
            db.AddParameter("@UserName", user, SqlDbType.VarChar, 50, ParameterDirection.Input)
            '   ModDate
            Dim strnow As Date = Utilities.GetAdvantageDBDateTime(Date.Now)
            db.AddParameter("@ModDate", strnow, SqlDbType.DateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_TimeInterval_Update")

            Return ""

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)


        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddTimeIntervalInfo(ByVal TimeIntervalInfo As TimeIntervalInfo, ByVal user As String) As String

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try

            '   TimeIntervalId
            db.AddParameter("@TimeIntervalId", New Guid(System.Guid.NewGuid.ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            '   TimeIntervalDescrip
            db.AddParameter("@TimeIntervalDescrip", TimeIntervalInfo.TimeIntervalDescrip, SqlDbType.DateTime, , ParameterDirection.Input)
            '   StatusId
            db.AddParameter("@StatusId", New Guid(TimeIntervalInfo.StatusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            '   ModUser
            db.AddParameter("@UserName", user, SqlDbType.VarChar, 50, ParameterDirection.Input)
            '   ModDate
            Dim strnow As Date = Utilities.GetAdvantageDBDateTime(Date.Now)
            db.AddParameter("@ModDate", strnow, SqlDbType.DateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_TimeInterval_Insert")

            '   return without errors
            Return ""

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteTimeIntervalInfo(ByVal TimeIntervalId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM cmTimeInterval ")
                .Append("WHERE TimeIntervalId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM cmTimeInterval WHERE TimeIntervalId = ? ")
            End With

            '   add parameters values to the query

            '   TimeIntervalId
            db.AddParameter("@TimeIntervalId", TimeIntervalId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TimeIntervalId
            db.AddParameter("@TimeIntervalId", TimeIntervalId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function DeleteTimeIntervalInfoMulti(ByVal TimeIntervalDescrip As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM cmTimeInterval ")
                .Append("WHERE TimeIntervalDescrip = ? ")
                .Append("SELECT count(*) FROM cmTimeInterval WHERE TimeIntervalDescrip = ? ")
            End With

            '   add parameters values to the query

            '   TimeIntervalId
            db.AddParameter("@TimeIntervalDescrip", TimeIntervalDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.AddParameter("@TimeIntervalDescrip", TimeIntervalDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
End Class

