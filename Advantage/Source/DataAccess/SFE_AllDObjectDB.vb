Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFE_AllDObjectDB

	Public Shared Function GetReportDataSetRaw(ByVal RptParamInfo As ReportParamInfoIPEDS) As DataSet
		' get list of students to include, based on report parameters
		Dim StudentList As String = IPEDSDB.GetStudentList(RptParamInfo)

		' if there are no students to include, return empty dataset
		If StudentList = "" Then
			Return New DataSet
		End If

		Dim sb As New System.Text.StringBuilder
		Dim dsRaw As New DataSet

		' Get list of students for report
		With sb
			.Append("SELECT DISTINCT arStudent.StudentId, ")

			' Retrieve Student Identifier
			.Append(IPEDSDB.GetSQL_StudentIdentifier & ", ")

			' retrieve all other needed columns
			.Append("arStudent.LastName, arStudent.FirstName, arStudent.MiddleName, ")
			.Append("(SELECT COUNT(*) ")
			.Append("	FROM arStuEnrollments A, arTransferGrades B ")
			.Append("	WHERE A.StudentId = arStudent.StudentId AND ")
			.Append("	      B.StuEnrollId = A.StuEnrollId) AS TransferGrades ")
			.Append("FROM arStudent ")

			' append list of appropriate students to include
			.Append("WHERE arStudent.StudentId IN (" & StudentList & ") ")

			' apply sort from report param info - either Student Identifier or Last Name
			.Append("ORDER BY " & IPEDSDB.GetSQL_StudentSort(RptParamInfo))

			' get list of enrollments and relevant info for same list of students
            .Append(";" & IPEDSDB.GetSQL_EnrollmentInfo(StudentList, RptParamInfo.FilterProgramIDs))
		End With

		' run query, add returned data to raw dataset
		dsRaw = IPEDSDB.DataAccessIPEDS().RunSQLDataSet(sb.ToString)

		' set table names for each part of returned data
		With dsRaw
			.Tables(0).TableName = TblNameStudents
			.Tables(1).TableName = TblnameEnrollments
		End With

		'return the raw dataset
		Return dsRaw

	End Function

End Class
