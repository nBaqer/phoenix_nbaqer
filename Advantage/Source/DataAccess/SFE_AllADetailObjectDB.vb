Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFE_AllADetailObjectDB

	Public Shared Function GetReportDatasetRaw(ByVal RptParamInfo As ReportParamInfoIPEDS) As DataSet

		' get list of students to include, based on report parameters, and using combined 
		'	list of all passed-in program ids 
        Dim StudentList As String, ProgIdString As String = ""

		With RptParamInfo
            If .FilterProgramIDsMult1 IsNot Nothing Then
                ProgIdString = .FilterProgramIDsMult1
            End If
			If .FilterProgramIDsMult2 IsNot Nothing Then
				If ProgIdString <> "" Then
					ProgIdString &= "," & .FilterProgramIDsMult2
				Else
					ProgIdString = .FilterProgramIDsMult2
				End If
			End If
			If .FilterProgramIDsMult3 IsNot Nothing Then
				If ProgIdString <> "" Then
					ProgIdString &= "," & .FilterProgramIDsMult3
				Else
					ProgIdString = .FilterProgramIDsMult3
				End If
			End If

			' if no programs of any type were passed in, return empty dataset
			If ProgIdString = "" Then
				Return New DataSet
			End If

			StudentList = IPEDSDB.GetStudentList(RptParamInfo, ProgIdString)
		End With

		' if there are no students to include, return empty dataset
		If StudentList = "" Then
			Return New DataSet
		End If

		Dim sb As New System.Text.StringBuilder
		Dim dsRaw As New DataSet

		With sb
			' get Students for report part 1
			.Append("SELECT DISTINCT arStudent.StudentId, ")

			' Retrieve Student Identifier
			.Append(IPEDSDB.GetSQL_StudentIdentifier & ", ")

			' retrieve all other needed columns
			.Append("arStudent.LastName, arStudent.FirstName, arStudent.MiddleName, ")
			.Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
			.Append("	FROM adGenders, ")
			.Append("		 syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
			.Append("	WHERE adGenders.GenderId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("	      syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("	      syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("	      syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("	      syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
			.Append("	      adGenders.GenderId = arStudent.Gender) AS GenderDescrip, ")
			.Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
			.Append("	FROM adEthCodes, ")
			.Append("		 syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
			.Append("	WHERE adEthCodes.EthCodeId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("	      syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("	      syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("	      syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("	      syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
			.Append("	      adEthCodes.EthCodeId = arStudent.Race) AS EthCodeDescrip ")
			.Append("FROM ")
			.Append("arStudent ")

			' append list of appropriate students to include
			.Append("WHERE arStudent.StudentId IN (" & StudentList & ") ")

			' apply primary sort on Gender and Race (for faster processing by Business Facade object)
			.Append("ORDER BY GenderDescrip, EthCodeDescrip, ")

			' apply secondary sort from report param info - either Student Identifier or Last Name
			.Append(IPEDSDB.GetSQL_StudentSort(RptParamInfo))

			' get list of enrollments and relevant info for same list of students
            .Append(";" & IPEDSDB.GetSQL_EnrollmentInfo(StudentList, ProgIdString))
		End With

		' run query, add returned data to raw dataset
		dsRaw = IPEDSDB.DataAccessIPEDS().RunSQLDataSet(sb.ToString)

		' set table names for each part of returned data to standard names
		With dsRaw
			.Tables(0).TableName = TblNameStudents
			.Tables(1).TableName = TblnameEnrollments
		End With

		'return the raw dataset
		Return dsRaw

	End Function

End Class