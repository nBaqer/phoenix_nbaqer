﻿Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common
Public Class MaintenanceBaseDB
    ''' <summary>
    ''' Store the connection string for this class
    ''' </summary>
    ''' <remarks></remarks>
    Private ReadOnly conString As String

    ''' <summary>
    ''' Application Setting for get some other different values from config.
    ''' </summary>
    ''' <remarks></remarks>                                                                                                             
    Private ReadOnly myAdvAppSettings As AdvAppSettings
    Sub New()
        myAdvAppSettings = AdvAppSettings.GetAppSettings()
        conString = myAdvAppSettings.AppSettings("ConString")
    End Sub
    Public Function BuildRadGrid(ByVal ResourceId As Integer, Optional ByVal StatusId As String = "") As DataSet
        Dim myDataSet As New DataSet()
        Dim mySqlDataAdapter As New SqlDataAdapter()
        myDataSet.Tables.Add("radGridDS")
        myDataSet.Tables.Add("Headers")
        Using myConnection As New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString())
            Using cmd1 As New SqlCommand("USP_Maintenance_GetDetails", myConnection)
                cmd1.CommandType = CommandType.StoredProcedure
                cmd1.Parameters.Add(New SqlParameter("@pResourceId", SqlDbType.Int)).Value = ResourceId
                cmd1.Parameters.Add(New SqlParameter("@Status", SqlDbType.VarChar)).Value = StatusId
                cmd1.Parameters.Add(New SqlParameter("@OnlyHeaders", SqlDbType.Bit)).Value = 0
                mySqlDataAdapter.SelectCommand = cmd1
                mySqlDataAdapter.Fill(myDataSet, "radGridDS")
            End Using

            Using cmd2 As New SqlCommand("USP_Maintenance_GetDetails", myConnection)
                cmd2.CommandType = CommandType.StoredProcedure
                cmd2.Parameters.Add(New SqlParameter("@pResourceId", SqlDbType.Int)).Value = ResourceId
                cmd2.Parameters.Add(New SqlParameter("@Status", SqlDbType.VarChar)).Value = StatusId
                cmd2.Parameters.Add(New SqlParameter("@OnlyHeaders", SqlDbType.Bit)).Value = 1           '    Only Headers
                mySqlDataAdapter.SelectCommand = cmd2
                mySqlDataAdapter.Fill(myDataSet, "Headers")
            End Using

        End Using
        mySqlDataAdapter.Dispose()
        Return myDataSet
    End Function

End Class
