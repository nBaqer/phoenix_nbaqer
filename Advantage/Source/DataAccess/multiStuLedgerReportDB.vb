Imports FAME.Advantage.Common

Public Class multiStuLedgerReportDB
#Region "Private Data Members"

    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")

#End Region


#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property
#End Region


    Public Function GetStudentLedger(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strWhere As String
        Dim strOrderBy As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Get ProgVerId and Where Clause from paramInfo.FilterList
        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        'Get StudentId and rest of Where Clause from paramInfo.FilterOther
        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        'Order By Clause.
        If paramInfo.OrderBy <> "" Then
            strOrderBy &= " ORDER BY " & paramInfo.OrderBy
        End If

        Dim strNewWhere As String = strWhere
        If (strWhere.ToUpper.IndexOf("TRANSDATE") > 0) Then
            strNewWhere = strWhere.Substring(0, strWhere.ToUpper.Substring(0, strWhere.ToUpper.IndexOf("TRANSDATE") - 1).LastIndexOf("AND"))
            strWhere = strWhere.Replace(strNewWhere, "")
        Else
            strWhere = ""
        End If


        ''Get student identifier depending on what field school is using.
        'If StudentIdentifier = "SSN" Then
        '    strStuID = "arStudent.SSN"
        'ElseIf StudentIdentifier = "EnrollmentId" Then
        '    strStuID = "arStuEnrollments.EnrollmentId"
        'ElseIf StudentIdentifier = "StudentId" Then
        '    strStuID = "arStudent.StudentNumber"
        'End If

        ''With sb
        ''    .Append("SELECT distinct arStudent.LastName,arStudent.FirstName,arStudent.MiddleName, ")
        ''    .Append("arStudent.StudentId,arStudent.SSN AS StudentIdentifier,arStuEnrollments.StuEnrollId, ")
        ''    .Append("upper(CampDescrip) as  CampDescrip ")
        ''    .Append(" FROM arStuEnrollments,arStudent,syCampuses,syCampGrps,arClassSections ")
        ''    .Append("WHERE arStudent.StudentId=arStuEnrollments.StudentId ")
        ''    .Append(" AND syCampuses.CampusId=arStuEnrollments.CampusId and syCampuses.CampusId='de596a18-c6f7-41c6-a018-3699267f61c4' ")
        ''    .Append(strWhere)
        ''End With

        With sb
            .Append("SELECT arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
            .Append("arStudent.StudentId,arStudent.SSN AS StudentIdentifier,arStuEnrollments.StuEnrollId, ")
            .Append("upper(CampDescrip) as  CampDescrip ")
            .Append(" FROM arStuEnrollments,arStudent,syCampuses,syCampGrps ")
            .Append("WHERE arStudent.StudentId=arStuEnrollments.StudentId ")
            .Append(" AND syCampuses.CampusId=arStuEnrollments.CampusId and syCampuses.CampusId='" & paramInfo.CampusId & "' ")
            .Append(strNewWhere)
            '.Append(strOrderBy)
        End With


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)
        Dim dr2 As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim ds2 As DataSet
        Dim i As Integer = 1
        Try
            'Dim dtStuInfo As DataTable = ds.Tables(0)
            'If ds.Tables.Count > 0 Then
            '    dtStuInfo.TableName = "MultipleStudentInformation"
            '    dtStuInfo.Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            '    dtStuInfo.Columns.Add(New DataColumn("Balance", System.Type.GetType("System.Decimal")))
            '    dtStuInfo.Columns.Add(New DataColumn("StuEnrollIdStr", System.Type.GetType("System.String")))
            '    'dtStuInfo.Columns.Add(New DataColumn("StudentIdentifier", System.Type.GetType("System.String")))

            Dim dtEnroll As DataTable = ds.Tables(0)
            If ds.Tables.Count > 0 Then
                dtEnroll.TableName = "MultipleStudentInformation"
                dtEnroll.Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
                dtEnroll.Columns.Add(New DataColumn("Balance", System.Type.GetType("System.Decimal")))
                dtEnroll.Columns.Add(New DataColumn("StuEnrollIdStr", System.Type.GetType("System.String")))
                'dtStuInfo.Columns.Add(New DataColumn("StudentIdentifier", System.Type.GetType("System.String")))

                Dim dt As New DataTable("MultipleStudentLedger")

                dt.Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
                'Added by Hepsiba for School Logo , Name , Filters.
                dt.Columns.Add(New DataColumn("SchoolLogo", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("SchoolName", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("ShowFilters", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Filters", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("StudentIdentifier", System.Type.GetType("System.String")))

                dt.Columns.Add(New DataColumn("TransactionId", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("StudentId", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("StuEnrollId", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("EnrollmentDescrip", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("TransCodeId", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("TransCodeDescrip", Type.GetType("System.String")))

                dt.Columns.Add(New DataColumn("TransDescrip", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("TransDate", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("TransAmount", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("TransReference", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("AcademicYearId", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("AcademicYearDescrip", Type.GetType("System.String")))

                dt.Columns.Add(New DataColumn("TermId", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("TermDescrip", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("TransTypeId", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("TransTypeDescrip", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("ModUser", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("ModDate", Type.GetType("System.String")))

                dt.Columns.Add(New DataColumn("Balance", Type.GetType("System.String")))
                ' Charge and Payment changed into string
                dt.Columns.Add(New DataColumn("Charge", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Payment", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("StuEnrollIdStr", System.Type.GetType("System.String")))


                dt.Columns.Add(New DataColumn("TotalBalance", Type.GetType("System.String")))

                ds.Tables.Add(dt)

            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        Finally
            If Not dr2.IsClosed Then dr2.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try

        Return ds
    End Function

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

End Class
