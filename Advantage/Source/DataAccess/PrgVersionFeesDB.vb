Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' PrgVersionFeesDB.vb
'
' PrgVersionFeesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class PrgVersionFeesDB
    Public Function GetAllPrgVersionFees(ByVal showActiveOnly As Boolean) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   PVF.PrgVerFeeId, ")
            .Append("         PVF.StatusId, ")
            .Append("         PVF.PrgVerId, ")
            .Append("         PVF.TransCodeId, ")
            .Append("         PVF.TuitionCategoryId, ")
            .Append("         PVF.Amount, ")
            .Append("         PVF.RateScheduleId, ")
            .Append("         PVF.UnitId ")
            .Append("FROM     saProgramVersionFees PVF, syStatuses ST ")
            .Append("WHERE    PVF.StatusId = ST.StatusId ")
            If showActiveOnly Then
                .Append(" AND     ST.Status = 'Active' ")
            End If
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetPrgVersionFeeInfo(ByVal PrgVerFeeId As String) As PrgVersionFeeInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT PVF.PrgVerFeeId, ")
            .Append("    PVF.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=PVF.StatusId) Status, ")
            .Append("    PVF.PrgVerId, ")
            .Append("    (Select PrgVerDescrip from arPrgVersions where PrgVerId=PVF.PrgVerId) ProgramVersion, ")
            .Append("    PVF.TransCodeId, ")
            .Append("    (Select TransCodeDescrip from saTransCodes where TransCodeId=PVF.TransCodeId) TransCode, ")
            .Append("    PVF.TuitionCategoryId, ")
            .Append("    (Select TuitionCategoryDescrip from saTuitionCategories where TuitionCategoryId=PVF.TuitionCategoryId) TuitionCategory, ")
            .Append("    PVF.Amount, ")
            .Append("    PVF.RateScheduleId, ")
            .Append("    (Select RateScheduleDescrip from saRateSchedules where RateScheduleId=PVF.RateScheduleId) RateSchedule, ")
            .Append("    PVF.UnitId, ")
            .Append("    PVF.ModUser, ")
            .Append("    PVF.ModDate ")
            .Append("FROM  saProgramVersionFees PVF ")
            .Append("WHERE PVF.PrgVerFeeId= ? ")
        End With

        ' Add the PrgVerFeeId to the parameter list
        db.AddParameter("@PrgVerFeeId", PrgVerFeeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim PrgVersionFeeInfo As New PrgVersionFeeInfo

        While dr.Read()

            '   set properties with data from DataReader
            With PrgVersionFeeInfo
                .PrgVerFeeId = PrgVerFeeId
                .IsInDB = True
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                If Not (dr("PrgVerId") Is System.DBNull.Value) Then .PrgVerId = CType(dr("PrgVerId"), Guid).ToString
                If Not (dr("ProgramVersion") Is System.DBNull.Value) Then .ProgramVersion = dr("ProgramVersion")
                If Not (dr("TransCodeId") Is System.DBNull.Value) Then .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                If Not (dr("TransCode") Is System.DBNull.Value) Then .TransCode = dr("TransCode")
                If Not (dr("TuitionCategoryId") Is System.DBNull.Value) Then .TuitionCategoryId = CType(dr("TuitionCategoryId"), Guid).ToString
                If Not (dr("TuitionCategory") Is System.DBNull.Value) Then .TuitionCategory = dr("TuitionCategory")
                .Amount = dr("Amount")
                If Not (dr("RateScheduleId") Is System.DBNull.Value) Then .RateScheduleId = CType(dr("RateScheduleId"), Guid).ToString
                If Not (dr("RateSchedule") Is System.DBNull.Value) Then .rateSchedule = dr("RateSchedule")
                .UnitId = CType(dr("UnitId"), UnitType)
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return PrgVersionFeeInfo
        Return PrgVersionFeeInfo

    End Function
    Public Function UpdatePrgVersionFeeInfo(ByVal PrgVersionFeeInfo As PrgVersionFeeInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saProgramVersionFees Set PrgVerFeeId = ?, ")
                .Append(" StatusId = ?, ")
                .Append(" PrgVerId = ?, TransCodeId = ?, TuitionCategoryId = ?, ")
                .Append(" Amount = ?, RateScheduleId = ?, UnitId = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE PrgVerFeeId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saProgramVersionFees where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   PrgVerFeeId
            db.AddParameter("@PrgVerFeeId", PrgVersionFeeInfo.PrgVerFeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", PrgVersionFeeInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   PrgVerId
            If PrgVersionFeeInfo.PrgVerId = Guid.Empty.ToString Then
                db.AddParameter("@PrgVerId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PrgVerId", PrgVersionFeeInfo.PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   TransCodeId
            If PrgVersionFeeInfo.TransCodeId = Guid.Empty.ToString Then
                db.AddParameter("@TransCodeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@TransCodeId", PrgVersionFeeInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   TuitionCategoryId
            If PrgVersionFeeInfo.TuitionCategoryId = Guid.Empty.ToString Then
                db.AddParameter("@TuitionCategoryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@TuitionCategoryId", PrgVersionFeeInfo.TuitionCategoryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Amount
            db.AddParameter("@Amount", PrgVersionFeeInfo.Amount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            '   RateScheduleId
            If PrgVersionFeeInfo.RateScheduleId = Guid.Empty.ToString Then
                db.AddParameter("@RateScheduleId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@RateScheduleId", PrgVersionFeeInfo.RateScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Unit
            db.AddParameter("@UnitId", PrgVersionFeeInfo.UnitId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   PrgVerFeeId
            db.AddParameter("@AdmDepositId", PrgVersionFeeInfo.PrgVerFeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", PrgVersionFeeInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddPrgVersionFeeInfo(ByVal PrgVersionFeeInfo As PrgVersionFeeInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saProgramVersionFees (PrgVerFeeId, StatusId, ")
                .Append("   PrgVerId, TransCodeId, TuitionCategoryId, ")
                .Append("   Amount, RateScheduleId, UnitId, ")
                .Append("   ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   PrgVerFeeId
            db.AddParameter("@PrgVerFeeId", PrgVersionFeeInfo.PrgVerFeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", PrgVersionFeeInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   PrgVerId
            If PrgVersionFeeInfo.PrgVerId = Guid.Empty.ToString Then
                db.AddParameter("@PrgVerId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PrgVerId", PrgVersionFeeInfo.PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   TransCodeId
            If PrgVersionFeeInfo.TransCodeId = Guid.Empty.ToString Then
                db.AddParameter("@TransCodeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@TransCodeId", PrgVersionFeeInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   TuitionCategoryId
            If PrgVersionFeeInfo.TuitionCategoryId = Guid.Empty.ToString Then
                db.AddParameter("@TuitionCategoryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@TuitionCategoryId", PrgVersionFeeInfo.TuitionCategoryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Amount
            db.AddParameter("@Amount", PrgVersionFeeInfo.Amount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            '   RateScheduleId
            If PrgVersionFeeInfo.RateScheduleId = Guid.Empty.ToString Then
                db.AddParameter("@RateScheduleId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@RateScheduleId", PrgVersionFeeInfo.RateScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Unit
            db.AddParameter("@UnitId", PrgVersionFeeInfo.UnitId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeletePrgVersionFeeInfo(ByVal PrgVerFeeId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saProgramVersionFees ")
                .Append("WHERE PrgVerFeeId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM saProgramVersionFees WHERE PrgVerFeeId = ? ")
            End With

            '   add parameters values to the query

            '   PrgVerFeeId
            db.AddParameter("@PrgVerFeeId", PrgVerFeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   PrgVerFeeId
            db.AddParameter("@PrgVerFeeId", PrgVerFeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetTransCodesByProgramVersion(ByVal courseId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   CF.PrgVerFeeId, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("         CF.PrgVerId, ")
            .Append("         (Select PrgVerDescrip from arPrgVersions where PrgVerId=CF.PrgVerId) As PrgVersion, ")
            .Append("         CF.TransCodeId, ")
            .Append("         (Select TransCodeDescrip from saTransCodes where TransCodeId=CF.TransCodeId) As TransCodeDescrip, ")
            .Append("         CF.TuitionCategoryId, ")
            .Append("         CF.Amount, ")
            .Append("         CF.RateScheduleId, ")
            .Append("         CF.UnitId ")
            .Append("FROM     saProgramVersionFees CF, syStatuses ST ")
            .Append("WHERE    CF.StatusId=ST.StatusId ")
            .Append("AND      CF.PrgVerId = ? ")
            .Append("ORDER BY TransCodeDescrip ")
        End With

        ' Add the courseId to the parameter list
        db.AddParameter("@CourseId", courseId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
End Class

