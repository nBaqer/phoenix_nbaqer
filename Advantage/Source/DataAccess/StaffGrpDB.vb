Imports FAME.Advantage.Common

Public Class StaffGrpDB
    Public Function GetAvailEmployees() As DataSet
        Dim db As New DataAccess
        Dim strSQL, strDescrip As String
        Dim ds As DataSet
        Dim dr As OleDbDataReader
        Dim strSQLString As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With strSQLString
            .Append("SELECT a.EmpId, a.LastName, a.FirstName ")
            .Append("FROM hrEmployees a ")
        End With
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        ds = db.RunSQLDataSet(strSQLString.ToString)

        'Close Connection
        db.CloseConnection()

        Return ds

    End Function

    Public Function GetEmployeesOnItemCmd(ByVal strGuid As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim da As OleDbDataAdapter
        Dim strSQL As String
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'Build query to obtain Campuses already assigned to the selected campus group
        strSQL = "SELECT t1.EmpId, t2.Code, t2.Role, t3.LastName, t3.FirstName  " & _
                 "FROM hrEmpRoles t1, syRoles t2, hrEmployees t3  " & _
                 "WHERE t1.RoleId = '" & strGuid & "'" & _
                 "AND t1.RoleId = t2.RoleId and t1.EmpId = t3.EmpId " & _
                 "ORDER BY t3.LastName"

        db.ConnectionString = "ConString"
        db.OpenConnection()
        da = db.RunParamSQLDataAdapter(strSQL)
        da.Fill(ds, "FldsSelectCamps")


        'Build query to obtain Campuses not yet assigned to the selected campus group       
        strSQL = "SELECT t1.EmpId,t1.LastName, t1.FirstName " & _
                 "FROM hrEmployees t1 " & _
                 "WHERE NOT EXISTS " & _
                 "(SELECT t2.EmpId FROM hrEmpRoles t2 WHERE t2.RoleId = '" & strGuid & "' AND t1.EmpId = t2.EmpId)" & _
                 "ORDER BY t1.LastName"

        da = db.RunParamSQLDataAdapter(strSQL)
        da.Fill(ds, "FldsAvailCamps")

        'Close Connection
        db.CloseConnection()

        Return ds
    End Function

    Public Function InsertStaffGrp(ByVal strGuid As String, ByVal Code As String, ByVal sStatusId As String, ByVal Description As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("INSERT INTO syRoles(RoleId,Code,StatusId,Descrip,Role) ")
            .Append("VALUES(?,?,?,?,?)")
        End With

        db.AddParameter("@campgrpid", strGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@campgrpcode", Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@statusid", sStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@campgrpdescrip", Description, DataAccess.OleDbDataType.OleDbString, 80, ParameterDirection.Input)
        db.AddParameter("@role", Description, DataAccess.OleDbDataType.OleDbString, 80, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

    End Function

    Public Function InsertStaffGrpEmps(ByVal strGuid As String, ByVal ID As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("INSERT INTO hrEmpRoles(RoleId,EmpId) ")
            .Append("VALUES(?,?)")
        End With

        db.AddParameter("@campgrpid", strGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@campusid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

    End Function

    Public Function UpdateStaffGrp(ByVal Code As String, ByVal sStatusId As String, ByVal Description As String, ByVal storedGuid As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("UPDATE syRoles ")
            .Append("SET Code=?,")
            .Append("StatusId=?,")
            .Append("Descrip=? ")
            .Append("WHERE RoleId =? ")
        End With
        db.AddParameter("@campgrpcode", Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@statusid", sStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@descrip", Description, DataAccess.OleDbDataType.OleDbString, 135, ParameterDirection.Input)
        db.AddParameter("@campgrpid", storedGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

    End Function

    Public Function UpdateRowAdded(ByVal storedGuid As String, ByVal iEmpId As String)

        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Insert row into syCampGrps table
        With sb
            .Append("INSERT INTO hrEmpRoles(RoleId,EmpId) ")
            .Append("VALUES(?,?)")
        End With

        db.AddParameter("@campgrpid", storedGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@campusid", iEmpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)
    End Function

    Public Function UpdateRowDeleted(ByVal storedGuid As String, ByVal iEmpId As String)

        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("DELETE FROM hrEmpRoles ")
            .Append("WHERE RoleId = ? ")
            .Append("AND EmpId = ? ")
        End With

        db.AddParameter("@campgrpid", storedGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@campusid", iEmpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

    End Function

    Public Function DeleteStaffGrp(ByVal sRoleId As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        With sb
            .Append("DELETE FROM hrEmpRoles ")
            .Append("WHERE RoleId =  ? ")
        End With
        db.AddParameter("@campgrpid", sRoleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        With sb
            .Append("DELETE FROM syRoles ")
            .Append("WHERE RoleId =  ? ")
        End With
        db.AddParameter("@campgrpid", sRoleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

    End Function

End Class
