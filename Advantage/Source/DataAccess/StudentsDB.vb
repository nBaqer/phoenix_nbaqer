' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' StudentsDB.vb
'
' StudentsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================

Imports FAME.Advantage.Common

#Region "Students DB"
Public Class StudentsDB

    Public Function GetAllEnrollmentsPerStudent(ByVal studentId As String) As DataSet
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append("SELECT SE.StuEnrollId,CONCAT(PV.PrgVerDescrip,'- ', ss.SysStatusDescrip, ' ',CONVERT(VARCHAR(10), se.StartDate, 101)) AS PrgVerDescrip,PV.PrgVerId  ")
            .Append(" FROM arStuEnrollments SE ")
            .Append(" JOIN arPrgVersions PV ON PV.PrgVerId = SE.PrgVerId ")
            .Append(" JOIN syStatusCodes SC ON SC.StatusCodeId = SE.StatusCodeId ")
            .Append(" JOIN dbo.sySysStatus SS ON SS.SysStatusId = SC.SysStatusId ")
            .Append(" WHERE    SE.StudentId = ? ")
            .Append(" ORDER BY SE.StartDate DESC, SE.ExpGradDate DESC")
        End With
        db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllStudents(ByVal showActiveOnly As Boolean) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   S.StudentId, (S.LastName + ', ' + S.FirstName + ' ' + Coalesce(S.MiddleName, ' ')) As StudentName ")
            .Append("FROM     arStudent S ")
            '.Append("WHERE ")
            'If showActiveOnly then
            '.Append("AND      S.StatusId = ST.StatusId ")
            '.Append(" AND     ST.Status = 'Active' ")
            'End If
            .Append("ORDER BY StudentName ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllTermsPerStudent(ByVal studentId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("   T.TermId, ")
            .Append("   (Select TermDescrip from arTerm where TermId=T.TermId) As TermDescrip ")
            .Append("FROM     saTransactions T ")
            .Append("WHERE    T.StuEnrollId IN (Select StuEnrollId from arStuEnrollments where StudentId = ?) ")
            .Append("AND      T.TermId is not null ")
            .Append("AND      T.Voided=0 ")
        End With

        '   add parameters values to the query

        ' Add the StudentId to the parameter list
        db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetTermsPerStudentEnrollment(ByVal stuEnrollId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("   T.TermId, ")
            .Append("   (Select TermDescrip from arTerm where TermId=T.TermId) As TermDescrip ")
            .Append("FROM     saTransactions T ")
            .Append("WHERE    T.StuEnrollId = ? ")
            .Append("AND      T.TermId is not null ")
            .Append("AND      T.Voided=0 ")
        End With

        '   add parameters values to the query

        ' Add the StudentId to the parameter list
        db.AddParameter("@StudentId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllProgramVersions(ByVal showActiveOnly As Boolean) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   PV.PrgVerId, PV.PrgVerDescrip ")
            .Append("FROM     arPrgVersions PV, syStatuses ST ")
            .Append("WHERE    ")
            .Append("         PV.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY PV.PrgVerDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function

    Public Function GetAllTermsForStudentEnrollment(ByVal stuEnrollid As String, ByVal campusid As String)
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" Select DISTINCT 		T.TermDescrip, T.TermId from ")
            .Append(" arStuEnrollments SE,  arPrgVersions PV ,arTerm T  WHERE  SE.PrgVerId=PV.PrgVerId ")
            .Append(" and (PV.ProgId=T.ProgId or T.ProgId is null) AND T.StartDate >= SE.ExpStartDate ")
            .Append(" and SE.StuEnrollId= ? ")
            .Append(" AND T.CampGrpId IN ( ")
            .Append(" SELECT Distinct CampGrpId FROM syCmpGrpCmps WHERE CampusId = ? ")
            .Append(" AND CampGrpId not in (SELECT Distinct CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL') ")
            .Append(" Union  SELECT Distinct CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL') ")
        End With

        '   add parameters values to the query

        ' Add the StudentId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusid", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

End Class
#End Region

#Region "Formally in StudentDB.vb"
Public Class StudentDB
    'Public Function GetAllDocuments() As DataSet
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder

    '    With sb
    '        .Append(" Select SG.DocumentDescrip,SG.DocumentId ")
    '        .Append(" from cmDocuments SG,syStatuses ST ")
    '        .Append(" where SG.StatusId = St.StatusId and ST.Status = 'Active' ")
    '        .Append("Order By SG.DocumentDescrip ")
    '    End With

    '    'return dataset
    '    Return db.RunSQLDataSet(sb.ToString)
    'End Function
    Public Function GetAllReqTypes() As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select adReqTypeId,Descrip ")
            .Append(" from adReqTypes ")
            .Append(" order By Descrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllDocuments1(ByVal ModuleID As Integer) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select Distinct SG.DocumentDescrip,SG.DocumentId ")
            .Append(" from adReqs SG,syStatuses ST ")
            .Append(" where SG.StatusId = St.StatusId and ST.Status = 'Active' ")
            If Not ModuleID = 0 Then
                .Append(" and SG.ModuleID = ? ")
            End If
            .Append("Order By SG.DocumentDescrip ")
        End With
        If Not ModuleID = 0 Then
            db.AddParameter("@ModuleID", ModuleID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        End If
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllDocumentStatus(ByVal CampusId As String, Optional ByVal StudentDocId As String = "") As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


        Dim sb As New StringBuilder
        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)

        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        With sb
            If Not StudentDocId = "" Then
                .Append(" select Distinct SD.DocStatusId as DocStatusId,'(X) ' + R.DocStatusDescrip as DocStatusDescrip ")
                .Append(" from ")
                .Append(" plStudentDocs  SD,syDocStatuses R ")
                .Append(" WHERE SD.DocStatusId = R.DocStatusId AND SD.StudentDocId='" & StudentDocId & "' ")
                .Append(" AND ")
                .Append(" SD.DocStatusId NOT IN  ")
                .Append(" (Select Distinct SG.DocStatusId  ")
                .Append(" from syDocStatuses SG,syStatuses ST  ")
                .Append(" where SG.StatusId = St.StatusId and ST.Status = 'Active' ")
                .Append("AND SG.CampGrpId in ('")
                .Append(strCampGrpId)
                .Append(")) ")
                .Append(" Union ")
            End If
            .Append(" Select Distinct SG.DocStatusId as DocStatusId,SG.DocStatusDescrip as DocStatusDescrip ")
            .Append(" from syDocStatuses SG,syStatuses ST  ")
            .Append(" where SG.StatusId = St.StatusId and ST.Status = 'Active' ")
            .Append("AND SG.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" Order By DocStatusDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllExistingDocumentStatus(ByVal StudentDocumentId As String, ByVal CampusId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)

        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        With sb
            .Append(" select Distinct SD.DocStatusId as DocStatusId,'(X) ' + R.DocStatusDescrip as DocStatusDescrip ")
            .Append(" from ")
            .Append(" plStudentDocs  SD,syDocStatuses R ")
            .Append(" WHERE SD.DocStatusId = R.DocStatusId AND SD.StudentDocId='" & StudentDocumentId & "' ")
            .Append("  AND SD.DocStatusId not in (Select Distinct DocStatusId from syDocStatuses WHERE StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ")
            .Append("  CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(")) ")
            .Append(" Union ")
            .Append(" Select SG.DocStatusId as DocStatusId, SG.DocStatusDescrip as DocStatusDescrip ")
            .Append(" from syDocStatuses SG,syStatuses ST  ")
            .Append(" where SG.StatusId = St.StatusId and ST.Status = 'Active' ")
            .Append(" AND SG.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" Order By DocStatusDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllLeadExistingDocumentStatus(ByVal LeadDocumentId As String, ByVal CampusId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)

        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        With sb
            .Append(" select Distinct SD.DocStatusId as DocStatusId,'(X) ' + R.DocStatusDescrip as DocStatusDescrip ")
            .Append(" from ")
            .Append(" adLeadDocsReceived  SD,syDocStatuses R ")
            .Append(" WHERE SD.DocStatusId = R.DocStatusId AND SD.LeadDocId='" & LeadDocumentId & "' ")
            .Append("  AND SD.DocStatusId not in (Select Distinct DocStatusId from syDocStatuses WHERE StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ")
            .Append("  CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(")) ")
            .Append(" Union ")
            .Append(" Select Distinct SG.DocStatusId as DocStatusId, SG.DocStatusDescrip as DocStatusDescrip ")
            .Append(" from syDocStatuses SG,syStatuses ST  ")
            .Append(" where SG.StatusId = St.StatusId and ST.Status = 'Active' ")
            If Not strCampGrpId = "" Then
                .Append(" AND SG.CampGrpId in ('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" Order By DocStatusDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function DeleteFile(ByVal studentDocId As String)
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" update plStudentDocs set img_data=NULL,img_ContentType=NULL where StudentDocId=?")
        End With
        db.AddParameter("@StudentDocId", studentDocId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'return dataset
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
    End Function
    Public Function GetAllDocumentModules() As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select SG.ModuleName,SG.ModuleId ")
            .Append(" from syModules SG where ModuleID not in (5,8)") ''i.e. System and HR Modules need not be shown for Admission requirements

            .Append(" Order By SG.ModuleName ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function

    'Public Function AddStudentDocs(ByVal StudentDocsInfo As StudentDocsInfo, ByVal user As String, ByVal ModuleID As Integer, ByVal imagedata() As Byte, ByVal imagetype As String) As String

    '    ''Connect To The Database
    '    Dim db As New DataAccess
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '    'Do an Update
    '    Try
    '        '    'Build The Query
    '        Dim sb As New StringBuilder
    '        With sb
    '            .Append("Insert plStudentDocs(StudentDocId,StudentId,DocumentId, ")
    '            .Append(" DocStatusId,RequestDate,ReceiveDate, ")
    '            .Append(" ScannedId, ModUser, ModDate,ModuleID) ")
    '            .Append(" Values(?,?,?,?,?,?,?,?,?,?) ")
    '        End With


    '        'Get StudentDocsId
    '        db.AddParameter("@StudentDocId", StudentDocsInfo.StudentDocsId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'StudentId
    '        db.AddParameter("@StudentId", StudentDocsInfo.StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'DocumentId
    '        If StudentDocsInfo.DocumentId = Guid.Empty.ToString Or StudentDocsInfo.DocumentId = "" Then
    '            db.AddParameter("@DocumentId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@DocumentId", StudentDocsInfo.DocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'DocStatusId
    '        If StudentDocsInfo.DocStatusId = Guid.Empty.ToString Or StudentDocsInfo.DocStatusId = "" Then
    '            db.AddParameter("@DocStatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@DocStatusId", StudentDocsInfo.DocStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'RequestDate
    '        If StudentDocsInfo.RequestDate = "" Then
    '            db.AddParameter("@RequestDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@RequestDate", StudentDocsInfo.RequestDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'ReceivedDate
    '        If StudentDocsInfo.ReceiveDate = "" Then
    '            db.AddParameter("@ReceiveDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@ReceiveDate", StudentDocsInfo.ReceiveDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If


    '        If StudentDocsInfo.ScannedId = "True" Then
    '            StudentDocsInfo.ScannedId = 1
    '        Else
    '            StudentDocsInfo.ScannedId = 0
    '        End If

    '        'ScannedId
    '        db.AddParameter("@ScannedId", StudentDocsInfo.ScannedId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)

    '        'ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'ModDate
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        'ModuleID
    '        db.AddParameter("@ModuleId", ModuleID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)


    '        'Execute The Query
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString)

    '        StudentDocsInfo.IsInDb = True


    '        If StudentDocsInfo.ScannedId = 1 Then
    '            'Find the Position of first semi colon
    '            Dim intconnString As Integer = InStr(SingletonAppSettings.AppSettings("ConString"), ";")

    '            'Modify the connectionstring
    '            Dim connString As String = Mid(SingletonAppSettings.AppSettings("ConString"), intconnString + 1)

    '            Dim sqlConnection As New sqlConnection(connString) 'Replace your_sql_connection with your connection 
    '            Dim sqlCommand As sqlCommand

    '            Dim strCom As String = "update plStudentDocs  set Img_data=@varImage,Img_contenttype=@varImageType where StudentDocId=@StudentDocId"
    '            sqlCommand = New sqlCommand(strCom, sqlConnection)

    '            'Image sqlparameter 
    '            Dim sqlParam1 As New SqlParameter
    '            With sqlParam1
    '                .ParameterName = "@varImage"
    '                .Value = imagedata
    '                .SqlDbType = SqlDbType.Image
    '            End With

    '            'ImageType sqlparameter 
    '            Dim sqlParam2 As New SqlParameter
    '            With sqlParam2
    '                .ParameterName = "@varImageType"
    '                .Value = imagetype
    '                .SqlDbType = SqlDbType.VarChar
    '            End With

    '            'StudentDocId sqlparameter 
    '            Dim sqlParam3 As New SqlParameter
    '            With sqlParam3
    '                .ParameterName = "@StudentDocId"
    '                .Value = StudentDocsInfo.StudentDocsId
    '                .SqlDbType = SqlDbType.VarChar
    '            End With

    '            'Add parameters to sqlcommand 
    '            sqlCommand.Parameters.Add(sqlParam1)
    '            sqlCommand.Parameters.Add(sqlParam2)
    '            sqlCommand.Parameters.Add(sqlParam3)

    '            sqlConnection.Open() 'Open sqlconnection 
    '            sqlCommand.ExecuteNonQuery() 'Insert the images 
    '            sqlConnection.Close() 'Close sqlconnection

    '        End If

    '        Return ""
    '        'Catch ex As System.Exception
    '        '    'Return an Error To Client
    '        '    Return -1
    '    Catch ex As OleDbException
    '        '   return an error to the client
    '        Return ex.Message
    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    'End Function
    Public Function CheckIfDocumentStatusMapsToApprovedStatus(ByVal DocStatusId As String) As Integer
        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'Do an Update
        '    'Build The Query
        Dim sb As New StringBuilder
        Dim intRecordCount As Integer = 0
        With sb
            .Append(" Select Count(*) from syDocStatuses where DocStatusId=? and SysDocStatusId=1 ")
        End With
        db.AddParameter("@DocStatusId", DocStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            intRecordCount = db.RunParamSQLScalar(sb.ToString)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try
        Return intRecordCount
    End Function
    Public Function AddStudentDocs(ByVal StudentDocsInfo As StudentDocsInfo, ByVal user As String, ByVal ModuleID As Integer) As String

        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'Do an Update
        Try
            '    'Build The Query
            Dim sb As New StringBuilder
            Dim docexistmsg As String = ""
            Dim intdupcnt As Int16 = 0
            With sb
                .Append("select COUNT(*) ")
                .Append(" FROM dbo.plStudentDocs WHERE StudentId= ? ")
                .Append(" AND DocumentId=? ")
                .Append(" AND ModuleID= ? ")
                .Append(" GROUP BY StudentId , DocumentId ,  ModuleID ")

                db.AddParameter("@StudentId", StudentDocsInfo.StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'DocumentId
                If StudentDocsInfo.DocumentId = Guid.Empty.ToString Or StudentDocsInfo.DocumentId = "" Then
                    db.AddParameter("@DocumentId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@DocumentId", StudentDocsInfo.DocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                'ModuleID
                db.AddParameter("@ModuleId", ModuleID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                'Dim drcount As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                intdupcnt = db.RunParamSQLScalar(sb.ToString)
                If intdupcnt > 0 Then
                    docexistmsg = "Cannot insert the document since it already exists for the student"
                End If
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            End With


            If docexistmsg = "" Then
                With sb
                    ''New Code Added By Vijay Ramteke On September 09, 2010 For Mantis Id 14922
                    ''.Append("Insert plStudentDocs(StudentDocId,StudentId,DocumentId, ")
                    .Append("Insert plStudentDocs(StudentId,DocumentId, ")
                    ''New Code Added By Vijay Ramteke On September 09, 2010 For Mantis Id 14922
                    .Append(" DocStatusId,RequestDate,ReceiveDate, ")
                    .Append(" ScannedId, ModUser, ModDate,ModuleID ")

                    ' commented by balaji on 10/8/08  and will be uncommented for regent
                    If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                        .Append(",trackcode,DueDate,NotifiedDate,CompletedDate,Notificationcode,Description")
                    End If
                    .Append(")")
                    ''New Code Added By Vijay Ramteke On September 09, 2010 For Mantis Id 14922
                    ''.Append(" Values(?,?,?,?,?,?,?,?,?,? ")
                    .Append(" Values(?,?,?,?,?,?,?,?,? ")
                    ''New Code Added By Vijay Ramteke On September 09, 2010 For Mantis Id 14922
                    If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                        .Append(",?,?,?,?,?,? ")
                    End If
                    .Append(")")
                End With


                ''New Code Added By Vijay Ramteke On September 09, 2010 For Mantis Id 14922
                'Get StudentDocsId
                ''db.AddParameter("@StudentDocId", StudentDocsInfo.StudentDocsId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Output)
                ''New Code Added By Vijay Ramteke On September 09, 2010 For Mantis Id 14922


                'StudentId
                db.AddParameter("@StudentId", StudentDocsInfo.StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'DocumentId
                If StudentDocsInfo.DocumentId = Guid.Empty.ToString Or StudentDocsInfo.DocumentId = "" Then
                    db.AddParameter("@DocumentId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@DocumentId", StudentDocsInfo.DocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                'DocStatusId
                If StudentDocsInfo.DocStatusId = Guid.Empty.ToString Or StudentDocsInfo.DocStatusId = "" Then
                    db.AddParameter("@DocStatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@DocStatusId", StudentDocsInfo.DocStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                'RequestDate
                If StudentDocsInfo.RequestDate = "" Then
                    db.AddParameter("@RequestDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@RequestDate", StudentDocsInfo.RequestDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                'ReceivedDate
                If StudentDocsInfo.ReceiveDate = "" Then
                    db.AddParameter("@ReceiveDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ReceiveDate", StudentDocsInfo.ReceiveDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If


                If StudentDocsInfo.ScannedId = "True" Then
                    StudentDocsInfo.ScannedId = 1
                Else
                    StudentDocsInfo.ScannedId = 0
                End If

                'ScannedId
                db.AddParameter("@ScannedId", StudentDocsInfo.ScannedId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)

                'ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'ModDate
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                'ModuleID
                db.AddParameter("@ModuleId", ModuleID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    If StudentDocsInfo.TrackCode = "" Then
                        db.AddParameter("@TrackCode", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    Else
                        db.AddParameter("@TrackCode", StudentDocsInfo.TrackCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    End If
                    If StudentDocsInfo.DueDate = "" Then
                        db.AddParameter("@DueDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    Else
                        db.AddParameter("@DueDate", StudentDocsInfo.DueDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    End If
                    If StudentDocsInfo.TransDate = "" Then
                        db.AddParameter("@TransDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    Else
                        db.AddParameter("@TransDate", StudentDocsInfo.TransDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    End If
                    If StudentDocsInfo.CompletedDate = "" Then
                        db.AddParameter("@CompletedDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    Else
                        db.AddParameter("@CompletedDate", StudentDocsInfo.CompletedDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    End If
                    If StudentDocsInfo.NotificationCode = "" Then
                        db.AddParameter("@NotificationCode", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    Else
                        db.AddParameter("@NotificationCode", StudentDocsInfo.NotificationCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    End If
                End If
                'Execute The Query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)

                With sb
                    .Append("delete from adEntrTestOverRide where StudentId=? and EntrTestId=? ")
                End With
                db.AddParameter("@StudentId", StudentDocsInfo.StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@EntrTestId", StudentDocsInfo.DocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)


                Dim sb6 As New StringBuilder
                With sb6
                    'With subqueries
                    .Append(" Select LeadId from arStuEnrollments where StudentId=? ")
                End With
                db.AddParameter("@StudentId", StudentDocsInfo.DocumentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'Execute the query
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb6.ToString)
                Dim strLeadId As String
                While dr.Read()
                    If Not (dr("LeadId") Is System.DBNull.Value) Then strLeadId = CType(dr("LeadId"), Guid).ToString Else strLeadId = ""
                End While

                If Not dr.IsClosed Then dr.Close()
                
                db.ClearParameters()
                sb6.Remove(0, sb6.Length)

                Dim intOverride As Integer
                If StudentDocsInfo.Override = True Then
                    intOverride = 1
                Else
                    intOverride = 0
                End If

                'check if document status maps to approved status, if so then uncheck override checkbox
                Dim intRecordCount As Integer = 0
                If StudentDocsInfo.DocStatusId = Guid.Empty.ToString Or StudentDocsInfo.DocStatusId = "" Then
                Else
                    intRecordCount = CheckIfDocumentStatusMapsToApprovedStatus(StudentDocsInfo.DocStatusId)
                    If intRecordCount >= 1 Then 'means the status maps to approved status
                        intOverride = 0 ' reset the override checkbox to 0
                    End If
                End If

                With sb
                    .Append(" Insert into adEntrTestOverRide(EntrTestOverRideId,LeadId,StudentId,EntrTestId,OverRide,ModUser,ModDate) ")
                    .Append(" values(?,?,?,?,?,?,?) ")
                End With
                db.AddParameter("@LeadEntrTestId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If Not strLeadId = "" Then
                    db.AddParameter("@LeadId", strLeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@LeadId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                db.AddParameter("@StudentId", StudentDocsInfo.StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@EntrTestId", StudentDocsInfo.DocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@OverRide", intOverride, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                'execute query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)


                StudentDocsInfo.IsInDb = True

                'Insert data into regent xml 
                If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    Dim regDB As New regentDB
                    Dim strFilename As String = AdvantageCommonValues.getStudentBatchFileName()
                    regDB.AddTrackingXML(StudentDocsInfo.StudentId, strFilename, StudentDocsInfo.StudentDocsId)
                End If

                Return ""
                'Catch ex As System.Exception
                '    'Return an Error To Client
                '    Return -1
            Else
                Return docexistmsg
            End If
        Catch ex As OleDbException
            '   return an error to the client
            Return ex.Message
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    'Public Function UpdateStudentDocs(ByVal StudentDocsInfo As StudentDocsInfo, ByVal user As String, ByVal StudentDocsId As String, ByVal ModuleID As Integer, ByVal imagedata() As Byte, ByVal imagetype As String) As String

    '    ''Connect To The Database
    '    Dim db As New DataAccess
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '    'Do an Update
    '    Try
    '        '    'Build The Query
    '        Dim sb As New StringBuilder
    '        With sb
    '            .Append("Update plStudentDocs set StudentId=?,DocumentId=?, ")
    '            .Append(" DocStatusId=?,RequestDate=?,ReceiveDate=?, ")
    '            .Append(" ScannedId=?, ModUser=?, ModDate=?,ModuleID=? ")
    '            .Append(" Where StudentDocId = ? ")
    '        End With

    '        'StudentId
    '        db.AddParameter("@StudentId", StudentDocsInfo.StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'DocumentId
    '        If StudentDocsInfo.DocumentId = Guid.Empty.ToString Or StudentDocsInfo.DocumentId = "" Then
    '            db.AddParameter("@DocumentId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@DocumentId", StudentDocsInfo.DocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'DocStatusId
    '        If StudentDocsInfo.DocStatusId = Guid.Empty.ToString Or StudentDocsInfo.DocStatusId = "" Then
    '            db.AddParameter("@DocStatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@DocStatusId", StudentDocsInfo.DocStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'RequestDate
    '        If StudentDocsInfo.RequestDate = "" Then
    '            db.AddParameter("@RequestDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@RequestDate", StudentDocsInfo.RequestDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'ReceivedDate
    '        If StudentDocsInfo.ReceiveDate = "" Then
    '            db.AddParameter("@ReceiveDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@ReceiveDate", StudentDocsInfo.ReceiveDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        If StudentDocsInfo.ScannedId = "True" Then
    '            StudentDocsInfo.ScannedId = 1
    '        Else
    '            StudentDocsInfo.ScannedId = 0
    '        End If

    '        'ScannedId
    '        db.AddParameter("@scannedid", StudentDocsInfo.ScannedId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

    '        'ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'ModDate
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        'ModuleId
    '        db.AddParameter("@ModuleId", ModuleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        'StudentDocId
    '        db.AddParameter("@StudentDocId", StudentDocsId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        'Execute The Query
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString)

    '        StudentDocsInfo.IsInDb = True

    '        If imagetype.Length > 2 Then
    '            'Find the Position of first semi colon
    '            Dim intconnString As Integer = InStr(SingletonAppSettings.AppSettings("ConString"), ";")

    '            'Modify the connectionstring
    '            Dim connString As String = Mid(SingletonAppSettings.AppSettings("ConString"), intconnString + 1)

    '            Dim sqlConnection As New sqlConnection(connString) 'Replace your_sql_connection with your connection 
    '            Dim sqlCommand As sqlCommand

    '            Dim strCom As String = "update plStudentDocs  set Img_data=@varImage,Img_contenttype=@varImageType where StudentDocId=@StudentDocId"
    '            sqlCommand = New sqlCommand(strCom, sqlConnection)

    '            'Image sqlparameter 
    '            Dim sqlParam1 As New SqlParameter
    '            With sqlParam1
    '                .ParameterName = "@varImage"
    '                .Value = imagedata
    '                .SqlDbType = SqlDbType.Image
    '            End With

    '            'ImageType sqlparameter 
    '            Dim sqlParam2 As New SqlParameter
    '            With sqlParam2
    '                .ParameterName = "@varImageType"
    '                .Value = imagetype
    '                .SqlDbType = SqlDbType.VarChar
    '            End With

    '            'StudentDocId sqlparameter 
    '            Dim sqlParam3 As New SqlParameter
    '            With sqlParam3
    '                .ParameterName = "@StudentDocId"
    '                .Value = StudentDocsId
    '                .SqlDbType = SqlDbType.VarChar
    '            End With

    '            'Add parameters to sqlcommand 
    '            sqlCommand.Parameters.Add(sqlParam1)
    '            sqlCommand.Parameters.Add(sqlParam2)
    '            sqlCommand.Parameters.Add(sqlParam3)

    '            sqlConnection.Open() 'Open sqlconnection 
    '            sqlCommand.ExecuteNonQuery() 'Insert the images 
    '            sqlConnection.Close() 'Close sqlconnection
    '        End If

    '        Return ""
    '        'Catch ex As System.Exception
    '        '    'Return an Error To Client
    '        '    Return -1
    '    Catch ex As OleDbException
    '        '   return an error to the client
    '        Return ex.Message
    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    '    '     End Try
    'End Function
    Public Function UpdateStudentDocs(ByVal StudentDocsInfo As StudentDocsInfo, ByVal user As String, ByVal StudentDocsId As String, ByVal ModuleID As Integer) As String

        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            '    'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("Update plStudentDocs set StudentId=?,DocumentId=?, ")
                .Append(" DocStatusId=?,RequestDate=?,ReceiveDate=?, ")
                .Append(" ScannedId=?, ModUser=?, ModDate=?,ModuleID=? ")
                .Append(" Where StudentDocId = ? ")
            End With

            'StudentId
            db.AddParameter("@StudentId", StudentDocsInfo.StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'DocumentId
            If StudentDocsInfo.DocumentId = Guid.Empty.ToString Or StudentDocsInfo.DocumentId = "" Then
                db.AddParameter("@DocumentId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@DocumentId", StudentDocsInfo.DocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'DocStatusId
            If StudentDocsInfo.DocStatusId = Guid.Empty.ToString Or StudentDocsInfo.DocStatusId = "" Then
                db.AddParameter("@DocStatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@DocStatusId", StudentDocsInfo.DocStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'RequestDate
            If StudentDocsInfo.RequestDate = "" Then
                db.AddParameter("@RequestDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@RequestDate", StudentDocsInfo.RequestDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ReceivedDate
            If StudentDocsInfo.ReceiveDate = "" Then
                db.AddParameter("@ReceiveDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ReceiveDate", StudentDocsInfo.ReceiveDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If StudentDocsInfo.ScannedId = "True" Then
                StudentDocsInfo.ScannedId = 1
            Else
                StudentDocsInfo.ScannedId = 0
            End If

            'ScannedId
            db.AddParameter("@scannedid", StudentDocsInfo.ScannedId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'ModuleId
            db.AddParameter("@ModuleId", ModuleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'StudentDocId
            db.AddParameter("@StudentDocId", StudentDocsId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            With sb
                .Append("delete from adEntrTestOverRide where StudentId=? and EntrTestId=? ")
            End With
            db.AddParameter("@StudentId", StudentDocsInfo.StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@EntrTestId", StudentDocsInfo.DocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)


            Dim sb6 As New StringBuilder
            With sb6
                'With subqueries
                .Append(" Select LeadId from arStuEnrollments where StudentId=? ")
            End With
            db.AddParameter("@StudentId", StudentDocsInfo.DocumentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb6.ToString)
            Dim strLeadId As String
            While dr.Read()
                If Not (dr("LeadId") Is System.DBNull.Value) Then strLeadId = CType(dr("LeadId"), Guid).ToString Else strLeadId = ""
            End While

            If Not dr.IsClosed Then dr.Close()

            db.ClearParameters()
            sb6.Remove(0, sb6.Length)

            Dim intOverride As Integer
            If StudentDocsInfo.Override = True Then
                intOverride = 1
            Else
                intOverride = 0
            End If

            'check if document status maps to approved status, if so then uncheck override checkbox
            Dim intRecordCount As Integer = 0
            If StudentDocsInfo.DocStatusId = Guid.Empty.ToString Or StudentDocsInfo.DocStatusId = "" Then
            Else
                intRecordCount = CheckIfDocumentStatusMapsToApprovedStatus(StudentDocsInfo.DocStatusId)
                If intRecordCount >= 1 Then 'means the status maps to approved status
                    intOverride = 0 ' reset the override checkbox to 0
                End If
            End If

            With sb
                .Append(" Insert into adEntrTestOverRide(EntrTestOverRideId,LeadId,StudentId,EntrTestId,OverRide,ModUser,ModDate) ")
                .Append(" values(?,?,?,?,?,?,?) ")
            End With
            db.AddParameter("@LeadEntrTestId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            If Not strLeadId = "" Then
                db.AddParameter("@LeadId", strLeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@LeadId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            db.AddParameter("@StudentId", StudentDocsInfo.StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@EntrTestId", StudentDocsInfo.DocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@OverRide", intOverride, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)


            StudentDocsInfo.IsInDb = True

            'Insert data into regent xml 
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                Dim regDB As New regentDB
                Dim strFilename As String = AdvantageCommonValues.getStudentBatchFileName()
                regDB.AddTrackingXML(StudentDocsInfo.StudentId, strFilename, StudentDocsInfo.StudentDocsId)
            End If

            Return ""
            'Catch ex As System.Exception
            '    'Return an Error To Client
            '    Return -1
        Catch ex As OleDbException
            '   return an error to the client
            Return ex.Message
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
        '     End Try
    End Function
    Public Function GetStudentDetails(ByVal StudentDocId As String) As StudentDocsInfo

        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select StudentId,DocumentId,DocStatusId,RequestDate,ReceiveDate,ScannedId, ")
            .Append(" (Select Descrip from adReqs where adReqTypeId=3 and adReqId = SD.documentid) as DocumentDescrip,  ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = SD.DocStatusId) as DocumentStatus, ")
            .Append(" ModuleId, ")
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                .Append("trackcode,DueDate,NotifiedDate,CompletedDate,Notificationcode,Description,")
            End If
            .Append(" case when (select count(*) from adEntrTestOverRide where EntrTestId=SD.DocumentId and StudentId=SD.StudentId and OverRide=1) >=1 then 'True' else 'False' End as OverRide")
            .Append(" from plStudentDocs SD where StudentDocId= ? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@StudentDocId", StudentDocId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim StudentDocs As New StudentDocsInfo

        While dr.Read()
            With StudentDocs
                .IsInDb = True
                If Not (dr("ScannedId") Is System.DBNull.Value) Then .ScannedId = dr("ScannedId") Else .ScannedId = 0
                If Not (dr("StudentId") Is System.DBNull.Value) Then .StudentId = CType(dr("StudentId"), Guid).ToString
                If Not (dr("DocumentDescrip") Is System.DBNull.Value) Then .DocumentDescrip = dr("DocumentDescrip") Else .DocumentDescrip = ""
                If Not (dr("DocumentId") Is System.DBNull.Value) Then .DocumentId = CType(dr("DocumentId"), Guid).ToString Else .DocumentId = ""
                If Not (dr("DocStatusId") Is System.DBNull.Value) Then .DocStatusId = CType(dr("DocStatusId"), Guid).ToString Else .DocStatusId = ""
                If Not (dr("DocumentStatus") Is System.DBNull.Value) Then .DocumentStatus = dr("DocumentStatus")
                If Not (dr("RequestDate") Is System.DBNull.Value) Then .RequestDate = dr("RequestDate") Else .RequestDate = ""
                If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    If Not (dr("DueDate") Is System.DBNull.Value) Then .DueDate = dr("DueDate") Else .DueDate = ""
                    If Not (dr("NotifiedDate") Is System.DBNull.Value) Then .TransDate = dr("NotifiedDate") Else .TransDate = ""
                    If Not (dr("CompletedDate") Is System.DBNull.Value) Then .CompletedDate = dr("CompletedDate") Else .CompletedDate = ""
                    If Not (dr("Description") Is System.DBNull.Value) Then .DocDescrip = dr("Description") Else .DocDescrip = ""
                    If Not (dr("TrackCode") Is System.DBNull.Value) Then .TrackCode = dr("TrackCode") Else .TrackCode = ""
                End If
                If Not (dr("ReceiveDate") Is System.DBNull.Value) Then .ReceiveDate = dr("ReceiveDate") Else .ReceiveDate = ""
                If Not (dr("ModuleId") Is System.DBNull.Value) Then .ModuleId = dr("ModuleId") Else .ModuleId = ""
                If Not (dr("OverRide") Is System.DBNull.Value) Then
                    .Override = CType(dr("Override"), Boolean)
                Else
                    .Override = False
                End If
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return StudentDocs
    End Function
    'Public Function GetDocsByStudentId(ByVal StudentId As String) As DataSet

    '    'connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    'build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        'With subqueries
    '        .Append(" select A.StudentDocId,(Select DocumentDescrip from cmDocuments where DocumentId = A.DocumentId) ")
    '        .Append(" as DocumentDescription from plStudentDocs A where A.StudentId= ? ")
    '    End With

    '    'Add the EmployerContactId the parameter list
    '    db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function
    'Public Function GetDocsByStudentId1(ByVal StudentId As String, ByVal ModuleID As Integer) As DataSet
    '    'connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    'build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        'With subqueries
    '        .Append(" select A.StudentDocId,(Select DocumentDescrip from cmDocuments where DocumentId = A.DocumentId) ")
    '        .Append(" as DocumentDescription from plStudentDocs A where A.StudentId= ? and A.ModuleID= ? ")
    '    End With

    '    'Add the EmployerContactId the parameter list
    '    db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@ModuleId", ModuleID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function
    Public Function GetDocsStudent(ByVal ModuleID As Integer, ByVal StudentId As String, ByVal CampusId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim da6 As New OleDbDataAdapter
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select A.StudentDocId,B.StudentId,  ")
            '            .Append(" ,B.FirstName + ' ' + B.LastName + ' ' + B.MiddleName + (Select Descrip from adReqs where adReqId = A.DocumentId) as Descrip from plStudentDocs A,arStudent B where A.StudentId=B.StudentId and A.ModuleID= ? ")
            .Append(" B.FirstName + ' ' + B.LastName + ' ' + B.MiddleName + ' - ' + (Select Descrip from adReqs where adReqId = A.DocumentId) as Descrip ")
            .Append(" from plStudentDocs A,arStudent B where A.StudentId=B.StudentId and A.ModuleID= ? ")
            If Not StudentId = Guid.Empty.ToString Then
                .Append(" and B.StudentId = ? ")
            End If
            If Not CampusId = "" Then
                .Append(" and CampusId=? ")
            End If
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@ModuleId", ModuleID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        If Not StudentId = Guid.Empty.ToString Then
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not CampusId = "" Then
            db.AddParameter("@campusid", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "StudentDT56")
        Catch ex As System.Exception
            Throw New System.Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    'Public Function GetDocsStudentByCampus(ByVal ModuleID As Integer, ByVal StudentId As String, ByVal CampusId As String) As DataSet
    '    'connect to the database
    '    Dim db As New DataAccess
    '    Dim da6 As New OleDbDataAdapter
    '    Dim ds As New DataSet

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    'build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        'With subqueries
    '        .Append(" select A.StudentDocId,B.StudentId,(Select DocumentDescrip from cmDocuments where DocumentId = A.DocumentId) ")
    '        .Append(" as DocumentDescription,B.FirstName,B.LastName,B.MiddleName from plStudentDocs A,arStudent B where A.StudentId=B.StudentId and B.CampusId=? and A.ModuleID= ? ")
    '        If Not StudentId = Guid.Empty.ToString Then
    '            .Append(" and StudentId = ? ")
    '        End If
    '    End With

    '    'Add the EmployerContactId the parameter list
    '    db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
    '    db.AddParameter("@ModuleId", ModuleID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
    '    If Not StudentId = Guid.Empty.ToString Then
    '        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    End If
    '    db.OpenConnection()
    '    da6 = db.RunParamSQLDataAdapter(sb.ToString)
    '    Try
    '        da6.Fill(ds, "StudentDT52")
    '    Catch ex As System.Exception
    '        Throw New System.Exception(ex.InnerException.Message)
    '    End Try
    '    db.CloseConnection()
    '    Return ds
    'End Function
    'Public Function GetDocsStudentByStudentId(ByVal ModuleID As Integer, ByVal StudentId As String) As DataSet
    '    'connect to the database
    '    Dim db As New DataAccess
    '    Dim da6 As New OleDbDataAdapter
    '    Dim ds As New DataSet

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    'build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        'With subqueries
    '        .Append(" select A.StudentDocId,B.StudentId,(Select DocumentDescrip from cmDocuments where DocumentId = A.DocumentId) ")
    '        .Append(" as DocumentDescription,B.FirstName,B.LastName,B.MiddleName from plStudentDocs A,arStudent B where A.StudentId=B.StudentId and B.StudentId=? ")
    '        If ModuleID >= 1 Then
    '            .Append(" and A.ModuleID= ? ")
    '        End If
    '    End With

    '    'Add the EmployerContactId the parameter list
    '    db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    If ModuleID >= 1 Then
    '        db.AddParameter("@ModuleId", ModuleID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
    '    End If
    '    db.OpenConnection()
    '    da6 = db.RunParamSQLDataAdapter(sb.ToString)
    '    Try
    '        da6.Fill(ds, "StudentDT5")
    '    Catch ex As System.Exception
    '        Throw New System.Exception(ex.InnerException.Message)
    '    End Try
    '    db.CloseConnection()
    '    Return ds
    'End Function
    Public Function GetAllStudents() As DataSet

        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select StudentDocId,FirstName,LastName from arStudent A,plStudentDocs B where A.studentId = b.studentid ")
        End With

        Return db.RunSQLDataSet(sb.ToString)
    End Function
    'Public Function GetDocsByStatus(ByVal DocumentStatusId As String, ByVal StudentId As String, ByVal ModuleID As Integer) As DataSet
    '    'connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    'build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        'With subqueries
    '        .Append(" select A.StudentDocId,(Select DocumentDescrip from cmDocuments where DocumentId = A.DocumentId) ")
    '        .Append(" as DocumentDescription from plStudentDocs A where   ")
    '        If DocumentStatusId <> "" Then
    '            .Append(" A.DocStatusId= ? and ")
    '        End If
    '        .Append(" A.StudentId = ? and A.ModuleID = ?")
    '    End With

    '    'Add the EmployerContactId the parameter list
    '    If DocumentStatusId <> "" Then
    '        db.AddParameter("@DocStatusId", DocumentStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    End If
    '    db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@ModuleId", ModuleID, DataAccess.OleDbDataType.OleDbInteger, ParameterDirection.Input)
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function
    Public Function GetAllDocsStudentByStatus(ByVal DocumentStatusId As String, ByVal ModuleID As Integer, ByVal StudentId As String, ByVal campusId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        Dim da6 As New OleDbDataAdapter
        Dim ds As New DataSet
        With sb
            'With subqueries
            .Append(" select A.StudentDocId,B.FirstName,B.LastName,B.MiddleName,B.StudentId,(Select DocumentDescrip from adReqs where adReqId = A.DocumentId) ")
            .Append(" as DocumentDescription from plStudentDocs A,arStudent B where A.StudentId=B.StudentId   ")
            If DocumentStatusId <> "" Then
                .Append(" and A.DocStatusId= ?  ")
            End If
            If ModuleID >= 1 Then
                .Append(" and A.ModuleID = ?")
            End If
            If Not StudentId = Guid.Empty.ToString Then
                .Append(" and B.StudentId = ?")
            End If
            If Not campusId = "" Then
                .Append(" and B.CampusId = ? ")
            End If
        End With

        'Add the EmployerContactId the parameter list
        If DocumentStatusId <> "" Then
            db.AddParameter("@DocStatusId", DocumentStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If ModuleID >= 1 Then
            db.AddParameter("@ModuleId", ModuleID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        End If
        If Not StudentId = Guid.Empty.ToString Then
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not campusId = "" Then
            db.AddParameter("@campusid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "StudentDT56")
        Catch ex As System.Exception
            Throw New System.Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        Return ds
    End Function
    Public Function DeleteStudentDocs(ByVal StudentDocId As String, ByVal StudId As String, ByVal DocumentId As String) As Integer
        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM plStudentDocs ")
                .Append("WHERE StudentDocId = ? ")
            End With

            '   add parameters values to the query

            '   BankId
            db.AddParameter("@StudentDocId", StudentDocId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            With sb
                .Append(" Delete from syDocumentHistory where StudentId=? and DocumentId=? ")
            End With
            db.AddParameter("@StudentId", StudId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@DocumentId", DocumentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            '   return without errors
            Return 0

        Catch ex As OleDbException
            '   return an error to the client
            Return -1
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetStudentsByDocument(ByVal DocumentId As String, ByVal DocStatusId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select StudentDocId,FirstName,LastName from arStudent,plStudentDocs where ")
            .Append(" arStudent.StudentId = plStudentDocs.StudentId and ")
            .Append(" DocumentId = ? and DocStatusId= ? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@DocumentId", DocumentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@DocStatusId", DocStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetStudentsByDocumentOnly(ByVal DocumentId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select StudentDocId,FirstName,LastName from arStudent,plStudentDocs where ")
            .Append(" arStudent.StudentId = plStudentDocs.StudentId and ")
            .Append(" DocumentId = ? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@DocumentId", DocumentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    'Public Function GetDocumentsByStudent(ByVal StudentId As String, ByVal DocStatusId As String) As DataSet
    '    'connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    'build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        'With subqueries
    '        .Append(" select t2.DocumentId,t2.StudentDocId,(select DocumentDescrip from cmDocuments where DocumentId=t2.DocumentId) as DocumentDescrip ")
    '        .Append(" from arStudent t1,plStudentDocs t2 where ")
    '        .Append(" t1.StudentId = t2.StudentId and ")
    '        .Append(" t1.StudentId = ? and t2.DocStatusId = ? ")
    '    End With

    '    'Add the EmployerContactId the parameter list
    '    db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@DocStatusId", DocStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function
    Public Function AddDocsByStudent(ByVal StudentDocsInfo As StudentDocsInfo, ByVal user As String) As Integer

        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        'Try
        '    'Build The Query
        Dim sb As New StringBuilder
        With sb
            .Append("Insert plDocsByStudent(StudentDocId,StudentId,DocumentId, ")
            .Append(" DocStatusId,RequestDate,ReceiveDate, ")
            .Append(" ScannedId, ModUser, ModDate) ")
            .Append(" Values(?,?,?,?,?,?,?,?,?) ")
        End With


        'Get StudentDocsId
        db.AddParameter("@StudentDocId", StudentDocsInfo.StudentDocsId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'StudentId
        db.AddParameter("@StudentId", StudentDocsInfo.StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'DocumentId
        If StudentDocsInfo.DocumentId = Guid.Empty.ToString Or StudentDocsInfo.DocumentId = "" Then
            db.AddParameter("@DocumentId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@DocumentId", StudentDocsInfo.DocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        'DocStatusId
        If StudentDocsInfo.DocStatusId = Guid.Empty.ToString Or StudentDocsInfo.DocStatusId = "" Then
            db.AddParameter("@DocStatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@DocStatusId", StudentDocsInfo.DocStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        'RequestDate
        If StudentDocsInfo.RequestDate = "" Then
            db.AddParameter("@RequestDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@RequestDate", StudentDocsInfo.RequestDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        'ReceivedDate
        If StudentDocsInfo.ReceiveDate = "" Then
            db.AddParameter("@ReceiveDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@ReceiveDate", StudentDocsInfo.ReceiveDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        If StudentDocsInfo.ScannedId = "True" Then
            StudentDocsInfo.ScannedId = 1
        Else
            StudentDocsInfo.ScannedId = 0
        End If

        'ScannedId
        db.AddParameter("@ScannedId", StudentDocsInfo.ScannedId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)

        'ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'ModDate
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        'Execute The Query
        db.RunParamSQLExecuteNoneQuery(sb.ToString)

        StudentDocsInfo.IsInDb = True

        'Retun Without Errors
        Return 0

        'Catch ex As System.Exception
        'Return an Error To Client
        '   Return -1

        '  Finally
        'Close Connection
        db.CloseConnection()
        '     End Try
    End Function
    Public Function UpdateDocsByStudent(ByVal StudentDocsInfo As StudentDocsInfo, ByVal user As String, ByVal StudentDocsId As String) As Integer

        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        'Try
        '    'Build The Query
        Dim sb As New StringBuilder
        With sb
            .Append("Update plStudentDocs set StudentId=?,DocumentId=?, ")
            .Append(" DocStatusId=?,RequestDate=?,ReceiveDate=?, ")
            .Append(" ScannedId=?, ModUser=?, ModDate=? ")
            .Append(" Where StudentDocId = ? ")
        End With

        'StudentId
        db.AddParameter("@StudentId", StudentDocsInfo.StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'DocumentId
        If StudentDocsInfo.DocumentId = Guid.Empty.ToString Or StudentDocsInfo.DocumentId = "" Then
            db.AddParameter("@DocumentId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@DocumentId", StudentDocsInfo.DocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        'DocStatusId
        If StudentDocsInfo.DocStatusId = Guid.Empty.ToString Or StudentDocsInfo.DocStatusId = "" Then
            db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@StatusId", StudentDocsInfo.DocStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        'RequestDate
        If StudentDocsInfo.RequestDate = "" Then
            db.AddParameter("@RequestDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@RequestDate", StudentDocsInfo.RequestDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        'ReceivedDate
        If StudentDocsInfo.ReceiveDate = "" Then
            db.AddParameter("@ReceiveDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@ReceiveDate", StudentDocsInfo.ReceiveDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        If StudentDocsInfo.ScannedId = "True" Then
            StudentDocsInfo.ScannedId = 1
        Else
            StudentDocsInfo.ScannedId = 0
        End If

        'ScannedId
        db.AddParameter("@scannedid", StudentDocsInfo.ScannedId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        'ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'ModDate
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        'StudentDocId
        db.AddParameter("@StudentDocId", StudentDocsId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute The Query
        db.RunParamSQLExecuteNoneQuery(sb.ToString)

        StudentDocsInfo.IsInDb = True

        'Retun Without Errors
        Return 0

        'Catch ex As System.Exception
        'Return an Error To Client
        '   Return -1

        '  Finally
        'Close Connection
        db.CloseConnection()
        '     End Try
    End Function
    'Public Function GetStudentDocumentDetails(ByVal StudentDocId As String) As StudentDocsInfo

    '    'connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    'build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        'With subqueries
    '        .Append(" select StudentId,(select FirstName from arStudent where StudentId=SD.StudentId) as FirstName,(select MiddleName from arStudent where StudentId=SD.StudentId) as MiddleName,(select LastName from arStudent where StudentId=SD.StudentId) as LastName, ")
    '        .Append(" DocumentId, DocStatusId, RequestDate, ReceiveDate, ScannedId, ")
    '        .Append(" (Select DocumentDescrip from cmDocuments where DocumentId = SD.documentid) as DocumentDescrip, ")
    '        .Append(" (Select DocStatusDescrip from cmDocumentStatus where DocStatusId = SD.docstatusid) as DocumentStatus ")
    '        .Append(" from plStudentDocs SD where StudentDocId= ? ")
    '    End With

    '    'Add the EmployerContactId the parameter list
    '    db.AddParameter("@StudentDocId", StudentDocId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    'Execute the query
    '    Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

    '    Dim StudentDocs As New StudentDocsInfo

    '    While dr.Read()
    '        With StudentDocs
    '            .IsInDb = True
    '            If Not (dr("StudentId") Is System.DBNull.Value) Then .StudentId = CType(dr("StudentId"), Guid).ToString
    '            If Not (dr("FirstName") Is System.DBNull.Value) Then .FirstName = CType(dr("FirstName"), String).ToString
    '            If Not (dr("LastName") Is System.DBNull.Value) Then .LastName = CType(dr("LastName"), String).ToString
    '            If Not (dr("MiddleName") Is System.DBNull.Value) Then .MiddleName = CType(dr("MiddleName"), String).ToString
    '            .ScannedId = dr("ScannedId")
    '            If Not (dr("DocumentDescrip") Is System.DBNull.Value) Then .DocumentDescrip = CType(dr("DocumentDescrip"), String).ToString Else .DocumentDescrip = ""
    '            If Not (dr("DocumentId") Is System.DBNull.Value) Then .DocumentId = CType(dr("DocumentId"), Guid).ToString
    '            If Not (dr("DocStatusId") Is System.DBNull.Value) Then .DocStatusId = CType(dr("DocStatusId"), Guid).ToString
    '            If Not (dr("RequestDate") Is System.DBNull.Value) Then .RequestDate = dr("RequestDate") Else .RequestDate = ""
    '            If Not (dr("ReceiveDate") Is System.DBNull.Value) Then .ReceiveDate = dr("ReceiveDate") Else .ReceiveDate = ""
    '        End With
    '    End While

    '    If Not dr.IsClosed Then dr.Close()
    '    If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    '    'Return BankInfo
    '    Return StudentDocs
    'End Function
    Public Function GetStudentDataToPostToSchoolDocs(ByVal StudentId As String) As SchoolDocsInfo
        'build the sql query
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select  ")
            .Append(" t1.FirstName, ")
            .Append("	t1.LastName, ")
            .Append("	t1.SSN, ")
            .Append("	t1.StudentStatus, ")
            .Append("	(select distinct Status from syStatuses where statusId=t1.StudentStatus) as Student_Status, ")
            .Append("	t1.DOB, ")
            .Append("	t1.Gender, ")
            .Append("	(select Distinct GenderDescrip from adGenders where GenderId=t1.Gender) as StudentGender, ")
            .Append("	t1.Race, ")
            .Append("	(select Distinct EthCodeDescrip from adEthCodes where EthCodeId=t1.Race) as StudentRace, ")
            .Append("	t1.StudentNumber, ")
            .Append("	t1.Sponsor, ")
            .Append("	(select Distinct AgencySpDescrip from adAgencySponsors where AgencySpId=t1.Race) as StudentRace, ")
            .Append("	t2.AdmissionsRep, ")
            .Append("	(select Distinct FullName from syUsers where UserId=t2.AdmissionsRep) as AdmissionRep, ")
            .Append("	t2.CampusId, ")
            .Append("	(select Distinct CampDescrip from syCampuses where CampusId=t2.CampusId) as Campuses, ")
            .Append("	t2.StartDate, ")
            .Append("	t2.DateDetermined, ")
            .Append("	t2.DropReasonId, ")
            .Append(" (select Distinct Descrip from arDropReasons where DropReasonId=t2.DropReasonId) as DropReason, ")
            .Append("	t2.EnrollDate, ")
            .Append("	t2.ExpGradDate, ")
            .Append("	t2.LDA, ")
            .Append("	t2.PrgVerId, ")
            .Append("	(select PrgVerDescrip from arPrgVersions where PrgVerId=t2.PrgVerId) as ProgramName, ")
            .Append("	Case when  ")
            .Append("			(select Count(*) from syStatusCodes ")
            .Append("			where StatusCodeId=t2.StatusCodeId and SysStatusId=12) >=1 ")
            .Append("	     then ")
            .Append("                'Yes' ")
            .Append("	     else ")
            .Append("                'No' ")
            .Append("                End ")
            .Append("	as Status_Dropped, ")
            .Append("	Case when  ")
            .Append("			(select Count(*) from syStatusCodes ")
            .Append("			where StatusCodeId=t2.StatusCodeId and SysStatusId=14) >=1 ")
            .Append("	     then ")
            .Append("                'Yes' ")
            .Append("	     else ")
            .Append("                'No' ")
            .Append("                End ")
            .Append("	as Status_Graduated ")
            .Append("                from ")
            .Append("	arStudent t1, arStuEnrollments t2 ")
            .Append("                where ")
            .Append("	t1.StudentId = t2.StudentId and ")
            .Append("	t1.StudentId=? ")

        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim objSchoolDocsInfo As New SchoolDocsInfo

        While dr.Read()
            With objSchoolDocsInfo
                If Not (dr("FirstName") Is System.DBNull.Value) Then .FirstName = dr("FirstName").ToString
                If Not (dr("LastName") Is System.DBNull.Value) Then .LastName = dr("LastName").ToString
                If Not (dr("SSN") Is System.DBNull.Value) Then .SSN = dr("SSN").ToString Else .SSN = ""
                If Not (dr("Student_Status") Is System.DBNull.Value) Then .StudentStatus = dr("Student_Status").ToString Else .StudentStatus = ""
                If Not (dr("DOB") Is System.DBNull.Value) Then .DOB = CType(dr("DOB"), Date) Else .DOB = "01/01/1900"
                If Not (dr("StudentGender") Is System.DBNull.Value) Then .StudentGender = dr("StudentGender").ToString Else .StudentGender = ""
                If Not (dr("StudentRace") Is System.DBNull.Value) Then .StudentRace = dr("StudentRace").ToString Else .StudentRace = ""
                If Not (dr("StudentRace") Is System.DBNull.Value) Then .StudentRace = dr("StudentRace").ToString Else .StudentRace = ""

                If Not (dr("AdmissionsRep") Is System.DBNull.Value) Then .AdmissionsRep = dr("AdmissionsRep").ToString Else .AdmissionsRep = ""
                If Not (dr("Campuses") Is System.DBNull.Value) Then .Campuses = dr("Campuses").ToString Else .Campuses = ""
                If Not (dr("StartDate") Is System.DBNull.Value) Then .StartDate = CType(dr("StartDate"), Date) Else .StartDate = "01/01/1900"
                If Not (dr("DateDetermined") Is System.DBNull.Value) Then .DateDetermined = CType(dr("DateDetermined"), Date) Else .DateDetermined = "01/01/1900"
                If Not (dr("DropReason") Is System.DBNull.Value) Then .DropReason = dr("DropReason").ToString Else .DropReason = ""
                If Not (dr("EnrollDate") Is System.DBNull.Value) Then .EnrollDate = CType(dr("EnrollDate"), Date) Else .EnrollDate = "01/01/1900"
                If Not (dr("ExpGradDate") Is System.DBNull.Value) Then .ExpGradDate = CType(dr("ExpGradDate"), Date) Else .ExpGradDate = "01/01/1900"
                If Not (dr("LDA") Is System.DBNull.Value) Then .LDA = CType(dr("LDA"), Date) Else .LDA = "01/01/1900"
                If Not (dr("ProgramName") Is System.DBNull.Value) Then .ProgramName = dr("ProgramName").ToString Else .ProgramName = ""
                If Not (dr("Status_Dropped") Is System.DBNull.Value) Then .StatusDropped = dr("Status_Dropped").ToString Else .StatusDropped = ""
                If Not (dr("Status_Graduated") Is System.DBNull.Value) Then .StatusGraduated = dr("Status_Graduated").ToString Else .StatusGraduated = ""

            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return objSchoolDocsInfo
    End Function

    Public Function GetFeeLevelId(ByVal stuEnrollId As String, ByVal transCodeId As String) As Integer
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append("select FeeLevelId = case ")
            .Append("                    when isnull((select top 1 PrgVerFeeId ")
            .Append("                                 from saProgramVersionFees pvf, arStuEnrollments se ")
            .Append("                                 where pvf.TransCodeId = ? ")
            .Append("                                 and pvf.PrgVerId=se.PrgVerId ")
            .Append("                                 and se.StuEnrollId = ? ")
            .Append("                                 and se.TuitionCategoryId=pvf.TuitionCategoryId),'00000000-0000-0000-0000-000000000000') <> '00000000-0000-0000-0000-000000000000' then 2 ")
            .Append("                    when isnull((select top 1 PrgVerFeeId ")
            .Append("                                 from saProgramVersionFees pvf, arStuEnrollments se ")
            .Append("                                 where pvf.TransCodeId = ? ")
            .Append("                                 and pvf.PrgVerId=se.PrgVerId ")
            .Append("                                 and se.StuEnrollId = ? ")
            .Append("                                 and se.TuitionCategoryId=pvf.TuitionCategoryId),'00000000-0000-0000-0000-000000000000') = '00000000-0000-0000-0000-000000000000' then ")
            .Append("                       case ")
            .Append("                           when isnull((select top 1 PrgVerFeeId ")
            .Append("                                        from saProgramVersionFees pvf2, saTransCodes tc, arStuEnrollments se2 ")
            .Append("                                        where pvf2.TransCodeId=tc.TransCodeId ")
            .Append("                                        and pvf2.PrgVerId=se2.PrgVerId ")
            .Append("                                        and se2.StuEnrollId = ? ")
            .Append("                                        and tc.SysTransCodeId=(select tc2.SysTransCodeId from saTransCodes tc2 where tc2.TransCodeId=?)),'00000000-0000-0000-0000-000000000000') = '00000000-0000-0000-0000-000000000000' then 1 ")
            .Append("                           else 2 ")
            .Append("                       end ")
            .Append("                   end ")
        End With

        db.AddParameter("@tc1", transCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@se1", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@tc2", transCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@se2", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@se3", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@tc3", transCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Return CInt(db.RunParamSQLScalar(sb.ToString))

    End Function
    ''Added by Saraswathi Lakshmanan to find the Student Ledger Balance
    ''ReturnDtatype changed from Int to double
    Public Function GetStudentLedgerBalance(ByVal StuEnrollId As String) As Double
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append(" Select isnull(Sum(TransAmount),0) from saTransactions ")
            .Append("  where StuEnrollId= ? ")
            .Append("   and Voided=0")

        End With
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Return CDbl(db.RunParamSQLScalar(sb.ToString))
    End Function
    ''Added by Saraswathi Lakshmanan to find the Student Ledger Balance
    ''ReturnDtatype changed from Int to double
    ''Converted to stored Procedure
    Public Function GetStudentLedgerBalance_sp(ByVal StuEnrollId As String) As Double
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Return CDbl(db.RunParamSQLScalar_SP("USP_SA_GetStudentLedgerBalance"))
    End Function

    ''Function added by saraswathi Lakshmanan on May 07/2010
    ''To show the default charge tpe for the selected transcode for a given student.
    Public Function GetDefaultChargesForTransCodeandStudent_Sp(ByVal StuEnrollId As String, ByVal TransCode As String) As DataSet
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@TransCodeId", New Guid(TransCode), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Try
            ds = db.RunParamSQLDataSet_SP("dbo.USP_SA_GetDefaultChargeCodeForTransCodeandStudent_GetList")
        Catch ex As System.Exception

        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return ds
    End Function
    ''Function added by saraswathi Lakshmanan on May 21 2010
    ''To get the Academic year lenght and payment period details

    Public Function GetChargePeriodSequences(ByVal StuEnrollId As String, ByVal Defaultperiod As Integer) As DataSet
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@Defperiod", Defaultperiod, SqlDbType.Int, , ParameterDirection.Input)

        Try
            ds = db.RunParamSQLDataSet_SP("dbo.USP_SA_GetChargePeriodSequences_GetList")
        Catch ex As System.Exception

        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return ds
    End Function

End Class

#End Region