Imports FAME.Advantage.Common

Public Class WeeklyAttendanceDB

#Region "Private Data Members"

    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")

#End Region


#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

#End Region


#Region "Public Methods"

    Public Function GetWeeklyAttendance(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String = ""
        Dim strOrderBy As String = ""
        Dim strFrom As String = ""
        Dim strStudentId As String = ""
        Try


            If paramInfo.FilterList <> "" Then
                strWhere &= " AND " & paramInfo.FilterList

                If MyAdvAppSettings.AppSettings("TrackSapAttendance", paramInfo.CampusId).ToString.ToLower = "byclass" Then
                    strWhere = Replace(strWhere, "syCampuses.CampusId", "F.CampusId")
                End If



            End If
            Dim arrValues() As String
            Dim strFromDate As String
            Dim StrWeekStartDate As String = Date.Now
            If paramInfo.FilterOther <> "" Then
                arrValues = paramInfo.FilterOther.Split(";")
                If arrValues.Length > 0 Then
                    strFromDate = arrValues(1).ToString
                    ' set the start date to the filter start date if it was passed in
                    Dim tDate As DateTime = Date.Now
                    If strFromDate <> "" Then
                        tDate = CType(strFromDate, Date)
                        ' Find the first date of the week
                        While tDate.DayOfWeek <> DayOfWeek.Monday
                            tDate = tDate.AddDays(-1)
                        End While
                    End If
                    StrWeekStartDate = tDate ' set it to the first sunday     
                End If
            End If

            'If paramInfo.OrderBy <> "" Then
            '    strOrderBy &= "," & paramInfo.OrderBy
            'End If

            If StudentIdentifier = "SSN" Then
                strStudentId = "arStudent.SSN AS StudentIdentifier,"
            ElseIf StudentIdentifier = "EnrollmentId" Then
                strStudentId = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
            ElseIf StudentIdentifier = "StudentId" Then
                strStudentId = "arStudent.StudentNumber AS StudentIdentifier,"
            End If
            Dim strGroupby As String = ""
            If StudentIdentifier = "SSN" Then
                strGroupby = "arStudent.SSN ,"
            ElseIf StudentIdentifier = "EnrollmentId" Then
                strGroupby = "arStuEnrollments.EnrollmentId ,"
            ElseIf StudentIdentifier = "StudentId" Then
                strGroupby = "arStudent.StudentNumber ,"
            End If

            Dim strSuppressDate As String = "no"
            Try
                strSuppressDate = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
            Catch ex As System.Exception
                strSuppressDate = "no"
            End Try

            If MyAdvAppSettings.AppSettings("TrackSapAttendance", paramInfo.CampusId).ToString.ToLower = "byday" Then



                With sb

                    ''---------
                    ''Attendance unit Type Added

                    .Append(" Select A.StuEnrollId, ")
                    .Append(strStudentId)
                    .Append(" IsNull((Select LastName from arStudent where StudentId in (Select studentId from ")
                    .Append(" arStuEnrollments where StuEnrollId=a.StuEnrollid)),' ') +','+ ")
                    .Append(" isNull((Select Firstname from arStudent where StudentId in (Select studentId from ")
                    .Append(" arStuEnrollments where StuEnrollId=a.StuEnrollid)),' ')+ ' '+ ")
                    .Append(" isNull((Select MiddleName from arStudent where StudentId in (Select studentId from ")
                    .Append(" arStuEnrollments where StuEnrollId=a.StuEnrollid)),' ') + ' ' as StudentName, ")
                    .Append(" dbo.GetWeeklyAtt(a.StuEnrollid,DateAdd(Day,6,'" + StrWeekStartDate + "'),DateAdd(Day,0,'" + StrWeekStartDate + "')) as Mon,")
                    .Append(" dbo.GetWeeklyAtt(a.StuEnrollid,DateAdd(Day,6,'" + StrWeekStartDate + "'),DateAdd(Day,1,'" + StrWeekStartDate + "')) as Tue,")
                    .Append(" dbo.GetWeeklyAtt(a.StuEnrollid,DateAdd(Day,6,'" + StrWeekStartDate + "'),DateAdd(Day,2,'" + StrWeekStartDate + "')) as Wed,")
                    .Append(" dbo.GetWeeklyAtt(a.StuEnrollid,DateAdd(Day,6,'" + StrWeekStartDate + "'),DateAdd(Day,3,'" + StrWeekStartDate + "')) as Thur,")
                    .Append(" dbo.GetWeeklyAtt(a.StuEnrollid,DateAdd(Day,6,'" + StrWeekStartDate + "'),DateAdd(Day,4,'" + StrWeekStartDate + "')) as Fri,")
                    .Append(" dbo.GetWeeklyAtt(a.StuEnrollid,DateAdd(Day,6,'" + StrWeekStartDate + "'),DateAdd(Day,5,'" + StrWeekStartDate + "')) as Sat,")
                    .Append(" dbo.GetWeeklyAtt(a.StuEnrollid,DateAdd(Day,6,'" + StrWeekStartDate + "'),DateAdd(Day,6,'" + StrWeekStartDate + "')) as Sun,")






                    '.Append(" (Select ActualHours from arStudentClockAttendance ")
                    '.Append("  where StuEnrollId=A.StuEnrollId and CONVERT(Date,RecordDate,101)='" + StrWeekStartDate + "'")
                    ' ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ' ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ' ''The attendance details related to the previous schedules are also displayed
                    ''.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    '.Append(" and ActualHours<>999.0 and ActualHours<>9999.0)as Mon  ")
                    '.Append(" ,(Select ActualHours from arStudentClockAttendance ")
                    '.Append("  where StuEnrollId=A.StuEnrollId and CONVERT(Date,RecordDate,101)=DateAdd(Day,1,'" + StrWeekStartDate + "') ")
                    ' ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ' ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ' ''The attendance details related to the previous schedules are also displayed
                    ''.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    '.Append(" and ActualHours<>999.0 and ActualHours<>9999.0)as Tue  ")
                    '.Append(" ,(Select ActualHours from arStudentClockAttendance ")
                    '.Append("  where StuEnrollId=A.StuEnrollId and CONVERT(Date,RecordDate,101)=DateAdd(Day,2,'" + StrWeekStartDate + "') ")
                    ' ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ' ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ' ''The attendance details related to the previous schedules are also displayed
                    ''.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    '.Append(" and ActualHours<>999.0 and ActualHours<>9999.0)as Wed ")
                    '.Append(" ,(Select ActualHours from arStudentClockAttendance ")
                    '.Append("  where StuEnrollId=A.StuEnrollId and CONVERT(Date,RecordDate,101)=DateAdd(Day,3,'" + StrWeekStartDate + "') ")
                    ' ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ' ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ' ''The attendance details related to the previous schedules are also displayed
                    '' .Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    '.Append(" and ActualHours<>999.0 and ActualHours<>9999.0)as Thur  ")
                    '.Append(" ,(Select ActualHours from arStudentClockAttendance ")
                    '.Append("  where StuEnrollId=A.StuEnrollId and CONVERT(Date,RecordDate,101)=DateAdd(Day,4,'" + StrWeekStartDate + "') ")
                    ' ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ' ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ' ''The attendance details related to the previous schedules are also displayed
                    ''.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    '.Append(" and ActualHours<>999.0 and ActualHours<>9999.0)as Fri  ")
                    '.Append(" ,(Select ActualHours from arStudentClockAttendance ")
                    '.Append("  where StuEnrollId=A.StuEnrollId and CONVERT(Date,RecordDate,101)=DateAdd(Day,5,'" + StrWeekStartDate + "') ")
                    ' ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ' ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ' ''The attendance details related to the previous schedules are also displayed
                    ''.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    '.Append(" and ActualHours<>999.0 and ActualHours<>9999.0)as Sat  ")
                    '.Append(" ,(Select ActualHours from arStudentClockAttendance ")
                    '.Append("  where StuEnrollId=A.StuEnrollId and CONVERT(Date,RecordDate,101)=DateAdd(Day,6,'" + StrWeekStartDate + "') ")
                    ' ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ' ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ' ''The attendance details related to the previous schedules are also displayed
                    ''.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    '.Append(" and ActualHours<>999.0 and ActualHours<>9999.0)as Sun ")

                    .Append(" (isnull((Select Sum(ActualHours) from arStudentClockAttendance where   CONVERT(Date,RecordDate,101)<=DateAdd(Day,6,'" + StrWeekStartDate + "') and  ")
                    .Append(" CONVERT(Date,RecordDate,101)>=DateAdd(Day,0,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId   ")
                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    '  .Append(" and ScheduleId in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId)")
                    .Append("   and ActualHours<>999.0 and ActualHours<>9999.0),0.00) - dbo.GetAbsentHoursFromTardyBetweenDates(A.StuEnrollid,DateAdd(Day,0,'" + StrWeekStartDate + "'),DateAdd(Day,6,'" + StrWeekStartDate + "'))) as WeekTotal")

                    ''   .Append(" ,sum(ActualHours) as WeekTotal ")
                    .Append(" ,(isnull((Select Sum(ActualHours) from arStudentClockAttendance where ")
                    .Append("  CONVERT(Date,RecordDate,101)<=DateAdd(Day,6,'" + StrWeekStartDate + "')  and StuEnrollId=a.StuEnrollId ")
                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    ' .Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    .Append(" and ActualHours<>999.0 and ActualHours<>9999.0),0.00) - dbo.GetAbsentHoursFromTardyForByDay(A.StuEnrollid,DateAdd(Day,6,'" + StrWeekStartDate + "')))as NewTotal ")
                    .Append(" ,isnull((Select Sum(SchedHours) from arStudentClockAttendance where ")
                    .Append("  CONVERT(Date,RecordDate,101)<=DateAdd(Day,6,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId ")
                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    '.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    .Append(" and ActualHours<>999.0 and ActualHours<>9999.0),0.00)as NewTotalSched  ")
                    .Append(" ,(isnull((Select Sum(SchedHours- ActualHours) from arStudentClockAttendance where ")
                    .Append("  CONVERT(Date,RecordDate,101)<=DateAdd(Day,6,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId ")
                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    '.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    .Append(" and ActualHours<>999.0 and ActualHours<>9999.0 and SchedHours is not null and SchedHours<>0 and  ")
                    .Append(" ActualHours is not null and SchedHours>ActualHours),0.00) + dbo.GetAbsentHoursFromTardyForByDay(A.StuEnrollid,DateAdd(Day,6,'" + StrWeekStartDate + "'))) as NewTotalDaysAbsent ")
                    .Append(" ,isnull((Select Sum(ActualHours- SchedHours) from arStudentClockAttendance where ")
                    .Append("  CONVERT(Date,RecordDate,101)<=DateAdd(Day,6,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId ")

                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    '.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    .Append(" and ActualHours<>999.0 and ActualHours<>9999.0 and SchedHours is not null  and  ")
                    .Append(" ActualHours is not null and ActualHours<>0 and SchedHours<ActualHours),0.00)as NewTotalMakeUpDays ")
                    .Append(" ,isnull(((Select Sum(SchedHours- ActualHours) from arStudentClockAttendance where ")
                    .Append("  CONVERT(Date,RecordDate,101)<=DateAdd(Day,6,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId ")

                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    '.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    .Append(" and ActualHours<>999.0 and ActualHours<>9999.0 and SchedHours is not null and SchedHours<>0 and  ")
                    .Append(" ActualHours is not null and SchedHours>ActualHours)  + dbo.GetAbsentHoursFromTardyForByDay(A.StuEnrollid,DateAdd(Day,6,'" + StrWeekStartDate + "')) - ")
                    .Append(" isnull((Select Sum(ActualHours- SchedHours) from arStudentClockAttendance where ")
                    .Append("  CONVERT(Date,RecordDate,101)<=DateAdd(Day,6,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId ")
                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    ' .Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    .Append(" and ActualHours<>999.0 and ActualHours<>9999.0 and SchedHours is not null  and  ")
                    .Append(" ActualHours is not null and ActualHours<>0 and SchedHours<ActualHours),0.00)),0.00) ")
                    .Append("  as NewNetDaysAbsent ")
                    .Append(" ,isnull((Select Sum(SchedHours) from arStudentClockAttendance where ")
                    .Append("  StuEnrollId=a.StuEnrollId ")
                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    '.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    .Append(" and ActualHours<>999.0 and ActualHours<>9999.0 AND RecordDate <= dbo.GetLDAFromAttendance(A.StuEnrollId)),0.00) as NewSchedDaysatLDA ")

                    '.Append(" ,(Select Convert(varchar,max(RecordDate),101) from arStudentClockAttendance where  ")
                    .Append(", dbo.GetLDAFromAttendance(A.StuEnrollId) AS LDA, syCampGrps.CampGrpDescrip,F.CampDescrip ")
                    ''Added by Saraswathi to find the unit type (hours or Days)
                    .Append(" ,(Select UnitTypeId from arPrgVersions PV where PV.PrgVerId in (Select PrgverId from arStuEnrollments  where StuEnrollId=A.StuEnrollid)) as AttTypeID, ")
                    .Append("'" & strSuppressDate & "'")
                    .Append(" as SuppressDate ")
                    .Append(", '' as ClassSection ")
                    .Append(" from arStudentClockAttendance A ,arstudent,arStuEnrollments , syCmpGrpCmps E,syCampuses F,syCampGrps,syCampuses ")
                    If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                        .Append(",adLeadByLeadGroups, adLeadGroups ")
                    End If
                    .Append(" where CONVERT(Date,RecordDate,101)>='" + StrWeekStartDate + "' and CONVERT(Date,RecordDate,101)<=DateAdd(Day,6,'" + StrWeekStartDate + "') AND ")
                    .Append("A.ActualHours <> 9999.00 AND ")
                    .Append(" A.StuEnrollid=arStuEnrollments.StuEnrollid and arstudent.StudentId=arStuEnrollments.StudentId ")
                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    ' .Append(" and A.ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= arSTuEnrollments.StuEnrollId) ")
                    .Append("  AND arStuEnrollments.CampusId=F.CampusId  ")
                    .Append("   AND F.CampusId=E.CampusId AND    E.CampGrpId = syCampGrps.CampGrpId  AND arStuEnrollments.CampusId=syCampuses.CampusId ")


                    If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                        .Append("AND adLeadByLeadGroups.LeadGrpId=adLeadGroups.LeadGrpId ")
                        .Append("AND adLeadByLeadGroups.StuEnrollId=arStuEnrollments.StuEnrollId ")
                    End If

                    .Append(strWhere)

                    .Append("   Group By  ")
                    .Append(" A.StuEnrollId,")
                    .Append(strGroupby)
                    .Append("syCampGrps.CampGrpDescrip,F.CampDescrip")


                End With

            Else
                'This is for Class Sections. Used when "TrackSapAttendance" = "byclass"


                With sb
                    .Append("SELECT ")
                    .Append("C.StuEnrollId, C.StudentIdentifier, C.StudentName, SUM(C.Mon) AS Mon, SUM(C.Tue) AS Tue, SUM(C.Wed) AS Wed, SUM(C.Thur) AS Thur, SUM(C.Fri)AS Fri, ")
                    .Append("SUM(C.Sat) AS Sat, SUM(C.Sun) AS Sun, SUM(C.WeekTotal) AS WeekTotal, SUM(C.NewTotal) AS NewTotal, SUM(C.NewTotalSched) AS NewTotalSched, SUM(C.NewTotalDaysAbsent) AS NewTotalDaysAbsent, ")
                    .Append("SUM(C.NewTotalMakeUpDays) AS NewTotalMakeUpDays, SUM(C.NewNetDaysAbsent) AS NewNetDaysAbsent, SUM(C.NewSchedDaysatLDA) AS NewSchedDaysatLDA, C.LDA, ")
                    .Append("C.CampGrpDescrip, C.AttTypeID, C.SuppressDate ")
                    .Append("FROM ")
                    .Append("(SELECT B.* ")
                    .Append("FROM ")
                    .Append("(SELECT A.StuEnrollId, ")
                    .Append(strStudentId)
                    .Append("IsNull((Select LastName from arStudent where StudentId in  ")
                    .Append("(Select studentId from  arStuEnrollments where StuEnrollId=a.StuEnrollid)),' ') +','+ ")
                    .Append("isNull((Select Firstname from arStudent where StudentId in  ")
                    .Append("(Select studentId from  arStuEnrollments where StuEnrollId=a.StuEnrollid)),' ')+ ' '+ ")
                    .Append("isNull((Select MiddleName from arStudent where StudentId in ")
                    .Append("(Select studentId from  arStuEnrollments where StuEnrollId=a.StuEnrollid)),' ') + ' ' as StudentName, ")
                    .Append("(Select SUM(Actual) from atClsSectAttendance where StuEnrollId=A.StuEnrollId and CONVERT(Date,MeetDate,101)='" + StrWeekStartDate + "' ")
                    .Append("and Actual<>999.0 and Actual<>9999.0)as Mon, ")
                    .Append("(Select SUM(Actual) from atClsSectAttendance where StuEnrollId=A.StuEnrollId and CONVERT(Date,MeetDate,101)=DateAdd(Day,1,'" + StrWeekStartDate + "') ")
                    .Append("and Actual<>999.0 and Actual<>9999.0)as Tue, ")
                    .Append("(Select SUM(Actual) from atClsSectAttendance where StuEnrollId=A.StuEnrollId and CONVERT(Date,MeetDate,101)=DateAdd(Day,2,'" + StrWeekStartDate + "') ")
                    .Append("and Actual<>999.0 and Actual<>9999.0)as Wed, ")
                    .Append("(Select SUM(Actual) from atClsSectAttendance where StuEnrollId=A.StuEnrollId and CONVERT(Date,MeetDate,101)=DateAdd(Day,3,'" + StrWeekStartDate + "') ")
                    .Append("and Actual<>999.0 and Actual<>9999.0)as Thur, ")
                    .Append("(Select SUM(Actual) from atClsSectAttendance where StuEnrollId=A.StuEnrollId and CONVERT(Date,MeetDate,101)=DateAdd(Day,4,'" + StrWeekStartDate + "') ")
                    .Append("and Actual<>999.0 and Actual<>9999.0)as Fri, ")
                    .Append("(Select SUM(Actual) from atClsSectAttendance where StuEnrollId=A.StuEnrollId and CONVERT(Date,MeetDate,101)=DateAdd(Day,5,'" + StrWeekStartDate + "') ")
                    .Append("and Actual<>999.0 and Actual<>9999.0)as Sat, ")
                    .Append("(Select SUM(Actual) from atClsSectAttendance where StuEnrollId=A.StuEnrollId and CONVERT(Date,MeetDate,101)=DateAdd(Day,6,'" + StrWeekStartDate + "') ")
                    .Append("and Actual<>999.0 and Actual<>9999.0)as Sun, ")
                    .Append("isnull((Select Sum(Actual) from atClsSectAttendance where (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "') ")
                    .Append("and (CONVERT(Date,MeetDate, 101))>=DateAdd(Day,0,'" + StrWeekStartDate + "')and StuEnrollId=a.StuEnrollId and Actual<>999.0 and Actual<>9999.0),0.00)as WeekTotal, ")
                    .Append("isnull((Select Sum(Actual) from atClsSectAttendance where (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "') ")
                    .Append("and StuEnrollId=a.StuEnrollId  and Actual<>999.0 and Actual<>9999.0),0.00) as NewTotal, ")
                    .Append("isnull((Select Sum(Scheduled) from atClsSectAttendance where (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId ")
                    .Append("and Actual<>999.0 and Actual<>9999.0),0.00)as NewTotalSched, ")
                    .Append("isnull((Select Sum(Scheduled- Actual) from atClsSectAttendance where (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId ")
                    .Append("and Actual<>999.0 and Actual<>9999.0 and Scheduled is not null ")
                    .Append("and Scheduled<>0 and Actual is not null and Scheduled>Actual),0.00)as NewTotalDaysAbsent, ")
                    .Append("isnull((Select Sum(Actual- Scheduled) from atClsSectAttendance where (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId ")
                    .Append("and Actual<>999.0 and Actual<>9999.0 and Scheduled is not null and Actual is not null ")
                    .Append("and Actual<>0 and Scheduled<Actual),0.00)as NewTotalMakeUpDays, ")
                    .Append("isnull(((Select Sum(Scheduled- Actual) from atClsSectAttendance where (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId ")
                    .Append("and Actual<>999.0 and Actual<>9999.0 and Scheduled is not null and Scheduled<>0 and Actual is not null ")
                    .Append("and Scheduled>Actual) -  (Select Sum(Actual- Scheduled) from atClsSectAttendance where (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "') ")
                    .Append("and StuEnrollId=a.StuEnrollId  and Actual<>999.0 and Actual<>9999.0 and Scheduled is not null and  ")
                    .Append("Actual is not null and Actual<>0 and Scheduled<Actual)),0.00) as NewNetDaysAbsent, ")
                    .Append("isnull((Select Sum(Scheduled) from atClsSectAttendance where StuEnrollId=a.StuEnrollId ")
                    .Append("and Actual<>999.0 and Actual<>9999.0   AND CONVERT(DATE, MeetDate, 101) <= dbo.GetLDAFromAttendance(A.StuEnrollId)),0.00)as NewSchedDaysatLDA, ")
                    .Append("(Select Convert(varchar,max(MeetDate),101) from atClsSectAttendance where StuEnrollId=a.StuEnrollId AND Actual<>999.0 and Actual<>9999.0 AND Actual <> 0.00)as LDA, ")
                    .Append("syCampGrps.CampGrpDescrip,F.CampDescrip, ")
                    .Append("(Select UnitTypeId from arPrgVersions PV where PV.PrgVerId in ")
                    .Append("(Select PrgverId from arStuEnrollments  where StuEnrollId=A.StuEnrollid)) as AttTypeID, ")
                    .Append("'" & strSuppressDate & "' as SuppressDate ")
                    '.Append(", ")
                    '.Append("(SELECT Descrip FROM arreqs WHERE ReqId IN  ")
                    '.Append("(SELECT ReqId FROM dbo.arClassSections WHERE ClsSectionId = A.ClsSectionId)) + ' (' + ")
                    '.Append("(SELECT ClsSection FROM arClassSections WHERE ClsSectionId = A.ClsSectionId) + ') - ' + ")
                    '.Append("(SELECT InstructionTypeDescrip FROM arInstructionType WHERE InstructionTypeId = ")
                    '.Append("(SELECT InstructionTypeId FROM arClsSectMeetings WHERE ClsSectMeetingId = ")
                    '.Append("(SELECT ClsSectMeetingId FROM atClsSectAttendance WHERE ClsSectAttId = A.ClsSectAttId))) AS ClassSection ")
                    .Append("FROM ")
                    .Append("atClsSectAttendance A ,arstudent,arStuEnrollments, syCmpGrpCmps E,syCampuses F,syCampGrps,syCampuses  ")

                    If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                        .Append(",adLeadByLeadGroups, adLeadGroups ")
                    End If

                    .Append("WHERE ")
                    .Append("(CONVERT(Date,MeetDate, 101))>='" + StrWeekStartDate + "' and (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "')  ")
                    .Append("and A.StuEnrollid=arStuEnrollments.StuEnrollid and arstudent.StudentId=arStuEnrollments.StudentId ")
                    .Append("AND arStuEnrollments.CampusId=F.CampusId ")
                    .Append("AND F.CampusId=E.CampusId AND E.CampGrpId = syCampGrps.CampGrpId AND arStuEnrollments.CampusId=syCampuses.CampusId ")

                    If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                        .Append("AND adLeadByLeadGroups.LeadGrpId=adLeadGroups.LeadGrpId ")
                        .Append("AND adLeadByLeadGroups.StuEnrollId=arStuEnrollments.StuEnrollId ")
                    End If

                    .Append(strWhere)

                    .Append(") B ")
                    .Append("GROUP BY B.StuEnrollId,B.StudentName, B.StudentIdentifier, B.CampGrpDescrip, B.CampDescrip, ")
                    .Append("B.Mon, B.Tue, B.Wed, B.Thur, B.Fri, B.Sat, B.Sun, B.WeekTotal, B.NewTotal, B.NewTotalSched, B.NewTotalDaysAbsent, ")
                    .Append("B.NewTotalMakeUpDays, B.NewNetDaysAbsent, B.NewSchedDaysatLDA, B.LDA, B.AttTypeID, B.SuppressDate) C ")
                    .Append("GROUP BY ")
                    .Append("C.StuEnrollId, C.StudentIdentifier, C.StudentName, C.LDA, C.CampGrpDescrip, C.AttTypeID, C.SuppressDate ")

                End With

            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()
            ds = db.RunParamSQLDataSet(sb.ToString)

            Dim dtGetWeeklyAttend_Sub As DataTable = GetWeeklyAttendance_Sub(paramInfo)
            dtGetWeeklyAttend_Sub.TableName = "dtGetWeeklyAttend_Sub"

            ds.Tables.Add(dtGetWeeklyAttend_Sub.Copy())

        Catch ex As Exception
            Return ds
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try
        Return ds
    End Function

    Public Function GetWeeklyAttendance_Sub(ByVal paramInfo As ReportParamInfo) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim dt As New DataTable
        Dim ds As New DataSet
        Dim strWhere As String = ""
        Dim strOrderBy As String = ""
        Dim strFrom As String = ""
        Dim strStudentId As String = ""
        Try

            If MyAdvAppSettings.AppSettings("TrackSapAttendance", paramInfo.CampusId).ToString.ToLower = "byclass" Then

                If MyAdvAppSettings.AppSettings("TimeClockClassSection", paramInfo.CampusId).ToString.ToLower = "yes" Then

                    If paramInfo.FilterList <> "" Then
                        strWhere &= " AND " & paramInfo.FilterList

                        If MyAdvAppSettings.AppSettings("TrackSapAttendance", paramInfo.CampusId).ToString.ToLower = "byclass" Then
                            strWhere = Replace(strWhere, "syCampuses.CampusId", "F.CampusId")
                        End If



                    End If
                    Dim arrValues() As String
                    Dim strFromDate As String
                    Dim StrWeekStartDate As String = Date.Now
                    If paramInfo.FilterOther <> "" Then
                        arrValues = paramInfo.FilterOther.Split(";")
                        If arrValues.Length > 0 Then
                            strFromDate = arrValues(1).ToString
                            ' set the start date to the filter start date if it was passed in
                            Dim tDate As DateTime = Date.Now
                            If strFromDate <> "" Then
                                tDate = CType(strFromDate, Date)
                                ' Find the first date of the week
                                While tDate.DayOfWeek <> DayOfWeek.Monday
                                    tDate = tDate.AddDays(-1)
                                End While
                            End If
                            StrWeekStartDate = tDate ' set it to the first sunday     
                        End If
                    End If

                    With sb
                        .Append("SELECT H.StuEnrollId, SUM(H.WeekTotal) AS WeekTotal, SUM(H.NewTotal) AS NewTotal, H.InstructionType ")
                        .Append("FROM ")
                        .Append("(SELECT G.* ")
                        .Append("FROM ")
                        .Append("(SELECT  ")
                        .Append("A.StuEnrollId, ")
                        .Append("isnull((Select Sum(Actual) from atClsSectAttendance where (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "') and (CONVERT(Date,MeetDate, 101))>=DateAdd(Day,0,'" + StrWeekStartDate + "') ")
                        .Append("and StuEnrollId=a.StuEnrollId and Actual<>999.0 and Actual<>9999.0 AND ClsSectionId = A.ClsSectionId),0.00)as WeekTotal, ")
                        .Append("isnull((Select Sum(Actual) from atClsSectAttendance where (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId  and Actual<>999.0 and Actual<>9999.0 ")
                        .Append("AND ClsSectionId = A.ClsSectionId),0.00) as NewTotal, ")
                        .Append("(SELECT InstructionTypeDescrip FROM arInstructionType WHERE InstructionTypeId =  ")
                        .Append("(SELECT InstructionTypeId FROM arClsSectMeetings WHERE ClsSectMeetingId = ")
                        .Append("(SELECT ClsSectMeetingId FROM atClsSectAttendance WHERE ClsSectAttId = A.ClsSectAttId))) AS InstructionType ")
                        .Append("FROM atClsSectAttendance A,arstudent,arStuEnrollments, syCmpGrpCmps E,syCampuses F,syCampGrps,syCampuses ")
                        .Append("WHERE ")
                        '.Append("(CONVERT(Date,MeetDate, 101))>='" + StrWeekStartDate + "' and (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "')  and ")
                        .Append("A.StuEnrollid=arStuEnrollments.StuEnrollid  ")
                        .Append("and arstudent.StudentId=arStuEnrollments.StudentId AND arStuEnrollments.CampusId=F.CampusId AND F.CampusId=E.CampusId  ")
                        .Append("AND E.CampGrpId = syCampGrps.CampGrpId AND arStuEnrollments.CampusId=syCampuses.CampusId ")
                        .Append(strWhere)
                        .Append(") G ")
                        .Append(" GROUP BY ")
                        .Append("G.StuEnrollId, G.WeekTotal, G.NewTotal, G.InstructionType) H ")
                        .Append("GROUP BY H.StuEnrollId, H.InstructionType")
                    End With
                Else
                    With sb
                        .Append("SELECT '' AS StuEnrollId, '' AS WeekTotal, '' AS NewTotal, '' AS InstructionType")
                    End With
                End If
            Else
                With sb
                    .Append("SELECT '' AS StuEnrollId, '' AS WeekTotal, '' AS NewTotal, '' AS InstructionType")
                End With
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()
            ds = db.RunParamSQLDataSet(sb.ToString)

            dt = ds.Tables(0)

        Catch ex As Exception
            Return dt
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try
        Return dt

    End Function

    ' US2729 12/23/2011 Janet Robinson add new method for single student weekly attendance report
    Public Function GetWeeklyAttendance_Single(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String = ""
        Dim strOrderBy As String = ""
        Dim strFrom As String = ""
        Dim strStudentId As String = ""
        Dim strSingleStudent As String = ""
        Try



            Dim arrValues() As String
            Dim strFromDate As String
            Dim StrWeekStartDate As String = Date.Now
            If paramInfo.FilterOther <> "" Then
                arrValues = paramInfo.FilterOther.Split(";")
                If arrValues.Length > 0 Then
                    ' US2729 12/23/2011 Janet Robinson add resourceid 771
                    ' DE7165 2/9/2012 Janet Robinson resid s/b 779
                    If paramInfo.ResId = 779 Then
                        strSingleStudent = " AND " & arrValues(0).ToString
                        strFromDate = arrValues(2).ToString
                    Else
                        strFromDate = arrValues(1).ToString
                    End If
                    ' set the start date to the filter start date if it was passed in
                    Dim tDate As DateTime = Date.Now
                    If strFromDate <> "" Then
                        tDate = CType(strFromDate, Date)
                        ' Find the first date of the week
                        While tDate.DayOfWeek <> DayOfWeek.Monday
                            tDate = tDate.AddDays(-1)
                        End While
                    End If
                    StrWeekStartDate = tDate ' set it to the first sunday    

                End If
            End If


            'If paramInfo.OrderBy <> "" Then
            '    strOrderBy &= "," & paramInfo.OrderBy
            'End If

            If StudentIdentifier = "SSN" Then
                strStudentId = "arStudent.SSN AS StudentIdentifier,"
            ElseIf StudentIdentifier = "EnrollmentId" Then
                strStudentId = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
            ElseIf StudentIdentifier = "StudentId" Then
                strStudentId = "arStudent.StudentNumber AS StudentIdentifier,"
            End If
            Dim strGroupby As String = ""
            If StudentIdentifier = "SSN" Then
                strGroupby = "arStudent.SSN "
            ElseIf StudentIdentifier = "EnrollmentId" Then
                strGroupby = "arStuEnrollments.EnrollmentId "
            ElseIf StudentIdentifier = "StudentId" Then
                strGroupby = "arStudent.StudentNumber "
            End If

            Dim strSuppressDate As String = "no"
            Try
                strSuppressDate = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
            Catch ex As System.Exception
                strSuppressDate = "no"
            End Try

            If MyAdvAppSettings.AppSettings("TrackSapAttendance", paramInfo.CampusId).ToString.ToLower = "byday" Then



                With sb

                    ''---------
                    ''Attendance unit Type Added

                    .Append(" Select A.StuEnrollId, ")
                    .Append(strStudentId)
                    .Append(" IsNull((Select LastName from arStudent where StudentId in (Select studentId from ")
                    .Append(" arStuEnrollments where StuEnrollId=a.StuEnrollid)),' ') +','+ ")
                    .Append(" isNull((Select Firstname from arStudent where StudentId in (Select studentId from ")
                    .Append(" arStuEnrollments where StuEnrollId=a.StuEnrollid)),' ')+ ' '+ ")
                    .Append(" isNull((Select MiddleName from arStudent where StudentId in (Select studentId from ")
                    .Append(" arStuEnrollments where StuEnrollId=a.StuEnrollid)),' ') + ' ' as StudentName, ")
                    .Append(" dbo.GetWeeklyAtt(a.StuEnrollid,DateAdd(Day,6,'" + StrWeekStartDate + "'),DateAdd(Day,0,'" + StrWeekStartDate + "')) as Mon,")
                    .Append(" dbo.GetWeeklyAtt(a.StuEnrollid,DateAdd(Day,6,'" + StrWeekStartDate + "'),DateAdd(Day,1,'" + StrWeekStartDate + "')) as Tue,")
                    .Append(" dbo.GetWeeklyAtt(a.StuEnrollid,DateAdd(Day,6,'" + StrWeekStartDate + "'),DateAdd(Day,2,'" + StrWeekStartDate + "')) as Wed,")
                    .Append(" dbo.GetWeeklyAtt(a.StuEnrollid,DateAdd(Day,6,'" + StrWeekStartDate + "'),DateAdd(Day,3,'" + StrWeekStartDate + "')) as Thur,")
                    .Append(" dbo.GetWeeklyAtt(a.StuEnrollid,DateAdd(Day,6,'" + StrWeekStartDate + "'),DateAdd(Day,4,'" + StrWeekStartDate + "')) as Fri,")
                    .Append(" dbo.GetWeeklyAtt(a.StuEnrollid,DateAdd(Day,6,'" + StrWeekStartDate + "'),DateAdd(Day,5,'" + StrWeekStartDate + "')) as Sat,")
                    .Append(" dbo.GetWeeklyAtt(a.StuEnrollid,DateAdd(Day,6,'" + StrWeekStartDate + "'),DateAdd(Day,6,'" + StrWeekStartDate + "')) as Sun,")
                    '4/23/2012'),'4/23/12') AS Mon")



                    '.Append(" (Select ActualHours from arStudentClockAttendance ")
                    '.Append("  where StuEnrollId=A.StuEnrollId and CONVERT(Date,RecordDate,101)='" + StrWeekStartDate + "'")
                    ' ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ' ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ' ''The attendance details related to the previous schedules are also displayed
                    ''.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    '.Append(" and ActualHours<>999.0 and ActualHours<>9999.0)as Mon  ")
                    '.Append(" ,(Select ActualHours from arStudentClockAttendance ")
                    '.Append("  where StuEnrollId=A.StuEnrollId and CONVERT(Date,RecordDate,101)=DateAdd(Day,1,'" + StrWeekStartDate + "') ")
                    ' ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ' ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ' ''The attendance details related to the previous schedules are also displayed
                    ''.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    '.Append(" and ActualHours<>999.0 and ActualHours<>9999.0)as Tue  ")
                    '.Append(" ,(Select ActualHours from arStudentClockAttendance ")
                    '.Append("  where StuEnrollId=A.StuEnrollId and CONVERT(Date,RecordDate,101)=DateAdd(Day,2,'" + StrWeekStartDate + "') ")
                    ' ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ' ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ' ''The attendance details related to the previous schedules are also displayed
                    ''.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    '.Append(" and ActualHours<>999.0 and ActualHours<>9999.0)as Wed ")
                    '.Append(" ,(Select ActualHours from arStudentClockAttendance ")
                    '.Append("  where StuEnrollId=A.StuEnrollId and CONVERT(Date,RecordDate,101)=DateAdd(Day,3,'" + StrWeekStartDate + "') ")
                    ' ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ' ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ' ''The attendance details related to the previous schedules are also displayed
                    '' .Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    '.Append(" and ActualHours<>999.0 and ActualHours<>9999.0)as Thur  ")
                    '.Append(" ,(Select ActualHours from arStudentClockAttendance ")
                    '.Append("  where StuEnrollId=A.StuEnrollId and CONVERT(Date,RecordDate,101)=DateAdd(Day,4,'" + StrWeekStartDate + "') ")
                    ' ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ' ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ' ''The attendance details related to the previous schedules are also displayed
                    ''.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    '.Append(" and ActualHours<>999.0 and ActualHours<>9999.0)as Fri  ")
                    '.Append(" ,(Select ActualHours from arStudentClockAttendance ")
                    '.Append("  where StuEnrollId=A.StuEnrollId and CONVERT(Date,RecordDate,101)=DateAdd(Day,5,'" + StrWeekStartDate + "') ")
                    ' ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ' ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ' ''The attendance details related to the previous schedules are also displayed
                    ''.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    '.Append(" and ActualHours<>999.0 and ActualHours<>9999.0)as Sat  ")
                    '.Append(" ,(Select ActualHours from arStudentClockAttendance ")
                    '.Append("  where StuEnrollId=A.StuEnrollId and CONVERT(Date,RecordDate,101)=DateAdd(Day,6,'" + StrWeekStartDate + "') ")
                    ' ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ' ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ' ''The attendance details related to the previous schedules are also displayed
                    ''.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    '.Append(" and ActualHours<>999.0 and ActualHours<>9999.0)as Sun ")

                    .Append(" (isnull((Select Sum(ActualHours) from arStudentClockAttendance where   CONVERT(Date,RecordDate,101)<=DateAdd(Day,6,'" + StrWeekStartDate + "') and  ")
                    .Append(" CONVERT(Date,RecordDate,101)>=DateAdd(Day,0,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId   ")
                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    '  .Append(" and ScheduleId in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId)")
                    .Append("   and ActualHours<>999.0 and ActualHours<>9999.0),0.00) - dbo.GetAbsentHoursFromTardyBetweenDates(A.StuEnrollid,DateAdd(Day,0,'" + StrWeekStartDate + "'),DateAdd(Day,6,'" + StrWeekStartDate + "'))) as WeekTotal")

                    ''   .Append(" ,sum(ActualHours) as WeekTotal ")
                    .Append(" ,(isnull((Select Sum(ActualHours) from arStudentClockAttendance where ")
                    .Append("  CONVERT(Date,RecordDate,101)<=DateAdd(Day,6,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId ")
                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    ' .Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    .Append(" and ActualHours<>999.0 and ActualHours<>9999.0),0.00) - dbo.GetAbsentHoursFromTardyForByDay(A.StuEnrollid,DateAdd(Day,6,'" + StrWeekStartDate + "')))as NewTotal ")




                    .Append(" ,isnull((Select Sum(SchedHours) from arStudentClockAttendance where ")
                    .Append("  CONVERT(Date,RecordDate,101)<=DateAdd(Day,6,'" + StrWeekStartDate + "')  and StuEnrollId=a.StuEnrollId ")
                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    '.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    .Append(" and ActualHours<>999.0 and ActualHours<>9999.0),0.00)as NewTotalSched  ")
                    .Append(" ,(isnull((Select Sum(SchedHours- ActualHours) from arStudentClockAttendance where ")
                    .Append("  CONVERT(Date,RecordDate,101)<=DateAdd(Day,6,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId ")
                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    '.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    .Append(" and ActualHours<>999.0 and ActualHours<>9999.0 and SchedHours is not null and SchedHours<>0 and  ")
                    .Append(" ActualHours is not null and SchedHours>ActualHours),0.00) + dbo.GetAbsentHoursFromTardyForByDay(A.StuEnrollid,DateAdd(Day,6,'" + StrWeekStartDate + "'))) as NewTotalDaysAbsent ")
                    .Append(" ,isnull((Select Sum(ActualHours- SchedHours) from arStudentClockAttendance where ")
                    .Append("  CONVERT(Date,RecordDate,101)<=DateAdd(Day,6,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId ")

                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    '.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    .Append(" and ActualHours<>999.0 and ActualHours<>9999.0 and SchedHours is not null  and  ")
                    .Append(" ActualHours is not null and ActualHours<>0 and SchedHours<ActualHours),0.00)as NewTotalMakeUpDays ")
                    .Append(" ,isnull(((Select Sum(SchedHours- ActualHours) from arStudentClockAttendance where ")
                    .Append("  CONVERT(Date,RecordDate,101)<=DateAdd(Day,6,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId ")

                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    '.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    .Append(" and ActualHours<>999.0 and ActualHours<>9999.0 and SchedHours is not null and SchedHours<>0 and  ")
                    .Append(" ActualHours is not null and SchedHours>ActualHours) + dbo.GetAbsentHoursFromTardyForByDay(A.StuEnrollid,DateAdd(Day,6,'" + StrWeekStartDate + "')) - ")
                    .Append("isnull((Select Sum(ActualHours- SchedHours) from arStudentClockAttendance where ")
                    .Append("  CONVERT(Date,RecordDate,101)<=DateAdd(Day,6,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId ")
                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    ' .Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    .Append(" and ActualHours<>999.0 and ActualHours<>9999.0 and SchedHours is not null  and  ")
                    .Append(" ActualHours is not null and ActualHours<>0 and SchedHours<ActualHours),0.00)),0.00) ")
                    .Append("  as NewNetDaysAbsent ")
                    .Append(" ,isnull((Select Sum(SchedHours) from arStudentClockAttendance where ")
                    .Append("  StuEnrollId=a.StuEnrollId ")
                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    '.Append(" and ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= A.StuEnrollId) ")
                    .Append(" and ActualHours<>999.0 and ActualHours<>9999.0 AND RecordDate <= dbo.GetLDAFromAttendance(A.StuEnrollId)),0.00) as NewSchedDaysatLDA ")

                    '.Append(" ,(Select Convert(varchar,max(RecordDate),101) from arStudentClockAttendance where  ")
                    .Append(",dbo.GetLDAFromAttendance(A.StuEnrollId) AS LDA, '' AS CampGrpDescrip, '' AS CampDescrip ")
                    ''Added by Saraswathi to find the unit type (hours or Days)
                    .Append(" ,(Select UnitTypeId from arPrgVersions PV where PV.PrgVerId in (Select PrgverId from arStuEnrollments  where StuEnrollId=A.StuEnrollid)) as AttTypeID, ")
                    .Append("'" & strSuppressDate & "'")
                    .Append(" as SuppressDate ")
                    .Append(", '' as ClassSection ")
                    .Append(" from arStudentClockAttendance A ,arstudent,arStuEnrollments  ")
                    '.Append(" from arStudentClockAttendance A ,arstudent,arStuEnrollments , syCmpGrpCmps E,syCampuses F,syCampGrps,syCampuses ")
                    If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                        .Append(",adLeadByLeadGroups, adLeadGroups ")
                    End If
                    .Append(" where CONVERT(Date,RecordDate,101)>='" + StrWeekStartDate + "' and CONVERT(Date,RecordDate,101)<=DateAdd(Day,6,'" + StrWeekStartDate + "') AND ")
                    .Append("A.ActualHours <> 9999.00 AND ")
                    .Append(" A.StuEnrollid=arStuEnrollments.StuEnrollid and arstudent.StudentId=arStuEnrollments.StudentId ")
                    .Append(strSingleStudent)
                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    ' .Append(" and A.ScheduleId  in (Select ScheduleId from arStudentSchedules where StuENrollId= arSTuEnrollments.StuEnrollId) ")
                    '.Append("  AND arStuEnrollments.CampusId=F.CampusId  ")
                    '.Append("   AND F.CampusId=E.CampusId AND    E.CampGrpId = syCampGrps.CampGrpId  AND arStuEnrollments.CampusId=syCampuses.CampusId ")


                    If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                        .Append("AND adLeadByLeadGroups.LeadGrpId=adLeadGroups.LeadGrpId ")
                        .Append("AND adLeadByLeadGroups.StuEnrollId=arStuEnrollments.StuEnrollId ")
                    End If

                    .Append(strWhere)

                    .Append("   Group By  ")
                    .Append(" A.StuEnrollId,")
                    .Append(strGroupby)
                    '.Append("CampGrpDescrip,CampDescrip")


                End With

            Else
                'This is for Class Sections. Used when "TrackSapAttendance" = "byclass"


                With sb
                    .Append("SELECT ")
                    .Append("C.StuEnrollId, C.StudentIdentifier, C.StudentName, SUM(C.Mon) AS Mon, SUM(C.Tue) AS Tue, SUM(C.Wed) AS Wed, SUM(C.Thur) AS Thur, SUM(C.Fri)AS Fri, ")
                    .Append("SUM(C.Sat) AS Sat, SUM(C.Sun) AS Sun, SUM(C.WeekTotal) AS WeekTotal, SUM(C.NewTotal) AS NewTotal, SUM(C.NewTotalSched) AS NewTotalSched, SUM(C.NewTotalDaysAbsent) AS NewTotalDaysAbsent, ")
                    .Append("SUM(C.NewTotalMakeUpDays) AS NewTotalMakeUpDays, SUM(C.NewNetDaysAbsent) AS NewNetDaysAbsent, SUM(C.NewSchedDaysatLDA) AS NewSchedDaysatLDA, C.LDA, ")
                    '.Append("C.AttTypeID, C.SuppressDate ")
                    .Append("C.CampGrpDescrip, C.AttTypeID, C.SuppressDate ")
                    .Append("FROM ")
                    .Append("(SELECT B.* ")
                    .Append("FROM ")
                    .Append("(SELECT A.StuEnrollId, ")
                    .Append(strStudentId)
                    .Append("IsNull((Select LastName from arStudent where StudentId in  ")
                    .Append("(Select studentId from  arStuEnrollments where StuEnrollId=a.StuEnrollid)),' ') +','+ ")
                    .Append("isNull((Select Firstname from arStudent where StudentId in  ")
                    .Append("(Select studentId from  arStuEnrollments where StuEnrollId=a.StuEnrollid)),' ')+ ' '+ ")
                    .Append("isNull((Select MiddleName from arStudent where StudentId in ")
                    .Append("(Select studentId from  arStuEnrollments where StuEnrollId=a.StuEnrollid)),' ') + ' ' as StudentName, ")
                    .Append("(Select SUM(Actual) from atClsSectAttendance where StuEnrollId=A.StuEnrollId and CONVERT(Date,MeetDate,101)='" + StrWeekStartDate + "' ")
                    .Append("and Actual<>999.0 and Actual<>9999.0)as Mon, ")
                    .Append("(Select SUM(Actual) from atClsSectAttendance where StuEnrollId=A.StuEnrollId and CONVERT(Date,MeetDate,101)=DateAdd(Day,1,'" + StrWeekStartDate + "') ")
                    .Append("and Actual<>999.0 and Actual<>9999.0)as Tue, ")
                    .Append("(Select SUM(Actual) from atClsSectAttendance where StuEnrollId=A.StuEnrollId and CONVERT(Date,MeetDate,101)=DateAdd(Day,2,'" + StrWeekStartDate + "') ")
                    .Append("and Actual<>999.0 and Actual<>9999.0)as Wed, ")
                    .Append("(Select SUM(Actual) from atClsSectAttendance where StuEnrollId=A.StuEnrollId and CONVERT(Date,MeetDate,101)=DateAdd(Day,3,'" + StrWeekStartDate + "') ")
                    .Append("and Actual<>999.0 and Actual<>9999.0)as Thur, ")
                    .Append("(Select SUM(Actual) from atClsSectAttendance where StuEnrollId=A.StuEnrollId and CONVERT(Date,MeetDate,101)=DateAdd(Day,4,'" + StrWeekStartDate + "') ")
                    .Append("and Actual<>999.0 and Actual<>9999.0)as Fri, ")
                    .Append("(Select SUM(Actual) from atClsSectAttendance where StuEnrollId=A.StuEnrollId and CONVERT(Date,MeetDate,101)=DateAdd(Day,5,'" + StrWeekStartDate + "') ")
                    .Append("and Actual<>999.0 and Actual<>9999.0)as Sat, ")
                    .Append("(Select SUM(Actual) from atClsSectAttendance where StuEnrollId=A.StuEnrollId and CONVERT(Date,MeetDate,101)=DateAdd(Day,6,'" + StrWeekStartDate + "') ")
                    .Append("and Actual<>999.0 and Actual<>9999.0)as Sun, ")
                    .Append("isnull((Select Sum(Actual) from atClsSectAttendance where (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "') ")
                    .Append("and (CONVERT(Date,MeetDate, 101))>=DateAdd(Day,0,'" + StrWeekStartDate + "')and StuEnrollId=a.StuEnrollId and Actual<>999.0 and Actual<>9999.0),0.00)as WeekTotal, ")
                    .Append("isnull((Select Sum(Actual) from atClsSectAttendance where (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "') ")
                    .Append("and StuEnrollId=a.StuEnrollId  and Actual<>999.0 and Actual<>9999.0),0.00) as NewTotal, ")
                    .Append("isnull((Select Sum(Scheduled) from atClsSectAttendance where (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId ")
                    .Append("and Actual<>999.0 and Actual<>9999.0),0.00)as NewTotalSched, ")
                    .Append("isnull((Select Sum(Scheduled- Actual) from atClsSectAttendance where (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId ")
                    .Append("and Actual<>999.0 and Actual<>9999.0 and Scheduled is not null ")
                    .Append("and Scheduled<>0 and Actual is not null and Scheduled>Actual),0.00)as NewTotalDaysAbsent, ")
                    .Append("isnull((Select Sum(Actual- Scheduled) from atClsSectAttendance where (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId ")
                    .Append("and Actual<>999.0 and Actual<>9999.0 and Scheduled is not null and Actual is not null ")
                    .Append("and Actual<>0 and Scheduled<Actual),0.00)as NewTotalMakeUpDays, ")
                    .Append("isnull(((Select Sum(Scheduled- Actual) from atClsSectAttendance where (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId ")
                    .Append("and Actual<>999.0 and Actual<>9999.0 and Scheduled is not null and Scheduled<>0 and Actual is not null ")
                    .Append("and Scheduled>Actual) -  (Select Sum(Actual- Scheduled) from atClsSectAttendance where (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "') ")
                    .Append("and StuEnrollId=a.StuEnrollId  and Actual<>999.0 and Actual<>9999.0 and Scheduled is not null and  ")
                    .Append("Actual is not null and Actual<>0 and Scheduled<Actual)),0.00) as NewNetDaysAbsent, ")
                    .Append("isnull((Select Sum(Scheduled) from atClsSectAttendance where StuEnrollId=a.StuEnrollId ")
                    .Append("and Actual<>999.0 and Actual<>9999.0   AND CONVERT(DATE, MeetDate, 101) <= dbo.GetLDAFromAttendance(A.StuEnrollId)),0.00)as NewSchedDaysatLDA, ")
                    .Append("dbo.GetLDAFromAttendance(A.StuEnrollId) AS LDA, ")
                    .Append(" '' AS CampGrpDescrip, '' AS CampDescrip, ")
                    .Append("(Select UnitTypeId from arPrgVersions PV where PV.PrgVerId in ")
                    .Append("(Select PrgverId from arStuEnrollments  where StuEnrollId=A.StuEnrollid)) as AttTypeID, ")
                    .Append("'" & strSuppressDate & "' as SuppressDate ")
                    '.Append(", ")
                    '.Append("(SELECT Descrip FROM arreqs WHERE ReqId IN  ")
                    '.Append("(SELECT ReqId FROM dbo.arClassSections WHERE ClsSectionId = A.ClsSectionId)) + ' (' + ")
                    '.Append("(SELECT ClsSection FROM arClassSections WHERE ClsSectionId = A.ClsSectionId) + ') - ' + ")
                    '.Append("(SELECT InstructionTypeDescrip FROM arInstructionType WHERE InstructionTypeId = ")
                    '.Append("(SELECT InstructionTypeId FROM arClsSectMeetings WHERE ClsSectMeetingId = ")
                    '.Append("(SELECT ClsSectMeetingId FROM atClsSectAttendance WHERE ClsSectAttId = A.ClsSectAttId))) AS ClassSection ")
                    .Append("FROM ")
                    .Append("atClsSectAttendance A ,arstudent,arStuEnrollments  ")
                    '.Append("atClsSectAttendance A ,arstudent,arStuEnrollments, syCmpGrpCmps E,syCampuses F,syCampGrps,syCampuses  ")
                    .Append("WHERE ")
                    .Append("(CONVERT(Date,MeetDate, 101))>='" + StrWeekStartDate + "' and (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "')  ")
                    .Append("and A.StuEnrollid=arStuEnrollments.StuEnrollid and arstudent.StudentId=arStuEnrollments.StudentId ")
                    .Append(strSingleStudent)
                    '.Append("AND arStuEnrollments.CampusId=F.CampusId ")
                    '.Append("AND F.CampusId=E.CampusId AND E.CampGrpId = syCampGrps.CampGrpId AND arStuEnrollments.CampusId=syCampuses.CampusId ")

                    If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                        .Append("AND adLeadByLeadGroups.LeadGrpId=adLeadGroups.LeadGrpId ")
                        .Append("AND adLeadByLeadGroups.StuEnrollId=arStuEnrollments.StuEnrollId ")
                    End If

                    .Append(strWhere)

                    .Append(") B ")
                    .Append("GROUP BY B.StuEnrollId,B.StudentName, B.StudentIdentifier, B.CampGrpDescrip, B.CampDescrip, ")
                    .Append("B.Mon, B.Tue, B.Wed, B.Thur, B.Fri, B.Sat, B.Sun, B.WeekTotal, B.NewTotal, B.NewTotalSched, B.NewTotalDaysAbsent, ")
                    .Append("B.NewTotalMakeUpDays, B.NewNetDaysAbsent, B.NewSchedDaysatLDA, B.LDA, B.AttTypeID, B.SuppressDate) C ")
                    .Append("GROUP BY ")
                    .Append("C.StuEnrollId, C.StudentIdentifier, C.StudentName, C.LDA, C.CampGrpDescrip, C.AttTypeID, C.SuppressDate ")

                End With

            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()
            ds = db.RunParamSQLDataSet(sb.ToString)

            Dim dtGetWeeklyAttend_Sub As DataTable = GetWeeklyAttendance_Sub_Single(paramInfo)
            dtGetWeeklyAttend_Sub.TableName = "dtGetWeeklyAttend_Sub"

            ds.Tables.Add(dtGetWeeklyAttend_Sub.Copy())


        Catch ex As Exception
            Return ds
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try
        Return ds
    End Function

    ' US2729 12/23/2011 Janet Robinson add new method for single student weekly attendance report
    Public Function GetWeeklyAttendance_Sub_Single(ByVal paramInfo As ReportParamInfo) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim dt As New DataTable
        Dim ds As New DataSet
        Dim strWhere As String = ""
        Dim strOrderBy As String = ""
        Dim strFrom As String = ""
        Dim strStudentId As String = ""
        Dim strSingleStudent As String = ""
        Try

            If MyAdvAppSettings.AppSettings("TrackSapAttendance", paramInfo.CampusId).ToString.ToLower = "byclass" Then

                If MyAdvAppSettings.AppSettings("TimeClockClassSection", paramInfo.CampusId).ToString.ToLower = "yes" Then


                    Dim arrValues() As String
                    Dim strFromDate As String
                    Dim StrWeekStartDate As String = Date.Now
                    If paramInfo.FilterOther <> "" Then
                        arrValues = paramInfo.FilterOther.Split(";")
                        If arrValues.Length > 0 Then
                            ' US2729 12/23/2011 Janet Robinson add resourceid 771
                            ' DE7165 2/9/2012 Janet Robinson resid s/b 779
                            If paramInfo.ResId = 779 Then
                                strSingleStudent = " AND " & arrValues(0).ToString
                                strFromDate = arrValues(2).ToString
                            Else
                                strFromDate = arrValues(1).ToString
                            End If
                            ' set the start date to the filter start date if it was passed in
                            Dim tDate As DateTime = Date.Now
                            If strFromDate <> "" Then
                                tDate = CType(strFromDate, Date)
                                ' Find the first date of the week
                                While tDate.DayOfWeek <> DayOfWeek.Monday
                                    tDate = tDate.AddDays(-1)
                                End While
                            End If
                            StrWeekStartDate = tDate ' set it to the first sunday    

                        End If
                    End If

                    With sb
                        .Append("SELECT H.StuEnrollId, SUM(H.WeekTotal) AS WeekTotal, SUM(H.NewTotal) AS NewTotal, H.InstructionType ")
                        .Append("FROM ")
                        .Append("(SELECT G.* ")
                        .Append("FROM ")
                        .Append("(SELECT  ")
                        .Append("A.StuEnrollId, ")
                        .Append("isnull((Select Sum(Actual) from atClsSectAttendance where (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "') and (CONVERT(Date,MeetDate, 101))>=DateAdd(Day,0,'" + StrWeekStartDate + "') ")
                        .Append("and StuEnrollId=a.StuEnrollId and Actual<>999.0 and Actual<>9999.0 AND ClsSectionId = A.ClsSectionId),0.00)as WeekTotal, ")
                        .Append("isnull((Select Sum(Actual) from atClsSectAttendance where (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "') and StuEnrollId=a.StuEnrollId  and Actual<>999.0 and Actual<>9999.0 ")
                        .Append("AND ClsSectionId = A.ClsSectionId),0.00) as NewTotal, ")
                        .Append("(SELECT InstructionTypeDescrip FROM arInstructionType WHERE InstructionTypeId =  ")
                        .Append("(SELECT InstructionTypeId FROM arClsSectMeetings WHERE ClsSectMeetingId = ")
                        .Append("(SELECT ClsSectMeetingId FROM atClsSectAttendance WHERE ClsSectAttId = A.ClsSectAttId))) AS InstructionType ")
                        .Append("FROM atClsSectAttendance A,arstudent,arStuEnrollments ")
                        '.Append("FROM atClsSectAttendance A,arstudent,arStuEnrollments, syCmpGrpCmps E,syCampuses F,syCampGrps,syCampuses ")
                        .Append("WHERE ")
                        '.Append("(CONVERT(Date,MeetDate, 101))>='" + StrWeekStartDate + "' and (CONVERT(Date,MeetDate, 101))<=DateAdd(Day,6,'" + StrWeekStartDate + "')  and ")
                        .Append("A.StuEnrollid=arStuEnrollments.StuEnrollid  ")
                        .Append("and arstudent.StudentId=arStuEnrollments.StudentId  ")
                        .Append(strSingleStudent)
                        '.Append("and arstudent.StudentId=arStuEnrollments.StudentId AND arStuEnrollments.CampusId=F.CampusId AND F.CampusId=E.CampusId  ")
                        '.Append("AND E.CampGrpId = syCampGrps.CampGrpId AND arStuEnrollments.CampusId=syCampuses.CampusId ")
                        .Append(strWhere)
                        .Append(") G ")
                        .Append(" GROUP BY ")
                        .Append("G.StuEnrollId, G.WeekTotal, G.NewTotal, G.InstructionType) H ")
                        .Append("GROUP BY H.StuEnrollId, H.InstructionType")
                    End With
                Else
                    With sb
                        .Append("SELECT '' AS StuEnrollId, '' AS WeekTotal, '' AS NewTotal, '' AS InstructionType")
                    End With
                End If

            Else
                With sb
                    .Append("SELECT '' AS StuEnrollId, '' AS WeekTotal, '' AS NewTotal, '' AS InstructionType")
                End With
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()
            ds = db.RunParamSQLDataSet(sb.ToString)

            dt = ds.Tables(0)

        Catch ex As Exception
            Return dt
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try
        Return dt

    End Function

#End Region

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

End Class
