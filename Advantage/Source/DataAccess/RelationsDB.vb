Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' RelationsDB.vb
'
' RelationsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class RelationsDB
    Public Function GetAllRelations() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("	      (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("	      CCT.RelationId, ")
            .Append("	      CCT.StatusId, ")
            .Append("	      CCT.RelationCode, ")
            .Append("	      CCT.RelationDescrip ")
            .Append("FROM     syRelations CCT, syStatuses ST ")
            .Append("WHERE    CCT.StatusId = ST.StatusId ")
            .Append("ORDER BY ST.Status,CCT.RelationDescrip asc")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetRelationInfo(ByVal RelationId As String) As RelationInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT CCT.RelationId, ")
            .Append("    CCT.RelationCode, ")
            .Append("    CCT.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=CCT.StatusId) As Status, ")
            .Append("    CCT.RelationDescrip, ")
            .Append("    CCT.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=CCT.CampGrpId) As CampGrpDescrip, ")
            .Append("    CCT.ModUser, ")
            .Append("    CCT.ModDate ")
            .Append("FROM  syRelations CCT ")
            .Append("WHERE CCT.RelationId= ? ")
        End With

        ' Add the RelationId to the parameter list
        db.AddParameter("@RelationId", RelationId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim RelationInfo As New RelationInfo

        While dr.Read()

            '   set properties with data from DataReader
            With RelationInfo
                .RelationId = RelationId
                .IsInDB = True
                .Code = dr("RelationCode")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Description = dr("RelationDescrip")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return RelationInfo
        Return RelationInfo

    End Function
    Public Function UpdateRelationInfo(ByVal RelationInfo As RelationInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syRelations Set RelationId = ?, RelationCode = ?, ")
                .Append(" StatusId = ?, RelationDescrip = ?, CampGrpId = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE RelationId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from syRelations where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   RelationId
            db.AddParameter("@RelationId", RelationInfo.RelationId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   EnrollmentId
            db.AddParameter("@RelationCode", RelationInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", RelationInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   RelationDescrip
            db.AddParameter("@RelationDescrip", RelationInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If RelationInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", RelationInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   RelationId
            db.AddParameter("@RelationId", RelationInfo.RelationId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", RelationInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If


        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)


        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddRelationInfo(ByVal RelationInfo As RelationInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT syRelations (RelationId, RelationCode, StatusId, ")
                .Append("   RelationDescrip, CampGrpId, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   RelationId
            db.AddParameter("@RelationId", RelationInfo.RelationId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   RelationCode
            db.AddParameter("@RelationCode", RelationInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", RelationInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   RelationDescrip
            db.AddParameter("@RelationDescrip", RelationInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If RelationInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", RelationInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteRelationInfo(ByVal RelationId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syRelations ")
                .Append("WHERE RelationId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM syRelations WHERE RelationId = ? ")
            End With

            '   add parameters values to the query

            '   RelationId
            db.AddParameter("@RelationId", RelationId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   RelationId
            db.AddParameter("@RelationId", RelationId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If


        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
End Class
