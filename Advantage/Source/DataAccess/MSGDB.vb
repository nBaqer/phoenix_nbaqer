' ===============================================================================
' MSGDB.vb
' DataAccess classes for the MSG project
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports System.Text
Imports System.Data
Imports System.Data.OleDb
Imports FAME.AdvantageV1.Common.MSG
Imports FAME.Advantage.Common

Namespace MSG
#Region "Groups"
    Public Class GroupsDB
        ''' <summary>
        ''' Get all groups for a specified campus
        ''' </summary>
        ''' <param name="ShowActive"></param>
        ''' <param name="ShowInactive"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAll(ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append(" MG.GroupId, " & vbCrLf)
            sb.Append(" MG.Code, " & vbCrLf)
            sb.Append(" MG.Descrip, " & vbCrLf)
            sb.Append(" MG.CampGroupId, " & vbCrLf)
            sb.Append(" MG.ModDate, " & vbCrLf)
            sb.Append(" MG.ModUser, " & vbCrLf)
            sb.Append(" MG.Active " & vbCrLf)
            sb.Append("FROM msgGroups MG " & vbCrLf)
            If ShowActive And Not ShowInactive Then
                sb.Append("WHERE " & vbCrLf)
                sb.Append("Active = ? " & vbCrLf)
                db.AddParameter("@Active", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            ElseIf Not ShowActive And ShowInactive Then
                sb.Append("WHERE " & vbCrLf)
                sb.Append("Active = ? " & vbCrLf)
                db.AddParameter("@Active", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If
            sb.Append("ORDER BY " & vbCrLf)
            sb.Append(" MG.Descrip asc ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieve a GroupInfo object from the database
        ''' </summary>
        ''' <param name="GroupId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetInfo(ByVal GroupId As String) As GroupInfo
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT GroupId, " + vbCrLf)
            sb.Append(" MG.GroupId, " & vbCrLf)
            sb.Append(" MG.Code, " & vbCrLf)
            sb.Append(" MG.Descrip, " & vbCrLf)
            sb.Append(" MG.CampGroupId, " & vbCrLf)
            sb.Append(" MG.ModDate, " & vbCrLf)
            sb.Append(" MG.ModUser, " & vbCrLf)
            sb.Append(" MG.Active " & vbCrLf)
            sb.Append("FROM  msgGroups MG " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("   GroupId = ? ")
            db.AddParameter("@GroupId", GroupId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            Try
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                Dim info As New GroupInfo
                If dr.Read() Then
                    '   set properties with data from DataReader
                    info.GroupId = GroupId
                    info.IsInDB = True
                    If Not (dr("CampGroupId") Is System.DBNull.Value) Then info.CampGroupId = CType(dr("CampGroupId"), Guid).ToString()
                    If Not (dr("Code") Is System.DBNull.Value) Then info.Code = dr("Code")
                    If Not (dr("Descrip") Is System.DBNull.Value) Then info.Descrip = dr("Descrip")
                    If Not (dr("ModUser") Is System.DBNull.Value) Then info.ModUser = dr("ModUser").ToString()
                    If Not (dr("ModDate") Is System.DBNull.Value) Then info.ModDate = dr("ModDate")
                    If Not (dr("Active") Is System.DBNull.Value) Then info.Active = CType(dr("Active"), Integer)
                End If

                If Not dr.IsClosed Then dr.Close()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

                Return info
            Catch ex As Exception
            End Try
            Return Nothing
        End Function

        ''' <summary>
        ''' Update a GroupInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Update(ByVal info As GroupInfo, ByVal user As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("UPDATE msgGroups " & vbCrLf)
                sb.Append("SET " & vbCrLf)
                sb.Append(" CampGroupId = ?, " & vbCrLf)
                sb.Append(" Code = ?," & vbCrLf)
                sb.Append(" Descrip = ?, " & vbCrLf)
                sb.Append(" ModUser = ?, ModDate = ?, " & vbCrLf)
                sb.Append(" Active = ?" + vbCrLf)
                sb.Append("WHERE GroupId = ? ")

                '   add parameters values to the query            
                If info.CampGroupId Is Nothing Or info.CampGroupId = Guid.Empty.ToString Then
                    db.AddParameter("@CampGroupId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CampGroupId", info.CampGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@Code", info.Code, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)
                db.AddParameter("@Descrip", info.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.ModUser Is Nothing Or info.ModUser = Guid.Empty.ToString Then
                    db.AddParameter("@ModUser", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ModUser", info.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                Dim now As Date = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@Active", info.Active, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@GroupId", info.GroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                ' run the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return False
        End Function

        ''' <summary>
        ''' Persists a new GroupInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Add(ByVal info As GroupInfo, ByVal user As String) As Boolean
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do an insert
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("INSERT msgGroups (GroupId, CampGroupId, Code, Descrip, ModDate, ModUser, Active) ")
                sb.Append(" VALUES (?,?,?,?,?,?,?) ")

                '   add parameters values to the query

                db.AddParameter("@GroupId", info.GroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.CampGroupId Is Nothing Or info.CampGroupId = "" Or info.CampGroupId = Guid.Empty.ToString Then
                    db.AddParameter("@CampGroupId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CampGroupId", info.CampGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@Code", info.Code, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)
                db.AddParameter("@Descrip", info.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If info.ModUser Is Nothing Or info.ModUser = Guid.Empty.ToString Then
                    db.AddParameter("@ModUser", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ModUser", info.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@Active", info.Active, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                '   return without errors
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return False
        End Function

        ''' <summary>
        ''' Delete from msgGroups given a GroupId
        ''' </summary>
        ''' <param name="Id"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Delete(ByVal Id As String, ByVal modDate As DateTime) As String
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do a delete
            Try
                ' check if there are already user tasks with this taskid
                Dim sql As String = String.Format("SELECT COUNT(*) from msgTemplates WHERE GroupId = '{0}'", Id)
                Dim rowCount As Integer = db.RunParamSQLScalar(sql)
                If rowCount > 0 Then
                    db.CloseConnection()
                    Return "Group cannot be deleted.  Group is used by one or more templates."
                Else
                    ' it is safe to delete the results
                    sql = "DELETE msgGroups WHERE GroupId = ? AND ModDate = ? " + vbCrLf
                    sql += "SELECT COUNT(*) from msgGroups where GroupId = ? " + vbCrLf
                    db.AddParameter("@Id", Id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@Id", Id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    rowCount = db.RunParamSQLScalar(sql)
                    '   If the row was not deleted then there was a concurrency problem
                    If rowCount = 0 Then
                        Return ""   ' return without errors
                    Else
                        Return DALExceptions.BuildConcurrencyExceptionMessage()
                    End If
                End If
                Return ""
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return "Unhandled Error"
        End Function

        Public Shared Function DeleteAll() As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do a delete
            Try
                '   build the query
                Dim sql As String = "DELETE msgGroups "
                Dim rowCount As Integer = db.RunParamSQLScalar(sql)
                db.CloseConnection()
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return False
        End Function
    End Class
#End Region

#Region "Messages"
    Public Class MessagingDB

        '   Gets all "to be sent" messages.  The parameters can be empty/nothing to exclude them from the filter.
        Public Shared Function GetAll_OutBox(ByVal filterRecipientType As String, ByVal filterRecipientId As Guid,
                ByVal filterDeliveryType As String, ByVal filterMessageGroupId As Guid, ByVal filterTemplateId As Guid,
                ByVal filterShowOnlyErrors As Boolean, ByVal filterMessageId As Guid) As DataSet
            '   connect to the database
            Dim db As New SQLDataAccess
            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Try
                db.AddParameter("@RecipientType", filterRecipientType, SqlDbType.VarChar, 15, ParameterDirection.Input)
                db.AddParameter("@RecipientId", filterRecipientId, SqlDbType.UniqueIdentifier, 36, ParameterDirection.Input)
                db.AddParameter("@DeliveryType", filterDeliveryType, SqlDbType.VarChar, 10, ParameterDirection.Input)
                db.AddParameter("@GroupId", filterMessageGroupId, SqlDbType.UniqueIdentifier, 36, ParameterDirection.Input)
                db.AddParameter("@TemplateId", filterTemplateId, SqlDbType.UniqueIdentifier, 36, ParameterDirection.Input)
                db.AddParameter("@filterShowOnlyErrors", filterShowOnlyErrors, SqlDbType.Bit, , ParameterDirection.Input)
                db.AddParameter("@MessageType", "outBox", SqlDbType.VarChar, 10, ParameterDirection.Input)
                db.AddParameter("@MessageId", filterMessageId, SqlDbType.UniqueIdentifier, 36, ParameterDirection.Input)
                Return db.RunParamSQLDataSet_SP("USP_Message_GetAllOutBox_Sent")
            Catch e As Exception
                Throw New Exception(e.Message)
            Finally
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
        End Function



        '   Gets all "to be sent" messages.  The parameters can be empty/nothing to exclude them from the filter.
        Public Shared Function GetAll_Sent(ByVal filterRecipientType As String, ByVal filterRecipientId As String,
                ByVal filterDeliveryType As String, ByVal filterMessageGroupId As String, ByVal filterTemplateId As String,
                ByVal filterShowOnlyErrors As Boolean) As DataSet

            '   connect to the database
            Dim db As New SQLDataAccess
            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Try
                db.AddParameter("@RecipientType", IIf(IsNothing(filterRecipientType), DBNull.Value, filterRecipientType), SqlDbType.VarChar, 15, ParameterDirection.Input)
                db.AddParameter("@RecipientId", IIf(IsNothing(filterRecipientId), DBNull.Value, filterRecipientId), SqlDbType.VarChar, 50, ParameterDirection.Input)
                db.AddParameter("@DeliveryType", IIf(IsNothing(filterDeliveryType), DBNull.Value, filterDeliveryType), SqlDbType.VarChar, 10, ParameterDirection.Input)
                db.AddParameter("@GroupId", IIf(IsNothing(filterMessageGroupId), DBNull.Value, filterMessageGroupId), SqlDbType.VarChar, 50, ParameterDirection.Input)
                db.AddParameter("@TemplateId", IIf(IsNothing(filterTemplateId), DBNull.Value, filterTemplateId), SqlDbType.VarChar, 50, ParameterDirection.Input)
                db.AddParameter("@filterShowOnlyErrors", filterShowOnlyErrors, SqlDbType.Bit, , ParameterDirection.Input)
                db.AddParameter("@MessageType", "sent", SqlDbType.VarChar, 10, ParameterDirection.Input)
                db.AddParameter("@MessageID", Nothing, SqlDbType.VarChar, 50, ParameterDirection.Input)
                Dim ds As DataSet = db.RunParamSQLDataSet_SP("USP_Message_GetAllOutBox_Sent")
                Return ds
            Catch e As Exception
                Throw New Exception(e.Message)
            Finally
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
        End Function

        ''' <summary>
        ''' Retrives a MessageInfo object given a messageid
        ''' </summary>
        ''' <param name="MessageId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetInfo(ByVal messageId As String) As MessageInfo
            '   Connect to the database
            Dim db As New SQLDataAccess
            Dim ds As DataSet
            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Try

                db.AddParameter("@MessageId", messageId, SqlDbType.VarChar, 50, ParameterDirection.Input)
                ds = db.RunParamSQLDataSet_SP("USP_Message_GetInfo")
                Dim info As New MessageInfo
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        info.IsInDB = True
                        info.MessageId = messageId
                        Dim dr As DataRow
                        dr = ds.Tables(0).Rows(0)
                        If Not (dr("TemplateId") Is System.DBNull.Value) Then info.TemplateId = CType(dr("TemplateId"), Guid).ToString
                        If Not (dr("FromId") Is System.DBNull.Value) Then info.FromId = CType(dr("FromId"), Guid).ToString
                        If Not (dr("MailFrom") Is System.DBNull.Value) Then info.MailFrom = dr("MailFrom")
                        If Not (dr("MailTo") Is System.DBNull.Value) Then info.MailTo = dr("MailTo")
                        If Not (dr("TemplateDescrip") Is System.DBNull.Value) Then info.TemplateDescrip = dr("TemplateDescrip")
                        If Not (dr("GroupDescrip") Is System.DBNull.Value) Then info.GroupDescrip = dr("GroupDescrip")
                        If Not (dr("DeliveryType") Is System.DBNull.Value) Then info.DeliveryType = dr("DeliveryType")
                        If Not (dr("RecipientId") Is System.DBNull.Value) Then info.RecipientId = CType(dr("RecipientId"), Guid).ToString
                        If Not (dr("RecipientType") Is System.DBNull.Value) Then info.RecipientType = dr("RecipientType")
                        If Not (dr("RecipientName") Is System.DBNull.Value) Then info.RecipientName = dr("RecipientName")
                        If Not (dr("ReId") Is System.DBNull.Value) Then info.ReId = CType(dr("ReId"), Guid).ToString
                        If Not (dr("ReType") Is System.DBNull.Value) Then info.ReType = dr("ReType")
                        If Not (dr("ReName") Is System.DBNull.Value) Then info.ReName = dr("ReName")
                        If Not (dr("ChangeId") Is System.DBNull.Value) Then info.ChangeId = CType(dr("ChangeId"), Guid).ToString
                        If Not (dr("TemplateData") Is System.DBNull.Value) Then info.TemplateData = dr("TemplateData")
                        If Not dr("MsgContent") Is System.DBNull.Value Then info.MsgContent = dr("MsgContent")
                        If Not dr("CreatedDate") Is System.DBNull.Value Then info.CreatedDate = dr("CreatedDate")
                        If Not dr("DeliveryDate") Is System.DBNull.Value Then info.DeliveryDate = dr("DeliveryDate")
                        If Not dr("LastDeliveryAttempt") Is System.DBNull.Value Then info.LastDeliveryAttempt = dr("LastDeliveryAttempt")
                        If Not dr("LastDeliveryMsg") Is System.DBNull.Value Then info.LastDeliveryMsg = dr("LastDeliveryMsg")
                        If Not dr("DateDelivered") Is System.DBNull.Value Then info.DateDelivered = dr("DateDelivered")
                        If Not (dr("ModUser") Is System.DBNull.Value) Then info.ModUser = CType(dr("ModUser"), Guid).ToString()
                        If Not (dr("ModDate") Is System.DBNull.Value) Then info.ModDate = dr("ModDate")
                    End If
                End If
                Return info
            Catch e As Exception
                Throw New Exception(e.Message)
            Finally
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
        End Function

        ''' <summary>
        ''' Add a MessageInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Add(ByVal info As MessageInfo, ByVal user As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("INSERT msgMessages (MessageId, TemplateId, FromId, DeliveryType, RecipientType, ")
                sb.Append("RecipientId, ReType, ReId, ChangeId, MsgContent, CreatedDate, DeliveryDate, ModDate, ModUser) ")
                sb.Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                '   add parameters values to the query
                db.AddParameter("@MessageId", info.MessageId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.TemplateId Is Nothing Or info.TemplateId = "" Then
                    db.AddParameter("@TemplateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@TemplateId", info.TemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.FromId Is Nothing Or info.FromId = "" Then
                    db.AddParameter("@FromId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@FromId", info.FromId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@DeliveryType", info.DeliveryType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@RecipientType", info.RecipientType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.RecipientId Is Nothing Or info.RecipientId = "" Or info.RecipientId = Guid.Empty.ToString Then
                    db.AddParameter("@RecipientId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@RecipientId", info.RecipientId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@ReType", info.ReType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.ReId Is Nothing Or info.ReId = "" Or info.ReId = Guid.Empty.ToString Then
                    db.AddParameter("@ReId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ReId", info.ReId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.ChangeId Is Nothing Or info.ChangeId = "" Or info.ChangeId = Guid.Empty.ToString Then
                    db.AddParameter("@ChangeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ChangeId", info.ChangeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@MsgContent", info.MsgContent, DataAccess.OleDbDataType.OleDbString)
                db.AddParameter("@CreatedDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@DeliveryDate", info.DeliveryDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                'db.AddParameter("@LastDeliveryAttempt", MessageInfo.LastDeliveryAttempt, FAME.Advantage.DataAccess.DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                'db.AddParameter("@LastDeliveryMsg", MessageInfo.LastDeliveryMsg, FAME.Advantage.DataAccess.DataAccess.OleDbDataType.OleDbString, 100, ParameterDirection.Input)
                'db.AddParameter("@DateDelivered", MessageInfo.DateDelivered, FAME.Advantage.DataAccess.DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                Return True
            Catch ex As Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return False
        End Function

        ''' <summary>
        ''' Updates a MessageInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Update(ByVal info As MessageInfo, ByVal user As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("UPDATE msgMessages " & vbCrLf)
                sb.Append("SET " & vbCrLf)
                sb.Append(" TemplateId = ?, " & vbCrLf)
                sb.Append(" FromId = ?, " & vbCrLf)
                sb.Append(" DeliveryType = ?, " & vbCrLf)
                sb.Append(" RecipientType = ?, " & vbCrLf)
                sb.Append(" RecipientId = ?, " & vbCrLf)
                sb.Append(" ReType = ?, " & vbCrLf)
                sb.Append(" ReId = ?, " & vbCrLf)
                sb.Append(" ChangeId = ?, " & vbCrLf)
                sb.Append(" MsgContent = ?, " & vbCrLf)
                sb.Append(" CreatedDate = ?, " & vbCrLf)
                sb.Append(" LastDeliveryAttempt = ?, " & vbCrLf)
                sb.Append(" LastDeliveryMsg = ?, " & vbCrLf)
                sb.Append(" DateDelivered = ?, " & vbCrLf)
                sb.Append(" ModUser = ?, ModDate = ? " & vbCrLf)
                sb.Append(" WHERE MessageId = ? ")
                '   add parameters values to the query            
                If info.TemplateId = Guid.Empty.ToString Then
                    db.AddParameter("@TemplateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@TemplateId", info.TemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.FromId = Guid.Empty.ToString Then
                    db.AddParameter("@FromId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@FromId", info.FromId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@DeliveryType", info.DeliveryType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@RecipientType", info.RecipientType, DataAccess.OleDbDataType.OleDbString, 15, ParameterDirection.Input)
                If info.RecipientId = Guid.Empty.ToString Then
                    db.AddParameter("@RecipientId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@RecipientId", info.RecipientId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@ReType", info.ReType, DataAccess.OleDbDataType.OleDbString, 15, ParameterDirection.Input)
                If info.ReId = Guid.Empty.ToString Then
                    db.AddParameter("@ReId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ReId", info.ReId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@ChangeId", info.ChangeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@MsgContent", info.MsgContent, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@CreatedDate", info.CreatedDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@LastDeliveryAttempt", info.LastDeliveryAttempt, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@LastDeliveryMsg", info.LastDeliveryMsg, DataAccess.OleDbDataType.OleDbString, 100, ParameterDirection.Input)
                If info.DateDelivered = DateTime.MinValue Then
                    db.AddParameter("@DateDelivered", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Else
                    db.AddParameter("@DateDelivered", info.DateDelivered, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Dim now As Date = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@MessageId", info.MessageId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                ' run the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return False
        End Function

        ''' <summary>
        ''' Delete a message given a MessageId
        ''' </summary>
        ''' <param name="MessageId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Delete(ByVal MessageId As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do a delete
            Try
                Dim sb As New StringBuilder
                sb.Append("DELETE FROM msgMessages ")
                sb.Append(" WHERE MessageId = ? ")
                db.AddParameter("@MessageId", MessageId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.RunParamSQLScalar(sb.ToString)
                db.CloseConnection()
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return False
        End Function
        Public Shared Function DeleteAdHoc(ByVal MessageIds As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do a delete
            Try
                Dim sb As New StringBuilder
                sb.Append("DELETE FROM msgMessages ")
                sb.Append(" WHERE MessageId in (" + MessageIds + ")")
                db.RunParamSQLScalar(sb.ToString)
                db.CloseConnection()
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return False
        End Function
    End Class
#End Region

#Region "Rules"
    Public Class RulesDB
        ''' <summary>
        ''' Get all rules defines in the system
        ''' </summary>
        ''' <param name="ShowActive"></param>
        ''' <param name="ShowInactive"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAll(ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append("     R.RuleId, " & vbCrLf)
            sb.Append("	    R.Type, " & vbCrLf)
            sb.Append("	    R.Code, " & vbCrLf)
            sb.Append("	    R.Descrip, " & vbCrLf)
            sb.Append("     R.CampGroupId, " + vbCrLf)
            sb.Append("	    R.RuleSql, " & vbCrLf)
            sb.Append("	    R.TemplateId, " & vbCrLf)
            sb.Append("	    (select T.Descrip from msgTemplates T where T.TemplateId=R.TemplateId) as TemplateDescrip, " & vbCrLf)
            sb.Append("     (select G.Descrip from msgTemplates T, msgGroups G where T.TemplateId=R.TemplateId and G.GroupId=T.GroupId) as GroupDescrip, " & vbCrLf)
            sb.Append("	    R.RecipientType, " & vbCrLf)
            sb.Append("	    R.ReType, " & vbCrLf)
            sb.Append("	    R.DeliveryType, " & vbCrLf)
            sb.Append("	    R.LastRun, " & vbCrLf)
            sb.Append("	    R.LastRunError, " & vbCrLf)
            sb.Append("     R.ModDate, " & vbCrLf)
            sb.Append("     R.ModUser, " & vbCrLf)
            sb.Append("     R.Active " & vbCrLf)
            sb.Append("FROM msgRules R " & vbCrLf)
            If ShowActive And Not ShowInactive Then
                sb.Append("WHERE " + vbCrLf)
                sb.Append(" R.Active = ? " & vbCrLf)
                db.AddParameter("@Active", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            ElseIf Not ShowActive And ShowInactive Then
                sb.Append("WHERE " + vbCrLf)
                sb.Append(" R.Active = ? " & vbCrLf)
                db.AddParameter("@Active", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If
            sb.Append("ORDER BY R.Descrip")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieve a RuleInfo object from the db given a RuleId
        ''' </summary>
        ''' <param name="RuleId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetInfo(ByVal RuleId As String) As RuleInfo
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append("     R.RuleId, " & vbCrLf)
            sb.Append("	    R.Type, " & vbCrLf)
            sb.Append("	    R.Code, " & vbCrLf)
            sb.Append("	    R.Descrip, " & vbCrLf)
            sb.Append("     R.CampGroupId, " + vbCrLf)
            sb.Append("	    R.RuleSql, " & vbCrLf)
            sb.Append("	    R.TemplateId, " & vbCrLf)
            sb.Append("	    (select T.Descrip from msgTemplates T where T.TemplateId=R.TemplateId) as TemplateDescrip, " & vbCrLf)
            sb.Append("     (select G.Descrip from msgTemplates T, msgGroups G where T.TemplateId=R.TemplateId and G.GroupId=T.GroupId) as GroupDescrip, " & vbCrLf)
            sb.Append("	    R.RecipientType, " & vbCrLf)
            sb.Append("	    R.ReType, " & vbCrLf)
            sb.Append("	    R.DeliveryType, " & vbCrLf)
            sb.Append("	    R.LastRun, " & vbCrLf)
            sb.Append("	    R.LastRunError, " & vbCrLf)
            sb.Append("     R.ModDate, " & vbCrLf)
            sb.Append("     R.ModUser, " & vbCrLf)
            sb.Append("     R.Active " & vbCrLf)
            sb.Append("FROM     msgRules R " & vbCrLf)
            sb.Append("WHERE    R.RuleId = ? " & vbCrLf)
            ' Add the RuleId to the parameter list and return the datareader
            db.AddParameter("@RuleId", RuleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            ' fill the the MessageTemplateInfo structure
            Dim info As New RuleInfo
            If dr.Read() Then
                '   set properties with data from DataReader
                info.IsInDB = True
                info.RuleId = RuleId
                If Not (dr("Type") Is System.DBNull.Value) Then info.Type = dr("Type")
                If Not (dr("Code") Is System.DBNull.Value) Then info.Code = dr("Code")
                If Not (dr("Descrip") Is System.DBNull.Value) Then info.Descrip = dr("Descrip")
                If Not (dr("CampGroupId") Is System.DBNull.Value) Then info.CampGroupId = CType(dr("CampGroupId"), Guid).ToString
                If Not (dr("RuleSql") Is System.DBNull.Value) Then info.RuleSql = dr("RuleSql")
                If Not (dr("TemplateId") Is System.DBNull.Value) Then info.TemplateId = CType(dr("TemplateId"), Guid).ToString
                If Not (dr("RecipientType") Is System.DBNull.Value) Then info.RecipientType = dr("RecipientType")
                If Not (dr("ReType") Is System.DBNull.Value) Then info.ReType = dr("ReType")
                If Not (dr("DeliveryType") Is System.DBNull.Value) Then info.DeliveryType = dr("DeliveryType")
                If Not (dr("LastRun") Is System.DBNull.Value) Then info.LastRun = dr("LastRun")
                If Not (dr("LastRunError") Is System.DBNull.Value) Then info.LastRunError = dr("LastRunError")
                If Not (dr("ModUser") Is System.DBNull.Value) Then info.ModUser = CType(dr("ModUser"), Guid).ToString()
                If Not (dr("ModDate") Is System.DBNull.Value) Then info.ModDate = dr("ModDate")
                If Not (dr("Active") Is System.DBNull.Value) Then info.Active = dr("Active")
            End If

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return info
        End Function

        ''' <summary>
        ''' Adds a new RuleInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Add(ByVal info As RuleInfo, ByVal user As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("INSERT msgRules (RuleId, Type, Code, Descrip, CampGroupId, RuleSql, ")
                sb.Append("   TemplateId, RecipientType, ReType, DeliveryType, ModDate, ModUser, Active) ")
                sb.Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?) ")

                '   add parameters values to the query
                db.AddParameter("@RuleId", info.RuleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@RuleType", info.Type, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@Code", info.Code, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)
                db.AddParameter("@RuleDescrip", info.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@CampGroupId", info.CampGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@RuleSql", info.RuleSql, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@TemplateId", info.TemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@RecipientType", info.RecipientType, DataAccess.OleDbDataType.OleDbString, 15, ParameterDirection.Input)
                db.AddParameter("@ReType", info.ReType, DataAccess.OleDbDataType.OleDbString, 15, ParameterDirection.Input)
                db.AddParameter("@DeliveryType", info.DeliveryType, DataAccess.OleDbDataType.OleDbString, 15, ParameterDirection.Input)
                db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Active", info.Active, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Return True
            Catch ex As OleDbException
            Finally
                db.CloseConnection()
            End Try
            Return False
        End Function

        ''' <summary>
        ''' Updates a RuleInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Update(ByVal info As RuleInfo, ByVal user As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("UPDATE msgRules " & vbCrLf)
                sb.Append("SET " & vbCrLf)
                sb.Append(" Type = ?, " & vbCrLf)
                sb.Append(" Descrip = ?, " & vbCrLf)
                sb.Append(" CampGroupId = ?, " & vbCrLf)
                sb.Append(" RuleSql = ?, " & vbCrLf)
                sb.Append(" TemplateId = ?, " & vbCrLf)
                sb.Append(" RecipientType = ?, " & vbCrLf)
                sb.Append(" ReType = ?, " & vbCrLf)
                sb.Append(" DeliveryType = ?, " & vbCrLf)
                sb.Append(" LastRun = ?, " & vbCrLf)
                sb.Append(" LastRunError = ?, " & vbCrLf)
                sb.Append(" Active = ?, " & vbCrLf)
                sb.Append(" ModUser = ?, ModDate = ? " & vbCrLf)
                sb.Append(" WHERE RuleId = ? ")
                '   add parameters values to the query
                db.AddParameter("@Type", info.Type, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@Descrip", info.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.CampGroupId = "" Or info.CampGroupId = Guid.Empty.ToString Then
                    db.AddParameter("@CampGroupId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CampGroupId", info.CampGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@RuleSql", info.RuleSql, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If info.TemplateId = "" Or info.TemplateId = Guid.Empty.ToString Then
                    db.AddParameter("@TemplateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@TemplateId", info.TemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@RecipientType", info.RecipientType, DataAccess.OleDbDataType.OleDbString, 15, ParameterDirection.Input)
                db.AddParameter("@ReType", info.ReType, DataAccess.OleDbDataType.OleDbString, 15, ParameterDirection.Input)
                db.AddParameter("@DeliveryType", info.DeliveryType, DataAccess.OleDbDataType.OleDbString, 15, ParameterDirection.Input)
                ' check for invalid date
                Try
                    info.LastRun = CType(info.LastRun, DateTime)
                    If info.LastRun <> Date.MinValue Then
                        db.AddParameter("@LastRun", info.LastRun, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@LastRun", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    End If
                Catch ex As Exception
                End Try
                db.AddParameter("@LastRunError", info.LastRunError, DataAccess.OleDbDataType.OleDbString, 255, ParameterDirection.Input)
                db.AddParameter("@Active", info.Active, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                If user Is Nothing Or user = "" Then
                    db.AddParameter("@ModUser", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                Dim now As Date = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@RuleId", info.RuleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return False
        End Function

        ''' <summary>
        ''' Delete all rules
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DeleteAll() As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                Dim sql As String = "DELETE msgRules"
                db.RunParamSQLExecuteNoneQuery(sql)
                db.CloseConnection()
                Return True
            Catch ex As Exception
            Finally
                db.CloseConnection()
            End Try
            Return False
        End Function
    End Class
#End Region

#Region "Special Fields"
    Public Class SpecialFieldsDB
        Public Shared Function GetTemplateSpecialFields(ByVal GroupId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT ")
                .Append(" CategoryGroup as Category " & vbCrLf)
                .Append("FROM msgCategoryLookup " & vbCrLf)
                .Append("WHERE " & vbCrLf)
                .Append("   GroupId = '" + GroupId + "'")
            End With
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        Public Shared Function GetTemplateSpecialFields(ByVal GroupId As String, ByVal strValue As String) As Boolean
            Dim db As New DataAccess
            Dim bReturn As Boolean = False

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT ")
                .Append(" CategoryGroup as Category " & vbCrLf)
                .Append("FROM msgCategoryLookup " & vbCrLf)
                .Append("WHERE " & vbCrLf)
                .Append("   GroupId = '" + GroupId + "'")
                .Append(" AND CategoryGroup = '" + strValue + "'")
            End With
            ' for debuging
            'Dim str As String = sb.ToString()
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            If dr.Read() Then
                bReturn = True
            End If
            If Not dr.IsClosed Then
                dr.Close()
            End If
            Return bReturn
        End Function

        Public Shared Function AddTemplateSpecialField(ByVal GroupId As String, ByVal strValue As String) As Integer
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim rowCount As Integer = 0
            '   build the sql query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT INTO msgCategoryLookup ")
                .Append("(GroupId,CategoryGroup) " & vbCrLf)
                .Append("VALUES " & vbCrLf)
                .Append("('" + GroupId + "','" + strValue + "')")
            End With
            Try
                ' for debuging
                '            Dim str As String = sb.ToString()
                rowCount = db.RunParamSQLScalar(sb.ToString)
                Return rowCount
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.ErrorCode
            End Try
        End Function

        Public Shared Function DeleteTemplateSpecialField(ByVal GroupId As String, ByVal strValue As String) As Integer
            Dim db As New DataAccess
            Dim rowCount As Integer = 0

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM msgCategoryLookup ")
                .Append("   (GroupId,CategoryGroup) " & vbCrLf)
                .Append("WHERE " & vbCrLf)
                .Append("   GroupId = '" + GroupId + "'")
                If strValue <> "" Then
                    .Append(" AND CategoryGroup='" + strValue + "'")
                End If
            End With
            Try
                ' for debuging
                '           Dim str As String = sb.ToString()
                rowCount = db.RunParamSQLScalar(sb.ToString)
                Return rowCount
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.ErrorCode
            End Try
            '        Return rowCount
        End Function
    End Class
#End Region

#Region "Templates"
    Public Class TemplatesDB
        ''' <summary>
        ''' Get all templates given a templateid
        ''' </summary>
        ''' <param name="CampGroupId"></param>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetTemplates(ByVal CampGroupId As String, ByVal ModuleEntityId As String, _
                                        ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append(" MT.TemplateId, " & vbCrLf)
            sb.Append(" MT.GroupId, " & vbCrLf)
            sb.Append(" G.Descrip as GroupDescrip, " + vbCrLf)
            sb.Append(" MT.ModuleEntityId, " & vbCrLf)
            sb.Append(" M.ResourceId as ModuleId, " & vbCrLf)
            sb.Append(" M.Resource as ModuleName, " & vbCrLf)
            sb.Append(" E.ResourceId as EntityId, " & vbCrLf)
            sb.Append(" E.Resource as EntityName, " & vbCrLf)
            sb.Append(" MT.CampGroupId, " & vbCrLf)
            sb.Append(" MT.Code, " + vbCrLf)
            sb.Append(" MT.Descrip, " & vbCrLf)
            sb.Append(" MT.Data, " & vbCrLf)
            sb.Append(" MT.ModDate, " & vbCrLf)
            sb.Append(" MT.ModUser, " & vbCrLf)
            sb.Append(" MT.Active " & vbCrLf)
            sb.Append("FROM  msgTemplates MT, msgGroups G, " & vbCrLf)
            sb.Append("      syAdvantageResourceRelations R, syResources E, syResources M " + vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            sb.Append("     G.GroupId=MT.GroupId and " + vbCrLf)
            sb.Append("     R.ResourceId = E.ResourceId and " + vbCrLf)
            sb.Append("     R.RelatedResourceId = M.ResourceId and " + vbCrLf)
            sb.Append("     E.ResourceTypeId=8 and " + vbCrLf)
            sb.Append("     M.ResourceTypeId=1 and " + vbCrLf)
            sb.Append("     R.ResRelId = MT.ModuleEntityId " + vbCrLf)
            If Not CampGroupId Is Nothing AndAlso CampGroupId <> "" Then
                sb.Append(" AND MT.CampGroupId = ? " & vbCrLf)
                db.AddParameter("@CampGroupId", CampGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            If Not ModuleEntityId Is Nothing AndAlso ModuleEntityId <> "" Then
                sb.Append(" AND MT.ModuleEntityId = ? " + vbCrLf)
                db.AddParameter("@ModuleEntityId", ModuleEntityId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            If ShowActive And Not ShowInactive Then
                sb.Append(" AND MT.Active = ? " & vbCrLf)
                db.AddParameter("@Active", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            ElseIf Not ShowActive And ShowInactive Then
                sb.Append(" AND MT.Active = ? " & vbCrLf)
                db.AddParameter("@Active", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If
            sb.Append("ORDER BY MT.Descrip asc ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Get all templates given a templateid
        ''' </summary>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAllForUser(ByVal UserId As String, ByVal CampusId As String, _
                                        ByVal ModuleId As String, ByVal EntityId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query            
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append("     distinct MT.TemplateId, " & vbCrLf)
            sb.Append("     MT.GroupId, " & vbCrLf)
            sb.Append("     (select Descrip from msgGroups G where G.GroupId=MT.GroupId) as GroupDescrip, " + vbCrLf)
            sb.Append("     MT.ModuleEntityId, " & vbCrLf)
            sb.Append("     R.RelatedResourceId as ModuleId, " & vbCrLf)
            sb.Append("     (select Resource from syResources SR where SR.ResourceId=R.RelatedResourceId) as ModuleName, " & vbCrLf)
            sb.Append("     R.ResourceId as EntityId, " & vbCrLf)
            sb.Append("     (select Resource from syResources SR where SR.ResourceId=R.ResourceId) as EntityName, " & vbCrLf)
            sb.Append("     MT.CampGroupId, " & vbCrLf)
            sb.Append("     MT.Code, " + vbCrLf)
            sb.Append("     MT.Descrip, " & vbCrLf)
            'sb.Append(" MT.Data, " & vbCrLf)
            sb.Append("     MT.ModDate, " & vbCrLf)
            sb.Append("     MT.ModUser, " & vbCrLf)
            sb.Append("     MT.Active " & vbCrLf)
            sb.Append("FROM msgTemplates MT, syUsersRolesCampGrps UR, syRolesModules RM, " & vbCrLf)
            sb.Append("     syAdvantageResourceRelations R")
            ' only add campus table if campus id was provided
            If Not CampusId Is Nothing AndAlso CampusId <> "" Then
                sb.Append(", syCmpGrpCmps CC ")
            End If
            sb.Append(vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            sb.Append("     RM.RoleId=UR.RoleId and " + vbCrLf)
            sb.Append("     RM.ModuleId=R.RelatedResourceId and " + vbCrLf)
            sb.Append("     MT.Active=1 and  " + vbCrLf)
            sb.Append("     R.ResRelId = MT.ModuleEntityId and " + vbCrLf)
            sb.Append("     UR.CampGrpId = CC.CampGrpId " + vbCrLf)

            ' only add where clause for campus if campus id was provided
            ' BEN: 10/27/06 - add support for "All Campus Group"
            If Not CampusId Is Nothing AndAlso CampusId <> "" Then
                sb.Append(" AND CC.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId = ?)")
                db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            ' add where clause for userid
            sb.Append("AND UR.UserId = ? " + vbCrLf)
            db.AddParameter("@UserId", UserId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            ' Add where clause for moduleid
            If Not ModuleId Is Nothing AndAlso ModuleId <> "" Then
                sb.Append("AND R.RelatedResourceId = ? " + vbCrLf)
                db.AddParameter("@ModuleId", ModuleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            sb.Append("ORDER BY MT.Descrip asc ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Get all templates for an entity
        ''' </summary>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAllForEntity(ByVal CampGroupId As String, ByVal EntityId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query            
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append(" MT.TemplateId, " & vbCrLf)
            sb.Append(" MT.GroupId, " & vbCrLf)
            sb.Append(" G.Descrip as GroupDescrip, " + vbCrLf)
            sb.Append(" MT.ModuleEntityId, " & vbCrLf)
            sb.Append(" M.ResourceId as ModuleId, " & vbCrLf)
            sb.Append(" M.Resource as ModuleName, " & vbCrLf)
            sb.Append(" E.ResourceId as EntityId, " & vbCrLf)
            sb.Append(" E.Resource as EntityName, " & vbCrLf)
            sb.Append(" MT.CampGroupId, " & vbCrLf)
            sb.Append(" MT.Code, " + vbCrLf)
            sb.Append(" MT.Descrip, " & vbCrLf)
            sb.Append(" MT.Data, " & vbCrLf)
            sb.Append(" MT.ModDate, " & vbCrLf)
            sb.Append(" MT.ModUser, " & vbCrLf)
            sb.Append(" MT.Active " & vbCrLf)
            sb.Append("FROM  msgTemplates MT, msgGroups G, " & vbCrLf)
            sb.Append("      syAdvantageResourceRelations R, syResources E, syResources M " + vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            sb.Append("     G.GroupId=MT.GroupId AND MT.Active=1 and " + vbCrLf)
            sb.Append("     R.ResourceId = E.ResourceId and " + vbCrLf)
            sb.Append("     R.RelatedResourceId = M.ResourceId and " + vbCrLf)
            sb.Append("     E.ResourceTypeId=8 and " + vbCrLf)
            sb.Append("     M.ResourceTypeId=1 and " + vbCrLf)
            sb.Append("     R.ResRelId = MT.ModuleEntityId " + vbCrLf)
            ' only add where clause for campus if campus id was provided
            If Not CampGroupId Is Nothing And CampGroupId <> "" Then
                sb.Append(" AND MT.CampGroupId = ? " + vbCrLf)
                db.AddParameter("@CampGroupId", CampGroupId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            ' Add where clause for moduleid
            If Not EntityId Is Nothing AndAlso EntityId <> "" Then
                sb.Append("AND E.ResourceId = ? " + vbCrLf)
                db.AddParameter("@EntityId", EntityId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            sb.Append("ORDER BY MT.Descrip asc ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieve a template info object from the database
        ''' </summary>
        ''' <param name="TemplateId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetInfo(ByVal TemplateId As String) As TemplateInfo
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append(" MT.TemplateId, " & vbCrLf)
            sb.Append(" MT.GroupId, " & vbCrLf)
            sb.Append(" (select G.Descrip from msgGroups G where G.GroupId=MT.GroupId) as GroupDescrip, " + vbCrLf)
            sb.Append(" MT.ModuleEntityId, " & vbCrLf)
            sb.Append(" M.ResourceId as ModuleId, " & vbCrLf)
            sb.Append(" M.Resource as ModuleName, " & vbCrLf)
            sb.Append(" E.ResourceId as EntityId, " & vbCrLf)
            sb.Append(" E.Resource as EntityName, " & vbCrLf)
            sb.Append(" MT.CampGroupId, " & vbCrLf)
            sb.Append(" MT.Code, " + vbCrLf)
            sb.Append(" MT.Descrip, " & vbCrLf)
            sb.Append(" MT.Data, " & vbCrLf)
            sb.Append(" MT.ModDate, " & vbCrLf)
            sb.Append(" MT.ModUser, " & vbCrLf)
            sb.Append(" MT.Active " & vbCrLf)
            sb.Append("FROM msgTemplates MT, " & vbCrLf)
            sb.Append("     syAdvantageResourceRelations R, syResources E, syResources M ")
            sb.Append("WHERE " & vbCrLf)
            sb.Append("     MT.TemplateId = ? AND ")
            sb.Append("     R.ResourceId = E.ResourceId and " + vbCrLf)
            sb.Append("     R.RelatedResourceId = M.ResourceId and " + vbCrLf)
            sb.Append("     E.ResourceTypeId=8 and " + vbCrLf)
            sb.Append("     M.ResourceTypeId=1 and " + vbCrLf)
            sb.Append("     R.ResRelId = MT.ModuleEntityId " + vbCrLf)

            db.AddParameter("@TemplateId", TemplateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            ' fill the the MessageTemplateInfo structure
            Dim info As New TemplateInfo
            If dr.Read() Then
                '   set properties with data from DataReader
                info.IsInDB = True
                info.TemplateId = TemplateId
                If Not (dr("GroupId") Is System.DBNull.Value) Then info.GroupId = dr("GroupId").ToString()
                If Not (dr("GroupDescrip") Is System.DBNull.Value) Then info.GroupDescrip = dr("GroupDescrip")
                If Not (dr("CampGroupId") Is System.DBNull.Value) Then info.CampGroupId = CType(dr("CampGroupId"), Guid).ToString
                If Not (dr("ModuleEntityId") Is System.DBNull.Value) Then info.ModuleEntityId = dr("ModuleEntityId").ToString
                If Not (dr("ModuleId") Is System.DBNull.Value) Then info.ModuleId = CType(dr("ModuleId"), Integer)
                If Not (dr("ModuleName") Is System.DBNull.Value) Then info.ModuleName = dr("ModuleName")
                If Not (dr("EntityId") Is System.DBNull.Value) Then info.EntityId = dr("EntityId").ToString
                If Not (dr("EntityName") Is System.DBNull.Value) Then info.EntityName = dr("EntityName")
                'If Not (dr("CampGroupDescrip") Is System.DBNull.Value) Then info.CampGroupDescrip = dr("CampGroupDescrip")
                If Not (dr("Code") Is System.DBNull.Value) Then info.Code = dr("Code")
                If Not (dr("Descrip") Is System.DBNull.Value) Then info.Descrip = dr("Descrip")
                If Not dr("Data") Is System.DBNull.Value Then info.Data = dr("Data")
                If Not (dr("ModUser") Is System.DBNull.Value) Then info.ModUser = CType(dr("ModUser"), Guid).ToString()
                If Not (dr("ModDate") Is System.DBNull.Value) Then info.ModDate = dr("ModDate")
                If Not (dr("Active") Is System.DBNull.Value) Then info.Active = CType(dr("Active"), Boolean)
            End If

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return info
        End Function

        ''' <summary>
        ''' Update a TemplateInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Update(ByVal info As TemplateInfo, ByVal user As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("UPDATE msgTemplates " & vbCrLf)
                sb.Append("SET " & vbCrLf)
                sb.Append("   GroupId = ?, " & vbCrLf)
                sb.Append("   ModuleEntityId = ?, " & vbCrLf)
                sb.Append("   CampGroupId = ?,  " & vbCrLf)
                sb.Append("   Code = ?,  " & vbCrLf)
                sb.Append("   Descrip = ?,  " & vbCrLf)
                sb.Append("   Data = ?,  " & vbCrLf)
                sb.Append("   ModUser = ?, ModDate = ?, " & vbCrLf)
                sb.Append("   Active = ? " + vbCrLf)
                sb.Append("WHERE TemplateId = ? ")
                '   add parameters values to the query
                db.AddParameter("@GroupId", info.GroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModuleEntityId", info.ModuleEntityId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@CampGroupId", info.CampGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Code", info.Code, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)
                db.AddParameter("@Descrip", info.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Data", info.Data, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@Active", info.Active, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                db.AddParameter("@TemplateId", info.TemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                Return True
            Catch ex As Exception
            Finally
                db.CloseConnection()
            End Try
            Return False
        End Function

        ''' <summary>
        ''' Add a TemplateInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Add(ByVal info As TemplateInfo, ByVal user As String) As Boolean
            ' TODO: add code to verify that a template with the same MessageGroupId and TemplateDescrip.
            ' If there is another record with this criteria, we need to do an update, not an add.
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("INSERT msgTemplates (TemplateId, GroupId, ModuleEntityId,")
                sb.Append(" CampGroupId, Code, Descrip, Data, ModDate, ModUser, Active) " + vbCrLf)
                sb.Append("VALUES (?,?,?,?,?,?,?,?,?,?) ")

                '   add parameters values to the query
                db.AddParameter("@TemplateId", info.TemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@GroupId", info.GroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModuleEntityId", info.ModuleEntityId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.CampGroupId Is Nothing Or info.CampGroupId = "" Or info.CampGroupId = Guid.Empty.ToString Then
                    db.AddParameter("@CampGroupId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CampGroupId", info.CampGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@Code", info.Code, DataAccess.OleDbDataType.OleDbString)
                db.AddParameter("@Descrip", info.Descrip, DataAccess.OleDbDataType.OleDbString)
                db.AddParameter("@Data", info.Data, DataAccess.OleDbDataType.OleDbString)
                db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Active", info.Active, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return False
        End Function

        ''' <summary>
        ''' Delete a template given a TemplateId
        ''' </summary>
        ''' <param name="Id"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Delete(ByVal Id As String, ByVal modDate As DateTime) As String
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do a delete
            Try
                ' check if there are already user tasks with this taskid
                Dim sql As String = String.Format("SELECT COUNT(*) from msgMessages WHERE TemplateId = '{0}'", Id)
                Dim rowCount As Integer = db.RunParamSQLScalar(sql)
                If rowCount > 0 Then
                    db.CloseConnection()
                    Return "Template cannot be deleted.  Template is used in one or more messages."
                Else
                    ' it is safe to delete
                    sql = "DELETE msgTemplates WHERE TemplateId = ? AND modDate = ? " + vbCrLf
                    sql += "SELECT COUNT(*) from msgTemplates WHERE TemplateId = ? " + vbCrLf

                    db.AddParameter("@Id", Id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@Id", Id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    '   execute the query
                    rowCount = db.RunParamSQLScalar(sql)
                    '   If the row was not deleted then there was a concurrency problem
                    If rowCount = 0 Then
                        Return ""   ' return without errors
                    Else
                        Return DALExceptions.BuildConcurrencyExceptionMessage()
                    End If
                End If
                Return ""
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return "Unhandled Error"
        End Function
    End Class
#End Region

#Region "Modules"
    Public Class ModulesDB
        Public Shared Function GetModules(ByVal UserId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ' build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append("			ResourceId as ModuleId, " & vbCrLf)
            sb.Append("			Resource as ModuleName " & vbCrLf)
            sb.Append("FROM		syResources M " & vbCrLf)
            sb.Append("WHERE	M.ResourceTypeId = 1 AND " & vbCrLf)
            ' 7/29/06: Ben - Only bring in modules that have more than 1 entity associated with it
            sb.Append("     (select count(*) from syAdvantageResourceRelations R, syResources E " + vbCrLf)
            sb.Append("         where R.ResourceId = E.ResourceId and R.RelatedResourceId = M.ResourceId and E.ResourceTypeId=8) > 0 " + vbCrLf)
            sb.Append("ORDER BY	M.Resource ")

            ' (Future) UserId not used right now
            If Not UserId Is Nothing And UserId <> "" Then
            End If
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        Public Shared Function GetModuleEntities(ByVal ModuleId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ' build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append("		   R.ResRelId as ModuleEntityId,		" & vbCrLf)
            sb.Append("        E.ResourceId as EntityId," & vbCrLf)
            sb.Append("        E.Resource as Descrip" & vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("        syAdvantageResourceRelations R," + vbCrLf)
            sb.Append("        syResources E," + vbCrLf)
            sb.Append("        syResources M " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("        R.ResourceId = E.ResourceId and " + vbCrLf)
            sb.Append("        R.RelatedResourceId = M.ResourceId and " + vbCrLf)
            sb.Append("        E.ResourceTypeId=8 and " + vbCrLf)
            sb.Append("        M.ResourceTypeId=1 and " + vbCrLf)
            sb.Append("	 	   M.ResourceId = ? " + vbCrLf)
            db.AddParameter("Id", ModuleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieve all the entities from the system
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetEntities() As DataSet
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append("			ResourceId as EntityId, " & vbCrLf)
            sb.Append("			Resource as Descrip " & vbCrLf)
            sb.Append("FROM		syResources M " & vbCrLf)
            sb.Append("WHERE	M.ResourceTypeId = 8 " & vbCrLf)
            sb.Append("ORDER BY	M.Resource ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieve all the entities from the system except Lender
        ''' Added on 09/20/2006 by balaji just to get rid of Lenders from 
        ''' AdHocReports in ReporType DDL
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetEntities(ByVal entityId As Integer) As DataSet
            '   connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append(" SELECT " & vbCrLf)
            sb.Append("			ResourceId as EntityId, " & vbCrLf)
            sb.Append("			Resource as Descrip " & vbCrLf)
            sb.Append(" FROM		syResources M " & vbCrLf)
            sb.Append(" WHERE	M.ResourceTypeId = 8 " & vbCrLf)
            sb.Append(" and ResourceId <> " & entityId & vbCrLf)
            sb.Append(" ORDER BY M.Resource ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function
    End Class
#End Region

#Region "Input Masks"
    Public Class InputMasksDB
        Private Function GetSQL(ByVal tableName As String, ByVal zipLen As Integer) As String
            Dim strTemplate As String

            If tableName <> "faLenders" Then
                strTemplate = "SELECT COUNT(*) " & _
                                      "FROM %TableName% " & _
                                      "WHERE LEN(Zip) = %ZipLen% " & _
                                      "AND ForeignZip = 0"

            Else
                strTemplate = "SELECT COUNT(*) " & _
                              "FROM %TableName% " & _
                              "WHERE LEN(PayZip) = %ZipLen% " & _
                              "OR LEN(Zip) = %ZipLen% " & _
                              "AND ForeignAddress = 0 " & _
                              "OR ForeignPayAddress = 0"
            End If

            'Replace the tokens with the values passed in
            strTemplate = strTemplate.Replace("%TableName%", tableName)
            strTemplate = strTemplate.Replace("%ZipLen%", zipLen)

            Return strTemplate

        End Function

        Public Function GetInputMasks() As DataTable
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            Dim ds As New DataSet

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            With sb
                .Append("SELECT InputMaskId, Item, Mask, ModUser, ModDate ")
                .Append("FROM syInputMasks ")
            End With

            ds = db.RunParamSQLDataSet(sb.ToString)

            Return ds.Tables(0)
        End Function

        Public Function GetInputMaskForItem(ByVal itemId As Integer) As String
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            Dim s As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            With sb
                .Append("SELECT Mask ")
                .Append("FROM syInputMasks ")
                .Append("WHERE InputMaskId = ?")
            End With

            db.AddParameter("@itemid", itemId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
            s = CType(db.RunParamSQLScalar(sb.ToString), String)

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return s

        End Function

        Public Sub UpdateInputMask(ByVal inputMaskId As Integer, ByVal mask As String)
            Dim db As New DataAccess
            Dim sb As New StringBuilder

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            With sb
                .Append("UPDATE syInputMasks ")
                .Append("SET Mask = ? ")
                .Append("WHERE InputMaskId = ?")
            End With

            db.AddParameter("@mask", mask, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@inputmaskid", inputMaskId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        End Sub

        Public Function UpdateInputMaskInfo(ByVal iMaskInfo As InputMaskInfo, ByVal user As String) As String
            Dim db As New DataAccess
            Dim sb As New StringBuilder

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            Try
                With sb
                    .Append("UPDATE syInputMasks ")
                    .Append("SET Mask = ?, ModUser = ?, ModDate = ? ")
                    .Append("WHERE InputMaskId = ? ")
                    .Append("AND ModDate = ? ;")
                    .Append("SELECT COUNT(*) FROM syInputMasks WHERE ModDate = ? and InputMaskId = ? ")
                End With

                db.AddParameter("@Mask", iMaskInfo.Mask, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   ModDate
                Dim now As Date = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   InputMaskId
                db.AddParameter("@InputMaskId", iMaskInfo.InputMaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   Original ModDate
                db.AddParameter("@Original_ModDate", iMaskInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   ModDate
                db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   InputMaskId
                db.AddParameter("@InputMaskId", iMaskInfo.InputMaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   execute the query
                Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

                'db.RunParamSQLExecuteNoneQuery(sb.ToString)
                'db.ClearParameters()

                '   If there were no updated rows then there was a concurrency problem
                If rowCount = 1 Then
                    '   return without errors
                    Return ""
                Else
                    'Return DALExceptions.BuildConcurrencyExceptionMessage()
                End If

            Catch ex As OleDbException
                '   return an error to the client
                'Return DALExceptions.BuildErrorMessage(ex)

            Finally
                'Close Connection
                db.CloseConnection()
            End Try
            Return Nothing
        End Function

        Public Function UpdateInputMaskDS(ByVal ds As DataSet) As String

            '   all updates must be encapsulated as one transaction

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
            connection.Open()

            Try
                '   build the sql query for the StudentAwardSchedules data adapter
                Dim sb As New StringBuilder
                With sb
                    .Append("SELECT InputMaskId, Item, Mask, ModUser, ModDate FROM syInputMasks")
                End With

                '   build select command
                Dim InputMaskCommand As New OleDbCommand(sb.ToString, connection)

                '   Create adapter to handle syInputMasks table
                Dim InputMaskDataAdapter As New OleDbDataAdapter(InputMaskCommand)

                '   build insert, update and delete commands for syInputMasks table
                Dim cb As New OleDbCommandBuilder(InputMaskDataAdapter)

                '   insert added rows in syInputMasks table
                InputMaskDataAdapter.Update(ds.Tables("InputMasks").Select(Nothing, Nothing, DataViewRowState.Added))

                '   delete rows in syInputMasks table
                InputMaskDataAdapter.Update(ds.Tables("InputMasks").Select(Nothing, Nothing, DataViewRowState.Deleted))

                '   update rows in syInputMasks table
                InputMaskDataAdapter.Update(ds.Tables("InputMasks").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

                '   return no errors
                Return ""

            Catch ex As OleDb.OleDbException
                '   something went wrong
                '   return error message
                'Return DALExceptions.BuildErrorMessage(ex)

            Catch ex As Exception
                '   something went wrong
                '   return error message
                Return ex.Message

            Finally

                '   close connection
                connection.Close()
            End Try
            Return Nothing
        End Function

        Public Function GetCountOfRecordsWithSpecifiedZipLength(ByVal tableName As String, ByVal zipLen As Integer) As Integer
            Dim strSQL As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            strSQL = GetSQL(tableName, zipLen)

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Return CInt(db.RunParamSQLScalar(strSQL))

        End Function

    End Class
#End Region

#Region "Campus Groups"
    Public Class CampGroupsDB
        Public Shared Function GetCampusGroups() As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ' build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append(" CampGrpId, " & vbCrLf)
            sb.Append(" CampGrpDescrip " & vbCrLf)
            sb.Append("FROM syCampGrps")
            ' return the result
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function
        Public Shared Function GetCampusGroups(ByVal CampusId As String, ByVal UserId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ' build the sql query
            ' query returns all campus group if the user is SA or AdvantageSuperUser
            ' if user is not sa or Advantage Super User returns only the campus the user has access to 
            Dim isSAorAdvantageSuperUser As Boolean = (New UserSecurityDB).IsSAorAdvantageSuperUser(UserId)

            Dim sb As New StringBuilder
            sb.Append(" SELECT Distinct ")
            sb.Append("             CG.CampGrpId, ")
            sb.Append("             CG.CampGrpDescrip ")
            sb.Append(" FROM        syCampGrps CG ")
            If Not isSAorAdvantageSuperUser Then
                sb.Append("         ,syUsersRolesCampGrps URCG ")
                sb.Append(" WHERE   CG.CampGrpId = URCG.CampGrpId ")
                sb.Append("         AND CG.CampGrpId IN (SELECT Distinct CampGrpId FROM syCmpGrpCmps WHERE CampusId='" & CampusId & "') ")
                sb.Append("         AND URCG.UserId ='" & UserId & "'  ")
            End If
            sb.Append(" ORDER BY CG.CampGrpDescrip ")
            ' return the result
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function
    End Class
#End Region

#Region "Special Fields / Entity to Field Groups mapping"
    Public Class EntitiesFieldGroupsDB
        ''' <summary>
        ''' Get all the allowable field groups for a specific entity
        ''' </summary>
        ''' <param name="ShowActive"></param>
        ''' <param name="ShowInactive"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAll(ByVal EntityId As String, ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append(" EntityFieldGroupId, " & vbCrLf)
            sb.Append(" EntityId, " & vbCrLf)
            sb.Append(" FieldGroupName, " & vbCrLf)
            sb.Append(" ModDate, " & vbCrLf)
            sb.Append(" ModUser, " & vbCrLf)
            sb.Append(" Active " & vbCrLf)
            sb.Append("FROM msgEntitiesFieldGroups " & vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            sb.Append(" EntityId = ? " + vbCrLf)
            db.AddParameter("@EntityId", EntityId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            If ShowActive And Not ShowInactive Then
                sb.Append("AND Active = ? " & vbCrLf)
                db.AddParameter("@Active", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            ElseIf Not ShowActive And ShowInactive Then
                sb.Append("AND Active = ? " & vbCrLf)
                db.AddParameter("@Active", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If
            sb.Append("ORDER BY FieldGroupName asc ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Return an EntityId from the given ModuleEntityId
        ''' A ModuleEntityId is really a ResRelId from syAdvantageResourceRelations
        ''' </summary>
        ''' <param name="ModuleEntityId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetEntityId(ByVal ModuleEntityId As String) As String
            Try
                '   connect to the database
                Dim db As New DataAccess

                Dim MyAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    MyAdvAppSettings = New AdvAppSettings
                End If

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                '   build the sql query                
                Dim sb As New StringBuilder()
                sb.Append("SELECT " + vbCrLf)
                sb.Append("     E.ResourceId " + vbCrLf)
                sb.Append("FROM syResources E, syAdvantageResourceRelations R " + vbCrLf)
                sb.Append("WHERE " + vbCrLf)
                sb.Append("     R.ResRelId = ? and " + vbCrLf)
                sb.Append("     E.ResourceTypeId = 8 and E.ResourceId = R.ResourceId ")
                db.AddParameter("@ModuleEntityId", ModuleEntityId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Return db.RunParamSQLScalar(sb.ToString()).ToString()
            Catch ex As Exception
            End Try
            Return ""
        End Function

        ''' <summary>
        ''' Retrieve all the entities from the system
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetEntities() As DataSet
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append("			ResourceId as EntityId, " & vbCrLf)
            sb.Append("			Resource as Descrip " & vbCrLf)
            sb.Append("FROM		syResources M " & vbCrLf)
            sb.Append("WHERE	M.ResourceTypeId = 8 " & vbCrLf)
            sb.Append("ORDER BY	M.Resource ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieve a EntityFieldGroupInfo object from the database
        ''' </summary>
        ''' <param name="EntityFieldGroupId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetInfo(ByVal EntityFieldGroupId As String) As EntityFieldGroupInfo
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append(" EntityFieldGroupId, " & vbCrLf)
            sb.Append(" EntityId, " & vbCrLf)
            sb.Append(" FieldGroupName, " & vbCrLf)
            sb.Append(" ModDate, " & vbCrLf)
            sb.Append(" ModUser, " & vbCrLf)
            sb.Append(" Active " & vbCrLf)
            sb.Append("FROM msgEntitiesFieldGroups " & vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("   EntityFieldGroupId = ? ")
            db.AddParameter("@EntityFieldGroupId", EntityFieldGroupId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            Try
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                Dim info As New EntityFieldGroupInfo
                If dr.Read() Then
                    '   set properties with data from DataReader
                    info.EntityFieldGroupId = EntityFieldGroupId
                    info.IsInDB = True
                    If Not (dr("EntityId") Is System.DBNull.Value) Then info.EntityId = CType(dr("EntityId"), Guid).ToString()
                    If Not (dr("FieldGroupName") Is System.DBNull.Value) Then info.FieldGroupName = dr("FieldGroupName")
                    If Not (dr("ModUser") Is System.DBNull.Value) Then info.ModUser = dr("ModUser").ToString()
                    If Not (dr("ModDate") Is System.DBNull.Value) Then info.ModDate = dr("ModDate")
                    If Not (dr("Active") Is System.DBNull.Value) Then info.Active = CType(dr("Active"), Integer)
                End If

                If Not dr.IsClosed Then dr.Close()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

                Return info
            Catch ex As Exception
            End Try
            Return Nothing
        End Function

        ''' <summary>
        ''' Update a EntityFieldGroupInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Update(ByVal info As EntityFieldGroupInfo, ByVal user As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("UPDATE msgEntitiesFieldGroups " & vbCrLf)
                sb.Append("SET " & vbCrLf)
                sb.Append(" EntityId = ?, " & vbCrLf)
                sb.Append(" FieldGroupName = ?," & vbCrLf)
                sb.Append(" ModUser = ?, ModDate = ?, " & vbCrLf)
                sb.Append(" Active = ?" + vbCrLf)
                sb.Append("WHERE EntityFieldGroupId = ? ")

                '   add parameters values to the query            
                If info.EntityId Is Nothing Or info.EntityId = Guid.Empty.ToString Then
                    db.AddParameter("@EntityId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@EntityId", info.EntityId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@FieldGroupName", info.FieldGroupName, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)
                If info.ModUser Is Nothing Or info.ModUser = Guid.Empty.ToString Then
                    db.AddParameter("@ModUser", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ModUser", info.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                Dim now As Date = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@Active", info.Active, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@EntityFieldGroupId", info.EntityFieldGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                ' run the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return False
        End Function

        ''' <summary>
        ''' Persists a new EntityFieldGroupInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Add(ByVal info As EntityFieldGroupInfo, ByVal user As String) As Boolean
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do an insert
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("INSERT msgEntitiesFieldGroups (EntityFieldGroupId, EntityId, FieldGroupName, ModDate, ModUser, Active) ")
                sb.Append(" VALUES (?,?,?,?,?,?) ")

                '   add parameters values to the query

                db.AddParameter("@EntityFieldGroupId", info.EntityFieldGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.EntityId Is Nothing Or info.EntityId = "" Or info.EntityId = Guid.Empty.ToString Then
                    db.AddParameter("@EntityId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@EntityId", info.EntityId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@FieldGroupName", info.FieldGroupName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If user Is Nothing Or user = Guid.Empty.ToString Then
                    db.AddParameter("@ModUser", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@Active", info.Active, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                '   return without errors
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return False
        End Function

        ''' <summary>
        ''' Delete from msgEntitiesFieldGroups given a EntityFieldGroupId
        ''' </summary>
        ''' <param name="EntityFieldGroupId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Delete(ByVal EntityFieldGroupId As String) As Integer
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do a delete
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("DELETE FROM msgEntitiesFieldGroups ")
                sb.Append(" WHERE EntityFieldGroupId = ? ")
                db.AddParameter("@EntityFieldGroupId", EntityFieldGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                '   execute the query
                Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)
                db.CloseConnection()
                '   If the row was not deleted then there was a concurrency problem
                Return rowCount
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.ErrorCode
            End Try
        End Function

        ''' <summary>
        ''' Delete all field group mappings for a specific entityid
        ''' </summary>
        ''' <param name="EntityId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DeleteAll(ByVal EntityId As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do a delete
            Try
                '   build the query
                Dim sql As String = "DELETE msgEntitiesFieldGroups WHERE EntityId = ? "
                db.AddParameter("@EntityId", EntityId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Dim rowCount As Integer = db.RunParamSQLScalar(sql)
                db.CloseConnection()
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return False
        End Function
    End Class
#End Region

End Namespace