Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class FIC_C3DetailObjectDB

	Public Shared Function GetReportDatasetRaw(ByVal RptParamInfo As ReportParamInfoIPEDS) As DataSet
		Dim sb As New System.Text.StringBuilder
		Dim dsRaw As New DataSet
		Dim DateStart As DateTime = RptParamInfo.RptEndDate.AddYears(-1)
		Dim StudentList As String

		' get list of Leads and relevant info 
		With sb
			.Append("SELECT ")
			.Append("adLeads.LeadId, adLeads.SSN, ")
			.Append("adLeads.LastName, adLeads.FirstName, adLeads.MiddleName, ")
			.Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
			.Append("	FROM adGenders, ")
			.Append("		 syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
			.Append("	WHERE adGenders.GenderId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("	      syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("	      syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("	      syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("	      syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
			.Append("	      adGenders.GenderId = adLeads.Gender) AS GenderDescrip ")
			.Append("FROM adLeads ")
			.Append("WHERE ")
			.Append("adLeads.CampusId = '" & RptParamInfo.FilterCampusID & "' AND ")
			.Append("adLeads.DateApplied BETWEEN '" & FmtRptDateParam(DateStart) & "' AND '" & FmtRptDateParam(RptParamInfo.RptEndDate) & "' ")

			.Append("ORDER BY ")
			Select Case RptParamInfo.SortByStu
				Case SortByStudentStudentId
					.Append("adLeads.SSN")
				Case SortByStudentLastName
					.Append("adLeads.LastName, adLeads.FirstName, adLeads.MiddleName")
			End Select

			.Append(";")

            StudentList = IPEDSDB.GetStudentList(RptParamInfo, , "EnrollDate >= '" & FmtRptDateParam(DateStart) & "'")
            Dim strSortBy As String = IPEDSDB.GetSQL_StudentIdentifier()
            If StudentList <> "" Then
                .Append("SELECT DISTINCT arStudent.StudentId, ")
                .Append(strSortBy)
                .Append("   ,arStudent.SSN, ")
                .Append(", arStudent.LastName, arStudent.FirstName, arStudent.MiddleName, ")
                .Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
                .Append("	FROM adGenders, ")
                .Append("		 syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
                .Append("	WHERE adGenders.GenderId = syRptAgencySchoolMapping.SchoolDescripId AND ")
                .Append("	      syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
                .Append("	      syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
                .Append("	      syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
                .Append("	      syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
                .Append("	      adGenders.GenderId = arStudent.Gender) AS GenderDescrip ")
                .Append("FROM arStudent ")

                ' append list of appropriate students to include
                .Append("WHERE arStudent.StudentId IN (" & StudentList & ") ")

                ' get list of enrollments and relevant info for same list of students
                .Append(";" & IPEDSDB.GetSQL_EnrollmentInfo(StudentList, RptParamInfo.FilterProgramIDs))
            End If
		End With

		dsRaw = IPEDSDB.DataAccessIPEDS().RunSQLDataSet(sb.ToString)

		' set table names for each part of returned data
		With dsRaw
			If .Tables(0).Rows.Count = 0 Then
				Return New DataSet
			End If
			.Tables(0).TableName = TblNameLeads
			If StudentList <> "" Then
				.Tables(1).TableName = TblNameStudents
				.Tables(2).TableName = TblNameEnrollments
			End If
		End With

		Return dsRaw

	End Function

End Class
