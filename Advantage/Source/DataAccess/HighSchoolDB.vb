Imports FAME.Advantage.Common

Public Class HighSchoolDB

   Private Function AddInstitutionAddress(ByVal HighSchoolInfo As HighSchool, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


         Try
            '   build the query
            Dim sb,sb1 As New StringBuilder
            With sb
               .Append(" INSERT INTO syInstitutionAddresses(InstitutionAddressId,InstitutionId,AddressTypeId,Address1,Address2,City,StateId,ZipCode,CountryId,StatusId,IsMailingAddress, ")
		        .Append(" ModUser,ModDate,State,IsInternational,CountyId,ForeignCountyStr,AddressApto,OtherState,IsDefault) ")
                .Append(" Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
            End With


           '   add parameters values to the query

           db.AddParameter("@InstitutionAddressId", System.Guid.NewGuid().ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   High School Id
            db.AddParameter("@InstitutionId", HighSchoolInfo.HsId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Address Type 
            If HighSchoolInfo.AddressTypeId = "" Then
                db.AddParameter("@AddressTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@AddressTypeId", HighSchoolInfo.AddressTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Address1
            If HighSchoolInfo.Address1 = "" Then
                db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address1", HighSchoolInfo.Address1, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            If HighSchoolInfo.Address2 = "" Then
                db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", HighSchoolInfo.Address2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'City
            If HighSchoolInfo.City = "" Then
                db.AddParameter("@City", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@City", HighSchoolInfo.City, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'StateId
            If HighSchoolInfo.StateId = "" Then
                db.AddParameter("@StateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StateId", HighSchoolInfo.StateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Zip
            If HighSchoolInfo.Zip = "" Then
                db.AddParameter("@ZipCode", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ZipCode", HighSchoolInfo.Zip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'CountryId
            If HighSchoolInfo.CountryId = "" Then
                db.AddParameter("@CountryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CountryId", HighSchoolInfo.CountryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

              'StatusId
            If HighSchoolInfo.StatusId = "" Then
                db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StatusId", HighSchoolInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

           'Mailing Address
            db.AddParameter("@IsMailingAddress", 0, DataAccess.OleDbDataType.OleDbInteger , 50, ParameterDirection.Input)
          

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", HighSchoolInfo.modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

             '   State
            db.AddParameter("@State", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'IsInternational
            db.AddParameter("@IsInternational", HighSchoolInfo.ForeignZip, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

             'CountyId
            If HighSchoolInfo.CountyId = "" Then
                db.AddParameter("@CountyId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CountyId", HighSchoolInfo.CountyId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

             'Foreign County
            db.AddParameter("@ForeignCountyStr", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'AddressApto
            db.AddParameter("@AddressApto", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'OtherState
            If HighSchoolInfo.OtherState = "" Then
                db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@OtherState", HighSchoolInfo.OtherState, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ForeignZip
            db.AddParameter("@IsDefault", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)


           '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

   Private Function AddInstitution(ByVal HighSchoolInfo As HighSchool, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb,sb1 As New StringBuilder
            With sb
                .Append(" INSERT syInstitutions (HsId, HsCode, StatusId, ")
                .Append(" HsName,  ")
                .Append(" CampGrpId, ModUser, ModDate,ImportTypeId,LevelId) ")
                .Append(" VALUES (?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   High School Id
            db.AddParameter("@HsId", HighSchoolInfo.HsId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Code
            db.AddParameter("@HsCode", HighSchoolInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", HighSchoolInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   AcademicYearDescrip
            db.AddParameter("@HsName", HighSchoolInfo.Name, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If HighSchoolInfo.CampusGroup = Guid.Empty.ToString Or HighSchoolInfo.CampusGroup = "" Then
                db.AddParameter("@CampusGroup", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampusGroup", HighSchoolInfo.CampusGroup, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", HighSchoolInfo.modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.AddParameter("@ImportTypeId", HighSchoolInfo.Source, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

          
            'InstitutionType
             db.AddParameter("@LevelId", HighSchoolInfo.InstitutionType, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)


            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)


            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Function AddHighSchoolInfo(ByVal HighSchoolInfo As HighSchool, ByVal user As String) As String

    Try
      Dim strAddInstitutionStatus As String = AddInstitution(HighSchoolInfo,user)
      Dim strAddInstitutionAddressStatus As String = AddInstitutionAddress(HighSchoolInfo,user)
      Dim strAddInstitutionPhone As String = AddInstitutionPhone(HighSchoolInfo,user)
      Dim strAddInstitutionContacts As String = AddInstitutionContacts(HighSchoolInfo,user)
      Return ""
     Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

     Finally
     
     End Try


    End Function
    Private Function AddInstitutionPhone(highSchoolInfo As HighSchool, user As String) As String
          '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


         Try
            '   build the query
            Dim sb,sb1 As New StringBuilder
            With sb
               .Append(" INSERT INTO syInstitutionPhone(InstitutionPhoneId,InstitutionId,PhoneTypeId,Phone,ModUser,ModDate,IsForeignPhone,StatusId,IsDefault) ")
                .Append(" Values(?,?,?,?,?,?,?,?,?)")
            End With


           '   add parameters values to the query
           db.AddParameter("@InstitutionPhoneId", System.Guid.NewGuid().ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   High School Id
            db.AddParameter("@InstitutionId", HighSchoolInfo.HsId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            db.AddParameter("@PhoneTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

             'Phone
            If HighSchoolInfo.Phone = "" Then
                db.AddParameter("@Phone", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Phone", HighSchoolInfo.Phone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

             '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", HighSchoolInfo.modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'IsForeignPhone
            db.AddParameter("@IsForeignPhone", 0, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", HighSchoolInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

             '   StatusId
            db.AddParameter("@IsDefault", 1, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)


          '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Private Function AddInstitutionContacts(highSchoolInfo As HighSchool, user As String) As String
          '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


         Try
            '   build the query
            Dim sb,sb1 As New StringBuilder
            With sb
               .Append(" INSERT INTO syInstitutionContacts(InstitutionContactId,InstitutionId,FirstName,LastName,MiddleName,StatusId,PrefixId,SuffixId,Title,Phone,PhoneExt,ForeignPhone,Email,ModUser,ModDate,IsDefault) ")
                .Append(" Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
            End With


           '   add parameters values to the query
           db.AddParameter("@InstitutionContactId", System.Guid.NewGuid().ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   High School Id
            db.AddParameter("@InstitutionId", HighSchoolInfo.HsId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


             'FirstName
            If highSchoolInfo.ContactFirstName = "" Then
                db.AddParameter("@FirstName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@FirstName", HighSchoolInfo.ContactFirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

              'LastName
            If highSchoolInfo.ContactLastName = "" Then
                db.AddParameter("@LastName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@LastName", HighSchoolInfo.ContactLastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

               'MiddleName
            If highSchoolInfo.ContactMiddleName = "" Then
                db.AddParameter("@MiddleName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@MiddleName", highSchoolInfo.ContactMiddleName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

             '   StatusId
            db.AddParameter("@StatusId", HighSchoolInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Prefix
            If highSchoolInfo.PrefixId = "" Then
                db.AddParameter("@PrefixId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PrefixId", highSchoolInfo.PrefixId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

              'Suffix
            If highSchoolInfo.SuffixId = "" Then
                db.AddParameter("@SuffixId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@SuffixId", highSchoolInfo.SuffixId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

             ' Title
            If highSchoolInfo.ContactTitle = "" Then
                db.AddParameter("@Title", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Title", HighSchoolInfo.ContactTitle, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Phone
            If highSchoolInfo.ContactPhone = "" Then
                db.AddParameter("@Phone", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Phone", HighSchoolInfo.ContactPhone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Phone Ext
            If highSchoolInfo.ContactPhoneExt = "" Then
                db.AddParameter("@PhoneExt", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PhoneExt", HighSchoolInfo.ContactPhoneExt, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'IsForeignPhone
            db.AddParameter("@IsForeignPhone", 0, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

             'Phone
            If highSchoolInfo.ContactEmail = "" Then
                db.AddParameter("@Email", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Email", HighSchoolInfo.ContactEmail, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

             '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", HighSchoolInfo.modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    

             '   StatusId
            db.AddParameter("@IsDefault", 1, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)


          '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function


    Private Function UpdateInstitution(ByVal HighSchoolInfo As HighSchool, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb,sb1 As New StringBuilder
            With sb
                .Append(" Update syInstitutions Set HsCode=?, StatusId=?, ")
                .Append(" HsName=?,  ")
                .Append(" CampGrpId=?, ModUser=?, ModDate=?,ImportTypeId=?,LevelId=? ")
                .Append(" Where HsId=? ")
            End With

            '   add parameters values to the query

           
            '   Code
            db.AddParameter("@HsCode", HighSchoolInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", HighSchoolInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   AcademicYearDescrip
            db.AddParameter("@HsName", HighSchoolInfo.Name, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If HighSchoolInfo.CampusGroup = Guid.Empty.ToString Or HighSchoolInfo.CampusGroup = "" Then
                db.AddParameter("@CampusGroup", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampusGroup", HighSchoolInfo.CampusGroup, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", HighSchoolInfo.modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

             db.AddParameter("@ImportTypeId", HighSchoolInfo.Source, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

          
            'InstitutionType
             db.AddParameter("@LevelId", HighSchoolInfo.InstitutionType, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)


             '   High School Id
            db.AddParameter("@HsId", HighSchoolInfo.HsId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)


            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Private Function UpdateInstitutionAddress(ByVal HighSchoolInfo As HighSchool, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


         Try
            '   build the query
            Dim sb,sb1 As New StringBuilder
            With sb
               .Append(" Update syInstitutionAddresses Set AddressTypeId=?,Address1=?,Address2=?,City=?,StateId=?,ZipCode=?, ")
                .Append(" CountryId=?,StatusId=?,IsMailingAddress=?, ")
		        .Append(" ModUser=?,ModDate=?,State=?,IsInternational=?,CountyId=?,ForeignCountyStr=?,AddressApto=?,OtherState=?,IsDefault=? ")
                '.Append(" Where InstitutionId=? and StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'")
                .Append(" Where InstitutionAddressId=? ")
            End With


           '   add parameters values to the query

                       'Address Type 
            If HighSchoolInfo.AddressTypeId = "" Then
                db.AddParameter("@AddressTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@AddressTypeId", HighSchoolInfo.AddressTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Address1
            If HighSchoolInfo.Address1 = "" Then
                db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address1", HighSchoolInfo.Address1, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            If HighSchoolInfo.Address2 = "" Then
                db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", HighSchoolInfo.Address2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'City
            If HighSchoolInfo.City = "" Then
                db.AddParameter("@City", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@City", HighSchoolInfo.City, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'StateId
            If HighSchoolInfo.StateId = "" Then
                db.AddParameter("@StateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StateId", HighSchoolInfo.StateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Zip
            If HighSchoolInfo.Zip = "" Then
                db.AddParameter("@ZipCode", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ZipCode", HighSchoolInfo.Zip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'CountryId
            If HighSchoolInfo.CountryId = "" Then
                db.AddParameter("@CountryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CountryId", HighSchoolInfo.CountryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

              'StatusId
            If HighSchoolInfo.StatusId = "" Then
                db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StatusId", HighSchoolInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

           'Mailing Address
            db.AddParameter("@IsMailingAddress", 0, DataAccess.OleDbDataType.OleDbInteger , 50, ParameterDirection.Input)
          

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", HighSchoolInfo.modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

             '   State
            db.AddParameter("@State", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'IsInternational
            db.AddParameter("@IsInternational", HighSchoolInfo.ForeignZip, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

             'CountyId
            If HighSchoolInfo.CountyId = "" Then
                db.AddParameter("@CountyId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CountyId", HighSchoolInfo.CountyId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

             'Foreign County
            db.AddParameter("@ForeignCountyStr", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'AddressApto
            db.AddParameter("@AddressApto", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'OtherState
            If HighSchoolInfo.OtherState = "" Then
                db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@OtherState", HighSchoolInfo.OtherState, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ForeignZip
            db.AddParameter("@IsDefault", HighSchoolInfo.IsDefaultAdd, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   High School Id
            db.AddParameter("@InstitutionAddressId", HighSchoolInfo.InstitutionAddressId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)


           '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Private Function UpdateInstitutionPhone(highSchoolInfo As HighSchool, user As String) As String
          '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


         Try
            '   build the query
            Dim sb,sb1 As New StringBuilder
            With sb
               .Append(" Update syInstitutionPhone set PhoneTypeId=?,Phone=?,ModUser=?,ModDate=?,IsForeignPhone=?,StatusId=?,IsDefault=? ")
                .Append(" Where InstitutionPhoneId=? ")
            End With


            db.AddParameter("@PhoneTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

             'Phone
            If HighSchoolInfo.Phone = "" Then
                db.AddParameter("@Phone", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Phone", highSchoolInfo.Phone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

             '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", highSchoolInfo.modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'IsForeignPhone
            db.AddParameter("@IsForeignPhone", highSchoolInfo.ForeignPhone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", highSchoolInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@IsDefault", highSchoolInfo.IsDefaultPhone, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)

            '   High School Id
            db.AddParameter("@InstitutionPhoneId", highSchoolInfo.InstitutionPhoneId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Private Function UpdateInstitutionContacts(highSchoolInfo As HighSchool, user As String) As String
          '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


         Try
            '   build the query
            Dim sb,sb1 As New StringBuilder
            With sb
               .Append(" Update syInstitutionContacts Set FirstName=?,LastName=?,MiddleName=?,StatusId=?,PrefixId=?,SuffixId=?, ")
                .Append(" Title=?,Phone=?,PhoneExt=?,ForeignPhone=?,Email=?,ModUser=?,ModDate=?,IsDefault=? ")
                .Append(" Where InstitutionContactId=?")
            End With


                      'FirstName
            If highSchoolInfo.ContactFirstName = "" Then
                db.AddParameter("@FirstName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@FirstName", HighSchoolInfo.ContactFirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

              'LastName
            If highSchoolInfo.ContactLastName = "" Then
                db.AddParameter("@LastName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@LastName", HighSchoolInfo.ContactLastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

               'MiddleName
            If highSchoolInfo.ContactMiddleName = "" Then
                db.AddParameter("@MiddleName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@MiddleName", highSchoolInfo.ContactMiddleName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

             '   StatusId
            db.AddParameter("@StatusId", HighSchoolInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Prefix
            If highSchoolInfo.PrefixId = "" Then
                db.AddParameter("@PrefixId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PrefixId", highSchoolInfo.PrefixId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

              'Suffix
            If highSchoolInfo.SuffixId = "" Then
                db.AddParameter("@SuffixId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@SuffixId", highSchoolInfo.SuffixId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

             ' Title
            If highSchoolInfo.ContactTitle = "" Then
                db.AddParameter("@Title", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Title", HighSchoolInfo.ContactTitle, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Phone
            If highSchoolInfo.ContactPhone = "" Then
                db.AddParameter("@Phone", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Phone", HighSchoolInfo.ContactPhone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Phone Ext
            If highSchoolInfo.ContactPhoneExt = "" Then
                db.AddParameter("@PhoneExt", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PhoneExt", HighSchoolInfo.ContactPhoneExt, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'IsForeignPhone
            db.AddParameter("@IsForeignPhone", 0, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

             'Phone
            If highSchoolInfo.ContactEmail = "" Then
                db.AddParameter("@Email", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Email", HighSchoolInfo.ContactEmail, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

             '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", HighSchoolInfo.modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)



            '   StatusId
            db.AddParameter("@IsDefault", highSchoolInfo.IsDefaultContact, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)

            '   High School Id
            db.AddParameter("@InstitutionContactId", highSchoolInfo.InstitutionContactId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function UpdateHighSchoolInfo(ByVal HighSchoolInfo As HighSchool, ByVal user As String) As String
         Try

      Dim boolContactsExists As Boolean = doesContactExist(HighSchoolInfo.HsId)
      Dim boolPhoneExists As Boolean = doesPhoneExist(HighSchoolInfo.HsId)
      Dim boolAddressExists As Boolean = doesAddressExist(HighSchoolInfo.HsId)
      Dim strAddInstitutionStatus,strAddInstitutionContacts,strAddInstitutionAddressStatus,strAddInstitutionPhone As String

      strAddInstitutionStatus = UpdateInstitution(HighSchoolInfo,user)
      If boolAddressExists = False Then
          strAddInstitutionAddressStatus = AddInstitutionAddress(HighSchoolInfo,user)
      Else
          strAddInstitutionAddressStatus = UpdateInstitutionAddress(HighSchoolInfo,user)
      End If
      If boolPhoneExists = False Then
          strAddInstitutionPhone = AddInstitutionPhone(HighSchoolInfo,user)
      Else
          strAddInstitutionPhone = UpdateInstitutionPhone(HighSchoolInfo,user)
      End If
      If boolContactsExists = False Then
           strAddInstitutionContacts = AddInstitutionContacts(HighSchoolInfo,user)
      Else
           strAddInstitutionContacts = UpdateInstitutionContacts(HighSchoolInfo,user)
      End If
      Return ""
     Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

     Finally
     
     End Try
     
    End Function
    Public Function GetHighSchoolInfo(ByVal HsId As String)
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT CCT.HsCode,CCT.HsId, ")
            .Append("    CCT.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=CCT.StatusId) As Status, ")
            .Append("    CCT.HSName, ")
            .Append("    (SELECT TOP 1 Address1 FROM syInstitutionAddresses WHERE InstitutionId=CCT.HSID Order by ModDate Desc  ) AS Address1, ")
            .Append("    (SELECT TOP 1 Address2 FROM syInstitutionAddresses WHERE InstitutionId=CCT.HSID Order by ModDate Desc ) AS Address2, ")
            .Append("    (SELECT TOP 1 City FROM syInstitutionAddresses WHERE InstitutionId=CCT.HSID Order by ModDate Desc) AS City,  ")
            .Append("    (SELECT TOP 1 StateId FROM syInstitutionAddresses WHERE InstitutionId=CCT.HSID Order by ModDate Desc) AS StateId, ")
            .Append("    (SELECT TOP 1 ZipCode FROM syInstitutionAddresses WHERE InstitutionId=CCT.HSID Order by ModDate Desc) AS Zip, ")
            .Append("    (SELECT TOP 1 CountryId FROM syInstitutionAddresses WHERE InstitutionId=CCT.HSID Order by ModDate Desc) AS CountryId,  ")
            .Append("    (SELECT TOP 1 IsInternational FROM syInstitutionAddresses WHERE InstitutionId=CCT.HSID Order by ModDate Desc ) AS IsInternational,  ")
            .Append("    (SELECT TOP 1 OtherState FROM syInstitutionAddresses WHERE InstitutionId=CCT.HSID Order by ModDate Desc) AS OtherState,  ")
            .Append("    (SELECT TOP 1 InstitutionAddressId FROM syInstitutionAddresses WHERE InstitutionId=CCT.HSID Order by ModDate Desc) AS InstitutionAddressId,  ")
            .Append("    (SELECT TOP 1 IsDefault FROM syInstitutionAddresses WHERE InstitutionId=CCT.HSID Order by ModDate Desc) AS IsDefaultAdd,  ")
            .Append("    CCT.CampGrpId, ")
            .Append("    (SELECT TOP 1 Phone FROM syInstitutionPhone WHERE InstitutionId=CCT.HSID Order by ModDate Desc) AS Phone,  ")
            .Append("    (SELECT TOP 1 IsForeignPhone FROM syInstitutionPhone WHERE InstitutionId=CCT.HSID Order by ModDate Desc) AS ForeignPhone,  ")
            .Append("    (SELECT TOP 1 InstitutionPhoneId FROM syInstitutionPhone WHERE InstitutionId=CCT.HSID Order by ModDate Desc) AS InstitutionPhoneId,  ")
            .Append("    (SELECT TOP 1 IsDefault FROM syInstitutionPhone WHERE InstitutionId=CCT.HSID Order by ModDate Desc) AS IsDefaultPhone,  ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=CCT.CampGrpId) As CampGrpDescrip, ")
            .Append("    CCT.ModUser, ")
            .Append("    CCT.ModDate,CCT.ImportTypeId,CCT.TypeId, ")
            .Append("    (SELECT TOP 1 CountyId FROM syInstitutionAddresses WHERE InstitutionId=CCT.HSID Order by ModDate Desc) AS CountyId, ")
            .Append("     (Select Top 1 CountyDescrip from adCounties t1 INNER JOIN syInstitutionAddresses t2 ON t1.CountyId=t2.CountyId WHERE InstitutionId=CCT.HSID ORDER BY t2.ModDate DESC ) As CountyDescrip, ")
            .Append("    (Select Status from syStatuses where StatusId=CCT.StatusId) As ContactStatus, ")
            .Append("    (SELECT TOP 1 PrefixId FROM syInstitutionContacts WHERE InstitutionId = CCT.HSId Order by ModDate Desc ) As PrefixId, ")
            .Append("    (SELECT TOP 1 SuffixId FROM syInstitutionContacts WHERE InstitutionId = CCT.HSId Order by ModDate Desc) As SuffixId, ")
            .Append("    (SELECT TOP 1 t1.PrefixDescrip FROM dbo.syPrefixes t1 INNER JOIN syInstitutionContacts t2 ON t1.PrefixId = t2.PrefixId WHERE InstitutionId = CCT.HSId ORDER BY t2.ModDate DESC ) As PrefixDescrip, ")
            .Append("    (SELECT TOP 1 t1.SuffixDescrip FROM dbo.sySuffixes t1 INNER JOIN syInstitutionContacts t2 ON t1.SuffixId = t2.SuffixId WHERE InstitutionId = CCT.HSId ORDER BY t2.ModDate DESC ) As SuffixDescrip, ")
            .Append("    (SELECT TOP 1 FirstName FROM syInstitutionContacts WHERE InstitutionId = CCT.HSId Order by ModDate Desc ) AS ContactFirstName, ")
            .Append("    (SELECT TOP 1 LastName FROM syInstitutionContacts WHERE InstitutionId = CCT.HSId Order by ModDate Desc ) AS ContactLastName, ")
            .Append("    (SELECT TOP 1 MiddleName FROM syInstitutionContacts WHERE InstitutionId = CCT.HSId Order by ModDate Desc ) AS ContactMiddleName, ")
            .Append("    (SELECT TOP 1 Title FROM syInstitutionContacts WHERE InstitutionId = CCT.HSId Order by ModDate Desc ) AS ContactTitle, ")
            .Append("    (SELECT TOP 1 Phone FROM syInstitutionContacts WHERE InstitutionId = CCT.HSId Order by ModDate Desc) AS ContactPhone, ")
            .Append("    (SELECT TOP 1 PhoneExt FROM syInstitutionContacts WHERE InstitutionId = CCT.HSId Order by ModDate Desc) AS ContactPhoneExt, ")
            .Append("    (SELECT TOP 1 Email FROM syInstitutionContacts WHERE InstitutionId = CCT.HSId Order by ModDate Desc) AS ContactEmail, ")
            .Append("    (SELECT TOP 1 InstitutionContactId FROM syInstitutionContacts WHERE InstitutionId = CCT.HSId Order by ModDate Desc) AS InstitutionContactId, ")
            .Append("    (SELECT TOP 1 IsDefault FROM syInstitutionContacts WHERE InstitutionId = CCT.HSId Order by ModDate Desc) AS IsDefaultContact, ")
            .Append("    (SELECT TOP 1 t1.Status FROM dbo.syStatuses t1 INNER JOIN syInstitutionContacts t2 ON t1.StatusId = t2.StatusId WHERE InstitutionId = CCT.HSId ) As ContactStatusId,LevelId ")
            .Append("FROM  syInstitutions CCT  ")
            .Append("WHERE CCT.HsId= ?  ")
        End With

        ' Add the AcademicYearId to the parameter list
        db.AddParameter("@HSId", HsId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim HighSchoolInfo As New HighSchool

        While dr.Read()

            '   set properties with data from DataReader
            With HighSchoolInfo
                .HsId = CType(dr("HsId"), Guid).ToString
                .IsInDB = True
                .Code = dr("HsCode")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Name = dr("HSName")
                If Not (dr("Address1") Is System.DBNull.Value) Then .Address1 = dr("Address1") Else .Address1 = ""
                If Not (dr("Address2") Is System.DBNull.Value) Then .Address2 = dr("Address2") Else .Address2 = ""
                If Not (dr("City") Is System.DBNull.Value) Then .City = dr("City") Else .City = ""
                If Not (dr("StateId") Is System.DBNull.Value) Then .StateId = CType(dr("StateId"), Guid).ToString Else .StateId = ""
                If Not (dr("CountryId") Is System.DBNull.Value) Then .CountryId = CType(dr("CountryId"), Guid).ToString Else .CountryId = ""
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampusGroup = CType(dr("CampGrpId"), Guid).ToString Else .CampusGroup = ""
                If Not (dr("Zip") Is System.DBNull.Value) Then .Zip = dr("Zip") Else .Zip = ""
                If Not (dr("Phone") Is System.DBNull.Value) Then .Phone = dr("Phone") Else .Phone = ""
                If Not (dr("ForeignPhone") Is System.DBNull.Value) Then .ForeignPhone = IIf(dr("ForeignPhone").ToString().Equals("True"), 1, 0) Else .ForeignPhone = 0
                If Not (dr("OtherState") Is System.DBNull.Value) Then .OtherState = dr("OtherState") Else .OtherState = ""
                If Not (dr("IsInternational") Is System.DBNull.Value) Then .ForeignZip = IIf(dr("IsInternational").ToString().Equals("True"), 1, 0) Else .ForeignZip = 0
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampusGroup = CType(dr("CampGrpId"), Guid).ToString
                .modDate = dr("ModDate")
                
                If Not (dr("ImportTypeId") Is System.DBNull.Value) AndAlso (dr("ImportTypeId")=1) Then .Source = 1 Else .Source=2
                If Not (dr("CountyId") Is System.DBNull.Value) Then .CountyId = CType(dr("CountyId"), Guid).ToString Else .CountyId=""
                If Not (dr("ContactTitle") Is System.DBNull.Value) Then .ContactTitle = dr("ContactTitle").ToString Else .ContactTitle=""
                If Not (dr("ContactFirstName") Is System.DBNull.Value) Then .ContactFirstName = dr("ContactFirstName").ToString Else .ContactFirstName=""
                If Not (dr("ContactLastName") Is System.DBNull.Value) Then .ContactLastName = dr("ContactLastName").ToString Else .ContactLastName=""
                If Not (dr("ContactMiddleName") Is System.DBNull.Value) Then .ContactMiddleName = dr("ContactMiddleName").ToString Else .ContactMiddleName=""
                If Not (dr("ContactEmail") Is System.DBNull.Value) Then .ContactEmail = dr("ContactEmail").ToString Else .ContactEmail=""
                If Not (dr("ContactPhone") Is System.DBNull.Value) Then .ContactPhone = dr("ContactPhone").ToString Else .ContactPhone=""
                If Not (dr("ContactPhoneExt") Is System.DBNull.Value) Then .ContactPhoneExt = dr("ContactPhoneExt").ToString Else .ContactPhoneExt=""
                If Not (dr("StatusId") Is System.DBNull.Value) Then .ContactStatus = CType(dr("StatusId"), Guid).ToString Else .ContactStatus=""
                If Not (dr("PrefixId") Is System.DBNull.Value) Then .PrefixId = CType(dr("PrefixId"), Guid).ToString Else .PrefixId=""
                If Not (dr("SuffixId") Is System.DBNull.Value) Then .SuffixId = CType(dr("SuffixId"), Guid).ToString Else .SuffixId=""
                If Not (dr("LevelId") Is System.DBNull.Value) Then .InstitutionType = dr("LevelId") Else .InstitutionType = 1

                If Not (dr("InstitutionAddressId") Is System.DBNull.Value) Then .InstitutionAddressId = CType(dr("InstitutionAddressId"), Guid).ToString Else .InstitutionAddressId = ""
                If Not (dr("InstitutionPhoneId") Is System.DBNull.Value) Then .InstitutionPhoneId = CType(dr("InstitutionPhoneId"), Guid).ToString Else .InstitutionPhoneId = ""
                If Not (dr("InstitutionContactId") Is System.DBNull.Value) Then .InstitutionContactId = CType(dr("InstitutionContactId"), Guid).ToString Else .InstitutionContactId = ""
                If Not (dr("IsDefaultAdd") Is System.DBNull.Value) Then .IsDefaultAdd = IIf(dr("IsDefaultAdd").ToString().Equals("True"), 1, 0) Else .IsDefaultAdd = 0
                If Not (dr("IsDefaultPhone") Is System.DBNull.Value) Then .IsDefaultPhone = IIf(dr("IsDefaultPhone").ToString().Equals("True"), 1, 0) Else .IsDefaultPhone = 0
                If Not (dr("IsDefaultContact") Is System.DBNull.Value) Then .IsDefaultContact = IIf(dr("IsDefaultContact").ToString().Equals("True"), 1, 0) Else .IsDefaultContact = 0
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return HighSchoolInfo

    End Function
    Public Function GetHighSchoolDataList(ByVal strSelectedIndex As Integer) As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append(" SELECT Top 100 CCT.HsCode,CCT.HsId,CCT.HSName,ST.Status, ")
            .Append(" CCT.StatusId ")
            .Append(" FROM  syInstitutions CCT,syStatuses ST ")
            .Append(" where CCT.StatusId = ST.StatusId  ")
            Select Case strSelectedIndex
                Case 0
                    .Append("And    ST.Status = 'Active' ")
                    .Append("ORDER BY CCT.HsName ")
                Case 1
                    .Append("AND    ST.Status = 'Inactive' ")
                    .Append("ORDER BY CCT.HSName ")
                Case Else
                    .Append("ORDER BY ST.Status, CCT.HSName ")
            End Select
        End With

        ' Add the AcademicYearId to the parameter list
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function getSchoolsList(ByVal strSelectedIndex As Integer, Optional ByVal HsName As String = "",Optional ByVal InstitutionType As Integer=0) As DataTable
        Dim db As New SQLDataAccess
        Dim ds As DataSet
        Dim strStoredProcedureName As String

         Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        strStoredProcedureName = "dbo.usp_highschool_getlist"

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            db.OpenConnection()
            db.AddParameter("@selectedIndex", strSelectedIndex, SqlDbType.Int, , ParameterDirection.Input)
            db.AddParameter("@HsName", HsName, SqlDbType.VarChar, , ParameterDirection.Input)
            db.AddParameter("@InstitutionType", InstitutionType, SqlDbType.Int, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP(strStoredProcedureName, "HighSchoolsList")
            Return ds.Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function doesContactExist(ByVal HsId As String) As Boolean

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("Select Count(*) FROM syInstitutionContacts where InstitutionId=?")
            End With

            '   add parameters values to the query

            '   AcademicYearId
            db.AddParameter("@InstitutionId", HsId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return False
            Else
                Return True
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function doesPhoneExist(ByVal HsId As String) As Boolean

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("Select Count(*) FROM syInstitutionPhone where InstitutionId=?")
            End With

            '   add parameters values to the query

            '   AcademicYearId
            db.AddParameter("@InstitutionId", HsId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return False
            Else
                Return True
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function doesAddressExist(ByVal HsId As String) As Boolean

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("Select Count(*) FROM syInstitutionAddresses where InstitutionId=?")
            End With

            '   add parameters values to the query

            '   AcademicYearId
            db.AddParameter("@InstitutionId", HsId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return False
            Else
                Return True
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function DeleteHighSchoolInfo(ByVal HsId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syInstitutionContacts where InstitutionId=?;")
                .Append("DELETE FROM syInstitutionAddresses where InstitutionId=?;")
                .Append("DELETE FROM syInstitutionPhone where InstitutionId=?;")
                .Append("DELETE FROM syInstitutions where HsId=?;")
                .Append("SELECT count(*) FROM syInstitutions WHERE HSId = ? ")
            End With

            '   add parameters values to the query

            '   AcademicYearId
            db.AddParameter("@InstitutionId", HsId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '   AcademicYearId
            db.AddParameter("@InstitutionId", HsId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '   AcademicYearId
            db.AddParameter("@InstitutionId", HsId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '   AcademicYearId
            db.AddParameter("@HSId", HsId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   AcademicYearId
            db.AddParameter("@HsId", HsId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function VerifyIfHighSchoolCanBeDeleted(ByVal HSId As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        ''Dim result As Boolean
        Dim sb As New StringBuilder

        Try
            '   build the query
            With sb
                .Append("SELECT LeadId ")
                .Append("FROM   adLeadEducation ")
                .Append("WHERE  EducationInstType='Schools' ")
                .Append("       AND EducationInstId=?")
            End With

            '   add parameter value to the query
            '   HSId
            db.AddParameter("@HSId", HSId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim obj As Object = db.RunParamSQLScalar(sb.ToString)

            '   Highschool is used in adLeadEducation table
            If Not (obj Is Nothing) Then
                '   return without errors
                Return "You cannot delete this item because it is being referenced in adLeadEducation Table."
            End If

            sb.Length = 0
            db.ClearParameters()


            '   build the query
            With sb
                .Append("SELECT StudentId ")
                .Append("FROM   plStEduExtracurricular ")
                .Append("WHERE  EducationInstId=?")
            End With

            '   add parameter value to the query
            '   HSId
            db.AddParameter("@HSId", HSId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            obj = db.RunParamSQLScalar(sb.ToString)

            '   Highschool is used in plStEduExtracurricular table
            If Not (obj Is Nothing) Then
                '   return without errors
                Return "You cannot delete this item because it is being referenced in plStEduExtracurricular Table."
            End If

            sb.Length = 0
            db.ClearParameters()


            '   build the query
            With sb
                .Append("SELECT StudentId ")
                .Append("FROM   plStudentEducation ")
                .Append("WHERE  EducationInstId=?")
            End With

            '   add parameter value to the query
            '   HSId
            db.AddParameter("@HSId", HSId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            obj = db.RunParamSQLScalar(sb.ToString)

            '   Highschool is used in plStudentEducation table
            If Not (obj Is Nothing) Then
                '   return without errors
                Return "You cannot delete this item because it is being referenced in plStudentEducation Table."
            End If

            '   Referential Integrity check:    
            '       Highschool can be deleted because it is not used in plStudentEducation, 
            '       neither in plStEduExtracurricular nor in adLeadEducation.
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

End Class
