Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' EmployeesDB.vb
'
' EmployeesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class EmployeesDB
    '
    '   EmployeesDB Class
    '
    Public Function GetInfo(ByVal empId As String) As EmployeeInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT     EM.StatusId, ")
            .Append("   (Select Status from syStatuses where StatusId=EM.StatusId) Status, ")
            .Append("   EM.LastName, ")
            .Append("   EM.FirstName, ")
            .Append("   EM.MI, ")
            .Append("   EM.PrefixId, ")
            .Append("   (Select PrefixDescrip from syPrefixes where PrefixId=EM.PrefixId) PrefixDescrip, ")
            .Append("   EM.SuffixId, ")
            .Append("   (Select SuffixDescrip from sySuffixes where SuffixId=EM.SuffixId) SuffixDescrip, ")
            .Append("    EM.SSN, ")
            .Append("    EM.ID, ")
            .Append("    EM.Birthdate, ")
            .Append("    EM.RaceId, ")
            .Append("   (Select EthCodeDescrip from adEthCodes where EthCodeId=EM.RaceId) RaceDescrip, ")
            .Append("    EM.GenderId, ")
            .Append("   (Select GenderDescrip from adGenders where GenderId=EM.GenderId) GenderDescrip, ")
            .Append("    EM.MaritalStatId, ")
            .Append("   (Select MaritalStatDescrip from adMaritalStatus where MaritalStatId=EM.MaritalStatId) MaritalStatusDescrip, ")
            .Append("   EM.Address1, ")
            .Append("   EM.Address2, ")
            .Append("   EM.City, ")
            .Append("   EM.StateId, ")
            .Append("   (Select StateDescrip from syStates where StateId=EM.StateId) StateDescrip, ")
            .Append("   EM.Zip, ")
            .Append("   EM.CountryId, ")
            .Append("   (Select CountryDescrip from AdCountries where CountryId=EM.CountryId) CountryDescrip, ")
            .Append("   EM.ForeignZip, ")
            .Append("   EM.OtherState, ")
            .Append("   EM.CampusId, ")
            .Append("   EM.ModUser, ")
            .Append("   EM.ModDate ")
            .Append("FROM  hrEmployees EM ")
            .Append("WHERE EM.empId = ? ")
        End With

        ' Add the empId to the parameter list
        db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim empInfo As New EmployeeInfo

        While dr.Read()

            '   set properties with data from DataReader
            With empInfo
                .IsInDB = True
                .EmpId = empId
                If Not (dr("StatusId") Is System.DBNull.Value) Then .StatusId = CType(dr("StatusId"), Guid).ToString Else .StatusId = Guid.Empty.ToString
                .Status = dr("Status")
                .LastName = dr("LastName")
                .FirstName = dr("FirstName")
                If Not (dr("MI") Is System.DBNull.Value) Then .MI = dr("MI")
                If Not (dr("PrefixId") Is System.DBNull.Value) Then .PrefixId = CType(dr("PrefixId"), Guid).ToString 'Else .PrefixId = Guid.Empty.ToString
                If Not (dr("PrefixDescrip") Is System.DBNull.Value) Then .Prefix = dr("PrefixDescrip")
                If Not (dr("SuffixId") Is System.DBNull.Value) Then .SuffixId = CType(dr("SuffixId"), Guid).ToString 'Else .SuffixId = Guid.Empty.ToString
                If Not (dr("SuffixDescrip") Is System.DBNull.Value) Then .Suffix = dr("SuffixDescrip")
                If Not (dr("SSN") Is System.DBNull.Value) Then .SSN = dr("SSN")
                If Not (dr("ID") Is System.DBNull.Value) Then .ID = dr("ID")
                If Not (dr("BirthDate") Is System.DBNull.Value) Then .BirthDate = dr("BirthDate")
                If Not (dr("RaceId") Is System.DBNull.Value) Then .RaceId = CType(dr("RaceId"), Guid).ToString 'Else .RaceId = Guid.Empty.ToString
                If Not (dr("RaceDescrip") Is System.DBNull.Value) Then .Race = dr("RaceDescrip")
                If Not (dr("GenderId") Is System.DBNull.Value) Then .GenderId = CType(dr("GenderId"), Guid).ToString 'Else .GenderId = Guid.Empty.ToString
                If Not (dr("GenderDescrip") Is System.DBNull.Value) Then .Gender = dr("GenderDescrip")
                If Not (dr("MaritalStatId") Is System.DBNull.Value) Then .MaritalStatId = CType(dr("MaritalStatId"), Guid).ToString 'Else .MaritalStatId = Guid.Empty.ToString
                If Not (dr("MaritalStatusDescrip") Is System.DBNull.Value) Then .MaritalStat = dr("MaritalStatusDescrip")
                If Not (dr("Address1") Is System.DBNull.Value) Then .Address1 = dr("Address1")
                If Not (dr("Address2") Is System.DBNull.Value) Then .Address2 = dr("Address2")
                If Not (dr("City") Is System.DBNull.Value) Then .City = dr("City")
                If Not (dr("StateId") Is System.DBNull.Value) Then .StateId = CType(dr("StateId"), Guid).ToString
                If Not (dr("StateDescrip") Is System.DBNull.Value) Then .State = dr("StateDescrip")
                If Not (dr("Zip") Is System.DBNull.Value) Then .Zip = dr("Zip")
                If Not (dr("CountryId") Is System.DBNull.Value) Then .CountryId = CType(dr("CountryId"), Guid).ToString
                If Not (dr("CountryDescrip") Is System.DBNull.Value) Then .Country = dr("CountryDescrip")
                If Not (dr("ForeignZip") Is System.DBNull.Value) Then .ForeignZip = dr("ForeignZip")
                If Not (dr("OtherState") Is System.DBNull.Value) Then .OtherState = dr("OtherState") Else .OtherState = ""
                If Not (dr("CampusId") Is System.DBNull.Value) Then .CampusId = CType(dr("CampusId"), Guid).ToString
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return EmployeeInfo
        Return empInfo

    End Function
    Public Function AddEmployee(ByVal empInfo As EmployeeInfo, ByVal user As String) As String
        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT hrEmployees (EmpId, StatusId, LastName, ")
                .Append("FirstName, MI, PrefixId, SuffixId, ")
                .Append("SSN, ID, BirthDate, RaceId, ")
                .Append("GenderId, MaritalStatId, ")
                .Append("Address1, Address2, City, ")
                .Append("StateId, Zip, CountryId, ForeignZip, OtherState, CampusId, ")
                .Append("ModUser, ModDate) ")
                .Append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?)")
            End With

            '   add parameters values to the query
            '   empId
            db.AddParameter("@EmpId", empInfo.EmpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            If empInfo.StatusId = Guid.Empty.ToString Then
                db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StatusId", empInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   LastName
            db.AddParameter("@LastName", empInfo.LastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   First Name
            db.AddParameter("@FirstName", empInfo.FirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   MI
            db.AddParameter("@MI", empInfo.MI, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   PrefixId
            If empInfo.PrefixId = Guid.Empty.ToString Then
                db.AddParameter("@PrefixId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PrefixId", empInfo.PrefixId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   SuffixId
            If empInfo.SuffixId = Guid.Empty.ToString Then
                db.AddParameter("@SuffixId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@SuffixId", empInfo.SuffixId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   SSN
            db.AddParameter("@SSN", empInfo.SSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ID
            db.AddParameter("@ID", empInfo.ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   BirthDate
            If empInfo.BirthDate = Date.MaxValue Then
                db.AddParameter("@BirthDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@BirthDate", empInfo.BirthDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            '   RaceId
            If empInfo.RaceId = Guid.Empty.ToString Then
                db.AddParameter("@RaceId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@RaceId", empInfo.RaceId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   GenderId
            If empInfo.GenderId = Guid.Empty.ToString Then
                db.AddParameter("@GenderId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@GenderId", empInfo.GenderId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   MaritalStatusId
            If empInfo.MaritalStatId = Guid.Empty.ToString Then
                db.AddParameter("@MaritalStatId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@MaritalStatId", empInfo.MaritalStatId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Address1
            If empInfo.Address1 = "" Then
                db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Address1", empInfo.Address1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Address2
            If empInfo.Address2 = "" Then
                db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", empInfo.Address2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   City
            If empInfo.City = "" Then
                db.AddParameter("@City", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@City", empInfo.City, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   StateId
            If empInfo.StateId = Guid.Empty.ToString Then
                db.AddParameter("@StateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@StateId", empInfo.StateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Zip
            If empInfo.Zip = "" Then
                db.AddParameter("@Zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Zip", empInfo.Zip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   CountryId
            If empInfo.CountryId = Guid.Empty.ToString Then
                db.AddParameter("@CountryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CountryId", empInfo.CountryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '  ForeignZip
            db.AddParameter("@ForeignZip", empInfo.ForeignZip, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            'OtherState
            If empInfo.OtherState = "" Then
                db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@OtherState", empInfo.OtherState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   CampusId
            If empInfo.CampusId = Guid.Empty.ToString Then
                db.AddParameter("@CampusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CampusId", empInfo.CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return ex.Message
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function UpdateInfo(ByVal empInfo As EmployeeInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE hrEmployees Set StatusId = ?, LastName = ?, ")
                .Append(" FirstName = ?, MI = ?, PrefixId = ?, SuffixId = ?, ")
                .Append(" SSN = ?, ID = ?, BirthDate = ?, RaceId = ?, ")
                .Append(" GenderId = ?, MaritalStatId = ?, ")
                .Append(" Address1 = ?, Address2 = ?, City = ?, ")
                .Append(" StateId = ?, Zip = ?, CountryId = ?, ForeignZip=?, OtherState=?, CampusId=?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE empId = ? ")
            End With

            '   add parameters values to the query

            '   StatusId
            If empInfo.StatusId = Guid.Empty.ToString Then
                db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StatusId", empInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   LastName
            db.AddParameter("@LastName", empInfo.LastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   First Name
            db.AddParameter("@FirstName", empInfo.FirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   MI
            db.AddParameter("@MI", empInfo.MI, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   PrefixId
            If empInfo.PrefixId = Guid.Empty.ToString Then
                db.AddParameter("@PrefixId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PrefixId", empInfo.PrefixId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   SuffixId
            If empInfo.SuffixId = Guid.Empty.ToString Then
                db.AddParameter("@SuffixId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@SuffixId", empInfo.SuffixId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   SSN
            db.AddParameter("@SSN", empInfo.SSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ID
            db.AddParameter("@ID", empInfo.ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   BirthDate
            If empInfo.BirthDate = Date.MaxValue Then
                db.AddParameter("@BirthDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@BirthDate", empInfo.BirthDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            '   RaceId
            If empInfo.RaceId = Guid.Empty.ToString Then
                db.AddParameter("@RaceId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@RaceId", empInfo.RaceId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   GenderId
            If empInfo.GenderId = Guid.Empty.ToString Then
                db.AddParameter("@GenderId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@GenderId", empInfo.GenderId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   MaritalStatusId
            If empInfo.MaritalStatId = Guid.Empty.ToString Then
                db.AddParameter("@MaritalStatId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@MaritalStatId", empInfo.MaritalStatId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Address1
            If empInfo.Address1 = "" Then
                db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Address1", empInfo.Address1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Address2
            If empInfo.Address2 = "" Then
                db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", empInfo.Address2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   City
            If empInfo.City = "" Then
                db.AddParameter("@City", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@City", empInfo.City, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   StateId
            If empInfo.StateId = Guid.Empty.ToString Then
                db.AddParameter("@StateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@StateId", empInfo.StateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Zip
            If empInfo.Zip = "" Then
                db.AddParameter("@Zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Zip", empInfo.Zip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   CountryId
            If empInfo.CountryId = Guid.Empty.ToString Then
                db.AddParameter("@CountryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CountryId", empInfo.CountryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '  ForeignZip
            db.AddParameter("@ForeignZip", empInfo.ForeignZip, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            'OtherState
            If empInfo.OtherState = "" Then
                db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@OtherState", empInfo.OtherState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   CampusId
            If empInfo.CampusId = Guid.Empty.ToString Then
                db.AddParameter("@CampusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CampusId", empInfo.CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   empId
            If empInfo.EmpId = Guid.Empty.ToString Then
                db.AddParameter("@EmpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@EmpId", empInfo.EmpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return ex.Message
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetSelectedRolesPerEmployee(ByVal empId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT     ER.RoleId, R.Role ")
            .Append("FROM       hrEmpRoles ER, syRoles R ")
            .Append("WHERE      ER.RoleId = R.RoleId ")
            .Append(" AND       ER.EmpId = ? ")
            .Append("ORDER BY   R.Role")
        End With

        ' Add the empId to the parameter list
        db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    Public Function GetAvailableRolesPerEmployee(ByVal empId As String) As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New System.Text.StringBuilder
        With sb
            .Append("SELECT     R.RoleId, R.Role ")
            .Append("FROM       syRoles R ")
            .Append("WHERE      (R.RoleId not in (select RoleId from hrEmpRoles where EmpId = ?)) ")
            .Append("ORDER BY   R.Role")
        End With

        ' Add EmpId to the parameter list
        db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    Public Function UpdateEmployeeRoles(ByVal empId As String, ByVal user As String, ByVal selectedRoles() As String)

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            '   First we have to delete all existing selections in the EmployeesRoles
            '   build the query
            Dim sb As New System.Text.StringBuilder
            With sb
                .Append("DELETE FROM hrEmpRoles WHERE EmpId = ?")
            End With

            '   Add the parameter with EmpId
            db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            '   Insert one record per each Item in the Selected Group
            Dim i As Integer
            For i = 0 To selectedRoles.Length - 1

                '   build the query
                With sb
                    .Append("INSERT INTO hrEmpRoles (EmpId, RoleId, ModDate, ModUser) ")
                    .Append("VALUES(?,?,?,?)")
                End With

                '   add parameters values to the query
                db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@RoleId", DirectCast(selectedRoles.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Next

            '   commit transaction 
            groupTrans.Commit()

        Catch ex As OleDbException
            groupTrans.Rollback()
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            End If
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetAvailableAssignedActivitiesPerEmployee(ByVal empId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New System.Text.StringBuilder
        With sb
            .Append("SELECT     R.RoleId, R.Role ")
            .Append("FROM       syRoles R ")
            .Append("WHERE     (R.RoleId not in (select RoleId from hrEmpAssActGrps where EmpId = ?)) ")
            .Append("ORDER BY   R.Role")
        End With

        ' Add the EmpId to the parameter list
        db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    Public Function GetSelectedAssignedActivitiesPerEmployee(ByVal empId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New System.Text.StringBuilder
        With sb
            .Append("SELECT    ER.RoleId, R.Role ")
            .Append("FROM      hrEmpAssActGrps ER, syRoles R ")
            .Append("WHERE     ER.RoleId = R.RoleId ")
            .Append(" AND      ER.EmpId = ? ")
            .Append("ORDER BY  R.Role")
        End With

        ' Add the EmpId to the parameter list
        db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    Public Function UpdateEmployeeAssignedActivities(ByVal empId As String, ByVal user As String, ByVal selectedAssignedActivities() As String)

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   First we have to delete all existing selections in the EmployeesAssignActivitiesRoles
        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("DELETE FROM hrEmpAssActGrps WHERE EmpId = ?")
        End With

        '   Add the parameter with EmpId
        db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   Execute the query
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        '   Insert one record per each Item in the Selected Group
        Dim i As Integer
        For i = 0 To selectedAssignedActivities.Length - 1

            '   build the query
            With sb
                .Append("INSERT INTO hrEmpAssActGrps (EmpId, RoleId, ModDate, ModUser) ")
                .Append("VALUES(?,?,?,?)")
            End With

            '   add parameters values to the query
            db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@RoleId", DirectCast(selectedAssignedActivities.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

        Next

        'Close Connection
        db.CloseConnection()

    End Function
    Public Function GetContactInfo(ByVal empId As String) As EmployeeContactInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT     EmpContactInfoId, ")
            .Append("           WorkPhone, ")
            .Append("           HomePhone, ")
            .Append("           CellPhone, ")
            .Append("           Beeper, ")
            .Append("           WorkEmail, ")
            .Append("           HomeEmail, ")
            .Append("           ForeignHomePhone, ")
            .Append("           ForeignWorkPhone, ")
            .Append("           ForeignCellPhone, ")
            .Append("           ModUser, ")
            .Append("           ModDate ")
            .Append("FROM       hrEmpContactInfo ")
            .Append("WHERE      empId = ? ")
        End With

        ' Add the empId to the parameter list
        db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim empInfoData As New EmployeeContactInfo(empId)

        While dr.Read()

            '   populate fields with data from DataReader
            With empInfoData
                .IsInDB = True
                .EmpContactInfoId = CType(dr("EmpContactInfoId"), Guid).ToString
                '.EmpId = empId
                If Not (dr("WorkPhone") Is System.DBNull.Value) Then .WorkPhone = dr("WorkPhone") Else .WorkPhone = ""
                If Not (dr("HomePhone") Is System.DBNull.Value) Then .HomePhone = dr("HomePhone") Else .HomePhone = ""
                If Not (dr("CellPhone") Is System.DBNull.Value) Then .CellPhone = dr("CellPhone") Else .CellPhone = ""
                If Not (dr("Beeper") Is System.DBNull.Value) Then .Beeper = dr("Beeper") Else .Beeper = ""
                If Not (dr("WorkEmail") Is System.DBNull.Value) Then .WorkEmail = dr("WorkEmail")
                If Not (dr("HomeEmail") Is System.DBNull.Value) Then .HomeEmail = dr("HomeEmail")
                .ForeignHomePhone = dr("ForeignHomePhone")
                .ForeignWorkPhone = dr("ForeignWorkPhone")
                .ForeignCellPhone = dr("ForeignCellPhone")
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return ContactInfo
        Return empInfoData

    End Function
    Public Function AddContactInfo(ByVal empContactInfo As EmployeeContactInfo, ByVal user As String) As String
        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT hrEmpContactInfo (EmpContactInfoId, EmpId, WorkPhone, HomePhone, ")
                .Append("       CellPhone, Beeper, WorkEmail, HomeEmail, ForeignWorkPhone, ForeignHomePhone, ForeignCellPhone, ")
                .Append("       ModUser, ModDate) ")
                .Append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
            End With

            '   add parameters values to the query
            '   empHRInfoId
            db.AddParameter("@EmpContactInfoId", empContactInfo.EmpContactInfoId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   empId
            db.AddParameter("@EmpId", empContactInfo.EmpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   add parameters values to the query
            '   WorkPhone
            db.AddParameter("@WorkPhone", empContactInfo.WorkPhone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   HomePhone
            db.AddParameter("@HomePhone", empContactInfo.HomePhone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CellPhone
            db.AddParameter("@CellPhone", empContactInfo.CellPhone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Beeper
            db.AddParameter("@Beeper", empContactInfo.Beeper, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   WorkEmail
            db.AddParameter("@WorkEmail", empContactInfo.WorkEmail, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   HomeEmail
            db.AddParameter("@HomeEmail", empContactInfo.HomeEmail, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '  Work Phone
            db.AddParameter("@ForeignWorkPhone", empContactInfo.ForeignWorkPhone, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            db.AddParameter("@ForeignHomePhone", empContactInfo.ForeignHomePhone, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            db.AddParameter("@ForeignCellPhone", empContactInfo.ForeignCellPhone, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return ex.Message
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function UpdateContactInfo(ByVal empContactInfoData As EmployeeContactInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE hrEmpContactInfo Set WorkPhone = ?, HomePhone = ?, ")
                .Append(" CellPhone = ?, Beeper = ?, WorkEmail = ?, HomeEmail = ?, FOreignWorkPhone=?, ForeignHomePhone=?, FOreignCellPhone=?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE empId = ?")
            End With

            '   add parameters values to the query
            db.AddParameter("@WorkPhone", empContactInfoData.WorkPhone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@HomePhone", empContactInfoData.HomePhone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@CellPhone", empContactInfoData.CellPhone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@Beeper", empContactInfoData.Beeper, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@WorkEmail", empContactInfoData.WorkEmail, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@HomeEmail", empContactInfoData.HomeEmail, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '  Work Phone
            db.AddParameter("@ForeignWorkPhone", empContactInfoData.ForeignWorkPhone, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            db.AddParameter("@ForeignHomePhone", empContactInfoData.ForeignHomePhone, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            db.AddParameter("@ForeignCellPhone", empContactInfoData.ForeignCellPhone, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.AddParameter("@EmpId", empContactInfoData.EmpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return ex.Message
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetHRInfo(ByVal empId As String) As EmployeeHRInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT EM.EmpHRInfoId, ")
            .Append("       EM.EmpId, ")
            .Append("       EM.HireDate, ")
            .Append("       EM.PositionId, ")
            .Append("       (Select PositionDescrip from syPositions where PositionId=EM.PositionId) As PositionDescrip, ")
            .Append("       EM.DepartmentId, ")
            .Append("       (Select HRDepartmentDescrip from syHRDepartments where HRDepartmentId=EM.DepartmentId) As DepartmentDescrip, ")
            .Append("       ModUser, ")
            .Append("       ModDate ")
            .Append("FROM   hrEmpHRInfo EM ")
            .Append("WHERE  EM.empId = ? ")
        End With

        ' Add the empId to the parameter list
        db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim empHRInfoData As New EmployeeHRInfo(empId)

        While dr.Read()

            '   populate fields with data from DataReader
            With empHRInfoData
                .IsInDB = True
                .EmpHRInfoId = CType(dr("EmpHRInfoId"), Guid).ToString
                '.EmpId = CType(dr("EmpId"), Guid).ToString
                If Not (dr("HireDate") Is System.DBNull.Value) Then .HireDate = dr("HireDate") Else .HireDate = Date.MaxValue
                If Not (dr("PositionId") Is System.DBNull.Value) Then .PositionId = CType(dr("PositionId"), Guid).ToString Else .PositionId = Guid.Empty.ToString
                If Not (dr("PositionDescrip") Is System.DBNull.Value) Then .Position = dr("PositionDescrip")
                If Not (dr("DepartmentId") Is System.DBNull.Value) Then .DepartmentId = CType(dr("DepartmentId"), Guid).ToString Else .DepartmentId = Guid.Empty.ToString
                If Not (dr("DepartmentDescrip") Is System.DBNull.Value) Then .Department = dr("DepartmentDescrip")
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return empHRInfo
        Return empHRInfoData

    End Function
    Public Function AddHRInfo(ByVal empHRInfo As EmployeeHRInfo, ByVal user As String) As String
        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT hrEmpHRInfo (EmpHRInfoId, EmpId, HireDate, PositionId, ")
                .Append("       DepartmentId, ")
                .Append("       ModUser, ModDate) ")
                .Append("VALUES (?, ?, ?, ?, ?, ?, ?)")
            End With

            '   add parameters values to the query
            '   empHRInfoId
            db.AddParameter("@EmpHRInfoId", empHRInfo.EmpHRInfoId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   empId
            db.AddParameter("@EmpId", empHRInfo.EmpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   HireDate
            If empHRInfo.HireDate = Date.MaxValue Then
                db.AddParameter("@HireDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@HireDate", empHRInfo.HireDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            '   PositionId
            If empHRInfo.PositionId = Guid.Empty.ToString Then
                db.AddParameter("@PositionId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PositionId", empHRInfo.PositionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   DepartmentId
            If empHRInfo.DepartmentId = Guid.Empty.ToString Then
                db.AddParameter("@DepartmentId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@DepartmentId", empHRInfo.DepartmentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return ex.Message
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function UpdateHRInfo(ByVal empHRInfo As EmployeeHRInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE hrEmpHRInfo Set HireDate = ?, ")
                .Append(" PositionId = ?, DepartmentId = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE EmpId = ? ")
            End With

            '   add parameters values to the query

            '   HireDate
            If empHRInfo.HireDate = Date.MaxValue Then
                db.AddParameter("@HireDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@HireDate", empHRInfo.HireDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            '   PositionId
            If empHRInfo.PositionId = Guid.Empty.ToString Then
                db.AddParameter("@PositionId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PositionId", empHRInfo.PositionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   DepartmentId
            If empHRInfo.DepartmentId = Guid.Empty.ToString Then
                db.AddParameter("@DepartmentId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@DepartmentId", empHRInfo.DepartmentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   empId
            db.AddParameter("@EmpId", empHRInfo.EmpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return ex.Message
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetDegreesPerEmployee(ByVal empId As String) As DataSet

        '   get the connection to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT EmpId, DegreeId ")
            .Append("FROM   hrEmpDegrees ")
            .Append("WHERE  EmpId = ?")
        End With

        ' Add the parameter
        db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return the dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetCertificationsPerEmployee(ByVal empId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT EmpId, CertId ")
            .Append("FROM   hrEmpCerts ")
            .Append("WHERE  EmpId = ?")
        End With

        ' Add the parameter
        db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function UpdateEmployeeDegrees(ByVal empId As String, ByVal user As String, ByVal selectedDegrees() As String)

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   First we have to delete all existing selections
        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("DELETE FROM hrEmpDegrees WHERE EmpId = ?")
        End With

        '   delete all selected items
        db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        '   Insert one record per each Item in the Selected Group
        If Not selectedDegrees Is Nothing Then
            Dim i As Integer
            For i = 0 To selectedDegrees.Length - 1

                '   build query
                With sb
                    .Append("INSERT INTO hrEmpDegrees (EmpId, DegreeId, ModDate, ModUser) ")
                    .Append("VALUES(?,?,?,?)")
                End With

                '   add parameters
                db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@DegreeId", DirectCast(selectedDegrees.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   execute query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Next
        End If

        'Close Connection
        db.CloseConnection()

    End Function
    Public Function UpdateEmployeeCertifications(ByVal empId As String, ByVal user As String, ByVal selectedCertifications() As String)

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   First we have to delete all existing selections
        '   build the query
        Dim sb As New System.Text.StringBuilder
        With sb
            .Append("DELETE FROM hrEmpCerts WHERE EmpId = ?")
        End With

        '   delete all selected items
        db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        '   Insert one record per each selected Certification
        If Not selectedCertifications Is Nothing Then
            Dim i As Integer
            For i = 0 To selectedCertifications.Length - 1

                '   build query
                With sb
                    .Append("INSERT INTO hrEmpCerts (EmpId, CertId, ModDate, ModUser) ")
                    .Append("VALUES(?,?,?,?)")
                End With

                '   add parameters
                db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@CertId", DirectCast(selectedCertifications.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Next
        End If
        'Close Connection
        db.CloseConnection()

    End Function
    Public Function GetUserInfo(ByVal empId As String) As EmployeeUserInfo

        '   connect to the database
        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT     EU.EmpUserInfoId, EU.EmpId, EU.UserName, ")
            .Append(" EU.Password, ")
            .Append(" EU.CampGrpId, ")
            .Append(" CG.CampGrpDescrip, ")
            .Append(" EU.ModUser, ")
            .Append(" EU.ModDate ")
            .Append("FROM       hrEmpUserInfo EU, syCampGrps CG ")
            .Append("WHERE EU.CampGrpId = CG.CampGrpId ")
            .Append(" AND      EU.empId = ? ")
        End With

        ' Add the empId to the parameter list
        db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim empInfoData As New EmployeeUserInfo(empId)

        While dr.Read()

            '   populate fields with data from DataReader
            With empInfoData
                .IsInDB = True
                .EmpUserInfoId = CType(dr("EmpUserInfoId"), Guid).ToString
                .EmpId = CType(dr("EmpId"), Guid).ToString
                If Not (dr("UserName") Is System.DBNull.Value) Then .UserName = dr("UserName") Else .UserName = ""
                If Not (dr("Password") Is System.DBNull.Value) Then .Password = dr("Password") Else .Password = ""
                .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                .CampGrpDescrip = dr("CampGrpDescrip")
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return UserInfo
        Return empInfoData

    End Function
    Public Function AddUserInfo(ByVal empUserInfo As EmployeeUserInfo, ByVal user As String) As String
        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT hrEmpUserInfo (EmpUserInfoId, EmpId, UserName, Password, ")
                .Append("       CampGrpId, ModUser, ModDate) ")
                .Append("VALUES (?, ?, ?, ?, ?, ?, ?)")
            End With

            '   add parameters values to the query
            '   empUserInfoId
            db.AddParameter("@EmpUserInfoId", empUserInfo.EmpUserInfoId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   empId
            db.AddParameter("@EmpId", empUserInfo.EmpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   UserName
            If empUserInfo.UserName = "" Then
                db.AddParameter("@UserName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@UserName", empUserInfo.UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Password
            If empUserInfo.Password = "" Then
                db.AddParameter("@Password", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Password", empUserInfo.Password, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   CampGrpId
            db.AddParameter("@CampGrpId", empUserInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return ex.Message
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function UpdateUserInfo(ByVal empUserInfo As EmployeeUserInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE hrEmpUserInfo Set EmpId = ?, UserName = ?, ")
                .Append(" Password = ?, CampGrpId = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE EmpUserInfoId = ?")
            End With

            '   add parameters values to the query
            '   empId
            db.AddParameter("@EmpId", empUserInfo.EmpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   UserName
            If empUserInfo.UserName = "" Then
                db.AddParameter("@UserName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@UserName", empUserInfo.UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Password
            If empUserInfo.Password = "" Then
                db.AddParameter("@Password", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Password", empUserInfo.Password, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   CampGrpId
            db.AddParameter("@CampGrpId", empUserInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   EmpUserInfoId
            db.AddParameter("@EmpUserInfoId", empUserInfo.EmpUserInfoId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return ex.Message
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetAllEmployeesForEmail(ByVal statusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("Select DISTINCT ")
            .Append("       E.EmpId, ")
            .Append("       (E.LastName + ' ' + E.FirstName) + Case Len(Coalesce((Select Coalesce(WorkEmail,HomeEmail) from hrEmpContactInfo where EmpId=E.EmpId), ' ')) when 0 then '(*)' else ' ' end As Name, ")
            .Append("       Case Len(Coalesce((Select Coalesce(WorkEmail,HomeEmail) from hrEmpContactInfo where EmpId=E.EmpId), ' ')) when 0 then 0 else 1 end As HasEmail,  ")
            .Append("       (Select Coalesce(WorkEmail,HomeEmail, '') from hrEmpContactInfo where EmpId=E.EmpId) as Email ")
            .Append("FROM   hrEmployees E ")
            .Append("WHERE  1=1 ")

            '   check if the selection is "All Statuses"
            If Not statusId = Guid.Empty.ToString Then
                .Append("AND    E.StatusId = ? ")
                db.AddParameter("@StatusId", statusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   order by employee name
            .Append("ORDER BY Name ")

        End With

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    Public Function GetAllEmployees(ByVal statusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       E.EmpId, ")
            .Append("       (E.LastName + ', ' + E.FirstName) As EmployeeName ")
            .Append("FROM   hrEmployees E, hrEmpContactInfo EC ")
            .Append("WHERE  E.EmpId=EC.EmpId ")

            '   check if the selection is "All Statuses"
            If Not statusId = Guid.Empty.ToString Then
                .Append("AND    E.StatusId = ? ")
                db.AddParameter("@StatusId", statusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   order by employee name
            .Append("ORDER BY EmployeeName ")

        End With

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    Public Function DeleteEmployee(ByVal empId As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM hrEmployeeEmergencyContacts ")
                .Append("WHERE empId = ? ;")
                .Append("DELETE FROM hrEmpHRInfo ")
                .Append("WHERE empId = ? ;")
                .Append("DELETE FROM hrEmpContactInfo ")
                .Append("WHERE empId = ? ;")
                .Append("DELETE FROM hrEmpDegrees ")
                .Append("WHERE empId = ? ;")
                .Append("DELETE FROM hrEmpCerts ")
                .Append("WHERE empId = ? ;")
                .Append("DELETE FROM hrEmployees ")
                .Append("WHERE empId = ? ;")
            End With

            '   add parameters values to the query

            '   EmpId
            db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   EmpId
            db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   EmpId
            db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   EmpId
            db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   EmpId
            db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   EmpId
            db.AddParameter("@EmpId", empId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
End Class




