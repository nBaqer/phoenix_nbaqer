Imports FAME.Advantage.Common

Public Class SAPD1
    Public Function GetControlCountByType(ByVal ModuleId As String, ByVal ResourceId As Integer) As String
        '**************************************************************************************************
        'Purpose:       This Procedure Determines The Total Count OF Controls Based on The Controls Type
        '               The Controls belong to following three types - Range,List,None
        'Parameters:
        'Returns:       String
        'Notes:         This sub relies on the ResourceId and ModuleCode when
        '               when a request for the page is made.
        '               intRangeCount --- Holds the Total Count for Range Controls
        '               intListCount  --- Holds the Total Count for List Controls
        '               intNoneCount  --- Holds the Total Count for Controls with no validation
        '**************************************************************************************************
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim intRangeCount As Integer
        Dim intListCount As Integer
        Dim intNoneCount As Integer
        Dim EndDate As String = "5/2/2005"
        Dim strTerms As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            With sb
                .Append("select Termid  from arTerm where EndDate <= ?")
            End With
            'Execute the query
            db.AddParameter("@EndDate", EndDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            While dr.Read()
                strTerms += dr("TermID") 'Get Count of Controls with List Controls
            End While
            dr.Close()
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            Return strTerms
        Catch ex As System.Exception
        Finally
            db.CloseConnection()
        End Try
    End Function
End Class
