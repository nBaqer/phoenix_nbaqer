﻿
Imports System.Data

Public Class ScheduledCreditsHoursDB
    Public Function GetEnrollmentList_sp(ByRef paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim strWhere As String
        Dim strTempWhere As String = ""
        Dim strOrder(2) As String
        Dim iRows As Integer
        Dim m_SchoolLogo As Byte()
        Dim rptDB As New ReportsDB
        Dim db As New SQLDataAccess
        'db.ConnectionString = SingletonAppSettings.AppSettings("ConnectionString")
        'Dim m_StudentIdentifier As String = SingletonAppSettings.AppSettings("StudentIdentifier")
      

        'Get ProgVerId and Where Clause from paramInfo.FilterList
        If paramInfo.FilterList <> "" Then
            strWhere &= " and " & paramInfo.FilterList
        End If


        '********************************************************************************'
        Dim campGrpId As String = String.Empty
        Dim termId As String = String.Empty
        Dim prgVerId As String = String.Empty
        Dim statusCodeId As String = String.Empty
        '********************************************************************************'





        strTempWhere = strWhere
        strTempWhere = strTempWhere.ToLower.Replace("and", ";")
        strWhere = ""
       
       
        '********************************************************************************'
        'get the values of the parameters
      
            campGrpId = strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in (")).ToLower.IndexOf(" and ")).Replace("t1.campgrpid in", "").Replace(")", "").Replace("(", "")

            'get the prgverid
            If strWhere.ToLower.Contains("arstuenrollments.prgverid in ") Then
                If strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid in ")).ToLower.IndexOf(" and ") >= 0 Then
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid in (")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.prgverid in", "").Replace(")", "").Replace("(", "")
                Else
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid in (")).Replace("arstuenrollments.prgverid in", "").Replace(")", "").Replace("(", "")
                End If
            Else
                If strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid = ")).ToLower.IndexOf(" and ") >= 0 Then
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid = "), strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid = ")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.prgverid =", "").Replace(")", "").Replace("(", "")
                Else
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid = ")).Replace("arstuenrollments.prgverid =", "").Replace(")", "").Replace("(", "")
                End If
            End If
            'end the prgverid

            'get the statuscode
            If strWhere.ToLower.Contains("arstuenrollments.statuscodeid") Then
                If strWhere.Contains("arstuenrollments.statuscodeid in ") Then
                    If strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid in ")).ToLower.IndexOf(" and ") >= 0 Then
                        statusCodeId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid in (")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.statuscodeid in", "").Replace(")", "").Replace("(", "")
                    Else
                        statusCodeId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid in (")).Replace("arstuenrollments.statuscodeid in", "").Replace(")", "").Replace("(", "")
                    End If
                Else
                    If strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).ToLower.IndexOf(" and ") >= 0 Then
                        statusCodeId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid = "), strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.statuscodeid =", "").Replace(")", "").Replace("(", "")
                    Else
                        statusCodeId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).Replace("arstuenrollments.statuscodeid =", "").Replace(")", "").Replace("(", "")
                    End If
                End If
            End If
            'end the statuscode

        '********************************************************************************'
        campGrpId = campGrpId.Replace(" ", "").Replace("'", "")
        db.AddParameter("@campGrpId", campGrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        db.AddParameter("@termId", termId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        If prgVerId = String.Empty Then
            db.AddParameter("@prgVerId", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        Else
            prgVerId = prgVerId.Replace(" ", "").Replace("'", "")
            'statusCodeId = "'" + statusCodeId + "'"
            db.AddParameter("@prgVerId", prgVerId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        End If
                    If statusCodeId = String.Empty Then
                        db.AddParameter("@statusCodeId", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    Else
                        statusCodeId = statusCodeId.Replace(" ", "").Replace("'", "")
                        'statusCodeId = "'" + statusCodeId + "'"
                        db.AddParameter("@statusCodeId", statusCodeId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    End If
                   


        ds = db.RunParamSQLDataSet_SP("dbo.usp_GetEnrollmentListWithNoLeadGrps_GroupByClass")






        If paramInfo.OrderBy <> "" Then
            strOrder = GetOrderArray(paramInfo.OrderBy.Replace("arStudent.", ""))
        Else
            strOrder(0) = "LastName"
            strOrder(1) = "LastName"
            strOrder(2) = "LastName"
            'strOrder = paramInfo.OrderBy.Replace("arStudent.", "")
        End If



        
      
            Dim SummaryInfo As DataTable = ds.Tables("SummaryInfo")
            ' SummaryInfo.DefaultView.Sort = strOrder
            ds.Tables.RemoveAt(0)

           
                Dim query = From order In SummaryInfo.AsEnumerable() _
                Select order Order By order.Field(Of String)(strOrder(0)), order.Field(Of String)(strOrder(1)), order.Field(Of String)(strOrder(2))

                If query.Count > 0 Then
                    SummaryInfo = query.CopyToDataTable()
                Else
                    SummaryInfo = New DataTable()
                End If

            ds.Tables.Add(SummaryInfo)
            ds.Tables(0).TableName = "SummaryInfo"

          

           
        Try
            Return ds
        Catch ex As Exception
        Finally
            db.CloseConnection()
        End Try



    End Function
    Private Function GetOrderArray(ByVal str As String) As String()
        Dim rtn(2) As String
        Dim result() As String = str.Split(",")
        If result.Length = 3 Then
            rtn(0) = result(0)
            rtn(1) = result(1)
            rtn(2) = result(2)

        ElseIf result.Length = 2 Then
            rtn(0) = result(0)
            rtn(1) = result(1)
            rtn(2) = result(1)
        Else
            rtn(0) = result(0)
            rtn(1) = result(0)
            rtn(2) = result(0)
        End If
        Return rtn
    End Function
End Class
