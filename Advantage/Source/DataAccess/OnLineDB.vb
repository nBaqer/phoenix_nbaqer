
' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' OnLineDB.vb
'
' OnLineDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2006 FAME Inc.
' All rights reserved.
' ===============================================================================
Imports System.Collections.Generic
Imports FAME.Advantage.Common

Public Class OnLineDB
    Public Function GetAllOnLineStudents() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select ")
            .Append("		CCS.StuEnrollId, ")
            .Append("		CCS.OnLineStudentId, ")
            .Append("		(select PrgVerCode from arPrgVersions where PrgVerId=(select PrgVerId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId)) as ProgramID, ")
            .Append("		(select LastName from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId)) As LastName, ")
            .Append(" 		(select FirstName from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId)) As FirstName, ")
            .Append("		(select Substring(MiddleName, 1,1) from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId)) As MiddleInitial, ")
            .Append("		(select top 1 Address1 from arStudAddresses where StudentId=(select top 1 StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId order by default1 desc)) as Address1, ")
            .Append("		(select top 1 Address2 from arStudAddresses where StudentId=(select top 1 StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId order by default1 desc)) as Address2, ")
            .Append("		(select top 1 City from arStudAddresses where StudentId=(select top 1 StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId order by default1 desc)) as City, ")
            .Append("		(select StateCode from syStates where StateId=(select top 1 StateId from arStudAddresses where StudentId=(select top 1 StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId order by default1 desc))) as State,  ")
            .Append("		(select top 1 Zip from arStudAddresses where StudentId=(select top 1 StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId order by default1 desc)) as ZipCode, ")
            .Append("		(select CountryDescrip from adCountries where CountryId=(select top 1 CountryId from arStudAddresses where StudentId=(select top 1 StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId order by default1 desc))) as Country,  ")
            .Append("		(select top 1 Phone from arStudentPhone where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId) order by default1 desc) as Phone1, ")
            .Append("		(select top 1 Phone from arStudentPhone where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId) order by default1 asc) as Phone2, ")
            .Append("		(select Coalesce(HomeEmail, WorkEmail) from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId)) As Email, ")
            .Append("       CCS.ModUser, ")
            .Append("       CCS.ModDate ")
            .Append("from	atOnLineStudents CCS ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function

    Public Function GetAllOnLineCoursesByStuEnrollId(ByVal stuEnrollId As String) As OnLineCourse()

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select ")
            '.Append("       RQ.ReqId, ")
            .Append("		CS.ClsSection as CourseShortName ")
            .Append("from	arResults RS, arClassSections CS, arReqs RQ  ")
            .Append("where  ")
            .Append("		RS.TestId=CS.ClsSectionId ")
            .Append("and		CS.ReqId=RQ.reqId ")
            .Append("and		RQ.IsOnLine=1 ")
            .Append("and        StuEnrollId = ? ")
        End With

        ' Add the StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim onLineCourses As New List(Of AdvantageV1.Common.OnLineCourse)()

        While dr.Read()
            Dim onLineCourse As New OnLineCourse()

            '   set properties with data from DataReader
            With onLineCourse
                .StuEnrollId = stuEnrollId
                '.ReqId = CType(dr("ReqId"), Guid).ToString()
                .CourseShortName = dr("CourseShortName")
                'If Not (dr("DateEntered") Is System.DBNull.Value) Then .DateEntered = dr("DateEntered") Else .DateEntered = String.Empty
                'If Not (dr("DateChanged") Is System.DBNull.Value) Then .DateChanged = dr("DateChanged") Else .DateChanged = String.Empty
                'If Not (dr("IsDrop") Is System.DBNull.Value) Then .IsDrop = dr("IsDrop") Else .IsDrop = String.Empty
                'If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser") Else .ModUser = String.Empty
                'If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate") Else .ModDate = Date.MinValue
            End With

            onLineCourses.Add(onLineCourse)

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return OnLineCourses array
        Return onLineCourses.ToArray()
    End Function
    Public Function IsOnLineClassSection(ByVal clsSectionId As String) As Boolean
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select  ")
            .Append("		count(*)  ")
            .Append("from arClassSections CS, arReqs RQ ")
            .Append("where ")
            .Append("		CS.ReqId=RQ.ReqId ")
            .Append("and		RQ.IsOnLine =1 ")
            .Append("and		CS.ClsSectionId =? ")
        End With

        ' Add the ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return scalar
        Dim obj As Object = db.RunParamSQLScalar(sb.ToString)
        If Not (obj Is System.DBNull.Value) Then
            Dim i As Integer = CType(obj, Integer)
            If i > 0 Then Return True
        End If
        Return False
    End Function
    Public Function IsEnrolledInAnyOnLineCourse(ByVal stuEnrollId As String) As Boolean
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select ")
            .Append("       count(*) ")
            .Append("from	arResults RS, arClassSections CS, arReqs RQ  ")
            .Append("where  ")
            .Append("		RS.TestId=CS.ClsSectionId ")
            .Append("and		CS.ReqId=RQ.reqId ")
            .Append("and		RQ.IsOnLine=1 ")
            .Append("and        StuEnrollId = ? ")
        End With

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   return scalar
        Dim obj As Object = db.RunParamSQLScalar(sb.ToString)
        If Not (obj Is System.DBNull.Value) Then
            Dim i As Integer = CType(obj, Integer)
            If i > 0 Then Return True
        End If
        Return False
    End Function
    Public Function IsOnLineStudent(ByVal studentId As String) As Boolean
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select ")
            .Append("       count(*) ")
            .Append("from	arResults RS, arClassSections CS, arReqs RQ  ")
            .Append("where  ")
            .Append("		    RS.TestId=CS.ClsSectionId ")
            .Append("and		CS.ReqId=RQ.reqId ")
            .Append("and		RQ.IsOnLine=1 ")
            .Append("and        StuEnrollId in (select StuEnrollId from arStuEnrollments where StudentId =?) ")
        End With

        '   StuEnrollId
        db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   return scalar
        Dim obj As Object = db.RunParamSQLScalar(sb.ToString)
        If Not (obj Is System.DBNull.Value) Then
            Dim i As Integer = CType(obj, Integer)
            If i > 0 Then Return True
        End If
        Return False
    End Function
    Public Function GetOnLineInfoByStuEnrollId(ByVal stuEnrollId As String) As OnLineInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("select ")
            '.Append("		CCS.StuEnrollId, ")
            .Append("		(select OnLineStudentId from atOnLineStudents where StuEnrollId =?) as OnLineStudentId, ")
            .Append("		(select Coalesce(HomeEmail, WorkEmail) from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId)) As UserName, ")
            .Append(" 		(select FirstName from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId)) As Password, ")
            .Append("		(select PrgVerCode from arPrgVersions where PrgVerId=(select PrgVerId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId)) as ProgramID, ")
            .Append("		(select LastName from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId)) As LastName, ")
            .Append(" 		(select FirstName from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId)) As FirstName, ")
            .Append("		(select Substring(MiddleName, 1,1) from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId)) As MiddleInitial, ")
            .Append("		(select top 1 Address1 from arStudAddresses where StudentId=(select top 1 StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId order by default1 desc)) as Address1, ")
            .Append("		(select top 1 Address2 from arStudAddresses where StudentId=(select top 1 StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId order by default1 desc)) as Address2, ")
            .Append("		(select top 1 City from arStudAddresses where StudentId=(select top 1 StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId order by default1 desc)) as City, ")
            .Append("		(select StateCode from syStates where StateId=(select top 1 StateId from arStudAddresses where StudentId=(select top 1 StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId order by default1 desc))) as State,  ")
            .Append("		(select top 1 Zip from arStudAddresses where StudentId=(select top 1 StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId order by default1 desc)) as ZipCode, ")
            .Append("		(select CountryDescrip from adCountries where CountryId=(select top 1 CountryId from arStudAddresses where StudentId=(select top 1 StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId order by default1 desc))) as Country,  ")
            .Append("		(select top 1 Phone from arStudentPhone where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId) order by default1 desc) as Phone1, ")
            .Append("		(select top 1 Phone from arStudentPhone where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId) order by default1 asc) as Phone2, ")
            .Append("		(select Coalesce(HomeEmail, WorkEmail) from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId)) As Email ")
            '.Append("       CCS.ModUser, ")
            '.Append("       CCS.ModDate ")
            .Append("from	arStuEnrollments CCS ")
            .Append("where	StuEnrollId = ? ")
        End With

        ' Add the StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add the StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim onLineInfo As New OnLineInfo(GetOnLineUrl())

        While dr.Read()

            '   set properties with data from DataReader
            With onLineInfo
                .StuEnrollId = stuEnrollId
                If Not (dr("OnLineStudentId") Is System.DBNull.Value) Then .OnLineStudentId = CType(dr("OnLineStudentId"), Integer) Else .OnLineStudentId = 0
                If Not (dr("UserName") Is System.DBNull.Value) Then .UserName = dr("UserName") Else .UserName = stuEnrollId.Substring(24) + "@OnLine.com"
                If Not (dr("Password") Is System.DBNull.Value) Then .Password = dr("Password") Else .Password = String.Empty
                If Not (dr("ProgramId") Is System.DBNull.Value) Then .ProgramId = dr("ProgramId") Else .ProgramId = String.Empty
                If Not (dr("FirstName") Is System.DBNull.Value) Then .FirstName = dr("FirstName") Else .FirstName = String.Empty
                If Not (dr("LastName") Is System.DBNull.Value) Then .LastName = dr("LastName") Else .LastName = String.Empty
                If Not (dr("MiddleInitial") Is System.DBNull.Value) Then .MiddleInitial = dr("MiddleInitial") Else .MiddleInitial = String.Empty
                If Not (dr("Address1") Is System.DBNull.Value) Then .Address1 = dr("Address1") Else .Address1 = String.Empty
                If Not (dr("Address2") Is System.DBNull.Value) Then .Address2 = dr("Address2") Else .Address2 = String.Empty
                If Not (dr("City") Is System.DBNull.Value) Then .City = dr("City") Else .City = String.Empty
                If Not (dr("State") Is System.DBNull.Value) Then .State = dr("State") Else .State = String.Empty
                If Not (dr("ZipCode") Is System.DBNull.Value) Then .ZipCode = dr("ZipCode") Else .ZipCode = String.Empty
                If Not (dr("Country") Is System.DBNull.Value) Then .Country = dr("Country") Else .Country = String.Empty
                If Not (dr("Phone1") Is System.DBNull.Value) Then .Phone1 = dr("Phone1") Else .Phone1 = String.Empty
                If Not (dr("Phone2") Is System.DBNull.Value) Then .Phone2 = dr("Phone2") Else .Phone2 = String.Empty
                If Not (dr("Email") Is System.DBNull.Value) Then .Email = dr("Email") Else .Email = stuEnrollId.Substring(24) + "@OnLine.com"
                'If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser") Else .ModUser = String.Empty
                'If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate") Else .ModDate = Date.MinValue
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return OnLineInfo
        Return onLineInfo

    End Function
    Public Function GetOnLineInfoByStudentId(ByVal studentId As String) As OnLineInfo
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("select ")
            .Append("		CCS.OnLineStudentId ")
            .Append("from	atOnLineStudents CCS ")
            .Append("where	StuEnrollId in (select StuEnrollId from arStuEnrollments where StudentId =?) ")
        End With

        ' Add the StudentId to the parameter list
        db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim obj As Object = db.RunParamSQLScalar(sb.ToString)
        If obj Is System.DBNull.Value Then
            Return Nothing
        Else
            Return GetOnLineInfoByOnLineStudentId(CType(obj, Integer))
        End If
    End Function

    Public Function GetOnLineInfoByOnLineStudentId(ByVal onLineStudentId As Integer) As OnLineInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("select ")
            .Append("		CCS.StuEnrollId, ")
            .Append("		CCS.OnLineStudentId, ")
            .Append("		(select Coalesce(HomeEmail, WorkEmail) from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId)) As UserName, ")
            .Append(" 		(select FirstName from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId)) As Password, ")
            .Append("		(select PrgVerCode from arPrgVersions where PrgVerId=(select PrgVerId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId)) as ProgramID, ")
            .Append("		(select LastName from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId)) As LastName, ")
            .Append(" 		(select FirstName from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId)) As FirstName, ")
            .Append("		(select Substring(MiddleName, 1,1) from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId)) As MiddleInitial, ")
            .Append("		(select top 1 Address1 from arStudAddresses where StudentId=(select top 1 StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId order by default1 desc)) as Address1, ")
            .Append("		(select top 1 Address2 from arStudAddresses where StudentId=(select top 1 StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId order by default1 desc)) as Address2, ")
            .Append("		(select top 1 City from arStudAddresses where StudentId=(select top 1 StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId order by default1 desc)) as City, ")
            .Append("		(select StateCode from syStates where StateId=(select top 1 StateId from arStudAddresses where StudentId=(select top 1 StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId order by default1 desc))) as State,  ")
            .Append("		(select top 1 Zip from arStudAddresses where StudentId=(select top 1 StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId order by default1 desc)) as ZipCode, ")
            .Append("		(select CountryDescrip from adCountries where CountryId=(select top 1 CountryId from arStudAddresses where StudentId=(select top 1 StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId order by default1 desc))) as Country,  ")
            .Append("		(select top 1 Phone from arStudentPhone where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId) order by default1 desc) as Phone1, ")
            .Append("		(select top 1 Phone from arStudentPhone where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId) order by default1 asc) as Phone2, ")
            .Append("		(select Coalesce(HomeEmail, WorkEmail) from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=CCS.StuEnrollId)) As Email, ")
            .Append("       CCS.ModUser, ")
            .Append("       CCS.ModDate ")
            .Append("from	atOnLineStudents CCS ")
            .Append("where	OnLineStudentId = ? ")
        End With

        ' Add the OnLineStudentId to the parameter list
        db.AddParameter("@OnLineStudentId", onLineStudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim OnLineInfo As New OnLineInfo(GetOnLineUrl())

        While dr.Read()

            '   set properties with data from DataReader
            With OnLineInfo
                .StuEnrollId = CType(dr("StuEnrollId"), Guid).ToString
                .OnLineStudentId = onLineStudentId
                If Not (dr("UserName") Is System.DBNull.Value) Then .UserName = dr("UserName") Else .UserName = .StuEnrollId.Substring(24) + "@OnLine.com"
                If Not (dr("Password") Is System.DBNull.Value) Then .Password = dr("Password") Else .Password = String.Empty
                If Not (dr("ProgramId") Is System.DBNull.Value) Then .ProgramId = dr("ProgramId") Else .ProgramId = String.Empty
                If Not (dr("FirstName") Is System.DBNull.Value) Then .FirstName = dr("FirstName") Else .FirstName = String.Empty
                If Not (dr("LastName") Is System.DBNull.Value) Then .LastName = dr("LastName") Else .LastName = String.Empty
                If Not (dr("MiddleInitial") Is System.DBNull.Value) Then .MiddleInitial = dr("MiddleInitial") Else .MiddleInitial = String.Empty
                If Not (dr("Address1") Is System.DBNull.Value) Then .Address1 = dr("Address1") Else .Address1 = String.Empty
                If Not (dr("Address2") Is System.DBNull.Value) Then .Address2 = dr("Address2") Else .Address2 = String.Empty
                If Not (dr("City") Is System.DBNull.Value) Then .City = dr("City") Else .City = String.Empty
                If Not (dr("State") Is System.DBNull.Value) Then .State = dr("State") Else .State = String.Empty
                If Not (dr("ZipCode") Is System.DBNull.Value) Then .ZipCode = dr("ZipCode") Else .ZipCode = String.Empty
                If Not (dr("Country") Is System.DBNull.Value) Then .Country = dr("Country") Else .Country = String.Empty
                If Not (dr("Phone1") Is System.DBNull.Value) Then .Phone1 = dr("Phone1") Else .Phone1 = String.Empty
                If Not (dr("Phone2") Is System.DBNull.Value) Then .Phone2 = dr("Phone2") Else .Phone2 = String.Empty
                If Not (dr("Email") Is System.DBNull.Value) Then .Email = dr("Email") Else .Email = .StuEnrollId.Substring(24) + "@OnLine.com"
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser") Else .ModUser = String.Empty
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate") Else .ModDate = Date.MinValue
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return OnLineInfo
        Return OnLineInfo

    End Function
    Public Function UpdateOnLineInfo(ByVal OnLineInfo As OnLineInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE atOnLineStudents ")
                .Append("Set    OnLineStudentId = ?, ")
                .Append("       ModUser = ?, ")
                .Append("       ModDate = ? ")
                .Append("WHERE  StuEnrollId = ? ")
            End With

            '   add parameters values to the query

            '   OnLineStudentId
            db.AddParameter("@OnLineStudentId", OnLineInfo.OnLineStudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   StuEnrollId
            db.AddParameter("@StuEnrollId", OnLineInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function UpdateOnLineStudentGradesWithOnLineData(ByVal results() As String, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            '   build the query
            Dim sb As New System.Text.StringBuilder
            With sb
                .Append("update arResults set GrdSysDetailId=( ")
                .Append("										select  ")
                .Append("												GrdSysDetailId ")
                .Append("										from	arGradeScaleDetails GSD ")
                .Append("										where	GSD.GrdScaleId=(select CS.GrdScaleId from arResults RS, arClassSections CS where RS.TestId=CS.ClsSectionId) ")
                .Append("										and		? >= GSD.MinVal ")
                .Append("										and		? <= GSD.MaxVal ")
                .Append("									  ) ")
                .Append("select ResultId  ")
                .Append("from arResults RS, arClassSections CS, arReqs RQ ")
                .Append("where ")
                .Append("		RS.TestId=CS.ClsSectionId ")
                .Append("and	CS.ReqId=RQ.ReqId ")
                .Append("and	CS.ClsSection =? ")
                .Append("and	RS.StuEnrollId=(Select StuEnrollId from atOnLineStudents where OnLineStudentId =?) ")
            End With

            For i As Integer = 1 To results.Length - 1
                db.ClearParameters()
                Dim params() As String = results(i).Split(";")

                '   Add parameter Grade
                Dim grade As Integer = OnLineInfo.ConvertOnLineGradeToInteger(params(2))
                db.AddParameter("@Grade", grade, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@Grade", grade, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                '   Add parameter Code
                db.AddParameter("@Code", params(0).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                '   Add parameter OnLineStudentId
                db.AddParameter("@OnLineStudentId", Integer.Parse(params(2)), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                '   Execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
            Next

            '   commit transaction 
            groupTrans.Commit()

            '   return without errors
            Return ""

        Catch ex As OleDbException
            groupTrans.Rollback()
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
                Return ex.Message

            End If
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function UpdateOnLineStudentGradesWithAdvantageData(ByVal results() As String, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            '   build the query
            Dim sb As New System.Text.StringBuilder
            With sb
                .Append("update arResults set GrdSysDetailId= ? ")
                .Append("where ")
                .Append("		ResultId= ? ")
            End With

            For i As Integer = 0 To results.Length - 1
                db.ClearParameters()
                Dim params() As String = results(i).Split(";")

                '   Add parameter GrdSysDetailId
                db.AddParameter("@GrdSysDetailId", params(1), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                '   Add parameter ResultId
                db.AddParameter("@ResultId", params(0), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                '   Execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
            Next

            '   commit transaction 
            groupTrans.Commit()

            '   return without errors
            Return ""

        Catch ex As OleDbException
            groupTrans.Rollback()
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
                Return ex.Message

            End If
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddOnLineInfo(ByVal OnLineInfo As OnLineInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT atOnLineStudents (StuEnrollId, OnLineStudentId,  ")
                .Append("   ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   StuEnrollId
            db.AddParameter("@StuEnrollId", OnLineInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   OnLineStudentId
            db.AddParameter("@OnLineStudentId", OnLineInfo.OnLineStudentId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteOnLineInfo(ByVal stuEnrollId As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM atOnLineStudents ")
                .Append("WHERE StuEnrollId = ? ")
            End With

            '   add parameters values to the query

            '   StuEnrollId
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            ' return without errors
            Return ""

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Private Shared Function GetOnLineUrl() As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        GetOnLineUrl = Replace(MyAdvAppSettings.AppSettings("OnLineUrl"), ";", "&")
    End Function
    Public Function GetAdvantageGrades(ByVal termGrades() As String, ByVal courseShortName As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select ")
            .Append("       RS.StuEnrollId, ")
            .Append("       CCS.OnLineStudentId, ")
            .Append("       RS.ResultId, ")
            .Append("       CS.ClsSection as CourseShortName, ")
            .Append("       S.LastName + ', ' + S.FirstName as StudentName, ")
            .Append("       Coalesce(Case Len(S.SSN) When 9 then '*******' + SUBSTRING(S.SSN,6,4) else ' ' end, ' ') as StudentId, ")
            .Append("       (select Grade from arGradeSystemDetails where GrdSysDetailId = RS.GrdSysDetailId) as Grade ")
            .Append("from arResults RS, arClassSections CS, arReqs RQ, atOnLineStudents CCS, arStuEnrollments SE, arStudent S ")
            .Append("where  ")
            .Append(" 		RS.TestId=CS.ClsSectionId  ")
            .Append("and	CS.ReqId=RQ.ReqId  ")
            .Append("and    CCS.StuEnrollId=SE.StuEnrollId ")
            .Append("and    SE.StudentId=S.StudentId ")
            .Append("and	CS.ClsSection =?  ")
            .Append("and    CCS.StuEnrollId=RS.StuEnrollId ")
            .Append("and	RS.StuEnrollId in (Select StuEnrollId from atOnLineStudents where OnLineStudentId in (" + BuildListOfStudents(termGrades, courseShortName) + ")) ")
        End With

        '   Code
        db.AddParameter("@Code", courseShortName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   get datatable
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'attach OnLine Fields to each advantage record
        Dim dt As DataTable = ds.Tables(0)
        dt.Columns.Add("OnLineFields", GetType(String))
        For i As Integer = 0 To dt.Rows.Count - 1
            UpdateOnLineFields(dt.Rows(i), termGrades)
        Next

        'insert one row for each item that exists in OnLine but not in Advantage
        InsertMissingAdvantageRows(dt, courseShortName, termGrades)

        'return ds
        Return ds
    End Function
    Public Function GetAdvantageCourseNameAndInstructor(ByVal courseShortName As String, ByVal termId As Integer) As String

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select  ")
            .Append("		(Select FullName from syUsers where UserId=CS.InstructorId) + ';' + RQ.Descrip ")
            .Append("from	arClassSections CS, arReqs RQ, arTerm T  ")
            .Append("where  ")
            .Append("		RQ.IsOnLine=1 ")
            .Append("and	CS.ReqId=RQ.ReqId ")
            .Append("and	CS.TermId=T.TermId ")
            .Append("and	CS.ClsSection = ? ")
            .Append("and    CS.InstructorId is not null ")
            '.Append("and	CS.TermId = (Select TermId from arTerm where TermCode = ? ) ")
        End With

        '   Code
        db.AddParameter("@Code", courseShortName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ''   TermId
        'db.AddParameter("@TermId", termId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'return ds
        Return db.RunParamSQLScalar(sb.ToString)

    End Function
    Public Function GetAdvantageGradeScaleForCourse(ByVal courseShortName As String, ByVal termId As Integer) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select ")
            .Append("		GrdSysDetailId, ")
            .Append("		(select Grade from arGradeSystemDetails where GrdSysDetailId=GSD.GrdSysDetailId) As Grade, ")
            .Append("		GSD.MinVal, ")
            .Append("		GSD.MaxVal  ")
            .Append("from	arGradeScaleDetails GSD  ")
            .Append("where	GSD.GrdScaleId =  ")
            .Append("							( ")
            .Append("								select  top 1  ")
            .Append("										CS.GrdScaleId ")
            .Append("								from	arClassSections CS, arReqs RQ, arTerm T   ")
            .Append("								where   ")
            .Append("									RQ.IsOnLine=1  ")
            .Append("								and	CS.ReqId=RQ.ReqId  ")
            .Append("								and	CS.TermId=T.TermId  ")
            .Append("								and	CS.ClsSection = ?  ")
            '.Append("								and	CS.TermId = (Select TermId from arTerm where TermCode = ? ) ")
            .Append("							)  ")
            .Append("order by GSD.MinVal ")
        End With

        '   Code
        db.AddParameter("@Code", courseShortName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ''   TermId
        'db.AddParameter("@TermId", termId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'return ds
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Private Sub UpdateOnLineFields(ByVal row As DataRow, ByVal termGrades() As String)
        Dim courseShortName As String = row("CourseShortName")
        Dim onLineStudentId As Integer = CType(row("OnLineStudentId"), Integer)
        For i As Integer = 0 To termGrades.Length - 1
            Dim field() As String = termGrades(i).Split(";")
            If field(0) = courseShortName And field(2) = onLineStudentId Then
                row("OnLineFields") = termGrades(i)
                Exit For
            End If
        Next
    End Sub
    Private Sub InsertMissingAdvantageRows(ByVal dt As DataTable, ByVal courseShortName As String, ByVal termGrades() As String)
        For i As Integer = 0 To termGrades.Length - 1
            Dim field() As String = termGrades(i).Split(";")
            If field(0) = courseShortName Then
                Dim rows() As DataRow = dt.Select("CourseShortName='" + courseShortName + "' and OnLineStudentId=" + field(2))
                If rows.Length = 0 Then
                    Dim newRow As DataRow = dt.NewRow()
                    newRow("OnLineFields") = termGrades(i)
                    dt.Rows.Add(newRow)
                End If
            End If
        Next
    End Sub
    Private Function BuildListOfStudents(ByVal termGrades() As String, ByVal courseShortName As String) As String
        Dim sb As New StringBuilder
        With sb
            For i As Integer = 0 To termGrades.Length - 1
                Dim field() As String = termGrades(i).Split(";")
                If field(0) = courseShortName Then
                    .Append(",")
                    .Append(field(2))
                End If
            Next
            'delete the first comma from the list
            If sb.Length > 1 Then
                Return sb.ToString.Substring(1)
            Else
                Return String.Empty
            End If
        End With
    End Function
    Public Function SyncOnLineAttendance(ByVal onLineTermAttendance As OnLineTermAttendance, ByVal userId As String) As String

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select  ")
            .Append("		OnLineStudentId, ")
            .Append("		CourseShortName, ")
            .Append("		AttendedDate, ")
            .Append("		Minutes, ")
            .Append("		OnLineTermId  ")
            .Append("from atOnLineAttendance ")
            .Append("where	 ")
            .Append("		OnLineTermId =? ")
            .Append("order by ")
            .Append("       CourseShortName, OnLineStudentId, AttendedDate ")
        End With

        '   OnLineTermId
        db.AddParameter("@OnLineTermId", onLineTermAttendance.OnLineTermId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        Dim dt As DataTable = db.RunParamSQLDataSet(sb.ToString).Tables(0)
        Dim rowsEnumerator As IEnumerator = dt.Rows.GetEnumerator()
        rowsEnumerator.MoveNext()
        Dim currentRow As DataRow = rowsEnumerator.Current
        Dim newRowsCollection As List(Of DataRow) = New List(Of DataRow)
        Dim cca As List(Of OnLineCourseAttendance) = onLineTermAttendance.OnLineCourseAttendanceCollection
        Dim ccaEnumerator As List(Of OnLineCourseAttendance).Enumerator = cca.GetEnumerator()
        While ccaEnumerator.MoveNext()
            Dim ccb As OnLineCourseAttendance = ccaEnumerator.Current
            Dim ccbEnumerator As List(Of OnLineStudentAttendance).Enumerator = ccb.OnLineStudentAttendanceCollection.GetEnumerator()
            While ccbEnumerator.MoveNext()
                Dim ccc As OnLineStudentAttendance = ccbEnumerator.Current
                Dim cccEnumerator As List(Of OnLineAttendedDate).Enumerator = ccc.AttendedDatesCollection.GetEnumerator()
                While cccEnumerator.MoveNext()
                    Dim ccd As OnLineAttendedDate = cccEnumerator.Current
                    Select Case CompareCurrentRowWithOnLineAttendedDate(currentRow, ccb.CourseShortName, ccc.StudentId, ccd)
                        Case 0
                            rowsEnumerator.MoveNext()
                            currentRow = rowsEnumerator.Current
                        Case -1
                            'Add row to table
                            Dim newRow As DataRow = dt.NewRow()
                            newRow("CourseShortName") = ccb.CourseShortName
                            newRow("OnLineStudentId") = ccc.StudentId
                            newRow("AttendedDate") = ccd.ActualDate
                            newRow("Minutes") = ccd.Minutes
                            newRow("OnLineTermId") = onLineTermAttendance.OnLineTermId
                            newRowsCollection.Add(newRow)
                    End Select
                End While
            End While
        End While
        For Each newRow As DataRow In newRowsCollection
            dt.Rows.Add(newRow)
        Next
        Dim selectCommand As OleDbCommand = BuildSelectCommand()
        selectCommand.Connection = New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
        Dim da As New OleDbDataAdapter(selectCommand)
        '   build insert, update and delete commands for atOnLineAttendance table
        Dim cb1 As New OleDbCommandBuilder(da)
        da.Update(dt)
        Return ""
    End Function
    Private Function CompareCurrentRowWithOnLineAttendedDate(ByVal currentRow As DataRow, ByVal courseShortName As String, ByVal onLineStudentId As Integer, ByVal ccd As OnLineAttendedDate) As Integer
        If currentRow Is Nothing Then Return -1
        If currentRow("CourseShortName") <> courseShortName Then Return -1
        If currentRow("OnLineStudentId") <> onLineStudentId Then Return -1
        If currentRow("AttendedDate") <> ccd.ActualDate Then Return -1
        Return 0
    End Function
    Private Function BuildSelectCommand() As OleDbCommand
        Dim sb As New StringBuilder()

        With sb
            .Append("select  ")
            .Append("		OnLineStudentId, ")
            .Append("		CourseShortName, ")
            .Append("		AttendedDate, ")
            .Append("		Minutes, ")
            .Append("		OnLineTermId  ")
            .Append("from atOnLineAttendance ")
        End With
        Return New OleDbCommand(sb.ToString)
    End Function
    Public Function SynchAdvantageWithComCourse(ByVal termGrades() As String, ByVal termId As String) As String
        For i As Integer = 0 To termGrades.Length - 1
            Dim field() As String = termGrades(i).Split(";")
            If Not IsStudentInAdvantageDB(CType(field(2), Integer)) Then
                Dim result As String = SynchStudent(field(2), field(4), field(3))
            End If
        Next
    End Function
    Private Function SynchStudent(ByVal onLineStudentId As Integer, ByVal lastName As String, ByVal firstName As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("if exists (select  ")
            .Append("					* ")
            .Append("			from	arStuEnrollments SE, arStudent S ")
            .Append("			where ")
            .Append("					SE.StudentId=S.StudentId ")
            .Append("			and		S.LastName like ? + '%' ")
            .Append("			and		S.FirstName like ? + '%') ")
            .Append("insert into atOnlineStudents (StuEnrollId, OnlineStudentId, ModUser, ModDate)  ")
            .Append("select  ")
            .Append("		(select top 1 StuEnrollId from arStuEnrollments SE, arStudent S ")
            .Append("		where	SE.StudentId=S.StudentId ")
            .Append("		and		S.LastName like ? + '%' ")
            .Append("		and		S.FirstName like ? + '%' ) As StuEnrollId, ")
            .Append("		? as OnlineStudentId, ")
            .Append("		'sa' as ModUser, ")
            .Append("		? as ModDate ")
        End With

        '   add parameters values to the query
        '   LastName
        db.AddParameter("@LastName", lastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   FirstName
        db.AddParameter("@FirstName", firstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   LastName
        db.AddParameter("@LastName", lastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   FirstName
        db.AddParameter("@FirstName", firstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   OnLineStudentId
        db.AddParameter("@OnLineStudentId", onLineStudentId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   ModDate
        Dim now As Date = Date.Now
        db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        '   execute the query
        db.RunParamSQLExecuteNoneQuery(sb.ToString)

        '   return without errors
        Return ""

        'Close Connection
        db.CloseConnection()

        '   return without errors
        Return ""

    End Function
    Private Function IsStudentInAdvantageDB(ByVal onLineStudentId As Integer) As Boolean
        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("select count(*) ")
            .Append("from atOnLineStudents ")
            .Append("where OnLineStudentId = ? ")
        End With

        '   add parameters values to the query
        '   OnLineStudentId
        db.AddParameter("@OnLineStudentId", onLineStudentId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   execute the query
        Dim obj As Object = db.RunParamSQLScalar(sb.ToString)

        If obj Is System.DBNull.Value Then
            Return False
        Else
            Return CType(CType(obj, Integer), Boolean)
        End If

    End Function

End Class

