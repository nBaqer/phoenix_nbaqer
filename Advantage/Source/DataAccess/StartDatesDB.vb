Imports FAME.Advantage.Common

Public Class StartDatesDB
    Public Function GetAllStartDates() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("	      (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("	      CCT.TermId, ")
            .Append("	      CCT.StatusId, ")
            .Append("	      CCT.TermCode, ")
            .Append("	      CCT.TermDescrip, ")
            .Append("         CCT.ProgId ")
            .Append("FROM     arTerm CCT, syStatuses ST ")
            .Append("WHERE    CCT.StatusId = ST.StatusId and CCT.TermTypeId = 4 ")
            '.Append("ORDER BY ST.Status,CCT.TermDescrip asc")
            .Append("ORDER BY CCT.StartDate asc")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function

    Public Function GetStartDatesShift(ByVal progId As String) As String
        Dim db As New DataAccess
        Dim shiftCount As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select count(shiftid) from ( ")
            .Append(" SELECT distinct cast(shiftid as char(50)) as ShiftID  ")
            .Append(" FROM     arTerm CCT, syStatuses ST  ")
            .Append(" WHERE    CCT.StatusId = ST.StatusId and CCT.TermTypeId = 4 and CCT.ProgId=  ")
            .Append("(select ProgId from  arPrgVersions where PrgVerId= ? ) ")
            .Append(" group by shiftid) T ")
        End With
        db.AddParameter("@progId", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        shiftCount = db.RunParamSQLScalar(sb.ToString)

        If shiftCount = 0 Then
            Return "00000000-0000-0000-0000-000000000000"

        ElseIf (shiftCount = 1) Then
            sb = New StringBuilder()
            db.ClearParameters()
            With sb
                .Append(" SELECT distinct shiftid ")
                .Append(" FROM     arTerm CCT, syStatuses ST ")
                .Append(" WHERE    CCT.StatusId = ST.StatusId and CCT.TermTypeId = 4 and CCT.ProgId=  ")
                .Append("(select ProgId from  arPrgVersions where PrgVerId= ? ) ")
                .Append(" group by shiftid ")
            End With
            db.AddParameter("@progId", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Return db.RunParamSQLScalar(sb.ToString).ToString
        Else
            Return ""
        End If

        '   return dataset

    End Function

    Public Function GetStartDatesShiftForProgram(ByVal progId As String) As String
        Dim db As New DataAccess
        Dim shiftCount As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select count(shiftid) from ( ")
            .Append(" SELECT distinct cast(shiftid as char(50)) as ShiftID  ")
            .Append(" FROM     arTerm CCT, syStatuses ST  ")
            .Append(" WHERE    CCT.StatusId = ST.StatusId and CCT.TermTypeId = 4 and CCT.ProgId=  ")
            .Append(" ? ")
            .Append(" group by shiftid) T ")
        End With
        db.AddParameter("@progId", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        shiftCount = db.RunParamSQLScalar(sb.ToString)

        If shiftCount = 0 Then
            Return "00000000-0000-0000-0000-000000000000"

        ElseIf (shiftCount = 1) Then
            sb = New StringBuilder()
            db.ClearParameters()
            With sb
                .Append(" SELECT distinct shiftid ")
                .Append(" FROM     arTerm CCT, syStatuses ST ")
                .Append(" WHERE    CCT.StatusId = ST.StatusId and CCT.TermTypeId = 4 and CCT.ProgId=  ")
                .Append(" ? ")
                .Append(" group by shiftid ")
            End With
            db.AddParameter("@progId", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Return db.RunParamSQLScalar(sb.ToString).ToString
        Else
            Return ""
        End If

        '   return dataset

    End Function
    Public Function GetTermStartDate(ByVal progid As String, ByVal expStartDate As String) As Boolean
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" SELECT CCT.startDate as startdate ")
            .Append(" FROM     arTerm CCT, syStatuses ST ")
            .Append(" WHERE    CCT.StatusId = ST.StatusId and CCT.TermTypeId = 4 and CCT.ProgId=  ")
            .Append("(select ProgId from  arPrgVersions where PrgVerId= ? ) ")

        End With

        ' Add the StartDateId to the parameter list
        db.AddParameter("@progid", progid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read()
            If (CDate(dr("startdate").ToString) = expStartDate) Then
                Return True
            End If

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return False
    End Function
    Public Function GetStartDateInfo(ByVal StartDateId As String) As StartDateInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT CCT.TermId, ")
            .Append("    CCT.TermCode, ")
            .Append("    CCT.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=CCT.StatusId) As Status, ")
            .Append("    CCT.TermDescrip, ")
            .Append("     CCT.ShiftId, ")
            .Append("     CCT.ProgId, ")
            .Append("     CCT.StartDate, ")
            .Append("     CCT.MidPtDate, ")
            .Append("     CCT.EndDate, ")
            .Append("     CCT.MaxGradDate, ")
            .Append("    CCT.ModUser, ")
            .Append("    CCT.ModDate, ")
            .Append("    CCT.CampGrpId ")
            .Append("FROM  arTerm CCT ")
            .Append("WHERE CCT.TermId= ? ")
        End With

        ' Add the StartDateId to the parameter list
        db.AddParameter("@StartDateId", StartDateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim StartDatesInfo As New StartDateInfo

        While dr.Read()

            '   set properties with data from DataReader
            With StartDatesInfo
                .StartDateId = StartDateId
                .IsInDB = True
                .Code = dr("TermCode")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Description = dr("TermDescrip")
                .ShiftId = dr("ShiftId").ToString
                .ProgId = dr("ProgId").ToString
                If Not (dr("EndDate") Is System.DBNull.Value) Then
                    .EndDate = dr("EndDate")
                Else
                    .EndDate = ""
                End If
                If Not (dr("MaxGradDate") Is System.DBNull.Value) Then
                    .AllowedMaxGradDate = dr("MaxGradDate")
                Else
                    .AllowedMaxGradDate = ""
                End If

                If Not (dr("MidPtDate") Is System.DBNull.Value) Then
                    .MidPtDate = dr("MidPtDate")
                Else
                    .MidPtDate = ""
                End If
                If Not (dr("StartDate") Is System.DBNull.Value) Then
                    .StartDate = dr("StartDate")
                Else
                    .StartDate = ""
                End If
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = dr("CampGrpId").ToString()
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return StartDateInfo
        Return StartDatesInfo

    End Function
    Public Function UpdateStartDateInfo(ByVal StartDatesInfo As StartDateInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE arTerm Set TermId = ?, TermCode = ?, ")
                .Append(" StatusId = ?, TermDescrip = ?,ShiftId = ?, ProgId = ?,  ")
                .Append(" StartDate = ?, MidPtDate = ?, EndDate = ?, MaxGradDate = ?,TermTypeId = 4, ")
                .Append(" ModUser = ?, ModDate = ? , CampGrpId = ?  ")
                .Append("WHERE TermId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from arTerm where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   StartDateId
            db.AddParameter("@StartDateId", StartDatesInfo.StartDateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   EnrollmentId
            db.AddParameter("@SDateCode", StartDatesInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", StartDatesInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   SDateDescrip
            db.AddParameter("@SDateDescrip", StartDatesInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            If StartDatesInfo.ShiftId = "" Then
                db.AddParameter("@shiftid", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@shiftid", StartDatesInfo.ShiftId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            db.AddParameter("@prgverid", StartDatesInfo.ProgId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@startDate", StartDatesInfo.StartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            If StartDatesInfo.MidPtDate = "" Then
                db.AddParameter("@midptdate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@midptdate", StartDatesInfo.MidPtDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            db.AddParameter("@enddate", StartDatesInfo.EndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@maxdate", StartDatesInfo.AllowedMaxGradDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            If StartDatesInfo.CampGrpId = "" Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", StartDatesInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   StartDateId
            db.AddParameter("@SDateSetupId", StartDatesInfo.StartDateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", StartDatesInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If


        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)


        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddStartDateInfo(ByVal StartDatesInfo As StartDateInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess
        Dim TermType As Integer = 4

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT arTerm (TermId, TermCode, StatusId, ")
                .Append("   TermDescrip, ShiftId, ProgId, StartDate, MidPtDate, EndDate, MaxGradDate, TermTypeId, ModUser, ModDate,CampGrpId) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   StartDateId
            db.AddParameter("@StartDateId", StartDatesInfo.StartDateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '  StartDateCode
            db.AddParameter("@SDateCode", StartDatesInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", StartDatesInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StartDateDescrip
            db.AddParameter("@SDateDescrip", StartDatesInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            If StartDatesInfo.ShiftId = "" Then
                db.AddParameter("@shiftid", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@shiftid", StartDatesInfo.ShiftId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            db.AddParameter("@prgverid", StartDatesInfo.ProgId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@startDate", StartDatesInfo.StartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            If StartDatesInfo.MidPtDate = "" Then
                db.AddParameter("@midptdate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@midptdate", StartDatesInfo.MidPtDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            db.AddParameter("@enddate", StartDatesInfo.EndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@maxdate", StartDatesInfo.AllowedMaxGradDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@TermTypeId", TermType, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)


            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            If StartDatesInfo.CampGrpId = "" Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", StartDatesInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteStartDateInfo(ByVal StartDateId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM arTerm ")
                .Append("WHERE TermId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM arTerm WHERE TermId = ? ")
            End With

            '   add parameters values to the query

            '   XXXXXXXXId
            db.AddParameter("@StartDateId", StartDateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   XXXXXXXXId
            db.AddParameter("@SDateId", StartDateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If


        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function GetStudentCount(ByVal termId As String) As Integer
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim rtn As Integer = 0

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append(" select count(*) from arResults a,arClassSections b, ")
                .Append(" arClassSectionTerms c,arTerm d where a.TestID=b.ClsSectionId ")
                .Append(" and b.ClsSectionId=c.ClsSectionId and c.TermId=d.TermId and ")
                .Append(" d.TermId = ? ")
            End With

            '   add parameters values to the query

            '   XXXXXXXXId
            db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            '   execute the query
            rtn = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
        Return rtn
    End Function
End Class
