﻿Imports FAME.Advantage.Common
Public Enum TaskStatus
    Pending = 0
    Cancelled = 1
    Completed = 2
    None = -1
End Enum
Public Class TaskManagerDB
#Region " Was used for testing purpose"
    Public Function GetAdmissionRepSchedule_SP() As DataTable
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Return db.RunParamSQLDataSet("dbo.usp_getSchedule", Nothing, "SP").Tables(0)
    End Function

    Public Function GetAdmissionRepUserDetails_SP(ByVal UserID As String) As DataTable

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@UserID", UserID, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        Return db.RunParamSQLDataSet_SP("dbo.USP_getAdmissionRepUserDetails").Tables(0)

    End Function

    Public Function GetTaskDetails_SP() As DataTable
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Return db.RunParamSQLDataSet("dbo.USP_getAllTasks", Nothing, "SP").Tables(0)
    End Function

#End Region


    Public Function GetUsernames_fortasks(ByVal OwnerId As String, ByVal CurrentUserID As String) As DataTable

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim OwnerWOCurrentUser As String

        If CurrentUserID <> OwnerId Then
            OwnerWOCurrentUser = OwnerId.Replace(CurrentUserID, "")
            OwnerWOCurrentUser = OwnerWOCurrentUser.Remove(OwnerWOCurrentUser.LastIndexOf(","))
        End If

        If CurrentUserID <> OwnerId Then
            db.AddParameter("@OwnerID", OwnerWOCurrentUser, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        Else

            db.AddParameter("@OwnerID", OwnerId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        End If

        If CurrentUserID <> OwnerId Then
            db.AddParameter("@CurrentUserID", CurrentUserID, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            Return db.RunParamSQLDataSet_SP("dbo.USP_GetUsernames_fortasks").Tables(0)
        Else
            Return db.RunParamSQLDataSet_SP("dbo.USP_GetUsernames_fortasks_forME").Tables(0)
        End If


    End Function

    Public Function GetUserIDFromUserName_SP(ByVal UserName As String) As DataTable

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@UserName", UserName, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        Return db.RunParamSQLDataSet_SP("dbo.USP_GetUserIdFromUserName").Tables(0)

    End Function

    Public Function GetAll(ByVal CampusId As String, _
                                ByVal AssignedById As String, _
                                ByVal OwnerId As String, _
                                ByVal ReId As String, _
                                ByVal Priority As String, _
                                ByVal Status As TaskStatus, _
                                ByVal DueDateMin As String, _
                                ByVal DueDateMax As String, _
                                Optional ByVal FilterOthers As String = "", _
                                Optional ByVal OwnerIdForAssignedToMe As String = "") As DataSet

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@CampusID", CampusId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        db.AddParameter("@AssignedByID", AssignedById, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        db.AddParameter("@OwnerID", OwnerId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        db.AddParameter("@REID", ReId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        db.AddParameter("@Priority", Priority, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        db.AddParameter("@Status", Status, SqlDbType.Int, , ParameterDirection.Input)
        db.AddParameter("@StartDate", DueDateMin, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@EndDate", DueDateMax, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@OwnerIDForAssignedtoMe", OwnerIdForAssignedToMe, SqlDbType.VarChar, 8000, ParameterDirection.Input)

        Return db.RunParamSQLDataSet_SP("dbo.USP_GetUserTasks")
    End Function


    Public Function GetUsersList_ForwhomtheUsercanAssignTasksTo(ByVal UserID As String, Optional ByVal RoleID As String = "", Optional ByVal CampusDesc As String = "", Optional ByVal Fullname As String = "") As DataTable

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@UserID", UserID, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        If RoleID <> "" Then
            db.AddParameter("@RoleID", RoleID, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        End If
        If CampusDesc <> "" Then
            db.AddParameter("@CampusDesc", CampusDesc, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        End If
        If Fullname <> "" Then
            db.AddParameter("@FullName", "%" + Fullname + "%", SqlDbType.VarChar, 100, ParameterDirection.Input)
        End If
        Return db.RunParamSQLDataSet_SP("dbo.USP_GetUserslist_ForwhomtheUserCanAssignTasksTo").Tables(0)
    End Function


    Public Function GetUsersCampusList_ForwhomtheUsercanAssignTasksTo(ByVal UserID As String, Optional ByVal RoleID As String = "") As DataTable

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@UserID", UserID, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        If RoleID <> "" Then
            db.AddParameter("@RoleID", RoleID, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        End If
        Return db.RunParamSQLDataSet_SP("dbo.USP_GetUsersCampuslist_ForwhomtheUserCanAssignTasksTo").Tables(0)
    End Function

    Public Function GetRoles_ForwhomtheUsercanAssignTasksTo(ByVal UserID As String) As DataTable

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@UserID", UserID, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        Return db.RunParamSQLDataSet_SP("dbo.USP_GetRoles_ForwhomtheUserCanAssignTasksTo").Tables(0)
    End Function

    Public Function InsertUsersPreferenceList(ByVal UserID As String, ByVal OtherUsersId As String, Optional ByVal ViewSettings As String = "") As String

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@UserID", UserID, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        db.AddParameter("@OtherUsersID", OtherUsersId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        If Not ViewSettings = "" Then
            db.AddParameter("@DefaultView", ViewSettings, SqlDbType.VarChar, 50, ParameterDirection.Input)
        End If
        Try
            db.RunParamSQLExecuteNoneQuery_SP("USP_TMInsertUsersPreferenceList ")
        Catch ex As Exception
            Return ex.Message
        End Try

        Return ""

    End Function

    Public Function GetUserTaskCalendarDefaultView_Users(ByVal UserID As String) As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@UserID", UserID, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        Return db.RunParamSQLDataSet_SP("USP_TMGetUserTasksCalendarDefaultView_Users ")
    End Function

    Public Function GetUserTaskCalendarDefaultView(ByVal UserID As String) As String
        Dim db As New SQLDataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@UserID", UserID, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet_SP("USP_TMGetUserTasksCalendarDefaultView ")
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)(0).ToString()
            Else
                Return ""
            End If
        Else
            Return ""
        End If
    End Function


    Public Function DeleteUsersPreferenceList(ByVal UserID As String) As String

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@UserID", UserID, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery_SP("USP_TMDeleteUsersPreferenceList ")
        Catch ex As Exception
            Return ex.Message
        End Try

        Return ""

    End Function
End Class

