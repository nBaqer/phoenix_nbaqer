Imports FAME.Advantage.Common

Public Class GrdBkWgtsDB
    Public Function GetAllStatuses() As DataSet
        Dim ds As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New System.Text.StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT t1.StatusId,Status ")
            .Append("FROM syStatuses t1 ")
        End With

        '   Execute the query
        ds = db.RunSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function GetGrdComponentTypes() As DataSet
        Dim ds As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New System.Text.StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT t1.GrdComponentTypeId,Descrip ")
            .Append("FROM arGrdComponentTypes t1 ")
        End With

        '   Execute the query
        ds = db.RunSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function GetInstrGrdBkWgts(ByVal Instructor As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT t1.InstrGrdBkWgtId,Descrip,StatusId,(select Distinct Status from syStatuses where StatusId=t1.StatusId) as Status ")
            .Append("FROM arGrdBkWeights t1 ")
            .Append("WHERE t1.InstructorId = ? ")
            .Append("ORDER BY t1.Descrip")

        End With
        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@InstructorId", Instructor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)


        'Close Connection
        db.CloseConnection()

        Return ds

    End Function

    Public Function GetGrdBkWgtsOnItmCmd(ByVal strGuid As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim da As OleDbDataAdapter
        Dim strSQL As String
        Dim ds As New DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'Build query to obtain Campuses already assigned to the selected campus group
        strSQL = "SELECT t1.InstrGrdBkWgtDetailId, t1.InstrGrdBkWgtId, t1.Code, t1.Descrip, t1.Seq, t1.Weight, t1.GrdComponentTypeId , t1.ModUser , t1.ModDate  " & _
                 "FROM arGrdBkWgtDetails t1 " & _
                 "WHERE t1.InstrGrdBkWgtId = '" & strGuid & "'" & _
                 "ORDER BY t1.Seq"

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@InstrGrdBkWgtid", strGuid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        da = db.RunParamSQLDataAdapter(strSQL)
        da.Fill(ds, "GrdBkWgtDetails")


        'Close Connection
        db.CloseConnection()
        Return ds


    End Function

    Public Function GetGrdBkWgtDescStatus(ByVal strGuid As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim da As OleDbDataAdapter
        Dim strSQL As String
        Dim ds As New DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'Build query to obtain Campuses already assigned to the selected campus group
        strSQL = "SELECT t1.StatusId, t1.Descrip,t1.ModUser,t1.ModDate " & _
                 "FROM arGrdBkWeights t1 " & _
                 "WHERE t1.InstrGrdBkWgtId = '" & strGuid & "'" & _
                 "ORDER BY t1.Descrip"

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@InstrGrdBkWgtid", strGuid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        da = db.RunParamSQLDataAdapter(strSQL)
        da.Fill(ds, "GrdBkWgtInfo")

        'Close Connection
        db.CloseConnection()

        Return ds

    End Function

    Public Function InsertGrdBkWgtToDB(ByVal GrdBkWgtObj As GrdBkWgtsInfo, ByVal user As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Try
            With sb
                .Append("INSERT INTO arGrdBkWeights(InstrGrdBkWgtId,InstructorId,StatusId,Descrip,ModUser,ModDate) ")
                .Append("VALUES(?,?,?,?,?,?)")
            End With

            db.AddParameter("@campgrpid", GrdBkWgtObj.GrdBkWgtId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@campgrpcode", GrdBkWgtObj.InstructorId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@statusid", GrdBkWgtObj.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@campgrpdescrip", GrdBkWgtObj.Descrip, DataAccess.OleDbDataType.OleDbString, 80, ParameterDirection.Input)
            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", GrdBkWgtObj.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            'Close Connection
            db.CloseConnection()
        End Try


    End Function

    Public Function UpdateGrdBkWgtToDB(ByVal GrdBkWgtObj As GrdBkWgtsInfo, ByVal user As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

     

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("UPDATE arGrdBkWeights ")
            .Append("SET Descrip=?,")
            .Append("StatusId=?,")
            .Append("InstructorId=?, ")
            .Append("ModUser=?, ")
            .Append("ModDate=? ")
            .Append("WHERE InstrGrdBkWgtId =? ")
        End With
        db.AddParameter("@descrip", GrdBkWgtObj.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@statusid", GrdBkWgtObj.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@instructor", GrdBkWgtObj.InstructorId, DataAccess.OleDbDataType.OleDbString, 135, ParameterDirection.Input)
        ''ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ''ModDate
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@grdbkwgtid", GrdBkWgtObj.GrdBkWgtId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)
        Return ""
    End Function

    Public Function InsertGrdBkWgtDet(ByVal objGrdBkWgtDet As GrdBkWgtDetailsInfo, ByVal user As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        'Set the connection string
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("INSERT INTO arGrdBkWgtDetails(InstrGrdBkWgtDetailId,InstrGrdBkWgtId,Code,Descrip,Weight,Seq,GrdComponentTypeId,ModUser,ModDate) ")
            .Append("VALUES(?,?,?,?,?,?,?,?,?)")

        End With

        db.AddParameter("@grdbkwgtdetid", objGrdBkWgtDet.GrdBkWgtDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@GrdBkWgtId", objGrdBkWgtDet.GrdBkWgtId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@Code", objGrdBkWgtDet.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@Descrip", objGrdBkWgtDet.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@weight", objGrdBkWgtDet.weight, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@seq", objGrdBkWgtDet.Seq, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@GrdCompTypId", objGrdBkWgtDet.GrdCompTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ''ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ''ModDate
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        Return ""

    End Function

    Public Function UpdateGrdBkWgtDet(ByVal objGrdBkWgtDet As GrdBkWgtDetailsInfo, ByVal user As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        'Set the connection string
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")


        With sb
            .Append("UPDATE arGrdBkWgtDetails Set Code = ?, Weight = ?, Seq = ?, Descrip = ?, GrdComponentTypeId = ?, ModUser = ?, ModDate = ? ")
            .Append("WHERE  InstrGrdBkWgtDetailId = ?")

        End With

        db.AddParameter("@Code", objGrdBkWgtDet.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@weight", objGrdBkWgtDet.weight, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@seq", objGrdBkWgtDet.Seq, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@Descrip", objGrdBkWgtDet.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@GrdCompTypId", objGrdBkWgtDet.GrdCompTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ''ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ''ModDate
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@grdbkwgtdetid", objGrdBkWgtDet.GrdBkWgtDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        Return ""

    End Function

    Public Function DeleteGrdBkWgtDet(ByVal objGrdBkWgtDet As GrdBkWgtDetailsInfo, ByVal modDate As DateTime) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        'Set the connection string
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Try
            With sb
                .Append("DELETE FROM arGrdBkWgtDetails ")
                .Append("WHERE InstrGrdBkWgtDetailId = ? ;")
                .Append("SELECT count(*) FROM arGrdBkWgtDetails WHERE InstrGrdBkWgtDetailId = ? ")

            End With

            db.AddParameter("@grdbkwgtdetid", objGrdBkWgtDet.GrdBkWgtDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.AddParameter("@grdbkwgtdetid2", objGrdBkWgtDet.GrdBkWgtDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'db.RunParamSQLExecuteNoneQuery(sb.ToString)
            'db.ClearParameters()
            'sb.Remove(0, sb.Length)

            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function DeleteGrdBkWgtDetails(ByVal GrdBkWgtId As String, ByVal modDate As DateTime) As String
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        Try
            With strSQL
                .Append("DELETE FROM arGrdBkWgtDetails WHERE InstrGrdBkWgtId =? ;")
                .Append("SELECT count(*) FROM arGrdBkWgtDetails WHERE InstrGrdBkWgtId = ? ")
            End With
            db.AddParameter("@grdbkwgtid", GrdBkWgtId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.AddParameter("@grdbkwgtid2", GrdBkWgtId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
            'db.ClearParameters()
            'strSQL.Remove(0, strSQL.Length)
            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(strSQL.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function DeleteGrdBkWgt(ByVal GrdBkWgtId As String, ByVal modDate As DateTime) As String
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        Try

            With strSQL
                .Append("DELETE FROM arGrdBkWeights WHERE InstrGrdBkWgtId =? ;")
                .Append("SELECT count(*) FROM arGrdBkWeights WHERE InstrGrdBkWgtId = ? ")
            End With
            db.AddParameter("@grdbkwgtid", GrdBkWgtId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.AddParameter("@grdbkwgtid2", GrdBkWgtId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
            'db.ClearParameters()
            'strSQL.Remove(0, strSQL.Length)

            'Catch ex As OleDbException
            '   return an error to the client
            'Return DALExceptions.BuildErrorMessage(ex)
            'Finally
            'Close Connection
            'db.CloseConnection()
            'End Try
            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(strSQL.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function UpdateGradeBookWeightsDS(ByVal dt As DataTable) As String


        '   all updates must be encapsulated as one transaction
        Dim connection As New OleDbConnection(GetAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            '   build select query for the GradeSystems data adapter
            Dim sb As New StringBuilder

            '   build select query for the GradeSystemDetails data adapter
            sb = New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("       InstrGrdBkWgtDetailId, ")
                .Append("       InstrGrdBkWgtId, ")
                .Append("       Code, ")
                .Append("       Descrip, ")
                .Append("       Seq, ")
                .Append("       Weight, ")
                .Append("       GrdComponentTypeId, ")
                .Append("       ModUser, ")
                .Append("       ModDate ")
                .Append("FROM arGrdBkWgtDetails ")
            End With

            '   build select command
            Dim gradeSystemDetailsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle GradeSystemsDetails table
            Dim gradeSystemDetailsDataAdapter As New OleDbDataAdapter(gradeSystemDetailsSelectCommand)

            '   build insert, update and delete commands for GradeSystemDetails table
            Dim cb1 As New OleDbCommandBuilder(gradeSystemDetailsDataAdapter)

            '   insert added rows in GradeSystemDetails table
            gradeSystemDetailsDataAdapter.Update(dt.Select(Nothing, Nothing, DataViewRowState.Added))

            '   delete rows in GradeSystemDetails table
            gradeSystemDetailsDataAdapter.Update(dt.Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   update rows in GradeSystemDetails table
            gradeSystemDetailsDataAdapter.Update(dt.Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            Return ""

        Catch ex As OleDb.OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally

            '   close connection
            connection.Close()
        End Try

    End Function
    Public Function GetGradebookWeightsByCourse(ByVal courseId As String) As DataSet


        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("		GBW.InstrGrdBkWgtId, ")
            .Append("       (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("       (Select count(*) from arClassSections CS, arResults R where CS.ReqId=R.TestId and ReqId=GBW.ReqId and StartDate<GBW.EffectiveDate) As HasBeenUsed, ")
            .Append("       GBW.Descrip, ")
            .Append("		GBW.StatusId, ")
            .Append("       GBW.EffectiveDate ")
            .Append("FROM	arGrdBkWeights GBW, syStatuses ST ")
            .Append("WHERE ")
            .Append("       GBW.StatusId = ST.StatusId ")
            .Append("AND    ReqId=? ")
            '.Append("ORDER BY ")
            '.Append("       GBW.EffectiveDate desc ")
        End With

        ' Add the courseId to the parameter list
        db.AddParameter("@CourseId", courseId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Return the datatable in the dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetInstrGrdBkWgtsInfo(ByVal instrGrdBkWgtId As String) As InstrGrdBkWgtsInfo
        Dim instrGrdBkWgtsInfo As New InstrGrdBkWgtsInfo

        Dim ds As DataSet = GetInstrGradeBookWeightingsDS(instrGrdBkWgtId)
        With instrGrdBkWgtsInfo
            .IsInDb = True
            .InstrGrdBkWgtId = instrGrdBkWgtId
            .GradebookWeightingDetailsDS = ds
            If ds.Tables("GrdBkWeights").Rows.Count > 0 Then
                Dim row As DataRow = ds.Tables("GrdBkWeights").Rows(0)
                .EffectiveDate = row("EffectiveDate")
                .ModUser = row("ModUser")
                .ModDate = row("ModDate")
            End If
        End With

        'return instrGrdBkWgtsInfo
        Return instrGrdBkWgtsInfo

    End Function

    Public Function UpdateInstrGradeBookWeightsDS(ByVal ds As DataSet) As String


        '   all updates must be encapsulated as one transaction
        Dim connection As New OleDbConnection(GetAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            '   build select query for the GradeBookWeights data adapter
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("       InstrGrdBkWgtId, ")
                .Append("       InstructorId, ")
                .Append("       Descrip, ")
                .Append("       StatusId, ")
                .Append("       ReqId, ")
                .Append("       EffectiveDate, ")
                .Append("       ModUser, ")
                .Append("       ModDate ")
                .Append("FROM arGrdBkWeights ")
            End With

            '   build select command
            Dim GradeBookWeightsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle GradeBookWeights table
            Dim GradeBookWeightsDataAdapter As New OleDbDataAdapter(GradeBookWeightsSelectCommand)

            '   build insert, update and delete commands for GradeBookWeights table
            Dim cb1 As New OleDbCommandBuilder(GradeBookWeightsDataAdapter)

            '   build select query for the GradeBookWeightDetails data adapter
            sb = New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("       InstrGrdBkWgtDetailId, ")
                .Append("       InstrGrdBkWgtId, ")
                .Append("       Code, ")
                .Append("       Descrip, ")
                .Append("       Weight, ")
                .Append("       Seq, ")
                .Append("       GrdComponentTypeId, ")
                .Append("       Number, ")
                .Append("       GrdPolicyId, ")
                .Append("       Parameter, ")
                .Append("       ModUser, ")
                .Append("       ModDate ")
                .Append("FROM arGrdBkWgtDetails ")
            End With

            '   build select command
            Dim GradeBookWeightDetailsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle GradeBookWeighsDetails table
            Dim GradeBookWeightDetailsDataAdapter As New OleDbDataAdapter(GradeBookWeightDetailsSelectCommand)

            '   build insert, update and delete commands for GradeBookWeighDetails table
            Dim cb2 As New OleDbCommandBuilder(GradeBookWeightDetailsDataAdapter)




            '   add rows in GradeBookWeights table
            GradeBookWeightsDataAdapter.Update(ds.Tables("GrdBkWeights").Select(Nothing, Nothing, DataViewRowState.Added))

            '   add rows in GradeBookWeightDetails table
            GradeBookWeightDetailsDataAdapter.Update(ds.Tables("GrdBkWgtDetails").Select(Nothing, Nothing, DataViewRowState.Added))

            '   delete rows in GradeBookWeightDetails table
            GradeBookWeightDetailsDataAdapter.Update(ds.Tables("GrdBkWgtDetails").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   delete rows in GradeBookWeights table
            GradeBookWeightsDataAdapter.Update(ds.Tables("GrdBkWeights").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   update rows in GradeBookWeights table
            GradeBookWeightsDataAdapter.Update(ds.Tables("GrdBkWeights").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   update rows in GradeBookWeightDetails table
            GradeBookWeightDetailsDataAdapter.Update(ds.Tables("GrdBkWgtDetails").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            Return ""

        Catch ex As OleDb.OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally

            '   close connection
            connection.Close()
        End Try

    End Function
    Public Function AreThereDuplicatedRecordsInGradeBookWeightings(ByVal ds As DataSet) As Boolean

     

        Dim db As New DataAccess
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder

        Dim rowsa() As DataRow = ds.Tables("GrdBkWeights").Select(Nothing, Nothing, DataViewRowState.Added)
        If rowsa.Length > 0 Then
            sb = New StringBuilder()
            Dim ed As Date = CType(rowsa(0)("EffectiveDate"), Date)
            With sb
                .Append("Select count(*) from arGrdBkWeights where reqId='")
                .Append(CType(rowsa(0)("ReqId"), Guid).ToString + "' AND EffectiveDate='")
                .Append(ed.Year.ToString + "-" + ed.Month.ToString + "-" + ed.Day.ToString + "'")
            End With

            Dim obj As Object = db.RunParamSQLScalar(sb.ToString)
            'Close Connection
            db.CloseConnection()
            If CType(obj, Integer) > 0 Then
                '   there are duplicated records
                Return True
            End If
        End If
        Dim rowsu() As DataRow = ds.Tables("GrdBkWeights").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent)
        If rowsu.Length > 0 Then
            sb = New StringBuilder()
            Dim ed As Date = CType(rowsu(0)("EffectiveDate"), Date)
            With sb
                .Append("Select count(*) from arGrdBkWeights where reqId='")
                .Append(CType(rowsu(0)("ReqId"), Guid).ToString + "' AND EffectiveDate='")
                .Append(ed.Year.ToString + "-" + ed.Month.ToString + "-" + ed.Day.ToString + "' ")
                .Append("AND InstrGrdBkWgtId<>'" + CType(rowsu(0)("InstrGrdBkWgtId"), Guid).ToString + "'")
            End With
            Dim obj As Object = db.RunParamSQLScalar(sb.ToString)
            'Close Connection
            db.CloseConnection()
            If CType(obj, Integer) > 0 Then
                '   there are duplicated records
                Return True
            End If
        End If

        Return False
    End Function
    Public Function GetInstrGradeBookWeightingsDS(ByVal instrGrdBkWgtId As String) As DataSet
        Dim ds As New DataSet()

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       InstrGrdBkWgtId, ")
            .Append("		InstructorId, ")
            .Append("		Descrip, ")
            .Append("       StatusId, ")
            .Append("		ReqId, ")
            .Append("       EffectiveDate, ")
            .Append("       ModUser, ")
            .Append("       ModDate ")
            .Append("FROM ")
            .Append("       arGrdBkWeights ")
            .Append("WHERE ")
            .Append("		InstrGrdBkWgtId = ? ")
        End With

        '   build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(GetAdvAppSettings.AppSettings("ConString")))

        '   add AcreditationAgencyId parameter
        sc.Parameters.Add(New OleDbParameter("@instrGrdBkWgtId", instrGrdBkWgtId))

        '   Create adapter to handle GrdBkWeights table
        Dim da As New OleDbDataAdapter(sc)

        '   Fill GrdBkWeights table
        da.Fill(ds, "GrdBkWeights")

        'Create query for the GrdBkWgtDetails table
        '   build the sql query
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       InstrGrdBkWgtDetailId, ")
            .Append("       InstrGrdBkWgtId, ")
            .Append("       Code, ")
            .Append("       Descrip, ")
            .Append("       Weight, ")
            .Append("       Seq, ")
            .Append("       GrdComponentTypeId, ")
            .Append("       Number, ")
            .Append("       GrdPolicyId, ")
            .Append("       Parameter, ")
            .Append("       ModUser, ")
            .Append("       ModDate ")
            .Append("FROM arGrdBkWgtDetails ")
            .Append("WHERE ")
            .Append("       InstrGrdBkWgtId = ? ")
            .Append("ORDER BY ")
            .Append("       Seq ")
        End With

        '   build select command
        sc = New OleDbCommand(sb.ToString, New OleDbConnection(GetAdvAppSettings.AppSettings("ConString")))

        '   add InstrGrdBkWgtId parameter
        sc.Parameters.Add(New OleDbParameter("InstrGrdBkWgtId", instrGrdBkWgtId))

        '   Create adapter to handle GrdBkWgtDetails table
        da = New OleDbDataAdapter(sc)

        '   Fill GrdBkWgtDetails table
        da.Fill(ds, "GrdBkWgtDetails")

        'Create query for the GrdPolicies table
        '   build the sql query
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       GrdPolicyId, ")
            .Append("       Descrip, ")
            .Append("       AllowParam ")
            .Append("FROM   syGrdPolicies ")
        End With

        '   build select command
        sc = New OleDbCommand(sb.ToString, New OleDbConnection(GetAdvAppSettings.AppSettings("ConString")))

        '   Create adapter to handle GrdPolicies table
        da = New OleDbDataAdapter(sc)

        '   Fill GrdPolicies table
        da.Fill(ds, "GrdPolicies")

        '   create primary and foreign key constraints

        '   set primary key for GrdBkWeights table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("GrdBkWeights").Columns("InstrGrdBkWgtId")
        ds.Tables("GrdBkWeights").PrimaryKey = pk0

        '   set primary key for GrdBkWgtDetails table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("GrdBkWgtDetails").Columns("InstrGrdBkWgtDetailId")
        ds.Tables("GrdBkWgtDetails").PrimaryKey = pk1

        '   set primary key for GrdPolicies table
        Dim pk2(0) As DataColumn
        pk2(0) = ds.Tables("GrdPolicies").Columns("GrdPolicyId")
        ds.Tables("GrdPolicies").PrimaryKey = pk2

        '   set foreign key column in GrdBkWgtDetails
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("GrdBkWgtDetails").Columns("InstrGrdBkWgtId")

        '   set foreign key column in GrdBkWgtDetails
        Dim fk1(0) As DataColumn
        fk1(0) = ds.Tables("GrdBkWgtDetails").Columns("GrdPolicyId")

        '   set relation between GrdBkWeights and GrdBkWgtDetails tables
        ds.Relations.Add("GrdBkWeightsGrdBkWgtDetails", pk0, fk0)

        '   set relation between GrdPolicies and GrdBkWgtDetails tables
        ds.Relations.Add("GrdPoliciesGrdBkWgtDetails", pk2, fk1)

        '   return dataset
        Return ds

    End Function
    Public Function GetInstrGrdBkWgtsInfoEmpty(ByVal courseId As String) As InstrGrdBkWgtsInfo
        Dim instrGrdBkWgtsInfo As New InstrGrdBkWgtsInfo

        Dim ds As DataSet = GetInstrGradeBookWeightingsDS(courseId)

        Dim dt As DataSet = GetGrdComponentTypes()

        Dim row As DataRow = ds.Tables("GrdBkWeights").NewRow()
        With instrGrdBkWgtsInfo
            Dim rightNow As Date = Date.Now()
            .IsInDb = True
            .InstrGrdBkWgtId = Guid.NewGuid.ToString
            Dim grdWeightsTable As DataTable = ds.Tables("GrdBkWeights")
            Dim newR As DataRow = grdWeightsTable.NewRow()
            newR("InstrGrdBkWgtId") = .InstrGrdBkWgtId
            newR("InstructorId") = System.DBNull.Value
            newR("Descrip") = "Automatic"
            newR("StatusId") = .StatusId
            newR("ReqId") = New Guid(courseId)
            newR("EffectiveDate") = .EffectiveDate
            grdWeightsTable.Rows.Add(newR)
            Dim grdWeightingsTable As DataTable = ds.Tables("GrdBkWgtDetails")
            For i As Integer = 0 To dt.Tables(0).Rows.Count - 1
                Dim oldRow As DataRow = dt.Tables(0).Rows(i)
                Dim newRow As DataRow = grdWeightingsTable.NewRow()
                newRow("InstrGrdBkWgtDetailId") = Guid.NewGuid
                newRow("InstrGrdBkWgtId") = New Guid(.InstrGrdBkWgtId)
                newRow("Code") = "Automatic"
                newRow("Descrip") = oldRow("Descrip")
                newRow("Weight") = 0
                newRow("Seq") = i
                newRow("GrdComponentTypeId") = oldRow("GrdComponentTypeId")
                'newRow("Number") = 0
                'newRow("GrdPolicyId") = System.DBNull.Value
                'newRow("Parameter") = System.DBNull.Value
                'newRow("ModUser") = System.DBNull.Value
                newRow("ModDate") = rightNow
                grdWeightingsTable.Rows.Add(newRow)
            Next
            '.EffectiveDate = row("EffectiveDate")
            '.ModUser = row("ModUser")
            '.ModDate = row("ModDate")
            .GradebookWeightingDetailsDS = ds
        End With

        'return instrGrdBkWgtsInfo
        Return instrGrdBkWgtsInfo

    End Function
#Region "Get AdvAppsetting object"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
End Class
