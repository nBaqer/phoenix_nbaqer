Imports FAME.Advantage.Common

Public Class FourteenDayLetterDB
    Public Function GetFourteenDayDS(ByVal StartDate As String, ByVal EndDate As String, ByVal campusId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            With sb
                .Append("select distinct firstname,middlename,lastname,transdate,transamount*-1 as transamount,Trans.TransactionID,fs.fundsourcedescrip,s.ssn, ")
                .Append("Case when (select count(*) from arStudaddresses where StudentId=S.studentId and default1=1)>=1 then 'True' else 'False' end as DefaultAddress ")
                .Append("from ")
                .Append("saTransactions Trans,saFundSources FS,arStuEnrollments SE,arStudent S ")
                .Append("where ")
                .Append(" Trans.Voided=0 and ")
                .Append("Trans.IsPosted = 1 and ")
                .Append("Trans.stuenrollid = se.stuenrollid and ")
                .Append("se.studentid = s.studentid and ")
                .Append("transdate >= '" & StartDate & "' and transdate <= '" & EndDate & "' and ")
                .Append("FS.AwardTypeID = 2 ")
                .Append("and SE.CampusId=? ")
                .Append("AND Trans.FundSourceId=FS.FundSourceId ")
            End With

            ' Add campusId to the parameter list
            db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

            db.CloseConnection()

            Return ds

        Catch ExceptionObject As System.Exception
            'Throw New Exception(m_ExceptionMessage, ExceptionObject)
            'Return "this is the error:" & sb.ToString
        Finally
            db.CloseConnection()
        End Try
    End Function

    Public Function GetLetterDS(ByVal TransactionId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            With sb
                .Append("select distinct firstname,middlename,lastname,transdate,transamount,Trans.TransactionID, ")
                .Append("fs.fundsourcedescrip,s.ssn,address1,address2,city, ")
                .Append("(Select statedescrip from syStates St where St.stateid = SAdd.StateId) AS Statedescrip, ")
                .Append("zip from saTransactions Trans,saFundSources FS, ")
                .Append("arStuEnrollments SE,arStudent S,arStudaddresses SAdd ")
                .Append("where ")
                .Append("Trans.stuenrollid = se.stuenrollid and se.studentid = s.studentid and SAdd.StudentId = S.studentId ")
                .Append("and SAdd.default1 = 1 and ")
                .Append("Trans.transactionid in(" & TransactionId & ") and ")
                .Append("Trans.Voided=0 ")
                .Append("and Trans.FundSourceId=FS.FundSourceId ")
            End With

            Dim ds As DataSet = db.RunSQLDataSet(sb.ToString)

            db.CloseConnection()

            Return ds

        Catch ExceptionObject As System.Exception
            'Throw New Exception(m_ExceptionMessage, ExceptionObject)
            'Return "this is the error:" & sb.ToString
        Finally
            db.CloseConnection()
        End Try
    End Function

End Class


