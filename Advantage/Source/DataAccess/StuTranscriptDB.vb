
Imports FAME.Advantage.Common

Public Class StuTranscriptDB

#Region "Private Data Members"
    Private ReadOnly myAdvAppSettings As AdvAppSettings 
    Private ReadOnly mStudentIdentifier As String            
    Private ReadOnly mMinimumGpa As String                   
    Private ReadOnly mGradeCourseRepetitionsMethod As String 
    Private ReadOnly mShowProgramOnTranscript As Boolean     
    Private ReadOnly mIncludeHoursForFailedGrade As String   

#End Region


#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return mStudentIdentifier
        End Get
    End Property

    Public ReadOnly Property MinimumGpa() As String
        Get
            Return mMinimumGpa
        End Get
    End Property

    Public ReadOnly Property GradeCourseRepetitionsMethod() As String
        Get
            Return mGradeCourseRepetitionsMethod
        End Get
    End Property

    Public ReadOnly Property ShowProgramOnTranscript() As Boolean
        Get
            Return mShowProgramOnTranscript
        End Get
    End Property

    Public ReadOnly Property IncludeHoursForFailedGrade() As Boolean
        Get
            If mIncludeHoursForFailedGrade.ToLower = "true" Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

#End Region

    #Region "Constructor"

    Sub New ()
        myAdvAppSettings = AdvAppSettings.GetAppSettings()
        
        mStudentIdentifier  = myAdvAppSettings.AppSettings("StudentIdentifier")
        mMinimumGpa  = myAdvAppSettings.AppSettings("MinimumGPA")
        mGradeCourseRepetitionsMethod = myAdvAppSettings.AppSettings("GradeCourseRepetitionsMethod")
        mShowProgramOnTranscript  = myAdvAppSettings.AppSettings("ShowProgramOnTranscript")
        mIncludeHoursForFailedGrade  = myAdvAppSettings.AppSettings("IncludeHoursForFailingGrade")
    End Sub
   
    #End Region


#Region "Public Methods"

    Public Function GetEnrollment(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStuID As String
        'Dim strStuEnrollId As String

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        'Code Added by Vijay Ramteke on May, 06 2009
        'Dim strTempWhere As String = ""
        Dim strTerm As String = ""
        Dim strClassDate As String = ""
        'Code Added by Vijay Ramteke on May, 06 2009

        'Get ProgVerId and Where Clause from paramInfo.FilterList
        If paramInfo.FilterList <> "" Then
            strWhere &= "AND " & paramInfo.FilterList
        End If

        'Get StudentId and rest of Where Clause from paramInfo.FilterOther
        If paramInfo.FilterOther <> "" Then
            strWhere &= "AND " & paramInfo.FilterOther
        End If

        'Order By Clause.
        If paramInfo.OrderBy <> "" Then
            strOrderBy &= " ORDER BY " & paramInfo.OrderBy
        End If

        'Code Added by Vijay Ramteke on May, 06 2009
        Dim strTempWhere As String = strWhere
        strTempWhere = strTempWhere.ToLower.Replace("and", ";")
        strWhere = ""
        Dim strArr As String() = strTempWhere.Remove(strTempWhere.IndexOf(";", StringComparison.Ordinal), 1).Split(";")
        Dim i As Integer
        For i = 0 To strArr.Length - 1
            If strArr(i).ToLower.IndexOf("arterm", StringComparison.Ordinal) > 0 Then
                strTerm &= " AND " & strArr(i)
            ElseIf strArr(i).ToLower.IndexOf("arclasssections", StringComparison.Ordinal) > 0 Then
                strClassDate &= " AND " & strArr(i)
            Else
                strWhere &= " AND " & strArr(i)
            End If
        Next
        'Code Added by Vijay Ramteke on May, 06 2009

        'Get student identifier depending on what field school is using.
        If StudentIdentifier = "SSN" Then
            strStuID = "A.SSN AS StudentIdentifier,"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStuID = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
        ElseIf StudentIdentifier = "StudentId" Then
            strStuID = "A.StudentNumber AS StudentIdentifier,"
        End If

        With sb
            .Append("SELECT DISTINCT")
            .Append("       arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId,")
            .Append("       (SELECT PrgVerDescrip FROM arPrgVersions WHERE PrgVerId=arStuEnrollments.PrgVerId) AS PrgVerDescrip,")
            '' Code Added by Kamalesh ahuja on 05th Feb to Resolve mantis Issue Id 13953 '''
            .Append("       (SELECT IsContinuingEd FROM arPrgVersions WHERE PrgVerId=arStuEnrollments.PrgVerId) AS IsContinuingEd,")
            ''''''''''''''''''''''''''''''''''''''
            .Append("       (SELECT dg.DegreeDescrip FROM arPrgVersions pv, arDegrees dg WHERE pv.PrgVerId=arStuEnrollments.PrgVerId AND pv.DegreeId=dg.DegreeId) AS DegreeDescrip,")
            .Append("       (SELECT ProgDescrip FROM arPrograms X,arPrgVersions Y WHERE Y.PrgVerId=arStuEnrollments.PrgVerId AND Y.ProgId=X.ProgId) AS ProgramDescrip,")
            .Append("       A.LastName,A.FirstName,A.MiddleName,A.SSN,A.StudentNumber," & strStuID)
            .Append("       arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip,")
            .Append("       (SELECT SysStatusId FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS SysStatusId, ")
            .Append("       arStuEnrollments.StartDate,arStuEnrollments.ExpGradDate,arStuEnrollments.EnrollmentId,arStuEnrollments.LDA,arStuEnrollments.DateDetermined,")
            .Append("       A.DOB,(SELECT TOP 1 Address1 FROM arStudAddresses T,syStatuses Y WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS Address1,")
            .Append("       (SELECT TOP 1 Address2 FROM arStudAddresses T,syStatuses Y WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS Address2,")
            .Append("       (SELECT TOP 1 City FROM arStudAddresses T,syStatuses Y WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS City,")
            .Append("       (SELECT TOP 1 StateDescrip FROM arStudAddresses T,syStates S,syStatuses Y WHERE T.StudentId=A.StudentId AND T.StateId=S.StateId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS StateDescrip,")
            .Append("       (SELECT TOP 1 Zip FROM arStudAddresses T,syStatuses Y WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY Default1 DESC) AS Zip,")
            .Append("       (SELECT TOP 1 ForeignZip FROM arStudAddresses T,syStatuses Y WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS ForeignZip,")
            .Append("       (SELECT TOP 1 OtherState FROM arStudAddresses T,syStatuses Y WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS OtherState,")
            .Append("       (SELECT TOP 1 CountryDescrip FROM arStudAddresses T,syStatuses Y,adCountries C WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' AND T.CountryId=C.CountryId ORDER BY T.Default1 DESC) AS CountryDescrip,")
            .Append("       (SELECT TOP 1 Phone FROM arStudentPhone T,syStatuses Y WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS Phone,")
            .Append("       (SELECT TOP 1 ForeignPhone FROM arStudentPhone T,syStatuses Y WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS ForeignPhone, ")
            .Append("       (select max(PostDate) from arGrdBkResults where StuEnrollId=arStuEnrollments.stuEnrollId) AS ExtDate, ")
            '.Append("       (select max(RecordDate) from arStudentClockAttendance where StuEnrollId=arStuEnrollments.stuEnrollId) AS AttDate, ")
            .Append("(select Max(LDA) from ")
            .Append("( ")
            .Append("	select max(AttendedDate)as LDA from arExternshipAttendance where StuEnrollId=arStuEnrollments.stuEnrollId ")
            .Append("	union all ")
            .Append("	select max(MeetDate) as LDA from atClsSectAttendance where StuEnrollId=arStuEnrollments.stuEnrollId and Actual >= 1.00 and  Actual <> 99.00 and Actual <> 999.00 and Actual <> 9999.00")
            .Append("	union all ")
            .Append("	select max(AttendanceDate) as LDA from atAttendance where EnrollId=arStuEnrollments.stuEnrollId and Actual >=1.00 ")
            .Append("	union all ")
            .Append("	select max(RecordDate) as LDA from arStudentClockAttendance where StuEnrollId=arStuEnrollments.stuEnrollId and (ActualHours >=1.00 and  ActualHours <> 99.00 and ActualHours <> 999.00 and ActualHours <> 9999.00) ")
            .Append("	union all ")
            .Append("	select max(MeetDate) as LDA from atConversionAttendance where StuEnrollId=arStuEnrollments.stuEnrollId and (Actual >=1.00 and  Actual <> 99.00 and Actual <> 999.00 and Actual <> 9999.00) ")
            .Append("	union all ")
            .Append(" select LDA from arStuEnrollments S where S.StuEnrollId=arStuEnrollments.stuEnrollId ")
            '.Append(")TR) as AttDate ")
            'Code Added by Vijay Ramteke on May, 06 2009,Add new column ShowDate Issue on May, 11 2009
            .Append(")TR) as AttDate, '" & IIf(strTerm <> "", strTerm.Replace("'", "''"), "") & "' AS TermCond, '" & IIf(strClassDate <> "", strClassDate.Replace("'", "''"), "") & "' AS ClassCond, " & IIf(paramInfo.ShowRptDateIssue = True, "1", "0") & " AS ShowDateIssue ")
            If MyAdvAppSettings.AppSettings("TranscriptType", paramInfo.CampusId).ToLower = "traditional_b" Then
                .Append(" ," & IIf(paramInfo.ShowRptClassDates = True, 1, 0) & " AS ShowClassDate ")
            Else
                .Append(" ,0 AS ShowClassDate ")
            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code Added by Kamalesh Ahuja on May, 31 2010 to resolve mantis issue id 18868
            .Append(" ," & IIf(paramInfo.ShowLegalDisclaimer = True, 1, 0) & " AS ShowLegalDisc ")
            ''''''''''''

            .Append(" ," & IIf(paramInfo.ShowTermOrModule = True, 1, 0) & " AS ShowTermOrModule ")
            .Append("FROM   arStuEnrollments,arStudent A ")
            .Append("WHERE  arStuEnrollments.StudentId=A.StudentId ")
            .Append(strWhere)
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "StudentTranscript"
            'Need to add these fields so they are to link the tables in report.
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("StrStuEnrollId", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("FullAddress", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("CreditsDesc", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("SignDesc", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("SuppressGrades", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("SuppressStudentAddress", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("SuppressStudentId", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("SuppressLDA", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("DisplayAsCourseCode", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("SuppressDate", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("IncludeAllCreditsAttempted", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("ClockHourSchool", Type.GetType("System.String")))
            'ds.Tables(0).Columns.Add(New DataColumn("ShowTermOrModule", System.Type.GetType("System.String")))
            'Add new table.
            Dim dt As New DataTable("EnrollmentSummary")
            dt.Columns.Add(New DataColumn("StrStuEnrollId", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("TotalClasses", Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("TotalCreditsAttempted", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TotalCreditsEarned", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TotalHours", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TotalGPA", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TotalGradePoints", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("ShowROSSOnlyTabsForStudent", Type.GetType("System.String")))
            ''''''Added By Kamalesh Ahuja on Jan 30, 2011 for Rally Issue Id 1208
            dt.Columns.Add(New DataColumn("TotalScheduledHours", Type.GetType("System.Decimal")))
            '''''''''
            ds.Tables.Add(dt)

            'Add new table.
            dt = New DataTable("EnrollmentCourses")
            dt.Columns.Add(New DataColumn("StrStuEnrollId", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("TermId", Type.GetType("System.Guid")))
            dt.Columns.Add(New DataColumn("TermDescrip", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("DescripXTranscript", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ReqId", Type.GetType("System.Guid")))
            dt.Columns.Add(New DataColumn("Code", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Descrip", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ClsSection", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Credits", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CreditsAttempted", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("StartDate", Type.GetType("System.DateTime")))
            dt.Columns.Add(New DataColumn("EndDate", Type.GetType("System.DateTime")))
            dt.Columns.Add(New DataColumn("TestId", Type.GetType("System.Guid")))
            dt.Columns.Add(New DataColumn("GrdSysDetailId", Type.GetType("System.Guid")))
            dt.Columns.Add(New DataColumn("Grade", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("IsPass", Type.GetType("System.Boolean")))
            dt.Columns.Add(New DataColumn("GPA", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("IsCreditsAttempted", Type.GetType("System.Boolean")))
            dt.Columns.Add(New DataColumn("IsCreditsEarned", Type.GetType("System.Boolean")))
            dt.Columns.Add(New DataColumn("IsInGPA", Type.GetType("System.Boolean")))
            dt.Columns.Add(New DataColumn("CourseCategory", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Hours", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("StrCourseCategory", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("TotalCredits", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TotalHours", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TermGPA", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CumGPA", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("Score", Type.GetType("System.Decimal")))
            'Code Column DateIssue Addded By Vijay Ramteke on May, 11 2009
            dt.Columns.Add(New DataColumn("DateIssue", Type.GetType("System.DateTime")))
            dt.Columns.Add(New DataColumn("ShowDateIssue", Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("ClassStartDate", Type.GetType("System.DateTime")))
            dt.Columns.Add(New DataColumn("ClassEndDate", Type.GetType("System.DateTime")))
            'column addition ends here by Balaji on 09/16/2009
            'Added By Vijay Ramteke on Feb 16, 2010
            dt.Columns.Add(New DataColumn("ScheduledHours", Type.GetType("System.Decimal")))
            'Added By Vijay Ramteke on Feb 16, 2010
            dt.Columns.Add(New DataColumn("AvgScoreNew", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("AvgGPANew", Type.GetType("System.Decimal")))
            ''Code Added By Vijay Ramteke For Rally Id DE1393 On December 22, 2010
            dt.Columns.Add(New DataColumn("IsTransferGrade", Type.GetType("System.Boolean")))
            ''Code Added By Vijay Ramteke For Rally Id DE1393 On December 22, 2010
            ds.Tables.Add(dt)

            'Add new table.
            dt = New DataTable("CourseCategoriesTotals")
            dt.Columns.Add(New DataColumn("StrStuEnrollId", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CourseCategory", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("TotalCredits", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TotalHours", Type.GetType("System.Decimal")))
            ds.Tables.Add(dt)

            '   Build ReportParams table, containing special information for Transcript report
            ''dt = GetTranscriptParams(paramInfo)
            ''ds.Tables.Add(dt.Copy)

            '   Build ReportParams table
            dt = New DataTable("ReportParams")
            dt.Columns.Add("CorporateName", Type.GetType("System.String"))
            dt.Columns.Add("FullAddress", Type.GetType("System.String"))
            dt.Columns.Add("Phone", Type.GetType("System.String"))
            dt.Columns.Add("Fax", Type.GetType("System.String"))
            dt.Columns.Add("SchoolLogo", Type.GetType("System.Byte[]"))
            dt.Columns.Add("StudentIdentifier", Type.GetType("System.String"))
            dt.Columns.Add("TranscriptAuthznTitle", Type.GetType("System.String"))
            dt.Columns.Add("TranscriptAuthznName", Type.GetType("System.String"))
            dt.Columns.Add("Website", Type.GetType("System.String"))
            dt.Columns.Add("SchoolName", Type.GetType("System.String"))
            dt.Columns.Add("SuppressHeader", Type.GetType("System.Boolean"))
            '' Code Added by Kamalesh ahuja on 05th Feb to Resolve mantis Issue Id 13953 '''
            dt.Columns.Add("SupressCreditHours", Type.GetType("System.Boolean"))
            dt.Columns.Add("ShowUseSignLineForAttnd", Type.GetType("System.Boolean"))
            dt.Columns.Add("ShowOfficialTranscript", Type.GetType("System.Boolean"))
            dt.Columns.Add("ShowTermProgressDescription", Type.GetType("System.Boolean"))
            ''''''''''''''''
            Dim dr As DataRow
            dr = dt.NewRow
            If Not (paramInfo.SchoolLogo Is Nothing) Then
                dr("SchoolLogo") = paramInfo.SchoolLogo
            End If
            dr("ShowUseSignLineForAttnd") = paramInfo.ShowUseSignLineForAttnd
            dr("ShowOfficialTranscript") = paramInfo.ShowOfficialTranscript
            dr("StudentIdentifier") = MyAdvAppSettings.AppSettings("StudentIdentifier") & ":"
            dr("SuppressHeader") = paramInfo.SuppressHeader
            '' Code Added by Kamalesh ahuja on 05th Feb to Resolve mantis Issue Id 13953 '''
            Try
                If MyAdvAppSettings.AppSettings("TranscriptType", paramInfo.CampusId).ToLower = "traditional" Then
                    dr("SupressCreditHours") = MyAdvAppSettings.AppSettings("SupressCreditHoursForContinuedEDProgram")
                Else
                    dr("SupressCreditHours") = False
                End If
            Catch ex As Exception
                dr("SupressCreditHours") = False
            End Try
            ''''''''''''''''''''''''''
            dr("ShowTermProgressDescription") = paramInfo.ShowTermProgressDescription

            dt.Rows.Add(dr)
            ds.Tables.Add(dt)

            'Troy:2/26/2007:Added this to bring the letter grades and their gpa and ranges, if any.
            dt = New DataTable("GradeDescriptions")
            dt.Columns.Add("Grade", Type.GetType("System.String"))
            dt.Columns.Add("GPA", Type.GetType("System.String"))
            dt.Columns.Add("Range", Type.GetType("System.String"))
            dt.Columns.Add("GradeDescription", Type.GetType("System.String"))
            dt.Columns.Add("Quality", Type.GetType("System.String"))
            dt.Columns.Add("SuppressGradeDescription", Type.GetType("System.String"))
            ds.Tables.Add(dt)

            dt = New DataTable("Legend")
            dt.Columns.Add("GradeDescription", Type.GetType("System.String"))
            ds.Tables.Add(dt)


        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        
        Return ds
    End Function
    Public Function GetEnrollmentList(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStuID As String

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        Dim strTempWhere As String 
        Dim strTerm As String = ""
        Dim strClassDate As String = ""
  
        'Get ProgVerId and Where Clause from paramInfo.FilterList
        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        'Get StudentId and rest of Where Clause from paramInfo.FilterOther
        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        'Order By Clause.
        If paramInfo.OrderBy <> "" Then
            strOrderBy &= " ORDER BY " & paramInfo.OrderBy
        End If

        'Code Added by Vijay Ramteke on May, 06 2009
        strTempWhere = strWhere
        strTempWhere = strTempWhere.ToLower.Replace("and", ";")
        strWhere = ""
        Dim strArr As String() = strTempWhere.Remove(strTempWhere.IndexOf(";", StringComparison.Ordinal), 1).Split(";")
        Dim i As Integer
        For i = 0 To strArr.Length - 1
            If strArr(i).ToLower.IndexOf("arterm", StringComparison.Ordinal) > 0 Then
                strTerm &= " AND " & strArr(i)
            ElseIf strArr(i).ToLower.IndexOf("arclasssections", StringComparison.Ordinal) > 0 Then
                strClassDate &= " AND " & strArr(i)
            Else
                strWhere &= " AND " & strArr(i)
            End If
        Next
        'Code Added by Vijay Ramteke on May, 06 2009

        'Get student identifier depending on what field school is using.
        If StudentIdentifier = "SSN" Then
            strStuID = "arStudent.SSN"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStuID = "arStuEnrollments.EnrollmentId"
        ElseIf StudentIdentifier = "StudentId" Then
            strStuID = "arStudent.StudentNumber"
        End If

        With sb
            .Append("SELECT ")
            .Append("       arStuEnrollments.StuEnrollId,arStudent.LastName,arStudent.MiddleName,arStudent.FirstName,arStudent.SSN,arStudent.StudentNumber,arStudent.DOB," & strStuID & " AS StudentIdentifier,")
            .Append("       arStuEnrollments.StartDate,arStuEnrollments.ExpGradDate,arStuEnrollments.PrgVerId,arPrgVersions.PrgVerDescrip,")
            '' Code Added by Kamalesh ahuja on 08th Feb to Resolve mantis Issue Id 13953 '''
            .Append("arPrgVersions.IsContinuingEd AS IsContinuingEd,")
            ''''''''''''''''''''''''''''''''''''''
            .Append("       (SELECT ProgDescrip FROM arPrograms WHERE ProgId=arPrgVersions.ProgId) AS ProgramDescrip,")
            .Append("       (SELECT dg.DegreeDescrip FROM arPrgVersions pv, arDegrees dg WHERE pv.PrgVerId=arStuEnrollments.PrgVerId AND pv.DegreeId=dg.DegreeId) AS DegreeDescrip,")
            .Append("       syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId,syCampuses.CampDescrip,")
            .Append("       arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip,")
            .Append("       arStuEnrollments.StartDate,arStuEnrollments.ExpGradDate,arStuEnrollments.EnrollmentId,arStuEnrollments.LDA,arStuEnrollments.DateDetermined,")
            .Append("       (SELECT SysStatusId FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS SysStatusId, ")
            .Append("       (SELECT TOP 1 Address1 FROM arStudAddresses T,syStatuses Y WHERE T.StudentId=arStudent.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS Address1,")
            .Append("       (SELECT TOP 1 Address2 FROM arStudAddresses T,syStatuses Y WHERE T.StudentId=arStudent.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS Address2,")
            .Append("       (SELECT TOP 1 City FROM arStudAddresses T,syStatuses Y WHERE T.StudentId=arStudent.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS City,")
            .Append("       (SELECT TOP 1 StateDescrip FROM arStudAddresses T,syStates S,syStatuses Y WHERE T.StudentId=arStudent.StudentId AND T.StateId=S.StateId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS StateDescrip,")
            .Append("       (SELECT TOP 1 Zip FROM arStudAddresses T,syStatuses Y WHERE T.StudentId=arStudent.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS Zip,")
            .Append("       (SELECT TOP 1 ForeignZip FROM arStudAddresses T,syStatuses Y WHERE T.StudentId=arStudent.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS ForeignZip,")
            .Append("       (SELECT TOP 1 OtherState FROM arStudAddresses T,syStatuses Y WHERE T.StudentId=arStudent.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS OtherState,")
            .Append("       (SELECT TOP 1 CountryDescrip FROM arStudAddresses T,syStatuses Y,adCountries C WHERE T.StudentId=arStudent.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' AND T.CountryId=C.CountryId ORDER BY T.Default1 DESC) AS CountryDescrip,")
            .Append("       (SELECT TOP 1 Phone FROM arStudentPhone T,syStatuses Y WHERE T.StudentId=arStudent.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS Phone,")
            .Append("       (SELECT TOP 1 ForeignPhone FROM arStudentPhone T,syStatuses Y WHERE T.StudentId=arStudent.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS ForeignPhone ,")
            .Append("       (select max(PostDate) from arGrdBkResults where StuEnrollId=arStuEnrollments.stuEnrollId) AS ExtDate, ")
            '.Append("       (select max(RecordDate) from arStudentClockAttendance where StuEnrollId=arStuEnrollments.stuEnrollId) AS AttDate, ")
            .Append("(select Max(LDA) from ")
            .Append("( ")
            .Append("	select max(AttendedDate)as LDA from arExternshipAttendance where StuEnrollId=arStuEnrollments.stuEnrollId ")
            .Append("	union all ")
            .Append("	select max(MeetDate) as LDA from atClsSectAttendance where StuEnrollId=arStuEnrollments.stuEnrollId and Actual >= 1 ")
            .Append("	union all ")
            .Append("	select max(AttendanceDate) as LDA from atAttendance where EnrollId=arStuEnrollments.stuEnrollId and Actual >=1 ")
            .Append("	union all ")
            .Append("	select max(RecordDate) as LDA from arStudentClockAttendance where StuEnrollId=arStuEnrollments.stuEnrollId and (ActualHours >=1.00 and  ActualHours <> 99.00 and ActualHours <> 999.00 and ActualHours <> 9999.00) ")
            .Append("	union all ")
            .Append("	select max(MeetDate) as LDA from atConversionAttendance where StuEnrollId=arStuEnrollments.stuEnrollId and (Actual >=1.00 and  Actual <> 99.00 and Actual <> 999.00 and Actual <> 9999.00) ")
            '.Append(")TR) as AttDate ")
            'Code Added by Vijay Ramteke on May, 06 2009
            .Append(")TR) as AttDate, '" & IIf(strTerm <> "", strTerm.Replace("'", "''"), "") & "' AS TermCond, '" & IIf(strClassDate <> "", strClassDate.Replace("'", "''"), "") & "' AS ClassCond, " & IIf(paramInfo.ShowRptDateIssue = True, "1", "0") & " AS ShowDateIssue  ")
            'Code Added by Vijay Ramteke on May, 06 2009

            'Code Added by Kamalesh Ahuja on Oct, 28 2009
            If MyAdvAppSettings.AppSettings("TranscriptType", paramInfo.CampusId).ToLower = "traditional_b" Then
                .Append(" ," & IIf(paramInfo.ShowRptClassDates = True, 1, 0) & " AS ShowClassDate ")
            Else
                .Append(" ,0 AS ShowClassDate ")
            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            .Append("FROM   arStuEnrollments,arStudent,arPrgVersions,syCmpGrpCmps,syCampGrps,syCampuses ")
            .Append("WHERE  arStuEnrollments.StudentId = arStudent.StudentId ")
            .Append("       AND arPrgVersions.PrgVerId=arStuEnrollments.PrgVerId ")
            .Append("       AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId ")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
            .Append("       AND syCampuses.CampusId=arStuEnrollments.CampusId ")
            .Append(strWhere)
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then


                ds.Tables(0).TableName = "MultiTranscripts"
                'Need to add these fields so they are to link the tables in report.
                ds.Tables(0).Columns.Add(New DataColumn("StudentName", Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("StudentName2", Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("StrStuEnrollId", Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("FullAddress", Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("CreditsDesc", Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("SignDesc", Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("SuppressGrades", Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("SuppressStudentAddress", Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("SuppressStudentId", Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("SuppressLDA", Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("DisplayAsCourseCode", Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("SuppressDate", Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("IncludeAllCreditsAttempted", Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("ClockHourSchool", Type.GetType("System.String")))

                'Add new table.
                Dim dt As New DataTable("EnrollmentSummary")
                dt.Columns.Add(New DataColumn("StrStuEnrollId", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("TotalClasses", Type.GetType("System.Int32")))
                dt.Columns.Add(New DataColumn("TotalCreditsAttempted", Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("TotalCreditsEarned", Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("TotalHours", Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("TotalGPA", Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("FirstName", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("LastName", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("TotalGradePoints", Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("ShowROSSOnlyTabsForStudent", Type.GetType("System.String")))
                ds.Tables.Add(dt)
                'Add new table.
                dt = New DataTable("EnrollmentCourses")
                dt.Columns.Add(New DataColumn("StrStuEnrollId", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("TermId", Type.GetType("System.Guid")))
                dt.Columns.Add(New DataColumn("TermDescrip", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("DescripXTranscript", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("ReqId", Type.GetType("System.Guid")))
                dt.Columns.Add(New DataColumn("Code", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Descrip", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("ClsSection", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Credits", Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("CreditsAttempted", Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("StartDate", Type.GetType("System.DateTime")))
                dt.Columns.Add(New DataColumn("EndDate", Type.GetType("System.DateTime")))
                dt.Columns.Add(New DataColumn("TestId", Type.GetType("System.Guid")))
                dt.Columns.Add(New DataColumn("GrdSysDetailId", Type.GetType("System.Guid")))
                dt.Columns.Add(New DataColumn("Grade", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("IsPass", Type.GetType("System.Boolean")))
                dt.Columns.Add(New DataColumn("GPA", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("IsCreditsAttempted", Type.GetType("System.Boolean")))
                dt.Columns.Add(New DataColumn("IsCreditsEarned", Type.GetType("System.Boolean")))
                dt.Columns.Add(New DataColumn("IsInGPA", Type.GetType("System.Boolean")))
                dt.Columns.Add(New DataColumn("CourseCategory", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Hours", Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("StrCourseCategory", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("TotalCredits", Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("TotalHours", Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("TermGPA", Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("CumGPA", Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Score", Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("FirstName", Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("LastName", Type.GetType("System.String")))
                'Code Column DateIssue Addded By Vijay Ramteke on May, 11 2009
                dt.Columns.Add(New DataColumn("DateIssue", Type.GetType("System.DateTime")))
                dt.Columns.Add(New DataColumn("ShowDateIssue", Type.GetType("System.Int32")))
                'Code Column DateIssue Addded By Vijay Ramteke on May, 11 2009
                'Code column added by Balaji on 09/16/2009
                dt.Columns.Add(New DataColumn("ClassStartDate", Type.GetType("System.DateTime")))
                dt.Columns.Add(New DataColumn("ClassEndDate", Type.GetType("System.DateTime")))
                'column addition ends here by Balaji on 09/16/2009
                'Added By Vijay Ramteke on Feb 16, 2010
                dt.Columns.Add(New DataColumn("ScheduledHours", Type.GetType("System.Decimal")))
                'Added By Vijay Ramteke on Feb 16, 2010
                ds.Tables.Add(dt)


                '   Build ReportParams table, containing special information for Transcript report
                'dt = GetTranscriptParams(paramInfo)
                'ds.Tables.Add(dt.Copy)

                '   Build ReportParams table
                dt = New DataTable("ReportParams")
                dt.Columns.Add("CorporateName", Type.GetType("System.String"))
                dt.Columns.Add("FullAddress", Type.GetType("System.String"))
                dt.Columns.Add("Phone", Type.GetType("System.String"))
                dt.Columns.Add("Fax", Type.GetType("System.String"))
                dt.Columns.Add("SchoolLogo", Type.GetType("System.Byte[]"))
                dt.Columns.Add("StudentIdentifier", Type.GetType("System.String"))
                dt.Columns.Add("TranscriptAuthznTitle", Type.GetType("System.String"))
                dt.Columns.Add("TranscriptAuthznName", Type.GetType("System.String"))
                dt.Columns.Add("Website", Type.GetType("System.String"))
                dt.Columns.Add("SchoolName", Type.GetType("System.String"))
                dt.Columns.Add("SuppressHeader", Type.GetType("System.Boolean"))
                '' Code Added by Kamalesh ahuja on 05th Feb to Resolve mantis Issue Id 13953 '''
                dt.Columns.Add("SupressCreditHours", Type.GetType("System.Boolean"))
                dt.Columns.Add("ShowTermProgressDescription", Type.GetType("System.Boolean"))
                ''''''''''''''''
                Dim dr As DataRow
                dr = dt.NewRow
                If Not (paramInfo.SchoolLogo Is Nothing) Then
                    dr("SchoolLogo") = paramInfo.SchoolLogo
                End If
                dr("StudentIdentifier") = MyAdvAppSettings.AppSettings("StudentIdentifier") & ":"
                dr("SuppressHeader") = paramInfo.SuppressHeader
                dr("ShowTermProgressDescription") = paramInfo.ShowTermProgressDescription
                '' Code Added by Kamalesh ahuja on 05th Feb to Resolve mantis Issue Id 13953 '''
                Try
                    If MyAdvAppSettings.AppSettings("TranscriptType", paramInfo.CampusId).ToLower = "traditional" Then
                        dr("SupressCreditHours") = MyAdvAppSettings.AppSettings("SupressCreditHoursForContinuedEDProgram")
                    Else
                        dr("SupressCreditHours") = False
                    End If
                Catch ex As Exception
                    dr("SupressCreditHours") = False
                End Try
                ''''''''''''''''''''''''''

                dt.Rows.Add(dr)
                ds.Tables.Add(dt)

                'Troy:4/15/2007:Added this to bring the letter grades and their gpa and ranges, if any.
                dt = New DataTable("GradeDescriptions")
                dt.Columns.Add("Grade", Type.GetType("System.String"))
                dt.Columns.Add("GPA", Type.GetType("System.String"))
                dt.Columns.Add("Range", Type.GetType("System.String"))
                dt.Columns.Add("GradeDescription", Type.GetType("System.String"))
                dt.Columns.Add("Quality", Type.GetType("System.String"))
                dt.Columns.Add("SuppressGradeDescription", Type.GetType("System.String"))
                ds.Tables.Add(dt)

                dt = New DataTable("Legend")
                dt.Columns.Add("GradeDescription", Type.GetType("System.String"))
                ds.Tables.Add(dt)
            End If
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    Public Function GetEnrollmentList_SP(ByVal paramInfo As ReportParamInfo) As DataSet
       Dim db As New SQLDataAccess
        Dim ds As DataSet
        Dim strWhere As String
       

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        Dim strTempWhere As String 
        Dim strTerm As String = ""
        Dim strClassDate As String = ""
 
        'Get ProgVerId and Where Clause from paramInfo.FilterList
        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        'Get StudentId and rest of Where Clause from paramInfo.FilterOther
        '********************************************************************************'
        Dim campGrpId As String 
        Dim prgVerId As String
        Dim statusCodeId As String = String.Empty

        '********************************************************************************'


        'Get StudentId and rest of Where Clause from paramInfo.FilterOther


        strTempWhere = strWhere
        strTempWhere = strTempWhere.ToLower.Replace("and", ";")
        strWhere = ""
        Dim strArr As String() = strTempWhere.Remove(strTempWhere.IndexOf(";", System.StringComparison.Ordinal), 1).Split(";")

        For i As Integer = 0 To strArr.Length - 1
            If strArr(i).ToLower.IndexOf("arterm", System.StringComparison.Ordinal) > 0 Then
                strTerm &= " and " & strArr(i)
            Else
                strWhere &= " and " & strArr(i)
            End If
        Next


        '********************************************************************************'
        'get the values of the parameters
        campGrpId = strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in (")).ToLower.IndexOf(" and ")).Replace("t1.campgrpid in", "").Replace(")", "").Replace("(", "")

        'get the prgverid
        If strWhere.ToLower.Contains("arstuenrollments.prgverid in ") Then
            If strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid in ")).ToLower.IndexOf(" and ") >= 0 Then
                prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid in (")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.prgverid in", "").Replace(")", "").Replace("(", "")
            Else
                prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid in (")).Replace("arstuenrollments.prgverid in", "").Replace(")", "").Replace("(", "")
            End If
        Else
            If strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid = ")).ToLower.IndexOf(" and ") >= 0 Then
                prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid = "), strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid = ")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.prgverid =", "").Replace(")", "").Replace("(", "")
            Else
                prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid = ")).Replace("arstuenrollments.prgverid =", "").Replace(")", "").Replace("(", "")
            End If
        End If
        'end the prgverid

        'get the statuscode
        If strWhere.ToLower.Contains("arstuenrollments.statuscodeid") Then
            If strWhere.Contains("arstuenrollments.statuscodeid in ") Then
                If strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid in ")).ToLower.IndexOf(" and ") >= 0 Then
                    statusCodeId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid in (")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.statuscodeid in", "").Replace(")", "").Replace("(", "")
                Else
                    statusCodeId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid in (")).Replace("arstuenrollments.statuscodeid in", "").Replace(")", "").Replace("(", "")
                End If
            Else
                If strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).ToLower.IndexOf(" and ") >= 0 Then
                    statusCodeId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid = "), strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.statuscodeid =", "").Replace(")", "").Replace("(", "")
                Else
                    statusCodeId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).Replace("arstuenrollments.statuscodeid =", "").Replace(")", "").Replace("(", "")
                End If
            End If
        End If
        'end the statuscode

        '********************************************************************************'


        'get the CohortStartDate
        Dim cohortStartDate1 As DateTime = "1/1/2000"
        Dim cohortStartDate2 As DateTime = "1/1/2099"
        Dim strCohort As String 
        If paramInfo.FilterOther.ToLower.Contains("arstuenrollments.cohortstartdate") Then
            If paramInfo.FilterOther.ToLower.Contains("arstuenrollments.expgraddate") Then
                strCohort = paramInfo.FilterOther.Substring(0, paramInfo.FilterOther.IndexOf("AND arStuEnrollments.ExpGradDate"))
                strCohort = strCohort.Substring(paramInfo.FilterOther.LastIndexOf("arStuEnrollments.CohortStartDate"))
                strCohort = strCohort.Replace("arStuEnrollments.CohortStartDate BETWEEN ", "")
            Else
                strCohort = paramInfo.FilterOther.Substring(paramInfo.FilterOther.LastIndexOf("arStuEnrollments.CohortStartDate"))
                strCohort = strCohort.Replace("arStuEnrollments.CohortStartDate BETWEEN ", "")
            End If
            cohortStartDate1 = strCohort.Substring(1, strCohort.IndexOf("AND") - 3).Trim
            cohortStartDate2 = strCohort.Substring(strCohort.IndexOf("AND")).Trim.Replace("AND", "").Replace("'", "").Trim
        End If

        'end the CohortStartDate

        '********************************************************************************'



        campGrpId = campGrpId.Replace(" ", "").Replace("'", "")
        'campGrpId = "'" + campGrpId + "'"
        prgVerId = prgVerId.Replace(" ", "").Replace("'", "")
        'prgVerId = "'" + prgVerId + "'"



        db.AddParameter("@campGrpId", campGrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        db.AddParameter("@campusId", New Guid(paramInfo.CampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@prgVerId", prgVerId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        If statusCodeId = String.Empty Then
            db.AddParameter("@statusCodeId", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        Else
            statusCodeId = statusCodeId.Replace(" ", "").Replace("'", "")
            'statusCodeId = "'" + statusCodeId + "'"
            db.AddParameter("@statusCodeId", statusCodeId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        End If

        db.AddParameter("@cohortStartDate1", cohortStartDate1, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@cohortStartDate2", cohortStartDate2, SqlDbType.DateTime, , ParameterDirection.Input)
        '' New Paramaters Added by Kamalesh Ahuja on March 08 2010 ''
        db.AddParameter("@StrTerm", strTerm, SqlDbType.VarChar, , ParameterDirection.Input)
        db.AddParameter("@strClassDates", strClassDate, SqlDbType.VarChar, , ParameterDirection.Input)
        db.AddParameter("@ShowClassDates", paramInfo.ShowRptClassDates, SqlDbType.Int, , ParameterDirection.Input)
        db.AddParameter("@ShowDateIssues", paramInfo.ShowRptDateIssue, SqlDbType.Int, , ParameterDirection.Input)
        db.AddParameter("@TranscriptType", MyAdvAppSettings.AppSettings("TranscriptType", paramInfo.CampusId).ToLower, SqlDbType.VarChar, , ParameterDirection.Input)
        ''''''''''''''''''

        '' New Paramater Added by Kamalesh Ahuja on May 31 2010 to resolve mantis issue id 18868 ''
        db.AddParameter("@ShowLegalDisc", paramInfo.ShowLegalDisclaimer, SqlDbType.Int, , ParameterDirection.Input)
        ''''''''''''''''
        ds = db.RunParamSQLDataSet_SP("dbo.usp_GetEnrollmentListForTranscript")



        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "MultiTranscripts"

            Dim gradDate1 As DateTime
            Dim gradDate2 As DateTime
            Dim str As String 
            Dim summaryInfo As DataTable = ds.Tables("MultiTranscripts")
            ' SummaryInfo.DefaultView.Sort = strOrder
            ds.Tables.RemoveAt(0)

            If paramInfo.FilterOther.ToLower.Contains("arstuenrollments.expgraddate") Then

                Dim graddateinput As String = paramInfo.FilterOther.Substring(paramInfo.FilterOther.IndexOf("arStuEnrollments.ExpGradDate"), paramInfo.FilterOther.Length - paramInfo.FilterOther.IndexOf("arStuEnrollments.ExpGradDate"))

                '*****************************************************************'
                'this varies depending upon the condition

                'If paramInfo.FilterOther.ToUpper.IndexOf("NOT BETWEEN") >= 0 Then
                If graddateinput.ToUpper.IndexOf("NOT BETWEEN") >= 0 Then
                    '"arStuEnrollments.ExpGradDate NOT BETWEEN '2/28/2010 12:00:00 AM' AND '2/28/2010 11:59:59 PM'
                    'str = paramInfo.FilterOther.Substring(0, paramInfo.FilterOther.LastIndexOf("'"))
                    str = graddateinput.Substring(0, graddateinput.LastIndexOf("'"))
                    str = str.Replace("arStuEnrollments.ExpGradDate NOT BETWEEN ", "")
                    gradDate1 = str.Substring(1, str.IndexOf("AND") - 3).Trim
                    gradDate2 = str.Substring(str.IndexOf("AND")).Trim.Replace("AND", "").Replace("'", "").Trim
                    Dim query = From order In summaryInfo.AsEnumerable() _
                    Where order.Field(Of DateTime)("ExpGradDate") < gradDate1 Or order.Field(Of DateTime)("ExpGradDate") > gradDate2

                    If query.Count > 0 Then
                        summaryInfo = query.CopyToDataTable()
                    Else
                        summaryInfo = New DataTable()
                    End If
                ElseIf paramInfo.FilterOther.ToUpper.IndexOf("BETWEEN") >= 0 Then
                    'str = paramInfo.FilterOther.Substring(0, paramInfo.FilterOther.LastIndexOf("'"))
                    str = paramInfo.FilterOther.Substring(paramInfo.FilterOther.LastIndexOf("arStuEnrollments.ExpGradDate"))
                    str = str.Replace("arStuEnrollments.ExpGradDate BETWEEN ", "")
                    gradDate1 = str.Substring(1, str.IndexOf("AND") - 3).Trim
                    gradDate2 = str.Substring(str.IndexOf("AND")).Trim.Replace("AND", "").Replace("'", "").Trim
                    Dim query = From order In summaryInfo.AsEnumerable() _
                    Where order.Field(Of DateTime)("ExpGradDate") >= gradDate1 And order.Field(Of DateTime)("ExpGradDate") <= gradDate2
                    If query.Count > 0 Then
                        summaryInfo = query.CopyToDataTable()
                    Else
                        summaryInfo = New DataTable()
                    End If

                ElseIf graddateinput.IndexOf(">") >= 0 Then
                    ''arStuEnrollments.ExpGradDate > '2/28/2010 11:59:59 PM'
                    str = graddateinput.Substring(0, graddateinput.LastIndexOf("'"))
                    str = str.Replace("arStuEnrollments.ExpGradDate > ", "").Replace("'", "").Trim
                    gradDate1 = str
                    Dim query = From order In summaryInfo.AsEnumerable() _
                   Where order.Field(Of DateTime)("ExpGradDate") > gradDate1
                    If query.Count > 0 Then
                        summaryInfo = query.CopyToDataTable()
                    Else
                        summaryInfo = New DataTable()
                    End If
                ElseIf graddateinput.IndexOf("<") >= 0 Then
                    str = graddateinput.Substring(0, graddateinput.LastIndexOf("'"))
                    str = str.Replace("arStuEnrollments.ExpGradDate < ", "").Replace("'", "").Trim
                    gradDate1 = str
                    Dim query = From order In summaryInfo.AsEnumerable() _
                   Where order.Field(Of DateTime)("ExpGradDate") < gradDate1
                    If query.Count > 0 Then
                        summaryInfo = query.CopyToDataTable()
                    Else
                        summaryInfo = New DataTable()
                    End If
                ElseIf graddateinput.IndexOf("IS NULL") >= 0 Then
                    'str = graddateinput.Substring(0, graddateinput.LastIndexOf("'"))
                    'str = str.Replace("arStuEnrollments.ExpGradDate IS NULL", "").Replace("'", "").Trim
                    'gradDate1 = str
                    Dim query = From order In summaryInfo.AsEnumerable() _
                   Where order.Field(Of Nullable(Of DateTime))("ExpGradDate") Is Nothing
                    If query.Count > 0 Then
                        summaryInfo = query.CopyToDataTable()
                    Else
                        summaryInfo = New DataTable()
                    End If
                End If
            Else

                Dim query = From order In summaryInfo.AsEnumerable()
                If query.Count > 0 Then
                    summaryInfo = query.CopyToDataTable()
                Else
                    summaryInfo = New DataTable()
                End If


            End If
            '********************************************************************'
            ds.Tables.Add(summaryInfo)
            ds.Tables(0).TableName = "MultiTranscripts"


            'Need to add these fields so they are to link the tables in report.
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("StudentName2", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("StrStuEnrollId", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("FullAddress", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("CreditsDesc", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("SignDesc", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("SuppressGrades", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("SuppressStudentAddress", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("SuppressStudentId", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("SuppressLDA", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("DisplayAsCourseCode", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("SuppressDate", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("IncludeAllCreditsAttempted", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("ClockHourSchool", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("ShowTermOrModule", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("StudentIdentifier", Type.GetType("System.String")))
            'ds.Tables(0).Columns.Add(New DataColumn("ShowDateIssue", System.Type.GetType("System.Int32")))
            'ds.Tables(0).Columns.Add(New DataColumn("ShowClassDate", System.Type.GetType("System.Int32")))
            'Add new table.

            Dim dt As New DataTable("EnrollmentSummary")
            dt.Columns.Add(New DataColumn("StrStuEnrollId", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("TotalClasses", Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("TotalCreditsAttempted", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TotalCreditsEarned", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TotalHours", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TotalGPA", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("FirstName", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("LastName", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("TotalGradePoints", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("ShowROSSOnlyTabsForStudent", Type.GetType("System.String")))
            ''''''Added By Kamalesh Ahuja on Jan 30, 2011 for Rally Issue Id 1208
            dt.Columns.Add(New DataColumn("TotalScheduledHours", Type.GetType("System.Decimal")))
            '''''''''
            ds.Tables.Add(dt)
            'Add new table.
            dt = New DataTable("EnrollmentCourses")
            dt.Columns.Add(New DataColumn("StrStuEnrollId", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("TermId", Type.GetType("System.Guid")))
            dt.Columns.Add(New DataColumn("TermDescrip", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("DescripXTranscript", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ReqId", Type.GetType("System.Guid")))
            dt.Columns.Add(New DataColumn("Code", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Descrip", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ClsSection", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Credits", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CreditsAttempted", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("StartDate", Type.GetType("System.DateTime")))
            dt.Columns.Add(New DataColumn("EndDate", Type.GetType("System.DateTime")))
            dt.Columns.Add(New DataColumn("TestId", Type.GetType("System.Guid")))
            dt.Columns.Add(New DataColumn("GrdSysDetailId", Type.GetType("System.Guid")))
            dt.Columns.Add(New DataColumn("Grade", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("IsPass", Type.GetType("System.Boolean")))
            dt.Columns.Add(New DataColumn("GPA", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("IsCreditsAttempted", Type.GetType("System.Boolean")))
            dt.Columns.Add(New DataColumn("IsCreditsEarned", Type.GetType("System.Boolean")))
            dt.Columns.Add(New DataColumn("IsInGPA", Type.GetType("System.Boolean")))
            dt.Columns.Add(New DataColumn("CourseCategory", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Hours", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("StrCourseCategory", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("TotalCredits", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TotalHours", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TermGPA", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CumGPA", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("Score", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("FirstName", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("LastName", Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("DateIssue", Type.GetType("System.DateTime")))
            dt.Columns.Add(New DataColumn("ShowDateIssue", Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("ClassStartDate", Type.GetType("System.DateTime")))
            dt.Columns.Add(New DataColumn("ClassEndDate", Type.GetType("System.DateTime")))
             dt.Columns.Add(New DataColumn("ScheduledHours", Type.GetType("System.Decimal")))
            '''' Code changes by kamalesh Ahuja on 17 dec 2010 to resolve issue id DE1394
            dt.Columns.Add(New DataColumn("AvgScoreNew", Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("AvgGPANew", Type.GetType("System.Decimal")))
            ''''''''''''''''''
            dt.Columns.Add(New DataColumn("IsTransferGrade", Type.GetType("System.Boolean")))
            ''Code Added By Vijay Ramteke For Rally Id DE1393 On December 23, 2010
            ds.Tables.Add(dt)


            '   Build ReportParams table, containing special information for Transcript report
            'dt = GetTranscriptParams(paramInfo)
            'ds.Tables.Add(dt.Copy)

            '   Build ReportParams table
            dt = New DataTable("ReportParams")
            dt.Columns.Add("CorporateName", Type.GetType("System.String"))
            dt.Columns.Add("FullAddress", Type.GetType("System.String"))
            dt.Columns.Add("Phone", Type.GetType("System.String"))
            dt.Columns.Add("Fax", Type.GetType("System.String"))
            dt.Columns.Add("SchoolLogo", Type.GetType("System.Byte[]"))
            dt.Columns.Add("StudentIdentifier", Type.GetType("System.String"))
            dt.Columns.Add("TranscriptAuthznTitle", Type.GetType("System.String"))
            dt.Columns.Add("TranscriptAuthznName", Type.GetType("System.String"))
            dt.Columns.Add("Website", Type.GetType("System.String"))
            dt.Columns.Add("SchoolName", Type.GetType("System.String"))
            dt.Columns.Add("SuppressHeader", Type.GetType("System.Boolean"))
            dt.Columns.Add("SupressCreditHours", Type.GetType("System.Boolean"))
            dt.Columns.Add("ShowUseSignLineForAttnd", Type.GetType("System.Boolean"))
            dt.Columns.Add("ShowOfficialTranscript", Type.GetType("System.Boolean"))
            dt.Columns.Add("ShowTermProgressDescription", Type.GetType("System.Boolean"))
            Dim dr As DataRow
            dr = dt.NewRow
            If Not (paramInfo.SchoolLogo Is Nothing) Then
                dr("SchoolLogo") = paramInfo.SchoolLogo
            End If
            dr("ShowUseSignLineForAttnd") = paramInfo.ShowUseSignLineForAttnd
            dr("ShowOfficialTranscript") = paramInfo.ShowOfficialTranscript
            dr("StudentIdentifier") = MyAdvAppSettings.AppSettings("StudentIdentifier") & ":"
            dr("SuppressHeader") = paramInfo.SuppressHeader
            dr("ShowTermProgressDescription") = paramInfo.ShowTermProgressDescription
            '' Code Added by Kamalesh ahuja on 05th Feb to Resolve mantis Issue Id 13953 '''
            Try
                If MyAdvAppSettings.AppSettings("TranscriptType", paramInfo.CampusId).ToLower = "traditional" Then
                    dr("SupressCreditHours") = MyAdvAppSettings.AppSettings("SupressCreditHoursForContinuedEDProgram")
                Else
                    dr("SupressCreditHours") = False
                End If
            Catch ex As Exception
                dr("SupressCreditHours") = False
            End Try

            dt.Rows.Add(dr)
            ds.Tables.Add(dt)

            'Troy:4/15/2007:Added this to bring the letter grades and their gpa and ranges, if any.
            dt = New DataTable("GradeDescriptions")
            dt.Columns.Add("Grade", Type.GetType("System.String"))
            dt.Columns.Add("GPA", Type.GetType("System.String"))
            dt.Columns.Add("Range", Type.GetType("System.String"))
            dt.Columns.Add("GradeDescription", Type.GetType("System.String"))
            dt.Columns.Add("Quality", Type.GetType("System.String"))
            dt.Columns.Add("SuppressGradeDescription", Type.GetType("System.String"))
            ds.Tables.Add(dt)

            dt = New DataTable("Legend")
            dt.Columns.Add("GradeDescription", Type.GetType("System.String"))
            ds.Tables.Add(dt)

        End If
        Return ds
    End Function

    Public Function GetStuLowGPA(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStuId As String

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        'Order By Clause.
        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        Else
            strOrderBy &= ",arPrgVersions.PrgVerDescrip,arStuEnrollments.PrgVerId,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName"
        End If

        If StudentIdentifier = "SSN" Then
            strStuId = "arStudent.SSN AS StudentIdentifier,"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStuId = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
        ElseIf StudentIdentifier = "StudentId" Then
            strStuId = "arStudent.StudentNumber AS StudentIdentifier,"
        End If

        With sb
            .Append("SELECT ")
            .Append("       arStuEnrollments.StuEnrollId,arStudent.LastName,")
            .Append("       arStudent.MiddleName,arStudent.FirstName,arStudent.DOB," & strStuId)
            .Append("       arStuEnrollments.ExpGradDate,arStuEnrollments.PrgVerId,arPrgVersions.PrgVerDescrip,")
            .Append("       syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId,syCampuses.CampDescrip,")
            .Append("       arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip ")
            .Append("FROM   arStuEnrollments,arStudent,arPrgVersions,syCmpGrpCmps,syCampGrps,syCampuses ")
            .Append("WHERE  arStuEnrollments.StudentId=arStudent.StudentId")
            .Append("       AND arPrgVersions.PrgVerId=arStuEnrollments.PrgVerId")
            .Append("       AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId")
            .Append("       AND syCampuses.CampusId=arStuEnrollments.CampusId")
            .Append(strWhere)
            .Append(" ORDER BY syCampGrps.CampGrpDescrip,syCmpGrpCmps.CampGrpId,syCampuses.CampDescrip,arStuEnrollments.CampusId")
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "StudentsWithLowGPA"
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("StudentCount", Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("GPA", Type.GetType("System.Decimal")))
            ds.Tables(0).Columns.Add(New DataColumn("PrgVerIdStr", Type.GetType("System.String")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

#End Region

#Region "Private Members"

   
    'Private Function GetTranscriptParams(ByVal paramInfo As ReportParamInfo) As DataTable
    '    Dim sb As New System.Text.StringBuilder
    '    Dim db As New DataAccess
    '    Dim ds As DataSet
    '    Dim dt As DataTable
    '    Dim dr As DataRow

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    If MyAdvAppSettings.AppSettings("CorporateName") = "" Then
    '        '   Retrieve information from syCampuses
    '        With sb
    '            .Append("SELECT ")
    '            .Append("       CampDescrip AS CorporateName,Address1,Address2,City,Zip,")
    '            .Append("       (SELECT StateDescrip FROM syStates WHERE StateId=C.StateId) AS State,")
    '            .Append("       (SELECT CountryDescrip FROM adCountries WHERE CountryId=C.CountryId) AS Country,Fax,")
    '            .Append("       Phone = CASE WHEN (Phone1 IS NULL) THEN (CASE WHEN (Phone2 IS NULL) THEN (CASE WHEN (Phone3 IS NULL) THEN '' ELSE Phone3 END) ELSE Phone2 END) ELSE Phone1 END ")
    '            .Append("FROM   syCampuses C ")
    '            .Append("WHERE  IsCorporate = 1 ")
    '        End With

    '        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '        db.OpenConnection()

    '        ds = db.RunParamSQLDataSet(sb.ToString)
    '        If ds.Tables.Count > 0 Then dt = ds.Tables(0)

    '    Else
    '        '   Build ReportParams table
    '        dt = New DataTable
    '        dt.Columns.Add("CorporateName", System.Type.GetType("System.String"))
    '        dt.Columns.Add("Address1", System.Type.GetType("System.String"))
    '        dt.Columns.Add("Address2", System.Type.GetType("System.String"))
    '        dt.Columns.Add("City", System.Type.GetType("System.String"))
    '        dt.Columns.Add("State", System.Type.GetType("System.String"))
    '        dt.Columns.Add("Zip", System.Type.GetType("System.String"))
    '        dt.Columns.Add("Phone", System.Type.GetType("System.String"))
    '        dt.Columns.Add("Fax", System.Type.GetType("System.String"))
    '        dt.Columns.Add("Country", System.Type.GetType("System.String"))

    '        dr = dt.NewRow
    '        '   Retrieve information from Web.Config
    '        dr("CorporateName") = MyAdvAppSettings.AppSettings("CorporateName").ToUpper
    '        dr("Address1") = MyAdvAppSettings.AppSettings("CorporateAddress1").ToUpper
    '        dr("Address2") = MyAdvAppSettings.AppSettings("CorporateAddress2").ToUpper
    '        dr("City") = MyAdvAppSettings.AppSettings("CorporateCity").ToUpper
    '        dr("State") = MyAdvAppSettings.AppSettings("CorporateState").ToUpper
    '        dr("Zip") = MyAdvAppSettings.AppSettings("CorporateZip").ToUpper
    '        '   Get first phone that it is not blank
    '        If MyAdvAppSettings.AppSettings("CorporatePhone1") <> "" Then
    '            dr("Phone") = MyAdvAppSettings.AppSettings("CorporatePhone1").ToUpper
    '        ElseIf MyAdvAppSettings.AppSettings("CorporatePhone2") <> "" Then
    '            dr("Phone") = MyAdvAppSettings.AppSettings("CorporatePhone2").ToUpper
    '        ElseIf MyAdvAppSettings.AppSettings("CorporatePhone3") <> "" Then
    '            dr("Phone") = MyAdvAppSettings.AppSettings("CorporatePhone3").ToUpper
    '        End If
    '        dr("Fax") = MyAdvAppSettings.AppSettings("CorporateFax").ToUpper
    '        dr("Country") = MyAdvAppSettings.AppSettings("CorporateCountry").ToUpper

    '        dt.Rows.Add(dr)
    '    End If

    '    If Not dt Is Nothing Then
    '        '   Name table and add additional columns
    '        dt.TableName = "ReportParams"
    '        dt.Columns.Add("SchoolLogo", System.Type.GetType("System.Byte[]"))
    '        dt.Columns.Add("StudentIdentifier", System.Type.GetType("System.String"))
    '        dt.Columns.Add("FullAddress", System.Type.GetType("System.String"))
    '        If Not dr Is Nothing Then
    '            dr("SchoolLogo") = paramInfo.SchoolLogo
    '            dr("StudentIdentifier") = MyAdvAppSettings.AppSettings("StudentIdentifier") & ":"
    '        End If
    '    End If

    '    If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    '    Return dt
    'End Function

    'Private Function GetStuEnrollId(ByVal rptparaminfo As ReportParamInfo) As String
    '    'The FilterOther property has the enrollment and term start cutoff 
    '    'The format is like this 'arStuEnrollments.StuEnrollId='xxxx' AND arTerm.StartDate='xxxx'
    '    Dim strArr() As String
    '    Dim strItem As String

    '    strArr = rptparaminfo.FilterOther.ToString.Split("AND")
    '    Return strArr(0)

    'End Function

#End Region
    Public Function GetWorkUnitResults(ByVal strStuEnrollId As String, ByVal reqId As String) As DataTable
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        With sb
            .Append("SELECT ")
            .Append("   GBCR.ReqId,GBCR.TermId,GCT.Descrip,GBCR.Score,GBCR.MinResult, ")
            .Append("   GBCR.Required,GBCR.MustPass, ")
            .Append("   (CASE ")
            .Append("       WHEN Score > MinResult THEN NULL ")
            .Append("       WHEN Score = MinResult THEN 0 ")
            .Append("       WHEN MinResult > Score THEN (MinResult - Score) ")
            .Append("    END) AS Remaining ")
            .Append("FROM arGrdBkConversionResults GBCR, arGrdComponentTypes GCT ")
            .Append("WHERE GBCR.GrdComponentTypeId=GCT.GrdComponentTypeId ")
            .Append("AND GBCR.StuEnrollId=? and ReqId= ? ")
            .Append(" Union ")
            .Append("SELECT ")
            .Append("   ReqId,TermId,Descrip,Score,MinResult,isnull(Required,0) as Required,isnull(MustPass,0) as MustPass , ")
            .Append("   (CASE ")
            .Append("     WHEN Score > MinResult THEN NULL ")
            .Append("     WHEN Score = MinResult THEN 0 ")
            .Append("     WHEN MinResult > Score THEN (MinResult - Score) ")
            .Append("    END) AS Remaining ")
            .Append("   FROM ")
            .Append("   (SELECT ")
            .Append("    CS.ReqId,CS.TermId,GCT.Descrip, ")
            .Append("     (CASE GCT.SysComponentTypeId ")
            .Append("       WHEN 544 THEN (SELECT SUM(HoursAttended) FROM arExternshipAttendance WHERE StuEnrollId= ? ) ")
            .Append("       ELSE GBRS.Score ")
            .Append("       END ")
            .Append("     ) AS Score, ")
            .Append("    (CASE GCT.SysComponentTypeId ")
            .Append("       WHEN 500 THEN GBWD.Number ")
            .Append("       WHEN 503 THEN GBWD.Number ")
            .Append("       ELSE (SELECT MIN(MinVal) ")
            .Append("             FROM arGradeScaleDetails GSD, arGradeSystemDetails GSS ")
            .Append("             WHERE GSD.GrdSysDetailId=GSS.GrdSysDetailId ")
            .Append("             AND GSS.IsPass=1) ")
            .Append("       END ")
            .Append("       )	AS MinResult, ")
            .Append("       GBWD.Required, GBWD.MustPass ")
            .Append("       FROM arGrdBkResults GBRS, arClassSections CS, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT ")
            .Append("       WHERE GBRS.StuEnrollId=? ")
            .Append("       AND GBRS.ClsSectionId=CS.ClsSectionId ")
            .Append("       AND GBRS.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
            .Append("       AND GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId  and ReqId= ? ) P ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()


        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function
    Public Function GetWorkUnitResults_SP(ByVal stuEnrollId As String, ByVal reqId As String) As DataTable
        Dim db As New DataAccess
        Dim ds As  DataSet
        Dim dt As  DataTable

        Try
            db.AddParameter("@StuEnrollID", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ReqId", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet("dbo.USP_AR_GetWorkUnitResults", Nothing, "SP")
            dt = ds.Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.Dispose()
        End Try

        Return dt
    End Function
    Public Function GetWeightSum(ByVal reqId As String) As Integer

        Dim db As New SQLDataAccess

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@reqId", New Guid(reqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        'ds = db.RunParamSQLDataSet_SP("dbo.usp_GetWeightSum")
        Dim dr As SqlDataReader = db.RunParamSQLDataReader_SP("dbo.usp_GetWeightSum")
        Dim rtnWeight As Integer = -1
        While dr.Read()
            '   set properties with data from DataReader
            rtnWeight = dr("WeightCnt")

        End While

        'Close Connection
        db.CloseConnection()

        'return receiptAddress
        Return rtnWeight
    End Function
End Class
