' ===============================================================================
' StudentDB.vb
' DataAccess classes for Students
' ===============================================================================
' Copyright (C) 2006,2007 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports System.Text
Imports System.Data
Imports System.Data.OleDb
Imports FAME.AdvantageV1.Common.AR
Imports FAME.Advantage.Common


Namespace AR

#Region "Student Master"
    Public Class StudentMasterDB
        Public Function AddStudent(ByVal studentinfo As StudentMasterInfo, ByVal user As String, ByVal strObjective As String) As String
            ''Connect To The Database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            'Do an Update
            Try
                'Build The Query
                Dim sb As New StringBuilder


                'If Not studentinfo.SSN = "" Then
                '    With sb
                '        .Append(" Select Count(*) from arStudent where SSN=?")
                '    End With
                '    db.AddParameter("@SSN", studentinfo.SSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                '    Dim intSSNCount As Integer = db.RunParamSQLScalar(sb.ToString)

                '    If intSSNCount >= 1 Then
                '        Return "Student already exists with the same SSN"
                '        Exit Function
                '    End If
                '    db.ClearParameters()
                '    sb.Remove(0, sb.Length)
                'End If

                With sb
                    .Append("Insert into arStudent(StudentId,FirstName, ")
                    .Append(" LastName,MiddleName,SSN,HomeEmail, ")
                    .Append(" WorkEmail, ")
                    .Append(" Prefix,Suffix,DOB,Sponsor,Gender, ")
                    .Append(" Race,MaritalStatus,FamilyIncome,Children, ")
                    .Append(" Nationality,Citizen,DrivLicStateID,DrivLicNumber,AlienNumber,Comments,ModUser,ModDate,AssignedDate,StudentStatus,StudentNumber,Objective,DependencyTypeId,GeographicTypeId,AdminCriteriaId,HousingId) ")
                    .Append(" Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                End With

                'Add Parameters To Query
                db.AddParameter("@StudentId", studentinfo.StudentID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'First Name
                If studentinfo.FirstName = "" Then
                    db.AddParameter("@FirstName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@FirstName", studentinfo.FirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If


                'Last Name
                If studentinfo.LastName = "" Then
                    db.AddParameter("@LastName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@LastName", studentinfo.LastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If


                'MiddleName
                If studentinfo.MiddleName = "" Then
                    db.AddParameter("@MiddleName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@MiddleName", studentinfo.MiddleName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If


                'SSN
                If studentinfo.SSN = "" Then
                    db.AddParameter("@SSN", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@SSN", studentinfo.SSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                'HomeEmail
                If studentinfo.HomeEmail = "" Then
                    db.AddParameter("@HomeEmail", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@HomeEmail", studentinfo.HomeEmail, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                'WorkEmail
                If studentinfo.WorkEmail = "" Then
                    db.AddParameter("@WorkEmail", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@WorkEmail", studentinfo.WorkEmail, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If


                'Prefix
                If studentinfo.Prefix = "" Or studentinfo.Prefix = Guid.Empty.ToString Then
                    db.AddParameter("@Prefix", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Prefix", studentinfo.Prefix, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If


                'Suffix
                If studentinfo.Suffix = "" Or studentinfo.Suffix = Guid.Empty.ToString Then
                    db.AddParameter("@Suffix", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Suffix", studentinfo.Suffix, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If


                'BirthDate
                If studentinfo.BirthDate = "" Then
                    db.AddParameter("@BirthDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@BirthDate", studentinfo.BirthDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'Sponsor
                If studentinfo.Sponsor = "" Or studentinfo.Sponsor = Guid.Empty.ToString Then
                    db.AddParameter("@Sponsor", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Sponsor", studentinfo.Sponsor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If


                'Gender
                If studentinfo.Gender = "" Or studentinfo.Gender = Guid.Empty.ToString Then
                    db.AddParameter("@Gender", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Gender", studentinfo.Gender, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If


                'Race
                If studentinfo.Race = "" Or studentinfo.Race = Guid.Empty.ToString Then
                    db.AddParameter("@Race", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Race", studentinfo.Race, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'MaritalStatus
                If studentinfo.MaritalStatus = "" Or studentinfo.MaritalStatus = Guid.Empty.ToString Then
                    db.AddParameter("@MaritalStatus", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@MaritalStatus", studentinfo.MaritalStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'FamilyIncome
                If String.IsNullOrEmpty(studentinfo.FamilyIncome) Or studentinfo.FamilyIncome = Guid.Empty.ToString() Then
                    db.AddParameter("@FamilyIncome", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@FamilyIncome", studentinfo.FamilyIncome, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'Children
                If studentinfo.Children = "" Then
                    db.AddParameter("@Children", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Children", studentinfo.Children, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'Nationality
                If studentinfo.Nationality = Guid.Empty.ToString Or studentinfo.Nationality = "" Then
                    db.AddParameter("@Nationality", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Nationality", studentinfo.Nationality, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'Citizen
                If studentinfo.Citizen = Guid.Empty.ToString Or studentinfo.Citizen = "" Then
                    db.AddParameter("@Citizen", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Citizen", studentinfo.Citizen, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If


                'DrivLicStateID
                If studentinfo.DriverLicState = Guid.Empty.ToString Or studentinfo.DriverLicState = "" Then
                    db.AddParameter("@DrivLicStateID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@DrivLicStateID", studentinfo.DriverLicState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'DriverLicNumber
                If studentinfo.DriverLicNumber = "" Then
                    db.AddParameter("@DrivLicNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@DrivLicNumber", studentinfo.DriverLicNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'AlienNumber
                If studentinfo.AlienNumber = "" Then
                    db.AddParameter("@AlienNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@AlienNumber", studentinfo.AlienNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'Comments
                If studentinfo.Notes = "" Then
                    db.AddParameter("@Comments", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
                Else
                    db.AddParameter("@Comments", studentinfo.Notes, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
                End If

                ''ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                ''ModDate
                db.AddParameter("@ModDate", studentinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                'Assigned Date
                If studentinfo.AssignmentDate = "" Then
                    db.AddParameter("@AssignedDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@AssignedDate", studentinfo.AssignmentDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'Student Status
                If studentinfo.Status = "" Or studentinfo.Status = Guid.Empty.ToString Then
                    db.AddParameter("@StudentStatus", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@StudentStatus", studentinfo.Status, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If



                'StudentNumber
                If studentinfo.StudentNumber = "" Then
                    db.AddParameter("@StudentNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@StudentNumber", studentinfo.StudentNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                'Resume Objective
                If strObjective = "" Then
                    db.AddParameter("@Objective", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
                Else
                    db.AddParameter("@Objective", strObjective, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
                End If

                'DependencyTypeId
                If studentinfo.DependencyTypeId = "" Then
                    db.AddParameter("@DependencyTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@DependencyTypeId", studentinfo.DependencyTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'GeographicTypeId
                If studentinfo.GeographicTypeId = "" Then
                    db.AddParameter("@GeographicTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@GeographicTypeId", studentinfo.GeographicTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'AdminCriteriaId
                If studentinfo.AdminCriteriaId = "" Then
                    db.AddParameter("@AdminCriteriaId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@AdminCriteriaId", studentinfo.AdminCriteriaId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'HousingId
                If studentinfo.HousingId = "" Then
                    db.AddParameter("@HousingId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@HousingId", studentinfo.HousingId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If


                'Execute The Query
                Try
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Catch ex As OleDbException
                    db.CloseConnection()
                    Return DALExceptions.BuildErrorMessage(ex)
                Finally
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)

                End Try

                Dim intForeignZip As Integer
                Dim intForeignPhone As Integer

                If studentinfo.ForeignZip = 1 Then
                    intForeignZip = 1
                Else
                    intForeignZip = 0
                End If

                If studentinfo.ForeignPhone = 1 Then
                    intForeignPhone = 1
                Else
                    intForeignPhone = 0
                End If

                Dim sb4 As New StringBuilder
                With sb4
                    .Append(" Insert into arStudAddresses(StudentId,Address1,Address2,")
                    .Append(" City,StateId,Zip,CountryId,AddressTypeId,ModUser,ModDate,StatusId,default1,ForeignZip,OtherState) ")
                    .Append(" values (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                End With

                'Add Parameters To Query
                db.AddParameter("@StudentId", studentinfo.StudentID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'Address
                If studentinfo.Address1 = "" Then
                    db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@Address1", studentinfo.Address1, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                'Address2
                If studentinfo.Address2 = "" Then
                    db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@Address2", studentinfo.Address2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                'city
                If studentinfo.City = "" Then
                    db.AddParameter("@city", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@city", studentinfo.City, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                'StateId
                If studentinfo.State = "" Then
                    db.AddParameter("@State", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@State", studentinfo.State, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                'Zip
                If studentinfo.Zip = "" Then
                    db.AddParameter("@zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@zip", studentinfo.Zip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                'Country
                If studentinfo.Country = "" Or studentinfo.Country = Guid.Empty.ToString Then
                    db.AddParameter("@Country", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Country", studentinfo.Country, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'AddressType
                If studentinfo.AddressType = "" Or studentinfo.AddressType = Guid.Empty.ToString Then
                    db.AddParameter("@AddressTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@AddressTypeId", studentinfo.AddressType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@StatusId", studentinfo.Status, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@default1", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ForeignZip", intForeignZip, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                'Other State
                If studentinfo.OtherState = "" Then
                    db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@OtherState", studentinfo.OtherState, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                'If Country is Domestic
                If intForeignZip = 0 Then
                    If Not studentinfo.Address1 = "" Or Not studentinfo.Address2 = "" Or Not studentinfo.City = "" Or Not studentinfo.Country = Guid.Empty.ToString Or Not studentinfo.State = Guid.Empty.ToString Or Not studentinfo.Zip = "" Or Not studentinfo.AddressStatus = Guid.Empty.ToString Or Not studentinfo.AddressType = Guid.Empty.ToString Then
                        db.RunParamSQLExecuteNoneQuery(sb4.ToString)
                    End If
                End If

                'If Country is International
                If intForeignZip = 1 Then
                    If Not studentinfo.Address1 = "" Or Not studentinfo.Address2 = "" Or Not studentinfo.City = "" Or Not studentinfo.Country = Guid.Empty.ToString Or Not studentinfo.OtherState = "" Or Not studentinfo.Zip = "" Or Not studentinfo.AddressStatus = Guid.Empty.ToString Or Not studentinfo.AddressType = Guid.Empty.ToString Then
                        db.RunParamSQLExecuteNoneQuery(sb4.ToString)
                    End If
                End If
                db.ClearParameters()
                sb4.Remove(0, sb4.Length)


                Dim sb5 As New StringBuilder
                With sb5
                    .Append(" Insert into arStudentPhone(StudentId,PhoneTypeId,Phone,")
                    .Append(" StatusId,ModUser,ModDate,default1,ForeignPhone) ")
                    .Append(" values(?,?,?,?,?,?,?,?) ")
                End With
                db.AddParameter("@StudentId", studentinfo.StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                'PhoneType
                If studentinfo.PhoneType = "" Or studentinfo.PhoneType = Guid.Empty.ToString Then
                    db.AddParameter("@PhoneType", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@PhoneType", studentinfo.PhoneType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'HomePhone
                If studentinfo.Phone = "" Then
                    db.AddParameter("@HomePhone", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@HomePhone", studentinfo.Phone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If


                db.AddParameter("@StatusId", studentinfo.Status, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@default1", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ForeignPhone", intForeignPhone, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                If Not studentinfo.Phone = "" Then
                    db.RunParamSQLExecuteNoneQuery(sb5.ToString)
                End If
                db.ClearParameters()
                sb5.Remove(0, sb5.Length)

                Return ""
            Catch ex As OleDbException
                '   return an error to the client
                Return ex.Message
            Finally
                'Close Connection
                db.CloseConnection()
            End Try
        End Function
        Public Function UpdateStudent(ByVal studentinfo As StudentMasterInfo, ByVal user As String, ByVal strObjective As String) As String
            ''Connect To The Database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            'Do an Update

            Dim intForeignZip As Integer
            Dim intForeignPhone As Integer

            If studentinfo.ForeignZip = 1 Then
                intForeignZip = 1
            Else
                intForeignZip = 0
            End If

            If studentinfo.ForeignPhone = 1 Then
                intForeignPhone = 1
            Else
                intForeignPhone = 0
            End If



            Try

                'Build The Query
                Dim sb As New StringBuilder
                'If Not studentinfo.SSN = "" Then
                '    With sb
                '        .Append(" Select Count(*) from arStudent where SSN=? ")
                '    End With
                '    db.AddParameter("@SSN", studentinfo.SSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                '    Dim intSSNCount As Integer = db.RunParamSQLScalar(sb.ToString)

                '    If intSSNCount >= 1 Then
                '        Return "Student already exists with the same SSN"
                '        Exit Function
                '    End If
                '    db.ClearParameters()
                '    sb.Remove(0, sb.Length)
                'End If

                With sb
                    .Append("Update arStudent set FirstName=?, ")
                    .Append(" LastName=?,MiddleName=?,SSN=?,HomeEmail=?, ")
                    .Append(" WorkEmail=?, ")
                    .Append(" Prefix=?,Suffix=?,DOB=?,Sponsor=?,Gender=?, ")
                    .Append(" Race=?,MaritalStatus=?,FamilyIncome=?,Children=?, ")
                    .Append(" Nationality=?,Citizen=?,DrivLicStateID=?,DrivLicNumber=?,AlienNumber=?,Comments=?,ModUser=?,ModDate=?,AssignedDate=?,StudentStatus=?,StudentNumber=?,Objective=?,")
                    .Append(" DependencyTypeId=?,GeographicTypeId=?,AdminCriteriaId=?,HousingId=? ")
                    .Append(" where StudentID = ? ")
                End With

                'First Name
                If studentinfo.FirstName = "" Then
                    db.AddParameter("@FirstName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@FirstName", studentinfo.FirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If


                'Last Name
                If studentinfo.LastName = "" Then
                    db.AddParameter("@LastName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@LastName", studentinfo.LastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If


                'MiddleName
                If studentinfo.MiddleName = "" Then
                    db.AddParameter("@MiddleName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@MiddleName", studentinfo.MiddleName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If


                'SSN
                If studentinfo.SSN = "" Then
                    db.AddParameter("@SSN", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@SSN", studentinfo.SSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If


                'HomeEmail
                If studentinfo.HomeEmail = "" Then
                    db.AddParameter("@HomeEmail", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@HomeEmail", studentinfo.HomeEmail, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If


                'WorkEmail
                If studentinfo.WorkEmail = "" Then
                    db.AddParameter("@WorkEmail", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@WorkEmail", studentinfo.WorkEmail, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If


                'Prefix
                If studentinfo.Prefix = "" Or studentinfo.Prefix = Guid.Empty.ToString Then
                    db.AddParameter("@Prefix", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Prefix", studentinfo.Prefix, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If


                'Suffix
                If studentinfo.Suffix = "" Or studentinfo.Suffix = Guid.Empty.ToString Then
                    db.AddParameter("@Suffix", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Suffix", studentinfo.Suffix, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If


                'BirthDate
                If studentinfo.BirthDate = "" Then
                    db.AddParameter("@BirthDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@BirthDate", studentinfo.BirthDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'Sponsor
                If studentinfo.Sponsor = "" Or studentinfo.Sponsor = Guid.Empty.ToString Then
                    db.AddParameter("@Sponsor", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Sponsor", studentinfo.Sponsor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'Gender
                If studentinfo.Gender = "" Or studentinfo.Gender = Guid.Empty.ToString Then
                    db.AddParameter("@Gender", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Gender", studentinfo.Gender, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'Race
                If studentinfo.Race = "" Or studentinfo.Race = Guid.Empty.ToString Then
                    db.AddParameter("@Race", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Race", studentinfo.Race, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'MaritalStatus
                If studentinfo.MaritalStatus = "" Or studentinfo.MaritalStatus = Guid.Empty.ToString Then
                    db.AddParameter("@MaritalStatus", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@MaritalStatus", studentinfo.MaritalStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'FamilyIncome
                If String.IsNullOrEmpty(studentinfo.FamilyIncome) Or studentinfo.FamilyIncome = Guid.Empty.ToString() Then
                    db.AddParameter("@FamilyIncome", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@FamilyIncome", studentinfo.FamilyIncome, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'Children
                If studentinfo.Children = "" Then
                    db.AddParameter("@Children", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Children", studentinfo.Children, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'Nationality
                If studentinfo.Nationality = Guid.Empty.ToString Or studentinfo.Nationality = "" Then
                    db.AddParameter("@Nationality", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Nationality", studentinfo.Nationality, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'Citizen
                If studentinfo.Citizen = Guid.Empty.ToString Or studentinfo.Citizen = "" Then
                    db.AddParameter("@Citizen", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Citizen", studentinfo.Citizen, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If


                'DrivLicStateID
                If studentinfo.DriverLicState = Guid.Empty.ToString Or studentinfo.DriverLicState = "" Then
                    db.AddParameter("@DrivLicStateID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@DrivLicStateID", studentinfo.DriverLicState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'DriverLicNumber
                If studentinfo.DriverLicNumber = "" Then
                    db.AddParameter("@DrivLicNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@DrivLicNumber", studentinfo.DriverLicNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'AlienNumber
                If studentinfo.AlienNumber = "" Then
                    db.AddParameter("@AlienNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@AlienNumber", studentinfo.AlienNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'Comments
                If studentinfo.Notes = "" Then
                    db.AddParameter("@Comments", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
                Else
                    db.AddParameter("@Comments", studentinfo.Notes, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
                End If

                'ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'ModDate
                db.AddParameter("@ModDate", studentinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                'Assigned Date
                If studentinfo.AssignmentDate = "" Then
                    db.AddParameter("@AssignedDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@AssignedDate", studentinfo.AssignmentDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If


                'Student Status
                If studentinfo.Status = "" Or studentinfo.Status = Guid.Empty.ToString Then
                    db.AddParameter("@StudentStatus", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@StudentStatus", studentinfo.Status, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'StudentNumber
                If studentinfo.StudentNumber = "" Then
                    db.AddParameter("@StudentNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@StudentNumber", studentinfo.StudentNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                'Resume Objective
                If strObjective = "" Then
                    db.AddParameter("@Objective", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
                Else
                    db.AddParameter("@Objective", strObjective, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
                End If

                'DependencyTypeId
                If studentinfo.DependencyTypeId = "" Then
                    db.AddParameter("@DependencyTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@DependencyTypeId", studentinfo.DependencyTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'GeographicTypeId
                If studentinfo.GeographicTypeId = "" Then
                    db.AddParameter("@GeographicTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@GeographicTypeId", studentinfo.GeographicTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'AdminCriteriaId
                If studentinfo.AdminCriteriaId = "" Then
                    db.AddParameter("@AdminCriteriaId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@AdminCriteriaId", studentinfo.AdminCriteriaId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'HousingId
                If studentinfo.HousingId = "" Then
                    db.AddParameter("@HousingId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@HousingId", studentinfo.HousingId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                'StudentId
                db.AddParameter("@StudentId", studentinfo.StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                'Execute The Query
                Try
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)
                Catch ex As OleDbException
                    Return DALExceptions.BuildErrorMessage(ex)
                End Try

                'Get The Primary Key Value From arStudentPhone Table(Only Default Phone)
                Dim sb1 As New StringBuilder
                With sb1
                    .Append("select StudentPhoneId from arSTudentPhone where StudentId=? and Default1=1 ")
                End With
                'StudentId
                db.AddParameter("@StudentId", studentinfo.StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                'Execute the query
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb1.ToString)
                Dim strPhoneId As String = ""
                While dr.Read()
                    'Get Default PhoneValue
                    If Not (dr("StudentPhoneId") Is System.DBNull.Value) Then strPhoneId = CType(dr("StudentPhoneId"), Guid).ToString Else strPhoneId = ""
                End While

                If Not dr.IsClosed Then dr.Close()

                db.ClearParameters()
                sb1.Remove(0, sb1.Length)

                'If Default Phone Exist for Student, update phone else insert phone
                If Not strPhoneId = "" Then
                    Dim sb5 As New StringBuilder
                    With sb5
                        .Append(" update arStudentPhone set StudentId=?,PhoneTypeId = ?,Phone=?,StatusId=?,Moduser=?,ModDate=?,ForeignPhone=?")
                        .Append(" where StudentPhoneId = ? ")
                    End With

                    db.AddParameter("@StudentId", studentinfo.StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                    If studentinfo.PhoneType = "" Or studentinfo.PhoneType = Guid.Empty.ToString Then
                        db.AddParameter("@PhoneTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@PhoneTypeId", studentinfo.PhoneType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    If studentinfo.Phone = "" Then
                        db.AddParameter("@Phone", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@Phone", studentinfo.Phone, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    If studentinfo.PhoneStatus = "" Or studentinfo.PhoneStatus = Guid.Empty.ToString Then
                        db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@StatusId", studentinfo.PhoneStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@ForeignPhone", intForeignPhone, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    db.AddParameter("@StudentPhoneId", strPhoneId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.RunParamSQLExecuteNoneQuery(sb5.ToString)
                    db.ClearParameters()
                    sb5.Remove(0, sb5.Length)
                Else
                    Dim sb51 As New StringBuilder
                    With sb51
                        .Append(" insert into arStudentPhone(StudentId,PhoneTypeId,Phone,StatusId,Moduser,ModDate,ForeignPhone,default1) ")
                        .Append(" values(?,?,?,?,?,?,?,?) ")
                    End With

                    db.AddParameter("@StudentId", studentinfo.StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                    If studentinfo.PhoneType = "" Or studentinfo.PhoneType = Guid.Empty.ToString Then
                        db.AddParameter("@PhoneTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@PhoneTypeId", studentinfo.PhoneType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    If studentinfo.Phone = "" Then
                        db.AddParameter("@Phone", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@Phone", studentinfo.Phone, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    If studentinfo.PhoneStatus = "" Or studentinfo.PhoneStatus = Guid.Empty.ToString Then
                        db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@StatusId", studentinfo.PhoneStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@ForeignPhone", intForeignPhone, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    db.AddParameter("@default1", 1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'Insert Only If Phone is not empty
                    If Not studentinfo.Phone = "" Then
                        db.RunParamSQLExecuteNoneQuery(sb51.ToString)
                    End If

                    db.ClearParameters()
                    sb51.Remove(0, sb51.Length)
                End If

                'Get The Primary Key Value From arStudentPhone Table(Only Default Phone)
                Dim sb61 As New StringBuilder
                With sb61
                    .Append("select StdAddressId from arStudAddresses where StudentId=? and Default1=1 ")
                End With
                'StudentId
                db.AddParameter("@StudentId", studentinfo.StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                'Execute the query
                Dim drAddress As OleDbDataReader = db.RunParamSQLDataReader(sb61.ToString)
                Dim strAddressId As String = ""
                While drAddress.Read()
                    'Get Default PhoneValue
                    If Not (drAddress("StdAddressId") Is System.DBNull.Value) Then strAddressId = CType(drAddress("StdAddressId"), Guid).ToString Else strAddressId = ""
                End While

                If Not drAddress.IsClosed Then drAddress.Close()

                db.ClearParameters()
                sb61.Remove(0, sb61.Length)

                'If Default Phone Exist for Student, update phone else insert phone
                If Not strAddressId = "" Then
                    Dim sb59 As New StringBuilder
                    With sb59
                        .Append(" update arStudAddresses set StudentId=?,Address1 = ?,Address2=?,City=?,StateId=?,Zip=?,CountryId=?,AddressTypeId=?, ")
                        .Append(" OtherState=?,ForeignZip=?,Moduser=?,ModDate=?,StatusId=? ")
                        .Append(" where StdAddressId = ? ")
                    End With

                    db.AddParameter("@StudentId", studentinfo.StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                    'Address1
                    If studentinfo.Address1 = "" Then
                        db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@Address1", studentinfo.Address1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    'Address2
                    If studentinfo.Address2 = "" Then
                        db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@Address2", studentinfo.Address2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    'City
                    If studentinfo.City = "" Then
                        db.AddParameter("@City", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@City", studentinfo.City, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    'StateId
                    If studentinfo.State = "" Or studentinfo.State = Guid.Empty.ToString Then
                        db.AddParameter("@State", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@State", studentinfo.State, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    'Zip
                    If studentinfo.Zip = "" Then
                        db.AddParameter("@Zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@Zip", studentinfo.Zip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    'CountryId
                    If studentinfo.Country = Guid.Empty.ToString Or studentinfo.Country = "" Then
                        db.AddParameter("@Country", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@Country", studentinfo.Country, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    'AddressTypeId
                    If studentinfo.AddressType = Guid.Empty.ToString Or studentinfo.AddressType = "" Then
                        db.AddParameter("@AddressType", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@AddressType", studentinfo.AddressType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    'OtherState
                    If studentinfo.OtherState = "" Then
                        db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@OtherState", studentinfo.OtherState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    'ForeignZip
                    db.AddParameter("@ForeignZip", intForeignZip, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                    'ModUser
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                    'ModDate
                    db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                    'AddressStatus
                    If studentinfo.AddressStatus = "" Or studentinfo.AddressStatus = Guid.Empty.ToString Then
                        db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@StatusId", studentinfo.AddressStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    'StdAddressId
                    db.AddParameter("@StudAddressId", strAddressId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.RunParamSQLExecuteNoneQuery(sb59.ToString)
                    db.ClearParameters()
                    sb59.Remove(0, sb59.Length)
                Else
                    Dim sb52 As New StringBuilder
                    With sb52
                        .Append(" insert into arStudAddresses(StudentId,Address1,Address2,City,StateId,Zip,CountryId,AddressTypeId,OtherState,ForeignZip,Moduser,ModDate,Default1,StatusId) ")
                        .Append(" values(?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                    End With

                    'StudentId 
                    db.AddParameter("@StudentId", studentinfo.StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                    'Address1
                    If studentinfo.Address1 = "" Then
                        db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@Address1", studentinfo.Address1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    'Address2
                    If studentinfo.Address2 = "" Then
                        db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@Address2", studentinfo.Address2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    'City
                    If studentinfo.City = "" Then
                        db.AddParameter("@City", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@City", studentinfo.City, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    'StateId
                    If studentinfo.State = "" Or studentinfo.State = Guid.Empty.ToString Then
                        db.AddParameter("@State", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@State", studentinfo.State, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    'Zip
                    If studentinfo.Zip = "" Then
                        db.AddParameter("@Zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@Zip", studentinfo.Zip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    'CountryId
                    If studentinfo.Country = Guid.Empty.ToString Or studentinfo.Country = "" Then
                        db.AddParameter("@Country", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@Country", studentinfo.Country, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    'AddressTypeId
                    If studentinfo.AddressType = Guid.Empty.ToString Or studentinfo.AddressType = "" Then
                        db.AddParameter("@AddressType", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@AddressType", studentinfo.AddressType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    'OtherState
                    If studentinfo.OtherState = "" Then
                        db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@OtherState", studentinfo.OtherState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    'ForeignZip
                    db.AddParameter("@ForeignZip", intForeignZip, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                    'ModUser
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                    'ModDate
                    db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                    'StdAddressId
                    db.AddParameter("@default1", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                    'AddressStatus
                    If studentinfo.AddressStatus = "" Or studentinfo.AddressStatus = Guid.Empty.ToString Then
                        db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@StatusId", studentinfo.AddressStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    'If Country is Domestic
                    If intForeignZip = 0 Then
                        If Not studentinfo.Address1 = "" Or Not studentinfo.Address2 = "" Or Not studentinfo.City = "" Or Not studentinfo.Country = Guid.Empty.ToString Or Not studentinfo.State = "" Or Not studentinfo.Zip = "" Or Not studentinfo.AddressStatus = Guid.Empty.ToString Or Not studentinfo.AddressType = Guid.Empty.ToString Then
                            db.RunParamSQLExecuteNoneQuery(sb52.ToString)
                        End If
                    End If

                    'If Country is International
                    If intForeignZip = 1 Then
                        If Not studentinfo.Address1 = "" Or Not studentinfo.Address2 = "" Or Not studentinfo.City = "" Or Not studentinfo.Country = Guid.Empty.ToString Or Not studentinfo.OtherState = "" Or Not studentinfo.Zip = "" Or Not studentinfo.AddressStatus = Guid.Empty.ToString Or Not studentinfo.AddressType = Guid.Empty.ToString Then
                            db.RunParamSQLExecuteNoneQuery(sb52.ToString)
                        End If
                    End If
                    db.ClearParameters()
                    sb52.Remove(0, sb52.Length)
                End If

                studentinfo.IsInDB = True
                Return ""
            Catch ex As OleDbException
                '   return an error to the client
                Return ex.Message
            Finally
                'Close Connection
                db.CloseConnection()
            End Try
        End Function
        Public Function DeleteStudent(ByVal StudentId As String, ByVal modDate As DateTime) As String
            'Connect To The Database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            'Do an Update
            'Try
            'Build The Query
            Dim sb As New StringBuilder
            Try
                With sb
                    .Append("Delete from arStudent where StudentId = ? ")
                    .Append(" AND ModDate = ? ;")
                    .Append("SELECT count(*) FROM arStudent WHERE StudentId = ? ")
                End With

                'StudentId
                db.AddParameter("@StudentID", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                'ModDate
                db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                'StudentId
                db.AddParameter("@StudentID", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                'execute the query
                Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

                'If the row was not deleted then there was a concurrency problem
                If rowCount = 0 Then
                    '   return without errors
                    Return ""
                Else
                    Return DALExceptions.BuildConcurrencyExceptionMessage()
                End If

            Catch ex As OleDbException
                '   return an error to the client
                Return DALExceptions.BuildErrorMessage(ex)
            Finally
                'Close Connection
                db.CloseConnection()
            End Try
        End Function
        Public Function GetStudentInfo(ByVal StudentId As String) As StudentMasterInfo
            'connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            'build the sql query
            Dim sb As New StringBuilder
            With sb
                'With subqueries
                .Append(" Select A.StudentId, ")
                .Append(" A.FirstName,A.LastName,A.MiddleName,A.SSN,A.HomeEmail, ")
                .Append(" (Select Address1 from arStudAddresses where StudentId=A.StudentId and default1=1) as Address1, ")
                .Append(" (Select Address2 from arStudAddresses where StudentId=A.StudentId and default1=1) as Address2, ")
                .Append(" (Select city from arStudAddresses where StudentId=A.StudentId and default1=1) as City, ")
                .Append(" (Select StateId from arStudAddresses where StudentId=A.StudentId and default1=1) as State, ")
                .Append(" (Select c.StateDescrip from arStudAddresses b, syStates c where b.StateId = c.StateId and b.StudentId = A.StudentId and b.default1 = 1) as StateDescrip, ")
                .Append(" (Select Zip from arStudAddresses where StudentId=A.StudentId and default1=1) as Zip, ")
                .Append(" (Select AddressTypeId from arStudAddresses where StudentId=A.StudentId and default1=1) as AddressType, ")
                .Append(" (select Distinct c.AddressDescrip from arStudAddresses b,plAddressTypes c where b.AddressTypeId=c.AddressTypeId and b.StudentId = A.StudentId and b.default1 = 1) as AddressTypeDescrip, ")
                .Append(" (Select CountryId from arStudAddresses where StudentId=A.StudentId and default1=1) as Country, ")
                .Append(" (select Distinct c.CountryDescrip from arStudAddresses b, adCountries c where b.CountryId=c.CountryId and b.StudentId = A.StudentId and b.default1 = 1) as CountryDescrip, ")
                .Append(" (Select PhoneTypeId from arStudentPhone where StudentId=A.StudentId and default1=1) as PhoneType, ")
                .Append(" (select Distinct c.PhoneTypeDescrip from arStudentPhone b, syPhoneType c where b.PhoneTypeId=c.PhoneTypeId and b.StudentId = A.StudentId and b.default1 = 1) as PhoneTypeDescrip, ")
                .Append(" (Select Phone from arStudentPhone where StudentId=A.StudentId and default1=1) as Phone, ")
                .Append(" (Select StatusId from arStudAddresses where StudentId=A.StudentId and default1=1) as AddressStatus, ")
                .Append(" (select Distinct c.Status from arStudAddresses b, syStatuses c where b.StatusId=c.StatusId and b.StudentId = A.StudentId and b.default1 = 1) as AddressStatusDescrip, ")
                .Append(" (Select StatusId from arStudentPhone where StudentId=A.StudentId and default1=1) as PhoneStatus, ")
                .Append(" (select Distinct c.Status from arStudentPhone b, syStatuses c where b.StatusId=c.StatusId and b.StudentId = A.StudentId and b.default1 = 1) as PhoneStatusDescrip, ")
                .Append(" (select ForeignPhone from arStudentPhone where StudentId=A.StudentId and default1=1) as ForeignPhone, ")
                .Append(" (select ForeignZip from arStudAddresses where StudentId=A.StudentId and default1=1) as ForeignZip, ")
                .Append(" (select OtherState from arStudAddresses where StudentId=A.StudentId and default1=1) as OtherState, ")
                .Append(" (select LeadId from arStuEnrollments where StudentId=A.StudentId and LeadId is not null) as LeadId, ")
                .Append(" A.StudentStatus, ")
                .Append(" (select Distinct Status from syStatuses where StatusId=A.StudentStatus) as StudentStatusDescrip, ")
                .Append(" A.WorkEmail, A.Prefix, A.Suffix, ")
                .Append(" (select Distinct PrefixDescrip from syPrefixes where PrefixId=A.Prefix) as PrefixDescrip, ")
                .Append(" (select Distinct SuffixDescrip from sySuffixes where SuffixId=A.Suffix) as SuffixDescrip, ")
                .Append(" A.DOB,A.Sponsor,A.AssignedDate, ")
                .Append(" (select Distinct AgencySpDescrip from adAgencySponsors where AgencySpId=A.Sponsor) as SponsorTypeDescrip, ")
                .Append(" A.Gender,A.Race,A.MaritalStatus,A.FamilyIncome,A.Children, ")
                .Append(" A.ExpectedStart,A.ShiftID, ")
                .Append(" A.Nationality,A.Citizen,A.DrivLicStateID,A.DrivLicNumber,A.AlienNumber, ")
                .Append(" A.Comments,A.StudentNumber,A.Objective,A.ModDate, ")
                .Append(" A.DependencyTypeId,A.GeographicTypeId,A.AdminCriteriaId,A.HousingId, ")
                .Append(" (select Distinct GenderDescrip from adGenders where GenderId=A.Gender) as GenderDescrip, ")
                .Append(" (select Distinct EthCodeDescrip from adEthCodes where EthCodeId=A.Race) as EthCodeDescrip, ")
                .Append(" (select Distinct MaritalStatDescrip from adMaritalStatus where MaritalStatId=A.MaritalStatus) as MaritalStatusDescrip, ")
                .Append(" (select Distinct FamilyIncomeDescrip from syFamilyIncome where FamilyIncomeId=A.FamilyIncome) as FamilyIncomeDescrip, ")
                .Append(" (select Distinct NationalityDescrip from adNationalities where NationalityId=A.Nationality) as NationalityDescrip, ")
                .Append(" (select Distinct CitizenShipDescrip from adCitizenships where CitizenshipId=A.Citizen) as CitizenDescrip, ")
                .Append(" (select Distinct StateDescrip from syStates where StateId=A.DrivLicStateId) as DriverLicenseStateDescrip, ")
                .Append(" (select Distinct Descrip from adDependencyTypes where DependencyTypeId=A.DependencyTypeId) as DependencyTypeDescrip, ")
                .Append(" (select Distinct Descrip from adGeographicTypes where GeographicTypeId=A.GeographicTypeId) as GeographicTypeDescrip, ")
                .Append(" (select Distinct Descrip from adAdminCriteria where AdminCriteriaId=A.AdminCriteriaId) as AdminCriteriaDescrip, ")
                .Append(" (select Distinct Descrip from arHousing where HousingId=A.HousingId) as HousingDescrip ")
                .Append(" from arStudent A ")
                .Append(" where A.StudentId = ? ")
            End With

            'Add the EmployerContactId the parameter list
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            Dim studentInfo As New StudentMasterInfo
            While dr.Read()
                'set properties with data from DataReader
                With studentInfo
                    .IsInDB = True
                    .StudentID = CType(dr("StudentId"), Guid).ToString()
                    If Not (dr("FirstName") Is System.DBNull.Value) Then .FirstName = CType(dr("FirstName"), String).ToString Else .FirstName = ""
                    If Not (dr("LastName") Is System.DBNull.Value) Then .LastName = CType(dr("LastName"), String).ToString Else .LastName = ""
                    If Not (dr("MiddleName") Is System.DBNull.Value) Then .MiddleName = CType(dr("MiddleName"), String).ToString Else .MiddleName = ""
                    If Not (dr("SSN") Is System.DBNull.Value) Then .SSN = CType(dr("SSN"), String).ToString Else .SSN = ""
                    If Not (dr("Phone") Is System.DBNull.Value) Then .Phone = CType(dr("Phone"), String).ToString Else .Phone = ""
                    If Not (dr("HomeEmail") Is System.DBNull.Value) Then .HomeEmail = CType(dr("HomeEmail"), String).ToString Else .HomeEmail = ""
                    If Not (dr("Address1") Is System.DBNull.Value) Then .Address1 = CType(dr("Address1"), String).ToString Else .Address1 = ""
                    If Not (dr("Address2") Is System.DBNull.Value) Then .Address2 = CType(dr("Address2"), String).ToString Else .Address2 = ""
                    If Not (dr("City") Is System.DBNull.Value) Then .City = CType(dr("City"), String).ToString Else .City = ""
                    If Not (dr("state") Is System.DBNull.Value) Then .State = CType(dr("State"), Guid).ToString Else .State = Guid.Empty.ToString
                    If Not (dr("LeadId") Is System.DBNull.Value) Then .LeadId = CType(dr("LeadId"), Guid).ToString Else .LeadId = ""

                    If Not (dr("zip") Is System.DBNull.Value) Then .Zip = CType(dr("zip"), String).ToString Else .Zip = ""

                    If Not (dr("Country") Is System.DBNull.Value) Then .Country = CType(dr("Country"), Guid).ToString Else .Country = Guid.Empty.ToString

                    If Not (dr("DOB") Is System.DBNull.Value) Then .BirthDate = CType(dr("DOB"), String).ToString Else .BirthDate = ""
                    If Not (dr("AssignedDate") Is System.DBNull.Value) Then .AssignmentDate = CType(dr("AssignedDate"), String).ToString Else .AssignmentDate = ""
                    If Not (dr("ExpectedStart") Is System.DBNull.Value) Then .ExpectedStart = CType(dr("ExpectedStart"), String).ToString Else .ExpectedStart = ""
                    If Not (dr("Comments") Is System.DBNull.Value) Then .Notes = CType(dr("Comments"), String).ToString Else .Notes = ""
                    If Not (dr("Children") Is System.DBNull.Value) Then .Children = CType(dr("Children"), String).ToString Else .Children = ""

                    If Not (dr("WorkEmail") Is System.DBNull.Value) Then .WorkEmail = CType(dr("WorkEmail"), String).ToString Else .WorkEmail = ""
                    If Not (dr("StudentStatus") Is System.DBNull.Value) Then .Status = CType(dr("StudentStatus"), Guid).ToString Else .Status = Guid.Empty.ToString
                    If Not (dr("AddressType") Is System.DBNull.Value) Then .AddressType = CType(dr("AddressType"), Guid).ToString Else .AddressType = Guid.Empty.ToString
                    If Not (dr("Prefix") Is System.DBNull.Value) Then .Prefix = CType(dr("Prefix"), Guid).ToString Else .Prefix = Guid.Empty.ToString
                    If Not (dr("Suffix") Is System.DBNull.Value) Then .Suffix = CType(dr("Suffix"), Guid).ToString Else .Suffix = Guid.Empty.ToString
                    If Not (dr("Sponsor") Is System.DBNull.Value) Then .Sponsor = CType(dr("Sponsor"), Guid).ToString Else .Sponsor = Guid.Empty.ToString
                    'If Not (dr("AdmissionsRep") Is System.DBNull.Value) Then .AdmissionsRep = CType(dr("AdmissionsRep"), Guid).ToString Else 
                    .AdmissionsRep = Guid.Empty.ToString
                    If Not (dr("Gender") Is System.DBNull.Value) Then .Gender = CType(dr("Gender"), Guid).ToString Else .Gender = Guid.Empty.ToString
                    If Not (dr("Race") Is System.DBNull.Value) Then .Race = CType(dr("Race"), Guid).ToString Else .Race = Guid.Empty.ToString
                    If Not (dr("MaritalStatus") Is System.DBNull.Value) Then .MaritalStatus = CType(dr("MaritalStatus"), Guid).ToString Else .MaritalStatus = Guid.Empty.ToString
                    If Not (dr("FamilyIncome") Is System.DBNull.Value) Then .FamilyIncome = CType(dr("FamilyIncome"), Guid).ToString Else .FamilyIncome = Guid.Empty.ToString
                    If Not (dr("PhoneType") Is System.DBNull.Value) Then .PhoneType = CType(dr("PhoneType"), Guid).ToString Else .PhoneType = Guid.Empty.ToString
                    If Not (dr("PhoneStatus") Is System.DBNull.Value) Then .PhoneStatus = CType(dr("PhoneStatus"), Guid).ToString Else .PhoneStatus = Guid.Empty.ToString
                    'If Not (dr("PrgVerId") Is System.DBNull.Value) Then .PrgVerId = CType(dr("PrgVerId"), Guid).ToString Else 
                    .PrgVerId = Guid.Empty.ToString
                    If Not (dr("ShiftId") Is System.DBNull.Value) Then .ShiftID = CType(dr("ShiftId"), Guid).ToString Else .ShiftID = Guid.Empty.ToString
                    If Not (dr("Nationality") Is System.DBNull.Value) Then .Nationality = CType(dr("Nationality"), Guid).ToString Else .Nationality = Guid.Empty.ToString
                    If Not (dr("Citizen") Is System.DBNull.Value) Then .Citizen = CType(dr("Citizen"), Guid).ToString Else .Citizen = Guid.Empty.ToString
                    If Not (dr("DrivLicStateId") Is System.DBNull.Value) Then .DriverLicState = CType(dr("DrivLicStateId"), Guid).ToString Else .DriverLicState = Guid.Empty.ToString
                    If Not (dr("DrivLicNumber") Is System.DBNull.Value) Then .DriverLicNumber = CType(dr("DrivLicNumber"), String).ToString Else .DriverLicNumber = ""
                    If Not (dr("AlienNumber") Is System.DBNull.Value) Then .AlienNumber = CType(dr("AlienNumber"), String).ToString Else .AlienNumber = ""


                    If Not (dr("AddressType") Is System.DBNull.Value) Then .AddressType = CType(dr("AddressType"), Guid).ToString Else .AddressType = Guid.Empty.ToString
                    If Not (dr("AddressStatus") Is System.DBNull.Value) Then .AddressStatus = CType(dr("AddressStatus"), Guid).ToString Else .AddressStatus = Guid.Empty.ToString
                    If Not (dr("StudentNumber") Is System.DBNull.Value) Then .StudentNumber = CType(dr("StudentNumber"), String).ToString Else .StudentNumber = ""
                    If Not (dr("Objective") Is System.DBNull.Value) Then .Objective = CType(dr("Objective"), String).ToString Else .Objective = ""
                    If Not (dr("ForeignPhone") Is System.DBNull.Value) Then .ForeignPhone = dr("ForeignPhone") Else .ForeignPhone = 0
                    If Not (dr("ForeignZip") Is System.DBNull.Value) Then .ForeignZip = dr("ForeignZip") Else .ForeignZip = 0

                    If Not (dr("OtherState") Is System.DBNull.Value) Then .OtherState = CType(dr("OtherState"), String).ToString Else .OtherState = ""
                    If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate") Else .ModDate = Date.MinValue
                    If Not (dr("DependencyTypeId") Is System.DBNull.Value) Then .DependencyTypeId = CType(dr("DependencyTypeId"), Guid).ToString Else .DependencyTypeId = ""
                    If Not (dr("GeographicTypeId") Is System.DBNull.Value) Then .GeographicTypeId = CType(dr("GeographicTypeId"), Guid).ToString Else .GeographicTypeId = ""
                    If Not (dr("AdminCriteriaId") Is System.DBNull.Value) Then .AdminCriteriaId = CType(dr("AdminCriteriaId"), Guid).ToString Else .AdminCriteriaId = ""
                    If Not (dr("HousingId") Is System.DBNull.Value) Then .HousingId = CType(dr("HousingId"), Guid).ToString Else .HousingId = ""
                    If Not (dr("StateDescrip") Is System.DBNull.Value) Then .StateDescrip = CType(dr("StateDescrip"), String).ToString Else .StateDescrip = ""
                    If Not (dr("AddressTypeDescrip") Is System.DBNull.Value) Then .AddressTypeDescrip = CType(dr("AddressTypeDescrip"), String).ToString Else .AddressTypeDescrip = ""
                    If Not (dr("PrefixDescrip") Is System.DBNull.Value) Then .PrefixDescrip = CType(dr("PrefixDescrip"), String).ToString Else .PrefixDescrip = ""
                    If Not (dr("SuffixDescrip") Is System.DBNull.Value) Then .SuffixDescrip = CType(dr("SuffixDescrip"), String).ToString Else .SuffixDescrip = ""
                    If Not (dr("AdminCriteriaDescrip") Is System.DBNull.Value) Then .AdminCriteriaDescrip = CType(dr("AdminCriteriaDescrip"), String).ToString Else .AdminCriteriaDescrip = ""
                    If Not (dr("GenderDescrip") Is System.DBNull.Value) Then .GenderDescrip = CType(dr("GenderDescrip"), String).ToString Else .GenderDescrip = ""
                    If Not (dr("EthCodeDescrip") Is System.DBNull.Value) Then .EthCodeDescrip = CType(dr("EthCodeDescrip"), String).ToString Else .EthCodeDescrip = ""
                    If Not (dr("MaritalStatusDescrip") Is System.DBNull.Value) Then .MaritalStatusDescrip = CType(dr("MaritalStatusDescrip"), String).ToString Else .MaritalStatusDescrip = ""
                    If Not (dr("FamilyIncomeDescrip") Is System.DBNull.Value) Then .FamilyIncomeDescrip = CType(dr("FamilyIncomeDescrip"), String).ToString Else .FamilyIncomeDescrip = ""
                    If Not (dr("PhoneTypeDescrip") Is System.DBNull.Value) Then .PhoneTypeDescrip = CType(dr("PhoneTypeDescrip"), String).ToString Else .PhoneTypeDescrip = ""
                    If Not (dr("NationalityDescrip") Is System.DBNull.Value) Then .NationalityDescrip = CType(dr("NationalityDescrip"), String).ToString Else .NationalityDescrip = ""
                    If Not (dr("CitizenDescrip") Is System.DBNull.Value) Then .CitizenDescrip = CType(dr("CitizenDescrip"), String).ToString Else .CitizenDescrip = ""
                    If Not (dr("DriverLicenseStateDescrip") Is System.DBNull.Value) Then .DriverLicenseStateDescrip = CType(dr("DriverLicenseStateDescrip"), String).ToString Else .DriverLicenseStateDescrip = ""
                    If Not (dr("CountryDescrip") Is System.DBNull.Value) Then .CountryDescrip = CType(dr("CountryDescrip"), String).ToString Else .CountryDescrip = ""
                    If Not (dr("AddressStatusDescrip") Is System.DBNull.Value) Then .AddressStatusDescrip = CType(dr("AddressStatusDescrip"), String).ToString Else .AddressStatusDescrip = ""
                    If Not (dr("PhoneStatusDescrip") Is System.DBNull.Value) Then .PhoneStatusDescrip = CType(dr("PhoneStatusDescrip"), String).ToString Else .PhoneStatusDescrip = ""
                    If Not (dr("StudentStatusDescrip") Is System.DBNull.Value) Then .StudentStatusDescrip = CType(dr("StudentStatusDescrip"), String).ToString Else .StudentStatusDescrip = ""
                    If Not (dr("DependencyTypeDescrip") Is System.DBNull.Value) Then .DependencyTypeDescrip = CType(dr("DependencyTypeDescrip"), String).ToString Else .DependencyTypeDescrip = ""
                    If Not (dr("GeographicTypeDescrip") Is System.DBNull.Value) Then .GeographicTypeDescrip = CType(dr("GeographicTypeDescrip"), String).ToString Else .GeographicTypeDescrip = ""
                    If Not (dr("HousingDescrip") Is System.DBNull.Value) Then .HousingDescrip = CType(dr("HousingDescrip"), String).ToString Else .HousingDescrip = ""
                    If Not (dr("SponsorTypeDescrip") Is System.DBNull.Value) Then .SponsorTypeDescrip = dr("SponsorTypeDescrip").ToString Else .SponsorTypeDescrip = ""

                    'If Not (dr("StudentGrpId") Is System.DBNull.Value) Then .StudentGrpId = CType(dr("StudentGrpId"), Guid).ToString Else 
                    .StudentGrpId = ""
                End With
            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            'Return StudentMasterInfo
            Return studentInfo
        End Function
        Public Function AddStudentIdFormat(ByVal strFormatType As String, ByVal strYearNumber As Integer, ByVal strMonthNumber As Integer, ByVal strDateNumber As Integer, ByVal strLNameNumber As Integer, ByVal strFNameNumber As Integer, ByVal strSeqNumber As Integer, ByVal strStartingSeqNumber As Integer, ByVal user As String) As Integer

            'Connect To The Database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            'Do an Update
            'Try
            Dim sb1 As New StringBuilder
            With sb1
                .Append("Truncate table syStudentFormat ")
            End With

            'Get FormatType
            db.AddParameter("@FormatType", strFormatType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb1.ToString)

            'Clear Parameters
            db.ClearParameters()
            sb1.Remove(0, sb1.Length)

            Dim sb As New StringBuilder
            With sb
                .Append("Insert syStudentFormat(Id,FormatType,YearNumber,MonthNumber,DateNumber,LNameNumber,FNameNumber,SeqNumber,SeqStartingNumber,ModUser,ModDate) ")
                .Append(" Values(?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            'Get Id
            db.AddParameter("@Id", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Get FormatType
            db.AddParameter("@FormatType", strFormatType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Get YearNumber
            If strYearNumber = 0 Then
                db.AddParameter("@YearNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@YearNumber", strYearNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Get MonthYear
            If strMonthNumber = 0 Then
                db.AddParameter("@MonthNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@MonthNumber", strMonthNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Get DateNumber
            If strDateNumber = 0 Then
                db.AddParameter("@DateNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@DateNumber", strDateNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Get LastName
            If strLNameNumber = 0 Then
                db.AddParameter("@LNameNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@LNameNumber", strLNameNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Get FirstName
            If strFNameNumber = 0 Then
                db.AddParameter("@FNameNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@FNameNumber", strFNameNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Get Sequential Number
            If strSeqNumber = 0 Then
                db.AddParameter("@SeqNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@SeqNumber", strSeqNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Get Sequential Number
            If strStartingSeqNumber = 0 Then
                db.AddParameter("@StartSeqNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StartSeqNumber", strStartingSeqNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            'Retun Without Errors
            Return 0

            'Catch ex As System.Exception
            'Return an Error To Client
            '    Return -1

            'Finally
            'Close Connection
            '    db.CloseConnection()
            'End Try
        End Function
        Public Function GetStudentFormat() As StudentMasterInfo
            'connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            'build the sql query
            Dim sb As New StringBuilder
            With sb
                'With subqueries
                .Append(" select YearNumber,MonthNumber,DateNumber, ")
                .Append(" LNameNumber,FNameNumber,SeqNumber, ")
                .Append(" SeqStartingNumber, FormatType ")
                .Append(" from syStudentFormat ")
            End With


            'Execute the query
            Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString)
            Dim studentInfo As New StudentMasterInfo
            While dr.Read()
                'set properties with data from DataReader
                With studentInfo
                    If Not (dr("YearNumber") Is System.DBNull.Value) Then .YearNumber = CType(dr("YearNumber"), Integer) Else .YearNumber = 0
                    If Not (dr("MonthNumber") Is System.DBNull.Value) Then .MonthNumber = CType(dr("MonthNumber"), Integer) Else .MonthNumber = 0
                    If Not (dr("DateNumber") Is System.DBNull.Value) Then .DateNumber = CType(dr("DateNumber"), Integer) Else .DateNumber = 0
                    If Not (dr("LNameNumber") Is System.DBNull.Value) Then .LNameNumber = CType(dr("LNameNumber"), Integer) Else .LNameNumber = 0
                    If Not (dr("FNameNumber") Is System.DBNull.Value) Then .FNameNumber = CType(dr("FNameNumber"), Integer) Else .FNameNumber = 0
                    If Not (dr("SeqNumber") Is System.DBNull.Value) Then .SeqNumber = CType(dr("SeqNumber"), Integer) Else .SeqNumber = 0
                    If Not (dr("FormatType") Is System.DBNull.Value) Then .FormatType = CType(dr("FormatType"), String) Else .FormatType = ""
                    If Not (dr("SeqStartingNumber") Is System.DBNull.Value) Then .SeqStartNumber = CType(dr("SeqStartingNumber"), Integer) Else .SeqStartNumber = 0
                End With
            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            'Return StudentMasterInfo
            Return studentInfo
        End Function
        Public Function checkChildExists(ByVal EducationInstId As String) As Integer
            'connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            With sb
                .Append(" select Count(*) as ChildExists from ")
                .Append(" (select Distinct EducationInstId from plStudentEducation where EducationInstId = ? ")
                .Append(" union ")
                .Append(" Select Distinct EducationInstId from adLeadEducation where EducationInstId = ?) ED ")
            End With
            db.AddParameter("@EducationInstId", EducationInstId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@EducationInstId", EducationInstId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Dim intCheckChild As Integer = db.RunParamSQLScalar(sb.ToString)
            Return intCheckChild
        End Function
        Public Function GetAllDependencyType() As DataSet

            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            '   build the sql query
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT   Distinct BC.DependencyTypeId, BC.Descrip ")
                .Append("FROM     adDependencyTypes BC, syStatuses ST ")
                .Append("WHERE    BC.StatusId = ST.StatusId ")
                .Append("AND      ST.Status = 'Active' ")
                .Append("ORDER BY BC.Descrip ")
            End With

            '   return dataset
            Return db.RunSQLDataSet(sb.ToString)
        End Function
        Public Function GetAllGeographicType() As DataSet
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            '   build the sql query
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT Distinct  BC.GeographicTypeId, BC.Descrip ")
                .Append("FROM     adGeographicTypes BC, syStatuses ST ")
                .Append("WHERE    BC.StatusId = ST.StatusId ")
                .Append("AND      ST.Status = 'Active' ")
                .Append("ORDER BY BC.Descrip ")
            End With

            '   return dataset
            Return db.RunSQLDataSet(sb.ToString)
        End Function
        Public Function GetAllAdminCriteria() As DataSet
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            '   build the sql query
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT Distinct  BC.AdminCriteriaId, BC.Descrip ")
                .Append("FROM     adAdminCriteria BC, syStatuses ST ")
                .Append("WHERE    BC.StatusId = ST.StatusId ")
                .Append("AND      ST.Status = 'Active' ")
                .Append("ORDER BY BC.Descrip ")
            End With

            '   return dataset
            Return db.RunSQLDataSet(sb.ToString)
        End Function
        Public Function GetAllHousingType() As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            With sb
                .Append("SELECT Distinct  BC.HousingId, BC.Descrip ")
                .Append("FROM     arHousing BC, syStatuses ST ")
                .Append("WHERE    BC.StatusId = ST.StatusId ")
                .Append("AND      ST.Status = 'Active' ")
                .Append("ORDER BY BC.Descrip ")
            End With
            Return db.RunSQLDataSet(sb.ToString)
        End Function
    End Class
#End Region

#Region "Student Enrollments"
    Public Class StudentEnrollmentsDB
        ''' <summary>
        ''' Returns all student enrollment status codes
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetEnrollmentStatusCodes(Optional ByVal campusId As String = "") As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("select distinct " + vbCrLf)
            sb.Append("     SC.StatusCodeId as ID, " + vbCrLf)
            sb.Append("     SC.StatusCodeDescrip as Descrip " + vbCrLf)
            sb.Append("from " + vbCrLf)
            sb.Append("     arStuEnrollments SE,syStatusCodes SC, sySysStatus SS,syStatuses " + vbCrLf)
            sb.Append("where " + vbCrLf)
            sb.Append("       SE.StatusCodeId=SC.StatusCodeId ")
            sb.Append(" and    SC.SysStatusId = SS.SysStatusId " + vbCrLf)
            sb.Append("     and SS.StatusLevelId=2  and SC.StatusId=syStatuses.StatusId and syStatuses.Status='Active'" + vbCrLf)
            If campusId <> "" Then
                sb.Append("AND	SC.CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
                sb.Append("AND    SE.CampusId = ? ")
            End If
            sb.Append("order by " + vbCrLf)
            sb.Append("     SC.StatusCodeDescrip " + vbCrLf)
            If campusId <> "" Then
                db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function
    End Class
#End Region

#Region "Attendance"
    Public Class AttendanceDB
        ''' <summary>
        ''' Retrieves schedule information given a bunch of different filters.
        ''' </summary>
        ''' <param name="filterProgId"></param>
        ''' <param name="filterPrgVerId"></param>
        ''' <param name="filterStuName"></param>
        ''' <param name="filterStuStatus"></param>
        ''' <param name="filterStartDate"></param>
        ''' <param name="filterEndDate"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetStudentSchedulesForAttendance(
                                    ByVal filterProgId As String,
                                    ByVal filterPrgVerId As String,
                                    ByVal filterStuName As String,
                                    ByVal filterStuStatus As String,
                                    ByVal filterStartDate As String,
                                    ByVal filterEndDate As String,
                                    ByVal filterUseTimeClock As String,
                                    ByVal filterBadgeNum As String,
                                    Optional ByVal campusId As String = "",
                                    Optional ByVal filterstudentgrpid As String = "",
                                    Optional ByVal filterCohortStartDate As String = "",
                                    Optional ByVal filteroutofschoolfilter As String = "") As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder

            If filterStuStatus <> "" And filteroutofschoolfilter <> "" Then
                filterStuStatus = filterStuStatus & ",'" & filteroutofschoolfilter
            ElseIf filterStuStatus = "" And filteroutofschoolfilter <> "" Then
                filterStuStatus = filteroutofschoolfilter
            End If

            sb.Append("select Distinct " + vbCrLf)
            sb.Append("     SE.StuEnrollID, " + vbCrLf)
            sb.Append("     SS.ScheduleId, " + vbCrLf)
            sb.Append("     S.FirstName, " + vbCrLf)
            sb.Append("     S.MIddleName, " + vbCrLf)
            sb.Append("     S.LastName, " + vbCrLf)
            sb.Append("     S.LastName + ','  +  S.FirstName + isnull(S.MiddleName,'') as FullName, " + vbCrLf)
            sb.Append("     SE.BadgeNumber, " + vbCrLf)
            sb.Append("     SE.DateDetermined, " + vbCrLf)
            sb.Append("     SE.ExpGradDate, " + vbCrLf)
            sb.Append("     (select SC.SysStatusId from syStatusCodes SC where SC.StatusCodeId=SE.StatusCodeId) as SysStatusId, " + vbCrLf)
            sb.Append("     (select StatusCodeDescrip from syStatusCodes SC where SC.StatusCodeId=SE.StatusCodeId) as StatusCodeDescrip, " + vbCrLf)
            sb.Append("     SS.StartDate, " + vbCrLf)
            sb.Append("     SS.EndDate, " + vbCrLf)
            'sb.Append("     SS.Active, " + vbCrLf)
            sb.Append("     PS.UseFlexTime, " + vbCrLf)
            sb.Append("     PS.Descrip as ScheduleDescrip,	 " + vbCrLf)
            sb.Append("     PS.PrgVerId, " + vbCrLf)
            sb.Append("     PV.PrgVerDescrip, " + vbCrLf)
            sb.Append("     PV.UseTimeClock, " + vbCrLf)
            'sb.Append("     SE.StartDate as StudentStartDate, " + vbCrLf)
            sb.Append("     ISNULL(SE.StartDate, SE.ExpStartDate) as StudentStartDate, " + vbCrLf)
            sb.Append("     (select total from arProgScheduleDetails PSD where PSD.ScheduleId=PS.ScheduleId and dw=0) as dw0total, " + vbCrLf)
            sb.Append("     (select total from arProgScheduleDetails PSD where PSD.ScheduleId=PS.ScheduleId and dw=1) as dw1total, " + vbCrLf)
            sb.Append("     (select total from arProgScheduleDetails PSD where PSD.ScheduleId=PS.ScheduleId and dw=2) as dw2total, " + vbCrLf)
            sb.Append("     (select total from arProgScheduleDetails PSD where PSD.ScheduleId=PS.ScheduleId and dw=3) as dw3total, " + vbCrLf)
            sb.Append("     (select total from arProgScheduleDetails PSD where PSD.ScheduleId=PS.ScheduleId and dw=4) as dw4total, " + vbCrLf)
            sb.Append("     (select total from arProgScheduleDetails PSD where PSD.ScheduleId=PS.ScheduleId and dw=5) as dw5total, " + vbCrLf)
            sb.Append("     (select total from arProgScheduleDetails PSD where PSD.ScheduleId=PS.ScheduleId and dw=6) as dw6total, " + vbCrLf)
            sb.Append("     AUT.UnitTypeDescrip,SS.Source " + vbCrLf)
            ''Added by saraswathi on April 21 2009
            ''The SSN And Student Id is added to show in tool tip 
            sb.Append(" ,S.SSN,S.StudentNumber  ")

            sb.Append("from " + vbCrLf)
            sb.Append("     arStudentSchedules SS, " + vbCrLf)
            sb.Append("     arProgSchedules PS, " + vbCrLf)
            sb.Append("     arPrgVersions PV, " + vbCrLf)
            sb.Append("     arPrograms P, " + vbCrLf)
            sb.Append("     arStuEnrollments SE, " + vbCrLf)
            sb.Append("     arStudent S, " + vbCrLf)
            sb.Append("     arAttUnitType AUT " + vbCrLf)
            If filterstudentgrpid IsNot Nothing AndAlso filterstudentgrpid <> "" Then
                sb.Append(" ,adLeadByLeadGroups LLG " + vbCrLf)
            End If
            sb.Append("where " + vbCrLf)
            sb.Append("     SS.StuEnrollId = SE.StuEnrollId " + vbCrLf)
            sb.Append("     and SE.StudentId = S.StudentId " + vbCrLf)
            sb.Append("     and SS.ScheduleId = PS.ScheduleId " + vbCrLf)
            sb.Append("     and PS.PrgVerId = PV.PrgVerId " + vbCrLf)
            sb.Append("     and SE.PrgVerId = PV.PrgVerId " + vbCrLf)
            sb.Append("     and PV.ProgId = P.ProgId " + vbCrLf)
            sb.Append("     and PV.UnitTypeId = AUT.UnitTypeId " + vbCrLf)
            sb.Append("     and SS.Active = 1 " + vbCrLf)
            If filterstudentgrpid IsNot Nothing AndAlso filterstudentgrpid <> "" Then
                sb.Append(" and SS.StuEnrollId = LLG.StuEnrollId " + vbCrLf)
            End If
            If campusId <> "" Then
                sb.Append("     and SE.CampusId = '" & campusId & "'" + vbCrLf)
            End If
            ' add the params
            If filterProgId IsNot Nothing AndAlso filterProgId <> "" Then
                sb.AppendFormat("       and PV.ProgId = '{0}' {1}", filterProgId, vbCrLf)
            End If
            If filterPrgVerId IsNot Nothing AndAlso filterPrgVerId <> "" Then
                sb.AppendFormat("       and PV.PrgVerId = '{0}' {1}", filterPrgVerId, vbCrLf)
            End If
            If filterStuName IsNot Nothing AndAlso filterStuName <> "" Then
                ''Modified by saraswathi to get the students name
                'sb.AppendFormat("  and (S.FirstName like '%{0}%' or S.LastName like '%{0}%' ) ", filterStuName)

                Dim i As Integer = filterStuName.IndexOf(" ")
                Dim filterLastName As String = filterStuName.Trim()
                Dim filterFirstName As String = ""
                If i > 0 AndAlso i < filterStuName.Length - 1 Then
                    filterLastName = filterStuName.Substring(i, filterStuName.Length - i).Trim()
                    filterFirstName = filterStuName.Substring(0, i).Trim()
                End If
                ''Modified by saraswathi on jan 20 2009
                ''to fetch the students by firstname,lastname or fullname
                'If filterLastName <> "" Then
                '    sb.AppendFormat("       and( S.LastName like '{0}%' ){1}", filterLastName, vbCrLf)
                'End If
                'If filterFirstName <> "" Then
                '    sb.AppendFormat("       or( S.FirstName like '{0}%' ){1}", filterFirstName, vbCrLf)
                'End If
                'If filterLastName <> "" And filterFirstName <> "" Then
                '    sb.AppendFormat("       or( S.LastName like '{0}%'  and S.FirstName like '{1}%')", filterLastName, filterFirstName)
                'End If
                ''Modified by saraswathi on jan 20 2009
                ''to fetch the students by firstname,lastname or fullname
                'If filterLastName <> "" Then
                '    sb.AppendFormat("       and( S.LastName like '{0}%' or S.FirstName like '{0}%'){1}", filterLastName, vbCrLf)
                'End If
                'If filterFirstName <> "" Then
                '    sb.AppendFormat("       or( S.FirstName like '{0}%' or S.LastName like '{0}%'){1}", filterFirstName, vbCrLf)
                'End If
                'If filterLastName <> "" And filterFirstName <> "" Then
                '    sb.AppendFormat("       or( S.LastName like '{0}%'  and S.FirstName like '{1}%')", filterLastName, filterFirstName)
                'End If

                If filterLastName <> "" And filterFirstName <> "" Then
                    sb.AppendFormat("       And(( S.LastName like '{0}%'  and S.FirstName like '{1}%')or ( S.LastName like '{1}%'  and S.FirstName like '{0}%') )", filterLastName, filterFirstName)
                ElseIf filterLastName <> "" Then
                    sb.AppendFormat("       and( S.LastName like '{0}%' or S.FirstName like '{0}%'){1}", filterLastName, vbCrLf)
                ElseIf filterFirstName <> "" Then
                    sb.AppendFormat("       and( S.FirstName like '{0}%' or S.LastName like '{0}%'){1}", filterFirstName, vbCrLf)
                End If


            End If
            If filterStuStatus IsNot Nothing AndAlso filterStuStatus <> "" Then
                'sb.AppendFormat("       and SE.StatusCodeId = '{0}' {1}", filterStuStatus, vbCrLf)
                sb.AppendFormat(" and SE.StatusCodeId in ('")
                sb.AppendFormat(filterStuStatus)
                sb.AppendFormat(") ")
            End If
            If filterStartDate IsNot Nothing AndAlso filterStartDate <> "" Then
                sb.AppendFormat("       and (SS.StartDate is null or SS.StartDate >= '{0}') {1}", filterStartDate, vbCrLf)
            End If
            If filterEndDate IsNot Nothing AndAlso filterEndDate <> "" Then
                sb.AppendFormat("       and (SS.EndDate is null or SS.EndDate <= '{0}') {1}", filterStartDate, vbCrLf)
            End If
            If filterUseTimeClock IsNot Nothing AndAlso filterUseTimeClock <> "" Then
                If filterUseTimeClock.ToLower = "true" Or filterUseTimeClock = "1" Then
                    sb.Append("       and PV.UseTimeClock = 1" + vbCrLf)
                Else
                    sb.Append("       and (PV.UseTimeClock = 0 or PV.UseTimeClock is null)" + vbCrLf)
                End If
            End If
            If filterBadgeNum IsNot Nothing AndAlso filterBadgeNum <> "" Then
                sb.AppendFormat("       and SE.BadgeNumber = '{0}' {1}", filterBadgeNum, vbCrLf)
            End If
            If filterstudentgrpid IsNot Nothing AndAlso filterstudentgrpid <> "" Then
                sb.AppendFormat(" and LLG.LeadGrpId = '{0}' {1}", filterstudentgrpid, vbCrLf)
            End If
            ''Added by Saraswathi to filter based on cohortStartDate
            If filterCohortStartDate IsNot Nothing AndAlso filterCohortStartDate <> "" Then
                sb.AppendFormat(" and SE.CohortStartDate = '{0}' {1}", filterCohortStartDate, vbCrLf)
            End If
            sb.Append("order by " + vbCrLf)
            sb.Append("     S.LastName, S.FirstName, S.MiddleName " + vbCrLf)

            If campusId <> "" Then
                db.AddParameter("cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            'sb.Append("     SCA.RecordDate " + vbCrLf)
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function
        Public Shared Function GetStudentStatusHistory(ByVal prgVerId As String, ByVal campusId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            Dim intRowCount As Integer = 0

            With sb
                .Append("SELECT ssc.StuEnrollId, ssc.OrigStatusId, ssc.NewStatusId, sc.SysStatusId, ssc.DateOfChange FROM dbo.syStudentStatusChanges ssc
                        INNER JOIN dbo.arStuEnrollments se ON se.StuEnrollId = ssc.StuEnrollId
                        INNER JOIN dbo.arPrgVersions pv ON pv.PrgVerId = se.PrgVerId
                        INNER JOIN dbo.syStatusCodes sc ON sc.StatusCodeId = ssc.NewStatusId
                        WHERE se.CampusId = ? ")
            End With

            If (Not String.IsNullOrEmpty(prgVerId)) Then
                sb.Append(" AND pv.PrgVerId = ? ")
            End If

            sb.Append(" ORDER BY ssc.StuEnrollId, ssc.DateOfChange")

            If (Not String.IsNullOrEmpty(prgVerId)) Then
                db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString)
            End If

            db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString)

            Try
                Dim ds As New DataSet
                ds = db.RunParamSQLDataSet(sb.ToString)
                Return ds

            Catch ex As System.Exception
                Return New DataSet
            Finally
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            End Try
        End Function

        Public Shared Function DoesProgramVersionUseTimeClock(ByVal filterprgverId As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            Dim intRowCount As Integer = 0
            With sb
                .Append(" Select Count(*) as TimeClock from arPrgVersions where PrgVerId=? and UseTimeClock=1 ")
            End With
            db.AddParameter("@PrgVerId", filterprgverId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                intRowCount = CType(db.RunParamSQLScalar(sb.ToString), Integer)
                If intRowCount >= 1 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As System.Exception
                Return False
            Finally
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            End Try
        End Function
#Region "GetClockHourAttendance   This function gets the attendance details related to the current Schedule the student is mapped to "
        'Public Shared Function GetClockHourAttendance( _
        '                            ByVal filterProgId As String, _
        '                            ByVal filterPrgVerId As String, _
        '                            ByVal filterStuName As String, _
        '                            ByVal filterStuStatus As String, _
        '                            ByVal filterStartDate As String, _
        '                            ByVal filterEndDate As String, _
        '                            ByVal filterUseTimeClock As String, _
        '                            ByVal filterBadgeNum As String, _
        '                            ByVal filterstudentgrpid As String, _
        '                            Optional ByVal filteroutofschoolfilter As String = "") As DataSet
        '    Dim db As New DataAccess
        '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

        '    'Dim boolUseTimeClock As Boolean = False
        '    'boolUseTimeClock = DoesProgramVersionUseTimeClock(filterPrgVerId)


        '    If filterStuStatus <> "" And filteroutofschoolfilter <> "" Then
        '        filterStuStatus = filterStuStatus & ",'" & filteroutofschoolfilter
        '    ElseIf filterStuStatus = "" And filteroutofschoolfilter <> "" Then
        '        filterStuStatus = filteroutofschoolfilter
        '    End If

        '    '   build the sql query
        '    Dim sb As New StringBuilder
        '    sb.Append("select Distinct " + vbCrLf)
        '    sb.Append("     SE.StuEnrollID, " + vbCrLf)
        '    sb.Append("     SS.ScheduleId, " + vbCrLf)
        '    sb.Append("     SCA.RecordDate, " + vbCrLf)
        '    ''Commented by Saraswathi Lakshmanan on august 28 2009
        '    ''To fix mantis case: Edit timeClock Scheduled Hours
        '    ''For any type of School the scheduled hours is taken from the arStudentClockAttendance table
        '    '  If boolUseTimeClock = False Then
        '    sb.Append("     SCA.SchedHours, " + vbCrLf)
        '    'Else
        '    'sb.Append("   (select Distinct Total from arProgScheduleDetails  " + vbCrLf)
        '    'sb.Append("   where ScheduleId=SS.ScheduleId and " + vbCrLf)
        '    'sb.Append("   dw=DatePart(dw,SCA.RecordDate) - 1) as SchedHours, " + vbCrLf)
        '    'End If
        '    sb.Append("     SCA.ActualHours, " + vbCrLf)
        '    sb.Append("     SCA.IsTardy as Tardy," + vbCrLf)
        '    sb.Append("     SCA.ModUser, " + vbCrLf)
        '    sb.Append("     SCA.ModDate, " + vbCrLf)
        '    sb.Append("     SS.StartDate, " + vbCrLf)
        '    sb.Append("     SS.EndDate, " + vbCrLf)
        '    sb.Append("     SS.Active, " + vbCrLf)
        '    sb.Append("     PS.UseFlexTime, " + vbCrLf)
        '    sb.Append("     PV.UseTimeClock, " + vbCrLf)
        '    sb.Append("     AUT.UnitTypeDescrip, " + vbCrLf)
        '    sb.Append("     'attendance' as Source, " + vbCrLf)
        '    sb.Append("     SCA.PostByException " + vbCrLf)
        '    sb.Append("from " + vbCrLf)
        '    sb.Append("     arStudentClockAttendance SCA, " + vbCrLf)
        '    sb.Append("     arStudentSchedules SS, " + vbCrLf)
        '    sb.Append("     arProgSchedules PS, " + vbCrLf)
        '    sb.Append("     arPrgVersions PV, " + vbCrLf)
        '    sb.Append("     arPrograms P, " + vbCrLf)
        '    sb.Append("     arStuEnrollments SE, " + vbCrLf)
        '    sb.Append("     arStudent S, " + vbCrLf)
        '    sb.Append("     arAttUnitType AUT " + vbCrLf)
        '    If filterstudentgrpid IsNot Nothing AndAlso filterstudentgrpid <> "" Then
        '        sb.Append(" ,adLeadByLeadGroups LLG " + vbCrLf)
        '    End If
        '    sb.Append("where " + vbCrLf)
        '    sb.Append("     SCA.StuEnrollId = SS.StuEnrollId " + vbCrLf)
        '    sb.Append("     and SCA.ScheduleId = SS.ScheduleId " + vbCrLf)
        '    sb.Append("     and SS.StuEnrollId = SE.StuEnrollId " + vbCrLf)
        '    sb.Append("     and SE.StudentId = S.StudentId " + vbCrLf)
        '    sb.Append("     and SCA.ScheduleId = SS.ScheduleId " + vbCrLf)
        '    sb.Append("     and SCA.ScheduleId = PS.ScheduleId " + vbCrLf)
        '    sb.Append("     and PS.PrgVerId = PV.PrgVerId " + vbCrLf)
        '    sb.Append("     and PV.ProgId = P.ProgId " + vbCrLf)
        '    sb.Append("     and PV.UnitTypeId = AUT.UnitTypeId " + vbCrLf)
        '    sb.Append("     and SS.Active = 1 " + vbCrLf)
        '    If filterstudentgrpid IsNot Nothing AndAlso filterstudentgrpid <> "" Then
        '        sb.Append(" and SCA.StuEnrollId = LLG.StuEnrollId " + vbCrLf)
        '    End If

        '    ' add the params
        '    If filterProgId IsNot Nothing AndAlso filterProgId <> "" Then
        '        sb.AppendFormat("       and PV.ProgId = '{0}' {1}", filterProgId, vbCrLf)
        '    End If
        '    If filterPrgVerId IsNot Nothing AndAlso filterPrgVerId <> "" Then
        '        sb.AppendFormat("       and PV.PrgVerId = '{0}' {1}", filterPrgVerId, vbCrLf)
        '    End If
        '    If filterStuName IsNot Nothing AndAlso filterStuName <> "" Then
        '        ''modified by saraswathi to find the Student last name or first name.
        '        ''Changed on Nov 20 2008
        '        '  sb.AppendFormat("  and (S.FirstName like '%{0}%' or S.LastName like '%{0}%' ) ", filterStuName)

        '        Dim i As Integer = filterStuName.IndexOf(" ")
        '        Dim filterLastName As String = filterStuName.Trim()
        '        Dim filterFirstName As String = ""
        '        If i > 0 AndAlso i < filterStuName.Length - 1 Then
        '            filterLastName = filterStuName.Substring(i, filterStuName.Length - i).Trim()
        '            filterFirstName = filterStuName.Substring(0, i).Trim()
        '        End If
        '        ''Modified by saraswathi on jan 20 2009
        '        ''to fetch the students by firstname,lastname or fullname
        '        'If filterLastName <> "" Then
        '        '    sb.AppendFormat("       and( S.LastName like '{0}%' or S.FirstName like '{0}%'){1}", filterLastName, vbCrLf)
        '        'End If
        '        'If filterFirstName <> "" Then
        '        '    sb.AppendFormat("       or( S.FirstName like '{0}%' or S.LastName like '{0}%'){1}", filterFirstName, vbCrLf)
        '        'End If
        '        'If filterLastName <> "" And filterFirstName <> "" Then
        '        '    sb.AppendFormat("       or( S.LastName like '{0}%'  and S.FirstName like '{1}%')", filterLastName, filterFirstName)
        '        'End If


        '        If filterLastName <> "" And filterFirstName <> "" Then
        '            sb.AppendFormat("       And(( S.LastName like '{0}%'  and S.FirstName like '{1}%')or ( S.LastName like '{1}%'  and S.FirstName like '{0}%') )", filterLastName, filterFirstName)
        '        ElseIf filterLastName <> "" Then
        '            sb.AppendFormat("       and( S.LastName like '{0}%' or S.FirstName like '{0}%'){1}", filterLastName, vbCrLf)
        '        ElseIf filterFirstName <> "" Then
        '            sb.AppendFormat("       and( S.FirstName like '{0}%' or S.LastName like '{0}%'){1}", filterFirstName, vbCrLf)
        '        End If

        '        'If filterLastName <> "" Then
        '        '    sb.AppendFormat("       and S.LastName like '{0}%' {1}", filterLastName, vbCrLf)
        '        'End If
        '        'If filterFirstName <> "" Then
        '        '    sb.AppendFormat("       and S.FirstName like '{0}%' {1}", filterFirstName, vbCrLf)
        '        'End If
        '    End If
        '    If filterStuStatus IsNot Nothing AndAlso filterStuStatus <> "" Then
        '        'sb.AppendFormat("       and SE.StatusCodeId = '{0}' {1}", filterStuStatus, vbCrLf)
        '        sb.AppendFormat(" and SE.StatusCodeId in ('")
        '        sb.AppendFormat(filterStuStatus)
        '        sb.AppendFormat(") ")
        '    End If
        '    'If filteroutofschoolfilter IsNot Nothing AndAlso filteroutofschoolfilter <> "" Then
        '    '    sb.AppendFormat(" and SE.StatusCodeId in ('")
        '    '    sb.AppendFormat(filteroutofschoolfilter)
        '    '    sb.AppendFormat(") ")
        '    'End If
        '    If filterStartDate IsNot Nothing AndAlso filterStartDate <> "" Then
        '        sb.AppendFormat("       and SCA.RecordDate >= '{0}' {1}", filterStartDate, vbCrLf)
        '    End If
        '    If filterEndDate IsNot Nothing AndAlso filterEndDate <> "" Then
        '        sb.AppendFormat("       and SCA.RecordDate <= '{0}' {1}", filterStartDate, vbCrLf)
        '    End If
        '    If filterUseTimeClock IsNot Nothing AndAlso filterUseTimeClock <> "" Then
        '        If filterUseTimeClock.ToLower = "true" Or filterUseTimeClock = "1" Then
        '            sb.Append("       and PV.UseTimeClock = 1" + vbCrLf)
        '        Else
        '            sb.Append("       and (PV.UseTimeClock = 0 or PV.UseTimeClock is null)" + vbCrLf)
        '        End If
        '    End If
        '    If filterBadgeNum IsNot Nothing AndAlso filterBadgeNum <> "" Then
        '        sb.AppendFormat("       and SE.BadgeNumber = '{0}' {1}", filterBadgeNum, vbCrLf)
        '    End If
        '    If filterstudentgrpid IsNot Nothing AndAlso filterstudentgrpid <> "" Then
        '        sb.AppendFormat(" and LLG.LeadGrpId = '{0}' {1}", filterstudentgrpid, vbCrLf)
        '    End If
        '    sb.Append("union " + vbCrLf)
        '    sb.Append("select Distinct " + vbCrLf)
        '    sb.Append("     SE.StuEnrollID, " + vbCrLf)
        '    sb.Append("     SS.ScheduleId, " + vbCrLf)
        '    sb.Append("     SCA.MeetDate, " + vbCrLf)
        '    ''Commented by Saraswathi Lakshmanan on august 28 2009
        '    ''To fix mantis case: Edit timeClock Scheduled Hours
        '    ''For any type of School the scheduled hours is taken from the arStudentClockAttendance table
        '    ' If boolUseTimeClock = False Then
        '    sb.Append("     SCA.Schedule, " + vbCrLf)
        '    'Else
        '    'sb.Append("   (select Distinct Total from arProgScheduleDetails  " + vbCrLf)
        '    'sb.Append("   where ScheduleId=SS.ScheduleId and " + vbCrLf)
        '    'sb.Append("   dw=DatePart(dw,SCA.MeetDate) - 1) as Schedule, " + vbCrLf)
        '    'End If
        '    sb.Append("     SCA.Actual, " + vbCrLf)
        '    sb.Append("     SCA.Tardy as Tardy," + vbCrLf)
        '    sb.Append("     '' as ModUser, " + vbCrLf)
        '    sb.Append("     '' as ModDate, " + vbCrLf)
        '    sb.Append("     SS.StartDate, " + vbCrLf)
        '    sb.Append("     SS.EndDate, " + vbCrLf)
        '    sb.Append("     SS.Active, " + vbCrLf)
        '    sb.Append("     PS.UseFlexTime, " + vbCrLf)
        '    sb.Append("     PV.UseTimeClock, " + vbCrLf)
        '    sb.Append("     AUT.UnitTypeDescrip, " + vbCrLf)
        '    sb.Append("     'conversion' as Source, " + vbCrLf)
        '    sb.Append("     SCA.PostByException " + vbCrLf)
        '    sb.Append("from " + vbCrLf)
        '    sb.Append("     atConversionAttendance SCA, " + vbCrLf)
        '    sb.Append("     arStudentSchedules SS, " + vbCrLf)
        '    sb.Append("     arProgSchedules PS, " + vbCrLf)
        '    sb.Append("     arPrgVersions PV, " + vbCrLf)
        '    sb.Append("     arPrograms P, " + vbCrLf)
        '    sb.Append("     arStuEnrollments SE, " + vbCrLf)
        '    sb.Append("     arStudent S, " + vbCrLf)
        '    sb.Append("     arAttUnitType AUT " + vbCrLf)
        '    If filterstudentgrpid IsNot Nothing AndAlso filterstudentgrpid <> "" Then
        '        sb.Append(" ,adLeadByLeadGroups LLG " + vbCrLf)
        '    End If
        '    sb.Append("where " + vbCrLf)
        '    sb.Append("      SCA.StuEnrollId = SS.StuEnrollId " + vbCrLf)
        '    sb.Append("      and SS.ScheduleId = PS.ScheduleId " + vbCrLf)
        '    sb.Append("      and SS.StuEnrollId = SE.StuEnrollId " + vbCrLf)
        '    sb.Append("      and SE.StudentId = S.StudentId " + vbCrLf)
        '    sb.Append("      and PS.PrgVerId = SE.PrgVerId " + vbCrLf)
        '    sb.Append("      and SE.PrgVerId = PV.PrgVerId " + vbCrLf)
        '    sb.Append("      and PV.ProgId = P.ProgId " + vbCrLf)
        '    sb.Append("      and PV.UnitTypeId = AUT.UnitTypeId " + vbCrLf)
        '    sb.Append("      and SS.Active = 1 " + vbCrLf)
        '    If filterstudentgrpid IsNot Nothing AndAlso filterstudentgrpid <> "" Then
        '        sb.Append(" and SCA.StuEnrollId = LLG.StuEnrollId " + vbCrLf)
        '    End If
        '    ' add the params
        '    If filterProgId IsNot Nothing AndAlso filterProgId <> "" Then
        '        sb.AppendFormat("       and PV.ProgId = '{0}' {1}", filterProgId, vbCrLf)
        '    End If
        '    If filterPrgVerId IsNot Nothing AndAlso filterPrgVerId <> "" Then
        '        sb.AppendFormat("       and PV.PrgVerId = '{0}' {1}", filterPrgVerId, vbCrLf)
        '    End If
        '    If filterStuName IsNot Nothing AndAlso filterStuName <> "" Then
        '        Dim i As Integer = filterStuName.IndexOf(" ")
        '        Dim filterLastName As String = filterStuName.Trim()
        '        Dim filterFirstName As String = ""
        '        If i > 0 AndAlso i < filterStuName.Length - 1 Then
        '            filterLastName = filterStuName.Substring(i, filterStuName.Length - i).Trim()
        '            filterFirstName = filterStuName.Substring(0, i).Trim()
        '        End If
        '        'If filterLastName <> "" Then
        '        '    sb.AppendFormat("       and S.LastName like '{0}%' {1}", filterLastName, vbCrLf)
        '        'End If
        '        'If filterFirstName <> "" Then
        '        '    sb.AppendFormat("       and S.FirstName like '{0}%' {1}", filterFirstName, vbCrLf)
        '        'End If
        '        ''Modified by saraswathi on jan 20 2009
        '        ''to fetch the students by firstname,lastname or fullname


        '        If filterLastName <> "" And filterFirstName <> "" Then
        '            sb.AppendFormat("       And(( S.LastName like '{0}%'  and S.FirstName like '{1}%')or ( S.LastName like '{1}%'  and S.FirstName like '{0}%') )", filterLastName, filterFirstName)
        '        ElseIf filterLastName <> "" Then
        '            sb.AppendFormat("       and( S.LastName like '{0}%' or S.FirstName like '{0}%'){1}", filterLastName, vbCrLf)
        '        ElseIf filterFirstName <> "" Then
        '            sb.AppendFormat("       and( S.FirstName like '{0}%' or S.LastName like '{0}%'){1}", filterFirstName, vbCrLf)
        '        End If


        '    End If
        '    'If filterStuStatus IsNot Nothing AndAlso filterStuStatus <> "" Then
        '    '    sb.AppendFormat("       and SE.StatusCodeId = '{0}' {1}", filterStuStatus, vbCrLf)
        '    'End If
        '    If filterStuStatus IsNot Nothing AndAlso filterStuStatus <> "" Then
        '        'sb.AppendFormat("       and SE.StatusCodeId = '{0}' {1}", filterStuStatus, vbCrLf)
        '        sb.AppendFormat(" and SE.StatusCodeId in ('")
        '        sb.AppendFormat(filterStuStatus)
        '        sb.AppendFormat(") ")
        '    End If
        '    'If filteroutofschoolfilter IsNot Nothing AndAlso filteroutofschoolfilter <> "" Then
        '    '    sb.AppendFormat(" and SE.StatusCodeId in ('")
        '    '    sb.AppendFormat(filteroutofschoolfilter)
        '    '    sb.AppendFormat(") ")
        '    'End If
        '    If filterStartDate IsNot Nothing AndAlso filterStartDate <> "" Then
        '        sb.AppendFormat("       and SCA.MeetDate >= '{0}' {1}", filterStartDate, vbCrLf)
        '    End If
        '    If filterEndDate IsNot Nothing AndAlso filterEndDate <> "" Then
        '        sb.AppendFormat("       and SCA.MeetDate <= '{0}' {1}", filterStartDate, vbCrLf)
        '    End If
        '    If filterUseTimeClock IsNot Nothing AndAlso filterUseTimeClock <> "" Then
        '        If filterUseTimeClock.ToLower = "true" Or filterUseTimeClock = "1" Then
        '            sb.Append("       and PV.UseTimeClock = 1" + vbCrLf)
        '        Else
        '            sb.Append("       and (PV.UseTimeClock = 0 or PV.UseTimeClock is null)" + vbCrLf)
        '        End If
        '    End If
        '    If filterBadgeNum IsNot Nothing AndAlso filterBadgeNum <> "" Then
        '        sb.AppendFormat("       and SE.BadgeNumber = '{0}' {1}", filterBadgeNum, vbCrLf)
        '    End If
        '    If filterstudentgrpid IsNot Nothing AndAlso filterstudentgrpid <> "" Then
        '        sb.AppendFormat(" and LLG.LeadGrpId = '{0}' {1}", filterstudentgrpid, vbCrLf)
        '    End If
        '    sb.Append("order by " + vbCrLf)
        '    sb.Append("     SE.StuEnrollId " + vbCrLf)
        '    'sb.Append("     SCA.RecordDate " + vbCrLf)
        '    Return db.RunParamSQLDataSet(sb.ToString)
        'End Function
#End Region

        ''Parameters made similar to get student schedule for attendance
        ''Modified by Saraswathi lakshmanan on april 12 2010
        ''To fix mantis case 18806 Ross Snow DAys
        'Added by Saraswathi lakshmanan to get attendance information of the student irrtrespective of the current schedule
#Region " GetClockHourAttendance -- Function gets the attendance details irrespective of the schedules. This helps us to get the attendance details of the previous schedules also"
        'Added by Saraswathi lakshmanan to get attendance information of the student irrtrespective of the current schedule
        Public Shared Function GetClockHourAttendance(
                                  ByVal filterProgId As String,
                                  ByVal filterPrgVerId As String,
                                  ByVal filterStuName As String,
                                  ByVal filterStuStatus As String,
                                  ByVal filterStartDate As String,
                                  ByVal filterEndDate As String,
                                  ByVal filterUseTimeClock As String,
                                  ByVal filterBadgeNum As String,
                                    Optional ByVal campusId As String = "",
                                    Optional ByVal filterstudentgrpid As String = "",
                                    Optional ByVal filterCohortStartDate As String = "",
                                    Optional ByVal filteroutofschoolfilter As String = "") As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            'Dim boolUseTimeClock As Boolean = False
            'boolUseTimeClock = DoesProgramVersionUseTimeClock(filterPrgVerId)


            If filterStuStatus <> "" And filteroutofschoolfilter <> "" Then
                filterStuStatus = filterStuStatus & ",'" & filteroutofschoolfilter
            ElseIf filterStuStatus = "" And filteroutofschoolfilter <> "" Then
                filterStuStatus = filteroutofschoolfilter
            End If

            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("select Distinct " + vbCrLf)
            sb.Append("     SE.StuEnrollID, " + vbCrLf)
            sb.Append("     SCA.ScheduleId, " + vbCrLf)
            sb.Append("     SCA.RecordDate, " + vbCrLf)
            ''Commented by Saraswathi Lakshmanan on august 28 2009
            ''To fix mantis case: Edit timeClock Scheduled Hours
            ''For any type of School the scheduled hours is taken from the arStudentClockAttendance table
            '  If boolUseTimeClock = False Then
            sb.Append("     SCA.SchedHours, " + vbCrLf)
            'Else
            'sb.Append("   (select Distinct Total from arProgScheduleDetails  " + vbCrLf)
            'sb.Append("   where ScheduleId=SS.ScheduleId and " + vbCrLf)
            'sb.Append("   dw=DatePart(dw,SCA.RecordDate) - 1) as SchedHours, " + vbCrLf)
            'End If
            sb.Append("     SCA.ActualHours, " + vbCrLf)
            sb.Append("     SCA.IsTardy as Tardy," + vbCrLf)
            sb.Append("     SCA.ModUser, " + vbCrLf)
            sb.Append("     SCA.ModDate, " + vbCrLf)
            sb.Append("     GetDate() as StartDate,GetDate() as EndDate,1 as Active,0 as UseFlexTime, " + vbCrLf)
            ''sb.Append("     SS.StartDate, " + vbCrLf)
            ''sb.Append("     SS.EndDate, " + vbCrLf)
            ''sb.Append("     SS.Active, " + vbCrLf)
            ''sb.Append("     PS.UseFlexTime, " + vbCrLf)
            sb.Append("     PV.UseTimeClock, " + vbCrLf)
            sb.Append("     AUT.UnitTypeDescrip, " + vbCrLf)
            sb.Append("     'attendance' as Source, " + vbCrLf)
            sb.Append("     SCA.PostByException, " + vbCrLf)
            sb.Append("     0 as TardyProcessed, " + vbCrLf)
            sb.Append("     SCA.IsTardy, " + vbCrLf)
            sb.Append("     SC.SysStatusId " + vbCrLf)
            sb.Append("from " + vbCrLf)
            sb.Append("     arStudentClockAttendance SCA, " + vbCrLf)
            'sb.Append("     arStudentSchedules SS, " + vbCrLf)
            'sb.Append("     arProgSchedules PS, " + vbCrLf)
            sb.Append("     arPrgVersions PV, " + vbCrLf)
            sb.Append("     arPrograms P, " + vbCrLf)
            sb.Append("     arStuEnrollments SE, " + vbCrLf)
            sb.Append("     arStudent S, " + vbCrLf)
            sb.Append("     arAttUnitType AUT, " + vbCrLf)
            sb.Append("     syStatusCodes SC, " + vbCrLf)
            sb.Append("     sySysStatus SSC " + vbCrLf)
            If filterstudentgrpid IsNot Nothing AndAlso filterstudentgrpid <> "" Then
                sb.Append(" ,adLeadByLeadGroups LLG " + vbCrLf)
            End If
            sb.Append("where " + vbCrLf)
            'sb.Append("     SCA.StuEnrollId = SS.StuEnrollId " + vbCrLf)
            'sb.Append("     and SCA.ScheduleId = SS.ScheduleId " + vbCrLf)
            'sb.Append("     and SS.StuEnrollId = SE.StuEnrollId " + vbCrLf)
            'sb.Append("     and "+ vbCrLf)
            sb.Append("     SE.StudentId = S.StudentId " + vbCrLf)
            'sb.Append("     and SCA.ScheduleId = SS.ScheduleId " + vbCrLf)
            'sb.Append("     and SCA.ScheduleId = PS.ScheduleId " + vbCrLf)
            'sb.Append("     and PS.PrgVerId = PV.PrgVerId " + vbCrLf)
            sb.Append("     and PV.ProgId = P.ProgId " + vbCrLf)
            sb.Append("     and PV.UnitTypeId = AUT.UnitTypeId " + vbCrLf)
            sb.Append("     and SE.StatusCodeId = SC.StatusCodeId " + vbCrLf)
            sb.Append("     and SC.SysStatusId = SSC.SysStatusId " + vbCrLf)
            'sb.Append("     and SS.Active = 1 " + vbCrLf)
            If filterstudentgrpid IsNot Nothing AndAlso filterstudentgrpid <> "" Then
                sb.Append(" and SCA.StuEnrollId = LLG.StuEnrollId " + vbCrLf)
            End If
            If campusId <> "" Then
                sb.Append("     and SE.CampusId = '" & campusId & "'" + vbCrLf)
            End If

            ' add the params
            If filterProgId IsNot Nothing AndAlso filterProgId <> "" Then
                sb.AppendFormat("       and PV.ProgId = '{0}' {1}", filterProgId, vbCrLf)
            End If
            If filterPrgVerId IsNot Nothing AndAlso filterPrgVerId <> "" Then
                sb.AppendFormat("       and PV.PrgVerId = '{0}' {1}", filterPrgVerId, vbCrLf)
                sb.Append(" and PV.PrgVerid=SE.PrgVerID " + vbCrLf)

            End If
            If filterStuName IsNot Nothing AndAlso filterStuName <> "" Then
                ''modified by saraswathi to find the Student last name or first name.
                ''Changed on Nov 20 2008
                '  sb.AppendFormat("  and (S.FirstName like '%{0}%' or S.LastName like '%{0}%' ) ", filterStuName)

                Dim i As Integer = filterStuName.IndexOf(" ")
                Dim filterLastName As String = filterStuName.Trim()
                Dim filterFirstName As String = ""
                If i > 0 AndAlso i < filterStuName.Length - 1 Then
                    filterLastName = filterStuName.Substring(i, filterStuName.Length - i).Trim()
                    filterFirstName = filterStuName.Substring(0, i).Trim()
                End If
                ''Modified by saraswathi on jan 20 2009
                ''to fetch the students by firstname,lastname or fullname
                'If filterLastName <> "" Then
                '    sb.AppendFormat("       and( S.LastName like '{0}%' or S.FirstName like '{0}%'){1}", filterLastName, vbCrLf)
                'End If
                'If filterFirstName <> "" Then
                '    sb.AppendFormat("       or( S.FirstName like '{0}%' or S.LastName like '{0}%'){1}", filterFirstName, vbCrLf)
                'End If
                'If filterLastName <> "" And filterFirstName <> "" Then
                '    sb.AppendFormat("       or( S.LastName like '{0}%'  and S.FirstName like '{1}%')", filterLastName, filterFirstName)
                'End If


                If filterLastName <> "" And filterFirstName <> "" Then
                    sb.AppendFormat("       And(( S.LastName like '{0}%'  and S.FirstName like '{1}%')or ( S.LastName like '{1}%'  and S.FirstName like '{0}%') )", filterLastName, filterFirstName)
                ElseIf filterLastName <> "" Then
                    sb.AppendFormat("       and( S.LastName like '{0}%' or S.FirstName like '{0}%'){1}", filterLastName, vbCrLf)
                ElseIf filterFirstName <> "" Then
                    sb.AppendFormat("       and( S.FirstName like '{0}%' or S.LastName like '{0}%'){1}", filterFirstName, vbCrLf)
                End If

                'If filterLastName <> "" Then
                '    sb.AppendFormat("       and S.LastName like '{0}%' {1}", filterLastName, vbCrLf)
                'End If
                'If filterFirstName <> "" Then
                '    sb.AppendFormat("       and S.FirstName like '{0}%' {1}", filterFirstName, vbCrLf)
                'End If
            End If
            If filterStuStatus IsNot Nothing AndAlso filterStuStatus <> "" Then
                'sb.AppendFormat("       and SE.StatusCodeId = '{0}' {1}", filterStuStatus, vbCrLf)
                sb.AppendFormat(" and SE.StatusCodeId in ('")
                sb.AppendFormat(filterStuStatus)
                sb.AppendFormat(") ")
            End If
            'If filteroutofschoolfilter IsNot Nothing AndAlso filteroutofschoolfilter <> "" Then
            '    sb.AppendFormat(" and SE.StatusCodeId in ('")
            '    sb.AppendFormat(filteroutofschoolfilter)
            '    sb.AppendFormat(") ")
            'End If
            If filterStartDate IsNot Nothing AndAlso filterStartDate <> "" Then
                sb.AppendFormat("       and SCA.RecordDate >= '{0}' {1}", filterStartDate, vbCrLf)
            End If
            If filterEndDate IsNot Nothing AndAlso filterEndDate <> "" Then
                'Code Added By Vijay Ramteke on April 16, 2010 For Mantis Id 18882
                'sb.AppendFormat("       and SCA.RecordDate <= '{0}' {1}", filterStartDate, vbCrLf)
                sb.AppendFormat("       and SCA.RecordDate <= '{0}' {1}", filterEndDate, vbCrLf)
                'Code Added By Vijay Ramteke on April 16, 2010 For Mantis Id 18882
            End If
            If filterUseTimeClock IsNot Nothing AndAlso filterUseTimeClock <> "" Then
                If filterUseTimeClock.ToLower = "true" Or filterUseTimeClock = "1" Then
                    sb.Append("       and PV.UseTimeClock = 1" + vbCrLf)
                Else
                    sb.Append("       and (PV.UseTimeClock = 0 or PV.UseTimeClock is null)" + vbCrLf)
                End If
            End If
            If filterBadgeNum IsNot Nothing AndAlso filterBadgeNum <> "" Then
                sb.AppendFormat("       and SE.BadgeNumber = '{0}' {1}", filterBadgeNum, vbCrLf)
            End If
            If filterstudentgrpid IsNot Nothing AndAlso filterstudentgrpid <> "" Then
                sb.AppendFormat(" and LLG.LeadGrpId = '{0}' {1}", filterstudentgrpid, vbCrLf)
            End If
            ''Added by Saraswathi to filter based on cohortStartDate
            If filterCohortStartDate IsNot Nothing AndAlso filterCohortStartDate <> "" Then
                sb.AppendFormat(" and SE.CohortStartDate = '{0}' {1}", filterCohortStartDate, vbCrLf)
            End If
            sb.Append("  and SE.StuEnrollId=SCA.StuEnrollID " + vbCrLf)

            sb.Append("union " + vbCrLf)
            sb.Append("select Distinct " + vbCrLf)
            sb.Append("     SE.StuEnrollID, " + vbCrLf)
            'sb.Append("     SS.ScheduleId, " + vbCrLf)
            sb.Append(" '00000000-0000-0000-0000-000000000000' as ScheduleID, " + vbCrLf)
            sb.Append("     SCA.MeetDate, " + vbCrLf)
            ''Commented by Saraswathi Lakshmanan on august 28 2009
            ''To fix mantis case: Edit timeClock Scheduled Hours
            ''For any type of School the scheduled hours is taken from the arStudentClockAttendance table
            ' If boolUseTimeClock = False Then
            sb.Append("     SCA.Schedule, " + vbCrLf)
            'Else
            'sb.Append("   (select Distinct Total from arProgScheduleDetails  " + vbCrLf)
            'sb.Append("   where ScheduleId=SS.ScheduleId and " + vbCrLf)
            'sb.Append("   dw=DatePart(dw,SCA.MeetDate) - 1) as Schedule, " + vbCrLf)
            'End If
            sb.Append("     SCA.Actual, " + vbCrLf)
            sb.Append("     SCA.Tardy as Tardy," + vbCrLf)
            sb.Append("     '' as ModUser, " + vbCrLf)
            sb.Append("     '' as ModDate, " + vbCrLf)
            'sb.Append("     SS.StartDate, " + vbCrLf)
            'sb.Append("     SS.EndDate, " + vbCrLf)
            'sb.Append("     SS.Active, " + vbCrLf)
            'sb.Append("     PS.UseFlexTime, " + vbCrLf)
            sb.Append("     GetDate() as StartDate,GetDate() as EndDate,1 as Active,0 as UseFlexTime, " + vbCrLf)
            sb.Append("     PV.UseTimeClock, " + vbCrLf)
            sb.Append("     AUT.UnitTypeDescrip, " + vbCrLf)
            sb.Append("     'conversion' as Source, " + vbCrLf)
            sb.Append("     SCA.PostByException, " + vbCrLf)
            sb.Append("     0 as TardyProcessed, " + vbCrLf)
            sb.Append("     SCA.Tardy as IsTardy, " + vbCrLf)
            sb.Append("     SC.SysStatusId " + vbCrLf)
            sb.Append("from " + vbCrLf)
            sb.Append("     atConversionAttendance SCA, " + vbCrLf)
            'sb.Append("     arStudentSchedules SS, " + vbCrLf)
            'sb.Append("     arProgSchedules PS, " + vbCrLf)
            sb.Append("     arPrgVersions PV, " + vbCrLf)
            sb.Append("     arPrograms P, " + vbCrLf)
            sb.Append("     arStuEnrollments SE, " + vbCrLf)
            sb.Append("     arStudent S, " + vbCrLf)
            sb.Append("     arAttUnitType AUT, " + vbCrLf)
            sb.Append("     syStatusCodes SC, " + vbCrLf)
            sb.Append("     sySysStatus SSC " + vbCrLf)
            If filterstudentgrpid IsNot Nothing AndAlso filterstudentgrpid <> "" Then
                sb.Append(" ,adLeadByLeadGroups LLG " + vbCrLf)
            End If
            sb.Append("where " + vbCrLf)
            'sb.Append("      SCA.StuEnrollId = SS.StuEnrollId " + vbCrLf)
            'sb.Append("      and SS.ScheduleId = PS.ScheduleId " + vbCrLf)
            'sb.Append("      and SS.StuEnrollId = SE.StuEnrollId " + vbCrLf)
            'sb.Append("      and ")
            sb.Append("       SE.StudentId = S.StudentId " + vbCrLf)
            'sb.Append("      and PS.PrgVerId = SE.PrgVerId " + vbCrLf)
            sb.Append("      and SE.PrgVerId = PV.PrgVerId " + vbCrLf)
            sb.Append("      and PV.ProgId = P.ProgId " + vbCrLf)
            sb.Append("      and PV.UnitTypeId = AUT.UnitTypeId " + vbCrLf)
            sb.Append("      and SE.StatusCodeId = SC.StatusCodeId " + vbCrLf)
            sb.Append("      and SC.SysStatusId = SSC.SysStatusId " + vbCrLf)
            'sb.Append("      and SS.Active = 1 " + vbCrLf)
            If filterstudentgrpid IsNot Nothing AndAlso filterstudentgrpid <> "" Then
                sb.Append(" and SCA.StuEnrollId = LLG.StuEnrollId " + vbCrLf)
            End If
            If campusId <> "" Then
                sb.Append("     and SE.CampusId = '" & campusId & "'" + vbCrLf)
            End If

            ' add the params
            If filterProgId IsNot Nothing AndAlso filterProgId <> "" Then
                sb.AppendFormat("       and PV.ProgId = '{0}' {1}", filterProgId, vbCrLf)
            End If
            If filterPrgVerId IsNot Nothing AndAlso filterPrgVerId <> "" Then
                sb.AppendFormat("       and PV.PrgVerId = '{0}' {1}", filterPrgVerId, vbCrLf)
                sb.Append("      and PV.PrgVerid=SE.PrgVerID " + vbCrLf)
            End If
            If filterStuName IsNot Nothing AndAlso filterStuName <> "" Then
                Dim i As Integer = filterStuName.IndexOf(" ")
                Dim filterLastName As String = filterStuName.Trim()
                Dim filterFirstName As String = ""
                If i > 0 AndAlso i < filterStuName.Length - 1 Then
                    filterLastName = filterStuName.Substring(i, filterStuName.Length - i).Trim()
                    filterFirstName = filterStuName.Substring(0, i).Trim()
                End If
                'If filterLastName <> "" Then
                '    sb.AppendFormat("       and S.LastName like '{0}%' {1}", filterLastName, vbCrLf)
                'End If
                'If filterFirstName <> "" Then
                '    sb.AppendFormat("       and S.FirstName like '{0}%' {1}", filterFirstName, vbCrLf)
                'End If
                ''Modified by saraswathi on jan 20 2009
                ''to fetch the students by firstname,lastname or fullname


                If filterLastName <> "" And filterFirstName <> "" Then
                    sb.AppendFormat("       And(( S.LastName like '{0}%'  and S.FirstName like '{1}%')or ( S.LastName like '{1}%'  and S.FirstName like '{0}%') )", filterLastName, filterFirstName)
                ElseIf filterLastName <> "" Then
                    sb.AppendFormat("       and( S.LastName like '{0}%' or S.FirstName like '{0}%'){1}", filterLastName, vbCrLf)
                ElseIf filterFirstName <> "" Then
                    sb.AppendFormat("       and( S.FirstName like '{0}%' or S.LastName like '{0}%'){1}", filterFirstName, vbCrLf)
                End If


            End If
            'If filterStuStatus IsNot Nothing AndAlso filterStuStatus <> "" Then
            '    sb.AppendFormat("       and SE.StatusCodeId = '{0}' {1}", filterStuStatus, vbCrLf)
            'End If
            If filterStuStatus IsNot Nothing AndAlso filterStuStatus <> "" Then
                'sb.AppendFormat("       and SE.StatusCodeId = '{0}' {1}", filterStuStatus, vbCrLf)
                sb.AppendFormat(" and SE.StatusCodeId in ('")
                sb.AppendFormat(filterStuStatus)
                sb.AppendFormat(") ")
            End If
            'If filteroutofschoolfilter IsNot Nothing AndAlso filteroutofschoolfilter <> "" Then
            '    sb.AppendFormat(" and SE.StatusCodeId in ('")
            '    sb.AppendFormat(filteroutofschoolfilter)
            '    sb.AppendFormat(") ")
            'End If
            If filterStartDate IsNot Nothing AndAlso filterStartDate <> "" Then
                sb.AppendFormat("       and SCA.MeetDate >= '{0}' {1}", filterStartDate, vbCrLf)
            End If
            If filterEndDate IsNot Nothing AndAlso filterEndDate <> "" Then
                'Code Added By Vijay Ramteke on April 16, 2010 For Mantis Id 18882
                'sb.AppendFormat("       and SCA.MeetDate <= '{0}' {1}", filterStartDate, vbCrLf)
                sb.AppendFormat("       and SCA.MeetDate <= '{0}' {1}", filterEndDate, vbCrLf)
                'Code Added By Vijay Ramteke on April 16, 2010 For Mantis Id 18882
            End If
            If filterUseTimeClock IsNot Nothing AndAlso filterUseTimeClock <> "" Then
                If filterUseTimeClock.ToLower = "true" Or filterUseTimeClock = "1" Then
                    sb.Append("       and PV.UseTimeClock = 1" + vbCrLf)
                Else
                    sb.Append("       and (PV.UseTimeClock = 0 or PV.UseTimeClock is null)" + vbCrLf)
                End If
            End If
            If filterBadgeNum IsNot Nothing AndAlso filterBadgeNum <> "" Then
                sb.AppendFormat("       and SE.BadgeNumber = '{0}' {1}", filterBadgeNum, vbCrLf)
            End If
            If filterstudentgrpid IsNot Nothing AndAlso filterstudentgrpid <> "" Then
                sb.AppendFormat(" and LLG.LeadGrpId = '{0}' {1}", filterstudentgrpid, vbCrLf)
            End If
            ''Added by Saraswathi to filter based on cohortStartDate
            If filterCohortStartDate IsNot Nothing AndAlso filterCohortStartDate <> "" Then
                sb.AppendFormat(" and SE.CohortStartDate = '{0}' {1}", filterCohortStartDate, vbCrLf)
            End If

            sb.Append("       and SE.StuEnrollId=SCA.StuEnrollID " + vbCrLf)
            sb.Append("order by " + vbCrLf)
            sb.Append("     SE.StuEnrollId " + vbCrLf)
            'sb.Append("     SCA.RecordDate " + vbCrLf)
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        Public Shared Function GetClockHourAttendance(
                             ByVal stuEnrollId As String, ByVal filterStartDate As String, ByVal filterEndDate As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("select Distinct " + vbCrLf)
            sb.Append("     SE.StuEnrollID, " + vbCrLf)
            sb.Append("     SCA.ScheduleId, " + vbCrLf)
            sb.Append("     SCA.RecordDate, " + vbCrLf)
            sb.Append("     SCA.SchedHours, " + vbCrLf)
            sb.Append("     SCA.ActualHours, " + vbCrLf)
            sb.Append("     SCA.IsTardy as Tardy," + vbCrLf)
            sb.Append("     SCA.ModUser, " + vbCrLf)
            sb.Append("     SCA.ModDate, " + vbCrLf)
            sb.Append("     GetDate() as StartDate,GetDate() as EndDate,1 as Active,0 as UseFlexTime, " + vbCrLf)
            sb.Append("     PV.UseTimeClock, " + vbCrLf)
            sb.Append("     AUT.UnitTypeDescrip, " + vbCrLf)
            sb.Append("     'attendance' as Source, " + vbCrLf)
            sb.Append("     SCA.PostByException, " + vbCrLf)
            sb.Append("     0 as TardyProcessed, " + vbCrLf)
            sb.Append("     SCA.IsTardy " + vbCrLf)
            sb.Append("from " + vbCrLf)
            sb.Append("     arStudentClockAttendance SCA, " + vbCrLf)
            'sb.Append("     arStudentSchedules SS, " + vbCrLf)
            'sb.Append("     arProgSchedules PS, " + vbCrLf)
            sb.Append("     arPrgVersions PV, " + vbCrLf)
            sb.Append("     arPrograms P, " + vbCrLf)
            sb.Append("     arStuEnrollments SE, " + vbCrLf)
            sb.Append("     arStudent S, " + vbCrLf)
            sb.Append("     arAttUnitType AUT " + vbCrLf)
            sb.Append("where " + vbCrLf)
            sb.Append("     SE.StudentId = S.StudentId " + vbCrLf)
            sb.Append("     and PV.ProgId = P.ProgId " + vbCrLf)
            sb.Append("     and PV.UnitTypeId = AUT.UnitTypeId " + vbCrLf)
            sb.AppendFormat("       and SCA.RecordDate >= '{0}' {1}", filterStartDate, vbCrLf)
            sb.AppendFormat("       and SCA.RecordDate <= '{0}' {1}", filterEndDate, vbCrLf)
            sb.Append("     and SE.StuEnrollid = '" & stuEnrollId & "'" + vbCrLf)
            Return db.RunParamSQLDataSet(sb.ToString)

        End Function
#End Region


        ''CAmpusID added by Saraswathi lakshmanan on April 01 2010
        ''For Ross Snow DAys issue
        Public Shared Function SaveClockHourAttendanceArray(ByVal info() As ClockHourAttendanceInfo, ByVal user As String, ByVal CampusID As String) As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim connstr = MyAdvAppSettings.AppSettings("ConString")
            Dim conn As New OleDbConnection(connstr)
            ' Code Added by Vijay Ramteke on April 14, 2010
            Dim errMsg As String = ""
            Dim dtDB As New GetDateFromHoursDB
            Dim upDateExpGradDateDB As New StudentEnrollmentDB
            ' Code Added by Vijay Ramteke on April 14, 2010
            Dim sb As New StringBuilder()
            Try
                conn.Open()
                Dim trans As OleDb.OleDbTransaction = conn.BeginTransaction()
                For Each i As ClockHourAttendanceInfo In info

                    'Ignore 9999.0 or 999.0
                    If (i.ActualHours = 9999.0 OrElse i.ActualHours = 999.0) And i.SchedHours = 0 Then
                        Continue For
                    End If

                    Dim sql As String = ""
                    If Not i.UnitTypeDescrip.ToString.Trim() = "" Then
                        If i.UnitTypeDescrip.ToLower.ToString.Substring(0, 7) = "present" Then
                            ''CAmpusID added by Saraswathi lakshmanan on April 01 2010
                            ''Modified by SAraswathi lakshmanan
                            sql = GetAddOrUpdateSql(i, user, CampusID)
                            Dim cmd As New OleDbCommand(sql, conn, trans)
                            Try
                                cmd.ExecuteNonQuery()
                            Catch ex As System.Exception
                            End Try
                        End If
                    Else
                        'If i.ActualHours = 0 Then
                        '    sql = GetDeleteSql(i)
                        'Else
                        ''Modified By Saraswathi Lakshmanan
                        ''For Ross Snow Days issue
                        sql = GetAddOrUpdateSql(i, user, CampusID)
                        'End If
                        Dim cmd As New OleDbCommand(sql, conn, trans)
                        Try
                            cmd.ExecuteNonQuery()
                        Catch ex As System.Exception
                        End Try
                    End If
                Next
                trans.Commit()
                conn.Close()
                ''Code Added by Vijay Ramteke on April 14, 2010 Mantis Id 18850
                'Dim UseTimeClock As Boolean = False
                'If SingletonAppSettings.AppSettings("TrackSapAttendance", campusId).ToLower = "byday" Then
                '    For Each i As ClockHourAttendanceInfo In info
                '        Dim dtExpectedDate As DateTime = dtDB.GetDateFromHours(i.StuEnrollId, errMsg, UseTimeClock)
                '        If errMsg = "" And Not dtExpectedDate = DateTime.MaxValue And UseTimeClock = True Then
                '            upDateExpGradDateDB.UpdateExpectedGraduationDate(dtExpectedDate, i.StuEnrollId)
                '            errMsg = "Expected graduation date has been recalculated"
                '        End If
                '    Next
                'End If

                ''Code Added by Vijay Ramteke on April 14, 2010 Mantis Id 18850
                'Return errMsg
                Return ""
            Catch ex As System.Exception
                conn.Close()
                Return ex.Message
            End Try
            Return "Unspecified Error"
        End Function
        ''' <summary>
        ''' Updates a Clock Hour attendance info object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        ''' ''CAmpusId added by Saraswathi lakshmanan on April 01 2010
        ''' ''For Ross Snow Days issue
        Public Shared Function Update(ByVal info As ClockHourAttendanceInfo, ByVal user As String, ByVal CAmpusID As String) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                '   build the query
                Dim sql As String = GetUpdateSql(info, user, CAmpusID)
                ' run the query
                db.RunParamSQLExecuteNoneQuery(sql)

                Return "" ' return success
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function
        Public Shared Function GetStatusCodes(ByVal CampusId As String, Optional ByVal strStatus As String = "Active", Optional ByVal StudentEnrollmentStatus As String = "") As DataSet

            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            '   build the sql query
            Dim sb As New StringBuilder
            With sb
                .Append(" select Distinct t1.StatusCodeId,t1.StatusCodeDescrip " + vbCrLf)
                .Append(" from syStatusCodes t1,sySysStatus t2,arStuEnrollments t3,syStatuses t4 " + vbCrLf)
                .Append(" where t1.SysStatusId=t2.SysStatusId and t1.StatusCodeId=t3.StatusCodeId and ")
                If StudentEnrollmentStatus = "InSchool" Then
                    .Append(" t2.InSchool = 1 and ")
                ElseIf StudentEnrollmentStatus = "OutofSchool" Then
                    .Append(" t2.InSchool = 0 and ")
                End If
                .Append(" t1.StatusId=t4.StatusId " + vbCrLf)
                If strStatus = "Active" Then
                    .Append(" and t4.Status='Active' ")
                ElseIf strStatus = "Inactive" Then
                    .Append(" and t4.Status='Inactive' ")
                End If
                .Append("AND t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                .Append("ORDER BY t1.StatusCodeDescrip ")
            End With
            db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   return dataset
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function
        Public Sub PostClockAttendance(ByVal xmlRules As String)
            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                'Call the procedure to insert more than one record all at once

                db.AddParameter("@AttValues", xmlRules, SqlDbType.VarChar, , ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery_SP("USP_PostClockAttendance_Insert", Nothing)
            Catch ex As System.Exception
            Finally
                db.ClearParameters()
                db.CloseConnection()
            End Try
        End Sub
#Region "SQL Heleper Functions"
        ''Added by Saraswathi lakshmanan on Apruil 01 2010
        ''For Ross Snow Days issue
        Public Shared Function GetAddOrUpdateSql(ByVal info As ClockHourAttendanceInfo, ByVal user As String, ByVal CAmpusID As String) As String
            Dim sb As New StringBuilder()
            sb.Append(GetUpdateSql(info, user, CAmpusID))
            sb.Append("if @@rowcount = 0 " + vbCrLf)
            sb.Append("begin " + vbCrLf)
            sb.Append(GetInsertSql(info, user, CAmpusID))
            sb.Append("end " + vbCrLf)
            Return sb.ToString()
        End Function


        Public Shared Function GetAddOrUpdateSql_byClass(ByVal info As ClsSectAttendanceInfo, ByVal user As String, ByVal CAmpusID As String, ByVal drpunches() As DataRow) As String
            Dim sb As New StringBuilder()
            sb.Append(GetUpdateSql_byClass(info, user, CAmpusID, drpunches))
            sb.Append(vbCrLf + "  if @@rowcount = 0 " + vbCrLf)
            sb.Append(vbCrLf + " begin " + vbCrLf)
            sb.Append(vbCrLf + GetInsertSql_byClass(info, user, CAmpusID, drpunches))
            sb.Append(vbCrLf + "  end " + vbCrLf)
            Return sb.ToString()
        End Function
        Public Shared Function GetAddOrUpdateSqlForEnteredTimeCLock_byClass(ByVal info As ClsSectAttendanceInfo, ByVal user As String, ByVal CAmpusID As String, ByVal drpunches() As DataRow) As String
            Dim sb As New StringBuilder()
            sb.Append(GetUpdateSqlForEnteredTimeClock_byClass(info, user, CAmpusID))
            sb.Append(vbCrLf + "  if @@rowcount = 0 " + vbCrLf)
            sb.Append(vbCrLf + " begin " + vbCrLf)
            sb.Append(vbCrLf + GetInsertSql_byClass(info, user, CAmpusID, drpunches))
            sb.Append(vbCrLf + "  end " + vbCrLf)
            Return sb.ToString()
        End Function
        Public Shared Function GetDeleteSql(ByVal info As ClockHourAttendanceInfo) As String
            Dim sb As New StringBuilder
            sb.Append("DELETE [arStudentClockAttendance] " + vbCrLf)
            sb.AppendFormat("WHERE StuEnrollId='{0}' {1}", info.StuEnrollId, vbCrLf)
            sb.AppendFormat("   AND ScheduleId='{0}' {1}", info.ScheduleId, vbCrLf)
            sb.AppendFormat("   AND RecordDate='{0}' {1}", info.RecordDate.ToString(), vbCrLf)
            sb.Append(vbCrLf)
            Return sb.ToString()
        End Function
        Public Shared Function GetStudentSource(ByVal StuEnrollId As String) As String
            Dim db As New DataAccess
            Dim sbSourceCount As New StringBuilder
            Dim intRowCount As Integer = 0
            db.OpenConnection()

            With sbSourceCount
                .Append("select count(*) from atConversionAttendance where StuEnrollId=? ")
            End With
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Try
                intRowCount = db.RunParamSQLScalar(sbSourceCount.ToString)
                If intRowCount >= 1 Then
                    Return "conversion"
                Else
                    Return "attendance"
                End If
            Catch ex As System.Exception
                Return "attendance"
            Finally
                sbSourceCount.Remove(0, sbSourceCount.Length)
                db.ClearParameters()
                db.CloseConnection()
            End Try
        End Function
        ''' <summary>
        ''' Returns an update sql string given a ClockHourAttendanceInfo object
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        ''' ''CAmpusID added by Saraswathi lakshmanan on April 01 2010
        ''' ''For Ross SnowDays Issue
        Protected Shared Function GetUpdateSql(ByVal info As ClockHourAttendanceInfo, ByVal user As String, ByVal CampusID As String) As String
            Dim sb As New StringBuilder
            info.Source = GetStudentSource(info.StuEnrollId)
            If info.Source.ToString.ToLower = "attendance" Then
                If IsHoliday(info.RecordDate, CampusID) Then
                    Dim MyAdvAppSettings As AdvAppSettings
                    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                    Else
                        MyAdvAppSettings = New AdvAppSettings
                    End If

                    If MyAdvAppSettings.AppSettings("postattendanceonholiday") Is Nothing OrElse MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then
                        info.SchedHours = 0
                    End If

                End If
                sb.Append("UPDATE [arStudentClockAttendance] " + vbCrLf)
                sb.Append("SET " + vbCrLf)
                If info.SchedHours = 0 Then
                    sb.AppendFormat("     SchedHours = {0}, {1}", "0.0", vbCrLf)
                Else
                    sb.AppendFormat("     SchedHours = {0}, {1}", info.SchedHours, vbCrLf)
                End If
                If Not info.UnitTypeDescrip.ToString.Trim() = "" Then
                    If info.ActualHours = 0 And Not info.UnitTypeDescrip.ToLower.ToString.Substring(0, 7) = "present" Then
                        sb.AppendFormat("     ActualHours = {0}, {1}", "null", vbCrLf)
                    ElseIf info.ActualHours = 0 And info.UnitTypeDescrip.ToLower.ToString.Substring(0, 7) = "present" Then
                        sb.AppendFormat("     ActualHours = {0}, {1}", "0.00", vbCrLf)
                    ElseIf info.ActualHours = "999.00" Then
                        sb.AppendFormat("     ActualHours = {0}, {1}", "null", vbCrLf)
                    ElseIf info.ActualHours = "9999.00" Then
                        sb.AppendFormat("     ActualHours = {0}, {1}", "9999.00", vbCrLf)
                    Else
                        sb.AppendFormat("     ActualHours = {0}, {1}", info.ActualHours, vbCrLf)
                    End If
                Else
                    sb.AppendFormat("     ActualHours = {0}, {1}", info.ActualHours, vbCrLf)
                End If
                If info.PostByException = "yes" Then
                    sb.AppendFormat("     PostByException = '{0}', {1}", "yes", vbCrLf)
                Else
                    sb.AppendFormat("     PostByException = '{0}', {1}", "no", vbCrLf)
                End If
                sb.AppendFormat("     ModDate = '{0}', {1}", Date.Now.ToString(), vbCrLf)
                sb.AppendFormat("     ModUser = '{0}', {1}", user, vbCrLf)
                If info.Tardy = True Or info.Tardy = 1 Or info.Tardy = True Then
                    sb.AppendFormat("    IsTardy = {0} ,{1}", 1, vbCrLf)
                Else
                    sb.AppendFormat("    IsTardy = {0}, {1}", 0, vbCrLf)
                End If
                ''Added by Saraswathi 
                If info.TardyProcessed = 0 Then
                    sb.AppendFormat("    TardyProcessed = {0} {1}", 0, vbCrLf)
                Else
                    sb.AppendFormat("    TardyProcessed = {0} {1}", 1, vbCrLf)
                End If
                sb.AppendFormat("WHERE " + vbCrLf)
                sb.AppendFormat("     StuEnrollId = '{0}' {1}", info.StuEnrollId, vbCrLf)
                ' sb.AppendFormat("     and ScheduleId = '{0}' {1}", info.ScheduleId, vbCrLf)
                sb.AppendFormat("     and RecordDate = '{0}' {1}", info.RecordDate.ToString(), vbCrLf)
                sb.Append(vbCrLf)
                Return sb.ToString()
            Else
                ''CAmpusID added by Saraswathi lakshmanan on April 01 2010
                ''For Ross Snow Days Issue
                If IsHoliday(info.RecordDate, CampusID) Then
                    info.SchedHours = 0
                End If
                sb.Append("UPDATE [atConversionAttendance] " + vbCrLf)
                sb.Append("SET " + vbCrLf)
                If info.SchedHours = 0 Then
                    sb.AppendFormat("     Schedule = {0}, {1}", "0.0", vbCrLf)
                Else
                    sb.AppendFormat("     Schedule = {0}, {1}", info.SchedHours, vbCrLf)
                End If
                If Not info.UnitTypeDescrip.ToString.Trim() = "" Then
                    If info.ActualHours = 0 And Not info.UnitTypeDescrip.ToLower.ToString.Substring(0, 7) = "present" Then
                        sb.AppendFormat("     Actual = {0}, {1}", "null", vbCrLf)
                    ElseIf info.ActualHours = 0 And info.UnitTypeDescrip.ToLower.ToString.Substring(0, 7) = "present" Then
                        sb.AppendFormat("     Actual = {0}, {1}", "0.00", vbCrLf)
                    ElseIf info.ActualHours = "999.00" Then
                        sb.AppendFormat("     Actual = {0}, {1}", "null", vbCrLf)
                    ElseIf info.ActualHours = "9999.00" Then
                        sb.AppendFormat("     Actual = {0}, {1}", "9999.00", vbCrLf)
                    Else
                        sb.AppendFormat("     Actual = {0}, {1}", info.ActualHours, vbCrLf)
                    End If
                Else
                    sb.AppendFormat("     Actual = {0}, {1}", info.ActualHours, vbCrLf)
                End If
                If info.PostByException = "yes" Then
                    sb.AppendFormat("     PostByException = '{0}', {1}", "yes", vbCrLf)
                Else
                    sb.AppendFormat("     PostByException = '{0}', {1}", "no", vbCrLf)
                End If
                'sb.AppendFormat("     ModDate = '{0}', {1}", Date.Now.ToString(), vbCrLf)
                'sb.AppendFormat("     ModUser = '{0}', {1}", user, vbCrLf)
                If info.Tardy = True Or info.Tardy = 1 Or info.Tardy = True Then
                    sb.AppendFormat("    Tardy = {0} {1}", 1, vbCrLf)
                Else
                    sb.AppendFormat("    Tardy = {0} {1}", 0, vbCrLf)
                End If
                sb.AppendFormat("WHERE " + vbCrLf)
                sb.AppendFormat("     StuEnrollId = '{0}' {1}", info.StuEnrollId, vbCrLf)
                ''Commented by Saraswathi lakshmanan on Jan 27 2010
                ''To Update the Actual Hours for the student
                ''the actual Hours will be updated for the existing Schedule
                ''18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 

                'sb.AppendFormat("     and ScheduleId = '{0}' {1}", info.ScheduleId, vbCrLf)
                sb.AppendFormat("     and MeetDate = '{0}' {1}", info.RecordDate.ToString(), vbCrLf)
                sb.Append(vbCrLf)
                Return sb.ToString()
            End If
        End Function


        Protected Shared Function GetUpdateSql_byClass(ByVal info As ClsSectAttendanceInfo, ByVal user As String, ByVal CampusID As String, ByVal drpunches() As DataRow) As String
            Dim sb As New StringBuilder

            If IsHoliday_ByClass(info.MeetDate, CampusID, info.ClsSectMeetingId.ToString) Then
                info.Scheduled = 0
            End If


            Dim ActualHours As Decimal = 0
            Dim db As New DataAccess
            Dim sb1 As New StringBuilder
            Dim dr As OleDbDataReader

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            sb1.Append(" Select Actual from atClsSectAttendance  ")
            sb1.AppendFormat("WHERE " + vbCrLf)
            sb1.AppendFormat("     StuEnrollId = '{0}' {1}", info.StuEnrollId, vbCrLf)
            sb1.AppendFormat("     and Convert(varchar, MeetDate,101) =  '{0}'  {1}", Format(info.MeetDate, "MM/dd/yyyy"), vbCrLf)
            sb1.AppendFormat("     and ClsSectMeetingID = '{0}' {1}", info.ClsSectMeetingId.ToString, vbCrLf)
            dr = db.RunParamSQLDataReader(sb1.ToString)
            Do While dr.Read
                ActualHours = dr("Actual")
            Loop

            If Not dr.IsClosed Then dr.Close()

            Dim dtINPunchesfromDB As DataTable
            Dim dtOutPunchesfromDB As DataTable
            Dim dbsql As New SQLDataAccess
            dbsql.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
            sb1.Clear()
            Dim isOverlapping As Boolean
            isOverlapping = False

            If (ActualHours <> 0 Or ActualHours <> 999 Or ActualHours <> 9999) And (info.Actual <> 0 Or info.Actual <> 9999 Or info.Actual <> 999) Then
                sb1.Append(" Select PunchTime,PunchType,id from arStudentTimeClockPunches  ")
                sb1.AppendFormat("WHERE  STATUS=1 and " + vbCrLf)
                sb1.AppendFormat("     StuEnrollId = '{0}' {1}", info.StuEnrollId, vbCrLf)
                sb1.AppendFormat("     and Convert(varchar, PunchTime,101) =  '{0}'  {1}", Format(info.MeetDate, "MM/dd/yyyy"), vbCrLf)
                sb1.AppendFormat("     and ClsSectMeetingID = '{0}' {1}", info.ClsSectMeetingId.ToString, vbCrLf)
                sb1.AppendFormat("     and PunchType = {0} {1}", 1, vbCrLf)
                dtINPunchesfromDB = dbsql.RunParamSQLDataSet(sb1.ToString).Tables(0)

                sb1.Clear()
                sb1.Append(" Select PunchTime,PunchType,id from arStudentTimeClockPunches  ")
                sb1.AppendFormat("WHERE STATUS=1  and " + vbCrLf)
                sb1.AppendFormat("     StuEnrollId = '{0}' {1}", info.StuEnrollId, vbCrLf)
                sb1.AppendFormat("     and Convert(varchar, PunchTime,101) =  '{0}'  {1}", Format(info.MeetDate, "MM/dd/yyyy"), vbCrLf)
                sb1.AppendFormat("     and ClsSectMeetingID = '{0}' {1}", info.ClsSectMeetingId.ToString, vbCrLf)
                sb1.AppendFormat("     and PunchType = {0} {1}", 2, vbCrLf)
                dtOutPunchesfromDB = dbsql.RunParamSQLDataSet(sb1.ToString).Tables(0)

                If dtINPunchesfromDB.Rows.Count = dtOutPunchesfromDB.Rows.Count Then
                    Dim drfilePunchIN As New ArrayList
                    Dim drfilePunchOut As New ArrayList

                    For Each row In drpunches
                        If row("Status") = 1 Then
                            If row("PunchType") = 1 Then
                                drfilePunchIN.Add(row("PunchTime"))
                            Else
                                drfilePunchOut.Add(row("PunchTime"))
                            End If
                        End If
                    Next

                    ' four cases
                    '1. PUnch in and punc out is before the available set
                    '2. PUnch in and punch out is after the availble set
                    '3. PUnch in and punch out is inside the available set -- invaid
                    '4. PUnch in or punch out is within the available set -- invalid set
                    ''case 5 PUnch in before dbpunch in and punch out is after dbpunch out -- invaid
                    Dim dbrowindex As Integer
                    Dim Arrayrowindex As Integer
                    For Arrayrowindex = 0 To drfilePunchIN.Count - 1
                        If isOverlapping = False Then
                            For dbrowindex = 0 To dtINPunchesfromDB.Rows.Count - 1
                                ''CAse 1  PUnch in and punc out is before the available set
                                If drfilePunchIN(Arrayrowindex) < dtINPunchesfromDB.Rows(dbrowindex)("PunchTime") And drfilePunchOut(Arrayrowindex) < dtINPunchesfromDB.Rows(dbrowindex)("PunchTime") Then
                                    isOverlapping = False
                                    Continue For
                                End If
                                ''CAse 2  PUnch in and punch out is after the availble set
                                If drfilePunchIN(Arrayrowindex) > dtOutPunchesfromDB.Rows(dbrowindex)("PunchTime") And drfilePunchOut(Arrayrowindex) > dtOutPunchesfromDB.Rows(dbrowindex)("PunchTime") Then
                                    isOverlapping = False
                                    Continue For

                                End If
                                ''case 3 PUnch in and punch out is inside the available set -- invaid
                                If drfilePunchIN(Arrayrowindex) > dtINPunchesfromDB.Rows(dbrowindex)("PunchTime") And drfilePunchOut(Arrayrowindex) < dtOutPunchesfromDB.Rows(dbrowindex)("PunchTime") Then
                                    isOverlapping = True
                                    Exit For

                                End If
                                ''case 4 PUnch in or punch out is within the available set -- invalid set
                                If (drfilePunchIN(Arrayrowindex) > dtINPunchesfromDB.Rows(dbrowindex)("PunchTime") And drfilePunchIN(Arrayrowindex) < dtOutPunchesfromDB.Rows(dbrowindex)("PunchTime")) Or (drfilePunchOut(Arrayrowindex) > dtINPunchesfromDB.Rows(dbrowindex)("PunchTime") And drfilePunchOut(Arrayrowindex) < dtOutPunchesfromDB.Rows(dbrowindex)("PunchTime")) Then
                                    isOverlapping = True
                                    Exit For
                                End If
                                ''case 5 PUnch in before dbpunch in and punch out is after dbpunch out -- invaid
                                If drfilePunchIN(Arrayrowindex) < dtINPunchesfromDB.Rows(dbrowindex)("PunchTime") And drfilePunchOut(Arrayrowindex) > dtOutPunchesfromDB.Rows(dbrowindex)("PunchTime") Then
                                    isOverlapping = True
                                    Exit For

                                End If
                            Next
                        Else
                            Exit For
                        End If
                    Next
                End If
            End If

            If ((ActualHours <> 0 Or ActualHours <> 999 Or ActualHours <> 9999) And (info.Actual = 9999 Or info.Actual = 999)) Or isOverlapping = True Then
                sb1.Clear()
                sb1.Append("update arStudentTimeClockPUnches set Status=10 ")
                sb1.AppendFormat("WHERE " + vbCrLf)
                sb1.AppendFormat("     StuEnrollId = '{0}' {1}", info.StuEnrollId, vbCrLf)
                sb1.AppendFormat("     and Convert(varchar, PunchTime,101) ='{0}' {1}", Format(info.MeetDate, "MM/dd/yyyy"), vbCrLf)
                sb1.AppendFormat("     and ClsSectMeetingID = '{0}' {1}", info.ClsSectMeetingId.ToString, vbCrLf)
                db.RunParamSQLExecuteNoneQuery(sb1.ToString)

            End If

            sb.Append("UPDATE [atClsSectAttendance] " + vbCrLf)
            sb.Append("SET " + vbCrLf)
            If info.Scheduled = 0 Then
                sb.AppendFormat("     Scheduled = {0}, {1}", "0.0", vbCrLf)
            Else
                sb.AppendFormat("     Scheduled = {0}, {1}", info.Scheduled, vbCrLf)
            End If
            If Not info.UnitTypeId.ToString.Trim() = "" Then
                If info.Actual = 0 And Not info.UnitTypeId.ToLower.ToString = "present" Then
                    ' sb.AppendFormat("     Actual  = {0}, {1}", "null", vbCrLf)
                    sb.AppendFormat("     Actual  = {0}, {1}", "0", vbCrLf)
                ElseIf info.Actual = 0 And info.UnitTypeId.ToLower = "present" Then
                    sb.AppendFormat("     Actual  = {0}, {1}", "0.00", vbCrLf)
                ElseIf info.Actual = "999.00" Then
                    '  sb.AppendFormat("     Actual  = {0}, {1}", "null", vbCrLf)
                    sb.AppendFormat("     Actual  = {0}, {1}", "9999.00", vbCrLf)
                ElseIf info.Actual = "9999.00" Then
                    sb.AppendFormat("     Actual  = {0}, {1}", "9999.00", vbCrLf)
                Else
                    If (ActualHours <> 999 And ActualHours <> 9999) And isOverlapping = False Then
                        ''If a Valid entry already exists for the same ClsSectMeetingID then append the Actual Hours to the atClsSectAttendance Table
                        sb.AppendFormat("     Actual  =Actual+ {0}, {1}", info.Actual, vbCrLf)
                    ElseIf isOverlapping = True Then

                        sb.AppendFormat("     Actual  = {0}, {1}", "9999.00", vbCrLf)
                    Else
                        sb.AppendFormat("     Actual  = {0}, {1}", info.Actual, vbCrLf)
                    End If

                End If
            Else
                If ActualHours <> 999 And ActualHours <> 9999 And isOverlapping = False Then
                    ''If a Valid entry already exists for the same ClsSectMeetingID then append the Actual Hours to the atClsSectAttendance Table
                    sb.AppendFormat("     Actual  =Actual+ {0}, {1}", info.Actual, vbCrLf)
                ElseIf isOverlapping = True Then

                    sb.AppendFormat("     Actual  = {0}, {1}", "9999.00", vbCrLf)
                Else

                    sb.AppendFormat("     Actual  = {0}, {1}", info.Actual, vbCrLf)
                End If
            End If
            'If info.PostByException = "yes" Then
            '    sb.AppendFormat("     PostByException = '{0}', {1}", "yes", vbCrLf)
            'Else
            '    sb.AppendFormat("     PostByException = '{0}', {1}", "no", vbCrLf)
            'End If
            'sb.AppendFormat("     ModDate = '{0}', {1}", Date.Now.ToString(), vbCrLf)
            'sb.AppendFormat("     ModUser = '{0}', {1}", user, vbCrLf)
            If info.Tardy = True Or info.Tardy = 1 Or info.Tardy = True Then
                sb.AppendFormat("    Tardy = {0} {1} ", 1, vbCrLf)
            Else
                sb.AppendFormat("    Tardy = {0} {1}", 0, vbCrLf)
            End If
            ''Added by Saraswathi 
            'If info.TardyProcessed = 0 Then
            '    sb.AppendFormat("    TardyProcessed = {0} {1}", 0, vbCrLf)
            'Else
            '    sb.AppendFormat("    TardyProcessed = {0} {1}", 1, vbCrLf)
            'End If
            sb.AppendFormat("WHERE " + vbCrLf)
            sb.AppendFormat("     StuEnrollId = '{0}' {1}", info.StuEnrollId, vbCrLf)
            ' sb.AppendFormat("     and ScheduleId = '{0}' {1}", info.ScheduleId, vbCrLf)
            sb.AppendFormat("      and Convert(varchar, MeetDate,101) =  '{0}'  {1}", Format(info.MeetDate, "MM/dd/yyyy"), vbCrLf)
            sb.AppendFormat("     and ClsSectMeetingID = '{0}' {1}", info.ClsSectMeetingId.ToString, vbCrLf)


            sb.Append(vbCrLf)
            Return sb.ToString()

        End Function

        Protected Shared Function GetUpdateSqlForEnteredTimeClock_byClass(ByVal info As ClsSectAttendanceInfo, ByVal user As String, ByVal CampusID As String) As String
            Dim sb As New StringBuilder

            If IsHoliday_ByClass(info.MeetDate, CampusID, info.ClsSectMeetingId.ToString) Then
                info.Scheduled = 0
            End If


            Dim ActualHours As Decimal = 0
            Dim db As New DataAccess
            Dim sb1 As New StringBuilder
            Dim dr As OleDbDataReader

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            'sb1.Append(" Select Actual from atClsSectAttendance  ")
            'sb1.AppendFormat("WHERE " + vbCrLf)
            'sb1.AppendFormat("     StuEnrollId = '{0}' {1}", info.StuEnrollId, vbCrLf)
            'sb1.AppendFormat("     and Convert(varchar, MeetDate,101) =  '{0}'  {1}", Format(info.MeetDate, "MM/dd/yyyy"), vbCrLf)
            'sb1.AppendFormat("     and ClsSectMeetingID = '{0}' {1}", info.ClsSectMeetingId.ToString, vbCrLf)
            'dr = db.RunParamSQLDataReader(sb1.ToString)
            'Do While dr.Read
            '    ActualHours = dr("Actual")
            'Loop
            'If (ActualHours <> 0 Or ActualHours <> 999 Or ActualHours <> 9999) And (info.Actual = 9999 Or info.Actual = 999) Then
            '    sb1.Clear()
            '    sb1.Append("update arStudentTimeClockPUnches set Status=10 ")
            '    sb1.AppendFormat("WHERE " + vbCrLf)
            '    sb1.AppendFormat("     StuEnrollId = '{0}' {1}", info.StuEnrollId, vbCrLf)
            '    sb1.AppendFormat("     and Convert(varchar, PunchTime,101) ='{0}' {1}", Format(info.MeetDate, "MM/dd/yyyy"), vbCrLf)
            '    sb1.AppendFormat("     and ClsSectMeetingID = '{0}' {1}", info.ClsSectMeetingId.ToString, vbCrLf)
            '    db.RunParamSQLExecuteNoneQuery(sb1.ToString)

            'End If

            sb.Append("UPDATE [atClsSectAttendance] " + vbCrLf)
            sb.Append("SET " + vbCrLf)
            If info.Scheduled = 0 Then
                sb.AppendFormat("     Scheduled = {0}, {1}", "0.0", vbCrLf)
            Else
                sb.AppendFormat("     Scheduled = {0}, {1}", info.Scheduled, vbCrLf)
            End If
            If Not info.UnitTypeId.ToString.Trim() = "" Then
                If info.Actual = 0 And Not info.UnitTypeId.ToLower.ToString = "present" Then
                    sb.AppendFormat("     Actual  = {0}, {1}", "0", vbCrLf)
                    'sb.AppendFormat("     Actual  = {0}, {1}", "null", vbCrLf)
                ElseIf info.Actual = 0 And info.UnitTypeId.ToLower = "present" Then
                    sb.AppendFormat("     Actual  = {0}, {1}", "0.00", vbCrLf)
                ElseIf info.Actual = "999.00" Then
                    '  sb.AppendFormat("     Actual  = {0}, {1}", "null", vbCrLf)
                    sb.AppendFormat("     Actual  = {0}, {1}", "9999.00", vbCrLf)
                ElseIf info.Actual = "9999.00" Then
                    sb.AppendFormat("     Actual  = {0}, {1}", "9999.00", vbCrLf)
                Else
                    'If ActualHours <> 999 And ActualHours <> 9999 Then
                    '    ''If a Valid entry already exists for the same ClsSectMeetingID then append the Actual Hours to the atClsSectAttendance Table
                    '    sb.AppendFormat("     Actual  =Actual+ {0}, {1}", info.Actual, vbCrLf)
                    'Else
                    sb.AppendFormat("     Actual  = {0}, {1}", info.Actual, vbCrLf)
                    '  End If

                End If
            Else
                'If ActualHours <> 999 And ActualHours <> 9999 Then
                '    ''If a Valid entry already exists for the same ClsSectMeetingID then append the Actual Hours to the atClsSectAttendance Table
                '    sb.AppendFormat("     Actual  =Actual+ {0}, {1}", info.Actual, vbCrLf)
                'Else
                sb.AppendFormat("     Actual  = {0}, {1}", info.Actual, vbCrLf)
                '  End If
            End If
            'If info.PostByException = "yes" Then
            '    sb.AppendFormat("     PostByException = '{0}', {1}", "yes", vbCrLf)
            'Else
            '    sb.AppendFormat("     PostByException = '{0}', {1}", "no", vbCrLf)
            'End If
            'sb.AppendFormat("     ModDate = '{0}', {1}", Date.Now.ToString(), vbCrLf)
            'sb.AppendFormat("     ModUser = '{0}', {1}", user, vbCrLf)
            If info.Tardy = True Or info.Tardy = 1 Or info.Tardy = True Then
                sb.AppendFormat("    Tardy = {0} {1} ", 1, vbCrLf)
            Else
                sb.AppendFormat("    Tardy = {0} {1}", 0, vbCrLf)
            End If
            ''Added by Saraswathi 
            'If info.TardyProcessed = 0 Then
            '    sb.AppendFormat("    TardyProcessed = {0} {1}", 0, vbCrLf)
            'Else
            '    sb.AppendFormat("    TardyProcessed = {0} {1}", 1, vbCrLf)
            'End If
            sb.AppendFormat("WHERE " + vbCrLf)
            sb.AppendFormat("     StuEnrollId = '{0}' {1}", info.StuEnrollId, vbCrLf)
            ' sb.AppendFormat("     and ScheduleId = '{0}' {1}", info.ScheduleId, vbCrLf)
            sb.AppendFormat("      and Convert(varchar, MeetDate,101) =  '{0}'  {1}", Format(info.MeetDate, "MM/dd/yyyy"), vbCrLf)
            sb.AppendFormat("     and ClsSectMeetingID = '{0}' {1}", info.ClsSectMeetingId.ToString, vbCrLf)


            sb.Append(vbCrLf)
            Return sb.ToString()

        End Function


        ''CampusID added by Saraswathi lakshmanan on April 01 2010 for Ross Snow Days Issue
        Protected Shared Function IsHoliday(ByVal hDate As Date, ByVal CAmpusID As String) As Boolean
            Dim ds As New DataSet
            ds = (New HolidaysDB).GetAllHolidays(CAmpusID)
            Dim holidayTable As System.Data.DataTable = ds.Tables(0)
            For i As Integer = 0 To holidayTable.Rows.Count - 1
                If hDate >= holidayTable.Rows(i)("HolidayStartDate") And hDate <= holidayTable.Rows(i)("HolidayEndDate") Then
                    Return True
                End If
            Next
            Return False
        End Function
        Protected Shared Function IsHoliday_ByClass(ByVal hDate As Date, ByVal CAmpusID As String, ByVal ClsSectMeetingID As String) As Boolean
            Dim ds As New DataSet
            ds = (New HolidaysDB).GetAllHolidays_ByClass(CAmpusID, ClsSectMeetingID)
            Dim holidayTable As System.Data.DataTable = ds.Tables(0)
            Dim StartTIme As String = ""
            Dim EndTime As String = ""

            If ds.Tables.Count > 1 Then
                If ds.Tables(1).Rows.Count > 0 Then
                    StartTIme = Format(ds.Tables(1).Rows(0)("StartTime"), "HH:mm:ss")
                    EndTime = Format(ds.Tables(1).Rows(0)("EndTime"), "HH:mm:ss")
                End If
            End If
            For i As Integer = 0 To holidayTable.Rows.Count - 1
                'If hDate >= holidayTable.Rows(i)("HolidayStartDate") And hDate <= holidayTable.Rows(i)("HolidayEndDate") Then
                '    Return True
                'End If

                If hDate >= holidayTable.Rows(i)("HolidayStartDate") And hDate <= holidayTable.Rows(i)("HolidayEndDate") Then
                    If holidayTable.Rows(i)("AllDay") = False Then
                        If (StartTIme >= holidayTable.Rows(i)("StartTime") And StartTIme <= holidayTable.Rows(i)("EndTime")) Or (EndTime >= holidayTable.Rows(i)("StartTime") And EndTime <= holidayTable.Rows(i)("EndTime")) Then
                            'If (StartTime >= Format(result("StartTime"), "HH:mm:ss") And StartTime <= Format(result("EndTime"), "HH:mm:ss")) Or (EndTime >= Format(result("StartTime"), "HH:mm:ss") And EndTime <= Format(result("EndTime"), "HH:mm:ss")) Then
                            Return True
                        ElseIf (StartTIme <= holidayTable.Rows(i)("StartTime") And EndTime >= holidayTable.Rows(i)("EndTime")) Then
                            Return True
                        ElseIf (StartTIme >= holidayTable.Rows(i)("StartTime") And EndTime <= holidayTable.Rows(i)("EndTime")) Then
                            Return True
                        End If
                    Else
                        Return True

                    End If
                End If

            Next
            Return False
        End Function


        ''CampusID added by Saraswathi lakshmanan on April 01 2010
        ''For Ross Snow Days Issue

        Protected Shared Function GetInsertSql(ByVal info As ClockHourAttendanceInfo, ByVal user As String, ByVal CampusID As String) As String
            Dim sb As New StringBuilder
            info.Source = GetStudentSource(info.StuEnrollId)
            If info.Source.ToString.ToLower = "attendance" Then
                sb.Append("INSERT INTO [arStudentClockAttendance] " + vbCrLf)
                sb.Append("         ([StuEnrollId],[ScheduleId],[RecordDate] " + vbCrLf)
                sb.Append("         ,[SchedHours],[ActualHours] " + vbCrLf)
                sb.Append("         ,[IsTardy],[PostByException] " + vbCrLf)
                sb.Append("         ,[ModDate],[ModUser],[TardyProcessed]) " + vbCrLf)
                sb.Append("VALUES " + vbCrLf)
                sb.AppendFormat("     ('{0}', {1}", info.StuEnrollId, vbCrLf)
                sb.AppendFormat("     '{0}', {1}", info.ScheduleId, vbCrLf)
                sb.AppendFormat("     '{0}', {1}", info.RecordDate.ToString(), vbCrLf)
                'If 
                ''CampusID added by Saraswathi lakshmanan on April 01 2010
                ''For Ross Snow Days issue
                If IsHoliday(info.RecordDate, CampusID) Then
                    Dim MyAdvAppSettings As AdvAppSettings
                    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                    Else
                        MyAdvAppSettings = New AdvAppSettings
                    End If

                    If MyAdvAppSettings.AppSettings("postattendanceonholiday") Is Nothing OrElse MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then
                        info.SchedHours = 0
                    End If
                End If
                If info.SchedHours = 0 Then
                    sb.AppendFormat("     {0}, {1}", "0", vbCrLf)
                Else
                    sb.AppendFormat("     {0}, {1}", info.SchedHours, vbCrLf)
                End If
                If Not info.UnitTypeDescrip.ToString.Trim() = "" Then
                    If info.ActualHours = 0 And Not info.UnitTypeDescrip.ToLower.ToString.Substring(0, 7) = "present" Then
                        sb.AppendFormat("     {0}, {1}", "null", vbCrLf)
                    ElseIf info.ActualHours = 0 And info.UnitTypeDescrip.ToLower.ToString.Substring(0, 7) = "present" Then
                        sb.AppendFormat("     {0}, {1}", "0", vbCrLf)
                    ElseIf info.ActualHours = "999.00" Then
                        sb.AppendFormat("     {0}, {1}", "null", vbCrLf)
                    ElseIf info.ActualHours = "9999.00" Then
                        sb.AppendFormat("     {0}, {1}", "9999.00", vbCrLf)
                    Else
                        sb.AppendFormat("     {0}, {1}", info.ActualHours, vbCrLf)
                    End If
                Else
                    sb.AppendFormat("     {0}, {1}", info.ActualHours, vbCrLf)
                End If

                If info.Tardy = True Or info.Tardy = 1 Or info.Tardy = True Then
                    sb.AppendFormat("     {0}, {1}", 1, vbCrLf)
                Else
                    sb.AppendFormat("     {0}, {1}", 0, vbCrLf)
                End If
                If info.PostByException = "yes" Then
                    sb.AppendFormat("     '{0}', {1}", "yes", vbCrLf)
                Else
                    sb.AppendFormat("     '{0}', {1}", "no", vbCrLf)
                End If
                sb.AppendFormat("     '{0}', {1}", Date.Now, vbCrLf)
                sb.AppendFormat("     '{0}', {1}", user, vbCrLf)
                sb.AppendFormat("     {0}) {1}", info.TardyProcessed, vbCrLf)
                sb.Append(vbCrLf)
                Return sb.ToString()
            Else
                sb.Append("INSERT INTO [atConversionAttendance] " + vbCrLf)
                sb.Append("         ([StuEnrollId],[MeetDate] " + vbCrLf)
                sb.Append("         ,[Schedule],[Actual],[PostByException] " + vbCrLf)
                sb.Append("         ,[Tardy]) " + vbCrLf)
                sb.Append("VALUES " + vbCrLf)
                sb.AppendFormat("     ('{0}', {1}", info.StuEnrollId, vbCrLf)
                sb.AppendFormat("     '{0}', {1}", info.RecordDate.ToString(), vbCrLf)
                ''Added by SAraswathi lakshamana on April 01 2010
                ''For rtoss Snow Dsy issue
                If IsHoliday(info.RecordDate, CampusID) Then
                    info.SchedHours = "0"
                End If
                If info.SchedHours = 0 Then
                    sb.AppendFormat("     {0}, {1}", "0", vbCrLf)
                Else
                    sb.AppendFormat("     {0}, {1}", info.SchedHours, vbCrLf)
                End If
                If Not info.UnitTypeDescrip.ToString.Trim() = "" Then
                    If info.ActualHours = 0 And Not info.UnitTypeDescrip.ToLower.ToString.Substring(0, 7) = "present" Then
                        sb.AppendFormat("     {0}, {1}", "null", vbCrLf)
                    ElseIf info.ActualHours = 0 And info.UnitTypeDescrip.ToLower.ToString.Substring(0, 7) = "present" Then
                        sb.AppendFormat("     {0}, {1}", "0", vbCrLf)
                    ElseIf info.ActualHours = "999.00" Then
                        sb.AppendFormat("     {0}, {1}", "null", vbCrLf)
                    ElseIf info.ActualHours = "9999.00" Then
                        sb.AppendFormat("     {0}, {1}", "9999.00", vbCrLf)
                    Else
                        sb.AppendFormat("     {0}, {1}", info.ActualHours, vbCrLf)
                    End If
                Else
                    sb.AppendFormat("     {0}, {1}", info.ActualHours, vbCrLf)
                End If
                If info.PostByException = "yes" Then
                    sb.AppendFormat("     '{0}', {1}", "yes", vbCrLf)
                Else
                    sb.AppendFormat("     '{0}', {1}", "no", vbCrLf)
                End If
                If info.Tardy = True Or info.Tardy = 1 Or info.Tardy = True Then
                    sb.AppendFormat("     {0}) {1}", 1, vbCrLf)
                Else
                    sb.AppendFormat("     {0}) {1}", 0, vbCrLf)
                End If


                sb.Append(vbCrLf)
                Return sb.ToString()
            End If
        End Function

        Protected Shared Function GetInsertSql_byClass(ByVal info As ClsSectAttendanceInfo, ByVal user As String, ByVal CampusID As String, ByVal drpunches() As DataRow) As String
            Dim sb As New StringBuilder

            sb.Append("INSERT INTO [atClsSectattendance] " + vbCrLf)
            sb.Append("         (ClsSectAttId,[StuEnrollId],[ClsSectMeetingID],[MeetDate] " + vbCrLf)
            sb.Append("         ,[Scheduled],[Actual] " + vbCrLf)
            sb.Append("         ,[ClsSectionID] " + vbCrLf)


            sb.Append("         ,[Tardy]) " + vbCrLf)
            sb.Append("VALUES " + vbCrLf)


            sb.AppendFormat("    ( newId() , ")
            sb.AppendFormat("     '{0}', {1}", info.StuEnrollId, vbCrLf)
            sb.AppendFormat("     '{0}', {1}", info.ClsSectMeetingId, vbCrLf)
            sb.AppendFormat("     '{0}', {1}", info.MeetDate.ToString(), vbCrLf)
            'If 
            ''CampusID added by Saraswathi lakshmanan on April 01 2010
            ''For Ross Snow Days issue
            If IsHoliday_ByClass(info.MeetDate, CampusID, info.ClsSectMeetingId.ToString) Then
                info.Scheduled = "0"
            End If
            If info.Scheduled = 0 Then
                sb.AppendFormat("     {0}, {1}", "0", vbCrLf)
            Else
                sb.AppendFormat("     {0}, {1}", info.Scheduled, vbCrLf)
            End If
            If Not info.UnitTypeId.ToString.Trim() = "" Then
                If info.Actual = 0 And Not info.UnitTypeId.ToLower = "present" Then
                    ' sb.AppendFormat("     {0}, {1}", "null", vbCrLf)
                    sb.AppendFormat("     {0}, {1}", "0", vbCrLf)
                ElseIf info.Actual = 0 And info.UnitTypeId.ToLower = "present" Then
                    sb.AppendFormat("     {0}, {1}", "0", vbCrLf)
                ElseIf info.Actual = "999.00" Then
                    ' sb.AppendFormat("     {0}, {1}", "null", vbCrLf)
                    sb.AppendFormat("     {0}, {1}", "9999.00", vbCrLf)
                ElseIf info.Actual = "9999.00" Then
                    sb.AppendFormat("     {0}, {1}", "9999.00", vbCrLf)
                Else
                    sb.AppendFormat("     {0}, {1}", info.Actual, vbCrLf)
                End If
            Else
                sb.AppendFormat("     {0}, {1}", info.Actual, vbCrLf)
            End If


            sb.AppendFormat("  (SELECT ClsSectionID FROM dbo.arClsSectMeetings WHERE ClsSectMeetingId='{0}')   , {1}", info.ClsSectMeetingId, vbCrLf)



            If info.Tardy = True Or info.Tardy = 1 Or info.Tardy = True Then
                sb.AppendFormat("     {0} ){1}", 1, vbCrLf)
            Else
                sb.AppendFormat("     {0}){1}", 0, vbCrLf)
            End If

            sb.Append(vbCrLf)
            Return sb.ToString()

        End Function


        Public Shared Sub UpdateStudentFromFutureStartToCurrentlyAttending(ByVal StuEnrollId As String)
            Dim db As New DataAccess
            Dim sb As New StringBuilder

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            With sb
                .Append("Update arStuEnrollments set StatusCodeId=(select Distinct StatusCodeId from syStatusCodes where StatusCodeDescrip='Currently Attending') where StuEnrollId=? and StatusCodeId=(select Distinct StatusCodeId from syStatusCodes where StatusCodeDescrip='Future Start') ")
            End With
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
            Catch ex As System.Exception
            Finally
                db.ClearParameters()
                sb.Remove(0, sb.Length)
                db.CloseConnection()
            End Try
        End Sub
        Public Function GetAttendanceSummary(ByVal StuEnrollId As String, Optional ByVal PrgVerId As String = "") As ClockHourAttendanceInfo
            Dim getTardyTrackingDetailsInfo As New TardyAttendanceUnitInfo
            Dim getAttendanceSummaryInfo As New ClockHourAttendanceInfo
            Dim strUnitDescription As String = ""
            Dim intTardiesForProgramVersion As Integer = 0
            Dim strUnitTypeId As String = ""
            Dim intPresentUnitFromTardyCalculation As Decimal = 0.0
            Dim intAbsentUnitFromTardyCalculation As Decimal = 0.0

            Dim decActualTardyHours As Decimal = 0
            Dim decNumberofTardy As Decimal = 0
            Dim decAdjustedActual As Decimal = 0
            Dim decAdjustedAbsent As Decimal = 0
            Dim decAdjustedTardy As Decimal = 0
            Dim decTardiesMakingAbsence As Decimal = 0

            'Get Tardy Information Based on Program Version
            getTardyTrackingDetailsInfo = getTardyByProgramVersion(StuEnrollId)
            With getTardyTrackingDetailsInfo
                intTardiesForProgramVersion = .TardiesMakingAbsence
                strUnitTypeId = .UnitTypeId
            End With

            If strUnitTypeId.ToString.Trim = AttendanceUnitTypes.PresentAbsentGuid Then
                strUnitDescription = "pa"
            ElseIf strUnitTypeId.ToString.Trim = AttendanceUnitTypes.MinutesGuid Then
                strUnitDescription = "minutes"
            Else
                strUnitDescription = "hrs"
            End If

            'Calculate Tardy
            getTardyTrackingDetailsInfo = CalculateTardy(GetStudentSummaryDetails(StuEnrollId, strUnitDescription, PrgVerId), intTardiesForProgramVersion, strUnitDescription)
            With getTardyTrackingDetailsInfo
                intPresentUnitFromTardyCalculation = .PresentUnitAfterTardy
                intAbsentUnitFromTardyCalculation = .AbsentUnitAfterTardy
                decActualTardyHours = .actualtardyhours
                decNumberofTardy = .numberoftardy
                decAdjustedActual = .adjustedactual
                decAdjustedAbsent = .adjustedabsent
                decAdjustedTardy = .adjustedtardy
            End With

            getAttendanceSummaryInfo = StudentSummaryForm(StuEnrollId, strUnitDescription)
            With getAttendanceSummaryInfo
                .ActualDaysPresent = .TotalHoursPresent
                .TotalHoursPresent = (.TotalHoursPresent - intAbsentUnitFromTardyCalculation)
                .actualdaysabsent = .TotalHoursAbsent
                .TotalHoursAbsent = (.TotalHoursAbsent + intAbsentUnitFromTardyCalculation)
                .LDA = GetLDA(StuEnrollId)
                Try
                    .AttendancePercentage = System.Math.Round((.TotalHoursPresent / .TotalHoursSched) * 100, 2)
                Catch ex As System.Exception
                    .AttendancePercentage = 0
                End Try
                .actualtardyhours = decActualTardyHours
                .numberoftardy = decNumberofTardy
                .adjustedactual = decAdjustedActual
                .adjustedabsent = decAdjustedAbsent
                .adjustedtardy = decAdjustedTardy
                .numberoftardiesmaking1absence = intTardiesForProgramVersion
            End With
            Return getAttendanceSummaryInfo
        End Function
        Public Function GetAttendanceSummaryForDateRange(ByVal StuEnrollId As String, ByVal startDate As Date, ByVal cutOffDate As Date, Optional campusid As String = "") As ClockHourAttendanceInfo
            Dim getTardyTrackingDetailsInfo As New TardyAttendanceUnitInfo
            Dim getAttendanceSummaryInfo As New ClockHourAttendanceInfo
            Dim strUnitDescription As String = ""
            Dim intTardiesForProgramVersion As Integer = 0
            Dim strUnitTypeId As String = ""
            Dim intPresentUnitFromTardyCalculation As Decimal = 0.0
            Dim intAbsentUnitFromTardyCalculation As Decimal = 0.0

            Dim decActualTardyHours As Decimal = 0
            Dim decNumberofTardy As Decimal = 0
            Dim decAdjustedActual As Decimal = 0
            Dim decAdjustedAbsent As Decimal = 0
            Dim decAdjustedTardy As Decimal = 0
            Dim decTardiesMakingAbsence As Decimal = 0

            'Get Tardy Information Based on Program Version
            getTardyTrackingDetailsInfo = getTardyByProgramVersion(StuEnrollId)
            With getTardyTrackingDetailsInfo
                intTardiesForProgramVersion = .TardiesMakingAbsence
                strUnitTypeId = .UnitTypeId
            End With

            If strUnitTypeId.ToString.Trim = AttendanceUnitTypes.PresentAbsentGuid Then
                strUnitDescription = "pa"
            ElseIf strUnitTypeId.ToString.Trim = AttendanceUnitTypes.MinutesGuid Then
                strUnitDescription = "minutes"
            Else
                strUnitDescription = "hrs"
            End If

            'Calculate Tardy
            getTardyTrackingDetailsInfo = CalculateTardy(GetStudentSummaryDetailsForDateRange(StuEnrollId, startDate, cutOffDate, strUnitDescription), intTardiesForProgramVersion, strUnitDescription)
            With getTardyTrackingDetailsInfo
                intPresentUnitFromTardyCalculation = .PresentUnitAfterTardy
                intAbsentUnitFromTardyCalculation = .AbsentUnitAfterTardy
                decActualTardyHours = .actualtardyhours
                decNumberofTardy = .numberoftardy
                decAdjustedActual = .adjustedactual
                decAdjustedAbsent = .adjustedabsent
                decAdjustedTardy = .adjustedtardy
            End With

            getAttendanceSummaryInfo = StudentSummaryFormForDateRange(StuEnrollId, strUnitDescription, startDate, cutOffDate)
            With getAttendanceSummaryInfo
                .ActualDaysPresent = .TotalHoursPresent
                .TotalHoursPresent = (.TotalHoursPresent - intAbsentUnitFromTardyCalculation)
                .actualdaysabsent = .TotalHoursAbsent
                .TotalHoursAbsent = (.TotalHoursAbsent + intAbsentUnitFromTardyCalculation)
                .LDA = GetLDAForTermCutOffDate(StuEnrollId, cutOffDate)
                Try
                    .AttendancePercentage = System.Math.Round((.TotalHoursPresent / .TotalHoursSched) * 100, 2)
                Catch ex As System.Exception
                    .AttendancePercentage = 0
                End Try
                .actualtardyhours = decActualTardyHours
                .numberoftardy = decNumberofTardy
                .adjustedactual = decAdjustedActual
                .adjustedabsent = decAdjustedAbsent
                .adjustedtardy = decAdjustedTardy
                .numberoftardiesmaking1absence = intTardiesForProgramVersion
            End With
            Return getAttendanceSummaryInfo
        End Function

        Public Function GetAttendanceSummaryForCutOffDate(ByVal StuEnrollId As String, ByVal cutOffDate As Date, Optional campusid As String = "") As ClockHourAttendanceInfo
            Dim getTardyTrackingDetailsInfo As New TardyAttendanceUnitInfo
            Dim getAttendanceSummaryInfo As New ClockHourAttendanceInfo
            Dim strUnitDescription As String = ""
            Dim intTardiesForProgramVersion As Integer = 0
            Dim strUnitTypeId As String = ""
            Dim intPresentUnitFromTardyCalculation As Decimal = 0.0
            Dim intAbsentUnitFromTardyCalculation As Decimal = 0.0

            Dim decActualTardyHours As Decimal = 0
            Dim decNumberofTardy As Decimal = 0
            Dim decAdjustedActual As Decimal = 0
            Dim decAdjustedAbsent As Decimal = 0
            Dim decAdjustedTardy As Decimal = 0
            Dim decTardiesMakingAbsence As Decimal = 0

            'Get Tardy Information Based on Program Version
            getTardyTrackingDetailsInfo = getTardyByProgramVersion(StuEnrollId)
            With getTardyTrackingDetailsInfo
                intTardiesForProgramVersion = .TardiesMakingAbsence
                strUnitTypeId = .UnitTypeId
            End With

            If strUnitTypeId.ToString.Trim = AttendanceUnitTypes.PresentAbsentGuid Then
                strUnitDescription = "pa"
            ElseIf strUnitTypeId.ToString.Trim = AttendanceUnitTypes.MinutesGuid Then
                strUnitDescription = "minutes"
            Else
                strUnitDescription = "hrs"
            End If

            'Calculate Tardy
            getTardyTrackingDetailsInfo = CalculateTardy(GetStudentSummaryDetailsForCutOffDate(StuEnrollId, cutOffDate, strUnitDescription), intTardiesForProgramVersion, strUnitDescription)
            With getTardyTrackingDetailsInfo
                intPresentUnitFromTardyCalculation = .PresentUnitAfterTardy
                intAbsentUnitFromTardyCalculation = .AbsentUnitAfterTardy
                decActualTardyHours = .actualtardyhours
                decNumberofTardy = .numberoftardy
                decAdjustedActual = .adjustedactual
                decAdjustedAbsent = .adjustedabsent
                decAdjustedTardy = .adjustedtardy
            End With

            getAttendanceSummaryInfo = StudentSummaryFormForCutOffDate(StuEnrollId, strUnitDescription, cutOffDate)
            With getAttendanceSummaryInfo
                .ActualDaysPresent = .TotalHoursPresent
                .TotalHoursPresent = (.TotalHoursPresent - intAbsentUnitFromTardyCalculation)
                .actualdaysabsent = .TotalHoursAbsent
                .TotalHoursAbsent = (.TotalHoursAbsent + intAbsentUnitFromTardyCalculation)
                .LDA = GetLDAForTermCutOffDate(StuEnrollId, cutOffDate)
                Try
                    .AttendancePercentage = System.Math.Round((.TotalHoursPresent / .TotalHoursSched) * 100, 2)
                Catch ex As System.Exception
                    .AttendancePercentage = 0
                End Try
                .actualtardyhours = decActualTardyHours
                .numberoftardy = decNumberofTardy
                .adjustedactual = decAdjustedActual
                .adjustedabsent = decAdjustedAbsent
                .adjustedtardy = decAdjustedTardy
                .numberoftardiesmaking1absence = intTardiesForProgramVersion
            End With
            Return getAttendanceSummaryInfo
        End Function
        Public Function GetAttendanceSummaryForCutOffDate_SP(ByVal StuEnrollId As String, ByVal cutOffDate As Date, ByVal unitTypeId As String, ByVal tardiesMakingAbsence As Integer) As ClockHourAttendanceInfo
            Dim getTardyTrackingDetailsInfo As New TardyAttendanceUnitInfo
            Dim getAttendanceSummaryInfo As New ClockHourAttendanceInfo
            Dim strUnitDescription As String = ""
            Dim intTardiesForProgramVersion As Integer = 0
            Dim strUnitTypeId As String = ""
            Dim intPresentUnitFromTardyCalculation As Decimal = 0.0
            Dim intAbsentUnitFromTardyCalculation As Decimal = 0.0

            Dim decActualTardyHours As Decimal = 0
            Dim decNumberofTardy As Decimal = 0
            Dim decAdjustedActual As Decimal = 0
            Dim decAdjustedAbsent As Decimal = 0
            Dim decAdjustedTardy As Decimal = 0
            Dim decTardiesMakingAbsence As Decimal = 0

            'Get Tardy Information Based on Program Version
            ' getTardyTrackingDetailsInfo = getTardyByProgramVersion(StuEnrollId)
            With getTardyTrackingDetailsInfo
                intTardiesForProgramVersion = tardiesMakingAbsence
                strUnitTypeId = unitTypeId
            End With

            If strUnitTypeId.ToString.Trim = AttendanceUnitTypes.PresentAbsentGuid Then
                strUnitDescription = "pa"
            ElseIf strUnitTypeId.ToString.Trim = AttendanceUnitTypes.MinutesGuid Then
                strUnitDescription = "minutes"
            Else
                strUnitDescription = "hrs"
            End If

            'Calculate Tardy
            getTardyTrackingDetailsInfo = CalculateTardy(GetStudentSummaryDetailsForCutOffDate_SP(StuEnrollId, cutOffDate, strUnitDescription), intTardiesForProgramVersion, strUnitDescription)
            With getTardyTrackingDetailsInfo
                intPresentUnitFromTardyCalculation = .PresentUnitAfterTardy
                intAbsentUnitFromTardyCalculation = .AbsentUnitAfterTardy
                decActualTardyHours = .actualtardyhours
                decNumberofTardy = .numberoftardy
                decAdjustedActual = .adjustedactual
                decAdjustedAbsent = .adjustedabsent
                decAdjustedTardy = .adjustedtardy
            End With

            getAttendanceSummaryInfo = StudentSummaryFormForCutOffDate_SP(StuEnrollId, strUnitDescription, cutOffDate)
            With getAttendanceSummaryInfo
                .ActualDaysPresent = .TotalHoursPresent
                .TotalHoursPresent = (.TotalHoursPresent - intAbsentUnitFromTardyCalculation)
                .actualdaysabsent = .TotalHoursAbsent
                .TotalHoursAbsent = (.TotalHoursAbsent + intAbsentUnitFromTardyCalculation)
                '.LDA = GetLDAForTermCutOffDate(StuEnrollId, cutOffDate)
                Try
                    .AttendancePercentage = System.Math.Round((.TotalHoursPresent / .TotalHoursSched) * 100, 2)
                Catch ex As System.Exception
                    .AttendancePercentage = 0
                End Try
                .actualtardyhours = decActualTardyHours
                .numberoftardy = decNumberofTardy
                .adjustedactual = decAdjustedActual
                .adjustedabsent = decAdjustedAbsent
                .adjustedtardy = decAdjustedTardy
                .numberoftardiesmaking1absence = intTardiesForProgramVersion
            End With
            Return getAttendanceSummaryInfo
        End Function
        Public Function GetSAPOffsetDate(ByVal StuEnrollId As String, ByVal CampusId As String, ByVal TriggerValue As Decimal, ByVal TriggerUnitType As Integer, Optional ByVal IncludeExternship As Boolean = False, Optional ByVal InludeTransferHours As Boolean = False) As DateTime
            'Dim ds As DataSet
            Dim db As New SQLDataAccess
            Dim offsetDate As DateTime = DateTime.Today
            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
            '   connect to the database
            '   Execute the query

            db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollCampusId", New Guid(CampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@TriggerValue", TriggerValue, SqlDbType.Decimal, , ParameterDirection.Input)
            db.AddParameter("@TriggerUnitTypeId",TriggerUnitType, SqlDbType.Int, , ParameterDirection.Input)
            db.AddParameter("@IncludeExternship", IncludeExternship, SqlDbType.Bit, , ParameterDirection.Input)
            db.AddParameter("@IncludeTransferHours", InludeTransferHours, SqlDbType.Bit, , ParameterDirection.Input)
            'add cutOff date parameter
            db.AddParameter("@TriggerMetDate", offsetDate, SqlDbType.DateTime, , ParameterDirection.Output)
            Try
                Dim obj As Object = db.RunParamSQLScalar_SP_WithOutput("dbo.USP_TitleIV_GetTriggerMetDate")
                If(db.OutputParameters.Item("@TriggerMetDate").Value IsNot DBNull.Value) Then
                    offsetDate = Ctype(db.OutputParameters.Item("@TriggerMetDate").Value, DateTime)
                End If
                Return offsetDate

            Catch ex As Exception
            Finally
                db.CloseConnection()
            End Try
            Return offsetDate
        End Function
        Private Function CalculateTardy(ByVal dsTardyRows As DataSet, ByVal intTardiesMakingAbsence As Integer, ByVal UnitTypeDescrip As String) As TardyAttendanceUnitInfo
            Dim TardyDetailsInfo As New TardyAttendanceUnitInfo
            Dim intActualHours As Decimal = 0.0
            Dim intRowNumber As Integer = 0
            Dim intAbsentHours As Decimal = 0.0
            Dim intTardyHours As Decimal = 0.0
            Dim decActualTardy As Decimal = 0.0
            Dim arrTardyArray As New ArrayList
            Dim decActualTardyHrs As Decimal = 0.0

            If UnitTypeDescrip = "hrs" Or UnitTypeDescrip = "minutes" Then
                'decActualTardy += Math.Abs(CType(dr("Tardy"), Decimal))
                arrTardyArray = CalculateTardyForClockHourAndMinutes(dsTardyRows, intTardiesMakingAbsence, UnitTypeDescrip)
                intActualHours = arrTardyArray(0) 'get the attended hours after tardy
                intAbsentHours = arrTardyArray(1) 'get the absent hours after tardy
                intTardyHours = arrTardyArray(2) 'get the tardy hours 
                decActualTardyHrs = arrTardyArray(3) ' get actual tardy hrs
                decActualTardy = intTardyHours
            ElseIf UnitTypeDescrip = "pa" Then
                arrTardyArray = CalculateTardyForPA(dsTardyRows, intTardiesMakingAbsence, UnitTypeDescrip)
                intActualHours = arrTardyArray(0) 'get the attended hours after tardy
                intAbsentHours = arrTardyArray(1) 'get the absent hours after tardy
                intTardyHours = arrTardyArray(2) 'get the tardy hours 
                decActualTardyHrs = arrTardyArray(3)
                decActualTardy = intTardyHours '+= 'Math.Abs(CType(dr("Tardy"), Integer))
            End If
            With TardyDetailsInfo
                .PresentUnitAfterTardy = intActualHours
                .AbsentUnitAfterTardy = intAbsentHours
                .TardyUnit = intTardyHours
                .actualtardyhours = decActualTardyHrs
                .numberoftardy = intRowNumber
                .TardiesMakingAbsence = intTardiesMakingAbsence
                .adjustedactual = intActualHours
                .adjustedabsent = intAbsentHours
                .adjustedtardy = intTardyHours
            End With
            Return TardyDetailsInfo

            'For Each dr As DataRow In dsTardyRows.Tables(0).Rows
            '    intRowNumber += 1 ' gets current row number
            '    If UnitTypeDescrip = "pa" Then
            '        decActualTardy += Math.Abs(CType(dr("Tardy"), Integer))
            '    Else
            '        decActualTardy += Math.Abs(CType(dr("Tardy"), Decimal))
            '    End If
            '    If UnitTypeDescrip = "pa" Then
            '        If (intRowNumber Mod intTardiesMakingAbsence = 0) Then 'if there are 30 rows and if 10 tardies make an absence, once row count
            '            intActualHours += 0                        'reaches a multiple of 10, then we need to make tardy count
            '            intAbsentHours += 1
            '            intTardyHours += 0
            '        Else
            '            intActualHours += CType(dr("ActualHours"), Integer)
            '            intAbsentHours += 0
            '            intTardyHours += CType(dr("Tardy"), Integer)
            '        End If
            '    ElseIf UnitTypeDescrip = "minutes" Then
            '        If (intTardiesMakingAbsence >= 1 And intRowNumber Mod intTardiesMakingAbsence = 0) Then 'if rule is 2 Tardies makes absence then once it hits 
            '            intActualHours += 0                        'second row then we know its the tardy we need to address
            '            intAbsentHours += CType(dr("ActualHours"), Decimal)
            '            'intAbsentHours += CType(dr("Tardy"), Decimal)
            '            intTardyHours += 0
            '        Else
            '            intActualHours += CType(dr("ActualHours"), Decimal)
            '            intAbsentHours += 0
            '            intTardyHours += CType(dr("Tardy"), Decimal)
            '            If intTardyHours < 0 Then
            '                If CType(dr("Tardy"), Boolean) = True Then
            '                    intTardyHours += 1
            '                Else
            '                    intTardyHours += 0
            '                End If
            '            End If
            '        End If
            '    End If
            'Next
            'With TardyDetailsInfo
            '    .PresentUnitAfterTardy = intActualHours
            '    .AbsentUnitAfterTardy = intAbsentHours
            '    .TardyUnit = intTardyHours
            '    .actualtardyhours = decActualTardy
            '    .numberoftardy = intRowNumber
            '    .TardiesMakingAbsence = intTardiesMakingAbsence
            '    .adjustedactual = intActualHours
            '    .adjustedabsent = intAbsentHours
            '    .adjustedtardy = intTardyHours
            'End With
            'Return TardyDetailsInfo
        End Function
        Private Function CalculateTardyForClockHourAndMinutes(
                ByVal dsTardyRows As DataSet,
                ByVal NumberofTardiesEqualAbsence As Integer,
                ByVal UnitTypeDescrip As String) As ArrayList

            'Note : This function deals with days only when student is tardy
            Dim intRowNumber As Integer = 0
            Dim decAttendedHours As Decimal = 0
            Dim decAbsentHours As Decimal = 0
            Dim decTardyHours As Decimal = 0
            Dim decActualTardyHrs As Decimal = 0
            Dim arrDataAfterTardyManipulation As New ArrayList
            For Each dr As DataRow In dsTardyRows.Tables(0).Rows
                intRowNumber += 1
                'if rule is 2 Tardies makes absence then once it hits 
                'second row then we know its the tardy we need to address
                ''modified by saraswathi on jan 27 2008
                ''Since, introwno/nooftardiesmaking absence throwing divide by zero error, when tardiesmakingabsence is 0.
                If (NumberofTardiesEqualAbsence >= 1) Then
                    If intRowNumber Mod NumberofTardiesEqualAbsence = 0 Then
                        decAttendedHours += 0
                        decAbsentHours += CType(dr("ActualHours"), Decimal)
                        decTardyHours += 0
                        decActualTardyHrs += CType(dr("TardyCount"), Decimal)
                    Else
                        decAttendedHours += CType(dr("ActualHours"), Decimal)
                        decAbsentHours += 0
                        If UnitTypeDescrip = "hrs" Or UnitTypeDescrip = "minutes" Then
                            ''Modified by saraswathi on jan 28 2009
                            '' decActualTardyHrs += decTardyHours should be dr("TardyCount") tardycount and not dectardyhours
                            'decTardyHours += CType(dr("TardyCount"), Decimal)
                            'decActualTardyHrs += decTardyHours
                            decTardyHours += CType(dr("TardyCount"), Decimal)
                            decActualTardyHrs += CType(dr("TardyCount"), Decimal)
                        End If
                    End If

                Else
                    decAttendedHours += CType(dr("ActualHours"), Decimal)
                    decAbsentHours += 0
                    If UnitTypeDescrip = "hrs" Or UnitTypeDescrip = "minutes" Then
                        ''Modified by saraswathi on jan 28 2009
                        '' decActualTardyHrs += decTardyHours should be dr("TardyCount") tardycount and not dectardyhours
                        'decTardyHours += CType(dr("TardyCount"), Decimal)
                        'decActualTardyHrs += decTardyHours
                        decTardyHours += CType(dr("TardyCount"), Decimal)
                        decActualTardyHrs += CType(dr("TardyCount"), Decimal)

                    End If
                End If
            Next
            With arrDataAfterTardyManipulation
                .Add(decAttendedHours)
                .Add(decAbsentHours)
                .Add(decTardyHours)
                .Add(decActualTardyHrs)
            End With
            Return arrDataAfterTardyManipulation
        End Function
        Private Function CalculateTardyForPA(
            ByVal dsTardyRows As DataSet,
            ByVal NumberofTardiesEqualAbsence As Integer,
            ByVal UnitTypeDescrip As String) As ArrayList

            'Note : This function deals with days only when student is tardy
            Dim intRowNumber As Integer = 0
            Dim intAttendedHours As Integer = 0
            Dim intAbsentHours As Integer = 0
            Dim intTardyHours As Integer = 0
            ''Added by Saraswathi lakshmanan on Mrach 23 2009
            ''Actual tardy and Adjusted tardy  calculateion
            Dim adjustedtardy As Decimal = 0
            Dim arrDataAfterTardyManipulation As New ArrayList
            For Each dr As DataRow In dsTardyRows.Tables(0).Rows
                intRowNumber += 1
                'if rule is 2 Tardies makes absence then once it hits 
                'second row then we know its the tardy we need to address
                ''Modified by Saraswathi on march 03 2009
                ''Since, introwno/nooftardiesmaking absence throwing divide by zero error, when tardiesmakingabsence is 0.
                If (NumberofTardiesEqualAbsence >= 1) Then

                    If intRowNumber Mod NumberofTardiesEqualAbsence = 0 Then
                        intAttendedHours += 0                        'reaches a multiple of 10, then we need to make tardy count
                        intAbsentHours += 1
                        intTardyHours += 1
                        adjustedtardy += 0
                    Else
                        intAttendedHours += CType(dr("ActualHours"), Integer)
                        intAbsentHours += 0
                        ''modified by Saraswathi nLakshmanan
                        ''on MArch 3 2009
                        '' Since tardy is boolean, Adding one, if it is true.
                        ''   intTardyHours += CType(dr("Tardy"), Integer)
                        If dr("Tardy") = True Then
                            intTardyHours += 1
                            ''Added by Saraswathi on march 23 2009
                            adjustedtardy += 1
                        Else
                            intTardyHours += 0
                        End If

                    End If
                Else
                    intAttendedHours += CType(dr("ActualHours"), Integer)
                    intAbsentHours += 0
                    ''modified by Saraswathi Lakshmanan
                    ''on MArch 3 2009
                    '' Since tardy is boolean, Adding one, if it is true.
                    ''   intTardyHours += CType(dr("Tardy"), Integer)
                    If dr("Tardy") = True Then
                        intTardyHours += 1
                        ''Added by Saraswathi on march 23 2009
                        adjustedtardy += 1
                    Else
                        intTardyHours += 0
                    End If
                End If

            Next
            With arrDataAfterTardyManipulation
                .Add(intAttendedHours)
                .Add(intAbsentHours)
                .Add(adjustedtardy)
                .Add(intTardyHours)
            End With
            Return arrDataAfterTardyManipulation
        End Function

        Public Function StudentSummaryForm(ByVal StuEnrollId As String, ByVal AttUnitType As String) As ClockHourAttendanceInfo
            Dim sb As New StringBuilder
            Dim sb1 As New StringBuilder
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            With sb
                If AttUnitType.ToString.ToLower = "pa" Then  'if unit type is Present Absent
                    .Append(" Select Distinct " + vbCrLf)
                    .Append("   SCA.StuEnrollId, " + vbCrLf)
                    .Append("   (select Sum(SchedHours) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and SchedHours>=1.00 and (ActualHours <> 999.00 and ActualHours <> 9999.00)) as SchedHours," + vbCrLf)
                    .Append("   (select Sum(ActualHours) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (ActualHours>=1.00 and ActualHours <> 999.00 and ActualHours <> 9999.00)) as TotalPresentHours," + vbCrLf)
                    .Append("   (select Sum(SchedHours-ActualHours) from arStudentClockAttendance " + vbCrLf)
                    '.Append("   where StuEnrollId = SCA.StuEnrollId and (ActualHours=0.00 and SchedHours>=1.00 and PostByException <> 'yes')) " + vbCrLf)
                    ''Query modified by Saraswathi
                    ''on March 3 2009
                    ''The PostbyException condiion is removed.
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (ActualHours<>999.0 and ActualHours<>9999.0 ) and (Schedhours>ActualHours)) " + vbCrLf)

                    .Append("   as TotalHoursAbsent," + vbCrLf)
                    .Append("   (select Sum(ActualHours-SchedHours) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and " + vbCrLf)
                    .Append("   ((ActualHours >=0.00 and ActualHours <> 999.00 and ActualHours <> 9999.00) and (SchedHours<ActualHours)))" + vbCrLf)
                    .Append("   as TotalMakeUpHours," + vbCrLf)
                    .Append("   (select Sum(t4.Total) from " + vbCrLf)
                    .Append("   arStuEnrollments t1,arStudentSchedules t2, arProgSchedules t3, arProgScheduleDetails t4 " + vbCrLf)
                    .Append("   where t1.StuEnrollId = t2.StuEnrollId and t1.PrgVerId=t3.PrgVerId " + vbCrLf)
                    .Append("   and t2.ScheduleId=t3.ScheduleId and t3.scheduleId=t4.ScheduleId and " + vbCrLf)
                    .Append("   t2.StuEnrollId=SCA.StuEnrollId  and t4.total is not null) as ScheduleHoursByWeek, " + vbCrLf)
                    .Append("  (select max(RecordDate) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (ActualHours>=1.00 and ActualHours <> 999.00 and ActualHours <> 9999.00)) as LastDateAttended, " + vbCrLf)
                    .Append("   (select count(*) from  arStudentClockAttendance where StuEnrollId = SCA.StuEnrollId and IsTardy=1) as TardyCount " + vbCrLf)
                    .Append("   from arStudentClockAttendance SCA " + vbCrLf)
                    .Append("   where SCA.StuEnrollId='" + StuEnrollId + "'" + vbCrLf)
                ElseIf AttUnitType.ToString.ToLower = "minutes" Then
                    .Append(" Select Distinct " + vbCrLf)
                    .Append("   SCA.StuEnrollId, " + vbCrLf)
                    .Append("   (select Sum(SchedHours) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and SchedHours>=0.00 and (ActualHours is not null and ActualHours <> 999.00 and ActualHours <> 9999.00)) as SchedHours," + vbCrLf)
                    .Append("   (select Sum(ActualHours) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (ActualHours>=0.00 and ActualHours <> 999.00 and ActualHours <> 9999.00)) as TotalPresentHours," + vbCrLf)
                    .Append("   (select Sum(SchedHours-ActualHours) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and ( ActualHours <> 999.00 and ActualHours <> 9999.00 and SchedHours>=0.00 and SchedHours>ActualHours)) " + vbCrLf)
                    .Append("   as TotalHoursAbsent," + vbCrLf)
                    .Append("   (select Sum(ActualHours-SchedHours) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and " + vbCrLf)
                    .Append("   ((ActualHours>=0.00 and ActualHours <> 999.00 and ActualHours <> 9999.00) and (SchedHours<ActualHours)))" + vbCrLf)
                    .Append("   as TotalMakeUpHours," + vbCrLf)
                    .Append("   (select Sum(t4.Total) from " + vbCrLf)
                    .Append("   arStuEnrollments t1,arStudentSchedules t2, arProgSchedules t3, arProgScheduleDetails t4 " + vbCrLf)
                    .Append("   where t1.StuEnrollId = t2.StuEnrollId and t1.PrgVerId=t3.PrgVerId " + vbCrLf)
                    .Append("   and t2.ScheduleId=t3.ScheduleId and t3.scheduleId=t4.ScheduleId and " + vbCrLf)
                    .Append("   t2.StuEnrollId=SCA.StuEnrollId  and t4.total is not null) as ScheduleHoursByWeek, " + vbCrLf)
                    .Append("  (select max(RecordDate) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (ActualHours>=1.00 and ActualHours <> 999.00 and ActualHours <> 9999.00)) as LastDateAttended, " + vbCrLf)
                    .Append("   (select count(*) from  arStudentClockAttendance where StuEnrollId = SCA.StuEnrollId and IsTardy=1) as TardyCount " + vbCrLf)
                    .Append("   from arStudentClockAttendance SCA " + vbCrLf)
                    .Append("   where SCA.StuEnrollId='" + StuEnrollId + "'" + vbCrLf)
                    ''Modified by Saraswathi for Timeclock Schools
                    ''ON Jan 27 2009
                    ''Actual hr>1 calculation removed
                ElseIf AttUnitType.ToString.ToLower = "hrs" Then
                    .Append(" Select Distinct " + vbCrLf)
                    .Append("   SCA.StuEnrollId, " + vbCrLf)
                    .Append("   (select Sum(SchedHours) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and  (ActualHours is not null and ActualHours <> 999.00 and ActualHours <> 9999.00)) as SchedHours," + vbCrLf)
                    .Append(" (select Sum(ActualHours) from arStudentClockAttendance  " + vbCrLf)
                    .Append(" where StuEnrollId = SCA.StuEnrollId and ( ActualHours <> 999.00 and ActualHours <> 9999.00)) " + vbCrLf)
                    .Append(" as TotalPresentHours, " + vbCrLf)
                    .Append(" (select Sum(SchedHours-ActualHours) from arStudentClockAttendance " + vbCrLf)
                    .Append(" where StuEnrollId = SCA.StuEnrollId and " + vbCrLf)
                    .Append(" (ActualHours <> 999.00 and ActualHours <> 9999.00 and  (ActualHours < SchedHours))) " + vbCrLf)
                    .Append(" as TotalHoursAbsent, " + vbCrLf)
                    .Append(" (select Sum(ActualHours-SchedHours) from arStudentClockAttendance  " + vbCrLf)
                    .Append(" where StuEnrollId = SCA.StuEnrollId and " + vbCrLf)
                    .Append(" (( ActualHours <> 999.00 and ActualHours <> 9999.00) and " + vbCrLf)
                    .Append(" (ActualHours >SchedHours))) " + vbCrLf)
                    .Append("  as TotalMakeUpHours, " + vbCrLf)
                    .Append("   (select Sum(t4.Total) from " + vbCrLf)
                    .Append("   arStuEnrollments t1,arStudentSchedules t2, arProgSchedules t3, arProgScheduleDetails t4 " + vbCrLf)
                    .Append("   where t1.StuEnrollId = t2.StuEnrollId and t1.PrgVerId=t3.PrgVerId " + vbCrLf)
                    .Append("   and t2.ScheduleId=t3.ScheduleId and t3.scheduleId=t4.ScheduleId and " + vbCrLf)
                    .Append("   t2.StuEnrollId=SCA.StuEnrollId  and t4.total is not null) as ScheduleHoursByWeek, " + vbCrLf)
                    .Append("  (select max(RecordDate) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (ActualHours <> 999.00 and ActualHours <> 9999.00)) as LastDateAttended, " + vbCrLf)
                    .Append("   (select count(*) from  arStudentClockAttendance where StuEnrollId = SCA.StuEnrollId and IsTardy=1) as TardyCount " + vbCrLf)
                    .Append("   from arStudentClockAttendance SCA " + vbCrLf)
                    .Append("   where SCA.StuEnrollId='" + StuEnrollId + "'" + vbCrLf)
                Else
                    .Append(" Select Distinct " + vbCrLf)
                    .Append("   SCA.StuEnrollId, " + vbCrLf)
                    .Append("   (select Sum(SchedHours) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and SchedHours>=1.00 and (ActualHours is not null and ActualHours <> 999.00 and ActualHours <> 9999.00)) as SchedHours," + vbCrLf)
                    .Append(" (select Sum(ActualHours) from arStudentClockAttendance  " + vbCrLf)
                    .Append(" where StuEnrollId = SCA.StuEnrollId and (ActualHours>=1.00 and ActualHours <> 999.00 and ActualHours <> 9999.00)) " + vbCrLf)
                    .Append(" as TotalPresentHours, " + vbCrLf)
                    .Append(" (select Sum(SchedHours-ActualHours) from arStudentClockAttendance " + vbCrLf)
                    .Append(" where StuEnrollId = SCA.StuEnrollId and " + vbCrLf)
                    .Append(" (ActualHours <> 999.00 and ActualHours <> 9999.00 and SchedHours>=1.00 and (ActualHours < SchedHours))) " + vbCrLf)
                    .Append(" as TotalHoursAbsent, " + vbCrLf)
                    .Append(" (select Sum(ActualHours) from arStudentClockAttendance  " + vbCrLf)
                    .Append(" where StuEnrollId = SCA.StuEnrollId and " + vbCrLf)
                    .Append(" ((ActualHours>=1.00 and ActualHours <> 999.00 and ActualHours <> 9999.00) and " + vbCrLf)
                    .Append(" (SchedHours is NULL or SchedHours=0.00 or SchedHours=999.00))) " + vbCrLf)
                    .Append("  as TotalMakeUpHours, " + vbCrLf)
                    .Append("   (select Sum(t4.Total) from " + vbCrLf)
                    .Append("   arStuEnrollments t1,arStudentSchedules t2, arProgSchedules t3, arProgScheduleDetails t4 " + vbCrLf)
                    .Append("   where t1.StuEnrollId = t2.StuEnrollId and t1.PrgVerId=t3.PrgVerId " + vbCrLf)
                    .Append("   and t2.ScheduleId=t3.ScheduleId and t3.scheduleId=t4.ScheduleId and " + vbCrLf)
                    .Append("   t2.StuEnrollId=SCA.StuEnrollId  and t4.total is not null) as ScheduleHoursByWeek, " + vbCrLf)
                    .Append("  (select max(RecordDate) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (ActualHours>=1.00 and ActualHours <> 999.00 and ActualHours <> 9999.00)) as LastDateAttended, " + vbCrLf)
                    .Append("   (select count(*) from  arStudentClockAttendance where StuEnrollId = SCA.StuEnrollId and IsTardy=1) as TardyCount " + vbCrLf)
                    .Append("   from arStudentClockAttendance SCA " + vbCrLf)
                    .Append("   where SCA.StuEnrollId='" + StuEnrollId + "'" + vbCrLf)
                End If
            End With
            With sb1
                If AttUnitType.ToString.ToLower = "pa" Then  'if unit type is Present Absent
                    .Append(" Select Distinct " + vbCrLf)
                    .Append("   SCA.StuEnrollId, " + vbCrLf)
                    .Append("   (select Sum(Schedule) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and Schedule>=1.00 and (Actual <> 999.00 and Actual <> 9999.00)) as SchedHours," + vbCrLf)
                    .Append("   (select Sum(Actual) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (Actual>=1.00 and Actual <> 999.00 and Actual <> 9999.00)) as TotalPresentHours," + vbCrLf)
                    .Append("   (select Sum(Schedule-Actual) from atConversionAttendance " + vbCrLf)

                    ''Query modified by Saraswathi
                    ''on March 3 2009
                    ''The PostbyException condiion is removed.
                    '.Append("   where StuEnrollId = SCA.StuEnrollId and (Actual <> 999.00 and Actual <> 9999.00 and Schedule>=1.00) and PostByException <> 'yes') " + vbCrLf)

                    .Append("   where StuEnrollId = SCA.StuEnrollId and (Actual <> 999.00 and Actual <> 9999.00 and Schedule>=1.00) ) " + vbCrLf)

                    .Append("   as TotalHoursAbsent," + vbCrLf)
                    .Append("   (select Sum(Actual) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and " + vbCrLf)
                    .Append("   ((Actual>=1.00 and Actual<> 999.00 and Actual <> 9999.00) and (Schedule is NULL or Schedule=0.00)))" + vbCrLf)
                    .Append("   as TotalMakeUpHours," + vbCrLf)
                    .Append("   (select Sum(t4.Total) from " + vbCrLf)
                    .Append("   arStuEnrollments t1,arStudentSchedules t2, arProgSchedules t3, arProgScheduleDetails t4 " + vbCrLf)
                    .Append("   where t1.StuEnrollId = t2.StuEnrollId and t1.PrgVerId=t3.PrgVerId " + vbCrLf)
                    .Append("   and t2.ScheduleId=t3.ScheduleId and t3.scheduleId=t4.ScheduleId and " + vbCrLf)
                    .Append("   t2.StuEnrollId=SCA.StuEnrollId  and t4.total is not null) as ScheduleHoursByWeek, " + vbCrLf)
                    .Append("  (select max(MeetDate) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (Actual>=1.00 and Actual <> 999.00 and Actual <> 9999.00)) as LastDateAttended, " + vbCrLf)
                    .Append("   (select Count(*) from  atConversionAttendance where StuEnrollId = SCA.StuEnrollId and Tardy=1) as TardyCount " + vbCrLf)
                    .Append("   from atConversionAttendance SCA " + vbCrLf)
                    .Append("   where SCA.StuEnrollId='" + StuEnrollId + "'" + vbCrLf)
                ElseIf AttUnitType.ToString.ToLower = "minutes" Then
                    .Append(" Select Distinct " + vbCrLf)
                    .Append("   SCA.StuEnrollId, " + vbCrLf)
                    .Append("   (select Sum(Schedule) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and Schedule>=1.00 and (Actual <> 999.00 and Actual <> 9999.00)) as SchedHours," + vbCrLf)
                    .Append("   (select Sum(Actual) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (Actual>=1.00 and Actual <> 999.00 and Actual <> 9999.00)) as TotalPresentHours," + vbCrLf)
                    .Append("   (select Sum(Schedule-Actual) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (Actual <> 999.00 and Actual <> 9999.00) and (Schedule>=1.00)) " + vbCrLf)
                    .Append("   as TotalHoursAbsent," + vbCrLf)
                    .Append("   (select Sum(Actual) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and " + vbCrLf)
                    .Append("   ((Actual=1.00 and Actual <> 999.00 and Actual <> 9999.00) and (Schedule is NULL or Schedule=0.00 or Schedule=999.00)))" + vbCrLf)
                    .Append("   as TotalMakeUpHours," + vbCrLf)
                    .Append("   (select Sum(t4.Total) from " + vbCrLf)
                    .Append("   arStuEnrollments t1,arStudentSchedules t2, arProgSchedules t3, arProgScheduleDetails t4 " + vbCrLf)
                    .Append("   where t1.StuEnrollId = t2.StuEnrollId and t1.PrgVerId=t3.PrgVerId " + vbCrLf)
                    .Append("   and t2.ScheduleId=t3.ScheduleId and t3.scheduleId=t4.ScheduleId and " + vbCrLf)
                    .Append("   t2.StuEnrollId=SCA.StuEnrollId  and t4.total is not null) as ScheduleHoursByWeek, " + vbCrLf)
                    .Append("  (select max(MeetDate) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (Actual>=1.00 and Actual <> 999.00 and Actual <> 9999.00)) as LastDateAttended, " + vbCrLf)
                    .Append("   (select count(*) from  atConversionAttendance where StuEnrollId = SCA.StuEnrollId and Tardy=1) as TardyCount " + vbCrLf)
                    .Append("   from atConversionAttendance SCA " + vbCrLf)
                    .Append("   where SCA.StuEnrollId='" + StuEnrollId + "'" + vbCrLf)
                    ''Added by saraswathi for timeclock Schools
                ElseIf AttUnitType.ToString.ToLower = "hrs" Then
                    .Append(" Select Distinct " + vbCrLf)
                    .Append("   SCA.StuEnrollId, " + vbCrLf)
                    .Append("   (select Sum(Schedule) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and  (Actual is not null and Actual <> 999.00 and Actual <> 9999.00)) as SchedHours," + vbCrLf)
                    .Append("   (select Sum(Actual) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and ( Actual <> 999.00 and Actual <> 9999.00)) as TotalPresentHours," + vbCrLf)
                    .Append("   (select Sum(Schedule-Actual) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and  (Actual <> 999.00 and Actual <> 9999.00)and (ActualHours < SchedHours)) " + vbCrLf)
                    .Append("   as TotalHoursAbsent," + vbCrLf)
                    .Append("   (select Sum(Actual-Schedule) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and " + vbCrLf)
                    .Append("   ((Actual <> 999.00 and Actual <> 9999.00) and (ActualHours >SchedHours)))" + vbCrLf)
                    .Append("   as TotalMakeUpHours," + vbCrLf)
                    .Append("   (select Sum(t4.Total) from " + vbCrLf)
                    .Append("   arStuEnrollments t1,arStudentSchedules t2, arProgSchedules t3, arProgScheduleDetails t4 " + vbCrLf)
                    .Append("   where t1.StuEnrollId = t2.StuEnrollId and t1.PrgVerId=t3.PrgVerId " + vbCrLf)
                    .Append("   and t2.ScheduleId=t3.ScheduleId and t3.scheduleId=t4.ScheduleId and " + vbCrLf)
                    .Append("   t2.StuEnrollId=SCA.StuEnrollId  and t4.total is not null) as ScheduleHoursByWeek, " + vbCrLf)
                    .Append("  (select max(MeetDate) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (Actual>=1.00 and Actual <> 999.00 and Actual <> 9999.00)) as LastDateAttended, " + vbCrLf)
                    .Append("   (select count(*) from  atConversionAttendance where StuEnrollId = SCA.StuEnrollId and Tardy=1) as TardyCount " + vbCrLf)
                    .Append("   from atConversionAttendance SCA " + vbCrLf)
                    .Append("   where SCA.StuEnrollId='" + StuEnrollId + "'" + vbCrLf)
                Else
                    .Append(" Select Distinct " + vbCrLf)
                    .Append("   SCA.StuEnrollId, " + vbCrLf)
                    .Append("   (select Sum(Schedule) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and Schedule>=1.00 and (Actual is not null and Actual <> 999.00 and Actual <> 9999.00)) as SchedHours," + vbCrLf)
                    .Append("   (select Sum(Actual) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (Actual>=1.00 and Actual <> 999.00 and Actual <> 9999.00)) as TotalPresentHours," + vbCrLf)
                    .Append("   (select Sum(Schedule-Actual) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (Schedule>=1.00) and (Actual <> 999.00 and Actual <> 9999.00)) " + vbCrLf)
                    .Append("   as TotalHoursAbsent," + vbCrLf)
                    .Append("   (select Sum(Actual) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and " + vbCrLf)
                    .Append("   ((Actual>=1.00 and Actual <> 999.00 and Actual <> 9999.00) and (Schedule is NULL or Schedule=0.00 or Schedule=999.00)))" + vbCrLf)
                    .Append("   as TotalMakeUpHours," + vbCrLf)
                    .Append("   (select Sum(t4.Total) from " + vbCrLf)
                    .Append("   arStuEnrollments t1,arStudentSchedules t2, arProgSchedules t3, arProgScheduleDetails t4 " + vbCrLf)
                    .Append("   where t1.StuEnrollId = t2.StuEnrollId and t1.PrgVerId=t3.PrgVerId " + vbCrLf)
                    .Append("   and t2.ScheduleId=t3.ScheduleId and t3.scheduleId=t4.ScheduleId and " + vbCrLf)
                    .Append("   t2.StuEnrollId=SCA.StuEnrollId  and t4.total is not null) as ScheduleHoursByWeek, " + vbCrLf)
                    .Append("  (select max(MeetDate) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (Actual>=1.00 and Actual <> 999.00 and Actual <> 9999.00)) as LastDateAttended, " + vbCrLf)
                    .Append("   (select count(*) from  atConversionAttendance where StuEnrollId = SCA.StuEnrollId and Tardy=1) as TardyCount " + vbCrLf)
                    .Append("   from atConversionAttendance SCA " + vbCrLf)
                    .Append("   where SCA.StuEnrollId='" + StuEnrollId + "'" + vbCrLf)
                End If
            End With
            Dim dr As OleDbDataReader
            Dim dr1 As OleDbDataReader
            Dim StudentSummaryInfo As New ClockHourAttendanceInfo
            Try
                dr = db.RunParamSQLDataReader(sb.ToString)
                While dr.Read()
                    With StudentSummaryInfo
                        If dr("TotalPresentHours") Is System.DBNull.Value Then .TotalHoursPresent = 0.0 Else .TotalHoursPresent = CType(dr("TotalPresentHours"), Decimal)
                        If dr("TotalHoursAbsent") Is System.DBNull.Value Then .TotalHoursAbsent = 0.0 Else .TotalHoursAbsent = CType(dr("TotalHoursAbsent"), Decimal)
                        If dr("SchedHours") Is System.DBNull.Value Then .TotalHoursSched = 0.0 Else .TotalHoursSched = CType(dr("SchedHours"), Decimal)
                        If dr("TotalMakeUpHours") Is System.DBNull.Value Then .TotalMakeUpHours = 0.0 Else .TotalMakeUpHours = CType(dr("TotalMakeUpHours"), Decimal)
                        If dr("ScheduleHoursByWeek") Is System.DBNull.Value Then .TotalSchedHoursByWeek = 0.0 Else .TotalSchedHoursByWeek = CType(dr("ScheduleHoursByWeek"), Decimal)
                        If Not dr("LastDateAttended") Is System.DBNull.Value Then .LDA = CType(dr("LastDateAttended"), DateTime)
                        .Tardy = CType(dr("TardyCount"), Integer)

                    End With
                End While
            Catch ex As System.Exception
            Finally
                dr.Close()
                'db.CloseConnection()
            End Try

            Try
                dr = db.RunParamSQLDataReader("   Select isNull(TransferHours,0) TransferHours from arStuEnrollments where StuEnrollId='" + StuEnrollId + "'")
                While dr.Read()
                    With StudentSummaryInfo
                        .TransferHours = CType(dr("TransferHours"), Decimal)
                    End With
                End While
            Catch ex As System.Exception
            Finally
                dr.Close()
                'db.CloseConnection()
            End Try


            Try
                dr = db.RunParamSQLDataReader(sb1.ToString)
                While dr.Read()
                    With StudentSummaryInfo
                        If dr("TotalPresentHours") Is System.DBNull.Value Then .TotalHoursPresent += 0.0 Else .TotalHoursPresent += CType(dr("TotalPresentHours"), Decimal)
                        If dr("TotalHoursAbsent") Is System.DBNull.Value Then .TotalHoursAbsent += 0.0 Else .TotalHoursAbsent += CType(dr("TotalHoursAbsent"), Decimal)
                        If dr("SchedHours") Is System.DBNull.Value Then .TotalHoursSched += 0.0 Else .TotalHoursSched += CType(dr("SchedHours"), Decimal)
                        If dr("TotalMakeUpHours") Is System.DBNull.Value Then .TotalMakeUpHours += 0.0 Else .TotalMakeUpHours += CType(dr("TotalMakeUpHours"), Decimal)
                        If dr("ScheduleHoursByWeek") Is System.DBNull.Value Then .TotalSchedHoursByWeek += 0.0 Else .TotalSchedHoursByWeek += CType(dr("ScheduleHoursByWeek"), Decimal)
                        If Not dr("LastDateAttended") Is System.DBNull.Value Then .LDA = CType(dr("LastDateAttended"), DateTime)
                        .Tardy = CType(dr("TardyCount"), Integer)
                    End With
                End While
            Catch ex As System.Exception
            Finally
                dr.Close()
                db.CloseConnection()
            End Try
            Return StudentSummaryInfo
        End Function
        Public Function StudentSummaryFormForCutOffDate_SP(ByVal StuEnrollId As String, ByVal AttUnitType As String, ByVal cutOffDate As Date) As ClockHourAttendanceInfo
            Dim ds As DataSet
            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
            Dim dr As SqlDataReader
            Dim dr1 As SqlDataReader

            If AttUnitType.ToString.ToLower = "pa" Then  'if unit type is Present Absent
                db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@cutOffDate", cutOffDate, SqlDbType.DateTime, , ParameterDirection.Input)
                dr = db.RunParamSQLDataReader_SP("dbo.usp_StudentSummaryFormPA")
            ElseIf AttUnitType.ToString.ToLower = "minutes" Then
                db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@cutOffDate", cutOffDate, SqlDbType.DateTime, , ParameterDirection.Input)
                dr = db.RunParamSQLDataReader_SP("dbo.usp_StudentSummaryFormMinutes")
            ElseIf AttUnitType.ToString.ToLower = "hrs" Then
                db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@cutOffDate", cutOffDate, SqlDbType.DateTime, , ParameterDirection.Input)
                dr = db.RunParamSQLDataReader_SP("dbo.usp_StudentSummaryFormHours")
            Else
                db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@cutOffDate", cutOffDate, SqlDbType.DateTime, , ParameterDirection.Input)
                dr = db.RunParamSQLDataReader_SP("dbo.usp_StudentSummaryForm")
            End If
            db.ClearParameters()
            If AttUnitType.ToString.ToLower = "pa" Then  'if unit type is Present Absent
                db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@cutOffDate", cutOffDate, SqlDbType.DateTime, , ParameterDirection.Input)
                dr1 = db.RunParamSQLDataReader_SP("dbo.usp_StudentSummaryFormConversionPA")
            ElseIf AttUnitType.ToString.ToLower = "minutes" Then
                db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@cutOffDate", cutOffDate, SqlDbType.DateTime, , ParameterDirection.Input)
                dr1 = db.RunParamSQLDataReader_SP("dbo.usp_StudentSummaryFormConversionMinutes")
            ElseIf AttUnitType.ToString.ToLower = "hrs" Then
                db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@cutOffDate", cutOffDate, SqlDbType.DateTime, , ParameterDirection.Input)
                dr1 = db.RunParamSQLDataReader_SP("dbo.usp_StudentSummaryFormConversionHours")
            Else
                db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@cutOffDate", cutOffDate, SqlDbType.DateTime, , ParameterDirection.Input)
                dr1 = db.RunParamSQLDataReader_SP("dbo.usp_StudentSummaryFormConversion")
            End If



            Dim StudentSummaryInfo As New ClockHourAttendanceInfo
            Try

                While dr.Read()
                    With StudentSummaryInfo
                        If dr("TotalPresentHours") Is System.DBNull.Value Then .TotalHoursPresent = 0.0 Else .TotalHoursPresent = CType(dr("TotalPresentHours"), Decimal)
                        If dr("TotalHoursAbsent") Is System.DBNull.Value Then .TotalHoursAbsent = 0.0 Else .TotalHoursAbsent = CType(dr("TotalHoursAbsent"), Decimal)
                        If dr("SchedHours") Is System.DBNull.Value Then .TotalHoursSched = 0.0 Else .TotalHoursSched = CType(dr("SchedHours"), Decimal)
                        If dr("TotalMakeUpHours") Is System.DBNull.Value Then .TotalMakeUpHours = 0.0 Else .TotalMakeUpHours = CType(dr("TotalMakeUpHours"), Decimal)
                        If dr("ScheduleHoursByWeek") Is System.DBNull.Value Then .TotalSchedHoursByWeek = 0.0 Else .TotalSchedHoursByWeek = CType(dr("ScheduleHoursByWeek"), Decimal)
                        If Not dr("LastDateAttended") Is System.DBNull.Value Then .LDA = CType(dr("LastDateAttended"), DateTime)
                        .Tardy = CType(dr("TardyCount"), Integer)
                    End With
                End While
            Catch ex As System.Exception
            Finally
                dr.Close()
                'db.CloseConnection()
            End Try
            Try

                While dr1.Read()
                    With StudentSummaryInfo
                        If dr1("TotalPresentHours") Is System.DBNull.Value Then .TotalHoursPresent += 0.0 Else .TotalHoursPresent += CType(dr1("TotalPresentHours"), Decimal)
                        If dr1("TotalHoursAbsent") Is System.DBNull.Value Then .TotalHoursAbsent += 0.0 Else .TotalHoursAbsent += CType(dr1("TotalHoursAbsent"), Decimal)
                        If dr1("SchedHours") Is System.DBNull.Value Then .TotalHoursSched += 0.0 Else .TotalHoursSched += CType(dr1("SchedHours"), Decimal)
                        If dr1("TotalMakeUpHours") Is System.DBNull.Value Then .TotalMakeUpHours += 0.0 Else .TotalMakeUpHours += CType(dr1("TotalMakeUpHours"), Decimal)
                        If dr1("ScheduleHoursByWeek") Is System.DBNull.Value Then .TotalSchedHoursByWeek += 0.0 Else .TotalSchedHoursByWeek += CType(dr1("ScheduleHoursByWeek"), Decimal)
                        If Not dr1("LastDateAttended") Is System.DBNull.Value Then .LDA = CType(dr1("LastDateAttended"), DateTime)
                        .Tardy = CType(dr1("TardyCount"), Integer)
                    End With
                End While
            Catch ex As System.Exception
            Finally
                dr1.Close()
                db.CloseConnection()
            End Try
            Return StudentSummaryInfo
        End Function
        Public Function StudentSummaryFormForDateRange(ByVal StuEnrollId As String, ByVal AttUnitType As String, ByVal startDate As Date, ByVal cutOffDate As Date) As ClockHourAttendanceInfo

            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@attUnitType", AttUnitType, SqlDbType.VarChar, 50, ParameterDirection.Input)
            db.AddParameter("@startDate", startDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@cutOffDate", cutOffDate, SqlDbType.DateTime, , ParameterDirection.Input)

            Dim dr As SqlDataReader
            Dim StudentSummaryInfo As New ClockHourAttendanceInfo
            Try
                dr = db.RunParamSQLDataReader_SP("dbo.USP_SchedAndCompletedHours_ByDay")
                While dr.Read()
                    With StudentSummaryInfo
                        If dr("TotalPresentHours") Is System.DBNull.Value Then .TotalHoursPresent = 0.0 Else .TotalHoursPresent = CType(dr("TotalPresentHours"), Decimal)
                        If dr("TotalHoursAbsent") Is System.DBNull.Value Then .TotalHoursAbsent = 0.0 Else .TotalHoursAbsent = CType(dr("TotalHoursAbsent"), Decimal)
                        If dr("SchedHours") Is System.DBNull.Value Then .TotalHoursSched = 0.0 Else .TotalHoursSched = CType(dr("SchedHours"), Decimal)
                        If dr("TotalMakeUpHours") Is System.DBNull.Value Then .TotalMakeUpHours = 0.0 Else .TotalMakeUpHours = CType(dr("TotalMakeUpHours"), Decimal)
                        If dr("ScheduleHoursByWeek") Is System.DBNull.Value Then .TotalSchedHoursByWeek = 0.0 Else .TotalSchedHoursByWeek = CType(dr("ScheduleHoursByWeek"), Decimal)
                        If Not dr("LastDateAttended") Is System.DBNull.Value Then .LDA = CType(dr("LastDateAttended"), DateTime)
                        .Tardy = CType(dr("TardyCount"), Integer)
                    End With
                End While
            Catch ex As System.Exception
            Finally
                dr.Close()
                db.CloseConnection()
            End Try

            Return StudentSummaryInfo
        End Function

        Public Function StudentSummaryFormForCutOffDate(ByVal StuEnrollId As String, ByVal AttUnitType As String, ByVal cutOffDate As Date) As ClockHourAttendanceInfo
            Dim sb As New StringBuilder
            Dim sb1 As New StringBuilder
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            With sb
                If AttUnitType.ToString.ToLower = "pa" Then  'if unit type is Present Absent
                    .Append(" Select top 1 " + vbCrLf)
                    .Append("   SCA.StuEnrollId, " + vbCrLf)
                    .Append("   (select Sum(SchedHours) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and Recorddate<=SCA.RecordDate and SchedHours>=1.00 and (ActualHours <> 999.00 and ActualHours <> 9999.00)) as SchedHours," + vbCrLf)
                    .Append("   (select Sum(ActualHours) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId  and Recorddate<=SCA.RecordDate and (ActualHours>=1.00 and ActualHours <> 999.00 and ActualHours <> 9999.00)) as TotalPresentHours," + vbCrLf)
                    .Append("   (select Sum(SchedHours-ActualHours) from arStudentClockAttendance " + vbCrLf)
                    '.Append("   where StuEnrollId = SCA.StuEnrollId and (ActualHours=0.00 and SchedHours>=1.00 and PostByException <> 'yes')) " + vbCrLf)
                    ''Query modified by Saraswathi
                    ''on March 3 2009
                    ''The PostbyException condiion is removed.
                    .Append("   where StuEnrollId = SCA.StuEnrollId  and Recorddate<=SCA.RecordDate and (ActualHours<>999.0 and ActualHours<>9999.0 ) and (Schedhours>ActualHours)) " + vbCrLf)

                    .Append("   as TotalHoursAbsent," + vbCrLf)
                    .Append("   (select Sum(ActualHours-SchedHours) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId  and Recorddate<=SCA.RecordDate and " + vbCrLf)
                    .Append("   ((ActualHours >=0.00 and ActualHours <> 999.00 and ActualHours <> 9999.00) and (SchedHours<ActualHours)))" + vbCrLf)
                    .Append("   as TotalMakeUpHours," + vbCrLf)
                    .Append("   (select Sum(t4.Total) from " + vbCrLf)
                    .Append("   arStuEnrollments t1,arStudentSchedules t2, arProgSchedules t3, arProgScheduleDetails t4 " + vbCrLf)
                    .Append("   where t1.StuEnrollId = t2.StuEnrollId and t1.PrgVerId=t3.PrgVerId " + vbCrLf)
                    .Append("   and t2.ScheduleId=t3.ScheduleId and t3.scheduleId=t4.ScheduleId and " + vbCrLf)
                    .Append("   t2.StuEnrollId=SCA.StuEnrollId  and t4.total is not null) as ScheduleHoursByWeek, " + vbCrLf)
                    .Append("  (select max(RecordDate) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (ActualHours>=1.00 and ActualHours <> 999.00 and ActualHours <> 9999.00)) as LastDateAttended, " + vbCrLf)
                    .Append("   (select count(*) from  arStudentClockAttendance where StuEnrollId = SCA.StuEnrollId and IsTardy=1) as TardyCount " + vbCrLf)
                    .Append("   from arStudentClockAttendance SCA " + vbCrLf)
                    .Append("   where SCA.StuEnrollId='" + StuEnrollId + "'" + vbCrLf)
                ElseIf AttUnitType.ToString.ToLower = "minutes" Then
                    .Append(" Select top 1 " + vbCrLf)
                    .Append("   SCA.StuEnrollId, " + vbCrLf)
                    .Append("   (select Sum(SchedHours) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and SchedHours>=0.00  and Recorddate<=SCA.RecordDate and (ActualHours is not null and ActualHours <> 999.00 and ActualHours <> 9999.00)) as SchedHours," + vbCrLf)
                    .Append("   (select Sum(ActualHours) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId  and Recorddate<=SCA.RecordDate and (ActualHours>=0.00 and ActualHours <> 999.00 and ActualHours <> 9999.00)) as TotalPresentHours," + vbCrLf)
                    .Append("   (select Sum(SchedHours-ActualHours) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId  and Recorddate<=SCA.RecordDate and ( ActualHours <> 999.00 and ActualHours <> 9999.00 and SchedHours>=0.00 and SchedHours>ActualHours)) " + vbCrLf)
                    .Append("   as TotalHoursAbsent," + vbCrLf)
                    .Append("   (select Sum(ActualHours-SchedHours) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId  and Recorddate<=SCA.RecordDate and " + vbCrLf)
                    .Append("   ((ActualHours>=0.00 and ActualHours <> 999.00 and ActualHours <> 9999.00) and (SchedHours<ActualHours)))" + vbCrLf)
                    .Append("   as TotalMakeUpHours," + vbCrLf)
                    .Append("   (select Sum(t4.Total) from " + vbCrLf)
                    .Append("   arStuEnrollments t1,arStudentSchedules t2, arProgSchedules t3, arProgScheduleDetails t4 " + vbCrLf)
                    .Append("   where t1.StuEnrollId = t2.StuEnrollId and t1.PrgVerId=t3.PrgVerId " + vbCrLf)
                    .Append("   and t2.ScheduleId=t3.ScheduleId and t3.scheduleId=t4.ScheduleId and " + vbCrLf)
                    .Append("   t2.StuEnrollId=SCA.StuEnrollId  and t4.total is not null) as ScheduleHoursByWeek, " + vbCrLf)
                    .Append("  (select max(RecordDate) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (ActualHours>=1.00 and ActualHours <> 999.00 and ActualHours <> 9999.00)) as LastDateAttended, " + vbCrLf)
                    .Append("   (select count(*) from  arStudentClockAttendance where StuEnrollId = SCA.StuEnrollId and IsTardy=1) as TardyCount " + vbCrLf)
                    .Append("   from arStudentClockAttendance SCA " + vbCrLf)
                    .Append("   where SCA.StuEnrollId='" + StuEnrollId + "'" + vbCrLf)
                    ''Modified by Saraswathi for Timeclock Schools
                    ''ON Jan 27 2009
                    ''Actual hr>1 calculation removed
                ElseIf AttUnitType.ToString.ToLower = "hrs" Then
                    .Append(" Select top 1 " + vbCrLf)
                    .Append("   SCA.StuEnrollId, " + vbCrLf)
                    .Append("   (select Sum(SchedHours) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId  and Recorddate<=SCA.RecordDate and  (ActualHours is not null and ActualHours <> 999.00 and ActualHours <> 9999.00)) as SchedHours," + vbCrLf)
                    .Append(" (select Sum(ActualHours) from arStudentClockAttendance  " + vbCrLf)
                    .Append(" where StuEnrollId = SCA.StuEnrollId  and Recorddate<=SCA.RecordDate and ( ActualHours <> 999.00 and ActualHours <> 9999.00)) " + vbCrLf)
                    .Append(" as TotalPresentHours, " + vbCrLf)
                    .Append(" (select Sum(SchedHours-ActualHours) from arStudentClockAttendance " + vbCrLf)
                    .Append(" where StuEnrollId = SCA.StuEnrollId  and Recorddate<=SCA.RecordDate and " + vbCrLf)
                    .Append(" (ActualHours <> 999.00 and ActualHours <> 9999.00 and  (ActualHours < SchedHours))) " + vbCrLf)
                    .Append(" as TotalHoursAbsent, " + vbCrLf)
                    .Append(" (select Sum(ActualHours-SchedHours) from arStudentClockAttendance  " + vbCrLf)
                    .Append(" where StuEnrollId = SCA.StuEnrollId  and Recorddate<=SCA.RecordDate and " + vbCrLf)
                    .Append(" (( ActualHours <> 999.00 and ActualHours <> 9999.00) and " + vbCrLf)
                    .Append(" (ActualHours >SchedHours))) " + vbCrLf)
                    .Append("  as TotalMakeUpHours, " + vbCrLf)
                    .Append("   (select Sum(t4.Total) from " + vbCrLf)
                    .Append("   arStuEnrollments t1,arStudentSchedules t2, arProgSchedules t3, arProgScheduleDetails t4 " + vbCrLf)
                    .Append("   where t1.StuEnrollId = t2.StuEnrollId and t1.PrgVerId=t3.PrgVerId " + vbCrLf)
                    .Append("   and t2.ScheduleId=t3.ScheduleId and t3.scheduleId=t4.ScheduleId and " + vbCrLf)
                    .Append("   t2.StuEnrollId=SCA.StuEnrollId  and t4.total is not null) as ScheduleHoursByWeek, " + vbCrLf)
                    .Append("  (select max(RecordDate) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (ActualHours <> 999.00 and ActualHours <> 9999.00)) as LastDateAttended, " + vbCrLf)
                    .Append("   (select count(*) from  arStudentClockAttendance where StuEnrollId = SCA.StuEnrollId and IsTardy=1) as TardyCount " + vbCrLf)
                    .Append("   from arStudentClockAttendance SCA " + vbCrLf)
                    .Append("   where SCA.StuEnrollId='" + StuEnrollId + "'" + vbCrLf)
                Else
                    .Append(" Select top 1 " + vbCrLf)
                    .Append("   SCA.StuEnrollId, " + vbCrLf)
                    .Append("   (select Sum(SchedHours) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and Recorddate<=SCA.RecordDate  and SchedHours>=1.00 and (ActualHours is not null and ActualHours <> 999.00 and ActualHours <> 9999.00)) as SchedHours," + vbCrLf)
                    .Append(" (select Sum(ActualHours) from arStudentClockAttendance  " + vbCrLf)
                    .Append(" where StuEnrollId = SCA.StuEnrollId  and Recorddate<=SCA.RecordDate and (ActualHours>=1.00 and ActualHours <> 999.00 and ActualHours <> 9999.00)) " + vbCrLf)
                    .Append(" as TotalPresentHours, " + vbCrLf)
                    .Append(" (select Sum(SchedHours-ActualHours) from arStudentClockAttendance " + vbCrLf)
                    .Append(" where StuEnrollId = SCA.StuEnrollId and Recorddate<=SCA.RecordDate  and " + vbCrLf)
                    .Append(" (ActualHours <> 999.00 and ActualHours <> 9999.00 and SchedHours>=1.00 and (ActualHours < SchedHours))) " + vbCrLf)
                    .Append(" as TotalHoursAbsent, " + vbCrLf)
                    .Append(" (select Sum(ActualHours) from arStudentClockAttendance  " + vbCrLf)
                    .Append(" where StuEnrollId = SCA.StuEnrollId and Recorddate<=SCA.RecordDate  and " + vbCrLf)
                    .Append(" ((ActualHours>=1.00 and ActualHours <> 999.00 and ActualHours <> 9999.00) and " + vbCrLf)
                    .Append(" (SchedHours is NULL or SchedHours=0.00 or SchedHours=999.00))) " + vbCrLf)
                    .Append("  as TotalMakeUpHours, " + vbCrLf)
                    .Append("   (select Sum(t4.Total) from " + vbCrLf)
                    .Append("   arStuEnrollments t1,arStudentSchedules t2, arProgSchedules t3, arProgScheduleDetails t4 " + vbCrLf)
                    .Append("   where t1.StuEnrollId = t2.StuEnrollId and t1.PrgVerId=t3.PrgVerId " + vbCrLf)
                    .Append("   and t2.ScheduleId=t3.ScheduleId and t3.scheduleId=t4.ScheduleId and " + vbCrLf)
                    .Append("   t2.StuEnrollId=SCA.StuEnrollId  and t4.total is not null) as ScheduleHoursByWeek, " + vbCrLf)
                    .Append("  (select max(RecordDate) from arStudentClockAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (ActualHours>=1.00 and ActualHours <> 999.00 and ActualHours <> 9999.00)) as LastDateAttended, " + vbCrLf)
                    .Append("   (select count(*) from  arStudentClockAttendance where StuEnrollId = SCA.StuEnrollId and IsTardy=1) as TardyCount " + vbCrLf)
                    .Append("   from arStudentClockAttendance SCA " + vbCrLf)
                    .Append("   where SCA.StuEnrollId='" + StuEnrollId + "'" + vbCrLf)
                End If
                .Append(" and SCA.RecordDate <='" + cutOffDate + "'  order by Schedhours desc,TotalPresentHours desc " + vbCrLf)
            End With
            With sb1
                If AttUnitType.ToString.ToLower = "pa" Then  'if unit type is Present Absent
                    .Append(" Select top 1 " + vbCrLf)
                    .Append("   SCA.StuEnrollId, " + vbCrLf)
                    .Append("   (select Sum(Schedule) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and MeetDate<=SCA.MeetDate and Schedule>=1.00 and (Actual <> 999.00 and Actual <> 9999.00)) as SchedHours," + vbCrLf)
                    .Append("   (select Sum(Actual) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and MeetDate<=SCA.MeetDate  and (Actual>=1.00 and Actual <> 999.00 and Actual <> 9999.00)) as TotalPresentHours," + vbCrLf)
                    .Append("   (select Sum(Schedule-Actual) from atConversionAttendance " + vbCrLf)

                    ''Query modified by Saraswathi
                    ''on March 3 2009
                    ''The PostbyException condiion is removed.
                    '.Append("   where StuEnrollId = SCA.StuEnrollId and (Actual <> 999.00 and Actual <> 9999.00 and Schedule>=1.00) and PostByException <> 'yes') " + vbCrLf)

                    .Append("   where StuEnrollId = SCA.StuEnrollId and MeetDate<=SCA.MeetDate  and (Actual <> 999.00 and Actual <> 9999.00 and Schedule>=1.00) ) " + vbCrLf)

                    .Append("   as TotalHoursAbsent," + vbCrLf)
                    .Append("   (select Sum(Actual) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and MeetDate<=SCA.MeetDate  and " + vbCrLf)
                    .Append("   ((Actual>=1.00 and Actual<> 999.00 and Actual <> 9999.00) and (Schedule is NULL or Schedule=0.00)))" + vbCrLf)
                    .Append("   as TotalMakeUpHours," + vbCrLf)
                    .Append("   (select Sum(t4.Total) from " + vbCrLf)
                    .Append("   arStuEnrollments t1,arStudentSchedules t2, arProgSchedules t3, arProgScheduleDetails t4 " + vbCrLf)
                    .Append("   where t1.StuEnrollId = t2.StuEnrollId and t1.PrgVerId=t3.PrgVerId " + vbCrLf)
                    .Append("   and t2.ScheduleId=t3.ScheduleId and t3.scheduleId=t4.ScheduleId and " + vbCrLf)
                    .Append("   t2.StuEnrollId=SCA.StuEnrollId  and t4.total is not null) as ScheduleHoursByWeek, " + vbCrLf)
                    .Append("  (select max(MeetDate) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (Actual>=1.00 and Actual <> 999.00 and Actual <> 9999.00)) as LastDateAttended, " + vbCrLf)
                    .Append("   (select Count(*) from  atConversionAttendance where StuEnrollId = SCA.StuEnrollId and Tardy=1) as TardyCount " + vbCrLf)
                    .Append("   from atConversionAttendance SCA " + vbCrLf)
                    .Append("   where SCA.StuEnrollId='" + StuEnrollId + "'" + vbCrLf)
                ElseIf AttUnitType.ToString.ToLower = "minutes" Then
                    .Append(" Select top 1 " + vbCrLf)
                    .Append("   SCA.StuEnrollId, " + vbCrLf)
                    .Append("   (select Sum(Schedule) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId  and MeetDate<=SCA.MeetDate and Schedule>=1.00 and (Actual <> 999.00 and Actual <> 9999.00)) as SchedHours," + vbCrLf)
                    .Append("   (select Sum(Actual) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and MeetDate<=SCA.MeetDate  and (Actual>=1.00 and Actual <> 999.00 and Actual <> 9999.00)) as TotalPresentHours," + vbCrLf)
                    .Append("   (select Sum(Schedule-Actual) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and MeetDate<=SCA.MeetDate  and (Actual <> 999.00 and Actual <> 9999.00) and (Schedule>=1.00)) " + vbCrLf)
                    .Append("   as TotalHoursAbsent," + vbCrLf)
                    .Append("   (select Sum(Actual) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and MeetDate<=SCA.MeetDate  and " + vbCrLf)
                    .Append("   ((Actual=1.00 and Actual <> 999.00 and Actual <> 9999.00) and (Schedule is NULL or Schedule=0.00 or Schedule=999.00)))" + vbCrLf)
                    .Append("   as TotalMakeUpHours," + vbCrLf)
                    .Append("   (select Sum(t4.Total) from " + vbCrLf)
                    .Append("   arStuEnrollments t1,arStudentSchedules t2, arProgSchedules t3, arProgScheduleDetails t4 " + vbCrLf)
                    .Append("   where t1.StuEnrollId = t2.StuEnrollId and t1.PrgVerId=t3.PrgVerId " + vbCrLf)
                    .Append("   and t2.ScheduleId=t3.ScheduleId and t3.scheduleId=t4.ScheduleId and " + vbCrLf)
                    .Append("   t2.StuEnrollId=SCA.StuEnrollId  and t4.total is not null) as ScheduleHoursByWeek, " + vbCrLf)
                    .Append("  (select max(MeetDate) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (Actual>=1.00 and Actual <> 999.00 and Actual <> 9999.00)) as LastDateAttended, " + vbCrLf)
                    .Append("   (select count(*) from  atConversionAttendance where StuEnrollId = SCA.StuEnrollId and Tardy=1) as TardyCount " + vbCrLf)
                    .Append("   from atConversionAttendance SCA " + vbCrLf)
                    .Append("   where SCA.StuEnrollId='" + StuEnrollId + "'" + vbCrLf)
                    ''Added by saraswathi for timeclock Schools
                ElseIf AttUnitType.ToString.ToLower = "hrs" Then
                    .Append(" Select top 1 " + vbCrLf)
                    .Append("   SCA.StuEnrollId, " + vbCrLf)
                    .Append("   (select Sum(Schedule) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId  and MeetDate<=SCA.MeetDate and  (Actual is not null and Actual <> 999.00 and Actual <> 9999.00)) as SchedHours," + vbCrLf)
                    .Append("   (select Sum(Actual) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId  and MeetDate<=SCA.MeetDate and ( Actual <> 999.00 and Actual <> 9999.00)) as TotalPresentHours," + vbCrLf)
                    .Append("   (select Sum(Schedule-Actual) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and MeetDate<=SCA.MeetDate  and  (Actual <> 999.00 and Actual <> 9999.00)and (ActualHours < SchedHours)) " + vbCrLf)
                    .Append("   as TotalHoursAbsent," + vbCrLf)
                    .Append("   (select Sum(Actual-Schedule) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and MeetDate<=SCA.MeetDate  and " + vbCrLf)
                    .Append("   ((Actual <> 999.00 and Actual <> 9999.00) and (ActualHours >SchedHours)))" + vbCrLf)
                    .Append("   as TotalMakeUpHours," + vbCrLf)
                    .Append("   (select Sum(t4.Total) from " + vbCrLf)
                    .Append("   arStuEnrollments t1,arStudentSchedules t2, arProgSchedules t3, arProgScheduleDetails t4 " + vbCrLf)
                    .Append("   where t1.StuEnrollId = t2.StuEnrollId and MeetDate<=SCA.MeetDate  and t1.PrgVerId=t3.PrgVerId " + vbCrLf)
                    .Append("   and t2.ScheduleId=t3.ScheduleId and t3.scheduleId=t4.ScheduleId and " + vbCrLf)
                    .Append("   t2.StuEnrollId=SCA.StuEnrollId  and t4.total is not null) as ScheduleHoursByWeek, " + vbCrLf)
                    .Append("  (select max(MeetDate) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (Actual>=1.00 and Actual <> 999.00 and Actual <> 9999.00)) as LastDateAttended, " + vbCrLf)
                    .Append("   (select count(*) from  atConversionAttendance where StuEnrollId = SCA.StuEnrollId and Tardy=1) as TardyCount " + vbCrLf)
                    .Append("   from atConversionAttendance SCA " + vbCrLf)
                    .Append("   where SCA.StuEnrollId='" + StuEnrollId + "'" + vbCrLf)
                Else
                    .Append(" Select top 1 " + vbCrLf)
                    .Append("   SCA.StuEnrollId, " + vbCrLf)
                    .Append("   (select Sum(Schedule) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and MeetDate<=SCA.MeetDate  and Schedule>=1.00 and (Actual is not null and Actual <> 999.00 and Actual <> 9999.00)) as SchedHours," + vbCrLf)
                    .Append("   (select Sum(Actual) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and MeetDate<=SCA.MeetDate  and (Actual>=1.00 and Actual <> 999.00 and Actual <> 9999.00)) as TotalPresentHours," + vbCrLf)
                    .Append("   (select Sum(Schedule-Actual) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and MeetDate<=SCA.MeetDate  and (Schedule>=1.00) and (Actual <> 999.00 and Actual <> 9999.00)) " + vbCrLf)
                    .Append("   as TotalHoursAbsent," + vbCrLf)
                    .Append("   (select Sum(Actual) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and MeetDate<=SCA.MeetDate  and " + vbCrLf)
                    .Append("   ((Actual>=1.00 and Actual <> 999.00 and Actual <> 9999.00) and (Schedule is NULL or Schedule=0.00 or Schedule=999.00)))" + vbCrLf)
                    .Append("   as TotalMakeUpHours," + vbCrLf)
                    .Append("   (select Sum(t4.Total) from " + vbCrLf)
                    .Append("   arStuEnrollments t1,arStudentSchedules t2, arProgSchedules t3, arProgScheduleDetails t4 " + vbCrLf)
                    .Append("   where t1.StuEnrollId = t2.StuEnrollId and t1.PrgVerId=t3.PrgVerId " + vbCrLf)
                    .Append("   and t2.ScheduleId=t3.ScheduleId and t3.scheduleId=t4.ScheduleId and " + vbCrLf)
                    .Append("   t2.StuEnrollId=SCA.StuEnrollId  and t4.total is not null) as ScheduleHoursByWeek, " + vbCrLf)
                    .Append("  (select max(MeetDate) from atConversionAttendance " + vbCrLf)
                    .Append("   where StuEnrollId = SCA.StuEnrollId and (Actual>=1.00 and Actual <> 999.00 and Actual <> 9999.00)) as LastDateAttended, " + vbCrLf)
                    .Append("   (select count(*) from  atConversionAttendance where StuEnrollId = SCA.StuEnrollId and Tardy=1) as TardyCount " + vbCrLf)
                    .Append("   from atConversionAttendance SCA " + vbCrLf)
                    .Append("   where SCA.StuEnrollId='" + StuEnrollId + "'" + vbCrLf)
                End If
                .Append(" and SCA.MeetDate <='" + cutOffDate + "'  order by Schedhours desc,TotalPresentHours desc " + vbCrLf)
            End With
            Dim dr As OleDbDataReader
            Dim dr1 As OleDbDataReader
            Dim StudentSummaryInfo As New ClockHourAttendanceInfo
            Try
                dr = db.RunParamSQLDataReader(sb.ToString)
                While dr.Read()
                    With StudentSummaryInfo
                        If dr("TotalPresentHours") Is System.DBNull.Value Then .TotalHoursPresent = 0.0 Else .TotalHoursPresent = CType(dr("TotalPresentHours"), Decimal)
                        If dr("TotalHoursAbsent") Is System.DBNull.Value Then .TotalHoursAbsent = 0.0 Else .TotalHoursAbsent = CType(dr("TotalHoursAbsent"), Decimal)
                        If dr("SchedHours") Is System.DBNull.Value Then .TotalHoursSched = 0.0 Else .TotalHoursSched = CType(dr("SchedHours"), Decimal)
                        If dr("TotalMakeUpHours") Is System.DBNull.Value Then .TotalMakeUpHours = 0.0 Else .TotalMakeUpHours = CType(dr("TotalMakeUpHours"), Decimal)
                        If dr("ScheduleHoursByWeek") Is System.DBNull.Value Then .TotalSchedHoursByWeek = 0.0 Else .TotalSchedHoursByWeek = CType(dr("ScheduleHoursByWeek"), Decimal)
                        If Not dr("LastDateAttended") Is System.DBNull.Value Then .LDA = CType(dr("LastDateAttended"), DateTime)
                        .Tardy = CType(dr("TardyCount"), Integer)
                    End With
                End While
            Catch ex As System.Exception
            Finally
                dr.Close()
                'db.CloseConnection()
            End Try
            Try
                dr = db.RunParamSQLDataReader(sb1.ToString)
                While dr.Read()
                    With StudentSummaryInfo
                        If dr("TotalPresentHours") Is System.DBNull.Value Then .TotalHoursPresent += 0.0 Else .TotalHoursPresent += CType(dr("TotalPresentHours"), Decimal)
                        If dr("TotalHoursAbsent") Is System.DBNull.Value Then .TotalHoursAbsent += 0.0 Else .TotalHoursAbsent += CType(dr("TotalHoursAbsent"), Decimal)
                        If dr("SchedHours") Is System.DBNull.Value Then .TotalHoursSched += 0.0 Else .TotalHoursSched += CType(dr("SchedHours"), Decimal)
                        If dr("TotalMakeUpHours") Is System.DBNull.Value Then .TotalMakeUpHours += 0.0 Else .TotalMakeUpHours += CType(dr("TotalMakeUpHours"), Decimal)
                        If dr("ScheduleHoursByWeek") Is System.DBNull.Value Then .TotalSchedHoursByWeek += 0.0 Else .TotalSchedHoursByWeek += CType(dr("ScheduleHoursByWeek"), Decimal)
                        If Not dr("LastDateAttended") Is System.DBNull.Value Then .LDA = CType(dr("LastDateAttended"), DateTime)
                        .Tardy = CType(dr("TardyCount"), Integer)
                    End With
                End While
            Catch ex As System.Exception
            Finally
                dr.Close()
                db.CloseConnection()
            End Try
            Return StudentSummaryInfo
        End Function
        Private Function getTardyByProgramVersion(ByVal StuEnrollId As String) As TardyAttendanceUnitInfo
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            Dim dr As OleDbDataReader
            Dim getTardyTrackDetails As New TardyAttendanceUnitInfo

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            With sb
                .Append("   Select Distinct TardiesMakingAbsence,UnitTypeId ")
                .Append("   from    arPrgVersions PV,arStuEnrollments SE ")
                .Append("   where PV.PrgVerId = SE.PrgVerId and SE.StuEnrollId=? ")
            End With
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                dr = db.RunParamSQLDataReader(sb.ToString)
                Do While dr.Read
                    With getTardyTrackDetails
                        If Not dr("TardiesMakingAbsence") Is System.DBNull.Value Then .TardiesMakingAbsence = CType(dr("TardiesMakingAbsence"), Integer) Else .TardiesMakingAbsence = 0
                        If Not dr("UnitTypeId") Is System.DBNull.Value Then .UnitTypeId = CType(dr("UnitTypeId"), Guid).ToString Else .UnitTypeId = ""
                    End With
                Loop
                ''modified by saraswathi Lakshmanan on Jan 27 2009
                ''When the gettardytrackdetails is of PresentAbsent or minutes or clockhour , tardy bycourse is not calculated

                If getTardyTrackDetails.UnitTypeId <> AdvantageCommonValues.PresentAbsentGuid And getTardyTrackDetails.UnitTypeId <> AdvantageCommonValues.MinutesGuid And getTardyTrackDetails.UnitTypeId <> AdvantageCommonValues.ClockHoursGuid Then

                    getTardyTrackDetails = getTardyByCourse()
                End If
                Return getTardyTrackDetails
            Catch ex As System.Exception
            Finally
                dr.Close()
                db.CloseConnection()
            End Try
        End Function
        Private Function getTardyByCourse() As TardyAttendanceUnitInfo
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            Dim dr As OleDbDataReader
            Dim getTardyTrackDetails As New TardyAttendanceUnitInfo

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            With sb
                .Append("   Select Distinct TardiesMakingAbsence,UnitTypeId ")
                .Append("   from    arReqs  where UnitTypeid is not null ")

            End With

            Try
                dr = db.RunParamSQLDataReader(sb.ToString)
                Do While dr.Read
                    With getTardyTrackDetails
                        If Not dr("TardiesMakingAbsence") Is System.DBNull.Value Then .TardiesMakingAbsence = CType(dr("TardiesMakingAbsence"), Integer) Else .TardiesMakingAbsence = 0
                        If Not dr("UnitTypeId") Is System.DBNull.Value Then .UnitTypeId = CType(dr("UnitTypeId"), Guid).ToString Else .UnitTypeId = ""
                    End With
                Loop
                Return getTardyTrackDetails
            Catch ex As System.Exception
            Finally
                dr.Close()
                db.CloseConnection()
            End Try
        End Function
        Public Function GetAttendanceByEnrollment(ByVal StuEnrollId As String) As DataSet
            Dim db As New DataAccess
            Dim sb As New StringBuilder

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            With sb
                .Append(" select Distinct StuEnrollId,RecordDate,SchedHours,ActualHours,ModDate,ModUser,isTardy ")
                '.Append(" select Distinct StuEnrollId,RecordDate,SchedHours,ActualHours,ModDate,ModUser,(SchedHours-ActualHours) as isTardy ")
                .Append(" from arStudentClockAttendance ")
                .Append(" where StuEnrollId=? ")
                .Append(" and  SchedHours  is not null ")
                .Append(" and (ActualHours is not null and ActualHours <> 999.00 and ActualHours <> 9999.00)  ")
                '.Append(" and (SchedHours-ActualHours) >= 1.00 ")
                '.Append(" and isTardy = 1")
                .Append(" union ")
                .Append(" select Distinct StuEnrollId,MeetDate as RecordDate,Schedule as SchedHours,Actual as ActualHours,'','',Tardy as IsTardy ")
                .Append(" from atConversionAttendance where StuEnrollId=?  ")
                .Append(" and  Schedule  is not null ")
                .Append(" and (Actual is not null and Actual <> 999.00 and Actual <> 9999.00)  ")
            End With
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function
        Public Function GetStudentSummaryDetails(ByVal StuEnrollId As String, Optional ByVal AttUnitType As String = "", Optional ByVal PrgVerId As String = "", Optional ByVal campusId As String = "") As DataSet
            Dim db As New DataAccess
            Dim sb As New StringBuilder

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ''Modified by saraswathi on jan 28 2009
            ''Schedule<>0  added
            If Not AttUnitType.ToString.ToLower = "hrs" And Not AttUnitType.ToString.ToLower = "minutes" Then
                ''Modified By Saraswathi on March 23 2009
                ''For P/A type there is no computing for trady. it is just the tardy count that is 1. For other types it is calculated based on the assigned tardy and max in tardy and punch time
                ''Since, this is not necessary for P/A type.
                ''For p/A type tardy is not marked in the Scheduled.

                ''Dim dsPunch As New DataSet
                ''Dim strWeekDays As String = ""
                ''dsPunch = dsTardyDate(PrgVerId, StuEnrollId)
                ''If dsPunch.Tables(0).Rows.Count >= 1 Then
                ''    For Each drPunch As DataRow In dsPunch.Tables(0).Rows
                ''        strWeekDays += CType(drPunch("dw"), Integer).ToString & ","
                ''    Next
                ''    If Not strWeekDays = "" Then
                ''        strWeekDays = "(" + Mid(strWeekDays, 1, InStrRev(strWeekDays, ",") - 1).ToString + ")"
                ''    End If
                ''End If

                With sb
                    .Append(" select SchedHours,ActualHours,RecordDate, IsTardy as Tardy ")
                    .Append(" from arStudentClockAttendance ")
                    .Append(" where StuEnrollId=? and ")
                    .Append(" SchedHours  is not null and SchedHours<>0 and (ActualHours is not null and ActualHours <> 999.00 and ActualHours <> 9999.00) ")
                    .Append(" and IsTardy = 1")
                    If MyAdvAppSettings.AppSettings("GradesFormat", campusId).ToString.ToLower = "numeric" Then
                        .Append(" union ")
                        .Append(" Select Schedule,Actual,MeetDate,Tardy  from atConversionAttendance where ")
                        .Append(" StuEnrollId=?  ")
                        .Append(" and Tardy=1 ")
                        .Append(" and Schedule  is not null and Schedule<>0 and (Actual is not null and Actual <> 999.00 and Actual <> 9999.00) ")
                    End If
                End With
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If MyAdvAppSettings.AppSettings("GradesFormat", campusId).ToString.ToLower = "numeric" Then
                    db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                Dim ds As New DataSet
                ds = db.RunParamSQLDataSet(sb.ToString)
                Dim colTardy As DataColumn = New DataColumn("TardyCount", System.Type.GetType("System.Decimal"))
                ds.Tables(0).Columns.Add(colTardy)
                If ds.Tables(0).Rows.Count >= 1 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        If CType(dr("Tardy"), Boolean) = True Then
                            ''Modified By Saraswathi on March 23 2009
                            ''For P/A type there is no computing for trady. it is just the tardy count that is 1. For other types it is calculated based on the assigned tardy and max in tardy and punch time

                            'dr("TardyCount") = CheckTardyIn(dsPunch, CType(dr("dw"), Integer), CType(dr("RecordDate"), Date))
                            dr("TardyCount") = 1
                        End If
                    Next
                End If
                Return ds

            Else
                Dim dsPunch As New DataSet
                Dim strWeekDays As String = ""
                dsPunch = dsTardyDate(PrgVerId, StuEnrollId)
                If dsPunch.Tables(0).Rows.Count >= 1 Then
                    For Each drPunch As DataRow In dsPunch.Tables(0).Rows
                        strWeekDays += CType(drPunch("dw"), Integer).ToString & ","
                    Next
                    If Not strWeekDays = "" Then
                        strWeekDays = "(" + Mid(strWeekDays, 1, InStrRev(strWeekDays, ",") - 1).ToString + ")"
                    End If
                End If
                ''Modified by saraswathi on jan 28 2009
                ''Schedule<>0  added
                With sb
                    .Append(" select Distinct SchedHours,ActualHours,RecordDate,DatePart(dw,RecordDate)-1 as dw, " + vbCrLf)
                    .Append(" IsTardy as Tardy " + vbCrLf)
                    .Append(" from arStudentClockAttendance " + vbCrLf)
                    .Append(" where StuEnrollId=? and " + vbCrLf)
                    .Append(" SchedHours  is not null and SchedHours <>0 and (ActualHours is not null and ActualHours <> 999.00 and ActualHours <> 9999.00) " + vbCrLf)
                    .Append(" and ((IsTardy=1) " + vbCrLf)
                    'If Not strWeekDays = "" Then
                    '    .Append(" or ((DatePart(dw,RecordDate)-1) in " & strWeekDays & ")" + vbCrLf)
                    'End If
                    .Append(")")
                    If MyAdvAppSettings.AppSettings("GradesFormat", campusId).ToString.ToLower = "numeric" Then
                        .Append(" union " + vbCrLf)
                        .Append(" Select Distinct Schedule,Actual,MeetDate as RecordDate,DatePart(dw,MeetDate)-1 as dw,Tardy  from atConversionAttendance where " + vbCrLf)
                        .Append(" StuEnrollId=?  " + vbCrLf)
                        .Append(" and Tardy=1 " + vbCrLf)
                        .Append(" and Schedule  is not null and Schedule <>0 and (Actual is not null and Actual <> 999.00 and Actual <> 9999.00) " + vbCrLf)
                    End If
                    ''Addded by SAraswathi on jan 28 2009
                    ''TO order the tardies, since, the nth tardy will be penealised, the order is important
                    .Append(" order by RecordDate ")
                End With
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If MyAdvAppSettings.AppSettings("GradesFormat", campusId).ToString.ToLower = "numeric" Then
                    db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                Dim ds As New DataSet
                ds = db.RunParamSQLDataSet(sb.ToString)
                Dim colTardy As DataColumn = New DataColumn("TardyCount", System.Type.GetType("System.Decimal"))
                ds.Tables(0).Columns.Add(colTardy)
                If ds.Tables(0).Rows.Count >= 1 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        If CType(dr("Tardy"), Boolean) = True Then
                            dr("TardyCount") = CheckTardyIn(dsPunch, CType(dr("dw"), Integer), CType(dr("RecordDate"), Date))
                        End If
                    Next
                End If
                Return ds
            End If
        End Function

        Public Function GetStudentSummaryDetailsForDateRange(ByVal StuEnrollId As String, ByVal startDate As Date, ByVal cutoffDate As Date, Optional ByVal AttUnitType As String = "") As DataSet
            Dim db As New DataAccess
            Dim sb As New StringBuilder

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ''Modified by saraswathi on jan 28 2009
            ''Schedule<>0  added
            If Not AttUnitType.ToString.ToLower = "hrs" And Not AttUnitType.ToString.ToLower = "minutes" Then
                ''Modified By Saraswathi on March 23 2009
                ''For P/A type there is no computing for trady. it is just the tardy count that is 1. For other types it is calculated based on the assigned tardy and max in tardy and punch time
                ''Since, this is not necessary for P/A type.
                ''For p/A type tardy is not marked in the Scheduled.

                ''Dim dsPunch As New DataSet
                ''Dim strWeekDays As String = ""
                ''dsPunch = dsTardyDate(PrgVerId, StuEnrollId)
                ''If dsPunch.Tables(0).Rows.Count >= 1 Then
                ''    For Each drPunch As DataRow In dsPunch.Tables(0).Rows
                ''        strWeekDays += CType(drPunch("dw"), Integer).ToString & ","
                ''    Next
                ''    If Not strWeekDays = "" Then
                ''        strWeekDays = "(" + Mid(strWeekDays, 1, InStrRev(strWeekDays, ",") - 1).ToString + ")"
                ''    End If
                ''End If

                With sb
                    .Append(" select SchedHours,ActualHours,RecordDate, IsTardy as Tardy ")
                    .Append(" from arStudentClockAttendance ")
                    .Append(" where StuEnrollId=? and ")
                    .Append(" SchedHours  is not null and SchedHours<>0 and (ActualHours is not null and ActualHours <> 999.00 and ActualHours <> 9999.00) ")
                    .Append(" and IsTardy = 1 and (RecordDate BETWEEN ? AND ?)")
                    If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                        .Append(" union ")
                        .Append(" Select Schedule,Actual,MeetDate,Tardy  from atConversionAttendance where ")
                        .Append(" StuEnrollId=?  ")
                        .Append(" and Tardy=1 ")
                        .Append(" and Schedule  is not null and Schedule<>0 and (Actual is not null and Actual <> 999.00 and Actual <> 9999.00)  AND (meetDate BETWEEN ? AND ? )")
                    End If
                End With
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@startDate", startDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@cutoff", cutoffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                    db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@startDate", startDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@cutoff", cutoffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                End If

                Dim ds As New DataSet
                ds = db.RunParamSQLDataSet(sb.ToString)
                Dim colTardy As DataColumn = New DataColumn("TardyCount", System.Type.GetType("System.Decimal"))
                ds.Tables(0).Columns.Add(colTardy)
                If ds.Tables(0).Rows.Count >= 1 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        If CType(dr("Tardy"), Boolean) = True Then
                            ''Modified By Saraswathi on March 23 2009
                            ''For P/A type there is no computing for trady. it is just the tardy count that is 1. For other types it is calculated based on the assigned tardy and max in tardy and punch time

                            'dr("TardyCount") = CheckTardyIn(dsPunch, CType(dr("dw"), Integer), CType(dr("RecordDate"), Date))
                            dr("TardyCount") = 1
                        End If
                    Next
                End If
                Return ds

            Else
                Dim dsPunch As New DataSet
                Dim strWeekDays As String = ""
                dsPunch = dsTardyDate("", StuEnrollId)
                If dsPunch.Tables(0).Rows.Count >= 1 Then
                    For Each drPunch As DataRow In dsPunch.Tables(0).Rows
                        strWeekDays += CType(drPunch("dw"), Integer).ToString & ","
                    Next
                    If Not strWeekDays = "" Then
                        strWeekDays = "(" + Mid(strWeekDays, 1, InStrRev(strWeekDays, ",") - 1).ToString + ")"
                    End If
                End If
                ''Modified by saraswathi on jan 28 2009
                ''Schedule<>0  added
                With sb
                    .Append(" select Distinct SchedHours,ActualHours,RecordDate,DatePart(dw,RecordDate)-1 as dw, " + vbCrLf)
                    .Append(" IsTardy as Tardy " + vbCrLf)
                    .Append(" from arStudentClockAttendance " + vbCrLf)
                    .Append(" where StuEnrollId=? and " + vbCrLf)
                    .Append(" SchedHours  is not null and SchedHours <>0  and (RecordDate BETWEEN ? AND ?) and (ActualHours is not null and ActualHours <> 999.00 and ActualHours <> 9999.00) " + vbCrLf)
                    .Append(" and ((IsTardy=1) " + vbCrLf)
                    'If Not strWeekDays = "" Then
                    '    .Append(" or ((DatePart(dw,RecordDate)-1) in " & strWeekDays & ")" + vbCrLf)
                    'End If
                    .Append(")")
                    If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                        .Append(" union " + vbCrLf)
                        .Append(" Select Distinct Schedule,Actual,MeetDate as RecordDate,DatePart(dw,MeetDate)-1 as dw,Tardy  from atConversionAttendance where " + vbCrLf)
                        .Append(" StuEnrollId=?  " + vbCrLf)
                        .Append(" and Tardy=1 " + vbCrLf)
                        .Append(" and Schedule  is not null and Schedule <>0  and (meetDate BETWEEN ? AND ?) and (Actual is not null and Actual <> 999.00 and Actual <> 9999.00) " + vbCrLf)
                    End If
                    ''Addded by SAraswathi on jan 28 2009
                    ''TO order the tardies, since, the nth tardy will be penealised, the order is important
                    .Append(" order by RecordDate ")
                End With
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@startDate", startDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                db.AddParameter("@cutoff", cutoffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                    db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@startDate", startDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                    db.AddParameter("@cutoff", cutoffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                Dim ds As New DataSet
                ds = db.RunParamSQLDataSet(sb.ToString)
                Dim colTardy As DataColumn = New DataColumn("TardyCount", System.Type.GetType("System.Decimal"))
                ds.Tables(0).Columns.Add(colTardy)
                If ds.Tables(0).Rows.Count >= 1 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        If CType(dr("Tardy"), Boolean) = True Then
                            dr("TardyCount") = CheckTardyIn(dsPunch, CType(dr("dw"), Integer), CType(dr("RecordDate"), Date))
                        End If
                    Next
                End If
                Return ds
            End If
        End Function

        Public Function GetStudentSummaryDetailsForCutOffDate(ByVal StuEnrollId As String, ByVal cutoffDate As Date, Optional ByVal AttUnitType As String = "") As DataSet
            Dim db As New DataAccess
            Dim sb As New StringBuilder

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ''Modified by saraswathi on jan 28 2009
            ''Schedule<>0  added
            If Not AttUnitType.ToString.ToLower = "hrs" And Not AttUnitType.ToString.ToLower = "minutes" Then
                ''Modified By Saraswathi on March 23 2009
                ''For P/A type there is no computing for trady. it is just the tardy count that is 1. For other types it is calculated based on the assigned tardy and max in tardy and punch time
                ''Since, this is not necessary for P/A type.
                ''For p/A type tardy is not marked in the Scheduled.

                ''Dim dsPunch As New DataSet
                ''Dim strWeekDays As String = ""
                ''dsPunch = dsTardyDate(PrgVerId, StuEnrollId)
                ''If dsPunch.Tables(0).Rows.Count >= 1 Then
                ''    For Each drPunch As DataRow In dsPunch.Tables(0).Rows
                ''        strWeekDays += CType(drPunch("dw"), Integer).ToString & ","
                ''    Next
                ''    If Not strWeekDays = "" Then
                ''        strWeekDays = "(" + Mid(strWeekDays, 1, InStrRev(strWeekDays, ",") - 1).ToString + ")"
                ''    End If
                ''End If

                With sb
                    .Append(" select SchedHours,ActualHours,RecordDate, IsTardy as Tardy ")
                    .Append(" from arStudentClockAttendance ")
                    .Append(" where StuEnrollId=? and ")
                    .Append(" SchedHours  is not null and SchedHours<>0 and (ActualHours is not null and ActualHours <> 999.00 and ActualHours <> 9999.00) ")
                    .Append(" and IsTardy = 1 and RecordDate <= ? ")
                    If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                        .Append(" union ")
                        .Append(" Select Schedule,Actual,MeetDate,Tardy  from atConversionAttendance where ")
                        .Append(" StuEnrollId=?  ")
                        .Append(" and Tardy=1 ")
                        .Append(" and Schedule  is not null and Schedule<>0 and (Actual is not null and Actual <> 999.00 and Actual <> 9999.00)  and meetDate <= ? ")
                    End If
                End With
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@cutoff", cutoffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                    db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@cutoff", cutoffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If

                Dim ds As New DataSet
                ds = db.RunParamSQLDataSet(sb.ToString)
                Dim colTardy As DataColumn = New DataColumn("TardyCount", System.Type.GetType("System.Decimal"))
                ds.Tables(0).Columns.Add(colTardy)
                If ds.Tables(0).Rows.Count >= 1 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        If CType(dr("Tardy"), Boolean) = True Then
                            ''Modified By Saraswathi on March 23 2009
                            ''For P/A type there is no computing for trady. it is just the tardy count that is 1. For other types it is calculated based on the assigned tardy and max in tardy and punch time

                            'dr("TardyCount") = CheckTardyIn(dsPunch, CType(dr("dw"), Integer), CType(dr("RecordDate"), Date))
                            dr("TardyCount") = 1
                        End If
                    Next
                End If
                Return ds

            Else
                Dim dsPunch As New DataSet
                Dim strWeekDays As String = ""
                dsPunch = dsTardyDate("", StuEnrollId)
                If dsPunch.Tables(0).Rows.Count >= 1 Then
                    For Each drPunch As DataRow In dsPunch.Tables(0).Rows
                        strWeekDays += CType(drPunch("dw"), Integer).ToString & ","
                    Next
                    If Not strWeekDays = "" Then
                        strWeekDays = "(" + Mid(strWeekDays, 1, InStrRev(strWeekDays, ",") - 1).ToString + ")"
                    End If
                End If
                ''Modified by saraswathi on jan 28 2009
                ''Schedule<>0  added
                With sb
                    .Append(" select Distinct SchedHours,ActualHours,RecordDate,DatePart(dw,RecordDate)-1 as dw, " + vbCrLf)
                    .Append(" IsTardy as Tardy " + vbCrLf)
                    .Append(" from arStudentClockAttendance " + vbCrLf)
                    .Append(" where StuEnrollId=? and " + vbCrLf)
                    .Append(" SchedHours  is not null and SchedHours <>0  and RecordDate <= ? and (ActualHours is not null and ActualHours <> 999.00 and ActualHours <> 9999.00) " + vbCrLf)
                    .Append(" and ((IsTardy=1) " + vbCrLf)
                    'If Not strWeekDays = "" Then
                    '    .Append(" or ((DatePart(dw,RecordDate)-1) in " & strWeekDays & ")" + vbCrLf)
                    'End If
                    .Append(")")
                    If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                        .Append(" union " + vbCrLf)
                        .Append(" Select Distinct Schedule,Actual,MeetDate as RecordDate,DatePart(dw,MeetDate)-1 as dw,Tardy  from atConversionAttendance where " + vbCrLf)
                        .Append(" StuEnrollId=?  " + vbCrLf)
                        .Append(" and Tardy=1 " + vbCrLf)
                        .Append(" and Schedule  is not null and Schedule <>0  and meetDate <= ? and (Actual is not null and Actual <> 999.00 and Actual <> 9999.00) " + vbCrLf)
                    End If
                    ''Addded by SAraswathi on jan 28 2009
                    ''TO order the tardies, since, the nth tardy will be penealised, the order is important
                    .Append(" order by RecordDate ")
                End With
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@cutoff", cutoffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                    db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@cutoff", cutoffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                Dim ds As New DataSet
                ds = db.RunParamSQLDataSet(sb.ToString)
                Dim colTardy As DataColumn = New DataColumn("TardyCount", System.Type.GetType("System.Decimal"))
                ds.Tables(0).Columns.Add(colTardy)
                If ds.Tables(0).Rows.Count >= 1 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        If CType(dr("Tardy"), Boolean) = True Then
                            dr("TardyCount") = CheckTardyIn(dsPunch, CType(dr("dw"), Integer), CType(dr("RecordDate"), Date))
                        End If
                    Next
                End If
                Return ds
            End If
        End Function
        Public Function GetStudentSummaryDetailsForCutOffDate_SP(ByVal StuEnrollId As String, ByVal cutoffDate As Date, Optional ByVal AttUnitType As String = "", Optional ByVal Campusid As String = "") As DataSet


            Dim ds As DataSet
            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

            'add studen enrollment parameter
            If Not AttUnitType.ToString.ToLower = "hrs" And Not AttUnitType.ToString.ToLower = "minutes" Then
                If MyAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                    db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

                    'add cutOff date parameter
                    db.AddParameter("@cutOffDate", cutoffDate, SqlDbType.DateTime, , ParameterDirection.Input)

                    ds = db.RunParamSQLDataSet_SP("dbo.usp_GetStudentSummaryDetailsPANumeric")


                Else
                    db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

                    'add cutOff date parameter
                    db.AddParameter("@cutOffDate", cutoffDate, SqlDbType.DateTime, , ParameterDirection.Input)

                    ds = db.RunParamSQLDataSet_SP("dbo.usp_GetStudentSummaryDetailsPANonNumeric")
                End If
                Dim colTardy As DataColumn = New DataColumn("TardyCount", System.Type.GetType("System.Decimal"))
                ds.Tables(0).Columns.Add(colTardy)
                If ds.Tables(0).Rows.Count >= 1 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        If CType(dr("Tardy"), Boolean) = True Then
                            dr("TardyCount") = 1
                        End If
                    Next
                End If
                Try
                    Return ds
                Catch ex As Exception
                    Return Nothing
                Finally
                    db.CloseConnection()
                End Try


            Else
                Dim dsPunch As New DataSet
                Dim strWeekDays As String = ""
                dsPunch = dsTardyDate("", StuEnrollId)
                If dsPunch.Tables(0).Rows.Count >= 1 Then
                    For Each drPunch As DataRow In dsPunch.Tables(0).Rows
                        strWeekDays += CType(drPunch("dw"), Integer).ToString & ","
                    Next
                    If Not strWeekDays = "" Then
                        strWeekDays = "(" + Mid(strWeekDays, 1, InStrRev(strWeekDays, ",") - 1).ToString + ")"
                    End If
                End If
                If MyAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                    db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

                    'add cutOff date parameter
                    db.AddParameter("@cutOffDate", cutoffDate, SqlDbType.DateTime, , ParameterDirection.Input)

                    ds = db.RunParamSQLDataSet_SP("dbo.usp_GetStudentSummaryDetailsHrsMinutesNumeric")


                Else
                    db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

                    'add cutOff date parameter
                    db.AddParameter("@cutOffDate", cutoffDate, SqlDbType.DateTime, , ParameterDirection.Input)

                    ds = db.RunParamSQLDataSet_SP("dbo.usp_GetStudentSummaryDetailsHrsMinutesNonNumeric")
                End If

                Dim colTardy As DataColumn = New DataColumn("TardyCount", System.Type.GetType("System.Decimal"))
                ds.Tables(0).Columns.Add(colTardy)
                If ds.Tables(0).Rows.Count >= 1 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        If CType(dr("Tardy"), Boolean) = True Then
                            dr("TardyCount") = CheckTardyIn(dsPunch, CType(dr("dw"), Integer), CType(dr("RecordDate"), Date))
                        End If
                    Next
                End If
                Try
                    Return ds
                Catch ex As Exception
                    Return Nothing
                Finally
                    db.CloseConnection()
                End Try

            End If

        End Function

        Public Function dsTardyDate(ByVal PrgVerId As String, ByVal StuEnrollId As String) As DataSet
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            Dim dr As OleDbDataReader
            Dim dsPunch As New DataSet
            Dim arrCheckTardyIn As New ArrayList

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                With sb
                    .Append("   Select Distinct t2.dw,t4.PunchedInTime,t2.DeadLineBeforeConsideredTardy, " + vbCrLf)
                    .Append("   t2.TimeTakenAsPunchIn,PunchInDay " + vbCrLf)
                    .Append("   from arProgSchedules t1, " + vbCrLf)
                    .Append("   (select Distinct ScheduleId,dw, " + vbCrLf)
                    .Append("   Convert(char(10),Max_beforeTardy,108) as DeadLineBeforeConsideredTardy, " + vbCrLf)
                    .Append("   Convert(char(10),tardy_intime,108) as TimeTakenAsPunchIn " + vbCrLf)
                    .Append("   from arProgScheduleDetails where Check_TardyIn=1) t2,arStudentSchedules t3, " + vbCrLf)
                    .Append(" (select Distinct Convert(char(10),PunchTime,101) as PunchInDay,StuEnrollId," + vbCrLf)
                    .Append(" (Select Top 1 Convert(char(10),PunchTime,108) from arStudentTimeClockPunches where PunchType=1 and " + vbCrLf)
                    .Append(" Convert(DATE,PunchTime,111)=Convert(DATE,t5.PunchTime,111) and StuEnrollId=t5.StuEnrollId  " + vbCrLf)
                    .Append(" ) as PunchedInTime " + vbCrLf)
                    .Append("   from arStudentTimeClockPunches t5 where PunchType=1) t4 " + vbCrLf)
                    .Append("   where t1.ScheduleId=t2.ScheduleId and t2.ScheduleId=t3.ScheduleId and " + vbCrLf)
                    .Append("   t3.StuEnrollId=t4.StuEnrollId and ")
                    If PrgVerId <> "" Then
                        .Append(" t1.PrgVerId=? and " + vbCrLf)
                    End If
                    .Append("   t3.StuEnrollId=? " + vbCrLf)
                    ''Added by saraswathi on jan 27 2008
                    .Append("   and t2.dw=DatePart(dw,PunchInDay)-1 ")
                    .Append("   and t4.PunchedInTime > t2.DeadLineBeforeConsideredTardy " + vbCrLf)
                    .Append("   Order by t2.dw ")
                End With
                If PrgVerId <> "" Then
                    db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                dsPunch = db.RunParamSQLDataSet(sb.ToString)
                Return dsPunch
            Catch ex As System.Exception
            Finally
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
        End Function

        Public Function dsTardyDate_SP(ByVal StuEnrollId As String) As DataSet
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            Dim dr As OleDbDataReader
            Dim dsPunch As New DataSet
            Dim arrCheckTardyIn As New ArrayList

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                With sb
                    .Append("   Select Distinct t2.dw,t4.PunchedInTime,t2.DeadLineBeforeConsideredTardy, " + vbCrLf)
                    .Append("   t2.TimeTakenAsPunchIn,PunchInDay " + vbCrLf)
                    .Append("   from arProgSchedules t1, " + vbCrLf)
                    .Append("   (select Distinct ScheduleId,dw, " + vbCrLf)
                    .Append("   Convert(char(10),Max_beforeTardy,108) as DeadLineBeforeConsideredTardy, " + vbCrLf)
                    .Append("   Convert(char(10),tardy_intime,108) as TimeTakenAsPunchIn " + vbCrLf)
                    .Append("   from arProgScheduleDetails where Check_TardyIn=1) t2,arStudentSchedules t3, " + vbCrLf)
                    .Append(" (select Distinct Convert(char(10),PunchTime,101) as PunchInDay,StuEnrollId," + vbCrLf)
                    .Append(" (Select Top 1 Convert(char(10),PunchTime,108) from arStudentTimeClockPunches where PunchType=1 and " + vbCrLf)
                    .Append(" Convert(DATE,PunchTime,111)=Convert(DATE,t5.PunchTime,111) and StuEnrollId=t5.StuEnrollId  " + vbCrLf)
                    .Append(" ) as PunchedInTime " + vbCrLf)
                    .Append("   from arStudentTimeClockPunches t5 where PunchType=1) t4 " + vbCrLf)
                    .Append("   where t1.ScheduleId=t2.ScheduleId and t2.ScheduleId=t3.ScheduleId and " + vbCrLf)
                    .Append("   t3.StuEnrollId=t4.StuEnrollId and ")

                    .Append("   t3.StuEnrollId=? " + vbCrLf)
                    ''Added by saraswathi on jan 27 2008
                    .Append("   and t2.dw=DatePart(dw,PunchInDay)-1 ")
                    .Append("   and t4.PunchedInTime > t2.DeadLineBeforeConsideredTardy " + vbCrLf)
                    .Append("   Order by t2.dw ")
                End With
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                dsPunch = db.RunParamSQLDataSet(sb.ToString)
                Return dsPunch
            Catch ex As System.Exception
            Finally
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
        End Function
        Private Function CheckTardyIn(ByVal dsPunch As DataSet, ByVal dw As Integer, ByVal dtDate As Date) As Decimal
            If dsPunch.Tables(0).Rows.Count >= 1 Then
                Dim dtPunch As DataTable = dsPunch.Tables(0)
                dtPunch.PrimaryKey = New DataColumn() {dtPunch.Columns("PunchInDay")}
                Dim drRow As DataRow = dtPunch.Rows.Find(dtDate.ToString("d", System.Globalization.DateTimeFormatInfo.InvariantInfo))
                If Not drRow Is Nothing Then
                    If dw = CType(drRow("dw"), Integer) Then
                        Dim TimeToBeConsideredPunchInTime As Date = "00:00:00"
                        Dim ActualPunchInTime As Date = "00:00:00"
                        Dim DeadLineBeforeConsideredTardy As Date = "00:00:00"
                        If Not drRow("PunchedInTime") Is System.DBNull.Value Then ActualPunchInTime = CType(drRow("PunchedInTime"), DateTime)
                        If Not drRow("DeadLineBeforeConsideredTardy") Is System.DBNull.Value Then DeadLineBeforeConsideredTardy = CType(drRow("DeadLineBeforeConsideredTardy"), DateTime)
                        If Not drRow("TimeTakenAsPunchIn") Is System.DBNull.Value Then TimeToBeConsideredPunchInTime = CType(drRow("TimeTakenAsPunchIn"), DateTime)
                        Dim decTardy As Decimal = ComputeTardy(ActualPunchInTime, DeadLineBeforeConsideredTardy, TimeToBeConsideredPunchInTime)
                        Return decTardy
                        Exit Function
                    Else
                        Return 0.0
                        Exit Function
                    End If
                End If
            Else
                Return 0.0
                Exit Function
            End If
        End Function
        Private Function ComputeTardy(ByVal ActualPunchInTime As Date, ByVal DeadLineBeforeConsideredTardy As Date, ByVal TimeToBeConsideredPunchInTime As Date) As Decimal
            Dim decTardyValue As Decimal = 0.0
            If Not ActualPunchInTime = "00:00:00" And Not TimeToBeConsideredPunchInTime = "00:00:00" And TimeToBeConsideredPunchInTime > ActualPunchInTime Then
                decTardyValue = ((TimeToBeConsideredPunchInTime - ActualPunchInTime).TotalHours)
            ElseIf Not ActualPunchInTime = "00:00:00" And Not TimeToBeConsideredPunchInTime = "00:00:00" And TimeToBeConsideredPunchInTime < ActualPunchInTime Then
                decTardyValue = ((ActualPunchInTime - TimeToBeConsideredPunchInTime).TotalHours)
            ElseIf Not ActualPunchInTime = "00:00:00" And TimeToBeConsideredPunchInTime = "00:00:00" And Not DeadLineBeforeConsideredTardy = "00:00:00" And DeadLineBeforeConsideredTardy > ActualPunchInTime Then
                decTardyValue = ((DeadLineBeforeConsideredTardy - ActualPunchInTime).TotalHours)
            ElseIf Not ActualPunchInTime = "00:00:00" And TimeToBeConsideredPunchInTime = "00:00:00" And Not DeadLineBeforeConsideredTardy = "00:00:00" And DeadLineBeforeConsideredTardy < ActualPunchInTime Then
                decTardyValue = ((ActualPunchInTime - DeadLineBeforeConsideredTardy).TotalHours)
            End If
            Return decTardyValue
        End Function
        Public Function GetLDAForDroppedStudent(ByVal StuEnrollId As String, Optional ByVal SysStatusId As Integer = 12) As String
            Dim sb As New StringBuilder
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            With sb
                .Append(" Select Distinct DateDetermined from arStuEnrollments " + vbCrLf)
                .Append(" where StuEnrollId=? and StatusCodeId in " + vbCrLf)
                .Append(" (select distinct StatusCodeId from syStatusCodes where SysStatusId=?)" + vbCrLf)
            End With
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@SysStatusId", SysStatusId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Dim dr As OleDbDataReader
            Dim dtLDA As String = ""
            Try
                dr = db.RunParamSQLDataReader(sb.ToString)
                While dr.Read()
                    If dr("DateDetermined") Is System.DBNull.Value Then dtLDA = "" Else dtLDA = CType(dr("DateDetermined"), Date).ToShortDateString
                End While
            Catch ex As System.Exception
            Finally
                dr.Close()
                db.CloseConnection()
            End Try
            Return dtLDA
        End Function
        Public Function GetLDA(ByVal stuEnrollId As String) As DateTime

            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            '   build the sql query
            Dim sb As New StringBuilder
            With sb
                .Append("select Max(LDA) from ")
                .Append("( ")
                .Append("	select max(AttendedDate)as LDA from arExternshipAttendance where StuEnrollId=? ")
                .Append("	union all ")
                .Append("	select max(MeetDate) as LDA from atClsSectAttendance where StuEnrollId=? and Actual >= 1 ")
                .Append("	union all ")
                .Append("	select max(AttendanceDate) as LDA from atAttendance where EnrollId=? and Actual >=1 ")
                .Append("	union all ")
                .Append("	select max(RecordDate) as LDA from arStudentClockAttendance where StuEnrollId=? and (ActualHours >=1.00 and  ActualHours <> 99.00 and ActualHours <> 999.00 and ActualHours <> 9999.00) ")
                .Append("	union all ")
                .Append("select max(MeetDate) as LDA from atConversionAttendance where StuEnrollId=? and (Actual >=1.00 and  Actual <> 99.00 and Actual <> 999.00 and Actual <> 9999.00) ")
                .Append("	union all ")
                .Append("	select  LDA from arStuEnrollments where StuEnrollId=?  ")
                .Append(")	T0 ")
            End With

            ' Add StuEnrollId to the parameter list
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            ' Add StuEnrollId to the parameter list
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            ' Add StuEnrollId to the parameter list
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            ' Add StuEnrollId to the parameter list
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            ' Add StuEnrollId to the parameter list
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            Dim obj As Object = db.RunParamSQLScalar(sb.ToString)
            If obj Is Nothing Then
                Return Date.MinValue
            ElseIf obj Is System.DBNull.Value Then
                Return Date.MinValue
            Else
                Return CType(obj, Date)
            End If
        End Function
        Public Function GetLDAForTermCutOffDate(ByVal stuEnrollId As String, ByVal termCutOffDate As Date) As DateTime

            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            '   build the sql query
            Dim sb As New StringBuilder
            With sb
                .Append("select Max(LDA) from ")
                .Append("( ")
                .Append("	select max(AttendedDate)as LDA from arExternshipAttendance where StuEnrollId=?  and AttendedDate <= ? ")
                .Append("	union all ")
                .Append("	select max(MeetDate) as LDA from atClsSectAttendance where StuEnrollId=? and Actual >= 1 and MeetDate <= ? ")
                .Append("	union all ")
                .Append("	select max(AttendanceDate) as LDA from atAttendance where EnrollId=? and Actual >=1 and AttendanceDate <= ? ")
                .Append("	union all ")
                .Append("	select max(RecordDate) as LDA from arStudentClockAttendance where StuEnrollId=? and (ActualHours >=1.00 and  ActualHours <> 99.00 and ActualHours <> 999.00 and ActualHours <> 9999.00) and RecordDate <= ? ")
                .Append("	union all ")
                .Append("	select max(MeetDate) as LDA from atConversionAttendance where StuEnrollId=? and (Actual >=1.00 and  Actual <> 99.00 and Actual <> 999.00 and Actual <> 9999.00) and MeetDate <= ? ")
                .Append("	union all ")
                .Append("	select LDA from arStuEnrollments where StuEnrollId=? and LDA <= ? ")
                .Append(")	T0 ")
            End With

            ' Add StuEnrollId to the parameter list
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@cutoff", termCutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            ' Add StuEnrollId to the parameter list
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@cutoff", termCutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            ' Add StuEnrollId to the parameter list
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@cutoff", termCutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            ' Add StuEnrollId to the parameter list
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@cutoff", termCutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            ' Add StuEnrollId to the parameter list
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@cutoff", termCutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@cutoff", termCutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   Execute the query
            Dim obj As Object = db.RunParamSQLScalar(sb.ToString)
            If obj Is Nothing Then
                Return Date.MinValue
            ElseIf obj Is System.DBNull.Value Then
                Return Date.MinValue
            Else
                Return CType(obj, Date)
            End If
        End Function
        Public Sub UpdateScheduleByDateAndEnrollment(ByVal StuEnrollId As String,
                                                          ByVal Source As String,
                                                          ByVal PostedDate As Date,
                                                          Optional ByVal ddlSDH0ChangedValue As String = "")
            Dim sb As New StringBuilder
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            If Source = "conversion" Then
                With sb
                    .Append(" Update atConversionAttendance " + vbCrLf)
                    .Append(" Set Schedule=? " + vbCrLf)
                    .Append(" where StuEnrollId=? and (Schedule = 1.0 or Schedule=0.0) and MeetDate=? " + vbCrLf)
                End With
                db.AddParameter("@Schedule", ddlSDH0ChangedValue, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@MeetDate", PostedDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Try
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Catch ex As System.Exception
                Finally
                    db.CloseConnection()
                End Try
            Else
                With sb
                    .Append(" Update arStudentClockAttendance " + vbCrLf)
                    .Append(" Set SchedHours=? " + vbCrLf)
                    .Append(" where StuEnrollId=? and (SchedHours = 1.0 or SchedHours=0.0) and RecordDate=? " + vbCrLf)
                End With
                db.AddParameter("@SchedHours", ddlSDH0ChangedValue, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@RecordDate", PostedDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Try
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Catch ex As System.Exception
                Finally
                    db.CloseConnection()
                End Try
            End If

        End Sub
        Public Function GetScheduledHoursByWeek(ByVal StuEnrollId As String) As Decimal
            Dim sb As New StringBuilder
            Dim db As New DataAccess
            Dim decSchedHours As Decimal = 0.0

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            With sb
                .Append(" select Sum(t4.Total) from " + vbCrLf)
                .Append(" arStuEnrollments t1,arStudentSchedules t2, arProgSchedules t3, arProgScheduleDetails t4 " + vbCrLf)
                .Append(" where t1.StuEnrollId = t2.StuEnrollId And t1.PrgVerId = t3.PrgVerId " + vbCrLf)
                .Append(" and t2.ScheduleId=t3.ScheduleId and t3.scheduleId=t4.ScheduleId and " + vbCrLf)
                .Append(" t2.StuEnrollId=? and t4.total is not null " + vbCrLf)
            End With
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                decSchedHours = CType(db.RunParamSQLScalar(sb.ToString), Decimal)
            Catch ex As System.Exception
                decSchedHours = 0.0
            Finally
                db.ClearParameters()
            End Try
            Return decSchedHours
        End Function


        Public Function GetScheduledHoursByWeek_SP(ByVal StuEnrollId As String) As Decimal
            Dim ds As DataSet
            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

            'add studen enrollment parameter

            db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            Dim decSchedHours As Decimal = 0.0
            Try
                decSchedHours = CType(db.RunParamSQLScalar_SP("dbo.usp_GetScheduledHoursByWeek"), Decimal)
            Catch ex As System.Exception
                decSchedHours = 0.0
            Finally
                db.ClearParameters()
                db.CloseConnection()
            End Try
            Return decSchedHours
        End Function
        ''Added by Saraswathi lakshmanan on Nov 17 th 2008
        ''To find if the student is in LOA on a given date
        ''This is used in Post attendance

        Public Function IsStudentLOADate(ByVal StuEnrollId As String, ByVal hdate As Date) As Boolean
            Dim sb As New StringBuilder
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            With sb
                .Append(" Select Count(StudentLOAId) from arSTudentLOAs where StuEnrollId= ? " + vbCrLf)
                .Append(" and ? >=StartDate and ? <=Enddate " + vbCrLf)
            End With
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@HDate", hdate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@HDate", hdate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Dim count As Integer

            Try
                'return 
                count = CType(db.RunParamSQLScalar(sb.ToString), Integer)
                If count >= 1 Then
                    Return True
                Else
                    Return False

                End If
            Catch ex As System.Exception
            Finally
                db.CloseConnection()
            End Try
        End Function
        'Added by Saraswathi lakshmanan on Nov 17 th 2008
        ''To find if the student was Suspended on a given date
        ''This is used in Post attendance

        Public Function IsStudentSuspendedDate(ByVal StuEnrollId As String, ByVal hdate As Date) As Boolean
            Dim sb As New StringBuilder
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            With sb
                .Append(" Select Count(StuSuspensionId) from arStdSuspensions where StuEnrollId= ? " + vbCrLf)
                .Append(" and ? >=StartDate and ? <=Enddate " + vbCrLf)
            End With
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@HDate", hdate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@HDate", hdate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Dim count As Integer

            Try
                'return 
                count = CType(db.RunParamSQLScalar(sb.ToString), Integer)
                If count >= 1 Then
                    Return True
                Else
                    Return False

                End If
            Catch ex As System.Exception
            Finally
                db.CloseConnection()
            End Try
        End Function
#End Region
        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub
    End Class
#End Region
End Namespace
