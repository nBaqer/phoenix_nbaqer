Imports System.Data
Imports System.Data.OleDb
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class StdPhoneDB
    Public Function GetStudentPhones(ByVal studentId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim dr As OleDbDataReader

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            '.Append("SELECT t1.StudentPhoneId, Phone,ST.StatusId,ST.Status, t1.default1, ")
            '.Append(" (select PhoneTypeDescrip from syPhoneType where PhoneTypeId = t1.PhoneTypeID) as Descrip ")
            '.Append("FROM arStudentPhone t1,syStatuses ST ")
            '.Append("WHERE StudentId = ? and t1.StatusId = ST.StatusId ")
            'If statusId = "True" Then
            '    .Append("AND    ST.Status = 'Active' ")
            '    .Append(" Order By t1.Phone ")
            'ElseIf statusId = "False" Then
            '    .Append("AND    ST.Status = 'InActive' ")
            '    .Append(" Order By t1.Phone ")
            'Else
            '    .Append("ORDER BY ST.Status,t1.Phone asc")
            'End If

            .Append("SELECT t1.StudentPhoneId, Phone,ST.StatusId, t1.ForeignPhone,t1.default1, ")
            .Append("(select PhoneTypeDescrip from syPhoneType where PhoneTypeId = t1.PhoneTypeID) as Descrip, ")
            .Append("(Case ST.Status when 'Active' then 1 else 0 end) As Status ")
            .Append("FROM arStudentPhone t1,syStatuses ST, syPhoneType PT ")
            .Append("WHERE StudentId = ? and t1.StatusId = ST.StatusId ")
            .Append(" and t1.PhoneTypeId=PT.PhoneTypeId ")
            '.Append(" and PT.StatusId=ST.StatusId and ST.Status='Active' ")
            .Append("ORDER BY ST.Status,t1.Phone asc")
        End With

        db.OpenConnection()

        db.AddParameter("@studid", studentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'db.AddParameter("@statusid", statusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)


        'Close Connection
        db.CloseConnection()

        Return ds

    End Function

    Public Function DoesDefaultPhoneExist(ByVal StudentId As String, ByVal StudentPhoneId As String) As Integer
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim sGrdSysDetailId As String
        Dim rowCount As Integer
        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("Select count(*) as Count from arStudentPhone a where a.StudentId = ? and default1 = 1 ")
                .Append("and StudentPhoneId <> ?")

            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@stdid", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@stdphoneid", StudentPhoneId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try

        'Return the datatable in the dataset
        Return rowCount

    End Function

    'Public Function GetStudentAddresses(ByVal studentId As String) As DataSet
    '    Dim db As New DataAccess
    '    Dim sb As New System.Text.StringBuilder
    '    Dim ds As DataSet
    '    Dim dr As OleDbDataReader

    '    Try

    '        db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

    '        'With sb
    '        '    .Append("SELECT t1.StdAddressId,Address1,ST.StatusId,ST.Status, t1.default1,t1.AddressTypeId, ")
    '        '    .Append(" (select AddressDescrip from plAddressTypes where AddressTypeId=t1.AddressTypeId) as AddressType ")
    '        '    .Append("FROM arStudAddresses t1,syStatuses ST ")
    '        '    .Append("WHERE StudentId = ? and t1.StatusId = ST.StatusId and Address1 is not null ")
    '        '    If statusId = "True" Then
    '        '        .Append("AND    ST.Status = 'Active' ")
    '        '        .Append(" Order By t1.Address1 ")
    '        '    ElseIf statusId = "False" Then
    '        '        .Append("AND    ST.Status = 'InActive' ")
    '        '        .Append(" Order By t1.Address1 ")
    '        '    Else
    '        '        .Append("ORDER BY ST.Status,t1.Address1 asc")
    '        '    End If
    '        'End With

    '        With sb
    '            .Append("SELECT t1.StudentPhoneId, Phone,ST.StatusId,ST.Status, t1.default1, ")
    '            .Append(" (select PhoneTypeDescrip from syPhoneType where PhoneTypeId = t1.PhoneTypeID) as Descrip ")
    '            .Append(" (Case ST.Status when 'Active' then 1 else 0 end) As Status ")
    '            .Append("FROM arStudentPhone t1,syStatuses ST ")
    '            .Append("WHERE StudentId = ? and t1.StatusId = ST.StatusId ")
    '            .Append("ORDER BY ST.Status,t1.Phone asc")
    '        End With

    '        db.OpenConnection()

    '        db.AddParameter("@studid", studentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        '            db.AddParameter("@statusid", statusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        ds = db.RunParamSQLDataSet(sb.ToString)
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)

    '        Return ds

    '    Catch ex As Exception
    '        Throw New BaseException("Error retrieving student phones - " & ex.InnerException.Message)
    '    Finally
    '        'Close Connection
    '        db.CloseConnection()

    '    End Try

    'End Function
    Public Function DoesDefaultAddExist(ByVal StudentId As String) As Integer
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim sGrdSysDetailId As String
        Dim rowCount As Integer
        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("Select count(*) as Count from arStudAddresses a where a.StudentId = ? and default1 = 1 ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@stdId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try

        'Return the datatable in the dataset
        Return rowCount

    End Function
    Public Function GetStudentPhoneInfo(ByVal StudentPhoneId As String) As StuPhoneInfo

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("    CCT.PhoneTypeId, ")
            .Append("    (Select PhoneTypeDescrip from syPhoneType where PhoneTypeId=CCT.PhoneTypeId) As PhoneType, ")
            .Append("    CCT.StudentPhoneId, ")
            .Append("    CCT.Phone, ")
            .Append("    CCT.BestTime, ")
            .Append("    (Select Status from syStatuses where StatusId=CCT.StatusId) As Status, ")
            .Append("    CCT.Ext, ")
            .Append("    CCT.StatusId, ")
            .Append("    CCT.ForeignPhone, ")
            .Append("    CCT.default1, ")
            .Append("    CCT.ModUser, ")
            .Append("    CCT.ModDate ")
            .Append("FROM  arStudentPhone CCT ")
            .Append("WHERE CCT.StudentPhoneId= ? ")
        End With

        ' Add the AcademicYearId to the parameter list
        db.AddParameter("@AcademicYearId", StudentPhoneId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim StudentPhoneInfo As New StuPhoneInfo

        While dr.Read()

            '   set properties with data from DataReader
            With StudentPhoneInfo
                'get IsInDB
                .IsInDB = True
                '.StudentID = CType(dr("StudentId"), Guid).ToString()

                'get AcademicYearId
                .StdPhoneId = CType(dr("StudentPhoneId"), Guid).ToString()

                If Not (dr("Phone") Is System.DBNull.Value) Then .Phone = CType(dr("Phone"), String).ToString Else .Phone = ""
                If Not (dr("BestTime") Is System.DBNull.Value) Then .BestTime = CType(dr("BestTime"), String).ToString Else .BestTime = ""
                If Not (dr("Ext") Is System.DBNull.Value) Then .Extension = CType(dr("Ext"), String).ToString Else .Extension = ""


                If Not (dr("ForeignPhone") Is System.DBNull.Value) Then .ForeignPhone = dr("ForeignPhone") Else .ForeignPhone = 0

                If Not (dr("default1") Is System.DBNull.Value) Then .Default1 = dr("default1") Else .Default1 = 0


                If Not (dr("StatusId") Is System.DBNull.Value) Then .PhoneStatus = CType(dr("StatusId"), Guid).ToString Else .PhoneStatus = Guid.Empty.ToString
                If Not (dr("PhoneTypeId") Is System.DBNull.Value) Then .PhoneTypeId = CType(dr("PhoneTypeId"), Guid).ToString
                If Not (dr("PhoneType") Is System.DBNull.Value) Then .PhoneType = dr("PhoneType")


                'get ModUser
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate") Else .ModDate = Date.MinValue

                .ModUser = dr("ModUser")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return StudentPhoneInfo

    End Function
    Public Function UpdateStudentPhone(ByVal StudentPhoneInfo As StuPhoneInfo, ByVal user As String) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        '   Connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim intForeignPhone As Integer
        Dim intDefault As Integer

        If StudentPhoneInfo.ForeignPhone = 1 Then
            intForeignPhone = 1
        Else
            intForeignPhone = 0
        End If
        If StudentPhoneInfo.Default1 = 1 Then
            intDefault = 1
        Else
            intDefault = 0
        End If

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("Update arStudentPhone set Phone=?, ")
                .Append(" BestTime=?,Ext=?,PhoneTypeId=?, ")
                .Append("ModUser=?,ModDate=?,StatusId=?,ForeignPhone=?,default1=? ")
                .Append(" where StudentPhoneId = ? ")
            End With

            '   add parameters values to the query

            'Address
            If StudentPhoneInfo.Phone = "" Then
                db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address1", StudentPhoneInfo.Phone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'city
            If StudentPhoneInfo.BestTime = "" Then
                db.AddParameter("@city", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@city", StudentPhoneInfo.BestTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Address2
            If StudentPhoneInfo.Extension = "" Then
                db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", StudentPhoneInfo.Extension, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            'AddressStatus
            If StudentPhoneInfo.PhoneType = "" Or StudentPhoneInfo.PhoneType = Guid.Empty.ToString Then
                db.AddParameter("@Phonetype", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PhoneType", StudentPhoneInfo.PhoneType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If



            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'AddressStatus
            If StudentPhoneInfo.PhoneStatus = "" Or StudentPhoneInfo.PhoneStatus = Guid.Empty.ToString Then
                db.AddParameter("@AddressStatus", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AddressStatus", StudentPhoneInfo.PhoneStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Foreign Zip
            db.AddParameter("@ForeignZip", StudentPhoneInfo.ForeignPhone, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@Default1", intDefault, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)




            'StudentId
            db.AddParameter("@StdaddressId", StudentPhoneInfo.StdPhoneId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)



            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            db.ClearParameters()
            sb.Remove(0, sb.Length)


            StudentPhoneInfo.IsInDB = True
            Return ""
        Catch ex As OleDbException
            '   return an error to the client
            Return ex.Message
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        ''   execute the query
        'Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

        ''   If there were no updated rows then there was a concurrency problem
        'If rowCount = 1 Then
        '    '   return without errors
        '    Return ""
        'Else
        '    Return DALExceptions.BuildConcurrencyExceptionMessage()
        'End If

        'Catch ex As OleDbException
        '    '   return an error to the client
        '    Return DALExceptions.BuildErrorMessage(ex)

        'Finally
        '    'Close Connection
        '    db.CloseConnection()
        'End Try

    End Function
    Public Function AddStudentPhone(ByVal StudentPhoneInfo As StuPhoneInfo, ByVal user As String) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        '   Connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim intForeignPhone As Integer
        Dim intDefault As Integer

        If StudentPhoneInfo.ForeignPhone = 1 Then
            intForeignPhone = 1
        Else
            intForeignPhone = 0
        End If
        If StudentPhoneInfo.Default1 = 1 Then
            intDefault = 1
        Else
            intDefault = 0
        End If

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            Dim sb4 As New StringBuilder
            With sb4
                .Append(" Insert into arStudentPhone(StudentPhoneId,StudentId,Phone,BestTime,")
                .Append(" Ext,PhoneTypeId,ModUser,ModDate,StatusId,default1,ForeignPhone)")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?) ")

            End With

            db.AddParameter("@StdaddressId", StudentPhoneInfo.StdPhoneId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@StudentId", StudentPhoneInfo.StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If StudentPhoneInfo.Phone = "" Then
                db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address1", StudentPhoneInfo.Phone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'city
            If StudentPhoneInfo.BestTime = "" Then
                db.AddParameter("@city", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@city", StudentPhoneInfo.BestTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            'Address2
            If StudentPhoneInfo.Extension = "" Then
                db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", StudentPhoneInfo.Extension, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'AddressStatus
            If StudentPhoneInfo.PhoneType = "" Or StudentPhoneInfo.PhoneType = Guid.Empty.ToString Then
                db.AddParameter("@Phonetype", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PhoneType", StudentPhoneInfo.PhoneType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If




            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'AddressStatus
            If StudentPhoneInfo.PhoneStatus = "" Or StudentPhoneInfo.PhoneStatus = Guid.Empty.ToString Then
                db.AddParameter("@AddressStatus", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AddressStatus", StudentPhoneInfo.PhoneStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If



            db.AddParameter("@Default1", intDefault, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.AddParameter("@ForeignZip", intForeignPhone, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb4.ToString)
            db.ClearParameters()
            sb4.Remove(0, sb4.Length)

            ''Insert data into regent xml 
            'If SingletonAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
            '    Dim regDB As New regentDB
            '    Dim strFilename As String = AdvantageCommonValues.getStudentBatchFileName()
            '    regDB.AddAddressXML(StudentAddressInfo.StudentID, strFilename, StudentAddressInfo.StdAddressId)
            'End If

            'need to add code for regent

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function DeleteStudentPhone(ByVal StudentPhoneId As String, ByVal modDate As DateTime) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        '   Connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM arStudentPhone ")
                .Append("WHERE StudentPhoneId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM arStudentPhone WHERE StudentPhoneId = ? ")
            End With

            '   add parameters values to the query

            '   AcademicYearId
            db.AddParameter("@StdPhoneId", StudentPhoneId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   AcademicYearId
            db.AddParameter("@StdPhoneId2", StudentPhoneId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

End Class
