Imports FAME.Advantage.Common

Public Class ClassScheduleDB

#Region "Public Methods"

    Public Function GetClassSchedule(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        With sb
            'Queries with subqueries
            .Append("SELECT Distinct ")
            .Append("       arClassSections.ClsSectionId,arClassSections.TermId,arTerm.TermDescrip,")
            .Append("       arClassSections.ReqId,arReqs.Descrip,arReqs.Code,arReqs.Credits,arReqs.Hours,arClassSections.ClsSection,")
            .Append("       arClassSections.InstructorId,(SELECT FullName FROM syUsers WHERE UserId=arClassSections.InstructorId) AS InstructorName,")
            .Append("       arClassSections.StartDate,arClassSections.EndDate,arClassSections.MaxStud,")
            .Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,arClassSections.CampusId,")
            .Append("       (SELECT CampDescrip FROM syCampuses WHERE CampusId=arClassSections.CampusId) AS CampusDescrip,")
            .Append("       ( SELECT    COUNT(DISTINCT se.StudentId ) FROM arResults rs, dbo.arStuEnrollments se WHERE rs.TestId = arClassSections.ClsSectionId AND	rs.StuEnrollId=se.StuEnrollId) AS EnrollmentCount ")
            .Append("FROM   arClassSections,arClassSectionTerms,arTerm,arReqs,syCampGrps,syCmpGrpCmps F ")
            .Append("WHERE  arClassSections.ClsSectionId=arClassSectionTerms.ClsSectionId")
            .Append("       AND arClassSectionTerms.TermId=arTerm.TermId ")
            .Append("       AND arClassSections.ReqId=arReqs.ReqId ")
            .Append("       AND syCampGrps.CampGrpId=F.CampGrpId ")
            .Append("       AND arClassSections.CampusId=F.CampusId ")
            .Append(strWhere)
            .Append(" ORDER BY syCampGrps.CampGrpDescrip,CampusDescrip")
            .Append(strOrderBy)
            .Append(",arClassSections.StartDate;")
            .Append("SELECT Distinct ")
            .Append("       A.ClsSectionId,")
            .Append("       B.Descrip AS RoomDescrip,")
            .Append("       A.StartDate, ")
            .Append("       A.EndDate, ")
            .Append("       WorkDaysDescrip=(	CASE WHEN  (A.TimeIntervalId IS NOT NULL)")
            .Append("					        THEN (SELECT WorkDaysDescrip FROM plWorkDays WHERE WorkDaysId=A.WorkDaysId)")
            .Append("					        ELSE (SELECT PeriodDescrip FROM syPeriods WHERE PeriodId=A.PeriodId)")
            .Append("					        END),")
            .Append("       TimeIn=(	CASE WHEN  (A.TimeIntervalId IS NOT NULL)")
            .Append("					THEN (SELECT TimeIntervalDescrip FROM cmTimeInterval WHERE TimeIntervalId=A.TimeIntervalId)")
            .Append("					ELSE (SELECT TimeIntervalDescrip FROM syPeriods,cmTimeInterval WHERE PeriodId=A.PeriodId AND StartTimeId=TimeIntervalId)")
            .Append("					END),")
            .Append("       TimeOut=(	CASE WHEN  (A.TimeIntervalId IS NOT NULL)")
            .Append("					THEN (SELECT TimeIntervalDescrip FROM cmTimeInterval WHERE TimeIntervalId=A.EndIntervalId)")
            .Append("					ELSE (SELECT TimeIntervalDescrip FROM syPeriods,cmTimeInterval WHERE PeriodId=A.PeriodId AND EndTimeId=TimeIntervalId)")
            .Append("					END),")
            .Append("       MeetingType=(   CASE WHEN  (A.TimeIntervalId IS NOT NULL)")
            .Append("                       THEN (0)")
            .Append("                       ELSE (1) END),")
            .Append("       ViewOrder=(	CASE WHEN  (A.TimeIntervalId is not NULL)")
            .Append("					THEN (SELECT ViewOrder FROM plWorkDays WHERE WorkDaysId=A.WorkDaysId)")
            .Append("					ELSE (0)")
            .Append("					END) ")
            .Append("FROM	arClsSectMeetings A,arRooms B ")
            .Append("WHERE  A.RoomId=B.RoomID")
            .Append("       AND EXISTS (SELECT * ")
            .Append("                   FROM    arClassSections,arClassSectionTerms,arTerm,arReqs,syCampGrps,syCmpGrpCmps F ")
            .Append("                   WHERE   arClassSections.ClsSectionId=A.ClsSectionId ")
            .Append("                           AND arClassSections.ClsSectionId=arClassSectionTerms.ClsSectionId")
            .Append("                           AND arClassSectionTerms.TermId=arTerm.TermId")
            .Append("                           AND arClassSections.ReqId=arReqs.ReqId")
            .Append("                           AND syCampGrps.CampGrpId=F.CampGrpId")
            .Append("                           AND arClassSections.CampusId=F.CampusId " & strWhere & ") ")
            .Append("UNION ")
            .Append("SELECT Distinct ")
            .Append("       A.ClsSectionId,")
            .Append("       B.Descrip AS RoomDescrip,")
            .Append("       A.StartDate, ")
            .Append("       A.EndDate, ")
            .Append("       WorkDaysDescrip=(SELECT PeriodDescrip FROM syPeriods WHERE PeriodId=A.AltPeriodId),")
            .Append("       TimeIn=(SELECT TimeIntervalDescrip FROM syPeriods,cmTimeInterval WHERE PeriodId=A.AltPeriodId AND StartTimeId=TimeIntervalId),")
            .Append("       TimeOut=(SELECT TimeIntervalDescrip FROM syPeriods,cmTimeInterval WHERE PeriodId=A.AltPeriodId AND EndTimeId=TimeIntervalId),")
            .Append("       MeetingType=(1),")
            .Append("       ViewOrder=(	CASE WHEN  (A.TimeIntervalId is not NULL)")
            .Append("					THEN (SELECT ViewOrder FROM plWorkDays WHERE WorkDaysId=A.WorkDaysId)")
            .Append("					ELSE (0)")
            .Append("					END) ")
            .Append("FROM	arClsSectMeetings A,arRooms B ")
            .Append("WHERE  A.RoomId=B.RoomID AND A.AltPeriodId IS NOT NULL")
            .Append("       AND EXISTS (SELECT * ")
            .Append("                   FROM    arClassSections,arClassSectionTerms,arTerm,arReqs,syCampGrps,syCmpGrpCmps F ")
            .Append("                   WHERE   arClassSections.ClsSectionId=A.ClsSectionId ")
            .Append("                           AND arClassSections.ClsSectionId=arClassSectionTerms.ClsSectionId")
            .Append("                           AND arClassSectionTerms.TermId=arTerm.TermId")
            .Append("                           AND arClassSections.ReqId=arReqs.ReqId")
            .Append("                           AND syCampGrps.CampGrpId=F.CampGrpId")
            .Append("                           AND arClassSections.CampusId=F.CampusId " & strWhere & ") ")
            .Append("       and exists (SELECT  * FROM syPeriods WHERE PeriodId=A.AltPeriodId) ")
            .Append("ORDER BY A.ClsSectionId,ViewOrder;")
            ''.Append("SELECT ")
            ''.Append("       A.ClsSectionId,B.Descrip AS RoomDescrip,C.WorkDaysDescrip,")
            ''.Append("       (SELECT TimeIntervalDescrip FROM cmTimeInterval WHERE TimeIntervalId=A.TimeIntervalId) AS TimeIn,")
            ''.Append("       (SELECT TimeIntervalDescrip FROM cmTimeInterval WHERE TimeIntervalId=A.EndIntervalId) AS TimeOut ")
            ''.Append("FROM   arClsSectMeetings A,arRooms B,plWorkDays C ")
            ''.Append("WHERE  A.RoomId=B.RoomId AND A.WorkDaysId=C.WorkDaysId ")
            ''.Append("       AND EXISTS (SELECT * ")
            ''.Append("                   FROM    arClassSections,arClassSectionTerms,arTerm,arReqs,syCampGrps,syCmpGrpCmps F ")
            ''.Append("                   WHERE   arClassSections.ClsSectionId=A.ClsSectionId ")
            ''.Append("                           AND arClassSections.ClsSectionId=arClassSectionTerms.ClsSectionId")
            ''.Append("                           AND arClassSectionTerms.TermId=arTerm.TermId")
            ''.Append("                           AND arClassSections.ReqId=arReqs.ReqId")
            ''.Append("                           AND syCampGrps.CampGrpId=F.CampGrpId")
            ''.Append("                           AND arClassSections.CampusId=F.CampusId " & strWhere & ") ")
            ''.Append("ORDER BY A.ClsSectionId,C.ViewOrder")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count = 2 Then
            ds.Tables(0).TableName = "ClassSchedule"
            ds.Tables(1).TableName = "ClsSectionMeetings"
            'Add columns for linking tables on CR.
            ds.Tables(0).Columns.Add(New DataColumn("ClsSectionIdStr", System.Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("ClsSectionIdStr", System.Type.GetType("System.String")))
            ds.Tables(1).Columns.Add(New DataColumn("SuppressDate", System.Type.GetType("System.String")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

#End Region

End Class
