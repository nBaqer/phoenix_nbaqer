Imports FAME.Advantage.Common

Public Class SignInDB

#Region "Private Data Members"
    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")

#End Region


#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

#End Region


#Region "Public Methods"

    Public Function GetStudentSignIn(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            If paramInfo.ResId = 551 Then
                strWhere &= " AND '" & paramInfo.FilterOtherString.Replace("Start Date Range Equal To ", "") & "' between arClassSections.StartDate and arClassSections.EndDate "
            Else
                strWhere &= " AND " & paramInfo.FilterOther
            End If

        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        With sb
            .Append("SELECT ")
            .Append("       arClassSections.ClsSectionId,arClassSections.TermId,arTerm.TermDescrip,")
            .Append("       arClassSections.ReqId,arReqs.Descrip,arReqs.Code,")
            .Append("       arReqs.Credits,arClassSections.ClsSection,")
            .Append("       arClassSections.InstructorId,(SELECT FullName FROM syUsers WHERE UserId=arClassSections.InstructorId) AS InstructorName,")
            .Append("       arClassSections.StartDate,arClassSections.EndDate,arClassSections.MaxStud,")
            .Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,arClassSections.CampusId,")
            .Append("       (SELECT CampDescrip FROM syCampuses WHERE CampusId=arClassSections.CampusId) AS CampusDescrip,")
            .Append("       (SELECT COUNT(*) FROM arResults WHERE TestId=arClassSections.ClsSectionId) AS EnrollmentCount ")
            .Append("FROM   arClassSections,arClassSectionTerms,arTerm,arReqs,syCampGrps,syCmpGrpCmps F ")
            .Append("WHERE  arClassSectionTerms.ClsSectionId=arClassSections.ClsSectionId")
            .Append("       AND arClassSectionTerms.TermId=arTerm.TermId")
            .Append("       AND arClassSections.ReqId=arReqs.ReqId")
            .Append("       AND syCampGrps.CampGrpId=F.CampGrpId")
            .Append("       AND arClassSections.CampusId=F.CampusId")
            .Append(strWhere)
            .Append(" ORDER BY syCampGrps.CampGrpDescrip,CampusDescrip,arTerm.TermDescrip")
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            'Add new column
            ds.Tables(0).Columns.Add(New DataColumn("ClsSectionStr", System.Type.GetType("System.String")))
            ds.Tables(0).TableName = "ClassRosterMain"

            'Get the meet date specified in the ui
            Dim strMeetDate As String
            strMeetDate = paramInfo.FilterOtherString.Replace("Start Date Range Equal To ", "").Trim

            ds.Merge(GetStuInSection(strWhere, paramInfo.ResId, strMeetDate))
            ds.Tables(1).TableName = "ClassRosterSub"
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function


    Public Function GetStuInSection(ByVal WhereClause As String, ByVal resId As Integer, Optional ByVal MeetDate As String = "") As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim stuId As String
        Dim MeetDateClause As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'for signin sheet its going to be always enrollment id
        If resId = 551 Then
            stuId = "B.EnrollmentId"
        Else
            If StudentIdentifier = "EnrollmentId" Then
                stuId = "B.EnrollmentId"
            ElseIf StudentIdentifier = "StudentId" Then
                stuId = "C.StudentNumber"
            Else
                'If StudentIdentifier = "SSN" Then
                stuId = "C.SSN"
            End If
        End If

        If MeetDate <> "" Then
            MeetDateClause = ", '" & MeetDate & "' AS MeetDate "
        End If

        With sb
            'query with subqueries
            .Append("SELECT ")
            .Append("       distinct A.StuEnrollId,")
            .Append("       C.LastName,C.FirstName,C.MiddleName," & stuId & " AS StudentIdentifier,")
            .Append("       G.ProgDescrip as PrgVerId,D.PrgVerDescrip,") 'A.GrdSysDetailId,")
            .Append("       (select ShiftDescrip from arShifts where ShiftId=G.ShiftId) AS GradeDescrip ,")
            .Append("       B.StatusCodeId,SC.StatusCodeDescrip,B.EnrollmentId, ")
            .Append("       B.CohortStartDate as StudentStartDate, ")
            .Append("       arReqs.Descrip + ' (' + arReqs.Code + ')' + ' - ' + arClassSections.ClsSection AS [ClassName], ")
            .Append("       syUsers.FullName, ")
            .Append("       arTerm.TermDescrip, ")
            .Append("       CONVERT(VARCHAR(10), arClassSections.StartDate, 101) AS StartDate, ")
            .Append("       CONVERT(VARCHAR(10), arClassSections.EndDate, 101) AS EndDate ")

            If MeetDate <> "" Then
                .Append(MeetDateClause)
            End If

            .Append(" FROM   arResults A,arStuEnrollments B,arStudent C,arPrgVersions D ,arPrograms G, syStatusCodes SC, arClassSections, arClassSectionTerms, arTerm, arReqs, syCampGrps, syCmpGrpCmps, syUsers ")
            .Append("WHERE  A.StuEnrollId=B.StuEnrollId and D.ProgId=G.ProgId ")
            .Append("       AND B.StudentId=C.StudentId AND B.PrgVerId=D.PrgVerId and B.StatusCodeId=SC.StatusCodeId and SC.SysStatusId not in ( 8 ,12 ) ")
            .Append("       AND A.TestId=arClassSections.ClsSectionId ")
            .Append("       AND arClassSections.TermId=arClassSectionTerms.TermId ")
            .Append("       AND arClassSectionTerms.TermId=arTerm.TermId ")
            .Append("       AND arClassSections.ReqId=arReqs.ReqId ")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
            .Append("       AND syCmpGrpCmps.CampusId=arClassSections.CampusId ")
            .Append("       AND syUsers.UserId=arClassSections.InstructorId ")
            .Append(WhereClause)
            '.Append("       AND EXISTS  (SELECT arClassSections.ClsSectionId ")
            '.Append("                    FROM   arClassSections,arClassSectionTerms,arTerm,arReqs,syCampGrps,syCmpGrpCmps F ")
            '.Append("                    WHERE  A.TestId=arClassSections.ClsSectionId ")
            '.Append("                           AND arClassSectionTerms.ClsSectionId=arClassSections.ClsSectionId")
            '.Append("                           AND arClassSectionTerms.TermId=arTerm.TermId")
            '.Append("                           AND arClassSections.ReqId=arReqs.ReqId")
            '.Append(WhereClause)
            '.Append("                           AND syCampGrps.CampGrpId=F.CampGrpId")
            '.Append("                           AND arClassSections.CampusId=F.CampusId) ")
            .Append("ORDER BY C.LastName,C.FirstName,C.MiddleName")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            'Add new columns
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("TestIdStr", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("StuIdentifierField", System.Type.GetType("System.String")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function

#End Region

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

End Class
