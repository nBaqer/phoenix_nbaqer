Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' AnnouncementsDB.vb
'
' AnnouncementsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class AnnouncementsDB
    Public Function GetAllAnnouncements(ByVal statusSelectIndex As Integer, ByVal moduleName As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         A.AnnouncementId, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As StatusSelect, ")
            .Append("         A.AnnouncementCode, ")
            .Append("         A.AnnouncementDescrip ")
            .Append("FROM     syAnnouncements A, syStatuses ST, syModules SM ")
            .Append("WHERE    A.StatusId = ST.StatusId ")
            .Append("AND      A.ModuleId = SM.ModuleId ")
            If Not moduleName Is Nothing Then
                .Append(" AND   SM.ModuleName = ? ")
                db.AddParameter("@ModuleName", moduleName, DataAccess.OleDbDataType.OleDbString, 70, ParameterDirection.Input)
            End If
            '   Conditionally include only Active, Inactive or All Items 
            Select Case statusSelectIndex
                Case 0
                    .Append("AND    ST.Status = 'Active' ")
                    .Append("ORDER BY A.AnnouncementCode ")
                Case 1
                    .Append("AND    ST.Status = 'Inactive' ")
                    .Append("ORDER BY A.AnnouncementCode ")
                Case Else
                    .Append("ORDER BY ST.Status, A.AnnouncementCode ")
            End Select
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAnnouncementInfo(ByVal AnnouncementId As String) As AnnouncementInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT A.AnnouncementId, ")
            .Append("    A.ModuleId, ")
            .Append("    A.AnnouncementCode, ")
            .Append("    A.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=A.StatusId) As Status, ")
            .Append("    A.AnnouncementDescrip, ")
            .Append("    A.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=A.CampGrpId) As CampGrpDescrip, ")
            .Append("    A.ModUser, ")
            .Append("    A.ModDate ")
            .Append("FROM  syAnnouncements A ")
            .Append("WHERE A.AnnouncementId= ? ")
        End With

        ' Add the AnnouncementId to the parameter list
        db.AddParameter("@AnnouncementId", AnnouncementId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim AnnouncementInfo As New AnnouncementInfo

        While dr.Read()

            '   set properties with data from DataReader
            With AnnouncementInfo
                .AnnouncementId = AnnouncementId
                .IsInDB = True
                .ModuleId = dr("ModuleId")
                .Code = dr("AnnouncementCode")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Description = dr("AnnouncementDescrip")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        
        '   Return AnnouncementInfo
        Return AnnouncementInfo

    End Function
    Public Function UpdateAnnouncementInfo(ByVal AnnouncementInfo As AnnouncementInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syAnnouncements Set AnnouncementId = ?, ModuleId = ?, AnnouncementCode = ?, ")
                .Append(" StatusId = ?, AnnouncementDescrip = ?, CampGrpId = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE AnnouncementId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from syAnnouncements where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   AnnouncementId
            db.AddParameter("@AnnouncementId", AnnouncementInfo.AnnouncementId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModuleId
            db.AddParameter("@ModuleId", AnnouncementInfo.ModuleId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)

            '   AnnouncementCode
            db.AddParameter("@AnnouncementCode", AnnouncementInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", AnnouncementInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   AnnouncementDescrip
            db.AddParameter("@AnnouncementDescrip", AnnouncementInfo.Description, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   CampGrpId
            If AnnouncementInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", AnnouncementInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   AnnouncementId
            db.AddParameter("@AdmDepositId", AnnouncementInfo.AnnouncementId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", AnnouncementInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)
            'db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddAnnouncementInfo(ByVal AnnouncementInfo As AnnouncementInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT syAnnouncements (AnnouncementId, ModuleId, AnnouncementCode, StatusId, ")
                .Append("   AnnouncementDescrip, CampGrpId, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   AnnouncementId
            db.AddParameter("@AnnouncementId", AnnouncementInfo.AnnouncementId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModuleId
            db.AddParameter("@ModuleId", AnnouncementInfo.ModuleId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)

            '   AnnouncementCode
            db.AddParameter("@AnnouncementCode", AnnouncementInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", AnnouncementInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   AnnouncementDescrip
            db.AddParameter("@AnnouncementDescrip", AnnouncementInfo.Description, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   CampGrpId
            If AnnouncementInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", AnnouncementInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteAnnouncementInfo(ByVal AnnouncementId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syAnnouncements ")
                .Append("WHERE AnnouncementId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM syAnnouncements WHERE AnnouncementId = ? ")
            End With

            '   add parameters values to the query

            '   AnnouncementId
            db.AddParameter("@AnnouncementId", AnnouncementId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   AnnouncementId
            db.AddParameter("@AnnouncementId", AnnouncementId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
End Class

