Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class StdProgressDB
    Public Function GetStudentEnrollments(ByVal StudentId As String, ByVal showActiveOnly As String, Optional ByVal campusId As String = Nothing) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try
            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            With sb
                .Append("SELECT t1.StuEnrollId,t1.PrgVerId, t2.PrgVerDescrip + ' (' + SC.StatusCodeDescrip + ')' AS PrgVerDescrip,ST.StatusId,ST.Status ")
                .Append("FROM arStuEnrollments t1, arPrgVersions t2,syStatuses ST,syStatusCodes SC ")
                .Append("WHERE t1.PrgVerId = t2.PrgVerId and t2.StatusId =ST.StatusId and t1.StatusCodeId=SC.StatusCodeId and t1.StudentId = ? ")
                If Not campusId Is Nothing Then
                    .Append("AND t1.CampusId = ? ")
                End If
                If showActiveOnly = "True" Then
                    .Append("AND    ST.Status = 'Active' ")
                    .Append(" Order By t1.ExpStartDate desc,t2.PrgVerDescrip ")
                ElseIf showActiveOnly = "False" Then
                    .Append("AND    ST.Status = 'Inactive' ")
                    .Append(" Order By t1.ExpStartDate desc,t2.PrgVerDescrip ")
                Else
                    .Append("ORDER BY ST.Status,t1.ExpStartDate desc,t2.PrgVerDescrip asc")
                End If
            End With
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            ' Add the campus id to the parameter list
            If Not campusId Is Nothing Then
                db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "StdEnrollments")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function GetStudentProgress(ByVal StuEnrollId As String) As StudentProgressInfo

        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb1 As New StringBuilder
        Dim sb2 As New StringBuilder
        Dim sb3 As New StringBuilder
        Dim StuEnrollInfo As New StudentProgressInfo

        'With sb1
        '    .Append("SELECT sum(t4.Credits) as CredsAttmptd ")
        '    .Append("FROM arPrgVersions t1, arStuProgVer t2, arResults t3, arReqs t4, arGradeSystemDetails t5, arTerm t6 WHERE ")
        '    .Append("t2.PrgVerId = t1.PrgVerId And t2.StudentId = t3.StudentId and ")
        '    .Append("t3.ReqId = t4.ReqId and t1.GrdSystemId = t5.GrdSystemId and t3.TermId = t6.TermId and ")
        '    .Append("t2.PrgVerId = ? and t2.StudentId = ?  and t3.Grade = t5.Grade and t5.IsCreditsAttempted = 1 ")
        'End With

        With sb1
            .Append("SELECT sum(t4.Credits) as CredsAttmptd  ")
            .Append("FROM  arResults t3, arClassSections t7, arReqs t4, arGradeSystemDetails t5 WHERE  ")
            .Append("t3.TestId = t7.ClsSectionId and t7.ReqId = t4.ReqId  and ")
            .Append("t3.StuEnrollId = ? and t3.GrdSysDetailId = t5.GrdSysDetailId and t5.IsCreditsAttempted = 1  ")
        End With

        'Add the parameter list
        'db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr1 As OleDbDataReader = db.RunParamSQLDataReader(sb1.ToString)
        While dr1.Read()
            With StuEnrollInfo
                If Not (dr1("CredsAttmptd") Is System.DBNull.Value) Then
                    .CredsAttmptd = dr1("CredsAttmptd").ToString
                End If
            End With
        End While
        db.ClearParameters()

        'With sb2
        '    .Append("SELECT sum(t4.Credits) as CredsCompltd ")
        '    .Append("FROM arPrgVersions t1, arStuProgVer t2, arResults t3, arReqs t4, arGradeSystemDetails t5, arTerm t6 WHERE ")
        '    .Append("t2.PrgVerId = t1.PrgVerId And t2.StudentId = t3.StudentId and ")
        '    .Append("t3.ReqId = t4.ReqId and t1.GrdSystemId = t5.GrdSystemId and ")
        '    .Append("t2.PrgVerId = ? and t2.StudentId = ?  and t3.Grade = t5.Grade and t5.IsCreditsEarned = 1 and ")
        '    .Append("t6.TermId = t3.TermId ")
        'End With

        With sb2
            .Append("SELECT sum(t4.Credits) as CredsCompltd  ")
            .Append("FROM  arResults t3, arClassSections t7, arReqs t4, arGradeSystemDetails t5 WHERE  ")
            .Append("t3.TestId = t7.ClsSectionId and t7.ReqId = t4.ReqId  and ")
            .Append("t3.StuEnrollId = ? and t3.GrdSysDetailId = t5.GrdSysDetailId and t5.IsCreditsEarned = 1  ")
        End With
        'Add the parameter list
        'db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'Execute the query
        Dim dr2 As OleDbDataReader = db.RunParamSQLDataReader(sb2.ToString)
        While dr2.Read()
            With StuEnrollInfo
                If Not (dr2("CredsCompltd") Is System.DBNull.Value) Then
                    .CredsEarned = dr2("CredsCompltd").ToString
                End If
            End With
        End While
        db.ClearParameters()

        'With sb3
        '    .Append("Select Sum(t4.Credits) as TotalCreds, Sum(t5.GPA * t4.Credits)as GradeWeight ")
        '    .Append("FROM arPrgVersions t1, arStuProgVer t2, arResults t3, arReqs t4, arGradeSystemDetails t5, arTerm t6 WHERE ")
        '    .Append("t2.PrgVerId = t1.PrgVerId And t2.StudentId = t3.StudentId and ")
        '    .Append("t3.ReqId = t4.ReqId and t1.GrdSystemId = t5.GrdSystemId and ")
        '    .Append("t2.PrgVerId = ? and t2.StudentId = ?  and t3.Grade = t5.Grade and t5.IsInGPA = 1 ")
        '    .Append("and t6.TermId = t3.TermId ")
        'End With

        With sb3
            .Append("Select Sum(t4.Credits) as TotalCreds, Sum(t5.GPA * t4.Credits)as GradeWeight ")
            .Append("FROM arResults t3, arReqs t4, arGradeSystemDetails t5, arClassSections t7 WHERE ")
            .Append("t3.TestId = t7.ClsSectionId and t7.ReqId = t4.ReqId and ")
            .Append("t3.StuEnrollId = ? and t3.GrdSysDetailId = t5.GrdSysDetailId and t5.IsInGPA = 1 ")

        End With
        'Add the parameter list
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr3 As OleDbDataReader = db.RunParamSQLDataReader(sb3.ToString)
        While dr3.Read()
            With StuEnrollInfo
                If Not (dr3("GradeWeight") Is System.DBNull.Value) And Not (dr3("TotalCreds") Is System.DBNull.Value) Then
                    .GPA = dr3("GradeWeight") / dr3("TotalCreds")
                End If
            End With
        End While
        db.ClearParameters()

        If Not dr1.IsClosed Then dr1.Close()
        If Not dr2.IsClosed Then dr2.Close()
        If Not dr3.IsClosed Then dr3.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        
        'Return BankInfo
        Return StuEnrollInfo
    End Function


End Class
