
Imports System.Data.OleDb
Imports System.Data
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class DropStudentDB

    Public Function GetDataGridInfo(ByVal stuEnrollId As String, ByVal term As String, ByVal campusId As String) As DataSet

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        'Prepare the connection
        Dim conn As SqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim sb As StringBuilder = New StringBuilder()

        With sb
            .Append(" SELECT DISTINCT  A.TestId,B.ClsSectionId,C.Code,C.Descrip, A.StuEnrollId,B.TermId ")
            .Append(" FROM arResults A ")
            .Append(" JOIN arClassSections B ON B.ClsSectionId = A.TestId ")
            .Append(" JOIN arReqs C ON C.ReqId = B.ReqId  ")
            .Append(" WHERE A.StuEnrollId = @stuenrollid  ")
            .Append(" AND B.TermId = @termid  ")
            .Append(" AND A.GrdSysDetailId IS NULL  ")
        End With

        Dim command As SqlCommand = New SqlCommand(sb.ToString(), conn)
        command.Parameters.AddWithValue("@stuenrollid", stuEnrollId)
        command.Parameters.AddWithValue("@termid", term)
        conn.Open()
        Try
            Dim dr As SqlDataReader = command.ExecuteReader()
            Dim ds As DataSet = New DataSet()
            Dim dt As DataTable = New DataTable()
            dt.Load(dr)
            ds.Tables.Add(dt)
            ds.AcceptChanges()
            Return ds
        Finally
            conn.Close()
        End Try
    End Function

    'Public Function GetDataGridInfo(ByVal stuEnrollId As String, ByVal term As String, ByVal campusId As String) As DataSet
    '    'Get the Activities DataList
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim da As OleDbDataAdapter
    '    Dim ds As New DataSet

    '    Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

    '    db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

    '    With sb
    '        .Append("SELECT Distinct A.TestId,B.ClsSectionId,C.Code,C.Descrip, ")
    '        .Append("A.StuEnrollId,B.TermId ")
    '        .Append("FROM arResults A,arClassSections B,arReqs C ")
    '        .Append("WHERE A.StuEnrollId = ? and B.TermId = ? ")
    '        .Append("AND A.TestId = B.ClsSectionId and B.ReqId = C.ReqId ")
    '        .Append("AND A.GrdSysDetailId IS NULL ")
    '    End With

    '    ' Add the PrgVerId and ChildId to the parameter list
    '    db.AddParameter("@stuenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@termid", term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    '   Execute the query       
    '    db.OpenConnection()
    '    Try
    '        da = db.RunParamSQLDataAdapter(sb.ToString)
    '        da.Fill(ds, "DropCourse")
    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    '    Return ds
    'End Function

    Public Function GetDropGradeSystemDetails(ByVal ClsSect As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            '   connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()


            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            With sb

                .Append("SELECT GrdSysDetailId,Grade ")

                .Append("FROM arGradeSystemDetails ")

                .Append("WHERE IsDrop = 1 ")

                .Append("AND GrdSystemId IN( ")

                .Append("SELECT GrdSystemId ")

                .Append("FROM arGradeScales ")

                .Append("WHERE GrdScaleId IN( ")

                .Append("SELECT GrdScaleId ")

                .Append("FROM arClassSections ")

                .Append("WHERE ClsSectionId = ? )) ")
            End With


            db.AddParameter("@clssectId", ClsSect, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "GradeSysDetails")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try
        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function GetDropAndFailGrades(ByVal clsSect As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            '   connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()


            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder


            With sb

                .Append("SELECT GrdSysDetailId,Grade ")

                .Append("FROM arGradeSystemDetails ")

                .Append("WHERE IsPass = 0 and GPA = 0 or ")

                .Append("IsDrop = 1 ")

                .Append("AND GrdSystemId IN( ")

                .Append("SELECT GrdSystemId ")

                .Append("FROM arGradeScales ")

                .Append("WHERE GrdScaleId IN( ")

                .Append("SELECT GrdScaleId ")

                .Append("FROM arClassSections ")

                .Append("WHERE ClsSectionId = ? )) ")
            End With


            db.AddParameter("@clssectId", clsSect, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "GradeSysDetails")

            'Close Connection
            db.CloseConnection()


        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try
        'Return the datatable in the dataset
        Return ds

    End Function

    ''' <summary>
    ''' Update the final grade if the GrdSysDetails is null
    ''' </summary>
    ''' <param name="pocoDropCourses"></param>
    Public Sub UpdateFinalGrade(ByVal pocoDropCourses As IList(Of PocoDropCourse))
        'Get settings
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        Dim sql As String
        If myAdvAppSettings.AppSettings("ReSchedDropStudCourses").ToString.ToUpper = "TRUE" Then
            sql = "UPDATE arResults SET GrdSysDetailId = @Detail, ModUser= @ModUser, ModDate= GETDATE(), DateDetermined= @DateD, IsCourseCompleted=1 WHERE TestId = @TestId and StuEnrollId = @EnrollId and GrdSysDetailId is NULL"
        Else
            sql = "UPDATE arResults SET GrdSysDetailId = @Detail, ModUser= @ModUser, ModDate= GETDATE(), DateDetermined= @DateD, IsCourseCompleted=1, DroppedInAddDrop=1, isClinicsSatisfied=1 WHERE TestId = @TestId and StuEnrollId = @EnrollId and GrdSysDetailId is NULL"

        End If

        'Prepare the connection
        Dim conn As SqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)

        ' Prepare the list of command
        Dim listOfCommand As IList(Of SqlCommand) = New List(Of SqlCommand)
        For Each course As PocoDropCourse In pocoDropCourses

            Dim command As SqlCommand = New SqlCommand(sql, conn)
            command.Parameters.AddWithValue("@Detail", course.Grade)
            command.Parameters.AddWithValue("@ModUser", course.User)
            command.Parameters.AddWithValue("@DateD", course.DateDetermined)
            command.Parameters.AddWithValue("@TestId", course.ClassId)
            command.Parameters.AddWithValue("@EnrollId", course.StuEnrollId)
            listOfCommand.Add(command)
        Next
        conn.Open()
        Dim transaction As SqlTransaction
        transaction = conn.BeginTransaction("Dropping")
        Try
            'Prepare the transaction
            For Each command As SqlCommand In listOfCommand
                command.Transaction = transaction
            Next

            'Execute the updates
            For Each command As SqlCommand In listOfCommand
                command.ExecuteNonQuery()
            Next

            'Commit
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback("Dropping")
            Throw
        Finally
            conn.Close()
        End Try
    End Sub


    'Public Function UpdateFinalGrade(ByVal FinalGrdObj As FinalGradeInfo, ByVal user As String)
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim clsSectId As String
    '    Dim stuEnrollId As String
    '    Dim grade As String
    '    Dim dateDetermined As String
    '    Dim intResultCount As Integer

    '    Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

    '    clsSectId = FinalGrdObj.ClsSectId
    '    stuEnrollId = FinalGrdObj.StuEnrollId
    '    grade = FinalGrdObj.Grade
    '    dateDetermined = FinalGrdObj.DateDetermined

    '    'Set the connection string
    '    db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
    '    db.OpenConnection()

    '    With sb
    '        .Append("Select Count(*) as TotalRowCount from arResults where TestId = ? and StuEnrollId = ? ")
    '    End With
    '    db.AddParameter("@TestId", clsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    Try
    '        intResultCount = db.RunParamSQLScalar(sb.ToString)
    '    Catch ex As Exception
    '        intResultCount = 0
    '    Finally
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)
    '    End Try

    '    If intResultCount = 0 Then
    '        With sb
    '            .Append(" Insert into arResults(ResultId,TestId,StuEnrollId,GrdSysDetailId,ModUser,ModDate,IsCourseCompleted) ")
    '            .Append(" Values(?,?,?,?,?,?,1) ")
    '        End With
    '        db.AddParameter("@ResultId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@TestId", clsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@GrdSysDetailId", grade, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)
    '    Else
    '        With sb
    '            .Append("UPDATE arResults Set GrdSysDetailId = ?, ")
    '            .Append("ModUser=?, ")
    '            .Append("ModDate=?, ")
    '            .Append("DateDetermined=?, ")
    '            .Append("IsCourseCompleted=1 ")

    '            If myAdvAppSettings.AppSettings("ReSchedDropStudCourses").ToString.ToUpper = "TRUE" Then
    '                .Append(", DroppedInAddDrop=1 ")
    '            End If
    '            .Append("WHERE TestId = ? and StuEnrollId = ? and GrdSysDetailId is NULL ")
    '        End With
    '        db.AddParameter("@grade", grade, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        ''ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        ''ModDate
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        db.AddParameter("@DateDetermined", dateDetermined, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        db.AddParameter("@clssectid", clsSectId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@stdenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)
    '    End If
    '    'Close Connection
    '    db.CloseConnection()
    'End Function


    'Public Function DeleteScheduledCourses(ByVal finalGrdObj As FinalGradeInfo, ByVal user As String)
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim clsSectId As String
    '    Dim stuEnrollId As String

    '    clsSectId = finalGrdObj.ClsSectId
    '    stuEnrollId = finalGrdObj.StuEnrollId

    '    Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

    '    'Set the connection string
    '    db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
    '    With sb
    '        .Append("Delete from arResults  ")
    '        .Append("WHERE TestId = ? and StuEnrollId = ? ;")
    '        .Append("delete from dbo.atClsSectAttendance where ")
    '        .Append(" ClsSectionId= ? and stuEnrollId= ? ;")
    '    End With
    '    db.AddParameter("@clssectid", clsSectId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@stdenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@clssectid", clsSectId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@stdenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '    db.OpenConnection()
    '    db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '    db.ClearParameters()
    '    sb.Remove(0, sb.Length)

    '    'Close Connection
    '    db.CloseConnection()
    'End Function

    Public Function GetCurrentTerms() As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim da As OleDbDataAdapter
        Dim ds As New DataSet

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()


        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Try
            With sb
                .Append("SELECT t1.TermId,t1.TermDescrip ")
                .Append("FROM arTerm t1,syStatuses t2  ")
                .Append("WHERE t2.Status = 'Active' and t1.StatusId = t2.StatusId ")
                .Append("ORDER BY t1.StartDate ")
            End With

            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "StdTerms")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds


    End Function
    Public Function GetCurrentTerms(ByVal stuEnrollId As String, Optional ByVal campusId As String = "") As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim da As OleDbDataAdapter
        Dim ds As New DataSet

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()


        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Try
            With sb
                .Append("SELECT distinct CST.TermId, T.TermDescrip ")
                .Append("FROM arResults R, arClassSectionTerms CST, arTerm T, syStatuses S ")
                .Append("WHERE ")
                .Append("   		R.TestId=CST.ClsSectionId ")
                .Append("AND		CST.TermId=T.TermId ")
                .Append("AND		T.EndDate > GetDate() ")
                .Append("AND		T.StatusId=S.StatusId ")
                .Append("AND		S.Status='Active' ")
                .Append("AND		StuEnrollId = ? ")
                If campusId <> "" Then
                    .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")

                    .Append("FROM syCmpGrpCmps ")

                    .Append("WHERE CampusId = '")
                    .Append(campusId)
                    .Append("' ")
                    .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                    .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                End If

            End With

            '   Execute the query
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "StdTerms")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds
    End Function
    Public Function GetAllTerms(ByVal stuEnrollId As String, Optional ByVal campusId As String = "") As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim da As OleDbDataAdapter
        Dim ds As New DataSet

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()


        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Try
            With sb
                .Append("SELECT distinct CST.TermId, T.TermDescrip ")
                .Append("FROM arResults R, arClassSectionTerms CST, arTerm T, syStatuses S ")
                .Append("WHERE ")
                .Append("   		R.TestId=CST.ClsSectionId ")
                .Append("AND		CST.TermId=T.TermId ")
                '.Append("AND		T.EndDate > GetDate() ")
                .Append("AND		T.StatusId=S.StatusId ")
                .Append("AND		S.Status='Active' ")
                .Append("AND		StuEnrollId = ? ")
                If campusId <> "" Then
                    .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")

                    .Append("FROM syCmpGrpCmps ")

                    .Append("WHERE CampusId = '")
                    .Append(campusId)
                    .Append("' ")
                    .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                    .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                End If
            End With

            '   Execute the query
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "StdTerms")

            'Close Connection
            db.CloseConnection()
        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds
    End Function


End Class
