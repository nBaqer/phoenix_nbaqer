
Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class MyPortalRegisterDb
    Public Function GetAllStatuses() As DataSet
        Dim ds As DataSet

        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                myPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                myPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = myPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.ClsSectStatusId,Descrip ")
                .Append("FROM syClsSectStatuses t1 ")
            End With

            '   Execute the query
            ds = db.RunSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function GetAllProgramsOfferedInCampus(ByVal campusId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                myPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                myPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = myPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("Select DISTINCT a.ProgId, a.ProgDescrip, ")
                .Append(" case when (select ShiftDescrip from arShifts where shiftid=a.shiftid) is null  then ")
                .Append(" a.ProgDescrip ")
                .Append(" else ")
                .Append("a.ProgDescrip + ' (' + (select ShiftDescrip from arShifts where shiftid=a.shiftid) + ')'  ")
                .Append(" end as ShiftDescrip  ")
                .Append(" from arPrograms a,  ")
                .Append("syStatuses c ")
                .Append("WHERE (CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId = ? ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("AND    a.StatusId = c.StatusId ")
                .Append(" AND     c.Status = 'Active' ")
                .Append("ORDER BY a.ProgDescrip ")


            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@cmpId", campusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "Programs")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds
    End Function
    Public Function GetCurrentAndFutureTermsForProgram(ByVal ProgId As String, Optional ByVal campusid As String = "") As DataSet
        Dim ds As DataSet

        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.TermId,t1.TermDescrip,t1.StartDate ")
                .Append("FROM arTerm t1 , syStatuses t2   ")
                .Append("WHERE  t1.StatusId=t2.StatusId and t2.Status='Active' and t1.EndDate >= ? and t1.IsModule = 0 ")
                .Append("AND (t1.ProgId = ? or t1.ProgId is NULL) ")
                If Not campusid = "" Then
                    .Append("AND ( t1.CampGrpId in (Select Distinct CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    .Append(" OR (CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')))")
                End If
                ''Query Modidfied By Saraswathi to show the terms which has externship courses past the end date of the term
                ''mantis 15439
                .Append(" Union ")
                .Append(" Select T1.TermId,T1.TermDescrip,t1.StartDate from arClassSections C,arTerm T1,arReqs R , syStatuses t2   where  ")
                .Append(" t1.StatusId=t2.StatusId and t2.Status='Active' and ")
                .Append(" T1.TermId = C.TermId")
                .Append("  and (t1.ProgId = ?")
                .Append(" or t1.ProgId is NULL)")
                .Append(" and t1.IsModule = 0 ")
                .Append(" and R.ReqId=C.ReqId and R.IsExternShip=1 and C.EndDate>= getdate() ")
                If Not campusid = "" Then
                    .Append("AND ( t1.CampGrpId in (Select Distinct CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    .Append(" OR (t1.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')))")
                End If
                .Append("ORDER BY t1.StartDate ")
            End With

            db.AddParameter("@edate", Date.Now.ToShortDateString, MyPortalDataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@progId", ProgId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Not campusid = "" Then
                db.AddParameter("@campusid", campusid, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            db.AddParameter("@progId", ProgId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Not campusid = "" Then
                db.AddParameter("@campusid", campusid, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            db.CloseConnection()
            Return ds

        Catch ex As System.Exception
            Throw New BaseException(ex.InnerException.Message)
        End Try

        'Return the 
    End Function

    'Public Function GetCurrentAndFutureTermsForProgram(ByVal ProgId As String, Optional ByVal campusid As String = "") As DataSet
    '    Dim ds As DataSet

    '    Try
    '        '   connect to the database
    '        Dim db As New MyPortalDataAccess
    '        db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '        Dim sb As New System.Text.StringBuilder

    '        '   build the sql query
    '        With sb
    '            .Append("SELECT t1.TermId,t1.TermDescrip ")
    '            .Append("FROM arTerm t1 , syStatuses t2   ")
    '            .Append("WHERE  t1.StatusId=t2.StatusId and t2.Status='Active' and t1.EndDate >= ? and t1.IsModule = 0 ")
    '            .Append("AND (t1.ProgId = ? or t1.ProgId is NULL) ")
    '            If Not campusid = "" Then
    '                .Append("AND ( t1.CampGrpId in (Select Distinct CampGrpId from syCmpGrpCmps where CampusId = ?) ")
    '                .Append(" OR (CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')))")
    '            End If
    '            .Append("ORDER BY t1.StartDate ")
    '        End With

    '        db.AddParameter("@edate", Date.Now.ToShortDateString, MyPortalDataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        db.AddParameter("@progId", ProgId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        If Not campusid = "" Then
    '            db.AddParameter("@campusid", campusid, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If
    '        ds = db.RunParamSQLDataSet(sb.ToString)
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)
    '        db.CloseConnection()
    '        Return ds

    '    Catch ex As System.Exception
    '        Throw New BaseException(ex.InnerException.Message)
    '    End Try

    '    'Return the 
    'End Function

    Public Function GetCurrentAndFutureTermsForStudent(ByVal stuEnrollId As String, Optional ByVal campusid As String = "") As DataSet
        Dim ds As DataSet

        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.TermId,t1.TermDescrip ")
                .Append("FROM arTerm t1 , syStatuses t2   ")
                .Append("WHERE  t1.StatusId=t2.StatusId and t2.Status='Active' and t1.EndDate >= ? and t1.IsModule = 0 ")
                .Append("AND (t1.ProgId is NULL or ( ")
                .Append(" Progid =(select Progid from arPrograms where Progid =(select progid from arPrgVersions where prgverid= ")
                .Append(" (select PrgVerid from arStuEnrollments where stuEnrollid= ? ))) )) ")
                If Not campusid = "" Then
                    .Append(" AND ( t1.CampGrpId in (Select Distinct CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    .Append(" OR (CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')))")
                End If
                .Append(" ORDER BY t1.StartDate ")
            End With

            db.AddParameter("@edate", Date.Now.ToShortDateString, MyPortalDataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@stuEnrollId", stuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Not campusid = "" Then
                db.AddParameter("@campusid", campusid, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            db.CloseConnection()
            Return ds

        Catch ex As System.Exception
            Throw New BaseException(ex.InnerException.Message)
        End Try

        'Return the 
    End Function
    Public Function PopulateClsSectDL(ByVal Term As String, ByVal Shift As String, ByVal CampusId As String, ByVal Course As String, Optional ByVal allTerm As String = "") As DataSet
        Dim ds As New DataSet
        Try
            ds = GetClsSects(Term, Shift, CampusId, Course, allTerm)
        Catch ex As System.Exception
            Throw New System.Exception(ex.Message)
        End Try
        Return ds
    End Function
    Public Function PopulateClsSectDLNew(ByVal ProgId As String, ByVal Term As String, ByVal Shift As String, ByVal CampusId As String, ByVal Course As String, Optional ByVal allTerm As String = "") As DataSet
        Dim ds As New DataSet
        Try
            ds = GetClsSectsNew(ProgId, Term, Shift, CampusId, Course, allTerm)
        Catch ex As System.Exception
            Throw New System.Exception(ex.Message)
        End Try
        Return ds
    End Function

    Public Function GetClsSects(ByVal Term As String, ByVal Shift As String, ByVal CampusId As String, ByVal course As String, Optional ByVal allTerm As String = "") As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT a.ClsSectionId, a.ClsSection, a.ReqId, a.StartDate, a.EndDate, a.MaxStud,a.ShiftId, b.Descrip as Class, b.Code ")
                .Append("FROM arClassSections a, arReqs b ")
                .Append("WHERE a.ReqId = b.ReqId ")
                .Append("AND a.EndDate  > ? ")
                .Append("AND a.CampusId = ? ")
                If Term.ToString <> "" Then
                    .Append(" and (a.TermId = ?) ")
                End If
                If Shift.ToString <> "" Then
                    .Append(" and (a.ShiftId = ?) ")
                End If
                If course.ToString <> "" Then
                    .Append(" and (a.ReqId = ?) ")
                End If
                If allTerm = "NotTermModule" Then
                    .Append(" and 0 = (select IsModule from arTerm where TermId=a.TermId) ")
                ElseIf allTerm = "TermModule" Then
                    .Append(" and 1 = (select IsModule from arTerm where TermId=a.TermId) ")
                End If

            End With

            db.AddParameter("@edate", Date.Now, MyPortalDataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@cmpid", CampusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Term.ToString <> "" Then
                ' Add the PrgVerId and ChildId to the parameter list
                db.AddParameter("@TermId", Term, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If Shift.ToString <> "" Then
                ' Add the PrgVerId and ChildId to the parameter list
                db.AddParameter("@ShiftId", Shift, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            If course.ToString <> "" Then
                ' Add the PrgVerId and ChildId to the parameter list
                db.AddParameter("@ReqId", course, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "TermClsSects")
            db.ClearParameters()
            sb.Remove(0, sb.Length)



            With sb
                .Append("Select a.TestId, count(*) as Cnt ")
                .Append("from arResults a, arClassSections b ")
                .Append("where(a.TestId = b.ClsSectionId) ")
                If Term.ToString <> "" Then
                    .Append("and b.TermId = ? ")
                End If
                If Shift.ToString <> "" Then
                    .Append("and b.ShiftId = ? ")
                End If
                If allTerm = "NotTermModule" Then
                    .Append(" and 0 = (select IsModule from arTerm where TermId=b.TermId) ")
                ElseIf allTerm = "TermModule" Then
                    .Append(" and 1 = (select IsModule from arTerm where TermId=b.TermId) ")
                End If
                ''Added by Saraswathi lakshmanan on March 16 2009
                ''to fix mantis 15478: BUG: Several courses show more students registered than there actually are. 
                .Append(" and a.ResultId not in (select ResultId from arResults R, arGradeSystemDetails GSD where R.GrdSysDetailId=GSD.GrdSysDetailId ")
                .Append(" and GSD.IsDrop=1 and TestId=a.TestID) ")
                .Append("group by a.TestId ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            If Term.ToString <> "" Then
                db.AddParameter("@TermId", Term, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            ' Add the PrgVerId and ChildId to the parameter list
            If Shift.ToString <> "" Then
                db.AddParameter("@shift", Shift, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "RemStdInfo")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try
        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function GetClsSectsNew(ByVal ProgId As String, ByVal Term As String, ByVal Shift As String, ByVal CampusId As String, ByVal course As String, Optional ByVal allTerm As String = "") As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT distinct a.ClsSectionId, a.ClsSection, a.ReqId, a.StartDate, a.EndDate, a.MaxStud,a.ShiftId, b.Descrip as Class, b.Code ")
                .Append("FROM arClassSections a, arReqs b,arProgVerDef c,arPrgVersions d,arPrograms e  ")
                .Append("WHERE a.ReqId = b.ReqId ")
                .Append("AND a.EndDate  > ? ")
                .Append("AND a.CampusId = ? ")
                If Term.ToString <> "" Then
                    .Append(" and (a.TermId = ?) ")
                End If
                If Shift.ToString <> "" Then
                    .Append(" and (a.ShiftId = ?) ")
                End If
                If course.ToString <> "" Then
                    .Append(" and (a.ReqId = ?) ")
                End If
                If allTerm = "NotTermModule" Then
                    .Append(" and 0 = (select IsModule from arTerm where TermId=a.TermId) ")
                ElseIf allTerm = "TermModule" Then
                    .Append(" and 1 = (select IsModule from arTerm where TermId=a.TermId) ")
                End If
                .Append(" and (b.Reqid=c.Reqid or b.reqid in(select reqid from dbo.arReqGrpDef where Grpid=c.reqid)) ")
                .Append(" and c.PrgVerId=d.PrgVerid and d.ProgId=e.ProgId and (a.ShiftId=e.Shiftid or e.shiftid is null) ")
                If ProgId <> "" Then
                    .Append(" and e.Progid= ? ")
                End If
            End With

            db.AddParameter("@edate", Date.Now, MyPortalDataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@cmpid", CampusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Term.ToString <> "" Then
                ' Add the PrgVerId and ChildId to the parameter list
                db.AddParameter("@TermId", Term, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If Shift.ToString <> "" Then
                ' Add the PrgVerId and ChildId to the parameter list
                db.AddParameter("@ShiftId", Shift, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            If course.ToString <> "" Then
                ' Add the PrgVerId and ChildId to the parameter list
                db.AddParameter("@ReqId", course, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If ProgId <> "" Then
                db.AddParameter("@ProgId", ProgId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "TermClsSects")
            db.ClearParameters()
            sb.Remove(0, sb.Length)



            With sb
                .Append("Select a.TestId, count(*) as Cnt ")
                .Append("from arResults a, arClassSections b ")
                .Append("where(a.TestId = b.ClsSectionId) ")
                If Term.ToString <> "" Then
                    .Append("and b.TermId = ? ")
                End If
                If Shift.ToString <> "" Then
                    .Append("and b.ShiftId = ? ")
                End If
                If allTerm = "NotTermModule" Then
                    .Append(" and 0 = (select IsModule from arTerm where TermId=b.TermId) ")
                ElseIf allTerm = "TermModule" Then
                    .Append(" and 1 = (select IsModule from arTerm where TermId=b.TermId) ")
                End If
                ''Added by Saraswathi lakshmanan on March 16 2009
                ''to fix mantis 15478: BUG: Several courses show more students registered than there actually are. 
                .Append(" and a.ResultId not in (select ResultId from arResults R, arGradeSystemDetails GSD where R.GrdSysDetailId=GSD.GrdSysDetailId ")
                .Append(" and GSD.IsDrop=1 and TestId=a.TestID) ")

                .Append("group by a.TestId ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            If Term.ToString <> "" Then
                db.AddParameter("@TermId", Term, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            ' Add the PrgVerId and ChildId to the parameter list
            If Shift.ToString <> "" Then
                db.AddParameter("@shift", Shift, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "RemStdInfo")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    'Public Function GetAvailSelectStdsFrClsSect(ByVal ClsSectId As String, ByVal campusId As String) As DataSet
    '    Dim ds As New DataSet
    '    Dim da As OleDbDataAdapter

    '    Try
    '        '   connect to the database
    '        Dim db As New MyPortalDataAccess
    '        db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '        Dim sb As New System.Text.StringBuilder


    '        With sb
    '            .Append("SELECT * FROM ( ")
    '            .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t4.ReqId as ReqId, t3.Descrip As Req ")
    '            .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6 ")
    '            .Append("WHERE t1.StudentId=t5.StudentId AND ")
    '            .Append("t3.ReqId = t4.ReqId AND ")
    '            .Append("t1.PrgVerId = t4.PrgVerId AND ")
    '            .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? ")



    '            .Append("UNION ")

    '            .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t7.ReqId as ReqId, t3.Descrip As Req ")
    '            .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6, arReqGrpDef t7 ")
    '            .Append("WHERE t1.StudentId=t5.StudentId AND ")
    '            .Append("t3.ReqId = t4.ReqId AND ")
    '            .Append("t1.PrgVerId = t4.PrgVerId AND ")
    '            .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? AND ")            
    '            .Append("t3.ReqTypeId = 2 AND ")
    '            .Append("t4.ReqId = t7.GrpId ")


    '            .Append("UNION ")

    '            .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t8.ReqId as ReqId, t3.Descrip As Req ")
    '            .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6, arReqGrpDef t7, arReqGrpDef t8 ")
    '            .Append("WHERE t1.StudentId=t5.StudentId AND ")
    '            .Append("t3.ReqId = t4.ReqId AND ")
    '            .Append("t1.PrgVerId = t4.PrgVerId AND ")
    '            .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? AND ")
    '            .Append("t3.ReqTypeId = 2 AND ")
    '            .Append("t4.ReqId = t7.GrpId AND ")
    '            .Append("t7.ReqId = t8.GrpId ")


    '            .Append("UNION ")

    '            .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t9.ReqId as ReqId, t3.Descrip As Req ")
    '            .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6, arReqGrpDef t7, arReqGrpDef t8, arReqGrpDef t9 ")
    '            .Append("WHERE t1.StudentId=t5.StudentId AND ")
    '            .Append("t3.ReqId = t4.ReqId AND ")
    '            .Append("t1.PrgVerId = t4.PrgVerId AND ")
    '            .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? AND ") 
    '            .Append("t3.ReqTypeId = 2 AND ")
    '            .Append("t4.ReqId = t7.GrpId AND ")
    '            .Append("t7.ReqId = t8.GrpId AND ")
    '            .Append("t8.ReqId = t9.GrpId ")


    '            .Append("UNION ")

    '            .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t10.ReqId as ReqId, t3.Descrip As Req ")
    '            .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6, arReqGrpDef t7, arReqGrpDef t8, arReqGrpDef t9, arReqGrpDef t10 ")
    '            .Append("WHERE t1.StudentId=t5.StudentId AND ")
    '            .Append("t3.ReqId = t4.ReqId AND ")
    '            .Append("t1.PrgVerId = t4.PrgVerId AND ")
    '            .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? AND ")
    '            .Append("t3.ReqTypeId = 2 AND ")
    '            .Append("t4.ReqId = t7.GrpId AND ")
    '            .Append("t7.ReqId = t8.GrpId AND ")
    '            .Append("t8.ReqId = t9.GrpId AND ")
    '            .Append("t9.ReqId = t10.GrpId ")


    '            .Append("UNION ")

    '            .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t11.ReqId as ReqId, t3.Descrip As Req ")
    '            .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6, arReqGrpDef t7, arReqGrpDef t8, arReqGrpDef t9, arReqGrpDef t10, arReqGrpDef t11 ")
    '            .Append("WHERE t1.StudentId=t5.StudentId AND ")
    '            .Append("t3.ReqId = t4.ReqId AND ")
    '            .Append("t1.PrgVerId = t4.PrgVerId AND ")
    '            .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? AND ")
    '            .Append("t3.ReqTypeId = 2 AND ")
    '            .Append("t4.ReqId = t7.GrpId AND ")
    '            .Append("t7.ReqId = t8.GrpId AND ")
    '            .Append("t8.ReqId = t9.GrpId AND ")
    '            .Append("t9.ReqId = t10.GrpId AND ")
    '            .Append("t10.ReqId = t11.GrpId ")


    '            .Append("UNION ")

    '            .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t12.ReqId as ReqId, t3.Descrip As Req ")
    '            .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6, arReqGrpDef t7, arReqGrpDef t8, arReqGrpDef t9, arReqGrpDef t10, arReqGrpDef t11, arReqGrpDef t12 ")
    '            .Append("WHERE t1.StudentId=t5.StudentId AND ")
    '            .Append("t3.ReqId = t4.ReqId AND ")
    '            .Append("t1.PrgVerId = t4.PrgVerId AND ")
    '            .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? AND ")
    '            .Append("t3.ReqTypeId = 2 AND ")
    '            .Append("t4.ReqId = t7.GrpId AND ")
    '            .Append("t7.ReqId = t8.GrpId AND ")
    '            .Append("t8.ReqId = t9.GrpId AND ")
    '            .Append("t9.ReqId = t10.GrpId AND ")
    '            .Append("t10.ReqId = t11.GrpId AND ")
    '            .Append("t11.ReqId = t12.GrpId ) R5, arClassSections R6 ")

    '            .Append("WHERE R5.ReqId = R6.ReqId ")

    '            .Append(" and R5.StuEnrollId not in ")
    '            .Append("  (Select Distinct(t7.StuEnrollId)from arResults t7,arClassSections t8 ")
    '            .Append("   where t8.ReqId in (Select ReqId from arClassSections where ClsSectionId = ?) ")
    '            .Append("  and t7.TestId = t8.ClsSectionId and t7.GrdSysDetailId is NULL) ")

    '            .Append("and R5.StatusCodeId IN (Select StatusCodeId from syStatusCodes where SysStatusId IN (7,9,13)) ")

    '            .Append("and ((R5.ShiftId = R6.ShiftId) or (R5.ShiftId is Null and R6.ShiftId is NULL)) ")

    '            .Append("and R6.clsSectionId= ? ")

    '            .Append("order by R5.ExpGradDate ")

    '        End With
    '        ' Add the PrgVerId and ChildId to the parameter list
    '        db.AddParameter("@campusid", campusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.AddParameter("@campusid", campusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.AddParameter("@campusid", campusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.AddParameter("@campusid", campusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.AddParameter("@campusid", campusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.AddParameter("@campusid", campusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.AddParameter("@campusid", campusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.AddParameter("@ClsSectId", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@ClsSectId2", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   Execute the query
    '        db.OpenConnection()
    '        da = db.RunParamSQLDataAdapter(sb.ToString)
    '        da.Fill(ds, "AvailStds")
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)

    '        '   build the sql query
    '        With sb
    '            .Append("SELECT distinct t1.StuEnrollId, t1.ModUser, t5.LastName, t5.FirstName, t6.ExpGradDate, t7.ReqId ")
    '            .Append("FROM arResults t1, arStudent t5, arStuEnrollments t6, arClassSections t7 where t1.TestId = ? and t1.StuEnrollId = t6.StuEnrollId and t6.StudentId = t5.StudentId ")
    '            .Append("and t1.TestId = t7.ClsSectionId ")
    '            'Added on 3/31/2005 by Troy. This is to accommodate dropping courses during the add/drop period.
    '            'During this period no grade is recorded when a course is dropped. Ideally the record should
    '            'be deleted from the arResults table. However, if we actually deleted the record it would no
    '            'longer appear in the instructor's gradebook. It is good to preserve the scores that were already
    '            'recorded in the grade book just in case the student decide to take up the course again.
    '            'The arResults table was modified to include a DroppedInAddDrop field. This defaults to false
    '            'but is set to true when a course is dropped during the add/drop period.
    '            .Append("and t1.DroppedInAddDrop = 0")
    '        End With

    '        ' Add the PrgVerId and ChildId to the parameter list
    '        db.AddParameter("@ClsSectId", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   Execute the query
    '        db.OpenConnection()
    '        da = db.RunParamSQLDataAdapter(sb.ToString)
    '        da.Fill(ds, "SelectedStuds")
    '        Dim iNum As Integer
    '        iNum = ds.Tables("SelectedStuds").Rows.Count
    '        'Close Connection
    '        db.CloseConnection()

    '    Catch ex As System.Exception
    '        Throw New BaseException(ex.Message)
    '    End Try

    '    'Return the datatable in the dataset
    '    Return ds

    'End Function

    Public Function GetAvailSelectStdsFrClsSect_SP(ByVal clsSectId As String, ByVal campusId As String) As DataSet
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        Dim db As New FAME.DataAccessLayer.SQLDataAccess

        Dim myPortalAdvAppSettings As MyPortalAdvAppSettings = MyPortalAdvAppSettings.GetAppSettings()

        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConnectionString")
        Dim termId As String = GetTermForClassSection(clsSectId)

        If termId <> "00000000-0000-0000-0000-000000000000" Then
            db.AddParameter("@ProgID", New Guid(termId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If
        db.AddParameter("@ClsSectionID", New Guid(clsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@CampusID", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.OpenConnection()

        Try
            If termId <> "00000000-0000-0000-0000-000000000000" Then
                da = db.RunParamSQLDataAdapter_SP("dbo.USP_AR_RegSt_GetAvailableStudentsfortheProgAndClsSection")
            Else
                da = db.RunParamSQLDataAdapter_SP("dbo.USP_AR_RegSt_GetAvailableStudentsforAllProgsAndClsSection")
            End If
            da.Fill(ds, "AvailStds")
        Finally
            db.CloseConnection()
        End Try

        db.ClearParameters()
        db.AddParameter("@TestId", New Guid(clsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter_SP("dbo.USP_AR_RegStd_GetSelectedStudentsforClsId")
            da.Fill(ds, "SelectedStuds")

        Finally
            db.CloseConnection()
        End Try

        Return ds

    End Function

    Public Function GetAvailSelectStdsFrClsSect(ByVal clsSectId As String, ByVal campusId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            Dim termId As String = GetTermForClassSection(clsSectId)

            With sb
                .Append("SELECT * FROM ( ")
                .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName,t1.StartDate, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t4.ReqId as ReqId, t3.Descrip As Req ")
                .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6 ")
                .Append("WHERE t1.StudentId=t5.StudentId AND ")
                .Append("t3.ReqId = t4.ReqId AND ")
                .Append("t1.PrgVerId = t4.PrgVerId AND ")
                .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? ")
                'Added to exclude the Continuing Ed program versions
                .Append("AND t6.IsContinuingEd = 0 ")
                If termId <> "00000000-0000-0000-0000-000000000000" Then
                    .Append(" and t6.ProgId = '" & termId & "'")
                End If



                .Append("UNION ")

                .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName,t1.StartDate, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t7.ReqId as ReqId, t3.Descrip As Req ")
                .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6, arReqGrpDef t7 ")
                .Append("WHERE t1.StudentId=t5.StudentId AND ")
                .Append("t3.ReqId = t4.ReqId AND ")
                .Append("t1.PrgVerId = t4.PrgVerId AND ")
                .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? AND ")
                .Append("t3.ReqTypeId = 2 AND ")
                .Append("t4.ReqId = t7.GrpId ")
                'Added to exclude the Continuing Ed program versions
                .Append("AND t6.IsContinuingEd = 0 ")
                If termId <> "00000000-0000-0000-0000-000000000000" Then
                    .Append(" and t6.ProgId = '" & termId & "'")
                End If

                .Append("UNION ")

                .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName,t1.StartDate, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t8.ReqId as ReqId, t3.Descrip As Req ")
                .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6, arReqGrpDef t7, arReqGrpDef t8 ")
                .Append("WHERE t1.StudentId=t5.StudentId AND ")
                .Append("t3.ReqId = t4.ReqId AND ")
                .Append("t1.PrgVerId = t4.PrgVerId AND ")
                .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? AND ")
                .Append("t3.ReqTypeId = 2 AND ")
                .Append("t4.ReqId = t7.GrpId AND ")
                .Append("t7.ReqId = t8.GrpId ")
                'Added to exclude the Continuing Ed program versions
                .Append("AND t6.IsContinuingEd = 0 ")
                If termId <> "00000000-0000-0000-0000-000000000000" Then
                    .Append(" and t6.ProgId = '" & termId & "'")
                End If

                .Append("UNION ")

                .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName,t1.StartDate, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t9.ReqId as ReqId, t3.Descrip As Req ")
                .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6, arReqGrpDef t7, arReqGrpDef t8, arReqGrpDef t9 ")
                .Append("WHERE t1.StudentId=t5.StudentId AND ")
                .Append("t3.ReqId = t4.ReqId AND ")
                .Append("t1.PrgVerId = t4.PrgVerId AND ")
                .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? AND ")
                .Append("t3.ReqTypeId = 2 AND ")
                .Append("t4.ReqId = t7.GrpId AND ")
                .Append("t7.ReqId = t8.GrpId AND ")
                .Append("t8.ReqId = t9.GrpId ")
                'Added to exclude the Continuing Ed program versions
                .Append("AND t6.IsContinuingEd = 0 ")
                If termId <> "00000000-0000-0000-0000-000000000000" Then
                    .Append(" and t6.ProgId = '" & termId & "'")
                End If

                .Append("UNION ")

                .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName,t1.StartDate, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t10.ReqId as ReqId, t3.Descrip As Req ")
                .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6, arReqGrpDef t7, arReqGrpDef t8, arReqGrpDef t9, arReqGrpDef t10 ")
                .Append("WHERE t1.StudentId=t5.StudentId AND ")
                .Append("t3.ReqId = t4.ReqId AND ")
                .Append("t1.PrgVerId = t4.PrgVerId AND ")
                .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? AND ")
                .Append("t3.ReqTypeId = 2 AND ")
                .Append("t4.ReqId = t7.GrpId AND ")
                .Append("t7.ReqId = t8.GrpId AND ")
                .Append("t8.ReqId = t9.GrpId AND ")
                .Append("t9.ReqId = t10.GrpId ")
                'Added to exclude the Continuing Ed program versions
                .Append("AND t6.IsContinuingEd = 0 ")
                If termId <> "00000000-0000-0000-0000-000000000000" Then
                    .Append(" and t6.ProgId = '" & termId & "'")
                End If

                .Append("UNION ")

                .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName,t1.StartDate, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t11.ReqId as ReqId, t3.Descrip As Req ")
                .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6, arReqGrpDef t7, arReqGrpDef t8, arReqGrpDef t9, arReqGrpDef t10, arReqGrpDef t11 ")
                .Append("WHERE t1.StudentId=t5.StudentId AND ")
                .Append("t3.ReqId = t4.ReqId AND ")
                .Append("t1.PrgVerId = t4.PrgVerId AND ")
                .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? AND ")
                .Append("t3.ReqTypeId = 2 AND ")
                .Append("t4.ReqId = t7.GrpId AND ")
                .Append("t7.ReqId = t8.GrpId AND ")
                .Append("t8.ReqId = t9.GrpId AND ")
                .Append("t9.ReqId = t10.GrpId AND ")
                .Append("t10.ReqId = t11.GrpId ")
                'Added to exclude the Continuing Ed program versions
                .Append("AND t6.IsContinuingEd = 0 ")
                If termId <> "00000000-0000-0000-0000-000000000000" Then
                    .Append(" and t6.ProgId = '" & termId & "'")
                End If

                .Append("UNION ")

                .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName,t1.StartDate, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t12.ReqId as ReqId, t3.Descrip As Req ")
                .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6, arReqGrpDef t7, arReqGrpDef t8, arReqGrpDef t9, arReqGrpDef t10, arReqGrpDef t11, arReqGrpDef t12 ")
                .Append("WHERE t1.StudentId=t5.StudentId AND ")
                .Append("t3.ReqId = t4.ReqId AND ")
                .Append("t1.PrgVerId = t4.PrgVerId AND ")
                .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? AND ")
                .Append("t3.ReqTypeId = 2 AND ")
                .Append("t4.ReqId = t7.GrpId AND ")
                .Append("t7.ReqId = t8.GrpId AND ")
                .Append("t8.ReqId = t9.GrpId AND ")
                .Append("t9.ReqId = t10.GrpId AND ")
                .Append("t10.ReqId = t11.GrpId AND ")
                .Append("t11.ReqId = t12.GrpId AND ")
                'Added to exclude the Continuing Ed program versions
                .Append("t6.IsContinuingEd = 0  ")
                If termId <> "00000000-0000-0000-0000-000000000000" Then
                    .Append(" and t6.ProgId = '" & termId & "'")
                End If

                '---------------------------------------------------------------------------------------------------

                '-- This section is the new union

                '---------------------------------------------------------------------------------------------------

                .Append("UNION ")


                .Append("SELECT distinct t1.StuEnrollId, t1.ShiftId,t5.LastName,t1.StartDate, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,  ")

                .Append("(SELECT ReqId FROM arClassSections WHERE ClsSectionId = ?) AS ReqId, ")

                .Append("(SELECT t1.Descrip FROM arReqs t1, arClassSections t2 WHERE t1.ReqId = t2.ReqId AND t2.ClsSectionId = ?) AS Req ")

                .Append("FROM arStuEnrollments t1, arStudent t5, arPrgVersions t6 ")

                .Append("WHERE t1.StudentId=t5.StudentId AND ")

                .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? AND ")

                .Append("t6.IsContinuingEd = 1   ")
                If termId <> "00000000-0000-0000-0000-000000000000" Then
                    .Append(" and t6.ProgId = '" & termId & "'")
                End If

                .Append(")R5, arClassSections R6 ")




                '------------------------------------------------------------------------------------------------------

                '--New union code ends here

                '------------------------------------------------------------------------------------------------------


                .Append("WHERE R5.ReqId = R6.ReqId and R5.StartDate < R6.EndDate ")

                .Append(" and R5.StuEnrollId not in ")
                .Append("  (Select Distinct(t7.StuEnrollId)from arResults t7,arClassSections t8 ")
                .Append("   where t8.ReqId in (Select ReqId from arClassSections where ClsSectionId = ?) ")
                .Append("  and t7.TestId = t8.ClsSectionId and t7.GrdSysDetailId is NULL) ")

                .Append("and R5.StatusCodeId IN (Select StatusCodeId from syStatusCodes where SysStatusId IN (7,9,13,20)) ")

                .Append("and ((R5.ShiftId = R6.ShiftId) or (R5.ShiftId is Null and R6.ShiftId is NULL)) ")

                '.Append("and R5.StuEnrollId not in (Select Distinct StuEnrollId from arTransferGrades t9,arGradeSystemDetails t10 ")
                '.Append(" where t9.GrdSysDetailID=t10.GrdSysDetailID and t10.IsPass=1 and t9.ReqId in (Select ReqId from arClassSections where ClsSectionId = ?)) ")

                .Append("and R6.clsSectionId= ? ")

                'check for Equivalent Courses
                .Append(" and R6.ClsSectionId not in ")
                .Append(" (select ClsSectionId  from arClassSections where Reqid in( ")
                .Append(" select c.EquivReqid from arResults a,arClassSections b,arCourseEquivalent  c,arGradeSystemDetails d    where  ")
                .Append(" a.TestId = b.ClsSectionId And a.StuEnrollId = R5.StuEnrollId  and a.GrdSysDetailId=d.GrdSysDetailId and d.IsPass=1 ")
                .Append(" and b.Reqid=c.Reqid and c.EquivReqid =R5.ReqId )) ")

                .Append("order by R5.ExpGradDate ")

            End With
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@campusid", campusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@campusid", campusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@campusid", campusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@campusid", campusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@campusid", campusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@campusid", campusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@campusid", campusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@ClsSectId8", clsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@ClsSectId8", clsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@campusid8", campusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@ClsSectId", clsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClsSectId3", clsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            ' db.AddParameter("@ClsSectId2", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "AvailStds")
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            With sb
                '   build the sql query
                .Append(" Select distinct ")

                .Append("t1.StuEnrollId, ")

                .Append("t1.GrdSysDetailId, ")

                .Append("t1.ModUser, ")

                .Append("t5.LastName, ")

                .Append("t5.FirstName, ")

                .Append("t6.ExpGradDate, ")

                .Append("t7.ReqId ")

                .Append("FROM arResults t1, arStudent t5, arStuEnrollments t6, arClassSections t7 ")

                .Append("where   t1.TestId = ? ")

                .Append("and     t1.StuEnrollId = t6.StuEnrollId ")

                .Append("and     t6.StudentId = t5.StudentId ")

                .Append("and     t1.TestId = t7.ClsSectionId ")
                'Added on 3/31/2005 by Troy. This is to accommodate dropping courses during the add/drop period.
                'During this period no grade is recorded when a course is dropped. Ideally the record should
                'be deleted from the arResults table. However, if we actually deleted the record it would no
                'longer appear in the instructor's gradebook. It is good to preserve the scores that were already
                'recorded in the grade book just in case the student decide to take up the course again.
                'The arResults table was modified to include a DroppedInAddDrop field. This defaults to false
                'but is set to true when a course is dropped during the add/drop period.
                .Append("and t1.DroppedInAddDrop = 0 ")
                'Added on 10/06/05 by Bhavana. This is to exclude students w/ drop grades.
                .Append("and t1.ResultId not in ")
                .Append("(select ResultId from arResults R, arGradeSystemDetails GSD where R.GrdSysDetailId=GSD.GrdSysDetailId and GSD.IsDrop=1 and TestId=?) ")
            End With


            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ClsSectId", clsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ClsSectId5", clsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "SelectedStuds")
            Dim iNum As Integer
            iNum = ds.Tables("SelectedStuds").Rows.Count
            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function GetStdClsSects(ByVal StuEnrollId As String, ByVal ClsSectId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            'With sb
            '    .Append("SELECT a.ClsSectionId, b.ClsSection ")
            '    .Append("FROM arRegisterStudents a, arClassSections b ")
            '    .Append("WHERE a.StudentId = ? and a.ClsSectionId = b.ClsSectionId ")
            'End With

            'With sb
            '    .Append("SELECT a.TestId, b.ClsSection ")
            '    .Append("FROM arResults a, arClassSections b ")
            '    .Append("WHERE a.StuEnrollId = ? and a.TestId = b.ClsSectionId ")
            'End With
            With sb
                .Append("SELECT a.TestId, b.ClsSection, b.StartDate,b.EndDate  ")
                .Append("FROM arResults a, arClassSections b ")
                .Append("WHERE a.StuEnrollId = ? and a.TestId = b.ClsSectionId  and ")
                .Append("b.TermId in (Select b.TermId from arClassSections b where b.ClsSectionId = ?) ")
                'Modified by Michelle R. Rodriguez on 01/23/2006
                'Exclude class sections that have overriden conflicts
                .Append("and not exists (select * from arOverridenConflicts where leftClsSectionId = ? or rightClsSectionId = ?) ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StuEnrollId", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClssectId", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClssectId2", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClssectId3", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "TermClsSects")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    ''Added by SAraswathi lakshmanan

    Public Function GetStdClsSects_SP(ByVal StuEnrollId As String, ByVal ClsSectId As String) As DataSet
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        '   connect to the database
        Dim db As New FAME.DataAccessLayer.SQLDataAccess
        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder


            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ClssectId", New Guid(ClsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter_SP("USP_AR_RegStd_GetStdClsSects")
            da.Fill(ds, "TermClsSects")



        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetStdClsSectsFrAllEnrollments(ByVal StuEnrollId As String, ByVal ClsSectId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            With sb
                .Append("SELECT a.TestId, b.ClsSection, b.ReqId,b.TermId,  ")
                .Append("(Select ReqId from arClassSections b where b.ClsSectionId = ? ) as CourseId, ")
                .Append(" (select Descrip from arReqs where ReqId=b.ReqId) as CourseDescrip ")
                .Append(" FROM arResults a, arClassSections b WHERE a.StuEnrollId in ")
                .Append("(Select StuEnrollId from arStuEnrollments where StudentId in ")
                .Append("(Select StudentId from arStuEnrollments where StuEnrollId = ?)) ")
                .Append("and a.TestId = b.ClsSectionId  and ")
                .Append("b.TermId in (Select b.TermId from arClassSections b where b.ClsSectionId = ?) ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ClssectId", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClssectId", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "TermClsSects")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function GetCoursePrereqs(ByVal ClsSectId As String, ByVal PrgverId As String, ByVal Campusid As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            'With sb
            '    .Append("SELECT a.ClsSectionId, b.PreCoReqId, c.Descrip ")
            '    .Append("FROM arClassSections a, arCourseReqs b,arReqs c where a.ReqId = b.ReqId and ")
            '    .Append("b.PreCoReqId = c.ReqId and b.CourseReqTypId = 1 ")
            '    .Append("and a.ClsSectionId = ? ")
            'End With


            'Commented out by bn 05/06/05
            'With sb
            '    .Append("Select c.ClsSectionId from arClassSections c where c.ReqId in ")
            '    .Append(" (Select b.PreCoReqId from arCourseReqs b where b.ReqId in ")
            '    .Append(" ( Select a.ReqId from arClassSections a where a.ClsSectionId = ?) and b.CourseReqTypId = 1) ")
            'End With

            'added by bn 05/06/05
            ''Modified by Saraswathi lakshmanan on August 5 2009
            ''For mantis 16753
            ''For preReqs related to program Version.
            ''The prereqs related to the chosen reqId and those mapped to the selected program Version is verified.

            With sb

                .Append(" Select arReqs.ReqID as PreCoReqId from arReqs,arProgVerDef where arReqs.reqid in (   Select PreCoreqId from arCourseReqs b where   ")
                .Append(" b.ReqId in (Select a.ReqId from arClassSections a where a.ClsSectionId = ?) ")
                .Append(" and (prgverId is Null or  prgverid=?) ")
                .Append(" and b.CourseReqTypId = 1 )")
                .Append(" and  arReqs.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' and arReqs.ReqTypeId = 1  ")
                .Append(" And (arReqs.ReqId=arProgVerDef.ReqId  or arReqs.ReqId in ")
                .Append(" (select Reqid from arReqGrpDef where GrpId in(select Reqid from arProgVerDef where    prgverid= ?))) ")
                .Append(" and arProgVerDef.PrgVerId = ?")
                .Append(" AND arReqs.CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId =  ?) ")


                '.Append("Select b.PreCoreqId from arCourseReqs b where  ")
                '.Append("b.ReqId in (Select a.ReqId from arClassSections a where a.ClsSectionId = ?) ")
                '.Append("and b.CourseReqTypId = 1 ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ClsSectId", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PrgverId", PrgverId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PrgverId", PrgverId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PrgverId", PrgverId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CampusID", Campusid, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "CoursePrereqs")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    ''Added by SAraswathi lakshmanan
    Public Function GetCoursePrereqs_Sp(ByVal ClsSectId As String, ByVal PrgverId As String, ByVal Campusid As String) As DataSet
        Dim ds As New DataSet
        Dim da As SqlClient.SqlDataAdapter
        Dim db As New FAME.DataAccessLayer.SQLDataAccess
        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder


            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ClsSectId", New Guid(ClsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@PrgverId", New Guid(PrgverId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@CampusID", New Guid(Campusid), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter_SP("USP_AR_GetCoursepreReqs")
            da.Fill(ds, "CoursePrereqs")

            'Close Connection


        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        Finally
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function GetPrereqClsSects(ByVal PreReqId As String, Optional ByVal stuEnrollId As String = "") As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder



            With sb
                .Append("Select b.PreCoReqId, c.ClsSectionId from arClassSections c, arCourseReqs b where c.ReqId = b.PreCoReqId ")
                .Append("and b.CourseReqTypId = 1 and b.PreCoReqId = ? ")
                If stuEnrollId <> "" Then
                    .Append("and c.StartDate >= (select ExpStartDate from arStuEnrollments where StuEnrollId=?) ")
                    .Append("and c.ShiftId=(select ShiftId from arStuEnrollments where StuEnrollId=?) ")
                    .Append("and c.CampusId=(select CampusId from arStuEnrollments where StuEnrollId=?) ")
                    .Append("and c.StartDate <= GetDate() ")

                End If
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@Prereq", PreReqId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If stuEnrollId <> "" Then
                db.AddParameter("@sid1", stuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@sid2", stuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@sid3", stuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "PrereqClsSects")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    ''Added by Saraswathi Lakshmanan
    Public Function GetPrereqClsSects_Sp(ByVal PreReqId As String, Optional ByVal stuEnrollId As String = "") As DataSet
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        '   connect to the database
        Dim db As New FAME.DataAccessLayer.SQLDataAccess
        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            db.AddParameter("@PreReqID", New Guid(PreReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            If stuEnrollId <> "" Then
                db.AddParameter("@StuEnrollID", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            End If
            '   Execute the query
            db.OpenConnection()
            If stuEnrollId <> "" Then
                da = db.RunParamSQLDataAdapter_SP("USP_AR_GetPrereqClsSectswithStuEnrollid")
            Else
                da = db.RunParamSQLDataAdapter_SP("USP_AR_GetPrereqClsSectsWOutStuEnrollid")
            End If

            da.Fill(ds, "PrereqClsSects")



        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function AddStdToClsSect(ByVal storedGuid As String, ByVal StuEnrollId As String, ByVal user As String, ByVal CampusId As String)
        Dim db As New MyPortalDataAccess

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

            'First do a search for the "default grade" in the gradesystem details tbl to
            'get the guid for this particular default grade. Later insert this guid w/
            'the student record in arResults.

            'Insert row into syCampGrps table
            Dim sb As New System.Text.StringBuilder
            With sb
                .Append("INSERT INTO arResults(TestId,StuEnrollId,ModUser,ModDate) ")
                .Append("VALUES(?,?,?,?)")
            End With

            db.AddParameter("@campgrpid", storedGuid, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@campusid", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ''ModUser
            db.AddParameter("@ModUser", user, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", Date.Now, MyPortalDataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   apply fees
            Dim result As String = (New TransactionsDB).ApplyFeesByCourse(StuEnrollId, storedGuid, user, CampusId, Date.Today)

            '   commit transaction
            groupTrans.Commit()

            '   register Gradebook Components

            'The grade book components for the course need to be registered only for Ross
            'for other school record will be inserted in to this table when instructor 
            'posts grade
            'If SingletonAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "true" Or _
            'SingletonAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "yes" Then
            result = RegisterStudentForGradeBookComponents(StuEnrollId, storedGuid, user, Date.Now)
            'End If


            '   return with no errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            Else
                '   report an error to the client
                DALExceptions.BuildErrorMessage(ex)
            End If

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Function DeleteStdFrmClsSect(ByVal clsSectionId As String, ByVal StuEnrollId As String, ByVal user As String)

        Dim db As New MyPortalDataAccess

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   delete fees
            'Dim result As String = (New TransactionsDB).DeleteAppliedFeesByCourse(StuEnrollId, storedGuid, user)

            'If Not result = "" Then
            '    Return result
            'End If

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

            Dim sb As New System.Text.StringBuilder
            With sb
                .Append("Delete from arGrdBkResults ")
                .Append("WHERE ClsSectionId = ? ")
                .Append("AND StuEnrollId = ? ; ")
                .Append("DELETE FROM arResults ")
                .Append("WHERE TestId = ? ")
                .Append("AND StuEnrollId = ? ;")
                .Append("delete from dbo.atClsSectAttendance where ")
                .Append(" ClsSectionId= ? and stuEnrollId= ? ;")
            End With

            db.AddParameter("@ClsSectionId", clsSectionId, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@campusid", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ClsSectionId", clsSectionId, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@campusid", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@TestId", clsSectionId, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@stuEnrollId", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   commit transaction 
            groupTrans.Commit()

            ''   delete Gradebook Components
            'Dim result As String = DeleteGradeBookComponentsForAClassSection(StuEnrollId, clsSectionId, user)

            '   return with no errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            Else
                '   report an error to the client
                DALExceptions.BuildErrorMessage(ex)
            End If

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetStdGrade(ByVal StuEnrollId As String, ByVal ClsSectId As String, Optional ByVal CoReqId As String = "") As String
        Dim da As OleDbDataAdapter
        Dim sGrade As String = String.Empty

        Try
            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            'Changed this function to include sub-query for StudentId & StuEnrollId to accomodate
            'situation when the Student did the course (and received a grade) in another enrollment.
            '#BN 05/06/05
            If CoReqId <> "" Then
                With sb
                    .Append("Select X.ABC ")
                    .Append("from ")
                    .Append("( ")
                    .Append("SELECT Coalesce ((Select top 1 b.Grade FROM arResults a, arGradeSystemDetails b ")
                    .Append("WHERE a.StuEnrollId = ?   ")
                    .Append("and a.TestId in  ")
                    .Append(" (Select c.ClsSectionId from arClassSections c where c.ReqId =  ?) and ")
                    .Append("b.GrdSysDetailId = a.GrdSysDetailId   order by a.moddate desc ),'')  as ABC ")
                    .Append("UNION ALL ")
                    .Append("Select Coalesce ((SELECT Top 1 b.Grade FROM arTransferGrades a, arGradeSystemDetails b,arTerm c ")
                    .Append("WHERE a.StuEnrollId = ? ")
                    .Append("and ")
                    .Append("a.ReqId = ? and ")
                    ''order by Changed by Saraswarthi Lakshmanan on July 15 2009
                    .Append(" b.GrdSysDetailId = a.GrdSysDetailId  and a.TermId=c.TermId order by c.StartDate desc,a.moddate desc),'') as ABC ")
                    .Append(" ) X where X.ABC <> '' ")
                    '.Append("Select X.ABC ")
                    '.Append("from ")
                    '.Append("( ")
                    '.Append("SELECT Coalesce ((Select distinct b.Grade FROM arResults a, arGradeSystemDetails b ")
                    '.Append("WHERE a.StuEnrollId = ?   ")
                    '.Append("and a.TestId in  ")
                    '.Append(" (Select c.ClsSectionId from arClassSections c where c.ReqId =  ?) and ")
                    '.Append("b.GrdSysDetailId = a.GrdSysDetailId),'')  as ABC ")
                    '.Append("UNION ALL ")

                    '.Append("Select Coalesce ((SELECT distinct b.Grade FROM arTransferGrades a, arGradeSystemDetails b ")
                    '.Append("WHERE a.StuEnrollId = ? ")
                    '.Append("and ")
                    '.Append("a.ReqId = ? and ")
                    '.Append(" b.GrdSysDetailId = a.GrdSysDetailId),'') as ABC ")
                    '.Append(" ) X where X.ABC <> '' ")
                End With
                ' Add the student enrollid and ClsSectId to the parameter list
                db.AddParameter("@StdId", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@req", CoReqId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StdId3", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@req2", CoReqId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                With sb
                    .Append("SELECT distinct b.Grade ")
                    .Append("FROM arResults a, arGradeSystemDetails b, arClassSections c ")
                    .Append(" WHERE a.StuEnrollId in  ")
                    .Append("(Select StuEnrollId from arStuEnrollments where StudentId in ") 'added 05/06/05
                    .Append(" (Select StudentId from arStuEnrollments where StuEnrollId = ?)) ")
                    '.Append("and a.TestId = ? ")
                    .Append(" and a.TestId = c.ClsSectionId ")
                    .Append(" and c.ReqId = (Select ReqId from arClassSections where ClsSectionId = ?) ")
                    .Append("and b.GrdSysDetailId = a.GrdSysDetailId")
                    .Append("UNION ALL ")
                    .Append("Select distinct b.Grade ")
                    .Append("FROM arTransferGrades a, arGradeSystemDetails b ")
                    .Append("WHERE a.StuEnrollId in  ")
                    .Append("(Select StuEnrollId from arStuEnrollments where StudentId in  ")
                    .Append("(Select StudentId from arStuEnrollments where StuEnrollId = ?)) ")
                    .Append("and a.ReqId in (Select ReqId from arClassSections where ClsSectionId = ?) ")
                    .Append("and b.GrdSysDetailId = a.GrdSysDetailId ")
                End With

                ' Add the student enrollid and ClsSectId to the parameter list
                db.AddParameter("@StdId", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@testId", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StdId2", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@testId2", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            '   build the sql query

            '   Execute the query
            db.OpenConnection()

            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            Dim rowcount As Integer = 0
            While dr.Read()
                If CoReqId <> "" Then
                    ''Modified by Saraswathi lakshmanan on july 14 2009
                    ''All the grades are added
                    rowcount = rowcount + 1
                    If rowcount > 1 Then
                        sGrade = sGrade + ";" + dr("ABC")
                    Else
                        sGrade = dr("ABC")
                    End If

                Else
                    sGrade = dr("Grade")
                End If
            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return sGrade

    End Function
    ''Added by SAraswathi lakshmanan

    Public Function GetStdGrade_Sp(ByVal StuEnrollId As String, ByVal ClsSectId As String, Optional ByVal CoReqId As String = "") As String
        Dim da As SqlDataAdapter
        Dim sGrade As String = String.Empty
        '   connect to the database
        Dim db As New FAME.DataAccessLayer.SQLDataAccess
        Try
            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings = MyPortalAdvAppSettings.GetAppSettings()

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            'Changed this function to include sub-query for StudentId & StuEnrollId to accomodate
            'situation when the Student did the course (and received a grade) in another enrollment.
            '#BN 05/06/05
            If CoReqId <> "" Then

                ' Add the student enrollid and ClsSectId to the parameter list
                db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@CoreqID", New Guid(CoReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            Else
                ' Add the student enrollid and ClsSectId to the parameter list
                db.AddParameter("@StuEnrollid", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@ClsSectID", New Guid(ClsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            End If
            '   build the sql query

            '   Execute the query
            '   db.OpenConnection()
            Dim dr As SqlDataReader
            If CoReqId <> "" Then
                dr = db.RunParamSQLDataReader_SP("USP_AR_RegStd_GetStudentGradewithCoreqID")
            Else
                dr = db.RunParamSQLDataReader_SP("USP_AR_RegStd_GetStudentGrade")
            End If
            'Execute the query


            Dim rowcount As Integer = 0
            While dr.Read()
                If CoReqId <> "" Then
                    ''Modified by Saraswathi lakshmanan on july 14 2009
                    ''All the grades are added
                    rowcount = rowcount + 1
                    If rowcount > 1 Then
                        sGrade = sGrade + ";" + dr("ABC")
                    Else
                        sGrade = dr("ABC")
                    End If

                Else
                    sGrade = dr("Grade")
                End If
            End While



        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return sGrade

    End Function


    Public Function GetStudentGrade(ByVal StuEnrollId As String, ByVal CoReqId As String) As String
        Dim da As OleDbDataAdapter
        Dim sGrade As String = String.Empty

        Try
            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            'Changed this function to include sub-query for StudentId & StuEnrollId to accomodate
            'situation when the Student did the course (and received a grade) in another enrollment.
            '#BN 05/06/05

            With sb
                .Append("Select X.ABC ")
                .Append("from ")
                .Append("( ")
                .Append("SELECT Coalesce ((Select Top 1 b.Grade FROM arResults a, arGradeSystemDetails b ")
                .Append("WHERE a.StuEnrollId = ?   ")
                .Append("and a.TestId in  ")
                .Append(" (Select c.ClsSectionId from arClassSections c where c.ReqId =  ?) and ")
                .Append("b.GrdSysDetailId = a.GrdSysDetailId  order by a.moddate desc ),'')  as ABC ")
                .Append("UNION ALL ")


                .Append("Select Coalesce ((SELECT Top 1 b.Grade FROM arTransferGrades a, arGradeSystemDetails b,arTerm c   ")
                .Append("WHERE a.StuEnrollId = ? ")
                .Append("and ")
                .Append("a.ReqId = ? and ")
                ''order by Changed by Saraswarthi Lakshmanan on July 15 2009
                .Append(" b.GrdSysDetailId = a.GrdSysDetailId   and a.TermId=c.TermId order by c.StartDate desc,a.ModDate desc),'') as ABC ")
                .Append(" ) X where X.ABC <> '' ")
            End With
            ' Add the student enrollid and ClsSectId to the parameter list
            db.AddParameter("@StdId", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@req", CoReqId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StdId3", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@req2", CoReqId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            '   build the sql query

            '   Execute the query
            db.OpenConnection()

            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()

                sGrade = dr("ABC")

            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return sGrade

    End Function
    Public Function DoesPrgVerHaveGrdOverride(ByVal PrgVerId As String, ByVal ReqId As String) As String
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim count As String
        Dim sGrdSysDetailId As String

        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("Select count(*) as Count, GrdSysDetailId from arProgVerDef where ReqId = ? and PrgVerId = ? and GrdSysDetailId is not null ")
                .Append("GROUP BY GrdSysDetailId ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ReqId", ReqId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PrgVerId", PrgVerId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            db.OpenConnection()
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                sGrdSysDetailId = dr("GrdSysDetailId").ToString
                count = dr("Count").ToString
            End While

            'ds = db.RunParamSQLDataSet(sb.ToString)
            'sb.Remove(0, sb.Length)

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return sGrdSysDetailId & "," & count

    End Function

    ''Added by SAraswathi lakshmanan
    Public Function DoesPrgVerHaveGrdOverride_SP(ByVal PrgVerId As String, ByVal ReqId As String) As String
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        Dim count As String
        Dim sGrdSysDetailId As String
        Dim db As New FAME.DataAccessLayer.SQLDataAccess
        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings = MyPortalAdvAppSettings.GetAppSettings()

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder


            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@PrgVerId", New Guid(PrgVerId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ReqId", New Guid(ReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)


            ' db.OpenConnection()
            'Execute the query
            Dim dr As SqlDataReader = db.RunParamSQLDataReader_SP("USP_AR_RegStd_DoesPrgVerHaveGrdOverride")

            While dr.Read()
                sGrdSysDetailId = dr("GrdSysDetailId").ToString
                count = dr("Count").ToString
            End While

            'Close Connection
            '   db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        Finally
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return sGrdSysDetailId & "," & count

    End Function

    Public Function GetHigherGrades(ByVal GrdSysDetailId As String) As String
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim count As String
        Dim sGrdSysDetailId As String
        Dim sGrade As String
        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("select Grade from arGradeSystemDetails where Vieworder <= ")
                .Append("((Select b.ViewOrder from arGradeSystemDetails b where b.GrdSystemId in ")
                .Append("(Select Distinct a.GrdSystemId from arGradeSystemDetails a where a.GrdSysDetailId = ?) ")
                .Append("and b.GrdSysDetailId = ?)) ")
                .Append("and GrdSystemId in  ")
                .Append("(Select Distinct a.GrdSystemId from arGradeSystemDetails a where a.GrdSysDetailId = ?) ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@GrdSysDetailId", GrdSysDetailId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@GrdSysDetailId", GrdSysDetailId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@GrdSysDetailId", GrdSysDetailId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)


            While dr.Read()
                sGrade = sGrade & dr("Grade") & ","
            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return sGrade

    End Function

    ''Added by SAraswathi lakshmanan

    Public Function GetHigherGrades_Sp(ByVal GrdSysDetailId As String) As String
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        Dim count As String
        Dim sGrdSysDetailId As String
        Dim sGrade As String
        Dim db As New FAME.DataAccessLayer.SQLDataAccess
        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder



            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@GrdSysDetailId", New Guid(GrdSysDetailId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            '   db.OpenConnection()
            'Execute the query
            Dim dr As SqlDataReader = db.RunParamSQLDataReader_SP("USP_AR_RegStd_GetHigherGrades")


            While dr.Read()
                sGrade = sGrade & dr("Grade") & ","
            End While


            'Close Connection


        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        Finally
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return sGrade

    End Function

    Public Function IsPassingGrade(ByVal Grade As String, ByVal PrgVerId As String) As Integer
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim count As String
        Dim sGrdSysDetailId As String
        Dim IsPass As Integer
        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                ''Added by Saraswathi lakshmanan on July 15 2009
                ''All the grades are passed and if any one of the grade is ispass then, it is considered to be ispass
                ''fixed for mantis case 16830
                If Grade.Contains(";") Then
                    Dim Grades() As String = Grade.Split(";")
                    Dim leng As Integer = Grades.Length
                    Dim strGrade As String = ""
                    Dim i As Integer
                    If leng > 0 Then

                        For i = 0 To leng - 1
                            strGrade = strGrade + "'" + Grades(i) + "',"
                        Next
                        strGrade = strGrade.Substring(0, strGrade.Length - 1)
                    End If

                    .Append("select IsPass from arGradeSystemDetails where Grade in (" + strGrade + ") and ")
                    .Append("GrdSystemID in (select GrdSystemId from arPrgVersions where PrgVerId=?) order by ispass desc ")

                    db.AddParameter("@PrgVerId", PrgVerId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    .Append("select IsPass from arGradeSystemDetails where Grade=? and ")
                    .Append("GrdSystemID in (select GrdSystemId from arPrgVersions where PrgVerId=?) ")
                    db.AddParameter("@Grade", Grade, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@PrgVerId", PrgVerId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If


            End With

            ' Add the PrgVerId and ChildId to the parameter list


            db.OpenConnection()
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)


            While dr.Read()
                If (dr("IsPass") = True) Then
                    IsPass = 1
                    Exit While
                Else
                    IsPass = 0
                End If

            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return IsPass

    End Function
    ''Added by Saraswathi lakshmanan
    Public Function IsPassingGrade_SP(ByVal Grade As String, ByVal PrgVerId As String) As Integer
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        Dim count As String
        Dim sGrdSysDetailId As String
        Dim IsPass As Integer
        '   connect to the database
        Dim db As New FAME.DataAccessLayer.SQLDataAccess
        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder
            Dim strGrade As String = ""
            ''Added by Saraswathi lakshmanan on July 15 2009
            ''All the grades are passed and if any one of the grade is ispass then, it is considered to be ispass
            ''fixed for mantis case 16830
            If Grade.Contains(";") Then
                Dim Grades() As String = Grade.Split(";")
                Dim leng As Integer = Grades.Length

                Dim i As Integer
                If leng > 0 Then
                    For i = 0 To leng - 1
                        strGrade = strGrade + "'" + Grades(i) + "',"
                    Next
                    strGrade = strGrade.Substring(0, strGrade.Length - 1)
                End If
            Else
                strGrade = "'" + Grade + "'"
            End If
            db.AddParameter("@Grade", Grade, SqlDbType.VarChar, , ParameterDirection.Input)
            db.AddParameter("@PrgVerId", New Guid(PrgVerId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            '  db.OpenConnection()
            'Execute the query
            Dim dr As SqlDataReader = db.RunParamSQLDataReader_SP("USP_AR_RegStd_IsPassingGrade")


            While dr.Read()
                If (dr("IsPass") = True) Then
                    IsPass = 1
                    Exit While
                Else
                    IsPass = 0
                End If

            End While




        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return IsPass

    End Function


    Public Function DoesReqHaveCoReqs(ByVal ReqId As String) As Integer
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim sGrdSysDetailId As String
        Dim rowCount As Integer
        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("Select count(*) as Count from arCourseReqs a where a.ReqId = ? and CourseReqTypId = 2 ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@PrgVerId", ReqId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try

        'Return the datatable in the dataset
        Return rowCount

    End Function
    ''Added by Saraswathi lakshmanan 

    Public Function DoesReqHaveCoReqs_Sp(ByVal reqId As String) As Integer
        Dim db As New MyPortalDataAccess
        Dim ds As New DataSet
        Dim coreqs As Integer = 0

        Try
            db.AddParameter("@ReqId", reqId, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet("dbo.USP_AR_DoesReqhaveCoReqs", Nothing, "SP")
            coreqs = ds.Tables(0).Rows(0)(0).ToString()
        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New BaseException(ex.Message)
            Else
                Throw New BaseException(ex.InnerException.ToString)
            End If
        Finally
            db.Dispose()
        End Try

        Return coreqs

    End Function
    Public Function GetCourseCoreqs(ByVal ReqId As String, ByVal TermId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("Select a.PreCoReqId, b.descrip from arCourseReqs a, arReqs b ")
                .Append("where a.CourseReqTypId = 2  and a.ReqId = ? and a.PreCoReqId = b.ReqId ")
            End With

            'With sb
            '    .Append("Select c.ClsSectionId from arClassSections c where c.ReqId in ")
            '    .Append(" (Select b.PreCoReqId from arCourseReqs b where b.ReqId in ")
            '    .Append(" ( Select a.ReqId from arClassSections a where a.ClsSectionId = ?) and b.CourseReqTypId = 2) ")
            'End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ReqId", ReqId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "CourseCoreqs")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetCourseCoreqs_Sp(ByVal reqId As String, ByVal termId As String) As DataSet
        Dim db As New MyPortalDataAccess
        Dim ds As New DataSet

        Try
            db.AddParameter("@ReqId", reqId, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet("dbo.USP_AR_GetCourseCoReqs", Nothing, "SP")
        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        Finally
            db.Dispose()
        End Try

        Return ds

    End Function

    ''Added by SAraswathi lakshmanan
    Public Function GetCoreqClsSects_SP(ByVal ReqId As String, ByVal TermId As String) As DataSet
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        Dim db As New FAME.DataAccessLayer.SQLDataAccess
        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ReqId", New Guid(ReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@TermId", New Guid(TermId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter_SP("USP_AR_GetCoreqClsSects")
            da.Fill(ds, "CoreqClsSects")

            'Close Connection


        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        Finally
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function IsCourseComplete(ByVal stuEnrollId As String, ByVal clsSectId As String, Optional ByVal coReqId As String = "") As Boolean

        Dim rowCount As Integer

        Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New MyPortalAdvAppSettings
        End If

        '   connect to the database
        Dim db As New MyPortalDataAccess

        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        If (coReqId <> "") Then
            With sb
                .Append("Select Sum(Num) ")
                .Append("from ")
                .Append("(Select Coalesce((Select count(*) as Count from arResults a where a.StuEnrollId in ")
                .Append("(Select StuEnrollId from arStuEnrollments where StudentId in ")
                .Append("(Select StudentId from arStuEnrollments where StuEnrollId = ?)) ")
                .Append("and a.TestId in (Select c.ClsSectionId from arClassSections c where c.ReqId = ?)),0) as Num ")

                .Append("union all ")

                .Append("Select Coalesce((Select count(*) as Count from arTransferGrades t9 where ")
                .Append("t9.StuEnrollId = ? and ")
                .Append("t9.ReqId = ?),0) as Num ")
                .Append(" )R1 ")
            End With

            ' Add the StdId and ClsSectId to the parameter list
            db.AddParameter("@Std", stuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CoReqId", coReqId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Std2", stuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CoReqId2", coReqId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Else
            With sb
                .Append("SELECT Sum(Num) from " & vbCrLf)
                .Append("( " & vbCrLf)
                .Append("Select Coalesce((Select count(*) as Count from arResults a, arClassSections b where a.StuEnrollId in " & vbCrLf)
                .Append("(Select StuEnrollId from arStuEnrollments where StudentId in " & vbCrLf)
                .Append("(Select StudentId from arStuEnrollments where StuEnrollId = ?)) " & vbCrLf)
                .Append("and a.TestId=b.ClsSectionId " & vbCrLf)
                .Append("and a.IsCourseCompleted = 1 " & vbCrLf)
                .Append("and b.ReqId = (Select ReqId from arClassSections where ClsSectionId = ?)),0 )as Num " & vbCrLf)

                .Append("union all " & vbCrLf)

                .Append("Select Coalesce((Select count(*) as Count from arTransferGrades t9 where " & vbCrLf)
                .Append("t9.StuEnrollId = ? and " & vbCrLf)
                .Append("t9.IsCourseCompleted = 1 and " & vbCrLf)
                .Append("t9.ReqId in (Select ReqId from arClassSections where ClsSectionId = ?)),0)as Num " & vbCrLf)
                .Append(")R1 " & vbCrLf)
            End With


            ' Add the StdId and ClsSectId to the parameter list

            db.AddParameter("@Std", stuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClsSectId", clsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Std3", stuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClsSectId2", clsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        db.OpenConnection()
        Try

            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)

        Finally
            If Not (db Is Nothing) Then
                db.CloseConnection()
            End If

        End Try

        If rowCount > 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function IsCourseDropped(ByVal stuEnrollId As String, ByVal clsSectId As String) As Boolean
        Dim rowCount As Integer
        Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New MyPortalAdvAppSettings
        End If

        '   connect to the database
        Dim db As New MyPortalDataAccess

        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT Sum(Num) from ")
            .Append("( ")
            .Append("Select Coalesce((Select count(*) as Count from arResults a, arClassSections b where a.StuEnrollId in ")
            .Append("(Select StuEnrollId from arStuEnrollments where StudentId in ")
            .Append("(Select StudentId from arStuEnrollments where StuEnrollId = ? )) ")
            .Append("and a.TestId=b.ClsSectionId  ")
            .Append("and a.DroppedInAddDrop = 1  ")
            .Append("and b.ReqId = (Select ReqId from arClassSections where ClsSectionId = ?)),0 )as Num ")

            '.Append("union all " )

            '.Append("Select Coalesce((Select count(*) as Count from arTransferGrades t9 where ")
            '.Append("t9.StuEnrollId = ? and ")
            '.Append("t9.IsCourseCompleted = 1 and " )
            '.Append("t9.ReqId in (Select ReqId from arClassSections where ClsSectionId = ?)),0)as Num " )
            .Append(")R1 ")
        End With


        ' Add the StdId and ClsSectId to the parameter list

        db.AddParameter("@Std", stuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ClsSectId", clsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@Std3", stuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@ClsSectId2", clsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.OpenConnection()
        Try

            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)

        Finally
            If Not (db Is Nothing) Then
                db.CloseConnection()
            End If

        End Try

        Return (rowCount > 0)
        'If rowCount > 0 Then
        '    Return True
        'Else
        '    Return False
        'End If

    End Function

    Public Function HasStdAttemptedReq(ByVal StuEnrollId As String, ByVal ClsSectId As String, Optional ByVal CoReqId As String = "") As Integer
        Dim rowCount As Integer

        Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New MyPortalAdvAppSettings
        End If

        '   connect to the database
        Dim db As New MyPortalDataAccess
        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        If (CoReqId <> "") Then
            With sb
                .Append("Select Sum(Num) ")
                .Append("from ")
                .Append("(Select Coalesce((Select count(*) as Count from arResults a where a.StuEnrollId in ")
                .Append("(Select StuEnrollId from arStuEnrollments where StudentId in ")
                .Append("(Select StudentId from arStuEnrollments where StuEnrollId = ?)) ")
                .Append("and a.TestId in (Select c.ClsSectionId from arClassSections c where c.ReqId = ?)),0) as Num ")

                .Append("union all ")

                .Append("Select Coalesce((Select count(*) as Count from arTransferGrades t9 where ")
                .Append("t9.StuEnrollId = ? and ")
                .Append("t9.ReqId = ?),0) as Num ")
                .Append(" )R1 ")
            End With

            ' Add the StdId and ClsSectId to the parameter list
            db.AddParameter("@Std", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CoReqId", CoReqId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Std2", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CoReqId2", CoReqId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            With sb
                .Append("SELECT Sum(Num) from " & vbCrLf)
                .Append("( " & vbCrLf)
                .Append("Select Coalesce((Select count(*) as Count from arResults a, arClassSections b where a.StuEnrollId in " & vbCrLf)
                .Append("(Select StuEnrollId from arStuEnrollments where StudentId in " & vbCrLf)
                .Append("(Select StudentId from arStuEnrollments where StuEnrollId = ?)) " & vbCrLf)
                .Append("and a.TestId=b.ClsSectionId " & vbCrLf)
                .Append("and b.ReqId = (Select ReqId from arClassSections where ClsSectionId = ?)),0 )as Num " & vbCrLf)

                .Append("union all " & vbCrLf)

                .Append("Select Coalesce((Select count(*) as Count from arTransferGrades t9 where " & vbCrLf)
                .Append("t9.StuEnrollId = ? and " & vbCrLf)
                .Append("t9.ReqId in (Select ReqId from arClassSections where ClsSectionId = ?)),0)as Num " & vbCrLf)
                .Append(")R1 " & vbCrLf)
            End With

            ' Add the StdId and ClsSectId to the parameter list
            db.AddParameter("@Std", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClsSectId", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Std3", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClsSectId2", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        db.OpenConnection()
        Try
            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)
        Finally
            db.CloseConnection() 'Close Connection
        End Try
        'Return the datatable in the dataset

        Return rowCount
    End Function

    Public Function GetAttemptedTermStartDate(ByVal stuEnrollId As String, ByVal clsSectId As String, Optional ByVal coReqId As String = "") As DateTime
        Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New MyPortalAdvAppSettings
        End If

        '   connect to the database
        Dim db As New FAME.DataAccessLayer.SQLDataAccess
        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@StuEnrollID", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@ClsSectionID", New Guid(clsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Dim dt As DataTable
        dt = db.RunParamSQLDataSet_SP("dbo.USP_AttemptedClass_TermStartDate", "SP").Tables(0)

        Dim dtTermStartDate As DateTime = Nothing
        For Each dr As DataRow In dt.Rows
            If (DateTime.TryParse(dr("TermStartDate"), dtTermStartDate)) = False Then
                Exit For
            End If
        Next

        Return dtTermStartDate
    End Function

    Public Function GetAttemptedClassStartDate(ByVal stuEnrollId As String, ByVal clsSectId As String, Optional ByVal coReqId As String = "") As DateTime
        Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New MyPortalAdvAppSettings
        End If

        '   connect to the database
        Dim db As New FAME.DataAccessLayer.SQLDataAccess
        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@StuEnrollID", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@ClsSectionID", New Guid(clsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Dim dt As DataTable
        dt = db.RunParamSQLDataSet_SP("dbo.USP_AttemptedClass_ClassStartDate", "SP").Tables(0)

        Dim dtTermStartDate As DateTime = Nothing

        For Each dr As DataRow In dt.Rows
            If (DateTime.TryParse(dr("ClassStartDate"), dtTermStartDate)) = False Then
                Exit For
            End If
        Next

        Return dtTermStartDate
    End Function

    ''Added by Saraswathi lakshmanan
    Public Function HasStdAttemptedReq_Sp(ByVal StuEnrollId As String, ByVal ClsSectId As String, Optional ByVal CoReqId As String = "") As Integer


        Dim rowCount As Integer
        'Dim sGrdSysDetailId As String
        '   connect to the database
        Dim db As New FAME.DataAccessLayer.SQLDataAccess
        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            'Dim sb As New System.Text.StringBuilder
            If (CoReqId <> "") Then
                ' Add the StdId and ClsSectId to the parameter list
                db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@CoReqId", New Guid(CoReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Else
                db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@ClsSectionID", New Guid(ClsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            End If

            '    db.OpenConnection()

            If (CoReqId <> "") Then
                rowCount = db.RunParamSQLScalar_SP("USP_AR_HasStudentAttemptedReqswithCoReqs")
            Else
                rowCount = db.RunParamSQLScalar_SP("USP_AR_HasStudentAttemptedReqswithoutCoReqs")
            End If




        Catch ex As System.Exception

            Throw New BaseException(ex.InnerException.ToString)
        Finally
            'Close Connection
            db.CloseConnection()

        End Try

        'Return the datatable in the dataset

        Return rowCount
    End Function

    Public Function IsStdRegFrClsSect(ByVal StuEnrollId As String, ByVal ClsSectId As String) As Integer

        Dim ds As New DataSet

        Dim da As OleDbDataAdapter

        Dim RowCount As Integer

        Dim sGrdSysDetailId As String



        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database

            Dim db As New MyPortalDataAccess

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

            Dim sb As New System.Text.StringBuilder



            '   build the sql query

            'With sb

            '    .Append("Select count(*) as Count from arRegisterStudents a where a.StudentId = ? and a.ClsSectionId = ? ")

            'End With

            With sb

                .Append("Select count(*) as Count from arResults a where a.StuEnrollId = ? and a.TestId = ? ")

            End With


            ' Add the PrgVerId and ChildId to the parameter list

            db.AddParameter("@Grade", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@PrgVerId", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)



            db.OpenConnection()

            'Execute the query

            '   execute the query
            RowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection

            db.CloseConnection()


        Catch ex As System.Exception

            Throw New BaseException(ex.InnerException.ToString)

        End Try

        'Return the datatable in the dataset

        Return RowCount
    End Function

    ''Added By Saraswathi

    Public Function IsStdRegFrClsSect_SP(ByVal StuEnrollId As String, ByVal ClsSectId As String) As Integer

        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        Dim RowCount As Integer
        Dim db As New FAME.DataAccessLayer.SQLDataAccess
        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder
            db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ClsSectID", New Guid(ClsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            '   db.OpenConnection()
            RowCount = db.RunParamSQLScalar_SP("USP_AR_IsStdRegFrClsSect")

        Catch ex As System.Exception
            Throw New BaseException(ex.InnerException.ToString)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
        'Return the datatable in the dataset
        Return RowCount
    End Function
    Public Function DoesStdHaveFinalGrade(ByVal StuEnrollId As String, ByVal ClsSectId As String, Optional ByVal CampusId As String = "") As Integer


        Dim ds As New DataSet

        Dim da As OleDbDataAdapter

        Dim RowCount As Integer

        Dim sGrdSysDetailId As String



        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database

            Dim db As New MyPortalDataAccess

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

            Dim sb As New System.Text.StringBuilder

            With sb
                If MyPortalAdvAppSettings.AppSettings("GradesFormat", CampusId).ToLower = "numeric" Then
                    .Append("Select count(*) as Count from arResults a where a.StuEnrollId = ? and a.TestId = ? and  a.Score is not null and a.Score <> 0 ")
                Else
                    .Append("Select count(*) as Count from arResults a where a.StuEnrollId = ? and a.TestId = ? and a.GrdSysDetailId is not null ")
                End If


            End With


            ' Add the stdenrollid and testid to the parameter list

            db.AddParameter("@Grade", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@Testid", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query

            '   execute the query
            RowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection

            db.CloseConnection()


        Catch ex As System.Exception

            Throw New BaseException(ex.InnerException.ToString)

        End Try


        'Return the datatable in the dataset
        Return RowCount

    End Function

    Public Function DoesStdHaveOverride(ByVal StuEnrollId As String, ByVal PrereqId As String) As Integer


        Dim ds As New DataSet

        Dim da As OleDbDataAdapter

        Dim RowCount As Integer

        Dim sGrdSysDetailId As String



        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database

            Dim db As New MyPortalDataAccess

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

            Dim sb As New System.Text.StringBuilder

            With sb

                .Append("Select count(*) as Count from arOverridenPreReqs a where a.StuEnrollId = ?  ")
                .Append("and a.ReqId in (Select ReqId from arClassSections where ClsSectionId = ?) ")

            End With


            ' Add the stdenrollid and testid to the parameter list

            db.AddParameter("@Grade", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@Testid", PrereqId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query

            '   execute the query
            RowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection

            db.CloseConnection()


        Catch ex As System.Exception

            Throw New BaseException(ex.InnerException.ToString)

        End Try


        'Return the datatable in the dataset
        Return RowCount

    End Function


    ''Added by Saraswathi Lakshmanan
    Public Function DoesStdHaveOverride_Sp(ByVal StuEnrollId As String, ByVal PrereqId As String) As Integer

        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        Dim RowCount As Integer
        Dim db As New FAME.DataAccessLayer.SQLDataAccess
        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

            db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@PreReqID", New Guid(PrereqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            '  db.OpenConnection()
            RowCount = db.RunParamSQLScalar_SP("USP_AR_DoesStudentHaveOverride")
            'Close Connection
            '  db.CloseConnection()
        Catch ex As System.Exception
            Throw New BaseException(ex.InnerException.ToString)
        Finally
            db.CloseConnection()
        End Try
        'Return the datatable in the dataset
        Return RowCount

    End Function
    Public Function GetClsSectDates(ByVal ClsSectId As String) As String
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim sdate As String
        Dim edate As String

        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("Select StartDate, EndDate from arClassSections where ClsSectionId = ? ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ReqId", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            db.OpenConnection()
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                sdate = dr("StartDate").ToString
                edate = dr("EndDate").ToString
            End While

            'ds = db.RunParamSQLDataSet(sb.ToString)
            'sb.Remove(0, sb.Length)

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return sdate & "," & edate

    End Function
    ''Added by SAraswathi Lakshmanan
    Public Function GetClsSectDates_sp(ByVal ClsSectId As String) As String
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        Dim sdate As String
        Dim edate As String
        Dim db As New FAME.DataAccessLayer.SQLDataAccess
        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")


            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ClsSectId", New Guid(ClsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)


            '   db.OpenConnection()
            'Execute the query
            Dim dr As SqlDataReader = db.RunParamSQLDataReader_SP("USP_AR_RegStd_GetClsSectDates")

            While dr.Read()
                sdate = dr("StartDate").ToString
                edate = dr("EndDate").ToString
            End While

            'ds = db.RunParamSQLDataSet(sb.ToString)
            'sb.Remove(0, sb.Length)


            'Close Connection

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        Finally
            db.CloseConnection()

        End Try

        'Return the datatable in the dataset
        Return sdate & "," & edate

    End Function

    Public Function IsStudentCurrentlyRegisteredInCourse(ByVal StuEnrollId As String, ByVal ClsSectId As String) As Integer

        Dim ds As New DataSet

        Dim da As OleDbDataAdapter

        Dim RowCount As Integer

        Dim sGrdSysDetailId As String



        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database

            Dim db As New MyPortalDataAccess

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

            Dim sb As New System.Text.StringBuilder



            '   build the sql query

            With sb

                .Append("Select count(*) as Count from arResults a, arClassSections b where a.StuEnrollId = ? ")
                .Append("and a.TestId in (Select c.ClsSectionId from arClassSections c where ReqId in(Select ReqId from arClassSections where ClsSectionId = ?)) ")
                .Append("and a.TestId = b.ClsSectionId ")
                .Append("and b.TermId in (Select TermId from arClassSections where ClsSectionId = ?) ")

            End With


            ' Add the PrgVerId and ChildId to the parameter list

            db.AddParameter("@Grade", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@clssectiond", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@clssectid", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query

            '   execute the query
            RowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection

            db.CloseConnection()


        Catch ex As System.Exception

            Throw New BaseException(ex.InnerException.ToString)

        End Try

        'Return the datatable in the dataset

        Return RowCount
    End Function
    ''Added by Saraswathi Lakshmanan

    Public Function IsStudentCurrentlyRegisteredInCourse_Sp(ByVal stuEnrollId As String, ByVal clsSectId As String) As Integer
        Dim db As New MyPortalDataAccess
        Dim ds As DataSet
        Dim courses As Integer

        Try
            db.AddParameter("@StuEnrollID", stuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ClsSectID", clsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet("dbo.USP_AR_RegStd_IsStudentRegisteredinCourse", Nothing, "SP")
            courses = ds.Tables(0).Rows(0)(0).ToString()
        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)
        Finally
            db.Dispose()
        End Try

        Return courses
    End Function

    ''Addded By Saraswathi lakshmanan on Feb 4th 2010
    Public Function DoesStdHavePassingGrdForNumeric_sp(ByVal StuEnrollId As String, ByVal ReqId As String, ByVal PrgVerId As String, ByVal ClsSectId As String) As Boolean

        Dim ds As New DataSet
        Dim da As SqlClient.SqlDataAdapter
        Dim returnValue As Boolean
        Dim db As New FAME.DataAccessLayer.SQLDataAccess
        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ReqID", New Guid(ReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@PrgVerID", New Guid(PrgVerId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ClsSectID", New Guid(ClsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ReturnValue", returnValue, SqlDbType.Bit, , ParameterDirection.Output)
            ' db.OpenConnection()
            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_AR_RegStd_DoesStdHavePassingGradeforNumeric")
            'Close Connection

        Catch ex As System.Exception
            Throw New BaseException(ex.InnerException.ToString)
        Finally
            db.CloseConnection()
        End Try
        'Return the datatable in the dataset
        Return returnValue
        'Dim dtWorkUnit As DataTable = (New StuTranscriptDB).GetWorkUnitResults(StuEnrollId, ReqId)
        'Dim avgScore As Decimal
        'Dim minPassScore As Decimal = (New StuProgressReportDB).GetMinPassingScore
        'If dtWorkUnit.Rows.Count > 0 Then
        '    Return IsPass(dtWorkUnit, StuEnrollId)
        'Else
        '    avgScore = (New SAPCheckDB).GetCourseAverage(StuEnrollId, ClsSectId)
        '    If SingletonAppSettings.AppSettings("GradeRounding").ToLower = "yes" Then
        '        avgScore = Math.Round(avgScore)
        '    End If
        '    If avgScore < minPassScore Then
        '        Return False
        '    End If
        'End If
        'Return True

    End Function
    ''Addded By Saraswathi lakshmanan on Feb 4th 2010
    Public Function DoesStdHavePassingGrd_sp(ByVal StuEnrollId As String, ByVal ReqId As String, ByVal PrgVerId As String, ByVal ClsSectId As String) As Boolean

        Dim ds As New DataSet
        Dim da As SqlClient.SqlDataAdapter
        Dim returnValue As Boolean
        Dim db As New FAME.DataAccessLayer.SQLDataAccess
        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ReqID", New Guid(ReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@PrgVerID", New Guid(PrgVerId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ClsSectID", New Guid(ClsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ReturnValue", returnValue, SqlDbType.Bit, , ParameterDirection.Output)
            '  db.OpenConnection()
            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_AR_RegStd_DoesStdHavePassingGrade")
            'Close Connection
            'db.CloseConnection()
        Catch ex As System.Exception
            Throw New BaseException(ex.InnerException.ToString)
        Finally
            db.CloseConnection()
        End Try
        'Return the datatable in the dataset
        Return returnValue
        'Dim dtWorkUnit As DataTable = (New StuTranscriptDB).GetWorkUnitResults(StuEnrollId, ReqId)
        'Dim avgScore As Decimal
        'Dim minPassScore As Decimal = (New StuProgressReportDB).GetMinPassingScore
        'If dtWorkUnit.Rows.Count > 0 Then
        '    Return IsPass(dtWorkUnit, StuEnrollId)
        'Else
        '    avgScore = (New SAPCheckDB).GetCourseAverage(StuEnrollId, ClsSectId)
        '    If SingletonAppSettings.AppSettings("GradeRounding").ToLower = "yes" Then
        '        avgScore = Math.Round(avgScore)
        '    End If
        '    If avgScore < minPassScore Then
        '        Return False
        '    End If
        'End If
        'Return True

    End Function
    Public Function RegisterStudentForGradeBookComponents(ByVal StuEnrollId As String, ByVal ClsSectionId As String, ByVal user As String, ByVal rightNow As Date) As String
        Dim db As New MyPortalDataAccess

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

            Dim sb As New System.Text.StringBuilder
            'Modified by Balaji on 04/03/2009 to fix issue 13655
            'When a student is registered for a class, records were inserted in to arResults table for all schools
            'For Ross Type School (defined by ShowRossOnlyTabs entry in web.config), in addition to inserting records in to arresults, records were inserted into arGrdBkResults table
            'It was realized later that for non-ross type schools, if student is registered in an Externship class then record needs to be inserted into 
            'arGrdBkResults table, otherwise records will not show up in Post Externship Attendance and Attendance cannot be posted for the externship hours attended by the student
            If MyPortalAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "true" Or _
              MyPortalAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "yes" Then
                With sb
                    .Append("insert into arGrdBkResults (GrdBkResultId, ClsSectionId, InstrGrdBkWgtDetailId, Comments, StuEnrollId, ModUser, ModDate) ")
                    .Append("select ")
                    .Append("		NewId() as GrdBkResultId,  ")
                    .Append("		CS.ClsSectionId, ")
                    .Append("		GBWD.InstrGrdBkWgtDetailId, ")
                    .Append("       GCT.Descrip, ")
                    .Append("		R.StuEnrollId, ")
                    .Append("		?, ")
                    .Append("		? ")
                    .Append("from	arResults R, arClassSections CS, arGrdBkWeights GBW, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT  ")
                    .Append("where	R.StuEnrollId=? ")
                    .Append("and		R.TestId=CS.ClsSectionId  ")
                    .Append("and		CS.ReqId=GBW.ReqId ")
                    .Append("and		GBW.InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId  ")
                    .Append("and		GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId  ")
                    .Append("and		GBW.EffectiveDate = (Select max(EffectiveDate) from arGrdBkWeights where ReqId=GBW.ReqId and EffectiveDate <= CS.StartDate) ")
                    .Append("and		not exists (select *  ")
                    .Append("					from arGrdBkResults  ")
                    .Append("					where ClsSectionId=CS.ClsSectionId ")
                    .Append("					and	  InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
                    .Append("					and	  StuEnrollId=?) ")
                    .Append("and    CS.ClsSectionId= ? ")
                End With
            Else
                With sb
                    .Append("insert into arGrdBkResults (GrdBkResultId, ClsSectionId, InstrGrdBkWgtDetailId, Comments, StuEnrollId, ModUser, ModDate) ")
                    .Append("select ")
                    .Append("		NewId() as GrdBkResultId,  ")
                    .Append("		CS.ClsSectionId, ")
                    .Append("		GBWD.InstrGrdBkWgtDetailId, ")
                    .Append("       GCT.Descrip, ")
                    .Append("		R.StuEnrollId, ")
                    .Append("		?, ")
                    .Append("		? ")
                    .Append("from	arResults R, arClassSections CS, arGrdBkWeights GBW, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT,arReqs RQ  ")
                    .Append("where	R.StuEnrollId=? ")
                    .Append("and		R.TestId=CS.ClsSectionId  ")
                    .Append("and		CS.ReqId=GBW.ReqId ")
                    .Append("and		GBW.InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId  ")
                    .Append("and		GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId  and CS.ReqId=RQ.ReqId and RQ.IsExternShip=1  and GCT.SysComponentTypeid=544 and R.score is null and R.GrdSysDetailid is null ")
                    .Append("and		GBW.EffectiveDate = (Select max(EffectiveDate) from arGrdBkWeights where ReqId=GBW.ReqId and EffectiveDate <= CS.StartDate) ")
                    .Append("and		not exists (select *  ")
                    .Append("					from arGrdBkResults  ")
                    .Append("					where ClsSectionId=CS.ClsSectionId ")
                    .Append("					and	  InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
                    .Append("					and	  StuEnrollId=?) ")
                    .Append("and    CS.ClsSectionId= ? ")
                End With
            End If
            'ModUser
            db.AddParameter("@ModUser", user, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", rightNow, MyPortalDataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'stuEnrollId
            db.AddParameter("@StuEnrollId", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'stuEnrollId
            db.AddParameter("@StuEnrollId", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'stuEnrollId
            db.AddParameter("@ClsSectionId", ClsSectionId, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   commit transaction
            groupTrans.Commit()

            '   return with no errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            ''   do not report sql lost connection
            'If Not groupTrans.Connection Is Nothing Then
            '    '   report an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
            'End If

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteGradeBookComponentsForAClassSection(ByVal StuEnrollId As String, ByVal ClsSectionId As String, ByVal user As String) As String
        Dim db As New MyPortalDataAccess

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")

            Dim sb As New System.Text.StringBuilder
            With sb
                .Append("Delete from arGrdBkResults GBR, arClassSections CS  ")
                .Append("where	GBR.ClsSectionId=CS.ClsSectionId ")
                .Append("and	StuEnrollId=? ")
                .Append("and	CS.ClsSectionId=? ")
            End With
            'stuEnrollId
            db.AddParameter("@StuEnrollId", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'stuEnrollId
            db.AddParameter("@ClsSectionId", ClsSectionId, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   commit transaction
            groupTrans.Commit()

            '   return with no errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            ''   do not report sql lost connection
            'If Not groupTrans.Connection Is Nothing Then
            '    '   report an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
            'End If

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    ''Function added by Saraswathi lakshmanan on August 20 2009
    ''by Saraswathi lakshmanan
    ''For use in Portal , While Registering a student fr the course, IF the prereqs are taken in the previous term and they are not graded then allow the student to register in the course.
    Public Function IsStudentRegisteredandIsCourseInDifferentTerm(ByVal StuEnrollId As String, ByVal Reqid As String, ByVal ClsSectId As String, ByVal CampusId As String) As Boolean

        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim RowCount As Integer = 0
        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder
            With sb
                .Append("Select count(*) as Count from arResults a, arClassSections b,arTerm C where a.StuEnrollId = ? ")
                .Append(" and a.TestId in (Select c.ClsSectionId from arClassSections c where ReqId=? and CampusId=?) ")
                .Append(" and a.TestId = b.ClsSectionId ")
                .Append(" and b.TermId <> (Select TermId from arClassSections where ClsSectionId = ? and CampusId=?)")
                .Append(" and b.CampusID=? ")
                .Append(" and a.GrdSysDetailid is NULL ")
                .Append(" and b.TermId=C.TermID and C.StartDate<(Select arTerm.StartDate from arClassSections,arTerm where ClsSectionId=? and CampusID=? and arterm.TermID=arClassSections.TermID) ")


            End With
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StuEnrollid", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Reqid", Reqid, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CampusID", CampusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClsSectionId", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CampusID", CampusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CampusID", CampusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClsSectionId", ClsSectId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CampusID", CampusId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)



            db.OpenConnection()
            '   execute the query
            RowCount = db.RunParamSQLScalar(sb.ToString)
            'Close Connection
            db.CloseConnection()
        Catch ex As System.Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try
        'Return the datatable in the dataset
        If RowCount = 0 Then
            Return False
        ElseIf RowCount >= 1 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function GetTermForClassSection(ByVal clsSectionId As String) As String
        Dim rtn As String = "00000000-0000-0000-0000-000000000000"
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter



        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append(" select isnull(ProgId,'00000000-0000-0000-0000-000000000000') as Progid from arTerm,arClassSections where arTerm.TermId=arClassSections.TermId and arClassSections.ClsSectionId= ? ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ReqId", clsSectionId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            db.OpenConnection()
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                rtn = dr("Progid").ToString

            End While

            'ds = db.RunParamSQLDataSet(sb.ToString)
            'sb.Remove(0, sb.Length)

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return rtn


    End Function



    Public Function HasStudentPassedSpeedTestsForCourse(ByVal StuEnrollId As String, ByVal ReqId As String) As Boolean
        Dim RowCount As Integer = 0


        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("select count(*) from ( ")
                .Append("select s.*, ")
                .Append("       (select count(*) ")
                .Append("        from arPostScoreDictationSpeedTest psdt ")
                .Append("        where psdt.grdcomponenttypeid=S.GrdComponentTypeId ")
                .Append("        and psdt.StuEnrollId=? ")
                .Append("        and psdt.Speed >= S.MinimumScore) As ActualNumberOfTestsPassed ")
                .Append("from ")
                .Append("( ")
                .Append("select rq.Descrip,gct.Descrip as Component,r.EffectiveDate,r.NumberOfTests,r.MinimumScore,rq.ReqId, g.grdcomponenttypeid ")
                .Append("from arBridge_GradeComponentTypes_Courses g, arRules_GradeComponentTypes_Courses r, arReqs rq, arGrdComponentTypes gct ")
                .Append("where g.GrdComponentTypeId_ReqId=r.GrdComponentTypeId_ReqId ")
                .Append("and g.ReqId=rq.ReqId ")
                .Append("and g.GrdComponentTypeId=gct.GrdComponentTypeId ")
                'The effective date must be based on the last time the student took the course
                .Append("and r.EffectiveDate <= (select top 1 cs.StartDate ")
                .Append("                        from arResults rs, arClassSections cs ")
                .Append("                        where rs.StuEnrollId=? ")
                .Append("                        and rs.TestId=cs.ClsSectionId ")
                .Append("                        and cs.ReqId=? ")
                .Append("                        and cs.ReqId=g.ReqId ")
                .Append("                        order by cs.StartDate desc ")
                .Append("                        ) ")
                .Append("and r.EffectiveDate = (select max(EffectiveDate) from 	")
                .Append("                           ( ")
                .Append("                               select r.EffectiveDate ")
                .Append("                               from arBridge_GradeComponentTypes_Courses g, arRules_GradeComponentTypes_Courses r, arReqs rq, arGrdComponentTypes gct ")
                .Append("                               where g.GrdComponentTypeId_ReqId=r.GrdComponentTypeId_ReqId ")
                .Append("                               and g.ReqId=rq.ReqId ")
                .Append("                               and g.GrdComponentTypeId=gct.GrdComponentTypeId ")
                .Append("                              and r.EffectiveDate <= (select top 1 cs.StartDate ")
                .Append("                                                       from arResults rs, arClassSections cs ")
                .Append("                                                       where rs.StuEnrollId=? ")
                .Append("                                                       and rs.TestId=cs.ClsSectionId ")
                .Append("                                                       and cs.ReqId=? ")
                .Append("                                                       and cs.ReqId=g.ReqId ")
                .Append("                                                       order by cs.StartDate desc ")
                .Append("                                                       ) ")
                .Append("                           ) R	")
                .Append("                       ) ")
                .Append(") S ")
                .Append(") T ")
                .Append("where NumberOfTests > ActualNumberOfTestsPassed ")
            End With


            db.AddParameter("@StuEnrollId", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ReqId", ReqId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ReqId", ReqId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   open the connection
            db.OpenConnection()
            '   execute the query
            RowCount = db.RunParamSQLScalar(sb.ToString)
            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw New BaseException(ex.Message)
            Else
                Throw New BaseException(ex.InnerException.Message)
            End If

        End Try

        'The query returns how many speed tests for a course for an enrollment have NOT been passed.
        'If 0 is returned then it means that all the speed tests have been satisifed
        'If any value greater than zero is returned it means that there are that number of speed tests that have
        'NOT been satisfied. For eg. if 2 is returned then it means that there are 2 speed tests not satisfied.
        If RowCount > 0 Then
            Return False
        Else
            Return True
        End If


    End Function


    Public Function HasStudentPassedSpeedTestsForCourse_sp(ByVal stuEnrollId As String, ByVal reqId As String) As Boolean
        Dim db As New MyPortalDataAccess
        Dim ds As New DataSet
        Dim notPassed As Integer = 0

        Try
            db.AddParameter("@StuEnrollId", stuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ReqId", reqId, MyPortalDataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet("dbo.USP_AR_HasStudentPassedSpeedTestsForCourse", Nothing, "SP")
            notPassed = ds.Tables(0).Rows(0)(0).ToString()
        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New BaseException(ex.Message)
            Else
                Throw New BaseException(ex.InnerException.Message)
            End If
        Finally
            db.Dispose()
        End Try

        If notPassed > 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    ''Added by SAraswathi lakshmanan
    Public Function ChkStudSchedConflict_SP(ByVal StuEnrollID As String, ByVal ClsSectID As String) As String
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        Dim ErrorString As String
        Dim db As New FAME.DataAccessLayer.SQLDataAccess
        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database

            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StuEnrollId", New Guid(StuEnrollID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ClsSectID", New Guid(ClsSectID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ErrorString", New Guid(ErrorString), SqlDbType.UniqueIdentifier, , ParameterDirection.Output)

            '   Execute the query
            'db.OpenConnection()
            db.RunParamSQLExecuteNoneQuery_SP("USP_AR_RegStd_CheckStdScheduleConflict")

            'Close Connection
            '  db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        Finally
            db.CloseConnection()
        End Try

        Return ErrorString
    End Function

    '' Code Added by Kamalesh Ahuja on June 09 2010 to resolve Mantis Issue Id 18141
    Public Function IsStudentHold(ByVal StuEnrollId As String) As String
        Dim ds As New DataSet

        Try
            '   connect to the database
            Dim db As New MyPortalDataAccess
            Dim sb As New System.Text.StringBuilder


            '   build the sql query
            With sb
                .Append("select count (*)as IsRegHold   from adStuGrpStudents SGS,adStudentGroups SG ,arStuEnrollments SE ")
                '' New Code Added By Vijay Ramteke On June 19, 2010 For Mantis Id 18141
                ''.Append(" where SGS.StudentId=SE.StudentId and SGS.StuGrpId=SG.StuGrpId and SG.IsRegHold=1 and SE.StuEnrollId=? ")
                .Append(" where SGS.StudentId=SE.StudentId and SGS.StuGrpId=SG.StuGrpId and SG.IsRegHold=1 AND SGS.IsDeleted=0 and SGS.StuEnrollId=? ")
                '' New Code Added By Vijay Ramteke On June 19, 2010 For Mantis Id 18141
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StuEnrollId", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            '   Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)



        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds.Tables(0).Rows(0)(0).ToString

    End Function
    Public Function IsStudentTranscriptHold(ByVal StuEnrollId As String) As String
        Dim ds As New DataSet

        Try
            '   connect to the database
            Dim db As New MyPortalDataAccess
            Dim sb As New System.Text.StringBuilder


            '   build the sql query
            With sb
                .Append("select count (*)as IsRegHold   from adStuGrpStudents SGS,adStudentGroups SG ,arStuEnrollments SE ")
                '' New Code Added By Vijay Ramteke On June 19, 2010 For Mantis Id 18141
                ''.Append(" where SGS.StudentId=SE.StudentId and SGS.StuGrpId=SG.StuGrpId and SG.IsRegHold=1 and SE.StuEnrollId=? ")
                .Append(" where SGS.StudentId=SE.StudentId and SGS.StuGrpId=SG.StuGrpId and SG.IsTransHold=1 AND SGS.IsDeleted=0 and SGS.StuEnrollId=? ")
                '' New Code Added By Vijay Ramteke On June 19, 2010 For Mantis Id 18141
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StuEnrollId", StuEnrollId, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            '   Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)



        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds.Tables(0).Rows(0)(0).ToString

    End Function
    ''''
    Public Function IsStudentEligibleForRegistration(ByVal StuEnrollId As String, ByVal ClsSectionId As String) As String
        Dim db As New FAME.DataAccessLayer.SQLDataAccess
        Dim ds As New DataSet
        Dim strReturnMessage As String

        Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New MyPortalAdvAppSettings
        End If

        db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, 50, ParameterDirection.Input)
            db.AddParameter("@ClsSectionId", New Guid(ClsSectionId), SqlDbType.UniqueIdentifier, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_IsStudentEligibleForRegistration_Count", "IsStudentEligibleForRegistration")

            If Not ds Is Nothing Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    strReturnMessage = dr(0)
                    Return strReturnMessage
                Next
            Else
                Return "no"
            End If
        Catch ex As System.Exception
            Return "no"
        Finally
            db.CloseConnection()
        End Try
    End Function
    'DE9177 3/5/2013 Janet Robinson
    Public Function GetProgAcademicCalendar(ByVal StuEnrollID As String) As String
        Dim da As OleDbDataAdapter
        Dim sAcadCal As String = String.Empty

        Try

            Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
                MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
            Else
                MyPortalAdvAppSettings = New MyPortalAdvAppSettings
            End If

            '   connect to the database
            Dim db As New MyPortalDataAccess
            db.ConnectionString = MyPortalAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder



            With sb
                .Append("Select D.ACDescrip ")
                .Append("from ")
                .Append("arStuEnrollments A ")
                .Append("INNER JOIN dbo.arPrgVersions B ON A.PrgVerId = B.PrgVerId ")
                .Append("INNER JOIN dbo.arPrograms C ON B.ProgId = C.ProgId ")
                .Append("INNER JOIN syAcademicCalendars D ON C.ACId = D.ACId ")
                .Append("WHERE a.StuEnrollId = ?   ")
            End With
            ' Add the student enrollid and ClsSectId to the parameter list
            db.AddParameter("@StdId", StuEnrollID, MyPortalDataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   build the sql query

            '   Execute the query
            db.OpenConnection()

            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()

                sAcadCal = dr("ACDescrip")

            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return sAcadCal
    End Function

    Public Function IsCourseExamsOnly(ByVal CourseCode As String, ByVal ResourceName As String) As Boolean
        Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New MyPortalAdvAppSettings
        End If

        Try

            Dim myDataSet As New DataSet()
            Using myConnection As New SqlConnection(MyPortalAdvAppSettings.AppSettings("ConnectionString").ToString)
                Using myCommand As New SqlCommand("USP_IsCourseExamsOnly", myConnection)
                    myCommand.CommandType = CommandType.StoredProcedure
                    myCommand.Parameters.Add(New SqlParameter("@CourseCode", SqlDbType.VarChar)).Value = CourseCode
                    myCommand.Parameters.Add(New SqlParameter("@ResourceName", SqlDbType.VarChar)).Value = ResourceName
                    Dim mySqlDataAdapter As New SqlDataAdapter(myCommand)
                    mySqlDataAdapter.Fill(myDataSet)
                    myCommand.Connection.Close()
                End Using
            End Using

            Dim bIsCourseExamsOnly As Boolean
            If myDataSet.Tables(0).Rows(0).Item(0) = 0 Then
                bIsCourseExamsOnly = False
            Else
                bIsCourseExamsOnly = True
            End If

            Return bIsCourseExamsOnly
        Catch ex As System.Exception
            Return Nothing
        Finally

        End Try
    End Function

    Public Function HasStudentCompletedClass(ByVal stuEnrollid As String, ByVal clsSectionId As String) As Boolean
        Dim myPortalAdvAppSettings As MyPortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("MyPortalAdvAppSettings"), MyPortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New MyPortalAdvAppSettings
        End If

        Try

            Dim myDataSet As New DataSet()
            Using myConnection As New SqlConnection(MyPortalAdvAppSettings.AppSettings("ConnectionString").ToString)
                Using myCommand As New SqlCommand("USP_CheckIfClassWasAlreadyCompleted", myConnection)
                    myCommand.CommandType = CommandType.StoredProcedure
                    myCommand.Parameters.Add(New SqlParameter("@StuEnrollId", SqlDbType.UniqueIdentifier)).Value = New Guid(stuEnrollid)
                    myCommand.Parameters.Add(New SqlParameter("@ClsSectionId", SqlDbType.UniqueIdentifier)).Value = New Guid(clsSectionId)
                    Dim mySqlDataAdapter As New SqlDataAdapter(myCommand)
                    mySqlDataAdapter.Fill(myDataSet)
                    myCommand.Connection.Close()
                End Using
            End Using

            Dim bIsClassAlreadyCompleted = myDataSet.Tables(0).Rows(0)("gradedcount") >= 1
            Return bIsClassAlreadyCompleted
        Catch ex As Exception
            Return Nothing
        Finally

        End Try
    End Function


End Class
