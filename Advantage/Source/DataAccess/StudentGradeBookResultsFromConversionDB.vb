Imports FAME.Advantage.Common

Public Class StudentGradeBookResultsFromConversionDB
    Public Function GetCourseResultsForEnrollment(ByVal stuEnrollId As String, ByVal reqId As String) As DataTable
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append("SELECT gct.Descrip,gbr.PostUser,gbr.PostDate,gbr.Comments,gbr.Score ")
            .Append("FROM arGrdBkConversionResults gbr, arGrdComponentTypes gct ")
            .Append("WHERE gbr.GrdComponentTypeId=gct.GrdComponentTypeId ")
            .Append("AND gbr.StuEnrollId = ? ")
            .Append("AND gbr.ReqId = ? ")
            .Append("ORDER BY gct.Descrip ")
        End With

        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@reqid", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        '   return DataTable
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function

    Public Function GetCoursesWithResultsForEnrollment(ByVal stuEnrollId As String) As DataTable
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append("SELECT distinct rq.Descrip,rq.ReqId ")
            .Append("FROM arGrdBkConversionResults gbr, arReqs rq ")
            .Append("WHERE gbr.ReqId=rq.ReqId ")
            .Append("AND gbr.StuEnrollId=? ")
            .Append("ORDER BY rq.Descrip ")
        End With

        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        '   return DataTable
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
    End Function
End Class
