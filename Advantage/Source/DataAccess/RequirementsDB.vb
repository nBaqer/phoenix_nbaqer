
Imports FAME.Advantage.Common


Public Class RequirementsDB
    Public Function StandardReqByGrpAndEffectiveDates(ByVal LeadId As String, ByVal strEnrollDate As Date) As String
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,R5.TestAttempted+R5.DocsAttempted+R5.TestDocumentAttempted as AttemptedReqs ")
            .Append(" from ")
            .Append(" (  ")
            ' Get Mandatory requirement group
            .Append("      select ")
            .Append("              	t7.ReqGrpId,t7.Descrip, ")
            .Append("             			( ")
            .Append("              			select Count(*) as NumReqs ")
            .Append("              			from ")
            .Append("      			 			( ")
            .Append("              					select ")
            .Append("								Distinct A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("              						adReqs A2,adReqsEffectiveDates A3  ")
            .Append("              					where ")
            .Append("					              	A2.adReqId = A3.adReqId  ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" and A2.reqforEnrollment=1 ")
            .Append("								    and A3.MandatoryRequirement=1 and  ")
            .Append("					              	A2.adreqTypeId in (1,3) ")
            .Append("              				) ")
            .Append("              				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("                			) ")
            .Append("              		as NumReqs, ")
            .Append("              		( ")
            .Append("              			select Count(*) as TestAttempted ")
            .Append("              			from ")
            .Append("              				( ")
            .Append("              					select ")
            .Append("								Distinct A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("						              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and  ")
            .Append("					              	A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1 and  ")
            .Append("					              	A2.adreqTypeId in (1) ")
            .Append("				              ) ")
            .Append("			              R1,adLeadEntranceTest R2 ")
            .Append("					where ")
            .Append("						R1.adReqId = R2.EntrTestId and ")
            .Append("						R2.LeadId='" & LeadId & "' ")
            .Append("	              			and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and R2.Pass=1 and ")
            .Append("                        R2.EntrTestId not in (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "') ")
            .Append("						and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("              		) as TestAttempted, ")
            .Append("             			( ")
            .Append("			              select Count(*) as DocsAttempted ")
            .Append("			              from ")
            .Append("				           ( ")
            .Append("				              select ")
            .Append("							Distinct A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("						from  ")
            .Append("					              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("					              	A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1 and  ")
            .Append("					              	A2.adreqTypeId in (3) ")
            .Append("					      ) ")
            .Append("		              	R1,adLeadDocsReceived R2,syDocStatuses R3 ")
            .Append("					where 	")
            .Append("						R1.adReqId = R2.DocumentId and ")
            .Append("						R2.LeadId='" & LeadId & "'  ")
            .Append("                       and R2.DocumentId not in (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "') ")
            .Append("                       and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 ")
            .Append("				        and R1.CurrentDate >= R1.StartDate and ")
            .Append("						(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("		              ) as DocsAttempted, ")
            .Append("              		( ")
            .Append("              			select Count(*) as TestDocumentAttempted ")
            .Append("              			from ")
            .Append("              				( ")
            .Append("              					select ")
            .Append("								Distinct A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("						              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("					              	A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1 and  ")
            .Append("					              	A2.adreqTypeId in (1,3) ")
            .Append("				              ) ")
            .Append("			              R1,adEntrTestOverRide R2 ")
            .Append("					where ")
            .Append("						R1.adReqId = R2.EntrTestId and ")
            .Append("						R2.LeadId='" & LeadId & "' ")
            .Append("	              			and R1.CurrentDate >= R1.StartDate and R2.OverRide=1 and ")
            .Append("						(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("              		) as TestDocumentAttempted ")
            .Append("       from ")
            .Append("		adReqGroups t7 ")
            .Append("	where ")
            .Append("		IsMandatoryReqGrp=1   ")
            .Append("   ) R5  ")
            .Append("   Union  ")
            .Append("   select '00000000-0000-0000-0000-000000000000' as RepGrpId,NULL as Miscellaneous,NULL as NumReqs,NULL as AttemptedReqs from adLeadGroups  ")
            .Append("   order by ReqGrpId desc  ")
        End With

        With sb1
            '  This Query Brings the ouput based on the following
            '  All mandatory requirements
            '  requirements assigned to a lead group
            .Append(" select ")
            .Append("           adReqId, ")
            .Append("           Descrip, ")
            .Append("           ReqGrpId, ")
            .Append("           StartDate, ")
            .Append("           EndDate, ")
            .Append("           ActualScore,  ")
            .Append("           TestTaken, ")
            .Append("           Comments, ")
            .Append("           Case when OverRide=1 then 'True' else 'False' end as OverRide, ")
            .Append("           Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount, ")
            .Append("           DocSubmittedCount, ")
            .Append("           Minscore, ")
            .Append("           Required, ")
            .Append("           DocStatusDescrip ")
            .Append("           from ")
            .Append("           ( ")
            .Append("           select  Distinct ")
            .Append("	           adReqId, ")
            .Append("	           Descrip, ")
            .Append("	           ReqGrpId, ")
            .Append("	           StartDate, ")
            .Append("	           EndDate, ")
            .Append("	           ActualScore, ")
            .Append("	           TestTaken, ")
            .Append("	           Comments, ")
            .Append("	           OverRide, ")
            .Append("	           Minscore, ")
            .Append("	           Required, ")
            .Append("	           DocSubmittedCount, ")
            .Append("	           DocStatusDescrip ")
            .Append("           from  ")
            .Append("           ( ")
            '  Get Requirement Group and requirements of mandatory requirement
            .Append("          	 select  ")
            .Append("	           	t1.adReqId, ")
            .Append("	           	t1.Descrip, ")
            .Append("	           	case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
            .Append("	           		(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
            .Append("	           	else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId,  ")
            .Append("	           	'" & strEnrollDate & "' as CurrentDate,  ")
            .Append("	           	t2.StartDate, ")
            .Append("	           	t2.EndDate,  ")
            .Append("	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId='" & LeadId & "')  as ActualScore,  ")
            .Append("	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId='" & LeadId & "')  as TestTaken,  ")
            .Append("	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId='" & LeadId & "')  as Comments,  ")
            .Append("	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and  ")
            .Append("	                              LeadId='" & LeadId & "') ")
            .Append("	                      as override, ")
            .Append("	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
            .Append("	           		LeadId='" & LeadId & "')  as DocSubmittedCount, ")
            ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
            ''.Append("	           	cast(t2.Minscore as int) as MinScore, ")
            .Append("	           	t2.Minscore  , ")
            ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
            .Append("	           	1 as Required, ")
            .Append("	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("	           s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("           from ")
            .Append("           	adReqs t1, ")
            .Append("           	adReqsEffectiveDates t2 ")
            .Append("           where  ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" t1.reqforEnrollment=1 and ")
            .Append("           	t1.adReqId = t2.adReqId and  ")
            .Append("           	t2.MandatoryRequirement=1 and ")
            .Append("           	t1.adReqTypeId in (1,3) ")
            .Append("           ) ")
            .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" union ")
            '  Get Requirements that were assigned to lead group but not part of any requirement group
            .Append(" select ")
            .Append("           adReqId, ")
            .Append("           Descrip, ")
            .Append("           ReqGrpId, ")
            .Append("           StartDate, ")
            .Append("           EndDate, ")
            .Append("           ActualScore,  ")
            .Append("           TestTaken, ")
            .Append("           Comments, ")
            .Append("           OverRide, ")
            .Append("	           Minscore, ")
            .Append("	           Required, ")
            .Append("	           DocSubmittedCount, ")
            .Append("	           DocStatusDescrip ")
            .Append("           from ")
            .Append("           ( ")
            '  Get Requirement Group and requirements that are assigned to a lead group
            '  and part of a requirement group
            .Append("           	 select  ")
            .Append(" 	           	t1.adReqId, ")
            .Append(" 	           	t1.Descrip, ")
            .Append(" 	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            .Append(" 	           	'" & strEnrollDate & "' as CurrentDate,  ")
            .Append(" 	           	t2.StartDate, ")
            .Append(" 	           	t2.EndDate,  ")
            .Append(" 	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append(" 	           		LeadId='" & LeadId & "')  as ActualScore,  ")
            .Append(" 	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append(" 	           		LeadId='" & LeadId & "')  as TestTaken,  ")
            .Append(" 	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append(" 	           		LeadId='" & LeadId & "')  as Comments,  ")
            .Append(" 	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append(" 	                              LeadId='" & LeadId & "') ")
            .Append(" 	                      as override, ")
            .Append(" 	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
            .Append(" 	           		LeadId='" & LeadId & "')  as DocSubmittedCount, ")
            ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
            ''.Append(" 	           	cast(t2.Minscore as int) as MinScore, ")
            .Append(" 	           	t2.Minscore  , ")
            ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
            .Append(" 			ISNULL(t3.IsRequired,0) as Required, ")
            .Append(" 	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" 	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" 	           s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("            from ")
            .Append("           	adReqs t1, ")
            .Append("           	adReqsEffectiveDates t2, ")
            .Append("		adReqLeadGroups t3 ")
            .Append("           where  ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" t1.reqforEnrollment=1 and ")
            .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId ")
            ' and t1.adReqId not in (select Distinct adReqId from adReqGrpDef)
            .Append("		and t3.LeadGrpId in ")
            .Append("		(select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and ")
            .Append("           	t1.adReqTypeId in (1,3) and t2.MandatoryRequirement <> 1 ")
            .Append("           ) ")
            .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" ) R2 ")
        End With

        '' Grab the Categories and Products table
        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        dadNorthwind = New OleDbDataAdapter(sb.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "Parent")

        dadNorthwind.SelectCommand = New OleDbCommand(sb1.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "Child")
        conNorthwind.Close()

        ' Add Parent/Child Relationship
        Try
            dstNorthwind.Relations.Add(
             "myrelation",
             dstNorthwind.Tables("Parent").Columns("ReqGrpId"),
             dstNorthwind.Tables("Child").Columns("ReqGrpId"))
        Catch ex As Exception
            Return "None"
            Exit Function
        End Try

        ''Display each Skill Group and Child Skills for the Student
        s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
        For Each drowParent In dstNorthwind.Tables("Parent").Rows
            If Not drowParent("Descrip") Is DBNull.Value Then
                s3 &= "<TR>"
                s3 &= "<td nowrap><font face=""verdana"" size=""2""><strong>" & drowParent("Descrip") & "</strong>&nbsp;</font>"
                If Not drowParent("NumReqs") Is DBNull.Value Then
                    If drowParent("NumReqs") >= 1 Then
                        s3 &= "<span style=""font: normal 12px arial; padding: 16px"">[Min Req:" & drowParent("NumReqs") & "]"
                        s3 &= "[Min Attempted:" & drowParent("AttemptedReqs") & "]</span>"
                    End If
                End If
                s3 &= "</td></tr>"
            Else
                s3 &= "<TR><td><font face=""verdana"" size=""2""><strong>Miscellaneous</strong></font></td></tr>"
            End If
            For Each drowChild In drowParent.GetChildRows("myrelation")
                If Not drowChild("Descrip") Is DBNull.Value Then
                    s3 &= "<tr>"
                    If Not drowChild("TestTakenCount") Is DBNull.Value And drowChild("TestTakenCount") >= 1 Then
                        s3 &= "<td style=""font-size: 12px""><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
                    ElseIf Not drowChild("TestTakenCount") Is DBNull.Value And drowChild("DocSubmittedCount") >= 1 Then
                        s3 &= "<td style=""font-size: 12px""><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
                    Else
                        s3 &= "<td style=""font-size: 12px""><ul><li><input type=""checkbox"" id=""chkTaken"" disabled></li>"
                    End If
                    s3 &= "<span style=""font:  normal 12px arial; padding: 16px"">" & drowChild("Descrip")
                    If drowChild("Required") = 1 Then
                        s3 &= "<span style=""color:red; padding-left: 3px;font: normal 12px arial;""><strong>(Required)</strong></span>"
                    End If

                    'Check If the Lead Has Passed The Test
                    If Not drowChild("ActualScore") Is DBNull.Value Then
                        If drowChild("MinScore") Is DBNull.Value Then
                            s3 &= ""
                        Else
                            If drowChild("ActualScore") >= 1 Then
                                If (drowChild("ActualScore") < drowChild("MinScore")) And (Not drowChild("TestTaken") = "") Then
                                    s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Fail)</strong></span>"
                                ElseIf (drowChild("ActualScore") >= drowChild("MinScore")) And (Not drowChild("TestTaken") = "") Then
                                    s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Pass)</strong></span>"
                                End If
                            End If
                        End If
                    End If

                    'Check If Document was Approved
                    If Not drowChild("DocStatusDescrip") Is DBNull.Value Then
                        If drowChild("DocStatusDescrip") = "Approved" Then
                            s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Approved)</strong></span>"
                        Else
                            s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Not Approved)</strong></span>"
                        End If
                    End If


                    If Not drowChild("OverRide") Is DBNull.Value Then
                        If drowChild("OverRide") = "True" Then
                            s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Overriden)</strong></span>"
                        End If
                    End If
                    s3 &= "</span></ul></td></tr>"
                End If
            Next
            s3 &= "<tr height=10><td>&nbsp;</td></tr>"
            forCounter = 1
        Next
        s3 &= "</table>"

        If forCounter = 1 Then
            Return s3.ToString()
        Else
            Return "None"
        End If
    End Function
    Public Function ReqByGrpandEffectiveDates(ByVal LeadId As String, ByVal PrgVerId As String, ByVal strEnrollDate As Date) As String
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Dim strCurrentDate As Date = strEnrollDate


        With sb
            .Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,R5.TestAttempted+R5.DocsAttempted+R5.TestDocumentAttempted as AttemptedReqs ")
            .Append(" from ")
            .Append(" (  ")
            ' Requiremt group assigned to a program version
            .Append("	Select  ")
            .Append("		Distinct ")
            .Append("			t1.ReqGrpId, ")
            .Append("			t2.Descrip, ")
            .Append("			t3.NumReqs as Numreqs,  ")
            .Append("	            	    ( ")
            .Append("       	     		select Count(*) as TestAttempted ")
            .Append("            				from ")
            .Append("			            		( ")
            .Append("            						select ")
            .Append("								Distinct A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("       	     						adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append("            						where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("				            			A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and ")
            .Append("								A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append("       		     					A4.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            .Append("		            					ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (1) ")
            .Append("	       			     	) ")
            .Append("					R1,adLeadEntranceTest R2 ")
            .Append("				where ")
            .Append("					R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "'")
            .Append("			            	and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and R2.Pass=1 ")
            .Append("                       and R2.EntrTestId not in (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "') ")
            .Append("					and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("	      	        )as TestAttempted, ")
            .Append("	               ( ")
            .Append("		            	select Count(*) as DocsAttempted ")
            .Append("       			     	from ")
            .Append("			            		( ")
            .Append("			            			select ")
            .Append("								Distinct A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("            							adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append("			            			where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("	            						A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append("			       		     		A4.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            .Append("            							ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (3) ")
            .Append("			            		) ")
            .Append("            				R1,adLeadDocsReceived R2,syDocStatuses R3 ")
            .Append("				where ")
            .Append("					R1.adReqId = R2.DocumentId and R2.LeadId='" & LeadId & "'")
            .Append("                       and R2.DocumentId not in (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "') ")
            .Append("                       and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 ")
            .Append("		            	and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("            		) as DocsAttempted,  ")
            .Append("	            	    ( ")
            .Append("       	     		select Count(*) as TestDocumentAttempted ")
            .Append("            				from ")
            .Append("			            		( ")
            .Append("            						select ")
            .Append("								Distinct A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("       	     						adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append("            						where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("				            			A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and ")
            .Append("								A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append("       		     					A4.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            .Append("		            					ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (1,3) ")
            .Append("	       			     	) ")
            .Append("					R1,adEntrTestOverRide R2 ")
            .Append("				where ")
            .Append("					R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "'")
            .Append("			            	and R1.CurrentDate >= R1.StartDate and R2.OverRide=1 and ")
            .Append("					(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("	      	        ) as TestDocumentAttempted ")
            .Append("      	from ")
            .Append("			adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3  ")
            .Append("       where  ")
            .Append(" 	              t1.ReqGrpId = t2.ReqGrpId and ")
            .Append("			t2.ReqGrpId = t3.ReqGrpId and ")
            .Append("			t1.PrgVerId='" & PrgVerId & "' and t2.IsMandatoryReqGrp <> 1  ")
            ''Commented for DE5552
            '.Append("                and     t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups ")
            '.Append("			where LeadId='" & LeadId & "') ")
            .Append(" union ")
            ' Get Mandatory requirement group
            .Append("      select ")
            .Append("              	t7.ReqGrpId,t7.Descrip, ")
            .Append("             			( ")
            .Append("              			select Count(*) as NumReqs ")
            .Append("              			from ")
            .Append("      			 			( ")
            .Append("              					select Distinct ")
            .Append("								A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("              						adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("					              	A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1   ")
            .Append("					              	and A2.adreqTypeId in (1,3) ")
            .Append("              				) ")
            .Append("              				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("                			) ")
            .Append("              		as NumReqs, ")
            .Append("              		( ")
            .Append("              			select Count(*) as TestAttempted ")
            .Append("              			from ")
            .Append("              				( ")
            .Append("              					select Distinct ")
            .Append("								A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("						              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("					              	A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1  ")
            .Append("					              	and A2.adreqTypeId in (1) ")
            .Append("				              ) ")
            .Append("			              R1,adLeadEntranceTest R2 ")
            .Append("					where ")
            .Append("						R1.adReqId = R2.EntrTestId and ")
            .Append("						R2.LeadId='" & LeadId & "'")
            .Append("	              			and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and R2.Pass=1 and ")
            .Append("                       R2.EntrTestId not in (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "') ")
            .Append("						and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("              		) as TestAttempted, ")
            .Append("             			( ")
            .Append(" 			              select Count(*) as DocsAttempted ")
            .Append(" 			              from ")
            .Append(" 				           ( ")
            .Append(" 				              select Distinct ")
            .Append(" 							A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" 						from  ")
            .Append(" 					              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("					              	A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1 ")
            .Append("					              	and A2.adreqTypeId in (3) ")
            .Append(" 					      ) ")
            .Append(" 		              	R1,adLeadDocsReceived R2,syDocStatuses R3  ")
            .Append(" 					where 	")
            .Append(" 						R1.adReqId = R2.DocumentId and ")
            .Append(" 						R2.LeadId='" & LeadId & "' ")
            .Append("                       and R2.DocumentId not in (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "') ")
            .Append("                       and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 ")
            .Append(" 				              and R1.CurrentDate >= R1.StartDate and ")
            .Append(" 						(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" 		              ) as DocsAttempted, ")
            .Append("              		( ")
            .Append("              			select Count(*) as TestDocumentAttempted ")
            .Append("              			from ")
            .Append("              				( ")
            .Append("              					select Distinct ")
            .Append("								A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("						              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("					              	A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1  ")
            .Append("					              	and A2.adreqTypeId in (1,3) ")
            .Append("				              ) ")
            .Append("			              R1,adEntrTestOverRide R2 ")
            .Append("					where ")
            .Append("						R1.adReqId = R2.EntrTestId and ")
            .Append("						R2.LeadId='" & LeadId & "'")
            .Append("	              			and R1.CurrentDate >= R1.StartDate and R2.OverRide=1 and ")
            .Append("						(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("              		) as TestDocumentAttempted ")
            .Append("       from ")
            .Append(" 		adReqGroups t7 ")
            .Append(" 	where ")
            .Append(" 		IsMandatoryReqGrp=1   ")
            .Append("    ) R5  ")
            .Append("    Union  ")
            .Append("    select '00000000-0000-0000-0000-000000000000' as RepGrpId,NULL as Miscellaneous,NULL as NumReqs,NULL as AttemptedReqs from adLeadGroups  ")
            .Append("    order by ReqGrpId desc  ")
        End With

        With sb1
            .Append(" select  distinct ")
            .Append("       adReqId, ")
            .Append("       Descrip, ")
            .Append("       ReqGrpId, ")
            .Append("       StartDate,  ")
            .Append("       EndDate, ")
            .Append("       ActualScore, ")
            .Append("       TestTaken, ")
            .Append("       Comments, ")
            .Append("       Case when OverRide>=1 then 'True' else 'False' end as OverRide, ")
            .Append("       Case when (select count(*) from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') >=1 then ")
            .Append("       (select MinScore from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') ")
            .Append("       else MinScore end as MinScore, ")
            .Append("       Required, ")
            .Append("       DocSubmittedCount, ")
            .Append("       DocStatusDescrip, ")
            .Append("       Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount ")
            .Append("       from  ")
            .Append("       ( ")
            ' Get Requirement Group and requirements of mandatory requirement
            .Append("       	select  ")
            .Append("			distinct ")
            .Append("		       	adReqId, ")
            .Append("		      		Descrip, ")
            .Append("		      		ReqGrpId, ")
            .Append("		      		StartDate, ")
            .Append("		      		EndDate, ")
            .Append("		      		ActualScore, ")
            .Append("		      		TestTaken, ")
            .Append("		      		Comments, ")
            .Append("		      		OverRide, ")
            .Append("		      		Minscore, ")
            .Append("		      		Required, ")
            .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
            .Append("		      		DocSubmittedCount, ")
            .Append("		      		DocStatusDescrip ")
            .Append("      	from  ")
            .Append("      		( ")
            .Append("      		select Distinct ")
            .Append("      				t1.adReqId, ")
            .Append("      				t1.Descrip, ")
            .Append("      				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
            .Append("      					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
            .Append("      					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
            .Append("      				'" & strEnrollDate & "' as CurrentDate, ")
            .Append("      				t2.StartDate, ")
            .Append("      				t2.EndDate, ")
            .Append("      				(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("      					LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append("      				(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("      					LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append("      				(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("      					LeadId = '" & LeadId & "' ) as Comments, ")
            .Append("      				       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("      				        LeadId = '" & LeadId & "' ) as override, ")
            .Append("      				(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            .Append("      					LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append("      				t2.Minscore, ")
            .Append("      				1 as Required, ")
            .Append("      				(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("      				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("      				s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("      				from  ")
            .Append("      				adReqs t1, ")
            .Append("      				adReqsEffectiveDates t2 ")
            .Append("      				where  ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" t1.reqforEnrollment=1 and ")
            .Append("      				t1.adReqId = t2.adReqId and ")
            .Append("       			t2.MandatoryRequirement=1 and ")
            .Append("      				t1.adReqTypeId in (1,3) ")
            .Append("       				) ")
            .Append("       				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("       		union ")

            ' Get The Requirements that was assigned to a program version
            .Append("       	select  ")
            .Append("			distinct ")
            .Append("		       	adReqId, ")
            .Append("		      		Descrip, ")
            .Append("		      		ReqGrpId, ")
            .Append("		      		StartDate, ")
            .Append("		      		EndDate, ")
            .Append("		      		ActualScore, ")
            .Append("		      		TestTaken, ")
            .Append("		      		Comments, ")
            .Append("		      		OverRide, ")
            .Append("		      		Minscore, ")
            .Append("		      		Required, ")
            .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
            .Append("		      		DocSubmittedCount, ")
            .Append("		      		DocStatusDescrip ")
            .Append("      				from  ")
            .Append("      					( ")
            .Append("      						select Distinct ")
            .Append("      							t1.adReqId, ")
            .Append("      							t1.Descrip, ")
            .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
            .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
            .Append("	      						t2.StartDate, ")
            .Append("	      						t2.EndDate, ")
            .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	      							LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("	      							LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("	      							LeadId = '" & LeadId & "' ) as Comments, ")
            .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("	      						        LeadId = '" & LeadId & "' ) ")
            .Append("	      						as override, ")
            .Append("	      						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            .Append("	      						LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append("		      					t2.Minscore, ")
            .Append("	      						1 as Required, ")
            .Append("	      						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("      							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("      							s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("      						from  ")
            .Append("      							adReqs t1, ")
            .Append("      							adReqsEffectiveDates t2, ")
            .Append("      							adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
            .Append("      						where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" t1.reqforEnrollment=1 and ")
            .Append("      							t1.adReqId = t2.adReqId and t2.MandatoryRequirement <> 1 and ")
            .Append("      							t1.adreqTypeId in (1,3) and ")
            .Append("      							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId ")
            .Append("      							and t1.adReqId = t5.adReqId and t5.PrgVerId = '" & PrgVerId & "'     ")
            ''Commented for DE5552
            '.Append("      					      and		t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where ")
            '.Append("	      						LeadId = '" & LeadId & "' ) ")
            .Append("      							and t5.adReqId is not null ")
            .Append("      						) ")
            .Append("      						R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" union ")
            ' Get Requirements === Requirement Group assigned to Program Version
            .Append("       	select  ")
            .Append("			distinct ")
            .Append("		       	adReqId, ")
            .Append("		      		Descrip, ")
            .Append("		      		ReqGrpId, ")
            .Append("		      		StartDate, ")
            .Append("		      		EndDate, ")
            .Append("		      		ActualScore, ")
            .Append("		      		TestTaken, ")
            .Append("		      		Comments, ")
            .Append("		      		OverRide, ")
            .Append("		      		Minscore, ")
            .Append("		      		Required, ")
            .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
            .Append("		      		DocSubmittedCount, ")
            .Append("		      		DocStatusDescrip ")
            .Append("      				from  ")
            .Append("      					( ")
            .Append("      						select Distinct ")
            .Append("      							t1.adReqId, ")
            .Append("      							t1.Descrip, ")
            .Append("	      						t6.ReqGrpId as reqGrpId, ")
            .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
            .Append("	      						t2.StartDate, ")
            .Append("	      						t2.EndDate, ")
            .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	      							LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("	      							LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("	      							LeadId = '" & LeadId & "' ) as Comments, ")
            .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("	      						        LeadId = '" & LeadId & "' ) ")
            .Append("	      						as override, ")
            .Append("	      						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            .Append("	      						LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append("		      					t2.Minscore, ")
            .Append("	      						ISNULL(t3.IsRequired,0) as Required, ")
            .Append("	      						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("      							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("      							s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("      						from  ")
            .Append("      							adReqs t1, ")
            .Append("							adReqsEffectiveDates t2, ")
            .Append("							adReqLeadGroups t3, ")
            .Append("							adPrgVerTestDetails t5, ")
            .Append("							adReqGrpDef t6, ")
            .Append("						adLeadByLeadGroups t7  ")
            .Append("      					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" t1.reqforEnrollment=1 and ")
            .Append("      						t1.adReqId = t2.adReqId and ")
            .Append("      						t1.adreqTypeId in (1,3) and ")
            .Append("      						t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and ")
            .Append("						t3.LeadGrpId = t7.LeadGrpId and ")
            .Append("      						t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
            .Append("	      					and t7.LeadId = '" & LeadId & "'  and  ")
            .Append("						t5.PrgVerId = '" & PrgVerId & "'   ")
            .Append("      		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" union ")
            ' Get Requirements that were assigned to lead group but not part of any requirement group
            .Append("	     select  distinct ")
            .Append("	           adReqId, ")
            .Append("	           Descrip, ")
            .Append("	           ReqGrpId, ")
            .Append("	           StartDate, ")
            .Append("	           EndDate, ")
            .Append("	           ActualScore, ")
            .Append("	           TestTaken, ")
            .Append("	           Comments, ")
            .Append("	           OverRide, ")
            .Append("	           Minscore, ")
            .Append("	           Required, ")
            .Append("	           Case  when ActualScore >= Minscore  then 'True' else 'False' End as Pass, ")
            .Append("	           DocSubmittedCount, ")
            .Append("	           DocStatusDescrip ")
            .Append("           from  ")
            .Append("           ( ")
            ' Get Requirement Group and requirements that are assigned to a lead group
            ' and part of a requirement group
            .Append("          	 select  distinct ")
            .Append("	           	t1.adReqId, ")
            .Append("	           	t1.Descrip, ")
            .Append("	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            .Append("	           	'" & strEnrollDate & " ' as CurrentDate,  ")
            .Append("	           	t2.StartDate, ")
            .Append("	           	t2.EndDate,  ")
            .Append("	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId = '" & LeadId & "' ) as ActualScore,  ")
            .Append("	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId = '" & LeadId & "' ) as TestTaken,  ")
            .Append("	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId = '" & LeadId & "' ) as Comments,  ")
            .Append("	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("	                              LeadId = '" & LeadId & "' ) ")
            .Append("	                      as override, ")
            .Append("	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
            .Append("	           		LeadId = '" & LeadId & "' ) as DocSubmittedCount, ")
            .Append("	           	t2.Minscore, ")
            .Append("			ISNULL(t3.IsRequired,0) as Required, ")
            .Append("	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("	           s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("           from ")
            .Append("           	adReqs t1, ")
            .Append("           	adReqsEffectiveDates t2, ")
            .Append("		adReqLeadGroups t3 ")
            .Append("           where  ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" t1.reqforEnrollment=1 and ")
            .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId ")
            .Append("		and ")
            .Append("		t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId='" & PrgVerId & "' and s2.adReqId is null) ")
            .Append("		and t3.LeadGrpId in ")
            .Append("		(select LeadGrpId from adLeadByLeadGroups where LeadId = '" & LeadId & "' ) and ")
            .Append("           	t1.adReqTypeId in (1,3) and t2.MandatoryRequirement <> 1 and ")
            .Append("		t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId='" & PrgVerId & "') ")
            .Append("           ) ")
            .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" ) R2 ")
        End With



        '' Grab the Categories and Products table
        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        dadNorthwind = New OleDbDataAdapter(sb.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "Parent")

        dadNorthwind.SelectCommand = New OleDbCommand(sb1.ToString, conNorthwind)


        dadNorthwind.Fill(dstNorthwind, "Child")
        conNorthwind.Close()

        '' Add Parent/Child Relationship
        dstNorthwind.Relations.Add(
         "myrelation",
         dstNorthwind.Tables("Parent").Columns("ReqGrpId"),
         dstNorthwind.Tables("Child").Columns("ReqGrpId"))


        ''Display each Skill Group and Child Skills for the Student
        s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
        For Each drowParent In dstNorthwind.Tables("Parent").Rows
            If Not drowParent("Descrip") Is DBNull.Value Then
                s3 &= "<TR>"
                s3 &= "<td nowrap><font face=""verdana"" size=""2""><strong>" & drowParent("Descrip") & "</strong>&nbsp;</font>"
                If Not drowParent("NumReqs") Is DBNull.Value Then
                    If drowParent("NumReqs") >= 1 Then
                        s3 &= "<span style=""font: normal 12px arial; padding: 16px"">[Min Req:" & drowParent("NumReqs") & "]"
                        s3 &= "[Min Attempted:" & drowParent("AttemptedReqs") & "]</span>"
                    End If
                End If
                s3 &= "</td></tr>"
            Else
                s3 &= "<TR><td><font face=""verdana"" size=""2""><strong>Miscellaneous</strong></font></td></tr>"
            End If
            For Each drowChild In drowParent.GetChildRows("myrelation")
                If Not drowChild("Descrip") Is DBNull.Value Then
                    s3 &= "<tr>"
                    If Not drowChild("TestTakenCount") Is DBNull.Value And drowChild("TestTakenCount") >= 1 Then
                        s3 &= "<td style=""font-size: 12px""><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
                    ElseIf Not drowChild("TestTakenCount") Is DBNull.Value And drowChild("DocSubmittedCount") >= 1 Then
                        s3 &= "<td style=""font-size: 12px""><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
                    Else
                        s3 &= "<td style=""font-size: 12px""><ul><li><input type=""checkbox"" id=""chkTaken"" disabled></li>"
                    End If
                    s3 &= "<span style=""font:  normal 12px arial; padding: 16px"">" & drowChild("Descrip")
                    If drowChild("Required") = 1 Then
                        s3 &= "<span style=""color:red; padding-left: 3px;font: normal 12px arial;""><strong>(Required)</strong></span>"
                    End If

                    'Check If the Lead Has Passed The Test
                    If Not drowChild("ActualScore") Is DBNull.Value Then
                        If drowChild("MinScore") Is DBNull.Value Then
                            s3 &= ""
                        Else
                            If drowChild("ActualScore") >= 1 Then
                                If (drowChild("ActualScore") < drowChild("MinScore")) And (Not drowChild("TestTaken") = "") Then
                                    s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Fail)</strong></span>"
                                ElseIf (drowChild("ActualScore") >= drowChild("MinScore")) And (Not drowChild("TestTaken") = "") Then
                                    s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Pass)</strong></span>"
                                End If
                            End If
                        End If
                    End If

                    'Check If Document was Approved
                    If Not drowChild("DocStatusDescrip") Is DBNull.Value Then
                        If drowChild("DocStatusDescrip") = "Approved" Then
                            s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Approved)</strong></span>"
                        Else
                            s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Not Approved)</strong></span>"
                        End If
                    End If


                    If Not drowChild("OverRide") Is DBNull.Value Then
                        If drowChild("OverRide") = "True" Then
                            s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Overriden)</strong></span>"
                        End If
                    End If

                    s3 &= "</span></ul></td></tr>"
                End If
            Next
            s3 &= "<tr height=10><td>&nbsp;</td></tr>"
            forCounter = 1
        Next
        s3 &= "</table>"

        If forCounter = 1 Then
            Return s3.ToString()
        Else
            Return "None"
        End If
    End Function

    Public Function GetGridRequirementDetailsByEffectiveDates(ByVal LeadId As String, ByVal PrgVerId As String, Optional ByVal CampusId As String = "") As DataSet
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strEnrolldate As Date = Date.Now.ToShortDateString

        Dim dsGetCmpGrps As DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        ' Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        With sb
            .Append(" select  distinct ")
            .Append("       adReqId, ")
            .Append("       Descrip, ")
            .Append("       ReqGrpId, ")
            .Append("       StartDate,  ")
            .Append("       EndDate, ")
            .Append("       ActualScore, ")
            .Append("       TestTaken, ")
            .Append("       Comments, ")
            .Append("       Case when OverRide>=1 then 'True' else 'False' end as OverRide, ")
            .Append("       Case when (select count(*) from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') >=1 then ")
            .Append("       (select MinScore from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') ")
            .Append("       else MinScore end as MinScore, ")
            .Append("       Case when (Required=1) then 1 else ")
            .Append("       (select  Top 1 C.IsRequired as Required from adReqs A,adReqsEffectiveDates B,adReqLeadGroups C ")
            .Append("       where A.adReqId = B.adReqId and B.adReqEffectiveDateId = C.adReqEffectiveDateId and ")
            '' Document Tracking
            .Append("( A.reqforEnrollment=1 ) and ")

            .Append("       C.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') ")
            .Append("       and A.adReqTypeId in (1) and B.MandatoryRequirement <> 1 and A.adReqId=R2.adReqId ")
            .Append("       Order by Required Desc ")
            .Append("       ) end as Required, ")
            .Append("       DocSubmittedCount, ")
            .Append("       DocStatusDescrip, ")
            .Append("       Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount,Pass ")
            .Append("       from  ")
            .Append("       ( ")
            ' Get Requirement Group and requirements of mandatory requirement
            .Append("       	select  ")
            .Append("			distinct ")
            .Append("		       	adReqId, ")
            .Append("		      		Descrip, ")
            .Append("		      		ReqGrpId, ")
            .Append("		      		StartDate, ")
            .Append("		      		EndDate, ")
            .Append("		      		ActualScore, ")
            .Append("		      		TestTaken, ")
            .Append("		      		Comments, ")
            .Append("		      		OverRide, ")
            .Append("		      		Minscore, ")
            .Append("		      		Required, ")
            .Append("		      		NotRequired, ")
            .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
            .Append("		      		DocSubmittedCount, ")
            .Append("		      		DocStatusDescrip,CampGrpId ")
            .Append("      	from  ")
            .Append("      		( ")
            .Append("      		select Distinct ")
            .Append("      				t1.adReqId, ")
            .Append("      				t1.Descrip, ")
            .Append("      				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
            .Append("      					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
            .Append("      					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
            .Append("      				'" & strEnrolldate & "' as CurrentDate, ")
            .Append("      				t2.StartDate, ")
            .Append("      				t2.EndDate, ")
            .Append("      				(select Top 1 ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("      					LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append("      				(select Top 1 TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("      					LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append("      				(select Top 1 Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("      					LeadId = '" & LeadId & "' ) as Comments, ")
            .Append("      				(select Top 1 OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("      				        LeadId = '" & LeadId & "' ) as override, ")
            .Append("      				(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            .Append("      					LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
            ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
            ''.Append("      				cast(t2.Minscore as int) as MinScore, ")
            .Append("      				t2.Minscore , ")
            ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
            .Append("      				1 as Required, ")
            .Append("                   0 as NotRequired, ")
            .Append("      				(select Top 1  s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("      				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("      				s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
            .Append("      				from  ")
            .Append("      				adReqs t1, ")
            .Append("      				adReqsEffectiveDates t2 ")
            .Append("      				where  ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" ( t1.reqforEnrollment=1 ) and ")
            .Append("      				t1.adReqId = t2.adReqId and ")
            .Append("       				t2.MandatoryRequirement=1 and ")
            .Append("      				t1.adReqTypeId in (1) and t1.StatusId='" & strActiveGUID & "' ")
            .Append("       				) ")
            .Append("       				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("       		union ")

            ' Begin Get Requeriments assigned to Program Version
            .Append("	SELECT  DISTINCT ")
            .Append("	 adReqId")
            .Append("	, Descrip")
            .Append("	, ReqGrpId")
            .Append("	, StartDate")
            .Append("	, EndDate")
            .Append("	, ActualScore, TestTaken, Comments, OverRide")
            .Append("	, Minscore")
            .Append("	, Required, NotRequired")
            .Append("	, CASE  when ActualScore >= Minscore then 'True' else 'False' End as Pass")
            .Append("	, DocSubmittedCount")
            .Append("	,  DocStatusDescrip")
            .Append("	, CampGrpId")
            .Append("	FROM")
            .Append("	 (SELECT distinct ")
            .Append("		t1.adReqId AS adReqId")
            .Append("		,Descrip")
            .Append("		,adReqTypeId")
            .Append("		, 1 as Required")
            .Append("		, 0 as NotRequired")
            .Append("		, t2.StartDate AS StartDate")
            .Append("		, t2.EndDate AS EndDate")
            .Append("		, t1.CampGrpId AS CampGrpId")
            .Append("		,  '00000000-0000-0000-0000-000000000000' as ReqGrpId")
            .Append("		,  (select Distinct Top 1 ActualScore FROM adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = @leadId ) as ActualScore")
            .Append("		,  (select Distinct Top 1 TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = @leadId  ) as TestTaken")
            .Append("		,  (select Distinct Top 1 Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = @leadId  ) as Comments")
            .Append("		,  (select Distinct Top 1 OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and LeadId = @leadId  ) as override")
            .Append("		,  (select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and LeadId = @leadId  ) as DocSubmittedCount")
            .Append("		,  t2.Minscore")
            .Append("		,  (select Distinct Top 1 s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3  ")
            .Append("				WHERE  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and  s3.LeadId = @leadId ")
            .Append("				and s3.DocumentId = t1.adReqId) as DocStatusDescrip")
            .Append("	FROM  (select Distinct adReqId,Descrip,adReqTypeId,CampGrpId ")
            .Append("	   FROM adReqs ")
            .Append("	   WHERE adReqTypeId in (1) ")
            .Append("			AND ReqforEnrollment=1  ")
            .Append("			AND StatusId= @strActiveGUID) t1")
            .Append("	,  (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId ")
            .Append("			FROM adReqsEffectiveDates ")
            .Append("			WHERE MandatoryRequirement <> 1 ")
            .Append("				AND getdate()>=StartDate ")
            .Append("				AND (getdate()<= EndDate or EndDate is NULL)) t2")
            .Append("	,  (select Distinct adReqId from adPrgVerTestDetails  WHERE PrgVerId = @PrgVerId  and ReqGrpId is null) t5  ")
            .Append(" WHERE t1.adReqId = t2.adReqId and t1.adReqId=t5.adReqId ) AS TestProgram")

            ' End Get Requeriments Test assigned to Program Version.
            .Append(" union ")
            ' Get Requirements === Requirement Group assigned to Program Version
            .Append("       	select  ")
            .Append("			distinct ")
            .Append("		       	adReqId, ")
            .Append("		      		Descrip, ")
            .Append("		      		ReqGrpId, ")
            .Append("		      		StartDate, ")
            .Append("		      		EndDate, ")
            .Append("		      		ActualScore, ")
            .Append("		      		TestTaken, ")
            .Append("		      		Comments, ")
            .Append("		      		OverRide, ")
            .Append("		      		Minscore, ")
            .Append("		      		Required, ")
            .Append("	                NotRequired, ")
            .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
            .Append("		      		DocSubmittedCount, ")
            .Append("		      		DocStatusDescrip,CampGrpId ")
            .Append("      				from  ")
            .Append("      					( ")
            .Append("      						select Distinct ")
            .Append("      							t1.adReqId, ")
            .Append("      							t1.Descrip, ")
            .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
            .Append("	      						'" & strEnrolldate & "' as CurrentDate, ")
            .Append("	      						t2.StartDate, ")
            .Append("	      						t2.EndDate, ")
            .Append("	      						(select Top 1 ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	      							LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append("	      						(select Top 1 TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("	      							LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append("      						    (select Top 1 Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("	      							LeadId = '" & LeadId & "' ) as Comments, ")
            .Append("	      					    (select Top 1 OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("	      						        LeadId = '" & LeadId & "' ) ")
            .Append("	      						as override, ")
            .Append("	      						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            .Append("	      						LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append("      				            t2.Minscore , ")
            .Append("	      						ISNULL(t6.IsRequired,0) as Required, ")


            .Append("                   0 as NotRequired, ")
            .Append("	      						(select Distinct Top 1 s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("      							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("      							s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
            .Append("      						from  ")
            .Append("      							adReqs t1, ")
            .Append("							adReqsEffectiveDates t2, ")
            .Append("							adReqLeadGroups t3, ")
            .Append("							adPrgVerTestDetails t5, ")
            .Append("							adReqGrpDef t6, ")
            .Append("						adLeadByLeadGroups t7  ")
            .Append("      					where ")
            '' Document Tracking
            .Append("( t1.reqforEnrollment=1 ) and ")
            .Append("      						t1.adReqId = t2.adReqId and ")
            .Append("      						t1.adreqTypeId in (1) and  t1.StatusId='" & strActiveGUID & "'")
            .Append("      						and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and ")
            .Append("						t3.LeadGrpId = t7.LeadGrpId and ")
            .Append("      						t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
            .Append("	      					and t7.LeadId = '" & LeadId & "'  and  ")
            .Append("						t5.PrgVerId = '" & PrgVerId & "'   ")
            .Append("      		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" union ")
            ' Get Requirements that were assigned to lead group but not part of any requirement group
            .Append("	     select  distinct ")
            .Append("	           adReqId, ")
            .Append("	           Descrip, ")
            .Append("	           ReqGrpId, ")
            .Append("	           StartDate, ")
            .Append("	           EndDate, ")
            .Append("	           ActualScore, ")
            .Append("	           TestTaken, ")
            .Append("	           Comments, ")
            .Append("	           OverRide, ")
            .Append("	           Minscore, ")
            .Append("	           Required, ")
            .Append("	           NotRequired, ")
            .Append("	           Case  when ActualScore >= Minscore  then 'True' else 'False' End as Pass, ")
            .Append("	           DocSubmittedCount, ")
            .Append("	           DocStatusDescrip,CampGrpId ")
            .Append("           from  ")
            .Append("           ( ")
            ' Get Requirement Group and requirements that are assigned to a lead group
            ' and part of a requirement group
            .Append("          	 select  distinct ")
            .Append("	           	t1.adReqId, ")
            .Append("	           	t1.Descrip, ")
            .Append("	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            .Append("	           	'" & strEnrolldate & " ' as CurrentDate,  ")
            .Append("	           	t2.StartDate, ")
            .Append("	           	t2.EndDate,  ")
            .Append("	           	(select Top 1 ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId = '" & LeadId & "' ) as ActualScore,  ")
            .Append("	           	(select Top 1 TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId = '" & LeadId & "' ) as TestTaken,  ")
            .Append("	           	(select Top 1 Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId = '" & LeadId & "' ) as Comments,  ")
            .Append("	            (select Top 1 OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("	                              LeadId = '" & LeadId & "' ) ")
            .Append("	                      as override, ")
            .Append("	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
            .Append("	           		LeadId = '" & LeadId & "' ) as DocSubmittedCount, ")
            .Append("      				t2.Minscore , ")
            .Append("	      						0 as Required, ")
            .Append("                   0 as NotRequired, ")
            .Append("	           (select Distinct Top 1 s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("	           s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
            .Append("           from ")
            .Append("           	adReqs t1, ")
            .Append("           	adReqsEffectiveDates t2, ")
            .Append("		adReqLeadGroups t3 ")
            .Append("           where  ")
            ''Document Tracking
            .Append("( t1.reqforEnrollment=1 ) and ")
            .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId ")
            .Append("		and t1.StatusId='" & strActiveGUID & "' and ")
            .Append("		t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId='" & PrgVerId & "' and s2.adReqId is null) ")
            .Append("		and t3.LeadGrpId in ")
            .Append("		(select LeadGrpId from adLeadByLeadGroups where LeadId = '" & LeadId & "' ) and ")
            .Append("           	t1.adReqTypeId in (1) and t2.MandatoryRequirement <> 1 and ")
            .Append("		t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId='" & PrgVerId & "') ")
            .Append("           ) ")
            .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" ) R2  ")
            If Not strCampGrpId = "" Then
                .Append("  where R2.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" order by Descrip ")
        End With

        Dim conn As New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString"))
        Dim sc1 As New SqlCommand(sb.ToString(), conn)
        sc1.Parameters.AddWithValue("@leadId", LeadId)
        sc1.Parameters.AddWithValue("@PrgVerId", PrgVerId)
        sc1.Parameters.AddWithValue("@strActiveGUID", strActiveGUID)
        Dim da1 As New SqlDataAdapter(sc1)
        conn.Open()
        Try
            'Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Finally
            conn.Close()
        End Try
    End Function
    Public Function GetAllStandardRequirementsByEffectiveDates(ByVal LeadId As String, Optional ByVal CampusId As String = "") As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strPreviousEducationId As String
        Dim strPreviousEduTest As String
        Dim strEnrollDate As Date = Date.Now.ToShortDateString

        Dim dsGetCmpGrps As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        Try
            With sb
                .Append(" select ")
                .Append("           adReqId, ")
                .Append("           Descrip, ")
                .Append("           ReqGrpId, ")
                .Append("           StartDate, ")
                .Append("           EndDate, ")
                .Append("           ActualScore,  ")
                .Append("           TestTaken, ")
                .Append("           Comments, ")
                .Append("           Case when OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append("           Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount, ")
                .Append("           DocSubmittedCount, ")
                .Append("           Minscore, ")
                .Append("           Case when Required is Null then 0 else Required end as Required,  ")
                .Append("		    Case when Pass=1 then 'True' else 'False' end as Pass, ")
                .Append("           DocStatusDescrip ")
                .Append("           from ")
                .Append("           ( ")
                .Append("           select  Distinct ")
                .Append("	           adReqId, ")
                .Append("	           Descrip, ")
                .Append("	           ReqGrpId, ")
                .Append("	           StartDate, ")
                .Append("	           EndDate, ")
                .Append("	           ActualScore, ")
                .Append("	           TestTaken, ")
                .Append("	           Comments, ")
                .Append("	           OverRide, ")
                .Append("	           Minscore, ")
                .Append("	           Required, ")
                .Append("	           Case  when ActualScore >= Minscore then 1 else 0 End as Pass, ")
                .Append("	           DocSubmittedCount, ")
                .Append("	           DocStatusDescrip,CampGrpId ")
                .Append("           from  ")
                .Append("           ( ")
                '  Get Requirement Group and requirements of mandatory requirement
                .Append("          	 select  Distinct ")
                .Append("	           	t1.adReqId, ")
                .Append("	           	t1.Descrip, ")
                .Append("               t1.StatusId, ")
                .Append("	           	case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("	           		(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("	           	else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId,  ")
                .Append("	           	'" & strEnrollDate & "' as CurrentDate,  ")
                .Append("	           	t2.StartDate, ")
                .Append("	           	t2.EndDate,  ")
                .Append("	           	(select Top 1 ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId='" & LeadId & "')  as ActualScore,  ")
                .Append("	           	(select Top 1 TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId='" & LeadId & "')  as TestTaken,  ")
                .Append("	           	(select top 1 Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId='" & LeadId & "')  as Comments,  ")
                .Append("	            (select Top 1 OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and  ")
                .Append("	                              LeadId='" & LeadId & "') ")
                .Append("	                      as override, ")
                .Append("	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
                .Append("	           		LeadId='" & LeadId & "')  as DocSubmittedCount, ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                ''.Append("      				cast(t2.Minscore as int) as MinScore, ")
                .Append("      				t2.Minscore , ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                .Append("	           	1 as Required, ")
                .Append("	           (select Distinct Top 1 s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("	           s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("           from ")
                .Append("           	adReqs t1, ")
                .Append("           	adReqsEffectiveDates t2 ")
                .Append("           where  ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" t1.reqforEnrollment=1 and ")
                .Append("           	t1.adReqId = t2.adReqId and  ")
                .Append("           	t2.MandatoryRequirement=1 and ")
                .Append("           	t1.adReqTypeId in (1) and t1.StatusId='" & strActiveGUID & "' ")
                .Append("           ) ")
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("                       and R1.StatusId='" + AdvantageCommonValues.ActiveGuid + "' ")
                .Append(" union ")
                '  Get Requirements that were assigned to lead group but not part of any requirement group
                .Append(" select ")
                .Append("           adReqId, ")
                .Append("           Descrip, ")
                .Append("           ReqGrpId, ")
                .Append("           StartDate, ")
                .Append("           EndDate, ")
                .Append("           ActualScore,  ")
                .Append("           TestTaken, ")
                .Append("           Comments, ")
                .Append("           OverRide, ")
                .Append("           Minscore, ")
                .Append("           Required, ")
                .Append("	        Case  when ActualScore >= Minscore then 1 else 0 End as Pass, ")
                .Append("           DocSubmittedCount, ")
                .Append("           DocStatusDescrip,CampGrpId ")
                .Append("           from ")
                .Append("           ( ")
                '  Get Requirement Group and requirements that are assigned to a lead group
                '  and part of a requirement group
                .Append("           	 select Distinct ")
                .Append(" 	           	t1.adReqId, ")
                .Append(" 	           	t1.Descrip, ")
                .Append("               t1.StatusId, ")
                .Append(" 	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append(" 	           	'" & strEnrollDate & "' as CurrentDate,  ")
                .Append(" 	           	t2.StartDate, ")
                .Append(" 	           	t2.EndDate,  ")
                .Append(" 	           	(select Top 1 ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append(" 	           		LeadId='" & LeadId & "')  as ActualScore,  ")
                .Append(" 	           	(select Top 1 TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append(" 	           		LeadId='" & LeadId & "')  as TestTaken,  ")
                .Append(" 	           	(select Top 1 Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append(" 	           		LeadId='" & LeadId & "')  as Comments,  ")
                .Append(" 	            (select Top 1 OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append(" 	                              LeadId='" & LeadId & "') ")
                .Append(" 	                      as override, ")
                .Append(" 	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
                .Append(" 	           		LeadId='" & LeadId & "')  as DocSubmittedCount, ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                ''.Append("      				cast(t2.Minscore as int) as MinScore, ")
                .Append("      				t2.Minscore , ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                '.Append(" 			t3.IsRequired as Required, ")
                .Append("           (  ")
                .Append("						select Top 1 C.IsRequired from adReqs A,adReqsEffectiveDates B,adReqLeadGroups C ")
                .Append("						where A.adReqId = B.adReqId and B.adReqEffectiveDateId = C.adReqEffectiveDateId and ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" A.reqforEnrollment=1 and ")
                .Append("						C.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') ")
                .Append("						and A.adReqTypeId in (1) and B.MandatoryRequirement <> 1  and A.adReqId=t1.adReqId ")
                .Append("						Order by C.IsRequired Desc ")
                .Append("					) Required, ")
                .Append(" 	           (select Distinct Top 1 s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append(" 	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append(" 	           s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("            from ")
                .Append("           	adReqs t1, ")
                .Append("           	adReqsEffectiveDates t2, ")
                .Append("		adReqLeadGroups t3 ")
                .Append("           where  ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" t1.reqforEnrollment=1 and ")
                .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t1.StatusId='" & strActiveGUID & "' ")
                ' and t1.adReqId not in (select Distinct adReqId from adReqGrpDef)
                .Append("		and t3.LeadGrpId in ")
                .Append("		(select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and ")
                .Append("           	t1.adReqTypeId in (1) and t2.MandatoryRequirement <> 1 ")
                .Append("           ) ")
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("                       and R1.StatusId='" + AdvantageCommonValues.ActiveGuid + "' ")
                .Append(" ) R2 ")
                If Not strCampGrpId = "" Then
                    .Append("  Where R2.CampGrpId in 	('")
                    .Append(strCampGrpId)
                    .Append(") ")
                End If
            End With
            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As Exception
        Finally
        End Try
    End Function


    ''' <summary>
    ''' Get all Docs Requeriments to Enroll a Lead.
    ''' </summary>
    ''' <param name="LeadId"></param>
    ''' <param name="PrgVerId"></param>
    ''' <param name="CampusId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetAllStandardDocumentsByEffectiveDatesAndPrgVersion(ByVal LeadId As String, ByVal PrgVerId As String, ByVal CampusId As String) As DataSet
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strEnrollDate As Date = Date.Now.ToShortDateString
        Dim dsGetCmpGrps As DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If


        'commented by balaji on 09/12/2005
        With sb
            .Append(" select  distinct ")
            .Append("       adReqId as DocumentId, ")
            .Append("       Descrip as DocumentDescrip, ")
            .Append("       ReqGrpId, ")
            .Append("       StartDate,  ")
            .Append("       EndDate, ")
            .Append("       ActualScore, ")
            .Append("       TestTaken, ")
            .Append("       Comments, ")
            .Append("       Case when OverRide>=1 then 'True' else 'False' end as OverRide, ")
            .Append("       Case when (select count(*) from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') >=1 then ")
            .Append("       (select MinScore from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') ")
            .Append("       else MinScore end as MinScore, ")
            .Append("       Required, ")
            .Append("       DocSubmittedCount, ")
            .Append("       case when DocStatusDescrip='Approved' then 'Approved' else 'Not Approved' end as DocStatusDescrip, ")
            .Append("       Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount,Pass,CampGrpId, ")
            .Append("       (select distinct ReceiveDate  from adLeadDocsReceived where DocumentId=adReqId and LeadId='" & LeadId & "') as DateReceived ")
            .Append("       from  ")
            .Append("       ( ")
            ' Get Requirement Group and requirements of mandatory requirement
            .Append("       	select  ")
            .Append("			distinct ")
            .Append("		       	adReqId, ")
            .Append("		      		Descrip, ")
            .Append("		      		ReqGrpId, ")
            .Append("		      		StartDate, ")
            .Append("		      		EndDate, ")
            .Append("		      		ActualScore, ")
            .Append("		      		TestTaken, ")
            .Append("		      		Comments, ")
            .Append("		      		OverRide, ")
            .Append("		      		Minscore, ")
            .Append("		      		Required, ")
            .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
            .Append("		      		DocSubmittedCount, ")
            .Append("		      		DocStatusDescrip,CampGrpId,COUNT(*) OVER (PARTITION BY adReqId) AS DupCount ")
            .Append("      	from  ")
            .Append("      		( ")
            .Append("      		select Distinct ")
            .Append("      				t1.adReqId, ")
            .Append("      				t1.Descrip, ")
            .Append("      				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
            .Append("      					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
            .Append("      					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
            .Append("      				'" & strEnrollDate & "' as CurrentDate, ")
            .Append("      				t2.StartDate, ")
            .Append("      				t2.EndDate, ")
            .Append("      				(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("      					LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append("      				(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("      					LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append("      				(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("      					LeadId = '" & LeadId & "' ) as Comments, ")
            .Append("      				       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("      				        LeadId = '" & LeadId & "' ) as override, ")
            .Append("      				(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            .Append("      					LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append("      				t2.Minscore, ")
            .Append("      				1 as Required, ")
            .Append("      				(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("      				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("      				s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
            .Append("      				from  ")
            .Append("      				adReqs t1, ")
            .Append("      				adReqsEffectiveDates t2 ")
            .Append("      				where  ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" t1.reqforEnrollment=1 and ")
            .Append("      				t1.adReqId = t2.adReqId and ")
            .Append("       				t2.MandatoryRequirement=1 and ")
            .Append("      				t1.adReqTypeId in (3) and t1.StatusId='" & strActiveGUID & "' ")
            .Append("       				) ")
            .Append("       				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("       		union ")

            ' Get The Requirements that was assigned to a program version


            .Append("       	select  ")
            .Append("			distinct ")
            .Append("		       	adReqId, ")
            .Append("		      		Descrip, ")
            .Append("		      		ReqGrpId, ")
            .Append("		      		StartDate, ")
            .Append("		      		EndDate, ")
            .Append("		      		ActualScore, ")
            .Append("		      		TestTaken, ")
            .Append("		      		Comments, ")
            .Append("		      		OverRide, ")
            .Append("		      		Minscore, ")
            .Append("		      		Required, ")
            .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
            .Append("		      		DocSubmittedCount, ")
            .Append("		      		DocStatusDescrip,CampGrpId,COUNT(*) OVER (PARTITION BY adReqId) AS DupCount ")
            .Append("      				from  ")
            .Append("      					( ")

            .Append("	SELECT Distinct  ")
            .Append("			t1.adReqId")
            .Append("			, t1.Descrip, '00000000-0000-0000-0000-000000000000' as reqGrpId")
            .Append("			, @strEnrollDate as CurrentDate")
            .Append("	       , 	dates.StartDate, dates.EndDate")
            .Append("		   , 		(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId =  @leadId  ) as ActualScore")
            .Append("		   , 	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId =  @leadId  ) as TestTaken")
            .Append("		   ,  (select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId =  @leadId  ) as Comments")
            .Append("		   ,  (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and   LeadId = @leadId  ) AS override")
            .Append("		   , (select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and 	LeadId =  @leadId   ) as DocSubmittedCount")
            .Append("		   , det.Minscore")
            .Append("		   , 1 AS Required")
            .Append("		   , (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("				WHERE  s1.SysDocStatusId = s2.sysDocStatusId ")
            .Append("				and s2.DocStatusId = s3.DocStatusId ")
            .Append("				and  s3.LeadId = 'd0cf3ad1-419f-4fc0-bf1c-ddc77baf9895' and s3.DocumentId = t1.adReqId) as DocStatusDescrip")
            .Append("		   ,t1.CampGrpId")
            .Append("	from  	adReqs t1")
            .Append("	JOIN adPrgVerTestDetails det ON det.adReqId = t1.adReqId")
            .Append("	JOIN adReqsEffectiveDates dates ON dates.adReqId = t1.adReqId")
            .Append("	where t1.reqforEnrollment=1 ")
            .Append("	AND MandatoryRequirement <> 1")
            .Append("	AND t1.adreqTypeId = 3")
            .Append("	AND t1.StatusId= @strActiveGUID")
            .Append("	AND det.PrgVerId = @PrgVerId ")
            .Append("	AND det.ReqGrpId is null ")



            '.Append("      						select Distinct ")
            '.Append("      							t1.adReqId, ")
            '.Append("      							t1.Descrip, ")
            '.Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
            '.Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
            '.Append("	      						t2.StartDate, ")
            '.Append("	      						t2.EndDate, ")
            '.Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '.Append("	      							LeadId = '" & LeadId & "' ) as ActualScore, ")
            '.Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            '.Append("	      							LeadId = '" & LeadId & "' ) as TestTaken, ")
            '.Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            '.Append("	      							LeadId = '" & LeadId & "' ) as Comments, ")
            '.Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            '.Append("	      						        LeadId = '" & LeadId & "' ) ")
            '.Append("	      						as override, ")
            '.Append("	      						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            '.Append("	      						LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
            '.Append("		      					t2.Minscore, ")
            '.Append("	      						1 as Required, ")
            '.Append("	      						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            '.Append("      							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            '.Append("      							s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
            '.Append("      						from  ")
            '.Append("      							adReqs t1, ")
            '.Append("      							adReqsEffectiveDates t2, ")
            '.Append("      							adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
            '.Append("      						where ")
            ' ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            '.Append(" t1.reqforEnrollment=1 and ")
            '.Append("      							t1.adReqId = t2.adReqId and t2.MandatoryRequirement <> 1 and ")
            '.Append("      							t1.adreqTypeId in (3) and ")
            '.Append("      							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId ")
            '.Append("      							and t1.adReqId = t5.adReqId and t5.PrgVerId = '" & PrgVerId & "'     ")
            '' .Append("      					    and  		t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where ")
            ''.Append("	      						LeadId = '" & LeadId & "' ) ")
            '.Append("      							and t5.adReqId is not null and t1.StatusId='" & strActiveGUID & "' ")




            .Append("      						) ")
            .Append("      						R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" union ")
            ' Get Requirements === Requirement Group assigned to Program Version



            .Append("       	select  ")
            .Append("			distinct ")
            .Append("		       	adReqId, ")
            .Append("		      		Descrip, ")
            .Append("		      		ReqGrpId, ")
            .Append("		      		StartDate, ")
            .Append("		      		EndDate, ")
            .Append("		      		ActualScore, ")
            .Append("		      		TestTaken, ")
            .Append("		      		Comments, ")
            .Append("		      		OverRide, ")
            .Append("		      		Minscore, ")
            .Append("		      		Required, ")
            .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
            .Append("		      		DocSubmittedCount, ")
            .Append("		      		DocStatusDescrip,CampGrpId,COUNT(*) OVER (PARTITION BY adReqId) AS DupCount ")
            .Append("      				from  ")
            .Append("      					( ")
            .Append("      						select Distinct ")
            .Append("      							t1.adReqId, ")
            .Append("      							t1.Descrip, ")
            .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
            .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
            .Append("	      						t2.StartDate, ")
            .Append("	      						t2.EndDate, ")
            .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	      							LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("	      							LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("	      							LeadId = '" & LeadId & "' ) as Comments, ")
            .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("	      						        LeadId = '" & LeadId & "' ) ")
            .Append("	      						as override, ")
            .Append("	      						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            .Append("	      						LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append("		      					t2.Minscore, ")
            ''Modified by SAraswathi lakshmanan on Jan 19 2011
            ''To fix rally case DE1231 QA: Issues with the entrance test and documents section when associated with a requirements group on the leads and students side.

            .Append("	      						1 as Required, ")
            ''.Append("	      						IsNUll(t6.IsRequired,0) as Required, ")
            .Append("	      						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("      							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("      							s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
            .Append("      						from  ")
            .Append("      							adReqs t1, ")
            .Append("							adReqsEffectiveDates t2, ")
            .Append("							adReqLeadGroups t3, ")
            .Append("							adPrgVerTestDetails t5, ")
            .Append("							adReqGrpDef t6 ")
            .Append("      					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append("t1.reqforEnrollment=1 and ")
            '.Append("      						t1.adReqId = t2.adReqId and ")
            '.Append("      						t1.adreqTypeId in (3) and ")
            '.Append("      						t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and ")
            '.Append("						t3.LeadGrpId = t7.LeadGrpId and ")
            '.Append("      						t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
            '.Append("	      					and t7.LeadId = '" & LeadId & "'  and  ")
            '.Append("						t5.PrgVerId = '" & PrgVerId & "'  and t1.StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' ")
            .Append("       t1.adReqId = t2.adReqId and t1.adreqTypeId in (3) and t1.StatusId='" & strActiveGUID & "' and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            .Append("       t5.PrgVerId = '" & PrgVerId & "' and t5.ReqGrpId is not null and t5.ReqGrpId = t6.ReqGrpId and t1.adReqId = t6.adReqId and ")
            ''     .Append("       t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') ")
            .Append("       t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')   AND t3.leadgrpID=t6.leadgrpId  ")
            .Append("      		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")








            .Append(" union ")
            ' Get Requirements that were assigned to lead group but not part of any requirement group
            .Append("	     select  distinct ")
            .Append("	           adReqId, ")
            .Append("	           Descrip, ")
            .Append("	           ReqGrpId, ")
            .Append("	           StartDate, ")
            .Append("	           EndDate, ")
            .Append("	           ActualScore, ")
            .Append("	           TestTaken, ")
            .Append("	           Comments, ")
            .Append("	           OverRide, ")
            .Append("	           Minscore, ")
            .Append("	           Required, ")
            .Append("	           Case  when ActualScore >= Minscore  then 'True' else 'False' End as Pass, ")
            .Append("	           DocSubmittedCount, ")
            .Append("	           DocStatusDescrip,CampGrpId,COUNT(*) OVER (PARTITION BY adReqId) AS DupCount ")
            .Append("           from  ")
            .Append("           ( ")
            ' Get Requirement Group and requirements that are assigned to a lead group
            ' and part of a requirement group
            .Append("          	 select  distinct ")
            .Append("	           	t1.adReqId, ")
            .Append("	           	t1.Descrip, ")
            .Append("	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            .Append("	           	'" & strEnrollDate & " ' as CurrentDate,  ")
            .Append("	           	t2.StartDate, ")
            .Append("	           	t2.EndDate,  ")
            .Append("	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId = '" & LeadId & "' ) as ActualScore,  ")
            .Append("	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId = '" & LeadId & "' ) as TestTaken,  ")
            .Append("	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId = '" & LeadId & "' ) as Comments,  ")
            .Append("	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("	                              LeadId = '" & LeadId & "' ) ")
            .Append("	                      as override, ")
            .Append("	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
            .Append("	           		LeadId = '" & LeadId & "' ) as DocSubmittedCount, ")
            .Append("	           	t2.Minscore, ")
            .Append("			ISNULL(t3.IsRequired,0) as Required, ")
            .Append("	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("	           s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
            .Append("           from ")
            .Append("           	adReqs t1, ")
            .Append("           	adReqsEffectiveDates t2, ")
            .Append("		adReqLeadGroups t3 ")
            .Append("           where  ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" t1.reqforEnrollment=1 and ")
            .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t1.StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' ")
            .Append("		and ")
            .Append("		t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId='" & PrgVerId & "' and s2.adReqId is null) ")
            .Append("		and t3.LeadGrpId in ")
            .Append("		(select LeadGrpId from adLeadByLeadGroups where LeadId = '" & LeadId & "' ) and ")
            .Append("           	t1.adReqTypeId in (3) and t2.MandatoryRequirement <> 1 and ")
            .Append("		t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId='" & PrgVerId & "') ")
            .Append("           ) ")
            .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" ) R2 ")
            .Append(" Where ((DupCount >1 AND  REQUIRED=1) or (DupCount=1 AND REQUIRED IN (0,1))) ")
            If Not strCampGrpId = "" Then
                .Append("  and R2.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If

            .Append("  order by DocumentDescrip ")
        End With

        Dim conn As New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString"))
        Dim sc1 As New SqlCommand(sb.ToString(), conn)
        sc1.Parameters.AddWithValue("@leadId", LeadId)
        sc1.Parameters.AddWithValue("@PrgVerId", PrgVerId)
        sc1.Parameters.AddWithValue("@strActiveGUID", strActiveGUID)
        sc1.Parameters.AddWithValue("@strEnrollDate", strEnrollDate)
        Dim da1 As New SqlDataAdapter(sc1)
        conn.Open()
        Try
            'Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Finally
            conn.Close()
        End Try

        'Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(myAdvAppSettings.AppSettings("ConString")))
        'Dim da1 As New OleDbDataAdapter(sc1)
        'da1.Fill(ds, "TestDetails")
        'sb.Remove(0, sb.Length)
        'Return ds

    End Function
    Public Function GetAllStandardDocumentsByEffectiveDates(ByVal LeadId As String, ByVal campusId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strPreviousEducationId As String
        Dim strPreviousEduTest As String
        Dim StrEnrollDate As Date = Date.Now.ToShortDateString
        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        Try


            With sb
                .Append(" select ")
                .Append("           adReqId as  DocumentId, ")
                .Append("           Descrip as DocumentDescrip, ")
                .Append("           ReqGrpId, ")
                .Append("           StartDate, ")
                .Append("           EndDate, ")
                .Append("           ActualScore,  ")
                .Append("           TestTaken, ")
                .Append("           Comments, ")
                .Append("           Case when OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append("           Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount, ")
                .Append("           DocSubmittedCount, ")
                .Append("           Minscore, ")
                .Append("           Required, ")
                .Append("		    case when Pass=1 then 'True' else 'False' end as Pass, ")
                .Append("       case when DocStatusDescrip='Approved' then 'Approved' else 'Not Approved' end as DocStatusDescrip, ")
                .Append("       (select distinct ReceiveDate  from adLeadDocsReceived where DocumentId=adReqId and LeadId='" & LeadId & "') as DateReceived ")
                .Append("           from ")
                .Append("           ( ")
                .Append("           select  Distinct ")
                .Append("	           adReqId, ")
                .Append("	           Descrip, ")
                .Append("	           ReqGrpId, ")
                .Append("	           StartDate, ")
                .Append("	           EndDate, ")
                .Append("	           ActualScore, ")
                .Append("	           TestTaken, ")
                .Append("	           Comments, ")
                .Append("	           OverRide, ")
                .Append("	           Minscore, ")
                .Append("	           Required, ")
                .Append("	           Case  when ActualScore >= Minscore then 1 else 0 End as Pass, ")
                .Append("	           DocSubmittedCount, ")
                .Append("	           DocStatusDescrip,CampGrpId,COUNT(*) OVER ( PARTITION BY adReqId ) AS DupCount ")
                .Append("           from  ")
                .Append("           ( ")
                '  Get Requirement Group and requirements of mandatory requirement
                .Append("          	 select distinct ")
                .Append("	           	t1.adReqId, ")
                .Append("	           	t1.Descrip, ")
                .Append("	           	case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("	           		(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("	           	else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId,  ")
                .Append("	           	'" & StrEnrollDate & "' as CurrentDate,  ")
                .Append("	           	t2.StartDate, ")
                .Append("	           	t2.EndDate,  ")
                .Append("	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId='" & LeadId & "')  as ActualScore,  ")
                .Append("	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId='" & LeadId & "')  as TestTaken,  ")
                .Append("	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId='" & LeadId & "')  as Comments,  ")
                .Append("	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and  ")
                .Append("	                              LeadId='" & LeadId & "') ")
                .Append("	                      as override, ")
                .Append("	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
                .Append("	           		LeadId='" & LeadId & "')  as DocSubmittedCount, ")
                .Append("	           	t2.Minscore, ")
                .Append("	           	1 as Required, ")
                .Append("	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("	           s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("           from ")
                .Append("           	adReqs t1, ")
                .Append("           	adReqsEffectiveDates t2 ")
                .Append("           where  ")
                ' ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                '.Append(" t1.reqforEnrollment=1 and ")
                .Append("           	t1.adReqId = t2.adReqId and  t1.reqforEnrollment = 1 and ")
                .Append("           	t2.MandatoryRequirement=1 and ")
                .Append("           	t1.adReqTypeId in (3)  and t1.StatusId='" & strActiveGUID & "' ")
                .Append("           ) ")
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                '  Get Requirements that were assigned to lead group but not part of any requirement group
                .Append(" select ")
                .Append("           adReqId, ")
                .Append("           Descrip, ")
                .Append("           ReqGrpId, ")
                .Append("           StartDate, ")
                .Append("           EndDate, ")
                .Append("           ActualScore,  ")
                .Append("           TestTaken, ")
                .Append("           Comments, ")
                .Append("           OverRide, ")
                .Append("	           Minscore, ")
                .Append("	           Required, ")
                .Append("	           Case  when ActualScore >= Minscore then 1 else 0 End as Pass, ")
                .Append("	           DocSubmittedCount, ")
                .Append("	           DocStatusDescrip,CampGrpId,COUNT(*) OVER ( PARTITION BY adReqId ) AS DupCount ")
                .Append("           from ")
                .Append("           ( ")
                '  Get Requirement Group and requirements that are assigned to a lead group
                '  and part of a requirement group
                .Append("           	 select distinct  ")
                .Append(" 	           	t1.adReqId, ")
                .Append(" 	           	t1.Descrip, ")
                .Append(" 	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append(" 	           	'" & StrEnrollDate & "' as CurrentDate,  ")
                .Append(" 	           	t2.StartDate, ")
                .Append(" 	           	t2.EndDate,  ")
                .Append(" 	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append(" 	           		LeadId='" & LeadId & "')  as ActualScore,  ")
                .Append(" 	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append(" 	           		LeadId='" & LeadId & "')  as TestTaken,  ")
                .Append(" 	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append(" 	           		LeadId='" & LeadId & "')  as Comments,  ")
                .Append(" 	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append(" 	                              LeadId='" & LeadId & "') ")
                .Append(" 	                      as override, ")
                .Append(" 	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
                .Append(" 	           		LeadId='" & LeadId & "')  as DocSubmittedCount, ")
                .Append(" 	           	t2.Minscore, ")
                .Append(" 			ISNULL(t3.IsRequired,0) as Required, ")
                .Append(" 	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append(" 	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append(" 	           s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("            from ")
                .Append("           	adReqs t1, ")
                .Append("           	adReqsEffectiveDates t2, ")
                .Append("		adReqLeadGroups t3 ")
                .Append("           where  ")
                ' ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                '.Append(" t1.reqforEnrollment=1 and ")
                .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t1.reqforEnrollment = 1 ")
                ' and t1.adReqId not in (select Distinct adReqId from adReqGrpDef)
                .Append("		and t3.LeadGrpId in ")
                .Append("		(select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and ")
                .Append("           	t1.adReqTypeId in (3) and t2.MandatoryRequirement <> 1  and t1.StatusId='" & strActiveGUID & "' ")
                .Append("           ) ")
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" ) R2 ")
                .Append(" Where ((DupCount >1 AND  REQUIRED=1) or (DupCount=1 AND REQUIRED IN (0,1))) ")
                If Not strCampGrpId = "" Then
                    .Append("  and R2.CampGrpId in 	('")
                    .Append(strCampGrpId)
                    .Append(") ")
                End If
                .Append("  order by DocumentDescrip ")
            End With

            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As Exception
        Finally
        End Try
    End Function
    Public Function CheckIfLeadHasPassedRequiredTestWithProgramVersion(ByVal LeadId As String, ByVal PrgVerId As String, Optional ByVal CampusId As String = "") As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strPreviousEducationId As String
        Dim strPreviousEduTest As String
        Dim strEnrollDate As Date = Date.Now.ToShortDateString

        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        Try
            With sb
                .Append(" select OverRide,Count(*) as OverRideCount from ")
                .Append(" ( ")
                .Append(" select  distinct ")
                .Append("       adReqId, ")
                .Append("       Descrip, ")
                .Append("       ReqGrpId, ")
                .Append("       StartDate,  ")
                .Append("       EndDate, ")
                .Append("       ActualScore, ")
                .Append("       TestTaken, ")
                .Append("       Comments, ")
                .Append("       Case when OverRide>=1 then 'True' else 'False' end as OverRide, ")
                .Append("       Case when (select count(*) from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') >=1 then ")
                .Append("       (select MinScore from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') ")
                .Append("       else MinScore end as MinScore, ")
                .Append("       Required, ")
                .Append("       DocSubmittedCount, ")
                .Append("       DocStatusDescrip, ")
                .Append("       Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount,Pass,CampGrpId ")
                .Append("       from  ")
                .Append("       ( ")
                ' Get Requirement Group and requirements of mandatory requirement
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate, ")
                .Append("		      		ActualScore, ")
                .Append("		      		TestTaken, ")
                .Append("		      		Comments, ")
                .Append("		      		OverRide, ")
                .Append("		      		Minscore, ")
                .Append("		      		Required, ")
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		      		DocSubmittedCount, ")
                .Append("		      		DocStatusDescrip,CampGrpId ")
                .Append("      	from  ")
                .Append("      		( ")
                .Append("      		select Distinct ")
                .Append("      				t1.adReqId, ")
                .Append("      				t1.Descrip, ")
                .Append("      				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("      					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("      					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                .Append("      				'" & strEnrollDate & "' as CurrentDate, ")
                .Append("      				t2.StartDate, ")
                .Append("      				t2.EndDate, ")
                .Append("      				(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("      					LeadId = '" & LeadId & "' ) as ActualScore, ")
                .Append("      				(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("      					LeadId = '" & LeadId & "' ) as TestTaken, ")
                .Append("      				(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("      					LeadId = '" & LeadId & "' ) as Comments, ")
                .Append("      				       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("      				        LeadId = '" & LeadId & "' ) as override, ")
                .Append("      				(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("      					LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                ''.Append("      				Cast(t2.MinScore as int) as MinScore, ")
                .Append("      				t2.MinScore , ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                .Append("      				1 as Required, ")
                .Append("      				(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("      				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("      				s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("      				from  ")
                .Append("      				adReqs t1, ")
                .Append("      				adReqsEffectiveDates t2 ")
                .Append("      				where  ")
                .Append("      				t1.adReqId = t2.adReqId and ")
                .Append("       				t2.MandatoryRequirement=1 and ")
                ''Added by Saraswathi Lakshmanan on Sept 15 2010
                ''To filter only the documents marked as required for Enrollment
                ''For Document tracking
                .Append("                   t1.ReqforEnrollment=1 and ")
                .Append("      				t1.adReqTypeId in (1) and t1.StatusId='" & strActiveGUID & "' ")
                .Append("       				) ")
                .Append("       				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("       		union ")

                ' Get The Requirements that was assigned to a program version
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate, ")
                .Append("		      		ActualScore, ")
                .Append("		      		TestTaken, ")
                .Append("		      		Comments, ")
                .Append("		      		OverRide, ")
                .Append("		      		Minscore, ")
                .Append("		      		Required, ")
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		      		DocSubmittedCount, ")
                .Append("		      		DocStatusDescrip,CampGrpId ")
                .Append("      				from  ")
                .Append("      					( ")
                .Append("      						select Distinct ")
                .Append("      							t1.adReqId, ")
                .Append("      							t1.Descrip, ")
                .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
                .Append("	      						t2.StartDate, ")
                .Append("	      						t2.EndDate, ")
                .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	      							LeadId = '" & LeadId & "' ) as ActualScore, ")
                .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							LeadId = '" & LeadId & "' ) as TestTaken, ")
                .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							LeadId = '" & LeadId & "' ) as Comments, ")
                .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("	      						        LeadId = '" & LeadId & "' ) ")
                .Append("	      						as override, ")
                .Append("	      						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("	      						LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
                '.Append("		      					Cast(t2.MinScore as int) as MinScore, ")
                .Append(" Case when (select count(*) from adPrgVerTestDetails where adReqId=t1.adReqId and PrgVerId='" & PrgVerId & "') >=1 then ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                '.Append(" (select cast(Minscore as int) from adPrgVerTestDetails where adReqId=t1.adReqId and PrgVerId='" & PrgVerId & "') ")
                '.Append(" else cast(t2.Minscore as int) end as MinScore, ")
                .Append(" (select Minscore from adPrgVerTestDetails where adReqId=t1.adReqId and PrgVerId='" & PrgVerId & "') ")
                .Append(" else t2.Minscore end as MinScore, ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                .Append("	      						1 as Required, ")
                .Append("	      						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("      							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("      							s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("      						from  ")
                .Append("      							adReqs t1, ")
                .Append("      							adReqsEffectiveDates t2, ")
                .Append("      							adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
                .Append("      						where ")
                .Append("      							t1.adReqId = t2.adReqId and t2.MandatoryRequirement <> 1 and ")
                ''Added by Saraswathi Lakshmanan on Sept 15 2010
                ''To filter only the documents marked as required for Enrollment
                ''For Document tracking
                .Append("                               t1.ReqforEnrollment=1 and ")
                .Append("      							t1.adreqTypeId in (1) and  t1.StatusId='" & strActiveGUID & "'")
                .Append("      							and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId ")
                .Append("      							and t1.adReqId = t5.adReqId and t5.PrgVerId = '" & PrgVerId & "'     ")
                ''Commented for De5552
                '.Append("      					      	and	t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where ")
                '.Append("	      						LeadId = '" & LeadId & "' ) ")
                .Append("      							and t5.adReqId is not null ")
                .Append("      						) ")
                .Append("      						R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                ' Get Requirements === Requirement Group assigned to Program Version
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate, ")
                .Append("		      		ActualScore, ")
                .Append("		      		TestTaken, ")
                .Append("		      		Comments, ")
                .Append("		      		OverRide, ")
                .Append("		      		Minscore, ")
                .Append("		      		Required, ")
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		      		DocSubmittedCount, ")
                .Append("		      		DocStatusDescrip,CampGrpId ")
                .Append("      				from  ")
                .Append("      					( ")
                .Append("      						select Distinct ")
                .Append("      							t1.adReqId, ")
                .Append("      							t1.Descrip, ")
                .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
                .Append("	      						t2.StartDate, ")
                .Append("	      						t2.EndDate, ")
                .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	      							LeadId = '" & LeadId & "' ) as ActualScore, ")
                .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							LeadId = '" & LeadId & "' ) as TestTaken, ")
                .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							LeadId = '" & LeadId & "' ) as Comments, ")
                .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("	      						        LeadId = '" & LeadId & "' ) ")
                .Append("	      						as override, ")
                .Append("	      						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("	      						LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                ''.Append("		      					Cast(t2.MinScore as int) as MinScore, ")
                .Append("		      					t2.MinScore , ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                .Append("	      						ISNULL(t3.IsRequired,0) as Required, ")
                .Append("	      						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("      							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("      							s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("      						from  ")
                .Append("      							adReqs t1, ")
                .Append("							adReqsEffectiveDates t2, ")
                .Append("							adReqLeadGroups t3, ")
                .Append("							adPrgVerTestDetails t5, ")
                .Append("							adReqGrpDef t6, ")
                .Append("						adLeadByLeadGroups t7  ")
                .Append("      					where ")
                .Append("      						t1.adReqId = t2.adReqId and ")
                .Append("      						t1.adreqTypeId in (1) and t1.StatusId='" & strActiveGUID & "' ")
                ''Added by Saraswathi Lakshmanan on Sept 15 2010
                ''To filter only the documents marked as required for Enrollment
                ''For Document tracking
                .Append("                          and  t1.ReqforEnrollment=1  ")

                .Append("      						and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and ")
                .Append("						t3.LeadGrpId = t7.LeadGrpId and ")
                .Append("      						t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
                .Append("	      					and t7.LeadId = '" & LeadId & "'  and  ")
                .Append("						t5.PrgVerId = '" & PrgVerId & "'   ")
                .Append("      		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                ' Get Requirements that were assigned to lead group but not part of any requirement group
                .Append("	     select  distinct ")
                .Append("	           adReqId, ")
                .Append("	           Descrip, ")
                .Append("	           ReqGrpId, ")
                .Append("	           StartDate, ")
                .Append("	           EndDate, ")
                .Append("	           ActualScore, ")
                .Append("	           TestTaken, ")
                .Append("	           Comments, ")
                .Append("	           OverRide, ")
                .Append("	           Minscore, ")
                .Append("	           Required, ")
                .Append("	           Case  when ActualScore >= Minscore  then 'True' else 'False' End as Pass, ")
                .Append("	           DocSubmittedCount, ")
                .Append("	           DocStatusDescrip,CampGrpId ")
                .Append("           from  ")
                .Append("           ( ")
                ' Get Requirement Group and requirements that are assigned to a lead group
                ' and part of a requirement group
                .Append("          	 select  distinct ")
                .Append("	           	t1.adReqId, ")
                .Append("	           	t1.Descrip, ")
                .Append("	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append("	           	'" & strEnrollDate & "' as CurrentDate,  ")
                .Append("	           	t2.StartDate, ")
                .Append("	           	t2.EndDate,  ")
                .Append("	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId = '" & LeadId & "' ) as ActualScore,  ")
                .Append("	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId = '" & LeadId & "' ) as TestTaken,  ")
                .Append("	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId = '" & LeadId & "' ) as Comments,  ")
                .Append("	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("	                              LeadId = '" & LeadId & "' ) ")
                .Append("	                      as override, ")
                .Append("	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
                .Append("	           		LeadId = '" & LeadId & "' ) as DocSubmittedCount, ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                ''.Append("	           	Cast(t2.MinScore as int) as MinScore, ")
                .Append("	           	t2.MinScore , ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                .Append("			ISNULL(t3.IsRequired,0) as Required, ")
                .Append("	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("	           s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("           from ")
                .Append("           	adReqs t1, ")
                .Append("           	adReqsEffectiveDates t2, ")
                .Append("		adReqLeadGroups t3 ")
                .Append("           where  ")
                .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t1.StatusId='" & strActiveGUID & "' ")
                .Append("		and ")
                ''Added by Saraswathi Lakshmanan on Sept 15 2010
                ''To filter only the documents marked as required for Enrollment
                ''For Document tracking
                .Append("                   t1.ReqforEnrollment=1 and ")

                .Append("		t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId='" & PrgVerId & "' and s2.adReqId is null) ")
                .Append("		and t3.LeadGrpId in ")
                .Append("		(select LeadGrpId from adLeadByLeadGroups where LeadId = '" & LeadId & "' ) and ")
                .Append("           	t1.adReqTypeId in (1) and t2.MandatoryRequirement <> 1 and ")
                .Append("		t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId='" & PrgVerId & "') ")
                .Append("           ) ")
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" ) R2 ")
                .Append(" where R2.Required = 1 and R2.Pass = 'False' ")
                .Append(" ) R3  ")
                If Not strCampGrpId = "" Then
                    .Append("  Where R3.CampGrpId in 	('")
                    .Append(strCampGrpId)
                    .Append(") ")
                End If
                .Append(" group by OverRide Having R3.OverRide='False' ")
            End With

            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            Dim studentinfo As New PlacementInfo
            Dim intLeadCount As Integer
            While dr.Read()
                If Not (dr("OverRideCount") Is DBNull.Value) Then intLeadCount = CType(dr("OverRideCount"), Integer) Else intLeadCount = 0
            End While

            If Not dr.IsClosed Then dr.Close()

            Return intLeadCount
        Catch ex As Exception
        Finally
        End Try
    End Function
    Public Function CheckAllApprovedDocumentsWithPrgVersion(ByVal LeadId As String, ByVal PrgVerId As String, Optional ByVal CampusId As String = "") As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim strEnrollDate As Date = Date.Now.ToShortDateString
        Dim dsGetCmpGrps As DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If


        Try

            With sb
                .Append(" select Count(*) from ")
                .Append(" ( ")
                .Append(" select  distinct ")
                .Append("       adReqId as DocumentId, ")
                .Append("       Descrip as DocumentDescrip, ")
                .Append("       ReqGrpId, ")
                .Append("       StartDate,  ")
                .Append("       EndDate, ")
                .Append("       ActualScore, ")
                .Append("       TestTaken, ")
                .Append("       Comments, ")
                .Append("       Case when OverRide>=1 then 'True' else 'False' end as OverRide, ")
                .Append("       Case when (select count(*) from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') >=1 then ")
                .Append("       (select MinScore from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') ")
                .Append("       else MinScore end as MinScore, ")
                .Append("       Required, ")
                .Append("       DocSubmittedCount, ")
                .Append("       case when DocStatusDescrip='Approved' then 'Approved' else 'Not Approved' end as DocStatusDescrip, ")
                .Append("       Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount,Pass,CampGrpId ")
                .Append("       from  ")
                .Append("       ( ")
                ' Get Requirement Group and requirements of mandatory requirement
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate, ")
                .Append("		      		ActualScore, ")
                .Append("		      		TestTaken, ")
                .Append("		      		Comments, ")
                .Append("		      		OverRide, ")
                .Append("		      		Minscore, ")
                .Append("		      		Required, ")
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		      		DocSubmittedCount, ")
                .Append("		      		DocStatusDescrip,CampGrpId ")
                .Append("      	from  ")
                .Append("      		( ")
                .Append("      		select Distinct ")
                .Append("      				t1.adReqId, ")
                .Append("      				t1.Descrip, ")
                .Append("      				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("      					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("      					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                .Append("      				'" & strEnrollDate & "' as CurrentDate, ")
                .Append("      				t2.StartDate, ")
                .Append("      				t2.EndDate, ")
                .Append("      				(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("      					LeadId = '" & LeadId & "' ) as ActualScore, ")
                .Append("      				(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("      					LeadId = '" & LeadId & "' ) as TestTaken, ")
                .Append("      				(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("      					LeadId = '" & LeadId & "' ) as Comments, ")
                .Append("      				       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("      				        LeadId = '" & LeadId & "' ) as override, ")
                .Append("      				(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("      					LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("      				t2.Minscore, ")
                .Append("      				1 as Required, ")
                .Append("      				(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("      				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("      				s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("      				from  ")
                .Append("      				adReqs t1, ")
                .Append("      				adReqsEffectiveDates t2 ")
                .Append("      				where  ")
                .Append("      				t1.adReqId = t2.adReqId and ")
                .Append("       				t2.MandatoryRequirement = 1 and ")
                ''Added by Saraswathi Lakshmanan on Sept 15 2010
                ''To filter only the documents marked as required for Enrollment
                ''For Document tracking
                .Append("                   t1.ReqforEnrollment=1 and ")
                .Append("      				t1.adReqTypeId in (3) and t1.StatusId ='" & strActiveGUID & "' ")
                .Append("       				) ")
                .Append("       				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("       		union ")

                ' Get The Requirements that was assigned to a program version
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate, ")
                .Append("		      		ActualScore, ")
                .Append("		      		TestTaken, ")
                .Append("		      		Comments, ")
                .Append("		      		OverRide, ")
                .Append("		      		Minscore, ")
                .Append("		      		Required, ")
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		      		DocSubmittedCount, ")
                .Append("		      		DocStatusDescrip,CampGrpId ")
                .Append("      				from  ")
                .Append("      					( ")
                .Append("      						select Distinct ")
                .Append("      							t1.adReqId, ")
                .Append("      							t1.Descrip, ")
                .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
                .Append("	      						t2.StartDate, ")
                .Append("	      						t2.EndDate, ")
                .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	      							LeadId = '" & LeadId & "' ) as ActualScore, ")
                .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							LeadId = '" & LeadId & "' ) as TestTaken, ")
                .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							LeadId = '" & LeadId & "' ) as Comments, ")
                .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("	      						        LeadId = '" & LeadId & "' ) ")
                .Append("	      						as override, ")
                .Append("	      						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("	      						LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("		      					t2.Minscore, ")
                .Append("	      						1 as Required, ")
                .Append("	      						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("      							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("      							s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("      						from  ")
                .Append("      							adReqs t1, ")
                .Append("      							adReqsEffectiveDates t2, ")
                .Append("      							adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
                .Append("      						where ")
                .Append("      							t1.adReqId = t2.adReqId and t2.MandatoryRequirement <> 1 and ")
                ''Added by Saraswathi Lakshmanan on Sept 15 2010
                ''To filter only the documents marked as required for Enrollment
                ''For Document tracking
                .Append("                   t1.ReqforEnrollment=1 and ")
                .Append("      							t1.adreqTypeId in (3) and t1.StatusId ='" & strActiveGUID & "' and ")
                .Append("      							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId ")
                .Append("      							and t1.adReqId = t5.adReqId and t5.PrgVerId = '" & PrgVerId & "'    ")
                '.Append("      					      	 and	t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where ")
                '.Append("	      						LeadId = '" & LeadId & "' ) ")
                .Append("      							and t5.adReqId is not null ")
                .Append("      						) ")
                .Append("      						R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                ' Get Requirements === Requirement Group assigned to Program Version
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate, ")
                .Append("		      		ActualScore, ")
                .Append("		      		TestTaken, ")
                .Append("		      		Comments, ")
                .Append("		      		OverRide, ")
                .Append("		      		Minscore, ")
                .Append("		      		Required, ")
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		      		DocSubmittedCount, ")
                .Append("		      		DocStatusDescrip,CampGrpId ")
                .Append("      				from  ")
                .Append("      					( ")
                .Append("      						select Distinct ")
                .Append("      							t1.adReqId, ")
                .Append("      							t1.Descrip, ")
                .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
                .Append("	      						t2.StartDate, ")
                .Append("	      						t2.EndDate, ")
                .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	      							LeadId = '" & LeadId & "' ) as ActualScore, ")
                .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							LeadId = '" & LeadId & "' ) as TestTaken, ")
                .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							LeadId = '" & LeadId & "' ) as Comments, ")
                .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("	      						        LeadId = '" & LeadId & "' ) ")
                .Append("	      						as override, ")
                .Append("	      						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("	      						LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("		      					t2.Minscore, ")
                .Append("	      						ISNULL(t3.IsRequired,0) as Required, ")
                .Append("	      						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("      							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("      							s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("      						from  ")
                .Append("      							adReqs t1, ")
                .Append("							adReqsEffectiveDates t2, ")
                .Append("							adReqLeadGroups t3, ")
                .Append("							adPrgVerTestDetails t5, ")
                .Append("							adReqGrpDef t6, ")
                .Append("						adLeadByLeadGroups t7  ")
                .Append("      					where ")
                .Append("      						t1.adReqId = t2.adReqId and ")
                ''Added by Saraswathi Lakshmanan on Sept 15 2010
                ''To filter only the documents marked as required for Enrollment
                ''For Document tracking
                .Append("                   t1.ReqforEnrollment=1 and ")
                .Append("      						t1.adreqTypeId in (3) and t1.StatusId ='" & strActiveGUID & "' and ")
                .Append("      						t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and ")
                .Append("						t3.LeadGrpId = t7.LeadGrpId and ")
                .Append("      						t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
                .Append("	      					and t7.LeadId = '" & LeadId & "'  and  ")
                .Append("						t5.PrgVerId = '" & PrgVerId & "'   ")
                .Append("      		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                ' Get Requirements that were assigned to lead group but not part of any requirement group
                .Append("	     select  distinct ")
                .Append("	           adReqId, ")
                .Append("	           Descrip, ")
                .Append("	           ReqGrpId, ")
                .Append("	           StartDate, ")
                .Append("	           EndDate, ")
                .Append("	           ActualScore, ")
                .Append("	           TestTaken, ")
                .Append("	           Comments, ")
                .Append("	           OverRide, ")
                .Append("	           Minscore, ")
                .Append("	           Required, ")
                .Append("	           Case  when ActualScore >= Minscore  then 'True' else 'False' End as Pass, ")
                .Append("	           DocSubmittedCount, ")
                .Append("	           DocStatusDescrip,CampGrpId ")
                .Append("           from  ")
                .Append("           ( ")
                ' Get Requirement Group and requirements that are assigned to a lead group
                ' and part of a requirement group
                .Append("          	 select  distinct ")
                .Append("	           	t1.adReqId, ")
                .Append("	           	t1.Descrip, ")
                .Append("	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append("	           	'" & strEnrollDate & " ' as CurrentDate,  ")
                .Append("	           	t2.StartDate, ")
                .Append("	           	t2.EndDate,  ")
                .Append("	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId = '" & LeadId & "' ) as ActualScore,  ")
                .Append("	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId = '" & LeadId & "' ) as TestTaken,  ")
                .Append("	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId = '" & LeadId & "' ) as Comments,  ")
                .Append("	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("	                              LeadId = '" & LeadId & "' ) ")
                .Append("	                      as override, ")
                .Append("	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
                .Append("	           		LeadId = '" & LeadId & "' ) as DocSubmittedCount, ")
                .Append("	           	t2.Minscore, ")
                .Append("			ISNULL(t3.IsRequired,0) as Required, ")
                .Append("	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("	           s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("           from ")
                .Append("           	adReqs t1, ")
                .Append("           	adReqsEffectiveDates t2, ")
                .Append("		adReqLeadGroups t3 ")
                .Append("           where  ")
                .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId ")
                ''Added by Saraswathi Lakshmanan on Sept 15 2010
                ''To filter only the documents marked as required for Enrollment
                ''For Document tracking
                .Append("            and       t1.ReqforEnrollment=1  ")
                .Append("		and  t1.StatusId ='" & strActiveGUID & "'")
                .Append("		and t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId='" & PrgVerId & "' and s2.adReqId is null) ")
                .Append("		and t3.LeadGrpId in ")
                .Append("		(select LeadGrpId from adLeadByLeadGroups where LeadId = '" & LeadId & "' ) and ")
                .Append("           	t1.adReqTypeId in (3) and t2.MandatoryRequirement <> 1 and ")
                .Append("		t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId='" & PrgVerId & "') ")
                .Append("           ) ")
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" ) R2 ")
                .Append(" ) R3 where R3.Required =1  and  R3.DocStatusDescrip = 'Not Approved' and R3.OverRide='False' ")
                If Not strCampGrpId = "" Then
                    .Append("  AND R3.CampGrpId in 	('")
                    .Append(strCampGrpId)
                    .Append(") ")
                End If
            End With

            Dim intLeadCount As Integer = db.RunParamSQLScalar(sb.ToString)
            Return intLeadCount
        Catch ex As Exception
        Finally
        End Try
    End Function
    Public Function CheckIfLeadHasPassedRequiredTestNoProgramVersion(ByVal LeadId As String) As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strPreviousEducationId As String
        Dim strPreviousEduTest As String
        Dim strEnrollDate As Date = Date.Now.ToShortDateString
        Try

            With sb
                .Append(" select Count(*) as CountOfLeadFailedRequiredTest,OverRide from ")
                .Append(" ( ")
                .Append(" select ")
                .Append("           adReqId, ")
                .Append("           Descrip, ")
                .Append("           ReqGrpId, ")
                .Append("           StartDate, ")
                .Append("           EndDate, ")
                .Append("           ActualScore,  ")
                .Append("           TestTaken, ")
                .Append("           Comments, ")
                .Append("           Case when OverRide=1 then 1 else 0 end as OverRide, ")
                .Append("           Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount, ")
                .Append("           DocSubmittedCount, ")
                .Append("           Minscore, ")
                .Append("           Required, ")
                .Append("		    Pass, ")
                .Append("           DocStatusDescrip ")
                .Append("           from ")
                .Append("           ( ")
                .Append("           select  Distinct ")
                .Append("	           adReqId, ")
                .Append("	           Descrip, ")
                .Append("	           ReqGrpId, ")
                .Append("	           StartDate, ")
                .Append("	           EndDate, ")
                .Append("	           ActualScore, ")
                .Append("	           TestTaken, ")
                .Append("	           Comments, ")
                .Append("	           OverRide, ")
                .Append("	           Minscore, ")
                .Append("	           Required, ")
                .Append("	           Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("	           DocSubmittedCount, ")
                .Append("	           DocStatusDescrip ")
                .Append("           from  ")
                .Append("           ( ")
                '  Get Requirement Group and requirements of mandatory requirement
                .Append("          	 select  ")
                .Append("	           	t1.adReqId, ")
                .Append("	           	t1.Descrip, ")
                .Append("	           	case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("	           		(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("	           	else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId,  ")
                .Append("	           	'" & strEnrollDate & "' as CurrentDate,  ")
                .Append("	           	t2.StartDate, ")
                .Append("	           	t2.EndDate,  ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                ''.Append("	           	(select cast(ActualScore as int)  from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           	(select ActualScore  from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                .Append("	           		LeadId='" & LeadId & "')  as ActualScore,  ")
                .Append("	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId='" & LeadId & "')  as TestTaken,  ")
                .Append("	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId='" & LeadId & "')  as Comments,  ")
                .Append("	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and  ")
                .Append("	                              LeadId='" & LeadId & "') ")
                .Append("	                      as override, ")
                .Append("	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
                .Append("	           		LeadId='" & LeadId & "')  as DocSubmittedCount, ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                ''.Append("	           	cast(t2.MinScore as int) as MinScore, ")
                .Append("	           	t2.MinScore , ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                .Append("	           	1 as Required, ")
                .Append("	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("	           s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("           from ")
                .Append("           	adReqs t1, ")
                .Append("           	adReqsEffectiveDates t2 ")
                .Append("           where  ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" t1.reqforEnrollment=1 and ")
                .Append("           	t1.adReqId = t2.adReqId and  ")
                .Append("           	t2.MandatoryRequirement=1 and ")
                .Append("           	t1.adReqTypeId in (1) ")
                .Append("           ) ")
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                '  Get Requirements that were assigned to lead group but not part of any requirement group
                .Append(" select ")
                .Append("           adReqId, ")
                .Append("           Descrip, ")
                .Append("           ReqGrpId, ")
                .Append("           StartDate, ")
                .Append("           EndDate, ")
                .Append("           ActualScore,  ")
                .Append("           TestTaken, ")
                .Append("           Comments, ")
                .Append("           OverRide, ")
                .Append("	           Minscore, ")
                .Append("	           Required, ")
                .Append("	           Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("	           DocSubmittedCount, ")
                .Append("	           DocStatusDescrip ")
                .Append("           from ")
                .Append("           ( ")
                '  Get Requirement Group and requirements that are assigned to a lead group
                '  and part of a requirement group
                .Append("           	 select  ")
                .Append(" 	           	t1.adReqId, ")
                .Append(" 	           	t1.Descrip, ")
                .Append(" 	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append(" 	           	'" & strEnrollDate & "' as CurrentDate,  ")
                .Append(" 	           	t2.StartDate, ")
                .Append(" 	           	t2.EndDate,  ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                ''.Append(" 	           	(select cast(ActualScore as int) from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append(" 	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                .Append(" 	           		LeadId='" & LeadId & "')  as ActualScore,  ")
                .Append(" 	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append(" 	           		LeadId='" & LeadId & "')  as TestTaken,  ")
                .Append(" 	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append(" 	           		LeadId='" & LeadId & "')  as Comments,  ")
                .Append(" 	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append(" 	                              LeadId='" & LeadId & "') ")
                .Append(" 	                      as override, ")
                .Append(" 	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
                .Append(" 	           		LeadId='" & LeadId & "')  as DocSubmittedCount, ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                ''.Append(" 	           	cast(t2.Minscore as int) as MinScore, ")
                .Append(" 	           	t2.Minscore , ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                .Append(" 			ISNULL(t3.IsRequired,0) as Required, ")
                .Append(" 	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append(" 	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append(" 	           s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("            from ")
                .Append("           	adReqs t1, ")
                .Append("           	adReqsEffectiveDates t2, ")
                .Append("		adReqLeadGroups t3 ")
                .Append("           where  ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" t1.reqforEnrollment=1 and ")
                .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId ")
                ' and t1.adReqId not in (select Distinct adReqId from adReqGrpDef)
                .Append("		and t3.LeadGrpId in ")
                .Append("		(select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and ")
                .Append("           	t1.adReqTypeId in (1) and t2.MandatoryRequirement <> 1 ")
                .Append("           ) ")
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" ) R2 ")
                .Append(" where R2.Required =1 and R2.Pass = 'False'  ")
                .Append(" ) R3 group by R3.OverRide having R3.OverRide=0 ")
            End With

            Dim intLeadCount As Integer = db.RunParamSQLScalar(sb.ToString)

            'Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(SingletonAppSettings.AppSettings("ConString")))
            'Dim da1 As New OleDbDataAdapter(sc1)
            'da1.Fill(ds, "TestDetails")
            'sb.Remove(0, sb.Length)
            Return intLeadCount
        Catch ex As Exception
        Finally
        End Try
    End Function
    Public Function CheckIfLeadHasApprovedRequiredDocumentsNoProgramVersion(ByVal LeadId As String) As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strPreviousEducationId As String
        Dim strPreviousEduTest As String
        Dim strEnrollDate As Date = Date.Now.ToShortDateString
        Try

            With sb
                .Append(" select Count(*) as CountOfLeadFailedRequiredDocuments,OverRide,DocStatusDescrip from ")
                .Append(" ( ")
                .Append(" select ")
                .Append("           adReqId as  DocumentId, ")
                .Append("           Descrip as DocumentDescrip, ")
                .Append("           ReqGrpId, ")
                .Append("           StartDate, ")
                .Append("           EndDate, ")
                .Append("           ActualScore,  ")
                .Append("           TestTaken, ")
                .Append("           Comments, ")
                .Append("           Case when OverRide=1 then 1 else 0 end as OverRide, ")
                .Append("           Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount, ")
                .Append("           DocSubmittedCount, ")
                .Append("           Minscore, ")
                .Append("           Required, ")
                .Append("		    Pass, ")
                .Append("           case when DocStatusDescrip='Approved' then 1 else 0 end as DocStatusDescrip ")
                .Append("           from ")
                .Append("           ( ")
                .Append("           select  Distinct ")
                .Append("	           adReqId, ")
                .Append("	           Descrip, ")
                .Append("	           ReqGrpId, ")
                .Append("	           StartDate, ")
                .Append("	           EndDate, ")
                .Append("	           ActualScore, ")
                .Append("	           TestTaken, ")
                .Append("	           Comments, ")
                .Append("	           OverRide, ")
                .Append("	           Minscore, ")
                .Append("	           Required, ")
                .Append("	           Case  when ActualScore >= Minscore then 1 else 0 End as Pass, ")
                .Append("	           DocSubmittedCount, ")
                .Append("	           DocStatusDescrip ")
                .Append("           from  ")
                .Append("           ( ")
                '  Get Requirement Group and requirements of mandatory requirement
                .Append("          	 select  ")
                .Append("	           	t1.adReqId, ")
                .Append("	           	t1.Descrip, ")
                .Append("	           	case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("	           		(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("	           	else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId,  ")
                .Append("	           	'" & strEnrollDate & "' as CurrentDate,  ")
                .Append("	           	t2.StartDate, ")
                .Append("	           	t2.EndDate,  ")
                .Append("	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId='" & LeadId & "')  as ActualScore,  ")
                .Append("	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId='" & LeadId & "')  as TestTaken,  ")
                .Append("	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId='" & LeadId & "')  as Comments,  ")
                .Append("	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and  ")
                .Append("	                              LeadId='" & LeadId & "') ")
                .Append("	                      as override, ")
                .Append("	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
                .Append("	           		LeadId='" & LeadId & "')  as DocSubmittedCount, ")
                .Append("	           	t2.Minscore, ")
                .Append("	           	1 as Required, ")
                .Append("	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("	           s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("           from ")
                .Append("           	adReqs t1, ")
                .Append("           	adReqsEffectiveDates t2 ")
                .Append("           where  ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" t1.reqforEnrollment=1 and ")
                .Append("           	t1.adReqId = t2.adReqId and  ")
                .Append("           	t2.MandatoryRequirement=1 and ")
                .Append("           	t1.adReqTypeId in (3) ")
                .Append("           ) ")
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                '  Get Requirements that were assigned to lead group but not part of any requirement group
                .Append(" select ")
                .Append("           adReqId, ")
                .Append("           Descrip, ")
                .Append("           ReqGrpId, ")
                .Append("           StartDate, ")
                .Append("           EndDate, ")
                .Append("           ActualScore,  ")
                .Append("           TestTaken, ")
                .Append("           Comments, ")
                .Append("           OverRide, ")
                .Append("	           Minscore, ")
                .Append("	           Required, ")
                .Append("	           Case  when ActualScore >= Minscore then 1 else 0 End as Pass, ")
                .Append("	           DocSubmittedCount, ")
                .Append("	           DocStatusDescrip ")
                .Append("           from ")
                .Append("           ( ")
                '  Get Requirement Group and requirements that are assigned to a lead group
                '  and part of a requirement group
                .Append("           	 select  ")
                .Append(" 	           	t1.adReqId, ")
                .Append(" 	           	t1.Descrip, ")
                .Append(" 	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append(" 	           	'" & strEnrollDate & "' as CurrentDate,  ")
                .Append(" 	           	t2.StartDate, ")
                .Append(" 	           	t2.EndDate,  ")
                .Append(" 	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append(" 	           		LeadId='" & LeadId & "')  as ActualScore,  ")
                .Append(" 	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append(" 	           		LeadId='" & LeadId & "')  as TestTaken,  ")
                .Append(" 	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append(" 	           		LeadId='" & LeadId & "')  as Comments,  ")
                .Append(" 	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append(" 	                              LeadId='" & LeadId & "') ")
                .Append(" 	                      as override, ")
                .Append(" 	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
                .Append(" 	           		LeadId='" & LeadId & "')  as DocSubmittedCount, ")
                .Append(" 	           	t2.Minscore, ")
                .Append(" 			ISNULL(t3.IsRequired,0) as Required, ")
                .Append(" 	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append(" 	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append(" 	           s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("            from ")
                .Append("           	adReqs t1, ")
                .Append("           	adReqsEffectiveDates t2, ")
                .Append("		adReqLeadGroups t3 ")
                .Append("           where  ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" t1.reqforEnrollment=1 and ")
                .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId ")
                ' and t1.adReqId not in (select Distinct adReqId from adReqGrpDef)
                .Append("		and t3.LeadGrpId in ")
                .Append("		(select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and ")
                .Append("           	t1.adReqTypeId in (3) and t2.MandatoryRequirement <> 1 ")
                .Append("           ) ")
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" ) R2 ")
                .Append(" where R2.Required = 1  ")
                .Append(" ) R3 ")
                .Append(" group by R3.OverRide,R3.DocStatusDescrip having R3.OverRide=0 and R3.DocStatusDescrip=0 ")
            End With

            Dim intLeadCount As Integer = db.RunParamSQLScalar(sb.ToString)
            Return intLeadCount
        Catch ex As Exception
        Finally
        End Try
    End Function

    Public Function GetHasLeadMetRequirements(ByVal LeadId As String) As Boolean

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        Dim db = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)

        Dim command = New SqlCommand("GetHasLeadMetrequirements", db)

        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@LeadId", LeadId)

        db.Open()

        Dim hasMetRequirements = False

        Try
            Dim sqlreader As SqlDataReader = command.ExecuteReader()
            While sqlreader.Read()
                hasMetRequirements = CType(sqlreader(0), Boolean)
            End While

            Return hasMetRequirements
        Catch
            Return False
        Finally
            db.Close()
        End Try

    End Function
    Public Function AddRequirementGroup(ByVal ReqGroupInfo As AdReqGrpInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append(" INSERT adReqGroups(ReqGrpId,Code,Descrip,StatusId,CampGrpId,ModDate,ModUser,IsMandatoryReqGrp) ")
                .Append(" VALUES (?,?,?,?,?,?,?,?) ")
            End With

            '  Req Group Id
            db.AddParameter("@ReqGrpId", ReqGroupInfo.ReqGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Code
            db.AddParameter("@Code", ReqGroupInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Descrip
            db.AddParameter("@Descrip", ReqGroupInfo.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", ReqGroupInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If ReqGroupInfo.CampGrpId = Guid.Empty.ToString Or ReqGroupInfo.CampGrpId = "" Then
                db.AddParameter("@CampusGrpId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampusGrpId", ReqGroupInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'OtherState
            If ReqGroupInfo.IsMandatoryReqGrp = True Then
                db.AddParameter("@MandatoryReqGroup", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            Else
                db.AddParameter("@MandatoryReqGroup", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If

            'execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            ' return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function UpdateRequirementGroup(ByVal ReqGroupInfo As AdReqGrpInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb5 As New StringBuilder
            With sb5
                .Append(" Update adReqGroups set Code=?,Descrip=?,StatusId=?,CampGrpId=?,ModDate=?,ModUser=?,IsMandatoryReqGrp=? where ReqGrpId=? ")
            End With

            '   Code
            db.AddParameter("@Code", ReqGroupInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Descrip
            db.AddParameter("@Descrip", ReqGroupInfo.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", ReqGroupInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If ReqGroupInfo.CampGrpId = Guid.Empty.ToString Or ReqGroupInfo.CampGrpId = "" Then
                db.AddParameter("@CampusGrpId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampusGrpId", ReqGroupInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'OtherState
            If ReqGroupInfo.IsMandatoryReqGrp = True Then
                db.AddParameter("@MandatoryReqGroup", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            Else
                db.AddParameter("@MandatoryReqGroup", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If

            '  Req Group Id
            db.AddParameter("@ReqGrpId", ReqGroupInfo.ReqGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb5.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetReqGroupInfo(ByVal ReqGrpId As String) As AdReqGrpInfo
        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("SELECT Code,Descrip,StatusId,CampGrpId,ModDate,ModUser,IsMandatoryReqGrp from adReqGroups where ReqGrpId=?  ")
        End With

        db.AddParameter("@ReqGrpId", ReqGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim ReqInfo As New AdReqGrpInfo
        While dr.Read()
            'set properties with data from DataReader
            With ReqInfo

                'Code
                If Not (dr("Code") Is DBNull.Value) Then .Code = dr("Code") Else .Code = ""

                'Descrip
                If Not (dr("Descrip") Is DBNull.Value) Then .Descrip = dr("Descrip") Else .Descrip = ""

                'CampusGrpId
                If Not (dr("CampGrpId") Is DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString Else .CampGrpId = ""

                'StatusId
                If Not (dr("StatusId") Is DBNull.Value) Then .StatusId = CType(dr("StatusId"), Guid).ToString Else .StatusId = ""

                'ModDate 
                If Not (dr("ModDate") Is DBNull.Value) Then .ModDate = dr("ModDate") Else .ModDate = ""

                'ModDate 
                If Not (dr("ModUser") Is DBNull.Value) Then .ModUser = dr("ModUser") Else .ModUser = ""

                'MandatoryReqGrp
                If Not (dr("IsMandatoryReqGrp") Is DBNull.Value) Then .IsMandatoryReqGrp = dr("IsMandatoryReqGrp") Else .IsMandatoryReqGrp = ""


            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return ReqInfo
    End Function
    Public Function GetReqGroups() As DataSet

        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT ")
            .Append("       A.ReqGrpId,")
            .Append("       A.Code,")
            .Append("       A.Descrip,")
            .Append("       A.StatusId,")
            .Append("       (CASE B.Status WHEN 'Active' THEN 1 ELSE 0 END) As Status,")
            .Append("       A.CampGrpId,")
            .Append("       A.IsMandatoryReqGrp,")
            .Append("       A.ModDate,")
            .Append("       A.ModUser,A.IsMandatoryReqGrp ")
            .Append("FROM   adReqGroups A,syStatuses B ")
            .Append("WHERE  A.StatusId = B.StatusId ")
            .Append("ORDER BY A.Descrip")
        End With

        'Build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
        Dim da As New OleDbDataAdapter(sc)
        da.Fill(ds, "ReqGroups")
        sb.Remove(0, sb.Length)
        Return ds
    End Function
    Public Function GetReqGroupsByStatus(ByVal Status As String) As DataSet
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT ")
            .Append("       A.ReqGrpId,")
            .Append("       A.Code,")
            .Append("       A.Descrip,")
            .Append("       A.StatusId,")
            .Append("       (CASE B.Status WHEN 'Active' THEN 1 ELSE 0 END) As Status,")
            .Append("       A.CampGrpId,")
            .Append("       A.IsMandatoryReqGrp,")
            .Append("       A.ModDate,")
            .Append("       A.ModUser,A.IsMandatoryReqGrp ")
            .Append("FROM   adReqGroups A,syStatuses B ")
            .Append("WHERE  A.StatusId = B.StatusId ")
            If Status = "Active" Then
                .Append(" and B.Status = 'Active' ")
            ElseIf Status = "Inactive" Then
                .Append(" and B.Status = 'Inactive' ")
            End If
            .Append("ORDER BY A.Descrip")
        End With

        'Build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
        Dim da As New OleDbDataAdapter(sc)
        da.Fill(ds, "ReqGroups")
        sb.Remove(0, sb.Length)
        Return ds
    End Function
    Public Function DeleteRequirementGroup(ByVal ReqGrpId As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'do an insert
        Try
            '   build the query
            Dim sb5 As New StringBuilder
            With sb5
                .Append(" Delete from adReqGroups  where ReqGrpId=? ")
            End With

            '  Req Group Id
            db.AddParameter("@ReqGrpId", ReqGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb5.ToString)

            '   return without errors
            Return ""
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function CheckIfMandatoryGrpExists() As Integer
        Dim db As New DataAccess
        Dim sbEnroll As New StringBuilder
        With sbEnroll
            .Append(" select Count(*) from adReqGroups where IsMandatoryReqGrp=1 ")
        End With
        Dim intLeadGenderUnknown As Integer = db.RunParamSQLScalar(sbEnroll.ToString)
        Return intLeadGenderUnknown
    End Function
    Public Function UpdateLeadGroupsAndStudentEnrollments(ByVal LeadId As String, ByVal user As String, ByVal selectedLeadGroups() As String, ByVal StuEnrollId As String) As Integer
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   First we have to delete all existing selections
        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("DELETE FROM adLeadByLeadGroups WHERE StuEnrollId = ?")
        End With

        '   delete all selected items
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        '   Insert one record per each Item in the Selected Group
        Dim i As Integer
        For i = 0 To selectedLeadGroups.Length - 1

            '   build query
            With sb
                .Append("INSERT INTO adLeadByLeadGroups(LeadGrpLeadId, StuEnrollId,LeadGrpId, ModDate, ModUser,LeadId) ")
                .Append("VALUES(?,?,?,?,?,?)")
            End With

            '   add parameters
            db.AddParameter("@LeadGrpLeadId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@LeadgrpId", DirectCast(selectedLeadGroups.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''DE5530 
            ''Name: QA: Getting an error page when we click the save button on the enrollments tab for some students. 

            If Not Trim(LeadId) = Guid.Empty.ToString And Not Trim(LeadId) = "" Then
                db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@LeadId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        Next

        'Close Connection
        db.CloseConnection()

    End Function
    Public Function GetLeadgroupsSelectedByStudentEnrollment(ByVal StuEnrollId As String) As DataSet

        '   get the connection to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append(" select LeadGrpLeadId,LeadId,LeadgrpId from adLeadByLeadGroups where StuEnrollId=? ")
        End With

        ' Add the parameter
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return the dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetLeadGroupCountByStudentEnrollment(ByVal StuEnrollId As String) As Integer
        '   get the connection to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder


        '   build the sql query
        With sb
            .Append("SELECT Count(*) ")
            .Append("FROM   adLeadByLeadGroups ")
            .Append("WHERE  StuEnrollId = ? ")
        End With

        ' Add the parameter
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim LeadgrpCount As Integer

        '   Return the dataset
        LeadgrpCount = db.RunParamSQLScalar(sb.ToString)
        Return LeadgrpCount
    End Function
    Public Function GetAllDocumentStudentNames(ByVal campusId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da6 As New OleDbDataAdapter

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append(" select Distinct t1.StudentId,t1.firstname,t1.lastname,t1.SSN,t1.StudentNumber ")
            .Append(" from arStudent t1,arStuEnrollments t2  ")
            .Append(" where t1.StudentId = t2.StudentId ")
            .Append(" and t2.campusId = ? ")
            .Append(" ORDER BY FirstName ")
        End With
        db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorStudent")
        Finally
            db.CloseConnection()
        End Try
        Return ds
    End Function
    Public Function GetAllStudentNamesAndIdentifier(ByVal campusId As String, ByVal strStudentIdentifier As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da6 As New OleDbDataAdapter

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        Dim strSQL As String
        strSQL = " select Distinct t1.StudentId,t1.firstname,t1.lastname,t1.SSN,t1.StudentNumber"
        If strStudentIdentifier = "EnrollmentId" Then
            strSQL += ",t2.EnrollmentId"
        end If 
        strSQL += " from arStudent t1,arStuEnrollments t2 "
        strSQL += " where t1.StudentId = t2.StudentId "
        strSQL += " and t2.campusid=? "
        strSQL += " order by firstname"

        db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(strSQL)
        Try
            da6.Fill(ds, "InstructorStudent")
        Finally
            db.CloseConnection()
        End Try
        Return ds
    End Function
    Public Function GetPrgVerIdByLeadId(ByVal LeadId As String) As String
        'connect to the database
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim dr As OleDbDataReader
        Dim strPrgVerId As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select Distinct PrgVerId from arprgVersions where LeadId=? ")
        End With
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.OpenConnection()

        dr = db.RunParamSQLDataReader(sb.ToString)
        While (dr.Read())
            If Not dr("PrgVerId") Is DBNull.Value Then strPrgVerId = CType(dr("PrgVerId"), Guid).ToString Else strPrgVerId = ""
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return strPrgVerId
    End Function
    Public Function GetAllStandardDocumentsByPrgVersionEnrollments(ByVal StudentId As String, ByVal ModuleId As Integer, ByVal CampusId As String, Optional ByVal StudentDocId As String = "") As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strPreviousEducationId As String
        Dim strPreviousEduTest As String
        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        Dim strEnrollDate As Date = Date.Now.ToShortDateString
        Try
            'commented by balaji on 09/12/2005
            With sb
                If Not StudentDocId = "" Then
                    .Append(" select  Distinct ")
                    .Append("       R.adReqId as DocumentId, ")
                    .Append("       '(X) ' + R.Descrip as DocumentDescrip, ")
                    .Append("       NULL as ReqGrpId, ")
                    .Append("       NULL as StartDate,  ")
                    .Append("       NULL as EndDate,SD.ModuleId,R.CampGrpId ")
                    .Append(" from ")
                    .Append("  plStudentDocs  SD,(Select Distinct adReqId,Descrip,CampGrpId from adReqs R) R ")
                    .Append("  where SD.DocumentId = R.adReqId ")
                    If Not StudentDocId = "" Then
                        .Append("  and SD.StudentDocId = '" & StudentDocId & "' ")
                    End If
                    .Append("  AND SD.DocumentId not in (Select Distinct adReqId from adReqs WHERE StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ")
                    .Append("  CampGrpId in 	('")
                    .Append(strCampGrpId)
                    .Append(")) ")
                    .Append(" Union ")
                End If
                .Append(" select  Distinct ")
                .Append("       adReqId as DocumentId, ")
                .Append("       Descrip as DocumentDescrip, ")
                .Append("       ReqGrpId, ")
                .Append("       StartDate,  ")
                .Append("       EndDate,ModuleId,CampGrpId ")
                .Append("       from  ")
                .Append("       ( ")
                ' Get Requirement Group and requirements of mandatory requirement
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate,ModuleId,CampGrpId ")
                .Append("      	from  ")
                .Append("      		( ")
                .Append("      		select Distinct ")
                .Append("      				t1.adReqId, ")
                .Append("      				t1.Descrip, ")
                .Append("                   t1.StatusId, ")
                .Append("      				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("      					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("      					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                .Append("      				'" & strEnrollDate & "' as CurrentDate, ")
                .Append("      				t2.StartDate, ")
                .Append("      				t2.EndDate,t1.ModuleId,t1.CampGrpId ")
                .Append("      				from  ")
                .Append("      				adReqs t1, ")
                .Append("      				adReqsEffectiveDates t2 ")
                .Append("      				where  ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" t1.reqforEnrollment=1 and ")
                .Append("      				t1.adReqId = t2.adReqId and ")
                .Append("       				t2.MandatoryRequirement=1 and ")
                .Append("      				t1.adReqTypeId in (3) ")
                .Append("       				) ")
                .Append("       				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("                       and R1.StatusId='" + AdvantageCommonValues.ActiveGuid + "' ")
                .Append("       		union ")

                ' Get The Requirements that was assigned to a program version
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate,ModuleId,CampGrpId ")
                .Append("      				from  ")
                .Append("      					( ")
                .Append("      						select Distinct ")
                .Append("      							t1.adReqId, ")
                .Append("      							t1.Descrip, ")
                .Append("                               t1.StatusId, ")
                .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
                .Append("	      						t2.StartDate, ")
                .Append("	      						t2.EndDate,t1.ModuleId,t1.CampGrpId ")
                .Append("      						from  ")
                .Append("      							adReqs t1, ")
                .Append("      							adReqsEffectiveDates t2, ")
                .Append("      							adReqLeadGroups t3,adPrgVerTestDetails t5 ")
                .Append("      						where ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" t1.reqforEnrollment=1 and ")
                .Append("      							t1.adReqId = t2.adReqId and t2.MandatoryRequirement <> 1 and ")
                .Append("      							t1.adreqTypeId in (3) and ")
                .Append("      							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  ")
                .Append("      							and t1.adReqId = t5.adReqId and t5.PrgVerId in (select distinct prgverId from arStuEnrollments where StudentId='" & StudentId & "') and ")
                '.Append("      					      	t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups s1,arStuEnrollments s2 where ")
                '.Append("	      						s1.StuEnrollId=s2.StuEnrollId and s2.StudentId='" & StudentId & "' ) ")
                .Append("      						 t5.adReqId is not null ")
                .Append("      						) ")
                .Append("      						R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("                           and R1.StatusId='" + AdvantageCommonValues.ActiveGuid + "' ")
                .Append(" union ")
                ' Get Requirements === Requirement Group assigned to Program Version
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate,ModuleId,CampGrpId ")
                .Append("      				from  ")
                .Append("      					( ")
                .Append("      						select Distinct ")
                .Append("      							t1.adReqId, ")
                .Append("      							t1.Descrip, ")
                .Append("                               t1.StatusId, ")
                .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
                .Append("	      						t2.StartDate, ")
                .Append("	      						t2.EndDate,t1.ModuleId,t1.CampGrpId ")
                .Append("      						from  ")
                .Append("      							adReqs t1, ")
                .Append("							    adReqsEffectiveDates t2, ")
                .Append("							    adReqLeadGroups t3, ")
                .Append("							    adPrgVerTestDetails t5, ")
                .Append("							    adReqGrpDef t6, ")
                .Append("						        adLeadByLeadGroups t7,arStuEnrollments t8  ")
                .Append("      					where ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" t1.reqforEnrollment=1 and ")
                .Append("      						t1.adReqId = t2.adReqId and ")
                .Append("      						t1.adreqTypeId in (3) and ")
                .Append("      						t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and ")
                .Append("						    t3.LeadGrpId = t7.LeadGrpId and ")
                .Append("      						t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
                .Append("	      					and t7.StuEnrollId = t8.StuEnrollId and t8.StudentId ='" & StudentId & "'  and  ")
                .Append("						    t5.PrgVerId in (select distinct prgverId from arStuEnrollments where StudentId='" & StudentId & "')   ")
                .Append("      		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("                       and R1.StatusId='" + AdvantageCommonValues.ActiveGuid + "' ")
                .Append(" union ")
                ' Get Requirements that were assigned to lead group but not part of any requirement group
                .Append("	     select  distinct ")
                .Append("	           adReqId, ")
                .Append("	           Descrip, ")
                .Append("	           ReqGrpId, ")
                .Append("	           StartDate, ")
                .Append("	           EndDate,ModuleId,CampGrpId ")
                .Append("           from  ")
                .Append("           ( ")
                ' Get Requirement Group and requirements that are assigned to a lead group
                ' and part of a requirement group
                .Append("          	 select  distinct ")
                .Append("	           	t1.adReqId, ")
                .Append("	           	t1.Descrip, ")
                .Append("               t1.StatusId, ")
                .Append("	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append("	           	'" & strEnrollDate & " ' as CurrentDate,  ")
                .Append("	           	t2.StartDate, ")
                .Append("	           	t2.EndDate,t1.ModuleId,t1.CampGrpId  ")
                .Append("           from ")
                .Append("           	adReqs t1, ")
                .Append("           	adReqsEffectiveDates t2, ")
                .Append("		        adReqLeadGroups t3 ")
                .Append("           where  ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" t1.reqforEnrollment=1 and ")
                .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId ")
                .Append("		and ")
                .Append("		t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId in (select distinct prgverId from arStuEnrollments where StudentId='" & StudentId & "') and s2.adReqId is null) ")
                .Append("      	AND t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups s1,arStuEnrollments s2 where ")
                .Append("	      						s1.StuEnrollId=s2.StuEnrollId and s2.StudentId='" & StudentId & "' ) ")
                .Append("       and   t1.adReqTypeId in (3) and t2.MandatoryRequirement <> 1 and ")
                .Append("		t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId in (select distinct prgverId from arStuEnrollments where StudentId='" & StudentId & "')) ")
                .Append("           ) ")
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("                       and R1.StatusId='" + AdvantageCommonValues.ActiveGuid + "' ")
                .Append(" ) R2 where R2.ModuleId=? ")
                If Not strCampGrpId = "" Then
                    .Append("   AND R2.CampGrpId in 	('")
                    .Append(strCampGrpId)
                    .Append(") ")
                End If
            End With

            db.AddParameter("@ModuleId", ModuleId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Return db.RunParamSQLDataSet(sb.ToString)

            'Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(SingletonAppSettings.AppSettings("ConString")))

            'Dim da1 As New OleDbDataAdapter(sc1)
            'da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)

        Catch ex As Exception
        Finally
        End Try
    End Function

    ''' <summary>
    ''' GetAllTerminationDocuments Return all termination related documentments like R2T4 and Termination Details
    ''' </summary>
    ''' <param name="ModuleId"></param>
    ''' <returns></returns>
    Public Function GetAllTerminationDocuments( ByVal ModuleId As Integer) As DataSet
        Dim db As New DataAccess 
        Dim sb As New StringBuilder
        With sb
            .Append(" SELECT DISTINCT ")
            .Append("      adReqId AS DocumentId, ")
            .Append("      Descrip AS DocumentDescrip ")
            .Append(" FROM adReqs ")
            .Append(" WHERE RequiredForTermination = 1 ")
            .Append(" AND ModuleId=? ")
        End With
        db.AddParameter("@ModuleId", ModuleId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function GetAllExistingStandardDocumentsByPrgVersionEnrollments(ByVal StudentId As String, ByVal ModuleId As Integer, ByVal StudentDocumentId As String, ByVal campusId As String, Optional ByVal includeSubmittedDocumentsThatAreExpired As Boolean = False) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strPreviousEducationId As String
        Dim strPreviousEduTest As String
        Dim strEnrollDate As Date = Date.Now.ToShortDateString
        Dim dsGetCmpGrps As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        Try

            'commented by balaji on 09/12/2005
            With sb
                .Append(" select Distinct R.adReqId as DocumentId,'(X) ' + R.Descrip as DocumentDescrip,NULL as ReqGrpId,NULL as StartDate,NULL as EndDate,SD.ModuleId as ModuleId,CampGrpId ")
                .Append(" from ")
                .Append(" plStudentDocs  SD,(Select adReqId,Descrip,CampGrpId,ModuleID from adReqs) R ")
                .Append(" WHERE SD.DocumentId = R.adReqId And SD.ModuleID=R.ModuleID  ")
                If Not StudentDocumentId = "" Then
                    .Append(" AND SD.StudentDocId='" & StudentDocumentId & "' ")
                End If
                ''Added to fix issue De7213
                .Append(" And SD.ModuleId=?  ")
                .Append("  AND SD.DocumentId not in (Select Distinct adReqId from adReqs WHERE StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ")
                .Append("  CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(")) ")
                .Append(" Union ")
                .Append(" select  distinct ")
                .Append("       adReqId as DocumentId, ")
                .Append("       Descrip as DocumentDescrip, ")
                .Append("       ReqGrpId, ")
                .Append("       StartDate,  ")
                .Append("       EndDate,ModuleId,CampGrpId ")
                .Append("       from  ")
                .Append("       ( ")
                ' Get Requirement Group and requirements of mandatory requirement
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate,ModuleId,CampGrpId ")
                .Append("      	from  ")
                .Append("      		( ")
                .Append("      		select Distinct ")
                .Append("      				t1.adReqId, ")
                .Append("      				t1.Descrip, ")
                .Append("                   t1.StatusId, ")
                .Append("      				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("      					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("      					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                .Append("      				'" & strEnrollDate & "' as CurrentDate, ")
                .Append("      				t2.StartDate, ")
                .Append("      				t2.EndDate,t1.ModuleId,t1.CampGrpId ")
                .Append("      				from  ")
                .Append("      				adReqs t1, ")
                .Append("      				adReqsEffectiveDates t2 ")
                .Append("      				where  ")
                ' ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                '.Append(" t1.reqforEnrollment=1 and ")
                .Append("      				t1.adReqId = t2.adReqId and ")
                .Append("       				t2.MandatoryRequirement=1 and ")
                .Append("      				t1.adReqTypeId in (3) ")
                .Append("       				) ")
                .Append("       				R1 where R1.CurrentDate >= R1.StartDate ")
                .Append("                       and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL ")
                If includeSubmittedDocumentsThatAreExpired Then
                    'We need to bring any mandatory requirements that are no longer effective but that the student had fulfilled
                    .Append("                            or R1.adReqId = (SELECT DocumentId ")
                    .Append("                                              FROM dbo.plStudentDocs SD ")
                    .Append("                                              WHERE SD.StudentDocId = '" & StudentDocumentId & "') ")
                End If
                .Append("   ) ")
                .Append("                       and R1.StatusId='" + AdvantageCommonValues.ActiveGuid + "' ")
                .Append("       		union ")

                ' Get The Requirements that was assigned to a program version
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate,ModuleId,CampGrpId ")
                .Append("      				from  ")
                .Append("      					( ")
                .Append("      						select Distinct ")
                .Append("      							t1.adReqId, ")
                .Append("      							t1.Descrip, ")
                .Append("                               t1.StatusId, ")
                .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
                .Append("	      						t2.StartDate, ")
                .Append("	      						t2.EndDate,t1.ModuleId,t1.CampGrpId ")
                .Append("      						from  ")
                .Append("      							adReqs t1, ")
                .Append("      							adReqsEffectiveDates t2, ")
                .Append("      							adReqLeadGroups t3,adPrgVerTestDetails t5 ")
                .Append("      						where ")
                ' ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                '.Append("t1.reqforEnrollment=1 and ")
                .Append("      							t1.adReqId = t2.adReqId and t2.MandatoryRequirement <> 1 and ")
                .Append("      							t1.adreqTypeId in (3) and ")
                .Append("      							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  ")
                .Append("      							and t1.adReqId = t5.adReqId and t5.PrgVerId in (select distinct prgverId from arStuEnrollments where StudentId='" & StudentId & "')  ")
                '.Append("      					    and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups s1,arStuEnrollments s2 where ")
                '.Append("	      						s1.StuEnrollId=s2.StuEnrollId and s2.StudentId='" & StudentId & "' ) ")
                .Append("      							and t5.adReqId is not null ")
                .Append("      						) ")
                .Append("      						R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL ")
                If includeSubmittedDocumentsThatAreExpired Then
                    'We need to bring any non-mandatory requirements that are no longer effective but that the student had fulfilled
                    .Append("                            or R1.adReqId = (SELECT DocumentId ")
                    .Append("                                              FROM dbo.plStudentDocs SD ")
                    .Append("                                              WHERE SD.StudentDocId = '" & StudentDocumentId & "') ")
                End If
                .Append("   ) ")
                .Append("                           and R1.StatusId='" + AdvantageCommonValues.ActiveGuid + "' ")
                .Append(" union ")
                ' Get Requirements === Requirement Group assigned to Program Version
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate,ModuleId,CampGrpId ")
                .Append("      				from  ")
                .Append("      					( ")
                .Append("      						select Distinct ")
                .Append("      							t1.adReqId, ")
                .Append("      							t1.Descrip, ")
                .Append("                               t1.StatusId, ")
                .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
                .Append("	      						t2.StartDate, ")
                .Append("	      						t2.EndDate,t1.ModuleId,t1.CampGrpId ")
                .Append("      						from  ")
                .Append("      							adReqs t1, ")
                .Append("							    adReqsEffectiveDates t2, ")
                .Append("							    adReqLeadGroups t3, ")
                .Append("							    adPrgVerTestDetails t5, ")
                .Append("							    adReqGrpDef t6, ")
                .Append("						        adLeadByLeadGroups t7,arStuEnrollments t8  ")
                .Append("      					where ")
                ' ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                '.Append(" t1.reqforEnrollment=1 and ")

                .Append("      						t1.adReqId = t2.adReqId and ")
                .Append("      						t1.adreqTypeId in (3) and ")
                .Append("      						t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and ")
                .Append("						    t3.LeadGrpId = t7.LeadGrpId and ")
                .Append("      						t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
                .Append("	      					and t7.StuEnrollId = t8.StuEnrollId and t8.StudentId ='" & StudentId & "'  and  ")
                .Append("						    t5.PrgVerId in (select distinct prgverId from arStuEnrollments where StudentId='" & StudentId & "')   ")
                .Append("      		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL ")
                If includeSubmittedDocumentsThatAreExpired Then
                    'We need to bring any mandatory requirements that are no longer effective but that the student had fulfilled
                    .Append("                            or R1.adReqId = (SELECT DocumentId ")
                    .Append("                                              FROM dbo.plStudentDocs SD ")
                    .Append("                                              WHERE SD.StudentDocId = '" & StudentDocumentId & "') ")
                End If
                .Append("   ) ")
                .Append("                       and R1.StatusId='" + AdvantageCommonValues.ActiveGuid + "' ")
                .Append(" union ")
                ' Get Requirements that were assigned to lead group but not part of any requirement group
                .Append("	     select  distinct ")
                .Append("	           adReqId, ")
                .Append("	           Descrip, ")
                .Append("	           ReqGrpId, ")
                .Append("	           StartDate, ")
                .Append("	           EndDate,ModuleId,CampGrpId ")
                .Append("           from  ")
                .Append("           ( ")
                ' Get Requirement Group and requirements that are assigned to a lead group
                ' and part of a requirement group
                .Append("          	 select  distinct ")
                .Append("	           	t1.adReqId, ")
                .Append("	           	t1.Descrip, ")
                .Append("               t1.StatusId, ")
                .Append("	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append("	           	'" & strEnrollDate & " ' as CurrentDate,  ")
                .Append("	           	t2.StartDate, ")
                .Append("	           	t2.EndDate,t1.ModuleId,t1.CampGrpId  ")
                .Append("           from ")
                .Append("           	adReqs t1, ")
                .Append("           	adReqsEffectiveDates t2, ")
                .Append("		        adReqLeadGroups t3 ")
                .Append("           where  ")
                ' ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                '.Append(" t1.reqforEnrollment=1 and ")
                .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId ")
                .Append("		and ")
                .Append("		t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId in (select distinct prgverId from arStuEnrollments where StudentId='" & StudentId & "') and s2.adReqId is null) ")
                .Append("      	AND t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups s1,arStuEnrollments s2 where ")
                .Append("	    s1.StuEnrollId=s2.StuEnrollId and s2.StudentId='" & StudentId & "' ) ")
                .Append("       and   t1.adReqTypeId in (3) and t2.MandatoryRequirement <> 1 and ")
                .Append("		t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId in (select distinct prgverId from arStuEnrollments where StudentId='" & StudentId & "')) ")
                .Append("           ) ")
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL ")
                If includeSubmittedDocumentsThatAreExpired Then
                    'We need to bring any non-mandatory requirements that are no longer effective but that the student had fulfilled
                    .Append("                            or R1.adReqId = (SELECT DocumentId ")
                    .Append("                                              FROM dbo.plStudentDocs SD ")
                    .Append("                                              WHERE SD.StudentDocId = '" & StudentDocumentId & "') ")
                End If
                .Append("   ) ")
                .Append("                       and R1.StatusId='" + AdvantageCommonValues.ActiveGuid + "' ")
                .Append(" ) R2 where R2.ModuleId=? ")
                If Not strCampGrpId = "" Then
                    .Append("   AND R2.CampGrpId in 	('")
                    .Append(strCampGrpId)
                    .Append(")")
                End If
                .Append("  ORDER BY documentDescrip ")
            End With

            db.AddParameter("@ModuleId", ModuleId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ModuleId", ModuleId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            Return db.RunParamSQLDataSet(sb.ToString)

            'Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(SingletonAppSettings.AppSettings("ConString")))

            'Dim da1 As New OleDbDataAdapter(sc1)
            'da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)

        Catch ex As Exception
        Finally
        End Try
    End Function
    Public Function GetAllExistingStandardDocumentsByEffectiveDatesAndPrgVersion(ByVal LeadId As String, ByVal PrgVerId As String, ByVal LeadDocId As String, ByVal CampusId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strPreviousEducationId As String
        Dim strPreviousEduTest As String
        Dim strEnrollDate As Date = Date.Now.ToShortDateString
        Dim dsGetCmpGrps As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        Try
            'commented by balaji on 09/12/2005
            With sb
                .Append(" select Distinct R.adReqId as DocumentId,'(X) ' + R.Descrip as DocumentDescrip,NULL as ReqGrpId,NULL as StartDate,NULL as EndDate, " + vbCrLf)
                .Append("       NULL as ActualScore, " + vbCrLf)
                .Append("       NULL as TestTaken, " + vbCrLf)
                .Append("       NULL as Comments, " + vbCrLf)
                .Append("       Case when OverRide>=1 then 'True' else 'False' end as OverRide, " + vbCrLf)
                .Append("       NULL as MinScore, " + vbCrLf)
                .Append("       NULL as Required, " + vbCrLf)
                .Append("       NULL as DocSubmittedCount, " + vbCrLf)
                .Append("       NULL as DocStatusDescrip, " + vbCrLf)
                .Append("       NULL as TestTakenCount,NULL as Pass,R.CampGrpId " + vbCrLf)
                .Append(" from " + vbCrLf)
                .Append(" adLeadDocsReceived  SD,(Select adReqId,Descrip,CampGrpId from adReqs) R " + vbCrLf)
                .Append(" WHERE SD.DocumentId = R.adReqId AND SD.LeadDocId='" & LeadDocId & "' " + vbCrLf)
                '.Append("  AND SD.DocumentId not in (Select Distinct adReqId from adReqs WHERE StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND " + vbCrLf)
                .Append(" AND SD.DocumentId not in ")
                .Append(" ( " + vbCrLf)
                .Append(" select  distinct " + vbCrLf)
                .Append("       adReqId as DocumentId " + vbCrLf)
                .Append("       from  " + vbCrLf)
                .Append("       ( " + vbCrLf)
                ' Get Requirement Group and requirements of mandatory requirement
                .Append("       	select  " + vbCrLf)
                .Append("			distinct " + vbCrLf)
                .Append("		       	adReqId " + vbCrLf)
                .Append("      	from  " + vbCrLf)
                .Append("      		( " + vbCrLf)
                .Append("      		select Distinct " + vbCrLf)
                .Append("      				t1.adReqId " + vbCrLf)
                .Append("      				from  " + vbCrLf)
                .Append("      				adReqs t1, " + vbCrLf)
                .Append("      				adReqsEffectiveDates t2 " + vbCrLf)
                .Append("      				where  " + vbCrLf)
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" t1.reqforEnrollment=1 and ")
                .Append("      				t1.adReqId = t2.adReqId and " + vbCrLf)
                .Append("       				t2.MandatoryRequirement=1 and " + vbCrLf)
                .Append("      				t1.adReqTypeId in (3) and t1.StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' " + vbCrLf)
                .Append("       				) " + vbCrLf)
                .Append("       				R1  " + vbCrLf)
                .Append("       		union " + vbCrLf)
                ' Get The Requirements that was assigned to a program version
                .Append("       	select  " + vbCrLf)
                .Append("			distinct " + vbCrLf)
                .Append("		       	adReqId " + vbCrLf)
                .Append("      				from  " + vbCrLf)
                .Append("      					( " + vbCrLf)
                .Append("      						select Distinct " + vbCrLf)
                .Append("      							t1.adReqId " + vbCrLf)
                .Append("      						from  " + vbCrLf)
                .Append("      							adReqs t1, " + vbCrLf)
                .Append("      							adReqsEffectiveDates t2, " + vbCrLf)
                .Append("      							adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 " + vbCrLf)
                .Append("      						where " + vbCrLf)
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" t1.reqforEnrollment=1 and ")
                .Append("      							t1.adReqId = t2.adReqId and t2.MandatoryRequirement <> 1 and " + vbCrLf)
                .Append("      							t1.adreqTypeId in (3) and " + vbCrLf)
                .Append("      							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId " + vbCrLf)
                .Append("      							and t1.adReqId = t5.adReqId and t5.PrgVerId = '" & PrgVerId & "'     " + vbCrLf)
                ''Commented for DE5552
                '.Append("      					     and 		t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where " + vbCrLf)
                '.Append("	      						LeadId = '" & LeadId & "' ) " + vbCrLf)
                .Append("      							and t5.adReqId is not null and t1.StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' " + vbCrLf)
                .Append("      						) " + vbCrLf)
                .Append("      						R1  " + vbCrLf)
                .Append(" union " + vbCrLf)
                ' Get Requirements === Requirement Group assigned to Program Version
                .Append("       	select  " + vbCrLf)
                .Append("			distinct " + vbCrLf)
                .Append("		       	adReqId " + vbCrLf)
                .Append("      				from  " + vbCrLf)
                .Append("      					( " + vbCrLf)
                .Append("      						select Distinct " + vbCrLf)
                .Append("      							t1.adReqId " + vbCrLf)
                .Append("      						from  " + vbCrLf)
                .Append("      							adReqs t1, " + vbCrLf)
                .Append("							adReqsEffectiveDates t2, " + vbCrLf)
                .Append("							adReqLeadGroups t3, " + vbCrLf)
                .Append("							adPrgVerTestDetails t5, " + vbCrLf)
                .Append("							adReqGrpDef t6, " + vbCrLf)
                .Append("						adLeadByLeadGroups t7  " + vbCrLf)
                .Append("      					where " + vbCrLf)
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" t1.reqforEnrollment=1 and ")
                .Append("      						t1.adReqId = t2.adReqId and " + vbCrLf)
                .Append("      						t1.adreqTypeId in (3) and t1.StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' and " + vbCrLf)
                .Append("      						t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and " + vbCrLf)
                .Append("						t3.LeadGrpId = t7.LeadGrpId and " + vbCrLf)
                .Append("      						t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null " + vbCrLf)
                .Append("	      					and t7.LeadId = '" & LeadId & "'  and  " + vbCrLf)
                .Append("						t5.PrgVerId = '" & PrgVerId & "'   " + vbCrLf)
                .Append("      		) R1 " + vbCrLf)
                .Append(" union " + vbCrLf)
                ' Get Requirements that were assigned to lead group but not part of any requirement group
                .Append("	     select  distinct " + vbCrLf)
                .Append("	           adReqId " + vbCrLf)
                .Append("           from  " + vbCrLf)
                .Append("           ( " + vbCrLf)
                ' Get Requirement Group and requirements that are assigned to a lead group
                ' and part of a requirement group
                .Append("          	 select  distinct " + vbCrLf)
                .Append("	           	t1.adReqId " + vbCrLf)
                .Append("           from " + vbCrLf)
                .Append("           	adReqs t1, " + vbCrLf)
                .Append("           	adReqsEffectiveDates t2, " + vbCrLf)
                .Append("		adReqLeadGroups t3 " + vbCrLf)
                .Append("           where  " + vbCrLf)
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" t1.reqforEnrollment=1 and ")
                .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t1.StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' " + vbCrLf)
                .Append("		and " + vbCrLf)
                .Append("		t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId='" & PrgVerId & "' and s2.adReqId is null) " + vbCrLf)
                .Append("		and t3.LeadGrpId in " + vbCrLf)
                .Append("		(select LeadGrpId from adLeadByLeadGroups where LeadId = '" & LeadId & "' ) and " + vbCrLf)
                .Append("           	t1.adReqTypeId in (3) and t2.MandatoryRequirement <> 1 and " + vbCrLf)
                .Append("		t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId='" & PrgVerId & "') " + vbCrLf)
                .Append("           ) " + vbCrLf)
                .Append("           R1 " + vbCrLf)
                .Append(" ) R2 " + vbCrLf)
                .Append(" ) " + vbCrLf)
                .Append(" AND R.CampGrpId in 	('" + strCampGrpId + ")" + vbCrLf)
                .Append(" Union " + vbCrLf)
                .Append(" select  distinct " + vbCrLf)
                .Append("       adReqId as DocumentId, " + vbCrLf)
                .Append("       Descrip as DocumentDescrip, " + vbCrLf)
                .Append("       ReqGrpId, " + vbCrLf)
                .Append("       StartDate,  " + vbCrLf)
                .Append("       EndDate, " + vbCrLf)
                .Append("       ActualScore, " + vbCrLf)
                .Append("       TestTaken, " + vbCrLf)
                .Append("       Comments, " + vbCrLf)
                .Append("       Case when OverRide>=1 then 'True' else 'False' end as OverRide, " + vbCrLf)
                .Append("       Case when (select count(*) from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') >=1 then " + vbCrLf)
                .Append("       (select MinScore from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') " + vbCrLf)
                .Append("       else MinScore end as MinScore, " + vbCrLf)
                .Append("       Required, " + vbCrLf)
                .Append("       DocSubmittedCount, " + vbCrLf)
                .Append("       case when DocStatusDescrip='Approved' then 'Approved' else 'Not Approved' end as DocStatusDescrip, " + vbCrLf)
                .Append("       Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount,Pass,CampGrpId " + vbCrLf)
                .Append("       from  " + vbCrLf)
                .Append("       ( " + vbCrLf)
                ' Get Requirement Group and requirements of mandatory requirement
                .Append("       	select  " + vbCrLf)
                .Append("			distinct " + vbCrLf)
                .Append("		       	adReqId, " + vbCrLf)
                .Append("		      		Descrip, " + vbCrLf)
                .Append("		      		ReqGrpId, " + vbCrLf)
                .Append("		      		StartDate, " + vbCrLf)
                .Append("		      		EndDate, " + vbCrLf)
                .Append("		      		ActualScore, " + vbCrLf)
                .Append("		      		TestTaken, " + vbCrLf)
                .Append("		      		Comments, " + vbCrLf)
                .Append("		      		OverRide, " + vbCrLf)
                .Append("		      		Minscore, " + vbCrLf)
                .Append("		      		Required, " + vbCrLf)
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, " + vbCrLf)
                .Append("		      		DocSubmittedCount, " + vbCrLf)
                .Append("		      		DocStatusDescrip,CampGrpId " + vbCrLf)
                .Append("      	from  " + vbCrLf)
                .Append("      		( " + vbCrLf)
                .Append("      		select Distinct " + vbCrLf)
                .Append("      				t1.adReqId, " + vbCrLf)
                .Append("      				t1.Descrip, " + vbCrLf)
                .Append("      				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then " + vbCrLf)
                .Append("      					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) " + vbCrLf)
                .Append("      					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, " + vbCrLf)
                .Append("      				'" & strEnrollDate & "' as CurrentDate, " + vbCrLf)
                .Append("      				t2.StartDate, " + vbCrLf)
                .Append("      				t2.EndDate, " + vbCrLf)
                .Append("      				(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and " + vbCrLf)
                .Append("      					LeadId = '" & LeadId & "' ) as ActualScore, " + vbCrLf)
                .Append("      				(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and " + vbCrLf)
                .Append("      					LeadId = '" & LeadId & "' ) as TestTaken, " + vbCrLf)
                .Append("      				(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  " + vbCrLf)
                .Append("      					LeadId = '" & LeadId & "' ) as Comments, " + vbCrLf)
                .Append("      				       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and " + vbCrLf)
                .Append("      				        LeadId = '" & LeadId & "' ) as override, " + vbCrLf)
                .Append("      				(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  " + vbCrLf)
                .Append("      					LeadId = '" & LeadId & "'  ) as DocSubmittedCount, " + vbCrLf)
                .Append("      				t2.Minscore, " + vbCrLf)
                .Append("      				1 as Required, " + vbCrLf)
                .Append("      				(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 " + vbCrLf)
                .Append("      				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and " + vbCrLf)
                .Append("      				s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId " + vbCrLf)
                .Append("      				from  " + vbCrLf)
                .Append("      				adReqs t1, " + vbCrLf)
                .Append("      				adReqsEffectiveDates t2 " + vbCrLf)
                .Append("      				where  " + vbCrLf)
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" t1.reqforEnrollment=1 and ")
                .Append("      				t1.adReqId = t2.adReqId and " + vbCrLf)
                .Append("       				t2.MandatoryRequirement=1 and " + vbCrLf)
                .Append("      				t1.adReqTypeId in (3) and t1.StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' " + vbCrLf)
                .Append("       				) " + vbCrLf)
                .Append("       				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) " + vbCrLf)
                .Append("       		union " + vbCrLf)

                ' Get The Requirements that was assigned to a program version
                .Append("       	select  " + vbCrLf)
                .Append("			distinct " + vbCrLf)
                .Append("		       	adReqId, " + vbCrLf)
                .Append("		      		Descrip, " + vbCrLf)
                .Append("		      		ReqGrpId, " + vbCrLf)
                .Append("		      		StartDate, " + vbCrLf)
                .Append("		      		EndDate, " + vbCrLf)
                .Append("		      		ActualScore, " + vbCrLf)
                .Append("		      		TestTaken, " + vbCrLf)
                .Append("		      		Comments, " + vbCrLf)
                .Append("		      		OverRide, " + vbCrLf)
                .Append("		      		Minscore, " + vbCrLf)
                .Append("		      		Required, " + vbCrLf)
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, " + vbCrLf)
                .Append("		      		DocSubmittedCount, " + vbCrLf)
                .Append("		      		DocStatusDescrip,CampGrpId " + vbCrLf)
                .Append("      				from  " + vbCrLf)
                .Append("      					( " + vbCrLf)
                .Append("      						select Distinct " + vbCrLf)
                .Append("      							t1.adReqId, " + vbCrLf)
                .Append("      							t1.Descrip, " + vbCrLf)
                .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, " + vbCrLf)
                .Append("	      						'" & strEnrollDate & "' as CurrentDate, " + vbCrLf)
                .Append("	      						t2.StartDate, " + vbCrLf)
                .Append("	      						t2.EndDate, " + vbCrLf)
                .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and " + vbCrLf)
                .Append("	      							LeadId = '" & LeadId & "' ) as ActualScore, " + vbCrLf)
                .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  " + vbCrLf)
                .Append("	      							LeadId = '" & LeadId & "' ) as TestTaken, " + vbCrLf)
                .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  " + vbCrLf)
                .Append("	      							LeadId = '" & LeadId & "' ) as Comments, " + vbCrLf)
                .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and " + vbCrLf)
                .Append("	      						        LeadId = '" & LeadId & "' ) " + vbCrLf)
                .Append("	      						as override, " + vbCrLf)
                .Append("	      						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  " + vbCrLf)
                .Append("	      						LeadId = '" & LeadId & "'  ) as DocSubmittedCount, " + vbCrLf)
                .Append("		      					t2.Minscore, " + vbCrLf)
                .Append("	      						1 as Required, " + vbCrLf)
                .Append("	      						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 " + vbCrLf)
                .Append("      							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and " + vbCrLf)
                .Append("      							s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId " + vbCrLf)
                .Append("      						from  " + vbCrLf)
                .Append("      							adReqs t1, " + vbCrLf)
                .Append("      							adReqsEffectiveDates t2, " + vbCrLf)
                .Append("      							adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 " + vbCrLf)
                .Append("      						where " + vbCrLf)
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" t1.reqforEnrollment=1 and ")
                .Append("      							t1.adReqId = t2.adReqId and t2.MandatoryRequirement <> 1 and " + vbCrLf)
                .Append("      							t1.adreqTypeId in (3) and " + vbCrLf)
                .Append("      							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId " + vbCrLf)
                .Append("      							and t1.adReqId = t5.adReqId and t5.PrgVerId = '" & PrgVerId & "'     " + vbCrLf)
                ''Commented for DE5552
                '.Append("      					      	and	t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where " + vbCrLf)
                '.Append("	      						LeadId = '" & LeadId & "' ) " + vbCrLf)
                .Append("      							and t5.adReqId is not null and t1.StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' " + vbCrLf)
                .Append("      						) " + vbCrLf)
                .Append("      						R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) " + vbCrLf)
                .Append(" union " + vbCrLf)
                ' Get Requirements === Requirement Group assigned to Program Version
                .Append("       	select  " + vbCrLf)
                .Append("			distinct " + vbCrLf)
                .Append("		       	adReqId, " + vbCrLf)
                .Append("		      		Descrip, " + vbCrLf)
                .Append("		      		ReqGrpId, " + vbCrLf)
                .Append("		      		StartDate, " + vbCrLf)
                .Append("		      		EndDate, " + vbCrLf)
                .Append("		      		ActualScore, " + vbCrLf)
                .Append("		      		TestTaken, " + vbCrLf)
                .Append("		      		Comments, " + vbCrLf)
                .Append("		      		OverRide, " + vbCrLf)
                .Append("		      		Minscore, " + vbCrLf)
                .Append("		      		Required, " + vbCrLf)
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, " + vbCrLf)
                .Append("		      		DocSubmittedCount, " + vbCrLf)
                .Append("		      		DocStatusDescrip,CampGrpId " + vbCrLf)
                .Append("      				from  " + vbCrLf)
                .Append("      					( " + vbCrLf)
                .Append("      						select Distinct " + vbCrLf)
                .Append("      							t1.adReqId, " + vbCrLf)
                .Append("      							t1.Descrip, " + vbCrLf)
                .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, " + vbCrLf)
                .Append("	      						'" & strEnrollDate & "' as CurrentDate, " + vbCrLf)
                .Append("	      						t2.StartDate, " + vbCrLf)
                .Append("	      						t2.EndDate, " + vbCrLf)
                .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and " + vbCrLf)
                .Append("	      							LeadId = '" & LeadId & "' ) as ActualScore, " + vbCrLf)
                .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  " + vbCrLf)
                .Append("	      							LeadId = '" & LeadId & "' ) as TestTaken, " + vbCrLf)
                .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  " + vbCrLf)
                .Append("	      							LeadId = '" & LeadId & "' ) as Comments, " + vbCrLf)
                .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and " + vbCrLf)
                .Append("	      						        LeadId = '" & LeadId & "' ) " + vbCrLf)
                .Append("	      						as override, " + vbCrLf)
                .Append("	      						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  " + vbCrLf)
                .Append("	      						LeadId = '" & LeadId & "'  ) as DocSubmittedCount, " + vbCrLf)
                .Append("		      					t2.Minscore, " + vbCrLf)
                .Append("	      						1 as Required, " + vbCrLf)
                .Append("	      						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 " + vbCrLf)
                .Append("      							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and " + vbCrLf)
                .Append("      							s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId " + vbCrLf)
                .Append("      						from  " + vbCrLf)
                .Append("      							adReqs t1, " + vbCrLf)
                .Append("							adReqsEffectiveDates t2, " + vbCrLf)
                .Append("							adReqLeadGroups t3, " + vbCrLf)
                .Append("							adPrgVerTestDetails t5, " + vbCrLf)
                .Append("							adReqGrpDef t6, " + vbCrLf)
                .Append("						adLeadByLeadGroups t7  " + vbCrLf)
                .Append("      					where " + vbCrLf)
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" t1.reqforEnrollment=1 and ")
                .Append("      						t1.adReqId = t2.adReqId and " + vbCrLf)
                .Append("      						t1.adreqTypeId in (3) and t1.StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' and " + vbCrLf)
                .Append("      						t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and " + vbCrLf)
                .Append("						t3.LeadGrpId = t7.LeadGrpId and " + vbCrLf)
                .Append("      						t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null " + vbCrLf)
                .Append("	      					and t7.LeadId = '" & LeadId & "'  and  " + vbCrLf)
                .Append("						t5.PrgVerId = '" & PrgVerId & "'   " + vbCrLf)
                .Append("      		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) " + vbCrLf)
                .Append(" union " + vbCrLf)
                ' Get Requirements that were assigned to lead group but not part of any requirement group
                .Append("	     select  distinct " + vbCrLf)
                .Append("	           adReqId, " + vbCrLf)
                .Append("	           Descrip, " + vbCrLf)
                .Append("	           ReqGrpId, " + vbCrLf)
                .Append("	           StartDate, " + vbCrLf)
                .Append("	           EndDate, " + vbCrLf)
                .Append("	           ActualScore, " + vbCrLf)
                .Append("	           TestTaken, " + vbCrLf)
                .Append("	           Comments, " + vbCrLf)
                .Append("	           OverRide, " + vbCrLf)
                .Append("	           Minscore, " + vbCrLf)
                .Append("	           Required, " + vbCrLf)
                .Append("	           Case  when ActualScore >= Minscore  then 'True' else 'False' End as Pass, " + vbCrLf)
                .Append("	           DocSubmittedCount, " + vbCrLf)
                .Append("	           DocStatusDescrip,CampGrpId " + vbCrLf)
                .Append("           from  " + vbCrLf)
                .Append("           ( " + vbCrLf)
                ' Get Requirement Group and requirements that are assigned to a lead group
                ' and part of a requirement group
                .Append("          	 select  distinct " + vbCrLf)
                .Append("	           	t1.adReqId, " + vbCrLf)
                .Append("	           	t1.Descrip, " + vbCrLf)
                .Append("	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, " + vbCrLf)
                .Append("	           	'" & strEnrollDate & " ' as CurrentDate,  " + vbCrLf)
                .Append("	           	t2.StartDate, " + vbCrLf)
                .Append("	           	t2.EndDate,  " + vbCrLf)
                .Append("	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and " + vbCrLf)
                .Append("	           		LeadId = '" & LeadId & "' ) as ActualScore,  " + vbCrLf)
                .Append("	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and " + vbCrLf)
                .Append("	           		LeadId = '" & LeadId & "' ) as TestTaken,  " + vbCrLf)
                .Append("	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and " + vbCrLf)
                .Append("	           		LeadId = '" & LeadId & "' ) as Comments,  " + vbCrLf)
                .Append("	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and " + vbCrLf)
                .Append("	                              LeadId = '" & LeadId & "' ) " + vbCrLf)
                .Append("	                      as override, " + vbCrLf)
                .Append("	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and " + vbCrLf)
                .Append("	           		LeadId = '" & LeadId & "' ) as DocSubmittedCount, " + vbCrLf)
                .Append("	           	t2.Minscore, " + vbCrLf)
                .Append("			ISNULL(t3.IsRequired,0) as Required, " + vbCrLf)
                .Append("	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 " + vbCrLf)
                .Append("	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and " + vbCrLf)
                .Append("	           s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId " + vbCrLf)
                .Append("           from " + vbCrLf)
                .Append("           	adReqs t1, " + vbCrLf)
                .Append("           	adReqsEffectiveDates t2, " + vbCrLf)
                .Append("		adReqLeadGroups t3 " + vbCrLf)
                .Append("           where  " + vbCrLf)
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" t1.reqforEnrollment=1 and ")
                .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t1.StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' " + vbCrLf)
                .Append("		and " + vbCrLf)
                .Append("		t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId='" & PrgVerId & "' and s2.adReqId is null) " + vbCrLf)
                .Append("		and t3.LeadGrpId in " + vbCrLf)
                .Append("		(select LeadGrpId from adLeadByLeadGroups where LeadId = '" & LeadId & "' ) and " + vbCrLf)
                .Append("           	t1.adReqTypeId in (3) and t2.MandatoryRequirement <> 1 and " + vbCrLf)
                .Append("		t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId='" & PrgVerId & "') " + vbCrLf)
                .Append("           ) " + vbCrLf)
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) " + vbCrLf)
                .Append(" ) R2 " + vbCrLf)
                If Not strCampGrpId = "" Then
                    .Append("   WHERE R2.CampGrpId in 	('" + strCampGrpId + ")" + vbCrLf)
                End If
            End With
            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As Exception
        Finally
        End Try
    End Function
    Public Function CheckIfReqGroupMeetsConditions(ByVal LeadId As String, ByVal PrgVerId As String, Optional ByVal CampusId As String = "") As String
        Dim sb As New StringBuilder
        Dim strEnrollDate As Date = Date.Now.ToShortDateString
        Dim dsGetCmpGrps As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        With sb
            .Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,R5.TestAttempted+R5.DocsAttempted+R5.TestOverRidden+R5.DocumentOverRidden as AttemptedReqs ")
            .Append(" from ")
            .Append(" (  ")
            ' Requiremt group assigned to a program version
            .Append("	Select  ")
            .Append("		Distinct ")
            .Append("			t1.ReqGrpId, ")
            .Append("			t2.Descrip,t2.CampGrpId, ")
            .Append("			t3.NumReqs as Numreqs,  ")
            .Append("	            	    ( ")
            .Append("       	     		select Count(*) as TestAttempted ")
            .Append("            				from ")
            .Append("			            		( ")
            .Append("            						select Distinct ")
            .Append("								A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("       	     						adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append("            						where ")
            .Append("				            			A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and ")
            ''Added by Saraswathi Lakshmanan on Sept 15 2010
            ''To filter only the documents marked as required for Enrollment
            ''For Document tracking
            .Append("                  A2.ReqforEnrollment=1 and ")

            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("								and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append("       		     					A4.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            .Append("		            					ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (1) and A2.StatusId='" & strActiveGUID & "' ")
            .Append("	       			     	) ")
            .Append("					R1,adLeadEntranceTest R2 ")
            .Append("				where ")
            .Append("					R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "'")
            .Append("			            	and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 ")
            .Append("                       and R2.Pass=1 and R2.EntrTestId not in (select Distinct EntrTestId from adEntrTestOverRide where LeadId='" & LeadId & "' and OverRide=1) and ")
            .Append("					(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("	      	        )as TestAttempted, ")
            .Append("	               ( ")
            .Append("		            	select Count(*) as DocsAttempted ")
            .Append("       			     	from ")
            .Append("			            		( ")
            .Append("			            			select Distinct ")
            .Append("								A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("            							adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append("			            			where ")
            .Append("	            						A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            ''Added by Saraswathi Lakshmanan on Sept 15 2010
            ''To filter only the documents marked as required for Enrollment
            ''For Document tracking
            .Append("                   A2.ReqforEnrollment=1  and ")

            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("			       		     		and A4.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            .Append("            							ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (3) and A2.StatusId='" & strActiveGUID & "' ")
            .Append("			            		) ")
            .Append("            				R1,adLeadDocsReceived R2,syDocStatuses R3 ")
            .Append("				where ")
            .Append("					    R1.adReqId = R2.DocumentId and R2.LeadId='" & LeadId & "'")
            .Append("                       and R2.DocumentId not in (select Distinct EntrTestId from adEntrTestOverRide where LeadId='" & LeadId & "' and OverRide=1) ")
            .Append("                       and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 ")
            .Append("		            	and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("            		) as DocsAttempted,  ")
            .Append(" (  ")
            .Append("     select Count(*) as OverRideTestAttempted ")
            .Append(" from ")
            .Append(" (  ")
            .Append(" select Distinct ")
            .Append("	A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" from ")
            .Append(" 	adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where ")
            .Append(" A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and ")
            ''Added by Saraswathi Lakshmanan on Sept 15 2010
            ''To filter only the documents marked as required for Enrollment
            ''For Document tracking
            .Append("                   A2.ReqforEnrollment=1  and ")
            .Append(" A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" and A4.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            .Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (1) and A2.StatusId='" & strActiveGUID & "' ")
            .Append(" )  ")
            .Append(" R1,adEntrTestOverRide R2 where ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.OverRide=1 and ")
            .Append(" R2.LeadId='" & LeadId & "' and R1.CurrentDate >= R1.StartDate and ")
            .Append(" (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)  ")
            '.Append(" and R1.adReqId not in (select Distinct EntrTestId from adLeadEntranceTest where LeadId='" & LeadId & "') ")
            .Append(" ) as TestOverRidden, ")
            .Append(" (  ")
            .Append("     select Count(*) as OverRideDocumentAttempted ")
            .Append(" from ")
            .Append(" (  ")
            .Append(" select Distinct ")
            .Append("	A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" from ")
            .Append(" 	adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where ")
            .Append(" A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and ")
            ''Added by Saraswathi Lakshmanan on Sept 15 2010
            ''To filter only the documents marked as required for Enrollment
            ''For Document tracking
            .Append("                   A2.ReqforEnrollment=1  and ")
            .Append(" A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" and A4.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            .Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (3) and A2.StatusId='" & strActiveGUID & "' ")
            .Append(" )  ")
            .Append(" R1,adEntrTestOverRide R2 where ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.OverRide=1 and ")
            .Append(" R2.LeadId='" & LeadId & "' and R1.CurrentDate >= R1.StartDate and ")
            .Append(" (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)  ")
            '.Append(" and R1.adReqId not in (select Distinct DocumentId from adLeadDocsReceived where LeadId='" & LeadId & "') ")
            .Append(" ) as DocumentOverRidden ")
            .Append("      	from ")
            .Append("			adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3, syStatuses t10  ")
            .Append("       where  ")
            .Append(" 	              t1.ReqGrpId = t2.ReqGrpId and ")
            .Append("			t2.ReqGrpId = t3.ReqGrpId and t2.ReqGrpId not in (select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) and ")
            .Append("			t3.StatusId = t10.StatusId and t10.Status='Active' and")
            .Append("			t1.PrgVerId='" & PrgVerId & "'  ")
            ''Commented for De5552
            '.Append("                and     t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups ")
            '.Append("			where LeadId='" & LeadId & "') ")
            .Append(" union ")
            ' Get Mandatory requirement group
            .Append("      select ")
            .Append("              	'CFD29DFD-4FFF-400B-B152-4FABB12E6E7B' as ReqGrpId, ")
            .Append("               case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then (select Descrip from adReqGroups where IsMandatoryReqGrp=1) else 'School Level Requirements' end  as Descrip, ")
            .Append("               t1.CampGrpId, ")
            .Append("             			( ")
            .Append("              			select Count(*) as NumReqs ")
            .Append("              			from ")
            .Append("      			 			( ")
            .Append("              					select Distinct ")
            .Append("								A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("              						adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            .Append("					              	 A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1 and ")
            ''Added by Saraswathi Lakshmanan on Sept 15 2010
            ''To filter only the documents marked as required for Enrollment
            ''For Document tracking
            .Append("                   A2.ReqforEnrollment=1  and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("					                and A2.adreqTypeId in (1,3) and A2.StatusId='" & strActiveGUID & "' ")
            .Append("              				) ")
            .Append("              				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("                			) ")
            .Append("              		as NumReqs, ")
            .Append("              		( ")
            .Append("              			select Count(*) as TestAttempted ")
            .Append("              			from ")
            .Append("              				( ")
            .Append("              					select Distinct ")
            .Append("								A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("						              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            .Append("					              	 A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1 and ")
            ''Added by Saraswathi Lakshmanan on Sept 15 2010
            ''To filter only the documents marked as required for Enrollment
            ''For Document tracking
            .Append("                   A2.ReqforEnrollment=1  and ")

            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("					                and A2.adreqTypeId in (1) and A2.StatusId='" & strActiveGUID & "' ")
            .Append("				              ) ")
            .Append("			              R1,adLeadEntranceTest R2 ")
            .Append("					where ")
            .Append("						R1.adReqId = R2.EntrTestId and ")
            .Append("						R2.LeadId='" & LeadId & "'")
            .Append("	              		and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 ")
            .Append("                       and R2.Pass=1 and R2.EntrTestId not in (select Distinct EntrTestId from adEntrTestOverRide where LeadId='" & LeadId & "' and OverRide=1) and ")
            .Append("						(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("              		) as TestAttempted, ")
            .Append("             			( ")
            .Append(" 			              select Count(*) as DocsAttempted ")
            .Append(" 			              from ")
            .Append(" 				           ( ")
            .Append(" 				              select Distinct ")
            .Append(" 							A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" 						from  ")
            .Append(" 					              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            .Append("					              	 A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1 and ")
            ''Added by Saraswathi Lakshmanan on Sept 15 2010
            ''To filter only the documents marked as required for Enrollment
            ''For Document tracking
            .Append("                   A2.ReqforEnrollment=1  and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("					                and A2.adreqTypeId in (3) and A2.StatusId='" & strActiveGUID & "' ")
            .Append(" 					      ) ")
            .Append(" 		              	R1,adLeadDocsReceived R2,syDocStatuses R3 ")
            .Append(" 					where 	")
            .Append(" 						R1.adReqId = R2.DocumentId and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 and ")
            .Append(" 						R2.LeadId='" & LeadId & "' ")
            .Append("                       and R2.DocumentId not in (select Distinct EntrTestId from adEntrTestOverRide where LeadId='" & LeadId & "' and OverRide=1)  ")

            .Append(" 				              and R1.CurrentDate >= R1.StartDate and ")
            .Append(" 						(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" 		              ) as DocsAttempted, ")
            .Append(" (  ")
            .Append("     select Count(*) as OverRideTestAttempted ")
            .Append(" from ")
            .Append(" (  ")
            .Append(" 				              select Distinct ")
            .Append(" 							A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" 						from  ")
            .Append(" 					              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            .Append("					              	 A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1 and ")
            ''Added by Saraswathi Lakshmanan on Sept 15 2010
            ''To filter only the documents marked as required for Enrollment
            ''For Document tracking
            .Append("                   A2.ReqforEnrollment=1  and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("					                and A2.adreqTypeId in (1) and A2.StatusId='" & strActiveGUID & "' ")
            .Append(" )  ")
            .Append(" R1,adEntrTestOverRide R2 where ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.OverRide=1 and ")
            .Append(" R2.LeadId='" & LeadId & "' and R1.CurrentDate >= R1.StartDate and ")
            .Append(" (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)  ")
            '.Append(" and R1.adReqId not in (select EntrTestId from adLeadEntranceTest where LeadId='" & LeadId & "') ")
            .Append(" ) as TestOverRidden, ")
            .Append(" (  ")
            .Append("     select Count(*) as OverRideDocumentAttempted ")
            .Append(" from ")
            .Append(" (  ")
            .Append(" 				              select Distinct ")
            .Append(" 							A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" 						from  ")
            .Append(" 					              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            .Append("					              	 A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1  and ")
            ''Added by Saraswathi Lakshmanan on Sept 15 2010
            ''To filter only the documents marked as required for Enrollment
            ''For Document tracking
            .Append("                   A2.ReqforEnrollment=1  and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("					                and A2.adreqTypeId in (3) and A2.StatusId='" & strActiveGUID & "' ")
            .Append(" )  ")
            .Append(" R1,adEntrTestOverRide R2 where ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.OverRide=1 and ")
            .Append(" R2.LeadId='" & LeadId & "' and R1.CurrentDate >= R1.StartDate and ")
            .Append(" (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)  ")
            '.Append(" and R1.adReqId not in (select Distinct DocumentId from adLeadDocsReceived where LeadId='" & LeadId & "') ")
            .Append(" ) as DocumentOverRidden ")
            .Append("       from ")
            .Append(" 		     adReqs t1,adReqsEffectiveDates t2  ")
            .Append(" 	where t1.adReqId = t2.adReqId and t2.MandatoryRequirement=1 and t1.adreqTypeId in (1,3) ")
            ''Added by Saraswathi Lakshmanan on Sept 15 2010
            ''To filter only the documents marked as required for Enrollment
            ''For Document tracking
            .Append("          and         t1.ReqforEnrollment=1   ")

            .Append("    ) R5  ")
            If Not strCampGrpId = "" Then
                .Append("   WHERE R5.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With

        '' Grab the Categories and Products table
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim dr As DataRow

        ds = db.RunParamSQLDataSet(sb.ToString)

        Dim strMessage As String
        Dim intRequiredReqs As Integer
        Dim intAttemptedReqs As Integer

        For Each dr In ds.Tables(0).Rows
            intRequiredReqs = dr("NumReqs")
            intAttemptedReqs = dr("AttemptedReqs")
            If intAttemptedReqs < intRequiredReqs Then
                strMessage = "Not Satisfied"
                Return "Not Enroll"
                Exit Function
            End If
            Dim sbCheckIfRequiredRequirementExistsWithInGroup As New StringBuilder
            Dim intReqExistsCount As Integer = 0
            With sbCheckIfRequiredRequirementExistsWithInGroup
                'If count is 0 then all the required requirements with in a group are satisfied.
                .Append(" select Count(*) from adReqGrpDef where ReqGrpId=? ")
                .Append(" and LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId=?) ")
                .Append(" and IsRequired=1 and  ")
                .Append(" ( ")
                'Test should have been failed
                .Append(" adReqId in (select distinct EntrTestId from adLeadEntranceTest where LeadId=? ")
                .Append(" and Pass=0 and EntrTestId not in (select Distinct EntrTestId from adEntrTestOverride where ")
                .Append(" LeadId=? and override=1)) ")
                .Append(" or  ")
                'or document should be not approved
                .Append(" adReqId in (select distinct DocumentId from adLeadDocsReceived t1,syDocStatuses t2 where ")
                .Append(" t1.LeadId=? and t1.DocStatusId = t2.DocStatusId and ")
                .Append(" t2.SysDocStatusId <> 1 and DocumentId not in ")
                .Append(" (select EntrTestId from adEntrTestOverride where LeadId=? ")
                .Append(" and override=1)) ")
                ''Commented by Saraswathi Lakshmanan on March 15 2011, while testing enroll leads for AMC by Sri,
                ''The Requirement group was mapped to a program version and it did not allow the student to be enrolled even though the requirements were approved.
                '.Append(" or ")
                '' or requirement should not be overriden
                '.Append(" adReqId in (select Distinct EntrTestId from adEntrTestOverride where LeadId=? ")
                '.Append(" and override=0) ")
                .Append(" ) ")
            End With
            db.ClearParameters()
            db.AddParameter("@ReqGrpId", CType(dr("ReqGrpId"), Guid).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                intReqExistsCount = db.RunParamSQLScalar(sbCheckIfRequiredRequirementExistsWithInGroup.ToString)
                'If count is >=1 then there is a requirement with in this group that has been marked as required 
                'but has not been satisfied or overriden
                If intReqExistsCount >= 1 Then
                    Return "Not Enroll"
                    Exit Function
                End If
            Catch ex As Exception
                strMessage = ""
            End Try
        Next
        If Not strMessage = "" Then
            Return "Not Enroll"
        Else
            Return "Enroll"
        End If
    End Function
    Public Function GetPendingDocsByStudentandPrgVersion(ByVal StudentId As String, ByVal campusId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strPreviousEducationId As String
        Dim strPreviousEduTest As String
        Dim strEnrollDate As Date = Date.Now.ToShortDateString
        Dim dsGetCmpGrps As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusId)
        Dim strCampGrpId As String = ""
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        'Modified by Michelle R. Rodriguez, 07/20/2006
        '   1. Subquery retrieving mandatory requirements and requirements within mandatory req. groups 
        '      has been modified.
        '   2. Subquery retrieving requirements and requirements within req. groups 
        '      assigned to a Program Version has been modified.
        '   3. In addition, the req. group description and NumReq are also retrieved.
        Try
            With sb
                .Append(" select  distinct ")
                .Append("                       adReqId as DocumentId, ")
                .Append("                       Descrip as DocumentDescrip, ")
                .Append("                       ReqGrpId, ")
                .Append("                       ReqGrpDescrip,")
                .Append("                       NumRequired, ")
                .Append("                       StartDate,  ")
                .Append("                       EndDate, ")
                .Append("			            Required, ")
                .Append("                       DocSubmittedCount,  ")
                .Append(" ReqforEnrollment, ReqforFinancialAid, ReqforGraduation, ")
                .Append("                       case when DocStatusDescrip='Approved' then 'Approved' else 'Not Approved' end as DocStatusDescrip ")
                .Append("                       from  ")
                .Append("                       ( ")
                '--Mandatory Requirements and requirements from Mandatory Requirement Groups
                .Append("					        select distinct ")
                .Append("           					        adReqId, ")
                .Append("							            Descrip, ")
                .Append("							            ReqGrpId, ")
                .Append("                                       ReqGrpDescrip,")
                .Append("                                       NumRequired, ")
                .Append("							            StartDate, ")
                .Append("							            EndDate, ")
                .Append("							            Required, ")
                .Append("							            DocSubmittedCount, ")
                .Append("							            DocStatusDescrip ")
                .Append(" ,ReqforEnrollment, ReqforFinancialAid, ReqforGraduation,CampGrpId ")
                .Append("                  	        from  ( ")
                '							                --Mandatory Requirements")
                .Append("							        select Distinct ")
                .Append("								                t1.adReqId, ")
                .Append("								                t1.Descrip, ")
                .Append("								                '00000000-0000-0000-0000-000000000000' AS ReqGrpId, ")
                .Append("								                '' AS ReqGrpDescrip,")
                .Append("								                0 AS NumRequired,")
                .Append("								                '" & strEnrollDate & "' as CurrentDate, ")
                .Append("								                t2.StartDate, ")
                .Append("								                t2.EndDate, ")
                .Append("								                DocSubmittedCount=(select Count(*) from plStudentDocs ")
                .Append("													                where DocumentId=t1.adReqId  ")
                .Append("   													            and StudentId='" & StudentId & "'), ")
                .Append("								                t2.Minscore, ")
                .Append("								                1 AS Required, 	")
                .Append("								                DocStatusDescrip=(select Distinct s1.DocStatusDescrip ")
                .Append("													                from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
                .Append("													                where s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId  ")
                .Append("															        and s3.StudentId='" & StudentId & "' and s3.DocumentId = t1.adReqId) ")
                .Append(" ,ReqforEnrollment, ReqforFinancialAid, ReqforGraduation,t1.CampGrpId ")
                .Append("							        from  adReqs t1,adReqsEffectiveDates t2 ")
                .Append("							        where   t1.adReqId = t2.adReqId and ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                '     .Append(" t1.reqforEnrollment=1 and ")

                .Append("								            t2.MandatoryRequirement=1 and ")
                .Append("								            t1.adReqTypeId in (3)   and t1.StatusId='" & strActiveGUID & "' ")
                .Append("							        UNION ")
                '                                           --Requirements from mandatory requirement Groups")
                .Append("							        select Distinct ")
                .Append("								                t3.adReqId, ")
                .Append("								                t4.Descrip, ")
                .Append("								                t1.ReqGrpId, ")
                .Append("								                t1.Descrip AS ReqGrpDescrip,")
                .Append("								                NumRequired=(select count(*) from adReqGrpDef where ReqGrpId=t1.ReqGrpId),")
                .Append("								                '" & strEnrollDate & "' as CurrentDate, ")
                .Append("								                t2.StartDate, ")
                .Append("								                t2.EndDate, ")
                .Append("								                DocSubmittedCount=(select Count(*) from plStudentDocs ")
                .Append("													                where DocumentId=t3.adReqId  ")
                .Append("   													            and StudentId='" & StudentId & "'), ")
                .Append("								                t2.Minscore, ")
                .Append("								                1 AS Required, 	")
                .Append("								                DocStatusDescrip=(select Distinct s1.DocStatusDescrip ")
                .Append("													                from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
                .Append("													                where s1.SysDocStatusId=s2.sysDocStatusId and s2.DocStatusId=s3.DocStatusId  ")
                .Append("													                and s3.StudentId='" & StudentId & "' and s3.DocumentId=t3.adReqId) ")
                .Append(" ,ReqforEnrollment, ReqforFinancialAid, ReqforGraduation,t4.CampGrpId ")
                .Append("							        from  adReqGroups t1,adReqGrpDef t3,adReqs t4,adReqsEffectiveDates t2 ")
                .Append("							        where   t1.IsMandatoryReqGrp=1 ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                ''' Code modified by kamalesh ahuja to resolve Rally issue id DE1073
                ''' .Append(" t4.reqforEnrollment=1 and")
                '  .Append(" and  t4.reqforEnrollment=1 ")
                '''''''''
                .Append("								            and t1.ReqGrpId=t3.ReqGrpId")
                .Append("								            and t3.adReqId=t2.adReqId")
                .Append("								            and t3.adReqId=t4.adReqId")
                .Append("								            and t4.adReqTypeId in (3)   and t4.StatusId='" & strActiveGUID & "' ")
                .Append("	                              )R1 ")
                .Append("					        where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")

                ' '' Get Requirement Group and requirements of mandatory requirement
                ''.Append("                       	select  ")
                ''.Append("                			distinct ")
                ''.Append("                       			adReqId, ")
                ''.Append("			                     Descrip, ")
                ''.Append("			                     ReqGrpId, ")
                ''.Append("			                     StartDate, ")
                ''.Append("			                     EndDate, ")
                ''.Append("						Required, ")
                ''.Append("			                     DocSubmittedCount, ")
                ''.Append("			                     DocStatusDescrip ")
                ''.Append("                      	from  ")
                ''.Append("		                      ( ")
                ''.Append("              			        select Distinct ")
                ''.Append("				                      t1.adReqId, ")
                ''.Append("				                      t1.Descrip, ")
                ''.Append("				                      case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                ''.Append("                      					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                ''.Append("				                      	else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                ''.Append("					                      '" & strEnrollDate & "' as CurrentDate, ")
                ''.Append("					                      t2.StartDate, ")
                ''.Append("					                      t2.EndDate, ")
                ''.Append("					                      (select Count(*) from plStudentDocs where DocumentId=t1.adReqId and  ")
                ''.Append("				                       	 StudentId =  '" & StudentId & "'  ) as DocSubmittedCount, ")
                ''.Append("					                      t2.Minscore, ")
                ''.Append("					                      1 as Required, ")
                ''.Append("					                      (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
                ''.Append("					                      where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                ''.Append("					                      s3.StudentId = '" & StudentId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                ''.Append("			                      from  ")
                ''.Append("                     				 adReqs t1, ")
                ''.Append("				                      adReqsEffectiveDates t2 ")
                ''.Append("			                      where  ")
                ''.Append("                     				 t1.adReqId = t2.adReqId and ")
                ''.Append("				                      t2.MandatoryRequirement=1 and ")
                ''.Append("				                      t1.adReqTypeId in (3) ")
                ''.Append("		                       ) ")
                ''.Append("              		         R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")

                .Append("                       union ")

                '                               --Requirement assigned to a program version are considered 'Required'")
                .Append("					    select distinct ")
                .Append("        						    adReqId, ")
                .Append("               	       			Descrip, ")
                .Append(" 	              				    ReqGrpId,")
                .Append("                                   ReqGrpDescrip,")
                .Append("                                   NumRequired, ")
                .Append("        	              		    StartDate, ")
                .Append(" 	              				    EndDate, ")
                .Append(" 			   					    Required, ")
                .Append(" 	       						    DocSubmittedCount, ")
                .Append("        	       				    DocStatusDescrip ")
                .Append(" ,ReqforEnrollment, ReqforFinancialAid, ReqforGraduation,CampGrpId ")
                .Append("					    from  ( ")
                '                                       --Requirements assigned to program version")
                .Append("							    select Distinct ")
                .Append(" 										    t1.adReqId, ")
                .Append("										    t1.Descrip, ")
                .Append("										    '00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("								            '' AS ReqGrpDescrip,")
                .Append("								            0 AS NumRequired,")
                .Append("										    '" & strEnrollDate & "' as CurrentDate, ")
                .Append("										    t2.StartDate, ")
                .Append("										    t2.EndDate, ")
                .Append("										    DocSubmittedCount=(select Count(*) ")
                .Append("															    from plStudentDocs ")
                .Append("															    where DocumentId=t1.adReqId ")
                .Append("       														and StudentId='" & StudentId & "'), ")
                .Append("										    t2.Minscore, ")
                .Append("  										    1 as Required, ")
                .Append("  										    DocStatusDescrip=(select Distinct s1.DocStatusDescrip ")
                .Append("															    from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
                .Append("															    where s1.SysDocStatusId=s2.sysDocStatusId ")
                .Append("															    and s2.DocStatusId=s3.DocStatusId ")
                .Append("															    and s3.StudentId='" & StudentId & "' ")
                .Append("															    and s3.DocumentId=t1.adReqId) ")
                .Append(" ,ReqforEnrollment, ReqforFinancialAid, ReqforGraduation,t1.CampGrpId ")
                .Append("							    from  adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
                .Append("							    where   t1.adReqId = t2.adReqId and t2.MandatoryRequirement <> 1   and t1.StatusId='" & strActiveGUID & "' ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                ''' Code modified by kamalesh ahuja to resolve Rally issue id DE1073
                ''.Append(" t1.reqforEnrollment=1 and ")
                '  .Append(" and t1.reqforEnrollment=1 ")
                '''''''
                .Append("								        and t1.adreqTypeId in (3) and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId ")
                .Append("								        and t4.PrgVerId=t5.PrgVerId and t1.adReqId=t5.adReqId ")
                .Append("								        and t5.PrgVerId in (select distinct PrgVerId from arStuEnrollments where StudentId='" & StudentId & "')  ")
                ''Commented for De5552
                '.Append("								        and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups ")
                '.Append("														        where StuEnrollId in (select StuEnrollId from arStuEnrollments where StudentId='" & StudentId & "')) ")
                .Append("								        and t5.adReqId is not null ")
                .Append("                              )R1 ")
                .Append("				        where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")

                .Append("				        UNION ")

                '                               --Requirements from requirement groups assigned to a program version.")
                '				                --If the number of satisfied requirements is less than the required number,")
                '				                --mark the requirement as 'Required'. Otherwise, it is not required.")
                .Append("                       select distinct ")
                .Append("                   			    adReqId, ")
                .Append("							        Descrip, ")
                .Append("							        ReqGrpId, ")
                .Append("                                   ReqGrpDescrip,")
                .Append("                                   NumRequired, ")
                .Append("							        StartDate, ")
                .Append("							        EndDate, ")
                .Append("							        Required, ")
                .Append("							        DocSubmittedCount, ")
                .Append("							        DocStatusDescrip ")
                .Append(" ,ReqforEnrollment, ReqforFinancialAid, ReqforGraduation,CampGrpId ")
                .Append("				        from  (")
                '               						--Requirements from requirement groups assigned to program version")
                .Append("						        select Distinct ")
                .Append(" 									        t13.adReqId, ")
                .Append("									        t1.Descrip, ")
                .Append("									        t11.ReqGrpId, ")
                .Append("									        t11.Descrip AS ReqGrpDescrip, ")
                .Append("									        Z.NumReqs AS NumRequired, ")
                .Append("									        '" & strEnrollDate & "' as CurrentDate, ")
                .Append("									        t2.StartDate, ")
                .Append("									        t2.EndDate,")
                .Append("									        DocSubmittedCount=(select Count(*) ")
                .Append("														        from plStudentDocs ")
                .Append("														        where DocumentId=t13.adReqId ")
                .Append("       													    and StudentId='" & StudentId & "'), ")
                .Append("									        t2.Minscore, ")

                ''Modified by Saraswathi lakshmanan on jan 19 2011
                ''To fix Rally case De1231 QA: Issues with the entrance test and documents section when associated with a requirements group on the leads and students side.

                .Append("  									        IsNUll(t13.IsRequired,0) AS Required,  ")
                .Append(" 									        DocStatusDescrip=(select Distinct s1.DocStatusDescrip ")
                .Append("														        from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
                .Append("														        where s1.SysDocStatusId=s2.sysDocStatusId ")
                .Append("														        and s2.DocStatusId=s3.DocStatusId ")
                .Append("														        and s3.StudentId='" & StudentId & "' ")
                .Append("														        and s3.DocumentId=t13.adReqId) ")
                .Append(" ,ReqforEnrollment, ReqforFinancialAid, ReqforGraduation,t1.CampGrpId ")
                .Append("						        from	adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5,")
                .Append("								        adReqGroups t11,adReqGrpDef t13,")
                .Append("								        (select LeadGrpId,ReqGrpId,NumReqs from adLeadGrpReqGroups where LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups ")
                .Append("													where StuEnrollId in (select StuEnrollId from arStuEnrollments where StudentId='" & StudentId & "')))Z")
                .Append("						        where   t11.ReqGrpId=t5.ReqGrpId   and t1.StatusId='" & strActiveGUID & "' ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                ''' Code modified by kamalesh ahuja to resolve Rally issue id DE1073
                ''.Append(" t1.reqforEnrollment=1 and ")
                '   .Append(" and t1.reqforEnrollment=1  ")
                '''''''''
                .Append("							            and t5.adReqId is null ")
                .Append("							            and t5.PrgVerId in (select distinct PrgVerId from arStuEnrollments where StudentId='" & StudentId & "')  ")
                .Append("							            and t4.PrgVerId=t5.PrgVerId")
                .Append("							            and t11.ReqGrpId=t13.ReqGrpId")
                .Append("							            and t13.adReqId=t2.adReqId")
                .Append("							            and t2.MandatoryRequirement <> 1 ")
                .Append("							            and t13.adReqId = t1.adReqId ")
                .Append("							            and t1.adreqTypeId in (3) and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId ")
                .Append("							            and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups ")
                .Append("													            where StuEnrollId in (select StuEnrollId from arStuEnrollments where StudentId='" & StudentId & "')) ")
                .Append("							            and Z.LeadGrpId=t3.LeadGrpId")
                .Append("							            and	Z.ReqGrpId=t11.ReqGrpId    ")
                ''Added by Saraswathi Lakshmanan to fix an error while running this page. The adreqID was duplicated with different isrequired.fixed on Feb 2 2011
                .Append("   AND t13.LeadGrpId= Z.LeadGrpId ")
                .Append("				                ) R1 ")
                .Append("				            where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")


                '' '' Get The Requirements that was assigned to a program version
                ' ''.Append("                      select  ")
                ' ''.Append(" 	                	distinct ")
                ' ''.Append("        	                	adReqId, ")
                ' ''.Append("               	       	Descrip, ")
                ' ''.Append(" 	              	       ReqGrpId, ")
                ' ''.Append("        	              	StartDate, ")
                ' ''.Append(" 	              	       EndDate, ")
                ' ''.Append(" 			   	       Required, ")
                ' ''.Append(" 	       	              DocSubmittedCount, ")
                ' ''.Append("        	       	       DocStatusDescrip ")
                ' ''.Append("                       from  ")
                ' ''.Append("                       	( ")
                ' ''.Append("               	        select Distinct ")
                ' ''.Append("                      	 	t1.adReqId, ")
                ' ''.Append("                       		t1.Descrip, ")
                ' ''.Append(" 	                	      '00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                ' ''.Append("        	         	      '" & strEnrollDate & "' as CurrentDate, ")
                ' ''.Append("               	  	      t2.StartDate, ")
                ' ''.Append("                 		      t2.EndDate, ")
                ' ''.Append(" 	                	      (select Count(*) from plStudentDocs where DocumentId=t1.adReqId and  ")
                ' ''.Append("        	         	       	StudentId =  '" & StudentId & "'  ) as DocSubmittedCount, ")
                ' ''.Append("                      		t2.Minscore, ")
                ' ''.Append("                	      		1 as Required, ")
                ' ''.Append("                	      		(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
                ' ''.Append("                      			where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                ' ''.Append("                      			s3.StudentId =  '" & StudentId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                ' ''.Append("                      from  ")
                ' ''.Append("                      	adReqs t1, ")
                ' ''.Append("                      	adReqsEffectiveDates t2, ")
                ' ''.Append("                      	adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
                ' ''.Append("                      where ")
                ' ''.Append("                      	t1.adReqId = t2.adReqId and ")
                ' ''.Append("				t2.MandatoryRequirement <> 1 and ")
                ' ''.Append("                      	t1.adreqTypeId in (3) and ")
                ' ''.Append("                      	t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
                ' ''.Append("				t4.PrgVerId = t5.PrgVerId ")
                ' ''.Append("                      	and t1.adReqId = t5.adReqId and ")
                ' ''.Append("				t5.PrgVerId in (select distinct PrgVerId from arStuEnrollments where StudentId='" & StudentId & "')  and ")
                ' ''.Append("                      	t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where ")
                ' ''.Append("			                	      StuEnrollId in (select StuEnrollId from arStuEnrollments where StudentId = '" & StudentId & "')) ")
                ' ''.Append("                      	and t5.adReqId is not null ")
                ' ''.Append("                      ) ")
                ' ''.Append("                      R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                ' ''.Append("                 union ")
                '' '' Get Requirements === Requirement Group assigned to Program Version
                ' ''.Append("                       	select  ")
                ' ''.Append("                	distinct ")
                ' ''.Append("                       	adReqId, ")
                ' ''.Append("                      Descrip, ")
                ' ''.Append("                      ReqGrpId, ")
                ' ''.Append("                      StartDate, ")
                ' ''.Append("                      EndDate, ")
                ' ''.Append("			 Required, ")
                ' ''.Append("                      DocSubmittedCount, ")
                ' ''.Append("                      DocStatusDescrip ")
                ' ''.Append("                      from  ")
                ' ''.Append("                      	( ")
                ' ''.Append("                      select Distinct ")
                ' ''.Append("                      	t1.adReqId, ")
                ' ''.Append("                      	t1.Descrip, ")
                ' ''.Append("                	      '00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                ' ''.Append("                	      '" & strEnrollDate & "' as CurrentDate, ")
                ' ''.Append("                	      t2.StartDate, ")
                ' ''.Append("                	      t2.EndDate, ")
                ' ''.Append("                	      (select Count(*) from plStudentDocs where DocumentId=t1.adReqId and  ")
                ' ''.Append("                	      StudentId =  '" & StudentId & "'  ) as DocSubmittedCount, ")
                ' ''.Append("                      	t2.Minscore, ")
                ' ''.Append("                	      1 as Required, ")
                ' ''.Append("                	      (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
                ' ''.Append("                      	where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                ' ''.Append("                      	s3.StudentId =  '" & StudentId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                ' ''.Append("                      from  ")
                ' ''.Append("                      	adReqs t1, ")
                ' ''.Append("                		adReqsEffectiveDates t2, ")
                ' ''.Append("                		adReqLeadGroups t3, ")
                ' ''.Append("                		adPrgVerTestDetails t5, ")
                ' ''.Append("                		adReqGrpDef t6, ")
                ' ''.Append("                		adLeadByLeadGroups t7  ")
                ' ''.Append("                      where ")
                ' ''.Append("                      	 t1.adReqId = t2.adReqId and ")
                ' ''.Append("	                      t1.adreqTypeId in (3) and ")
                ' ''.Append("       	               t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and ")
                ' ''.Append("              		 t3.LeadGrpId = t7.LeadGrpId and ")
                ' ''.Append("	                      t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
                ' ''.Append("                		 and t7.StuEnrollId in (select StuEnrollId from arStuEnrollments  where StudentId =  '" & StudentId & "')  and  ")
                ' ''.Append("       		        t5.PrgVerId in (select distinct PrgVerId from arStuEnrollments where StudentId='" & StudentId & "')    ")
                ' ''.Append("                      ) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")

                .Append("                 union ")

                'Get Requirements that were assigned to lead group but not part of any requirement group
                .Append("                 	     select  distinct ")
                .Append("                 	           adReqId, ")
                .Append("                 	           Descrip, ")
                .Append("                 	           ReqGrpId, ")
                .Append("                              ReqGrpDescrip,")
                .Append("                              NumRequired, ")
                .Append("                 	           StartDate, ")
                .Append("                 	           EndDate, ")
                .Append(" 				               Required, ")
                .Append(" 		                       DocSubmittedCount, ")
                .Append("                              DocStatusDescrip ")
                .Append(" ,ReqforEnrollment, ReqforFinancialAid, ReqforGraduation,CampGrpId ")
                .Append("                            from  ")
                .Append("                            ( ")
                ' Get Requirement Group and requirements that are assigned to a lead group
                ' and part of a requirement group
                .Append("                           	 select  distinct ")
                .Append("                 	           	t1.adReqId, ")
                .Append("                 	           	t1.Descrip, ")
                .Append("                 	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append("								'' AS ReqGrpDescrip,")
                .Append("								0 AS NumRequired,")
                .Append("                 	           	'" & strEnrollDate & " ' as CurrentDate,  ")
                .Append("                 	           	t2.StartDate, ")
                .Append("                 	           	t2.EndDate,  ")
                .Append(" 	           	   		(select Count(*) from plStudentDocs where DocumentId=t1.adReqId and ")
                .Append(" 	                	           StudentId =  '" & StudentId & "' ) as DocSubmittedCount, ")
                .Append("                 	           	t2.Minscore, ")
                .Append("        		         	IsNUll(t3.IsRequired,0) as Required, ")
                .Append("                 		       (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
                .Append("                 	       	   where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append(" 	              	           s3.StudentId =  '" & StudentId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append(" ,ReqforEnrollment, ReqforFinancialAid, ReqforGraduation,t1.CampGrpId ")
                .Append("                            from ")
                .Append("        	                    	adReqs t1, ")
                .Append("               	             	adReqsEffectiveDates t2, ")
                .Append(" 			              adReqLeadGroups t3 ")
                .Append("                            where  ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                '  .Append(" t1.reqforEnrollment=1 and ")
                .Append("                      	      	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId   and t1.StatusId='" & strActiveGUID & "' ")
                .Append(" 			              and ")
                .Append(" 			              t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId in (select distinct PrgVerId from arStuEnrollments where StudentId='" & StudentId & "') and s2.adReqId is null) ")
                .Append(" 			              and t3.LeadGrpId in ")
                .Append(" 			               (select LeadGrpId from adLeadByLeadGroups where StuEnrollId in (select Distinct StuEnrollId from arStuEnrollments where StudentId = '" & StudentId & "')) and ")
                .Append("                      	      	t1.adReqTypeId in (3) and t2.MandatoryRequirement <> 1 and ")
                .Append(" 			              t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where PrgVerId in (select distinct PrgVerId from arStuEnrollments where StudentId='" & StudentId & "') ) ")
                .Append("                            ) ")
                .Append("                            R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("                  ) R2 ")
                If Not strCampGrpId = "" Then
                    .Append("   WHERE R2.CampGrpId in 	('")
                    .Append(strCampGrpId)
                    .Append(") ")
                End If
                .Append("ORDER BY ReqGrpDescrip,DocumentDescrip")
            End With
            Return db.RunParamSQLDataSet(sb.ToString)
        Catch ex As Exception
        End Try
    End Function

    Public Function GetPendingDocsByStudentandPrgVersion_SP(ByVal StudentId As String, ByVal campusId As String) As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' Add empId to the parameter list
        db.AddParameter("@StudentId", New Guid(StudentId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@CampusID", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As New DataSet

        ds = db.RunParamSQLDataSet_SP("USP_GetPendingDocsbyStudentandPrgVersion")
        Return ds
        db.CloseConnection()

    End Function


    Public Function GetStudentGridRequirementDetailsByEffectiveDates(ByVal StudentId As String, ByVal campusid As String) As DataSet
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strEnrolldate As Date = Date.Now.ToShortDateString
        Dim dsGetCmpGrps As DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusid)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        Try


            'commented by balaji on 09/12/2005
            With sb
                .Append(" select  distinct ")
                .Append("       adReqId, ")
                .Append("       Descrip, ")
                .Append("       ReqGrpId, ")
                .Append("       StartDate,  ")
                .Append("       EndDate, ")
                .Append("       ActualScore, ")
                .Append("       TestTaken, ")
                .Append("       Comments, ")
                .Append("       Case when OverRide>=1 then 'True' else 'False' end as OverRide, ")
                .Append("       Case when (select count(*) from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId in (select PrgVerId from arStuEnrollments where StudentId='" & StudentId & "')) >=1 then ")
                .Append("       (select MinScore from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId in (select PrgVerId from arStuEnrollments where StudentId='" & StudentId & "')) ")
                .Append("       else MinScore end as MinScore, ")
                .Append("       Required, ")

                .Append(" reqforEnrollment,reqforFinancialAid,reqforGraduation, ModuleName,  ")

                .Append("       Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount,Pass ")
                .Append("       from  ")
                .Append("       ( ")
                ' Get Requirement Group and requirements of mandatory requirement
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate, ")
                .Append("		      		ActualScore, ")
                .Append("		      		TestTaken, ")
                .Append("		      		Comments, ")
                .Append("		      		OverRide, ")
                .Append("		      		Minscore, ")
                .Append("		      		Required, ")

                .Append(" reqforEnrollment,reqforFinancialAid,reqforGraduation,  ")
                .Append("ModuleName, ")
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass,CampGrpId ")
                .Append("      	from  ")
                .Append("      		( ")
                .Append("      		select Distinct ")
                .Append("      				t1.adReqId, ")
                .Append("      				t1.Descrip, ")
                .Append("      				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("      					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("      					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                .Append("      				'" & strEnrolldate & "' as CurrentDate, ")
                .Append("      				t2.StartDate, ")
                .Append("      				t2.EndDate, ")
                .Append("      				(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("      					StudentId = '" & StudentId & "' ) as ActualScore, ")
                .Append("      				(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("      					StudentId = '" & StudentId & "') as TestTaken, ")
                .Append("      				(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("      					StudentId = '" & StudentId & "') as Comments, ")
                .Append("      				       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("      				        StudentId = '" & StudentId & "') as override, ")
                .Append("      				t2.Minscore, ")
                .Append("      				1 as Required ")

                .Append(" ,reqforEnrollment,reqforFinancialAid,reqforGraduation  ")
                .Append(", (SELECT MOduleName FROM dbo.syModules A WHERE A.ModuleID=t1.moduleID) AS ModuleName,t1.CampGrpId ")

                .Append("      				from  ")
                .Append("      				adReqs t1, ")
                .Append("      				adReqsEffectiveDates t2 ")
                .Append("      				where  ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                ''.Append(" (t1.reqforEnrollment=1 or t1.ReqforFinancialAid=1 or t1.ReqforGraduation=1 ) and  ")
                .Append("      				t1.adReqId = t2.adReqId and ")
                .Append("       				t2.MandatoryRequirement=1 and ")
                .Append("      				t1.adReqTypeId in (1)  and t1.StatusId='" & strActiveGUID & "' ")
                .Append("       				) ")
                .Append("       				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("       		union ")

                ' Get The Requirements that was assigned to a program version
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate, ")
                .Append("		      		ActualScore, ")
                .Append("		      		TestTaken, ")
                .Append("		      		Comments, ")
                .Append("		      		OverRide, ")
                .Append("		      		Minscore, ")
                .Append("		      		Required, ")
                .Append(" reqforEnrollment,reqforFinancialAid,reqforGraduation,  ")
                .Append(" ModuleName, ")
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass,CampGrpId ")
                .Append("      				from  ")
                .Append("      					( ")
                .Append("      						select Distinct ")
                .Append("      							t1.adReqId, ")
                .Append("      							t1.Descrip, ")
                .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("	      						'" & strEnrolldate & "' as CurrentDate, ")
                .Append("	      						t2.StartDate, ")
                .Append("	      						t2.EndDate, ")
                .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	      							StudentId = '" & StudentId & "') as ActualScore, ")
                .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							StudentId = '" & StudentId & "') as TestTaken, ")
                .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							StudentId = '" & StudentId & "') as Comments, ")
                .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("	      						        StudentId = '" & StudentId & "') ")
                .Append("	      						as override, ")
                .Append("		      					t2.Minscore, ")
                .Append("	      						1 as Required ")
                .Append(", reqforEnrollment,reqforFinancialAid,reqforGraduation  ")
                .Append(", (SELECT MOduleName FROM dbo.syModules A WHERE A.ModuleID=t1.moduleID) AS ModuleName,t1.CampGrpId  ")
                .Append("      						from  ")
                .Append("      							adReqs t1, ")
                .Append("      							adReqsEffectiveDates t2, ")
                .Append("      							adReqLeadGroups t3,adPrgVerTestDetails t5 ")
                .Append("      						where ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                ''  .Append(" (t1.reqforEnrollment=1 or t1.ReqforFinancialAid=1 or t1.ReqforGraduation=1 ) and  ")
                .Append("      							t1.adReqId = t2.adReqId and t2.MandatoryRequirement <> 1 and ")
                .Append("      							t1.adreqTypeId in (1) and ")
                .Append("      							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  ")
                .Append("      							and t1.adReqId = t5.adReqId and t5.PrgVerId in (select PrgVerId from arStuEnrollments where StudentId='" & StudentId & "')  ")
                ''Commented for De5552
                '.Append("      					      	and	t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where ")
                '.Append("	      						    StuEnrollId in (select StuEnrollId from arStuEnrollments where StudentId='" & StudentId & "')) ")
                .Append("      							and t5.adReqId is not null  and t1.StatusId='" & strActiveGUID & "' ")
                .Append("      						) ")
                .Append("      						R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                ' Get Requirements === Requirement Group assigned to Program Version
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate, ")
                .Append("		      		ActualScore, ")
                .Append("		      		TestTaken, ")
                .Append("		      		Comments, ")
                .Append("		      		OverRide, ")
                .Append("		      		Minscore, ")
                .Append("		      		Required, ")
                .Append(" reqforEnrollment,reqforFinancialAid,reqforGraduation,  ")
                .Append(" ModuleName, ")
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass,CampGrpId ")
                .Append("      				from  ")
                .Append("      					( ")
                .Append("      						select Distinct ")
                .Append("      							t1.adReqId, ")
                .Append("      							t1.Descrip, ")
                .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("	      						'" & strEnrolldate & "' as CurrentDate, ")
                .Append("	      						t2.StartDate, ")
                .Append("	      						t2.EndDate, ")
                .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	      							StudentId = '" & StudentId & "') as ActualScore, ")
                .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							StudentId = '" & StudentId & "') as TestTaken, ")
                .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							StudentId = '" & StudentId & "') as Comments, ")
                .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("	      						        StudentId = '" & StudentId & "') ")
                .Append("	      						as override, ")
                .Append("		      					t2.Minscore, ")
                ''Modified by Saraswathi lakshmanan on jan 19 2011
                ''To fix Rally case De1231 QA: Issues with the entrance test and documents section when associated with a requirements group on the leads and students side.
                ''  .Append("	      						1 as Required ")
                .Append("	      						IsNUll(t6.IsRequired,0) as Required ")
                .Append(" ,reqforEnrollment,reqforFinancialAid,reqforGraduation  ")
                .Append(" ,(SELECT MOduleName FROM dbo.syModules A WHERE A.ModuleID=t1.moduleID) AS ModuleName,t1.CampGrpId  ")
                .Append("      						from  ")
                .Append("      							adReqs t1, ")
                .Append("							adReqsEffectiveDates t2, ")
                .Append("							adReqLeadGroups t3, ")
                .Append("							adPrgVerTestDetails t5, ")
                .Append("							adReqGrpDef t6, ")
                .Append("						adLeadByLeadGroups t7  ")
                .Append("      					where ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                ''  .Append(" (t1.reqforEnrollment=1 or t1.ReqforFinancialAid=1 or t1.ReqforGraduation=1 ) and  ")
                .Append("      						t1.adReqId = t2.adReqId and ")
                .Append("      						t1.adreqTypeId in (1) and ")
                .Append("      						t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and ")
                .Append("						t3.LeadGrpId = t7.LeadGrpId and ")
                .Append("      						t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
                .Append("	      					and t7.StuEnrollId in (select StuEnrollId from arStuEnrollments where StudentId='" & StudentId & "')  and  ")
                .Append("						t5.PrgVerId in (select PrgVerId from arStuEnrollments where StudentId='" & StudentId & "')  and t1.StatusId='" & strActiveGUID & "' ")
                .Append("      		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                ' Get Requirements that were assigned to lead group but not part of any requirement group
                .Append("	     select  distinct ")
                .Append("	           adReqId, ")
                .Append("	           Descrip, ")
                .Append("	           ReqGrpId, ")
                .Append("	           StartDate, ")
                .Append("	           EndDate, ")
                .Append("	           ActualScore, ")
                .Append("	           TestTaken, ")
                .Append("	           Comments, ")
                .Append("	           OverRide, ")
                .Append("	           Minscore, ")
                .Append("	           Required, ")
                .Append(" reqforEnrollment,reqforFinancialAid,reqforGraduation,  ")
                .Append("  ModuleName, ")
                .Append("	           Case  when ActualScore >= Minscore  then 'True' else 'False' End as Pass,CampGrpId ")
                .Append("           from  ")
                .Append("           ( ")
                ' Get Requirement Group and requirements that are assigned to a lead group
                ' and part of a requirement group
                .Append("          	 select  distinct ")
                .Append("	           	t1.adReqId, ")
                .Append("	           	t1.Descrip, ")
                .Append("	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append("	           	'" & strEnrolldate & " ' as CurrentDate,  ")
                .Append("	           	t2.StartDate, ")
                .Append("	           	t2.EndDate,  ")
                .Append("	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		StudentId = '" & StudentId & "') as ActualScore,  ")
                .Append("	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		StudentId = '" & StudentId & "') as TestTaken,  ")
                .Append("	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		StudentId = '" & StudentId & "') as Comments,  ")
                .Append("	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("	                              StudentId = '" & StudentId & "') ")
                .Append("	                      as override, ")
                .Append("	           	t2.Minscore, ")
                .Append("			IsNUll(t3.IsRequired,0) as Required ")
                .Append(" ,reqforEnrollment,reqforFinancialAid,reqforGraduation  ")
                .Append(", (SELECT MOduleName FROM dbo.syModules A WHERE A.ModuleID=t1.moduleID) AS ModuleName,t1.CampGrpId  ")
                .Append("           from ")
                .Append("           	adReqs t1, ")
                .Append("           	adReqsEffectiveDates t2, ")
                .Append("		adReqLeadGroups t3 ")
                .Append("           where  ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                ''   .Append(" (t1.reqforEnrollment=1 or t1.ReqforFinancialAid=1 or t1.ReqforGraduation=1 ) and  ")
                .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId ")
                .Append("		and   t1.StatusId='" & strActiveGUID & "' and ")
                .Append("		t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId in (select PrgVerId from arStuEnrollments where StudentId='" & StudentId & "') and s2.adReqId is null) ")
                .Append("		and t3.LeadGrpId in ")
                .Append("		(select LeadGrpId from adLeadByLeadGroups where StuEnrollId in (select StuEnrollId from arStuEnrollments where StudentId='" & StudentId & "')) and ")
                .Append("           	t1.adReqTypeId in (1) and t2.MandatoryRequirement <> 1 and ")
                .Append("		t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where  adReqId IS NOT NULL and PrgVerId in (select PrgVerId from arStuEnrollments where StudentId='" & StudentId & "')) ")
                .Append("           ) ")
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" ) R2 ")
                If Not strCampGrpId = "" Then
                    .Append("  WHERE R2.CampGrpId in 	('")
                    .Append(strCampGrpId)
                    .Append(") ")
                End If
            End With


            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(myAdvAppSettings.AppSettings("ConString")))

            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As Exception
        Finally
            'db.CloseConnection()
        End Try
    End Function
    Public Function GetStudentGridRequirementDetailsByEffectiveDates_sp(ByVal StudentId As String, ByVal campusid As String) As DataSet

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' Add empId to the parameter list
        db.AddParameter("@StudentId", New Guid(StudentId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@CampusID", New Guid(campusid), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet

        ds = db.RunParamSQLDataSet_SP("USP_GetStudentGridTestRequirementDetailsByEffectiveDates")
        Return ds
    End Function


    Public Function GetAllStudentStandardRequirementsByEffectiveDates(ByVal StudentId As String) As DataSet
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strEnrollDate As Date = Date.Now.ToShortDateString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try


            With sb
                .Append(" select ")
                .Append("           adReqId, ")
                .Append("           Descrip, ")
                .Append("           ReqGrpId, ")
                .Append("           StartDate, ")
                .Append("           EndDate, ")
                .Append("           ActualScore,  ")
                .Append("           TestTaken, ")
                .Append("           Comments, ")
                .Append("           Case when OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append("           Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount, ")
                .Append("           Minscore, ")
                .Append("           Required, ")
                .Append("		    Pass ")
                .Append("           from ")
                .Append("           ( ")
                .Append("           select  Distinct ")
                .Append("	           adReqId, ")
                .Append("	           Descrip, ")
                .Append("	           ReqGrpId, ")
                .Append("	           StartDate, ")
                .Append("	           EndDate, ")
                .Append("	           ActualScore, ")
                .Append("	           TestTaken, ")
                .Append("	           Comments, ")
                .Append("	           OverRide, ")
                .Append("	           Minscore, ")
                .Append("	           Required, ")
                .Append("	           Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass ")
                .Append("           from  ")
                .Append("           ( ")
                '  Get Requirement Group and requirements of mandatory requirement
                .Append("          	 select  ")
                .Append("	           	t1.adReqId, ")
                .Append("	           	t1.Descrip, ")
                .Append("	           	case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("	           		(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("	           	else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId,  ")
                .Append("	           	'" & strEnrollDate & "' as CurrentDate,  ")
                .Append("	           	t2.StartDate, ")
                .Append("	           	t2.EndDate,  ")
                .Append("	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		StudentId='" & StudentId & "')  as ActualScore,  ")
                .Append("	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		StudentId='" & StudentId & "')  as TestTaken,  ")
                .Append("	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		StudentId='" & StudentId & "')  as Comments,  ")
                .Append("	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and  ")
                .Append("	                              StudentId='" & StudentId & "') ")
                .Append("	                      as override, ")
                .Append("	           	t2.Minscore, ")
                .Append("	           	1 as Required ")
                .Append("           from ")
                .Append("           	adReqs t1, ")
                .Append("           	adReqsEffectiveDates t2 ")
                .Append("           where  ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" t1.reqforEnrollment=1 and ")
                .Append("           	t1.adReqId = t2.adReqId and  ")
                .Append("           	t2.MandatoryRequirement=1 and ")
                .Append("           	t1.adReqTypeId in (1) ")
                .Append("           ) ")
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                '  Get Requirements that were assigned to lead group but not part of any requirement group
                .Append(" select ")
                .Append("           adReqId, ")
                .Append("           Descrip, ")
                .Append("           ReqGrpId, ")
                .Append("           StartDate, ")
                .Append("           EndDate, ")
                .Append("           ActualScore,  ")
                .Append("           TestTaken, ")
                .Append("           Comments, ")
                .Append("           Case when OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append("           Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount, ")
                .Append("           Minscore, ")
                .Append("           Required, ")
                .Append("	           Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass ")
                .Append("           from ")
                .Append("           ( ")
                '  Get Requirement Group and requirements that are assigned to a lead group
                '  and part of a requirement group
                .Append("           	 select  ")
                .Append(" 	           	t1.adReqId, ")
                .Append(" 	           	t1.Descrip, ")
                .Append(" 	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append(" 	           	'" & strEnrollDate & "' as CurrentDate,  ")
                .Append(" 	           	t2.StartDate, ")
                .Append(" 	           	t2.EndDate,  ")
                .Append(" 	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append(" 	           		StudentId='" & StudentId & "')  as ActualScore,  ")
                .Append(" 	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append(" 	           		StudentId='" & StudentId & "')  as TestTaken,  ")
                .Append(" 	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append(" 	           		StudentId='" & StudentId & "')  as Comments,  ")
                .Append(" 	            (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append(" 	                              StudentId='" & StudentId & "') ")
                .Append(" 	                      as override, ")
                .Append(" 	           	t2.Minscore, ")
                .Append(" 			IsNull(t3.IsRequired,0) as Required ")
                .Append("            from ")
                .Append("           	adReqs t1, ")
                .Append("           	adReqsEffectiveDates t2, ")
                .Append("		adReqLeadGroups t3 ")
                .Append("           where  ")
                ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
                .Append(" t1.reqforEnrollment=1 and ")
                .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId ")
                ' and t1.adReqId not in (select Distinct adReqId from adReqGrpDef)
                .Append("		and t3.LeadGrpId in ")
                .Append("		(select Distinct LeadGrpId from adLeadByLeadGroups where StuEnrollId in (select Distinct StuEnrollId from arStuEnrollments where StudentId='" & StudentId & "') and ")
                .Append("           	t1.adReqTypeId in (1) and t2.MandatoryRequirement <> 1 ")
                .Append("           ) ")
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" ) R2 ")
            End With
            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As Exception
        Finally
        End Try
    End Function
    Public Function CheckIfReqGroupMeetsConditionsNoPrgVersion(ByVal LeadId As String, Optional ByVal CampusId As String = "") As String

        Dim sb As New StringBuilder
        Dim strEnrollDate As Date = Date.Now.ToShortDateString

        Dim dsGetCmpGrps As DataSet

        'Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            'strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If


        With sb
            .Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,R5.TestAttempted+R5.DocsAttempted+R5.TestOverRidden+R5.DocumentOverRidden as AttemptedReqs ")
            .Append(" from ")
            .Append(" (  ")
            ' Get Mandatory requirement group
            .Append("      select ")
            .Append("              	t7.ReqGrpId,t7.Descrip, ")
            .Append("             			( ")
            .Append("              			select Count(*) as NumReqs ")
            .Append("              			from ")
            .Append("      			 			( ")
            .Append("              					select Distinct ")
            .Append("								A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("              						adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("					              	 A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1  ")
            .Append("					                and A2.adreqTypeId in (1,3) ")
            .Append("              				) ")
            .Append("              				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("                			) ")
            .Append("              		as NumReqs, ")
            .Append("              		( ")
            .Append("              			select Count(*) as TestAttempted ")
            .Append("              			from ")
            .Append("              				( ")
            .Append("              					select Distinct ")
            .Append("								A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("						              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("					              	 A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1 ")
            .Append("					                and A2.adreqTypeId in (1) ")
            .Append("				              ) ")
            .Append("			              R1,adLeadEntranceTest R2 ")
            .Append("					where ")
            .Append("						R1.adReqId = R2.EntrTestId and ")
            .Append("						R2.LeadId='" & LeadId & "'")
            .Append("	              		and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and ")
            .Append("						(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("              		) as TestAttempted, ")
            .Append("             			( ")
            .Append(" 			              select Count(*) as DocsAttempted ")
            .Append(" 			              from ")
            .Append(" 				           ( ")
            .Append(" 				              select Distinct ")
            .Append(" 							A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" 						from  ")
            .Append(" 					              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("					              	 A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1  ")
            .Append("					                and A2.adreqTypeId in (3) ")
            .Append(" 					      ) ")
            .Append(" 		              	R1,adLeadDocsReceived R2 ")
            .Append(" 					where 	")
            .Append(" 						R1.adReqId = R2.DocumentId and ")
            .Append(" 						R2.LeadId='" & LeadId & "' ")
            .Append(" 				              and R1.CurrentDate >= R1.StartDate and ")
            .Append(" 						(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" 		              ) as DocsAttempted, ")
            .Append(" (  ")
            .Append("     select Count(*) as OverRideTestAttempted ")
            .Append(" from ")
            .Append(" (  ")
            .Append(" 				              select Distinct ")
            .Append(" 							A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" 						from  ")
            .Append(" 					              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("					              	 A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1  ")
            .Append("					                and A2.adreqTypeId in (1) ")
            .Append(" )  ")
            .Append(" R1,adEntrTestOverRide R2 where ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.OverRide=1 and ")
            .Append(" R2.LeadId='" & LeadId & "' and R1.CurrentDate >= R1.StartDate and ")
            .Append(" (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) and ")
            .Append(" R1.adReqId not in (select EntrTestId from adLeadEntranceTest where LeadId='" & LeadId & "') ")
            .Append(" ) as TestOverRidden, ")
            .Append(" (  ")
            .Append("     select Count(*) as OverRideDocumentAttempted ")
            .Append(" from ")
            .Append(" (  ")
            .Append(" 				              select Distinct ")
            .Append(" 							A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" 						from  ")
            .Append(" 					              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("					              	 A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1  ")
            .Append("					                and A2.adreqTypeId in (3) ")
            .Append(" )  ")
            .Append(" R1,adEntrTestOverRide R2 where ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.OverRide=1 and ")
            .Append(" R2.LeadId='" & LeadId & "' and R1.CurrentDate >= R1.StartDate and ")
            .Append(" (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) and ")
            .Append(" R1.adReqId not in (select Distinct DocumentId from adLeadDocsReceived where LeadId='" & LeadId & "') ")
            .Append(" ) as DocumentOverRidden ")
            .Append("       from ")
            .Append(" 		adReqGroups t7 ")
            .Append(" 	where ")
            .Append(" 		IsMandatoryReqGrp=1   ")
            .Append("    ) R5  ")
        End With

        '' Grab the Categories and Products table
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim dr As DataRow

        ds = db.RunParamSQLDataSet(sb.ToString)

        Dim strMessage As String
        Dim intRequiredReqs As Integer
        Dim intAttemptedReqs As Integer

        For Each dr In ds.Tables(0).Rows
            intRequiredReqs = dr("NumReqs")
            intAttemptedReqs = dr("AttemptedReqs")

            If intAttemptedReqs < intRequiredReqs Then
                strMessage = "Not Satisfied"
            End If
        Next
        If Not strMessage = "" Then
            Return "Not Enroll"
        Else
            Return "Enroll"
        End If
    End Function
    Public Function GetRequirementGroupsByLead(ByVal LeadId As String, ByVal strEnrollDate As Date) As DataSet
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim sb As New StringBuilder


        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        With sb
            .Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,R5.TestAttempted+R5.DocsAttempted as AttemptedReqs ")
            .Append(" from ")
            .Append(" (  ")
            ' Get Mandatory requirement group
            .Append("      select ")
            .Append("              	t7.ReqGrpId,t7.Descrip, ")
            .Append("             			( ")
            .Append("              			select Count(*) as NumReqs ")
            .Append("              			from ")
            .Append("      			 			( ")
            .Append("              					select ")
            .Append("								Distinct A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("              						adReqs A2,adReqsEffectiveDates A3  ")
            .Append("              					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("					              	A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1 and  ")
            .Append("					              	A2.adreqTypeId in (1,3) ")
            .Append("              				) ")
            .Append("              				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("                			) ")
            .Append("              		as NumReqs, ")
            .Append("              		( ")
            .Append("              			select Count(*) as TestAttempted ")
            .Append("              			from ")
            .Append("              				( ")
            .Append("              					select ")
            .Append("								Distinct A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("						              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("					              	A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1 and  ")
            .Append("					              	A2.adreqTypeId in (1) ")
            .Append("				              ) ")
            .Append("			              R1,adLeadEntranceTest R2 ")
            .Append("					where ")
            .Append("						R1.adReqId = R2.EntrTestId and ")
            .Append("						R2.LeadId='" & LeadId & "' ")
            .Append("	              			and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and ")
            .Append("						(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("              		) as TestAttempted, ")
            .Append("             			( ")
            .Append("			              select Count(*) as DocsAttempted ")
            .Append("			              from ")
            .Append("				           ( ")
            .Append("				              select ")
            .Append("							Distinct A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("						from  ")
            .Append("					              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("					              	A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1 and  ")
            .Append("					              	A2.adreqTypeId in (3) ")
            .Append("					      ) ")
            .Append("		              	R1,adLeadDocsReceived R2 ")
            .Append("					where 	")
            .Append("						R1.adReqId = R2.DocumentId and ")
            .Append("						R2.LeadId='" & LeadId & "'  ")
            .Append("				              and R1.CurrentDate >= R1.StartDate and ")
            .Append("						(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("		              ) as DocsAttempted ")
            .Append("       from ")
            .Append("		adReqGroups t7 ")
            .Append("	where ")
            .Append("		IsMandatoryReqGrp=1   ")
            .Append("   ) R5  ")
            .Append("   Union  ")
            .Append("   select '00000000-0000-0000-0000-000000000000' as RepGrpId,NULL as Miscellaneous,NULL as NumReqs,NULL as AttemptedReqs from adLeadGroups  ")
            .Append("   order by ReqGrpId desc  ")
        End With


        '' Grab the Categories and Products table
        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        dadNorthwind = New OleDbDataAdapter(sb.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "Parent")

        conNorthwind.Close()
        Return dstNorthwind
    End Function
    Public Function GetRequirementGroupsByLeadAndPrgVersion(ByVal LeadId As String, ByVal PrgVerId As String, ByVal strEnrollDate As Date) As DataSet
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim sb As New StringBuilder

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        'Dim strCurrentDate As Date = strEnrollDate


        With sb

            .Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,R5.TestAttempted+R5.DocsAttempted as AttemptedReqs ")
            .Append(" from ")
            .Append(" (  ")
            ' Requiremt group assigned to a program version
            .Append("	Select  ")
            .Append("		Distinct ")
            .Append("			t1.ReqGrpId, ")
            .Append("			t2.Descrip, ")
            .Append("			t3.NumReqs as Numreqs,  ")
            .Append("	            	    ( ")
            .Append("       	     		select Count(*) as TestAttempted ")
            .Append("            				from ")
            .Append("			            		( ")
            .Append("            						select ")
            .Append("								Distinct A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("       	     						adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append("            						where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("				            			A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and ")
            .Append("								A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append("       		     					A4.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            .Append("		            					ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (1) ")
            .Append("	       			     	) ")
            .Append("					R1,adLeadEntranceTest R2 ")
            .Append("				where ")
            .Append("					R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "'")
            .Append("			            	and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and ")
            .Append("					(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("	      	        )as TestAttempted, ")
            .Append("	               ( ")
            .Append("		            	select Count(*) as DocsAttempted ")
            .Append("       			     	from ")
            .Append("			            		( ")
            .Append("			            			select ")
            .Append("								Distinct A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("            							adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append("			            			where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("	            						A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append("			       		     		A4.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            .Append("            							ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (3) ")
            .Append("			            		) ")
            .Append("            				R1,adLeadDocsReceived R2 ")
            .Append("				where ")
            .Append("					R1.adReqId = R2.DocumentId and R2.LeadId='" & LeadId & "'")
            .Append("		            		and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("            		) as DocsAttempted  ")
            .Append("      	from ")
            .Append("			adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3  ")
            .Append("       where  ")
            .Append(" 	              t1.ReqGrpId = t2.ReqGrpId and ")
            .Append("			t2.ReqGrpId = t3.ReqGrpId and ")
            .Append("			t1.PrgVerId='" & PrgVerId & "' and t2.IsMandatoryReqGrp <> 1  ")
            ''Commented for DE5552
            '.Append("                  and   t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups ")
            '.Append("			where LeadId='" & LeadId & "') ")
            .Append(" union ")
            ' Get Mandatory requirement group
            .Append("      select ")
            .Append("              	t7.ReqGrpId,t7.Descrip, ")
            .Append("             			( ")
            .Append("              			select Count(*) as NumReqs ")
            .Append("              			from ")
            .Append("      			 			( ")
            .Append("              					select Distinct ")
            .Append("								A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("              						adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("					              	A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1   ")
            .Append("					              	and A2.adreqTypeId in (1,3) ")
            .Append("              				) ")
            .Append("              				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("                			) ")
            .Append("              		as NumReqs, ")
            .Append("              		( ")
            .Append("              			select Count(*) as TestAttempted ")
            .Append("              			from ")
            .Append("              				( ")
            .Append("              					select Distinct ")
            .Append("								A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("						              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("					              	A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1  ")
            .Append("					              	and A2.adreqTypeId in (1) ")
            .Append("				              ) ")
            .Append("			              R1,adLeadEntranceTest R2 ")
            .Append("					where ")
            .Append("						R1.adReqId = R2.EntrTestId and ")
            .Append("						R2.LeadId='" & LeadId & "'")
            .Append("	              			and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and ")
            .Append("						(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("              		) as TestAttempted, ")
            .Append("             			( ")
            .Append(" 			              select Count(*) as DocsAttempted ")
            .Append(" 			              from ")
            .Append(" 				           ( ")
            .Append(" 				              select Distinct ")
            .Append(" 							A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" 						from  ")
            .Append(" 					              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" A2.reqforEnrollment=1 and ")
            .Append("					              	A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1 ")
            .Append("					              	and A2.adreqTypeId in (3) ")
            .Append(" 					      ) ")
            .Append(" 		              	R1,adLeadDocsReceived R2 ")
            .Append(" 					where 	")
            .Append(" 						R1.adReqId = R2.DocumentId and ")
            .Append(" 						R2.LeadId='" & LeadId & "' ")
            .Append(" 				              and R1.CurrentDate >= R1.StartDate and ")
            .Append(" 						(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" 		              ) as DocsAttempted ")
            .Append("       from ")
            .Append(" 		adReqGroups t7 ")
            .Append(" 	where ")
            .Append(" 		IsMandatoryReqGrp=1   ")
            .Append("    ) R5  ")
            .Append("    Union  ")
            .Append("    select '00000000-0000-0000-0000-000000000000' as RepGrpId,NULL as Miscellaneous,NULL as NumReqs,NULL as AttemptedReqs from adLeadGroups  ")
            .Append("    order by ReqGrpId desc  ")
        End With



        '' Grab the Categories and Products table
        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        dadNorthwind = New OleDbDataAdapter(sb.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "Parent")

        Return dstNorthwind
    End Function
    Public Function GetEntranceTestsByGroupAndLead(ByVal LeadId As String, ByVal strEnrollDate As Date, ByVal ReqGrpId As String) As DataSet
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim sb1 As New StringBuilder

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        With sb1
            '  This Query Brings the ouput based on the following
            '  All mandatory requirements
            '  requirements assigned to a lead group
            .Append(" select ")
            .Append("           adReqId, ")
            .Append("           Descrip, ")
            .Append("           ReqGrpId, ")
            .Append("           StartDate, ")
            .Append("           EndDate, ")
            .Append("           ActualScore,  ")
            .Append("           TestTaken, ")
            .Append("           Comments, ")
            .Append("           Case when OverRide=1 then 'True' else 'False' end as OverRide, ")
            .Append("           Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount, ")
            .Append("           DocSubmittedCount, ")
            .Append("           Minscore, ")
            .Append("           Required, ")
            .Append("           DocStatusDescrip ")
            .Append("           from ")
            .Append("           ( ")
            .Append("           select  Distinct ")
            .Append("	           adReqId, ")
            .Append("	           Descrip, ")
            .Append("	           ReqGrpId, ")
            .Append("	           StartDate, ")
            .Append("	           EndDate, ")
            .Append("	           ActualScore, ")
            .Append("	           TestTaken, ")
            .Append("	           Comments, ")
            .Append("	           OverRide, ")
            .Append("	           Minscore, ")
            .Append("	           Required, ")
            .Append("	           DocSubmittedCount, ")
            .Append("	           DocStatusDescrip ")
            .Append("           from  ")
            .Append("           ( ")
            '  Get Requirement Group and requirements of mandatory requirement
            .Append("          	 select  ")
            .Append("	           	t1.adReqId, ")
            .Append("	           	t1.Descrip, ")
            .Append("	           	case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
            .Append("	           		(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
            .Append("	           	else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId,  ")
            .Append("	           	'" & strEnrollDate & "' as CurrentDate,  ")
            .Append("	           	t2.StartDate, ")
            .Append("	           	t2.EndDate,  ")
            .Append("	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId='" & LeadId & "')  as ActualScore,  ")
            .Append("	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId='" & LeadId & "')  as TestTaken,  ")
            .Append("	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId='" & LeadId & "')  as Comments,  ")
            .Append("	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and  ")
            .Append("	                              LeadId='" & LeadId & "') ")
            .Append("	                      as override, ")
            .Append("	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
            .Append("	           		LeadId='" & LeadId & "')  as DocSubmittedCount, ")
            ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
            ''.Append("	           	cast(t2.Minscore as int) as MinScore, ")
            .Append("	           	t2.Minscore , ")
            ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
            .Append("	           	1 as Required, ")
            .Append("	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("	           s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("           from ")
            .Append("           	adReqs t1, ")
            .Append("           	adReqsEffectiveDates t2 ")
            .Append("           where  ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" t1.reqforEnrollment=1 and ")
            .Append("           	t1.adReqId = t2.adReqId and  ")
            .Append("           	t2.MandatoryRequirement=1 and ")
            .Append("           	t1.adReqTypeId in (1) ")
            .Append("           ) ")
            .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" union ")
            '  Get Requirements that were assigned to lead group but not part of any requirement group
            .Append(" select ")
            .Append("           adReqId, ")
            .Append("           Descrip, ")
            .Append("           ReqGrpId, ")
            .Append("           StartDate, ")
            .Append("           EndDate, ")
            .Append("           ActualScore,  ")
            .Append("           TestTaken, ")
            .Append("           Comments, ")
            .Append("           OverRide, ")
            .Append("	           Minscore, ")
            .Append("	           Required, ")
            .Append("	           DocSubmittedCount, ")
            .Append("	           DocStatusDescrip ")
            .Append("           from ")
            .Append("           ( ")
            '  Get Requirement Group and requirements that are assigned to a lead group
            '  and part of a requirement group
            .Append("           	 select  ")
            .Append(" 	           	t1.adReqId, ")
            .Append(" 	           	t1.Descrip, ")
            .Append(" 	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            .Append(" 	           	'" & strEnrollDate & "' as CurrentDate,  ")
            .Append(" 	           	t2.StartDate, ")
            .Append(" 	           	t2.EndDate,  ")
            .Append(" 	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append(" 	           		LeadId='" & LeadId & "')  as ActualScore,  ")
            .Append(" 	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append(" 	           		LeadId='" & LeadId & "')  as TestTaken,  ")
            .Append(" 	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append(" 	           		LeadId='" & LeadId & "')  as Comments,  ")
            .Append(" 	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append(" 	                              LeadId='" & LeadId & "') ")
            .Append(" 	                      as override, ")
            .Append(" 	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
            .Append(" 	           		LeadId='" & LeadId & "')  as DocSubmittedCount, ")
            ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
            ''.Append(" 	           	cast(t2.Minscore as int) as MinScore, ")
            .Append(" 	           	t2.Minscore , ")
            ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
            .Append(" 			IsNUll(t3.IsRequired,0) as Required, ")
            .Append(" 	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" 	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" 	           s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("            from ")
            .Append("           	adReqs t1, ")
            .Append("           	adReqsEffectiveDates t2, ")
            .Append("		adReqLeadGroups t3 ")
            .Append("           where  ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" t1.reqforEnrollment=1 and ")
            .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId ")
            ' and t1.adReqId not in (select Distinct adReqId from adReqGrpDef)
            .Append("		and t3.LeadGrpId in ")
            .Append("		(select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and ")
            .Append("           	t1.adReqTypeId in (1) and t2.MandatoryRequirement <> 1 ")
            .Append("           ) ")
            .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" ) R2 where ReqGrpId='" & ReqGrpId & "' ")
        End With

        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        dadNorthwind = New OleDbDataAdapter(sb1.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "Parent")

        conNorthwind.Close()
        Return dstNorthwind
    End Function
    Public Function GetDocumentsByGroupAndLead(ByVal LeadId As String, ByVal strEnrollDate As Date, ByVal ReqGrpId As String) As DataSet
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim sb1 As New StringBuilder

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        With sb1
            '  This Query Brings the ouput based on the following
            '  All mandatory requirements
            '  requirements assigned to a lead group
            .Append(" select ")
            .Append("           adReqId, ")
            .Append("           Descrip, ")
            .Append("           ReqGrpId, ")
            .Append("           StartDate, ")
            .Append("           EndDate, ")
            .Append("           ActualScore,  ")
            .Append("           TestTaken, ")
            .Append("           Comments, ")
            .Append("           Case when OverRide=1 then 'True' else 'False' end as OverRide, ")
            .Append("           Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount, ")
            .Append("           DocSubmittedCount, ")
            .Append("           Minscore, ")
            .Append("           Required, ")
            .Append("           DocStatusDescrip ")
            .Append("           from ")
            .Append("           ( ")
            .Append("           select  Distinct ")
            .Append("	           adReqId, ")
            .Append("	           Descrip, ")
            .Append("	           ReqGrpId, ")
            .Append("	           StartDate, ")
            .Append("	           EndDate, ")
            .Append("	           ActualScore, ")
            .Append("	           TestTaken, ")
            .Append("	           Comments, ")
            .Append("	           OverRide, ")
            .Append("	           Minscore, ")
            .Append("	           Required, ")
            .Append("	           DocSubmittedCount, ")
            .Append("	           DocStatusDescrip ")
            .Append("           from  ")
            .Append("           ( ")
            '  Get Requirement Group and requirements of mandatory requirement
            .Append("          	 select  ")
            .Append("	           	t1.adReqId, ")
            .Append("	           	t1.Descrip, ")
            .Append("	           	case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
            .Append("	           		(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
            .Append("	           	else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId,  ")
            .Append("	           	'" & strEnrollDate & "' as CurrentDate,  ")
            .Append("	           	t2.StartDate, ")
            .Append("	           	t2.EndDate,  ")
            .Append("	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId='" & LeadId & "')  as ActualScore,  ")
            .Append("	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId='" & LeadId & "')  as TestTaken,  ")
            .Append("	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId='" & LeadId & "')  as Comments,  ")
            .Append("	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and  ")
            .Append("	                              LeadId='" & LeadId & "') ")
            .Append("	                      as override, ")
            .Append("	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
            .Append("	           		LeadId='" & LeadId & "')  as DocSubmittedCount, ")
            ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
            ''.Append("	           	cast(t2.Minscore as int) as MinScore, ")
            .Append("	           	t2.Minscore , ")
            ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
            .Append("	           	1 as Required, ")
            .Append("	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("	           s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("           from ")
            .Append("           	adReqs t1, ")
            .Append("           	adReqsEffectiveDates t2 ")
            .Append("           where  ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" t1.reqforEnrollment=1 and ")
            .Append("           	t1.adReqId = t2.adReqId and  ")
            .Append("           	t2.MandatoryRequirement=1 and ")
            .Append("           	t1.adReqTypeId in (3) ")
            .Append("           ) ")
            .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" union ")
            '  Get Requirements that were assigned to lead group but not part of any requirement group
            .Append(" select ")
            .Append("           adReqId, ")
            .Append("           Descrip, ")
            .Append("           ReqGrpId, ")
            .Append("           StartDate, ")
            .Append("           EndDate, ")
            .Append("           ActualScore,  ")
            .Append("           TestTaken, ")
            .Append("           Comments, ")
            .Append("           OverRide, ")
            .Append("	           Minscore, ")
            .Append("	           Required, ")
            .Append("	           DocSubmittedCount, ")
            .Append("	           DocStatusDescrip ")
            .Append("           from ")
            .Append("           ( ")
            '  Get Requirement Group and requirements that are assigned to a lead group
            '  and part of a requirement group
            .Append("           	 select  ")
            .Append(" 	           	t1.adReqId, ")
            .Append(" 	           	t1.Descrip, ")
            .Append(" 	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            .Append(" 	           	'" & strEnrollDate & "' as CurrentDate,  ")
            .Append(" 	           	t2.StartDate, ")
            .Append(" 	           	t2.EndDate,  ")
            .Append(" 	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append(" 	           		LeadId='" & LeadId & "')  as ActualScore,  ")
            .Append(" 	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append(" 	           		LeadId='" & LeadId & "')  as TestTaken,  ")
            .Append(" 	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append(" 	           		LeadId='" & LeadId & "')  as Comments,  ")
            .Append(" 	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append(" 	                              LeadId='" & LeadId & "') ")
            .Append(" 	                      as override, ")
            .Append(" 	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
            .Append(" 	           		LeadId='" & LeadId & "')  as DocSubmittedCount, ")
            ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
            ''.Append(" 	           	cast(t2.Minscore as int) as MinScore, ")
            .Append(" 	           	t2.Minscore , ")
            ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
            .Append(" 			IsNULL(t3.IsRequired,0) as Required, ")
            .Append(" 	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" 	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" 	           s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("            from ")
            .Append("           	adReqs t1, ")
            .Append("           	adReqsEffectiveDates t2, ")
            .Append("		adReqLeadGroups t3 ")
            .Append("           where  ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" t1.reqforEnrollment=1 and ")
            .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId ")
            ' and t1.adReqId not in (select Distinct adReqId from adReqGrpDef)
            .Append("		and t3.LeadGrpId in ")
            .Append("		(select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and ")
            .Append("           	t1.adReqTypeId in (3) and t2.MandatoryRequirement <> 1 ")
            .Append("           ) ")
            .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" ) R2 where ReqGrpId='" & ReqGrpId & "' ")
        End With

        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        dadNorthwind = New OleDbDataAdapter(sb1.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "Parent")

        conNorthwind.Close()
        Return dstNorthwind
    End Function
    Public Function GetEntranceTestsByGroupAndLeadAndPrgVersion(ByVal LeadId As String, ByVal PrgVerId As String, ByVal strEnrollDate As Date, ByVal ReqGrpId As String) As DataSet
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim sb1 As New StringBuilder


        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        With sb1
            .Append(" select  distinct ")
            .Append("       adReqId, ")
            .Append("       Descrip, ")
            .Append("       ReqGrpId, ")
            .Append("       StartDate,  ")
            .Append("       EndDate, ")
            .Append("       ActualScore, ")
            .Append("       TestTaken, ")
            .Append("       Comments, ")
            .Append("       Case when OverRide>=1 then 'True' else 'False' end as OverRide, ")
            .Append("       Case when (select count(*) from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') >=1 then ")
            .Append("       (select MinScore from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') ")
            .Append("       else MinScore end as MinScore, ")
            .Append("       Required, ")
            .Append("       DocSubmittedCount, ")
            .Append("       DocStatusDescrip, ")
            .Append("       Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount ")
            .Append("       from  ")
            .Append("       ( ")
            ' Get Requirement Group and requirements of mandatory requirement
            .Append("       	select  ")
            .Append("			distinct ")
            .Append("		       	adReqId, ")
            .Append("		      		Descrip, ")
            .Append("		      		ReqGrpId, ")
            .Append("		      		StartDate, ")
            .Append("		      		EndDate, ")
            .Append("		      		ActualScore, ")
            .Append("		      		TestTaken, ")
            .Append("		      		Comments, ")
            .Append("		      		OverRide, ")
            .Append("		      		Minscore, ")
            .Append("		      		Required, ")
            .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
            .Append("		      		DocSubmittedCount, ")
            .Append("		      		DocStatusDescrip ")
            .Append("      	from  ")
            .Append("      		( ")
            .Append("      		select Distinct ")
            .Append("      				t1.adReqId, ")
            .Append("      				t1.Descrip, ")
            .Append("      				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
            .Append("      					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
            .Append("      					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
            .Append("      				'" & strEnrollDate & "' as CurrentDate, ")
            .Append("      				t2.StartDate, ")
            .Append("      				t2.EndDate, ")
            .Append("      				(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("      					LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append("      				(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("      					LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append("      				(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("      					LeadId = '" & LeadId & "' ) as Comments, ")
            .Append("      				       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("      				        LeadId = '" & LeadId & "' ) as override, ")
            .Append("      				(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            .Append("      					LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append("      				t2.Minscore, ")
            .Append("      				1 as Required, ")
            .Append("      				(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("      				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("      				s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("      				from  ")
            .Append("      				adReqs t1, ")
            .Append("      				adReqsEffectiveDates t2 ")
            .Append("      				where  ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" t1.reqforEnrollment=1 and ")

            .Append("      				t1.adReqId = t2.adReqId and ")
            .Append("       			t2.MandatoryRequirement=1 and ")
            .Append("      				t1.adReqTypeId in (1) ")
            .Append("       				) ")
            .Append("       				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("       		union ")

            ' Get The Requirements that was assigned to a program version
            .Append("       	select  ")
            .Append("			distinct ")
            .Append("		       	adReqId, ")
            .Append("		      		Descrip, ")
            .Append("		      		ReqGrpId, ")
            .Append("		      		StartDate, ")
            .Append("		      		EndDate, ")
            .Append("		      		ActualScore, ")
            .Append("		      		TestTaken, ")
            .Append("		      		Comments, ")
            .Append("		      		OverRide, ")
            .Append("		      		Minscore, ")
            .Append("		      		Required, ")
            .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
            .Append("		      		DocSubmittedCount, ")
            .Append("		      		DocStatusDescrip ")
            .Append("      				from  ")
            .Append("      					( ")
            .Append("      						select Distinct ")
            .Append("      							t1.adReqId, ")
            .Append("      							t1.Descrip, ")
            .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
            .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
            .Append("	      						t2.StartDate, ")
            .Append("	      						t2.EndDate, ")
            .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	      							LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("	      							LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("	      							LeadId = '" & LeadId & "' ) as Comments, ")
            .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("	      						        LeadId = '" & LeadId & "' ) ")
            .Append("	      						as override, ")
            .Append("	      						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            .Append("	      						LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append("		      					t2.Minscore, ")
            .Append("	      						1 as Required, ")
            .Append("	      						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("      							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("      							s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("      						from  ")
            .Append("      							adReqs t1, ")
            .Append("      							adReqsEffectiveDates t2, ")
            .Append("      							adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
            .Append("      						where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" t1.reqforEnrollment=1 and ")
            .Append("      							t1.adReqId = t2.adReqId and t2.MandatoryRequirement <> 1 and ")
            .Append("      							t1.adreqTypeId in (1) and ")
            .Append("      							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId ")
            .Append("      							and t1.adReqId = t5.adReqId and t5.PrgVerId = '" & PrgVerId & "'     ")
            ''Commented for DE5552
            '.Append("      					      and		t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where ")
            '.Append("	      						LeadId = '" & LeadId & "' ) ")
            .Append("      							and t5.adReqId is not null ")
            .Append("      						) ")
            .Append("      						R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" union ")
            ' Get Requirements === Requirement Group assigned to Program Version
            .Append("       	select  ")
            .Append("			distinct ")
            .Append("		       	adReqId, ")
            .Append("		      		Descrip, ")
            .Append("		      		ReqGrpId, ")
            .Append("		      		StartDate, ")
            .Append("		      		EndDate, ")
            .Append("		      		ActualScore, ")
            .Append("		      		TestTaken, ")
            .Append("		      		Comments, ")
            .Append("		      		OverRide, ")
            .Append("		      		Minscore, ")
            .Append("		      		Required, ")
            .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
            .Append("		      		DocSubmittedCount, ")
            .Append("		      		DocStatusDescrip ")
            .Append("      				from  ")
            .Append("      					( ")
            .Append("      						select Distinct ")
            .Append("      							t1.adReqId, ")
            .Append("      							t1.Descrip, ")
            .Append("	      						t6.ReqGrpId as reqGrpId, ")
            .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
            .Append("	      						t2.StartDate, ")
            .Append("	      						t2.EndDate, ")
            .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	      							LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("	      							LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("	      							LeadId = '" & LeadId & "' ) as Comments, ")
            .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("	      						        LeadId = '" & LeadId & "' ) ")
            .Append("	      						as override, ")
            .Append("	      						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            .Append("	      						LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append("		      					t2.Minscore, ")
            .Append("	      						IsNULL(t3.IsRequired,0) as Required, ")
            .Append("	      						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("      							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("      							s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("      						from  ")
            .Append("      							adReqs t1, ")
            .Append("							adReqsEffectiveDates t2, ")
            .Append("							adReqLeadGroups t3, ")
            .Append("							adPrgVerTestDetails t5, ")
            .Append("							adReqGrpDef t6, ")
            .Append("						adLeadByLeadGroups t7  ")
            .Append("      					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" t1.reqforEnrollment=1 and ")
            .Append("      						t1.adReqId = t2.adReqId and ")
            .Append("      						t1.adreqTypeId in (1) and ")
            .Append("      						t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and ")
            .Append("						t3.LeadGrpId = t7.LeadGrpId and ")
            .Append("      						t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
            .Append("	      					and t7.LeadId = '" & LeadId & "'  and  ")
            .Append("						t5.PrgVerId = '" & PrgVerId & "'   ")
            .Append("      		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" union ")
            ' Get Requirements that were assigned to lead group but not part of any requirement group
            .Append("	     select  distinct ")
            .Append("	           adReqId, ")
            .Append("	           Descrip, ")
            .Append("	           ReqGrpId, ")
            .Append("	           StartDate, ")
            .Append("	           EndDate, ")
            .Append("	           ActualScore, ")
            .Append("	           TestTaken, ")
            .Append("	           Comments, ")
            .Append("	           OverRide, ")
            .Append("	           Minscore, ")
            .Append("	           Required, ")
            .Append("	           Case  when ActualScore >= Minscore  then 'True' else 'False' End as Pass, ")
            .Append("	           DocSubmittedCount, ")
            .Append("	           DocStatusDescrip ")
            .Append("           from  ")
            .Append("           ( ")
            ' Get Requirement Group and requirements that are assigned to a lead group
            ' and part of a requirement group
            .Append("          	 select  distinct ")
            .Append("	           	t1.adReqId, ")
            .Append("	           	t1.Descrip, ")
            .Append("	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            .Append("	           	'" & strEnrollDate & " ' as CurrentDate,  ")
            .Append("	           	t2.StartDate, ")
            .Append("	           	t2.EndDate,  ")
            .Append("	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId = '" & LeadId & "' ) as ActualScore,  ")
            .Append("	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId = '" & LeadId & "' ) as TestTaken,  ")
            .Append("	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId = '" & LeadId & "' ) as Comments,  ")
            .Append("	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("	                              LeadId = '" & LeadId & "' ) ")
            .Append("	                      as override, ")
            .Append("	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
            .Append("	           		LeadId = '" & LeadId & "' ) as DocSubmittedCount, ")
            .Append("	           	t2.Minscore, ")
            .Append("			IsNull(t3.IsRequired,0) as Required, ")
            .Append("	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("	           s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("           from ")
            .Append("           	adReqs t1, ")
            .Append("           	adReqsEffectiveDates t2, ")
            .Append("		adReqLeadGroups t3 ")
            .Append("           where  ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" t1.reqforEnrollment=1 and ")
            .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId ")
            .Append("		and ")
            .Append("		t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId='" & PrgVerId & "' and s2.adReqId is null) ")
            .Append("		and t3.LeadGrpId in ")
            .Append("		(select LeadGrpId from adLeadByLeadGroups where LeadId = '" & LeadId & "' ) and ")
            .Append("           	t1.adReqTypeId in (1) and t2.MandatoryRequirement <> 1 and ")
            .Append("		t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId='" & PrgVerId & "') ")
            .Append("           ) ")
            .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" ) R2 where ReqGrpId='" & ReqGrpId & "' ")
        End With


        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        dadNorthwind = New OleDbDataAdapter(sb1.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "Parent")

        conNorthwind.Close()
        Return dstNorthwind
    End Function
    Public Function GetDocumentsByGroupAndLeadAndPrgVersion(ByVal LeadId As String, ByVal PrgVerId As String, ByVal strEnrollDate As Date, ByVal ReqGrpId As String) As DataSet
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim sb1 As New StringBuilder

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        With sb1
            .Append(" select  distinct ")
            .Append("       adReqId, ")
            .Append("       Descrip, ")
            .Append("       ReqGrpId, ")
            .Append("       StartDate,  ")
            .Append("       EndDate, ")
            .Append("       ActualScore, ")
            .Append("       TestTaken, ")
            .Append("       Comments, ")
            .Append("       Case when OverRide>=1 then 'True' else 'False' end as OverRide, ")
            .Append("       Case when (select count(*) from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') >=1 then ")
            .Append("       (select MinScore from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') ")
            .Append("       else MinScore end as MinScore, ")
            .Append("       Required, ")
            .Append("       DocSubmittedCount, ")
            .Append("       DocStatusDescrip, ")
            .Append("       Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount ")
            .Append("       from  ")
            .Append("       ( ")
            ' Get Requirement Group and requirements of mandatory requirement
            .Append("       	select  ")
            .Append("			distinct ")
            .Append("		       	adReqId, ")
            .Append("		      		Descrip, ")
            .Append("		      		ReqGrpId, ")
            .Append("		      		StartDate, ")
            .Append("		      		EndDate, ")
            .Append("		      		ActualScore, ")
            .Append("		      		TestTaken, ")
            .Append("		      		Comments, ")
            .Append("		      		OverRide, ")
            .Append("		      		Minscore, ")
            .Append("		      		Required, ")
            .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
            .Append("		      		DocSubmittedCount, ")
            .Append("		      		DocStatusDescrip ")
            .Append("      	from  ")
            .Append("      		( ")
            .Append("      		select Distinct ")
            .Append("      				t1.adReqId, ")
            .Append("      				t1.Descrip, ")
            .Append("      				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
            .Append("      					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
            .Append("      					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
            .Append("      				'" & strEnrollDate & "' as CurrentDate, ")
            .Append("      				t2.StartDate, ")
            .Append("      				t2.EndDate, ")
            .Append("      				(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("      					LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append("      				(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("      					LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append("      				(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("      					LeadId = '" & LeadId & "' ) as Comments, ")
            .Append("      				       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("      				        LeadId = '" & LeadId & "' ) as override, ")
            .Append("      				(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            .Append("      					LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append("      				t2.Minscore, ")
            .Append("      				1 as Required, ")
            .Append("      				(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("      				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("      				s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("      				from  ")
            .Append("      				adReqs t1, ")
            .Append("      				adReqsEffectiveDates t2 ")
            .Append("      				where  ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" t1.reqforEnrollment=1 and ")
            .Append("      				t1.adReqId = t2.adReqId and ")
            .Append("       			t2.MandatoryRequirement=1 and ")
            .Append("      				t1.adReqTypeId in (3) ")
            .Append("       				) ")
            .Append("       				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("       		union ")

            ' Get The Requirements that was assigned to a program version
            .Append("       	select  ")
            .Append("			distinct ")
            .Append("		       	adReqId, ")
            .Append("		      		Descrip, ")
            .Append("		      		ReqGrpId, ")
            .Append("		      		StartDate, ")
            .Append("		      		EndDate, ")
            .Append("		      		ActualScore, ")
            .Append("		      		TestTaken, ")
            .Append("		      		Comments, ")
            .Append("		      		OverRide, ")
            .Append("		      		Minscore, ")
            .Append("		      		Required, ")
            .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
            .Append("		      		DocSubmittedCount, ")
            .Append("		      		DocStatusDescrip ")
            .Append("      				from  ")
            .Append("      					( ")
            .Append("      						select Distinct ")
            .Append("      							t1.adReqId, ")
            .Append("      							t1.Descrip, ")
            .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
            .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
            .Append("	      						t2.StartDate, ")
            .Append("	      						t2.EndDate, ")
            .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	      							LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("	      							LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("	      							LeadId = '" & LeadId & "' ) as Comments, ")
            .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("	      						        LeadId = '" & LeadId & "' ) ")
            .Append("	      						as override, ")
            .Append("	      						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            .Append("	      						LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append("		      					t2.Minscore, ")
            .Append("	      						1 as Required, ")
            .Append("	      						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("      							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("      							s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("      						from  ")
            .Append("      							adReqs t1, ")
            .Append("      							adReqsEffectiveDates t2, ")
            .Append("      							adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
            .Append("      						where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" t1.reqforEnrollment=1 and ")
            .Append("      							t1.adReqId = t2.adReqId and t2.MandatoryRequirement <> 1 and ")
            .Append("      							t1.adreqTypeId in (3) and ")
            .Append("      							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId ")
            .Append("      							and t1.adReqId = t5.adReqId and t5.PrgVerId = '" & PrgVerId & "'     ")
            ''Commented for DE5552
            '.Append("      					      and		t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where ")
            '.Append("	      						LeadId = '" & LeadId & "' ) ")
            .Append("      							and t5.adReqId is not null ")
            .Append("      						) ")
            .Append("      						R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" union ")
            ' Get Requirements === Requirement Group assigned to Program Version
            .Append("       	select  ")
            .Append("			distinct ")
            .Append("		       	adReqId, ")
            .Append("		      		Descrip, ")
            .Append("		      		ReqGrpId, ")
            .Append("		      		StartDate, ")
            .Append("		      		EndDate, ")
            .Append("		      		ActualScore, ")
            .Append("		      		TestTaken, ")
            .Append("		      		Comments, ")
            .Append("		      		OverRide, ")
            .Append("		      		Minscore, ")
            .Append("		      		Required, ")
            .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
            .Append("		      		DocSubmittedCount, ")
            .Append("		      		DocStatusDescrip ")
            .Append("      				from  ")
            .Append("      					( ")
            .Append("      						select Distinct ")
            .Append("      							t1.adReqId, ")
            .Append("      							t1.Descrip, ")
            .Append("	      						t6.ReqGrpId as reqGrpId, ")
            .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
            .Append("	      						t2.StartDate, ")
            .Append("	      						t2.EndDate, ")
            .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	      							LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("	      							LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("	      							LeadId = '" & LeadId & "' ) as Comments, ")
            .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("	      						        LeadId = '" & LeadId & "' ) ")
            .Append("	      						as override, ")
            .Append("	      						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            .Append("	      						LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append("		      					t2.Minscore, ")
            .Append("	      						IsNUll(t3.IsRequired,0) as Required, ")
            .Append("	      						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("      							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("      							s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("      						from  ")
            .Append("      							adReqs t1, ")
            .Append("							adReqsEffectiveDates t2, ")
            .Append("							adReqLeadGroups t3, ")
            .Append("							adPrgVerTestDetails t5, ")
            .Append("							adReqGrpDef t6, ")
            .Append("						adLeadByLeadGroups t7  ")
            .Append("      					where ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" t1.reqforEnrollment=1 and ")
            .Append("      						t1.adReqId = t2.adReqId and ")
            .Append("      						t1.adreqTypeId in (3) and ")
            .Append("      						t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and ")
            .Append("						t3.LeadGrpId = t7.LeadGrpId and ")
            .Append("      						t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
            .Append("	      					and t7.LeadId = '" & LeadId & "'  and  ")
            .Append("						t5.PrgVerId = '" & PrgVerId & "'   ")
            .Append("      		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" union ")
            ' Get Requirements that were assigned to lead group but not part of any requirement group
            .Append("	     select  distinct ")
            .Append("	           adReqId, ")
            .Append("	           Descrip, ")
            .Append("	           ReqGrpId, ")
            .Append("	           StartDate, ")
            .Append("	           EndDate, ")
            .Append("	           ActualScore, ")
            .Append("	           TestTaken, ")
            .Append("	           Comments, ")
            .Append("	           OverRide, ")
            .Append("	           Minscore, ")
            .Append("	           Required, ")
            .Append("	           Case  when ActualScore >= Minscore  then 'True' else 'False' End as Pass, ")
            .Append("	           DocSubmittedCount, ")
            .Append("	           DocStatusDescrip ")
            .Append("           from  ")
            .Append("           ( ")
            ' Get Requirement Group and requirements that are assigned to a lead group
            ' and part of a requirement group
            .Append("          	 select  distinct ")
            .Append("	           	t1.adReqId, ")
            .Append("	           	t1.Descrip, ")
            .Append("	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            .Append("	           	'" & strEnrollDate & " ' as CurrentDate,  ")
            .Append("	           	t2.StartDate, ")
            .Append("	           	t2.EndDate,  ")
            .Append("	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId = '" & LeadId & "' ) as ActualScore,  ")
            .Append("	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId = '" & LeadId & "' ) as TestTaken,  ")
            .Append("	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("	           		LeadId = '" & LeadId & "' ) as Comments,  ")
            .Append("	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("	                              LeadId = '" & LeadId & "' ) ")
            .Append("	                      as override, ")
            .Append("	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
            .Append("	           		LeadId = '" & LeadId & "' ) as DocSubmittedCount, ")
            .Append("	           	t2.Minscore, ")
            .Append("			isNull(t3.IsRequired,0) as Required, ")
            .Append("	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("	           s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("           from ")
            .Append("           	adReqs t1, ")
            .Append("           	adReqsEffectiveDates t2, ")
            .Append("		adReqLeadGroups t3 ")
            .Append("           where  ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" t1.reqforEnrollment=1 and ")
            .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId ")
            .Append("		and ")
            .Append("		t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId='" & PrgVerId & "' and s2.adReqId is null) ")
            .Append("		and t3.LeadGrpId in ")
            .Append("		(select LeadGrpId from adLeadByLeadGroups where LeadId = '" & LeadId & "' ) and ")
            .Append("           	t1.adReqTypeId in (3) and t2.MandatoryRequirement <> 1 and ")
            .Append("		t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId='" & PrgVerId & "') ")
            .Append("           ) ")
            .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" ) R2 where ReqGrpId='" & ReqGrpId & "' ")
        End With


        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        dadNorthwind = New OleDbDataAdapter(sb1.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "Parent")

        conNorthwind.Close()
        Return dstNorthwind
    End Function
    Public Function GetAdmissionRequirements(ByVal LeadId As String, ByVal PrgVerId As String, ByVal strEnrollDate As Date) As String
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim drowRootParent As DataRow
        Dim drowSchoolLevelSummary As DataRow
        Dim drowSchoolLevelDetails As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb, sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
        Dim drowRequirementAssignedDirectlyToPrgVersion, drowRequirementAssignedDirectlyToLeadGroup As DataRow
        Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder

        Dim intRowCounter As Integer

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        'This query gets a list of all Lead Groups
        With sbroot
            .Append(" select Distinct t1.LeadGrpId,Descrip from adLeadGroups t1,adLeadByLeadGroups t2 where t1.LeadGrpId=t2.LeadGrpId and t2.LeadId='" & LeadId & "'  ")
        End With

        dadNorthwind = New OleDbDataAdapter(sbroot.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "ParentRoot")


        'Get the summary of all school level requirements
        With sbMandatorySummary
            '*********************************************************************************************************************************************
            ' This Query gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted for Mandatory Requirement Group
            '*********************************************************************************************************************************************
            .Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,R5.TestPassed+R5.DocsApproved+R5.TestDocumentAttempted as AttemptedReqs ")
            .Append(" from ")
            .Append(" ( ")
            .Append(" select t7.ReqGrpId,t7.Descrip, ")
            .Append(" (select Count(*) as NumReqs from ")
            .Append(" (select Distinct A2.adReqId,'12/10/2006' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" from adReqs A2,adReqsEffectiveDates A3 ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" where  A2.reqforEnrollment=1 and ")
            .Append("  A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and A2.adreqTypeId in (1,3)) ")
            .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as NumReqs, ")
            ''''''''''''' This Query gives the number of test passed by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'12/10/2006' as CurrentDate,A3.StartDate,A3.EndDate  ")
            .Append(" from adReqs A2,adReqsEffectiveDates A3 ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" where  A2.reqforEnrollment=1 and ")
            .Append("  A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and A2.adreqTypeId=1) R1,adLeadEntranceTest R2  where ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "'  and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and R2.Pass=1 and ")
            .Append(" R2.EntrTestId not in (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "') and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as TestPassed,")
            ''''''''''''' This Query gives the number of docs approved by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'12/10/2006' as CurrentDate,A3.StartDate,A3.EndDate from   adReqs A2,adReqsEffectiveDates A3 ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" where  A2.reqforEnrollment=1 and ")
            .Append("  A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and A2.adreqTypeId=3) R1,adLeadDocsReceived R2,syDocStatuses R3  ")
            .Append(" where R1.adReqId = R2.DocumentId and R2.LeadId='" & LeadId & "'  and R2.DocumentId not in  (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "') ")
            .Append(" and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as DocsApproved,  ")
            ''''''''''''' This Query gives the number of test and documents approved by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'12/10/2006' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" from   adReqs A2,adReqsEffectiveDates A3 where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and A2.adreqTypeId in (1,3) ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" and  A2.reqforEnrollment=1 ) ")
            .Append(" R1,adEntrTestOverRide R2 where R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "'  ")
            .Append(" and R1.CurrentDate >= R1.StartDate and R2.OverRide=1 and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" ) as TestDocumentAttempted,NULL as LeadGrpId from adReqGroups t7 ")
            .Append(" where IsMandatoryReqGrp = 1  ")
            .Append(" ) R5 ")
        End With
        'End of the summary of all school level requirements

        'Start query to get all school level requirements
        With sbMandatoryDetails
            .Append(" select distinct t1.adReqId,Descrip, ")
            .Append(" case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then (select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
            .Append(" (select Distinct ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append(" (select Distinct TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append(" (select Distinct Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as Comments, ")
            .Append(" (select Distinct OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as override, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,1 as Required, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,'00000000-0000-0000-0000-000000000000' as LeadGrpId ")

            .Append(" from (select Distinct adReqId,Descrip from adReqs where adReqTypeId in (1,3) and  reqforEnrollment=1) t1, ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking

            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement=1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2 ")
            .Append(" where t1.adReqId = t2.adReqId ")
        End With
        'End query to get all school level requirements

        dadNorthwind.SelectCommand = New OleDbCommand(sbMandatorySummary.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "SchoolLevelRequirementSummary")

        dadNorthwind.SelectCommand = New OleDbCommand(sbMandatoryDetails.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "SchoolLevelRequirementDetails")

        'Set the relationship between school level requirements
        dstNorthwind.Relations.Add("SchoolLevelRelation", dstNorthwind.Tables("SchoolLevelRequirementSummary").Columns("ReqGrpId"),
        dstNorthwind.Tables("SchoolLevelRequirementDetails").Columns("ReqGrpId"))

        'Query gets the requirement group summary assigned to LeadGroup and Program Version
        With sb
            .Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,R5.TestPassed+R5.DocsApproved+R5.TestDocumentAttempted as AttemptedReqs,  ")
            .Append(" Case when LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            '*********************************************************************************************************************************************
            ' This Query gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted by Lead and Program Version 
            '*********************************************************************************************************************************************
            .Append(" (Select  Distinct t1.ReqGrpId,t2.Descrip,t3.NumReqs as Numreqs,  ")
            ''''''''''''' This Query gives the number of test passed by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'12/10/2006' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and  ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append("   A2.reqforEnrollment=1 and ")
            .Append(" A4.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=1) ")
            .Append(" R1,adLeadEntranceTest R2 WHERE ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "' and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and R2.Pass=1 and ")
            .Append(" R2.EntrTestId not in (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "')  and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) Group by LeadGrpId)as TestPassed, ")
            ''''''''''''' This Query gives the number of documents approved by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from  ")
            .Append(" (select Distinct A2.adReqId,'12/10/2006' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append("   A2.reqforEnrollment=1 and ")
            .Append(" A4.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            .Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=3) R1,adLeadDocsReceived R2,syDocStatuses R3 WHERE ")
            .Append(" R1.adReqId = R2.DocumentId and R2.LeadId='" & LeadId & "'  and R2.DocumentId not in (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "') ")
            .Append(" and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) Group by LeadGrpId) as DocsApproved, ")
            '''''''''''' This Query gives the number of test and documents attempted(no matter pass or fail,no matter approved/not approved) grouped by lead group ''''''''
            .Append(" (select Count(*)  from ")
            .Append(" (select Distinct A2.adReqId,'12/10/2006' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append("   A2.reqforEnrollment=1 and ")
            .Append(" A4.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            .Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (1,3)) R1,adEntrTestOverRide R2 ")
            .Append(" where R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "'  and R1.CurrentDate >= R1.StartDate and R2.OverRide=1 and ")
            .Append(" (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) Group BY LeadGrpId) as TestDocumentAttempted,t4.LeadGrpId ")
            .Append(" from adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3,adLeadGroups t4  where ")
            .Append(" t1.ReqGrpId = t2.ReqGrpId and t2.ReqGrpId = t3.ReqGrpId and t3.LeadGrpId = t4.LeadGrpId and ")
            .Append(" t1.PrgVerId='" & PrgVerId & "'  and t2.IsMandatoryReqGrp <> 1  ")
            .Append(" and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') ")
            .Append(" ) R5 ")
            '**************************************************************************************************************************************************************
            'End of the  Query that gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted by Lead and Program Version
            '**************************************************************************************************************************************************************
        End With

        'Query gets the requirements assigned to LeadGroup and Program Version
        With sb1
            .Append(" select distinct t1.adReqId,Descrip, ")
            .Append(" t6.ReqGrpId as ReqGrpId, ")
            .Append(" (select Distinct ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append(" (select Distinct TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append(" (select Distinct Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as Comments, ")
            .Append(" (select Distinct OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as override, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and LeadId = '" & LeadId & "' ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,IsNULL(t3.IsRequired,0) as Required, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
            .Append(" Case when t3.LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t3.LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" (select Distinct adReqId,Descrip from adReqs where  reqforEnrollment=1 and adReqTypeId in (1,3)) t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append(" (select Distinct LeadGrpId,adReqEffectiveDateId,IsNUll(IsRequired,0) from adReqLeadGroups where LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' )) t3, ")
            .Append(" (select Distinct ReqGrpId from adPrgVerTestDetails  where PrgVerId = '" & PrgVerId & "'  and ReqGrpId is not null) t5, ")
            .Append(" (select Distinct adReqId,ReqGrpId,LeadGrpId from adReqGrpDef) t6, ")
            .Append(" (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') t7  ")
            .Append(" where t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            .Append(" t5.ReqGrpId = t6.ReqGrpId And t3.LeadGrpId = t7.LeadGrpId And t6.LeadGrpId = t7.LeadGrpId And t1.adReqId = t6.adReqId ")
        End With

        dadNorthwind.SelectCommand = New OleDbCommand(sb.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "Parent")

        dadNorthwind.SelectCommand = New OleDbCommand(sb1.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "Child")

        dstNorthwind.Relations.Add("myRootRelation", dstNorthwind.Tables("ParentRoot").Columns("LeadGrpId"),
                                                     dstNorthwind.Tables("Parent").Columns("LeadGrpId"))

        Dim colsParent, colsChild As DataColumn()
        colsParent = New DataColumn() {dstNorthwind.Tables("Parent").Columns("ReqGrpId"), dstNorthwind.Tables("Parent").Columns("LeadGrpId")}
        colsChild = New DataColumn() {dstNorthwind.Tables("Child").Columns("ReqGrpId"), dstNorthwind.Tables("Child").Columns("LeadGrpId")}
        Dim rel As DataRelation
        rel = New DataRelation("myrelation", colsParent, colsChild)
        dstNorthwind.Relations.Add(rel)

        With sbReqAssignedDirectlyToPrgVersion
            ' Requirements(not part of any requirement group) assigned directly to program version
            .Append(" select distinct t1.adReqId,Descrip, ")
            .Append(" t6.ReqGrpId as ReqGrpId, ")
            .Append(" (select Distinct ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append(" (select Distinct TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append(" (select Distinct Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as Comments, ")
            .Append(" (select Distinct OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as override, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and LeadId = '" & LeadId & "' ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,ISNUll(t3.IsRequired,0) as Required, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
            .Append(" Case when t3.LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t3.LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" (select Distinct adReqId,Descrip from adReqs where reqforEnrollment=1 and adReqTypeId in (1,3)) t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append(" (select Distinct LeadGrpId,adReqEffectiveDateId,ISNUll(IsRequired,0) from adReqLeadGroups where LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')) t3, ")
            .Append(" (select Distinct ReqGrpId from adPrgVerTestDetails  where PrgVerId = '" & PrgVerId & "'  and ReqGrpId is not null) t5, ")
            .Append(" (select Distinct adReqId,ReqGrpId,LeadGrpId from adReqGrpDef) t6, ")
            .Append(" (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' ) t7  ")
            .Append(" where t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            .Append(" t5.ReqGrpId = t6.ReqGrpId And t3.LeadGrpId = t7.LeadGrpId And t6.LeadGrpId = t7.LeadGrpId And t1.adReqId = t6.adReqId ")
        End With

        With sbReqsAssignedDirectlyToLeadGroup
            .Append(" select distinct t1.adReqId,Descrip, ")
            .Append(" '00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            .Append(" (select Distinct ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append(" (select Distinct TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append(" (select Distinct Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as Comments, ")
            .Append(" (select Distinct OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as override, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and LeadId = '" & LeadId & "' ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,ISNUll(t3.IsRequired,0) as Required, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
            .Append(" Case when t3.LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t3.LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" (select Distinct adReqId,Descrip from adReqs where reqforEnrollment=1 and adReqTypeId in (1,3)) t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append(" adReqLeadGroups t3 ")
            .Append(" where ")
            .Append(" t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            ' Requirement should not be in requirement group assigned to program version
            .Append(" t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId='" & PrgVerId & "' and s2.adReqId is null)   ")
            .Append(" and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and ")
            ' Requirement should not be directly assigned to program version
            .Append(" t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId='" & PrgVerId & "') ")
        End With

        dadNorthwind.SelectCommand = New OleDbCommand(sbReqAssignedDirectlyToPrgVersion.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "RequirementAssignedDirectlyToPrgVersion")

        'Set Relationship between LeadGroups and RequirementAssignedDirectlyToPrgVersion
        dstNorthwind.Relations.Add("RequirementAssignedToPrgVersionRelation", dstNorthwind.Tables("ParentRoot").Columns("LeadGrpId"),
                                    dstNorthwind.Tables("RequirementAssignedDirectlyToPrgVersion").Columns("LeadGrpId"))

        dadNorthwind.SelectCommand = New OleDbCommand(sbReqsAssignedDirectlyToLeadGroup.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "RequirementAssignedDirectlyToLeadGroup")

        'Set Relationship between LeadGroups and RequirementAssignedDirectlyToPrgVersion
        dstNorthwind.Relations.Add("RequirementAssignedDirectlyToLeadGroupRelation", dstNorthwind.Tables("ParentRoot").Columns("LeadGrpId"),
                                    dstNorthwind.Tables("RequirementAssignedDirectlyToLeadGroup").Columns("LeadGrpId"))
        conNorthwind.Close()

        'Display School Level Requirements
        s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"" BorderWidth=""1px"" BorderStyle=""Solid"" BorderColor=""#A3C7E2"">"
        intRowCounter = 0
        For Each drowSchoolLevelSummary In dstNorthwind.Tables("SchoolLevelRequirementSummary").Rows
            If Not drowSchoolLevelSummary("Descrip") Is DBNull.Value Then
                s3 &= "<TR>"
                s3 &= "<td nowrap><font face=""verdana"" size=""2""><strong>" & drowSchoolLevelSummary("Descrip") & "</strong>&nbsp;</font>"
                If Not drowSchoolLevelSummary("NumReqs") Is DBNull.Value Then
                    If drowSchoolLevelSummary("NumReqs") >= 1 Then
                        s3 &= "<span style=""font: normal 12px arial; padding: 16px"">[Min Req:" & drowSchoolLevelSummary("NumReqs") & "]"
                        s3 &= "[Min Attempted:" & drowSchoolLevelSummary("AttemptedReqs") & "]</span>"
                    End If
                End If
                s3 &= "</td></tr>"
            Else
                s3 &= "<TR><td><font face=""verdana"" size=""2""><strong>Miscellaneous</strong></font></td></tr>"
            End If
            s3 &= "<tr>"
            intRowCounter = 0
            For Each drowSchoolLevelDetails In drowSchoolLevelSummary.GetChildRows("SchoolLevelRelation")
                If Not drowSchoolLevelDetails("Descrip") Is DBNull.Value Then
                    If Not drowSchoolLevelDetails("TestTaken") Is DBNull.Value Then
                        s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
                    ElseIf Not drowSchoolLevelDetails("TestTaken") Is DBNull.Value And drowSchoolLevelDetails("DocSubmittedCount") >= 1 Then
                        s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
                    Else
                        s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled></li>"
                    End If
                    s3 &= "<span style=""font:  normal 12px arial; padding: 16px"">" & drowSchoolLevelDetails("Descrip")
                    If drowSchoolLevelDetails("Required") = 1 Then
                        s3 &= "<span style=""color:red; padding-left: 3px;font: normal 12px arial;""><strong>(Required)</strong></span>"
                    End If

                    'Check If the Lead Has Passed The Test
                    If Not drowSchoolLevelDetails("ActualScore") Is DBNull.Value Then
                        If drowSchoolLevelDetails("MinScore") Is DBNull.Value Then
                            s3 &= ""
                        Else
                            If drowSchoolLevelDetails("ActualScore") >= 1 Then
                                If (drowSchoolLevelDetails("ActualScore") < drowSchoolLevelDetails("MinScore")) And (Not drowSchoolLevelDetails("TestTaken") = "") Then
                                    s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Fail)</strong></span>"
                                ElseIf (drowSchoolLevelDetails("ActualScore") >= drowSchoolLevelDetails("MinScore")) And (Not drowSchoolLevelDetails("TestTaken") = "") Then
                                    s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Pass)</strong></span>"
                                End If
                            End If
                        End If
                    End If

                    'Check If Document was Approved
                    If Not drowSchoolLevelDetails("DocStatusDescrip") Is DBNull.Value Then
                        If drowSchoolLevelDetails("DocStatusDescrip") = "Approved" Then
                            s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Approved)</strong></span>"
                        Else
                            s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Not Approved)</strong></span>"
                        End If
                    End If


                    If Not drowSchoolLevelDetails("OverRide") Is DBNull.Value Then
                        If drowSchoolLevelDetails("OverRide") = "True" Then
                            s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Overriden)</strong></span>"
                        End If
                    End If
                End If
                s3 &= "</span></ul>"
                s3 &= "</td>"
                intRowCounter += 1
                If intRowCounter = 3 Then
                    s3 &= "</tr><tr>"
                    intRowCounter = 0
                End If
            Next
            s3 &= "</tr><tr height=10><td>&nbsp;</td></tr>"
            forCounter = 1
        Next
        s3 &= "</table>"
        s3 &= "<p></p>"
        s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
        For Each drowRootParent In dstNorthwind.Tables("ParentRoot").Rows
            If Not drowRootParent("Descrip") Is DBNull.Value Then
                s3 &= "<tr bgcolor=""navy""><td nowrap width=""100%""><font face=""verdana"" size=""2"" color=""white""><strong>Requirements for " & drowRootParent("Descrip") & " Applicants</strong>&nbsp;</font></td></tr>"
            End If
            s3 &= "</table>"
            s3 &= "<p></p>"
            ' Display Requirements assigned directly to Program Version
            s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
            s3 &= "<tr><td nowrap><font face=""verdana"" size=""2""><strong>Miscellaneous Requirements</strong></font></td></tr>"
            s3 &= "</table>"
            s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
            s3 &= "<tr>"
            intRowCounter = 0
            For Each drowRequirementAssignedDirectlyToPrgVersion In drowRootParent.GetChildRows("RequirementAssignedToPrgVersionRelation")
                If Not drowRequirementAssignedDirectlyToPrgVersion("Descrip") Is DBNull.Value Then
                    If Not drowRequirementAssignedDirectlyToPrgVersion("TestTaken") Is DBNull.Value Then
                        s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
                    ElseIf Not drowRequirementAssignedDirectlyToPrgVersion("TestTaken") Is DBNull.Value And drowRequirementAssignedDirectlyToPrgVersion("DocSubmittedCount") >= 1 Then
                        s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
                    Else
                        s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled></li>"
                    End If
                    s3 &= "<span style=""font:  normal 12px arial; padding: 16px"">" & drowRequirementAssignedDirectlyToPrgVersion("Descrip")
                    If drowRequirementAssignedDirectlyToPrgVersion("Required") = 1 Then
                        s3 &= "<span style=""color:red; padding-left: 3px;font: normal 12px arial;""><strong>(Required)</strong></span>"
                    End If

                    'Check If the Lead Has Passed The Test
                    If Not drowRequirementAssignedDirectlyToPrgVersion("ActualScore") Is DBNull.Value Then
                        If drowRequirementAssignedDirectlyToPrgVersion("MinScore") Is DBNull.Value Then
                            s3 &= ""
                        Else
                            If drowRequirementAssignedDirectlyToPrgVersion("ActualScore") >= 1 Then
                                If (drowRequirementAssignedDirectlyToPrgVersion("ActualScore") < drowRequirementAssignedDirectlyToPrgVersion("MinScore")) And (Not drowRequirementAssignedDirectlyToPrgVersion("TestTaken") = "") Then
                                    s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Fail)</strong></span>"
                                ElseIf (drowRequirementAssignedDirectlyToPrgVersion("ActualScore") >= drowRequirementAssignedDirectlyToPrgVersion("MinScore")) And (Not drowRequirementAssignedDirectlyToPrgVersion("TestTaken") = "") Then
                                    s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Pass)</strong></span>"
                                End If
                            End If
                        End If
                    End If

                    'Check If Document was Approved
                    If Not drowRequirementAssignedDirectlyToPrgVersion("DocStatusDescrip") Is DBNull.Value Then
                        If drowRequirementAssignedDirectlyToPrgVersion("DocStatusDescrip") = "Approved" Then
                            s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Approved)</strong></span>"
                        Else
                            s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Not Approved)</strong></span>"
                        End If
                    End If


                    If Not drowRequirementAssignedDirectlyToPrgVersion("OverRide") Is DBNull.Value Then
                        If drowRequirementAssignedDirectlyToPrgVersion("OverRide") = "True" Then
                            s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Overriden)</strong></span>"
                        End If
                    End If
                End If
                s3 &= "</span></ul>"
                s3 &= "</td>"
                intRowCounter += 1
                If intRowCounter = 3 Then
                    s3 &= "</tr><tr>"
                    intRowCounter = 0
                End If
            Next
            s3 &= "</tr><tr height=10><td>&nbsp;</td></tr>"
            's3 &= "</table>"
            'End of Requirements assigned Directly to Program Version
            'Start Requirements assigned Directly to LeadGroup
            's3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
            's3 &= "<tr><td nowrap><font face=""verdana"" size=""2""><strong>Requirements for " & drowRootParent("Descrip") & " Applicants </strong>&nbsp;</font></td></tr>"
            's3 &= "</table>"
            ' Display Requirements assigned directly to Program Version
            's3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
            s3 &= "<tr>"
            intRowCounter = 0
            For Each drowRequirementAssignedDirectlyToLeadGroup In drowRootParent.GetChildRows("RequirementAssignedDirectlyToLeadGroupRelation")
                If Not drowRequirementAssignedDirectlyToLeadGroup("Descrip") Is DBNull.Value Then
                    If Not drowRequirementAssignedDirectlyToLeadGroup("TestTaken") Is DBNull.Value Then
                        s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
                    ElseIf Not drowRequirementAssignedDirectlyToLeadGroup("TestTaken") Is DBNull.Value And drowRequirementAssignedDirectlyToLeadGroup("DocSubmittedCount") >= 1 Then
                        s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
                    Else
                        s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled></li>"
                    End If
                    s3 &= "<span style=""font:  normal 12px arial; padding: 16px"">" & drowRequirementAssignedDirectlyToLeadGroup("Descrip")
                    If drowRequirementAssignedDirectlyToLeadGroup("Required") = 1 Then
                        s3 &= "<span style=""color:red; padding-left: 3px;font: normal 12px arial;""><strong>(Required)</strong></span>"
                    End If

                    'Check If the Lead Has Passed The Test
                    If Not drowRequirementAssignedDirectlyToLeadGroup("ActualScore") Is DBNull.Value Then
                        If drowRequirementAssignedDirectlyToLeadGroup("MinScore") Is DBNull.Value Then
                            s3 &= ""
                        Else
                            If drowRequirementAssignedDirectlyToLeadGroup("ActualScore") >= 1 Then
                                If (drowRequirementAssignedDirectlyToLeadGroup("ActualScore") < drowRequirementAssignedDirectlyToLeadGroup("MinScore")) And (Not drowRequirementAssignedDirectlyToLeadGroup("TestTaken") = "") Then
                                    s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Fail)</strong></span>"
                                ElseIf (drowRequirementAssignedDirectlyToLeadGroup("ActualScore") >= drowRequirementAssignedDirectlyToLeadGroup("MinScore")) And (Not drowRequirementAssignedDirectlyToLeadGroup("TestTaken") = "") Then
                                    s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Pass)</strong></span>"
                                End If
                            End If
                        End If
                    End If

                    'Check If Document was Approved
                    If Not drowRequirementAssignedDirectlyToLeadGroup("DocStatusDescrip") Is DBNull.Value Then
                        If drowRequirementAssignedDirectlyToLeadGroup("DocStatusDescrip") = "Approved" Then
                            s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Approved)</strong></span>"
                        Else
                            s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Not Approved)</strong></span>"
                        End If
                    End If


                    If Not drowRequirementAssignedDirectlyToLeadGroup("OverRide") Is DBNull.Value Then
                        If drowRequirementAssignedDirectlyToLeadGroup("OverRide") = "True" Then
                            s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Overriden)</strong></span>"
                        End If
                    End If
                End If
                s3 &= "</span></ul>"
                s3 &= "</td>"
                intRowCounter += 1
                If intRowCounter = 3 Then
                    s3 &= "</tr><tr>"
                    intRowCounter = 0
                End If
            Next
            s3 &= "</tr><tr height=10><td>&nbsp;</td></tr>"
            s3 &= "</table>"
            'End of Requirement assigned directly to Lead Group
            s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"" BorderWidth=""1px"" BorderStyle=""Solid"" BorderColor=""#A3C7E2"">"
            For Each drowParent In dstNorthwind.Tables("Parent").Rows
                If Not drowParent("Descrip") Is DBNull.Value Then
                    s3 &= "<TR>"
                    s3 &= "<td nowrap><font face=""verdana"" size=""2""><strong>" & drowParent("Descrip") & "</strong>&nbsp;</font>"
                    If Not drowParent("NumReqs") Is DBNull.Value Then
                        If drowParent("NumReqs") >= 1 Then
                            s3 &= "<span style=""font: normal 12px arial; padding: 16px"">[Min Req:" & drowParent("NumReqs") & "]"
                            s3 &= "[Min Attempted:" & drowParent("AttemptedReqs") & "]</span>"
                        End If
                    End If
                    s3 &= "</td></tr>"
                Else
                    s3 &= "<TR><td><font face=""verdana"" size=""2""><strong>Miscellaneous</strong></font></td></tr>"
                End If
                s3 &= "</table>"
                s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
                s3 &= "<tr>"
                intRowCounter = 0
                For Each drowChild In drowParent.GetChildRows("myrelation")
                    If Not drowChild("Descrip") Is DBNull.Value Then
                        If Not drowChild("TestTaken") Is DBNull.Value Then
                            s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
                        ElseIf Not drowChild("TestTaken") Is DBNull.Value And drowChild("DocSubmittedCount") >= 1 Then
                            s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
                        Else
                            s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled></li>"
                        End If
                        s3 &= "<span style=""font:  normal 12px arial; padding: 16px"">" & drowChild("Descrip")
                        If drowChild("Required") = 1 Then
                            s3 &= "<span style=""color:red; padding-left: 3px;font: normal 12px arial;""><strong>(Required)</strong></span>"
                        End If

                        'Check If the Lead Has Passed The Test
                        If Not drowChild("ActualScore") Is DBNull.Value Then
                            If drowChild("MinScore") Is DBNull.Value Then
                                s3 &= ""
                            Else
                                If drowChild("ActualScore") >= 1 Then
                                    If (drowChild("ActualScore") < drowChild("MinScore")) And (Not drowChild("TestTaken") = "") Then
                                        s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Fail)</strong></span>"
                                    ElseIf (drowChild("ActualScore") >= drowChild("MinScore")) And (Not drowChild("TestTaken") = "") Then
                                        s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Pass)</strong></span>"
                                    End If
                                End If
                            End If
                        End If

                        'Check If Document was Approved
                        If Not drowChild("DocStatusDescrip") Is DBNull.Value Then
                            If drowChild("DocStatusDescrip") = "Approved" Then
                                s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Approved)</strong></span>"
                            Else
                                s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Not Approved)</strong></span>"
                            End If
                        End If


                        If Not drowChild("OverRide") Is DBNull.Value Then
                            If drowChild("OverRide") = "True" Then
                                s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Overriden)</strong></span>"
                            End If
                        End If


                    End If
                    s3 &= "</span></ul>"
                    s3 &= "</td>"
                    intRowCounter += 1
                    If intRowCounter = 3 Then
                        s3 &= "</tr><tr>"
                        intRowCounter = 0
                    End If
                Next
                s3 &= "</tr><tr height=10><td>&nbsp;</td></tr>"
                forCounter = 1
            Next
        Next
        s3 &= "</table>"

        If forCounter = 1 Then
            Return s3.ToString()
        Else
            Return "None"
        End If
    End Function
    Public Function GetEntranceTestId(ByVal LeadId As String) As DataSet
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        With sb
            .Append(" select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "' ")
        End With
        'Build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(myAdvAppSettings.AppSettings("ConString")))
        Dim da As New OleDbDataAdapter(sc)
        da.Fill(ds, "ReqGroups")
        sb.Remove(0, sb.Length)
        Return ds
    End Function
    Public Function GetDescrip(ByVal PrgVerId As String) As String
        Dim sb As New StringBuilder
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        With sb
            .Append(" select Distinct PrgVerDescrip from arPrgVersions where PrgVerId='" & PrgVerId & "' ")
        End With
        'Build select command
        Dim strPrgVerDescrip As String = db.RunParamSQLScalar(sb.ToString)
        Return strPrgVerDescrip
    End Function
    Public Function GetLeadGroups(ByVal LeadId As String) As DataSet
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        With sb
            .Append(" select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' ")
        End With
        'Build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(myAdvAppSettings.AppSettings("ConString")))
        Dim da As New OleDbDataAdapter(sc)
        da.Fill(ds, "LeadGroups")
        sb.Remove(0, sb.Length)
        Return ds
    End Function

#Region "Get Requeriments for enroll Lead"

    Public Function GetRequirementsAssignedToProgramVersion(ByVal PrgVerId As String) As DataSet
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        With sb
            .Append(" select Distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId='" & PrgVerId & "' ")
        End With
        'Build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(myAdvAppSettings.AppSettings("ConString")))
        Dim da As New OleDbDataAdapter(sc)
        da.Fill(ds, "GetRequirementsList")
        sb.Remove(0, sb.Length)
        Return ds
    End Function
    Public Function GetRequirementsDirectlyAssignedToProgramVersion(ByVal PrgVerId As String) As DataSet
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        With sb
            .Append(" select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId='" & PrgVerId & "' and s2.adReqId is null ")
        End With
        'Build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(myAdvAppSettings.AppSettings("ConString")))
        Dim da As New OleDbDataAdapter(sc)
        da.Fill(ds, "GetRequirementsList")
        sb.Remove(0, sb.Length)
        Return ds
    End Function

    Public Function GetSchoolLevelAdmissionRequirements(ByVal LeadId As String, ByVal PrgVerId As String, ByVal strEnrollDate As Date, ByVal CampusId As String) As DataSet
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
        Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Dim dsGetCmpGrps As DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        'This query gets a list of all Lead Groups
        With sbroot
            .Append(" select Distinct t1.LeadGrpId,Descrip from adLeadGroups t1,adLeadByLeadGroups t2 where t1.LeadGrpId=t2.LeadGrpId and t2.LeadId='" & LeadId & "'  ")
        End With

        dadNorthwind = New OleDbDataAdapter(sbroot.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "ParentRoot")

        Dim strCurrentDate As String = Date.Now.ToShortDateString

        'Get the summary of all school level requirements
        With sbMandatorySummary
            '*********************************************************************************************************************************************
            ' This Query gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted for Mandatory Requirement Group
            '*********************************************************************************************************************************************
            .Append(" select Top 1 ReqGrpId,R5.Descrip,R5.NumReqs,R5.TestPassed+R5.DocsApproved+R5.TestDocumentAttempted as AttemptedReqs ")
            .Append(" from ")
            .Append(" ( ")
            .Append(" select 'CFD29DFD-4FFF-400B-B152-4FABB12E6E7B' as ReqGrpId, ")
            .Append(" case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then (select Distinct Descrip from adReqGroups where IsMandatoryReqGrp=1) else 'School Level Requirements' end  as Descrip,  ")
            .Append(" CampGrpId, ")
            .Append(" (select Count(*) as NumReqs from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" from adReqs A2,adReqsEffectiveDates A3 ")
            .Append(" where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and A2.StatusId='" & strActiveGUID & "' and ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append("   A2.reqforEnrollment=1 and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" and A2.adreqTypeId in (1,3)) ")
            .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as NumReqs, ")
            ''''''''''''' This Query gives the number of test passed by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate  ")
            .Append(" from adReqs A2,adReqsEffectiveDates A3 ")
            .Append(" where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append("   A2.reqforEnrollment=1 and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" and A2.adreqTypeId=1 and A2.StatusId='" & strActiveGUID & "') R1,adLeadEntranceTest R2  where ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "'  and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and R2.Pass=1 and ")
            .Append(" R2.EntrTestId not in (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "') and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as TestPassed,")
            ''''''''''''' This Query gives the number of docs approved by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate from   adReqs A2,adReqsEffectiveDates A3 ")
            .Append(" where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append("   A2.reqforEnrollment=1 and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" and A2.adreqTypeId=3 and A2.StatusId='" & strActiveGUID & "') R1,adLeadDocsReceived R2,syDocStatuses R3  ")
            .Append(" where R1.adReqId = R2.DocumentId and R2.LeadId='" & LeadId & "'  and R2.DocumentId not in  (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "') ")
            .Append(" and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as DocsApproved,  ")
            ''''''''''''' This Query gives the number of test and documents approved by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" from   adReqs A2,adReqsEffectiveDates A3 where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append("   A2.reqforEnrollment=1 and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" and A2.adreqTypeId in (1,3)  and A2.StatusId='" & strActiveGUID & "') ")
            .Append(" R1,adEntrTestOverRide R2 where R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "'  ")
            .Append(" and R1.CurrentDate >= R1.StartDate and R2.OverRide=1 and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" ) as TestDocumentAttempted,NULL as LeadGrpId  ")
            '.Append(" from adReqGroups t7 where IsMandatoryReqGrp = 1  ")
            .Append("   from adReqs t1,adReqsEffectiveDates t2 ")
            .Append("   where t1.adReqId = t2.adReqId and t2.MandatoryRequirement=1 and t1.adreqTypeId in (1,3)  ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" and  t1.reqforEnrollment=1  ")
            .Append(" ) R5 ")
            If Not strCampGrpId = "" Then
                .Append("  WHERE R5.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With
        'End of the summary of all school level requirements


        'Start query to get all school level requirements
        With sbMandatoryDetails
            .Append(" select distinct t1.adReqId,Descrip,t1.adReqTypeId, ")
            '.Append(" case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then (select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
            .Append(" 'CFD29DFD-4FFF-400B-B152-4FABB12E6E7B' as ReqGrpId, ")
            .Append(" (select Distinct Top 1 ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append(" (select Distinct Top 1 TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append(" (select Distinct Top 1 Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as Comments, ")
            .Append(" (select Distinct Top 1 OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as override, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,1 as Required, ")
            .Append(" (select Distinct Top 1 s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,'00000000-0000-0000-0000-000000000000' as LeadGrpId ")
            ''MOdified by Saraswathi Lakshmanan On Sept 21 2010
            ''For document tracking
            ''Check for the field ReqforEnrollment if it is set to 1.
            .Append(" from (select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and ReqforEnrollment=1 and StatusId='" & strActiveGUID & "') t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement=1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2 ")
            .Append(" where t1.adReqId = t2.adReqId ")
            If Not strCampGrpId = "" Then
                .Append("  and t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With
        'End query to get all school level requirements

        dadNorthwind.SelectCommand = New OleDbCommand(sbMandatorySummary.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "SchoolLevelRequirementSummary")

        dadNorthwind.SelectCommand = New OleDbCommand(sbMandatoryDetails.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "SchoolLevelRequirementDetails")


        'Get Requirements assigned directly to program version and this Requirement should not be part of
        'the requirement group that is assigned to program version
        With sbReqAssignedDirectlyToPrgVersion
            ' Requirements(not part of any requirement group) assigned directly to program version
            .Append(" select distinct t1.adReqId,Descrip,adReqTypeId, ")
            .Append(" '00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            .Append(" (select Distinct Top 1 ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append(" (select Distinct Top 1 TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append(" (select Distinct Top 1 Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as Comments, ")
            .Append(" (select Distinct Top 1 OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as override, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and LeadId = '" & LeadId & "' ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,1 as Required, ")
            .Append(" (select Distinct Top 1 s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append(" from ")
            ''MOdified by Saraswathi Lakshmanan On Sept 21 2010
            ''For document tracking
            ''Check for the field ReqforEnrollment if it is set to 1.
            .Append(" (select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and ReqforEnrollment=1  and StatusId='" & strActiveGUID & "') t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append(" (select Distinct adReqId from adPrgVerTestDetails  where PrgVerId = '" & PrgVerId & "'  and ReqGrpId is null) t5 ")
            .Append(" where t1.adReqId = t2.adReqId and t1.adReqId=t5.adReqId ")
            If Not strCampGrpId = "" Then
                .Append("  and t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With
        dadNorthwind.SelectCommand = New OleDbCommand(sbReqAssignedDirectlyToPrgVersion.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "RequirementsAssignedToProgramVersion")




        '    'Set the relationship between school level requirements
        '    dstNorthwind.Relations.Add("SchoolLevelRelation", dstNorthwind.Tables("SchoolLevelRequirementSummary").Columns("ReqGrpId"), _
        '    dstNorthwind.Tables("SchoolLevelRequirementDetails").Columns("ReqGrpId"))



        With sbReqsAssignedDirectlyToLeadGroup
            .Append(" select distinct t1.adReqId,Descrip, ")
            .Append(" '00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            .Append(" (select Distinct ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append(" (select Distinct TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append(" (select Distinct Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as Comments, ")
            .Append(" (select Distinct OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as override, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and LeadId = '" & LeadId & "' ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,ISNull(t3.IsRequired,0) as Required, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
            .Append(" Case when t3.LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t3.LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            ''MOdified by Saraswathi Lakshmanan On Sept 21 2010
            ''For document tracking
            ''Check for the field ReqforEnrollment if it is set to 1.
            .Append(" (select Distinct adReqId,Descrip,CampGrpId from adReqs where adReqTypeId in (1,3) and ReqforEnrollment=1  and StatusId='" & strActiveGUID & "') t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append(" adReqLeadGroups t3 ")
            .Append(" where ")
            .Append(" t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            ' Requirement should not be in requirement group assigned to program version
            .Append(" t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId='" & PrgVerId & "' and s2.adReqId is null)   ")
            .Append(" and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and ")
            ' Requirement should not be directly assigned to program version
            .Append(" t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId='" & PrgVerId & "') ")
            If Not strCampGrpId = "" Then
                .Append("  and t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With

        dadNorthwind.SelectCommand = New OleDbCommand(sbReqAssignedDirectlyToPrgVersion.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "RequirementAssignedDirectlyToLeadGroup")


        Return dstNorthwind

    End Function

    ''' <summary>
    ''' Get the requeriment (docs and Test) associated with a program version
    ''' The returned list also has the information if the requeriment was passed or not.
    ''' </summary>
    ''' <param name="prgVerId">Program Version Associate with the requeriment</param>
    ''' <param name="leadId">The Lead Id of the prospect student in question</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetProgramVersionRequerimentsStatus(ByVal prgVerId As String, ByVal leadId As String) As IList(Of AdRequerimentsProgramVersion)
        Dim sql As StringBuilder = New StringBuilder()
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim conn As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlConn As SqlConnection = New SqlConnection(conn)

        With sql
            .Append("SELECT DISTINCT")
            .Append("	req.Descrip, req.adReqTypeId, test.MinScore")
            .Append("	, (SELECT Distinct MAX(ActualScore) from adLeadEntranceTest where EntrTestId=req.adReqId and LeadId = @leadId  ) as ActualScore")
            .Append("	, (select Distinct Top 1 OverRide from adEntrTestOverRide where EntrTestId=req.adReqId and LeadId = @leadId ) as TestOverride")
            .Append("	, (select Count(*) from adLeadDocsReceived where DocumentId=req.adReqId and LeadId = @leadId  ) as DocReceived ")
            .Append(",  (select Distinct Top 1 s2.DocStatusCode ")
            .Append("			FROM sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3")
            .Append("			WHERE  s1.SysDocStatusId = s2.sysDocStatusId ")
            .Append("			AND s2.DocStatusId = s3.DocStatusId ")
            .Append("			AND  s3.LeadId = @leadId ")
            .Append("			AND s3.DocumentId = req.adReqId")
            .Append("	) as DocStatusCode ")

            .Append(" FROM adReqs req")
            .Append(" JOIN dbo.adPrgVerTestDetails test ON test.adReqId = req.adReqId")
            .Append(" JOIN adReqsEffectiveDates dates ON dates.adReqId = req.adReqId")
            .Append(" LEFT JOIN dbo.adLeadEntranceTest lead ON lead.EntrTestId = req.adReqId")
            .Append(" WHERE")
            .Append("	dates.MandatoryRequirement <> 1")
            .Append("	AND getdate()>=StartDate ")
            .Append("	AND (getdate()<= EndDate OR EndDate is NULL) ")
            .Append("	AND PrgVerId = @prgVerId")
            .Append("	AND test.ReqGrpId is NULL")
        End With

        Dim command As SqlCommand = New SqlCommand(sql.ToString(), sqlConn)
        command.Parameters.AddWithValue("@prgVerId", prgVerId)
        command.Parameters.AddWithValue("@leadId", leadId)
        Dim resultlist As IList(Of AdRequerimentsProgramVersion) = New List(Of AdRequerimentsProgramVersion)
        sqlConn.Open()
        Try

            Dim reader As SqlDataReader = command.ExecuteReader()
            While reader.Read()
                Dim op As AdRequerimentsProgramVersion = New AdRequerimentsProgramVersion()
                op.Description = reader.GetString(0)
                op.TypeReq = reader.GetInt32(1)
                op.MinScore = reader.GetDecimal(2)
                If reader.IsDBNull(3) Then op.ActualScore = 0 Else op.ActualScore = reader.GetDecimal(3)
                If reader.IsDBNull(4) Then op.Override = False Else op.Override = (reader.GetString(4) = "1")
                op.DocReceived = reader.GetInt32(5)
                If reader.IsDBNull(6) Then op.DocStatusCode = "NONE" Else op.DocStatusCode = reader.GetString(6)
                resultlist.Add(op)
            End While

            reader.Close()

        Finally
            sqlConn.Close()

        End Try

        Return resultlist
    End Function


    Public Function GetSchoolLevelAdmissionRequirementsWithNoProgramVersion(ByVal LeadId As String, ByVal PrgVerId As String, ByVal strEnrollDate As Date, ByVal campusid As String) As DataSet
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim sbroot, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
        Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder


        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = myAdvAppSettings.AppSettings("ConString")


        Dim dsGetCmpGrps As DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusid)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        'This query gets a list of all Lead Groups
        With sbroot
            .Append(" select Distinct t1.LeadGrpId,Descrip from adLeadGroups t1,adLeadByLeadGroups t2 where t1.LeadGrpId=t2.LeadGrpId and t2.LeadId='" & LeadId & "'  ")
        End With

        dadNorthwind = New OleDbDataAdapter(sbroot.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "ParentRoot")

        Dim strCurrentDate As String = Date.Now.ToShortDateString

        'Get the summary of all school level requirements
        With sbMandatorySummary
            '*********************************************************************************************************************************************
            ' This Query gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted for Mandatory Requirement Group
            '*********************************************************************************************************************************************
            .Append(" select Top 1 ReqGrpId,R5.Descrip,R5.NumReqs,R5.TestPassed+R5.DocsApproved+R5.TestDocumentAttempted as AttemptedReqs ")
            .Append(" from ")
            .Append(" ( ")
            .Append(" select 'CFD29DFD-4FFF-400B-B152-4FABB12E6E7B' as ReqGrpId, ")
            .Append(" case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then (select Distinct Descrip from adReqGroups where IsMandatoryReqGrp=1) else 'School Level Requirements' end  as Descrip,  ")
            .Append(" CampGrpId, ")
            .Append(" (select Count(*) as NumReqs from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" from adReqs A2,adReqsEffectiveDates A3 ")
            .Append(" where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append("   A2.reqforEnrollment=1 and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" and A2.adreqTypeId in (1,3) and A2.StatusId='" & strActiveGUID & "') ")
            .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as NumReqs, ")
            ''''''''''''' This Query gives the number of test passed by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate  ")
            .Append(" from adReqs A2,adReqsEffectiveDates A3 ")
            .Append(" where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append("   A2.reqforEnrollment=1 and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" and A2.adreqTypeId=1 and A2.StatusId='" & strActiveGUID & "') R1,adLeadEntranceTest R2  where ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "'  and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and R2.Pass=1 and ")
            .Append(" R2.EntrTestId not in (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "') and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as TestPassed,")
            ''''''''''''' This Query gives the number of docs approved by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate from   adReqs A2,adReqsEffectiveDates A3 ")
            .Append(" where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append("   A2.reqforEnrollment=1 and ")

            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" and A2.adreqTypeId=3 and A2.StatusId='" & strActiveGUID & "') R1,adLeadDocsReceived R2,syDocStatuses R3  ")
            .Append(" where R1.adReqId = R2.DocumentId and R2.LeadId='" & LeadId & "'  and R2.DocumentId not in  (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "') ")
            .Append(" and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as DocsApproved,  ")
            ''''''''''''' This Query gives the number of test and documents approved by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" from   adReqs A2,adReqsEffectiveDates A3 where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            ''MOdified by Saraswathi Lakshmanan On Sept 21 2010
            ''For document tracking
            ''Check for the field ReqforEnrollment if it is set to 1.
            .Append(" and A2.adreqTypeId in (1,3) and A2.ReqforEnrollment=1  and A2.StatusId='" & strActiveGUID & "') ")
            .Append(" R1,adEntrTestOverRide R2 where R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "'  ")
            .Append(" and R1.CurrentDate >= R1.StartDate and R2.OverRide=1 and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" ) as TestDocumentAttempted,NULL as LeadGrpId  ")
            '.Append(" from adReqGroups t7 where IsMandatoryReqGrp = 1  ")
            .Append("   from adReqs t1,adReqsEffectiveDates t2 ")
            ''MOdified by Saraswathi Lakshmanan On Sept 21 2010
            ''For document tracking
            ''Check for the field ReqforEnrollment if it is set to 1.
            .Append("   where t1.adReqId = t2.adReqId and t2.MandatoryRequirement=1 and t1.adreqTypeId in (1,3,4,5,6) and t1.ReqforEnrollment=1   ")
            .Append(" ) R5 ")
            If Not strCampGrpId = "" Then
                .Append("  WHERE R5.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With
        'End of the summary of all school level requirements


        'Start query to get all school level requirements
        With sbMandatoryDetails
            .Append(" select distinct t1.adReqId,Descrip,t1.adReqTypeId, ")
            '.Append(" case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then (select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
            .Append(" 'CFD29DFD-4FFF-400B-B152-4FABB12E6E7B' as ReqGrpId, ")
            .Append(" (select Distinct Top 1 ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append(" (select Distinct Top 1 TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append(" (select Distinct Top 1 Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as Comments, ")
            .Append(" (select Distinct Top 1 OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as override, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,1 as Required, ")
            .Append(" (select Distinct Top 1 s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,'00000000-0000-0000-0000-000000000000' as LeadGrpId ")
            ''MOdified by Saraswathi Lakshmanan On Sept 21 2010
            ''For document tracking
            ''Check for the field ReqforEnrollment if it is set to 1.
            .Append(" from (select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3,4,5,6) and ReqforEnrollment=1  and StatusId='" & strActiveGUID & "') t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement=1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2 ")
            .Append(" where t1.adReqId = t2.adReqId ")
            If Not strCampGrpId = "" Then
                .Append("  and t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With
        'End query to get all school level requirements

        dadNorthwind.SelectCommand = New OleDbCommand(sbMandatorySummary.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "SchoolLevelRequirementSummary")

        dadNorthwind.SelectCommand = New OleDbCommand(sbMandatoryDetails.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "SchoolLevelRequirementDetails")



        '    'Set the relationship between school level requirements
        '    dstNorthwind.Relations.Add("SchoolLevelRelation", dstNorthwind.Tables("SchoolLevelRequirementSummary").Columns("ReqGrpId"), _
        '    dstNorthwind.Tables("SchoolLevelRequirementDetails").Columns("ReqGrpId"))



        With sbReqsAssignedDirectlyToLeadGroup
            .Append(" select distinct t1.adReqId,Descrip, ")
            .Append(" '00000000-0000-0000-0000-000000000000' as ReqGrpId,adReqTypeId, ")
            .Append(" (select Distinct ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append(" (select Distinct TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append(" (select Distinct Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as Comments, ")
            .Append(" (select Distinct OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as override, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and LeadId = '" & LeadId & "' ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,IsNull(t3.IsRequired,0) as Required, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
            .Append(" Case when t3.LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t3.LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            ''MOdified by Saraswathi Lakshmanan On Sept 21 2010
            ''For document tracking
            ''Check for the field ReqforEnrollment if it is set to 1.
            .Append(" (select Distinct adReqId,Descrip,CampGrpId,adReqTypeId from adReqs where adReqTypeId in (1,3,4,5,6) and ReqforEnrollment=1 and StatusId='" & strActiveGUID & "') t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append(" adReqLeadGroups t3 ")
            .Append(" where ")
            .Append(" t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            .Append(" t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') ")
            If Not strCampGrpId = "" Then
                .Append("  and t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With
        dadNorthwind.SelectCommand = New OleDbCommand(sbReqsAssignedDirectlyToLeadGroup.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "RequirementAssignedDirectlyToLeadGroup")


        Return dstNorthwind


    End Function
    Public Function GetSchoolLevelAdmissionRequirements1(ByVal LeadId As String, ByVal PrgVerId As String, ByVal strEnrollDate As Date) As DataSet
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter

        Dim sb, sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
        Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")



        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'This query gets a list of all Lead Groups
        'With sbroot
        '    .Append(" select Distinct t1.LeadGrpId,Descrip from adLeadGroups t1,adLeadByLeadGroups t2 where t1.LeadGrpId=t2.LeadGrpId and t2.LeadId='" & LeadId & "'  ")
        'End With

        'dadNorthwind = New OleDbDataAdapter(sbroot.ToString, conNorthwind)
        conNorthwind.Open()
        'dadNorthwind.Fill(dstNorthwind, "ParentRoot")


        'Get the summary of all school level requirements
        With sbMandatorySummary
            '*********************************************************************************************************************************************
            ' This Query gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted for Mandatory Requirement Group
            '*********************************************************************************************************************************************
            .Append(" select Top 1 ReqGrpId,R5.Descrip,R5.NumReqs,R5.TestPassed+R5.DocsApproved+R5.TestDocumentAttempted as AttemptedReqs ")
            .Append(" from ")
            .Append(" ( ")
            .Append(" select t7.ReqGrpId,t7.Descrip, ")
            .Append(" (select Count(*) as NumReqs from ")
            .Append(" (select Distinct A2.adReqId,'12/10/2006' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" from adReqs A2,adReqsEffectiveDates A3 ")
            ''MOdified by Saraswathi Lakshmanan On Sept 21 2010
            ''For document tracking
            ''Check for the field ReqforEnrollment if it is set to 1.
            .Append(" where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and A2.adreqTypeId in (1,3) and A2.ReqforEnrollment=1) ")
            .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as NumReqs, ")
            ''''''''''''' This Query gives the number of test passed by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'12/10/2006' as CurrentDate,A3.StartDate,A3.EndDate  ")
            .Append(" from adReqs A2,adReqsEffectiveDates A3 ")
            .Append(" where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and A2.adreqTypeId=1 and A2.reqforEnrollment) R1,adLeadEntranceTest R2  where ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "'  and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and R2.Pass=1 and ")
            .Append(" R2.EntrTestId not in (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "') and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as TestPassed,")
            ''''''''''''' This Query gives the number of docs approved by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'12/10/2006' as CurrentDate,A3.StartDate,A3.EndDate from   adReqs A2,adReqsEffectiveDates A3 ")
            ''MOdified by Saraswathi Lakshmanan On Sept 21 2010
            ''For document tracking
            ''Check for the field ReqforEnrollment if it is set to 1.
            .Append(" where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and A2.adreqTypeId=3 and a2.ReqforEnrollment =1 ) R1,adLeadDocsReceived R2,syDocStatuses R3  ")
            .Append(" where R1.adReqId = R2.DocumentId and R2.LeadId='" & LeadId & "'  and R2.DocumentId not in  (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "') ")
            .Append(" and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as DocsApproved,  ")
            ''''''''''''' This Query gives the number of test and documents approved by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'12/10/2006' as CurrentDate,A3.StartDate,A3.EndDate ")
            ''MOdified by Saraswathi Lakshmanan On Sept 21 2010
            ''For document tracking
            ''Check for the field ReqforEnrollment if it is set to 1.
            .Append(" from   adReqs A2,adReqsEffectiveDates A3 where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and A2.ReqforEnrollment=1  and A2.adreqTypeId in (1,3)) ")
            .Append(" R1,adEntrTestOverRide R2 where R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "'  ")
            .Append(" and R1.CurrentDate >= R1.StartDate and R2.OverRide=1 and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" ) as TestDocumentAttempted,NULL as LeadGrpId from adReqGroups t7 ")
            .Append(" where IsMandatoryReqGrp = 1  ")
            .Append(" ) R5 ")
        End With
        'End of the summary of all school level requirements


        'Start query to get all school level requirements
        With sbMandatoryDetails
            .Append(" select distinct t1.adReqId,Descrip, ")
            .Append(" case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then (select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
            .Append(" (select Distinct ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append(" (select Distinct TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append(" (select Distinct Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as Comments, ")
            .Append(" (select Distinct OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as override, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and LeadId = '" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,1 as Required, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,'00000000-0000-0000-0000-000000000000' as LeadGrpId ")
            ''MOdified by Saraswathi Lakshmanan On Sept 21 2010
            ''For document tracking
            ''Check for the field ReqforEnrollment if it is set to 1.
            .Append(" from (select Distinct adReqId,Descrip from adReqs where adReqTypeId in (1,3) and ReqforEnrollment=1 ) t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement=1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2 ")
            .Append(" where t1.adReqId = t2.adReqId ")
        End With
        'End query to get all school level requirements

        dadNorthwind.SelectCommand = New OleDbCommand(sbMandatorySummary.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "SchoolLevelRequirementSummary")

        dadNorthwind.SelectCommand = New OleDbCommand(sbMandatoryDetails.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "SchoolLevelRequirementDetails")

        Return dstNorthwind

        '    'Set the relationship between school level requirements
        '    dstNorthwind.Relations.Add("SchoolLevelRelation", dstNorthwind.Tables("SchoolLevelRequirementSummary").Columns("ReqGrpId"), _
        '    dstNorthwind.Tables("SchoolLevelRequirementDetails").Columns("ReqGrpId"))

        '    'Query gets the requirement group summary assigned to LeadGroup and Program Version
        '    With sb
        '        .Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,R5.TestPassed+R5.DocsApproved+R5.TestDocumentAttempted as AttemptedReqs,  ")
        '        .Append(" Case when LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else LeadGrpId end as LeadGrpId ")
        '        .Append(" from ")
        '        '*********************************************************************************************************************************************
        '        ' This Query gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted by Lead and Program Version 
        '        '*********************************************************************************************************************************************
        '        .Append(" (Select  Distinct t1.ReqGrpId,t2.Descrip,t3.NumReqs as Numreqs,  ")
        '        ''''''''''''' This Query gives the number of test passed by the lead and grouped by lead group ''''''''
        '        .Append(" (select Count(*) from ")
        '        .Append(" (select Distinct A2.adReqId,'12/10/2006' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
        '        .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
        '        .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and  ")
        '        .Append(" A4.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=1) ")
        '        .Append(" R1,adLeadEntranceTest R2 WHERE ")
        '        .Append(" R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "' and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and R2.Pass=1 and ")
        '        .Append(" R2.EntrTestId not in (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "')  and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) Group by LeadGrpId)as TestPassed, ")
        '        ''''''''''''' This Query gives the number of documents approved by the lead and grouped by lead group ''''''''
        '        .Append(" (select Count(*) from  ")
        '        .Append(" (select Distinct A2.adReqId,'12/10/2006' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
        '        .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
        '        .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
        '        .Append(" A4.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
        '        .Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=3) R1,adLeadDocsReceived R2,syDocStatuses R3 WHERE ")
        '        .Append(" R1.adReqId = R2.DocumentId and R2.LeadId='" & LeadId & "'  and R2.DocumentId not in (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "') ")
        '        .Append(" and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) Group by LeadGrpId) as DocsApproved, ")
        '        '''''''''''' This Query gives the number of test and documents attempted(no matter pass or fail,no matter approved/not approved) grouped by lead group ''''''''
        '        .Append(" (select Count(*)  from ")
        '        .Append(" (select Distinct A2.adReqId,'12/10/2006' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
        '        .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
        '        .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
        '        .Append(" A4.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
        '        .Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (1,3)) R1,adEntrTestOverRide R2 ")
        '        .Append(" where R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "'  and R1.CurrentDate >= R1.StartDate and R2.OverRide=1 and ")
        '        .Append(" (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) Group BY LeadGrpId) as TestDocumentAttempted,t4.LeadGrpId ")
        '        .Append(" from adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3,adLeadGroups t4  where ")
        '        .Append(" t1.ReqGrpId = t2.ReqGrpId and t2.ReqGrpId = t3.ReqGrpId and t3.LeadGrpId = t4.LeadGrpId and ")
        '        .Append(" t1.PrgVerId='" & PrgVerId & "'  and t2.IsMandatoryReqGrp <> 1 and ")
        '        .Append(" t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') ")
        '        .Append(" ) R5 ")
        '        '**************************************************************************************************************************************************************
        '        'End of the  Query that gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted by Lead and Program Version
        '        '**************************************************************************************************************************************************************
        '    End With

        '    'Query gets the requirements assigned to LeadGroup and Program Version
        '    With sb1
        '        .Append(" select distinct t1.adReqId,Descrip, ")
        '        .Append(" t6.ReqGrpId as ReqGrpId, ")
        '        .Append(" (select Distinct ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as ActualScore, ")
        '        .Append(" (select Distinct TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as TestTaken, ")
        '        .Append(" (select Distinct Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as Comments, ")
        '        .Append(" (select Distinct OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as override, ")
        '        .Append(" (select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and LeadId = '" & LeadId & "' ) as DocSubmittedCount, ")
        '        .Append(" t2.Minscore,t3.IsRequired as Required, ")
        '        .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
        '        .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
        '        .Append(" s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
        '        .Append(" Case when t3.LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t3.LeadGrpId end as LeadGrpId ")
        '        .Append(" from ")
        '        .Append(" (select Distinct adReqId,Descrip from adReqs where adReqTypeId in (1,3)) t1, ")
        '        .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
        '        .Append(" (select Distinct LeadGrpId,adReqEffectiveDateId,IsRequired from adReqLeadGroups where LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' )) t3, ")
        '        .Append(" (select Distinct ReqGrpId from adPrgVerTestDetails  where PrgVerId = '" & PrgVerId & "'  and ReqGrpId is not null) t5, ")
        '        .Append(" (select Distinct adReqId,ReqGrpId,LeadGrpId from adReqGrpDef) t6, ")
        '        .Append(" (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') t7  ")
        '        .Append(" where t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
        '        .Append(" t5.ReqGrpId = t6.ReqGrpId And t3.LeadGrpId = t7.LeadGrpId And t6.LeadGrpId = t7.LeadGrpId And t1.adReqId = t6.adReqId ")
        '    End With

        '    dadNorthwind.SelectCommand = New OleDbCommand(sb.ToString, conNorthwind)
        '    dadNorthwind.Fill(dstNorthwind, "Parent")

        '    dadNorthwind.SelectCommand = New OleDbCommand(sb1.ToString, conNorthwind)
        '    dadNorthwind.Fill(dstNorthwind, "Child")

        '    dstNorthwind.Relations.Add("myRootRelation", dstNorthwind.Tables("ParentRoot").Columns("LeadGrpId"), _
        '                                                 dstNorthwind.Tables("Parent").Columns("LeadGrpId"))

        '    Dim colsParent, colsChild As DataColumn()
        '    colsParent = New DataColumn() {dstNorthwind.Tables("Parent").Columns("ReqGrpId"), dstNorthwind.Tables("Parent").Columns("LeadGrpId")}
        '    colsChild = New DataColumn() {dstNorthwind.Tables("Child").Columns("ReqGrpId"), dstNorthwind.Tables("Child").Columns("LeadGrpId")}
        '    Dim rel As DataRelation
        '    rel = New DataRelation("myrelation", colsParent, colsChild)
        '    dstNorthwind.Relations.Add(rel)



        With sbReqsAssignedDirectlyToLeadGroup
            .Append(" select distinct t1.adReqId,Descrip, ")
            .Append(" '00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            .Append(" (select Distinct ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append(" (select Distinct TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append(" (select Distinct Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as Comments, ")
            .Append(" (select Distinct OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as override, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and LeadId = '" & LeadId & "' ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,ISNUll(t3.IsRequired,0) as Required, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
            .Append(" Case when t3.LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t3.LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            ''Modified by Saraswathi Lakshmanan on Sept 21 2010
            ''For document tracking
            .Append(" (select Distinct adReqId,Descrip from adReqs where adReqTypeId in (1,3) and ReqforEnrollment=1 ) t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append(" adReqLeadGroups t3 ")
            .Append(" where ")
            .Append(" t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            ' Requirement should not be in requirement group assigned to program version
            .Append(" t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId='" & PrgVerId & "' and s2.adReqId is null)   ")
            .Append(" and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and ")
            ' Requirement should not be directly assigned to program version
            .Append(" t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId='" & PrgVerId & "') ")
        End With

        '    dadNorthwind.SelectCommand = New OleDbCommand(sbReqAssignedDirectlyToPrgVersion.ToString, conNorthwind)
        '    dadNorthwind.Fill(dstNorthwind, "RequirementAssignedDirectlyToPrgVersion")

        '    'Set Relationship between LeadGroups and RequirementAssignedDirectlyToPrgVersion
        '    dstNorthwind.Relations.Add("RequirementAssignedToPrgVersionRelation", dstNorthwind.Tables("ParentRoot").Columns("LeadGrpId"), _
        '                                dstNorthwind.Tables("RequirementAssignedDirectlyToPrgVersion").Columns("LeadGrpId"))

        '    dadNorthwind.SelectCommand = New OleDbCommand(sbReqsAssignedDirectlyToLeadGroup.ToString, conNorthwind)
        '    dadNorthwind.Fill(dstNorthwind, "RequirementAssignedDirectlyToLeadGroup")

        '    'Set Relationship between LeadGroups and RequirementAssignedDirectlyToPrgVersion
        '    dstNorthwind.Relations.Add("RequirementAssignedDirectlyToLeadGroupRelation", dstNorthwind.Tables("ParentRoot").Columns("LeadGrpId"), _
        '                                dstNorthwind.Tables("RequirementAssignedDirectlyToLeadGroup").Columns("LeadGrpId"))
        '    conNorthwind.Close()

        '    'Display School Level Requirements
        '    s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"" BorderWidth=""1px"" BorderStyle=""Solid"" BorderColor=""#A3C7E2"">"
        '    intRowCounter = 0
        '    For Each drowSchoolLevelSummary In dstNorthwind.Tables("SchoolLevelRequirementSummary").Rows
        '        If Not drowSchoolLevelSummary("Descrip") Is System.DBNull.Value Then
        '            s3 &= "<TR>"
        '            s3 &= "<td nowrap><font face=""verdana"" size=""2""><strong>" & drowSchoolLevelSummary("Descrip") & "</strong>&nbsp;</font>"
        '            If Not drowSchoolLevelSummary("NumReqs") Is System.DBNull.Value Then
        '                If drowSchoolLevelSummary("NumReqs") >= 1 Then
        '                    s3 &= "<span style=""font: normal 12px arial; padding: 16px"">[Min Req:" & drowSchoolLevelSummary("NumReqs") & "]"
        '                    s3 &= "[Min Attempted:" & drowSchoolLevelSummary("AttemptedReqs") & "]</span>"
        '                End If
        '            End If
        '            s3 &= "</td></tr>"
        '        Else
        '            s3 &= "<TR><td><font face=""verdana"" size=""2""><strong>Miscellaneous</strong></font></td></tr>"
        '        End If
        '        s3 &= "<tr>"
        '        intRowCounter = 0
        '        For Each drowSchoolLevelDetails In drowSchoolLevelSummary.GetChildRows("SchoolLevelRelation")
        '            If Not drowSchoolLevelDetails("Descrip") Is System.DBNull.Value Then
        '                If Not drowSchoolLevelDetails("TestTaken") Is System.DBNull.Value Then
        '                    s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
        '                ElseIf Not drowSchoolLevelDetails("TestTaken") Is System.DBNull.Value And drowSchoolLevelDetails("DocSubmittedCount") >= 1 Then
        '                    s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
        '                Else
        '                    s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled></li>"
        '                End If
        '                s3 &= "<span style=""font:  normal 12px arial; padding: 16px"">" & drowSchoolLevelDetails("Descrip")
        '                If drowSchoolLevelDetails("Required") = 1 Then
        '                    s3 &= "<span style=""color:red; padding-left: 3px;font: normal 12px arial;""><strong>(Required)</strong></span>"
        '                End If

        '                'Check If the Lead Has Passed The Test
        '                If Not drowSchoolLevelDetails("ActualScore") Is System.DBNull.Value Then
        '                    If drowSchoolLevelDetails("MinScore") Is System.DBNull.Value Then
        '                        s3 &= ""
        '                    Else
        '                        If drowSchoolLevelDetails("ActualScore") >= 1 Then
        '                            If (drowSchoolLevelDetails("ActualScore") < drowSchoolLevelDetails("MinScore")) And (Not drowSchoolLevelDetails("TestTaken") = "") Then
        '                                s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Fail)</strong></span>"
        '                            ElseIf (drowSchoolLevelDetails("ActualScore") >= drowSchoolLevelDetails("MinScore")) And (Not drowSchoolLevelDetails("TestTaken") = "") Then
        '                                s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Pass)</strong></span>"
        '                            End If
        '                        End If
        '                    End If
        '                End If

        '                'Check If Document was Approved
        '                If Not drowSchoolLevelDetails("DocStatusDescrip") Is System.DBNull.Value Then
        '                    If drowSchoolLevelDetails("DocStatusDescrip") = "Approved" Then
        '                        s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Approved)</strong></span>"
        '                    Else
        '                        s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Not Approved)</strong></span>"
        '                    End If
        '                End If


        '                If Not drowSchoolLevelDetails("OverRide") Is System.DBNull.Value Then
        '                    If drowSchoolLevelDetails("OverRide") = "True" Then
        '                        s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Overriden)</strong></span>"
        '                    End If
        '                End If
        '            End If
        '            s3 &= "</span></ul>"
        '            s3 &= "</td>"
        '            intRowCounter += 1
        '            If intRowCounter = 3 Then
        '                s3 &= "</tr><tr>"
        '                intRowCounter = 0
        '            End If
        '        Next
        '        s3 &= "</tr><tr height=10><td>&nbsp;</td></tr>"
        '        forCounter = 1
        '    Next
        '    s3 &= "</table>"
        '    s3 &= "<p></p>"
        '    s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
        '    For Each drowRootParent In dstNorthwind.Tables("ParentRoot").Rows
        '        If Not drowRootParent("Descrip") Is System.DBNull.Value Then
        '            s3 &= "<tr bgcolor=""navy""><td nowrap width=""100%""><font face=""verdana"" size=""2"" color=""white""><strong>Requirements for " & drowRootParent("Descrip") & " Applicants</strong>&nbsp;</font></td></tr>"
        '        End If
        '        s3 &= "</table>"
        '        s3 &= "<p></p>"
        '        ' Display Requirements assigned directly to Program Version
        '        s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
        '        s3 &= "<tr><td nowrap><font face=""verdana"" size=""2""><strong>Miscellaneous Requirements</strong></font></td></tr>"
        '        s3 &= "</table>"
        '        s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
        '        s3 &= "<tr>"
        '        intRowCounter = 0
        '        For Each drowRequirementAssignedDirectlyToPrgVersion In drowRootParent.GetChildRows("RequirementAssignedToPrgVersionRelation")
        '            If Not drowRequirementAssignedDirectlyToPrgVersion("Descrip") Is System.DBNull.Value Then
        '                If Not drowRequirementAssignedDirectlyToPrgVersion("TestTaken") Is System.DBNull.Value Then
        '                    s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
        '                ElseIf Not drowRequirementAssignedDirectlyToPrgVersion("TestTaken") Is System.DBNull.Value And drowRequirementAssignedDirectlyToPrgVersion("DocSubmittedCount") >= 1 Then
        '                    s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
        '                Else
        '                    s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled></li>"
        '                End If
        '                s3 &= "<span style=""font:  normal 12px arial; padding: 16px"">" & drowRequirementAssignedDirectlyToPrgVersion("Descrip")
        '                If drowRequirementAssignedDirectlyToPrgVersion("Required") = 1 Then
        '                    s3 &= "<span style=""color:red; padding-left: 3px;font: normal 12px arial;""><strong>(Required)</strong></span>"
        '                End If

        '                'Check If the Lead Has Passed The Test
        '                If Not drowRequirementAssignedDirectlyToPrgVersion("ActualScore") Is System.DBNull.Value Then
        '                    If drowRequirementAssignedDirectlyToPrgVersion("MinScore") Is System.DBNull.Value Then
        '                        s3 &= ""
        '                    Else
        '                        If drowRequirementAssignedDirectlyToPrgVersion("ActualScore") >= 1 Then
        '                            If (drowRequirementAssignedDirectlyToPrgVersion("ActualScore") < drowRequirementAssignedDirectlyToPrgVersion("MinScore")) And (Not drowRequirementAssignedDirectlyToPrgVersion("TestTaken") = "") Then
        '                                s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Fail)</strong></span>"
        '                            ElseIf (drowRequirementAssignedDirectlyToPrgVersion("ActualScore") >= drowRequirementAssignedDirectlyToPrgVersion("MinScore")) And (Not drowRequirementAssignedDirectlyToPrgVersion("TestTaken") = "") Then
        '                                s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Pass)</strong></span>"
        '                            End If
        '                        End If
        '                    End If
        '                End If

        '                'Check If Document was Approved
        '                If Not drowRequirementAssignedDirectlyToPrgVersion("DocStatusDescrip") Is System.DBNull.Value Then
        '                    If drowRequirementAssignedDirectlyToPrgVersion("DocStatusDescrip") = "Approved" Then
        '                        s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Approved)</strong></span>"
        '                    Else
        '                        s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Not Approved)</strong></span>"
        '                    End If
        '                End If


        '                If Not drowRequirementAssignedDirectlyToPrgVersion("OverRide") Is System.DBNull.Value Then
        '                    If drowRequirementAssignedDirectlyToPrgVersion("OverRide") = "True" Then
        '                        s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Overriden)</strong></span>"
        '                    End If
        '                End If
        '            End If
        '            s3 &= "</span></ul>"
        '            s3 &= "</td>"
        '            intRowCounter += 1
        '            If intRowCounter = 3 Then
        '                s3 &= "</tr><tr>"
        '                intRowCounter = 0
        '            End If
        '        Next
        '        s3 &= "</tr><tr height=10><td>&nbsp;</td></tr>"
        '        's3 &= "</table>"
        '        'End of Requirements assigned Directly to Program Version
        '        'Start Requirements assigned Directly to LeadGroup
        '        's3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
        '        's3 &= "<tr><td nowrap><font face=""verdana"" size=""2""><strong>Requirements for " & drowRootParent("Descrip") & " Applicants </strong>&nbsp;</font></td></tr>"
        '        's3 &= "</table>"
        '        ' Display Requirements assigned directly to Program Version
        '        's3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
        '        s3 &= "<tr>"
        '        intRowCounter = 0
        '        For Each drowRequirementAssignedDirectlyToLeadGroup In drowRootParent.GetChildRows("RequirementAssignedDirectlyToLeadGroupRelation")
        '            If Not drowRequirementAssignedDirectlyToLeadGroup("Descrip") Is System.DBNull.Value Then
        '                If Not drowRequirementAssignedDirectlyToLeadGroup("TestTaken") Is System.DBNull.Value Then
        '                    s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
        '                ElseIf Not drowRequirementAssignedDirectlyToLeadGroup("TestTaken") Is System.DBNull.Value And drowRequirementAssignedDirectlyToLeadGroup("DocSubmittedCount") >= 1 Then
        '                    s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
        '                Else
        '                    s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled></li>"
        '                End If
        '                s3 &= "<span style=""font:  normal 12px arial; padding: 16px"">" & drowRequirementAssignedDirectlyToLeadGroup("Descrip")
        '                If drowRequirementAssignedDirectlyToLeadGroup("Required") = 1 Then
        '                    s3 &= "<span style=""color:red; padding-left: 3px;font: normal 12px arial;""><strong>(Required)</strong></span>"
        '                End If

        '                'Check If the Lead Has Passed The Test
        '                If Not drowRequirementAssignedDirectlyToLeadGroup("ActualScore") Is System.DBNull.Value Then
        '                    If drowRequirementAssignedDirectlyToLeadGroup("MinScore") Is System.DBNull.Value Then
        '                        s3 &= ""
        '                    Else
        '                        If drowRequirementAssignedDirectlyToLeadGroup("ActualScore") >= 1 Then
        '                            If (drowRequirementAssignedDirectlyToLeadGroup("ActualScore") < drowRequirementAssignedDirectlyToLeadGroup("MinScore")) And (Not drowRequirementAssignedDirectlyToLeadGroup("TestTaken") = "") Then
        '                                s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Fail)</strong></span>"
        '                            ElseIf (drowRequirementAssignedDirectlyToLeadGroup("ActualScore") >= drowRequirementAssignedDirectlyToLeadGroup("MinScore")) And (Not drowRequirementAssignedDirectlyToLeadGroup("TestTaken") = "") Then
        '                                s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Pass)</strong></span>"
        '                            End If
        '                        End If
        '                    End If
        '                End If

        '                'Check If Document was Approved
        '                If Not drowRequirementAssignedDirectlyToLeadGroup("DocStatusDescrip") Is System.DBNull.Value Then
        '                    If drowRequirementAssignedDirectlyToLeadGroup("DocStatusDescrip") = "Approved" Then
        '                        s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Approved)</strong></span>"
        '                    Else
        '                        s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Not Approved)</strong></span>"
        '                    End If
        '                End If


        '                If Not drowRequirementAssignedDirectlyToLeadGroup("OverRide") Is System.DBNull.Value Then
        '                    If drowRequirementAssignedDirectlyToLeadGroup("OverRide") = "True" Then
        '                        s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Overriden)</strong></span>"
        '                    End If
        '                End If
        '            End If
        '            s3 &= "</span></ul>"
        '            s3 &= "</td>"
        '            intRowCounter += 1
        '            If intRowCounter = 3 Then
        '                s3 &= "</tr><tr>"
        '                intRowCounter = 0
        '            End If
        '        Next
        '        s3 &= "</tr><tr height=10><td>&nbsp;</td></tr>"
        '        s3 &= "</table>"
        '        'End of Requirement assigned directly to Lead Group
        '        s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"" BorderWidth=""1px"" BorderStyle=""Solid"" BorderColor=""#A3C7E2"">"
        '        For Each drowParent In dstNorthwind.Tables("Parent").Rows
        '            If Not drowParent("Descrip") Is System.DBNull.Value Then
        '                s3 &= "<TR>"
        '                s3 &= "<td nowrap><font face=""verdana"" size=""2""><strong>" & drowParent("Descrip") & "</strong>&nbsp;</font>"
        '                If Not drowParent("NumReqs") Is System.DBNull.Value Then
        '                    If drowParent("NumReqs") >= 1 Then
        '                        s3 &= "<span style=""font: normal 12px arial; padding: 16px"">[Min Req:" & drowParent("NumReqs") & "]"
        '                        s3 &= "[Min Attempted:" & drowParent("AttemptedReqs") & "]</span>"
        '                    End If
        '                End If
        '                s3 &= "</td></tr>"
        '            Else
        '                s3 &= "<TR><td><font face=""verdana"" size=""2""><strong>Miscellaneous</strong></font></td></tr>"
        '            End If
        '            s3 &= "</table>"
        '            s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
        '            s3 &= "<tr>"
        '            intRowCounter = 0
        '            For Each drowChild In drowParent.GetChildRows("myrelation")
        '                If Not drowChild("Descrip") Is System.DBNull.Value Then
        '                    If Not drowChild("TestTaken") Is System.DBNull.Value Then
        '                        s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
        '                    ElseIf Not drowChild("TestTaken") Is System.DBNull.Value And drowChild("DocSubmittedCount") >= 1 Then
        '                        s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
        '                    Else
        '                        s3 &= "<td style=""font-size: 12px"" nowrap><ul><li><input type=""checkbox"" id=""chkTaken"" disabled></li>"
        '                    End If
        '                    s3 &= "<span style=""font:  normal 12px arial; padding: 16px"">" & drowChild("Descrip")
        '                    If drowChild("Required") = 1 Then
        '                        s3 &= "<span style=""color:red; padding-left: 3px;font: normal 12px arial;""><strong>(Required)</strong></span>"
        '                    End If

        '                    'Check If the Lead Has Passed The Test
        '                    If Not drowChild("ActualScore") Is System.DBNull.Value Then
        '                        If drowChild("MinScore") Is System.DBNull.Value Then
        '                            s3 &= ""
        '                        Else
        '                            If drowChild("ActualScore") >= 1 Then
        '                                If (drowChild("ActualScore") < drowChild("MinScore")) And (Not drowChild("TestTaken") = "") Then
        '                                    s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Fail)</strong></span>"
        '                                ElseIf (drowChild("ActualScore") >= drowChild("MinScore")) And (Not drowChild("TestTaken") = "") Then
        '                                    s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Pass)</strong></span>"
        '                                End If
        '                            End If
        '                        End If
        '                    End If

        '                    'Check If Document was Approved
        '                    If Not drowChild("DocStatusDescrip") Is System.DBNull.Value Then
        '                        If drowChild("DocStatusDescrip") = "Approved" Then
        '                            s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Approved)</strong></span>"
        '                        Else
        '                            s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Not Approved)</strong></span>"
        '                        End If
        '                    End If


        '                    If Not drowChild("OverRide") Is System.DBNull.Value Then
        '                        If drowChild("OverRide") = "True" Then
        '                            s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Overriden)</strong></span>"
        '                        End If
        '                    End If


        '                End If
        '                s3 &= "</span></ul>"
        '                s3 &= "</td>"
        '                intRowCounter += 1
        '                If intRowCounter = 3 Then
        '                    s3 &= "</tr><tr>"
        '                    intRowCounter = 0
        '                End If
        '            Next
        '            s3 &= "</tr><tr height=10><td>&nbsp;</td></tr>"
        '            forCounter = 1
        '        Next
        '    Next
        '    s3 &= "</table>"

        '    If forCounter = 1 Then
        '        Return s3.ToString()
        '    Else
        '        Return "None"
        '    End If
    End Function
    Public Function GetRequirementsByLeadGroup(ByVal LeadId As String, ByVal PrgVerId As String, ByVal EnrollDate As Date, ByVal LeadGrpId As String, ByVal CampusId As String) As DataSet
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim drowRootParent As DataRow
        Dim drowSchoolLevelSummary As DataRow
        Dim drowSchoolLevelDetails As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb, sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
        Dim drowRequirementAssignedDirectlyToPrgVersion, drowRequirementAssignedDirectlyToLeadGroup As DataRow
        Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder

        Dim dsGetEntranceTestId As New DataSet
        Dim dsGetLeadGrpId As New DataSet
        Dim dstGetRequirementsAssignedToProgramVersion As New DataSet
        Dim dstGetRequirementsDirectlyAssigned As New DataSet
        Dim intRowCounter As Integer
        Dim strCurrentDate As String = Date.Now.ToShortDateString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        'Query gets the requirement group summary assigned to LeadGroup and Program Version
        With sb
            'TestDocumentAttempted removed from the count query by Balaji on 4/23/2007
            '.Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,IsNull(R5.TestPassed,0)+IsNULL(R5.DocsApproved,0)+IsNULL(R5.TestDocumentAttempted,0)+IsNULL(R5.TestOverriden,0)+IsNULL(R5.DocumentOverriden,0) as AttemptedReqs,  ")
            .Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,IsNull(R5.TestPassed,0)+IsNULL(R5.DocsApproved,0)+IsNULL(R5.TestOverriden,0)+IsNULL(R5.DocumentOverriden,0) as AttemptedReqs,  ")
            .Append(" Case when LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            '*********************************************************************************************************************************************
            ' This Query gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted by Lead and Program Version 
            '*********************************************************************************************************************************************
            .Append(" (Select  Distinct t1.ReqGrpId,t2.Descrip,t2.CampGrpId,t3.NumReqs as Numreqs,  ")
            ''''''''''''' This Query gives the number of test passed by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            ''Modified by Saraswathi Lakhsmanan on Sept 21 2010 For Document tracking
            .Append(" where A1.adReqId = A2.adReqId and A2.ReqforEnrollment=1  and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and  ")
            .Append(" A4.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=1) ")
            .Append(" R1,adLeadEntranceTest R2 WHERE ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "' and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and R2.Pass=1 and ")
            .Append(" R2.EntrTestId not in (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "')  and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) and LeadGrpId='" & LeadGrpId & "') as TestPassed, ")
            '''''''''''' This Query gives the number of overriden test
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            ''Modified by Saraswathi Lakhsmanan on Sept 21 2010 For Document tracking
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where A1.adReqId = A2.adReqId and A2.ReqforEnrollment=1 and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and  ")
            .Append(" A4.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=1) ")
            .Append(" R1,adEntrTestOverride R2 WHERE ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "' and R2.Override=1 and R1.CurrentDate >= R1.StartDate   ")
            .Append(" and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) and LeadGrpId='" & LeadGrpId & "') as TestOverriden, ")
            '''''''''''' This Query gives the number of overriden documents
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            ''Modified BY Saraswathi Lakshmanan on Sept 21 2010
            .Append(" where A1.adReqId = A2.adReqId and A2.ReqforEnrollment=1  and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and  ")
            .Append(" A4.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=3) ")
            .Append(" R1,adEntrTestOverride R2 WHERE ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "' and R2.Override=1 and R1.CurrentDate >= R1.StartDate   ")
            .Append(" and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) and LeadGrpId='" & LeadGrpId & "') as DocumentOverriden, ")
            ''''''''''''' This Query gives the number of documents approved by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from  ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where A1.adReqId = A2.adReqId and A2.ReqforEnrollment=1 and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append(" A4.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            .Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=3) R1,adLeadDocsReceived R2,syDocStatuses R3 WHERE ")
            .Append(" R1.adReqId = R2.DocumentId and R2.LeadId='" & LeadId & "'  and R2.DocumentId not in (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and LeadId='" & LeadId & "') ")
            .Append(" and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) and LeadGrpId='" & LeadGrpId & "') as DocsApproved, ")
            '''''''''''' This Query gives the number of test and documents attempted(no matter pass or fail,no matter approved/not approved) grouped by lead group ''''''''
            .Append(" (select Count(*)  from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            ''MOdified by Saraswathi lakshmanan on Sepot 21 2010
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where A1.adReqId = A2.adReqId and A2.ReqforEnrollment=1  and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append(" A4.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            .Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (1,3)) R1,adEntrTestOverRide R2 ")
            .Append(" where R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "'  and R1.CurrentDate >= R1.StartDate and R2.OverRide=1 and ")
            .Append(" (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) Group BY LeadGrpId) as TestDocumentAttempted,t4.LeadGrpId ")
            .Append(" from adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3,adLeadGroups t4  where ")
            .Append(" t1.ReqGrpId = t2.ReqGrpId and t2.ReqGrpId = t3.ReqGrpId and t3.LeadGrpId = t4.LeadGrpId and ")
            .Append(" t1.PrgVerId='" & PrgVerId & "'  and t2.IsMandatoryReqGrp <> 1  ")
            .Append(" and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') ")
            .Append(" ) R5 where LeadGrpId='" & LeadGrpId & "' ")
            If Not strCampGrpId = "" Then
                .Append("  and R5.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            '**************************************************************************************************************************************************************
            'End of the  Query that gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted by Lead and Program Version
            '**************************************************************************************************************************************************************
        End With
        dadNorthwind = New OleDbDataAdapter(sb.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "RequirementGroup")
        Return dstNorthwind
    End Function
    Public Function GetRequirementsByLeadGroupAndReqGroup(ByVal LeadId As String, ByVal PrgVerId As String, ByVal EnrollDate As Date, ByVal LeadGrpId As String, ByVal ReqGrpId As String, ByVal CampusId As String) As DataSet
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim drowRootParent As DataRow
        Dim drowSchoolLevelSummary As DataRow
        Dim drowSchoolLevelDetails As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb, sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
        Dim drowRequirementAssignedDirectlyToPrgVersion, drowRequirementAssignedDirectlyToLeadGroup As DataRow
        Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder

        Dim dsGetEntranceTestId As New DataSet
        Dim dsGetLeadGrpId As New DataSet
        Dim dstGetRequirementsAssignedToProgramVersion As New DataSet
        Dim dstGetRequirementsDirectlyAssigned As New DataSet
        Dim intRowCounter As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim dsGetCmpGrps As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If


        'Query gets the requirements assigned to LeadGroup and Program Version
        With sb1
            .Append(" select distinct t1.adReqId,Descrip,adReqTypeId, ")
            .Append(" t6.ReqGrpId as ReqGrpId, ")
            .Append(" (select Distinct ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append(" (select Distinct TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append(" (select Distinct Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as Comments, ")
            .Append(" (select Distinct OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as override, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and LeadId = '" & LeadId & "' ) as DocSubmittedCount, ")
            .Append(" t2.Minscore, ")
            .Append(" Case when t3.IsRequired=1 then t3.IsRequired else IsNULL(t6.IsRequired,0) end as Required, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
            .Append(" Case when t3.LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t3.LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            ''Modified by Saraswathi Lakshmanan on Sept 21 2010
            ''For Document Tracking
            .Append(" (select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and ReqforEnrollment=1  and StatusId='" & strActiveGUID & "') t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append(" (select Distinct LeadGrpId,adReqEffectiveDateId,ISNULL(IsRequired,0) AS isRequired from adReqLeadGroups where LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' )) t3, ")
            .Append(" (select Distinct ReqGrpId from adPrgVerTestDetails  where PrgVerId = '" & PrgVerId & "' and ReqGrpId='" & ReqGrpId & "' and ReqGrpId is not null) t5, ")
            .Append(" (select Distinct adReqId,ReqGrpId,LeadGrpId,ISNULL(IsRequired,0) AS isRequired from adReqGrpDef where ReqGrpId='" & ReqGrpId & "') t6, ")
            .Append(" (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' and LeadGrpId='" & LeadGrpId & "') t7  ")
            .Append(" where t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            .Append(" t5.ReqGrpId = t6.ReqGrpId And t3.LeadGrpId = t7.LeadGrpId And t6.LeadGrpId = t7.LeadGrpId And t1.adReqId = t6.adReqId ")
            If Not strCampGrpId = "" Then
                .Append("  AND t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With
        dadNorthwind = New OleDbDataAdapter(sb1.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "Requirements")
        Return dstNorthwind
    End Function
    Public Function GetRequirementsAssignedToLeadReqGroup(ByVal LeadId As String, ByVal PrgVerId As String, ByVal EnrollDate As Date, ByVal LeadGrpId As String, ByVal CampusId As String) As DataSet
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim drowRootParent As DataRow
        Dim drowSchoolLevelSummary As DataRow
        Dim drowSchoolLevelDetails As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb, sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
        Dim drowRequirementAssignedDirectlyToPrgVersion, drowRequirementAssignedDirectlyToLeadGroup As DataRow
        Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder

        Dim dsGetEntranceTestId As New DataSet
        Dim dsGetLeadGrpId As New DataSet
        Dim dstGetRequirementsAssignedToProgramVersion As New DataSet
        Dim dstGetRequirementsDirectlyAssigned As New DataSet
        Dim intRowCounter As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If


        'Query gets the requirements assigned to LeadGroup and Program Version
        With sbReqsAssignedDirectlyToLeadGroup
            .Append(" select distinct t1.adReqId,Descrip,adReqTypeId, ")
            .Append(" '00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            .Append(" (select Distinct ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append(" (select Distinct TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append(" (select Distinct Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as Comments, ")
            .Append(" (select Distinct OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as override, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and LeadId = '" & LeadId & "' ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,ISNUll(t3.IsRequired,0) as Required, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
            .Append(" Case when t3.LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t3.LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            ''Modified by Saraswathi Lakshmanan on Sept 21 2010
            ''For document tracking
            .Append(" (select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and reqforEnrollment =1 and StatusId='" & strActiveGUID & "') t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append(" adReqLeadGroups t3 ")
            .Append(" where ")
            .Append(" t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            ' Requirement should not be in requirement group assigned to program version
            .Append(" t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId='" & PrgVerId & "' and s2.adReqId is null)   ")
            .Append(" and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and t3.LeadGrpId='" & LeadGrpId & "' ")
            ' Requirement should not be directly assigned to program version
            .Append(" and t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId='" & PrgVerId & "') ")
            If Not strCampGrpId = "" Then
                .Append("  and t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With

        dadNorthwind = New OleDbDataAdapter(sbReqsAssignedDirectlyToLeadGroup.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "RequirementsToLeadGroup")

        Return dstNorthwind

    End Function
    Public Function GetRequirementsAssignedToLeadReqGroupNoPrgVersion(ByVal LeadId As String, ByVal EnrollDate As Date, ByVal LeadGrpId As String, ByVal CampusId As String) As DataSet
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim sb, sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder


        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim dsGetCmpGrps As DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If


        'Query gets the requirements assigned to LeadGroup and Program Version
        With sbReqsAssignedDirectlyToLeadGroup
            .Append(" select distinct t1.adReqId,Descrip,adReqTypeId, ")
            .Append(" '00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            .Append(" (select Distinct ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append(" (select Distinct TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append(" (select Distinct Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as Comments, ")
            .Append(" (select Distinct OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as override, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and LeadId = '" & LeadId & "' ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,ISNULL(t3.IsRequired,0) as Required, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
            .Append(" Case when t3.LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t3.LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010
            ''For Document Tracking
            .Append(" (select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and ReqforEnrollment=1  and StatusId='" & strActiveGUID & "') t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append(" adReqLeadGroups t3 ")
            .Append(" where ")
            .Append(" t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  ")
            .Append(" and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and t3.LeadGrpId='" & LeadGrpId & "' ")
            If Not strCampGrpId = "" Then
                .Append("  and t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With

        dadNorthwind = New OleDbDataAdapter(sbReqsAssignedDirectlyToLeadGroup.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "RequirementsToLeadGroup")

        Return dstNorthwind

    End Function
    Public Function GetRequirementsAssignedToLeadReqGroupNoProgramVersion(ByVal LeadId As String, ByVal PrgVerId As String, ByVal EnrollDate As Date, ByVal LeadGrpId As String, ByVal CampusId As String) As DataSet
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim sb, sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If


        'Query gets the requirements assigned to LeadGroup and Program Version
        With sbReqsAssignedDirectlyToLeadGroup
            .Append(" select distinct t1.adReqId,Descrip,adReqTypeId, ")
            .Append(" '00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            .Append(" (select Distinct ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as ActualScore, ")
            .Append(" (select Distinct TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as TestTaken, ")
            .Append(" (select Distinct Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as Comments, ")
            .Append(" (select Distinct OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and LeadId = '" & LeadId & "' ) as override, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and LeadId = '" & LeadId & "' ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,ISNUll(t3.IsRequired,0) as Required, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
            .Append(" Case when t3.LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t3.LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            ''Modofied by Saraswathi lakshmanan on Sept 21 2010
            ''For Document Tracking
            .Append(" (select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and ReqforEnrollment=1 and StatusId='" & strActiveGUID & "') t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append(" adReqLeadGroups t3 ")
            .Append(" where ")
            .Append(" t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  ")
            ' Requirement should not be in requirement group assigned to program version
            '            .Append(" t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId='" & PrgVerId & "' and s2.adReqId is null)   ")
            .Append(" and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and t3.LeadGrpId='" & LeadGrpId & "' ")
            ' Requirement should not be directly assigned to program version
            '.Append(" and t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId='" & PrgVerId & "') ")
            If Not strCampGrpId = "" Then
                .Append("  and t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With

        dadNorthwind = New OleDbDataAdapter(sbReqsAssignedDirectlyToLeadGroup.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "RequirementsToLeadGroup")

        Return dstNorthwind

    End Function
    Public Function GetAllExistingStandardDocumentsByEffectiveDates(ByVal LeadId As String, ByVal CampusId As String, ByVal LeadDocId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strPreviousEducationId As String
        Dim strPreviousEduTest As String
        Dim StrEnrollDate As Date = Date.Now.ToShortDateString
        Dim dsGetCmpGrps As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String = ""
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        Try
            With sb
                .Append(" select Distinct R.adReqId as DocumentId,'(X) ' + R.Descrip as DocumentDescrip,NULL as ReqGrpId,NULL as StartDate,NULL as EndDate, ")
                .Append("       NULL as ActualScore, ")
                .Append("       NULL as TestTaken, ")
                .Append("       NULL as Comments, ")
                .Append("       OverRide as OverRide, ")
                .Append("       NULL as MinScore, ")
                .Append("       NULL as Required, ")
                .Append("       NULL as DocSubmittedCount, ")
                .Append("       NULL as DocStatusDescrip, ")
                .Append("       NULL as TestTakenCount,NULL as Pass,R.CampGrpId ")
                .Append(" from ")
                .Append(" adLeadDocsReceived  SD,(Select adReqId,Descrip,CampGrpId from adReqs) R ")
                .Append(" WHERE SD.DocumentId = R.adReqId AND SD.LeadDocId='" & LeadDocId & "' ")
                .Append("  AND SD.DocumentId not in (Select Distinct adReqId from adReqs WHERE StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ")
                .Append("  CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(")) ")
                .Append(" Union ")
                .Append(" select ")
                .Append("           adReqId as  DocumentId, ")
                .Append("           Descrip as DocumentDescrip, ")
                .Append("           ReqGrpId, ")
                .Append("           StartDate, ")
                .Append("           EndDate, ")
                .Append("           ActualScore,  ")
                .Append("           TestTaken, ")
                .Append("           Comments, ")
                .Append("           Case when OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append("           Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount, ")
                .Append("           DocSubmittedCount, ")
                .Append("           Minscore, ")
                .Append("           Required, ")
                .Append("		    case when Pass=1 then 'True' else 'False' end as Pass, ")
                .Append("       case when DocStatusDescrip='Approved' then 'Approved' else 'Not Approved' end as DocStatusDescrip,CampGrpId ")
                .Append("           from ")
                .Append("           ( ")
                .Append("           select  Distinct ")
                .Append("	           adReqId, ")
                .Append("	           Descrip, ")
                .Append("	           ReqGrpId, ")
                .Append("	           StartDate, ")
                .Append("	           EndDate, ")
                .Append("	           ActualScore, ")
                .Append("	           TestTaken, ")
                .Append("	           Comments, ")
                .Append("	           OverRide, ")
                .Append("	           Minscore, ")
                .Append("	           Required, ")
                .Append("	           Case  when ActualScore >= Minscore then 1 else 0 End as Pass, ")
                .Append("	           DocSubmittedCount, ")
                .Append("	           DocStatusDescrip,CampGrpId ")
                .Append("           from  ")
                .Append("           ( ")
                '  Get Requirement Group and requirements of mandatory requirement
                .Append("          	 select  ")
                .Append("	           	t1.adReqId, ")
                .Append("	           	t1.Descrip, ")
                .Append("               t1.StatusId, ")
                .Append("	           	case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("	           		(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("	           	else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId,  ")
                .Append("	           	'" & StrEnrollDate & "' as CurrentDate,  ")
                .Append("	           	t2.StartDate, ")
                .Append("	           	t2.EndDate,  ")
                .Append("	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId='" & LeadId & "')  as ActualScore,  ")
                .Append("	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId='" & LeadId & "')  as TestTaken,  ")
                .Append("	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId='" & LeadId & "')  as Comments,  ")
                .Append("	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and  ")
                .Append("	                              LeadId='" & LeadId & "') ")
                .Append("	                      as override, ")
                .Append("	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
                .Append("	           		LeadId='" & LeadId & "')  as DocSubmittedCount, ")
                .Append("	           	t2.Minscore, ")
                .Append("	           	1 as Required, ")
                .Append("	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("	           s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("           from ")
                .Append("           	adReqs t1, ")
                .Append("           	adReqsEffectiveDates t2 ")
                .Append("           where  ")
                ''Modified by Saraswathi Lakshmanan on Sept 21 2010
                ''For document tracking
                .Append(" t1.ReqforEnrollment=1 and  ")
                .Append("           	t1.adReqId = t2.adReqId and  ")
                .Append("           	t2.MandatoryRequirement=1 and ")
                .Append("           	t1.adReqTypeId in (3)  ")
                .Append("           ) ")
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("                       and R1.StatusId='" + AdvantageCommonValues.ActiveGuid + "' ")
                .Append(" union ")
                '  Get Requirements that were assigned to lead group but not part of any requirement group
                .Append(" select ")
                .Append("           adReqId, ")
                .Append("           Descrip, ")
                .Append("           ReqGrpId, ")
                .Append("           StartDate, ")
                .Append("           EndDate, ")
                .Append("           ActualScore,  ")
                .Append("           TestTaken, ")
                .Append("           Comments, ")
                .Append("           OverRide, ")
                .Append("	           Minscore, ")
                .Append("	           Required, ")
                .Append("	           Case  when ActualScore >= Minscore then 1 else 0 End as Pass, ")
                .Append("	           DocSubmittedCount, ")
                .Append("	           DocStatusDescrip,CampGrpId ")
                .Append("           from ")
                .Append("           ( ")
                '  Get Requirement Group and requirements that are assigned to a lead group
                '  and part of a requirement group
                .Append("           	 select  ")
                .Append(" 	           	t1.adReqId, ")
                .Append(" 	           	t1.Descrip, ")
                .Append("               t1.StatusId, ")
                .Append(" 	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append(" 	           	'" & StrEnrollDate & "' as CurrentDate,  ")
                .Append(" 	           	t2.StartDate, ")
                .Append(" 	           	t2.EndDate,  ")
                .Append(" 	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append(" 	           		LeadId='" & LeadId & "')  as ActualScore,  ")
                .Append(" 	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append(" 	           		LeadId='" & LeadId & "')  as TestTaken,  ")
                .Append(" 	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append(" 	           		LeadId='" & LeadId & "')  as Comments,  ")
                .Append(" 	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append(" 	                              LeadId='" & LeadId & "') ")
                .Append(" 	                      as override, ")
                .Append(" 	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
                .Append(" 	           		LeadId='" & LeadId & "')  as DocSubmittedCount, ")
                .Append(" 	           	t2.Minscore, ")
                .Append(" 			ISNULL(t3.IsRequired,0) as Required, ")
                .Append(" 	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append(" 	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append(" 	           s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("            from ")
                .Append("           	adReqs t1, ")
                .Append("           	adReqsEffectiveDates t2, ")
                .Append("		adReqLeadGroups t3 ")
                .Append("           where  ")
                .Append(" t1.reqforEnrollment=1 and  ")
                .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId ")
                ' and t1.adReqId not in (select Distinct adReqId from adReqGrpDef)
                .Append("		and t3.LeadGrpId in ")
                .Append("		(select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and ")
                .Append("           	t1.adReqTypeId in (3) and t2.MandatoryRequirement <> 1 ")
                .Append("           ) ")
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("                       and R1.StatusId='" + AdvantageCommonValues.ActiveGuid + "' ")
                .Append(" ) R2 ")
                If Not strCampGrpId = "" Then
                    .Append("   WHERE R2.CampGrpId in 	('")
                    .Append(strCampGrpId)
                    .Append(") ")
                End If
            End With

            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As Exception
        Finally
        End Try
    End Function


#End Region
    ''Added by Saraswathi Lakshmanan on Sept 23 2010
    ''For finAid Requirement Document
    Public Function CheckAllApprovedDocumentsWithPrgVersion_FinAid_LeadandStudent(ByVal StudentID As String, ByVal PrgVerId As String, Optional ByVal CampusId As String = "") As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim strEnrollDate As Date = Date.Now.ToShortDateString
        Dim LeadID As String

        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If


        Try

            With sb
                .Append(" select Count(*) from ")
                .Append(" ( ")
                .Append(" select  distinct ")
                .Append("       adReqId as DocumentId, ")
                .Append("       Descrip as DocumentDescrip, ")
                .Append("       ReqGrpId, ")
                .Append("       StartDate,  ")
                .Append("       EndDate, ")
                .Append("       ActualScore, ")
                .Append("       TestTaken, ")
                .Append("       Comments, ")
                .Append("       Case when OverRide>=1 then 'True' else 'False' end as OverRide, ")
                .Append("       Case when (select count(*) from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') >=1 then ")
                .Append("       (select MinScore from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') ")
                .Append("       else MinScore end as MinScore, ")
                .Append("       Required, ")
                .Append("       DocSubmittedCount, ")
                .Append("       case when DocStatusDescrip='Approved' then 'Approved' else 'Not Approved' end as DocStatusDescrip, ")
                .Append("       Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount,Pass,CampGrpId ")
                .Append("       from  ")
                .Append("       ( ")
                ' Get Requirement Group and requirements of mandatory requirement
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate, ")
                .Append("		      		ActualScore, ")
                .Append("		      		TestTaken, ")
                .Append("		      		Comments, ")
                .Append("		      		OverRide, ")
                .Append("		      		Minscore, ")
                .Append("		      		Required, ")
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		      		DocSubmittedCount, ")
                .Append("		      		DocStatusDescrip,CampGrpId ")
                .Append("      	from  ")
                .Append("      		( ")
                .Append("      		select Distinct ")
                .Append("      				t1.adReqId, ")
                .Append("      				t1.Descrip, ")
                .Append("      				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("      					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("      					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                .Append("      				'" & strEnrollDate & "' as CurrentDate, ")
                .Append("      				t2.StartDate, ")
                .Append("      				t2.EndDate, ")
                .Append("      				(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("      					LeadId = '" & LeadID & "' ) as ActualScore, ")
                .Append("      				(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("      					LeadId = '" & LeadID & "' ) as TestTaken, ")
                .Append("      				(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("      					LeadId = '" & LeadID & "' ) as Comments, ")
                .Append("      				       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("      				        LeadId = '" & LeadID & "' ) as override, ")

                .Append("      				(IsNull((select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("      					LeadId = '" & LeadID & "'  ),0) + ")

                .Append("      				IsNull(select Count(*) from plStudentDocs where DocumentId=t1.adReqId and  ")
                .Append("      					StudentID = '" & StudentID & "'  ) ,0))as DocSubmittedCount, ")

                .Append("      				t2.Minscore, ")
                .Append("      				1 as Required, ")
                .Append("      				(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("      				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("      				s3.LeadId = '" & LeadID & "' and s3.DocumentId = t1.adReqId)")

                .Append("      				union (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
                .Append("      				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("      				s3.StudentID = '" & StudentID & "' and s3.DocumentId = t1.adReqId)")

                .Append("                   as DocStatusDescrip,t1.CampGrpId ")
                .Append("      				from  ")
                .Append("      				adReqs t1, ")
                .Append("      				adReqsEffectiveDates t2 ")
                .Append("      				where  ")
                .Append("      				t1.adReqId = t2.adReqId and ")
                .Append("       				t2.MandatoryRequirement=1 and ")
                ''Added by Saraswathi Lakshmanan on Sept 15 2010
                ''To filter only the documents marked as required for Enrollment
                ''For Document tracking
                .Append("                   t1.ReqforEnrollment=1 and ")
                .Append("      				t1.adReqTypeId in (3) and t1.StatusId ='" & strActiveGUID & "' ")
                .Append("       				) ")
                .Append("       				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("       		union ")

                ' Get The Requirements that was assigned to a program version
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate, ")
                .Append("		      		ActualScore, ")
                .Append("		      		TestTaken, ")
                .Append("		      		Comments, ")
                .Append("		      		OverRide, ")
                .Append("		      		Minscore, ")
                .Append("		      		Required, ")
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		      		DocSubmittedCount, ")
                .Append("		      		DocStatusDescrip,CampGrpId ")
                .Append("      				from  ")
                .Append("      					( ")
                .Append("      						select Distinct ")
                .Append("      							t1.adReqId, ")
                .Append("      							t1.Descrip, ")
                .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
                .Append("	      						t2.StartDate, ")
                .Append("	      						t2.EndDate, ")
                .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	      							LeadId = '" & LeadID & "' ) as ActualScore, ")
                .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							LeadId = '" & LeadID & "' ) as TestTaken, ")
                .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							LeadId = '" & LeadID & "' ) as Comments, ")
                .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("	      						        LeadId = '" & LeadID & "' ) ")
                .Append("	      						as override, ")

                .Append("      				(IsNull((select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("      					LeadId = '" & LeadID & "'  ),0) + ")

                .Append("      				IsNull(select Count(*) from plStudentDocs where DocumentId=t1.adReqId and  ")
                .Append("      					StudentID = '" & StudentID & "'  ) ,0))as DocSubmittedCount, ")

                .Append("		      					t2.Minscore, ")
                .Append("	      						1 as Required, ")

                .Append("      				(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("      				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("      				s3.LeadId = '" & LeadID & "' and s3.DocumentId = t1.adReqId)")

                .Append("      				union (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
                .Append("      				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("      				s3.StudentID = '" & StudentID & "' and s3.DocumentId = t1.adReqId)")

                .Append("                               as DocStatusDescrip,t1.CampGrpId ")
                .Append("      						from  ")
                .Append("      							adReqs t1, ")
                .Append("      							adReqsEffectiveDates t2, ")
                .Append("      							adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
                .Append("      						where ")
                .Append("      							t1.adReqId = t2.adReqId and t2.MandatoryRequirement <> 1 and ")
                ''Added by Saraswathi Lakshmanan on Sept 15 2010
                ''To filter only the documents marked as required for Enrollment
                ''For Document tracking
                .Append("                   t1.ReqforEnrollment=1 and ")
                .Append("      							t1.adreqTypeId in (3) and t1.StatusId ='" & strActiveGUID & "' and ")
                .Append("      							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId ")
                .Append("      							and t1.adReqId = t5.adReqId and t5.PrgVerId = '" & PrgVerId & "'    ")
                '.Append("      					      	 and	t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where ")
                '.Append("	      						LeadId = '" & LeadId & "' ) ")
                .Append("      							and t5.adReqId is not null ")
                .Append("      						) ")
                .Append("      						R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                ' Get Requirements === Requirement Group assigned to Program Version
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate, ")
                .Append("		      		ActualScore, ")
                .Append("		      		TestTaken, ")
                .Append("		      		Comments, ")
                .Append("		      		OverRide, ")
                .Append("		      		Minscore, ")
                .Append("		      		Required, ")
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		      		DocSubmittedCount, ")
                .Append("		      		DocStatusDescrip,CampGrpId ")
                .Append("      				from  ")
                .Append("      					( ")
                .Append("      						select Distinct ")
                .Append("      							t1.adReqId, ")
                .Append("      							t1.Descrip, ")
                .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
                .Append("	      						t2.StartDate, ")
                .Append("	      						t2.EndDate, ")
                .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	      							LeadId = '" & LeadID & "' ) as ActualScore, ")
                .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							LeadId = '" & LeadID & "' ) as TestTaken, ")
                .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							LeadId = '" & LeadID & "' ) as Comments, ")
                .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("	      						        LeadId = '" & LeadID & "' ) ")
                .Append("	      						as override, ")

                .Append("      				(IsNull((select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("      					LeadId = '" & LeadID & "'  ),0) + ")

                .Append("      				IsNull(select Count(*) from plStudentDocs where DocumentId=t1.adReqId and  ")
                .Append("      					StudentID = '" & StudentID & "'  ) ,0))as DocSubmittedCount, ")

                .Append("		      					t2.Minscore, ")
                .Append("	      						ISNULL(t3.IsRequired,0) as Required, ")

                .Append("      				(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("      				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("      				s3.LeadId = '" & LeadID & "' and s3.DocumentId = t1.adReqId)")

                .Append("      				union (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
                .Append("      				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("      				s3.StudentID = '" & StudentID & "' and s3.DocumentId = t1.adReqId)")

                '.Append("	      						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                '.Append("      							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                '.Append("      							s3.LeadId = '" & LeadID & "' and s3.DocumentId = t1.adReqId) ")

                .Append("                               as DocStatusDescrip,t1.CampGrpId ")
                .Append("      						from  ")
                .Append("      							adReqs t1, ")
                .Append("							adReqsEffectiveDates t2, ")
                .Append("							adReqLeadGroups t3, ")
                .Append("							adPrgVerTestDetails t5, ")
                .Append("							adReqGrpDef t6, ")
                .Append("						adLeadByLeadGroups t7  ")
                .Append("      					where ")
                .Append("      						t1.adReqId = t2.adReqId and ")
                ''Added by Saraswathi Lakshmanan on Sept 15 2010
                ''To filter only the documents marked as required for Enrollment
                ''For Document tracking
                .Append("                   t1.ReqforEnrollment=1 and ")
                .Append("      						t1.adreqTypeId in (3) and t1.StatusId ='" & strActiveGUID & "' and ")
                .Append("      						t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and ")
                .Append("						t3.LeadGrpId = t7.LeadGrpId and ")
                .Append("      						t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
                .Append("	      					and t7.LeadId = '" & LeadID & "'  and  ")
                .Append("						t5.PrgVerId = '" & PrgVerId & "'   ")
                .Append("      		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                ' Get Requirements that were assigned to lead group but not part of any requirement group
                .Append("	     select  distinct ")
                .Append("	           adReqId, ")
                .Append("	           Descrip, ")
                .Append("	           ReqGrpId, ")
                .Append("	           StartDate, ")
                .Append("	           EndDate, ")
                .Append("	           ActualScore, ")
                .Append("	           TestTaken, ")
                .Append("	           Comments, ")
                .Append("	           OverRide, ")
                .Append("	           Minscore, ")
                .Append("	           Required, ")
                .Append("	           Case  when ActualScore >= Minscore  then 'True' else 'False' End as Pass, ")
                .Append("	           DocSubmittedCount, ")
                .Append("	           DocStatusDescrip,CampGrpId ")
                .Append("           from  ")
                .Append("           ( ")
                ' Get Requirement Group and requirements that are assigned to a lead group
                ' and part of a requirement group
                .Append("          	 select  distinct ")
                .Append("	           	t1.adReqId, ")
                .Append("	           	t1.Descrip, ")
                .Append("	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append("	           	'" & strEnrollDate & " ' as CurrentDate,  ")
                .Append("	           	t2.StartDate, ")
                .Append("	           	t2.EndDate,  ")
                .Append("	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId = '" & LeadID & "' ) as ActualScore,  ")
                .Append("	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId = '" & LeadID & "' ) as TestTaken,  ")
                .Append("	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		LeadId = '" & LeadID & "' ) as Comments,  ")
                .Append("	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("	                              LeadId = '" & LeadID & "' ) ")
                .Append("	                      as override, ")

                .Append("      				(IsNull((select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("      					LeadId = '" & LeadID & "'  ),0) + ")

                .Append("      				IsNull(select Count(*) from plStudentDocs where DocumentId=t1.adReqId and  ")
                .Append("      					StudentID = '" & StudentID & "'  ) ,0))as DocSubmittedCount, ")

                '.Append("	           	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
                '.Append("	           		LeadId = '" & LeadID & "' ) as DocSubmittedCount, ")

                .Append("	           	t2.Minscore, ")
                .Append("			ISNULL(t3.IsRequired,0) as Required, ")


                .Append("      				(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("      				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("      				s3.LeadId = '" & LeadID & "' and s3.DocumentId = t1.adReqId)")

                .Append("      				union (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
                .Append("      				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("      				s3.StudentID = '" & StudentID & "' and s3.DocumentId = t1.adReqId)")

                '.Append("	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                '.Append("	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                '.Append("	           s3.LeadId = '" & LeadID & "' and s3.DocumentId = t1.adReqId) ")


                .Append("              as DocStatusDescrip,t1.CampGrpId ")
                .Append("           from ")
                .Append("           	adReqs t1, ")
                .Append("           	adReqsEffectiveDates t2, ")
                .Append("		adReqLeadGroups t3 ")
                .Append("           where  ")
                .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId ")
                ''Added by Saraswathi Lakshmanan on Sept 15 2010
                ''To filter only the documents marked as required for Enrollment
                ''For Document tracking
                .Append("            and       t1.ReqforEnrollment=1  ")
                .Append("		and  t1.StatusId ='" & strActiveGUID & "'")
                .Append("		and t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId='" & PrgVerId & "' and s2.adReqId is null) ")
                .Append("		and t3.LeadGrpId in ")

                .Append("		((select LeadGrpId from adLeadByLeadGroups where LeadId = '" & LeadID & "' ) union")

                .Append(" 	(select LeadGrpId from adLeadByLeadGroups where StuEnrollId in (Select StuEnrollId from arStuEnrollments where StudentID='" & StudentID & "')) )")

                .Append(" and ")
                .Append("           	t1.adReqTypeId in (3) and t2.MandatoryRequirement <> 1 and ")
                .Append("		t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId='" & PrgVerId & "') ")
                .Append("           ) ")
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" ) R2 ")
                .Append(" ) R3 where R3.Required =1  and  R3.DocStatusDescrip = 'Not Approved' and R3.OverRide='False' ")
                If Not strCampGrpId = "" Then
                    .Append("  AND R3.CampGrpId in 	('")
                    .Append(strCampGrpId)
                    .Append(") ")
                End If
            End With

            Dim intLeadCount As Integer = db.RunParamSQLScalar(sb.ToString)
            Return intLeadCount
        Catch ex As Exception
        Finally
        End Try
    End Function


    ''Function Added by Saraswathi Lakshmanan on Sept 28 2010
    ''Function to find the graduation Requirements

    Public Function HasStudentMetGraduationDocumentRequirements_Sp(ByVal StuEnrollId As String) As Integer
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' Add empId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   Execute the query
        Dim result As Integer
        result = db.RunParamSQLScalar_SP("USP_HasStudentMetGraduationDocumentRequirements")

        Return result

        'If result >= 1 Then
        '    Return False
        'Else
        '    Return True
        'End If
        db.CloseConnection()
    End Function

    Public Function HasStudentMetEnrollmentDocumentRequirements_Sp(ByVal StuEnrollId As String) As Integer
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' Add empId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   Execute the query
        Dim result As Integer
        result = db.RunParamSQLScalar_SP("USP_HasStudentMetEnrollmentDocumentRequirements")

        Return result

        'If result >= 1 Then
        '    Return False
        'Else
        '    Return True
        'End If
        db.CloseConnection()
    End Function

    Public Function CheckIfReqGroupMeetsConditions_Graduation_SP(ByVal StuEnrollId As String) As String

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' Add empId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Dim ds As New DataSet
        Dim dr As DataRow

        ds = db.RunParamSQLDataSet_SP("USP_DoesReqGroupMeetsConditionsforGraduation")

        Dim strMessage As String
        Dim intRequiredReqs As Integer
        Dim intAttemptedReqs As Integer

        For Each dr In ds.Tables(0).Rows
            intRequiredReqs = dr("NumReqs")
            intAttemptedReqs = dr("AttemptedReqs")
            If intAttemptedReqs < intRequiredReqs Then
                strMessage = "Not Satisfied"
                Return "Not Eligible"
                Exit Function
            End If
            Dim sbCheckIfRequiredRequirementExistsWithInGroup As New StringBuilder
            Dim intReqExistsCount As Integer = 0

            db.ClearParameters()
            db.AddParameter("@ReqGrpId", dr("ReqGrpId"), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Try
                intReqExistsCount = db.RunParamSQLScalar("USP_DoesReqRequirementsExistWithinGroup")
                'If count is >=1 then there is a requirement with in this group that has been marked as required 
                'but has not been satisfied or overriden
                If intReqExistsCount >= 1 Then
                    Return "Not Eligible"
                    Exit Function
                End If
            Catch ex As Exception
                strMessage = ""
            End Try
        Next
        If Not strMessage = "" Then
            Return "Not Eligible"
        Else
            Return "Eligible"
        End If
    End Function

    Public Function CheckIfReqGroupMeetsConditions_Enrollment_SP(ByVal StuEnrollId As String) As String

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' Add empId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Dim ds As New DataSet
        Dim dr As DataRow

        ds = db.RunParamSQLDataSet_SP("USP_DoesReqGroupMeetsConditionsforEnrollment")

        Dim strMessage As String
        Dim intRequiredReqs As Integer
        Dim intAttemptedReqs As Integer

        For Each dr In ds.Tables(0).Rows
            intRequiredReqs = dr("NumReqs")
            intAttemptedReqs = dr("AttemptedReqs")
            If intAttemptedReqs < intRequiredReqs Then
                strMessage = "Not Satisfied"
                Return "Not Eligible"
                Exit Function
            End If
            Dim sbCheckIfRequiredRequirementExistsWithInGroup As New StringBuilder
            Dim intReqExistsCount As Integer = 0

            db.ClearParameters()
            db.AddParameter("@ReqGrpId", dr("ReqGrpId"), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Try
                intReqExistsCount = db.RunParamSQLScalar("USP_DoesReqRequirementsExistWithinGroupEnrollment")
                'If count is >=1 then there is a requirement with in this group that has been marked as required 
                'but has not been satisfied or overriden
                If intReqExistsCount >= 1 Then
                    Return "Not Eligible"
                    Exit Function
                End If
            Catch ex As Exception
                strMessage = ""
            End Try
        Next
        If Not strMessage = "" Then
            Return "Not Eligible"
        Else
            Return "Eligible"
        End If
    End Function
    ''Function Added by Saraswathi Lakshmanan on Sept 28 2010
    ''Function to find the graduation Requirements
    '    Public Function CheckAllApprovedDocumentsWithPrgVersion_FinAid_Student(ByVal StuEnrollID As String, Optional ByVal CampusId As String = "") As Integer
    Public Function HasStudentMetFinAidDocumentRequirements_Sp(ByVal StuEnrollId As String) As Integer
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' Add empId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   Execute the query
        Dim result As Integer
        result = db.RunParamSQLScalar_SP("USP_HasStudentMetFinAidDocumentRequirements")

        Return result

        'If result >= 1 Then
        '    Return False
        'Else
        '    Return True
        'End If
        db.CloseConnection()
    End Function

    ''Function Added by Saraswathi Lakshmanan on Sept 28 2010
    ''Function to find the graduation Requirements
    '    Public Function CheckAllApprovedDocumentsWithPrgVersion_FinAid_Student(ByVal StuEnrollID As String, Optional ByVal CampusId As String = "") As Integer
    Public Function HasStudentMetFinAidTestRequirements_Sp(ByVal StuEnrollId As String) As Integer
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' Add empId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   Execute the query
        Dim result As Integer
        result = db.RunParamSQLScalar_SP("USP_HasStudentMetFinAidTestRequirements")

        Return result

        'If result >= 1 Then
        '    Return False
        'Else
        '    Return True
        'End If
        db.CloseConnection()
    End Function



    Public Function CheckIfReqGroupMeetsConditions_FinAid_SP(ByVal StuEnrollId As String) As String

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' Add empId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Dim ds As New DataSet
        Dim dr As DataRow

        ds = db.RunParamSQLDataSet_SP("USP_DoesReqGroupMeetsConditionsforFinAid")

        Dim strMessage As String
        Dim intRequiredReqs As Integer
        Dim intAttemptedReqs As Integer

        For Each dr In ds.Tables(0).Rows
            intRequiredReqs = dr("NumReqs")
            intAttemptedReqs = dr("AttemptedReqs")
            If intAttemptedReqs < intRequiredReqs Then
                strMessage = "Not Satisfied"
                Return "Not Eligible"
                Exit Function
            End If
            Dim sbCheckIfRequiredRequirementExistsWithInGroup As New StringBuilder
            Dim intReqExistsCount As Integer = 0

            db.ClearParameters()
            db.AddParameter("@ReqGrpId", dr("ReqGrpId"), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Try
                intReqExistsCount = db.RunParamSQLScalar("USP_DoesReqRequirementsExistWithinGroup")
                'If count is >=1 then there is a requirement with in this group that has been marked as required 
                'but has not been satisfied or overriden
                If intReqExistsCount >= 1 Then
                    Return "Not Eligible"
                    Exit Function
                End If
            Catch ex As Exception
                strMessage = ""
            End Try
        Next
        If Not strMessage = "" Then
            Return "Not Eligible"
        Else
            Return "Eligible"
        End If
    End Function
    Public Function HasStudentMetGraduationTestRequirements_Sp(ByVal StuEnrollId As String) As Integer
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' Add empId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   Execute the query
        Dim result As Integer
        result = db.RunParamSQLScalar_SP("USP_HasStudentMetGraduationTestRequirements")

        Return result

        'If result >= 1 Then
        '    Return False
        'Else
        '    Return True
        'End If
        db.CloseConnection()
    End Function
    Public Function HasStudentMetEnrollmentTestRequirements_Sp(ByVal StuEnrollId As String) As Integer
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' Add empId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   Execute the query
        Dim result As Integer
        result = db.RunParamSQLScalar_SP("USP_HasStudentMetEnrollmentTestRequirements")

        Return result

        'If result >= 1 Then
        '    Return False
        'Else
        '    Return True
        'End If
        db.CloseConnection()
    End Function
    '' New code added by kamalesh Ahuja on December 30 for Rally Issue id DE 1276
    Public Function GetSchoolLevelAdmissionRequirementsWithNoProgramVersionForStudent(ByVal StudentId As String, ByVal PrgVerId As String, ByVal strEnrollDate As Date, ByVal campusid As String, ByVal SelectedLeadGroups As String) As DataSet
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim drowRootParent As DataRow
        Dim drowSchoolLevelSummary As DataRow
        Dim drowSchoolLevelDetails As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb, sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
        Dim drowRequirementAssignedDirectlyToPrgVersion, drowRequirementAssignedDirectlyToLeadGroup As DataRow
        Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder

        Dim dsGetEntranceTestId As New DataSet
        Dim dsGetLeadGrpId As New DataSet
        Dim dstGetRequirementsAssignedToProgramVersion As New DataSet
        Dim dstGetRequirementsDirectlyAssigned As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        'Dim db As New DataAccess
        'db.ConnectionString = SingletonAppSettings.AppSettings("ConString")


        Dim intRowCounter As Integer

        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


        Dim dsGetCmpGrps As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusid)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        'This query gets a list of all Lead Groups
        With sbroot
            .Append(" select Distinct LeadGrpId,Descrip from adLeadGroups where LeadGrpId in (" & SelectedLeadGroups & ") ")
        End With

        dadNorthwind = New OleDbDataAdapter(sbroot.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "ParentRoot")

        Dim strCurrentDate As String = Date.Now.ToShortDateString

        'Get the summary of all school level requirements
        With sbMandatorySummary
            '*********************************************************************************************************************************************
            ' This Query gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted for Mandatory Requirement Group
            '*********************************************************************************************************************************************
            .Append(" select Top 1 ReqGrpId,R5.Descrip,R5.NumReqs,R5.TestPassed+R5.DocsApproved+R5.TestDocumentAttempted as AttemptedReqs ")
            .Append(" from ")
            .Append(" ( ")
            .Append(" select 'CFD29DFD-4FFF-400B-B152-4FABB12E6E7B' as ReqGrpId, ")
            .Append(" case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then (select Distinct Descrip from adReqGroups where IsMandatoryReqGrp=1) else 'School Level Requirements' end  as Descrip,  ")
            .Append(" CampGrpId, ")
            .Append(" (select Count(*) as NumReqs from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" from adReqs A2,adReqsEffectiveDates A3 ")
            .Append(" where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append("   A2.reqforEnrollment=1 and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" and A2.adreqTypeId in (1,3) and A2.StatusId='" & strActiveGUID & "') ")
            .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as NumReqs, ")
            ''''''''''''' This Query gives the number of test passed by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate  ")
            .Append(" from adReqs A2,adReqsEffectiveDates A3 ")
            .Append(" where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append("   A2.reqforEnrollment=1 and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" and A2.adreqTypeId=1 and A2.StatusId='" & strActiveGUID & "') R1,adLeadEntranceTest R2  where ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.StudentId='" & StudentId & "'  and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and R2.Pass=1 and ")
            .Append(" R2.EntrTestId not in (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and StudentId='" & StudentId & "') and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as TestPassed,")
            ''''''''''''' This Query gives the number of docs approved by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate from   adReqs A2,adReqsEffectiveDates A3 ")
            .Append(" where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append("   A2.reqforEnrollment=1 and ")

            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" and A2.adreqTypeId=3 and A2.StatusId='" & strActiveGUID & "') R1,plStudentDocs R2,syDocStatuses R3  ")
            .Append(" where R1.adReqId = R2.DocumentId and R2.StudentId='" & StudentId & "'  and R2.DocumentId not in  (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and StudentId='" & StudentId & "') ")
            .Append(" and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as DocsApproved,  ")
            ''''''''''''' This Query gives the number of test and documents approved by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" from   adReqs A2,adReqsEffectiveDates A3 where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            ''MOdified by Saraswathi Lakshmanan On Sept 21 2010
            ''For document tracking
            ''Check for the field ReqforEnrollment if it is set to 1.
            .Append(" and A2.adreqTypeId in (1,3) and A2.ReqforEnrollment=1  and A2.StatusId='" & strActiveGUID & "') ")
            .Append(" R1,adEntrTestOverRide R2 where R1.adReqId = R2.EntrTestId and R2.StudentId='" & StudentId & "'  ")
            .Append(" and R1.CurrentDate >= R1.StartDate and R2.OverRide=1 and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" ) as TestDocumentAttempted,NULL as LeadGrpId  ")
            '.Append(" from adReqGroups t7 where IsMandatoryReqGrp = 1  ")
            .Append("   from adReqs t1,adReqsEffectiveDates t2 ")
            ''MOdified by Saraswathi Lakshmanan On Sept 21 2010
            ''For document tracking
            ''Check for the field ReqforEnrollment if it is set to 1.
            .Append("   where t1.adReqId = t2.adReqId and t2.MandatoryRequirement=1 and t1.adreqTypeId in (1,3) and t1.ReqforEnrollment=1   ")
            .Append(" ) R5 ")
            If Not strCampGrpId = "" Then
                .Append("  WHERE R5.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With
        'End of the summary of all school level requirements


        'Start query to get all school level requirements
        With sbMandatoryDetails
            .Append(" select distinct t1.adReqId,Descrip,t1.adReqTypeId, ")
            '.Append(" case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then (select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
            .Append(" 'CFD29DFD-4FFF-400B-B152-4FABB12E6E7B' as ReqGrpId, ")
            .Append(" (select Distinct Top 1 ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as ActualScore, ")
            .Append(" (select Distinct Top 1 TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as TestTaken, ")
            .Append(" (select Distinct Top 1 Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as Comments, ")
            .Append(" (select Distinct Top 1 OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as override, ")
            .Append(" (select Count(*) from plStudentDocs where DocumentId=t1.adReqId and StudentId = '" & StudentId & "'  ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,1 as Required, ")
            .Append(" (select Distinct Top 1 s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.StudentId = '" & StudentId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,'00000000-0000-0000-0000-000000000000' as LeadGrpId ")
            ''MOdified by Saraswathi Lakshmanan On Sept 21 2010
            ''For document tracking
            ''Check for the field ReqforEnrollment if it is set to 1.
            .Append(" from (select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and ReqforEnrollment=1  and StatusId='" & strActiveGUID & "') t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement=1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2 ")
            .Append(" where t1.adReqId = t2.adReqId ")
            If Not strCampGrpId = "" Then
                .Append("  and t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With
        'End query to get all school level requirements

        dadNorthwind.SelectCommand = New OleDbCommand(sbMandatorySummary.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "SchoolLevelRequirementSummary")

        dadNorthwind.SelectCommand = New OleDbCommand(sbMandatoryDetails.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "SchoolLevelRequirementDetails")



        '    'Set the relationship between school level requirements
        '    dstNorthwind.Relations.Add("SchoolLevelRelation", dstNorthwind.Tables("SchoolLevelRequirementSummary").Columns("ReqGrpId"), _
        '    dstNorthwind.Tables("SchoolLevelRequirementDetails").Columns("ReqGrpId"))



        With sbReqsAssignedDirectlyToLeadGroup
            .Append(" select distinct t1.adReqId,Descrip, ")
            .Append(" '00000000-0000-0000-0000-000000000000' as ReqGrpId,adReqTypeId, ")
            .Append(" (select Distinct ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as ActualScore, ")
            .Append(" (select Distinct TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as TestTaken, ")
            .Append(" (select Distinct Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as Comments, ")
            .Append(" (select Distinct OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as override, ")
            .Append(" (select Count(*) from plStudentDocs where DocumentId=t1.adReqId and StudentId = '" & StudentId & "' ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,ISNULL(t3.IsRequired,0) as Required, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.StudentId = '" & StudentId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
            .Append(" Case when t3.LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t3.LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            ''MOdified by Saraswathi Lakshmanan On Sept 21 2010
            ''For document tracking
            ''Check for the field ReqforEnrollment if it is set to 1.
            .Append(" (select Distinct adReqId,Descrip,CampGrpId,adReqTypeId from adReqs where adReqTypeId in (1,3) and ReqforEnrollment=1 and StatusId='" & strActiveGUID & "') t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append(" adReqLeadGroups t3 ")
            .Append(" where ")
            .Append(" t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            .Append(" t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where StudentId='" & StudentId & "') ")
            If Not strCampGrpId = "" Then
                .Append("  and t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With
        dadNorthwind.SelectCommand = New OleDbCommand(sbReqsAssignedDirectlyToLeadGroup.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "RequirementAssignedDirectlyToLeadGroup")


        Return dstNorthwind


    End Function
    '' New code added by kamalesh Ahuja on December 30 for Rally Issue id DE 1276
    Public Function GetSchoolLevelAdmissionRequirementsForStudent(ByVal StudentId As String, ByVal PrgVerId As String, ByVal strEnrollDate As Date, ByVal CampusId As String, ByVal SelectedLeadGroups As String) As DataSet
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim drowRootParent As DataRow
        Dim drowSchoolLevelSummary As DataRow
        Dim drowSchoolLevelDetails As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb, sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
        Dim drowRequirementAssignedDirectlyToPrgVersion, drowRequirementAssignedDirectlyToLeadGroup As DataRow
        Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder

        Dim dsGetEntranceTestId As New DataSet
        Dim dsGetLeadGrpId As New DataSet
        Dim dstGetRequirementsAssignedToProgramVersion As New DataSet
        Dim dstGetRequirementsDirectlyAssigned As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Dim db As New DataAccess
        'db.ConnectionString = SingletonAppSettings.AppSettings("ConString")


        Dim intRowCounter As Integer

        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


        Dim dsGetCmpGrps As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        'This query gets a list of all Lead Groups
        With sbroot
            .Append(" select Distinct LeadGrpId,Descrip from adLeadGroups  where LeadGrpId in (" & SelectedLeadGroups & ")  ")
        End With

        dadNorthwind = New OleDbDataAdapter(sbroot.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "ParentRoot")

        Dim strCurrentDate As String = Date.Now.ToShortDateString

        'Get the summary of all school level requirements
        With sbMandatorySummary
            '*********************************************************************************************************************************************
            ' This Query gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted for Mandatory Requirement Group
            '*********************************************************************************************************************************************
            .Append(" select Top 1 ReqGrpId,R5.Descrip,R5.NumReqs,R5.TestPassed+R5.DocsApproved+R5.TestDocumentAttempted as AttemptedReqs ")
            .Append(" from ")
            .Append(" ( ")
            .Append(" select 'CFD29DFD-4FFF-400B-B152-4FABB12E6E7B' as ReqGrpId, ")
            .Append(" case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then (select Distinct Descrip from adReqGroups where IsMandatoryReqGrp=1) else 'School Level Requirements' end  as Descrip,  ")
            .Append(" CampGrpId, ")
            .Append(" (select Count(*) as NumReqs from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" from adReqs A2,adReqsEffectiveDates A3 ")
            .Append(" where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and A2.StatusId='" & strActiveGUID & "' and ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append("   A2.reqforEnrollment=1 and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" and A2.adreqTypeId in (1,3)) ")
            .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as NumReqs, ")
            ''''''''''''' This Query gives the number of test passed by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate  ")
            .Append(" from adReqs A2,adReqsEffectiveDates A3 ")
            .Append(" where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append("   A2.reqforEnrollment=1 and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" and A2.adreqTypeId=1 and A2.StatusId='" & strActiveGUID & "') R1,adLeadEntranceTest R2  where ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.StudentId='" & StudentId & "'  and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and R2.Pass=1 and ")
            .Append(" R2.EntrTestId not in (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and StudentId='" & StudentId & "') and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as TestPassed,")
            ''''''''''''' This Query gives the number of docs approved by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate from   adReqs A2,adReqsEffectiveDates A3 ")
            .Append(" where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append("   A2.reqforEnrollment=1 and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" and A2.adreqTypeId=3 and A2.StatusId='" & strActiveGUID & "') R1,plStudentDocs R2,syDocStatuses R3  ")
            .Append(" where R1.adReqId = R2.DocumentId and R2.StudentId='" & StudentId & "'  and R2.DocumentId not in  (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and StudentId='" & StudentId & "') ")
            .Append(" and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as DocsApproved,  ")
            ''''''''''''' This Query gives the number of test and documents approved by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" from   adReqs A2,adReqsEffectiveDates A3 where A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append("   A2.reqforEnrollment=1 and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" and A2.adreqTypeId in (1,3)  and A2.StatusId='" & strActiveGUID & "') ")
            .Append(" R1,adEntrTestOverRide R2 where R1.adReqId = R2.EntrTestId and R2.StudentId='" & StudentId & "'  ")
            .Append(" and R1.CurrentDate >= R1.StartDate and R2.OverRide=1 and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" ) as TestDocumentAttempted,NULL as LeadGrpId  ")
            '.Append(" from adReqGroups t7 where IsMandatoryReqGrp = 1  ")
            .Append("   from adReqs t1,adReqsEffectiveDates t2 ")
            .Append("   where t1.adReqId = t2.adReqId and t2.MandatoryRequirement=1 and t1.adreqTypeId in (1,3)  ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010 For Document Tracking
            .Append(" and  t1.reqforEnrollment=1  ")
            .Append(" ) R5 ")
            If Not strCampGrpId = "" Then
                .Append("  WHERE R5.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With
        'End of the summary of all school level requirements


        'Start query to get all school level requirements
        With sbMandatoryDetails
            .Append(" select distinct t1.adReqId,Descrip,t1.adReqTypeId, ")
            '.Append(" case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then (select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
            .Append(" 'CFD29DFD-4FFF-400B-B152-4FABB12E6E7B' as ReqGrpId, ")
            .Append(" (select Distinct Top 1 ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as ActualScore, ")
            .Append(" (select Distinct Top 1 TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as TestTaken, ")
            .Append(" (select Distinct Top 1 Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as Comments, ")
            .Append(" (select Distinct Top 1 OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as override, ")
            .Append(" (select Count(*) from plStudentDocs where DocumentId=t1.adReqId and StudentId = '" & StudentId & "'  ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,1 as Required, ")
            .Append(" (select Distinct Top 1 s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.StudentId = '" & StudentId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,'00000000-0000-0000-0000-000000000000' as LeadGrpId ")
            ''MOdified by Saraswathi Lakshmanan On Sept 21 2010
            ''For document tracking
            ''Check for the field ReqforEnrollment if it is set to 1.
            .Append(" from (select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and ReqforEnrollment=1 and StatusId='" & strActiveGUID & "') t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement=1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2 ")
            .Append(" where t1.adReqId = t2.adReqId ")
            If Not strCampGrpId = "" Then
                .Append("  and t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With
        'End query to get all school level requirements

        dadNorthwind.SelectCommand = New OleDbCommand(sbMandatorySummary.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "SchoolLevelRequirementSummary")

        dadNorthwind.SelectCommand = New OleDbCommand(sbMandatoryDetails.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "SchoolLevelRequirementDetails")


        'Get Requirements assigned directly to program version and this Requirement should not be part of
        'the requirement group that is assigned to program version
        With sbReqAssignedDirectlyToPrgVersion
            ' Requirements(not part of any requirement group) assigned directly to program version
            .Append(" select distinct t1.adReqId,Descrip,adReqTypeId, ")
            .Append(" '00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            .Append(" (select Distinct Top 1 ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as ActualScore, ")
            .Append(" (select Distinct Top 1 TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as TestTaken, ")
            .Append(" (select Distinct Top 1 Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as Comments, ")
            .Append(" (select Distinct Top 1 OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as override, ")
            .Append(" (select Count(*) from plStudentDocs where DocumentId=t1.adReqId and StudentId = '" & StudentId & "' ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,1 as Required, ")
            .Append(" (select Distinct Top 1 s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.StudentId = '" & StudentId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append(" from ")
            ''MOdified by Saraswathi Lakshmanan On Sept 21 2010
            ''For document tracking
            ''Check for the field ReqforEnrollment if it is set to 1.
            .Append(" (select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and ReqforEnrollment=1  and StatusId='" & strActiveGUID & "') t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append(" (select Distinct adReqId from adPrgVerTestDetails  where PrgVerId = '" & PrgVerId & "'  and ReqGrpId is null) t5 ")
            .Append(" where t1.adReqId = t2.adReqId and t1.adReqId=t5.adReqId ")
            If Not strCampGrpId = "" Then
                .Append("  and t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With
        dadNorthwind.SelectCommand = New OleDbCommand(sbReqAssignedDirectlyToPrgVersion.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "RequirementsAssignedToProgramVersion")




        '    'Set the relationship between school level requirements
        '    dstNorthwind.Relations.Add("SchoolLevelRelation", dstNorthwind.Tables("SchoolLevelRequirementSummary").Columns("ReqGrpId"), _
        '    dstNorthwind.Tables("SchoolLevelRequirementDetails").Columns("ReqGrpId"))



        With sbReqsAssignedDirectlyToLeadGroup
            .Append(" select distinct t1.adReqId,Descrip, ")
            .Append(" '00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            .Append(" (select Distinct ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as ActualScore, ")
            .Append(" (select Distinct TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as TestTaken, ")
            .Append(" (select Distinct Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as Comments, ")
            .Append(" (select Distinct OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as override, ")
            .Append(" (select Count(*) from plStudentDocs where DocumentId=t1.adReqId and StudentId = '" & StudentId & "' ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,ISNULL(t3.IsRequired,0) as Required, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.StudentId = '" & StudentId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
            .Append(" Case when t3.LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t3.LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            ''MOdified by Saraswathi Lakshmanan On Sept 21 2010
            ''For document tracking
            ''Check for the field ReqforEnrollment if it is set to 1.
            .Append(" (select Distinct adReqId,Descrip,CampGrpId from adReqs where adReqTypeId in (1,3) and ReqforEnrollment=1  and StatusId='" & strActiveGUID & "') t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append(" adReqLeadGroups t3 ")
            .Append(" where ")
            .Append(" t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            ' Requirement should not be in requirement group assigned to program version
            .Append(" t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId='" & PrgVerId & "' and s2.adReqId is null)   ")
            .Append(" and t3.LeadGrpId in (" & SelectedLeadGroups & ") and ")
            ' Requirement should not be directly assigned to program version
            .Append(" t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId='" & PrgVerId & "') ")
            If Not strCampGrpId = "" Then
                .Append("  and t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With

        dadNorthwind.SelectCommand = New OleDbCommand(sbReqAssignedDirectlyToPrgVersion.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "RequirementAssignedDirectlyToLeadGroup")


        Return dstNorthwind

    End Function
    '' New code added by kamalesh Ahuja on December 30 for Rally Issue id DE 1276
    Public Function CheckIfStudentHasPassedRequiredTestWithProgramVersion(ByVal StudentId As String, ByVal PrgVerId As String, Optional ByVal CampusId As String = "", Optional ByVal SelectedLeadGroups As String = Nothing) As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strPreviousEducationId As String
        Dim strPreviousEduTest As String
        Dim strEnrollDate As Date = Date.Now.ToShortDateString

        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        Try
            With sb
                .Append(" select OverRide,Count(*) as OverRideCount from ")
                .Append(" ( ")
                .Append(" select  distinct ")
                .Append("       adReqId, ")
                .Append("       Descrip, ")
                .Append("       ReqGrpId, ")
                .Append("       StartDate,  ")
                .Append("       EndDate, ")
                .Append("       ActualScore, ")
                .Append("       TestTaken, ")
                .Append("       Comments, ")
                .Append("       Case when OverRide>=1 then 'True' else 'False' end as OverRide, ")
                .Append("       Case when (select count(*) from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') >=1 then ")
                .Append("       (select MinScore from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') ")
                .Append("       else MinScore end as MinScore, ")
                .Append("       Required, ")
                .Append("       DocSubmittedCount, ")
                .Append("       DocStatusDescrip, ")
                .Append("       Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount,Pass,CampGrpId ")
                .Append("       from  ")
                .Append("       ( ")
                ' Get Requirement Group and requirements of mandatory requirement
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate, ")
                .Append("		      		ActualScore, ")
                .Append("		      		TestTaken, ")
                .Append("		      		Comments, ")
                .Append("		      		OverRide, ")
                .Append("		      		Minscore, ")
                .Append("		      		Required, ")
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		      		DocSubmittedCount, ")
                .Append("		      		DocStatusDescrip,CampGrpId ")
                .Append("      	from  ")
                .Append("      		( ")
                .Append("      		select Distinct ")
                .Append("      				t1.adReqId, ")
                .Append("      				t1.Descrip, ")
                .Append("      				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("      					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("      					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                .Append("      				'" & strEnrollDate & "' as CurrentDate, ")
                .Append("      				t2.StartDate, ")
                .Append("      				t2.EndDate, ")
                .Append("      				(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("      					StudentId = '" & StudentId & "' ) as ActualScore, ")
                .Append("      				(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("      					StudentId = '" & StudentId & "' ) as TestTaken, ")
                .Append("      				(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("      					StudentId = '" & StudentId & "' ) as Comments, ")
                .Append("      				       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("      				        StudentId = '" & StudentId & "' ) as override, ")
                .Append("      				(select Count(*) from plStudentDocs where DocumentId=t1.adReqId and  ")
                .Append("      					StudentId = '" & StudentId & "'  ) as DocSubmittedCount, ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                ''.Append("      				Cast(t2.MinScore as int) as MinScore, ")
                .Append("      				t2.MinScore , ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                .Append("      				1 as Required, ")
                .Append("      				(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
                .Append("      				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("      				s3.StudentId = '" & StudentId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("      				from  ")
                .Append("      				adReqs t1, ")
                .Append("      				adReqsEffectiveDates t2 ")
                .Append("      				where  ")
                .Append("      				t1.adReqId = t2.adReqId and ")
                .Append("       				t2.MandatoryRequirement=1 and ")
                ''Added by Saraswathi Lakshmanan on Sept 15 2010
                ''To filter only the documents marked as required for Enrollment
                ''For Document tracking
                .Append("                   t1.ReqforEnrollment=1 and ")
                .Append("      				t1.adReqTypeId in (1) and t1.StatusId='" & strActiveGUID & "' ")
                .Append("       				) ")
                .Append("       				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("       		union ")

                ' Get The Requirements that was assigned to a program version
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate, ")
                .Append("		      		ActualScore, ")
                .Append("		      		TestTaken, ")
                .Append("		      		Comments, ")
                .Append("		      		OverRide, ")
                .Append("		      		Minscore, ")
                .Append("		      		Required, ")
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		      		DocSubmittedCount, ")
                .Append("		      		DocStatusDescrip,CampGrpId ")
                .Append("      				from  ")
                .Append("      					( ")
                .Append("      						select Distinct ")
                .Append("      							t1.adReqId, ")
                .Append("      							t1.Descrip, ")
                .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
                .Append("	      						t2.StartDate, ")
                .Append("	      						t2.EndDate, ")
                .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	      							StudentId = '" & StudentId & "' ) as ActualScore, ")
                .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							StudentId = '" & StudentId & "' ) as TestTaken, ")
                .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							StudentId = '" & StudentId & "' ) as Comments, ")
                .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("	      						        StudentId = '" & StudentId & "' ) ")
                .Append("	      						as override, ")
                .Append("	      						(select Count(*) from plStudentDocs where DocumentId=t1.adReqId and  ")
                .Append("	      						StudentId = '" & StudentId & "'  ) as DocSubmittedCount, ")
                '.Append("		      					Cast(t2.MinScore as int) as MinScore, ")
                .Append(" Case when (select count(*) from adPrgVerTestDetails where adReqId=t1.adReqId and PrgVerId='" & PrgVerId & "') >=1 then ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                '.Append(" (select cast(Minscore as int) from adPrgVerTestDetails where adReqId=t1.adReqId and PrgVerId='" & PrgVerId & "') ")
                '.Append(" else cast(t2.Minscore as int) end as MinScore, ")
                .Append(" (select Minscore from adPrgVerTestDetails where adReqId=t1.adReqId and PrgVerId='" & PrgVerId & "') ")
                .Append(" else t2.Minscore end as MinScore, ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                .Append("	      						1 as Required, ")
                .Append("	      						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
                .Append("      							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("      							s3.StudentId = '" & StudentId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("      						from  ")
                .Append("      							adReqs t1, ")
                .Append("      							adReqsEffectiveDates t2, ")
                .Append("      							adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
                .Append("      						where ")
                .Append("      							t1.adReqId = t2.adReqId and t2.MandatoryRequirement <> 1 and ")
                ''Added by Saraswathi Lakshmanan on Sept 15 2010
                ''To filter only the documents marked as required for Enrollment
                ''For Document tracking
                .Append("                               t1.ReqforEnrollment=1 and ")
                .Append("      							t1.adreqTypeId in (1) and  t1.StatusId='" & strActiveGUID & "'")
                .Append("      							and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId ")
                .Append("      							and t1.adReqId = t5.adReqId and t5.PrgVerId = '" & PrgVerId & "'     ")
                ''Commented for DE5552
                '.Append("      					     and 		t3.LeadGrpId in (" & SelectedLeadGroups & ") ")
                .Append("      							and t5.adReqId is not null ")
                .Append("      						) ")
                .Append("      						R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                ' Get Requirements === Requirement Group assigned to Program Version
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate, ")
                .Append("		      		ActualScore, ")
                .Append("		      		TestTaken, ")
                .Append("		      		Comments, ")
                .Append("		      		OverRide, ")
                .Append("		      		Minscore, ")
                .Append("		      		Required, ")
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		      		DocSubmittedCount, ")
                .Append("		      		DocStatusDescrip,CampGrpId ")
                .Append("      				from  ")
                .Append("      					( ")
                .Append("      						select Distinct ")
                .Append("      							t1.adReqId, ")
                .Append("      							t1.Descrip, ")
                .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
                .Append("	      						t2.StartDate, ")
                .Append("	      						t2.EndDate, ")
                .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	      							StudentId = '" & StudentId & "' ) as ActualScore, ")
                .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							StudentId = '" & StudentId & "' ) as TestTaken, ")
                .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							StudentId = '" & StudentId & "' ) as Comments, ")
                .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("	      						        StudentId = '" & StudentId & "' ) ")
                .Append("	      						as override, ")
                .Append("	      						(select Count(*) from plStudentDocs where DocumentId=t1.adReqId and  ")
                .Append("	      						StudentId = '" & StudentId & "'  ) as DocSubmittedCount, ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                ''.Append("		      					Cast(t2.MinScore as int) as MinScore, ")
                .Append("		      					t2.MinScore , ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                .Append("	      						ISNULL(t3.IsRequired,0) as Required, ")
                .Append("	      						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
                .Append("      							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("      							s3.StudentId = '" & StudentId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("      						from  ")
                .Append("      							adReqs t1, ")
                .Append("							adReqsEffectiveDates t2, ")
                .Append("							adReqLeadGroups t3, ")
                .Append("							adPrgVerTestDetails t5, ")
                .Append("							adReqGrpDef t6, ")
                .Append("						adLeadByLeadGroups t7  ")
                .Append("      					where ")
                .Append("      						t1.adReqId = t2.adReqId and ")
                .Append("      						t1.adreqTypeId in (1) and t1.StatusId='" & strActiveGUID & "' ")
                ''Added by Saraswathi Lakshmanan on Sept 15 2010
                ''To filter only the documents marked as required for Enrollment
                ''For Document tracking
                .Append("                          and  t1.ReqforEnrollment=1  ")

                .Append("      						and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and ")
                .Append("						t3.LeadGrpId = t7.LeadGrpId and ")
                .Append("      						t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
                .Append("	      					and t7.StudentId = '" & StudentId & "'  and  ")
                .Append("						t5.PrgVerId = '" & PrgVerId & "'   ")
                .Append("      		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                ' Get Requirements that were assigned to lead group but not part of any requirement group
                .Append("	     select  distinct ")
                .Append("	           adReqId, ")
                .Append("	           Descrip, ")
                .Append("	           ReqGrpId, ")
                .Append("	           StartDate, ")
                .Append("	           EndDate, ")
                .Append("	           ActualScore, ")
                .Append("	           TestTaken, ")
                .Append("	           Comments, ")
                .Append("	           OverRide, ")
                .Append("	           Minscore, ")
                .Append("	           Required, ")
                .Append("	           Case  when ActualScore >= Minscore  then 'True' else 'False' End as Pass, ")
                .Append("	           DocSubmittedCount, ")
                .Append("	           DocStatusDescrip,CampGrpId ")
                .Append("           from  ")
                .Append("           ( ")
                ' Get Requirement Group and requirements that are assigned to a lead group
                ' and part of a requirement group
                .Append("          	 select  distinct ")
                .Append("	           	t1.adReqId, ")
                .Append("	           	t1.Descrip, ")
                .Append("	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append("	           	'" & strEnrollDate & "' as CurrentDate,  ")
                .Append("	           	t2.StartDate, ")
                .Append("	           	t2.EndDate,  ")
                .Append("	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		StudentId = '" & StudentId & "' ) as ActualScore,  ")
                .Append("	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		StudentId = '" & StudentId & "' ) as TestTaken,  ")
                .Append("	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		StudentId = '" & StudentId & "' ) as Comments,  ")
                .Append("	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("	                              StudentId = '" & StudentId & "' ) ")
                .Append("	                      as override, ")
                .Append("	           	(select Count(*) from plStudentDocs where DocumentId=t1.adReqId and ")
                .Append("	           		StudentId = '" & StudentId & "' ) as DocSubmittedCount, ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                ''.Append("	           	Cast(t2.MinScore as int) as MinScore, ")
                .Append("	           	t2.MinScore , ")
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                .Append("			ISNULL(t3.IsRequired,0) as Required, ")
                .Append("	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
                .Append("	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("	           s3.StudentId = '" & StudentId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("           from ")
                .Append("           	adReqs t1, ")
                .Append("           	adReqsEffectiveDates t2, ")
                .Append("		adReqLeadGroups t3 ")
                .Append("           where  ")
                .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t1.StatusId='" & strActiveGUID & "' ")
                .Append("		and ")
                ''Added by Saraswathi Lakshmanan on Sept 15 2010
                ''To filter only the documents marked as required for Enrollment
                ''For Document tracking
                .Append("                   t1.ReqforEnrollment=1 and ")

                .Append("		t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId='" & PrgVerId & "' and s2.adReqId is null) ")
                .Append("		and t3.LeadGrpId in ")
                .Append("		(" & SelectedLeadGroups & " ) and ")
                .Append("           	t1.adReqTypeId in (1) and t2.MandatoryRequirement <> 1 and ")
                .Append("		t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId='" & PrgVerId & "') ")
                .Append("           ) ")
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" ) R2 ")
                .Append(" where R2.Required = 1 and R2.Pass = 'False' ")
                .Append(" ) R3  ")
                If Not strCampGrpId = "" Then
                    .Append("  Where R3.CampGrpId in 	('")
                    .Append(strCampGrpId)
                    .Append(") ")
                End If
                .Append(" group by OverRide Having R3.OverRide='False' ")
            End With

            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            Dim studentinfo As New PlacementInfo
            Dim intLeadCount As Integer
            While dr.Read()
                If Not (dr("OverRideCount") Is DBNull.Value) Then intLeadCount = CType(dr("OverRideCount"), Integer) Else intLeadCount = 0
            End While

            If Not dr.IsClosed Then dr.Close()

            Return intLeadCount
        Catch ex As Exception
        Finally
        End Try
    End Function

    '' New code added by kamalesh Ahuja on December 30 for Rally Issue id DE 1276
    Public Function CheckAllApprovedDocumentsWithPrgVersionforStudent(ByVal StudentId As String, ByVal PrgVerId As String, Optional ByVal CampusId As String = "", Optional ByVal SelectedLeadGroups As String = Nothing) As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strPreviousEducationId As String
        Dim strPreviousEduTest As String
        Dim strEnrollDate As Date = Date.Now.ToShortDateString

        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If


        Try

            With sb
                .Append(" select Count(*) from ")
                .Append(" ( ")
                .Append(" select  distinct ")
                .Append("       adReqId as DocumentId, ")
                .Append("       Descrip as DocumentDescrip, ")
                .Append("       ReqGrpId, ")
                .Append("       StartDate,  ")
                .Append("       EndDate, ")
                .Append("       ActualScore, ")
                .Append("       TestTaken, ")
                .Append("       Comments, ")
                .Append("       Case when OverRide>=1 then 'True' else 'False' end as OverRide, ")
                .Append("       Case when (select count(*) from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') >=1 then ")
                .Append("       (select MinScore from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') ")
                .Append("       else MinScore end as MinScore, ")
                .Append("       Required, ")
                .Append("       DocSubmittedCount, ")
                .Append("       case when DocStatusDescrip='Approved' then 'Approved' else 'Not Approved' end as DocStatusDescrip, ")
                .Append("       Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount,Pass,CampGrpId ")
                .Append("       from  ")
                .Append("       ( ")
                ' Get Requirement Group and requirements of mandatory requirement
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate, ")
                .Append("		      		ActualScore, ")
                .Append("		      		TestTaken, ")
                .Append("		      		Comments, ")
                .Append("		      		OverRide, ")
                .Append("		      		Minscore, ")
                .Append("		      		Required, ")
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		      		DocSubmittedCount, ")
                .Append("		      		DocStatusDescrip,CampGrpId ")
                .Append("      	from  ")
                .Append("      		( ")
                .Append("      		select Distinct ")
                .Append("      				t1.adReqId, ")
                .Append("      				t1.Descrip, ")
                .Append("      				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("      					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("      					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                .Append("      				'" & strEnrollDate & "' as CurrentDate, ")
                .Append("      				t2.StartDate, ")
                .Append("      				t2.EndDate, ")
                .Append("      				(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("      					StudentId = '" & StudentId & "' ) as ActualScore, ")
                .Append("      				(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("      					StudentId = '" & StudentId & "' ) as TestTaken, ")
                .Append("      				(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("      					StudentId = '" & StudentId & "' ) as Comments, ")
                .Append("      				       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("      				        StudentId = '" & StudentId & "' ) as override, ")
                .Append("      				(select Count(*) from plStudentDocs where DocumentId=t1.adReqId and  ")
                .Append("      					StudentId = '" & StudentId & "'  ) as DocSubmittedCount, ")
                .Append("      				t2.Minscore, ")
                .Append("      				1 as Required, ")
                .Append("      				(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
                .Append("      				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("      				s3.StudentId = '" & StudentId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("      				from  ")
                .Append("      				adReqs t1, ")
                .Append("      				adReqsEffectiveDates t2 ")
                .Append("      				where  ")
                .Append("      				t1.adReqId = t2.adReqId and ")
                .Append("       				t2.MandatoryRequirement=1 and ")
                ''Added by Saraswathi Lakshmanan on Sept 15 2010
                ''To filter only the documents marked as required for Enrollment
                ''For Document tracking
                .Append("                   t1.ReqforEnrollment=1 and ")
                .Append("      				t1.adReqTypeId in (3) and t1.StatusId ='" & strActiveGUID & "' ")
                .Append("       				) ")
                .Append("       				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("       		union ")

                ' Get The Requirements that was assigned to a program version
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate, ")
                .Append("		      		ActualScore, ")
                .Append("		      		TestTaken, ")
                .Append("		      		Comments, ")
                .Append("		      		OverRide, ")
                .Append("		      		Minscore, ")
                .Append("		      		Required, ")
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		      		DocSubmittedCount, ")
                .Append("		      		DocStatusDescrip,CampGrpId ")
                .Append("      				from  ")
                .Append("      					( ")
                .Append("      						select Distinct ")
                .Append("      							t1.adReqId, ")
                .Append("      							t1.Descrip, ")
                .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
                .Append("	      						t2.StartDate, ")
                .Append("	      						t2.EndDate, ")
                .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	      							StudentId = '" & StudentId & "' ) as ActualScore, ")
                .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							StudentId = '" & StudentId & "' ) as TestTaken, ")
                .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							StudentId = '" & StudentId & "' ) as Comments, ")
                .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("	      						        StudentId = '" & StudentId & "' ) ")
                .Append("	      						as override, ")
                .Append("	      						(select Count(*) from plStudentDocs where DocumentId=t1.adReqId and  ")
                .Append("	      						StudentId = '" & StudentId & "'  ) as DocSubmittedCount, ")
                .Append("		      					t2.Minscore, ")
                .Append("	      						1 as Required, ")
                .Append("	      						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
                .Append("      							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("      							s3.StudentId = '" & StudentId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("      						from  ")
                .Append("      							adReqs t1, ")
                .Append("      							adReqsEffectiveDates t2, ")
                .Append("      							adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
                .Append("      						where ")
                .Append("      							t1.adReqId = t2.adReqId and t2.MandatoryRequirement <> 1 and ")
                ''Added by Saraswathi Lakshmanan on Sept 15 2010
                ''To filter only the documents marked as required for Enrollment
                ''For Document tracking
                .Append("                   t1.ReqforEnrollment=1 and ")
                .Append("      							t1.adreqTypeId in (3) and t1.StatusId ='" & strActiveGUID & "' and ")
                .Append("      							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId ")
                .Append("      							and t1.adReqId = t5.adReqId and t5.PrgVerId = '" & PrgVerId & "'    ")
                '.Append("      					      	 and	t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where ")
                '.Append("	      						LeadId = '" & LeadId & "' ) ")
                .Append("      							and t5.adReqId is not null ")
                .Append("      						) ")
                .Append("      						R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                ' Get Requirements === Requirement Group assigned to Program Version
                .Append("       	select  ")
                .Append("			distinct ")
                .Append("		       	adReqId, ")
                .Append("		      		Descrip, ")
                .Append("		      		ReqGrpId, ")
                .Append("		      		StartDate, ")
                .Append("		      		EndDate, ")
                .Append("		      		ActualScore, ")
                .Append("		      		TestTaken, ")
                .Append("		      		Comments, ")
                .Append("		      		OverRide, ")
                .Append("		      		Minscore, ")
                .Append("		      		Required, ")
                .Append("		      		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		      		DocSubmittedCount, ")
                .Append("		      		DocStatusDescrip,CampGrpId ")
                .Append("      				from  ")
                .Append("      					( ")
                .Append("      						select Distinct ")
                .Append("      							t1.adReqId, ")
                .Append("      							t1.Descrip, ")
                .Append("	      						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("	      						'" & strEnrollDate & "' as CurrentDate, ")
                .Append("	      						t2.StartDate, ")
                .Append("	      						t2.EndDate, ")
                .Append("	      						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	      							StudentId = '" & StudentId & "' ) as ActualScore, ")
                .Append("	      						(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							StudentId = '" & StudentId & "' ) as TestTaken, ")
                .Append("      						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("	      							StudentId = '" & StudentId & "' ) as Comments, ")
                .Append("	      					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("	      						        StudentId = '" & StudentId & "' ) ")
                .Append("	      						as override, ")
                .Append("	      						(select Count(*) from plStudentDocs where DocumentId=t1.adReqId and  ")
                .Append("	      						StudentId = '" & StudentId & "'  ) as DocSubmittedCount, ")
                .Append("		      					t2.Minscore, ")
                .Append("	      						ISNULL(t3.IsRequired,0) as Required, ")
                .Append("	      						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
                .Append("      							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("      							s3.StudentId = '" & StudentId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("      						from  ")
                .Append("      							adReqs t1, ")
                .Append("							adReqsEffectiveDates t2, ")
                .Append("							adReqLeadGroups t3, ")
                .Append("							adPrgVerTestDetails t5, ")
                .Append("							adReqGrpDef t6, ")
                .Append("						adLeadByLeadGroups t7  ")
                .Append("      					where ")
                .Append("      						t1.adReqId = t2.adReqId and ")
                ''Added by Saraswathi Lakshmanan on Sept 15 2010
                ''To filter only the documents marked as required for Enrollment
                ''For Document tracking
                .Append("                   t1.ReqforEnrollment=1 and ")
                .Append("      						t1.adreqTypeId in (3) and t1.StatusId ='" & strActiveGUID & "' and ")
                .Append("      						t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and ")
                .Append("						t3.LeadGrpId = t7.LeadGrpId and ")
                .Append("      						t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
                .Append("	      					and t7.StudentId = '" & StudentId & "'  and  ")
                .Append("						t5.PrgVerId = '" & PrgVerId & "'   ")
                .Append("      		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                ' Get Requirements that were assigned to lead group but not part of any requirement group
                .Append("	     select  distinct ")
                .Append("	           adReqId, ")
                .Append("	           Descrip, ")
                .Append("	           ReqGrpId, ")
                .Append("	           StartDate, ")
                .Append("	           EndDate, ")
                .Append("	           ActualScore, ")
                .Append("	           TestTaken, ")
                .Append("	           Comments, ")
                .Append("	           OverRide, ")
                .Append("	           Minscore, ")
                .Append("	           Required, ")
                .Append("	           Case  when ActualScore >= Minscore  then 'True' else 'False' End as Pass, ")
                .Append("	           DocSubmittedCount, ")
                .Append("	           DocStatusDescrip,CampGrpId ")
                .Append("           from  ")
                .Append("           ( ")
                ' Get Requirement Group and requirements that are assigned to a lead group
                ' and part of a requirement group
                .Append("          	 select  distinct ")
                .Append("	           	t1.adReqId, ")
                .Append("	           	t1.Descrip, ")
                .Append("	         	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append("	           	'" & strEnrollDate & " ' as CurrentDate,  ")
                .Append("	           	t2.StartDate, ")
                .Append("	           	t2.EndDate,  ")
                .Append("	           	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		StudentId = '" & StudentId & "' ) as ActualScore,  ")
                .Append("	           	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		StudentId = '" & StudentId & "' ) as TestTaken,  ")
                .Append("	           	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("	           		StudentId = '" & StudentId & "' ) as Comments,  ")
                .Append("	               (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("	                              StudentId = '" & StudentId & "' ) ")
                .Append("	                      as override, ")
                .Append("	           	(select Count(*) from plStudentDocs where DocumentId=t1.adReqId and ")
                .Append("	           		StudentId = '" & StudentId & "' ) as DocSubmittedCount, ")
                .Append("	           	t2.Minscore, ")
                .Append("			ISNULL(t3.IsRequired,0) as Required, ")
                .Append("	           (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
                .Append("	           where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("	           s3.StudentId = '" & StudentId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t1.CampGrpId ")
                .Append("           from ")
                .Append("           	adReqs t1, ")
                .Append("           	adReqsEffectiveDates t2, ")
                .Append("		adReqLeadGroups t3 ")
                .Append("           where  ")
                .Append("           	t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId ")
                ''Added by Saraswathi Lakshmanan on Sept 15 2010
                ''To filter only the documents marked as required for Enrollment
                ''For Document tracking
                .Append("            and       t1.ReqforEnrollment=1  ")
                .Append("		and  t1.StatusId ='" & strActiveGUID & "'")
                .Append("		and t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId='" & PrgVerId & "' and s2.adReqId is null) ")
                .Append("		and t3.LeadGrpId in ")
                .Append("		(" & SelectedLeadGroups & " ) and ")
                .Append("           	t1.adReqTypeId in (3) and t2.MandatoryRequirement <> 1 and ")
                .Append("		t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId='" & PrgVerId & "') ")
                .Append("           ) ")
                .Append("           R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" ) R2 ")
                .Append(" ) R3 where R3.Required =1  and  R3.DocStatusDescrip = 'Not Approved' and R3.OverRide='False' ")
                If Not strCampGrpId = "" Then
                    .Append("  AND R3.CampGrpId in 	('")
                    .Append(strCampGrpId)
                    .Append(") ")
                End If
            End With

            Dim intLeadCount As Integer = db.RunParamSQLScalar(sb.ToString)
            Return intLeadCount
        Catch ex As Exception
        Finally
        End Try
    End Function
    '' New Function added by kamalesh Ahuja on December 30 for Rally Issue id DE 1276
    Public Function CheckIfReqGroupMeetsConditionsForStudent(ByVal StudentId As String, ByVal PrgVerId As String, Optional ByVal CampusId As String = "", Optional ByVal SelectedLeadGroups As String = Nothing) As String
        Dim sb As New StringBuilder
        Dim strEnrollDate As Date = Date.Now.ToShortDateString
        Dim dsGetCmpGrps As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        With sb
            .Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,R5.TestAttempted+R5.DocsAttempted+R5.TestOverRidden+R5.DocumentOverRidden as AttemptedReqs ")
            .Append(" from ")
            .Append(" (  ")
            ' Requiremt group assigned to a program version
            .Append("	Select  ")
            .Append("		Distinct ")
            .Append("			t1.ReqGrpId, ")
            .Append("			t2.Descrip,t2.CampGrpId, ")
            .Append("			t3.NumReqs as Numreqs,  ")
            .Append("	            	    ( ")
            .Append("       	     		select Count(*) as TestAttempted ")
            .Append("            				from ")
            .Append("			            		( ")
            .Append("            						select Distinct ")
            .Append("								A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("       	     						adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append("            						where ")
            .Append("				            			A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and ")
            ''Added by Saraswathi Lakshmanan on Sept 15 2010
            ''To filter only the documents marked as required for Enrollment
            ''For Document tracking
            .Append("                  A2.ReqforEnrollment=1 and ")

            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("								and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append("       		     					A4.LeadGrpId in (" & SelectedLeadGroups & ")  and ")
            .Append("		            					ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (1) and A2.StatusId='" & strActiveGUID & "' ")
            .Append("	       			     	) ")
            .Append("					R1,adLeadEntranceTest R2 ")
            .Append("				where ")
            .Append("					R1.adReqId = R2.EntrTestId and R2.StudentId='" & StudentId & "'")
            .Append("			            	and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 ")
            .Append("                       and R2.Pass=1 and R2.EntrTestId not in (select Distinct EntrTestId from adEntrTestOverRide where StudentId='" & StudentId & "' and OverRide=1) and ")
            .Append("					(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("	      	        )as TestAttempted, ")
            .Append("	               ( ")
            .Append("		            	select Count(*) as DocsAttempted ")
            .Append("       			     	from ")
            .Append("			            		( ")
            .Append("			            			select Distinct ")
            .Append("								A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("            							adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append("			            			where ")
            .Append("	            						A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            ''Added by Saraswathi Lakshmanan on Sept 15 2010
            ''To filter only the documents marked as required for Enrollment
            ''For Document tracking
            .Append("                   A2.ReqforEnrollment=1  and ")

            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("			       		     		and A4.LeadGrpId in (" & SelectedLeadGroups & ")  and ")
            .Append("            							ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (3) and A2.StatusId='" & strActiveGUID & "' ")
            .Append("			            		) ")
            .Append("            				R1,plStudentDocs R2,syDocStatuses R3 ")
            .Append("				where ")
            .Append("					    R1.adReqId = R2.DocumentId and R2.StudentId='" & StudentId & "'")
            .Append("                       and R2.DocumentId not in (select Distinct EntrTestId from adEntrTestOverRide where StudentId='" & StudentId & "' and OverRide=1) ")
            .Append("                       and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 ")
            .Append("		            	and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("            		) as DocsAttempted,  ")
            .Append(" (  ")
            .Append("     select Count(*) as OverRideTestAttempted ")
            .Append(" from ")
            .Append(" (  ")
            .Append(" select Distinct ")
            .Append("	A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" from ")
            .Append(" 	adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where ")
            .Append(" A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and ")
            ''Added by Saraswathi Lakshmanan on Sept 15 2010
            ''To filter only the documents marked as required for Enrollment
            ''For Document tracking
            .Append("                   A2.ReqforEnrollment=1  and ")
            .Append(" A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" and A4.LeadGrpId in (" & SelectedLeadGroups & ")  and ")
            .Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (1) and A2.StatusId='" & strActiveGUID & "' ")
            .Append(" )  ")
            .Append(" R1,adEntrTestOverRide R2 where ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.OverRide=1 and ")
            .Append(" R2.StudentId='" & StudentId & "' and R1.CurrentDate >= R1.StartDate and ")
            .Append(" (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)  ")
            '.Append(" and R1.adReqId not in (select Distinct EntrTestId from adLeadEntranceTest where LeadId='" & LeadId & "') ")
            .Append(" ) as TestOverRidden, ")
            .Append(" (  ")
            .Append("     select Count(*) as OverRideDocumentAttempted ")
            .Append(" from ")
            .Append(" (  ")
            .Append(" select Distinct ")
            .Append("	A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" from ")
            .Append(" 	adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where ")
            .Append(" A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and ")
            ''Added by Saraswathi Lakshmanan on Sept 15 2010
            ''To filter only the documents marked as required for Enrollment
            ''For Document tracking
            .Append("                   A2.ReqforEnrollment=1  and ")
            .Append(" A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" and A4.LeadGrpId in (" & SelectedLeadGroups & ")  and ")
            .Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (3) and A2.StatusId='" & strActiveGUID & "' ")
            .Append(" )  ")
            .Append(" R1,adEntrTestOverRide R2 where ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.OverRide=1 and ")
            .Append(" R2.StudentId='" & StudentId & "' and R1.CurrentDate >= R1.StartDate and ")
            .Append(" (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)  ")
            '.Append(" and R1.adReqId not in (select Distinct DocumentId from adLeadDocsReceived where LeadId='" & LeadId & "') ")
            .Append(" ) as DocumentOverRidden ")
            .Append("      	from ")
            .Append("			adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3  ")
            .Append("       where  ")
            .Append(" 	              t1.ReqGrpId = t2.ReqGrpId and ")
            .Append("			t2.ReqGrpId = t3.ReqGrpId and t2.ReqGrpId not in (select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) and ")
            .Append("			t1.PrgVerId='" & PrgVerId & "' and ")
            .Append("                     t3.LeadGrpId in (" & SelectedLeadGroups & ") ")
            .Append(" union ")
            ' Get Mandatory requirement group
            .Append("      select ")
            .Append("              	'CFD29DFD-4FFF-400B-B152-4FABB12E6E7B' as ReqGrpId, ")
            .Append("               case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then (select Descrip from adReqGroups where IsMandatoryReqGrp=1) else 'School Level Requirements' end  as Descrip, ")
            .Append("               t1.CampGrpId, ")
            .Append("             			( ")
            .Append("              			select Count(*) as NumReqs ")
            .Append("              			from ")
            .Append("      			 			( ")
            .Append("              					select Distinct ")
            .Append("								A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("              						adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            .Append("					              	 A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1 and ")
            ''Added by Saraswathi Lakshmanan on Sept 15 2010
            ''To filter only the documents marked as required for Enrollment
            ''For Document tracking
            .Append("                   A2.ReqforEnrollment=1  and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("					                and A2.adreqTypeId in (1,3) and A2.StatusId='" & strActiveGUID & "' ")
            .Append("              				) ")
            .Append("              				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("                			) ")
            .Append("              		as NumReqs, ")
            .Append("              		( ")
            .Append("              			select Count(*) as TestAttempted ")
            .Append("              			from ")
            .Append("              				( ")
            .Append("              					select Distinct ")
            .Append("								A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("							from ")
            .Append("						              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            .Append("					              	 A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1 and ")
            ''Added by Saraswathi Lakshmanan on Sept 15 2010
            ''To filter only the documents marked as required for Enrollment
            ''For Document tracking
            .Append("                   A2.ReqforEnrollment=1  and ")

            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("					                and A2.adreqTypeId in (1) and A2.StatusId='" & strActiveGUID & "' ")
            .Append("				              ) ")
            .Append("			              R1,adLeadEntranceTest R2 ")
            .Append("					where ")
            .Append("						R1.adReqId = R2.EntrTestId and ")
            .Append("						R2.StudentId='" & StudentId & "'")
            .Append("	              		and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 ")
            .Append("                       and R2.Pass=1 and R2.EntrTestId not in (select Distinct EntrTestId from adEntrTestOverRide where StudentId='" & StudentId & "' and OverRide=1) and ")
            .Append("						(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("              		) as TestAttempted, ")
            .Append("             			( ")
            .Append(" 			              select Count(*) as DocsAttempted ")
            .Append(" 			              from ")
            .Append(" 				           ( ")
            .Append(" 				              select Distinct ")
            .Append(" 							A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" 						from  ")
            .Append(" 					              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            .Append("					              	 A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1 and ")
            ''Added by Saraswathi Lakshmanan on Sept 15 2010
            ''To filter only the documents marked as required for Enrollment
            ''For Document tracking
            .Append("                   A2.ReqforEnrollment=1  and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("					                and A2.adreqTypeId in (3) and A2.StatusId='" & strActiveGUID & "' ")
            .Append(" 					      ) ")
            .Append(" 		              	R1,plStudentDocs R2,syDocStatuses R3 ")
            .Append(" 					where 	")
            .Append(" 						R1.adReqId = R2.DocumentId and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 and ")
            .Append(" 						R2.StudentId='" & StudentId & "' ")
            .Append("                       and R2.DocumentId not in (select Distinct EntrTestId from adEntrTestOverRide where StudentId='" & StudentId & "' and OverRide=1)  ")

            .Append(" 				              and R1.CurrentDate >= R1.StartDate and ")
            .Append(" 						(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" 		              ) as DocsAttempted, ")
            .Append(" (  ")
            .Append("     select Count(*) as OverRideTestAttempted ")
            .Append(" from ")
            .Append(" (  ")
            .Append(" 				              select Distinct ")
            .Append(" 							A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" 						from  ")
            .Append(" 					              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            .Append("					              	 A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1 and ")
            ''Added by Saraswathi Lakshmanan on Sept 15 2010
            ''To filter only the documents marked as required for Enrollment
            ''For Document tracking
            .Append("                   A2.ReqforEnrollment=1  and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("					                and A2.adreqTypeId in (1) and A2.StatusId='" & strActiveGUID & "' ")
            .Append(" )  ")
            .Append(" R1,adEntrTestOverRide R2 where ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.OverRide=1 and ")
            .Append(" R2.StudentId='" & StudentId & "' and R1.CurrentDate >= R1.StartDate and ")
            .Append(" (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)  ")
            '.Append(" and R1.adReqId not in (select EntrTestId from adLeadEntranceTest where LeadId='" & LeadId & "') ")
            .Append(" ) as TestOverRidden, ")
            .Append(" (  ")
            .Append("     select Count(*) as OverRideDocumentAttempted ")
            .Append(" from ")
            .Append(" (  ")
            .Append(" 				              select Distinct ")
            .Append(" 							A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate ")
            .Append(" 						from  ")
            .Append(" 					              adReqs A2,adReqsEffectiveDates A3 ")
            .Append("              					where ")
            .Append("					              	 A2.adReqId = A3.adReqId  ")
            .Append("								    and A3.MandatoryRequirement=1  and ")
            ''Added by Saraswathi Lakshmanan on Sept 15 2010
            ''To filter only the documents marked as required for Enrollment
            ''For Document tracking
            .Append("                   A2.ReqforEnrollment=1  and ")
            .Append(" A2.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("					                and A2.adreqTypeId in (3) and A2.StatusId='" & strActiveGUID & "' ")
            .Append(" )  ")
            .Append(" R1,adEntrTestOverRide R2 where ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.OverRide=1 and ")
            .Append(" R2.StudentId='" & StudentId & "' and R1.CurrentDate >= R1.StartDate and ")
            .Append(" (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)  ")
            '.Append(" and R1.adReqId not in (select Distinct DocumentId from adLeadDocsReceived where LeadId='" & LeadId & "') ")
            .Append(" ) as DocumentOverRidden ")
            .Append("       from ")
            .Append(" 		     adReqs t1,adReqsEffectiveDates t2  ")
            .Append(" 	where t1.adReqId = t2.adReqId and t2.MandatoryRequirement=1 and t1.adreqTypeId in (1,3) ")
            ''Added by Saraswathi Lakshmanan on Sept 15 2010
            ''To filter only the documents marked as required for Enrollment
            ''For Document tracking
            .Append("          and         t1.ReqforEnrollment=1   ")

            .Append("    ) R5  ")
            If Not strCampGrpId = "" Then
                .Append("   WHERE R5.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With

        '' Grab the Categories and Products table
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim dr As DataRow

        ds = db.RunParamSQLDataSet(sb.ToString)

        Dim strMessage As String
        Dim intRequiredReqs As Integer
        Dim intAttemptedReqs As Integer

        For Each dr In ds.Tables(0).Rows
            intRequiredReqs = dr("NumReqs")
            intAttemptedReqs = dr("AttemptedReqs")
            If intAttemptedReqs < intRequiredReqs Then
                strMessage = "Not Satisfied"
                Return "Not Enroll"
                Exit Function
            End If
            Dim sbCheckIfRequiredRequirementExistsWithInGroup As New StringBuilder
            Dim intReqExistsCount As Integer = 0
            With sbCheckIfRequiredRequirementExistsWithInGroup
                'If count is 0 then all the required requirements with in a group are satisfied.
                .Append(" select Count(*) from adReqGrpDef where ReqGrpId=? ")
                .Append(" and LeadGrpId in (" & SelectedLeadGroups & ") ")
                .Append(" and IsRequired=1 and  ")
                .Append(" ( ")
                'Test should have been failed
                .Append(" adReqId in (select distinct EntrTestId from adLeadEntranceTest where StudentId=? ")
                .Append(" and Pass=0 and EntrTestId not in (select Distinct EntrTestId from adEntrTestOverride where ")
                .Append(" StudentId=? and override=1)) ")
                .Append(" or  ")
                'or document should be not approved
                .Append(" adReqId in (select distinct DocumentId from plStudentDocs t1,syDocStatuses t2 where ")
                .Append(" t1.StudentId=? and t1.DocStatusId = t2.DocStatusId and ")
                .Append(" t2.SysDocStatusId <> 1 and DocumentId not in ")
                .Append(" (select EntrTestId from adEntrTestOverride where StudentId=? ")
                .Append(" and override=1)) ")
                .Append(" or ")
                ' or requirement should not be overriden
                .Append(" adReqId in (select Distinct EntrTestId from adEntrTestOverride where StudentId=? ")
                .Append(" and override=0) ")
                .Append(" ) ")
            End With
            db.ClearParameters()
            db.AddParameter("@ReqGrpId", CType(dr("ReqGrpId"), Guid).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            ''db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                intReqExistsCount = db.RunParamSQLScalar(sbCheckIfRequiredRequirementExistsWithInGroup.ToString)
                'If count is >=1 then there is a requirement with in this group that has been marked as required 
                'but has not been satisfied or overriden
                If intReqExistsCount >= 1 Then
                    Return "Not Enroll"
                    Exit Function
                End If
            Catch ex As Exception
                strMessage = ""
            End Try
        Next
        If Not strMessage = "" Then
            Return "Not Enroll"
        Else
            Return "Enroll"
        End If
    End Function

    '' New code added by kamalesh Ahuja on December 30 for Rally Issue id DE 1276
    Public Function GetRequirementsByLeadGroupForStudent(ByVal StudentId As String, ByVal PrgVerId As String, ByVal EnrollDate As Date, ByVal LeadGrpId As String, ByVal CampusId As String, ByVal SelectedLeadGroups As String) As DataSet
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim drowRootParent As DataRow
        Dim drowSchoolLevelSummary As DataRow
        Dim drowSchoolLevelDetails As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb, sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
        Dim drowRequirementAssignedDirectlyToPrgVersion, drowRequirementAssignedDirectlyToLeadGroup As DataRow
        Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder

        Dim dsGetEntranceTestId As New DataSet
        Dim dsGetLeadGrpId As New DataSet
        Dim dstGetRequirementsAssignedToProgramVersion As New DataSet
        Dim dstGetRequirementsDirectlyAssigned As New DataSet
        Dim intRowCounter As Integer
        Dim strCurrentDate As String = Date.Now.ToShortDateString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        'Query gets the requirement group summary assigned to LeadGroup and Program Version
        With sb
            'TestDocumentAttempted removed from the count query by Balaji on 4/23/2007
            '.Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,IsNull(R5.TestPassed,0)+IsNULL(R5.DocsApproved,0)+IsNULL(R5.TestDocumentAttempted,0)+IsNULL(R5.TestOverriden,0)+IsNULL(R5.DocumentOverriden,0) as AttemptedReqs,  ")
            .Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,IsNull(R5.TestPassed,0)+IsNULL(R5.DocsApproved,0)+IsNULL(R5.TestOverriden,0)+IsNULL(R5.DocumentOverriden,0) as AttemptedReqs,  ")
            .Append(" Case when LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            '*********************************************************************************************************************************************
            ' This Query gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted by Lead and Program Version 
            '*********************************************************************************************************************************************
            .Append(" (Select  Distinct t1.ReqGrpId,t2.Descrip,t2.CampGrpId,t3.NumReqs as Numreqs,  ")
            ''''''''''''' This Query gives the number of test passed by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            ''Modified by Saraswathi Lakhsmanan on Sept 21 2010 For Document tracking
            .Append(" where A1.adReqId = A2.adReqId and A2.ReqforEnrollment=1  and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and  ")
            .Append(" A4.LeadGrpId in (select Distinct LeadGrpId from adLeadGroups where LeadGrpId in (" & SelectedLeadGroups & "))  and ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=1) ")
            .Append(" R1,adLeadEntranceTest R2 WHERE ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.StudentId='" & StudentId & "' and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and R2.Pass=1 and ")
            .Append(" R2.EntrTestId not in (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and StudentId='" & StudentId & "')  and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) and LeadGrpId='" & LeadGrpId & "') as TestPassed, ")
            '''''''''''' This Query gives the number of overriden test
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            ''Modified by Saraswathi Lakhsmanan on Sept 21 2010 For Document tracking
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where A1.adReqId = A2.adReqId and A2.ReqforEnrollment=1 and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and  ")
            .Append(" A4.LeadGrpId in (select Distinct LeadGrpId from adLeadGroups where LeadGrpId in (" & SelectedLeadGroups & "))  and ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=1) ")
            .Append(" R1,adEntrTestOverride R2 WHERE ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.StudentId='" & StudentId & "' and R2.Override=1 and R1.CurrentDate >= R1.StartDate   ")
            .Append(" and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) and LeadGrpId='" & LeadGrpId & "') as TestOverriden, ")
            '''''''''''' This Query gives the number of overriden documents
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            ''Modified BY Saraswathi Lakshmanan on Sept 21 2010
            .Append(" where A1.adReqId = A2.adReqId and A2.ReqforEnrollment=1  and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and  ")
            .Append(" A4.LeadGrpId in (select Distinct LeadGrpId from adLeadGroups where LeadGrpId in (" & SelectedLeadGroups & "))  and ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=3) ")
            .Append(" R1,adEntrTestOverride R2 WHERE ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.StudentId='" & StudentId & "' and R2.Override=1 and R1.CurrentDate >= R1.StartDate   ")
            .Append(" and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) and LeadGrpId='" & LeadGrpId & "') as DocumentOverriden, ")
            ''''''''''''' This Query gives the number of documents approved by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from  ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where A1.adReqId = A2.adReqId and A2.ReqforEnrollment=1 and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append(" A4.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where StudentId='" & StudentId & "')  and ")
            .Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=3) R1,plStudentDocs R2,syDocStatuses R3 WHERE ")
            .Append(" R1.adReqId = R2.DocumentId and R2.StudentId='" & StudentId & "'  and R2.DocumentId not in (select Distinct EntrTestId from adEntrTestOverRide where OverRide=1 and StudentId='" & StudentId & "') ")
            .Append(" and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) and LeadGrpId='" & LeadGrpId & "') as DocsApproved, ")
            '''''''''''' This Query gives the number of test and documents attempted(no matter pass or fail,no matter approved/not approved) grouped by lead group ''''''''
            .Append(" (select Count(*)  from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            ''MOdified by Saraswathi lakshmanan on Sepot 21 2010
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where A1.adReqId = A2.adReqId and A2.ReqforEnrollment=1  and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append(" A4.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where StudentId='" & StudentId & "')  and ")
            .Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (1,3)) R1,adEntrTestOverRide R2 ")
            .Append(" where R1.adReqId = R2.EntrTestId and R2.StudentId='" & StudentId & "'  and R1.CurrentDate >= R1.StartDate and R2.OverRide=1 and ")
            .Append(" (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) Group BY LeadGrpId) as TestDocumentAttempted,t4.LeadGrpId ")
            .Append(" from adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3,adLeadGroups t4  where ")
            .Append(" t1.ReqGrpId = t2.ReqGrpId and t2.ReqGrpId = t3.ReqGrpId and t3.LeadGrpId = t4.LeadGrpId and ")
            .Append(" t1.PrgVerId='" & PrgVerId & "'  and t2.IsMandatoryReqGrp <> 1 and ")
            .Append(" t3.LeadGrpId in (select Distinct LeadGrpId from adLeadGroups where LeadGrpId in (" & SelectedLeadGroups & ")) ")
            .Append(" ) R5 where LeadGrpId='" & LeadGrpId & "' ")
            If Not strCampGrpId = "" Then
                .Append("  and R5.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            '**************************************************************************************************************************************************************
            'End of the  Query that gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted by Lead and Program Version
            '**************************************************************************************************************************************************************
        End With
        dadNorthwind = New OleDbDataAdapter(sb.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "RequirementGroup")
        Return dstNorthwind
    End Function

    '' New code added by kamalesh Ahuja on December 30 for Rally Issue id DE 1276
    Public Function GetRequirementsByLeadGroupAndReqGroupForStudent(ByVal StudentId As String, ByVal PrgVerId As String, ByVal EnrollDate As Date, ByVal LeadGrpId As String, ByVal ReqGrpId As String, ByVal CampusId As String, ByVal SelectedLeadGroups As String) As DataSet
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim drowRootParent As DataRow
        Dim drowSchoolLevelSummary As DataRow
        Dim drowSchoolLevelDetails As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb, sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
        Dim drowRequirementAssignedDirectlyToPrgVersion, drowRequirementAssignedDirectlyToLeadGroup As DataRow
        Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder

        Dim dsGetEntranceTestId As New DataSet
        Dim dsGetLeadGrpId As New DataSet
        Dim dstGetRequirementsAssignedToProgramVersion As New DataSet
        Dim dstGetRequirementsDirectlyAssigned As New DataSet
        Dim intRowCounter As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim dsGetCmpGrps As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If


        'Query gets the requirements assigned to LeadGroup and Program Version
        With sb1
            .Append(" select distinct t1.adReqId,Descrip,adReqTypeId, ")
            .Append(" t6.ReqGrpId as ReqGrpId, ")
            .Append(" (select Distinct ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as ActualScore, ")
            .Append(" (select Distinct TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as TestTaken, ")
            .Append(" (select Distinct Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as Comments, ")
            .Append(" (select Distinct OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as override, ")
            .Append(" (select Count(*) from plStudentDocs where DocumentId=t1.adReqId and StudentId = '" & StudentId & "' ) as DocSubmittedCount, ")
            .Append(" t2.Minscore, ")
            .Append(" Case when t3.IsRequired=1 then t3.IsRequired else IsNULL(t6.IsRequired,0) end as Required, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.StudentId = '" & StudentId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
            .Append(" Case when t3.LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t3.LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            ''Modified by Saraswathi Lakshmanan on Sept 21 2010
            ''For Document Tracking
            .Append(" (select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and ReqforEnrollment=1  and StatusId='" & strActiveGUID & "') t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")

            ''New Code Added By Vijay Ramteke On January 31, 2011 For Mantis Id DE4958
            ''.Append(" (select Distinct LeadGrpId,adReqEffectiveDateId,IsRequired from adReqLeadGroups where LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where StudentId='" & StudentId & "' )) t3, ")
            .Append(" (select Distinct LeadGrpId,adReqEffectiveDateId,ISNULL(IsRequired,0) as IsRequired from adReqLeadGroups where LeadGrpId in (" & SelectedLeadGroups & ")) t3, ")
            ''New Code Added By Vijay Ramteke On January 31, 2011 For Mantis Id DE4958

            .Append(" (select Distinct ReqGrpId from adPrgVerTestDetails  where PrgVerId = '" & PrgVerId & "' and ReqGrpId='" & ReqGrpId & "' and ReqGrpId is not null) t5, ")
            .Append(" (select Distinct adReqId,ReqGrpId,LeadGrpId,ISNULL(IsRequired,0) as IsRequired from adReqGrpDef where ReqGrpId='" & ReqGrpId & "') t6, ")
            .Append(" (select Distinct LeadGrpId from adLeadGroups where LeadGrpId in (" & SelectedLeadGroups & ") and LeadGrpId='" & LeadGrpId & "') t7  ")
            .Append(" where t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            .Append(" t5.ReqGrpId = t6.ReqGrpId And t3.LeadGrpId = t7.LeadGrpId And t6.LeadGrpId = t7.LeadGrpId And t1.adReqId = t6.adReqId ")
            If Not strCampGrpId = "" Then
                .Append("  AND t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With
        dadNorthwind = New OleDbDataAdapter(sb1.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "Requirements")
        Return dstNorthwind
    End Function
    '' New code added by kamalesh Ahuja on December 30 for Rally Issue id DE 1276
    Public Function GetRequirementsAssignedToLeadReqGroupForStudent(ByVal StudentId As String, ByVal PrgVerId As String, ByVal EnrollDate As Date, ByVal LeadGrpId As String, ByVal CampusId As String, ByVal SelectedLeadGroups As String) As DataSet
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim drowRootParent As DataRow
        Dim drowSchoolLevelSummary As DataRow
        Dim drowSchoolLevelDetails As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb, sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
        Dim drowRequirementAssignedDirectlyToPrgVersion, drowRequirementAssignedDirectlyToLeadGroup As DataRow
        Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder

        Dim dsGetEntranceTestId As New DataSet
        Dim dsGetLeadGrpId As New DataSet
        Dim dstGetRequirementsAssignedToProgramVersion As New DataSet
        Dim dstGetRequirementsDirectlyAssigned As New DataSet
        Dim intRowCounter As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If


        'Query gets the requirements assigned to LeadGroup and Program Version
        With sbReqsAssignedDirectlyToLeadGroup
            .Append(" select distinct t1.adReqId,Descrip,adReqTypeId, ")
            .Append(" '00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            .Append(" (select Distinct ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as ActualScore, ")
            .Append(" (select Distinct TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as TestTaken, ")
            .Append(" (select Distinct Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as Comments, ")
            .Append(" (select Distinct OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as override, ")
            .Append(" (select Count(*) from plStudentDocs where DocumentId=t1.adReqId and StudentId = '" & StudentId & "' ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,ISNULL(t3.IsRequired,0) as Required, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.StudentId = '" & StudentId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
            .Append(" Case when t3.LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t3.LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            ''Modified by Saraswathi Lakshmanan on Sept 21 2010
            ''For document tracking
            .Append(" (select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and reqforEnrollment =1 and StatusId='" & strActiveGUID & "') t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append(" adReqLeadGroups t3 ")
            .Append(" where ")
            .Append(" t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            ' Requirement should not be in requirement group assigned to program version
            .Append(" t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId='" & PrgVerId & "' and s2.adReqId is null)   ")
            .Append(" and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadGroups where LeadGrpId in (" & SelectedLeadGroups & ")) and t3.LeadGrpId='" & LeadGrpId & "' ")
            ' Requirement should not be directly assigned to program version
            .Append(" and t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId='" & PrgVerId & "') ")
            If Not strCampGrpId = "" Then
                .Append("  and t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With

        dadNorthwind = New OleDbDataAdapter(sbReqsAssignedDirectlyToLeadGroup.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "RequirementsToLeadGroup")

        Return dstNorthwind

    End Function

    '' New code added by kamalesh Ahuja on December 30 for Rally Issue id DE 1276
    Public Function GetRequirementsAssignedToLeadReqGroupNoPrgVersionForStudent(ByVal StudentId As String, ByVal EnrollDate As Date, ByVal LeadGrpId As String, ByVal CampusId As String, ByVal SelectedLeadGroup As String) As DataSet
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim drowRootParent As DataRow
        Dim drowSchoolLevelSummary As DataRow
        Dim drowSchoolLevelDetails As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb, sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
        Dim drowRequirementAssignedDirectlyToPrgVersion, drowRequirementAssignedDirectlyToLeadGroup As DataRow
        Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder

        Dim dsGetEntranceTestId As New DataSet
        Dim dsGetLeadGrpId As New DataSet
        Dim dstGetRequirementsAssignedToProgramVersion As New DataSet
        Dim dstGetRequirementsDirectlyAssigned As New DataSet
        Dim intRowCounter As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If


        'Query gets the requirements assigned to LeadGroup and Program Version
        With sbReqsAssignedDirectlyToLeadGroup
            .Append(" select distinct t1.adReqId,Descrip,adReqTypeId, ")
            .Append(" '00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            .Append(" (select Distinct ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as ActualScore, ")
            .Append(" (select Distinct TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as TestTaken, ")
            .Append(" (select Distinct Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as Comments, ")
            .Append(" (select Distinct OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and StudentId = '" & StudentId & "' ) as override, ")
            .Append(" (select Count(*) from plStudentDocs where DocumentId=t1.adReqId and StudentId = '" & StudentId & "' ) as DocSubmittedCount, ")
            .Append(" t2.Minscore,ISNULL(t3.IsRequired,0) as Required, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,plStudentDocs s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.StudentId = '" & StudentId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
            .Append(" Case when t3.LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t3.LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            ''Modified by Saraswathi lakshmanan on Sept 21 2010
            ''For Document Tracking
            .Append(" (select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and ReqforEnrollment=1  and StatusId='" & strActiveGUID & "') t1, ")
            .Append(" (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append(" adReqLeadGroups t3 ")
            .Append(" where ")
            .Append(" t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  ")
            .Append(" and t3.LeadGrpId in (" & SelectedLeadGroup & ") and t3.LeadGrpId='" & LeadGrpId & "' ")
            If Not strCampGrpId = "" Then
                .Append("  and t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With

        dadNorthwind = New OleDbDataAdapter(sbReqsAssignedDirectlyToLeadGroup.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "RequirementsToLeadGroup")

        Return dstNorthwind

    End Function

    Public Function CheckandListEnrollmentDocumentRequirements_Sp(ByVal StuEnrollId As String) As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' Add empId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   Execute the query
        Dim resultds As New DataSet
        resultds = db.RunParamSQLDataSet_SP("USP_ListMissingEnrollmentDocumentRequirements")

        Return resultds

        'If result >= 1 Then
        '    Return False
        'Else
        '    Return True
        'End If
        db.CloseConnection()
    End Function
    Public Function CheckandListEnrollmentTestRequirements_Sp(ByVal StuEnrollId As String) As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' Add empId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   Execute the query
        Dim resultds As DataSet
        resultds = db.RunParamSQLDataSet_SP("USP_ListMissingEnrollmentTestRequirements")

        Return resultds

        'If result >= 1 Then
        '    Return False
        'Else
        '    Return True
        'End If
        db.CloseConnection()
    End Function
    Public Function CheckandListGradDocumentRequirements_Sp(ByVal StuEnrollId As String) As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' Add empId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   Execute the query
        Dim resultds As DataSet
        resultds = db.RunParamSQLDataSet_SP("USP_ListMissingGradDocumentRequirements")

        Return resultds

        'If result >= 1 Then
        '    Return False
        'Else
        '    Return True
        'End If
        db.CloseConnection()
    End Function
    Public Function CheckandListGradTestRequirements_Sp(ByVal StuEnrollId As String) As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' Add empId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   Execute the query
        Dim resultds As DataSet
        resultds = db.RunParamSQLDataSet_SP("USP_ListMissingGradTestRequirements")

        Return resultds

        'If result >= 1 Then
        '    Return False
        'Else
        '    Return True
        'End If
        db.CloseConnection()
    End Function
    Public Function CheckandListGradReqGroups_Sp(ByVal StuEnrollId As String) As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' Add empId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Dim ds As New DataSet
        Dim dr As DataRow

        ds = db.RunParamSQLDataSet_SP("USP_DoesReqGroupMeetsConditionsforGraduation")

        Dim strMessage As String = ""
        Dim intRequiredReqs As Integer
        Dim intAttemptedReqs As Integer

        For Each dr In ds.Tables(0).Rows
            intRequiredReqs = dr("NumReqs")
            intAttemptedReqs = dr("AttemptedReqs")
            If intAttemptedReqs < intRequiredReqs Then
                Return ds
                'strMessage = strMessage + "The Student hasn't satisfied the requirement groups for graduation. The requirement(s) is/are " + dr
                ''   Return "Not Eligible"
                ''   Exit Function
            End If
            Dim sbCheckIfRequiredRequirementExistsWithInGroup As New StringBuilder
            Dim intReqExistsCount As Integer = 0

            db.ClearParameters()
            db.AddParameter("@ReqGrpId", dr("ReqGrpId"), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Try
                ds = db.RunParamSQLDataSet_SP("USP_ListReqRequirementsExistWithinGroup")
                'If count is >=1 then there is a requirement with in this group that has been marked as required 
                'but has not been satisfied or overriden
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        Return ds
                    End If
                End If

                'If intReqExistsCount >= 1 Then
                '    'Return "Not Eligible"
                '    'Exit Function
                'End If
            Catch ex As Exception
                strMessage = ""
            End Try
        Next

        Return ds
        'If Not strMessage = "" Then
        '    Return "Not Eligible"
        'Else
        '    Return "Eligible"
        'End If
    End Function
    Public Function CheckandListEnrollReqGroups_Sp(ByVal StuEnrollId As String) As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' Add empId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Dim ds As New DataSet
        Dim dr As DataRow

        ds = db.RunParamSQLDataSet_SP("USP_DoesReqGroupMeetsConditionsforEnrollment")

        Dim strMessage As String = ""
        Dim intRequiredReqs As Integer
        Dim intAttemptedReqs As Integer

        For Each dr In ds.Tables(0).Rows
            intRequiredReqs = dr("NumReqs")
            intAttemptedReqs = dr("AttemptedReqs")
            If intAttemptedReqs < intRequiredReqs Then
                Return ds
            End If

            db.ClearParameters()
            db.AddParameter("@ReqGrpId", dr("ReqGrpId"), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Try
                ds = db.RunParamSQLDataSet_SP("USP_ListReqRequirementsExistWithinGroup")
                'If count is >=1 then there is a requirement with in this group that has been marked as required 
                'but has not been satisfied or overriden
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        Return ds
                    End If
                End If
            Catch ex As Exception
                strMessage = ""
            End Try
        Next

        Return ds
    End Function


    '''---------------------------------------------------------------------------------------------------------------------------
    Public Function CheckandListFinAidReqGroups_Sp(ByVal StuEnrollId As String) As DataSet
        Dim db As New SQLDataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        ' Add empId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Dim ds As DataSet
        Dim dr As DataRow

        ds = db.RunParamSQLDataSet_SP("USP_DoesReqGroupMeetsConditionsforFinAid")

        Dim strMessage As String = ""
        Dim intRequiredReqs As Integer
        Dim intAttemptedReqs As Integer

        For Each dr In ds.Tables(0).Rows
            intRequiredReqs = dr("NumReqs")
            intAttemptedReqs = dr("AttemptedReqs")
            If intAttemptedReqs < intRequiredReqs Then
                Return ds
            End If

            db.ClearParameters()
            db.AddParameter("@ReqGrpId", dr("ReqGrpId"), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Try
                ds = db.RunParamSQLDataSet_SP("USP_ListReqRequirementsExistWithinGroup")
                'If count is >=1 then there is a requirement with in this group that has been marked as required 
                'but has not been satisfied or overriden
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        Return ds
                    End If
                End If
            Catch ex As Exception
                strMessage = ""
            End Try
        Next

        Return ds
    End Function


    Public Function CheckandListFinAidDocumentRequirements_Sp(ByVal StuEnrollId As String) As DataSet
        Dim db As New SQLDataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' Add empId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   Execute the query
        Dim resultds As DataSet
        resultds = db.RunParamSQLDataSet_SP("USP_ListMissingFinAidDocumentRequirements")
        db.CloseConnection()
        Return resultds

    End Function
    Public Function CheckandListFinAidTestRequirements_Sp(ByVal StuEnrollId As String) As DataSet
        Dim db As New SQLDataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        ' Add empId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   Execute the query
        Dim resultds As DataSet
        resultds = db.RunParamSQLDataSet_SP("USP_ListMissingFinAidTestRequirements")
        db.CloseConnection()
        Return resultds

    End Function

End Class
