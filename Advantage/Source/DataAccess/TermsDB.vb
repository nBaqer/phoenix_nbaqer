
Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' TermsDB.vb
'
' TermsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class TermsDB
    Private ReadOnly myAdvAppSettings As AdvAppSettings

    Sub New()
        myAdvAppSettings = AdvAppSettings.GetAppSettings()
    End Sub

    Public Function GetAllTermsByUser(ByVal showActiveOnly As Boolean, Optional ByVal campusId As String = "", Optional ByVal username As String = "", Optional ByVal userid As String = "") As DataSet

        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   T.TermId, ")
            .Append("         T.StatusId, ")
            .Append("         T.TermCode, ")
            .Append("         T.TermDescrip, ")
            .Append("         T.StartDate, ")
            .Append("         T.EndDate, ")
            .Append("         T.ShiftId, ")
            .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saProgramVersionFees PVF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=PVF.TransCodeId and PVF.PrgVerId=SE.PrgVerId and T1.TermId=T.TermId and Voided=0 and IsAutomatic=1) ")
            .Append("         + ")
            .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saCourseFees CF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=CF.TransCodeId and T1.TermId=T.TermId and Voided=0 and IsAutomatic=1) ")
            .Append("         + ")
            .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saPeriodicFees PF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=PF.TransCodeId and PF.TermId=T1.TermId and  T1.TermId=T.TermId  and Voided=0 and IsAutomatic=1) As NumberOfTransactionsPosted ")
            .Append("FROM     arTerm T, syStatuses ST ")
            .Append("WHERE    T.StatusId = ST.StatusId ")
            If showActiveOnly Then
                .Append(" AND     ST.Status = 'Active' ")
            End If
            If campusId <> "" Then
                .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")

                .Append("FROM syCmpGrpCmps ")

                .Append("WHERE CampusId = '")
                .Append(campusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
            If Not username = "sa" And Not userid = "" Then
                .Append(" and T.CampGrpId in ")
                .Append(" ( ")
                .Append("SELECT DISTINCT A.CampGrpId ")
                .Append("FROM syUsersRolesCampGrps A, syCampGrps B ")
                .Append("WHERE A.CampGrpId = B.CampGrpId ")
                .Append("AND A.UserId=? ")
                .Append(")")
            End If

            .Append("ORDER BY T.StartDate,T.TermDescrip")
        End With
        If Not username = "sa" And Not userid = "" Then
            db.AddParameter("@UserId", userid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function

    Public Function GetAllTerms(ByVal showActiveOnly As Boolean, Optional ByVal campusId As String = "") As DataSet

        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   T.TermId, ")
            .Append("         T.StatusId, ")
            .Append("         T.TermCode, ")
            .Append("         T.TermDescrip, ")
            .Append("         T.StartDate, ")
            .Append("         T.EndDate, ")
            .Append("         T.ShiftId, ")
            .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saProgramVersionFees PVF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=PVF.TransCodeId and PVF.PrgVerId=SE.PrgVerId and T1.TermId=T.TermId and Voided=0 and IsAutomatic=1) ")
            .Append("         + ")
            .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saCourseFees CF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=CF.TransCodeId and T1.TermId=T.TermId and Voided=0 and IsAutomatic=1) ")
            .Append("         + ")
            .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saPeriodicFees PF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=PF.TransCodeId and PF.TermId=T1.TermId and  T1.TermId=T.TermId  and Voided=0 and IsAutomatic=1) As NumberOfTransactionsPosted ")
            .Append("FROM     arTerm T, syStatuses ST ")
            .Append("WHERE    T.StatusId = ST.StatusId ")
            If showActiveOnly Then
                .Append(" AND     ST.Status = 'Active' ")
            End If
            If campusId <> "" Then
                .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")

                .Append("FROM syCmpGrpCmps ")

                .Append("WHERE CampusId = '")
                .Append(campusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If

            .Append("ORDER BY T.StartDate,T.TermDescrip")
        End With
        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllTerms_SP(ByVal showActiveOnly As Boolean, Optional ByVal campusId As String = "") As DataTable
        Dim db As New DataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@showActiveOnly", showActiveOnly, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet("dbo.usp_GetAllTerms", Nothing, "SP").Tables(0)

    End Function

    Public Function GetAllTerms(ByVal feeType As AdvantageCommonValues.TuitionFeeTypes, ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   T.TermId, ")
            .Append("         T.StatusId, ")
            .Append("         T.TermCode, ")
            .Append("         T.TermDescrip, ")
            .Append("         T.StartDate, ")
            .Append("         T.EndDate, ")
            .Append("         T.ShiftId, ")
            Select Case feeType
                Case AdvantageCommonValues.TuitionFeeTypes.Program
                    .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saProgramVersionFees PVF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=PVF.TransCodeId and PVF.PrgVerId=SE.PrgVerId and T1.TermId=T.TermId and Voided=0 and IsAutomatic=1 and T1.FeeLevelId=2 and T1.CampusId = '" + campusId + "' ) As NumberOfTransactionsPosted ")
                Case AdvantageCommonValues.TuitionFeeTypes.Course
                    .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saCourseFees CF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=CF.TransCodeId and T1.TermId=T.TermId and Voided=0 and IsAutomatic=1 and T1.FeeLevelId=3 and T1.CampusId = '" + campusId + "' ) As NumberOfTransactionsPosted ")
                Case AdvantageCommonValues.TuitionFeeTypes.Term
                    .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saPeriodicFees PF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=PF.TransCodeId and T1.TermId=T.TermId and Voided=0 and IsAutomatic=1 and T1.FeeLevelId=1 and T1.CampusId = '" + campusId + "' ) As NumberOfTransactionsPosted ")
            End Select
            .Append("FROM     arTerm T, syStatuses ST ")
            .Append("WHERE    T.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            If campusId <> "" Then
                .Append("AND T.TermId in ")
                .Append("(Select distinct CST.TermId from arStuEnrollments SE, arResults R, arClassSectionTerms CST,syStatusCodes SC  where SE.CampusId='" + campusId + "' and R.TestId=CST.ClsSectionId ")
                .Append("AND    SE.StatusCodeId=SC.StatusCodeId ")
                .Append("AND    SC.SysStatusId In (7,9,13,20)) ")

                .Append("    AND (T.CampGrpId IN(SELECT CampGrpId  ")
                .Append("    FROM syCmpGrpCmps  ")
                .Append("    WHERE CampusId = '" + campusId + "' ")
                .Append("    AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL'))  ")
                .Append("    OR T.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL'))  ")
            End If
            .Append("ORDER BY T.StartDate,T.TermDescrip")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllCohorts(ByVal feeType As AdvantageCommonValues.TuitionFeeTypes, ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            ''Cohort Start Date Formatted
            .Append(" select  Convert(varchar(10),R.CohortStartDate,101)as CohortStartDate , ")
            Select Case feeType
                Case AdvantageCommonValues.TuitionFeeTypes.Program
                    .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saProgramVersionFees PVF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=PVF.TransCodeId and PVF.PrgVerId=SE.PrgVerId and SE.CohortStartDate=R.CohortStartDate and Voided=0 and IsAutomatic=1 and T1.FeeLevelId=2 and T1.CampusId = '" + campusId + "' ) As NumberOfTransactionsPosted ")
                Case AdvantageCommonValues.TuitionFeeTypes.Course
                    .Append("         (select count(*) from saTransactions T1, arStuEnrollments SE, saCourseFees CF where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=CF.TransCodeId and SE.cohortstartdate = R.CohortStartdate  and Voided=0 and IsAutomatic=1 and T1.FeeLevelId=3 and T1.CampusId = '" + campusId + "' ) As NumberOfTransactionsPosted ")

                Case AdvantageCommonValues.TuitionFeeTypes.Term
                    .Append("         (select count(distinct(TransactionId)) from saTransactions T1 where T1.StuEnrollId in (Select StuEnrollId from arStuEnrollments where CohortStartDate=R.CohortStartDate) and T1.TransCodeId in(Select TransCodeId from saPeriodicFees  WHERE TermId IN (SELECT arClassSections.TermId FROM arClassSections,arTerm WHERE arClassSections.TermId =arTerm.TermId AND  cohortstartdate =R.CohortStartDate)  )  and Voided=0 and IsAutomatic=1 and T1.FeeLevelId=1  and T1.CampusId = '" + campusId + "' ) As NumberOfTransactionsPosted ")

            End Select
            .Append("  ,year(R.CohortStartDate) ")
            .Append(" from (Select distinct SE.CohortStartdate as CohortStartDate ")
            .Append(" from arStuEnrollments SE, ")
            .Append(" arResults R, arClassSectionTerms CST,syStatusCodes SC where ")
            .Append(" R.TestId=CST.ClsSectionId and SE.CohortStartdate is not null ")
            .Append("AND    SE.StatusCodeId=SC.StatusCodeId ")
            .Append("AND    SC.SysStatusId In (7,9,13,20) ")

            If campusId <> "" Then
                .Append(" and SE.CampusId='" + campusId + "' ")
            End If
            .Append(") R ")
            .Append(" ORDER BY year(R.CohortStartdate) ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetNoneFutureActiveTerms(Optional ByVal campusId As String = "") As DataSet
        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   T.TermId, ")
            .Append("         T.StatusId, ")
            .Append("         T.TermCode, ")
            .Append("         T.TermDescrip, ")
            .Append("         T.StartDate, ")
            .Append("         T.EndDate, ")
            .Append("         T.ShiftId ")
            .Append("FROM     arTerm T, syStatuses ST ")
            .Append("WHERE    T.StatusId = ST.StatusId ")
            .Append("AND T.StartDate <= GetDate() ")
            .Append(" AND     ST.Status = 'Active' ")
            If campusId <> "" Then
                .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")

                .Append("FROM syCmpGrpCmps ")

                .Append("WHERE CampusId = '")
                .Append(campusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
            .Append("ORDER BY T.StartDate,T.TermDescrip")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetTermInfo(ByVal termId As String) As TermInfo

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with sub-queries
            .Append("SELECT T.TermId, ")
            .Append("    T.TermCode, ")
            .Append("    T.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=T.StatusId) As Status, ")
            .Append("    T.TermDescrip, ")
            .Append("    T.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=T.CampGrpId) As CampGrpdescrip, ")
            .Append("    T.StartDate, ")
            .Append("    T.EndDate, ")
            .Append("    T.ShiftId, ")
            .Append("    (Select ShiftDescrip from arShifts where ShiftId=T.ShiftId) As ShiftDescrip ")
            .Append("FROM  arTerm T ")
            .Append("WHERE T.TermId= ? ")
        End With

        ' Add the TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim termInfo As New TermInfo

        While dr.Read()

            '   set properties with data from DataReader
            With termInfo
                .TermId = termId
                .IsInDB = True
                .Code = dr("TermCode")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Description = dr("TermDescrip")
                If Not (dr("CampGrpId") Is DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpdescrip") Is DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                If Not (dr("StartDate") Is DBNull.Value) Then .StartDate = CType(dr("StartDate"), Date)
                If Not (dr("EndDate") Is DBNull.Value) Then .EndDate = CType(dr("Enddate"), Date)
                If Not (dr("ShiftId") Is DBNull.Value) Then .ShiftId = CType(dr("ShiftId"), Guid).ToString
                If Not (dr("Shiftdescrip") Is DBNull.Value) Then .ShiftDescrip = dr("ShiftDescrip")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return termInfo

    End Function

    Public Function GetTermEndDate_SP(ByVal termId As String) As Date

        '   connect to the database
        Dim termEndDate As Date
        Dim db As New SQLDataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")

        '   build the sql query
        db.AddParameter("@TermId", New Guid(termId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   Execute the query
        Try
            termEndDate = CDate(db.RunParamSQLScalar_SP("dbo.usp_GetTermEndDate"))
        Finally
            db.CloseConnection()
        End Try
        Return termEndDate
    End Function

    Public Function UpdateTermInfo(ByVal termInfo As TermInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess



        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE arTerm Set TermId = ?, TermCode = ?, ")
                .Append(" StatusId = ?, TermDescrip = ?, CampGrpId = ?, ")
                .Append(" StartDate = ?, EndDate = ?, ShiftId = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE TermId = ? ")
            End With

            '   add parameters values to the query

            '   TermId
            db.AddParameter("@TermId", termInfo.TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TermCode
            db.AddParameter("@TermCode", termInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", termInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TermDescrip
            db.AddParameter("@TermDescrip", termInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If termInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", termInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   StartDate
            db.AddParameter("@StartDate", termInfo.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   EndDate
            db.AddParameter("@EndDate", termInfo.EndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ShiftId
            If termInfo.ShiftId = Guid.Empty.ToString Then
                db.AddParameter("@ShiftId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ShiftId", termInfo.ShiftId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TermId
            db.AddParameter("@TermId", termInfo.TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return 0

        Catch ex As OleDbException
            '   return an error to the client
            Return -1
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddTermInfo(ByVal termInfo As TermInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess



        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT arTerm (TermId, TermCode, StatusId, ")
                .Append("   TermDescrip, CampGrpId, StartDate, EndDate, ShiftId, ")
                .Append("   ModUser, ModDate, IsModule) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   TermId
            db.AddParameter("@TermId", termInfo.TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TermCode
            db.AddParameter("@TermCode", termInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", termInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TermDescrip
            db.AddParameter("@TermDescrip", termInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If termInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", termInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   StartDate
            db.AddParameter("@StartDate", termInfo.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   EndDate
            db.AddParameter("@EndDate", termInfo.EndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ShiftId
            If termInfo.ShiftId = Guid.Empty.ToString Then
                db.AddParameter("@ShiftId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ShiftId", termInfo.ShiftId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   IsModule
            If termInfo.IsModule = True Then
                db.AddParameter("@ismodule", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Else
                db.AddParameter("@ismodule", 0, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End If

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return 0

        Catch ex As OleDbException
            '   return an error to the client
            Return -1
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function


    Public Function DeleteTermInfo(ByVal termId As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM arTerm ")
                .Append("WHERE TermId = ? ")
            End With

            '   add parameters values to the query

            '   BankId
            db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function IsThisTermBeingUsed(ByVal termId As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        With sb
            .Append("Select ")
            .Append("    	(select Count(*) from arClasssections where TermId=?) + ")
            .Append("   	(select Count(*) from arTransferGrades where TermId= ? ) + ")
            .Append("       (select Count(*) from saPeriodicFees where TermId= ? ) ")
        End With

        'add TermId to the parameters list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        'return 
        Return CType(db.RunParamSQLScalar(sb.ToString), Boolean)

    End Function
    Public Function ClsSectionForTerm(ByVal termId As String, ByVal campGrpId As String, Optional ByVal newCampGrpId As String = "") As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim strCampusId As String
        Dim strNewCampusId As String

        strCampusId = GetCampusByCampusGroup(campGrpId)
        strNewCampusId = GetCampusByCampusGroup(newCampGrpId)
        With sb
            .Append("   select Distinct t1.ClsSection,t3.Descrip as Course,t2.CampDescrip as Campus ")
            .Append("   from arClassSections t1,syCampuses t2,arReqs t3 ")
            .Append("   where t1.CampusId = t2.CampusId And ")
            .Append("   t1.ReqId=t3.ReqId and ")
            .Append("   t1.TermId=? and t1.CampusId in ('")
            .Append(strCampusId)
            .Append(") ")
            .Append(" and t1.CampusId not in ('")
            .Append(strNewCampusId)
            .Append(") ")
        End With

        'add TermId to the parameters list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return 
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function GetMinAndMaxDatesForClassSections(ByVal termId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        With sb
            .Append("   Select MIN(StartDate) MinDate,Max(EndDate)MaxDate from arclassSections ")
            .Append("   where TermID=? ")

        End With

        'add TermId to the parameters list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return 
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function IsThereAnyClsSectionForTerm(ByVal termId As String, ByVal campGrpId As String, Optional ByVal newCampGrpId As String = "") As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim strCampusId As String
        Dim strNewCampusId As String
        strCampusId = GetCampusByCampusGroup(campGrpId)
        strNewCampusId = GetCampusByCampusGroup(newCampGrpId)
        With sb
            .Append("   select Count(*) ")
            .Append("   from arClassSections t1 ")
            .Append("   where ")
            .Append("   t1.TermId=? and t1.CampusId in ('")
            .Append(strCampusId)
            .Append(") ")
            .Append(" and t1.CampusId not in ('")
            .Append(strNewCampusId)
            .Append(") ")
        End With

        'add TermId to the parameters list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'return 
        Return db.RunParamSQLScalar(sb.ToString)
    End Function
    Public Function IsThereAnyFERPACategory(ByVal ferpaCategoryID As String, ByVal campGrpId As String, Optional ByVal newCampGrpId As String = "") As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim strCampusId As String
        Dim strNewCampusId As String
        strCampusId = GetCampusByCampusGroup(campGrpId)
        strNewCampusId = GetCampusByCampusGroup(newCampGrpId)
        With sb
            .Append("   select Count(*) ")
            .Append("   from arFERPAPolicy t1,arStuEnrollments t2  ")
            .Append("   where t1.StudentId=t2.StudentId and t1.FERPACategoryId = ? ")
            .Append("   and t2.CampusId in ('")
            .Append(strCampusId)
            .Append(") ")
            .Append(" and t2.CampusId not in ('")
            .Append(strNewCampusId)
            .Append(") ")
        End With

        'add TermId to the parameters list
        db.AddParameter("@FERPACategoryID", ferpaCategoryID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'return 
        Return db.RunParamSQLScalar(sb.ToString)
    End Function

    Public Function IsThereAnyFERPAEntity(ByVal ferpaEntityID As String, ByVal campGrpId As String, Optional ByVal newCampGrpId As String = "") As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim strCampusId As String
        Dim strNewCampusId As String
        strCampusId = GetCampusByCampusGroup(campGrpId)
        strNewCampusId = GetCampusByCampusGroup(newCampGrpId)
        With sb
            .Append("   select Count(*) ")
            .Append("   from arFERPAPolicy t1,arStuEnrollments t2  ")
            .Append("   where t1.StudentId=t2.StudentId and t1.FERPAEntityID = ? ")
            .Append("   and t2.CampusId in ('")
            .Append(strCampusId)
            .Append(") ")
            .Append(" and t2.CampusId not in ('")
            .Append(strNewCampusId)
            .Append(") ")
        End With

        'add TermId to the parameters list
        db.AddParameter("@FERPACategoryID", ferpaEntityID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'return 
        Return db.RunParamSQLScalar(sb.ToString)
    End Function

    Public Function GetActiveShiftsByCampus(ByVal campusId As String) As DataSet
        Dim db As New DataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet("dbo.USP_GetAllActiveShiftsByCampus", Nothing, "SP")
    End Function

    Private Function GetCampusByCampusGroup(ByVal campGrpId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet
        'Dim ds1 As New DataSet
        Dim strCampusId As String = ""
        If Not campGrpId = "" Then
            With sb
                .Append(" Select Distinct CampusId from syCmpGrpCmps where CampGrpId=?")
            End With
            db.AddParameter("@CampGrpId", campGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet(sb.ToString)
            If ds.Tables(0).Rows.Count >= 1 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    strCampusId &= row("CampusId").ToString & "','"
                Next
                strCampusId = Mid(strCampusId, 1, InStrRev(strCampusId, "'") - 2)
            End If
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            ds.Dispose()
        End If
        Return strCampusId
    End Function
    Public Function GetAllTermIdsWithProgIds(ByVal showActiveOnly As Boolean, Optional ByVal campusId As String = "") As DataSet

        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select  distinct ")
            .Append("		T.TermId, ")
            .Append("		T.ProgId, ")
            .Append("       (Select ProgDescrip from arPrograms where ProgId=T.ProgId) As ProgDescrip, ")
            .Append(" case when (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=T.ProgId) is null  then ")
            .Append(" (Select ProgDescrip from arPrograms where ProgId=T.ProgId) ")
            .Append(" else ")
            .Append("(Select ProgDescrip from arPrograms where ProgId=T.ProgId) + ' (' + (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=T.ProgId) + ')'  ")
            .Append(" end as ShiftDescrip  ")
            .Append("from	arTerm T, syStatuses ST ")
            .Append("where    T.StatusId = ST.StatusId ")
            If showActiveOnly Then
                .Append(" AND     ST.Status = 'Active' ")
            End If
            If campusId <> "" Then
                .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId = '")
                .Append(campusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR     T.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
            .Append("Order By ProgDescrip")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function

    '''  issue 14296: Instructor Supervisors and Education Directors 
    '''  need to be able to post and modify attendance. 
    '''  The term is populated based on the role of the user 
    ''' The Academic advisors can view all the terms and 
    ''' the instructor supervisors can view their respective instructors terms only
    Public Function GetNoneFutureActiveTermsbyUserandRoles(ByVal userId As String, ByVal userName As String, ByVal isAcademicAdvisor As Boolean, Optional ByVal campusId As String = "") As DataSet
        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            If userName.ToLower <> "sa" And userName.ToLower <> "support" And isAcademicAdvisor = False Then

                .Append("SELECT distinct  T.TermId, ")
                .Append("T.StatusId, ")
                .Append("T.TermCode, ")
                .Append("T.TermDescrip, ")
                .Append("T.StartDate, ")
                .Append("T.EndDate, ")
                .Append("T.ShiftId ")
                .Append("FROM arTerm T, syStatuses ST ,arClassSections t1 ")
                .Append("WHERE T.StatusId = ST.StatusId And (T.StartDate <= GetDate() OR (t.StartDate IS NULL and t.EndDate IS NULL AND t.ProgramVersionId IS NOT NULL) ) ")
                .Append(" AND     ST.Status = 'Active' ")
                If campusId <> "" Then
                    .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")
                    .Append("FROM syCmpGrpCmps ")
                    .Append("WHERE CampusId = '")
                    .Append(campusId)
                    .Append("' ")
                    .Append(" AND T.TermId=t1.TermId) ) ")
                End If

                .Append("AND (t1.InstructorId in (Select InstructorId ")
                .Append("FROM arInstructorsSupervisors ")
                .Append("where SupervisorId= '")
                .Append(userId)
                .Append("' ) ")
                .Append("OR t1.InstructorId='")
                .Append(userId)
                .Append("' ) ")
                .Append("ORDER BY T.StartDate,T.TermDescrip")


            Else

                .Append("SELECT distinct  T.TermId, ")
                .Append("         T.StatusId, ")
                .Append("         T.TermCode, ")
                .Append("         T.TermDescrip, ")
                .Append("         T.StartDate, ")
                .Append("         T.EndDate, ")
                .Append("         T.ShiftId ")
                .Append("FROM     arTerm T, syStatuses ST ")
                .Append("WHERE    T.StatusId = ST.StatusId ")
                .Append("AND T.StartDate <= GetDate() ")
                .Append(" AND     ST.Status = 'Active' ")
                If campusId <> "" Then
                    .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")

                    .Append("FROM syCmpGrpCmps ")

                    .Append("WHERE CampusId = '")
                    .Append(campusId)
                    .Append("' ")
                    .Append("))")
                End If


                .Append("ORDER BY T.StartDate,T.TermDescrip")
            End If

        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    '' to get the CohortStartDAtefor the past and Current Active terms
    '''  issue 14296: Instructor Supervisors and Education Directors 
    '''  need to be able to post and modify attendance. 
    '''  The term is populated based on the role of the user 
    ''' The Academic advisors can view all the terms and 
    ''' the instructor supervisors can view their respective instructors terms only
    Public Function GetCohortStartDateforNoneFutureActiveTermsbyUserandRoles(ByVal userId As String, ByVal userName As String, ByVal isAcademicAdvisor As Boolean, Optional ByVal campusId As String = "") As DataSet
        '   connect to the database
        Dim db As New DataAccess



        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            If userName <> "sa" And isAcademicAdvisor = False Then
                .Append("SELECT distinct  Convert(varchar(10),CS.CohortStartDate,101)as CohortStartDate,year(CS.CohortStartDate) ")
                .Append(" FROM     arTerm T, syStatuses ST ,arClassSections CS ")
                .Append("WHERE    T.StatusId = ST.StatusId ")
                .Append("AND T.StartDate <= GetDate() and CS.CohortStartDate is not null ")
                .Append(" AND     ST.Status = 'Active' ")
                If campusId <> "" Then
                    .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")

                    .Append("FROM syCmpGrpCmps ")

                    .Append("WHERE CampusId = '")
                    .Append(campusId)
                    .Append("' ")
                    .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                    .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")

                    ''Added this Code so that
                    ''' The Academic advisors can view all the terms and 
                    ''' the instructor supervisors can view their respective instructors terms only
                End If


                .Append(" AND T.TermId=CS.TermId  ")

                .Append(" AND (CS.InstructorId in (Select InstructorId ")
                .Append(" from arInstructorsSupervisors where")
                .Append(" SupervisorId= '")
                .Append(userId)
                .Append("' )")
                .Append(" OR CS.InstructorId='")
                .Append(userId)
                .Append("') ")
                .Append("ORDER BY year(CS.CohortStartDate)")

                db.AddParameter("userId", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("userId", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            Else

                .Append("SELECT Distinct Convert(varchar(10),CS.CohortStartDate,101)as CohortStartDate  ")
                .Append("FROM     arTerm T, syStatuses ST,arClassSections CS ")
                .Append("WHERE    T.StatusId = ST.StatusId ")
                .Append("AND T.StartDate <= GetDate() and CS.CohortStartDate is not null ")
                .Append(" AND     ST.Status = 'Active' ")
                .Append(" AND T.TermId=CS.TermId  ")
                If campusId <> "" Then
                    .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")

                    .Append("FROM syCmpGrpCmps ")

                    .Append("WHERE CampusId = '")
                    .Append(campusId)
                    .Append("' ")
                    .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                    .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                End If


                .Append("ORDER BY CohortStartDate")
            End If

        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function

    ''' <summary>
    ''' Return the list of records associated to enrollment list with Scores for the given ClassId
    ''' by components
    ''' </summary>
    ''' <param name="enrollmentIdToModifyList">List of enrollment Guid as string</param>
    ''' <param name="classId">Class Scored</param>
    ''' <returns></returns>
    Public Function GetAllScoredRecordsForEnrollmentAndClassId(ByVal enrollmentIdToModifyList As List(Of String), ByVal classId As String, Optional ByVal isCourseLevel As Boolean = False) As List(Of GrdRecordsPoco)

        Dim enroll As String = String.Empty
        Dim resultList As List(Of GrdRecordsPoco) = New List(Of GrdRecordsPoco)()

        If enrollmentIdToModifyList.Count = 0 Then
            Return resultList  ' This should not be happen, because only enrollments that can be scored should be showed
        End If

        For Each s As String In enrollmentIdToModifyList
            If Not (String.IsNullOrEmpty(enroll)) Then
                enroll += ","
            End If
            enroll += "'" + s + "'"
        Next

        'Convert the list in CSV...
        'Dim enrollments As String = String.Join(", ", enrollmentIdToModifyList)


        Dim sb As New StringBuilder

        If isCourseLevel Then
            With sb
                .Append(" SELECT  grd.GrdBkResultId ")
                .Append("        ,enroll.StuEnrollId ")
                .Append("        ,gct.Descrip ")
                .Append("        ,grd.Score ")
                .Append("        ,grd.Comments ")
                .Append("        ,grd.ResNum ")
                .Append("        ,grd.InstrGrdBkWgtDetailId ")
                .Append(" FROM    arGrdBkResults grd ")
                .Append(" JOIN    dbo.arStuEnrollments enroll ON enroll.StuEnrollId = grd.StuEnrollId ")
                .Append(" JOIN    dbo.arGrdBkWgtDetails wgt ON wgt.InstrGrdBkWgtDetailId = grd.InstrGrdBkWgtDetailId ")
                .Append(" JOIN    dbo.arGrdComponentTypes gct ON gct.GrdComponentTypeId = wgt.GrdComponentTypeId  ")
                .Append(" WHERE   grd.ClsSectionId = @ClassId ")
                .Append("         AND grd.StuEnrollId IN ( " + enroll + " ) ")
            End With
        Else
            With sb
                .Append(" SELECT  grd.GrdBkResultId ")
                .Append("        ,enroll.StuEnrollId ")
                .Append("        ,wgt.Descrip ")
                .Append("        ,grd.Score ")
                .Append("        ,grd.Comments ")
                .Append("        ,grd.ResNum ")
                .Append("        ,grd.InstrGrdBkWgtDetailId ")
                .Append(" FROM    arGrdBkResults grd ")
                .Append(" JOIN    dbo.arStuEnrollments enroll ON enroll.StuEnrollId = grd.StuEnrollId ")
                .Append(" JOIN    dbo.arGrdBkWgtDetails wgt ON wgt.InstrGrdBkWgtDetailId = grd.InstrGrdBkWgtDetailId ")
                .Append(" WHERE   grd.ClsSectionId = @ClassId ")
                .Append("         AND grd.StuEnrollId IN ( " + enroll + " ) ")
            End With

        End If


        Dim conn As SqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim command As SqlCommand = New SqlCommand(sb.ToString(), conn)
        command.Parameters.AddWithValue("@ClassId", classId)

        conn.Open()
        Try
            Dim reader As SqlDataReader = command.ExecuteReader()
            While reader.Read()
                Dim p As New GrdRecordsPoco()
                p.GrdBkResultId = reader("GrdBkResultId").ToString()
                p.StuEnrollId = reader("StuEnrollId").ToString()
                Dim desc = reader("Descrip")
                p.Descrip = If(IsDBNull(desc), Nothing, desc)
                p.Score = If(IsDBNull(reader("Score")), "", reader("Score"))
                Dim comment = reader("Comments")
                p.Comments = If(IsDBNull(comment), Nothing, comment)
                p.ResNum = If(IsDBNull(reader("ResNum")), 0, reader("ResNum"))
                p.InstrGrdBkWgtDetailId = If(IsDBNull(reader("InstrGrdBkWgtDetailId")), Guid.Empty, reader("InstrGrdBkWgtDetailId").ToString())
                resultList.Add(p)
            End While

            Return resultList

        Finally
            conn.Close()
        End Try
    End Function

    Public Sub ExecuteScoreUpdates(ByVal recordsToDeleteList As List(Of GrdRecordsPoco), ByVal recordsToUpdateList As List(Of GrdRecordsPoco), ByVal recordsToInsertList As List(Of GrdRecordsPoco), ByVal enrollmentIdToModifyList As List(Of String), ByVal arRecordValues As GrdRecordsPoco)

        'Create list of command SQL to execute
        Dim comlist As List(Of SqlCommand) = New List(Of SqlCommand)()

        'Enable MARS (Multiple Active Result Sets)
        Dim connectinString As String = myAdvAppSettings.AppSettings("ConnectionString").ToString '+ ";MultipleActiveResultSets=True"

        'Create connection object
        Dim conn As SqlConnection = New SqlConnection(connectinString)

        'Prepare all Delete Queries
        Const sqlDelete As String = "DELETE FROM arGrdBkResults WHERE GrdBkResultId = @GrdBkResultId"
        For Each poco As GrdRecordsPoco In recordsToDeleteList
            Dim com As SqlCommand = New SqlCommand(sqlDelete, conn)
            com.Parameters.AddWithValue("@GrdBkResultId", poco.GrdBkResultId)
            comlist.Add(com)
        Next

        'Prepare all update queries
        Const sqlUpdate As String = "UPDATE arGrdBkResults SET	Score = @Score, DateCompleted = @DateCompleted, PostDate = @PostDate, Comments = @Comments, ModUser = @ModUser, ModDate = GETDATE(), IsCompGraded = @IsCompGraded WHERE GrdBkResultId = @GrdBkResultId "
        For Each poco As GrdRecordsPoco In recordsToUpdateList
            Dim com As SqlCommand = New SqlCommand(sqlUpdate, conn)
            com.Parameters.AddWithValue("@GrdBkResultId", poco.GrdBkResultId)
            If IsNothing(poco.Score) Then
                com.Parameters.AddWithValue("@Score", DBNull.Value)
            Else
                com.Parameters.AddWithValue("@Score", poco.Score)
            End If

            com.Parameters.AddWithValue("@Comments", poco.Comments)
            com.Parameters.AddWithValue("@DateCompleted", If(poco.DateCompleted.HasValue, poco.DateCompleted, DBNull.Value))
            com.Parameters.AddWithValue("@PostDate", If(poco.DateCompleted.HasValue, poco.DateCompleted, DBNull.Value))
            com.Parameters.AddWithValue("@ModUser", arRecordValues.UserName)
            com.Parameters.AddWithValue("@IsCompGraded", Not poco.IsIncomplete)
            comlist.Add(com)
        Next

        'Prepare all Insert Queries
        Dim sb As New StringBuilder
        With sb
            .Append(" INSERT  INTO arGrdBkResults ")
            .Append("         (GrdBkResultId ")
            .Append("         ,ClsSectionId ")
            .Append("         ,StuEnrollId ")
            .Append("         ,InstrGrdBkWgtDetailId ")
            .Append("         ,Score ")
            .Append("         ,Comments ")
            .Append("         ,ResNum ")
            .Append("         ,ModUser ")
            .Append("         ,ModDate ")
            .Append("         ,PostDate ")
            .Append("         ,IsCompGraded  ")
            .Append(" 		  ,DateCompleted ")
            .Append(" 		  				 ")
            .Append("         ) ")
            .Append(" VALUES  (@GrdBkResultId ")
            .Append("         ,@ClsSectionId ")
            .Append("         ,@StuEnrollId ")
            .Append("         ,@InstrGrdBkWgtDetailId ")
            .Append("         ,@Score ")
            .Append("         ,@Comments ")
            .Append("         ,@ResNum ")
            .Append("         ,@ModUser ")
            .Append("         ,GETDATE() ")
            .Append("         ,@PostDate ")
            .Append("         ,@IsCompGraded ")
            .Append("         ,@DateCompleted ")
            .Append("         ) ")
        End With
        For Each poco As GrdRecordsPoco In recordsToInsertList
            Dim com As SqlCommand = New SqlCommand(sb.ToString(), conn)
            com.Parameters.AddWithValue("@GrdBkResultId", poco.GrdBkResultId)
            com.Parameters.AddWithValue("@ClsSectionId", arRecordValues.ClsSectionId)
            com.Parameters.AddWithValue("@StuEnrollId", poco.StuEnrollId)
            com.Parameters.AddWithValue("@InstrGrdBkWgtDetailId", poco.InstrGrdBkWgtDetailId)
            com.Parameters.AddWithValue("@ResNum", poco.ResNum)
            com.Parameters.AddWithValue("@Score", If(poco.Score Is Nothing, DBNull.Value, poco.Score))
            com.Parameters.AddWithValue("@Comments", poco.Comments)
            com.Parameters.AddWithValue("@ModUser", arRecordValues.UserName)
            com.Parameters.AddWithValue("@IsCompGraded", Not poco.IsIncomplete)
            com.Parameters.AddWithValue("@DateCompleted", If(poco.DateCompleted.HasValue, poco.DateCompleted, DBNull.Value))
            com.Parameters.AddWithValue("@PostDate", If(poco.DateCompleted.HasValue, poco.DateCompleted, DBNull.Value))
            comlist.Add(com)
        Next

        'Prepare all Update ArRequest Table
        'Prepare all update queries
        'Const sqlArResultUpdate As String = "UPDATE arResults SET  IsInComplete = @IsCompGraded, ModUser = @ModUser, ModDate =GETDATE()  WHERE  TestId = @ClsSectionId AND StuEnrollId = @StuEnrollId "
        'For Each id As String In enrollmentIdToModifyList
        '    Dim com As SqlCommand = New SqlCommand(sqlArResultUpdate, conn)
        '    com.Parameters.AddWithValue("@StuEnrollId", id)
        '    com.Parameters.AddWithValue("@ClsSectionId", arRecordValues.ClsSectionId)
        '    com.Parameters.AddWithValue("@ModUser", arRecordValues.UserName)
        '    com.Parameters.AddWithValue("@IsCompGraded", Not arRecordValues.IsIncomplete)
        '    comlist.Add(com)
        'Next

        Dim sqlTrans As SqlTransaction
        conn.Open()
        'Create and assigned transaction
        sqlTrans = conn.BeginTransaction("ScoreClass")
        For Each command As SqlCommand In comlist
            command.Transaction = sqlTrans 'Attach commands to Transaction
        Next
        Try
            'Execute Command
            For Each command As SqlCommand In comlist
                command.ExecuteNonQuery()
            Next

            'Close Transaction
            sqlTrans.Commit()

        Catch ex As Exception
            sqlTrans.Rollback()
            Throw
        Finally
            conn.Close()  'Close Connexion
        End Try
    End Sub
End Class
