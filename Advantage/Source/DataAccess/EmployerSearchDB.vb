Imports FAME.Advantage.Common

Public Class EmployerSearchDB
    Public Function GetEmployerID(ByVal EmployerCode As String) As String
        '**************************************************************************************************
        'Purpose:       This Procedure Determines The Total Count OF Controls Based on The Controls Type
        '               The Controls belong to following three types - Range,List,None
        'Parameters:
        'Returns:       String
        'Notes:         This sub relies on the ResourceId and ModuleCode when
        '               when a request for the page is made.
        '               intRangeCount --- Holds the Total Count for Range Controls
        '               intListCount  --- Holds the Total Count for List Controls
        '               intNoneCount  --- Holds the Total Count for Controls with no validation
        '**************************************************************************************************
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim EmployerID As String
        Dim EmployerDescrip As String


        'connect to the database

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            'With subqueries
            .Append("SELECT     B.EmployerID,B.EmployerDescrip ")
            .Append("FROM  plEmployers B ")
            .Append("WHERE B.Code = ? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@code", EmployerCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read()
            'set properties with data from DataReader
            EmployerID = dr("EmployerID").ToString
            EmployerDescrip = dr("EmployerDescrip")
        End While

        If Not dr.IsClosed Then dr.Close()

        If EmployerID <> "" Then
            Return EmployerID & "," & EmployerDescrip
        Else
            Return "None"
        End If
    End Function
    Public Function GetEmployerNameByID(ByVal EmployerID As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim EmployerDescrip As String
        'connect to the database


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        'build the sql query
        With sb
            'With subqueries
            .Append("SELECT B.EmployerDescrip ")
            .Append("FROM  plEmployers B ")
            .Append("WHERE B.EmployerID = ? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@EmployerID", EmployerID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read()
            'set properties with data from DataReader
            EmployerDescrip = dr("EmployerDescrip")
        End While

        If Not dr.IsClosed Then dr.Close()

        Return EmployerDescrip
    End Function
    Public Function GetStudentID(ByVal SSN As String) As String
        '**************************************************************************************************
        'Purpose:       This Procedure Determines The Total Count OF Controls Based on The Controls Type
        '               The Controls belong to following three types - Range,List,None
        'Parameters:
        'Returns:       String
        'Notes:         This sub relies on the ResourceId and ModuleCode when
        '               when a request for the page is made.
        '               intRangeCount --- Holds the Total Count for Range Controls
        '               intListCount  --- Holds the Total Count for List Controls
        '               intNoneCount  --- Holds the Total Count for Controls with no validation
        '**************************************************************************************************
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim StudentID As String


        'connect to the database

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            'With subqueries
            .Append("SELECT     B.StudentID ")
            .Append("FROM  arStudent B ")
            .Append("WHERE B.SSN = ? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@SSN", SSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read()
            'set properties with data from DataReader
            StudentID = dr("StudentID").ToString
        End While

        If Not dr.IsClosed Then dr.Close()
        
        If StudentID <> "" Then
            Return StudentID
        Else
            Return "None"
        End If
    End Function
    Public Function GetStudentName(ByVal SSN As String) As String
        '**************************************************************************************************
        'Purpose:       This Procedure Determines The Total Count OF Controls Based on The Controls Type
        '               The Controls belong to following three types - Range,List,None
        'Parameters:
        'Returns:       String
        'Notes:         This sub relies on the ResourceId and ModuleCode when
        '               when a request for the page is made.
        '               intRangeCount --- Holds the Total Count for Range Controls
        '               intListCount  --- Holds the Total Count for List Controls
        '               intNoneCount  --- Holds the Total Count for Controls with no validation
        '**************************************************************************************************
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim studentName As String


        'connect to the database

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            'With subqueries
            .Append("SELECT    B.FirstName , B.MiddleName , B.LastName as StudentName ")
            .Append("FROM  arStudent B ")
            .Append("WHERE B.SSN = ? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@SSN", SSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read()
            'set properties with data from DataReader
            studentName = dr("StudentName").ToString
        End While

        If Not dr.IsClosed Then dr.Close()

        If studentName <> "" Then
            Return studentName
        Else
            Return "None"
        End If
    End Function
    Public Function GetStudentIdBySSN(ByVal SSN As String) As String
        '**************************************************************************************************
        'Purpose:       This Procedure Determines The Total Count OF Controls Based on The Controls Type
        '               The Controls belong to following three types - Range,List,None
        'Parameters:
        'Returns:       String
        'Notes:         This sub relies on the ResourceId and ModuleCode when
        '               when a request for the page is made.
        '               intRangeCount --- Holds the Total Count for Range Controls
        '               intListCount  --- Holds the Total Count for List Controls
        '               intNoneCount  --- Holds the Total Count for Controls with no validation
        '**************************************************************************************************
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim studentid As String
        'connect to the database


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            'With subqueries
            .Append("SELECT    B.StudentId ")
            .Append("FROM  arStudent B ")
            .Append("WHERE B.SSN = ? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@SSN", SSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read()
            'set properties with data from DataReader
            studentid = dr("StudentId").ToString
        End While

        If Not dr.IsClosed Then dr.Close()

        If studentid <> "" Then
            Return studentid
        Else
            Return "None"
        End If
    End Function
    Public Function GetEmployerCount(ByVal EmployerCode As String) As Integer
        '**************************************************************************************************
        'Purpose:       This Procedure Determines The Total Count OF Controls Based on The Controls Type
        '               The Controls belong to following three types - Range,List,None
        'Parameters:
        'Returns:       String
        'Notes:         This sub relies on the ResourceId and ModuleCode when
        '               when a request for the page is made.
        '               intRangeCount --- Holds the Total Count for Range Controls
        '               intListCount  --- Holds the Total Count for List Controls
        '               intNoneCount  --- Holds the Total Count for Controls with no validation
        '**************************************************************************************************
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim intCount As Integer
        'connect to the database


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            'With subqueries
            .Append("SELECT  Count(*) as EmployerCount ")
            .Append("FROM  plEmployers B ")
            .Append("WHERE B.Code = ? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@code", EmployerCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read()
            'set properties with data from DataReader
            intCount = dr("EmployerCount")
        End While

        If Not dr.IsClosed Then dr.Close()

        Return intCount
    End Function
    Public Function GetStudentCount(ByVal SSN As String) As Integer
        '**************************************************************************************************
        'Purpose:       This Procedure Determines The Total Count OF Controls Based on The Controls Type
        '               The Controls belong to following three types - Range,List,None
        'Parameters:
        'Returns:       String
        'Notes:         This sub relies on the ResourceId and ModuleCode when
        '               when a request for the page is made.
        '               intRangeCount --- Holds the Total Count for Range Controls
        '               intListCount  --- Holds the Total Count for List Controls
        '               intNoneCount  --- Holds the Total Count for Controls with no validation
        '**************************************************************************************************
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim intCount As Integer
        'connect to the database

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            'With subqueries
            .Append("SELECT  Count(*) as StudentCount ")
            .Append("FROM  arStudent B ")
            .Append("WHERE B.SSN = ? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@SSN", SSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read()
            'set properties with data from DataReader
            intCount = dr("StudentCount")
        End While

        If Not dr.IsClosed Then dr.Close()

        Return intCount
    End Function
    Public Function GetAllEmployersForEmail(ByVal statusId As String, ByVal country As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT Distinct ")
            '.Append("       (EC.LastName + ', ' + EC.FirstName) As Name, ")
            .Append("       EC.LastName + ', ' + EC.FirstName + (Select Case Len(Coalesce(EC.WorkEmail, EC.HomeEmail, '')) when 0 then '(*)' else '' end) Name, ")
            .Append("       (Select Case Len(Coalesce(EC.WorkEmail, EC.HomeEmail, '')) when 0 then 0 else 1 end) As HasEmail, ")
            .Append("       Coalesce(EC.WorkEmail, EC.HomeEmail, '') as Email ")
            .Append("FROM   plEmployerContact EC ")
            .Append("WHERE ")
            '.Append(        (WorkEmail is not null OR HomeEmail Is not null) ")
            .Append("       1=1 ")

            '   check if the selection is "All Statuses"
            If Not statusId = Guid.Empty.ToString Then
                .Append("AND    EC.StatusId = ? ")
                db.AddParameter("@StatusId", statusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   check if the selection is "All Countries"
            If Not country = Guid.Empty.ToString Then
                .Append("AND    EC.Country = ? ")
                db.AddParameter("@Country", country, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   order by employer name
            .Append("ORDER BY Name ")

        End With

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    Public Function GetAllCountriesUsedByEmployers() As DataSet


        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT DISTINCT ")
            .Append("         PR.CountryId, PR.CountryDescrip ")
            .Append("FROM     plEmployerContact EC, adCountries PR, syStatuses ST ")
            .Append("WHERE    EC.Country=PR.CountryId ")
            .Append(" AND     PR.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY PR.CountryDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function StudentSearchResultsBySSN(ByVal strSSN As String) As DataSet
        '   connect to the database
        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" SELECT Distinct  S1.StudentId,  S1.LastName,S1.FirstName, ")
            .Append(" S1.SSN, ")
            .Append(" s3.PrgVerDescrip  ")
            .Append(" FROM  arStudent S1,arStuEnrollments s2,arPrgVersions s3 where  ")
            .Append(" s1.StudentId = s2.StudentId and s2.PrgVerId = s3.PrgVerId  ")
            .Append(" and SSN like  + '%' + ? + '%'")
            .Append(" ORDER BY S1.LastName ")

            db.AddParameter("@SSN", strSSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End With
        'Execute the query and return Dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
#Region "Get AdvAppsetting for Manage Config entry"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
End Class
