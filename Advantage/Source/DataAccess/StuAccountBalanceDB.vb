Imports FAME.Advantage.Common

Public Class StuAccountBalanceDB

#Region "Private Data Members"

    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")
    Private m_OrderAccountsReceivableDeferredIncome As String = MyAdvAppSettings.AppSettings("OrderAccountsReceivableDeferredIncome")
    Private m_DeferredRevenueCalcMethod As String = MyAdvAppSettings.AppSettings("DeferredRevenueCalcMethod")
#End Region


#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property
    Public ReadOnly Property OrderAccountsReceivableDeferredIncome() As String
        Get
            Return m_OrderAccountsReceivableDeferredIncome
        End Get
    End Property

#End Region


#Region "Public Methods"

    Public Function GetStuAccountBalance(ByVal paramInfo As ReportParamInfo, Optional ByVal bCreditOnly As Boolean = False) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStuID As String

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        ''If paramInfo.ResId = 275 Then
        ''    If paramInfo.OrderBy <> "" Then
        ''        If paramInfo.OrderBy.Contains("LastName") Then
        ''            strOrderBy &= "," & paramInfo.OrderBy & ",arStudent.FirstName,arStudent.MiddleName"
        ''        Else
        ''            strOrderBy &= "," & paramInfo.OrderBy & ",arStudent.LastName,arStudent.FirstName,arStudent.MiddleName"
        ''        End If

        ''    Else
        ''        strOrderBy &= ",arStudent.LastName,arStudent.FirstName,arStudent.MiddleName"
        ''    End If
        ''Else
        ''    If paramInfo.OrderBy <> "" Then
        ''        strOrderBy &= "," & paramInfo.OrderBy & ",arStudent.LastName,arStudent.FirstName,arStudent.MiddleName"
        ''    Else
        ''        strOrderBy &= ",arStudent.LastName,arStudent.FirstName,arStudent.MiddleName"
        ''    End If
        ''End If
        '' Commented By Vijay Ramteke on July 10, 2009 for Issue Id : 16589
        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy & ",arStudent.LastName,arStudent.FirstName,arStudent.MiddleName"
        Else
            strOrderBy &= ",arStudent.LastName,arStudent.FirstName,arStudent.MiddleName"
        End If

        If StudentIdentifier = "SSN" Then
            strStuID = "arStudent.SSN AS StudentIdentifier,"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStuID = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
        ElseIf StudentIdentifier = "StudentId" Then
            strStuID = "arStudent.StudentNumber AS StudentIdentifier,"

        End If

        If InStr(strWhere, "Balance") >= 1 And paramInfo.ResId = 275 Then
            Dim strOldString As String = "Balance"
            Dim strNewString As String = "(SELECT SUM(B.TransAmount) FROM saTransactions B WHERE B.StuEnrollId=A.StuEnrollId AND B.IsPosted=1 and B.Voided=0)"
            strWhere = Replace(strWhere, strOldString, strNewString)
        End If


        'Build SQL query with subqueries
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       A.StuEnrollId,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName," & strStuID)
            .Append("       (SELECT Phone FROM arStudentPhone WHERE arStudent.StudentId=arStudentPhone.StudentId AND arStudentPhone.default1=1) AS Phone,")
            .Append("       (SELECT ForeignPhone FROM arStudentPhone WHERE arStudent.StudentId=arStudentPhone.StudentId AND arStudentPhone.default1=1) AS ForeignPhone,")
            .Append("       arStuEnrollments.StartDate,")
            .Append("       (SELECT ShiftDescrip FROM arShifts WHERE ShiftId=arStuEnrollments.ShiftId) AS ShiftDescrip,")
            .Append("       (SELECT PrgVerDescrip FROM arPrgVersions WHERE PrgVerId=arStuEnrollments.PrgVerId) AS PrgVerDescrip,")
            .Append("       (SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip,")
            .Append("       (SELECT SUM(B.TransAmount) FROM saTransactions B WHERE B.StuEnrollId=A.StuEnrollId AND B.IsPosted=1 and B.Voided=0) AS Balance,")
            .Append("       syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId,syCampuses.CampDescrip ")
            .Append("FROM   saTransactions A,arStuEnrollments,arStudent,syCmpGrpCmps,syCampGrps,syCampuses ")
            .Append("WHERE  A.StuEnrollId=arStuEnrollments.StuEnrollId AND A.IsPosted=1 ")
            .Append("       AND arStuEnrollments.StudentId=arStudent.StudentId ")
            .Append("       AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId ")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
            .Append("       AND syCampuses.CampusId=arStuEnrollments.CampusId ")
            .Append("       AND A.Voided=0 ")
            If bCreditOnly Then
                .Append("  AND  ( SELECT    SUM(B.TransAmount) FROM      saTransactions B ")
                .Append(" WHERE B.StuEnrollId = A.StuEnrollId  AND B.IsPosted = 1 ")
                .Append(" AND B.Voided = 0  ) < 0.00 ")
            End If
            .Append(strWhere)
            .Append(" ORDER BY syCampGrps.CampGrpDescrip,syCmpGrpCmps.CampGrpId,syCampuses.CampDescrip,arStuEnrollments.CampusId")
            .Append(strOrderBy)
        End With

        'With sb
        '    .Append("SELECT DISTINCT A.StuEnrollId,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
        '    .Append(strStuID & "arStudent.HomePhone,arStudent.ForeignPhone,arStuEnrollments.StartDate,")
        '    .Append("(SELECT ShiftDescrip FROM arShifts WHERE ShiftId=arStuEnrollments.ShiftId) AS ShiftDescrip,")
        '    .Append("(SELECT PrgVerDescrip FROM arPrgVersions WHERE PrgVerId=arStuEnrollments.PrgVerId) AS PrgVerDescrip,")
        '    .Append("(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip,")
        '    .Append("(SELECT SUM(B.TransAmount) FROM saTransactions B WHERE B.StuEnrollId=A.StuEnrollId) AS Balance,")
        '    .Append("syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId,syCampuses.CampDescrip ")
        '    .Append("FROM saTransactions A,arStuEnrollments,arStudent,syCmpGrpCmps,syCampGrps,syCampuses ")
        '    .Append("WHERE A.StuEnrollId=arStuEnrollments.StuEnrollId ")
        '    .Append("AND arStuEnrollments.StudentId=arStudent.StudentId ")
        '    .Append("AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId ")
        '    .Append("AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
        '    .Append("AND syCampuses.CampusId=arStuEnrollments.CampusId ")
        '    .Append(strWhere)
        '    .Append(strOrderBy)
        'End With

        'With sb
        '    .Append("SELECT DISTINCT A.StuEnrollId,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
        '    .Append(strStuID & "arStudent.HomePhone,arStudent.ForeignPhone,arStuEnrollments.StartDate,")
        '    .Append("(SELECT ShiftDescrip FROM arShifts WHERE ShiftId=arStuEnrollments.ShiftId) AS ShiftDescrip,")
        '    .Append("(SELECT PrgVerDescrip FROM arPrgVersions WHERE PrgVerId=arStuEnrollments.PrgVerId) AS PrgVerDescrip,")
        '    .Append("(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip,")
        '    .Append("(SELECT SUM(B.TransAmount) FROM saTransactions B WHERE B.StuEnrollId=A.StuEnrollId) AS Balance, ")
        '    .Append("syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId,syCampuses.CampDescrip ")
        '    .Append("FROM saTransactions A,arStuEnrollments,arStudent,syCmpGrpCmps,syCampGrps,syCampuses ")
        '    .Append("WHERE A.StuEnrollId=arStuEnrollments.StuEnrollId ")
        '    .Append("AND arStuEnrollments.StudentId=arStudent.StudentId ")
        '    .Append("AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId ")
        '    .Append("AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
        '    .Append("AND syCampuses.CampusId=arStuEnrollments.CampusId ")
        '    .Append(strWhere)
        '    .Append(strOrderBy)
        'End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "StuAccountBalance"
            ' Add columns
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("AccountCount", System.Type.GetType("System.Int32")))
            '
            'Dim dt As DataTable = BuildReportParamsTable(paramInfo)
            'Dim dt2 As DataTable = dt.Copy
            'ds.Tables.Add(dt2)

            Dim dt As New DataTable("CampusTotals")
            dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CBalance", System.Type.GetType("System.Decimal")))
            ds.Tables.Add(dt)

            dt = New DataTable("CampusGroupTotals")
            dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CGBalance", System.Type.GetType("System.Decimal")))
            ds.Tables.Add(dt)
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    Public Function GetStuAccountBalanceMultipleEnrollments(ByVal paramInfo As ReportParamInfo) As DataSet
        Try
            Dim sb As New System.Text.StringBuilder
            Dim db As New DataAccess
            Dim ds As New DataSet
            Dim strWhere As String
            Dim strOrderBy As String
            Dim strStuID As String

            db.CommandTimeout = 10800

            If paramInfo.FilterList <> "" Then
                strWhere &= " AND " & paramInfo.FilterList
            End If

            If paramInfo.FilterOther <> "" Then
                strWhere &= " AND " & paramInfo.FilterOther
            End If

            If paramInfo.OrderBy <> "" Then
                strOrderBy &= "," & paramInfo.OrderBy & ",arStudent.LastName,arStudent.FirstName,arStudent.MiddleName"
            Else
                strOrderBy &= ",arStudent.LastName,arStudent.FirstName,arStudent.MiddleName"
            End If

            If StudentIdentifier = "SSN" Then
                strStuID = "arStudent.SSN AS StudentIdentifier,"
            ElseIf StudentIdentifier = "EnrollmentId" Then
                strStuID = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
            ElseIf StudentIdentifier = "StudentId" Then
                strStuID = "arStudent.StudentNumber AS StudentIdentifier,"
            End If

            'Build SQL query with subqueries
            With sb
                .Append("SELECT  ")
                .Append("		saTransactions.StuEnrollId, ")
                .Append("		arStudent.LastName, ")
                .Append("		arStudent.FirstName, ")
                .Append("		arStudent.MiddleName, " & strStuID)
                .Append("        (SELECT Phone FROM arStudentPhone WHERE arStudent.StudentId=arStudentPhone.StudentId AND arStudentPhone.default1=1) AS Phone, ")
                .Append("        (SELECT ForeignPhone FROM arStudentPhone WHERE arStudent.StudentId=arStudentPhone.StudentId AND arStudentPhone.default1=1) AS ForeignPhone, ")
                .Append("        arStuEnrollments.StartDate, ")
                .Append("        (SELECT ShiftDescrip FROM arShifts WHERE ShiftId=arStuEnrollments.ShiftId) AS ShiftDescrip, ")
                .Append("        (SELECT PrgVerDescrip FROM arPrgVersions WHERE PrgVerId=arStuEnrollments.PrgVerId) AS PrgVerDescrip, ")
                .Append("        (SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip, ")
                .Append("        (SELECT SUM(B.TransAmount) FROM saTransactions B WHERE B.StuEnrollId=saTransactions.StuEnrollId AND B.Voided=0 AND B.IsPosted=1) AS Balance, ")
                .Append("        syCmpGrpCmps.CampGrpId, ")
                .Append("		syCampGrps.CampGrpDescrip, ")
                .Append("		arStuEnrollments.CampusId, ")
                .Append("		syCampuses.CampDescrip ")
                .Append("FROM	saTransactions, arStuEnrollments, arStudent, syCmpGrpCmps, syCampGrps, syCampuses  ")
                .Append("WHERE ")
                .Append("		saTransactions.StuEnrollId=arStuEnrollments.StuEnrollId ")
                .Append("AND		arStuEnrollments.StudentId=arStudent.StudentId	 ")
                .Append("AND		saTransactions.IsPosted=1 ")
                .Append("AND		syCmpGrpCmps.CampusId = arStuEnrollments.CampusId  ")
                .Append("AND		syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId  ")
                .Append("AND		syCampuses.CampusId=arStuEnrollments.CampusId  ")
                .Append("AND		saTransactions.Voided=0  ")
                .Append("AND		arStudent.StudentId in ")
                .Append("		( ")
                .Append("			SELECT  ")
                .Append("					W.StudentId ")
                .Append("			FROM ")
                .Append("			( ")
                .Append("			SELECT  ")
                .Append("					S.StudentId, ")
                .Append("					S.LastName, ")
                .Append("					S.FirstName, ")
                .Append("					T.StuEnrollId, ")
                .Append("					Sum(T.TransAmount) as Amount ")
                .Append("			FROM	saTransactions T, arStuEnrollments SE, arStudent S ")
                .Append("			WHERE ")
                .Append("					T.StuEnrollId=SE.StuEnrollId ")
                .Append("			AND		SE.StudentId=S.StudentId	 ")
                .Append("			AND		T.IsPosted=1 ")
                .Append("			GROUP BY  ")
                .Append("					S.StudentId, S.LastName, S.FirstName, T.StuEnrollId ")
                .Append("			HAVING	Sum(T.TransAmount) <> 0.00 ")
                .Append("			) W ")
                .Append("			GROUP BY ")
                .Append("					W.StudentId ")
                .Append("			HAVING	count(*) > 1 ")
                .Append("		) ")
                .Append(strWhere)
                .Append("GROUP BY  ")
                .Append("		syCampGrps.CampGrpDescrip, syCmpGrpCmps.CampGrpId, syCampuses.CampDescrip, arStuEnrollments.CampusId, arStudent.StudentId, arStudent.SSN, arStudent.LastName, arStudent.FirstName, arStudent.MiddleName, saTransactions.StuEnrollId, arStuEnrollments.StartDate, arStuEnrollments.ShiftId, arStuEnrollments.PrgVerId, arStuEnrollments.StatusCodeId ")
                ''Added by Saraswathi to fix  error when studentidentifier is set to StudentID
                ''Added on July 2 - for patch build 2.2
                If StudentIdentifier = "StudentId" Then
                    .Append("	,arStudent.StudentNumber ")
                End If
            End With

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            ds = db.RunParamSQLDataSet(sb.ToString)

            If ds.Tables.Count > 0 Then
                ds.Tables(0).TableName = "StuAccountBalance"
                ' Add columns
                ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
                ds.Tables(0).Columns.Add(New DataColumn("AccountCount", System.Type.GetType("System.Int32")))

                Dim dt As New DataTable("CampusTotals")
                dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("CampusDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("CBalance", System.Type.GetType("System.Decimal")))
                ds.Tables.Add(dt)

                dt = New DataTable("CampusGroupTotals")
                dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("CGBalance", System.Type.GetType("System.Decimal")))
                ds.Tables.Add(dt)
            End If

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return ds
        Catch ex As Exception

        End Try

    End Function
    'Get all charges sorted by campus group, campus and program version
    Public Function GetRevenuePosted(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStuID As String

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy & ",arStudent.LastName,arStudent.FirstName,arStudent.MiddleName"
        Else
            strOrderBy &= ",arStudent.LastName,arStudent.FirstName,arStudent.MiddleName"
        End If

        If StudentIdentifier = "SSN" Then
            strStuID = "arStudent.SSN AS StudentIdentifier,"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStuID = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
        ElseIf StudentIdentifier = "StudentId" Then
            strStuID = "arStudent.StudentNumber AS StudentIdentifier,"
        End If

        'Build SQL query with subqueries
        With sb
            .Append("SELECT ")
            .Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,syCampuses.CampDescrip,")
            .Append("       PV.PrgVerDescrip,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName," & strStuID)
            .Append("       (SELECT TermDescrip FROM arTerm WHERE TermId=saTransactions.TermId) AS TermDescrip,")
            .Append("       (SELECT TransCodeDescrip FROM saTransCodes WHERE TransCodeId=saTransactions.TransCodeId) AS TransCodeDescrip,")
            .Append("       saTransactions.TransDate,saTransactions.TransDescrip,saTransactions.TransReference,saTransactions.TransAmount ")
            .Append("FROM   saTransactions,arStuEnrollments,arStudent,arPrgVersions PV,syCampGrps,syCmpGrpCmps,syCampuses ")
            .Append("WHERE  NOT EXISTS (SELECT * FROM saPayments WHERE TransactionId=saTransactions.TransactionId)")
            .Append("       AND NOT EXISTS (SELECT * FROM saRefunds WHERE TransactionId=saTransactions.TransactionId)")
            .Append("       AND saTransactions.StuEnrollId=arStuEnrollments.StuEnrollId AND arStuEnrollments.StudentId=arStudent.StudentId")
            .Append("       AND saTransactions.IsPosted=1 AND arStuEnrollments.PrgVerId=PV.PrgVerId")
            .Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=arStuEnrollments.CampusId")
            .Append("       AND saTransactions.Voided=0 ")
            .Append(strWhere)
            .Append(" ORDER BY syCampGrps.CampGrpDescrip,syCampGrps.CampGrpId,syCampuses.CampDescrip,syCampuses.CampusId,")
            .Append("       PV.PrgVerDescrip,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName")
            .Append(";")
            .Append("SELECT ")
            .Append("       (SELECT PrgVerDescrip FROM arPrgVersions WHERE PrgVerId=arStuEnrollments.PrgVerId) AS PrgVerDescrip,")
            .Append("       (SELECT TransCodeDescrip FROM saTransCodes WHERE TransCodeId=saTransactions.TransCodeId) AS TransCodeDescrip,")
            .Append("       SUM(saTransactions.TransAmount) AS TotalAmount ")
            .Append("FROM   saTransactions,arStuEnrollments,syCampGrps,syCmpGrpCmps,syCampuses ")
            .Append("WHERE  NOT EXISTS (SELECT * FROM saPayments WHERE TransactionId=saTransactions.TransactionId)")
            .Append("       AND NOT EXISTS (SELECT * FROM saRefunds WHERE TransactionId=saTransactions.TransactionId)")
            .Append("       AND saTransactions.StuEnrollId=arStuEnrollments.StuEnrollId AND saTransactions.IsPosted=1")
            .Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=arStuEnrollments.CampusId ")
            .Append("       AND saTransactions.Voided=0 ")
            .Append(strWhere)
            .Append(" GROUP BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,arStuEnrollments.PrgVerId,saTransactions.TransCodeId")
            .Append(" ORDER BY syCampGrps.CampGrpDescrip, syCampuses.CampDescrip, PrgVerDescrip, TransCodeDescrip")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "RevenuePosted"
            ds.Tables(1).TableName = "SummaryRevenuePosted"
            '
            ' Add columns
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("RecCount", System.Type.GetType("System.Int32")))
            '
            Dim dt As New DataTable("CampusGroupTotals")
            dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("TotalAmount", System.Type.GetType("System.Decimal")))
            ds.Tables.Add(dt)

            dt = New DataTable("CampusTotals")
            dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("TotalAmount", System.Type.GetType("System.Decimal")))
            ds.Tables.Add(dt)

            dt = New DataTable("PrgVerTotals")
            dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("PrgVerDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("TotalAmount", System.Type.GetType("System.Decimal")))
            ds.Tables.Add(dt)
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Public Function GetRevenuePostedSummary(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStuID As String

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        'Build SQL query with subqueries
        With sb
            .Append("SELECT ")
            .Append("       syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,")
            .Append("       (SELECT PrgVerDescrip FROM arPrgVersions WHERE PrgVerId=arStuEnrollments.PrgVerId) AS PrgVerDescrip,")
            .Append("       (SELECT TransCodeDescrip FROM saTransCodes WHERE TransCodeId=saTransactions.TransCodeId) AS TransCodeDescrip,")
            .Append("       SUM(saTransactions.TransAmount) AS TotalAmount ")
            .Append("FROM   saTransactions,arStuEnrollments,syCampGrps,syCmpGrpCmps,syCampuses ")
            .Append("WHERE  NOT EXISTS (SELECT * FROM saPayments WHERE TransactionId=saTransactions.TransactionId)")
            .Append("       AND NOT EXISTS (SELECT * FROM saRefunds WHERE TransactionId=saTransactions.TransactionId)")
            .Append("       AND saTransactions.StuEnrollId=arStuEnrollments.StuEnrollId AND saTransactions.IsPosted=1")
            .Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=arStuEnrollments.CampusId ")
            .Append("       AND saTransactions.Voided=0 ")
            .Append(strWhere)
            .Append(" GROUP BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,arStuEnrollments.PrgVerId,saTransactions.TransCodeId")
            .Append(" ORDER BY syCampGrps.CampGrpDescrip, syCampuses.CampDescrip, PrgVerDescrip, TransCodeDescrip")
            .Append(";")
            .Append("SELECT ")
            .Append("       CampGrpDescrip,CampDescrip,SUM(TotalAmount) AS SummaryAmount ")
            .Append("FROM   (SELECT syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,")
            .Append("               (SELECT PrgVerDescrip FROM arPrgVersions WHERE PrgVerId=arStuEnrollments.PrgVerId) AS PrgVerDescrip,")
            .Append("               (SELECT TransCodeDescrip FROM saTransCodes WHERE TransCodeId=saTransactions.TransCodeId) AS TransCodeDescrip,")
            .Append("               SUM(saTransactions.TransAmount) AS TotalAmount")
            .Append("        FROM   saTransactions,arStuEnrollments,syCampGrps,syCmpGrpCmps,syCampuses")
            .Append("        WHERE  NOT EXISTS (SELECT * FROM saPayments WHERE TransactionId=saTransactions.TransactionId)")
            .Append("               AND NOT EXISTS (SELECT * FROM saRefunds WHERE TransactionId=saTransactions.TransactionId)")
            .Append("               AND saTransactions.StuEnrollId=arStuEnrollments.StuEnrollId AND saTransactions.IsPosted=1")
            .Append("               AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId")
            .Append("               AND syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=arStuEnrollments.CampusId ")
            .Append("               AND saTransactions.Voided=0 ")
            .Append(strWhere)
            .Append("       GROUP BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,arStuEnrollments.PrgVerId,saTransactions.TransCodeId) X")
            .Append(" GROUP BY CampGrpDescrip,CampDescrip")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "SummaryRevenuePosted"
            ds.Tables(1).TableName = "SummaryTotals"
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Public Function GetChargesWithBalanceGreaterThanZero(ByVal stuEnrollId As String) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        With sb
            '.Append("SELECT R.*,(R.TransAmount - ISNULL(R.TotalApplied,0)) AS Balance ")
            '.Append("FROM ")
            '.Append("(SELECT t1.TransactionId,t1.TransDate,t2.TransCodeDescrip,t1.TransReference,t1.TransAmount, ")
            '.Append("   (SELECT SUM(Amount) ")
            '.Append("    FROM saAppliedPayments t3 ")
            '.Append("    WHERE t1.TransactionId = t3.ApplyToTransId) AS TotalApplied ")
            '.Append("FROM saTransactions t1, saTransCodes t2 ")
            '.Append("WHERE StuEnrollId = ? ")
            '.Append("AND t1.TransCodeId=t2.TransCodeId ")
            '.Append("AND t1.TransTypeId = 0) R ")
            '.Append("WHERE (R.TransAmount - ISNULL(R.TotalApplied,0)) > 0 ")
            '.Append("ORDER BY TransDate, TransCodeDescrip ")
            .Append("SELECT R.*,(R.TransAmount - ISNULL(R.TotalApplied,0)) AS Balance  ")
            .Append(" FROM  ")
            .Append("(SELECT t1.TransactionId,t1.TransDate,t1.TransReference,t1.TransAmount,t1.TransDescrip, ")
            .Append("      TransCodeDescrip = CASE  ")
            .Append("                                          WHEN LEN(t1.TransCodeId) > 0 THEN (SELECT t2.TransCodeDescrip ")
            .Append("                                          FROM saTransCodes t2 ")
            .Append("                                          WHERE t1.TransCodeId=t2.TransCodeId) ")
            .Append("                                    ELSE ")
            .Append("                                          t1.TransDescrip ")
            .Append("                                 END, ")
            .Append("      (SELECT ISNULL(SUM(Amount),0)  ")
            '.Append("     FROM saAppliedPayments t3  ")
            '.Append("     WHERE t1.TransactionId = t3.ApplyToTransId) AS TotalApplied  ")
            .Append("       FROM saAppliedPayments t3, saTransactions t4, saTransactions t5  ")
            .Append("       WHERE t1.TransactionId = t3.ApplyToTransId and t3.ApplyToTransId=t4.TransactionId and t4.Voided=0 and t3.TransactionId=t5.TransactionId and t5.Voided=0) AS TotalApplied  ")
            .Append("FROM saTransactions t1 ")
            .Append("WHERE StuEnrollId = ? ")
            .Append("AND t1.Voided=0 ")
            .Append("AND t1.TransAmount > 0) R ")
            .Append("WHERE (R.TransAmount - ISNULL(R.TotalApplied,0)) > 0  ")
            .Append("ORDER BY TransDate, TransCodeDescrip  ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        db.AddParameter("@SID", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function
    ''Added by Saraswathi lakshmanan on April 13 2010
    ''Converted to stored Procedure
    Public Function GetChargesWithBalanceGreaterThanZero_sp(ByVal stuEnrollId As String) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New SQLDataAccess
        Dim ds As New DataSet

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()
        db.AddParameter("@StuEnrollID", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        ds = db.RunParamSQLDataSet_SP("USP_SA_GetChargesWithBalanceGreaterThanZero")

        Return ds.Tables(0)
    End Function
    Public Function GetDeferredRevenue(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStuID As String
        Dim endDate As String
        Dim TransactionStartingfromDAteExists As Boolean = False
        Dim StrTransDate As String
        Dim TransDate As Date

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            'Parse FilterOther in order to get Deferred Revenue Date
            Dim idx, idx2, idx3 As Integer
            idx = paramInfo.FilterOther.IndexOf("BETWEEN")
            If idx <> -1 Then
                idx2 = paramInfo.FilterOther.IndexOf("AM")
                If idx2 <> -1 Then
                    endDate = paramInfo.FilterOther.Substring(idx + 9, idx2 + 2 - (idx + 8)).Trim
                Else
                    endDate = paramInfo.FilterOther.Substring(idx + 8).Trim
                End If
                idx3 = endDate.IndexOf(" ")
                If idx3 > 0 Then
                    endDate = endDate.Substring(0, idx3).Trim
                End If
            End If
            If paramInfo.FilterOther.Contains("TransactionDate:") Then
                TransactionStartingfromDAteExists = True
                Dim indx As Integer
                indx = paramInfo.FilterOther.LastIndexOf(":")
                StrTransDate = paramInfo.FilterOther.Substring(indx, paramInfo.FilterOther.Length - indx)
                StrTransDate = StrTransDate.Substring(1, StrTransDate.Length - 1)
                TransDate = CDate(StrTransDate)
                StrTransDate = Format(TransDate, "MM/dd/yyyy")

            End If


        Else
            'Use today's date as default Deferred Revenue Date
            endDate = Date.Today.ToShortDateString
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        If StudentIdentifier = "SSN" Then
            strStuID = "arStudent.SSN AS StudentIdentifier,"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStuID = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
        ElseIf StudentIdentifier = "StudentId" Then
            strStuID = "arStudent.StudentNumber AS StudentIdentifier,"
        End If

        'Build SQL query with subqueries
        Dim myCulture As New System.Globalization.CultureInfo("en-US", True)
        Dim dtEndDate As Date = Date.Parse(endDate, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
        Dim plusOneDay As Date = dtEndDate.AddDays(1)
        Dim firstOfMonth As Date = Convert.ToDateTime(dtEndDate.Month & "/01/" & dtEndDate.Year)

        With sb
            ''.Append("SELECT	")
            ''.Append("       syCampGrps.CampGrpId,")
            ''.Append("       syCampGrps.CampGrpDescrip,")
            ''.Append("       E.CampusId,")
            ''.Append("       syCampuses.CampDescrip,")
            ''.Append("       E.stuEnrollId,")
            ''.Append("       arStudent.LastName,")
            ''.Append("       arStudent.FirstName,")
            ''.Append("       arStudent.MiddleName," & strStuID)
            ''.Append(" syStatuscodes.SysStatusId,E.LDA,E.ExpGraddate,E.DateDetermined , ")
            ''.Append(" (select max(TransDate) from saTransactions where stuEnrollid=E.StuEnrollId ")
            '' ''Added by saraswathi on march 16 2009 to fix issue 15412 & 15413
            '' ''  .Append(" and TransTypeId<>2 ")

            ''.Append(" ) as LastTransactionDate,'" & firstOfMonth)
            ''.Append("' as SDate, ")
            ''.Append("       E.PrgVerId,")
            ''.Append("       (SELECT prgVerDescrip FROM arPrgVersions WHERE PrgVerId=E.PrgVerId) AS PrgVerDescrip,")
            ''.Append("       W.TransDescrip,")
            ''.Append("       W.TransCodeDescrip,")
            ''.Append("       PreviousCost=COALESCE(TotalCost,0)-COALESCE(PreviousAccrualBalance,0),")
            ' ''.Append("       PreviousAccrualBal=(COALESCE(PreviousAccrualBalance,0)),")
            ''.Append(" case when DefEarnings = 0 and Transdate < ?  then ")
            ''.Append(" COALESCE(TotalCost, 0) ")
            ''.Append(" else	COALESCE(PreviousAccrualBalance,0)  End as PreviousAccrualBal, ")
            ''.Append("       TotalCost=(COALESCE(TotalCost,0)),")
            ' ''.Append("       CurrentAccrual=(COALESCE(CurrentAccrual,0)),")
            ''.Append(" case when DefEarnings = 0 and Transdate >= ?  then ")
            ''.Append(" COALESCE(TotalCost, 0) ")
            ''.Append(" else	COALESCE(CurrentAccrual,0)  End as CurrentAccrual, ")

            ' ''.Append("       BalanceForward = COALESCE(TotalCost, 0) - COALESCE(PreviousAccrualBalance, 0) - COALESCE(CurrentAccrual, 0)")
            ''.Append(" case DefEarnings when 1 then ")
            ''.Append(" COALESCE(TotalCost, 0) - COALESCE(PreviousAccrualBalance, 0) - COALESCE(CurrentAccrual, 0) ")
            ''.Append(" else 0  End as  BalanceForward ")
            ''.Append(",(select isnull(sum(TransAmount),0) from saTransactions A where ")
            ''.Append(" A.stuEnrollid = E.StuEnrollId and A.Voided=0 and ")
            '' ''Added by saraswathi on march 16 2009 to fix issue 15412 & 15413
            ' ''.Append("  A.TransTypeId<>2   and ")

            ''.Append("  A.TransDate < ? ) as BalanceOwed ")
            ''.Append(" FROM	arStuEnrollments E,arStudent,syStatuscodes,syCmpGrpCmps,syCampGrps,syCampuses,")
            ''.Append("       (	SELECT	A.StuEnrollId,A.TransactionId,A.TransDescrip,B.TransCodeDescrip,")
            ''.Append("			        PreviousAccrualBalance=(SELECT  SUM(Amount) FROM    saDeferredRevenues")
            ''.Append("									        WHERE   TransactionId=A.TransactionId AND DefRevenueDate<?),")
            ''.Append("		            TotalCost=(A.TransAmount),")
            ''.Append("		            CurrentAccrual=(SELECT  SUM(Amount) FROM saDeferredRevenues ")
            ''.Append("                                   WHERE   TransactionId=A.TransactionId")
            ''.Append("						                    AND DefRevenueDate>=? AND DefRevenueDate<?),B.DefEarnings,A.TransDate  ")
            ''.Append("           FROM	saTransactions A,saTransCodes B,syCampGrps ")
            ''.Append("           WHERE   A.TransCodeId=B.TransCodeId AND ")
            ''If m_DeferredRevenueCalcMethod = "Hybrid" Then
            ''    .Append("  ( B.DefEarnings=1 or  B.IsInstCharge=1 )  ")
            ''Else
            ''    .Append(" B.DefEarnings=1 ")
            ''End If
            ''.Append(" AND A.IsPosted=1")
            ''.Append("                   AND A.Voided=0 ")

            ' ''Added by saraswathi on march 16 2009 to fix issue 15412 & 15413
            ''.Append(" and TransTypeId<>2    ")

            ' ''.Append("                   AND B.CampGrpId=syCampGrps.CampGrpId " & strWhere)
            ''.Append("                   AND B.CampGrpId=syCampGrps.CampGrpId and A.TransDate < ? ")
            ''.Append("       ) W ")
            ''.Append("WHERE  E.stuEnrollId=W.stuEnrollId")
            ''.Append("       AND E.StudentId=arStudent.StudentId")
            ''.Append("  and E.StatusCodeId=syStatuscodes.StatusCodeId ")
            ''.Append("       AND E.CampusId=syCampuses.CampusId")
            ''.Append("       AND E.CampusId=syCmpGrpCmps.CampusId")
            ''.Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId")
            ' ''.Append("       AND E.StartDate <= ? ")
            ''.Append(strWhere)
            ''.Append(" ORDER BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,PrgVerDescrip,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,TotalCost desc,StudentIdentifier")

            '' ''Added by Saraswathi lakshmanan
            '' ''to get the list of students

            ''.Append(";SELECT	distinct   E.StuEnrollid ")
            ''.Append(" FROM	arStuEnrollments E,arStudent,syStatuscodes,syCmpGrpCmps,syCampGrps,syCampuses, ")
            ''.Append("       (	SELECT	A.StuEnrollId  ")
            ''.Append("           FROM	saTransactions A,saTransCodes B,syCampGrps ")
            ''.Append("           WHERE   A.TransCodeId=B.TransCodeId AND ")
            ''If m_DeferredRevenueCalcMethod = "Hybrid" Then
            ''    .Append("  ( B.DefEarnings=1 or  B.IsInstCharge=1 )  ")
            ''Else
            ''    .Append(" B.DefEarnings=1 ")
            ''End If
            ''.Append(" AND A.IsPosted=1")
            ''.Append("                   AND A.Voided=0 ")
            '' ''Added by saraswathi on march 16 2009 to fix issue 15412 & 15413
            ' ''  .Append(" and TransTypeId<>2    ")
            ' ''.Append("                   AND B.CampGrpId=syCampGrps.CampGrpId " & strWhere)
            ''.Append("                   AND B.CampGrpId=syCampGrps.CampGrpId and A.TransDate < ? ")
            ''.Append("       ) W ")
            ''.Append("  WHERE  E.stuEnrollId=W.stuEnrollId       AND E.StudentId=arStudent.StudentId  ")
            ''.Append(" and E.StatusCodeId=syStatuscodes.StatusCodeId        AND E.CampusId=syCampuses.CampusId       AND E.CampusId=syCmpGrpCmps.CampusId       AND")
            ''.Append(" syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
            ''.Append(strWhere)
            ''.Append("  ORDER BY E.StuEnrollid")
            .Append("SELECT	")
            .Append("       syCampGrps.CampGrpId,")
            .Append("       syCampGrps.CampGrpDescrip,")
            .Append("       E.CampusId,")
            .Append("       syCampuses.CampDescrip,")
            .Append("       E.stuEnrollId,")
            .Append("       arStudent.LastName,")
            .Append("       arStudent.FirstName,")
            .Append("       arStudent.MiddleName," & strStuID)
            .Append(" syStatuscodes.SysStatusId,E.LDA,E.ExpGraddate,E.DateDetermined , ")
            .Append(" (select max(TransDate) from saTransactions where stuEnrollid=E.StuEnrollId ")
            ''Added by saraswathi on march 16 2009 to fix issue 15412 & 15413
            ''  .Append(" and TransTypeId<>2 ")

            .Append(" ) as LastTransactionDate,'" & firstOfMonth)
            .Append("' as SDate, ")
            .Append("       E.PrgVerId,")
            .Append("       (SELECT prgVerDescrip FROM arPrgVersions WHERE PrgVerId=E.PrgVerId) AS PrgVerDescrip,")
            .Append("       W.TransDescrip,")
            .Append("       W.TransCodeDescrip,")
            .Append("       PreviousCost=Case when COALESCE(PreviousAccrualBalance,0) = 0 then 0 else COALESCE(TotalCost,0)-COALESCE(PreviousAccrualBalance,0) end,")
            '.Append("       PreviousAccrualBal=(COALESCE(PreviousAccrualBalance,0)),")
            .Append(" case when DefEarnings = 0 and Transdate < ? ")

            If TransactionStartingfromDAteExists = True Then
                .Append("  and TransDate>='" & StrTransDate & "' ")
            End If

            .Append(" then ")
            .Append(" COALESCE(TotalCost, 0) ")
            .Append(" else	COALESCE(PreviousAccrualBalance,0)  End as PreviousAccrualBal, ")
            .Append("       TotalCost=(COALESCE(TotalCost,0)),")
            '.Append("       CurrentAccrual=(COALESCE(CurrentAccrual,0)),")
            .Append(" case when DefEarnings = 0 and Transdate >= ?  then ")
            .Append(" COALESCE(TotalCost, 0) ")
            .Append(" else	COALESCE(CurrentAccrual,0)  End as CurrentAccrual, ")

            '.Append("       BalanceForward = COALESCE(TotalCost, 0) - COALESCE(PreviousAccrualBalance, 0) - COALESCE(CurrentAccrual, 0)")
            .Append(" case DefEarnings when 1 then ")
            .Append(" COALESCE(TotalCost, 0) - COALESCE(PreviousAccrualBalance, 0) - COALESCE(CurrentAccrual, 0) ")
            .Append(" else 0  End as  BalanceForward ")
            .Append(",(select isnull(sum(TransAmount),0) from saTransactions A where ")
            .Append(" A.stuEnrollid = E.StuEnrollId and A.Voided=0 and ")
            ''Added by saraswathi on march 16 2009 to fix issue 15412 & 15413
            '.Append("  A.TransTypeId<>2   and ")

            .Append("  A.TransDate < ? ")
            If TransactionStartingfromDAteExists = True Then
                .Append("  and TransDate>='" & StrTransDate & "' ")
            End If
            .Append("  ) as BalanceOwed ")
            .Append("  ,	E.CohortStartDate ")
            ''Added by saraswathi on sep 23 2009 for Student GRoup in Filter section
            If paramInfo.ShowRptStudentGroup = True Then
                .Append(" ,adLeadGroups.Descrip  StudentGroup ")
            Else
                .Append(" ,''  StudentGroup ")
            End If

            .Append(" FROM	arStuEnrollments E,arStudent,syStatuscodes,syCmpGrpCmps,syCampGrps,syCampuses,")

            ''Added by saraswathi on sep 23 2009 for Student GRoup in Filter section
            If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Or paramInfo.ShowRptStudentGroup = True Then
                .Append("adLeadByLeadGroups, adLeadGroups, ")
            End If

            .Append("       (	SELECT	A.StuEnrollId,A.TransactionId,A.TransDescrip,B.TransCodeDescrip,")
            .Append("			        PreviousAccrualBalance=(SELECT  SUM(Amount) FROM    saDeferredRevenues")
            .Append("									        WHERE   TransactionId=A.TransactionId AND DefRevenueDate<?),")
            .Append("		            TotalCost=(A.TransAmount),")
            .Append("		            CurrentAccrual=(SELECT  SUM(Amount) FROM saDeferredRevenues ")
            .Append("                                   WHERE   TransactionId=A.TransactionId")
            .Append("						                    AND DefRevenueDate>=? AND DefRevenueDate<?),B.DefEarnings,A.TransDate  ")
            .Append("           FROM	saTransactions A,saTransCodes B,syCampGrps ")
            .Append("           WHERE   A.TransCodeId=B.TransCodeId AND ")
            If m_DeferredRevenueCalcMethod = "Hybrid" Then
                .Append("  ( B.DefEarnings=1 or  B.IsInstCharge=1 )  ")
            Else
                .Append(" B.DefEarnings=1 ")
            End If
            .Append(" AND A.IsPosted=1")
            .Append("                   AND A.Voided=0 ")

            'Added by saraswathi on march 16 2009 to fix issue 15412 & 15413
            .Append(" and TransTypeId<>2    ")

            '.Append("                   AND B.CampGrpId=syCampGrps.CampGrpId " & strWhere)
            .Append("                   AND B.CampGrpId=syCampGrps.CampGrpId and A.TransDate < ? ")

            If TransactionStartingfromDAteExists = True Then
                .Append("  and A.TransDate>='" & StrTransDate & "' ")
            End If

            .Append("       ) W ")
            .Append("WHERE  E.stuEnrollId=W.stuEnrollId")
            .Append("       AND E.StudentId=arStudent.StudentId")
            .Append("  and E.StatusCodeId=syStatuscodes.StatusCodeId ")
            .Append("       AND E.CampusId=syCampuses.CampusId")
            .Append("       AND E.CampusId=syCmpGrpCmps.CampusId")
            .Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId")

            ''Added by saraswathi on sep 23 2009 for Student GRoup in Filter section
            If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Or paramInfo.ShowRptStudentGroup = True Then
                .Append("       AND ((adLeadGroups.CampGrpId=syCmpGrpCmps.CampGrpId) ")
                .Append(" or (adLeadGroups.CampGrpId =(select CampGrpId from syCampGrps where IsAllCampusGrp=1)))")
                .Append("       AND adLeadGroups.UseForScheduling=1")
                .Append("AND adLeadByLeadGroups.LeadGrpId=adLeadGroups.LeadGrpId ")
                .Append("AND adLeadByLeadGroups.StuEnrollId=E.StuEnrollId ")
            End If

            '.Append("       AND E.StartDate <= ? ")
            .Append(strWhere)
            .Append(" ORDER BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,PrgVerDescrip,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,TotalCost desc,StudentIdentifier")

            If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Or paramInfo.ShowRptStudentGroup = True Then
                .Append(" ,adLeadGroups.LeadGrpId ")
            End If

            ''Added by Saraswathi lakshmanan
            ''to get the list of students

            .Append(";SELECT	distinct   E.StuEnrollid ")
            .Append(" FROM	arStuEnrollments E,arStudent,syStatuscodes,syCmpGrpCmps,syCampGrps,syCampuses,  ")

            ''Added by saraswathi on sep 23 2009 for Student GRoup in Filter section
            If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Or paramInfo.ShowRptStudentGroup = True Then
                .Append("adLeadByLeadGroups, adLeadGroups, ")
            End If


            .Append("       (	SELECT	A.StuEnrollId  ")
            .Append("           FROM	saTransactions A,saTransCodes B,syCampGrps ")
            .Append("           WHERE   A.TransCodeId=B.TransCodeId AND ")
            If m_DeferredRevenueCalcMethod = "Hybrid" Then
                .Append("  ( B.DefEarnings=1 or  B.IsInstCharge=1 )  ")
            Else
                .Append(" B.DefEarnings=1 ")
            End If
            .Append(" AND A.IsPosted=1")
            .Append("                   AND A.Voided=0 ")
            ''Added by saraswathi on march 16 2009 to fix issue 15412 & 15413
            '  .Append(" and TransTypeId<>2    ")
            '.Append("                   AND B.CampGrpId=syCampGrps.CampGrpId " & strWhere)
            .Append("                   AND B.CampGrpId=syCampGrps.CampGrpId and A.TransDate < ?  ")

            If TransactionStartingfromDAteExists = True Then
                .Append("  and A.TransDate>='" & StrTransDate & "' ")
            End If

            .Append("       ) W ")
            .Append("  WHERE  E.stuEnrollId=W.stuEnrollId       AND E.StudentId=arStudent.StudentId  ")
            .Append("  AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId  ")
            ''Added by saraswathi on sep 23 2009 for Student GRoup in Filter section
            If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Or paramInfo.ShowRptStudentGroup = True Then
                .Append("       AND ((adLeadGroups.CampGrpId=syCmpGrpCmps.CampGrpId)")
                .Append(" or (adLeadGroups.CampGrpId =(select CampGrpId from syCampGrps where IsAllCampusGrp=1)))")
                .Append("       AND adLeadGroups.UseForScheduling=1")
                .Append("AND adLeadByLeadGroups.LeadGrpId=adLeadGroups.LeadGrpId ")
                .Append("AND adLeadByLeadGroups.StuEnrollId=E.StuEnrollId ")
            End If

            .Append(" and E.StatusCodeId=syStatuscodes.StatusCodeId        AND E.CampusId=syCampuses.CampusId       AND E.CampusId=syCmpGrpCmps.CampusId       AND")
            .Append(" syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")

            .Append(strWhere)
            .Append("  ORDER BY E.StuEnrollid")


        End With


        db.AddParameter("@DefRevDate", firstOfMonth, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@DefRevDate", firstOfMonth, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@DefRevDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@DefRevDate", firstOfMonth, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@DefRevDate", firstOfMonth, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@DefRevDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@DefRevDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'db.AddParameter("@DefRevDate", firstOfMonth, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

        db.AddParameter("@DefRevDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = DeleteStudent(db.RunParamSQLDataSet(sb.ToString))

        ''Added by Saraswathi lakshmanan on April 23 2009
        ''The students with Current Accural and  balance Forward=0  are not shown for Accural Method
        ''And All the students are listed even if, they don't contribute towards deferred revenue in Hybrid Method.
        ''There are two types in DeferredRevenueCalcMethod. Accrual and Hybrid.
        'Only Ross uses Hybrid method. All the other Schools use Accrual method.
        'In Accrual method: 
        'We show the transactions of all the students whose transaction dates are less than or Equal to that selected month.
        'We donot filter out any students based on the status or Dates.If there is a prior transaction for the student , the student will show up.

        'For Hybrid method:
        'We show the institutional charges also.
        'if, the students staus is Nostart, Dropped or Graduated,and the balance owed is zero, and there is not transaction in the current month, and if the Graduated date or datedetermined is also in the prior months, then we hide those students.
        'These students are not shown in the report.
        'In addition if the Current Accrual and balance forward is zero we hide the students.
        ''This logic is Now applied to accrual and Hybrid method.
        ''Modified by Saraswathi lakshmanan on Sept 28 2009
        ''for mantis case 17687


        'If m_DeferredRevenueCalcMethod = "Hybrid" Then
        Dim i As Integer
        Dim j As Integer
        Dim dr() As DataRow
        Dim booldelete As Boolean
        For i = 0 To ds.Tables(1).Rows.Count - 1

            dr = ds.Tables(0).Select("StuEnrollid='" & ds.Tables(1).Rows(i)("StuEnrollid").ToString & "'")
            If dr.Length > 0 Then
                booldelete = False
                For j = 0 To dr.Length - 1
                    If dr(j)("CurrentAccrual") <> 0 Or dr(j)("BalanceForward") <> 0 Then
                        booldelete = False
                        Exit For
                    Else
                        booldelete = True
                    End If
                Next j
                If booldelete = True Then
                    Dim rowcount As Integer = dr.Length - 1
                    For j = rowcount To 0 Step -1
                        dr(j).Delete()
                        ds.AcceptChanges()
                    Next j
                End If
            End If
        Next
        '  End If

        ds.Tables.Remove(ds.Tables(1))

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "DeferredRevenueDetail"

            '
            ' Add columns
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("RecCount", System.Type.GetType("System.Int32")))
            'ds.Tables(0).Columns.Add(New DataColumn("CohortStartDate", System.Type.GetType("System.DateTime")))

            'Campus Group Totals
            Dim dt As New DataTable("CampusGroupTotals")
            dt.Columns.Add(New DataColumn("CampGrpId", System.Type.GetType("System.Guid")))
            dt.Columns.Add(New DataColumn("PreviousCost", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("PreviousAccrualBal", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TotalCost", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CurrentAccrual", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("BalanceForward", System.Type.GetType("System.Decimal")))
            ds.Tables.Add(dt)


            'Campus Totals
            dt = New DataTable("CampusTotals")
            dt.Columns.Add(New DataColumn("CampGrpId", System.Type.GetType("System.Guid")))
            dt.Columns.Add(New DataColumn("CampusId", System.Type.GetType("System.Guid")))
            dt.Columns.Add(New DataColumn("PreviousCost", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("PreviousAccrualBal", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TotalCost", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CurrentAccrual", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("BalanceForward", System.Type.GetType("System.Decimal")))
            ds.Tables.Add(dt)

            'Program Version Totals
            dt = New DataTable("PrgVerTotals")
            dt.Columns.Add(New DataColumn("CampGrpId", System.Type.GetType("System.Guid")))
            dt.Columns.Add(New DataColumn("CampusId", System.Type.GetType("System.Guid")))
            dt.Columns.Add(New DataColumn("PrgVerId", System.Type.GetType("System.Guid")))
            dt.Columns.Add(New DataColumn("PreviousCost", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("PreviousAccrualBal", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TotalCost", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CurrentAccrual", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("BalanceForward", System.Type.GetType("System.Decimal")))
            ds.Tables.Add(dt)
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    Private Function DeleteStudent(ByVal ds As DataSet) As DataSet
        Dim deferredRevenueCalcMethod As String = MyAdvAppSettings.AppSettings("DeferredRevenueCalcMethod")
        Dim result As Boolean = False
        ''This logic is Now applied to accrual and Hybrid method.
        ''Modified by Saraswathi lakshmanan on Sept 28 2009
        ''for mantis case 17687
        ' If deferredRevenueCalcMethod = "Hybrid" Then
        For Each dr As DataRow In ds.Tables(0).Rows
            'Compute Total Amounts per Campus Groups and Campuses.
            'CampGrpDescrip and CampDescrip columns cannot be NULL.
            result = False
            If dr("sysStatusId") = 12 Or dr("sysStatusId") = 14 Or dr("sysStatusId") = 8 Then
                result = ExcludeStudent(dr("sysStatusId"), dr("SDate"), dr("LDA").ToString, dr("ExpGradDate").ToString, dr("DateDetermined").ToString, dr("BalanceOwed"), dr("LastTransactionDate"))
            End If
            If result Then

                dr.Delete()
            End If

        Next
        ds.Tables(0).AcceptChanges()
        ' End If
        Return ds
    End Function
    Private Function ExcludeStudent(ByVal sysStatusId As Integer, ByVal SDate As String, ByVal m_LDA As String, ByVal m_ExpGradDate As String, ByVal m_DateDetermined As String, ByVal BalanceOwed As Decimal, ByVal LastTransactionDate As Date) As Boolean
        Dim rtn As Boolean = False
        Dim dDate As Date
        Try

            Dim LDA As Date
            Dim ExpGradDate As Date
            Dim DateDetermined As Date
            If m_LDA.ToString = "" Then
                LDA = Date.Parse("1/1/1800")
            Else
                LDA = Date.Parse(m_LDA)
            End If

            If m_ExpGradDate.ToString = "" Then
                ExpGradDate = Date.Parse("1/1/1800")
            Else
                ExpGradDate = Date.Parse(m_ExpGradDate)
            End If

            If m_DateDetermined.ToString = "" Then
                DateDetermined = Date.Parse("1/1/1800")
            Else
                DateDetermined = Date.Parse(m_DateDetermined)
            End If



            If LDA > Date.Parse(SDate) And LDA <> "1/1/1800" Then
                dDate = LDA
            Else
                ''Modified by Saraswathi lakshmanan on july 29 2009
                ''The code was not handling no start students and LDA being Empty
                If sysStatusId = 14 And ExpGradDate <> "1/1/1800" Then
                    dDate = ExpGradDate
                ElseIf sysStatusId = 12 And DateDetermined <> "1/1/1800" Then
                    dDate = DateDetermined
                ElseIf sysStatusId = 8 Then
                    dDate = LDA
                Else
                    Return False
                End If
            End If
            ''This Balance Owed is modified to <=0
            ''Modified by Saraswathi lakshmanan on Sept 28 2009
            ''for mantis case 17687

            If BalanceOwed <= 0 And LastTransactionDate <= Date.Parse(SDate) And dDate < Date.Parse(SDate) Then
                rtn = True
            End If


        Catch ex As Exception
            Dim strErr As String = ex.Message
        End Try
        Return rtn
    End Function
    Public Function GetDeferredIncome(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStuID As String
        Dim endDate As String

        Dim TransactionStartingfromDAteExists As Boolean = False
        Dim StrTransDate As String
        Dim TransDate As Date


        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            'Parse FilterOther in order to get Deferred Revenue Date
            Dim idx, idx2, idx3 As Integer
            idx = paramInfo.FilterOther.IndexOf("BETWEEN")
            If idx <> -1 Then
                idx2 = paramInfo.FilterOther.IndexOf("AM")
                If idx2 <> -1 Then
                    endDate = paramInfo.FilterOther.Substring(idx + 9, idx2 + 2 - (idx + 8)).Trim
                Else
                    endDate = paramInfo.FilterOther.Substring(idx + 8).Trim
                End If
                idx3 = endDate.IndexOf(" ")
                If idx3 > 0 Then
                    endDate = endDate.Substring(0, idx3).Trim
                End If
            End If

            'If paramInfo.FilterOther.Contains("TransactionDate:") Then
            '    TransactionStartingfromDAteExists = True
            '    Dim indx As Integer
            '    indx = paramInfo.FilterOther.LastIndexOf(":")
            '    StrTransDate = paramInfo.FilterOther.Substring(indx, paramInfo.FilterOther.Length - indx)
            '    StrTransDate = StrTransDate.Substring(1, StrTransDate.Length - 1)
            '    TransDate = CDate(StrTransDate)
            '    StrTransDate = Format(TransDate, "MM/dd/yyyy")

            'End If



        Else
            'Use today's date as default Deferred Revenue Date
            endDate = Date.Today.ToShortDateString
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        If StudentIdentifier = "SSN" Then
            strStuID = "arStudent.SSN AS StudentIdentifier,"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStuID = "E.EnrollmentId AS StudentIdentifier,"
        ElseIf StudentIdentifier = "StudentId" Then
            strStuID = "arStudent.StudentNumber AS StudentIdentifier,"
        End If

        'Build SQL query with subqueries

        Dim myCulture As New System.Globalization.CultureInfo("en-US", True)
        Dim dtEndDate As Date = Date.Parse(endDate, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
        Dim plusOneDay As Date = dtEndDate.AddDays(1)
        Dim firstOfMonth As Date = Convert.ToDateTime(dtEndDate.Month & "/01/" & dtEndDate.Year)

        Try
            With sb
                .Append(" select W.*,isnull((TotalCosts-BalanceOwed),0) as AppliedToBalance, ")
                .Append(" CASE  WHEN isnull((Accrued-(TotalCosts-BalanceOwed)),0) < 0  THEN 0 ")
                .Append(" ELSE isnull((Accrued-(TotalCosts-BalanceOwed)),0)     End as AccountsReceivable,")
                .Append(" CASE  WHEN isnull(((TotalCosts-BalanceOwed)-Accrued),0) < 0  THEN 0 ")
                .Append(" ELSE isnull(((TotalCosts-BalanceOwed)-Accrued),0)     End as DeferredIncome ")
                .Append(" from ( ")
                .Append(" select arStudent.StudentNumber,E.StuEnrollId," & strStuID)
                .Append(" arStudent.LastName, arStudent.FirstName, ")
                .Append(" arStudent.MiddleName,StatusCodeDescrip, ")
                .Append(" syStatuscodes.SysStatusId,E.LDA,E.ExpGraddate,E.DateDetermined , ")
                .Append(" (select max(TransDate) from saTransactions where stuEnrollid=E.StuEnrollId) as LastTransactionDate,'" & firstOfMonth)
                .Append("' as SDate, (SELECT prgVerDescrip FROM arPrgVersions WHERE PrgVerId=E.PrgVerId) AS PrgVerDescrip,syCampGrps.CampGrpDescrip, ")
                .Append(" syCampuses.CampDescrip,  syCampGrps.CampGrpId,  E.CampusId, E.PrgVerId ,")
                .Append(" (select isnull(sum(TransAmount),0) from saTransactions A,saTransCodes B where  ")
                .Append(" A.Transcodeid=B.TranscodeId and B.IsInstCharge=1 and ")
                .Append(" stuEnrollid = E.StuEnrollId and  A.Voided=0")
                ''Added by saraswathi on march 16 2009 to fix issue 15412 & 15413
                .Append("  and A.TransTypeId<>2    ")

                If TransactionStartingfromDAteExists = True Then
                    .Append("  and TransDate>='" & StrTransDate & "' ")
                End If

                .Append("  and TransDate < ? ")



                .Append(" ) as TotalCosts, ")
                .Append(" (select isnull(sum(TransAmount),0) from saTransactions A where ")
                .Append(" A.stuEnrollid = E.StuEnrollId and A.Voided=0 ")
                ''Commented by Saraswathi lakshmanan
                '' ''Added by saraswathi on march 16 2009 to fix issue 15412 & 15413
                ''.Append("  and A.TransTypeId<>2    ")
                If TransactionStartingfromDAteExists = True Then
                    .Append("  and A.TransDate>='" & StrTransDate & "' ")
                End If
                .Append(" and  A.TransDate < ? ")

                .Append("  ) as BalanceOwed, ")
                .Append(" ((select isnull(sum(A.Amount),0) from saDeferredRevenues A,saTransactions B ")
                .Append(" where A.Transactionid = B.Transactionid And B.StuEnrollId = E.StuEnrollId ")
                .Append(" and B.Voided=0 ")
                ''Added by saraswathi on march 16 2009 to fix issue 15412 & 15413
                ''.Append("  and B.TransTypeId<>2    ")

                .Append(" and A.DefRevenueDate < ? ) + ")
                .Append(" ( select isnull(sum(A.TransAmount),0) from saTransactions A,saTranscodes B ")
                .Append(" where A.Transcodeid = B.Transcodeid And A.StuEnrollId = E.StuEnrollId  ")
                ''Added by saraswathi on march 16 2009 to fix issue 15412 & 15413
                '' .Append("  and A.TransTypeId<>2    ")

                .Append(" and A.Voided=0 and B.IsInstCharge=1 and B.DefEarnings=0 ")
                If TransactionStartingfromDAteExists = True Then
                    .Append("  and A.TransDate>='" & StrTransDate & "' ")
                End If
                .Append(" and A.TransDate < ? ")

                .Append("  ))")
                .Append(" as Accrued ")
                .Append(" from ")
                .Append(" arStuEnrollments E, arStudent, syCmpGrpCmps, syCampGrps, syCampuses, syStatuscodes ")
                .Append(" WHERE E.StudentId = arStudent.StudentId And E.CampusId = syCampuses.CampusId And E.CampusId = syCmpGrpCmps.CampusId ")
                .Append(" AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId    ")
                .Append(" and E.StatusCodeId=syStatuscodes.StatusCodeId ")
                '.Append(" and E.StartDate <= ? ")
                .Append(strWhere)
                .Append(" ) W where LastTransactionDate is not null ")

                If StudentIdentifier = "StudentId" And OrderAccountsReceivableDeferredIncome = "Numeric" Then
                    .Append(" ORDER BY CampGrpDescrip,CampDescrip, cast(StudentNumber as int) ")
                Else
                    .Append(" ORDER BY CampGrpDescrip,CampDescrip, LastName, FirstName, MiddleName ")
                End If
            End With



            db.AddParameter("@DefRevDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            db.AddParameter("@DefRevDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            db.AddParameter("@DefRevDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            db.AddParameter("@DefRevDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            'db.AddParameter("@DefRevDate", firstOfMonth, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            ''modified by saraswathi lakshmanan on march 17 2009
            ''copied the code as in deferred revenue
            ''i.e Any enrollment with a status mapped to graduate or drop with a zero balance is only shown if it has transactions during the report period.  For example, if the report is being run for 4/1/2008 to 4/30/2008 it should only show students with a drop or graduated status with zero balances if they had at least one transaction during 4/1/2008 to 4/30/2008.


            '  ds = db.RunParamSQLDataSet(sb.ToString)
            ds = DeleteStudent(db.RunParamSQLDataSet(sb.ToString))
            If ds.Tables.Count > 0 Then
                ds.Tables(0).TableName = "DeferredRevenueDetail"
                '
                ' Add columns
                ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
                'ds.Tables(0).Columns.Add(New DataColumn("RecCount", System.Type.GetType("System.Int32")))


            End If

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            'Dim url As String = HttpContext.Current.Server.MapPath("ErrorPage.aspx")
            'HttpContext.Current.Session("Error") = ex.InnerException.Message.ToString
            HttpContext.Current.Session("Error") = "The error occured since the config entry for 'OrderAccountsReceivableDeferredIncome' is set to 'Numeric' and the 'StudentIdentifier' is set to 'StudentId',but the database has Alphanumeric value for studentId"

            HttpContext.Current.Response.Redirect("../ErrorPage.aspx")
        End Try
        Return ds

    End Function
    Public Function GetDeferredRevenueSummary(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStuID As String
        Dim endDate As String

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            'Parse FilterOther in order to get Deferred Revenue Date
            Dim idx, idx2, idx3 As Integer
            idx = paramInfo.FilterOther.IndexOf("BETWEEN")
            If idx <> -1 Then
                idx2 = paramInfo.FilterOther.IndexOf("AM")
                If idx2 <> -1 Then
                    endDate = paramInfo.FilterOther.Substring(idx + 9, idx2 + 2 - (idx + 8)).Trim
                Else
                    endDate = paramInfo.FilterOther.Substring(idx + 8).Trim
                End If
                idx3 = endDate.IndexOf(" ")
                If idx3 > 0 Then
                    endDate = endDate.Substring(0, idx3).Trim
                End If
            End If
        Else
            'Use today's date as default Deferred Revenue Date
            endDate = Date.Today.ToShortDateString
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        If StudentIdentifier = "SSN" Then
            strStuID = "arStudent.SSN"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStuID = "E.EnrollmentId"
        ElseIf StudentIdentifier = "StudentId" Then
            strStuID = "arStudent.StudentNumber"
        End If

        'Build SQL query with subqueries
        With sb
            .Append("SELECT * ")
            .Append("FROM (")
            .Append("   SELECT	syCampGrps.CampGrpId,")
            .Append("          syCampGrps.CampGrpDescrip,")
            .Append("          E.CampusId,")
            .Append("          syCampuses.CampDescrip,")
            .Append("          E.PrgVerId,")
            .Append("          (SELECT PrgVerDescrip FROM arPrgVersions WHERE PrgVerId=E.PrgVerId) AS PrgVerDescrip,")
            .Append("          E.stuEnrollId,")
            .Append("          arStudent.LastName,")
            .Append("          arStudent.FirstName,")
            .Append("          arStudent.MiddleName,")
            .Append(strStuID & " AS StudentIdentifier,")
            .Append("          PreviousCost=coalesce(SUM(TotalCost),0)-coalesce(SUM(NewCosts),0)-coalesce(SUM(PreviousAccrualBalance),0),")
            .Append("          PreviousAccrualBal=coalesce(SUM(PreviousAccrualBalance),0),")
            .Append("          NewCosts=coalesce(SUM(NewCosts),0),")
            .Append("          TotalCost=coalesce(SUM(TotalCost),0),")
            .Append("          CurrentAccrual=coalesce(SUM(CurrentAccrual),0),")
            .Append("          BalanceForward = coalesce(SUM(TotalCost),0)-coalesce(SUM(NewCosts),0)-coalesce(SUM(PreviousAccrualBalance),0)-coalesce(SUM(CurrentAccrual), 0) ")
            .Append("   FROM	arStuEnrollments E,arStudent,syCmpGrpCmps,syCampGrps,syCampuses,")
            .Append("          (	SELECT	A.StuEnrollId,A.TransactionId,A.TransDescrip,")
            .Append("                      PreviousAccrualBalance=(SELECT  SUM(Amount)")
            .Append("                                              FROM    saDeferredRevenues")
            .Append("                                              WHERE   TransactionId=A.TransactionId AND DefRevenueDate<?),")
            .Append("                      NewCosts=(  SELECT  SUM(TransAmount) FROM saTransactions X,saTransCodes Y ")
            .Append("                                  WHERE   X.TransCodeId=Y.TransCodeId AND Y.DefEarnings=1 AND X.IsPosted=1")
            .Append("                                          AND X.StuEnrollId=A.StuEnrollId AND X.TransactionId<>A.TransactionId")
            .Append("                                          AND NOT EXISTS (SELECT * FROM saDeferredRevenues DR WHERE DR.TransactionId=X.TransactionId)")
            .Append("                                          AND X.TransDate>=? AND X.TransDate<?),")
            .Append("                      TotalCost=(A.TransAmount),")
            .Append("                      CurrentAccrual=(SELECT  SUM(Amount) FROM saDeferredRevenues ")
            .Append("                                      WHERE   TransactionId=A.TransactionId")
            .Append("                                              AND DefRevenueDate>=? AND DefRevenueDate<?)")
            .Append("              FROM	saTransactions A,saTransCodes B,syCampGrps ")
            .Append("              WHERE   A.TransCodeId=B.TransCodeId AND B.DefEarnings=1 And A.IsPosted=1")
            .Append("                      AND A.Voided=0 ")
            '.Append("                      AND B.CampGrpId=syCampGrps.CampGrpId" & strWhere)
            .Append("                      AND B.CampGrpId=syCampGrps.CampGrpId")
            .Append("          ) W ")
            .Append("   WHERE  E.stuEnrollId=W.stuEnrollId")
            .Append("          AND E.StudentId=arStudent.StudentId")
            .Append("          AND E.CampusId=syCampuses.CampusId")
            .Append("          AND E.CampusId=syCmpGrpCmps.CampusId")
            .Append("          AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId")
            .Append(strWhere)
            .Append("   GROUP BY syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,E.CampusId,syCampuses.CampDescrip,")
            .Append("               E.PrgVerId,E.stuEnrollId,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName," & strStuID)
            .Append("   ) Q ")
            .Append("   WHERE  NOT (PreviousCost=0 and PreviousAccrualBal=0 and NewCosts=0 and TotalCost=0 and CurrentAccrual=0 and BalanceForward=0)")
            .Append("   ORDER BY CampGrpDescrip,CampDescrip,PrgVerDescrip,LastName,FirstName,MiddleName,StudentIdentifier")
        End With

        Dim myCulture As New System.Globalization.CultureInfo("en-US", True)
        Dim dtEndDate As Date = Date.Parse(endDate, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
        Dim plusOneDay As Date = dtEndDate.AddDays(1)
        Dim firstOfMonth As Date = Convert.ToDateTime(dtEndDate.Month & "/01/" & dtEndDate.Year)

        db.AddParameter("@DefRevDate", firstOfMonth, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@DefRevDate", firstOfMonth, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@DefRevDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@DefRevDate", firstOfMonth, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@DefRevDate", plusOneDay, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "DeferredRevenueSummary"
            '
            ' Add columns
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("RecCount", System.Type.GetType("System.Int32")))

            'Campus Group Totals
            Dim dt As New DataTable("CampusGroupTotals")
            dt.Columns.Add(New DataColumn("CampGrpId", System.Type.GetType("System.Guid")))
            dt.Columns.Add(New DataColumn("PreviousCost", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("PreviousAccrualBal", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("NewCosts", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TotalCost", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CurrentAccrual", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("BalanceForward", System.Type.GetType("System.Decimal")))
            ds.Tables.Add(dt)

            'Campus Totals
            dt = New DataTable("CampusTotals")
            dt.Columns.Add(New DataColumn("CampGrpId", System.Type.GetType("System.Guid")))
            dt.Columns.Add(New DataColumn("CampusId", System.Type.GetType("System.Guid")))
            dt.Columns.Add(New DataColumn("PreviousCost", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("PreviousAccrualBal", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("NewCosts", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TotalCost", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CurrentAccrual", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("BalanceForward", System.Type.GetType("System.Decimal")))
            ds.Tables.Add(dt)

            'Program Version Totals
            dt = New DataTable("PrgVerTotals")
            dt.Columns.Add(New DataColumn("CampGrpId", System.Type.GetType("System.Guid")))
            dt.Columns.Add(New DataColumn("CampusId", System.Type.GetType("System.Guid")))
            dt.Columns.Add(New DataColumn("PrgVerId", System.Type.GetType("System.Guid")))
            dt.Columns.Add(New DataColumn("PreviousCost", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("PreviousAccrualBal", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("NewCosts", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TotalCost", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CurrentAccrual", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("BalanceForward", System.Type.GetType("System.Decimal")))
            ds.Tables.Add(dt)
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function


#End Region


#Region "Private Methods"


    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

    Private Function BuildReportParamsTable(ByVal rptParamInfo As ReportParamInfo) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        With sb
            .Append("select Image AS Logo from tblTestImage where ImgId=3")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        Dim dt As DataTable = ds.Tables(0)
        dt.TableName = "ReportParams"

        dt.Columns.Add("SchoolName", System.Type.GetType("System.String"))
        dt.Columns.Add("ShowFilters", System.Type.GetType("System.Boolean"))
        dt.Columns.Add("Filters", System.Type.GetType("System.String"))
        dt.Columns.Add("FilterOthers", System.Type.GetType("System.String"))
        dt.Columns.Add("ShowSortBy", System.Type.GetType("System.Boolean"))
        dt.Columns.Add("SortBy", System.Type.GetType("System.String"))
        dt.Columns.Add("ShowRptDescription", System.Type.GetType("System.Boolean"))
        dt.Columns.Add("ShowRptInstructions", System.Type.GetType("System.Boolean"))
        dt.Columns.Add("ShowRptNotes", System.Type.GetType("System.Boolean"))
        dt.Columns.Add("StudentIdentifier", System.Type.GetType("System.String"))

        Dim r As DataRow = dt.NewRow
        r("SchoolName") = MyAdvAppSettings.AppSettings("SchoolName").ToUpper 'SchoolName
        r("ShowFilters") = rptParamInfo.ShowFilters
        If Not (rptParamInfo.FilterListString Is Nothing) Then
            r("Filters") = "Filter criteria: " & rptParamInfo.FilterListString
        Else
            r("Filters") = rptParamInfo.FilterListString
        End If
        If (rptParamInfo.FilterListString Is Nothing) And Not (rptParamInfo.FilterOtherString Is Nothing) Then
            r("FilterOthers") = "Filter criteria: " & rptParamInfo.FilterOtherString
        Else
            r("FilterOthers") = rptParamInfo.FilterOtherString
        End If
        r("ShowSortBy") = rptParamInfo.ShowSortBy
        If Not (rptParamInfo.OrderByString Is Nothing) Then
            r("SortBy") = "Sort order: " & rptParamInfo.OrderByString
        Else
            r("SortBy") = rptParamInfo.OrderByString
        End If
        r("ShowRptDescription") = rptParamInfo.ShowRptDescription
        r("ShowRptInstructions") = rptParamInfo.ShowRptInstructions
        r("ShowRptNotes") = rptParamInfo.ShowRptNotes

        r("StudentIdentifier") = MyAdvAppSettings.AppSettings("StudentIdentifier")
        r("StudentIdentifier") &= ":"

        dt.Rows.Add(r)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return dt
    End Function

#End Region


End Class
