Imports FAME.Advantage.Common

Public Class PrintAttendanceSheetRep

#Region "Private Data Members"
    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")

#End Region


#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

#End Region


#Region "Public Methods"

    Public Function GetPrintAttendanceSheet(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String = ""
        Dim strOrderBy As String = ""
        Dim strFrom As String = ""
        Dim strStudentId As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try


            If paramInfo.FilterList <> "" Then
                strWhere &= " AND " & paramInfo.FilterList
            End If
            If paramInfo.FilterOther <> "" Then
                strWhere &= " AND " & paramInfo.FilterOther
            End If

            'If paramInfo.OrderBy <> "" Then
            '    strOrderBy &= "," & paramInfo.OrderBy
            'End If
            ''''''''''''''''''''''''''
            Dim RecordStartDate As Date
            Dim RecordEndDate As Date
            Dim icount As Integer
            Dim noofDays As Integer = 1
            '  Dim reportStartDate As Date

            Dim arrValues() As String
            If paramInfo.FilterOtherString <> "" Then
                arrValues = paramInfo.FilterOtherString.Split(";")
                For icount = 0 To arrValues.Length - 1
                    If arrValues(icount).Contains("Number of Days=") Then
                        noofDays = CInt(arrValues(icount).Substring(arrValues(icount).IndexOf("=") + 1))
                        ' noofDays = noofDays - NZ 11/19 assignment has no effect
                    ElseIf arrValues(icount).Contains("Report Start Date=") Then
                        RecordStartDate = Format(CDate(arrValues(icount).Substring(arrValues(icount).IndexOf("=") + 1)), "MM/dd/yyyy")
                    End If
                Next
            End If
            If noofDays = 1 Then
                RecordEndDate = RecordStartDate
            Else
                RecordEndDate = DateAdd(DateInterval.Day, noofDays - 1, RecordStartDate)
            End If
            ''''''''''''''''''''''''

            If StudentIdentifier = "SSN" Then
                strStudentId = "arStudent.SSN AS StudentIdentifier,"
            ElseIf StudentIdentifier = "EnrollmentId" Then
                strStudentId = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
            ElseIf StudentIdentifier = "StudentId" Then
                strStudentId = "arStudent.StudentNumber AS StudentIdentifier,"
            End If
            Dim strGroupby As String = ""
            If StudentIdentifier = "SSN" Then
                strGroupby = "arStudent.SSN ,"
            ElseIf StudentIdentifier = "EnrollmentId" Then
                strGroupby = "arStuEnrollments.EnrollmentId ,"
            ElseIf StudentIdentifier = "StudentId" Then
                strGroupby = "arStudent.StudentNumber,"
            End If

            Dim strSuppressDate As String = "no"
            Try
                strSuppressDate = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
            Catch ex As System.Exception
                strSuppressDate = "no"
            End Try

            With sb


                .Append(" select ")
                .Append(" arStuEnrollments.StuEnrollID, ")
                .Append(strStudentId)
                .Append(" IsNull(arStudent.LastName,' ') + ' ' + ")
                .Append(" IsNull(arStudent.MIddleName,' ') + ' ' + ")
                .Append("  IsNull(arStudent.FirstName, ' ')as StudentName, ")
                .Append(" IsNull((select total from arProgScheduleDetails PSD where PSD.ScheduleId=PS.ScheduleId and dw=0),0.00) as Sun, ")
                .Append(" IsNull((select total from arProgScheduleDetails PSD where PSD.ScheduleId=PS.ScheduleId and dw=1),0.00) as Mon, ")
                .Append(" IsNull((select total from arProgScheduleDetails PSD where PSD.ScheduleId=PS.ScheduleId and dw=2),0.00) as Tue, ")
                .Append(" IsNull((select total from arProgScheduleDetails PSD where PSD.ScheduleId=PS.ScheduleId and dw=3),0.00) as Wed, ")
                .Append(" IsNull((select total from arProgScheduleDetails PSD where PSD.ScheduleId=PS.ScheduleId and dw=4),0.00) as Thur, ")
                .Append(" IsNull((select total from arProgScheduleDetails PSD where PSD.ScheduleId=PS.ScheduleId and dw=5),0.00) as Fri, ")
                .Append(" IsNull((select total from arProgScheduleDetails PSD where PSD.ScheduleId=PS.ScheduleId and dw=6),0.00) as Sat, ")
                .Append("  syCampGrps.CampGrpDescrip,F.CampDescrip, ")
                .Append("'" & strSuppressDate & "'")
                .Append(" as SuppressDate ")
                .Append(" from ")
                .Append(" arStudentSchedules SS, ")
                .Append(" arProgSchedules PS, ")
                .Append(" arPrgVersions PV, ")
                .Append("      arStuEnrollments , ")
                .Append(" arStudent , ")
                .Append(" syStatusCodes  SC ")
                If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                    .Append(",adLeadByLeadGroups, adLeadGroups ")
                End If
                If paramInfo.FilterList.Contains("arStuEnrollments.PrgVerId") Then
                    .Append(",arPrgVersions ")
                End If
                .Append("   , syCmpGrpCmps E,syCampuses F,syCampGrps,Sycampuses  ")

                .Append(" where ")
                .Append("  SS.StuEnrollId = arStuEnrollments.StuEnrollId")
                .Append(" and arStuEnrollments.StudentId = arStudent.StudentId ")
                .Append(" and SS.ScheduleId = PS.ScheduleId ")
                .Append(" and PS.PrgVerId = PV.PrgVerId ")
                .Append("  and SS.Active = 1 ")
                .Append(" and arStuEnrollments.StatusCodeId=SC.StatusCodeId ")
                .Append(" and SysStatusid not in (12,14,19,8) ")
                'and   StatusCodeDescrip not like 'Dropped%' and")
                '            .Append(" StatusCodeDescrip not like 'Graduated%'")
                '            .Append(" and StatusCodeDescrip not like 'Tran%' ")
                '            .Append(" and StatusCodeDescrip not like 'No Start%'")
                .Append(" and arStuEnrollments.StartDate<='" + RecordStartDate + "' and arStuEnrollments.ExpGradDate>='" + RecordStartDate + "'")

                .Append("  AND arStuEnrollments.CampusId=F.CampusId  ")
                .Append("   AND F.CampusId=E.CampusId AND    E.CampGrpId = syCampGrps.CampGrpId  ")
                .Append(" AND arStuEnrollments.CampusId=syCampuses.CampusId ")

                If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                    .Append("AND adLeadByLeadGroups.LeadGrpId=adLeadGroups.LeadGrpId ")
                    .Append("AND adLeadByLeadGroups.StuEnrollId=arStuEnrollments.StuEnrollId ")
                End If
                .Append(strWhere)

                '.Append("   Group By  arStuEnrollments.StuEnrollId ")
                '.Append(strGroupby)
                '.Append("syCampGrps.CampGrpDescrip,F.CampDescrip ")

                .Append("             union")
                .Append(" select ")
                .Append(" arStuEnrollments.StuEnrollID, ")
                .Append(strStudentId)
                .Append(" IsNull(arStudent.LastName,' ')+ ' ' +")
                .Append(" IsNull(arStudent.MIddleName,' ') +' '+ ")
                .Append(" IsNull(arStudent.FirstName,' ')as StudentName, ")
                .Append(" 0 as Sun, ")
                .Append(" 0 as Mon, ")
                .Append(" 0 as Tue, ")
                .Append(" 0 as Wed, ")
                .Append(" 0 as Thur, ")
                .Append(" 0 as Fri, ")
                .Append(" 0 as Sat, ")
                .Append("  syCampGrps.CampGrpDescrip,F.CampDescrip, ")
                .Append("'" & strSuppressDate & "'")
                .Append(" as SuppressDate ")
                .Append(" from ")
                .Append("      arStuEnrollments ,")
                .Append(" arStudent , ")
                .Append(" syStatusCodes SC")

                If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                    .Append(",adLeadByLeadGroups, adLeadGroups ")
                End If
                If paramInfo.FilterList.Contains("arStuEnrollments.PrgVerId") Then
                    .Append(",arPrgVersions ")
                End If
                .Append("   , syCmpGrpCmps E,syCampuses F,syCampGrps,Sycampuses ")

                .Append("             where ")
                .Append(" arStuEnrollments.StudentId = arStudent.StudentId ")
                .Append(" and arStuEnrollments.StatusCodeId=SC.StatusCodeId ")
                .Append(" and arStuEnrollments.StuEnrollId not in (Select StuEnrollid from arStudentSchedules)")
                .Append(" and arStuEnrollments.StatusCodeId=SC.StatusCodeId  ")
                .Append(" and SysStatusid not in (12,14,19,8) ")
                ' ''and   StatusCodeDescrip not like 'Dropped%' and")
                ''.Append(" StatusCodeDescrip not like 'Graduated%'")
                ''.Append(" and StatusCodeDescrip not like 'Tran%' ")
                ''.Append(" and StatusCodeDescrip not like 'No Start%'")
                .Append(" and arStuEnrollments.StartDate<='" + RecordStartDate + "' and arStuEnrollments.ExpGradDate>='" + RecordStartDate + "'")



                .Append("  AND arStuEnrollments.CampusId=F.CampusId  ")
                .Append("   AND F.CampusId=E.CampusId AND    E.CampGrpId = syCampGrps.CampGrpId  ")
                .Append(" AND arStuEnrollments.CampusId=syCampuses.CampusId ")
                If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                    .Append("AND adLeadByLeadGroups.LeadGrpId=adLeadGroups.LeadGrpId ")
                    .Append("AND adLeadByLeadGroups.StuEnrollId=arStuEnrollments.StuEnrollId ")
                End If
                .Append(strWhere)
                .Append(" Order By StudentName")
                '.Append("   Group By    arStuEnrollments.StuEnrollId  ")
                '.Append(strGroupby)
                '.Append("syCampGrps.CampGrpDescrip,F.CampDescrip ")

                .Append(" ;Select StuEnrollId,StartDate,EndDate from arStudentLOAs ;")
                .Append(" Select StuEnrollId,StartDate,EndDate from arStdSuspensions ;")
                .Append(" SELECT      CCT.HolidayCode,  CCT.HolidayStartDate StartDate, ")
                .Append("     CCT.HolidayEndDate EndDate, ")
                .Append(" CCT.HolidayDescrip ")
                .Append(" FROM     syHolidays CCT, syStatuses ST ")
                .Append(" WHERE    CCT.StatusId = ST.StatusId  and ST.Status='Active'")
            End With

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()
            ds = db.RunParamSQLDataSet(sb.ToString)

        Catch ex As Exception
            Return ds
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try
        If ds.Tables.Count = 4 Then
            ds.Tables(0).TableName = "dsAttendSheet"
            ds.Tables(1).TableName = "LOA"
            ds.Tables(2).TableName = "Susp"
            ds.Tables(3).TableName = "Hols"
        End If

        Return ds
    End Function
#End Region

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

End Class
