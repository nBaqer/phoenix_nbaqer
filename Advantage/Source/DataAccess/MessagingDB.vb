Imports System.Xml
Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' MessagingDB.vb
'
' MessagingDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================


Public Class MessagingDB
#Region "Get AdvAppsetting object"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
    Public Function GetAllMessageTemplates(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("	      (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("	      MT.MessageTemplateId, ")
            .Append("	      MT.StatusId, ")
            .Append("	      MT.TemplateDescrip, ")
            .Append("	      MT.SpsData ")
            .Append("FROM     syMessageTemplates MT, syStatuses ST ")
            .Append("WHERE    MT.StatusId = ST.StatusId ")
            .Append("AND      MT.CampGrpId In (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("ORDER BY ")
            .Append("         ST.Status, ")
            .Append("         MT.TemplateDescrip asc ")
        End With

        ' Add the campusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetActiveMessageTemplatesOnly(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("	      MT.MessageTemplateId, ")
            .Append("	      MT.TemplateDescrip ")
            .Append("FROM     syMessageTemplates MT, syStatuses ST ")
            .Append("WHERE    ST.Status = 'Active' ")
            .Append("AND      MT.CampGrpId In (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("ORDER BY ")
            .Append("         MT.TemplateDescrip asc ")
        End With

        ' Add the campusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllFreedomTemplates(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("	      (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("	      MT.MessageTemplateId, ")
            .Append("	      MT.StatusId, ")
            .Append("	      MT.TemplateDescrip, ")
            .Append("	      MT.SpsData ")
            .Append("FROM     syMessageTemplates MT, syStatuses ST ")
            .Append("WHERE    MT.StatusId = ST.StatusId ")
            .Append("AND      MT.CampGrpId In (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("ORDER BY ")
            .Append("         ST.Status, ")
            .Append("         MT.TemplateDescrip asc ")
        End With

        ' Add the campusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllMessageGroups(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("	      (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("	      MG.MessageGroupId, ")
            .Append("	      MG.StatusId, ")
            .Append("	      MG.GroupDescrip ")
            .Append("FROM     syMessageGroups MG, syStatuses ST ")
            .Append("WHERE    MG.StatusId = ST.StatusId ")
            .Append("AND      MG.CampGrpId In (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("ORDER BY ")
            .Append("         ST.Status, ")
            .Append("         MG.GroupDescrip asc ")
        End With

        ' Add the campusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllMessageSchemas(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("	      (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("	      MS.MessageSchemaId, ")
            .Append("	      MS.MessageSchemaDescrip ")
            .Append("FROM     syMessageSchemas MS, syStatuses ST ")
            .Append("WHERE    MS.StatusId = ST.StatusId ")
            .Append("AND      MS.CampGrpId In (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("ORDER BY ")
            .Append("         ST.Status, ")
            .Append("         MS.MessageSchemaDescrip asc ")
        End With

        ' Add the campusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllMessageGroupSchemas(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("	      (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("	      MS.MessageSchemaId, ")
            .Append("	      MS.MessageSchemaDescrip ")
            .Append("FROM     syMessageSchemas MS, syMessageGroupSchemas MGS, syStatuses ST ")
            .Append("WHERE    MS.StatusId = ST.StatusId ")
            .Append("AND      MS.MessageSchemaId=MGS.MessageSchemaId ")
            .Append("AND      MS.CampGrpId In (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("ORDER BY ")
            .Append("         ST.Status, ")
            .Append("         MS.MessageSchemaDescrip asc ")
        End With

        ' Add the campusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllOnDemandMessageTemplates(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("	      (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("	      MT.MessageTemplateId, ")
            .Append("	      MT.StatusId, ")
            .Append("	      MT.TemplateDescrip, ")
            .Append("	      MT.SpsData ")
            .Append("FROM     syMessageTemplates MT, syStatuses ST ")
            .Append("WHERE    MT.StatusId = ST.StatusId ")
            .Append("AND      MT.CampGrpId In (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("AND      MT.AdvantageEventId = ? ")
            .Append("ORDER BY ")
            .Append("         ST.Status, ")
            .Append("         MT.TemplateDescrip asc ")
        End With

        ' Add the campusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add AdvantageEventId (No_Event) to the parameter list
        db.AddParameter("@AdvantageEventId", CType(AdvantageEvent.On_Demand, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllQueryMessageTemplates(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("	      (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("	      MT.MessageTemplateId, ")
            .Append("	      MT.StatusId, ")
            .Append("	      MT.TemplateDescrip, ")
            .Append("	      MT.SpsData ")
            .Append("FROM     syMessageTemplates MT, syStatuses ST ")
            .Append("WHERE    MT.StatusId = ST.StatusId ")
            .Append("AND      MT.CampGrpId In (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("AND      MT.AdvantageEventId = ? ")
            .Append("ORDER BY ")
            .Append("         ST.Status, ")
            .Append("         MT.TemplateDescrip asc ")
        End With

        ' Add the campusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add AdvantageEventId (No_Event) to the parameter list
        db.AddParameter("@AdvantageEventId", CType(AdvantageEvent.Query, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetMessageTemplateInfo(ByVal MessageTemplateId As String) As MessageTemplateInfo

        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT MT.MessageTemplateId, ")
            .Append("    MT.TemplateDescrip, ")
            .Append("    MT.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=MT.StatusId) As Status, ")
            .Append("    MT.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=MT.CampGrpId) As CampGrpDescrip, ")
            .Append("    MT.MessageSchemaId, ")
            .Append("    MS.MessageSchema, ")
            .Append("    MT.AdvantageEventId, ")
            .Append("    MT.RecipientTypeId, ")
            .Append("    MS.SqlStatement, ")
            .Append("    MT.Xslt_Html, ")
            .Append("    MT.Xslt_Rtf, ")
            .Append("    MT.Xsl_Fo, ")
            .Append("    MT.SpsData, ")
            .Append("    MT.MessageTypeId, ")
            .Append("    MT.MessageFormatId, ")
            .Append("    MT.SendImmediately, ")
            .Append("    MT.KeepHistory, ")
            .Append("    MT.DelayTimeSpanUnitId, ")
            .Append("    MT.DelayTimeSpanNumberOfUnits, ")
            .Append("    (Select count(*) from syMessages where MessageTemplateId=MT.MessageTemplateId) As NumberOfMessages, ")
            .Append("    MT.ModUser, ")
            .Append("    MT.ModDate ")
            .Append("FROM  syMessageTemplates MT, syMessageSchemas MS ")
            .Append("WHERE ")
            .Append("    MT.MessageSchemaId=MS.MessageSchemaId ")
            .Append("AND MT.MessageTemplateId= ? ")
        End With

        ' Add the MessageTemplateId to the parameter list
        db.AddParameter("@MessageTemplateId", MessageTemplateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim MessageTemplateInfo As New MessageTemplateInfo

        While dr.Read()

            '   set properties with data from DataReader
            With MessageTemplateInfo
                .MessageTemplateId = MessageTemplateId
                .IsInDB = True
                .TemplateDescrip = dr("TemplateDescrip")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                .MessageSchemaId = CType(dr("MessageSchemaId"), Guid).ToString
                If Not (dr("MessageSchema") Is System.DBNull.Value) Then .MessageSchema = dr("MessageSchema")
                .AdvantageEventId = dr("AdvantageEventId")
                .RecipientTypeId = dr("RecipientTypeId")
                If Not dr("SqlStatement") Is System.DBNull.Value Then .SqlStatement = dr("SqlStatement")
                If Not dr("Xslt_Html") Is System.DBNull.Value Then .Xslt_Html = dr("Xslt_Html")
                If Not dr("Xslt_Rtf") Is System.DBNull.Value Then .Xslt_Rtf = dr("Xslt_Rtf")
                If Not dr("Xsl_Fo") Is System.DBNull.Value Then .Xsl_Fo = dr("Xsl_Fo")
                If Not dr("SpsData") Is System.DBNull.Value Then .SpsData = dr("SpsData")
                .MessageTypeId = dr("MessageTypeId")
                .MessageFormatId = dr("MessageFormatId")
                .SendImmediately = dr("SendImmediately")
                .KeepHistory = dr("KeepHistory")
                .DelayTimeSpanUnitId = dr("DelayTimeSpanUnitId")
                .DelayTimeSpanNumberOfUnits = dr("DelayTimeSpanNumberOfUnits")
                .NumberOfMessages = dr("NumberOfMessages")
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return MessageTemplateInfo
        Return MessageTemplateInfo

    End Function
    Public Function GetFreedomTemplateInfo(ByVal FreedomTemplateId As String) As FreedomTemplateInfo

        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT MT.MessageTemplateId, ")
            .Append("    MT.TemplateDescrip, ")
            .Append("    MT.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=MT.StatusId) As Status, ")
            .Append("    MT.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=MT.CampGrpId) As CampGrpDescrip, ")
            .Append("    MT.MessageSchemaId, ")
            .Append("    MS.MessageSchema, ")
            .Append("    MT.AdvantageEventId, ")
            .Append("    MT.RecipientTypeId, ")
            .Append("    MS.SqlStatement, ")
            .Append("    MT.Xslt_Html, ")
            .Append("    MT.Xslt_Rtf, ")
            .Append("    MT.Xsl_Fo, ")
            .Append("    MT.SpsData, ")
            .Append("    MT.MessageTypeId, ")
            .Append("    MT.MessageFormatId, ")
            .Append("    MT.SendImmediately, ")
            .Append("    MT.KeepHistory, ")
            .Append("    MT.DelayTimeSpanUnitId, ")
            .Append("    MT.DelayTimeSpanNumberOfUnits, ")
            .Append("    MT.ModUser, ")
            .Append("    MT.ModDate ")
            .Append("FROM  syMessageTemplates MT, syMessageSchemas MS ")
            .Append("WHERE ")
            .Append("    MT.MessageSchemaId=MS.MessageSchemaId ")
            .Append("AND MT.MessageTemplateId= ? ")
        End With

        ' Add the FreedomTemplateId to the parameter list
        db.AddParameter("@MessageTemplateId", FreedomTemplateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim freedomTemplateInfo As New FreedomTemplateInfo

        While dr.Read()

            '   set properties with data from DataReader
            With freedomTemplateInfo
                .freedomTemplateId = FreedomTemplateId
                .IsInDB = True
                .TemplateDescrip = dr("TemplateDescrip")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                .MessageSchemaId = CType(dr("MessageSchemaId"), Guid).ToString
                If Not (dr("MessageSchema") Is System.DBNull.Value) Then .MessageSchema = dr("MessageSchema")
                If Not dr("SqlStatement") Is System.DBNull.Value Then .SqlStatement = dr("SqlStatement")
                If Not dr("Xslt_Rtf") Is System.DBNull.Value Then .Xslt_Rtf = dr("Xslt_Rtf")
                If Not dr("SpsData") Is System.DBNull.Value Then .SpsData = dr("SpsData")
                .KeepHistory = dr("KeepHistory")
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return FreedomTemplateInfo
        Return freedomTemplateInfo

    End Function
    Public Function GetMessageGroupInfo(ByVal MessageGroupId As String) As MessageGroupInfo

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT MG.MessageGroupId, ")
            .Append("    MG.GroupDescrip, ")
            .Append("    MG.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=MG.StatusId) As Status, ")
            .Append("    MG.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=MG.CampGrpId) As CampGrpDescrip, ")
            .Append("    MG.MessageSchemaId, ")
            .Append("    MS.MessageSchema, ")
            .Append("    MGS.SpsData, ")
            .Append("    MGS.SqlStatement, ")
            .Append("    MGS.Html_Xslt, ")
            .Append("    Coalesce(MG.XmlData, MGS.XmlDefaultData) as XmlData, ")
            .Append("    MG.ModUser, ")
            .Append("    MG.ModDate ")
            .Append("FROM  syMessageGroups MG, syMessageSchemas MS, syMessageGroupSchemas MGS ")
            .Append("WHERE ")
            .Append("    MG.MessageSchemaId=MS.MessageSchemaId ")
            .Append("AND MS.MessageSchemaId=MGS.MessageSchemaId ")
            .Append("AND MG.MessageGroupId= ? ")
        End With

        ' Add the MessageGroupId to the parameter list
        db.AddParameter("@MessageGroupId", MessageGroupId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim MessageGroupInfo As New MessageGroupInfo

        While dr.Read()

            '   set properties with data from DataReader
            With MessageGroupInfo
                .MessageGroupId = MessageGroupId
                .IsInDB = True
                .GroupDescrip = dr("GroupDescrip")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                .MessageSchemaId = CType(dr("MessageSchemaId"), Guid).ToString
                If Not (dr("MessageSchema") Is System.DBNull.Value) Then .MessageSchema = dr("MessageSchema")
                If Not dr("SpsData") Is System.DBNull.Value Then .SpsData = dr("SpsData")
                If Not dr("Html_Xslt") Is System.DBNull.Value Then .Html_Xslt = dr("Html_Xslt")
                If Not dr("XmlData") Is System.DBNull.Value Then .XmlData = dr("XmlData")
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return MessageGroupInfo
        Return MessageGroupInfo

    End Function
    Public Function GetMessageSchemaInfo(ByVal MessageSchemaId As String) As MessageSchemaInfo

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT MS.MessageSchemaId, ")
            .Append("    MS.MessageSchemaDescrip, ")
            .Append("    MS.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=MS.StatusId) As Status, ")
            .Append("    MS.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=MS.CampGrpId) As CampGrpDescrip, ")
            .Append("    MS.MessageSchema, ")
            .Append("    MS.SqlStatement, ")
            .Append("    MS.MessageSchemaURL, ")
            .Append("    (Select count(*) from syMessageTemplates where MessageSchemaId=MS.MessageSchemaId) As NumberOfMessageTemplates, ")
            .Append("    MS.ModUser, ")
            .Append("    MS.ModDate ")
            .Append("FROM  syMessageSchemas MS ")
            .Append("WHERE ")
            .Append("    MS.MessageSchemaId= ? ")
        End With

        ' Add the MessageSchemaId to the parameter list
        db.AddParameter("@MessageSchemaId", MessageSchemaId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim MessageSchemaInfo As New MessageSchemaInfo

        While dr.Read()

            '   set properties with data from DataReader
            With MessageSchemaInfo
                .MessageSchemaId = MessageSchemaId
                .IsInDB = True
                .MessageSchemaDescrip = dr("MessageSchemaDescrip")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                If Not (dr("MessageSchema") Is System.DBNull.Value) Then .MessageSchema = dr("MessageSchema")
                If Not dr("SqlStatement") Is System.DBNull.Value Then .SqlStatement = dr("SqlStatement")
                If Not (dr("MessageSchemaURL") Is System.DBNull.Value) Then .MessageSchemaURL = dr("MessageSchemaURL")
                .NumberOfMessageTemplates = dr("NumberOfMessageTemplates")
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return MessageSchemaInfo
        Return MessageSchemaInfo

    End Function
    Public Function UpdateMessageTemplateInfo(ByVal MessageTemplateInfo As MessageTemplateInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syMessageTemplates Set ")
                .Append("   MessageTemplateId = ?, TemplateDescrip = ?,  StatusId = ?, ")
                .Append("   CampGrpId = ?, MessageSchemaId = ?, AdvantageEventId = ?, ")
                .Append("   RecipientTypeId = ?, Xslt_Html = ?, ")
                .Append("   Xslt_Rtf = ?, Xsl_Fo = ?, SpsData = ?, ")
                .Append("   MessageTypeId = ?, MessageFormatId = ?, SendImmediately = ?, ")
                .Append("   Keephistory = ?, DelayTimeSpanUnitId = ?, DelayTimeSpanNumberOfUnits = ?, ")
                .Append("   ModUser = ?, ModDate = ? ")
                .Append("WHERE MessageTemplateId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from syMessageTemplates where ModDate = ? ")
            End With

            '   add parameters values to the query

            'MessageTemplateId
            db.AddParameter("@MessageTemplateId", MessageTemplateInfo.MessageTemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'MessageTemplateDescrip
            db.AddParameter("@TemplateDescrip", MessageTemplateInfo.TemplateDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'StatusId
            db.AddParameter("@StatusId", MessageTemplateInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'CampGrpId
            If MessageTemplateInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", MessageTemplateInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'MessageSchemaId
            db.AddParameter("MessageSchemaId", MessageTemplateInfo.MessageSchemaId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'AdvantageEventId
            db.AddParameter("AdvantageEventId", MessageTemplateInfo.AdvantageEventId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'RecipientTypeId
            db.AddParameter("RecipientTypeId", MessageTemplateInfo.RecipientTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            ''SqlStatement
            'db.AddParameter("SqlStatement", MessageTemplateInfo.SqlStatement, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Xslt_Html
            If MessageTemplateInfo.Xslt_Html = "" Then
                db.AddParameter("Xslt_Html", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("Xslt_Html", MessageTemplateInfo.Xslt_Html, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Xslt_Rtf
            If MessageTemplateInfo.Xslt_Rtf = "" Then
                db.AddParameter("Xslt_Rtf", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("Xslt_Rtf", MessageTemplateInfo.Xslt_Rtf, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Xsl_Fo
            If MessageTemplateInfo.Xsl_Fo = "" Then
                db.AddParameter("Xsl_Fo", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("Xsl_Fo", MessageTemplateInfo.Xsl_Fo, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'SpsData
            If MessageTemplateInfo.SpsData = "" Then
                db.AddParameter("@SpsData", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@SpsData", MessageTemplateInfo.SpsData, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'MessageTypeId
            db.AddParameter("MessageTypeId", MessageTemplateInfo.MessageTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'MessageFormatId
            db.AddParameter("MessageFormatId", MessageTemplateInfo.MessageFormatId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'SendImmediately
            db.AddParameter("SendImmediately", MessageTemplateInfo.SendImmediately, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            'get KeepHistory
            db.AddParameter("KeepHistory", MessageTemplateInfo.KeepHistory, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            'get DelayTimeSpanUnitId
            db.AddParameter("DelayTimeSpanUnitId", MessageTemplateInfo.DelayTimeSpanUnitId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'get DelayTimeSpanNumberOfUnits
            db.AddParameter("DelayTimeSpanNumberOfUnits", MessageTemplateInfo.DelayTimeSpanNumberOfUnits, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   MessageTemplateId
            db.AddParameter("@MessageTemplateId", MessageTemplateInfo.MessageTemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", MessageTemplateInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If


        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)


        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function UpdateFreedomTemplateInfo(ByVal FreedomTemplateInfo As FreedomTemplateInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syMessageTemplates Set ")
                .Append("   MessageTemplateId = ?, TemplateDescrip = ?,  StatusId = ?, ")
                .Append("   CampGrpId = ?, MessageSchemaId = ?, AdvantageEventId = ?, ")
                .Append("   RecipientTypeId = ?, Xslt_Html = ?, ")
                .Append("   Xslt_Rtf = ?, Xsl_Fo = ?, SpsData = ?, ")
                .Append("   MessageTypeId = ?, MessageFormatId = ?, SendImmediately = ?, ")
                .Append("   Keephistory = ?, DelayTimeSpanUnitId = ?, DelayTimeSpanNumberOfUnits = ?, ")
                .Append("   ModUser = ?, ModDate = ? ")
                .Append("WHERE MessageTemplateId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from syMessageTemplates where ModDate = ? ")
            End With

            '   add parameters values to the query

            'FreedomTemplateId
            db.AddParameter("@FreedomTemplateId", FreedomTemplateInfo.freedomTemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'FreedomTemplateDescrip
            db.AddParameter("@TemplateDescrip", FreedomTemplateInfo.TemplateDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'StatusId
            db.AddParameter("@StatusId", FreedomTemplateInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'CampGrpId
            If FreedomTemplateInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", FreedomTemplateInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'MessageSchemaId
            db.AddParameter("MessageSchemaId", FreedomTemplateInfo.MessageSchemaId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Xslt_Rtf
            If FreedomTemplateInfo.Xslt_Rtf = "" Then
                db.AddParameter("Xslt_Rtf", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("Xslt_Rtf", FreedomTemplateInfo.Xslt_Rtf, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   FreedomTemplateId
            db.AddParameter("@MessageTemplateId", FreedomTemplateInfo.freedomTemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", FreedomTemplateInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If


        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)


        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function UpdateMessageGroupInfo(ByVal MessageGroupInfo As MessageGroupInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syMessageGroups Set ")
                .Append("   MessageGroupId = ?, GroupDescrip = ?,  StatusId = ?, ")
                .Append("   CampGrpId = ?, MessageSchemaId = ?, XmlData = ?, ")
                .Append("   ModUser = ?, ModDate = ? ")
                .Append("WHERE MessageGroupId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from syMessageGroups where ModDate = ? ")
            End With

            '   add parameters values to the query

            'MessageGroupId
            db.AddParameter("@MessageGroupId", MessageGroupInfo.MessageGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'MessageGroupDescrip
            db.AddParameter("@GroupDescrip", MessageGroupInfo.GroupDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'StatusId
            db.AddParameter("@StatusId", MessageGroupInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'CampGrpId
            If MessageGroupInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", MessageGroupInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'MessageSchemaId
            db.AddParameter("MessageSchemaId", MessageGroupInfo.MessageSchemaId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'XmlData
            If MessageGroupInfo.XmlData = "" Then
                db.AddParameter("@XmlData", GetXmlDocWithSchemaDefaultValue(MessageGroupInfo.MessageSchemaId), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@XmlData", MessageGroupInfo.XmlData, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   MessageGroupId
            db.AddParameter("@MessageGroupId", MessageGroupInfo.MessageGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", MessageGroupInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            'If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function UpdateMessageSchemaInfo(ByVal messageSchemaInfo As MessageSchemaInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syMessageSchemas Set ")
                .Append("   MessageSchemaId = ?, MessageSchemaDescrip = ?,  StatusId = ?, ")
                .Append("   CampGrpId = ?, MessageSchema = ?, SqlStatement = ?, MessageSchemaURL = ?, ")
                .Append("   ModUser = ?, ModDate = ? ")
                .Append("WHERE MessageSchemaId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from syMessageSchemas where ModDate = ? ")
            End With

            '   add parameters values to the query

            'MessageSchemaId
            db.AddParameter("@MessageSchemaId", messageSchemaInfo.MessageSchemaId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'MessageSchemaDescrip
            db.AddParameter("@MessageSchemaDescrip", messageSchemaInfo.MessageSchemaDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'StatusId
            db.AddParameter("@StatusId", messageSchemaInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'CampGrpId
            If messageSchemaInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", messageSchemaInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'MessageSchema
            db.AddParameter("@MessageSchema", messageSchemaInfo.MessageSchema, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'SqlStatement
            db.AddParameter("SqlStatement", messageSchemaInfo.SqlStatement, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   MessageSchemaURL
            db.AddParameter("@MessageSchemaURL", messageSchemaInfo.MessageSchemaURL, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   MessageSchemaId
            db.AddParameter("@MessageSchemaId", messageSchemaInfo.MessageSchemaId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", messageSchemaInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If


        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddMessageTemplateInfo(ByVal MessageTemplateInfo As MessageTemplateInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT syMessageTemplates (MessageTemplateId, TemplateDescrip, StatusId, ")
                .Append("   CampGrpId, MessageSchemaId, AdvantageEventId, ")
                .Append("   RecipientTypeId, Xslt_Html, ")
                .Append("   Xslt_Rtf, Xsl_Fo, SpsData, ")
                .Append("   MessageTypeId, MessageFormatId, SendImmediately, ")
                .Append("   Keephistory, DelayTimeSpanUnitId, DelayTimeSpanNumberOfUnits, ")
                .Append("   ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            'MessageTemplateId
            db.AddParameter("@MessageTemplateId", MessageTemplateInfo.MessageTemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'MessageTemplateDescrip
            db.AddParameter("@TemplateDescrip", MessageTemplateInfo.TemplateDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'StatusId
            db.AddParameter("@StatusId", MessageTemplateInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'CampGrpId
            If MessageTemplateInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", MessageTemplateInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'MessageSchemaId
            db.AddParameter("MessageSchemaId", MessageTemplateInfo.MessageSchemaId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'AdvantageEventId
            db.AddParameter("AdvantageEventId", MessageTemplateInfo.AdvantageEventId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'RecipientTypeId
            db.AddParameter("RecipientTypeId", MessageTemplateInfo.RecipientTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            ''SqlStatement
            'db.AddParameter("SqlStatement", MessageTemplateInfo.SqlStatement, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Xslt_Html
            If MessageTemplateInfo.Xslt_Html = "" Then
                db.AddParameter("Xslt_Html", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("Xslt_Html", MessageTemplateInfo.Xslt_Html, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Xslt_Rtf
            If MessageTemplateInfo.Xslt_Rtf = "" Then
                db.AddParameter("Xslt_Rtf", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("Xslt_Rtf", MessageTemplateInfo.Xslt_Rtf, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Xsl_Fo
            If MessageTemplateInfo.Xsl_Fo = "" Then
                db.AddParameter("Xsl_Fo", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("Xsl_Fo", MessageTemplateInfo.Xsl_Fo, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'SpsData
            If MessageTemplateInfo.SpsData = "" Then
                db.AddParameter("@SpsData", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@SpsData", MessageTemplateInfo.SpsData, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'MessageTypeId
            db.AddParameter("MessageTypeId", MessageTemplateInfo.MessageTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'MessageFormatId
            db.AddParameter("MessageFormatId", MessageTemplateInfo.MessageFormatId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'SendImmediately
            db.AddParameter("SendImmediately", MessageTemplateInfo.SendImmediately, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            'get KeepHistory
            db.AddParameter("KeepHistory", MessageTemplateInfo.KeepHistory, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            'get DelayTimeSpanUnitId
            db.AddParameter("DelayTimeSpanUnitId", MessageTemplateInfo.DelayTimeSpanUnitId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'get DelayTimeSpanNumberOfUnits
            db.AddParameter("DelayTimeSpanNumberOfUnits", MessageTemplateInfo.DelayTimeSpanNumberOfUnits, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddFreedomTemplateInfo(ByVal FreedomTemplateInfo As FreedomTemplateInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT syMessageTemplates (MessageTemplateId, TemplateDescrip, StatusId, ")
                .Append("   CampGrpId, MessageSchemaId, AdvantageEventId, ")
                .Append("   RecipientTypeId, Xslt_Html, ")
                .Append("   Xslt_Rtf, Xsl_Fo, SpsData, ")
                .Append("   MessageTypeId, MessageFormatId, SendImmediately, ")
                .Append("   Keephistory, DelayTimeSpanUnitId, DelayTimeSpanNumberOfUnits, ")
                .Append("   ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            'MessageTemplateId
            db.AddParameter("@MessageTemplateId", FreedomTemplateInfo.freedomTemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'MessageTemplateDescrip
            db.AddParameter("@TemplateDescrip", FreedomTemplateInfo.TemplateDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'StatusId
            db.AddParameter("@StatusId", FreedomTemplateInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'CampGrpId
            If FreedomTemplateInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", FreedomTemplateInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Xslt_Rtf
            If FreedomTemplateInfo.Xslt_Rtf = "" Then
                db.AddParameter("Xslt_Rtf", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("Xslt_Rtf", FreedomTemplateInfo.Xslt_Rtf, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddMessageGroupInfo(ByVal MessageGroupInfo As MessageGroupInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT syMessageGroups (MessageGroupId, GroupDescrip, StatusId, ")
                .Append("   CampGrpId, MessageSchemaId, XmlData, ")
                .Append("   ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            'MessageGroupId
            db.AddParameter("@MessageGroupId", MessageGroupInfo.MessageGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'MessageGroupDescrip
            db.AddParameter("@TemplateDescrip", MessageGroupInfo.GroupDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'StatusId
            db.AddParameter("@StatusId", MessageGroupInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'CampGrpId
            If MessageGroupInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", MessageGroupInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'MessageSchemaId
            db.AddParameter("MessageSchemaId", MessageGroupInfo.MessageSchemaId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'XmlData
            If MessageGroupInfo.XmlData = "" Then
                db.AddParameter("@XmlData", GetXmlDocWithSchemaDefaultValue(MessageGroupInfo.MessageSchemaId), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@XmlData", MessageGroupInfo.XmlData, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddMessageSchemaInfo(ByVal messageSchemaInfo As MessageSchemaInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT syMessageSchemas (MessageSchemaId, MessageSchemaDescrip, StatusId, ")
                .Append("   CampGrpId, MessageSchema, SqlStatement, MessageSchemaURL, ")
                .Append("   ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            'MessageSchemaId
            db.AddParameter("@MessageSchemaId", messageSchemaInfo.MessageSchemaId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'MessageSchemaDescrip
            db.AddParameter("@MessageSchemaDescrip", messageSchemaInfo.MessageSchemaDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'StatusId
            db.AddParameter("@StatusId", messageSchemaInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'CampGrpId
            If messageSchemaInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", messageSchemaInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'MessageSchema
            db.AddParameter("@MessageSchema", messageSchemaInfo.MessageSchema, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'SqlStatement
            db.AddParameter("SqlStatement", messageSchemaInfo.SqlStatement, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'MessageSchemaURL
            db.AddParameter("@MessageSchemaURL", messageSchemaInfo.MessageSchemaURL, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteMessageTemplateInfo(ByVal MessageTemplateId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syMessageTemplates ")
                .Append("WHERE MessageTemplateId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM syMessageTemplates WHERE MessageTemplateId = ? ")
            End With

            '   add parameters values to the query

            '   MessageTemplateId
            db.AddParameter("@MessageTemplateId", MessageTemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   MessageTemplateId
            db.AddParameter("@MessageTemplateId", MessageTemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If


        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function DeleteFreedomTemplateInfo(ByVal MessageTemplateId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syMessageTemplates ")
                .Append("WHERE MessageTemplateId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM syMessageTemplates WHERE MessageTemplateId = ? ")
            End With

            '   add parameters values to the query

            '   MessageTemplateId
            db.AddParameter("@MessageTemplateId", MessageTemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   MessageTemplateId
            db.AddParameter("@MessageTemplateId", MessageTemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If


        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function DeleteMessageGroupInfo(ByVal MessageGroupId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syMessageGroups ")
                .Append("WHERE MessageGroupId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM syMessageGroups WHERE MessageGroupId = ? ")
            End With

            '   add parameters values to the query

            '   MessageGroupId
            db.AddParameter("@MessageGroupId", MessageGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   MessageGroupId
            db.AddParameter("@MessageGroupId", MessageGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If


        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function DeleteMessageSchemaInfo(ByVal messageSchemaId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syMessageSchemas ")
                .Append("WHERE MessageSchemaId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM syMessageTemplates WHERE MessageSchemaId = ? ")
            End With

            '   add parameters values to the query

            '   MessageSchemaId
            db.AddParameter("@MessageSchemaId", messageSchemaId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   MessageSchemaId
            db.AddParameter("@MessageSchemaId", messageSchemaId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Sub AddAdvantageMessage(ByVal sender As Object, ByVal e As AdvantageMessagingEventArgs)

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'Create a table with all Message Templates for this Event
        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("	      MT.MessageTemplateId, ")
            .Append("	      MT.StatusId, ")
            .Append("	      MS.SqlStatement ")
            .Append("FROM     syMessageTemplates MT, syMessageSchemas MS, syStatuses ST ")
            .Append("WHERE    MT.StatusId = ST.StatusId ")
            .Append("AND      MT.MessageSchemaId=MS.MessageSchemaId ")
            .Append("AND      MT.AdvantageEventId= ? ")
            .Append("AND      MT.CampGrpId In (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("ORDER BY ")
            .Append("         ST.Status, ")
            .Append("         MT.TemplateDescrip asc ")
        End With

        ' Add the eventId to the parameter list
        db.AddParameter("@EventId", CType(e.EventId, Integer))

        ' Add the campusId to the parameter list
        db.AddParameter("@CampusId", e.CampusId)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        'create and open connection for Xml sql command 
        Dim sc As New SqlClient.SqlCommand
        sc.Connection = New SqlClient.SqlConnection(GetAdvAppSettings.AppSettings("ConnectionString"))
        sc.Connection.Open()

        While dr.Read()

            'build parameters collection for each sql statement
            Dim sqlStatement = CType(dr("SqlStatement"), String)
            BuildSqlParametersCollection(sc, sqlStatement, e.SqlParameters)

            'execute the query
            sc.CommandText = sqlStatement
            Dim xmlRdr As System.Xml.XmlReader = sc.ExecuteXmlReader()

            'process all messages
            While xmlRdr.Read()
                Select Case (xmlRdr.NodeType)
                    'ignore all nodes except "advantageMessage"
                    Case XmlNodeType.ProcessingInstruction, XmlNodeType.DocumentType, XmlNodeType.Comment, XmlNodeType.Text, XmlNodeType.Whitespace

                        'process all "advantageMessage" nodes
                    Case XmlNodeType.Element
                        If xmlRdr.Name = "advantageMessage" Then
                            While xmlRdr.Name = "advantageMessage"
                                InsertAdvantageMessage(xmlRdr.ReadOuterXml, CType(dr("MessageTemplateId"), Guid).ToString)
                            End While
                        End If
                End Select
            End While

        End While

        If Not dr.IsClosed Then dr.Close()

    End Sub
    Private Sub InsertAdvantageMessage(ByVal xmlContent As String, ByVal messageTemplateId As String)

        'this is the third connection. Insert messages in syMessages table 
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the insert query
        Dim sb As New StringBuilder
        With sb
            .Append("INSERT syMessages (MessageTemplateId, RecipientId, StudentId, XmlContent, CreatedDate) ")
            .Append("VALUES (?,?,?,?,?) ")
        End With

        'get xmlDoc
        Dim xmlDoc As New XmlDocument
        xmlDoc.LoadXml(xmlContent)

        '   MessageTemplateId
        db.AddParameter("@MessageTemplateId", messageTemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   RecipientId
        db.AddParameter("@RecipientId", xmlDoc.FirstChild.Attributes("recipientId").Value(), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   studentId
        db.AddParameter("@StudentId", xmlDoc.FirstChild.Attributes("studentId").Value(), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   XmlContent
        db.AddParameter("@XmlContent", xmlContent, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   CreatedDate
        db.AddParameter("@CreatedDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        '   execute the query
        db.RunParamSQLExecuteNoneQuery(sb.ToString)

    End Sub
    Private Sub BuildSqlParametersCollection(ByVal sc As System.Data.SqlClient.SqlCommand, ByRef sqlStatement As String, ByVal sqlParameters As ArrayList)

        'create parameters collection
        sc.Parameters.Clear()
        For i As Integer = 0 To sqlParameters.Count - 1
            Dim parameter As AdvantageMessagingSqlParameter
            parameter = CType(sqlParameters(i), AdvantageMessagingSqlParameter)
            sc.Parameters.Add(parameter.ParameterName, parameter.ParameterValue)
        Next

    End Sub
    Public Function GetAllMessagesToProcess(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("	      (Case Len(M.ActuallySentOn) when  19 then 0 else 1 end) As Status, ")
            .Append("         M.MessageId, ")
            .Append("         M.RecipientId, ")
            .Append("         MT.RecipientTypeId, ")
            .Append("         (Case MT.recipientTypeId when 1 then ")
            .Append("                                       (Select LastName + ', ' + FirstName from arStudent where StudentId=M.RecipientId) ")
            .Append("                                  when 2 then ")
            .Append("                                       (Select LenderDescrip from faLenders where LenderId=M.RecipientId) ")
            .Append("                                  end) As Recipient, ")
            .Append("         (Case MT.RecipientTypeId when 1 then 'Student' ")
            .Append("                                  when 2 then 'Lender' ")
            .Append("                                  else 'Other' ")
            .Append("                                  end) As RecipientType, ")
            .Append("         M.StudentId, ")
            .Append("         (Select LastName + ', ' + FirstName from arStudent where StudentId=M.StudentId) As StudentName, ")
            .Append("         MT.MessageTemplateId, ")
            .Append("         MT.TemplateDescrip, ")
            .Append("         MT.MessageTypeId, ")
            .Append("         (Case MT.MessageTypeId when 1 then 'Email' ")
            .Append("                                when 2 then 'Print' ")
            .Append("                                else 'Other' ")
            .Append("                                end) As MessageType, ")
            .Append("         MT.MessageFormatId, ")
            .Append("         (Case MT.MessageFormatId when 1 then 'HTML' ")
            .Append("                                  when 2 then 'RTF' ")
            .Append("                                  when 3 then 'PDF' ")
            .Append("                                  else 'Other' ")
            .Append("                                  end) As MessageFormat, ")
            .Append("         (Case MT.DelayTimeSpanUnitId ")
            .Append("                                  when 1 then DateAdd(second, MT.DelayTimeSpanNumberOfUnits, M.CreatedDate) ")
            .Append("                                  when 2 then DateAdd(minute, MT.DelayTimeSpanNumberOfUnits, M.CreatedDate) ")
            .Append("                                  when 3 then DateAdd(Hour, MT.DelayTimeSpanNumberOfUnits, M.CreatedDate) ")
            .Append("                                  when 4 then DateAdd(Day, MT.DelayTimeSpanNumberOfUnits, M.CreatedDate) ")
            .Append("                                  when 5 then DateAdd(Week, MT.DelayTimeSpanNumberOfUnits, M.CreatedDate) ")
            .Append("                                  when 6 then DateAdd(Month, MT.DelayTimeSpanNumberOfUnits, M.CreatedDate) ")
            .Append("                                  when 7 then DateAdd(Year, MT.DelayTimeSpanNumberOfUnits, M.CreatedDate) ")
            .Append("                                  else M.CreatedDate ")
            .Append("                                  end) As SendOnOrAfter, ")
            .Append("         M.ActuallySentOn, ")
            .Append("         M.CreatedDate ")
            .Append("FROM     syMessageTemplates MT, syMessages M ")
            .Append("WHERE    MT.MessageTemplateId=M.MessageTemplateId ")
            .Append("AND      MT.CampGrpId In (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("AND      (M.ActuallySentOn is Null or M.ActuallySentOn > dateAdd(day, -1, GetDate())) ")
            .Append("ORDER BY ")
            .Append("         M.CreatedDate desc ")
        End With

        ' Add the campusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllMessagesByStudent(ByVal studentId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("	      (Case Len(M.ActuallySentOn) when  19 then 0 else 1 end) As Status, ")
            .Append("         M.MessageId, ")
            .Append("         M.RecipientId, ")
            .Append("         MT.RecipientTypeId, ")
            .Append("         (Case MT.recipientTypeId when 1 then ")
            .Append("                                       (Select LastName + ', ' + FirstName from arStudent where StudentId=M.RecipientId) ")
            .Append("                                  when 2 then ")
            .Append("                                       (Select LenderDescrip from faLenders where LenderId=M.RecipientId) ")
            .Append("                                  end) As Recipient, ")
            .Append("         (Case MT.RecipientTypeId when 1 then 'Student' ")
            .Append("                                  when 2 then 'Lender' ")
            .Append("                                  else 'Other' ")
            .Append("                                  end) As RecipientType, ")
            .Append("         M.StudentId, ")
            .Append("         (Select LastName + ', ' + FirstName from arStudent where StudentId=M.StudentId) As StudentName, ")
            .Append("         MT.TemplateDescrip, ")
            .Append("         MT.MessageTypeId, ")
            .Append("         (Case MT.MessageTypeId when 1 then 'Email' ")
            .Append("                                when 2 then 'Print' ")
            .Append("                                when 3 then 'Msg to Cell Phone' ")
            .Append("                                else 'Other' ")
            .Append("                                end) As MessageType, ")
            .Append("         MT.MessageFormatId, ")
            .Append("         (Case MT.MessageFormatId when 1 then 'HTML' ")
            .Append("                                  when 2 then 'RTF' ")
            .Append("                                  when 3 then 'PDF' ")
            .Append("                                  else 'Other' ")
            .Append("                                  end) As MessageFormat, ")
            .Append("         (Case MT.DelayTimeSpanUnitId ")
            .Append("                                  when 1 then DateAdd(second, MT.DelayTimeSpanNumberOfUnits, M.CreatedDate) ")
            .Append("                                  when 2 then DateAdd(minute, MT.DelayTimeSpanNumberOfUnits, M.CreatedDate) ")
            .Append("                                  when 3 then DateAdd(Hour, MT.DelayTimeSpanNumberOfUnits, M.CreatedDate) ")
            .Append("                                  when 4 then DateAdd(Day, MT.DelayTimeSpanNumberOfUnits, M.CreatedDate) ")
            .Append("                                  when 5 then DateAdd(Week, MT.DelayTimeSpanNumberOfUnits, M.CreatedDate) ")
            .Append("                                  when 6 then DateAdd(Month, MT.DelayTimeSpanNumberOfUnits, M.CreatedDate) ")
            .Append("                                  when 7 then DateAdd(Year, MT.DelayTimeSpanNumberOfUnits, M.CreatedDate) ")
            .Append("                                  else M.CreatedDate ")
            .Append("                                  end) As SendOnOrAfter, ")
            .Append("         M.ActuallySentOn, ")
            .Append("         M.CreatedDate ")
            .Append("FROM     syMessageTemplates MT, syMessages M ")
            .Append("WHERE    MT.MessageTemplateId=M.MessageTemplateId ")
            .Append("AND      M.StudentId = ? ")
            .Append("ORDER BY ")
            .Append("         M.CreatedDate desc ")
        End With

        ' Add the studentId to the parameter list
        db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllMessagesToBeSentImmediately(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         M.MessageId, ")
            .Append("         MT.MessageTypeId, ")
            .Append("FROM     syMessageTemplates MT, syMessages M ")
            .Append("WHERE    MT.MessageTemplateId=M.MessageTemplateId ")
            .Append("AND      MT.CampGrpId In (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("AND      M.ActuallySentOn is null ")
            .Append("AND      MT.SendImmediately = 1 ")
            .Append("AND      MT.MessageTypeId <> 2 ")
            .Append("ORDER BY ")
            .Append("         M.CreatedDate desc ")
        End With

        ' Add the studentId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetMessagingInfo(ByVal messageId As String) As MessagingInfo

        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("       M.MessageId, ")
            .Append("       M.MessageTemplateId, ")
            .Append("       MT.TemplateDescrip, ")
            .Append("       M.RecipientId, ")
            .Append("       M.XmlContent, ")
            .Append("       M.CreatedDate, ")
            .Append("       M.ActuallySentOn, ")
            .Append("       MS.MessageSchemaDescrip, ")
            .Append("       MS.MessageSchemaURL, ")
            .Append("       MS.MessageSchema, ")
            .Append("       MT.AdvantageEventId, ")
            .Append("       MT.RecipientTypeId, ")
            .Append("       MS.SqlStatement, ")
            .Append("       MT.Xslt_Html, ")
            .Append("       MT.Xslt_Rtf, ")
            .Append("       MT.Xsl_Fo, ")
            .Append("       MT.SpsData, ")
            .Append("       MT.MessageTypeId, ")
            .Append("       MT.MessageFormatId, ")
            .Append("       MT.SendImmediately, ")
            .Append("       MT.KeepHistory, ")
            .Append("       MT.DelayTimeSpanUnitId, ")
            .Append("       MT.DelayTimeSpanNumberOfUnits, ")
            .Append("       M.ModUser, ")
            .Append("       M.ModDate ")
            .Append("FROM  syMessages M, syMessageTemplates MT, syMessageSchemas MS ")
            .Append("WHERE ")
            .Append("       M.MessageTemplateId=MT.MessageTemplateId ")
            .Append("AND    MT.MessageSchemaId=MS.MessageSchemaId ")
            .Append("AND    M.MessageId= ? ")
        End With

        ' Add the MessageId to the parameter list
        db.AddParameter("@MessageId", messageId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim messagingInfo As New MessagingInfo

        While dr.Read()

            '   set properties with data from DataReader
            With messagingInfo
                .IsInDB = True
                .MessageId = messageId
                .MessageTemplateId = CType(dr("MessageTemplateId"), Guid).ToString
                .TemplateDescrip = dr("TemplateDescrip")
                .RecipientId = CType(dr("RecipientId"), Guid).ToString
                .XmlContent = dr("XmlContent")
                .CreatedDate = dr("CreatedDate")
                If Not dr("MessageSchema") Is System.DBNull.Value Then .MessageSchema = dr("MessageSchema")
                If Not dr("MessageSchemaDescrip") Is System.DBNull.Value Then .MessageSchemaDescrip = dr("MessageSchemaDescrip")
                If Not dr("MessageSchemaURL") Is System.DBNull.Value Then .MessageSchemaURL = dr("MessageSchemaURL")
                .advantageEventId = dr("AdvantageEventId")
                .RecipientTypeId = dr("RecipientTypeId")
                If Not dr("SqlStatement") Is System.DBNull.Value Then .SqlStatement = dr("SqlStatement")
                If Not dr("Xslt_Html") Is System.DBNull.Value Then .Xslt_Html = dr("Xslt_Html")
                If Not dr("Xslt_Rtf") Is System.DBNull.Value Then .Xslt_Rtf = dr("Xslt_Rtf")
                If Not dr("Xsl_Fo") Is System.DBNull.Value Then .Xsl_Fo = dr("Xsl_Fo")
                If Not dr("SpsData") Is System.DBNull.Value Then .SpsData = dr("SpsData")
                .MessageTypeId = dr("MessageTypeId")
                .MessageFormatId = dr("MessageFormatId")
                .SendImmediately = dr("SendImmediately")
                .KeepHistory = dr("KeepHistory")
                .DelayTimeSpanUnitId = dr("DelayTimeSpanUnitId")
                .DelayTimeSpanNumberOfUnits = dr("DelayTimeSpanNumberOfUnits")
                If Not (dr("ActuallySentOn") Is System.DBNull.Value) Then .ActuallySentOn = dr("ActuallySentOn")
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return MessagingInfo
        Return messagingInfo

    End Function
    Public Function UpdateMessagingInfoContent(ByVal messagingInfo As MessagingInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syMessages ")
                .Append("SET ")
                .Append("       XmlContent = ?, ")
                .Append("       ModUser = ?, ")
                .Append("       ModDate = ? ")
                .Append("WHERE MessageId = ? ")
                .Append("AND    ModDate = ?; ")
                .Append("Select count(*) from syMessages where MessageId=? and ModDate = ? ")

            End With

            '   add parameters values to the query

            '   XmlContent
            db.AddParameter("@XmlContent", messagingInfo.XmlContent, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   MessageId
            db.AddParameter("@MessageId", messagingInfo.MessageId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", messagingInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   MessageId
            db.AddParameter("@MessageId", messagingInfo.MessageId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function UpdateMessagingInfoSentDate(ByVal messageId As String, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syMessages ")
                .Append("SET ")
                .Append("       ActuallySentOn = ?, ")
                .Append("       ModUser = ?, ")
                .Append("       ModDate = ? ")
                .Append("WHERE MessageId = ? ")

            End With

            '   add parameters values to the query

            '   SentDate
            Dim now As Date = Date.Now
            db.AddParameter("@Sentdate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   MessageId
            db.AddParameter("@MessageId", messageId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function UpdateMessagingInfoSentDate(ByVal messageIds As String(), ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syMessages ")
                .Append("SET ")
                .Append("       ActuallySentOn = ?, ")
                .Append("       ModUser = ?, ")
                .Append("       ModDate = ? ")
                '.Append("WHERE MessageId = ? ")
                .Append("WHERE MessageId In (" + New StringBuilder(ConvertStringArrayToCommaSeparatedList(messageIds)).ToString + ") ")

            End With

            '   add parameters values to the query

            '   SentDate
            Dim now As Date = Date.Now
            db.AddParameter("@Sentdate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   MessageId
            'db.AddParameter("@MessageId", messageId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function UpdateMessagingInfo(ByVal messagingInfo As MessagingInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        If messagingInfo.IsInDB Then
            '   do an update
            Try
                '   build the query
                Dim sb As New StringBuilder
                With sb
                    .Append("UPDATE syMessages ")
                    .Append("SET ")
                    .Append("       XmlContent = ?, ")
                    .Append("       ModUser = ?, ")
                    .Append("       ModDate = ? ")
                    .Append("WHERE MessageId = ? ")
                    .Append("AND    ModDate = ?; ")
                    .Append("Select count(*) from syMessages where MessageId=? and ModDate = ? ")

                End With

                '   add parameters values to the query

                '   XmlContent
                db.AddParameter("@XmlContent", messagingInfo.XmlContent, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                '   ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   ModDate
                Dim now As Date = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   MessageId
                db.AddParameter("@MessageId", messagingInfo.MessageId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   ModDate
                db.AddParameter("@Original_ModDate", messagingInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   MessageId
                db.AddParameter("@MessageId", messagingInfo.MessageId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   ModDate
                db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   execute the query
                Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

                '   If there were no updated rows then there was a concurrency problem
                If rowCount = 1 Then
                    '   return without errors
                    Return ""
                Else
                    Return DALExceptions.BuildConcurrencyExceptionMessage()
                End If

            Catch ex As OleDbException

                '   return error message
                Return DALExceptions.BuildErrorMessage(ex)

            Finally
                'Close Connection
                db.CloseConnection()
            End Try
        Else
            '   do an insert
            Try
                '   build the query
                Dim sb As New StringBuilder
                '   build the insert query
                With sb
                    .Append("INSERT syMessages (MessageId, MessageTemplateId, RecipientId, StudentId, XmlContent, CreatedDate, ModUser, ModDate) ")
                    .Append("VALUES (?,?,?,?,?,?,?,?) ")
                End With

                Dim xmlDoc As New System.Xml.XmlDocument
                xmlDoc.LoadXml(messagingInfo.XmlContent)

                '   add parameters values to the query
                '   MessageId
                db.AddParameter("@MessageId", messagingInfo.MessageId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   MessageTemplateId
                db.AddParameter("@MessageTemplateId", messagingInfo.MessageTemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   RecipientId
                db.AddParameter("@RecipientId", xmlDoc.FirstChild.Attributes("recipientId").Value(), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   studentId
                If Not xmlDoc.FirstChild.Attributes("studentId") Is Nothing Then
                    db.AddParameter("@StudentId", xmlDoc.FirstChild.Attributes("studentId").Value(), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@StudentId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                '   XmlContent
                db.AddParameter("@XmlContent", messagingInfo.XmlContent, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                '   CreatedDate
                db.AddParameter("@CreatedDate", messagingInfo.CreatedDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   ModDate
                Dim now As Date = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)

            Catch ex As OleDbException

                '   return error message
                Return DALExceptions.BuildErrorMessage(ex)

            Finally
                'Close Connection
                db.CloseConnection()
            End Try
        End If
    End Function
    Public Function AddMessagingInfo(ByVal messagingInfo As MessagingInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT syMessageTemplates (MessageTemplateId, TemplateDescrip, StatusId, ")
                .Append("   SpsData, CampGrpId, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   MessageTemplateId
            'db.AddParameter("@MessageTemplateId", MessageTemplateInfo.MessageTemplateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   MessageTemplateCode
            'db.AddParameter("@MessageTemplateCode", MessageTemplateInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            'db.AddParameter("@StatusId", MessageTemplateInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   SpsData
            'db.AddParameter("@SpsData", MessageTemplateInfo.SpsData, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function BuildStudentMessagingInfo(ByVal messageTemplateId As String, ByVal studentId As String) As MessagingInfo

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'Create a table with all Message Templates for this Event
        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT MT.MessageTemplateId, ")
            .Append("    MT.TemplateDescrip, ")
            .Append("    MT.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=MT.StatusId) As Status, ")
            .Append("    MT.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=MT.CampGrpId) As CampGrpDescrip, ")
            .Append("    MT.MessageSchemaId, ")
            .Append("    MS.MessageSchemaDescrip, ")
            .Append("    MS.MessageSchemaURL, ")
            .Append("    MS.MessageSchema, ")
            .Append("    MT.AdvantageEventId, ")
            .Append("    MT.RecipientTypeId, ")
            .Append("    MS.SqlStatement, ")
            .Append("    MT.Xslt_Html, ")
            .Append("    MT.Xslt_Rtf, ")
            .Append("    MT.Xsl_Fo, ")
            .Append("    MT.SpsData, ")
            .Append("    MT.MessageTypeId, ")
            .Append("    MT.MessageFormatId, ")
            .Append("    MT.SendImmediately, ")
            .Append("    MT.KeepHistory, ")
            .Append("    MT.DelayTimeSpanUnitId, ")
            .Append("    MT.DelayTimeSpanNumberOfUnits, ")
            .Append("    MT.ModUser, ")
            .Append("    MT.ModDate ")
            .Append("FROM  syMessageTemplates MT, syMessageSchemas MS ")
            .Append("WHERE ")
            .Append("       MT.MessageSchemaId=MS.MessageSchemaId ")
            .Append("AND    MT.MessageTemplateId= ? ")
        End With

        ' Add messageTemplateId to the parameter list
        db.AddParameter("@MessageTemplateId", messageTemplateId)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        'create and open connection for Xml sql command 
        Dim sc As New SqlClient.SqlCommand
        sc.Connection = New SqlClient.SqlConnection(GetAdvAppSettings.AppSettings("ConnectionString"))
        sc.Connection.Open()

        'create the new Messaging Info
        Dim messagingInfo As New MessagingInfo

        While dr.Read()

            'build parameters collection for sql statement
            'this is the sql statement
            Dim sqlStatement = CType(dr("SqlStatement"), String)

            'Only parameter needed is StudentId
            Dim sqlParameter As New AdvantageMessagingSqlParameter("@StudentId", studentId)
            Dim sqlParametersArrayList As New ArrayList
            sqlParametersArrayList.Add(sqlParameter)

            'build parameters collection
            BuildSqlParametersCollection(sc, sqlStatement, sqlParametersArrayList)

            'execute the query
            sc.CommandText = sqlStatement
            Dim xmlContent As String = sc.ExecuteScalar()

            '   set properties with data from DataReader
            With messagingInfo
                .IsInDB = False
                .MessageId = Guid.NewGuid().ToString
                .MessageTemplateId = CType(dr("MessageTemplateId"), Guid).ToString
                .TemplateDescrip = dr("TemplateDescrip")
                .RecipientId = studentId
                .XmlContent = xmlContent
                .CreatedDate = Date.Now
                If Not dr("MessageSchema") Is System.DBNull.Value Then .MessageSchema = dr("MessageSchema")
                If Not dr("MessageSchemaDescrip") Is System.DBNull.Value Then .MessageSchemaDescrip = dr("MessageSchemaDescrip")
                If Not dr("MessageSchemaURL") Is System.DBNull.Value Then .MessageSchemaURL = dr("MessageSchemaURL")
                .advantageEventId = dr("AdvantageEventId")
                .RecipientTypeId = dr("RecipientTypeId")
                If Not dr("SqlStatement") Is System.DBNull.Value Then .SqlStatement = dr("SqlStatement")
                If Not dr("Xslt_Html") Is System.DBNull.Value Then .Xslt_Html = dr("Xslt_Html")
                If Not dr("Xslt_Rtf") Is System.DBNull.Value Then .Xslt_Rtf = dr("Xslt_Rtf")
                If Not dr("Xsl_Fo") Is System.DBNull.Value Then .Xsl_Fo = dr("Xsl_Fo")
                If Not dr("SpsData") Is System.DBNull.Value Then .SpsData = dr("SpsData")
                .MessageTypeId = dr("MessageTypeId")
                .MessageFormatId = dr("MessageFormatId")
                .SendImmediately = dr("SendImmediately")
                .KeepHistory = dr("KeepHistory")
                .DelayTimeSpanUnitId = dr("DelayTimeSpanUnitId")
                .DelayTimeSpanNumberOfUnits = dr("DelayTimeSpanNumberOfUnits")
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        
        'return MessagingInfo
        Return messagingInfo

    End Function
    'Public Function GenerateInsertStatementsForTable(ByVal tableName As String) As String

    '    'create and open connection for Xml sql command 
    '    Dim sc As New SqlClient.SqlCommand
    '    'sc.Connection = New SqlClient.SqlConnection(SingletonAppSettings.AppSettings("ConnectionString"))
    '    sc.Connection = New SqlClient.SqlConnection("server=SLJUSSAR\DEVSERVER;User=sa;password=fame.fame;database=AdvantageV1_0405")

    '    sc.Connection.Open()

    '    sc.CommandText = "sp_generate_inserts"
    '    sc.CommandType = CommandType.StoredProcedure

    '    sc.Parameters.Add("@table_name", tableName)


    '    Dim dr As System.Data.SqlClient.SqlDataReader = sc.ExecuteReader()

    '    Dim insertStatements As New StringBuilder

    '    While dr.Read()
    '        insertStatements.Append(dr.Item(0))
    '    End While

    '    'return insert statements
    '    Return insertStatements.ToString

    'End Function
    Public Function GetXmlDocGroupList(ByVal messageGroupId As String, ByVal xmlDocWithSqlParameters As String, ByVal user As String) As XmlDocument

        'Connect to the database
        Dim db As New DataAccess
        Dim sqlStatement As String

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'get SqlStatement
        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       MGS.SqlStatement ")
            .Append("FROM   syMessageGroups MG, syMessageGroupSchemas MGS ")
            .Append("WHERE ")
            .Append("       MG.MessageSchemaId=MGS.MessageSchemaId ")
            .Append("AND    MessageGroupId = ? ")
        End With

        '   add parameters values to the query
        '   MessageGroupId
        db.AddParameter("@MessageGroupId", messageGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   execute the query
        sqlStatement = db.RunParamSQLScalar(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'create and open connection for Xml sql command 
        Dim sc As New SqlClient.SqlCommand
        sc.Connection = New SqlClient.SqlConnection(GetAdvAppSettings.AppSettings("ConnectionString"))
        sc.Connection.Open()

        'build parameters collection for each sql statement
        BuildSqlParametersCollection(sc, sqlStatement, BuildListOfSqlParameters(xmlDocWithSqlParameters))

        'execute the query
        sc.CommandText = sqlStatement
        Dim xmlDoc As New XmlDocument
        xmlDoc.Load(sc.ExecuteXmlReader())

        'return xmlDocument
        Return xmlDoc

    End Function
    Private Function BuildListOfSqlParameters(ByVal xmlDocWithSqlParameters As String) As ArrayList

        'get an XmlDocument from the xml string
        Dim xmlDoc As New XmlDocument
        xmlDoc.LoadXml(xmlDocWithSqlParameters)

        'create a navigator to navigate the xmldocument
        Dim xmlDocNav As System.Xml.XPath.XPathNavigator = xmlDoc.CreateNavigator()
        xmlDocNav.MoveToRoot()

        'select all nodes that have an "sqlParameter" attribute
        Dim xmlXPNodeIterator As System.Xml.XPath.XPathNodeIterator = xmlDocNav.Select("//@sqlParameter")

        'this is the result array list
        Dim sqlParameters As New ArrayList

        'iterate throughout all the nodes
        While xmlXPNodeIterator.MoveNext()

            Dim parameter As New AdvantageMessagingSqlParameter
            'the attribute value will become the parameter name
            parameter.ParameterName = xmlXPNodeIterator.Current.Value

            'the node value will become the parameter value
            xmlXPNodeIterator.Current.MoveToParent()
            parameter.ParameterValue = xmlXPNodeIterator.Current.Value

            'add sql parameter to the array list
            sqlParameters.Add(parameter)
        End While

        'return sqlParameters
        Return sqlParameters
    End Function
    Private Function GetXmlDocWithSchemaDefaultValue(ByVal MessageSchemaId As String) As String

        'Connect to the database
        Dim db As New DataAccess
        Dim sqlStatement As String

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'get SqlStatement
        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       MS.SqlStatement ")
            .Append("FROM   syMessageSchemas MS ")
            .Append("WHERE ")
            .Append("       MessageSchemaId = ? ")
        End With

        '   add parameters values to the query
        '   MessageSchemaId
        db.AddParameter("@MessageSchemaId", MessageSchemaId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   execute the query
        sqlStatement = db.RunParamSQLScalar(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'create and open connection for Xml sql command 
        Dim sc As New SqlClient.SqlCommand
        sc.Connection = New SqlClient.SqlConnection(GetAdvAppSettings.AppSettings("ConnectionString"))
        sc.Connection.Open()

        'execute the query
        sc.CommandText = sqlStatement
        Dim xmlDoc As New XmlDocument
        xmlDoc.Load(sc.ExecuteXmlReader())

        'return xmlDocument
        Return xmlDoc.OuterXml

    End Function
    Public Function MergeSeveralMessagesInOnlyOneXmlDocument(ByVal messageId() As String) As String
        If messageId.Length = 0 Then Return ""

        Dim messageList As New StringBuilder
        'convert string array to comma separated list
        For i As Integer = 0 To messageId.Length - 1
            If i > 0 Then messageList.Append(",")
            messageList.Append("'")
            messageList.Append(messageId(i).ToString)
            messageList.Append("'")
        Next

        'Connect to the database
        Dim db As New DataAccess
        Dim sqlStatement As String

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'get SqlStatement
        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       M.XmlContent ")
            .Append("FROM   syMessages M ")
            .Append("WHERE ")
            .Append("       MessageId In (" + messageList.ToString + ") ")
        End With

        '   execute the query
        Dim xmlContentTable As DataTable = db.RunSQLDataSet(sb.ToString).Tables(0)

        'Close Connection
        db.CloseConnection()

        Dim xmlDoc As New XmlDocument
        Dim root As XmlNode = xmlDoc.CreateElement("Messages")
        xmlDoc.AppendChild(root)

        For i As Integer = 0 To xmlContentTable.Rows.Count - 1
            Dim node As XmlNode = xmlDoc.CreateNode(XmlNodeType.Element, "AdvantageNode", Nothing)
            node.InnerXml = CType(xmlContentTable.Rows(i)("XmlContent"), String).ToString
            root.AppendChild(node)
        Next

        'return xmlDocument
        Return xmlDoc.OuterXml

    End Function
    Public Function GetXsltTransforms(ByVal messageIds() As String) As String()
        Dim str() As String
        If messageIds.Length = 0 Then Return str

        'Connect to the database
        Dim db As New DataAccess
        Dim sqlStatement As String

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'get SqlStatement
        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("   Case MT.MessageFormatId ")
            .Append("       when 1 then Xslt_Html ")
            .Append("       when 2 then Xslt_Rtf ")
            .Append("       when 3 then Xsl_Fo ")
            .Append("        end As XsltTransform ")
            .Append("FROM   syMessages M, syMessageTemplates MT ")
            .Append("WHERE ")
            .Append("       M.MessageTemplateId = MT.MessageTemplateId ")
            .Append("AND    M.MessageId In (" + New StringBuilder(ConvertStringArrayToCommaSeparatedList(messageIds)).ToString + ") ")
        End With

        '   execute the query
        Dim xsltTransformsContentTable As DataTable = db.RunSQLDataSet(sb.ToString).Tables(0)

        'Close Connection
        db.CloseConnection()

        Dim xsltTransforms(xsltTransformsContentTable.Rows.Count - 1) As String

        For i As Integer = 0 To xsltTransformsContentTable.Rows.Count - 1
            xsltTransforms(i) = CType(xsltTransformsContentTable.Rows(i)("XsltTransform"), String).ToString
        Next

        'return xmlDocument
        Return xsltTransforms

    End Function
    Public Function GetFormattedMessages(ByVal messageIds() As String) As FormattedMessage

        'Connect to the database
        Dim db As New DataAccess
        Dim sqlStatement As String

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'get SqlStatement
        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       M.XmlContent, ")
            .Append("       MT.MessageTemplateId, ")
            .Append("       MT.MessageTypeId, ")
            .Append("       MT.MessageFormatId, ")
            .Append("       Case MT.MessageFormatId ")
            .Append("           when 1 then Xslt_Html ")
            .Append("           when 2 then Xslt_Rtf ")
            .Append("           when 3 then Xsl_Fo ")
            .Append("        end As XsltTransform ")
            .Append("FROM   syMessages M, syMessageTemplates MT ")
            .Append("WHERE ")
            .Append("       M.MessageTemplateId=MT.MessageTemplateId ")
            .Append("AND    MessageId In (" + New StringBuilder(ConvertStringArrayToCommaSeparatedList(messageIds)).ToString + ") ")
        End With

        '   execute the query
        Dim xmlContentTable As DataTable = db.RunSQLDataSet(sb.ToString).Tables(0)

        'Close Connection
        db.CloseConnection()

        Dim xmlDoc As New XmlDocument
        Dim root As XmlNode = xmlDoc.CreateElement("Messages")
        xmlDoc.AppendChild(root)

        'this is the xml document with all messages
        Dim mf As MessageFormat
        Dim mt As MessageType
        Dim xsltTransforms(xmlContentTable.Rows.Count - 1) As String
        For i As Integer = 0 To xmlContentTable.Rows.Count - 1
            Dim node As XmlNode = xmlDoc.CreateNode(XmlNodeType.Element, "AdvantageNode", Nothing)
            node.InnerXml = CType(xmlContentTable.Rows(i)("XmlContent"), String).ToString
            root.AppendChild(node)
            If i = 0 Then
                mf = CType(xmlContentTable.Rows(i)("MessageFormatId"), MessageFormat)
                mt = CType(xmlContentTable.Rows(i)("MessageTypeId"), MessageType)
            End If
            xsltTransforms(i) = CType(xmlContentTable.Rows(i)("MessageTemplateId"), Guid).ToString
        Next

        'sort MessageTemplateIds in order to remove duplicates
        xsltTransforms.Sort(xsltTransforms)
        Dim count As Integer = xsltTransforms.Length
        If xsltTransforms.Length > 1 Then
            For i As Integer = 1 To xsltTransforms.Length - 1
                If xsltTransforms(i - 1) = xsltTransforms(i) Then
                    xsltTransforms(i - 1) = ""
                    count -= 1
                End If
            Next
        End If

        'select only one xsltTransforms (do not include duplicates)
        Dim xsltTransform(count - 1) As String
        count = 0
        For i As Integer = 0 To xsltTransforms.Length - 1
            If xsltTransforms(i) <> "" Then
                xsltTransform(count) = CType(xmlContentTable.Rows(i)("XsltTransform"), String).ToString
                count += 1
            End If
        Next

        'return formattedMessage
        Select Case mf
            Case MessageFormat.HTML
                Dim fm As HTMLFormattedMessage = New HTMLFormattedMessage(xmlDoc.OuterXml, xsltTransform, mt)
                Return CType(fm, FormattedMessage)
            Case MessageFormat.RTF
                Dim fm As RTFFormattedMessage = New RTFFormattedMessage(xmlDoc.OuterXml, xsltTransform, mt)
                Return CType(fm, FormattedMessage)
            Case MessageFormat.PDF
                Dim fm As PDFFormattedMessage = New PDFFormattedMessage(xmlDoc.OuterXml, xsltTransform, mt)
                Return CType(fm, FormattedMessage)
        End Select

    End Function
    Public Function GetAdvMessages(ByVal messageIds() As String) As AdvMessage

        'Connect to the database
        Dim db As New DataAccess
        Dim sqlStatement As String

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'get SqlStatement
        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       M.XmlContent, ")
            .Append("       MT.MessageTemplateId, ")
            .Append("       MT.MessageTypeId, ")
            .Append("       MT.MessageFormatId, ")
            .Append("       MT.TemplateDescrip, ")
            .Append("       Case MT.MessageFormatId ")
            .Append("           when 1 then Xslt_Html ")
            .Append("           when 2 then Xslt_Rtf ")
            .Append("           when 3 then Xsl_Fo ")
            .Append("        end As XsltTransform ")
            .Append("FROM   syMessages M, syMessageTemplates MT ")
            .Append("WHERE ")
            .Append("       M.MessageTemplateId=MT.MessageTemplateId ")
            .Append("AND    MessageId In (" + New StringBuilder(ConvertStringArrayToCommaSeparatedList(messageIds)).ToString + ") ")
        End With

        '   execute the query
        Dim xmlContentTable As DataTable = db.RunSQLDataSet(sb.ToString).Tables(0)

        'Close Connection
        db.CloseConnection()

        Dim xmlDoc As New XmlDocument
        Dim root As XmlNode = xmlDoc.CreateElement("Messages")
        xmlDoc.AppendChild(root)

        'this is the xml document with all messages
        Dim mf As MessageFormat
        Dim mt As MessageType
        Dim name As String
        Dim xsltTransforms(xmlContentTable.Rows.Count - 1) As String
        For i As Integer = 0 To xmlContentTable.Rows.Count - 1
            Dim node As XmlNode = xmlDoc.CreateNode(XmlNodeType.Element, "AdvantageNode", Nothing)
            node.InnerXml = CType(xmlContentTable.Rows(i)("XmlContent"), String).ToString
            root.AppendChild(node)
            If i = 0 Then
                mf = CType(xmlContentTable.Rows(i)("MessageFormatId"), MessageFormat)
                mt = CType(xmlContentTable.Rows(i)("MessageTypeId"), MessageType)
                name = xmlContentTable.Rows(i)("TemplateDescrip")
            End If
            xsltTransforms(i) = CType(xmlContentTable.Rows(i)("MessageTemplateId"), Guid).ToString
        Next

        'sort MessageTemplateIds in order to remove duplicates
        xsltTransforms.Sort(xsltTransforms)
        Dim count As Integer = xsltTransforms.Length
        If xsltTransforms.Length > 1 Then
            For i As Integer = 1 To xsltTransforms.Length - 1
                If xsltTransforms(i - 1) = xsltTransforms(i) Then
                    xsltTransforms(i - 1) = ""
                    count -= 1
                End If
            Next
        End If

        'select only one xsltTransforms (do not include duplicates)
        Dim xsltTransform(count - 1) As String
        count = 0
        For i As Integer = 0 To xsltTransforms.Length - 1
            If xsltTransforms(i) <> "" Then
                xsltTransform(count) = CType(xmlContentTable.Rows(i)("XsltTransform"), String).ToString
                count += 1
            End If
        Next

        'return formattedMessage
        Dim advMessage As AdvMessage
        Dim content As Content
        Select Case mf
            Case MessageFormat.HTML
                content = New HTMLContent(xmlDoc.OuterXml, xsltTransform(0))
            Case MessageFormat.RTF
                content = New RTFContent(xmlDoc.OuterXml, xsltTransform(0))
            Case MessageFormat.PDF
                content = New PDFContent(xmlDoc.OuterXml, xsltTransform(0))
        End Select

        content.Name = name

        Select Case mt
            Case MessageType.Print
                advMessage = New PrintAdvantageMessage(messageIds, content)
            Case MessageType.Email
                advMessage = New EmailAdvantageMessage(EmailAdvantageMessage.GetEmailAddressFromContent(content), messageIds, content)
        End Select

        Return advMessage

    End Function
    Private Function ConvertStringArrayToCommaSeparatedList(ByVal strArray() As String) As String
        Dim list As New StringBuilder

        'convert string array to comma separated list
        For i As Integer = 0 To strArray.Length - 1
            If i > 0 Then list.Append(",")
            list.Append("'")
            list.Append(strArray(i).ToString)
            list.Append("'")
        Next

        'return string
        Return list.ToString
    End Function
    Public Function GenerateMessagesInHTMLForMigration() As String
        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("          M.MessageId,  ")
            .Append("          M.XmlContent, ")
            .Append("          MT.Xslt_Html ")
            .Append("FROM      syMessageTemplates MT, syMessages M ")
            .Append("WHERE     MT.MessageTemplateId = M.MessageTemplateId ")
            .Append("AND       MT.MessageFormatId = 1 ")
        End With

        'get dataset
        Dim ds As DataSet = db.RunSQLDataSet(sb.ToString)

        'loop throughout all the records
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            Dim row As DataRow = ds.Tables(0).Rows(i)
            Dim ms As AdvantageMessageMemoryStream = CreateMessageMemoryStream(row("XmlContent"), row("Xslt_Html"), MessageFormat.HTML)
            Dim result As String = UpdateMessagesTableForMigration(CType(row("MessageId"), Guid).ToString, System.Text.Encoding.UTF8.GetString(ms.DocumentMemoryStream.ToArray()))
            If Not result = "" Then Return result
        Next
        Return ""
    End Function
    Private Function UpdateMessagesTableForMigration(ByVal messageId As String, ByVal htmlContent As String) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syMessages ")
                .Append("SET ")
                .Append("       HtmlContent = ? ")
                .Append("WHERE  MessageId = ? ")
            End With

            '   add parameters values to the query

            '   XmlContent
            db.AddParameter("@htmlContent", htmlContent, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   MessageId
            db.AddParameter("@MessageId", messageId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            'return without errors
            Return ""

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Private Function CreateMessageMemoryStream(ByVal xmlContent As String, ByVal XslStylesheet As String, ByVal msgFormat As MessageFormat) As AdvantageMessageMemoryStream
        'this is the object that makes the transform
        Dim xslt As New System.Xml.Xsl.XslTransform

        'this is the Xml content. Xml content must be Navigable
        Dim xpathdocument As New System.Xml.XPath.XPathDocument(New System.IO.StringReader(xmlContent))

        'define writer
        Dim documentMemoryStream As New System.IO.MemoryStream
        Dim writer As New XmlTextWriter(documentMemoryStream, Nothing)
        writer.Formatting = Formatting.Indented

        'define AdvantageMessageMemoryStream
        Dim report As New AdvantageMessageMemoryStream

        'apply proper stylesheet
        Select Case msgFormat
            Case MessageFormat.HTML
                'Xsl stylesheet must be navigable
                xslt.Load(New System.Xml.XPath.XPathDocument(New System.IO.StringReader(XslStylesheet)), Nothing, Nothing)

                'transform
                xslt.Transform(xpathdocument, Nothing, writer, Nothing)

                '   save document and document type 
                report.DocumentMemoryStream = documentMemoryStream
                report.ContentType = "text/html; charset=UTF-8"

            Case MessageFormat.PDF


            Case MessageFormat.RTF
                'Xsl stylesheet must be navigable
                xslt.Load(New System.Xml.XPath.XPathDocument(New System.IO.StringReader(XslStylesheet)), Nothing, Nothing)
                xslt.Transform(xpathdocument, Nothing, writer, Nothing)

                '   save document and document type 
                report.ContentType = "application/msword"
                'report.ContentType = "application/vnd.ms-word"
                report.DocumentMemoryStream = documentMemoryStream
        End Select

        'return report
        Return report
    End Function
End Class
