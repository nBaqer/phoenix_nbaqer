Imports System.Data
Imports System.Data.OleDb
Imports System.Text

Public Class ActivityStatusDB

    Public Function AddActivityStatus(ByVal ActivityStatus As ActivityStatusInfo) As Int32

        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        Dim dr As OleDbDataReader
        Dim NewActivityStatusId As Int32
        ' We are adding a record to ActivityStatus, so we need to do a Max() on the ActivityStatusId 
        ' field so that we can get the next available in the list

        With strSQL
            .Append("SELECT MAX(ActivityStatusId)")
            .Append(" FROM cmActivityStatus")
        End With

        dr = db.RunSQLDataReader(strSQL.ToString)

        While dr.Read()
            NewActivityStatusId = dr(0)
        End While

        If Not dr.IsClosed Then dr.Close()

        ' Add one to the Max id found so that we can write the new record
        NewActivityStatusId += 1

        ' Add the new record to the table
        strSQL.Remove(0, strSQL.Length)
        db.CloseConnection()

        With strSQL
            .Append("INSERT INTO cmActivityStatus")
            .Append(" (ActivityStatusId, ActivityStatusCode, StatusId, ActivityStatusDescrip, CampGrpId)")
            .Append(" VALUES(?,?,?,?,?)")
        End With
        db.AddParameter("@activitystatusid", NewActivityStatusId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        db.AddParameter("@activitystatuscode", ActivityStatus.ActivityStatusCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@statusid", ActivityStatus.StatusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@activitystatusdescrip", ActivityStatus.ActivityStatusDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@campgrpid", ActivityStatus.CampGrpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
        Return NewActivityStatusId

    End Function

    Public Sub UpdateActivityStatus(ByVal ActivityStatus As ActivityStatusInfo)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("UPDATE cmActivityStatus SET ")
            .Append("ActivityStatusCode = ?")
            .Append(",StatusId = ?")
            .Append(",ActivityStatusDescrip = ?")
            .Append(",CampGrpId = ?")
            .Append(" WHERE ActivityStatusId = ? ")
        End With

        db.AddParameter("@activitystatuscode", ActivityStatus.ActivityStatusCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@statusid", ActivityStatus.StatusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@activitystatusdescrip", ActivityStatus.ActivityStatusDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@campgrpid", ActivityStatus.CampGrpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@activitystatusid", ActivityStatus.ActivityStatusId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Sub

    Public Sub DeleteActivityStatus(ByVal ActivityStatusID As Int32)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("DELETE FROM cmActivityStatus WHERE ActivityStatusId = ?")
        End With
        db.AddParameter("@activitystatusid", ActivityStatusID, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Sub
    Public Function GetSingleActivityStatus(ByRef ActivityStatusId As Int32) As DataSet
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim strSQLString As New StringBuilder
        With strSQLString
            .Append("Select * from cmActivityStatus")
            .Append(" where ActivityStatusId = ? ")
        End With
        Dim objSingleActivityStatus As New OleDbDataAdapter
        db.OpenConnection()
        db.AddParameter("@AssignmentId", ActivityStatusId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        Try
            objSingleActivityStatus = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        objSingleActivityStatus.Fill(ds, "SingleAssignment")
        db.ClearParameters()
        db.CloseConnection()
        Return ds
    End Function
End Class
