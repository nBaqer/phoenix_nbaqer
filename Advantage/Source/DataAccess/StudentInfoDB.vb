Public Class StudentInfoDB
    Private m_ConnString As String
    Public Sub New(ByVal connString As String)
        m_ConnString = connString
    End Sub


    Public Function GetStudentInfoBySSN(ByVal intSSN As Integer) As DataTable
        Dim rtn As New DataTable
        Dim db As New DataAccess(m_ConnString)
        Dim dr As OleDbDataReader
        Dim sb As New StringBuilder(1000)
        With sb
            .Append("Select top 1 * from arStudent where ")
            .Append(" SSN = ? ")

            db.AddParameter("@SSN", intSSN, DataAccess.OleDbDataType.OleDbInteger, 0, ParameterDirection.Input)
        End With

        dr = db.RunParamSQLDataReader(sb.ToString())
        If (dr.HasRows) Then
            rtn.Load(dr)

        End If

        If Not dr.IsClosed Then dr.Close()
        
        Return rtn

    End Function
    

    Public Function UpdateStudentInfo(ByVal studentInfo As StudentMasterInfo) As String
        Dim errMessage As String = String.Empty
        Dim db As New DataAccess(m_ConnString)
        Dim sb As New StringBuilder(1000)
        Dim groupTrans As OleDbTransaction = db.StartTransaction()
        With sb
            .Append(" Update arStudent set FirstName=?,LastName=?,")
            .Append("StudentStatus=?,Prefix=?,Suffix=?,Gender=?,Race=?,MaritalStatus=?, ")
            .Append("Nationality=? where SSN=? ")
        End With
        db.AddParameter("@FirstName", studentInfo.FirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@LastName", studentInfo.LastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@StudentStatus", studentInfo.Status, DataAccess.OleDbDataType.OleDbGuid, 50, ParameterDirection.Input)
        db.AddParameter("@Prefix", studentInfo.Prefix, DataAccess.OleDbDataType.OleDbGuid, 50, ParameterDirection.Input)
        db.AddParameter("@Suffix", studentInfo.Suffix, DataAccess.OleDbDataType.OleDbGuid, 50, ParameterDirection.Input)
        db.AddParameter("@Gender", studentInfo.Gender, DataAccess.OleDbDataType.OleDbGuid, 50, ParameterDirection.Input)
        db.AddParameter("@Race", studentInfo.Race, DataAccess.OleDbDataType.OleDbGuid, 50, ParameterDirection.Input)
        db.AddParameter("@MaritalStatus", studentInfo.MaritalStatus, DataAccess.OleDbDataType.OleDbGuid, 50, ParameterDirection.Input)
        db.AddParameter("@Nationality", studentInfo.Nationality, DataAccess.OleDbDataType.OleDbGuid, 50, ParameterDirection.Input)
        db.AddParameter("@SSN", studentInfo.SSN, DataAccess.OleDbDataType.OleDbInteger, 0, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString(), groupTrans)
            groupTrans.Commit()
            errMessage = "Updated Successfully"
        Catch ex As Exception
            groupTrans.Rollback()
            errMessage = "Not Updated"
        End Try
        Return errMessage
    End Function


End Class
