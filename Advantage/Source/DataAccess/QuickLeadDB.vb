Imports FAME.Advantage.Common

Public Class QuickLead
    Public Function GetAllNotRequiredStudentFields() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            '.Append(" select syFields.fldid,syFields.fldname,syFldCaptions.Caption from sytblflds,sytables,syFields,syFldCaptions where syTables.tblid = syTblflds.tblid   ")
            '.Append(" and sytblflds.fldid = syFields.fldid and syFields.FldId = syFldCaptions.FldId and syTables.tblname = 'adLeads' and syFields.Fldid not in ")
            '.Append(" (select syTblflds.FldId from syTblflds,syTables,syRestblflds where  ")
            '.Append(" syTblflds.tblid =  syTables.tblid and syTblflds.tblfldsid = syrestblflds.tblfldsid ")
            '.Append(" and syTables.tblname='adLeads') ")

            '.Append(" SELECT t1.ResDefId,t2.FldId,t5.TblName,t3.FldName,t1.Required,t3.DDLId,t3.FldLen,t4.FldType,t5.TblPK,t6.Caption  ")
            '.Append(" FROM syResTblFlds t1,syTblFlds t2,syFields t3,syFieldTypes t4,syTables t5,syFldCaptions t6,syLangs t7  ")
            '.Append(" WHERE t1.TblFldsId = t2.TblFldsId AND t2.FldId=t3.FldId  ")
            '.Append(" AND t2.TblId=t5.TblId  AND t3.FldTypeId=t4.FldTypeId  ")
            '.Append(" AND t2.FldId=t6.FldId  AND t6.LangId=t7.LangId  aND t1.ResourceId=206 ")
            '.Append("   AND t7.LangName= 'EN-US' and t1.Required = 0 ")


            .Append(" select t1.FldId,t1.FldName,(select Distinct Caption from syFldCaptions where FldId=t1.FldId) as Caption ")
            .Append(" from syFields t1,adExpQuickLeadSections  t2 ")
            .Append(" where t1.FldID = t2.FldId And t2.SectionId = 5555 ")
            .Append(" union ")
            .Append(" select SectionId as FldId,NULL as FldName,SectionName as Caption from adQuickLeadSections ")
            .Append(" where SectionId <> 5555 ")
            .Append(" order by t1.FldName ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllRequiredStudentFields() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim da6 As New OleDbDataAdapter
        Dim ds As New DataSet

        '   build the sql query
        With sb
            '.Append(" select syFields.FldID,Fldname + '-' + 'Required' as FldName,syFldCaptions.Caption  ")
            '.Append(" from syFields, syFldCaptions  where syFields.FldId = syFldCaptions.FldId and syFields.FldId in ( ")
            '.Append(" select syTblflds.FldId from syTblflds,syTables,syRestblflds where  ")
            '.Append(" syTblflds.tblid = syTables.tblid And syTblflds.tblfldsid = syrestblflds.tblfldsid ")
            '.Append(" and syTables.tblname='adLeads' and Required=1) ")
            .Append(" SELECT t1.ResDefId,t2.FldId,t5.TblName,t3.FldName,t1.Required,t3.DDLId,t3.FldLen,t4.FldType,t5.TblPK,t6.Caption + '-' + 'Required' as Caption ")
            .Append(" FROM syResTblFlds t1,syTblFlds t2,syFields t3,syFieldTypes t4,syTables t5,syFldCaptions t6,syLangs t7  ")
            .Append(" WHERE t1.TblFldsId = t2.TblFldsId AND t2.FldId=t3.FldId  ")
            .Append(" AND t2.TblId=t5.TblId  AND t3.FldTypeId=t4.FldTypeId  ")
            .Append(" AND t2.FldId=t6.FldId  AND t6.LangId=t7.LangId  aND t1.ResourceId=170 ")
            .Append("   AND t7.LangName= 'EN-US' and t1.Required = 1 ")
            .Append(" order by t3.FldName ")
        End With

        'return dataset
        'Return db.RunSQLDataSet(sb.ToString)
        ' db.OpenConnection()
        Return db.RunParamSQLDataSet(sb.ToString)
        'Try
        '    da6.Fill(ds, "ReqDT")
        'Catch ex As System.Exception
        '    Throw New System.Exception(ex.InnerException.Message)
        'End Try
        'db.CloseConnection()
        'Return ds
    End Function
    Public Function GetLeadCount() As String
        Dim db As New DataAccess
        Dim result As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append(" select Count(*) from adLeadFields  ")
        End With

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            result = dr(0)
        End While

        If Not dr.IsClosed Then dr.Close()

        Return result
    End Function
    Public Function GetAllLeadFields() As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            '.Append(" select LeadID,LeadField from adLeadFields ")
            .Append(" select t2.FldId as LeadId,t2.FldName as LeadField, CASE t2.FldName WHEN  'LeadStatus' THEN 1 ELSE t4.Required END  AS Required from  ")
            .Append(" adLeadFields t1,syFields t2,syTblFlds t3,syResTblFlds t4  where t1.LeadId = t2.FldId And LeadField = 5555 and t2.FldId = t3.FldId and t3.TblFLdsId = t4.TblFldsId and ResourceId=170")
            .Append(" union ")
            .Append(" select sectionId as LeadId,SectionName as LeadField,CASE s1.SectionId WHEN 5561 THEN 1 ELSE 0 END as Required from ")
            .Append("adQuickLeadSections s1,adLeadFields s2 where s1.SectionId = s2.LeadField ")
            .Append(" and s2.LeadField <> 5555 ")
            .Append(" order by t2.FldName ")

        End With
        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllNotRequiredLeadFields() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append(" select Distinct t1.FldId,t1.FldName,(select Distinct Caption from syFldCaptions where FldId=t1.FldId) as Caption ")
            .Append(" from syFields t1,adExpQuickLeadSections  t2 ")
            .Append(" where t1.FldID = t2.FldId And t2.SectionId = 5555 and t1.FldId not in (select Distinct LeadId from adLeadFields where LeadField=5555) ")
            .Append(" union ")
            .Append(" select Distinct SectionId as FldId,NULL as FldName,SectionName as Caption from adQuickLeadSections ")
            .Append(" where SectionId <> 5555 and SectionId not in (select Distinct LeadField from adLeadFields) ")
            .Append(" Order by t1.FldName ")

            '.Append(" select syFields.fldid,syFields.fldname,syFldCaptions.Caption  from sytblflds,sytables,syFields,syFldCaptions where syTables.tblid = syTblflds.tblid   ")
            '.Append(" and sytblflds.fldid = syFields.fldid and syFields.FldId =syFldCaptions.FldId and syTables.tblname = 'adLeads' and syFields.Fldid not in ")
            '.Append(" (select LeadID from adLeadFields)  ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function UpdateModules(ByVal user As String, ByVal selectedDegrees() As String, ByVal selectedText() As String)
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   First we have to delete all existing selections
        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append(" DELETE FROM adLeadFields ")
        End With
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'insert record for lead group information
        With sb
            .Append(" INSERT INTO adLeadFields (LeadId,LeadField,ModDate, ModUser) ")
            .Append("VALUES(5561,5561,?,?)")
        End With
        'add parameters
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'execute query
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        '   Insert one record per each Item in the Selected Group
        Dim i As Integer
        Dim strLeadField As String
        For i = 0 To selectedDegrees.Length - 1
            strLeadField = DirectCast(selectedDegrees.GetValue(i), String)
            If Not strLeadField = "5556" Or Not strLeadField = "5557" Or Not strLeadField = "5558" Or Not strLeadField = "5559" Or Not strLeadField = "5560" Or Not strLeadField = "5561" Then
                'build query
                With sb
                    .Append("INSERT INTO adLeadFields (LeadId,LeadField,ModDate, ModUser) ")
                    .Append("VALUES(?,?,?,?)")
                End With

                'add parameters
                db.AddParameter("@LeadId", DirectCast(selectedDegrees.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                ' db.AddParameter("@LeadField", DirectCast(selectedText.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@LeadField", 5555, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'execute query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            End If
            If strLeadField = "5556" Then
                With sb
                    .Append(" INSERT INTO adLeadFields (LeadId,LeadField,ModDate, ModUser) ")
                    .Append(" Select FldId,5556,?,? from adExpQuickLeadSections where SectionId=5556 ")
                End With
                'add parameters
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'execute query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            End If
            If strLeadField = "5557" Then
                With sb
                    .Append(" INSERT INTO adLeadFields (LeadId,LeadField,ModDate, ModUser) ")
                    .Append(" Select FldId,5557,?,? from adExpQuickLeadSections where SectionId=5557 ")
                End With
                'add parameters
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'execute query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            End If
            If strLeadField = "5558" Then
                With sb
                    .Append(" INSERT INTO adLeadFields (LeadId,LeadField,ModDate, ModUser) ")
                    .Append(" Select FldId,5558,?,? from adExpQuickLeadSections where SectionId=5558 ")
                End With
                'add parameters
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'execute query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            End If
            If strLeadField = "5559" Then
                With sb
                    .Append(" INSERT INTO adLeadFields (LeadId,LeadField,ModDate, ModUser) ")
                    .Append(" Select FldId,5559,?,? from adExpQuickLeadSections where SectionId=5559 ")
                End With
                'add parameters
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'execute query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            End If
            If strLeadField = "5560" Then
                With sb
                    .Append(" INSERT INTO adLeadFields (LeadId,LeadField,ModDate, ModUser) ")
                    .Append(" Select FldId,5560,?,? from adExpQuickLeadSections where SectionId=5560 ")
                End With
                'add parameters
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'execute query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            End If
        Next

        'Close Connection
        db.CloseConnection()

    End Function
    Public Function GetAllLeads(ByVal showActiveOnly As Boolean) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   BC.LastName,BC.FirstName,BC.MiddleName,BC.EmployerContactId ")
            .Append("FROM     plEmployerContact BC, syStatuses ST ")
            .Append("WHERE    BC.StatusId = ST.StatusId ")


            '   Conditionally include only Active Items 
            If showActiveOnly Then
                .Append(" AND     ST.Status = 'Active' ")
            Else
                .Append(" AND ST.Status = 'Inactive' ")
            End If
            .Append("ORDER BY BC.FirstName ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllStudentField() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select syFields.FldName from syFields,syTblflds where ")
            .Append(" syFields.FldID = syTblflds.FldID and  ")
            .Append(" tblid=86 ")
        End With
        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function

    Public Function GetAllFieldCount() As Integer

        '   connect to the database
        Dim db As New DataAccess
        Dim result As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select Count(syFields.FldName) as FieldCount from syFields,syTblflds where ")
            .Append(" syFields.FldID = syTblflds.FldID and  ")
            .Append(" tblid=86 ")
        End With

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            result = dr(0)
        End While

        If Not dr.IsClosed Then dr.Close()

        Return result
    End Function

End Class
