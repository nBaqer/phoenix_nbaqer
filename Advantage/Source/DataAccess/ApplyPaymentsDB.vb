
Imports FAME.Advantage.Common

Public Class ApplyPaymentsDB
    Public Function GetPaymentsToApply(ByVal campusId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select  ")
            .Append("		T1.TransactionId, ")
            .Append("       T1.StuEnrollId, ")
            .Append("		(Select LastName + ' ' + FirstName from arStudent where StudentId=SE.StudentId) As StudentName, ")
            .Append("		(Select PrgVerDescrip from arPrgVersions where PrgVerId=SE.PrgVerId) As PrgVerDescrip,  ")
            .Append("		T1.TransDate, ")
            .Append("		T1.TransAmount * (-1) As TransAmount, ")
            .Append("		T1.UnAppliedAmount * (-1) As UnAppliedAmount, ")
            .Append("		T1.TransDescrip ")
            .Append("from ")
            .Append("( ")
            '.Append("--this query returns payments that are not fully applied ")
            .Append("SELECT ")
            .Append("		T.TransactionId, ")
            .Append("		T.StuEnrollId, ")
            .Append("		T.TransAmount, ")
            .Append("		T.TransDate, ")
            .Append("		T.TransDescrip, ")
            .Append("		T.AppliedAmount, ")
            .Append("		(T.TransAmount + T.AppliedAmount) As UnAppliedAmount ")
            .Append("FROM ")
            .Append("		(select  ")
            .Append("				T.TransactionId, ")
            .Append("				T.StuEnrollId, ")
            .Append("				T.TransAmount, ")
            .Append("				T.TransDate, ")
            .Append("				T.TransDescrip, ")
            .Append("				Coalesce((Select Sum(Amount) from saAppliedPayments where TransactionId=T.TransactionId),0) As AppliedAmount  ")
            .Append("		from	saTransactions T ")
            .Append("		where	TransAmount < 0 ")
            .Append("       and     T.Voided=0 ")
            .Append("   ) T ")
            .Append("WHERE ")
            .Append("		(T.TransAmount + T.AppliedAmount ) <> 0 ")
            '.Append("--end of query that returns payments that are not fully applied ")
            .Append(") T1, arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("		T1.StuEnrollId=SE.StuEnrollId ")
            .Append("AND    SE.CampusId = ? ")
            .Append("AND	T1.StuEnrollId IN ")
            .Append("( ")
            '.Append("--This query returns charges that are pending or have not been applied  ")
            .Append("select distinct ")
            .Append("		T.StuEnrollId ")
            .Append("from  ")
            .Append("		saTransactions T ")
            .Append("where ")
            .Append("		(T.TransAmount > 0) ")
            .Append("AND    T.Voided=0 ")
            .Append("AND	T.TransactionId not in ")
            .Append("( ")
            '.Append("--this query returns all charges that have been fully paid ")
            .Append("select	distinct ")
            .Append("		T.TransactionId  ")
            .Append("from  ")
            .Append("		saTransactions T, saAppliedPayments AP ")
            .Append("where ")
            .Append("		T.TransactionId=AP.ApplyToTransId ")
            '.Append("AND	T.TransAmount=AP.Amount ")
            .Append("       AND T.Voided=0 ")
            .Append("Group by T.TransactionId, T.TransAmount ")
            .Append("Having(Sum(AP.Amount) = T.TransAmount) ")
            '.Append("--end of query that returns all charges that have been fully paid ")
            .Append(") ")
            '.Append("--end of query that returns charges that are pending or not fully paid for a specific enrollmentId ")
            .Append(") ")
            .Append("ORDER BY StudentName, TransDate desc ")
        End With

        'add parameter CampusId
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAvailableCharges(ByVal transactionId As String) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT ")
            .Append("'" + transactionId + "' as PaymentId, ")
            .Append("   R.*,(R.TransAmount - ISNULL(R.TotalApplied,0)) AS Balance ")
            .Append("FROM ")
            .Append("(SELECT t1.TransactionId,t1.TransDate,Coalesce((Select TransCodeDescrip from saTransCodes where TransCodeId=t1.TransCodeId), 'No Reference') As TransCodeDescrip,t1.TransReference,t1.TransAmount, ")
            .Append("   Coalesce((SELECT SUM(t3.Amount) ")
            .Append("    FROM saAppliedPayments t3, saTransactions tr  ")
            .Append("    WHERE t1.TransactionId = t3.ApplyToTransId and t3.TransactionId=tr.TransactionId and tr.Voided=0), 0.00) AS TotalApplied ")
            .Append("FROM saTransactions t1 ")
            .Append("WHERE StuEnrollId = (Select StuEnrollId from saTransactions where TransactionId=? and Voided=0) ")
            .Append("AND t1.Voided=0 ")
            .Append("AND t1.TransAmount > 0) R ")
            .Append("WHERE (R.TransAmount - ISNULL(R.TotalApplied,0)) > 0 ")
            .Append("ORDER BY TransDate, TransCodeDescrip ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        db.AddParameter("@TransactionId", transactionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    End Function
    Public Function SaveNewAppliedPayments(ByVal appliedPaymentsCollection As ArrayList) As String
        'return if the collection is empty
        If appliedPaymentsCollection Is Nothing Then Return ""

        Dim db As New DataAccess

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            Dim sbAppliedPayment As New StringBuilder

            With sbAppliedPayment
                .Append("INSERT INTO saAppliedPayments(AppliedPmtId,TransactionId,ApplyToTransId,Amount,ModUser,ModDate) ")
                .Append("VALUES(?,?,?,?,?,?) ")
            End With

            For Each appPmtInfo As AppliedPaymentInfo In appliedPaymentsCollection

                'Add params
                db.AddParameter("@AppliedPmtId", appPmtInfo.AppliedPaymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@TransId", appPmtInfo.PaymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@chargeid", appPmtInfo.ChargeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@amount", appPmtInfo.Amount, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                db.AddParameter("@moduser", appPmtInfo.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@moddate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                'Execute the query
                db.RunParamSQLExecuteNoneQuery(sbAppliedPayment.ToString, groupTrans)
                db.ClearParameters()
            Next

            '   commit transaction 
            groupTrans.Commit()

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

End Class
