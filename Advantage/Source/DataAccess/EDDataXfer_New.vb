﻿
Imports FAME.Advantage.Common

Public Class EDDataXfer_New
    Public m_ExceptionMessage As String

#Region " Methods"
    Public Function GetAwardTypeID(ByRef db As DataAccess, ByRef AwardTypeID As String, ByVal strFund As String, ByVal strMsgType As String) As String
        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbAwardTypeID As New System.Text.StringBuilder(1000)
        Dim blnYYYY As Boolean = False
        Dim strFN As String

        'GetAwardTypeID = False
        m_ExceptionMessage = ""
        Try
            '   connect to the database
            'If Not IsNumeric(strFund) Then
            '    Return "Unable to get award type as fund value provided is invalid"
            '    Exit Function
            'End If
            If strMsgType.ToUpper = "PELL" Then
                If strFund = "" Or strFund.ToUpper = "P" Then
                    strFN = "PELL"
                ElseIf strFund = "TT" Then
                    strFN = "TEACH"
                ElseIf strFund = "A" Then
                    strFN = "ACG"
                ElseIf strFund = "S" Or strFund = "T" Then
                    strFN = "SMART"
                End If
            Else
                If strFund = "P" Then
                    strFN = "DL - PLUS"
                ElseIf strFund = "S" Then
                    strFN = "DL - SUB"
                ElseIf strFund = "U" Then
                    strFN = "DL - UNSUB"
                Else
                    strFN = ""
                End If
            End If


            sbAwardTypeID = New StringBuilder
            With sbAwardTypeID
                .Append("SELECT FundSourceId, FundSourceCode, AwardYear, advFundSourceId, ")
                .Append("(CASE WHEN (SELECT COUNT(*) FROM saFundSources LEFT OUTER JOIN syAdvFundSources ON saFundSources.AdvFundSourceId=syAdvFundSources.AdvFundSourceId WHERE  syAdvFundSources.Descrip= ? ) > 1 THEN 'True' ELSE 'False' END) AS useAwdyr ")
                .Append("FROM  saFundSources ")
                .Append("WHERE (advFundSourceId = ")
                .Append("(SELECT TOP 1 AdvFundSourceId ")
                .Append("FROM syAdvFundSources ")
                .Append("WHERE (Descrip = ?))) ")
                .Append("ORDER BY AwardYear ASC ")
            End With
            db.ClearParameters()
            db.AddParameter("@advFundSourceId", strFN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@advFundSourceId", strFN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            dr = db.RunParamSQLDataReader(sbAwardTypeID.ToString())
            'check for exception - no data
            If Not (dr.HasRows) Then
                'EXCEPTION REPORT for this query
                Return "Unable to match fundsource records for the given fund type "
                Exit Function
            End If
            While dr.Read()
                If Not (dr("FundSourceId") Is System.DBNull.Value) Then
                    If dr("useAwdyr") = "False" Then  'only one record
                        AwardTypeID = dr("FundSourceId").ToString
                    Else
                        'If Not (dr("AwardYear") Is System.DBNull.Value) Then blnYYYY = (strAwdyr = dr("AwardYear"))
                        AwardTypeID = dr("FundSourceId").ToString
                        'If blnYYYY Then Exit While
                    End If
                End If
            End While
            Return ""
        Finally
            dr.Close()
        End Try
    End Function
    Public Function GetPendingPayments(ByRef db As DataAccess, ByVal FileName As String, ByVal FilterCampusIds As String, ByVal FilterEnrollmentStatusIds As String, ByVal FilterFundSourceIds As String, ByVal FilterLenderIds As String, ByVal ExpDateFrom As String, ByVal ExpDateTo As String) As DataSet
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        'Try
        '    db.ClearParameters()
        '    db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
        '    db.AddParameter("@CampusIds", FilterCampusIds, DataAccess.OleDbDataType.OleDbString, 1000, ParameterDirection.Input)
        '    db.AddParameter("@EnrollmentStatustIds", FilterEnrollmentStatusIds, DataAccess.OleDbDataType.OleDbString, 1000, ParameterDirection.Input)
        '    db.AddParameter("@FundSourceIds", FilterFundSourceIds, DataAccess.OleDbDataType.OleDbString, 1000, ParameterDirection.Input)
        '    db.AddParameter("@LenderIds", FilterLenderIds, DataAccess.OleDbDataType.OleDbString, 1000, ParameterDirection.Input)
        '    db.AddParameter("@ExpDateFrom", IIf(ExpDateFrom & "" <> "", ExpDateFrom, DBNull.Value), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '    db.AddParameter("@ExpDateTo", IIf(ExpDateTo & "" <> "", ExpDateFrom, DBNull.Value), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '    ds = db.RunParamSQLDataSetUsingSP("usp_EdExpress_GetPendingPayments", "PendingPayments")
        'Catch

        'Finally

        'End Try
        With sb
            .Append(" Select ST.ParentId, DT.DetailId,ST.StudentAwardID, DT.AwardScheduleId,  ST.FirstName, ST.LastName,ST.SSN,ST.OriginalSSN, ST.CampusName,ST.StuEnrollmentId, ST.CampusId, ")
            .Append(" (select Max(LDA) from ( select max(AttendedDate)as LDA from arExternshipAttendance where StuEnrollId=ST.StuEnrollmentId union all select max(MeetDate) as LDA from atClsSectAttendance where StuEnrollId=ST.StuEnrollmentId and Actual >= 1 union all select max(AttendanceDate) as LDA from atAttendance where EnrollId=ST.StuEnrollmentId and Actual >=1 union all select max(RecordDate) as LDA from arStudentClockAttendance where StuEnrollId=ST.StuEnrollmentId and (ActualHours >=1.00 and  ActualHours <> 99.00 and ActualHours <> 999.00 and ActualHours <> 9999.00) union all select max(MeetDate) as LDA from atConversionAttendance where StuEnrollId=ST.StuEnrollmentId and (Actual >=1.00 and  Actual <> 99.00 and Actual <> 999.00 and Actual <> 9999.00) )TR) as LDA, ")
            ''New Code Added By Vijay Ramteke On June 25, 2010 For DL Case
            '.Append(" ST.AwardId, CASE(ST.DbIn) When 'T' Then Case (ST.GrantType )when '' then 'PELL' When 'P' then 'PELL' When 'A' then 'ACG' when 'S' then 'SMART' when 'T' then 'SMART' else '' End Else 'TEACH' End AS GrantType, DT.DisbDate as ExpDisbDate ,DT.DisbNum,DT.DusbSeqNum,DT.SimittedDisbAmount,DT.AccDisbAmount,DT.AccDisbAmount as PaymentAmount, DT.DisbDate, ST.IsInSchool AS Pay, ST.IsInSchool, ST.AcademicYearId ")
            .Append(" ST.AwardId, CASE(ST.DbIn) When 'T' Then Case (ST.GrantType )when '' then 'PELL' When 'P' then 'PELL' When 'A' then 'ACG' when 'S' then 'SMART' when 'T' then 'SMART' else '' End When 'D' Then Case (ST.GrantType ) When 'P' then 'DL-PLUS' When 's' then 'DL-SUB' when 'U' then 'DL-UNSUB' else '' End Else 'TEACH' End AS GrantType, DT.DisbDate as ExpDisbDate ,DT.DisbNum,DT.DusbSeqNum,DT.SimittedDisbAmount,DT.AccDisbAmount,DT.AccDisbAmount as PaymentAmount, DT.DisbDate, ST.IsInSchool AS Pay, ST.IsInSchool, ST.AcademicYearId ")
            ''New Code Added By Vijay Ramteke On June 25, 2010 For DL Case
            .Append(" From syEDExpNotPostPell_ACG_SMART_Teach_StudentTable ST, syEDExpNotPostPell_ACG_SMART_Teach_DisbursementTable DT ")
            If Not FilterEnrollmentStatusIds = "" Then
                .Append(", arStuEnrollments SE ")
            End If
            If Not FilterFundSourceIds = "" Or Not FilterLenderIds = "" Then
                .Append(", faStudentAwards SA ")
            End If
            .Append(" Where ST.ParentId=DT.ParentId ")
            If Not FilterEnrollmentStatusIds = "" Then
                .Append(" and SE.StuEnrollId=ST.StuEnrollmentId ")
            End If
            If Not FilterFundSourceIds = "" Or Not FilterLenderIds = "" Then
                .Append(" and SA.StudentAwardId=ST.StudentAwardId ")
            End If
            If Not FileName = "" Then
                .Append(" And ST.FileName=? ")
            End If
            If Not FilterCampusIds = "" Then
                .Append(FilterCampusIds)
            End If
            If Not FilterEnrollmentStatusIds = "" Then
                .Append(FilterEnrollmentStatusIds)
            End If
            If Not FilterFundSourceIds = "" Then
                .Append(FilterFundSourceIds)
            End If
            If Not FilterLenderIds = "" Then
                .Append(FilterLenderIds)
            End If
            ''If Not FilterCampusIds = "" Then
            ''    .Append(" And ST.CampusId in (?) ")
            ''End If
            ''If Not FilterEnrollmentStatusIds = "" Then
            ''    .Append(" And SE.StatusCodeId in(?) ")
            ''End If
            ''If Not FilterFundSourceIds = "" Then
            ''    .Append(" And ST.AwardTypeId in (?) ")
            ''End If
            ''If Not FilterLenderIds = "" Then
            ''    .Append(" And SA.LenderId in (?) ")
            ''End If
            If Not ExpDateFrom & "" = "" And Not ExpDateTo & "" = "" Then
                .Append(" And DT.DisbDate>=? and DT.DisbDate<=? ")
            End If
            If Not ExpDateFrom & "" = "" And ExpDateTo & "" = "" Then
                .Append(" And DT.DisbDate>=? ")
            End If
            If ExpDateFrom & "" = "" And Not ExpDateTo & "" = "" Then
                .Append(" And DT.DisbDate<=? ")
            End If
            .Append(" Order by ST.CampusName, ST.LastName, ST.FirstName, DisbNum,DusbSeqNum  ")
        End With
        db.ClearParameters()

        If Not FileName = "" Then
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, 250, ParameterDirection.Input)
        End If
        ''If Not FilterCampusIds = "" Then
        ''    db.AddParameter("@CampusId", FilterCampusIds, DataAccess.OleDbDataType.OleDbString, 8000, ParameterDirection.Input)
        ''End If
        ''If Not FilterEnrollmentStatusIds = "" Then
        ''    db.AddParameter("@EnrollmentStatusId", FilterEnrollmentStatusIds, DataAccess.OleDbDataType.OleDbString, 8000, ParameterDirection.Input)
        ''End If
        ''If Not FilterFundSourceIds = "" Then
        ''    db.AddParameter("@FundSourceId", FilterFundSourceIds, DataAccess.OleDbDataType.OleDbString, 8000, ParameterDirection.Input)
        ''End If
        ''If Not FilterLenderIds = "" Then
        ''    db.AddParameter("@LenderId", FilterLenderIds, DataAccess.OleDbDataType.OleDbString, 8000, ParameterDirection.Input)
        ''End If
        If Not ExpDateFrom & "" = "" And Not ExpDateTo & "" = "" Then
            db.AddParameter("@FromDate", ExpDateFrom, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@ToDate", ExpDateTo, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If
        If Not ExpDateFrom & "" = "" And ExpDateTo & "" = "" Then
            db.AddParameter("@FromDate", ExpDateFrom, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If
        If ExpDateFrom & "" = "" And Not ExpDateTo & "" = "" Then
            db.AddParameter("@ToDate", ExpDateTo, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If
        ds = db.RunParamSQLDataSet(sb.ToString, "PendingPayments")

        Return ds
    End Function
    Public Function GetFundSourceDetails(ByRef db As DataAccess, ByRef FundSourceDesc As String, ByRef FundSourceId As String, ByVal strFund As String, ByVal strMsgType As String) As String
        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbAwardTypeID As New System.Text.StringBuilder(1000)
        Dim blnYYYY As Boolean = False
        Dim strFN As String

        'GetAwardTypeID = False
        m_ExceptionMessage = ""
        Try
            If strMsgType.ToUpper = "PELL" Then
                If strFund = "" Or strFund.ToUpper = "P" Then
                    strFN = "PELL"
                ElseIf strFund = "TT" Then
                    strFN = "TEACH"
                ElseIf strFund = "A" Then
                    strFN = "ACG"
                ElseIf strFund = "S" Or strFund = "T" Then
                    strFN = "SMART"
                End If
            Else
                If strFund = "P" Then
                    strFN = "DL - PLUS"
                ElseIf strFund = "S" Then
                    strFN = "DL - SUB"
                ElseIf strFund = "U" Then
                    strFN = "DL - UNSUB"
                Else
                    strFN = ""
                End If
            End If

            sbAwardTypeID = New StringBuilder
            With sbAwardTypeID
                .Append("SELECT FundSourceId, FundSourceDescrip ")
                .Append("FROM  saFundSources ")
                .Append("WHERE (advFundSourceId = ")
                .Append("(SELECT TOP 1 AdvFundSourceId ")
                .Append("FROM syAdvFundSources ")
                .Append("WHERE (Descrip = ?))) ")
                .Append("ORDER BY AwardYear ASC ")
            End With
            db.ClearParameters()
            db.AddParameter("@advFundSourceId", strFN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'db.AddParameter("@advFundSourceId", strFN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            dr = db.RunParamSQLDataReader(sbAwardTypeID.ToString())
            'check for exception - no data
            If Not (dr.HasRows) Then
                'EXCEPTION REPORT for this query
                Return "Unable to match fundsource records for the given fund type "
                Exit Function
            End If
            While dr.Read()
                FundSourceDesc = dr("FundSourceDescrip").ToString
                FundSourceId = dr("FundSourceId").ToString
            End While
            Return ""
        Finally
            dr.Close()
        End Try
        'Dim ds As New DataSet()
        'db.ClearParameters()
        'db.AddParameter("@GrantType", strMsgType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'ds = db.RunParamSQLDataSetUsingSP("usp_EdExpress_GetStudentInfo")
        'For Each dr As DataRow In ds.Tables(0).Rows
        '    FundSourceDesc = dr("FundSourceDescrip").ToString
        '    FundSourceId = dr("FundSourceId").ToString

        '    Return ""

    End Function
    Public Function GetFundSourceID(ByRef db As DataAccess, ByRef FundSourceID As String, ByRef RefundTypeID As Integer, ByVal strFund As String) As String
        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbAwardTypeID As New System.Text.StringBuilder(1000)
        Dim intFund As Integer

        'GetFundSourceID = False
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(strFund) Then
            Return "Unable to get fund source as the provided fund type is not found"
            Exit Function
        End If
        Try
            '   connect to the database
            If Not IsNumeric(strFund) Then
                Return "Unable to get fund source as the provided fund type must be a number"
                Exit Function
            End If
            intFund = CInt(strFund)
            sbAwardTypeID = New StringBuilder
            With sbAwardTypeID
                .Append("SELECT Top 1  FundSourceId, advFundSourceId, AwardTypeId ")
                .Append("FROM saFundSources ")
                .Append("WHERE advFundSourceId = ")
                .Append("(SELECT TOP 1 AdvFundSourceId ")
                .Append("FROM syAdvFundSources ")
                .Append("WHERE AdvFundSourceId = ?) ")
            End With
            db.ClearParameters()
            db.AddParameter("@advFundSourceId", intFund, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            '   Execute the query
            dr = db.RunParamSQLDataReader(sbAwardTypeID.ToString())
            'check for exception - no data
            If Not (dr.HasRows) Then
                Return "Unable to match fund source data found for the given fund type"
                Exit Function
            End If
            While dr.Read()
                If Not (dr("FundSourceId") Is System.DBNull.Value) Then
                    FundSourceID = dr("FundSourceId").ToString
                    If Not (dr("AwardTypeId") Is System.DBNull.Value) Then
                        RefundTypeID = dr("advFundSourceId").ToString
                    End If
                    Return ""
                End If
            End While
        Finally
            dr.Close()
        End Try
        'Return GetFundSourceID
    End Function
    Public Function GetAwardScheduleID(ByRef db As DataAccess, ByRef AwardScheduleID As String, ByVal strStudentAwardID As String, Optional ByVal StudentName As String = "") As String
        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbScheduleID As New System.Text.StringBuilder(1000)

        'GetAwardScheduleID = False
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(strStudentAwardID) Then
            Return "Unable to get award schedule as student award provided for student " & StudentName & " was not found"
            Exit Function
        End If
        '   connect to the database
        sbScheduleID = New StringBuilder
        With sbScheduleID
            .Append(" SELECT Top 1 ")
            .Append(" StudentAwardId, ExpectedDate, AwardScheduleID ")
            .Append(" FROM  faStudentAwardSchedule ")
            .Append(" WHERE(StudentAwardId =  ? )")
            .Append(" ORDER BY ExpectedDate ASC ")
        End With
        Try
            db.ClearParameters()
            db.AddParameter("@StudentAwardID", strStudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            dr = db.RunParamSQLDataReader(sbScheduleID.ToString())
            'check for exception - no data
            If Not (dr.HasRows) Then
                'EXCEPTION REPORT for this query
                Return "Unable to match award schedule data for the student " & StudentName
                Exit Function
            End If
            While dr.Read()
                If Not (dr("AwardScheduleID") Is System.DBNull.Value) Then
                    AwardScheduleID = dr("AwardScheduleID").ToString
                    Return ""
                End If
            End While
        Catch ex As System.Exception
            Return "Unable to match award schedule data for the student " & StudentName
            Exit Function
        Finally
            dr.Close()
        End Try
        'Return GetAwardScheduleID
    End Function
    Public Function GetStudentAwardID(ByRef db As DataAccess, ByRef StudentAwardID As String, ByVal strStuEnrollID As String, ByVal strFAID As String, Optional ByVal StudentName As String = "") As String
        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbAwardID As New System.Text.StringBuilder(1000)

        If String.IsNullOrEmpty(strFAID) Then
            Return "Unable to get student award for student " & StudentName & " as the FAID provided is empty"
            Exit Function
        End If

        If String.IsNullOrEmpty(strStuEnrollID) Then
            Return "Unable to find student enrollment information for the student " & StudentName
            Exit Function
        End If

        sbAwardID = New StringBuilder
        With sbAwardID
            .Append(" SELECT ")
            .Append(" StudentAwardId, fa_id, StuEnrollId ")
            .Append(" FROM  faStudentAwards ")
            .Append(" WHERE(faStudentAwards.fa_id  = ? ) AND (StuEnrollId = ? ) ")
        End With

        Try
            db.ClearParameters()
            db.AddParameter("@FAID", strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@STUENROLLID", strStuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            dr = db.RunParamSQLDataReader(sbAwardID.ToString())
            'check for exception - no data
            If Not (dr.HasRows) Then
                Return ("Unable to find student awards for the student " & StudentName & " and FA Identifier")
                Exit Function
            End If
            While dr.Read()
                If Not (dr("StudentAwardID") Is System.DBNull.Value) Then
                    StudentAwardID = dr("StudentAwardID").ToString
                    Return ""
                Else
                    Return "Unable to find a matching student award information for the student" & StudentName
                    Exit Function
                End If
            End While
        Catch ex As System.Exception
            Return "Unable to find student awards for the student " & StudentName & " and FA Identifier"
            Exit Function
        Finally
            dr.Close()
        End Try
    End Function
    Public Function GetStuEnrollIDbySSN(ByRef db As DataAccess, ByRef stuEnrollID As String, ByVal strMsgSSN As String) As String
        'returns the StuEnrollmentID given a valid SSN, else returns blank string
        Dim ExceptionObject As System.Exception = Nothing
        Dim dr As OleDbDataReader = Nothing
        Dim sbEnrollID As New System.Text.StringBuilder(1000)
        Dim strStudentID As String = ""
        Dim strEnrollId As String = ""
        Dim ds As New DataSet
        GetStuEnrollIDbySSN = False
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(strMsgSSN) Then
            Return "Student enrollment information not found as ssn is empty"
            Exit Function
        End If
        Try
            sbEnrollID = New StringBuilder
            With sbEnrollID
                .Append(" SELECT Distinct ")
                .Append(" StudentID ")
                .Append(" FROM ")
                .Append(" arStudent ")
                .Append(" WHERE ")
                .Append(" SSN = ? ")
            End With
            db.ClearParameters()
            db.AddParameter("@SSN", strMsgSSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            Try
                strStudentID = CType(db.RunParamSQLScalar(sbEnrollID.ToString), Guid).ToString
                If Not strStudentID = "" And Not strStudentID = Guid.Empty.ToString Then
                    Dim sbID As New StringBuilder
                    With sbID
                        .Append("SELECT ")
                        .Append(" SE.StuEnrollId, SC.SysStatusId ")
                        .Append("FROM ")
                        .Append(" arStuEnrollments SE, syStatusCodes SC ")
                        .Append("WHERE SE.StatusCodeId=SC.StatusCodeId AND ")
                        .Append(" SE.studentId = ?")
                        .Append(" order by EnrollDate Desc ")
                    End With
                    db.ClearParameters()
                    db.AddParameter("@StudentId", strStudentID, DataAccess.OleDbDataType.OleDbString, 0, ParameterDirection.Input)
                    Try
                        ''strEnrollId = CType(db.RunParamSQLScalar(sbID.ToString), Guid).ToString
                        ds = db.RunParamSQLDataSet(sbID.ToString)

                        Dim tempDR() As DataRow

                        tempDR = ds.Tables(0).Select("SysStatusId=9")
                        If (tempDR.Length > 0) Then
                            strEnrollId = tempDR(0)("StuEnrollId").ToString
                        Else
                            strEnrollId = ds.Tables(0).Rows(0)("StuEnrollId").ToString
                        End If

                        If Not strEnrollId = "" And Not strEnrollId = Guid.Empty.ToString Then
                            Return strEnrollId
                            Exit Function
                        Else
                            Return "Student enrollment information not found for SSN:" & strMsgSSN
                            Exit Function
                        End If
                    Catch ex As System.Exception
                        Return "Student enrollment information not found for SSN:" & strMsgSSN
                        Exit Function
                    End Try
                Else
                    Return "Student not found for the given SSN:" & strMsgSSN
                    Exit Function
                End If
            Catch ex As System.Exception
                Return "Student not found for the given SSN:" & strMsgSSN
                Exit Function
            End Try
        Catch ex As System.Exception
            Return "Student not found for the given SSN:" & strMsgSSN
            Exit Function
        End Try
    End Function
    Public Function GetStudentInfo(ByRef db As DataAccess, ByVal SSN As String, ByRef StudentFirstName As String, ByRef StudentLastName As String, ByRef StuEnrollment As String, ByRef CampusId As String, ByRef CampusName As String, ByRef IsInSchool As Boolean, ByVal AwardId As String)
        Dim ExceptionObject As System.Exception = Nothing
        Dim dr As OleDbDataReader = Nothing
        Dim sb As New System.Text.StringBuilder()
        'Dim Dbnew As New FAME.DataAccessLayer.SQLDataAccess
        Dim ds As New DataSet

        If String.IsNullOrEmpty(SSN) Then
            StudentFirstName = ""
            StudentLastName = ""
            StuEnrollment = ""
            IsInSchool = False
            CampusId = ""
            CampusName = ""
            Return "Student not found for the given SSN:" & SSN
            Exit Function
        End If
        Try
            db.ClearParameters()
            db.AddParameter("@SSN", SSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ' ''' Code Added by Kamalesh Ahuja 09th December 2010 to Award Id criteria to get the student enrollment''
            db.AddParameter("@AwardId", AwardId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '''''''''''

            ds = db.RunParamSQLDataSetUsingSP("usp_EdExpress_GetStudentInfo")

            If ds.Tables(0).Rows.Count > 0 Then
                Dim tempDR() As DataRow
                tempDR = ds.Tables(0).Select("SysStatusId=9")
                If (tempDR.Length > 0) Then
                    StudentFirstName = tempDR(0)("FirstName").ToString
                    StudentLastName = tempDR(0)("LastName").ToString
                    ''StudentName = tempDR(0)("FirstName").ToString & " " & tempDR(0)("LastName").ToString
                    StuEnrollment = tempDR(0)("StuEnrollId").ToString
                    IsInSchool = CType(tempDR(0)("InSchool").ToString, Boolean)
                    CampusId = tempDR(0)("CampusId").ToString
                    CampusName = tempDR(0)("CampDescrip").ToString
                Else
                    StudentFirstName = ds.Tables(0).Rows(0)("FirstName").ToString
                    StudentLastName = ds.Tables(0).Rows(0)("LastName").ToString
                    StuEnrollment = ds.Tables(0).Rows(0)("StuEnrollId").ToString
                    IsInSchool = CType(ds.Tables(0).Rows(0)("InSchool").ToString, Boolean)
                    CampusId = ds.Tables(0).Rows(0)("CampusId").ToString
                    CampusName = ds.Tables(0).Rows(0)("CampDescrip").ToString
                End If
                If Not CampusId = "" And Not CampusId = Guid.Empty.ToString And Not StuEnrollment = "" And Not StuEnrollment = Guid.Empty.ToString Then
                    Return ""
                    Exit Function
                Else
                    If StuEnrollment = "" And StuEnrollment = Guid.Empty.ToString Then
                        StudentFirstName = ""
                        StudentLastName = ""
                        StuEnrollment = ""
                        IsInSchool = False
                        CampusId = ""
                        CampusName = ""
                        Return "Student Enrollment information not found for SSN:" & SSN
                        Exit Function
                    ElseIf CampusId = "" And CampusId = Guid.Empty.ToString Then
                        StudentFirstName = ""
                        StudentLastName = ""
                        StuEnrollment = ""
                        IsInSchool = False
                        CampusId = ""
                        CampusName = ""
                        Return "Student campus information not found for SSN:" & SSN
                        Exit Function
                    End If
                End If
            Else
                StudentFirstName = ""
                StudentLastName = ""
                StuEnrollment = ""
                IsInSchool = False
                CampusId = ""
                CampusName = ""
                Return "Student campus information not found for SSN:" & SSN
                Exit Function
            End If
        Catch ex As System.Exception
            StudentFirstName = ""
            StudentLastName = ""
            StuEnrollment = ""
            IsInSchool = False
            CampusId = ""
            CampusName = ""
            Return "Student campus information not found for SSN:" & SSN
            Exit Function
        End Try
    End Function
    Public Function GetAwardInfoForDL(ByRef db As DataAccess, ByVal Loanid As String) As DataSet
        Dim ExceptionObject As System.Exception = Nothing
        Dim dr As OleDbDataReader = Nothing
        Dim sb As New System.Text.StringBuilder()
        Dim ds As New DataSet
        Try
            db.ClearParameters()
            db.AddParameter("@LoanId", Loanid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSetUsingSP("usp_EdExpress_GetAwardInfoForDL", "AwardInfo")
        Catch ex As Exception

        End Try
        Return ds
    End Function
    Public Function GetStudentInfo(ByRef db As DataAccess, ByVal SSN As String, ByRef StudentFirstName As String, ByRef StudentLastName As String, ByRef CampusName As String)
        Dim ExceptionObject As System.Exception = Nothing
        Dim dr As OleDbDataReader = Nothing
        Dim sb As New System.Text.StringBuilder()
        Dim ds As New DataSet

        If String.IsNullOrEmpty(SSN) Then
            Return "Student not found for the given SSN:" & SSN
            Exit Function
        End If
        Try
            With sb
                .Append(" Select S.FirstName, S.LastName, SE.StuEnrollId, SE.CampusId, SS.InSchool, SS.SysStatusId, C.CampDescrip  ")
                .Append(" From arStudent S, arStuEnrollments SE, syStatusCodes SC, sySysStatus SS, syCampuses C ")
                .Append(" Where S.StudentId=SE.StudentId And SE.StatusCodeId=SC.StatusCodeId And SC.SysStatusId=SS.SysStatusId And C.CampusId=SE.CampusId And SE.StudentId in (Select StudentId From arStudent Where SSN=?) ")
                .Append(" Order By EnrollDate Desc ")
            End With
            'Add Parameters
            db.ClearParameters()
            db.AddParameter("@SSN", SSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet(sb.ToString)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim tempDR() As DataRow
                tempDR = ds.Tables(0).Select("SysStatusId=9")
                If (tempDR.Length > 0) Then
                    StudentFirstName = tempDR(0)("FirstName").ToString
                    StudentLastName = tempDR(0)("LastName").ToString
                    CampusName = tempDR(0)("CampDescrip").ToString
                Else
                    StudentFirstName = ds.Tables(0).Rows(0)("FirstName").ToString & " " & ds.Tables(0).Rows(0)("LastName").ToString
                    StudentLastName = ds.Tables(0).Rows(0)("LastName").ToString
                    CampusName = ds.Tables(0).Rows(0)("CampDescrip").ToString
                End If
            Else
                StudentFirstName = ""
                StudentLastName = ""
                CampusName = ""
                Exit Function
            End If
        Catch ex As System.Exception
            StudentFirstName = ""
            StudentLastName = ""
            CampusName = ""
            Exit Function
        End Try
    End Function
    Public Function GetStuCampusIDbySSN(ByRef db As DataAccess, ByRef stuCampusID As String, ByVal strMsgSSN As String) As String
        'returns the StuEnrollmentID given a valid SSN, else returns blank string
        Dim ExceptionObject As System.Exception = Nothing
        Dim dr As OleDbDataReader = Nothing
        Dim sbEnrollID As New System.Text.StringBuilder(1000)
        Dim strStudentID As String = ""
        Dim ds As New DataSet
        Dim strCampusId As String = ""
        GetStuCampusIDbySSN = False
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(strMsgSSN) Then
            Return "Student campus information not found as SSN is empty"
            Exit Function
        End If
        Try
            sbEnrollID = New StringBuilder
            With sbEnrollID
                .Append(" SELECT Distinct ")
                .Append(" StudentID ")
                .Append(" FROM ")
                .Append(" arStudent ")
                .Append(" WHERE ")
                .Append(" SSN = ? ")
            End With
            db.ClearParameters()
            db.AddParameter("@SSN", strMsgSSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            Try
                strStudentID = CType(db.RunParamSQLScalar(sbEnrollID.ToString), Guid).ToString
                If Not strStudentID = "" And Not strStudentID = Guid.Empty.ToString Then
                    Dim sbID As New StringBuilder
                    With sbID
                        .Append("SELECT ")
                        .Append(" SE.CampusId, SC.SysStatusId ")
                        .Append("FROM ")
                        .Append(" arStuEnrollments SE, syStatusCodes SC ")
                        .Append("WHERE SE.StatusCodeId=SC.StatusCodeId AND ")
                        .Append(" SE.studentId = ?")
                        .Append(" order by EnrollDate Desc ")
                    End With
                    db.ClearParameters()
                    db.AddParameter("@StudentId", strStudentID, DataAccess.OleDbDataType.OleDbString, 0, ParameterDirection.Input)
                    Try
                        'strCampusId = CType(db.RunParamSQLScalar(sbID.ToString), Guid).ToString
                        ds = db.RunParamSQLDataSet(sbID.ToString)

                        Dim tempDR() As DataRow

                        tempDR = ds.Tables(0).Select("SysStatusId=9")
                        If (tempDR.Length > 0) Then
                            strCampusId = tempDR(0)("CampusId").ToString
                        Else
                            strCampusId = ds.Tables(0).Rows(0)("CampusId").ToString
                        End If

                        If Not strCampusId = "" And Not strCampusId = Guid.Empty.ToString Then
                            Return strCampusId
                            Exit Function
                        Else
                            Return "Student campus information not found for SSN:" & strMsgSSN
                            Exit Function
                        End If
                    Catch ex As System.Exception
                        Return "Student campus information not found for SSN:" & strMsgSSN
                        Exit Function
                    End Try
                Else
                    Return "Student not found for the given SSN:" & strMsgSSN
                    Exit Function
                End If
            Catch ex As System.Exception
                Return "Student not found for the given SSN:" & strMsgSSN
                Exit Function
            End Try
        Catch ex As System.Exception
            Return "Student not found for the given SSN:" & strMsgSSN
            Exit Function
        End Try
    End Function
    Public Function GetStudentInfo(ByVal strMsgSSN As String) As String
        Dim ExceptionObject As System.Exception = Nothing
        Dim dr As OleDbDataReader = Nothing
        Dim sb As New StringBuilder
        Dim dt As New DataTable
        Dim db As New DataAccess
        Dim strStudentName As String = ""

        With sb
            .Append(" Select Distinct FirstName,LastName from arStudent where SSN=? ")
        End With
        '   Execute the query
        Try
            db.ClearParameters()
            db.AddParameter("@SSN", strMsgSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            dr = db.RunParamSQLDataReader(sb.ToString())
            'check for exception - no data
            If Not (dr.HasRows) Then
                Return "Student not found for the given SSN: " & strMsgSSN
                Exit Function
            End If
            While dr.Read()
                strStudentName = dr("FirstName").ToString + " " + dr("LastName").ToString
                Return strStudentName
            End While
        Catch ex As System.Exception
            Return "Student not found for the given SSN: " & strMsgSSN
            Exit Function
        Finally
            dr.Close()
        End Try
    End Function
    Public Function Add_saTransaction(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef TransactionID As String, ByVal StuEnrollID As String, ByVal StuCampusID As String, ByVal msgEntry As EDMessageInfo_New, ByVal FundSourceDesc As String, Optional ByVal StudentName As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal IsUseExpDate As Boolean = False, Optional ByVal SpecificDate As String = "") As String
        Dim sbTransaction As New System.Text.StringBuilder(1000)
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(StuEnrollID) Then
            Return "Unable to match financial aid data with student " & StudentName & " enrollment information"
            Exit Function
        End If


        'TransactionID = Guid.NewGuid.ToString

        Dim strReference As String

        Dim sbFundDescrip As New StringBuilder
        Dim strTransDescrip As String
        Dim dbAmount As Double = 0.0
        Dim sbAcademicYear As New StringBuilder

        strTransDescrip = "EdExpress"
        If Not FundSourceDesc = "" Then
            strTransDescrip &= "-" & FundSourceDesc
        End If

        strReference = "EdExpress"
        If msgEntry.strMsgType.ToUpper = "PELL" Then
            strReference &= "-" & FormatDate(msgEntry.strDisbursementDate)
        Else
            strReference &= "-" & FormatDate(msgEntry.strActualDisbursementDate)
        End If
        'If Not FundSourceDesc = "" Then
        '    strReference &= "-" & FundSourceDesc
        'End If
        Try
            If msgEntry.strMsgType.ToUpper = "PELL" Then
                dbAmount = Convert.ToDouble(msgEntry.strAcceptedDisbursementAmount)
            Else
                dbAmount = Convert.ToDouble(msgEntry.strActualDisbursementNetAmount)
            End If

        Catch ex As Exception
            dbAmount = 0.0
        End Try



        Try
            Dim TransTypeID As Integer = 2
            Dim IsPosted As Boolean = True
            Dim IsAutomatic As Boolean = False
            Dim intDuplicates As Integer = 0
            Dim sbDuplicates As New StringBuilder
            Dim sbgetTransactionId As New StringBuilder

            If Not TransactionID + "" = "" Then
                With sbTransaction
                    .Append("Update saTransactions set StuEnrollID=?,CampusId=?,TransDate=?,TransAmount=?,TransReference=?,TransDescrip=?,moduser=?,moddate=?,TransTypeID=?,IsPosted=?,IsAutomatic=?,AcademicYearId=? ")
                    .Append(" Where TransactionID=? ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@CampusId", StuCampusID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If msgEntry.strMsgType.ToUpper = "PELL" Then
                    If IsUseExpDate = True Then
                        db.AddParameter("@TransDate", SpecificDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@TransDate", FormatDate(msgEntry.strDisbursementDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    End If
                Else
                    If IsUseExpDate = True Then
                        db.AddParameter("@TransDate", SpecificDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@TransDate", FormatDate(msgEntry.strActualDisbursementDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    End If
                End If
                db.AddParameter("@TransAmount", dbAmount * (-1.0), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@TransReference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@TransDescrip", strTransDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@TransTypeID", TransTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@IsPosted", IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@IsAutomatic", IsAutomatic, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@AcademicYearId", msgEntry.strAcademicYearId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                TransactionID = Guid.NewGuid.ToString
                With sbTransaction
                    .Append("INSERT INTO saTransactions(TransactionID,StuEnrollID,CampusId,TransDate,TransAmount,CreateDate,TransReference,TransDescrip,moduser,moddate,TransTypeID,IsPosted,IsAutomatic,AcademicYearId) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@CampusId", StuCampusID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If msgEntry.strMsgType.ToUpper = "PELL" Then
                    If IsUseExpDate = True Then
                        db.AddParameter("@TransDate", SpecificDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@TransDate", FormatDate(msgEntry.strDisbursementDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    End If
                Else
                    If IsUseExpDate = True Then
                        db.AddParameter("@TransDate", SpecificDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@TransDate", FormatDate(msgEntry.strActualDisbursementDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    End If
                End If
                db.AddParameter("@TransAmount", dbAmount * (-1.0), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@CreateDate", DateTime.Now(), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@TransReference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@TransDescrip", strTransDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@TransTypeID", TransTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@IsPosted", IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@IsAutomatic", IsAutomatic, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@AcademicYearId", msgEntry.strAcademicYearId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'execute the query
            Try
                db.RunParamSQLExecuteNoneQuery(sbTransaction.ToString)
                Return ""
                Exit Function
            Catch ex As System.Exception
                Return "Unable to apply payment for " & StudentName
                Exit Function
            End Try
        Finally
        End Try
    End Function
    Public Function Add_saTransactionNew(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef TransactionID As String, ByVal StuEnrollID As String, ByVal StuCampusID As String, ByVal drChild As DataRow, ByVal FundSourceDesc As String, Optional ByVal StudentName As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal IsUseExpDate As Boolean = False, Optional ByVal SpecificDate As String = "") As String
        Dim sbTransaction As New System.Text.StringBuilder(1000)
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(StuEnrollID) Then
            Return "Unable to match financial aid data with student " & StudentName & " enrollment information"
            Exit Function
        End If


        'TransactionID = Guid.NewGuid.ToString

        Dim strReference As String

        Dim sbFundDescrip As New StringBuilder
        Dim strTransDescrip As String
        Dim dbAmount As Double = 0.0
        Dim sbAcademicYear As New StringBuilder

        strTransDescrip = "EdExpress"
        If Not FundSourceDesc = "" Then
            strTransDescrip &= "-" & FundSourceDesc
        End If

        strReference = "EdExpress"
        If drChild("dbIndicator").ToString.ToUpper = "T" Then
            strReference &= "-" & drChild("DisbDate").ToString
        Else
            strReference &= "-" & drChild("DisbDate").ToString
        End If
        'If Not FundSourceDesc = "" Then
        '    strReference &= "-" & FundSourceDesc
        'End If
        Try
            If drChild("dbIndicator").ToString.ToUpper = "T" Then
                dbAmount = Convert.ToDouble(drChild("AccDisbAmt").ToString)
            Else
                dbAmount = Convert.ToDouble(drChild("AccDisbAmt").ToString)
            End If

        Catch ex As Exception
            dbAmount = 0.0
        End Try



        Try
            Dim TransTypeID As Integer = 2
            Dim IsPosted As Boolean = True
            Dim IsAutomatic As Boolean = False
            Dim intDuplicates As Integer = 0
            Dim sbDuplicates As New StringBuilder
            Dim sbgetTransactionId As New StringBuilder

            If Not TransactionID + "" = "" Then
                With sbTransaction
                    .Append("Update saTransactions set StuEnrollID=?,CampusId=?,TransDate=?,TransAmount=?,TransReference=?,TransDescrip=?,moduser=?,moddate=?,TransTypeID=?,IsPosted=?,IsAutomatic=?,AcademicYearId=? ")
                    .Append(" Where TransactionID=? ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@CampusId", StuCampusID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If IsUseExpDate = True Then
                    db.AddParameter("@TransDate", drChild("SpecifiedDate").ToString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Else
                    db.AddParameter("@TransDate", drChild("DisbDate").ToString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                'If msgEntry.strMsgType.ToUpper = "PELL" Then
                '    If IsUseExpDate = True Then
                '        db.AddParameter("@TransDate", SpecificDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                '    Else
                '        db.AddParameter("@TransDate", FormatDate(msgEntry.strDisbursementDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                '    End If
                'Else
                '    If IsUseExpDate = True Then
                '        db.AddParameter("@TransDate", SpecificDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                '    Else
                '        db.AddParameter("@TransDate", FormatDate(msgEntry.strActualDisbursementDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                '    End If
                'End If
                db.AddParameter("@TransAmount", dbAmount * (-1.0), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@TransReference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@TransDescrip", strTransDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moduser", "sa", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", DateTime.Now.ToString("MM/dd/yyyy"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@TransTypeID", TransTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@IsPosted", IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@IsAutomatic", IsAutomatic, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@AcademicYearId", drChild("AcademicYrId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                TransactionID = Guid.NewGuid.ToString
                With sbTransaction
                    .Append("INSERT INTO saTransactions(TransactionID,StuEnrollID,CampusId,TransDate,TransAmount,CreateDate,TransReference,TransDescrip,moduser,moddate,TransTypeID,IsPosted,IsAutomatic,AcademicYearId) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@CampusId", StuCampusID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If IsUseExpDate = True Then
                    db.AddParameter("@TransDate", drChild("SpecifiedDate").ToString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Else
                    db.AddParameter("@TransDate", drChild("DisbDate").ToString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                'If msgEntry.strMsgType.ToUpper = "PELL" Then
                '    If IsUseExpDate = True Then
                '        db.AddParameter("@TransDate", SpecificDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                '    Else
                '        db.AddParameter("@TransDate", FormatDate(msgEntry.strDisbursementDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                '    End If
                'Else
                '    If IsUseExpDate = True Then
                '        db.AddParameter("@TransDate", SpecificDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                '    Else
                '        db.AddParameter("@TransDate", FormatDate(msgEntry.strActualDisbursementDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                '    End If
                'End If
                db.AddParameter("@TransAmount", dbAmount * (-1.0), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@CreateDate", DateTime.Now(), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@TransReference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@TransDescrip", strTransDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moduser", "sa", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", DateTime.Now.ToString("MM/dd/yyyy"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@TransTypeID", TransTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@IsPosted", IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@IsAutomatic", IsAutomatic, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@AcademicYearId", drChild("AcademicYrId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'execute the query
            Try
                db.RunParamSQLExecuteNoneQuery(sbTransaction.ToString)
                Return ""
                Exit Function
            Catch ex As System.Exception
                Return "Unable to apply payment for " & StudentName
                Exit Function
            End Try
        Finally
        End Try
    End Function
    Public Function Add_saPayments(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal TransactionID As String, ByVal msgEntry As EDMessageInfo_New, Optional ByVal StudentName As String = "") As String
        Dim sbPayments As New System.Text.StringBuilder(1000)
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(TransactionID) Then
            Return "Unable to apply payments for student " & StudentName & " as transaction is not found"
            Exit Function
        End If

        Dim intDuplicates As Integer = 0
        Dim sbAwardScheduleId As New StringBuilder
        Dim sbDuplicates As New StringBuilder
        Dim sbgetTransactionId As New StringBuilder
        Dim PaymentTypeID As Integer = 2
        Dim SchedulePayment As Boolean = False
        Dim IsDeposited As Boolean = False
        With sbDuplicates
            .Append(" select Count(*) from saPayments where TransactionId=? and PaymentTypeId=?")
        End With
        db.ClearParameters()
        db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PaymentTypeID", PaymentTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        Try
            intDuplicates = db.RunParamSQLScalar(sbDuplicates.ToString)
            If intDuplicates >= 1 Then
                Return ""
                Exit Function
            End If
        Catch ex As System.Exception
        End Try

        If intDuplicates = 0 Then
            Try
                With sbPayments
                    .Append("INSERT INTO saPayments(TransactionID, CheckNumber, ScheduledPayment, PaymentTypeID, IsDeposited, moduser, moddate) ")
                    .Append("VALUES(?,?,?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@CheckNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ScheduledPayment", SchedulePayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@PaymentTypeID", PaymentTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@IsDeposited", SchedulePayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sbPayments.ToString)
            Catch ex As System.Exception
                'Can put a message here but may disrupt the flow of RCVD Messages
                'so do nothing in the exception
                Return "Unable to post payment for " & StudentName
                Exit Function
            End Try
        End If
        'Return Add_saPayments
    End Function
    Public Function Add_saPaymentsNew(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal TransactionID As String, ByVal drChild As DataRow, Optional ByVal StudentName As String = "") As String
        Dim sbPayments As New System.Text.StringBuilder(1000)
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(TransactionID) Then
            Return "Unable to apply payments for student " & StudentName & " as transaction is not found"
            Exit Function
        End If

        Dim intDuplicates As Integer = 0
        Dim sbAwardScheduleId As New StringBuilder
        Dim sbDuplicates As New StringBuilder
        Dim sbgetTransactionId As New StringBuilder
        Dim PaymentTypeID As Integer = 2
        Dim SchedulePayment As Boolean = False
        Dim IsDeposited As Boolean = False
        With sbDuplicates
            .Append(" select Count(*) from saPayments where TransactionId=? and PaymentTypeId=?")
        End With
        db.ClearParameters()
        db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PaymentTypeID", PaymentTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        Try
            intDuplicates = db.RunParamSQLScalar(sbDuplicates.ToString)
            If intDuplicates >= 1 Then
                Return ""
                Exit Function
            End If
        Catch ex As System.Exception
        End Try

        If intDuplicates = 0 Then
            Try
                With sbPayments
                    .Append("INSERT INTO saPayments(TransactionID, CheckNumber, ScheduledPayment, PaymentTypeID, IsDeposited, moduser, moddate) ")
                    .Append("VALUES(?,?,?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@CheckNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ScheduledPayment", SchedulePayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@PaymentTypeID", PaymentTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@IsDeposited", SchedulePayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@moduser", "sa", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", DateTime.Now.ToString("MM/dd/yyyy"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sbPayments.ToString)
            Catch ex As System.Exception
                'Can put a message here but may disrupt the flow of RCVD Messages
                'so do nothing in the exception
                Return "Unable to post payment for " & StudentName
                Exit Function
            End Try
        End If
        'Return Add_saPayments
    End Function

    Public Function Add_saPmtDisbRel(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef PmtDisbRelID As String, ByVal TransactionID As String, ByVal StuAwardID As String, ByVal StuAwardScheduleID As String, ByVal msgEntry As EDMessageInfo_New, Optional ByVal StudentName As String = "") As String
        Dim sbPmtDisbRel As New System.Text.StringBuilder(1000)
        Dim sbPmtDisbRel1 As New System.Text.StringBuilder(1000)
        Dim intDisbCount As Integer = 0
        Dim dbAmount As Double
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(TransactionID) Then
            Return "Unable to add payment disbursement for student" & StudentName & " as provided transaction Id is not found"
            Exit Function
        End If
        If String.IsNullOrEmpty(StuAwardID) Then
            Return "Unable to add payment disbursement as student award is missing for student " & StudentName
            Exit Function
        End If
        PmtDisbRelID = Guid.NewGuid.ToString
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        'Dim myconn As New OleDbConnection(ConfigurationManager.AppSettings("ConString"))
        Dim myconn As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString").ToString)
        myconn.Open()
        Try
            If msgEntry.strMsgType.ToUpper = "PELL" Then
                dbAmount = Convert.ToDouble(msgEntry.strAcceptedDisbursementAmount)
            Else
                dbAmount = Convert.ToDouble(msgEntry.strActualDisbursementNetAmount)
            End If

        Catch ex As Exception
            dbAmount = 0.0
        End Try

        Try
            With sbPmtDisbRel
                .Append("Select Count(*) from saPmtDisbRel where TransactionID=? and Amount=? and StuAwardID=? ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Amount", dbAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                intDisbCount = db.RunParamSQLScalar(sbPmtDisbRel.ToString)
                sbPmtDisbRel.Remove(0, sbPmtDisbRel.Length)
                If intDisbCount >= 1 Then
                    With sbPmtDisbRel
                        .Append("Select PmtDisbRelId from saPmtDisbRel where TransactionID=? and Amount=? and StuAwardID=? and AwardScheduleId=?")
                    End With
                    'Add params
                    db.ClearParameters()
                    db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@Amount", dbAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardScheduleID", StuAwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Try
                        PmtDisbRelID = CType(db.RunParamSQLScalar(sbPmtDisbRel.ToString), Guid).ToString
                        sbPmtDisbRel.Remove(0, sbPmtDisbRel.Length)
                        sbPmtDisbRel.Append("Update saPmtDisbRel Set TransactionID=?,Amount=?,StuAwardID=?,AwardScheduleId=?,moduser=?,moddate=? ")
                        sbPmtDisbRel.Append(" Where PmtDisbRelId=? ")

                        db.ClearParameters()

                        db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@Amount", dbAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                        db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@AwardScheduleId", StuAwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                        db.AddParameter("@PmtDisbRelID", PmtDisbRelID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.RunParamSQLExecuteNoneQuery(sbPmtDisbRel.ToString)
                        Return ""
                        Exit Function
                    Catch ex As System.Exception
                    End Try
                End If
            Catch ex As System.Exception
                Return "Unable to apply payment for student " & StudentName
                Exit Function
            End Try

            With sbPmtDisbRel1
                .Append("INSERT INTO saPmtDisbRel(PmtDisbRelID,TransactionID,Amount,StuAwardID,AwardScheduleId,moduser,moddate) ")
                .Append("VALUES(?,?,?,?,?,?,?) ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@PmtDisbRelID", PmtDisbRelID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Amount", dbAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AwardScheduleId", StuAwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sbPmtDisbRel1.ToString)
                Return ""
                Exit Function
            Catch ex As System.Exception
                Dim sbGetAmount As New StringBuilder
                Dim strGetAmount As String = ""
                Dim sMessage As String = ""
                With sbGetAmount
                    .Append(" select Top 1 Amount from faStudentAwardSchedule where StudentAwardId=? and (Reference is Null or Reference='') order by ExpectedDate ")
                End With
                db.ClearParameters()
                db.AddParameter("@StudentAwardId", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Try
                    strGetAmount = CType(db.RunParamSQLScalar(sbGetAmount.ToString), Decimal).ToString
                Catch ex9 As Exception
                    strGetAmount = ""
                End Try
                sMessage = "Unable to apply payment as the disbursement amount did not match for student " & StudentName & "." & vbLf
                If Not strGetAmount = "" Then
                    sMessage &= "Disbursement amount in Advantage : " & CType(strGetAmount, Decimal).ToString("#0.00")
                End If
                Return sMessage
                Exit Function
            End Try
        Finally
        End Try
        'Return Add_saPmtDisbRel
    End Function
    Public Function Add_saPmtDisbRelNew(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef PmtDisbRelID As String, ByVal TransactionID As String, ByVal StuAwardID As String, ByVal StuAwardScheduleID As String, ByVal drChild As DataRow, Optional ByVal StudentName As String = "") As String
        Dim sbPmtDisbRel As New System.Text.StringBuilder(1000)
        Dim sbPmtDisbRel1 As New System.Text.StringBuilder(1000)
        Dim intDisbCount As Integer = 0
        Dim dbAmount As Double
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(TransactionID) Then
            Return "Unable to add payment disbursement for student" & StudentName & " as provided transaction Id is not found"
            Exit Function
        End If
        If String.IsNullOrEmpty(StuAwardID) Then
            Return "Unable to add payment disbursement as student award is missing for student " & StudentName
            Exit Function
        End If
        PmtDisbRelID = Guid.NewGuid.ToString
        'Dim myconn As New OleDbConnection(ConfigurationManager.AppSettings("ConString"))
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Dim myconn As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString").ToString)
        myconn.Open()
        Try
            If drChild("dbIndicator").ToString.ToUpper = "T" Then
                dbAmount = Convert.ToDouble(drChild("AccDisbAmt").ToString)
            Else
                dbAmount = Convert.ToDouble(drChild("AccDisbAmt").ToString)
            End If

        Catch ex As Exception
            dbAmount = 0.0
        End Try

        Try
            With sbPmtDisbRel
                .Append("Select Count(*) from saPmtDisbRel where TransactionID=? and Amount=? and StuAwardID=? ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Amount", dbAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                intDisbCount = db.RunParamSQLScalar(sbPmtDisbRel.ToString)
                sbPmtDisbRel.Remove(0, sbPmtDisbRel.Length)
                If intDisbCount >= 1 Then
                    With sbPmtDisbRel
                        .Append("Select PmtDisbRelId from saPmtDisbRel where TransactionID=? and Amount=? and StuAwardID=? and AwardScheduleId=?")
                    End With
                    'Add params
                    db.ClearParameters()
                    db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@Amount", dbAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardScheduleID", StuAwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Try
                        PmtDisbRelID = CType(db.RunParamSQLScalar(sbPmtDisbRel.ToString), Guid).ToString
                        sbPmtDisbRel.Remove(0, sbPmtDisbRel.Length)
                        sbPmtDisbRel.Append("Update saPmtDisbRel Set TransactionID=?,Amount=?,StuAwardID=?,AwardScheduleId=?,moduser=?,moddate=? ")
                        sbPmtDisbRel.Append(" Where PmtDisbRelId=? ")

                        db.ClearParameters()

                        db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@Amount", dbAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                        db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@AwardScheduleId", StuAwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@moduser", "sa", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@moddate", DateTime.Now.ToString("MM/dd/yyyy"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                        db.AddParameter("@PmtDisbRelID", PmtDisbRelID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.RunParamSQLExecuteNoneQuery(sbPmtDisbRel.ToString)
                        Return ""
                        Exit Function
                    Catch ex As System.Exception
                    End Try
                End If
            Catch ex As System.Exception
                Return "Unable to apply payment for student " & StudentName
                Exit Function
            End Try

            With sbPmtDisbRel1
                .Append("INSERT INTO saPmtDisbRel(PmtDisbRelID,TransactionID,Amount,StuAwardID,AwardScheduleId,moduser,moddate) ")
                .Append("VALUES(?,?,?,?,?,?,?) ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@PmtDisbRelID", PmtDisbRelID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Amount", dbAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AwardScheduleId", StuAwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moduser", "sa", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", DateTime.Now.ToString("MM/dd/yyyy"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sbPmtDisbRel1.ToString)
                Return ""
                Exit Function
            Catch ex As System.Exception
                Dim sbGetAmount As New StringBuilder
                Dim strGetAmount As String = ""
                Dim sMessage As String = ""
                With sbGetAmount
                    .Append(" select Top 1 Amount from faStudentAwardSchedule where StudentAwardId=? and (Reference is Null or Reference='') order by ExpectedDate ")
                End With
                db.ClearParameters()
                db.AddParameter("@StudentAwardId", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Try
                    strGetAmount = CType(db.RunParamSQLScalar(sbGetAmount.ToString), Decimal).ToString
                Catch ex9 As Exception
                    strGetAmount = ""
                End Try
                sMessage = "Unable to apply payment as the disbursement amount did not match for student " & StudentName & "." & vbLf
                If Not strGetAmount = "" Then
                    sMessage &= "Disbursement amount in Advantage : " & CType(strGetAmount, Decimal).ToString("#0.00")
                End If
                Return sMessage
                Exit Function
            End Try
        Finally
        End Try
        'Return Add_saPmtDisbRel
    End Function
    Public Function Add_faAwardScheduleForPell(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef AwardScheduleID As String, ByRef TransactionID As String, ByVal StudentAwardID As String, ByVal msgEntry As EDMessageInfo_New, Optional ByVal StudentName As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal ExceptionGUID As String = "") As String
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwards As New StringBuilder
        Dim sbAwardScheduleId As New StringBuilder
        Dim intDuplicates As Integer = 0
        Dim dr As OleDbDataReader
        m_ExceptionMessage = ""
        'Add_faAwardSchedule = False
        If String.IsNullOrEmpty(StudentAwardID) Then
            Return "Unable to add disbursement as student award for student " & StudentName & " cannot be empty"
            Exit Function
        End If

        AwardScheduleID = Guid.NewGuid.ToString
        Try
            With sbCheckDuplicateAwards
                .Append(" Select Count(*) from faStudentAwardSchedule where ")
                .Append(" StudentAwardID=? and DisbursementNumber=? and SequenceNumber=? ")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@DisbursementNumber", msgEntry.strDisbursementNumber.Trim(), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@SequenceNumber", msgEntry.strDisbursementSequenceNumber, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            Try
                intDuplicates = db.RunParamSQLScalar(sbCheckDuplicateAwards.ToString)
                If intDuplicates >= 1 Then
                    With sbAwardScheduleId
                        .Append(" Select AwardScheduleID,TransactionId from faStudentAwardSchedule where ")
                        .Append(" StudentAwardID=? and DisbursementNumber=? and SequenceNumber=? ")
                    End With
                    'Add params
                    db.ClearParameters()
                    db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@DisbursementNumber", msgEntry.strDisbursementNumber.Trim(), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@SequenceNumber", msgEntry.strDisbursementSequenceNumber, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    dr = db.RunParamSQLDataReader(sbAwardScheduleId.ToString)
                    While dr.Read()
                        AwardScheduleID = dr("AwardScheduleID").ToString
                        TransactionID = dr("TransactionId").ToString
                    End While
                    dr.Close()
                    Return ""
                    Exit Function
                End If
            Catch ex As System.Exception
                intDuplicates = 0
                dr.Close()
            End Try

            With sbAwardSchedule
                .Append("INSERT INTO faStudentAwardSchedule(AwardScheduleID,StudentAwardID,ExpectedDate,Amount,Reference,moduser,moddate,recid,DisbursementNumber,SequenceNumber) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?) ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@AwardScheduleID", AwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ExpectedDate", FormatDate(msgEntry.strDisbursementDate.Trim()), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Amount", IIf(msgEntry.strAcceptedDisbursementAmount.Trim() = "", 0.0, msgEntry.strAcceptedDisbursementAmount), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@Reference", "EDExpress-" & FormatDate(msgEntry.strDisbursementDate.Trim()), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@recid", "", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@DisbursementNumber", msgEntry.strDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@SequenceNumber", msgEntry.strDisbursementSequenceNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                db.RunParamFLSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                TransactionID = ""
                Return ""
                Exit Function
            Catch ex As System.Exception
                Return "Unable to add disbursement for the selected student award for student" & StudentName
                Exit Function
            End Try
        Finally

        End Try
    End Function
    Public Function Add_faAwardScheduleForPellNew(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal StudentAwardID As String, ByVal GrantType As String, ByVal drChild As DataRow, ByVal DisbNums As String, ByVal ParentId As String, ByVal FileName As String, ByVal Recid As String, ByVal IsPostPayment As Boolean, ByVal IsPayOutOfSchool As Boolean, ByVal IsUseExpDate As Boolean, ByVal SpecifiedDate As String, ByVal PaymentType As String, Optional ByVal IsOverrideAdvDateWithEDExpDate As Boolean = True, Optional ByVal IsOverrideAdvAmtWithEDExpAmt As Boolean = True) As String
        Dim AwardScheduleId As String
        Dim ds As New DataSet
        Try
            db.ClearParameters()
            db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollmentID", drChild("StuenrollmentId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@CampusID", drChild("CampusId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ExpectedDate", drChild("DisbDate").ToString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Amount", drChild("AccDisbAmt").ToString, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@moduser", "sa", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@moddate", DateTime.Now.ToString("MM/dd/yyyy"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'db.AddParameter("@recid", "", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@recid", Recid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@DisbursementNumber", drChild("DisbNum").ToString, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@SequenceNumber", drChild("SeqNum").ToString, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@RelInd", drChild("DisbRelInd").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@GrantType", GrantType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@AcademicYearId", drChild("AcademicYrId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'db.AddParameter("@IsPostPayment", drChild("IsPostPayment").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'db.AddParameter("@IsInSchool", drChild("IsInSchool").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'db.AddParameter("@IsPayOutOfSchool", drChild("IsPayOutOfSchoolStudent").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'db.AddParameter("@IsUseExpDate", drChild("IsUseExpDate").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'db.AddParameter("@SpecifiedDate", IIf(drChild("SpecifiedDate").ToString + "" = "", DBNull.Value, drChild("SpecifiedDate").ToString), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@IsPostPayment", IsPostPayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@IsInSchool", drChild("IsInSchool").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@IsPayOutOfSchool", IsPayOutOfSchool, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@IsUseExpDate", IsUseExpDate, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@SpecifiedDate", IIf(SpecifiedDate = "", DBNull.Value, SpecifiedDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@DisbNums", DisbNums, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PaymentType", CType(PaymentType, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.AddParameter("@ParentId", ParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@DbIn", drChild("dbIndicator").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@Filter", drChild("dbFilter").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@SimittedDisbAmount", drChild("SubDisbAmt").ToString, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@ActionStatusDisb", drChild("ActionStatus").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@OriginalSSN", drChild("SSNOriginal").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, 250, ParameterDirection.Input)
            db.AddParameter("@Show", drChild("Show").ToString, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@IsOverrideAdvDateWithEDExpDate", IsOverrideAdvDateWithEDExpDate, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@IsOverrideAdvAmtWithEDExpAmt", IsOverrideAdvAmtWithEDExpAmt, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            ds = db.RunParamSQLDataSetUsingSP("usp_EdExpress_StudentAwardSchedule")
            If ds.Tables(0).Rows.Count > 0 Then
                AwardScheduleId = ds.Tables(0).Rows(0)(0).ToString
            End If

            Return AwardScheduleId

        Catch ex As System.Exception
            Return "Unable to post the record"
        End Try
    End Function
    Public Function Add_faStudentAwardsFromDLNew(ByRef groupTrans As OleDbTransaction, ByRef db As DataAccess, ByVal drParent As DataRow, ByVal DisbNumber As String, ByVal ParentId As String, ByVal FileName As String, ByVal IsOverrideAdvDateWithEDExpDate As Boolean, ByVal IsOverrideAdvAmtWithEDExpAmt As Boolean, ByVal IsPostPayment As Boolean, ByVal IsPayOutOfSchool As Boolean) As String
        Dim StudentAwardID As String = ""
        Dim ds As New DataSet
        Try
            db.ClearParameters()
            db.AddParameter("@StuEnrollID", drParent("StuEnrollmentId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@AcademicYearId", drParent("AcademicYearId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@fa_id", drParent("LoanId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@GrossAmount", drParent("GrossAmount").ToString, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@AwardStartDate", drParent("LoanStartDate").ToString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@AwardEndDate", drParent("LoanEndDate").ToString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@LoanFees", drParent("Fees").ToString, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@moduser", "sa", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@moddate", DateTime.Now.ToString("MM/dd/yyyy"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Disbursements", DisbNumber, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@LoanId", drParent("LoanId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@GrantType", drParent("LoanType").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'db.AddParameter("@IsOverrideAdvDateWithEDExpDate", drParent("IsOverrideAdvDateWithEDExpDate").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'db.AddParameter("@IsOverrideAdvAmtWithEDExpAmt", drParent("IsOverrideAdvAmtWithEDExpAm").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'db.AddParameter("@IsPostPayment", drParent("IsPostPayment").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'db.AddParameter("@IsPayOutOfSchool", drParent("IsPayOutOfSchoolStudent").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@IsOverrideAdvDateWithEDExpDate", IsOverrideAdvDateWithEDExpDate, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@IsOverrideAdvAmtWithEDExpAmt", IsOverrideAdvAmtWithEDExpAmt, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@IsPostPayment", IsPostPayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@IsPayOutOfSchool", IsPayOutOfSchool, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            db.AddParameter("@ParentId", ParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@DbIn", drParent("dbIndicator").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@Filter", drParent("dbFilter").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@FirstName", drParent("FirstName").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@LastName", drParent("LastName").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@SSN", drParent("SSN").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@CampusName", drParent("Campus").ToString, DataAccess.OleDbDataType.OleDbString, 500, ParameterDirection.Input)
            db.AddParameter("@CampusId", drParent("CampusId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@AddDate", drParent("AddDate").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@AddTime", drParent("AddTime").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@OriginalSSN", drParent("SSNOriginal").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@UpdateDate", drParent("UpdatedDate").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@UpdateTime", drParent("UpdatedTime").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@OriginationStatus", "", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, 250, ParameterDirection.Input)
            db.AddParameter("@IsInSchool", drParent("IsInSchool").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            ds = db.RunParamSQLDataSetUsingSP("usp_EdExpress_StudentAward_DL")
            If ds.Tables(0).Rows.Count > 0 Then
                StudentAwardID = ds.Tables(0).Rows(0)(0).ToString
            End If
            If StudentAwardID.Contains("Unable award") Then
                StudentAwardID = "Unable to match fund source data found for the given fund type"
                Return StudentAwardID
            Else
                Return StudentAwardID
            End If

        Catch ex As System.Exception
            Return "Unable to post the record"
        End Try

    End Function
    Public Function Add_faAwardScheduleForDLNew(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal StudentAwardID As String, ByVal GrantType As String, ByVal drChild As DataRow, ByVal DisbNums As String, ByVal ParentId As String, ByVal FileName As String, ByVal Recid As String, ByVal IsPostPayment As Boolean, ByVal IsPayOutOfSchool As Boolean, ByVal IsUseExpDate As Boolean, ByVal SpecifiedDate As String, ByVal PaymentType As String, Optional ByVal ProcessMsgType As String = "SD", Optional ByVal IsOverrideAdvDateWithEDExpDate As Boolean = True, Optional ByVal IsOverrideAdvAmtWithEDExpAmt As Boolean = True) As String
        Dim AwardScheduleId As String
        Dim ds As New DataSet
        Try
            db.ClearParameters()
            db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollmentID", drChild("StuenrollmentId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@CampusID", drChild("CampusId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ExpectedDate", drChild("DisbDate").ToString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Amount", drChild("NetAmt").ToString, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@moduser", "sa", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@moddate", DateTime.Now.ToString("MM/dd/yyyy"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'db.AddParameter("@recid", "", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@recid", Recid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@DisbursementNumber", drChild("DisbNum").ToString, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@SequenceNumber", drChild("SeqNum").ToString, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@RelInd", drChild("DisbRelInd").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@GrantType", GrantType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@AcademicYearId", drChild("AcademicYrId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'db.AddParameter("@IsPostPayment", drChild("IsPostPayment").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'db.AddParameter("@IsInSchool", drChild("IsInSchool").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'db.AddParameter("@IsPayOutOfSchool", drChild("IsPayOutOfSchoolStudent").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'db.AddParameter("@IsUseExpDate", drChild("IsUseExpDate").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'db.AddParameter("@SpecifiedDate", IIf(drChild("SpecifiedDate").ToString + "" = "", DBNull.Value, drChild("SpecifiedDate").ToString), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@IsPostPayment", IsPostPayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@IsInSchool", drChild("IsInSchool").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@IsPayOutOfSchool", IsPayOutOfSchool, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@IsUseExpDate", IsUseExpDate, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@SpecifiedDate", IIf(SpecifiedDate = "", DBNull.Value, SpecifiedDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@DisbNums", DisbNums, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PaymentType", CType(PaymentType, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.AddParameter("@ParentId", ParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@DbIn", drChild("dbIndicator").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@Filter", drChild("dbFilter").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@SimittedDisbAmount", drChild("NetAmt").ToString, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@ActionStatusDisb", "", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@OriginalSSN", drChild("SSNOriginal").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, 250, ParameterDirection.Input)
            db.AddParameter("@Show", drChild("Show").ToString, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ProcessMsgType", ProcessMsgType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@IsOverrideAdvDateWithEDExpDate", IsOverrideAdvDateWithEDExpDate, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@IsOverrideAdvAmtWithEDExpAmt", IsOverrideAdvAmtWithEDExpAmt, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            ds = db.RunParamSQLDataSetUsingSP("usp_EdExpress_StudentAwardSchedule_DL")

            If ds.Tables(0).Rows.Count > 0 Then
                AwardScheduleId = ds.Tables(0).Rows(0)(0).ToString
            End If

            Return AwardScheduleId

        Catch ex As System.Exception
            Return "Unable to post the record"
        End Try
    End Function
    Public Function PostPendingDisbursement(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal PayOutOfSchool As Boolean, ByVal IsUsedExpDate As Boolean, ByVal SpecifiedDate As String, ByVal Recid As String, ByVal PaymentType As Integer, ByVal dr As DataRow) As String
        Dim AwardScheduleId As String
        Dim ds As New DataSet
        Try
            db.ClearParameters()
            db.AddParameter("@StudentAwardID", dr("StudentAwardID").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@AwardScheduleID", dr("AwardScheduleId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollmentID", dr("StuEnrollmentId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@CampusID", dr("CampusId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ExpectedDate", dr("ExpDisbDate").ToString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Amount", dr("PaymentAmount").ToString, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@moduser", "sa", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@moddate", DateTime.Now.ToString("MM/dd/yyyy"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@recid", Recid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@GrantType", dr("GrantType").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@AcademicYearId", dr("AcademicYearId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@IsPostPayment", dr("Pay").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@IsInSchool", dr("IsInSchool").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@IsPayOutOfSchool", PayOutOfSchool, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@IsUseExpDate", IsUsedExpDate, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@SpecifiedDate", IIf(SpecifiedDate = "", DBNull.Value, SpecifiedDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@PaymentType", CType(PaymentType, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ParentId", dr("ParentId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@DetailId", dr("DetailId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            ds = db.RunParamSQLDataSetUsingSP("usp_EdExpress_PostPendingDisbursement")
            If ds.Tables(0).Rows.Count > 0 Then
                AwardScheduleId = ds.Tables(0).Rows(0)(0).ToString
            End If

            Return AwardScheduleId

        Catch ex As System.Exception
            Return "Unable to post the record"
        End Try
    End Function
    Public Function RemoveDisbursementFromNotPostTable(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal ParentId As String, ByVal DetailId As String) As String
        ''  Dim AwardScheduleId As String
        Dim ds As New DataSet
        Try
            db.ClearParameters()
            db.AddParameter("@ParentId", ParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@DetailId", DetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSetUsingSP("usp_EdExpressDeleteDisbursementFormNotPost")
            Return ""
        Catch ex As System.Exception
            Return "Unable to delete the record"
        End Try
    End Function
    Public Function Add_faAwardScheduleForPellNew(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef AwardScheduleID As String, ByRef TransactionID As String, ByVal StudentAwardID As String, ByVal drChild As DataRow, Optional ByVal StudentName As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal ExceptionGUID As String = "") As String
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwards As New StringBuilder
        Dim sbAwardScheduleId As New StringBuilder
        Dim intDuplicates As Integer = 0
        Dim dr As OleDbDataReader
        m_ExceptionMessage = ""
        'Add_faAwardSchedule = False
        If String.IsNullOrEmpty(StudentAwardID) Then
            Return "Unable to add disbursement as student award for student " & StudentName & " cannot be empty"
            Exit Function
        End If

        AwardScheduleID = Guid.NewGuid.ToString
        Try
            With sbCheckDuplicateAwards
                .Append(" Select Count(*) from faStudentAwardSchedule where ")
                .Append(" StudentAwardID=? and DisbursementNumber=? and SequenceNumber=? ")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@DisbursementNumber", drChild("DisbNum").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@SequenceNumber", drChild("SeqNum").ToString, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            Try
                intDuplicates = db.RunParamSQLScalar(sbCheckDuplicateAwards.ToString)
                If intDuplicates >= 1 Then
                    With sbAwardScheduleId
                        .Append(" Select AwardScheduleID,TransactionId from faStudentAwardSchedule where ")
                        .Append(" StudentAwardID=? and DisbursementNumber=? and SequenceNumber=? ")
                    End With
                    'Add params
                    db.ClearParameters()
                    db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@DisbursementNumber", drChild("DisbNum").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@SequenceNumber", drChild("SeqNum").ToString, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    dr = db.RunParamSQLDataReader(sbAwardScheduleId.ToString)
                    While dr.Read()
                        AwardScheduleID = dr("AwardScheduleID").ToString
                        TransactionID = dr("TransactionId").ToString
                    End While
                    dr.Close()
                    Return ""
                    Exit Function
                End If
            Catch ex As System.Exception
                intDuplicates = 0
                dr.Close()
            End Try

            With sbAwardSchedule
                .Append("INSERT INTO faStudentAwardSchedule(AwardScheduleID,StudentAwardID,ExpectedDate,Amount,Reference,moduser,moddate,recid,DisbursementNumber,SequenceNumber) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?) ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@AwardScheduleID", AwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ExpectedDate", drChild("DisbDate").ToString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Amount", drChild("AccDisbAmt").ToString, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@Reference", "EDExpress-" & drChild("DisbDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moduser", "sa", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", DateTime.Now.ToString("MM/dd/yyyy"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@recid", "", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@DisbursementNumber", drChild("DisbNum").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@SequenceNumber", drChild("SeqNum").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                db.RunParamFLSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                TransactionID = ""
                Return ""
                Exit Function
            Catch ex As System.Exception
                Return "Unable to add disbursement for the selected student award for student" & StudentName
                Exit Function
            End Try
        Finally

        End Try
    End Function
    Public Function Update_faAwardScheduleForPell(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef AwardScheduleID As String, ByRef TransactionID As String) As String
        Dim sbAwardSchedule As New StringBuilder
        Try
            With sbAwardSchedule
                .Append("Update faStudentAwardSchedule Set TransactionId=? ")
                .Append(" Where AwardScheduleID=? ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AwardScheduleID", AwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                db.RunParamFLSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                Return ""
                Exit Function
            Catch ex As System.Exception
                Return "Unable to update transaction id for the selected award schedule "
                Exit Function
            End Try
        Finally

        End Try
    End Function
    Public Function Add_faAwardScheduleForDL(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef AwardScheduleID As String, ByRef TransactionID As String, ByVal StudentAwardID As String, ByVal msgEntry As EDMessageInfo_New, Optional ByVal StudentName As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal ExceptionGUID As String = "") As String
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbAwardScheduleId As New StringBuilder
        Dim sbCheckDuplicateAwards As New StringBuilder
        Dim sbCheckDuplicateAwardsNoDate As New StringBuilder
        Dim intDuplicatesNoDate As Integer = 0
        Dim intDuplicates As Integer = 0
        Dim dr As OleDbDataReader
        Dim intDisbNum As Integer = 0
        Dim intLoop As Integer = 1
        m_ExceptionMessage = ""
        'Add_faAwardSchedule = False
        If String.IsNullOrEmpty(StudentAwardID) Then
            Return "Unable to add disbursement as student award for student " & StudentName & " cannot be empty"
            Exit Function
        End If

        AwardScheduleID = Guid.NewGuid.ToString
        Try
            If msgEntry.strDatabaseIndicator.ToUpper = "M" Then
                intDisbNum = Convert.ToInt32(msgEntry.strActualDisbursementNumber)
                With sbCheckDuplicateAwards
                    .Append(" Select AwardScheduleID,TransactionId from faStudentAwardSchedule where ")
                    .Append(" StudentAwardID=?  Order By ExpectedDate Asc")
                End With
                db.ClearParameters()
                db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                dr = db.RunParamSQLDataReader(sbCheckDuplicateAwards.ToString)
                While dr.Read()
                    If intLoop = intDisbNum Then
                        AwardScheduleID = dr("AwardScheduleID").ToString
                        TransactionID = dr("TransactionId").ToString
                        Exit While
                    End If
                    intLoop += 1
                End While
                dr.Close()
                Return ""
                Exit Function
            Else
                With sbCheckDuplicateAwards
                    .Append(" Select Count(*) from faStudentAwardSchedule where ")
                    .Append(" StudentAwardID=? ")
                    .Append(" and DisbursementNumber=?")
                    .Append(" and SequenceNumber=? ")
                End With
                'Add params
                db.ClearParameters()
                db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If msgEntry.strDatabaseIndicator.ToUpper = "N" Then
                    db.AddParameter("@DisbursementNumber", msgEntry.strAnticipatedDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@SequenceNumber", msgEntry.strDisbursementSequenceNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'db.AddParameter("@Amount", IIf(msgEntry.strAnticipatedDisbursementNetAmount.Trim() = "", 0.0, msgEntry.strAnticipatedDisbursementNetAmount), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                Else
                    db.AddParameter("@DisbursementNumber", msgEntry.strActualDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@SequenceNumber", msgEntry.strActualDisbursementSequenceNumber, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                End If

                Try
                    intDuplicates = db.RunParamSQLScalar(sbCheckDuplicateAwards.ToString)
                    If intDuplicates >= 1 Then
                        With sbAwardScheduleId
                            .Append(" Select AwardScheduleID,TransactionId from faStudentAwardSchedule where ")
                            .Append(" StudentAwardID=? and DisbursementNumber=? and SequenceNumber=? ")
                        End With
                        'Add params
                        db.ClearParameters()
                        db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        If msgEntry.strDatabaseIndicator.ToUpper = "N" Then
                            db.AddParameter("@DisbursementNumber", msgEntry.strAnticipatedDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@SequenceNumber", msgEntry.strDisbursementSequenceNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        Else
                            db.AddParameter("@DisbursementNumber", msgEntry.strActualDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@SequenceNumber", msgEntry.strActualDisbursementSequenceNumber, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                        End If
                        dr = db.RunParamSQLDataReader(sbAwardScheduleId.ToString)
                        While dr.Read()
                            AwardScheduleID = dr("AwardScheduleID").ToString
                            TransactionID = dr("TransactionId").ToString
                        End While
                        dr.Close()
                        Return ""
                        Exit Function
                    End If
                Catch ex As System.Exception
                    intDuplicates = 0
                End Try
            End If


            With sbAwardSchedule
                .Append("INSERT INTO faStudentAwardSchedule(AwardScheduleID,StudentAwardID,ExpectedDate,Amount,Reference,moduser,moddate,recid,DisbursementNumber,SequenceNumber) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?) ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@AwardScheduleID", AwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            If msgEntry.strDatabaseIndicator.ToUpper = "N" Then
                db.AddParameter("@ExpectedDate", FormatDate(msgEntry.strAnticipatedDisbursementDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@Amount", IIf(msgEntry.strAnticipatedDisbursementNetAmount.Trim() = "", 0.0, msgEntry.strAnticipatedDisbursementNetAmount), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            Else
                db.AddParameter("@ExpectedDate", FormatDate(msgEntry.strActualDisbursementDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@Amount", IIf(msgEntry.strActualDisbursementNetAmount.Trim() = "", 0.0, msgEntry.strActualDisbursementNetAmount), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            End If
            If msgEntry.strDatabaseIndicator.ToUpper = "N" Then
                db.AddParameter("@Reference", "EDExpress-" & FormatDate(msgEntry.strAnticipatedDisbursementDate), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Reference", "EDExpress-" & FormatDate(msgEntry.strActualDisbursementDate), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@recid", "", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If msgEntry.strDatabaseIndicator.ToUpper = "N" Then
                db.AddParameter("@DisbursementNumber", msgEntry.strAnticipatedDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@DisbursementNumber", msgEntry.strDisbursementSequenceNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@DisbursementNumber", msgEntry.strActualDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@SequenceNumber", msgEntry.strActualDisbursementSequenceNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            Try
                db.RunParamFLSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                TransactionID = ""
                Return ""
                Exit Function
            Catch ex As System.Exception
                Return "Unable to add disbursement for the selected student award for student" & StudentName
                Exit Function
            End Try

        Finally
        End Try
    End Function
    Public Function Update_faAwardScheduleForDL(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef AwardScheduleID As String, ByRef TransactionID As String) As String
        Dim sbAwardSchedule As New StringBuilder
        Try
            With sbAwardSchedule
                .Append("Update faStudentAwardSchedule Set TransactionId=? ")
                .Append(" Where AwardScheduleID=? ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AwardScheduleID", AwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                db.RunParamFLSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                Return ""
                Exit Function
            Catch ex As System.Exception
                Return "Unable to update transaction id for the selected award schedule "
                Exit Function
            End Try
        Finally

        End Try
    End Function
    Public Function SaveNotPostedRecords(ByRef db As DataAccess, ByVal FileName As String, ByRef myMsgCollection As EDCollectionMessageInfos_New, ByVal MsgType As String, ByVal drP As DataRow, Optional ByVal drD1() As DataRow = Nothing, Optional ByVal drD2() As DataRow = Nothing) As String

        ''drD1 is used for Pell and Anticipated Direct Loan
        ''drD2 is used for Actual Direct Loan

        Dim sbParent As New System.Text.StringBuilder
        Dim sbChild As New StringBuilder
        Dim msgEntryParent As EDMessageInfo_New
        Dim msgEntryChild As EDMessageInfo_New
        Dim strParentId As String
        strParentId = Guid.NewGuid.ToString
        Try
            If MsgType = "PELL" Then
                msgEntryParent = myMsgCollection.Items(Convert.ToInt32(drP("IndexId").ToString))
                With sbParent
                    .Append("INSERT INTO syEDExpNotPostPell_ACG_SMART_Teach_StudentTable(ParentId,DbIn,Filter,AwardAmount,AwardId,GrantType,AddDate,AddTime,OriginalSSN,UpdateDate,UpdateTime,OriginationStatus,AcademicYearId,AwardYearStartDate,AwardYearEndDate,ModDate,FileName) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                    'Add params
                    db.ClearParameters()
                    db.AddParameter("@ParentId", strParentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@DbIn", msgEntryParent.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@Filter", msgEntryParent.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    If msgEntryParent.strDatabaseIndicator.ToUpper = "T" Then
                        db.AddParameter("@AwardAmount", msgEntryParent.strAwardAmountForEntireYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@AwardAmount", msgEntryParent.strAwardAmountForEntireSchoolYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    db.AddParameter("@AwardId", msgEntryParent.strAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@GrantType", IIf(msgEntryParent.strGrandType = "TT", "", msgEntryParent.strGrandType), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    If msgEntryParent.strDatabaseIndicator.ToUpper = "T" Then
                        db.AddParameter("@AddDate", msgEntryParent.strPellAddDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@AddDate", msgEntryParent.strTeachAddDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    If msgEntryParent.strDatabaseIndicator.ToUpper = "T" Then
                        db.AddParameter("@AddTime", msgEntryParent.strPellAddTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@AddTime", msgEntryParent.strTeachAddTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    If msgEntryParent.strDatabaseIndicator.ToUpper = "T" Then
                        db.AddParameter("@UpdateDate", msgEntryParent.strPellUpdateDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@UpdateDate", msgEntryParent.strTeachUpdateDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    If msgEntryParent.strDatabaseIndicator.ToUpper = "T" Then
                        db.AddParameter("@UpdateTime", msgEntryParent.strPellUpdateTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@UpdateTime", msgEntryParent.strTeachUpdateTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    db.AddParameter("@OriginationStatus", msgEntryParent.strOriginationStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AcademicYearId", msgEntryParent.strAcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardYearStartDate", msgEntryParent.strAwardYearStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardYearEndDate", msgEntryParent.strAwardYearEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End With
                Try
                    db.RunParamSQLExecuteNoneQuery(sbParent.ToString)
                Catch ex As System.Exception
                End Try
                sbParent = sbParent.Remove(0, sbParent.Length)
                If Not drD1 Is Nothing Then
                    For Each drT As DataRow In drD1
                        msgEntryChild = myMsgCollection.Items(Convert.ToInt32(drT("IndexID").ToString))
                        With sbChild
                            .Append("INSERT INTO syEDExpNotPostPell_ACG_SMART_Teach_DisbursementTable(DetailId,ParentId,DbIn,Filter,AccDisbAmount,DisbDate,DisbNum,DisbRelIndi,DusbSeqNum,SimittedDisbAmount,ActionStatusDisb,OriginalSSN,ModDate,FileName) ")
                            .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                            'Add params
                            db.ClearParameters()
                            db.AddParameter("@DetailId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@ExceptionReportId", strParentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DbIn", msgEntryChild.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@Filter", msgEntryChild.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@AccDisbAmount", msgEntryChild.strAcceptedDisbursementAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DisbDate", msgEntryChild.strDisbursementDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DisbNum", msgEntryChild.strDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DisbRelIndi", msgEntryChild.strDisbursementRealeaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DusbSeqNum", msgEntryChild.strDisbursementSequenceNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@SimittedDisbAmount", msgEntryChild.strSubmittedDisbursementAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@ActionStatusDisb", msgEntryChild.strActionStatusDisbursement, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        End With
                        Try
                            db.RunParamSQLExecuteNoneQuery(sbChild.ToString)
                        Catch ex As System.Exception
                        End Try
                        sbChild = sbChild.Remove(0, sbChild.Length)
                    Next
                End If

            Else
                ''msgEntryParent = myMsgCollection.Items(Convert.ToInt32(drP("IndexID").ToString))
                ''intNPDM += 1
                ''With sbParent
                ''    .Append("INSERT INTO syEDExpressExceptionReportDirectLoan_StudentTable(ExceptionReportId,DbIn,Filter,AddDate,AddTime,LoanAmtApproved,LoanFeePer,LoanId,LoanPeriodEndDate,LoanPeriodStartDate,LoanStatus,LoanType,MPNStatus,OriginalSSN,UpdateDate,UpdateTime,AcademicYearId,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                ''    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                ''    'Add params
                ''    db.ClearParameters()
                ''    db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@DbIn", msgEntryParent.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@Filter", msgEntryParent.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@AddDate", msgEntryParent.strLoanAddDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@AddTime", msgEntryParent.strLoanAddTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanAmtApproved", msgEntryParent.strLoanAmountApproved, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanFeePer", msgEntryParent.strLoanFeePercentage, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanId", msgEntryParent.strLoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanPeriodEndDate", msgEntryParent.strLoanPeriodEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanPeriodStartDate", msgEntryParent.strLoanPeriodStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanStatus", msgEntryParent.strLoanStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanType", msgEntryParent.strLoanType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@MPNStatus", msgEntryParent.strMPNStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@UpdateDate", msgEntryParent.strLoanUpdateDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@UpdateTime", msgEntryParent.strLoanUpdateTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@AcademicYearId", msgEntryParent.strAcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                ''    db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''End With
                ''Try
                ''    db.RunParamSQLExecuteNoneQuery(sbParent.ToString)
                ''Catch ex As System.Exception
                ''End Try
                ''sbParent = sbParent.Remove(0, sbParent.Length)
                ''If Not drD1 Is Nothing Then
                ''    For Each drT1 As DataRow In drD1
                ''        msgEntryChild = myMsgCollection.Items(Convert.ToInt32(drT1("IndexID").ToString))
                ''        If msgEntryChild.blnIsParsed Then
                ''            intNPDD += 1
                ''            With sbChild
                ''                .Append("INSERT INTO syEDExpressExceptionReportDirectLoan_DisbursementTable(DetailId,ExceptionReportId,DbIn,Filter,DisbDate,DisbGrossAmount,DisbIntRebAmount,DisbLoanFeeAmount,DisbNetAdjAmount,DisbNetAmount,DisbNum,DisbSeqNum,DisbStatus_RelInd,DisbType,LoanId,OriginalSSN,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                ''                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                ''                'Add params
                ''                db.ClearParameters()
                ''                db.AddParameter("@DetailId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DbIn", msgEntryChild.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@Filter", msgEntryChild.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbDate", msgEntryChild.strAnticipatedDisbursementDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbGrossAmount", msgEntryChild.strAnticipatedDisbursementGrossAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbIntRebAmount", msgEntryChild.strAnticipatedDisbursementInterestRebateAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbLoanFeeAmount", msgEntryChild.strAnticipatedDisbursementFeeAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbNetAdjAmount", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbNetAmount", msgEntryChild.strAnticipatedDisbursementNetAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbNum", msgEntryChild.strAnticipatedDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbSeqNum", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbStatus_RelInd", msgEntryChild.strAnticipatedDisbursementRealeaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbType", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@LoanId", msgEntryChild.strLoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                ''                db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''            End With
                ''            Try
                ''                db.RunParamSQLExecuteNoneQuery(sbChild.ToString)
                ''            Catch ex As System.Exception
                ''            End Try
                ''        End If
                ''        sbChild = sbChild.Remove(0, sbChild.Length)
                ''    Next
                ''End If
                ''If Not drD2 Is Nothing Then
                ''    For Each drT2 As DataRow In drD2
                ''        msgEntryChild = myMsgCollection.Items(Convert.ToInt32(drT2("IndexID").ToString))
                ''        If msgEntryChild.blnIsParsed Then
                ''            intNPAD += 1
                ''            With sbChild
                ''                .Append("INSERT INTO syEDExpressExceptionReportDirectLoan_DisbursementTable(DetailId,ExceptionReportId,DbIn,Filter,DisbDate,DisbGrossAmount,DisbIntRebAmount,DisbLoanFeeAmount,DisbNetAdjAmount,DisbNetAmount,DisbNum,DisbSeqNum,DisbStatus_RelInd,DisbType,LoanId,OriginalSSN,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                ''                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                ''                'Add params
                ''                db.ClearParameters()
                ''                db.AddParameter("@DetailId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DbIn", msgEntryChild.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@Filter", msgEntryChild.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbDate", msgEntryChild.strActualDisbursementDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbGrossAmount", msgEntryChild.strActualDisbursementGrossAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbIntRebAmount", msgEntryChild.strActualDisbursementInterestRebateAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbLoanFeeAmount", msgEntryChild.strActualDisbursementLoanFeeAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbNetAdjAmount", msgEntryChild.strActualDisbursementNetAdjustmentAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbNetAmount", msgEntryChild.strActualDisbursementNetAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbNum", msgEntryChild.strActualDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbSeqNum", msgEntryChild.strActualDisbursementSequenceNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbStatus_RelInd", msgEntryChild.strActualDisbursementStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbType", msgEntryChild.strActualDisbursementType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@LoanId", msgEntryChild.strLoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                ''                db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''            End With
                ''            Try
                ''                db.RunParamSQLExecuteNoneQuery(sbChild.ToString)
                ''            Catch ex As System.Exception
                ''            End Try
                ''        End If
                ''        sbChild = sbChild.Remove(0, sbChild.Length)
                ''    Next
                ''End If
            End If
        Finally
        End Try
    End Function
    Public Function SaveNotPostedRecordsNew(ByRef db As DataAccess, ByVal FileName As String, ByVal MsgType As String, ByVal drP As DataRow, Optional ByVal drD1() As DataRow = Nothing, Optional ByVal drD2() As DataRow = Nothing) As String

        ''drD1 is used for Pell and Anticipated Direct Loan
        ''drD2 is used for Actual Direct Loan

        Dim sbParent As New System.Text.StringBuilder
        Dim sbChild As New StringBuilder
        'Dim msgEntryParent As EDMessageInfo_New
        'Dim msgEntryChild As EDMessageInfo_New
        Dim strParentId As String
        strParentId = Guid.NewGuid.ToString
        Try
            If MsgType = "PELL" Then
                With sbParent
                    .Append("INSERT INTO syEDExpNotPostPell_ACG_SMART_Teach_StudentTable(ParentId,DbIn,Filter,AwardAmount,AwardId,GrantType,AddDate,AddTime,OriginalSSN,UpdateDate,UpdateTime,OriginationStatus,AcademicYearId,AwardYearStartDate,AwardYearEndDate,ModDate,FileName) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                    'Add params
                    db.ClearParameters()
                    db.AddParameter("@ParentId", strParentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@DbIn", drP("dbIndicator").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@Filter", drP("dbFilter").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    If drP("dbIndicator").ToString.ToUpper = "T" Then
                        db.AddParameter("@AwardAmount", drP("AwardAmount").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@AwardAmount", drP("AwardAmount").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    db.AddParameter("@AwardId", drP("AwardId").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@GrantType", IIf(drP("GrantType").ToString = "TT", "", drP("GrantType").ToString), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    If drP("dbIndicator").ToString.ToUpper = "T" Then
                        db.AddParameter("@AddDate", drP("AddDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@AddDate", drP("AddDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    If drP("dbIndicator").ToString.ToUpper = "T" Then
                        db.AddParameter("@AddTime", drP("AddTime").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@AddTime", drP("AddTime").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    db.AddParameter("@OriginalSSN", drP("SSNOriginal").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    If drP("dbIndicator").ToString.ToUpper = "T" Then
                        db.AddParameter("@UpdateDate", drP("UpdatedDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@UpdateDate", drP("UpdatedDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    If drP("dbIndicator").ToString.ToUpper = "T" Then
                        db.AddParameter("@UpdateTime", drP("UpdatedTime").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@UpdateTime", drP("UpdatedTime").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    db.AddParameter("@OriginationStatus", drP("OriginationStatus").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AcademicYearId", drP("AcademicYearId").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardYearStartDate", drP("AwardStartDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardYearEndDate", drP("AwardEndDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End With
                Try
                    db.RunParamSQLExecuteNoneQuery(sbParent.ToString)
                Catch ex As System.Exception
                End Try
                sbParent = sbParent.Remove(0, sbParent.Length)
                If Not drD1 Is Nothing Then
                    For Each drT As DataRow In drD1

                        With sbChild
                            .Append("INSERT INTO syEDExpNotPostPell_ACG_SMART_Teach_DisbursementTable(DetailId,ParentId,DbIn,Filter,AccDisbAmount,DisbDate,DisbNum,DisbRelIndi,DusbSeqNum,SimittedDisbAmount,ActionStatusDisb,OriginalSSN,ModDate,FileName) ")
                            .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                            'Add params
                            db.ClearParameters()
                            db.AddParameter("@DetailId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@ExceptionReportId", strParentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DbIn", drT("dbIndicator").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@Filter", drT("dbFilter").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@AccDisbAmount", drT("AccDisbAmt").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DisbDate", drT("DisbDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DisbNum", drT("DisbNum").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DisbRelIndi", drT("DisbRelInd").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DusbSeqNum", drT("SeqNum").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@SimittedDisbAmount", drT("SubDisbAmt").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@ActionStatusDisb", drT("ActionStatus").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@OriginalSSN", drT("SSNOriginal").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        End With
                        Try
                            db.RunParamSQLExecuteNoneQuery(sbChild.ToString)
                        Catch ex As System.Exception
                        End Try
                        sbChild = sbChild.Remove(0, sbChild.Length)
                    Next
                End If

            Else
                ''msgEntryParent = myMsgCollection.Items(Convert.ToInt32(drP("IndexID").ToString))
                ''intNPDM += 1
                ''With sbParent
                ''    .Append("INSERT INTO syEDExpressExceptionReportDirectLoan_StudentTable(ExceptionReportId,DbIn,Filter,AddDate,AddTime,LoanAmtApproved,LoanFeePer,LoanId,LoanPeriodEndDate,LoanPeriodStartDate,LoanStatus,LoanType,MPNStatus,OriginalSSN,UpdateDate,UpdateTime,AcademicYearId,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                ''    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                ''    'Add params
                ''    db.ClearParameters()
                ''    db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@DbIn", msgEntryParent.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@Filter", msgEntryParent.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@AddDate", msgEntryParent.strLoanAddDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@AddTime", msgEntryParent.strLoanAddTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanAmtApproved", msgEntryParent.strLoanAmountApproved, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanFeePer", msgEntryParent.strLoanFeePercentage, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanId", msgEntryParent.strLoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanPeriodEndDate", msgEntryParent.strLoanPeriodEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanPeriodStartDate", msgEntryParent.strLoanPeriodStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanStatus", msgEntryParent.strLoanStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanType", msgEntryParent.strLoanType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@MPNStatus", msgEntryParent.strMPNStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@UpdateDate", msgEntryParent.strLoanUpdateDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@UpdateTime", msgEntryParent.strLoanUpdateTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@AcademicYearId", msgEntryParent.strAcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                ''    db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''End With
                ''Try
                ''    db.RunParamSQLExecuteNoneQuery(sbParent.ToString)
                ''Catch ex As System.Exception
                ''End Try
                ''sbParent = sbParent.Remove(0, sbParent.Length)
                ''If Not drD1 Is Nothing Then
                ''    For Each drT1 As DataRow In drD1
                ''        msgEntryChild = myMsgCollection.Items(Convert.ToInt32(drT1("IndexID").ToString))
                ''        If msgEntryChild.blnIsParsed Then
                ''            intNPDD += 1
                ''            With sbChild
                ''                .Append("INSERT INTO syEDExpressExceptionReportDirectLoan_DisbursementTable(DetailId,ExceptionReportId,DbIn,Filter,DisbDate,DisbGrossAmount,DisbIntRebAmount,DisbLoanFeeAmount,DisbNetAdjAmount,DisbNetAmount,DisbNum,DisbSeqNum,DisbStatus_RelInd,DisbType,LoanId,OriginalSSN,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                ''                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                ''                'Add params
                ''                db.ClearParameters()
                ''                db.AddParameter("@DetailId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DbIn", msgEntryChild.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@Filter", msgEntryChild.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbDate", msgEntryChild.strAnticipatedDisbursementDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbGrossAmount", msgEntryChild.strAnticipatedDisbursementGrossAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbIntRebAmount", msgEntryChild.strAnticipatedDisbursementInterestRebateAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbLoanFeeAmount", msgEntryChild.strAnticipatedDisbursementFeeAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbNetAdjAmount", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbNetAmount", msgEntryChild.strAnticipatedDisbursementNetAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbNum", msgEntryChild.strAnticipatedDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbSeqNum", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbStatus_RelInd", msgEntryChild.strAnticipatedDisbursementRealeaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbType", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@LoanId", msgEntryChild.strLoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                ''                db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''            End With
                ''            Try
                ''                db.RunParamSQLExecuteNoneQuery(sbChild.ToString)
                ''            Catch ex As System.Exception
                ''            End Try
                ''        End If
                ''        sbChild = sbChild.Remove(0, sbChild.Length)
                ''    Next
                ''End If
                ''If Not drD2 Is Nothing Then
                ''    For Each drT2 As DataRow In drD2
                ''        msgEntryChild = myMsgCollection.Items(Convert.ToInt32(drT2("IndexID").ToString))
                ''        If msgEntryChild.blnIsParsed Then
                ''            intNPAD += 1
                ''            With sbChild
                ''                .Append("INSERT INTO syEDExpressExceptionReportDirectLoan_DisbursementTable(DetailId,ExceptionReportId,DbIn,Filter,DisbDate,DisbGrossAmount,DisbIntRebAmount,DisbLoanFeeAmount,DisbNetAdjAmount,DisbNetAmount,DisbNum,DisbSeqNum,DisbStatus_RelInd,DisbType,LoanId,OriginalSSN,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                ''                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                ''                'Add params
                ''                db.ClearParameters()
                ''                db.AddParameter("@DetailId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DbIn", msgEntryChild.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@Filter", msgEntryChild.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbDate", msgEntryChild.strActualDisbursementDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbGrossAmount", msgEntryChild.strActualDisbursementGrossAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbIntRebAmount", msgEntryChild.strActualDisbursementInterestRebateAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbLoanFeeAmount", msgEntryChild.strActualDisbursementLoanFeeAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbNetAdjAmount", msgEntryChild.strActualDisbursementNetAdjustmentAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbNetAmount", msgEntryChild.strActualDisbursementNetAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbNum", msgEntryChild.strActualDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbSeqNum", msgEntryChild.strActualDisbursementSequenceNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbStatus_RelInd", msgEntryChild.strActualDisbursementStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbType", msgEntryChild.strActualDisbursementType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@LoanId", msgEntryChild.strLoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                ''                db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''            End With
                ''            Try
                ''                db.RunParamSQLExecuteNoneQuery(sbChild.ToString)
                ''            Catch ex As System.Exception
                ''            End Try
                ''        End If
                ''        sbChild = sbChild.Remove(0, sbChild.Length)
                ''    Next
                ''End If
            End If
        Finally
        End Try
    End Function

    Public Function BuildExceptionReport(ByRef db As DataAccess, ByVal ErrMsg As String, ByVal FileName As String, ByRef myMsgCollection As EDCollectionMessageInfos_New, ByVal MsgType As String, ByRef intNPDM As Integer, ByRef intNPDD As Integer, ByVal drP As DataRow, Optional ByVal drD1() As DataRow = Nothing, Optional ByVal drD2() As DataRow = Nothing, Optional ByVal ExceptionGUID As String = "", Optional ByRef intNPAD As Integer = 0) As String

        ''drD1 is used for Pell and Anticipated Direct Loan
        ''drD2 is used for Actual Direct Loan

        Dim sbParent As New System.Text.StringBuilder
        Dim sbChild As New StringBuilder
        Dim msgEntryParent As EDMessageInfo_New
        Dim msgEntryChild As EDMessageInfo_New
        Dim strExceptionReportId As String
        strExceptionReportId = Guid.NewGuid.ToString
        Try
            If MsgType = "PELL" Then
                intNPDM += 1
                msgEntryParent = myMsgCollection.Items(Convert.ToInt32(drP("IndexID").ToString))
                With sbParent
                    .Append("INSERT INTO syEDExpressExceptionReportPell_ACG_SMART_Teach_StudentTable(ExceptionReportId,DbIn,Filter,AwardAmount,AwardId,GrantType,AddDate,AddTime,OriginalSSN,UpdateDate,UpdateTime,OriginationStatus,AcademicYearId,AwardYearStartDate,AwardYearEndDate,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                    'Add params
                    db.ClearParameters()
                    db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@DbIn", msgEntryParent.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@Filter", msgEntryParent.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    If msgEntryParent.strDatabaseIndicator.ToUpper = "T" Then
                        db.AddParameter("@AwardAmount", msgEntryParent.strAwardAmountForEntireYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@AwardAmount", msgEntryParent.strAwardAmountForEntireSchoolYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    db.AddParameter("@AwardId", msgEntryParent.strAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@GrantType", IIf(msgEntryParent.strGrandType = "TT", "", msgEntryParent.strGrandType), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    If msgEntryParent.strDatabaseIndicator.ToUpper = "T" Then
                        db.AddParameter("@AddDate", msgEntryParent.strPellAddDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@AddDate", msgEntryParent.strTeachAddDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    If msgEntryParent.strDatabaseIndicator.ToUpper = "T" Then
                        db.AddParameter("@AddTime", msgEntryParent.strPellAddTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@AddTime", msgEntryParent.strTeachAddTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    If msgEntryParent.strDatabaseIndicator.ToUpper = "T" Then
                        db.AddParameter("@UpdateDate", msgEntryParent.strPellUpdateDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@UpdateDate", msgEntryParent.strTeachUpdateDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    If msgEntryParent.strDatabaseIndicator.ToUpper = "T" Then
                        db.AddParameter("@UpdateTime", msgEntryParent.strPellUpdateTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@UpdateTime", msgEntryParent.strTeachUpdateTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    db.AddParameter("@OriginationStatus", msgEntryParent.strOriginationStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AcademicYearId", msgEntryParent.strAcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardYearStartDate", msgEntryParent.strAwardYearStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardYearEndDate", msgEntryParent.strAwardYearEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End With
                Try
                    db.RunParamSQLExecuteNoneQuery(sbParent.ToString)
                Catch ex As System.Exception
                End Try
                sbParent = sbParent.Remove(0, sbParent.Length)
                If Not drD1 Is Nothing Then
                    For Each drT As DataRow In drD1
                        intNPDD += 1
                        msgEntryChild = myMsgCollection.Items(Convert.ToInt32(drT("IndexID").ToString))
                        With sbChild
                            .Append("INSERT INTO syEDExpressExceptionReportPell_ACG_SMART_Teach_DisbursementTable(DetailId,ExceptionReportId,DbIn,Filter,AccDisbAmount,DisbDate,DisbNum,DisbRelIndi,DusbSeqNum,SimittedDisbAmount,ActionStatusDisb,OriginalSSN,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                            .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                            'Add params
                            db.ClearParameters()
                            db.AddParameter("@DetailId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DbIn", msgEntryChild.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@Filter", msgEntryChild.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@AccDisbAmount", msgEntryChild.strAcceptedDisbursementAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DisbDate", msgEntryChild.strDisbursementDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DisbNum", msgEntryChild.strDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DisbRelIndi", msgEntryChild.strDisbursementRealeaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DusbSeqNum", msgEntryChild.strDisbursementSequenceNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@SimittedDisbAmount", msgEntryChild.strSubmittedDisbursementAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@ActionStatusDisb", msgEntryChild.strActionStatusDisbursement, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                            db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        End With
                        Try
                            db.RunParamSQLExecuteNoneQuery(sbChild.ToString)
                        Catch ex As System.Exception
                        End Try
                        sbChild = sbChild.Remove(0, sbChild.Length)
                    Next
                End If

            Else
                msgEntryParent = myMsgCollection.Items(Convert.ToInt32(drP("IndexID").ToString))
                intNPDM += 1
                With sbParent
                    .Append("INSERT INTO syEDExpressExceptionReportDirectLoan_StudentTable(ExceptionReportId,DbIn,Filter,AddDate,AddTime,LoanAmtApproved,LoanFeePer,LoanId,LoanPeriodEndDate,LoanPeriodStartDate,LoanStatus,LoanType,MPNStatus,OriginalSSN,UpdateDate,UpdateTime,AcademicYearId,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                    'Add params
                    db.ClearParameters()
                    db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@DbIn", msgEntryParent.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@Filter", msgEntryParent.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AddDate", msgEntryParent.strLoanAddDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AddTime", msgEntryParent.strLoanAddTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@LoanAmtApproved", msgEntryParent.strLoanAmountApproved, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@LoanFeePer", msgEntryParent.strLoanFeePercentage, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@LoanId", msgEntryParent.strLoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@LoanPeriodEndDate", msgEntryParent.strLoanPeriodEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@LoanPeriodStartDate", msgEntryParent.strLoanPeriodStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@LoanStatus", msgEntryParent.strLoanStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@LoanType", msgEntryParent.strLoanType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@MPNStatus", msgEntryParent.strMPNStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@UpdateDate", msgEntryParent.strLoanUpdateDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@UpdateTime", msgEntryParent.strLoanUpdateTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AcademicYearId", msgEntryParent.strAcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End With
                Try
                    db.RunParamSQLExecuteNoneQuery(sbParent.ToString)
                Catch ex As System.Exception
                End Try
                sbParent = sbParent.Remove(0, sbParent.Length)
                If Not drD1 Is Nothing Then
                    For Each drT1 As DataRow In drD1
                        msgEntryChild = myMsgCollection.Items(Convert.ToInt32(drT1("IndexID").ToString))
                        If msgEntryChild.blnIsParsed Then
                            intNPDD += 1
                            With sbChild
                                .Append("INSERT INTO syEDExpressExceptionReportDirectLoan_DisbursementTable(DetailId,ExceptionReportId,DbIn,Filter,DisbDate,DisbGrossAmount,DisbIntRebAmount,DisbLoanFeeAmount,DisbNetAdjAmount,DisbNetAmount,DisbNum,DisbSeqNum,DisbStatus_RelInd,DisbType,LoanId,OriginalSSN,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                                'Add params
                                db.ClearParameters()
                                db.AddParameter("@DetailId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DbIn", msgEntryChild.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@Filter", msgEntryChild.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbDate", msgEntryChild.strAnticipatedDisbursementDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbGrossAmount", msgEntryChild.strAnticipatedDisbursementGrossAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbIntRebAmount", msgEntryChild.strAnticipatedDisbursementInterestRebateAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbLoanFeeAmount", msgEntryChild.strAnticipatedDisbursementFeeAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbNetAdjAmount", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbNetAmount", msgEntryChild.strAnticipatedDisbursementNetAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbNum", msgEntryChild.strAnticipatedDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbSeqNum", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbStatus_RelInd", msgEntryChild.strAnticipatedDisbursementRealeaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbType", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@LoanId", msgEntryChild.strLoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                                db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            End With
                            Try
                                db.RunParamSQLExecuteNoneQuery(sbChild.ToString)
                            Catch ex As System.Exception
                            End Try
                        End If
                        sbChild = sbChild.Remove(0, sbChild.Length)
                    Next
                End If
                If Not drD2 Is Nothing Then
                    For Each drT2 As DataRow In drD2
                        msgEntryChild = myMsgCollection.Items(Convert.ToInt32(drT2("IndexID").ToString))
                        If msgEntryChild.blnIsParsed Then
                            intNPAD += 1
                            With sbChild
                                .Append("INSERT INTO syEDExpressExceptionReportDirectLoan_DisbursementTable(DetailId,ExceptionReportId,DbIn,Filter,DisbDate,DisbGrossAmount,DisbIntRebAmount,DisbLoanFeeAmount,DisbNetAdjAmount,DisbNetAmount,DisbNum,DisbSeqNum,DisbStatus_RelInd,DisbType,LoanId,OriginalSSN,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                                'Add params
                                db.ClearParameters()
                                db.AddParameter("@DetailId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DbIn", msgEntryChild.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@Filter", msgEntryChild.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbDate", msgEntryChild.strActualDisbursementDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbGrossAmount", msgEntryChild.strActualDisbursementGrossAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbIntRebAmount", msgEntryChild.strActualDisbursementInterestRebateAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbLoanFeeAmount", msgEntryChild.strActualDisbursementLoanFeeAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbNetAdjAmount", msgEntryChild.strActualDisbursementNetAdjustmentAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbNetAmount", msgEntryChild.strActualDisbursementNetAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbNum", msgEntryChild.strActualDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbSeqNum", msgEntryChild.strActualDisbursementSequenceNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbStatus_RelInd", msgEntryChild.strActualDisbursementStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbType", msgEntryChild.strActualDisbursementType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@LoanId", msgEntryChild.strLoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                                db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            End With
                            Try
                                db.RunParamSQLExecuteNoneQuery(sbChild.ToString)
                            Catch ex As System.Exception
                            End Try
                        End If
                        sbChild = sbChild.Remove(0, sbChild.Length)
                    Next
                End If
            End If
        Finally
        End Try
    End Function
    Public Function BuildExceptionReportNew(ByRef db As DataAccess, ByVal ErrMsg As String, ByVal FileName As String, ByVal MsgType As String, ByRef intNPDM As Integer, ByRef intNPDD As Integer, ByVal drP As DataRow, Optional ByVal drD1() As DataRow = Nothing, Optional ByVal drD2() As DataRow = Nothing, Optional ByVal ExceptionGUID As String = "", Optional ByRef intNPAD As Integer = 0) As String
        ''drD1 is used for Pell and Anticipated Direct Loan
        ''drD2 is used for Actual Direct Loan

        Dim sbParent As New System.Text.StringBuilder
        Dim sbChild As New StringBuilder
        'Dim msgEntryParent As EDMessageInfo_New
        'Dim msgEntryChild As EDMessageInfo_New
        Dim strExceptionReportId As String
        strExceptionReportId = Guid.NewGuid.ToString
        Try
            If MsgType = "PELL" Then
                intNPDM += 1
                With sbParent
                    .Append("INSERT INTO syEDExpressExceptionReportPell_ACG_SMART_Teach_StudentTable(ExceptionReportId,DbIn,Filter,AwardAmount,AwardId,GrantType,AddDate,AddTime,OriginalSSN,UpdateDate,UpdateTime,OriginationStatus,AcademicYearId,AwardYearStartDate,AwardYearEndDate,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                    'Add params
                    db.ClearParameters()
                    db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@DbIn", drP("dbIndicator").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@Filter", drP("dbFilter").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardAmount", drP("AwardAmount").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardId", drP("AwardId").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@GrantType", IIf(drP("GrantType").ToString = "TT", "", drP("GrantType").ToString), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AddDate", drP("AddDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AddTime", drP("AddTime").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@OriginalSSN", drP("SSNOriginal").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@UpdateDate", drP("UpdatedDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@UpdateTime", drP("UpdatedTime").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@OriginationStatus", drP("OriginationStatus").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AcademicYearId", drP("AcademicYearId").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardYearStartDate", drP("AwardStartDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardYearEndDate", drP("AwardEndDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, 500, ParameterDirection.Input)
                    db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, 250, ParameterDirection.Input)
                    db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End With
                Try
                    db.RunParamSQLExecuteNoneQuery(sbParent.ToString)
                Catch ex As System.Exception
                End Try
                sbParent = sbParent.Remove(0, sbParent.Length)
                If Not drD1 Is Nothing Then
                    For Each drT As DataRow In drD1
                        intNPDD += 1
                        With sbChild
                            .Append("INSERT INTO syEDExpressExceptionReportPell_ACG_SMART_Teach_DisbursementTable(DetailId,ExceptionReportId,DbIn,Filter,AccDisbAmount,DisbDate,DisbNum,DisbRelIndi,DusbSeqNum,SimittedDisbAmount,ActionStatusDisb,OriginalSSN,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                            .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                            'Add params
                            db.ClearParameters()
                            db.AddParameter("@DetailId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DbIn", drT("dbIndicator").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@Filter", drT("dbFilter").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@AccDisbAmount", drT("AccDisbAmt").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DisbDate", drT("DisbDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DisbNum", drT("DisbNum").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DisbRelIndi", drT("DisbRelInd").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DusbSeqNum", drT("SeqNum").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@SimittedDisbAmount", drT("SubDisbAmt").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@ActionStatusDisb", drT("ActionStatus").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@OriginalSSN", drT("SSNOriginal").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                            db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, 500, ParameterDirection.Input)
                            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, 250, ParameterDirection.Input)
                            db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        End With
                        Try
                            db.RunParamSQLExecuteNoneQuery(sbChild.ToString)
                        Catch ex As System.Exception
                        End Try
                        sbChild = sbChild.Remove(0, sbChild.Length)
                    Next
                End If

            Else
                ''msgEntryParent = myMsgCollection.Items(Convert.ToInt32(drP("IndexID").ToString))
                ''intNPDM += 1
                ''With sbParent
                ''    .Append("INSERT INTO syEDExpressExceptionReportDirectLoan_StudentTable(ExceptionReportId,DbIn,Filter,AddDate,AddTime,LoanAmtApproved,LoanFeePer,LoanId,LoanPeriodEndDate,LoanPeriodStartDate,LoanStatus,LoanType,MPNStatus,OriginalSSN,UpdateDate,UpdateTime,AcademicYearId,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                ''    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                ''    'Add params
                ''    db.ClearParameters()
                ''    db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@DbIn", msgEntryParent.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@Filter", msgEntryParent.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@AddDate", msgEntryParent.strLoanAddDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@AddTime", msgEntryParent.strLoanAddTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanAmtApproved", msgEntryParent.strLoanAmountApproved, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanFeePer", msgEntryParent.strLoanFeePercentage, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanId", msgEntryParent.strLoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanPeriodEndDate", msgEntryParent.strLoanPeriodEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanPeriodStartDate", msgEntryParent.strLoanPeriodStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanStatus", msgEntryParent.strLoanStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@LoanType", msgEntryParent.strLoanType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@MPNStatus", msgEntryParent.strMPNStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@UpdateDate", msgEntryParent.strLoanUpdateDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@UpdateTime", msgEntryParent.strLoanUpdateTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@AcademicYearId", msgEntryParent.strAcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                ''    db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''    db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''End With
                ''Try
                ''    db.RunParamSQLExecuteNoneQuery(sbParent.ToString)
                ''Catch ex As System.Exception
                ''End Try
                ''sbParent = sbParent.Remove(0, sbParent.Length)
                ''If Not drD1 Is Nothing Then
                ''    For Each drT1 As DataRow In drD1
                ''        msgEntryChild = myMsgCollection.Items(Convert.ToInt32(drT1("IndexID").ToString))
                ''        If msgEntryChild.blnIsParsed Then
                ''            intNPDD += 1
                ''            With sbChild
                ''                .Append("INSERT INTO syEDExpressExceptionReportDirectLoan_DisbursementTable(DetailId,ExceptionReportId,DbIn,Filter,DisbDate,DisbGrossAmount,DisbIntRebAmount,DisbLoanFeeAmount,DisbNetAdjAmount,DisbNetAmount,DisbNum,DisbSeqNum,DisbStatus_RelInd,DisbType,LoanId,OriginalSSN,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                ''                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                ''                'Add params
                ''                db.ClearParameters()
                ''                db.AddParameter("@DetailId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DbIn", msgEntryChild.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@Filter", msgEntryChild.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbDate", msgEntryChild.strAnticipatedDisbursementDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbGrossAmount", msgEntryChild.strAnticipatedDisbursementGrossAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbIntRebAmount", msgEntryChild.strAnticipatedDisbursementInterestRebateAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbLoanFeeAmount", msgEntryChild.strAnticipatedDisbursementFeeAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbNetAdjAmount", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbNetAmount", msgEntryChild.strAnticipatedDisbursementNetAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbNum", msgEntryChild.strAnticipatedDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbSeqNum", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbStatus_RelInd", msgEntryChild.strAnticipatedDisbursementRealeaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbType", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@LoanId", msgEntryChild.strLoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                ''                db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''            End With
                ''            Try
                ''                db.RunParamSQLExecuteNoneQuery(sbChild.ToString)
                ''            Catch ex As System.Exception
                ''            End Try
                ''        End If
                ''        sbChild = sbChild.Remove(0, sbChild.Length)
                ''    Next
                ''End If
                ''If Not drD2 Is Nothing Then
                ''    For Each drT2 As DataRow In drD2
                ''        msgEntryChild = myMsgCollection.Items(Convert.ToInt32(drT2("IndexID").ToString))
                ''        If msgEntryChild.blnIsParsed Then
                ''            intNPAD += 1
                ''            With sbChild
                ''                .Append("INSERT INTO syEDExpressExceptionReportDirectLoan_DisbursementTable(DetailId,ExceptionReportId,DbIn,Filter,DisbDate,DisbGrossAmount,DisbIntRebAmount,DisbLoanFeeAmount,DisbNetAdjAmount,DisbNetAmount,DisbNum,DisbSeqNum,DisbStatus_RelInd,DisbType,LoanId,OriginalSSN,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                ''                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                ''                'Add params
                ''                db.ClearParameters()
                ''                db.AddParameter("@DetailId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DbIn", msgEntryChild.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@Filter", msgEntryChild.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbDate", msgEntryChild.strActualDisbursementDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbGrossAmount", msgEntryChild.strActualDisbursementGrossAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbIntRebAmount", msgEntryChild.strActualDisbursementInterestRebateAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbLoanFeeAmount", msgEntryChild.strActualDisbursementLoanFeeAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbNetAdjAmount", msgEntryChild.strActualDisbursementNetAdjustmentAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbNetAmount", msgEntryChild.strActualDisbursementNetAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbNum", msgEntryChild.strActualDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbSeqNum", msgEntryChild.strActualDisbursementSequenceNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbStatus_RelInd", msgEntryChild.strActualDisbursementStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@DisbType", msgEntryChild.strActualDisbursementType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@LoanId", msgEntryChild.strLoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                ''                db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''                db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''            End With
                ''            Try
                ''                db.RunParamSQLExecuteNoneQuery(sbChild.ToString)
                ''            Catch ex As System.Exception
                ''            End Try
                ''        End If
                ''        sbChild = sbChild.Remove(0, sbChild.Length)
                ''    Next
                ''End If
            End If
        Finally
        End Try
    End Function
    Public Function ValidateDate(ByRef DateToFormat As String) As String
        Dim dt As DateTime
        Dim strDD As String
        Dim strMM As String
        Dim strYYYY As String

        Try
            strYYYY = DateToFormat.Substring(0, 4)
            strMM = DateToFormat.Substring(4, 2)
            strDD = DateToFormat.Substring(6, 2)
            dt = Convert.ToDateTime(strMM + "/" + strDD + "/" + strYYYY)
            Return dt.ToString("MM/dd/yyyy")
        Catch ex As Exception
            Return ""
        End Try
    End Function
    Public Sub DeleteFromExceptionReportNew(ByRef db As DataAccess, ByVal FileName As String, ByVal MsgType As String, ByVal ExceptionID As String)
        Try

            db.ClearParameters()

            db.AddParameter("@MsgType", MsgType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ExceptionGUID", ExceptionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Try
                db.RunParamSQLDataSetUsingSP("usp_EdExpress_DeleteFromExceptionReport")
                Exit Sub
            Catch ex As System.Exception
            End Try
        Finally
        End Try
    End Sub
    Public Sub DeleteFromNotPostRecordsNew(ByRef db As DataAccess, ByVal FileName As String, ByVal MsgType As String)
        Try

            db.ClearParameters()

            db.AddParameter("@MsgType", MsgType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                db.RunParamSQLDataSetUsingSP("usp_EdExpress_DeleteFromNotPostRecords")
                Exit Sub
            Catch ex As System.Exception
            End Try
        Finally
        End Try
    End Sub
    Public Function BuildExceptionReportNew2(ByRef db As DataAccess, ByVal ErrMsg As String, ByVal FileName As String, ByVal MsgType As String, ByRef intNPDM As Integer, ByRef intNPDD As Integer, ByRef dbAANP As Decimal, ByRef dbDANP As Decimal, ByVal drP As DataRow, Optional ByVal drD1() As DataRow = Nothing, Optional ByVal drD2() As DataRow = Nothing, Optional ByVal ExceptionGUID As String = "", Optional ByRef intNPAD As Integer = 0, Optional ByRef dbDAANP As Decimal = 0.0) As String

        ''drD1 is used for Pell and Anticipated Direct Loan
        ''drD2 is used for Actual Direct Loan

        Dim strExceptionReportId As String
        strExceptionReportId = Guid.NewGuid.ToString
        Try
            If MsgType = "PELL" Then
                intNPDM += 1

                Try
                    dbAANP += Convert.ToDecimal(drP("AwardAmount").ToString)
                Catch ex As Exception
                    dbAANP += 0.0
                End Try

                db.ClearParameters()
                db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@DbIn", drP("dbIndicator").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@Filter", drP("dbFilter").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardAmount", drP("AwardAmount").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardId", drP("AwardId").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@GrantType", IIf(drP("GrantType").ToString = "TT", "", drP("GrantType").ToString), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AddDate", drP("AddDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AddTime", drP("AddTime").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@OriginalSSN", drP("SSNOriginal").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@UpdateDate", drP("UpdatedDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@UpdateTime", drP("UpdatedTime").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@OriginationStatus", drP("OriginationStatus").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AcademicYearId", drP("AcademicYearId").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardYearStartDate", drP("AwardStartDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardYearEndDate", drP("AwardEndDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@MsgType", MsgType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Try
                    db.RunParamSQLDataSetUsingSP("usp_EdExpress_BuildExceptionReport_StudentTable")
                Catch ex As System.Exception
                End Try

                If Not drD1 Is Nothing Then
                    For Each drT As DataRow In drD1
                        intNPDD += 1

                        Try
                            dbDANP += Convert.ToDecimal(drP("AccDisbAmt").ToString)
                        Catch ex As Exception
                            dbDANP += 0.0
                        End Try

                        db.ClearParameters()
                        db.AddParameter("@DetailId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@DbIn", drT("dbIndicator").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@Filter", drT("dbFilter").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@AccDisbAmount", drT("AccDisbAmt").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@DisbDate", drT("DisbDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@DisbNum", drT("DisbNum").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@DisbRelIndi", drT("DisbRelInd").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@DusbSeqNum", drT("SeqNum").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@SimittedDisbAmount", drT("SubDisbAmt").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@ActionStatusDisb", drT("ActionStatus").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@OriginalSSN", drT("SSNOriginal").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                        db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@MsgType", MsgType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        Try
                            db.RunParamSQLDataSetUsingSP("usp_EdExpress_BuildExceptionReport_DisbursementTable")
                        Catch ex As System.Exception
                        End Try
                    Next
                End If

            Else
                intNPDM += 1
                Try
                    dbAANP += Convert.ToDecimal(drP("GrossAmount").ToString)
                Catch ex As Exception
                    dbAANP += 0.0
                End Try

                db.ClearParameters()
                db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@DbIn", drP("dbIndicator").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@Filter", drP("dbFilter").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardAmount", drP("GrossAmount").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardId", drP("LoanId").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@GrantType", drP("LoanType").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AddDate", drP("AddDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AddTime", drP("AddTime").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@OriginalSSN", drP("SSNOriginal").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@UpdateDate", drP("UpdatedDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@UpdateTime", drP("UpdatedTime").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@OriginationStatus", "", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AcademicYearId", drP("AcademicYearId").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardYearStartDate", drP("LoanStartDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardYearEndDate", drP("LoanEndDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@MsgType", MsgType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@LoanFee", drP("Fees").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Try

                    db.RunParamSQLDataSetUsingSP("usp_EdExpress_BuildExceptionReport_StudentTable")
                Catch ex As System.Exception
                End Try
                If Not drD1 Is Nothing Then
                    For Each drT1 As DataRow In drD1
                        intNPDD += 1
                        Try
                            dbDANP += Convert.ToDecimal(drT1("GrossAmt").ToString)
                        Catch ex As Exception
                            dbDANP += 0.0
                        End Try

                        db.ClearParameters()
                        db.AddParameter("@DetailId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@DbIn", drT1("dbIndicator").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@Filter", drT1("dbFilter").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@AccDisbAmount", drT1("NetAmt").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@DisbDate", drT1("DisbDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@DisbNum", drT1("DisbNum").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@DisbRelIndi", drT1("DisbRelInd").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@DusbSeqNum", drT1("SeqNum").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@SimittedDisbAmount", drT1("GrossAmt").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@ActionStatusDisb", "", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@OriginalSSN", drT1("SSNOriginal").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                        db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@MsgType", MsgType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@LoanId", drT1("LoanId").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@DisbLoanFeeAmount", drT1("Fees").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        Try
                            db.RunParamSQLDataSetUsingSP("usp_EdExpress_BuildExceptionReport_DisbursementTable")
                        Catch ex As System.Exception
                        End Try
                    Next
                End If
                If Not drD2 Is Nothing Then
                    For Each drT2 As DataRow In drD2
                        intNPAD += 1
                        Try
                            dbDAANP += Convert.ToDecimal(drT2("GrossAmt").ToString)
                        Catch ex As Exception
                            dbDAANP += 0.0
                        End Try
                        db.ClearParameters()
                        db.AddParameter("@DetailId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@DbIn", drT2("dbIndicator").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@Filter", drT2("dbFilter").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@AccDisbAmount", drT2("NetAmt").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@DisbDate", drT2("DisbDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@DisbNum", drT2("DisbNum").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@DisbRelIndi", drT2("DisbRelInd").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@DusbSeqNum", drT2("SeqNum").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@SimittedDisbAmount", drT2("GrossAmt").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@ActionStatusDisb", "", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@OriginalSSN", drT2("SSNOriginal").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                        db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@MsgType", MsgType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@LoanId", drT2("LoanId").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@DisbLoanFeeAmount", drT2("Fees").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        Try
                            db.RunParamSQLDataSetUsingSP("usp_EdExpress_BuildExceptionReport_DisbursementTable")
                        Catch ex As System.Exception
                        End Try
                    Next
                End If
            End If
        Finally
        End Try
    End Function
    Public Sub DeleteFromExceptionReport(ByRef db As DataAccess, ByVal FileName As String, ByVal MsgType As String)
        Dim sbDelete As New System.Text.StringBuilder(1000)
        Try
            With sbDelete
                If MsgType.ToUpper = "PELL" Then
                    .Append("Delete from  syEDExpressExceptionReportPell_ACG_SMART_Teach_StudentTable where FileName=? ")
                    .Append("Delete from  syEDExpressExceptionReportPell_ACG_SMART_Teach_DisbursementTable where FileName=? ")
                Else
                    .Append("Delete from  syEDExpressExceptionReportDirectLoan_StudentTable where FileName=? ")
                    .Append("Delete from  syEDExpressExceptionReportDirectLoan_DisbursementTable where FileName=? ")
                End If
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sbDelete.ToString)
                Exit Sub
            Catch ex As System.Exception
            End Try
        Finally
        End Try
    End Sub
    Public Function GetMatchingStudentAwards(ByRef db As DataAccess, ByVal strStuEnrollID As String, ByVal strAwardId As String, ByVal strFundDescrip As String, ByVal GrossAmount As String, Optional ByVal AwardStartDate As String = "", Optional ByVal AwardEndDate As String = "", Optional ByVal StudentName As String = "", Optional ByVal FileType As String = "") As String
        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbAwardID As New System.Text.StringBuilder(1000)
        Dim sbAcademicYear As New StringBuilder
        Dim StudentAwardID As String
        Dim strFN As String

        If String.IsNullOrEmpty(strAwardId) Then
            Return "Unable to get student award for student " & StudentName & " as the fund provided is empty"
            Exit Function
        End If
        If String.IsNullOrEmpty(strStuEnrollID) Then
            Return "Unable to find student enrollment information for the student" & StudentName
            Exit Function
        End If
        If FileType.ToUpper = "PELL" Then
            If strFundDescrip = "" Or strFundDescrip.ToUpper = "P" Then
                strFN = "PELL"
            ElseIf strFundDescrip = "TT" Then
                strFN = "TEACH"
            ElseIf strFundDescrip = "A" Then
                strFN = "ACG"
            ElseIf strFundDescrip = "S" Or strFundDescrip = "T" Then
                strFN = "SMART"
            End If
        Else
            If strFundDescrip = "P" Then
                strFN = "DL - PLUS"
            ElseIf strFundDescrip = "S" Then
                strFN = "DL - SUB"
            ElseIf strFundDescrip = "U" Then
                strFN = "DL - UNSUB"
            Else
                strFN = ""
            End If
        End If


        sbAwardID = New StringBuilder
        With sbAwardID
            .Append(" select Distinct Top 1 StudentAwardId from faStudentAwards where StuEnrollId=? and ")
            .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources, syAdvFundSources where syAdvFundSources.AdvFundSourceId=saFundSources.AdvFundSourceId AND syAdvFundSources.Descrip=?)  ")
            .Append(" and FA_ID =? ")
            '.Append(" and AwardStartDate=? ")
            '.Append(" and AwardEndDate=? ")
        End With
        Try
            db.ClearParameters()
            db.AddParameter("@STUENROLLID", strStuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FundDescrip", strFN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FA_ID", strAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AwardStartDate", AwardStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@AwardEndDate", AwardEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            dr = db.RunParamSQLDataReader(sbAwardID.ToString())
            If Not (dr.HasRows) Then
                Return ""
                Exit Function
            End If
            While dr.Read()
                Try
                    StudentAwardID = dr("StudentAwardID").ToString
                    Return StudentAwardID
                Catch ex As System.Exception
                    Return ""
                    Exit Function
                End Try
            End While
        Catch ex As System.Exception
            Return ""
            Exit Function
        Finally
            dr.Close()
        End Try
    End Function
    Public Function Add_faStudentAwardsFromPell(ByRef groupTrans As OleDbTransaction, ByRef db As DataAccess, ByRef StudentAwardID As String, ByVal StuEnrollID As String, ByVal msgEntry As EDMessageInfo_New, ByVal DisbNumber As String, Optional ByVal strStudentName As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal IsOverrideAdvDateWithEDExpDate As Boolean = False, Optional ByVal IsOverrideAdvAmtWithEDExpAmt As Boolean = False) As String
        Dim sbStudentAwards As New System.Text.StringBuilder(1000)
        Dim sbUpdateStudentAwards As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwards As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchool As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchoolDiffLoanPeriods As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchoolWithDiffGrossAmount As New System.Text.StringBuilder(1000)


        Dim AwardTypeID As String = " "
        Dim getAwardTypeMessage As String = ""
        Dim Disbursements As Integer = 0
        Dim LoanID As String = ""
        Dim intDuplicates As Integer = 0
        Dim intDuplicatesBySchool As Integer = 0
        Dim intDuplicatesBySchoolDiffLoanPeriods As Integer = 0
        Dim intDuplicatesBySchoolDiffGrossAmount As Integer = 0
        Dim intDuplicatesBySchoolSameAwardYearDiffLoanPeriods As Integer = 0

        Dim sbAcademicYear As New StringBuilder
        'Dim strAwardYear As String
        'Dim dr As OleDbDataReader
        m_ExceptionMessage = ""

        'db.OpenConnection()

        If String.IsNullOrEmpty(StuEnrollID) Or StuEnrollID = Guid.Empty.ToString Then
            Return "Unable to find enrollment information for the student" & strStudentName
            Exit Function
        End If

        'Look for matching records
        Try
            ''StudentAwardID = GetMatchingStudentAwards(db, StuEnrollID, msgEntry.strAwardId, msgEntry.strGrandType, msgEntry.strAwardAmountForEntireYear, "", "", strStudentName, msgEntry.strMsgType)
            If msgEntry.strDatabaseIndicator = "T" Then
                StudentAwardID = GetMatchingStudentAwards(db, StuEnrollID, msgEntry.strAwardId, msgEntry.strGrandType, msgEntry.strAwardAmountForEntireYear, msgEntry.strAwardYearStartDate, msgEntry.strAwardYearEndDate, strStudentName, msgEntry.strMsgType)
            Else
                StudentAwardID = GetMatchingStudentAwards(db, StuEnrollID, msgEntry.strAwardId, msgEntry.strGrandType, msgEntry.strAwardAmountForEntireSchoolYear, msgEntry.strAwardYearStartDate, msgEntry.strAwardYearEndDate, strStudentName, msgEntry.strMsgType)
            End If

            If Not StudentAwardID = "" Then
                'Update existing record with this FAID
                With sbUpdateStudentAwards
                    .Append(" Update faStudentAwards set fa_id=?,moduser=?,moddate=?,Disbursements=?,LoanId=? ")
                    If IsOverrideAdvDateWithEDExpDate = True Then
                        .Append(" ,AcademicYearId=?,AwardStartDate=?,AwardEndDate=? ")
                    End If
                    If IsOverrideAdvAmtWithEDExpAmt = True Then
                        .Append(" ,GrossAmount=? ")
                    End If
                    .Append(" where StudentAwardId=? ")
                End With
                db.ClearParameters()
                db.AddParameter("@fa_id", msgEntry.strAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@Disbursements", DisbNumber, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@LoanId", "", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If IsOverrideAdvDateWithEDExpDate = True Then
                    db.AddParameter("@AcademicYearId", msgEntry.strAcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardStartDate", msgEntry.strAwardYearStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardEndDate", msgEntry.strAwardYearEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                If IsOverrideAdvAmtWithEDExpAmt = True Then
                    If msgEntry.strDatabaseIndicator = "T" Then
                        db.AddParameter("@GrossAmount", msgEntry.strAwardAmountForEntireYear.Trim(), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@GrossAmount", msgEntry.strAwardAmountForEntireSchoolYear.Trim(), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    End If
                End If
                db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Try
                    db.RunParamSQLExecuteNoneQuery(sbUpdateStudentAwards.ToString)
                    Return StudentAwardID
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to match/update FAID with student awards for student " & strStudentName
                    Exit Function
                End Try
            Else
                StudentAwardID = Guid.NewGuid.ToString
            End If
        Catch ex As System.Exception
            StudentAwardID = Guid.NewGuid.ToString
        End Try

        'Before Creating new awards check to see if the Award Start Date falls after the AwardsCuttOffDate
        'in web.config
        Try
            getAwardTypeMessage = GetAwardTypeID(db, AwardTypeID, msgEntry.strGrandType, msgEntry.strMsgType)
            If Not getAwardTypeMessage = "" Then
                Return "Unable to add student awards for student " & strStudentName & " as the provided award type was not found"
                Exit Function
            End If

            With sbStudentAwards
                .Append("INSERT INTO faStudentAwards(StudentAwardID,StuEnrollID,AwardTypeID,AcademicYearId,fa_id,GrossAmount,AwardStartDate,AwardEndDate,LoanFees,moduser,moddate,Disbursements,LoanId) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AwardTypeID", AwardTypeID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AcademicYearId", msgEntry.strAcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@fa_id", msgEntry.strAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If msgEntry.strDatabaseIndicator = "T" Then
                db.AddParameter("@GrossAmount", msgEntry.strAwardAmountForEntireYear.Trim(), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            Else
                db.AddParameter("@GrossAmount", msgEntry.strAwardAmountForEntireSchoolYear.Trim(), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            End If
            db.AddParameter("@AwardStartDate", msgEntry.strAwardYearStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AwardEndDate", msgEntry.strAwardYearEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@LoanFees", 0.0, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Disbursements", DisbNumber, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@LoanId", "", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   execute the query
            Try
                db.RunParamSQLExecuteNoneQuery(sbStudentAwards.ToString)
                Return StudentAwardID
                Exit Function
            Catch ex As System.Exception
                Return "Unable to create student awards for student " & strStudentName
                'db.CloseConnection()
                Exit Function
            End Try
            '' ''End If
        Catch ex As System.Exception
            Return "Unable to create student award for student " & strStudentName
            Exit Function
        Finally
        End Try
    End Function
    Public Function Add_faStudentAwardsFromPellNew(ByRef groupTrans As OleDbTransaction, ByRef db As DataAccess, ByRef StudentAwardID As String, ByVal StuEnrollID As String, ByVal drParent As DataRow, ByVal DisbNumber As String, Optional ByVal strStudentName As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal IsOverrideAdvDateWithEDExpDate As Boolean = False, Optional ByVal IsOverrideAdvAmtWithEDExpAmt As Boolean = False) As String
        Dim sbStudentAwards As New System.Text.StringBuilder(1000)
        Dim sbUpdateStudentAwards As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwards As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchool As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchoolDiffLoanPeriods As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchoolWithDiffGrossAmount As New System.Text.StringBuilder(1000)


        Dim AwardTypeID As String = " "
        Dim getAwardTypeMessage As String = ""
        Dim Disbursements As Integer = 0
        Dim LoanID As String = ""
        Dim intDuplicates As Integer = 0
        Dim intDuplicatesBySchool As Integer = 0
        Dim intDuplicatesBySchoolDiffLoanPeriods As Integer = 0
        Dim intDuplicatesBySchoolDiffGrossAmount As Integer = 0
        Dim intDuplicatesBySchoolSameAwardYearDiffLoanPeriods As Integer = 0

        Dim sbAcademicYear As New StringBuilder
        'Dim strAwardYear As String
        ''  Dim dr As OleDbDataReader
        m_ExceptionMessage = ""

        'db.OpenConnection()

        If String.IsNullOrEmpty(StuEnrollID) Or StuEnrollID = Guid.Empty.ToString Then
            Return "Unable to find enrollment information for the student" & strStudentName
            Exit Function
        End If

        'Look for matching records
        Try
            ''StudentAwardID = GetMatchingStudentAwards(db, StuEnrollID, msgEntry.strAwardId, msgEntry.strGrandType, msgEntry.strAwardAmountForEntireYear, "", "", strStudentName, msgEntry.strMsgType)
            If drParent("dbIndicator").ToString = "T" Then
                StudentAwardID = GetMatchingStudentAwards(db, StuEnrollID, drParent("AwardId").ToString, drParent("GrantType").ToString, drParent("AwardAmount").ToString, drParent("AwardStartDate").ToString, drParent("AwardEndDate").ToString, strStudentName, "PELL")
            Else
                StudentAwardID = GetMatchingStudentAwards(db, StuEnrollID, drParent("AwardId").ToString, drParent("GrantType").ToString, drParent("AwardAmount").ToString, drParent("AwardStartDate").ToString, drParent("AwardEndDate").ToString, strStudentName, "PELL")
            End If

            If Not StudentAwardID = "" Then
                'Update existing record with this FAID
                With sbUpdateStudentAwards
                    .Append(" Update faStudentAwards set fa_id=?,moduser=?,moddate=?,Disbursements=?,LoanId=? ")
                    If IsOverrideAdvDateWithEDExpDate = True Then
                        .Append(" ,AcademicYearId=?,AwardStartDate=?,AwardEndDate=? ")
                    End If
                    If IsOverrideAdvAmtWithEDExpAmt = True Then
                        .Append(" ,GrossAmount=? ")
                    End If
                    .Append(" where StudentAwardId=? ")
                End With
                db.ClearParameters()
                db.AddParameter("@fa_id", drParent("AwardId").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moduser", "sa", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", DateTime.Now.ToString("MM/dd/yyyy"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@Disbursements", DisbNumber, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@LoanId", "", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If IsOverrideAdvDateWithEDExpDate = True Then
                    db.AddParameter("@AcademicYearId", drParent("AcademicYearId").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardStartDate", drParent("AwardStartDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardEndDate", drParent("AwardEndDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                If IsOverrideAdvAmtWithEDExpAmt = True Then
                    If drParent("dbIndicator").ToString = "T" Then
                        db.AddParameter("@GrossAmount", drParent("AwardAmount").ToString, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@GrossAmount", drParent("AwardAmount").ToString, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    End If
                End If
                db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Try
                    db.RunParamSQLExecuteNoneQuery(sbUpdateStudentAwards.ToString)
                    Return StudentAwardID
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to match/update FAID with student awards for student " & strStudentName
                    Exit Function
                End Try
            Else
                StudentAwardID = Guid.NewGuid.ToString
            End If
        Catch ex As System.Exception
            StudentAwardID = Guid.NewGuid.ToString
        End Try

        'Before Creating new awards check to see if the Award Start Date falls after the AwardsCuttOffDate
        'in web.config
        Try
            getAwardTypeMessage = GetAwardTypeID(db, AwardTypeID, drParent("GrantType").ToString, "PELL")
            If Not getAwardTypeMessage = "" Then
                Return "Unable to add student awards for student " & strStudentName & " as the provided award type was not found"
                Exit Function
            End If

            With sbStudentAwards
                .Append("INSERT INTO faStudentAwards(StudentAwardID,StuEnrollID,AwardTypeID,AcademicYearId,fa_id,GrossAmount,AwardStartDate,AwardEndDate,LoanFees,moduser,moddate,Disbursements,LoanId) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AwardTypeID", AwardTypeID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AcademicYearId", drParent("AcademicYearId").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@fa_id", drParent("AwardId").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If drParent("dbIndicator").ToString = "T" Then
                db.AddParameter("@GrossAmount", drParent("AwardAmount").ToString, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            Else
                db.AddParameter("@GrossAmount", drParent("AwardAmount").ToString, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            End If
            db.AddParameter("@AwardStartDate", drParent("AwardStartDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AwardEndDate", drParent("AwardEndDate").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@LoanFees", 0.0, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@moduser", "sa", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", DateTime.Now.ToString("MM/dd/yyyy"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Disbursements", DisbNumber, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@LoanId", "", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   execute the query
            Try
                db.RunParamSQLExecuteNoneQuery(sbStudentAwards.ToString)
                Return StudentAwardID
                Exit Function
            Catch ex As System.Exception
                Return "Unable to create student awards for student " & strStudentName
                'db.CloseConnection()
                Exit Function
            End Try
            '' ''End If
        Catch ex As System.Exception
            Return "Unable to create student award for student " & strStudentName
            Exit Function
        Finally
        End Try
    End Function
    Public Function Add_faStudentAwardsFromPellNew(ByRef groupTrans As OleDbTransaction, ByRef db As DataAccess, ByVal drParent As DataRow, ByVal DisbNumber As String, ByVal ParentId As String, ByVal FileName As String, ByVal IsOverrideAdvDateWithEDExpDate As Boolean, ByVal IsOverrideAdvAmtWithEDExpAmt As Boolean, ByVal IsPostPayment As Boolean, ByVal IsPayOutOfSchool As Boolean) As String
        Dim StudentAwardID As String = ""
        Dim ds As New DataSet
        Try
            db.ClearParameters()
            db.AddParameter("@StuEnrollID", drParent("StuEnrollmentId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@AcademicYearId", drParent("AcademicYearId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@fa_id", drParent("AwardId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@GrossAmount", drParent("AwardAmount").ToString, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@AwardStartDate", drParent("AwardStartDate").ToString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@AwardEndDate", drParent("AwardEndDate").ToString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@LoanFees", 0, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@moduser", "sa", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@moddate", DateTime.Now.ToString("MM/dd/yyyy"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Disbursements", DisbNumber, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@LoanId", "", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@GrantType", drParent("GrantType").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'db.AddParameter("@IsOverrideAdvDateWithEDExpDate", drParent("IsOverrideAdvDateWithEDExpDate").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'db.AddParameter("@IsOverrideAdvAmtWithEDExpAmt", drParent("IsOverrideAdvAmtWithEDExpAm").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'db.AddParameter("@IsPostPayment", drParent("IsPostPayment").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'db.AddParameter("@IsPayOutOfSchool", drParent("IsPayOutOfSchoolStudent").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@IsOverrideAdvDateWithEDExpDate", IsOverrideAdvDateWithEDExpDate, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@IsOverrideAdvAmtWithEDExpAmt", IsOverrideAdvAmtWithEDExpAmt, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@IsPostPayment", IsPostPayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@IsPayOutOfSchool", IsPayOutOfSchool, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            db.AddParameter("@ParentId", ParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@DbIn", drParent("dbIndicator").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@Filter", drParent("dbFilter").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@FirstName", drParent("FirstName").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@LastName", drParent("LastName").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@SSN", drParent("SSN").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@CampusName", drParent("Campus").ToString, DataAccess.OleDbDataType.OleDbString, 500, ParameterDirection.Input)
            db.AddParameter("@CampusId", drParent("CampusId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@AddDate", drParent("AddDate").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@AddTime", drParent("AddTime").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@OriginalSSN", drParent("SSNOriginal").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@UpdateDate", drParent("UpdatedDate").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@UpdateTime", drParent("UpdatedTime").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@OriginationStatus", drParent("OriginationStatus").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, 250, ParameterDirection.Input)
            db.AddParameter("@IsInSchool", drParent("IsInSchool").ToString, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)



            ds = db.RunParamSQLDataSetUsingSP("usp_EdExpress_StudentAward")
            If ds.Tables(0).Rows.Count > 0 Then
                StudentAwardID = ds.Tables(0).Rows(0)(0).ToString
            End If
            If StudentAwardID.Contains("Unable award") Then
                StudentAwardID = "Unable to match fund source data found for the given fund type"
                Return StudentAwardID
            Else
                Return StudentAwardID
            End If

        Catch ex As System.Exception
            Return "Unable to post the record"
        End Try

    End Function

    Public Function GetDisbursementNumber(ByVal drTempRows() As DataRow) As String
        Dim DisbursementNumber As String = ""
        For Each drC As DataRow In drTempRows

            If DisbursementNumber = "" Then
                DisbursementNumber = drC("DisbNum").ToString
            Else
                DisbursementNumber = DisbursementNumber & "," & drC("DisbNum").ToString
            End If
        Next
        Return DisbursementNumber
    End Function

    Public Sub GetSequenceNumber(ByRef drTempRows() As DataRow)
        Dim SeqNum As Integer
        Dim DisbNum As Integer
        Dim SeqNumOld As Integer = 0
        Dim DisbNumOld As Integer = 0
        For i As Integer = 0 To drTempRows.Length - 1
            If SeqNumOld = 0 And DisbNumOld = 0 Then
                SeqNumOld = drTempRows(i)("SeqNum")
                DisbNumOld = drTempRows(i)("DisbNum")
            Else
                If DisbNum = DisbNumOld And SeqNum <> SeqNumOld Then

                End If
            End If
            DisbNum = drTempRows(i)("DisbNum")
            SeqNum = drTempRows(i)("SeqNum")
        Next
    End Sub

    Public Function Check_StudentSchedulePell(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal StudentAwardID As String, ByVal myMsgCollection As EDCollectionMessageInfos_New, ByVal drTempRows() As DataRow) As String
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim dsTemp As New DataSet
        Dim msgEntry As EDMessageInfo_New
        Dim strAwardScheduleId As String = ""
        Dim strDisbNum As String = ""
        ''Dim drTemp() As DataRow
        Try
            For Each drC As DataRow In drTempRows
                msgEntry = myMsgCollection.Items(Convert.ToInt32(drC("IndexId").ToString))
                If strDisbNum = "" Then
                    strDisbNum = msgEntry.strDisbursementNumber
                Else
                    strDisbNum = strDisbNum & "," & msgEntry.strDisbursementNumber
                End If

            Next
            With sbAwardSchedule
                .Append(" Delete From faStudentAwardSchedule where ")
                .Append(" StudentAwardID=?  and DisbursementNumber Not In ( " & strDisbNum & " )")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                Exit Function
            Catch ex As System.Exception
            End Try
        Finally
        End Try
    End Function
    Public Function Check_StudentSchedulePellNew(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal StudentAwardID As String, ByVal drTempRows() As DataRow) As String
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim dsTemp As New DataSet
        '' Dim msgEntry As EDMessageInfo_New
        Dim strAwardScheduleId As String = ""
        Dim strDisbNum As String = ""
        ''  Dim drTemp() As DataRow
        Try
            For Each drC As DataRow In drTempRows
                If strDisbNum = "" Then
                    strDisbNum = drC("DisbNum").ToString
                Else
                    strDisbNum = strDisbNum & "," & drC("DisbNum").ToString
                End If

            Next
            With sbAwardSchedule
                .Append(" Delete From faStudentAwardSchedule where ")
                .Append(" StudentAwardID=?  and DisbursementNumber Not In ( " & strDisbNum & " )")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                Exit Function
            Catch ex As System.Exception
            End Try
        Finally
        End Try
    End Function
    Public Function Check_StudentAwardAndSchedulePell(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal myMsgCollection As EDCollectionMessageInfos_New, ByVal StuEnrollment As String, ByRef ParentRow As DataRow, ByVal drTempRows() As DataRow) As String
        Dim sbStudentAward As New System.Text.StringBuilder
        Dim sbStudentAwardSchedule As New System.Text.StringBuilder
        Dim dsTempSA As New DataSet
        Dim dsTempSAS As New DataSet
        Dim msgEntryParent As EDMessageInfo_New
        Dim msgEntryChild As EDMessageInfo_New
        Dim StudentAwardId As String = ""
        Dim strDisbNum As String = ""
        msgEntryParent = myMsgCollection.Items(Convert.ToInt32(ParentRow("IndexID").ToString))
        With sbStudentAward
            .Append(" Select StudentAwardId, GrossAmount, Disbursements, FA_Id From faStudentAwards ")
            .Append(" Where StuEnrollId=? And FA_Id=? ")
        End With
        'Add params
        db.ClearParameters()
        db.AddParameter("@StuEnrollId", StuEnrollment, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@FA_Id", msgEntryParent.strAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        dsTempSA = db.RunParamSQLDataSet(sbStudentAward.ToString)
        If dsTempSA.Tables(0).Rows.Count > 0 Then
            StudentAwardId = dsTempSA.Tables(0).Rows(0)("StudentAwardId").ToString
            If msgEntryParent.strDatabaseIndicator = "T" Then
                If Convert.ToDecimal(dsTempSA.Tables(0).Rows(0)("GrossAmount").ToString) <> Convert.ToDecimal(msgEntryParent.strAwardAmountForEntireYear) Then
                    msgEntryParent.strErrorType = 3
                    ParentRow("ErrorType") = 3
                    Exit Function
                End If
            Else
                If Convert.ToDecimal(dsTempSA.Tables(0).Rows(0)("GrossAmount").ToString) <> Convert.ToDecimal(msgEntryParent.strAwardAmountForEntireSchoolYear) Then
                    msgEntryParent.strErrorType = 3
                    ParentRow("ErrorType") = 3
                    Exit Function
                End If
            End If
            If Convert.ToInt32(dsTempSA.Tables(0).Rows(0)("Disbursements").ToString) <> drTempRows.Length Then
                msgEntryParent.strErrorType = 3
                ParentRow("ErrorType") = 3
                Exit Function
            End If
            With sbStudentAwardSchedule
                .Append(" Select AwardScheduleId, ExpectedDate, Amount, DisbursementNumber, SequenceNumber From faStudentAwardSchedule")
                .Append(" Where StudentAwardId=? Order By DisbursementNumber")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@StudentAwardID", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            dsTempSAS = db.RunParamSQLDataSet(sbStudentAwardSchedule.ToString)

            If drTempRows.Length = dsTempSAS.Tables(0).Rows.Count Then
                For Each dr As DataRow In drTempRows
                    msgEntryChild = myMsgCollection.Items(Convert.ToInt32(dr("IndexID").ToString))
                    Dim drMatch() As DataRow
                    drMatch = dsTempSAS.Tables(0).Select("ExpectedDate=#" + FormatDate(msgEntryChild.strDisbursementDate.Trim()) + "# And Amount=" + IIf(msgEntryChild.strAcceptedDisbursementAmount.Trim() = "", 0.0, msgEntryChild.strAcceptedDisbursementAmount) + " And DisbursementNumber=" + msgEntryChild.strDisbursementNumber + " And SequenceNumber=" + msgEntryChild.strDisbursementSequenceNumber)
                    If drMatch.Length = 0 Then
                        msgEntryParent.strErrorType = 3
                        ParentRow("ErrorType") = 3
                        Exit Function
                    End If
                Next
            Else
                msgEntryParent.strErrorType = 3
                ParentRow("ErrorType") = 3
                Exit Function
            End If
        Else
            msgEntryParent.strErrorType = 2
            ParentRow("ErrorType") = 2
            Exit Function
        End If
    End Function
    Public Function Check_StudentAwardAndSchedulePellNew(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef ParentRow As DataRow, ByRef drTempRows() As DataRow) As String
        Dim strResult As String = ""
        Dim dsTempSA As New DataSet
        Dim dsTempSAS As New DataSet
        Dim StudentAwardId As String
        Dim intIndex As Integer = 0
        Dim intDisbNum As Integer = 0
        Dim intSeqNum As Integer = 0
        Dim IsShow As Boolean = False
        Try
            db.ClearParameters()
            db.AddParameter("@StuEnrollId", ParentRow("StuEnrollmentId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@AwardId", ParentRow("AwardId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            dsTempSA = db.RunParamSQLDataSetUsingSP("usp_EdExpress_Check_StudentAward")
            If dsTempSA.Tables(0).Rows.Count > 0 Then
                StudentAwardId = dsTempSA.Tables(0).Rows(0)("StudentAwardId").ToString
                If Convert.ToDecimal(dsTempSA.Tables(0).Rows(0)("GrossAmount").ToString) <> Convert.ToDecimal(ParentRow("AwardAmount").ToString) Then
                    If IsShow = False Then
                        IsShow = True
                    End If
                    strResult = "Changed"
                End If
                db.ClearParameters()
                db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                dsTempSAS = db.RunParamSQLDataSetUsingSP("usp_EdExpress_Check_StudentAwardSchedule")
                For Each dr As DataRow In drTempRows
                    Dim drMatch() As DataRow
                    Dim drInDatabase() As DataRow
                    Dim RelInd As String
                    RelInd = dr("DisbRelInd").ToString
                    intSeqNum = CType(dr("SeqNum").ToString, Integer)
                    If RelInd = "Y" Or RelInd = "R" Then
                        RelInd = "Y"
                    End If
                    Dim dtTemp As New DataTable()
                    drMatch = dsTempSAS.Tables(0).Select("ExpectedDate=#" + dr("DisbDate").ToString + "# And Amount=" + dr("AccDisbAmt").ToString + " And DisbursementNumber=" + dr("DisbNum").ToString + " And SequenceNumber=" + dr("SeqNum").ToString + " And RelInd='" + RelInd + "'")
                    If drMatch.Length = 0 Then
                        Dim IsRefund As Integer = 0
                        drInDatabase = dsTempSAS.Tables(0).Select("DisbursementNumber=" + dr("DisbNum").ToString)
                        Dim intSN As Integer = 0
                        Dim intDN As Integer = 0
                        If drInDatabase.Length = 0 Then
                            If CType(dr("SeqNum").ToString, Integer) > 1 And CType(dr("AccDisbAmt").ToString, Decimal) <= 0 Then
                                dr("ErrorType") = 0
                                drTempRows(intIndex)("Show") = 0
                                drTempRows(intIndex - 1)("Show") = 0
                            Else
                                dr("ErrorType") = 3
                                dr("Show") = 1
                                strResult = "Changed"
                                If IsShow = False Then
                                    IsShow = True
                                End If
                            End If
                        Else
                            IsRefund = Convert.ToInt32(drInDatabase(0)("IsRefund").ToString)
                            If IsRefund = 1 Then
                                dr("ErrorType") = 0
                                dr("Show") = 0
                                If IsShow = False Then
                                    IsShow = True
                                End If
                                If intSeqNum > 1 Then
                                    If intIndex > 0 Then
                                        drTempRows(intIndex - 1)("Show") = 0
                                    End If
                                End If
                            Else
                                dr("ErrorType") = 3
                                dr("Show") = 1
                                strResult = "Changed"
                                If IsShow = False Then
                                    IsShow = True
                                End If
                                If intSeqNum > 1 Then
                                    If intIndex > 0 Then
                                        drTempRows(intIndex - 1)("Show") = 0
                                    End If
                                End If
                            End If

                        End If
                        'strResult = "Changed"
                    Else
                        If intSeqNum > 1 Then
                            If intIndex > 0 Then
                                drTempRows(intIndex - 1)("Show") = 0
                            End If
                        End If
                        dr("ErrorType") = 0
                        dr("Show") = 0
                    End If
                    intIndex = intIndex + 1
                Next
            Else
                If IsShow = False Then
                    IsShow = True
                End If
                For Each drC As DataRow In drTempRows
                    intSeqNum = CType(drC("SeqNum").ToString, Integer)
                    drC("ErrorType") = 2
                    drC("Show") = 1
                    If IsShow = False Then
                        IsShow = True
                    End If
                    If intSeqNum > 1 Then
                        If intIndex > 0 Then
                            drTempRows(intIndex - 1)("Show") = 0
                        End If
                    End If
                    intIndex = intIndex + 1
                Next
                strResult = "Changed"
            End If
            ParentRow("ErrorType") = 0
            For Each dr1 As DataRow In drTempRows
                If CType(dr1("Show").ToString, Integer) = 1 Then
                    ParentRow("ErrorType") = 2
                    Exit For
                End If
            Next
            Return strResult
        Catch ex As System.Exception
            Return "Changed"
        End Try
    End Function
    Public Function Check_StudentAwardAndScheduleDL(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal myMsgCollection As EDCollectionMessageInfos_New, ByVal StuEnrollment As String, ByRef ParentRow As DataRow, ByVal drTempRowsSD() As DataRow, ByVal drTempRowsAD() As DataRow) As String
        'Dim sbStudentAward As New System.Text.StringBuilder
        'Dim sbStudentAwardSchedule As New System.Text.StringBuilder
        'Dim dsTempSA As New DataSet
        'Dim dsTempSAS As New DataSet
        'Dim msgEntryParent As EDMessageInfo_New
        'Dim msgEntryChildSD As EDMessageInfo_New
        'Dim msgEntryChildAD As EDMessageInfo_New
        'Dim StudentAwardId As String = ""
        'Dim strDisbNum As String = ""
        'msgEntryParent = myMsgCollection.Items(Convert.ToInt32(ParentRow("IndexID").ToString))
        'With sbStudentAward
        '    .Append(" Select StudentAwardId, GrossAmount,LaonFees, Disbursements From faStudentAwards ")
        '    .Append(" Where StuEnrollId=? And FA_Id=? ")
        'End With
        ''Add params
        'db.ClearParameters()
        'db.AddParameter("@StuEnrollId", StuEnrollment, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@FA_Id", msgEntryParent.strLoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'dsTempSA = db.RunParamSQLDataSet(sbStudentAward.ToString)
        'If dsTempSA.Tables(0).Rows.Count > 0 Then
        '    StudentAwardId = dsTempSA.Tables(0).Rows(0)("StudentAwardId").ToString
        '    If Convert.ToDecimal(dsTempSA.Tables(0).Rows(0)("GrossAmount").ToString) <> Convert.ToDecimal(msgEntryParent.strLoanAmountApproved) Then
        '        msgEntryParent.strErrorType = 3
        '        ParentRow("ErrorType") = 3
        '        Exit Function
        '    End If
        '    If Convert.ToDecimal(dsTempSA.Tables(0).Rows(0)("LaonFees").ToString) <> Convert.ToDecimal(msgEntryParent.strLoanAmountAp) Then
        '        msgEntryParent.strErrorType = 3
        '        ParentRow("ErrorType") = 3
        '        Exit Function
        '    End If

        '    If Convert.ToInt32(dsTempSA.Tables(0).Rows(0)("Disbursements").ToString) <> drTempRowsSD.Length Then
        '        msgEntryParent.strErrorType = 3
        '        ParentRow("ErrorType") = 3
        '        Exit Function
        '    End If
        '    With sbStudentAwardSchedule
        '        .Append(" Select AwardScheduleId, ExpectedDate, Amount, DisbursementNumber, SequenceNumber From faStudentAwardSchedule")
        '        .Append(" Where StudentAwardId=? Order By DisbursementNumber")
        '    End With
        '    'Add params
        '    db.ClearParameters()
        '    db.AddParameter("@StudentAwardID", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '    dsTempSAS = db.RunParamSQLDataSet(sbStudentAwardSchedule.ToString)

        '    If drTempRowsSD.Length = dsTempSAS.Tables(0).Rows.Count Then
        '        For Each dr As DataRow In drTempRowsSD
        '            msgEntryChild = myMsgCollection.Items(Convert.ToInt32(dr("IndexID").ToString))
        '            Dim drMatch() As DataRow
        '            drMatch = dsTempSAS.Tables(0).Select("ExpectedDate=#" + FormatDate(msgEntryChild.strDisbursementDate.Trim()) + "# And Amount=" + IIf(msgEntryChild.strAcceptedDisbursementAmount.Trim() = "", 0.0, msgEntryChild.strAcceptedDisbursementAmount) + " And DisbursementNumber=" + msgEntryChild.strDisbursementNumber + " And SequenceNumber=" + msgEntryChild.strDisbursementSequenceNumber)
        '            If drMatch.Length = 0 Then
        '                msgEntryParent.strErrorType = 3
        '                ParentRow("ErrorType") = 3
        '                Exit Function
        '            End If
        '        Next
        '    Else
        '        msgEntryParent.strErrorType = 3
        '        ParentRow("ErrorType") = 3
        '        Exit Function
        '    End If
        'Else
        '    msgEntryParent.strErrorType = 2
        '    ParentRow("ErrorType") = 2
        '    Exit Function
        'End If
    End Function

    Public Function Check_StudentAwardAndScheduleDLNew(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef ParentRow As DataRow, ByRef drTempRowsSD() As DataRow, ByRef drTempRowsAD() As DataRow) As String
        Dim strResult As String = ""
        Dim dsTempSA As New DataSet
        Dim dsTempSAS As New DataSet
        Dim StudentAwardId As String
        Dim intIndex As Integer = 0
        Dim intDisbNum As Integer = 0
        Dim intSeqNum As Integer = 0
        Dim IsShow As Boolean = False
        Try
            db.ClearParameters()
            db.AddParameter("@StuEnrollId", ParentRow("StuEnrollmentId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@AwardId", ParentRow("LoanId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            dsTempSA = db.RunParamSQLDataSetUsingSP("usp_EdExpress_Check_StudentAward")
            If dsTempSA.Tables(0).Rows.Count > 0 Then
                StudentAwardId = dsTempSA.Tables(0).Rows(0)("StudentAwardId").ToString
                If Convert.ToDecimal(dsTempSA.Tables(0).Rows(0)("GrossAmount").ToString) <> Convert.ToDecimal(ParentRow("GrossAmount").ToString) Then
                    If IsShow = False Then
                        IsShow = True
                    End If
                    strResult = "Changed"
                End If
                db.ClearParameters()
                db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                dsTempSAS = db.RunParamSQLDataSetUsingSP("usp_EdExpress_Check_StudentAwardSchedule")
                For Each dr As DataRow In drTempRowsSD
                    Dim drMatch() As DataRow
                    Dim drInDatabase() As DataRow
                    Dim RelInd As String
                    RelInd = dr("DisbRelInd").ToString
                    intSeqNum = CType(dr("SeqNum").ToString, Integer)
                    If RelInd = "Y" Or RelInd = "R" Then
                        RelInd = "Y"
                    End If
                    Dim dtTemp As New DataTable()
                    drMatch = dsTempSAS.Tables(0).Select("ExpectedDate=#" + dr("DisbDate").ToString + "# And Amount=" + dr("NetAmt").ToString + " And DisbursementNumber=" + dr("DisbNum").ToString + " And SequenceNumber=" + dr("SeqNum").ToString + " And RelInd='" + RelInd + "'")
                    If drMatch.Length = 0 Then
                        drInDatabase = dsTempSAS.Tables(0).Select("DisbursementNumber=" + dr("DisbNum").ToString)
                        Dim intSN As Integer = 0
                        Dim intDN As Integer = 0
                        If drInDatabase.Length = 0 Then
                            If CType(dr("SeqNum").ToString, Integer) > 1 And CType(dr("NetAmt").ToString, Decimal) <= 0 Then
                                dr("ErrorType") = 0
                                drTempRowsSD(intIndex)("Show") = 0
                                drTempRowsSD(intIndex - 1)("Show") = 0
                            Else
                                dr("ErrorType") = 3
                                dr("Show") = 1
                                If IsShow = False Then
                                    IsShow = True
                                End If
                            End If
                        Else
                            dr("ErrorType") = 3
                            dr("Show") = 1
                            If IsShow = False Then
                                IsShow = True
                            End If
                            If intSeqNum > 1 Then
                                If intIndex > 0 Then
                                    drTempRowsSD(intIndex - 1)("Show") = 0
                                End If
                            End If
                        End If
                        strResult = "Changed"
                    Else
                        If intSeqNum > 1 Then
                            If intIndex > 0 Then
                                drTempRowsSD(intIndex - 1)("Show") = 0
                            End If
                        End If
                        dr("ErrorType") = 0
                        dr("Show") = 0
                    End If
                    intIndex = intIndex + 1
                Next
                intIndex = 0
                For Each dr As DataRow In drTempRowsAD
                    Dim drMatch() As DataRow
                    Dim drInDatabase() As DataRow
                    Dim RelInd As String
                    RelInd = dr("DisbRelInd").ToString
                    intSeqNum = CType(dr("SeqNum").ToString, Integer)
                    If RelInd = "A" Then
                        RelInd = "Y"
                    Else
                        RelInd = "N"
                    End If
                    Dim dtTemp As New DataTable()
                    ''Troy: 7/22/2011
                    ''We are processing actual disbursements here. However, the original code was checking the scheduled disbursements 
                    ''which created issues when the school had to make adjustments to the actual disbursements without updating the
                    ''scheduled disbursements. For eg. the school scheduled disb 6 for $583, received that amount and then had to
                    ''refund $562. The refund was posted to the student's ledger and a sequence 2 was created for the net amount of $21
                    ''in EdExpress. When the next import was done, this $21 was showing up as a pending payment. Note also that the 
                    ''scheduled disbursement amount ended up being updated to $21 as well. 
                    ''When an actual disbursement has already been paid on the student ledger as indicated in the saPmtDisbRel table then
                    ''it should be ignored as we do not want to ever overwrite anything posted to the student ledger.
                    ''When an actual disbursement has NOT been paid on the student ledger then we should post the record with the highest
                    ''sequence number. 
                    ''For example, assume that the actual disbursement 5 has sequence 1 for $500, sequence 2 for $250 and
                    ''sequence 3 for $0. If this disbursement does not have a record in the saPmtDisbRel table then the final
                    ''result of the processing should be that there is a payment for sequence 3 recorded on the ledger. 
                    ''In this case since sequence 3 is for $0 we would not actually post anything to the ledger.

                    If HasDisbursementBeenPaid(dr("LoanId"), dr("DisbNum")) Then
                        If intSeqNum > 1 Then
                            If intIndex > 0 Then
                                drTempRowsAD(intIndex - 1)("Show") = 0
                            End If
                        End If
                        dr("ErrorType") = 0
                        dr("Show") = 0
                    Else
                        If intSeqNum > 1 Then
                            If intIndex > 0 Then
                                drTempRowsAD(intIndex - 1)("Show") = 0
                            End If
                        End If
                        If CType(dr("NetAmt").ToString, Decimal) = 0 Then
                            dr("ErrorType") = 0
                            dr("Show") = 0
                        Else
                            dr("ErrorType") = 3
                            dr("Show") = 1
                            If IsShow = False Then
                                IsShow = True
                            End If
                            strResult = "Changed"
                        End If
                    End If


                    'drMatch = dsTempSAS.Tables(0).Select("ExpectedDate=#" + dr("DisbDate").ToString + "# And Amount=" + dr("NetAmt").ToString + " And DisbursementNumber=" + dr("DisbNum").ToString + " And SequenceNumber=" + dr("SeqNum").ToString + " And RelInd='" + RelInd + "'")
                    'If drMatch.Length = 0 Then
                    '    drInDatabase = dsTempSAS.Tables(0).Select("DisbursementNumber=" + dr("DisbNum").ToString)
                    '    Dim intSN As Integer = 0
                    '    Dim intDN As Integer = 0
                    '    If drInDatabase.Length = 0 Then
                    '        If CType(dr("SeqNum").ToString, Integer) > 1 And CType(dr("NetAmt").ToString, Decimal) <= 0 Then
                    '            dr("ErrorType") = 0
                    '            drTempRowsAD(intIndex)("Show") = 0
                    '            drTempRowsAD(intIndex - 1)("Show") = 0
                    '        Else
                    '            dr("ErrorType") = 3
                    '            dr("Show") = 1
                    '            If IsShow = False Then
                    '                IsShow = True
                    '            End If
                    '        End If
                    '    Else
                    '        dr("ErrorType") = 3
                    '        dr("Show") = 1
                    '        If IsShow = False Then
                    '            IsShow = True
                    '        End If
                    '        If intSeqNum > 1 Then
                    '            If intIndex > 0 Then
                    '                drTempRowsAD(intIndex - 1)("Show") = 0
                    '            End If
                    '        End If
                    '    End If
                    '    strResult = "Changed"
                    'Else
                    '    If intSeqNum > 1 Then
                    '        If intIndex > 0 Then
                    '            drTempRowsAD(intIndex - 1)("Show") = 0
                    '        End If
                    '    End If
                    '    dr("ErrorType") = 0
                    '    dr("Show") = 0
                    'End If
                    intIndex = intIndex + 1
                Next
            Else
                If IsShow = False Then
                    IsShow = True
                End If
                For Each drS As DataRow In drTempRowsSD
                    intSeqNum = CType(drS("SeqNum").ToString, Integer)
                    drS("ErrorType") = 2
                    drS("Show") = 1
                    If IsShow = False Then
                        IsShow = True
                    End If
                    If intSeqNum > 1 Then
                        If intIndex > 0 Then
                            drTempRowsSD(intIndex - 1)("Show") = 0
                        End If
                    End If
                    intIndex = intIndex + 1
                Next
                intIndex = 0
                For Each drA As DataRow In drTempRowsAD
                    intSeqNum = CType(drA("SeqNum").ToString, Integer)
                    drA("ErrorType") = 2
                    drA("Show") = 1
                    If IsShow = False Then
                        IsShow = True
                    End If
                    If intSeqNum > 1 Then
                        If intIndex > 0 Then
                            drTempRowsAD(intIndex - 1)("Show") = 0
                        End If
                    End If
                    intIndex = intIndex + 1
                Next
                strResult = "Changed"

            End If
            ParentRow("ErrorType") = 0
            For Each dr1 As DataRow In drTempRowsSD
                If CType(dr1("Show").ToString, Integer) = 1 Then
                    ParentRow("ErrorType") = 2
                    Exit For
                End If
            Next
            For Each dr2 As DataRow In drTempRowsAD
                If CType(dr2("Show").ToString, Integer) = 1 Then
                    ParentRow("ErrorType") = 2
                    Exit For
                End If
            Next
            Return strResult
        Catch ex As System.Exception
            Return "Changed"
        End Try
    End Function

    Public Function Check_StudentScheduleDL(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal StudentAwardID As String, ByVal myMsgCollection As EDCollectionMessageInfos_New, ByVal drTempRows() As DataRow) As String
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim dsTemp As New DataSet
        Dim msgEntry As EDMessageInfo_New
        Dim strAwardScheduleId As String = ""
        Dim strDisbNum As String = ""
        '' Dim drTemp() As DataRow
        Try
            For Each drC As DataRow In drTempRows
                msgEntry = myMsgCollection.Items(Convert.ToInt32(drC("IndexID").ToString))
                If strDisbNum = "" Then
                    strDisbNum = msgEntry.strAnticipatedDisbursementNumber
                Else
                    strDisbNum = strDisbNum & "," & msgEntry.strAnticipatedDisbursementNumber
                End If
            Next
            With sbAwardSchedule
                .Append(" Delete From faStudentAwardSchedule where ")
                .Append(" StudentAwardID=?  and DisbursementNumber Not In ( " & strDisbNum & " )")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                Exit Function
            Catch ex As System.Exception
            End Try
        Finally
        End Try
    End Function
    Public Function Check_TransactionPosted(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal StudentAwardScheduleID As String, ByRef TransactionID As String) As Boolean
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim dsTemp As New DataSet
        '' Dim msgEntry As EDMessageInfo
        Try
            With sbAwardSchedule
                .Append(" Select TransactionId from saPmtDisbRel where ")
                .Append(" AwardScheduleId=? ")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@AwardScheduleId", StudentAwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                dsTemp = db.RunParamSQLDataSet(sbAwardSchedule.ToString)
                If dsTemp.Tables(0).Rows.Count = 0 Then
                    Return False
                    Exit Function
                Else
                    TransactionID = dsTemp.Tables(0).Rows(0)("TransactionId").ToString
                    Return True
                    Exit Function
                End If
            Catch ex As System.Exception
            End Try
        Finally
        End Try
    End Function
    Public Function Add_faStudentAwardsFromDL(ByRef groupTrans As OleDbTransaction, ByRef db As DataAccess, ByRef StudentAwardID As String, ByVal StuEnrollID As String, ByVal msgEntry As EDMessageInfo_New, ByVal DisbNumber As String, Optional ByVal strStudentName As String = "", Optional ByVal SourceToTarget As String = "") As String
        Dim sbStudentAwards As New System.Text.StringBuilder(1000)
        Dim sbUpdateStudentAwards As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwards As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchool As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchoolDiffLoanPeriods As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchoolWithDiffGrossAmount As New System.Text.StringBuilder(1000)
        ''  Dim dr As OleDbDataReader

        Dim AwardTypeID As String = " "
        Dim getAwardTypeMessage As String = ""
        Dim Disbursements As Integer = 0
        Dim LoanID As String = ""
        Dim intDuplicates As Integer = 0
        Dim intDuplicatesBySchool As Integer = 0
        Dim intDuplicatesBySchoolDiffLoanPeriods As Integer = 0
        Dim intDuplicatesBySchoolDiffGrossAmount As Integer = 0
        Dim intDuplicatesBySchoolSameAwardYearDiffLoanPeriods As Integer = 0

        Dim sbAcademicYear As New StringBuilder
        m_ExceptionMessage = ""

        'db.OpenConnection()

        If String.IsNullOrEmpty(StuEnrollID) Or StuEnrollID = Guid.Empty.ToString Then
            Return "Unable to find enrollment information for the student" & strStudentName
            Exit Function
        End If
        Dim dbAmount As Double = 0.0
        Dim dbFeePer As Double = 0.0
        Dim dbLoanFees As Double = 0.0
        Try
            dbAmount = Convert.ToDouble(msgEntry.strLoanAmountApproved)
        Catch ex As Exception
            dbAmount = 0.0
        End Try
        Try
            dbFeePer = Convert.ToDouble(msgEntry.strLoanFeePercentage)
        Catch ex As Exception
            dbFeePer = 0.0
        End Try
        dbLoanFees = (dbAmount * dbFeePer) / 100
        'Look for matching records
        Try

            StudentAwardID = GetMatchingStudentAwards(db, StuEnrollID, msgEntry.strLoanId, msgEntry.strLoanType, msgEntry.strLoanAmountApproved, msgEntry.strAwardYearStartDate, msgEntry.strAwardYearEndDate, strStudentName, msgEntry.strMsgType)

            If Not StudentAwardID = "" Then
                'Update existing record with this FAID
                With sbUpdateStudentAwards
                    .Append("Update faStudentAwards set AcademicYearId=?,GrossAmount=?,LoanFees=?,AwardStartDate=?,AwardEndDate=?,moduser=?,moddate=?, ")
                    If Not DisbNumber = "0" Then
                        .Append(" Disbursements=?, ")
                    End If
                    .Append(" fa_id=? where StudentAwardId=? ")
                End With
                db.ClearParameters()
                db.AddParameter("@AcademicYearId", msgEntry.strAcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@GrossAmount", dbAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@LoanFees", dbLoanFees, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@AwardStartDate", msgEntry.strAwardYearStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardEndDate", msgEntry.strAwardYearEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If Not DisbNumber = "0" Then
                    db.AddParameter("@Disbursements", DisbNumber, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                End If
                db.AddParameter("@fa_id", msgEntry.strLoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Try
                    db.RunParamSQLExecuteNoneQuery(sbUpdateStudentAwards.ToString)
                    Return StudentAwardID
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to match/update FAID with student awards for student " & strStudentName
                    Exit Function
                End Try
            Else
                StudentAwardID = Guid.NewGuid.ToString
            End If
        Catch ex As System.Exception
            StudentAwardID = Guid.NewGuid.ToString
        End Try

        'Before Creating new awards check to see if the Award Start Date falls after the AwardsCuttOffDate
        'in web.config
        Try

            getAwardTypeMessage = GetAwardTypeID(db, AwardTypeID, msgEntry.strLoanType, msgEntry.strMsgType)

            If Not getAwardTypeMessage = "" Then
                Return "Unable to add student awards for student " & strStudentName & " as the provided award type was not found"
                Exit Function
            End If

            With sbStudentAwards
                .Append("INSERT INTO faStudentAwards(StudentAwardID,StuEnrollID,AwardTypeID,AcademicYearId,fa_id,GrossAmount,LoanFees,AwardStartDate,AwardEndDate,moduser,moddate,Disbursements,LoanId) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AwardTypeID", AwardTypeID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AcademicYearId", msgEntry.strAcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@fa_id", msgEntry.strLoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@GrossAmount", dbAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@LoanFees", dbLoanFees, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@AwardStartDate", msgEntry.strAwardYearStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AwardEndDate", msgEntry.strAwardYearEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Disbursements", DisbNumber, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@LoanId", LoanID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   execute the query
            Try
                db.RunParamSQLExecuteNoneQuery(sbStudentAwards.ToString)
                Return StudentAwardID
                Exit Function
            Catch ex As System.Exception
                Return "Unable to create student awards for student " & strStudentName
                'db.CloseConnection()
                Exit Function
            End Try
            '' ''End If
        Catch ex As System.Exception
            Return "Unable to create student award for student " & strStudentName
            Exit Function
        Finally
        End Try
    End Function

    Public Function FormatDate(ByRef DateToFormat As String) As String
        Dim strDD As String
        Dim strMM As String
        Dim strYYYY As String
        Try
            strYYYY = DateToFormat.Substring(0, 4)
            strMM = DateToFormat.Substring(4, 2)
            strDD = DateToFormat.Substring(6, 2)
            Return strMM + "/" + strDD + "/" + strYYYY
        Catch ex As Exception
            Return DateToFormat
        End Try
    End Function
    Public Function FormatTime(ByRef TimeToFormat As String) As String
        Dim strHH As String
        Dim strMM As String
        Dim strSS As String
        Try
            strHH = TimeToFormat.Substring(0, 2)
            strMM = TimeToFormat.Substring(2, 2)
            strSS = TimeToFormat.Substring(4, 2)
            Return strHH + ":" + strMM + ":" + strSS
        Catch ex As Exception
            Return TimeToFormat
        End Try
    End Function
    Public Function GetFileNames() As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Try
            ds = db.RunParamSQLDataSetUsingSP("usp_EdExpress_GetFileNames", "FileNames")
            Return ds
        Catch ex As Exception
            Return ds
        End Try
    End Function
    Public Function getExceptionReport(ByVal ExceptionGUID As String, ByVal MsgType As String) As DataSet
        Dim db As New DataAccess
        Dim sbException As New StringBuilder
        Dim ds As New DataSet
        Dim dsTemp As New DataSet
        Dim dsR As New DataSet
        Dim dtParent As New DataTable("Parent")
        Dim dtChild As New DataTable("Child")
        ''  Dim drTemp() As DataRow
        Dim drParent As DataRow
        Dim drChild As DataRow
        Dim strERID_Old As String = ""
        Dim strERID_New As String = ""
        Dim strGrantType As String = ""
        ds = Nothing

        Dim dbValue As Double = 0.0

        dtParent.Columns.Add("ExceptionReportId", GetType(String))
        dtParent.Columns.Add("SSN", GetType(String))
        dtParent.Columns.Add("Campus", GetType(String))
        dtParent.Columns.Add("Amount", GetType(String))
        dtParent.Columns.Add("AwardIdLoanId", GetType(String))
        dtParent.Columns.Add("GrantTypeLoanType", GetType(String))
        dtParent.Columns.Add("FileName", GetType(String))
        dtParent.Columns.Add("Message", GetType(String))
        dtParent.Columns.Add("ModDate", GetType(DateTime))

        dtChild.Columns.Add("ExceptionReportId", GetType(String))
        dtChild.Columns.Add("DisbType", GetType(String))
        dtChild.Columns.Add("SSN", GetType(String))
        dtChild.Columns.Add("Campus", GetType(String))
        dtChild.Columns.Add("DisbDate", GetType(String))
        dtChild.Columns.Add("AccDisbAmtDisbGrossAmt", GetType(String))
        dtChild.Columns.Add("SubDisbAmtDisbNetAmt", GetType(String))
        dtChild.Columns.Add("FileName", GetType(String))
        dtChild.Columns.Add("Message", GetType(String))
        dtChild.Columns.Add("ModDate", GetType(DateTime))

        If MsgType = "PELL" Then

            Try
                With sbException
                    .Append(" Select PT.ExceptionReportId,PT.DbIn, PT.OriginalSSN ,PT.AwardAmount,PT.AwardId,PT.GrantType,PT.FileName,PT.ErrorMsg,PT.ModDate, ")
                    .Append(" (SELECT TOP 1 C.CampDescrip FROM arStuEnrollments SE INNER JOIN arStudent S ON SE.StudentId = S.StudentId INNER JOIN syCampuses C ON SE.CampusId=C.CampusId WHERE PT.OriginalSSN=S.SSN  order by EnrollDate Desc ) AS CampDescrip ")
                    .Append(" From syEDExpressExceptionReportPell_ACG_SMART_Teach_StudentTable PT Where PT.ExceptionGUID='" & ExceptionGUID & "' Order By PT.OriginalSSN")
                End With
                ds = db.RunParamSQLDataSet(sbException.ToString)
                For Each dr As DataRow In ds.Tables(0).Rows
                    drParent = dtParent.NewRow()
                    drParent("ExceptionReportId") = dr("ExceptionReportId").ToString
                    drParent("SSN") = dr("OriginalSSN").ToString
                    drParent("Campus") = dr("CampDescrip").ToString
                    Try
                        dbValue = Convert.ToDouble(dr("AwardAmount").ToString)
                    Catch ex As Exception
                        dbValue = 0.0
                    End Try
                    drParent("Amount") = dbValue.ToString("0.00")
                    drParent("AwardIdLoanId") = dr("AwardId").ToString
                    If dr("DbIn").ToString.ToUpper = "H" Then
                        strGrantType = "TEACH"
                    Else
                        strGrantType = IIf(dr("GrantType").ToString = "" Or dr("GrantType").ToString = "P", "PELL", IIf(dr("GrantType").ToString = "A", "ACG", IIf(dr("GrantType").ToString = "S" Or dr("GrantType").ToString = "T", "SMART", "")))
                    End If
                    drParent("GrantTypeLoanType") = strGrantType
                    drParent("FileName") = dr("FileName").ToString
                    drParent("Message") = dr("ErrorMsg").ToString
                    drParent("ModDate") = dr("ModDate").ToString
                    dtParent.Rows.Add(drParent)
                    sbException = sbException.Remove(0, sbException.Length)
                    With sbException
                        .Append(" Select CT.DbIn,CT.OriginalSSN ,CT.DisbDate,CT.AccDisbAmount,CT.SimittedDisbAmount,CT.FileName,CT.ErrorMsg,CT.ModDate, ")
                        .Append(" (SELECT TOP 1 C.CampDescrip FROM arStuEnrollments SE INNER JOIN arStudent S ON SE.StudentId = S.StudentId INNER JOIN syCampuses C ON SE.CampusId=C.CampusId WHERE CT.OriginalSSN=S.SSN  order by EnrollDate Desc ) AS CampDescrip ")
                        .Append(" From syEDExpressExceptionReportPell_ACG_SMART_Teach_DisbursementTable CT Where CT.ExceptionReportId='" & dr("ExceptionReportId").ToString & "' Order By CT.OriginalSSN")
                    End With
                    dsTemp = db.RunParamSQLDataSet(sbException.ToString)
                    For Each drC As DataRow In dsTemp.Tables(0).Rows
                        drChild = dtChild.NewRow()
                        drChild("ExceptionReportId") = dr("ExceptionReportId").ToString
                        drChild("SSN") = dr("OriginalSSN").ToString
                        drChild("Campus") = dr("CampDescrip").ToString
                        drChild("DisbDate") = FormatDate(drC("DisbDate").ToString)
                        Try
                            dbValue = Convert.ToDouble(drC("AccDisbAmount").ToString)
                        Catch ex As Exception
                            dbValue = 0.0
                        End Try
                        drChild("AccDisbAmtDisbGrossAmt") = dbValue.ToString("0.00")
                        Try
                            dbValue = Convert.ToDouble(drC("SimittedDisbAmount").ToString)
                        Catch ex As Exception
                            dbValue = 0.0
                        End Try
                        drChild("SubDisbAmtDisbNetAmt") = dbValue.ToString("0.00")
                        drChild("FileName") = drC("FileName").ToString
                        drChild("Message") = drC("ErrorMsg").ToString
                        drChild("ModDate") = drC("ModDate").ToString
                        dtChild.Rows.Add(drChild)
                    Next
                Next
                dsR.Tables.Add(dtParent)
                dsR.Tables.Add(dtChild)
                Return dsR
            Catch ex As Exception
                Return dsR
            End Try
        Else
            Try
                With sbException
                    .Append(" Select PT.ExceptionReportId,PT.DbIn, PT.OriginalSSN ,PT.LoanAmtApproved,PT.LoanId,PT.LoanType,PT.FileName,PT.ErrorMsg,PT.ModDate, ")
                    .Append(" (SELECT TOP 1 C.CampDescrip FROM arStuEnrollments SE INNER JOIN arStudent S ON SE.StudentId = S.StudentId INNER JOIN syCampuses C ON SE.CampusId=C.CampusId WHERE PT.OriginalSSN=S.SSN  order by EnrollDate Desc ) AS CampDescrip ")
                    .Append(" From syEDExpressExceptionReportDirectLoan_StudentTable PT Where PT.ExceptionGUID='" & ExceptionGUID & "' Order By PT.OriginalSSN")
                End With
                ds = db.RunParamSQLDataSet(sbException.ToString)
                For Each dr As DataRow In ds.Tables(0).Rows
                    drParent = dtParent.NewRow()
                    drParent("ExceptionReportId") = dr("ExceptionReportId").ToString
                    drParent("SSN") = dr("OriginalSSN").ToString
                    Try
                        dbValue = Convert.ToDouble(dr("LoanAmtApproved").ToString)
                    Catch ex As Exception
                        dbValue = 0.0
                    End Try
                    drParent("Amount") = dbValue.ToString("0.00")
                    drParent("Campus") = dr("CampDescrip").ToString
                    drParent("AwardIdLoanId") = dr("LoanId").ToString
                    drParent("GrantTypeLoanType") = IIf(dr("LoanType").ToString = "P", "DL - PLUS", IIf(dr("LoanType").ToString = "S", "DL - SUB", IIf(dr("LoanType").ToString = "U", "DL - UNSUB", "")))
                    drParent("FileName") = dr("FileName").ToString
                    drParent("Message") = dr("ErrorMsg").ToString
                    drParent("ModDate") = dr("ModDate").ToString
                    dtParent.Rows.Add(drParent)
                    sbException = sbException.Remove(0, sbException.Length)
                    With sbException
                        .Append(" Select CT.DbIn,  CASE(CT.DbIn) When 'M' then 'Actual Disbursement Record' else 'Anticipated Disbursement Record' end As DisbType, CT.OriginalSSN ,CT.DisbDate,CT.DisbGrossAmount,CT.DisbNetAmount,CT.FileName,CT.ErrorMsg,CT.ModDate, ")
                        .Append(" (SELECT TOP 1 C.CampDescrip FROM arStuEnrollments SE INNER JOIN arStudent S ON SE.StudentId = S.StudentId INNER JOIN syCampuses C ON SE.CampusId=C.CampusId WHERE CT.OriginalSSN=S.SSN  order by EnrollDate Desc ) AS CampDescrip ")
                        .Append(" From syEDExpressExceptionReportDirectLoan_DisbursementTable CT Where CT.ExceptionReportId='" & dr("ExceptionReportId").ToString & "' Order By CT.OriginalSSN, DisbType")
                    End With
                    dsTemp = db.RunParamSQLDataSet(sbException.ToString)
                    For Each drC As DataRow In dsTemp.Tables(0).Rows
                        drChild = dtChild.NewRow()
                        drChild("ExceptionReportId") = dr("ExceptionReportId").ToString
                        drChild("DisbType") = drC("DisbType").ToString
                        drChild("SSN") = dr("OriginalSSN").ToString
                        drChild("Campus") = dr("CampDescrip").ToString
                        drChild("DisbDate") = FormatDate(drC("DisbDate").ToString)
                        Try
                            dbValue = Convert.ToDouble(drC("DisbGrossAmount").ToString)
                        Catch ex As Exception
                            dbValue = 0.0
                        End Try
                        drChild("AccDisbAmtDisbGrossAmt") = dbValue.ToString("0.00")
                        Try
                            dbValue = Convert.ToDouble(drC("DisbNetAmount").ToString)
                        Catch ex As Exception
                            dbValue = 0.0
                        End Try
                        drChild("SubDisbAmtDisbNetAmt") = dbValue.ToString("0.00")
                        drChild("FileName") = drC("FileName").ToString
                        drChild("Message") = drC("ErrorMsg").ToString
                        drChild("ModDate") = drC("ModDate").ToString
                        dtChild.Rows.Add(drChild)
                    Next
                Next
                dsR.Tables.Add(dtParent)
                dsR.Tables.Add(dtChild)
                Return dsR
            Catch ex As Exception
                Return dsR
            End Try
        End If
    End Function
    Public Function GetAwardYearForDL() As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        With sb
            .Append("SELECT   CCT.AcademicYearId, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("         CCT.AcademicYearCode, ")
            .Append("         CCT.AcademicYearDescrip ")
            .Append("FROM     saAcademicYears CCT, syStatuses ST ")
            .Append("WHERE    CCT.StatusId = ST.StatusId AND ST.Status='Active' ")
            .Append("ORDER BY ST.Status,CCT.AcademicYearDescrip asc")
        End With
        Return db.RunSQLDataSet(sb.ToString, "AwardYears")
    End Function
    Public Function GetPaymentsTypes() As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        With sb
            .Append("SELECT   PaymentTypeId, Description From saPaymentTypes Where PaymentTypeId in (2,4,6) ")
            .Append("Order By Description Asc")
        End With
        Return db.RunSQLDataSet(sb.ToString, "PaymentTypes")
    End Function

    Public Function HasDisbursementBeenPaid(ByVal FAID As String, ByVal DisbNum As Integer) As Boolean
        Dim db As New DataAccess
        Dim numRecs As Integer

        Try
            numRecs = db.RunParamSQLScalar("usp_EdExpress_HasDisbursementBeenPaid")

            If numRecs > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error getting count of payments for scheduled disbursement - " & ex.Message)
            Else
                Throw New Exception("Error getting count of payments for scheduled disbursement - " & ex.InnerException.Message)
            End If
        End Try



    End Function

    Public Function DeleteNotPostedParentRecordsWithNoChildRecords() As String
        Dim db As New SQLDataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            db.RunParamSQLExecuteNoneQuery_SP("usp_EdExpress_DeleteNotPostedParentRecordsWithNoChildRecords", Nothing)
            Return ""
        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Return ex.Message.ToString
            Else
                Return ex.InnerException.Message.ToString
            End If
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try

    End Function

#End Region
End Class
