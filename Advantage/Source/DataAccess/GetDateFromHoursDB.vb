﻿
Imports FAME.Advantage.Common

Public Class GetDateFromHoursDB
    Public Function GetDateFromHours(ByVal StuEnrollId As String, ByRef Msg As String, ByRef UseTimeClock As Boolean, ByVal CampusId As String) As DateTime
        Dim dtDate As DateTime = Nothing
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim da As System.Data.OleDb.OleDbDataAdapter
        Dim PrgVerId As String
        Dim ScheduleId As String
        Dim TotalPrgHours As Double = 0.0
        Dim TransfreGradeHours As Double = 0.0

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.OpenConnection()
        With sb
            '.Append(" Select SE.StartDate, PV.Hours, SE.PrgVerId, SE.ScheduleId from arStuEnrollments SE, arPrgVersions PV where SE.PrgVerId=PV.PrgVerId and SE.StuEnrollId='" & StuEnrollId & "' ")
            '' Code changed by kamalesh ahuja on June 02 2010 to resolve mantis issue id 19089 and 19113
            ''.Append(" Select SE.StartDate, PV.Hours, SE.PrgVerId, SE.ScheduleId, CASE(UnitTypeDescrip) When 'Clock Hours'  Then 1 When 'Minutes' Then 1 Else 0 End As UseTimeClock from arStuEnrollments SE, arPrgVersions PV, arAttUnitType UT where PV.UnitTypeId=UT.UnitTypeId And SE.PrgVerId=PV.PrgVerId and SE.StuEnrollId='" & StuEnrollId & "' ")
            ''Code Changed By Vijay Ramteke On June 17, 2010 For Mantis Id 19068
            ''.Append(" Select SE.StartDate, PV.Hours, SE.PrgVerId, SE.ScheduleId, CASE(ACDescrip) When 'Clock Hour'  Then 1 Else 0 End As UseTimeClock from arStuEnrollments SE, arPrgVersions PV, arPrograms P,syAcademicCalendars AC where SE.PrgVerId=PV.PrgVerId  and P.ProgId=PV.ProgId  and P.ACId=AC.ACId  and SE.StuEnrollId='" & StuEnrollId & "' ")
            .Append(" SELECT SE.StartDate, PV.Hours, SE.PrgVerId, CASE(SELECT ACDescrip FROM syAcademicCalendars AC WHERE P.ACId=AC.ACId) WHEN 'Clock Hour' THEN (CASE(SELECT UnitTypeDescrip FROM arAttUnitType UT WHERE PV.UnitTypeId=UT.UnitTypeId) WHEN 'Clock Hours' THEN 1 WHEN 'Minutes' THEN 1 ELSE 0 END) ELSE 0 END AS UseTimeClock FROM arStuEnrollments SE, arPrgVersions PV, arPrograms P WHERE SE.PrgVerId=PV.PrgVerId AND P.ProgId=PV.ProgId AND SE.StuEnrollId='" & StuEnrollId & "' ")
            '''''''''''''''''''''''''
        End With
        da = db.RunParamSQLDataAdapter(sb.ToString)
        da.Fill(ds, "ProgramHours")

        TotalPrgHours = Convert.ToDecimal(ds.Tables("ProgramHours").Rows(0)("Hours").ToString)
        UseTimeClock = CType(ds.Tables("ProgramHours").Rows(0)("UseTimeClock").ToString, Boolean)

        If Not TotalPrgHours = 0 And UseTimeClock = True Then
            dtDate = Convert.ToDateTime(ds.Tables("ProgramHours").Rows(0)("StartDate").ToString)
            PrgVerId = ds.Tables("ProgramHours").Rows(0)("PrgVerId").ToString
            'If Not ds.Tables("ProgramHours").Rows(0)("ScheduleId") Is DBNull.Value Then
            '    ScheduleId = ds.Tables("ProgramHours").Rows(0)("ScheduleId").ToString
            'Else
            '    ScheduleId = Guid.Empty.ToString
            'End If
            ScheduleId = Guid.Empty.ToString
            sb = sb.Remove(0, sb.Length)
            With sb
                .Append(" Select PS.ScheduleId, PSD.dw, PSD.total, (Select SUM(PSDI.total) From arProgScheduleDetails PSDI Where PSDI.ScheduleId=PS.ScheduleId) as HoursTotal  from arStuEnrollments SE, arProgSchedules PS, arProgScheduleDetails PSD, arStudentSchedules SS  Where SE.PrgVerId=PS.PrgVerId and SE.StuEnrollId =SS.StuEnrollId and SS.ScheduleId =PS.ScheduleId And PS.ScheduleId=PSD.ScheduleId And PSD.total>0  And SE.StuEnrollId='" & StuEnrollId & "' Order by dw ")
            End With
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "WeekDays")
            sb = sb.Remove(0, sb.Length)
            With sb
                .Append(" Select LOA.StartDate, LOA.EndDate from arStudentLOAs LOA Where LOA.StuEnrollId='" & StuEnrollId & "' Order By LOA.StartDate ")
            End With
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "StudentLOAs")
            sb = sb.Remove(0, sb.Length)
            With sb
                .Append(" Select SS.StartDate, SS.EndDate from arStdSuspensions SS Where SS.StuEnrollId='" & StuEnrollId & "' Order by SS.StartDate ")
            End With
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "StudentSuspensions")
            sb = sb.Remove(0, sb.Length)
            With sb
                ''New Code Added By Vijay Ramteke On June 15, 2010 For Mantis Id 19167 
                ''.Append(" Select HolidayStartDate, HolidayEndDate, AllDay From syHolidays Order by HolidayStartDate ")
                '.Append(" Select HolidayStartDate, HolidayEndDate, AllDay ")
                '.Append(" FROM syHolidays H, syCampGrps CG, syCampuses C, syStatuses S ")
                '.Append(" WHERE H.CampGrpId=CG.CampGrpId AND C.CampusId=CG.CampusId  AND S.StatusId=H.StatusId AND S.Status='Active' ")
                '.Append(" AND C.CampusId='" + CampusId + "' ")
                .Append(" Select HolidayStartDate, HolidayEndDate, AllDay  FROM syHolidays H, syCmpGrpCmps t1, syCampGrps t2, syStatuses S ")
                .Append(" where t1.CampGrpId=t2.CampGrpId AND S.StatusId=H.StatusId AND S.Status='Active' ")
                .Append(" and H.CampGrpId=t2.CampGrpId and t1.CampusId='" + CampusId + "' ")
                ''New Code Added By Vijay Ramteke On June 15, 2010 For Mantis Id 19167
            End With
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "Holidays")
            sb = sb.Remove(0, sb.Length)
            With sb
                'Commented By Vijay Ramteke on April 14, 2010 Mantis id 18850
                '.Append(" Select SUM(ActualHours) As ActualHours,Max(RecordDate) as RecordDate from arStudentClockAttendance Where StuEnrollId='" & StuEnrollId & "' And SchedHours>0 And ActualHours<>9999.0 ")
                'Commented By Vijay Ramteke on April 14, 2010 Mantis id 18850
                .Append(" Select SUM(ActualHours) As ActualHours,Max(RecordDate) as RecordDate from arStudentClockAttendance Where StuEnrollId='" & StuEnrollId & "' And ActualHours<>9999.0 ")
                If Not ScheduleId = Guid.Empty.ToString Then
                    .Append("And ScheduleId='" & ScheduleId & "'  Order by RecordDate ")
                Else
                    .Append("And ScheduleId in (Select Distinct PS.ScheduleId from arStuEnrollments SE, arProgSchedules PS, arProgScheduleDetails PSD Where (SE.PrgVerId=PS.PrgVerId) and PS.ScheduleId=PSD.ScheduleId And SE.StuEnrollId='" & StuEnrollId & "') ")
                End If
            End With
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "StudentAttendance")
            sb = sb.Remove(0, sb.Length)
            With sb
                'Code Updated by Vijay Ramteke on April 20, 2010 for Mantis Id 18850 to remove GSD.Grade='TC' condition as per discussion with Troy 
                '.Append(" Select R.Hours from arTransferGrades TG, arStuEnrollments SE, arGradeSystemDetails GSD, arReqs R Where TG.GrdSysDetailId=GSD.GrdSysDetailId and (GSD.IsTransferGrade=1 and GSD.Grade='TC') and TG.ReqId=R.ReqId and SE.StuEnrollId=TG.StuEnrollId and  SE.StuEnrollId='" & StuEnrollId & "' ")
                .Append(" Select R.Hours from arTransferGrades TG, arStuEnrollments SE, arGradeSystemDetails GSD, arReqs R Where TG.GrdSysDetailId=GSD.GrdSysDetailId and GSD.IsTransferGrade=1 and TG.ReqId=R.ReqId and SE.StuEnrollId=TG.StuEnrollId and  SE.StuEnrollId='" & StuEnrollId & "' ")
                'Code Updated by Vijay Ramteke on April 20, 2010 for Mantis Id 18850 to remove GSD.Grade='TC' condition as per discussion with Troy 
            End With
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "StudentTransferGrades")
            sb = sb.Remove(0, sb.Length)
            '''''
            Dim blSunday As Boolean = False
            Dim dbSunHours As Double = 0.0
            Dim blMonday As Boolean = False
            Dim dbMonHours As Double = 0.0
            Dim blTuesday As Boolean = False
            Dim dbTueHours As Double = 0.0
            Dim blWednesday As Boolean = False
            Dim dbWedHours As Double = 0.0
            Dim blThursday As Boolean = False
            Dim dbThuHours As Double = 0.0
            Dim blFriday As Boolean = False
            Dim dbFriHours As Double = 0.0
            Dim blSaturday As Boolean = False
            Dim dbSatHours As Double = 0.0

            For Each drTG As DataRow In ds.Tables("StudentTransferGrades").Rows
                TransfreGradeHours += Convert.ToDouble(drTG("Hours").ToString)
            Next

            If MyAdvAppSettings.AppSettings("IncludeTransferGradesInGradDateCalc").ToString.ToUpper = "YES" Then
                TotalPrgHours -= TransfreGradeHours
            End If

            If TotalPrgHours > 0 Then
                If (ds.Tables("StudentAttendance").Rows.Count > 0) Then
                    If Not (ds.Tables("StudentAttendance").Rows(0)("RecordDate").ToString + "") = "" Then
                        dtDate = Convert.ToDateTime(ds.Tables("StudentAttendance").Rows(0)("RecordDate").ToString).AddDays(1)
                        TotalPrgHours -= Convert.ToDecimal(ds.Tables("StudentAttendance").Rows(0)("ActualHours").ToString)
                    End If
                End If
            End If

            For Each drdw As DataRow In ds.Tables("WeekDays").Rows
                If drdw("dw").ToString = "0" Then
                    blSunday = True
                    dbSunHours = CType(drdw("total").ToString, Double)
                End If
                If drdw("dw").ToString = "1" Then
                    blMonday = True
                    dbMonHours = CType(drdw("total").ToString, Double)
                End If
                If drdw("dw").ToString = "2" Then
                    blTuesday = True
                    dbTueHours = CType(drdw("total").ToString, Double)
                End If
                If drdw("dw").ToString = "3" Then
                    blWednesday = True
                    dbWedHours = CType(drdw("total").ToString, Double)
                End If
                If drdw("dw").ToString = "4" Then
                    blThursday = True
                    dbThuHours = CType(drdw("total").ToString, Double)
                End If
                If drdw("dw").ToString = "5" Then
                    blFriday = True
                    dbFriHours = CType(drdw("total").ToString, Double)
                End If
                If drdw("dw").ToString = "6" Then
                    blSaturday = True
                    dbSatHours = CType(drdw("total").ToString, Double)
                End If
            Next

            If (dbSunHours + dbMonHours + dbTueHours + dbWedHours + dbThuHours + dbFriHours + dbSatHours) > 0 Then

                While (TotalPrgHours > 0)
                    Dim drSLOA() As DataRow 'Student LOAs
                    Dim drSS() As DataRow 'Student Suspensions
                    Dim drH() As DataRow 'Holiday
                    drSLOA = ds.Tables("StudentLOAs").Select("StartDate<='" & dtDate.ToString("MM/dd/yyyy") & "' AND EndDate>='" & dtDate.ToString("MM/dd/yyyy") & "' ")
                    drSS = ds.Tables("StudentSuspensions").Select("StartDate<='" & dtDate.ToString("MM/dd/yyyy") & "' AND EndDate>='" & dtDate.ToString("MM/dd/yyyy") & "' ")
                    drH = ds.Tables("Holidays").Select("HolidayStartDate<='" & dtDate.ToString("MM/dd/yyyy") & "' AND HolidayEndDate>='" & dtDate.ToString("MM/dd/yyyy") & "' ")
                    If drSLOA.Length = 0 And drSS.Length = 0 And drH.Length = 0 Then
                        If dtDate.DayOfWeek = 0 Then
                            If blSunday = True Then
                                dtDate = dtDate.AddDays(1)
                                TotalPrgHours -= dbSunHours
                            Else
                                dtDate = dtDate.AddDays(1)
                            End If
                        ElseIf dtDate.DayOfWeek = 1 Then
                            If blMonday = True Then
                                dtDate = dtDate.AddDays(1)
                                TotalPrgHours -= dbMonHours
                            Else
                                dtDate = dtDate.AddDays(1)
                            End If
                        ElseIf dtDate.DayOfWeek = 2 Then
                            If blTuesday = True Then
                                dtDate = dtDate.AddDays(1)
                                TotalPrgHours -= dbTueHours
                            Else
                                dtDate = dtDate.AddDays(1)
                            End If
                        ElseIf dtDate.DayOfWeek = 3 Then
                            If blWednesday = True Then
                                dtDate = dtDate.AddDays(1)
                                TotalPrgHours -= dbWedHours
                            Else
                                dtDate = dtDate.AddDays(1)
                            End If
                        ElseIf dtDate.DayOfWeek = 4 Then
                            If blThursday = True Then
                                dtDate = dtDate.AddDays(1)
                                TotalPrgHours -= dbThuHours
                            Else
                                dtDate = dtDate.AddDays(1)
                            End If
                        ElseIf dtDate.DayOfWeek = 5 Then
                            If blFriday = True Then
                                dtDate = dtDate.AddDays(1)
                                TotalPrgHours -= dbFriHours
                            Else
                                dtDate = dtDate.AddDays(1)
                            End If
                        ElseIf dtDate.DayOfWeek = 6 Then
                            If blSaturday = True Then
                                dtDate = dtDate.AddDays(1)
                                TotalPrgHours -= dbSatHours
                            Else
                                dtDate = dtDate.AddDays(1)
                            End If
                        End If
                    Else
                        dtDate = dtDate.AddDays(1)
                    End If
                End While
                '''''
            End If

            dtDate = dtDate.AddDays(-1)
            db.CloseConnection()
            db.Dispose()

            If dtDate.DayOfWeek = 0 Then
                dtDate = dtDate.AddDays(1)
            ElseIf dtDate.DayOfWeek = 6 Then
                dtDate = dtDate.AddDays(2)
            End If
        ElseIf UseTimeClock = False Then
            Msg = ""
            dtDate = DateTime.MaxValue
        Else
            Msg = "Expected Graduation Date cannot be calculated as Progarm Version Hours are Zero"
            dtDate = DateTime.MaxValue
        End If

        Return dtDate
    End Function
    Public Function GetDateFromHours(ByVal ProgramVersionId As String, ByVal StartDate As DateTime, ByVal ScheduleId As String, ByRef Msg As String, ByRef UseTimeClock As Boolean, ByVal CampusId As String) As DateTime
        Dim dtDate As DateTime = Nothing
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim da As System.Data.OleDb.OleDbDataAdapter
        Dim PrgVerId As String
        Dim TotalPrgHours As Double = 0.0
        Dim TransfreGradeHours As Double = 0.0
        db.OpenConnection()
        With sb
            '' Code changed by kamalesh ahuja on June 02 2010 to resolve mantis issue id 19089 and 19113
            ''.Append(" Select PV.Hours, CASE(UnitTypeDescrip) When 'Clock Hours'  Then 1 When 'Minutes' Then 1 Else 0 End As UseTimeClock from  arPrgVersions PV , arAttUnitType UT where PV.UnitTypeId=UT.UnitTypeId And  PV.PrgVerId='" & ProgramVersionId & "' ")
            ''Code Changed By Vijay Ramteke On June 17, 2010 For Mantis Id 19068
            ''.Append(" Select PV.Hours, CASE(ACDescrip) When 'Clock Hour'  Then 1 Else 0 End As UseTimeClock from  arPrgVersions PV , arPrograms P,syAcademicCalendars AC where PV.ProgId=P.ProgId and P.ACId=AC.ACId And  PV.PrgVerId='" & ProgramVersionId & "' ")
            .Append(" SELECT PV.Hours, CASE(SELECT ACDescrip FROM syAcademicCalendars AC WHERE P.ACId=AC.ACId) WHEN 'Clock Hour' THEN (CASE(SELECT UnitTypeDescrip FROM arAttUnitType UT WHERE PV.UnitTypeId=UT.UnitTypeId) WHEN 'Clock Hours' THEN 1 WHEN 'Minutes' THEN 1 ELSE 0 END) ELSE 0 END AS UseTimeClock FROM  arPrgVersions PV , arPrograms P WHERE PV.ProgId=P.ProgId AND PV.PrgVerId='" & ProgramVersionId & "' ")
            ''''''''''''''''''''''''''
        End With
        da = db.RunParamSQLDataAdapter(sb.ToString)
        da.Fill(ds, "ProgramHours")

        TotalPrgHours = Convert.ToDecimal(ds.Tables("ProgramHours").Rows(0)("Hours").ToString)
        UseTimeClock = CType(ds.Tables("ProgramHours").Rows(0)("UseTimeClock").ToString, Boolean)

        If Not TotalPrgHours = 0 And UseTimeClock = True Then
            dtDate = StartDate

            sb = sb.Remove(0, sb.Length)
            With sb
                .Append(" Select PS.ScheduleId, PSD.dw, PSD.total, (Select SUM(PSDI.total) From arProgScheduleDetails PSDI Where PSDI.ScheduleId=PS.ScheduleId) as HoursTotal  from arProgSchedules PS,  arProgScheduleDetails PSD  Where  PS.ScheduleId=PSD.ScheduleId And PSD.total>0  And PS.ScheduleId ='" & ScheduleId & "' Order by dw ")
            End With
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "WeekDays")
            sb = sb.Remove(0, sb.Length)
            With sb
                ''New Code Added By Vijay Ramteke On June 15, 2010 For Mantis Id 19167 
                ''.Append(" Select HolidayStartDate, HolidayEndDate, AllDay From syHolidays Order by HolidayStartDate ")
                '.Append(" Select HolidayStartDate, HolidayEndDate, AllDay ")
                '.Append(" FROM syHolidays H, syCampGrps CG, syCampuses C, syStatuses S ")
                '.Append(" WHERE H.CampGrpId=CG.CampGrpId AND C.CampusId=CG.CampusId  AND S.StatusId=H.StatusId AND S.Status='Active' ")
                '.Append(" AND C.CampusId='" + CampusId + "' ")
                .Append(" Select HolidayStartDate, HolidayEndDate, AllDay  FROM syHolidays H, syCmpGrpCmps t1, syCampGrps t2, syStatuses S ")
                .Append(" where t1.CampGrpId=t2.CampGrpId AND S.StatusId=H.StatusId AND S.Status='Active' ")
                .Append(" and H.CampGrpId=t2.CampGrpId and t1.CampusId='" + CampusId + "' ")
                ''New Code Added By Vijay Ramteke On June 15, 2010 For Mantis Id 19167
            End With
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "Holidays")
            sb = sb.Remove(0, sb.Length)
            '''''
            Dim blSunday As Boolean = False
            Dim dbSunHours As Double = 0.0
            Dim blMonday As Boolean = False
            Dim dbMonHours As Double = 0.0
            Dim blTuesday As Boolean = False
            Dim dbTueHours As Double = 0.0
            Dim blWednesday As Boolean = False
            Dim dbWedHours As Double = 0.0
            Dim blThursday As Boolean = False
            Dim dbThuHours As Double = 0.0
            Dim blFriday As Boolean = False
            Dim dbFriHours As Double = 0.0
            Dim blSaturday As Boolean = False
            Dim dbSatHours As Double = 0.0

            For Each drdw As DataRow In ds.Tables("WeekDays").Rows
                If drdw("dw").ToString = "0" Then
                    blSunday = True
                    dbSunHours = CType(drdw("total").ToString, Double)
                End If
                If drdw("dw").ToString = "1" Then
                    blMonday = True
                    dbMonHours = CType(drdw("total").ToString, Double)
                End If
                If drdw("dw").ToString = "2" Then
                    blTuesday = True
                    dbTueHours = CType(drdw("total").ToString, Double)
                End If
                If drdw("dw").ToString = "3" Then
                    blWednesday = True
                    dbWedHours = CType(drdw("total").ToString, Double)
                End If
                If drdw("dw").ToString = "4" Then
                    blThursday = True
                    dbThuHours = CType(drdw("total").ToString, Double)
                End If
                If drdw("dw").ToString = "5" Then
                    blFriday = True
                    dbFriHours = CType(drdw("total").ToString, Double)
                End If
                If drdw("dw").ToString = "6" Then
                    blSaturday = True
                    dbSatHours = CType(drdw("total").ToString, Double)
                End If
            Next

            If (dbSunHours + dbMonHours + dbTueHours + dbWedHours + dbThuHours + dbFriHours + dbSatHours) > 0 Then

                While (TotalPrgHours > 0)
                    Dim drH() As DataRow 'Holiday                   
                    drH = ds.Tables("Holidays").Select("HolidayStartDate<='" & dtDate.ToString("MM/dd/yyyy") & "' AND HolidayEndDate>='" & dtDate.ToString("MM/dd/yyyy") & "' ")
                    If drH.Length = 0 Then
                        If dtDate.DayOfWeek = 0 Then
                            If blSunday = True Then
                                dtDate = dtDate.AddDays(1)
                                TotalPrgHours -= dbSunHours
                            Else
                                dtDate = dtDate.AddDays(1)
                            End If
                        ElseIf dtDate.DayOfWeek = 1 Then
                            If blMonday = True Then
                                dtDate = dtDate.AddDays(1)
                                TotalPrgHours -= dbMonHours
                            Else
                                dtDate = dtDate.AddDays(1)
                            End If
                        ElseIf dtDate.DayOfWeek = 2 Then
                            If blTuesday = True Then
                                dtDate = dtDate.AddDays(1)
                                TotalPrgHours -= dbTueHours
                            Else
                                dtDate = dtDate.AddDays(1)
                            End If
                        ElseIf dtDate.DayOfWeek = 3 Then
                            If blWednesday = True Then
                                dtDate = dtDate.AddDays(1)
                                TotalPrgHours -= dbWedHours
                            Else
                                dtDate = dtDate.AddDays(1)
                            End If
                        ElseIf dtDate.DayOfWeek = 4 Then
                            If blThursday = True Then
                                dtDate = dtDate.AddDays(1)
                                TotalPrgHours -= dbThuHours
                            Else
                                dtDate = dtDate.AddDays(1)
                            End If
                        ElseIf dtDate.DayOfWeek = 5 Then
                            If blFriday = True Then
                                dtDate = dtDate.AddDays(1)
                                TotalPrgHours -= dbFriHours
                            Else
                                dtDate = dtDate.AddDays(1)
                            End If
                        ElseIf dtDate.DayOfWeek = 6 Then
                            If blSaturday = True Then
                                dtDate = dtDate.AddDays(1)
                                TotalPrgHours -= dbSatHours
                            Else
                                dtDate = dtDate.AddDays(1)
                            End If
                        End If
                    Else
                        dtDate = dtDate.AddDays(1)
                    End If
                End While
                '''''
            End If

            dtDate = dtDate.AddDays(-1)
            db.CloseConnection()
            db.Dispose()

            If dtDate.DayOfWeek = 0 Then
                dtDate = dtDate.AddDays(1)
            ElseIf dtDate.DayOfWeek = 6 Then
                dtDate = dtDate.AddDays(2)
            End If
        ElseIf UseTimeClock = False Then
            Msg = ""
            dtDate = DateTime.MaxValue
        Else
            Msg = "Expected Graduation Date cannot be calculated as Program Version Hours are Zero"
            dtDate = DateTime.MaxValue
        End If

        Return dtDate
    End Function
End Class
