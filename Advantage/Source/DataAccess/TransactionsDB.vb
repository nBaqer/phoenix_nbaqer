Imports System
Imports System.Data
Imports FAME.Advantage.Common
Imports System.Data.SqlClient

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' TransactionsDB.vb
'
' TransactionsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Imports System.Globalization

Public Class TransactionsDB
    Public Function GetPostChargeInfo(ByVal postChargeId As String) As PostChargeInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT TransactionId As PostChargeId, ")
            .Append("    (Select StudentId from arStuEnrollments where StuEnrollId=T.StuEnrollId) StudentId, ")
            .Append("    (Select (FirstName + ' ' + LastName) As StudentName from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=T.StuEnrollId)) StudentName, ")
            .Append("    T.StuEnrollId, ")
            .Append("    (Select PrgVerDescrip from arPrgVersions where PrgVerId=(select PrgVerId from arStuEnrollments where StuEnrollId=T.StuEnrollId)) EnrollmentDescrip, ")
            .Append("    T.TransCodeId, ")
            .Append("    (Select TransCodeDescrip from saTransCodes where TransCodeId=T.TransCodeId) TransCodeDescrip, ")
            .Append("    T.TransDescrip, ")
            .Append("    T.TransDate, ")
            .Append("    T.TransAmount, ")
            .Append("    T.TransReference, ")
            .Append("    T.AcademicYearId, ")
            .Append("    (Select AcademicYearDescrip from saAcademicYears where AcademicYearId=T.AcademicYearId) AcademicYearDescrip, ")
            .Append("    T.TermId, ")
            .Append("    (Select TermDescrip from arTerm where TermId=T.TermId) TermDescrip, ")
            .Append("    (Select StartDate from arTerm where TermId=T.TermId) EarliestAllowedDate, ")
            .Append("    (Select EndDate from arTerm where TermId=T.TermId) LatestAllowedDate, ")
            .Append("    T.TransTypeId, ")
            .Append("    T.ModUser, ")
            .Append("    T.ModDate ")
            ''Added by Saraswathi lakshmanan on may 18 2009 to get the fee levelid
            .Append("   ,T.FeeLevelId ")
            .Append("FROM  saTransactions T ")
            .Append("WHERE T.TransactionId= ? ")
            .Append("AND T.Voided=0 ")

        End With

        ' Add the PostChargeId to the parameter list
        db.AddParameter("@TransactionId", postChargeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim PostChargeInfo As New PostChargeInfo

        While dr.Read()

            '   set properties with data from DataReader
            With PostChargeInfo
                .IsInDB = True
                .PostChargeId = postChargeId
                .StudentId = CType(dr("StudentId"), Guid).ToString
                .StudentName = dr("StudentName")
                .StuEnrollId = CType(dr("StuEnrollId"), Guid).ToString
                .EnrollmentDescription = dr("EnrollmentDescrip")
                If Not dr("TransCodeId") Is DBNull.Value Then .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                If Not dr("TransCodeDescrip") Is DBNull.Value Then .TransCodeDescription = dr("TransCodeDescrip")
                .PostChargeDescription = dr("TransDescrip")
                .PostChargeDate = dr("TransDate")
                .Amount = dr("TransAmount")
                If Not dr("TransReference") Is DBNull.Value Then .Reference = dr("TransReference")
                If Not dr("AcademicYearId") Is DBNull.Value Then .AcademicYearId = CType(dr("AcademicYearId"), Guid).ToString
                If Not dr("AcademicYearDescrip") Is DBNull.Value Then .AcademicYearDescrip = dr("AcademicYearDescrip")
                If Not dr("TermId") Is DBNull.Value Then .TermId = CType(dr("TermId"), Guid).ToString
                If Not dr("TermDescrip") Is DBNull.Value Then .TermDescrip = dr("TermDescrip")
                If Not dr("EarliestAllowedDate") Is DBNull.Value Then .EarliestAllowedDate = dr("EarliestAllowedDate")
                If Not dr("LatestAllowedDate") Is DBNull.Value Then .LatestAllowedDate = dr("LatestAllowedDate")
                .TransTypeId = dr("TransTypeId")
                If Not dr("ModUser") Is DBNull.Value Then .ModUser = dr("ModUser")
                If Not dr("ModDate") Is DBNull.Value Then .ModDate = dr("ModDate")
                ''Added by Saraswathi lakshmanan on may 18 2009
                If Not dr("FeeLevelId") Is DBNull.Value Then .FeeLevelId = dr("FeeLevelId")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return PostChargeInfo

    End Function
    Public Function UpdatePostChargeInfo(ByVal PostChargeInfo As PostChargeInfo, ByVal user As String) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   Connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saTransactions Set StuEnrollId = ?, TermId = ?, ")
                .Append("   TransDate = ?, TransCodeId = ?, TransReference = ?, TransDescrip = ?, ")
                .Append("   AcademicYearId = ?, TransAmount = ?, TransTypeId = ?, IsPosted = ?, ")
                .Append("   ModUser = ?, ModDate = ?, FeeLevelId = ? ,PrgChrPeriodSeqId=? ")
                .Append("WHERE TransactionId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saTransactions where ModDate = ? ")
                .Append("and saTransactions.Voided=0 ")

            End With

            '   add parameters values to the query

            '   StuEnrollId
            db.AddParameter("@StuEnrollId", PostChargeInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TermId
            If PostChargeInfo.TermId = Guid.Empty.ToString Then
                db.AddParameter("@TermId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TermId", PostChargeInfo.TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Date
            db.AddParameter("@TransDate", PostChargeInfo.PostChargeDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TransCodeId
            db.AddParameter("@TransCodeId", PostChargeInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Transaction Reference
            db.AddParameter("@TransReference", PostChargeInfo.Reference, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Transaction Description
            db.AddParameter("@TransDescrip", PostChargeInfo.PostChargeDescription, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   AcademicYearId
            If PostChargeInfo.AcademicYearId = Guid.Empty.ToString Then
                db.AddParameter("@AcademicYearId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AcademicYearId", PostChargeInfo.AcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Amount
            db.AddParameter("@TransAmount", PostChargeInfo.Amount, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

            '   TransTypeId
            db.AddParameter("@TransTypeId", PostChargeInfo.TransTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   IsPosted
            db.AddParameter("@IsPosted", PostChargeInfo.IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   PostChargeId
            db.AddParameter("@TransactionId", PostChargeInfo.PostChargeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", PostChargeInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   FeeLevelId
            db.AddParameter("@FeeLevelId", PostChargeInfo.FeeLevelId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            ''Added by Saraswathi Lakshmanan to save the ChargePeriod Sequence on May 28 2010
            If PostChargeInfo.StuEnrollPayPeriodId = Guid.Empty.ToString Then
                db.AddParameter("@ChargeperiodSeq", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@ChargeperiodSeq", PostChargeInfo.StuEnrollPayPeriodId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddReversedPostChargeInfo(ByVal PostChargeInfo As PostChargeInfo, ByVal user As String) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   Connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saTransactions (TransactionId, StuEnrollId, CampusId, TermId, ")
                .Append("   TransDate, TransCodeId, TransReference, TransDescrip, AcademicYearId, ")
                .Append("   TransAmount, TransTypeId, IsPosted, ")
                .Append("   CreateDate, ModUser, ModDate, FeeLevelId) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   PostChargeId
            db.AddParameter("@TransactionId", PostChargeInfo.PostChargeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StuEnrollId
            db.AddParameter("@StuEnrollId", PostChargeInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampusId
            If PostChargeInfo.CampusId = Guid.Empty.ToString Then
                db.AddParameter("@CampusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CampusId", PostChargeInfo.CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   TermId
            If PostChargeInfo.TermId = Guid.Empty.ToString Then
                db.AddParameter("@TermId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TermId", PostChargeInfo.TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Date
            db.AddParameter("@TransDate", PostChargeInfo.PostChargeDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TransCodeId
            If PostChargeInfo.TransCodeId = Guid.Empty.ToString Then
                db.AddParameter("@TransCodeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TransCodeId", PostChargeInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Reference
            db.AddParameter("@TransReference", PostChargeInfo.Reference, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Transaction Description
            db.AddParameter("@TransDescrip", PostChargeInfo.PostChargeDescription, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   AcademicYearId
            If PostChargeInfo.AcademicYearId = Guid.Empty.ToString Then
                db.AddParameter("@AcademicYearId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AcademicYearId", PostChargeInfo.AcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Amount
            db.AddParameter("@TransAmount", PostChargeInfo.Amount, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

            '   TransTypeId
            db.AddParameter("@TransTypeId", PostChargeInfo.TransTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   IsPosted
            db.AddParameter("@IsPosted", PostChargeInfo.IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   create timestamp
            Dim rightNow As DateTime = Date.Now

            '   CreateDate
            db.AddParameter("@CreateDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   FeeLevelId
            db.AddParameter("@FeeLevelId", PostChargeInfo.FeeLevelId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   add ReversedTransaction
            sb = New StringBuilder
            With sb
                .Append("INSERT saReversedTransactions (TransactionId, ReversedTransactionId, ")
                .Append("   ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?) ")
            End With

            '   add parameters values to the query
            db.ClearParameters()

            '   PostChargeId
            db.AddParameter("@TransactionId", PostChargeInfo.PostChargeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   OriginalTransactionId
            db.AddParameter("@OriginalTransactionId", PostChargeInfo.OriginalTransactionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   commit transaction 
            groupTrans.Commit()

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Catch ex As Exception

            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return an error to the client   
            Return ex.Message()

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddPostChargeInfo(ByVal PostChargeInfo As PostChargeInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saTransactions (TransactionId, StuEnrollId, CampusId, TermId, ")
                .Append("   TransDate, TransCodeId, TransReference, TransDescrip, AcademicYearId, ")
                .Append("   TransAmount, TransTypeId, IsPosted, ")
                ''Charge Period Sequence ID added by Saraswathi lakshmanan on may 28 2010
                .Append("   CreateDate, ModUser, ModDate, FeeLevelId,StuEnrollPayPeriodId) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   PostChargeId
            db.AddParameter("@TransactionId", PostChargeInfo.PostChargeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StuEnrollId
            db.AddParameter("@StuEnrollId", PostChargeInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampusId
            If PostChargeInfo.CampusId = Guid.Empty.ToString Then
                db.AddParameter("@CampusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CampusId", PostChargeInfo.CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   TermId
            If PostChargeInfo.TermId = Guid.Empty.ToString Then
                db.AddParameter("@TermId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TermId", PostChargeInfo.TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Date
            db.AddParameter("@TransDate", PostChargeInfo.PostChargeDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TransCodeId
            If PostChargeInfo.TransCodeId = Guid.Empty.ToString Then
                db.AddParameter("@TransCodeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TransCodeId", PostChargeInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Reference
            db.AddParameter("@TransReference", PostChargeInfo.Reference, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Transaction Description
            db.AddParameter("@TransDescrip", PostChargeInfo.PostChargeDescription, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   AcademicYearId
            If PostChargeInfo.AcademicYearId = Guid.Empty.ToString Then
                db.AddParameter("@AcademicYearId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AcademicYearId", PostChargeInfo.AcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Amount
            db.AddParameter("@TransAmount", PostChargeInfo.Amount, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

            '   TransTypeId
            db.AddParameter("@TransTypeId", PostChargeInfo.TransTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   IsPosted
            db.AddParameter("@IsPosted", PostChargeInfo.IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   create timestamp
            Dim rightNow As DateTime = Date.Now

            '   CreateDate
            db.AddParameter("@CreateDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   FeeLevelId
            db.AddParameter("@FeeLevelId", PostChargeInfo.FeeLevelId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            If PostChargeInfo.StuEnrollPayPeriodId = Guid.Empty.ToString Then
                db.AddParameter("@ChargeperiodSeq", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@ChargeperiodSeq", PostChargeInfo.StuEnrollPayPeriodId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Dim PostCharge As New PostChargeInfo
            PostCharge = GetPostChargeInfo(PostChargeInfo.PostChargeId)

            If PostCharge.PostChargeId <> PostChargeInfo.PostChargeId Then Return "Cannot save the transaction since a similar transaction has already been posted for the same day."

            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeletePostChargeInfo(ByVal PostChargeId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saTransactions ")
                .Append("WHERE TransactionId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("Select count (*) from saTransactions where TransactionId = ? ")
            End With

            '   add parameters values to the query

            '   TransactionId
            db.AddParameter("@TransactionId", PostChargeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TransactionId
            db.AddParameter("@TransactionId", PostChargeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetPostPaymentInfo(ByVal postPaymentId As String) As PostPaymentInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT /*TransactionId As PostPaymentId,*/ ")
            .Append("    (Select StudentId from arStuEnrollments where StuEnrollId=T.StuEnrollId) StudentId, ")
            '.Append("    (Select (FirstName + ' ' + LastName) As StudentName from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=T.StuEnrollId)) StudentName, ")
            ''Modified by Saraswathi lakshmanan on Sept 18 2009
            ''To fix mantis case: 17600: QA: There is a difference in the payment receipt on the post payments page and transaction search page. 
            .Append("    (Select (LastName + ', ' + FirstName) As StudentName from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=T.StuEnrollId)) StudentName, ")
            .Append("    T.StuEnrollId, ")
            .Append("    (Select PrgVerDescrip from arPrgVersions where PrgVerId=(select PrgVerId from arStuEnrollments where StuEnrollId=T.StuEnrollId)) as EnrollmentDescrip, ")
            .Append("    T.TransCodeId, ")
            .Append("    (Select TransCodeDescrip from saTransCodes where TransCodeId=T.TransCodeId) As TransCodeDescrip, ")
            .Append("    T.TransDescrip, ")
            .Append("    T.TransDate, ")
            .Append("    T.TransAmount, ")
            .Append("    T.TransReference, ")
            .Append("    T.AcademicYearId, ")
            .Append("    (Select AcademicYearDescrip from saAcademicYears where AcademicYearId=T.AcademicYearId) As AcademicYearDescrip, ")
            .Append("    T.TermId, ")
            .Append("    (Select TermDescrip from arTerm where TermId=T.TermId) As TermDescrip, ")
            .Append("    (Select StartDate from arTerm where TermId=T.TermId) as EarliestAllowedDate, ")
            .Append("    (Select EndDate from arTerm where TermId=T.TermId) as LatestAllowedDate, ")
            .Append("    T.TransTypeId, ")
            .Append("    T.IsPosted, ")
            .Append("    P.PaymentTypeId, ")
            .Append("    P.CheckNumber, ")
            .Append("    P.ScheduledPayment, ")
            '.Append("    P.StudentAwardId, ")
            '.Append("    (Select AwardTypeDescrip from faAwardTypes where AwardTypeId=(Select AwardTypeId from faStudentAwards where StudentAwardId=P.StudentAwardId)) + '/' + ")
            '.Append("    (Select FundSourceDescrip from saFundSources where FundSourceId=(Select AwardTypeId from faStudentAwards where StudentAwardId=P.StudentAwardId)) + '/' + ")
            '.Append("    (Select AwardYearDescrip from faAwardYears where AwardYearId=(Select AwardYearId from faStudentAwards where StudentAwardId=P.StudentAwardId)) + '/' + ")
            '.Append("    (Select AcademicYearDescrip from saAcademicYears where AcademicYearId=(Select AcademicYearId from faStudentAwards where StudentAwardId=P.StudentAwardId)) + '/' + ")
            '.Append("    (Select Cast(GrossAmount as varchar(10)) from faStudentAwards where StudentAwardId=P.StudentAwardId) As AwardDescrip, ")
            .Append("    P.BankAcctId, ")
            .Append("    (Select BankAcctDescrip from saBankAccounts where BankAcctId=P.BankAcctId) As BankAcctDescrip, ")
            .Append("    T.ModUser, ")
            .Append("    T.ModDate, ")
            .Append("    P.ModUser as ModUser1, ")
            .Append("    P.ModDate as ModDate1 ")
            .Append("FROM  saTransactions T, saPayments P ")
            .Append("WHERE T.TransactionId=P.TransactionId ")
            .Append(" AND  T.TransactionId= ? ")
            .Append(" AND T.Voided=0 ")
        End With

        ' Add the PostPaymentId to the parameter list
        db.AddParameter("@TransactionId", postPaymentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim PostPaymentInfo As New PostPaymentInfo

        While dr.Read()

            '   set properties with data from DataReader
            With PostPaymentInfo
                .IsInDB = True
                .PostPaymentId = postPaymentId
                .StudentId = CType(dr("StudentId"), Guid).ToString
                .StudentName = dr("StudentName")
                .StuEnrollId = CType(dr("StuEnrollId"), Guid).ToString
                .EnrollmentDescription = dr("EnrollmentDescrip")
                If Not dr("TransCodeId") Is DBNull.Value Then .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                If Not dr("TransCodeDescrip") Is DBNull.Value Then .TransCodeDescription = dr("TransCodeDescrip")
                .PostPaymentDescription = dr("TransDescrip")
                .PostPaymentDate = dr("TransDate")
                .Amount = -dr("TransAmount")
                If Not dr("TransReference") Is DBNull.Value Then .Reference = dr("TransReference")
                If Not dr("AcademicYearId") Is DBNull.Value Then .AcademicYearId = CType(dr("AcademicYearId"), Guid).ToString
                If Not dr("AcademicYearDescrip") Is DBNull.Value Then .AcademicYearDescrip = dr("AcademicYearDescrip")
                If Not dr("TermId") Is DBNull.Value Then .TermId = CType(dr("TermId"), Guid).ToString
                If Not dr("TermDescrip") Is DBNull.Value Then .TermDescrip = dr("TermDescrip")
                If Not dr("EarliestAllowedDate") Is DBNull.Value Then .EarliestAllowedDate = dr("EarliestAllowedDate")
                If Not dr("LatestAllowedDate") Is DBNull.Value Then .LatestAllowedDate = dr("LatestAllowedDate")
                .TransTypeId = dr("TransTypeId")
                .IsPosted = dr("IsPosted")
                .PaymentTypeId = dr("PaymentTypeId")
                If Not dr("CheckNumber") Is DBNull.Value Then .CheckNumber = dr("CheckNumber")
                .ScheduledPayment = dr("ScheduledPayment")
                'If Not dr("StudentAwardId") Is System.DBNull.Value Then .StudentAwardId = CType(dr("AwardTypeId"), Guid).ToString
                'If Not dr("AwardDescrip") Is System.DBNull.Value Then .AwardDescrip = dr("AwardDescrip")
                If Not dr("BankAcctId") Is DBNull.Value Then .BankAcctId = CType(dr("BankAcctId"), Guid).ToString
                If Not dr("BankAcctDescrip") Is DBNull.Value Then .BankAcctDescrip = dr("BankAcctDescrip")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
                .ModUser1 = dr("ModUser1")
                .ModDate1 = dr("ModDate1")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return PostPaymentInfo

    End Function
    ''Added by Saraswathi Lakshmanan on April 13 2010
    ''Converted to stored Procedure
    Public Function GetPostPaymentInfo_sp(ByVal postPaymentId As String) As PostPaymentInfo

        '   connect to the database
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder


        ' Add the PostPaymentId to the parameter list
        db.AddParameter("@TransactionId", New Guid(postPaymentId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As SqlDataReader = db.RunParamSQLDataReader_SP("USP_SA_GetPostPaymentInfo")

        Dim PostPaymentInfo As New PostPaymentInfo

        While dr.Read()

            '   set properties with data from DataReader
            With PostPaymentInfo
                .IsInDB = True
                .PostPaymentId = postPaymentId
                .StudentId = CType(dr("StudentId"), Guid).ToString
                .StudentName = dr("StudentName")
                .StuEnrollId = CType(dr("StuEnrollId"), Guid).ToString
                .EnrollmentDescription = dr("EnrollmentDescrip")
                If Not dr("TransCodeId") Is DBNull.Value Then .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                If Not dr("TransCodeDescrip") Is DBNull.Value Then .TransCodeDescription = dr("TransCodeDescrip")
                .PostPaymentDescription = dr("TransDescrip")
                .PostPaymentDate = dr("TransDate")
                .Amount = -dr("TransAmount")
                If Not dr("TransReference") Is DBNull.Value Then .Reference = dr("TransReference")
                If Not dr("AcademicYearId") Is DBNull.Value Then .AcademicYearId = CType(dr("AcademicYearId"), Guid).ToString
                If Not dr("AcademicYearDescrip") Is DBNull.Value Then .AcademicYearDescrip = dr("AcademicYearDescrip")
                If Not dr("TermId") Is DBNull.Value Then .TermId = CType(dr("TermId"), Guid).ToString
                If Not dr("TermDescrip") Is DBNull.Value Then .TermDescrip = dr("TermDescrip")
                If Not dr("EarliestAllowedDate") Is DBNull.Value Then .EarliestAllowedDate = dr("EarliestAllowedDate")
                If Not dr("LatestAllowedDate") Is DBNull.Value Then .LatestAllowedDate = dr("LatestAllowedDate")
                .TransTypeId = dr("TransTypeId")
                .IsPosted = dr("IsPosted")
                .PaymentTypeId = dr("PaymentTypeId")
                If Not dr("CheckNumber") Is DBNull.Value Then .CheckNumber = dr("CheckNumber")
                .ScheduledPayment = dr("ScheduledPayment")
                'If Not dr("StudentAwardId") Is System.DBNull.Value Then .StudentAwardId = CType(dr("AwardTypeId"), Guid).ToString
                'If Not dr("AwardDescrip") Is System.DBNull.Value Then .AwardDescrip = dr("AwardDescrip")
                If Not dr("BankAcctId") Is DBNull.Value Then .BankAcctId = CType(dr("BankAcctId"), Guid).ToString
                If Not dr("BankAcctDescrip") Is DBNull.Value Then .BankAcctDescrip = dr("BankAcctDescrip")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
                .ModUser1 = dr("ModUser1")
                .ModDate1 = dr("ModDate1")
            End With

        End While

        'Close Connection
        db.CloseConnection()

        '   Return BankInfo
        Return PostPaymentInfo

    End Function

    Public Function UpdatePostPaymentInfo(ByVal PostPaymentInfo As PostPaymentInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saTransactions Set StuEnrollId = ?, TermId = ?, ")
                .Append("   TransDate = ?, TransCodeId = ?, TransReference = ?, TransDescrip = ?, ")
                .Append("   AcademicYearId = ?, TransAmount = ?, TransTypeId = ?, IsPosted = ?, ")
                .Append("   ModUser = ?, ModDate = ? ")
                .Append("WHERE TransactionId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saTransactions where ModDate = ? ")

            End With

            '   add parameters values to the query
            '   StuEnrollId
            db.AddParameter("@StuEnrollId", PostPaymentInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TermId
            If PostPaymentInfo.TermId = Guid.Empty.ToString Then
                db.AddParameter("@TermId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TermId", PostPaymentInfo.TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Date
            db.AddParameter("@TransDate", PostPaymentInfo.PostPaymentDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TransCodeId
            If PostPaymentInfo.TransCodeId = Guid.Empty.ToString Then
                db.AddParameter("@TransCodeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TransCodeId", PostPaymentInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Reference
            db.AddParameter("@TransReference", PostPaymentInfo.Reference, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Transaction Description
            db.AddParameter("@TransDescrip", PostPaymentInfo.PostPaymentDescription, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   AcademicYearId
            If PostPaymentInfo.AcademicYearId = Guid.Empty.ToString Then
                db.AddParameter("@AcademicYearId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AcademicYearId", PostPaymentInfo.AcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Amount
            db.AddParameter("@TransAmount", PostPaymentInfo.Amount, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

            '   TransTypeId
            db.AddParameter("@TransTypeId", PostPaymentInfo.TransTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   IsPosted
            db.AddParameter("@IsPosted", PostPaymentInfo.IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   PostPaymentId
            db.AddParameter("@TransactionId", PostPaymentInfo.PostPaymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", PostPaymentInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString, groupTrans)
            'Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If Not (rowCount = 1) Then

                '   rollback transaction
                groupTrans.Rollback()

                '   return an error message
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

            '
            '   update payments table
            '

            '   build query
            sb = New StringBuilder
            With sb
                .Append("UPDATE saPayments Set PaymentTypeId = ?, CheckNumber = ?, ")
                .Append("   ScheduledPayment = ?, AwardTypeId = ?, BankAcctId = ?, IsDeposited, ")
                .Append("   ModUser = ?, ModDate = ? ")
                .Append("WHERE TransactionId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saPayments where ModDate = ? ")
            End With

            '   add parameters values to the query. Clear previous list
            db.ClearParameters()

            '   PaymentTypeId
            db.AddParameter("@PaymentTypeId", PostPaymentInfo.PaymentTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CheckNumber
            If PostPaymentInfo.CheckNumber = "" Then
                db.AddParameter("@CheckNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CheckNumber", PostPaymentInfo.CheckNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ScheduledPayment
            db.AddParameter("@ScheduledPayment", PostPaymentInfo.ScheduledPayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   AwardTypeId
            'If PostPaymentInfo.StudentAwardId = Guid.Empty.ToString Then
            '    db.AddParameter("@StudentAwardId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'Else
            '    db.AddParameter("@StudentAwardId", PostPaymentInfo.StudentAwardId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'End If

            '   BankAcctId
            If PostPaymentInfo.BankAcctId = Guid.Empty.ToString Then
                db.AddParameter("@BankAcctId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@BankAcctId", PostPaymentInfo.BankAcctId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   IsDeposited
            db.AddParameter("@IsDeposited", PostPaymentInfo.IsDeposited, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   PostPaymentId
            db.AddParameter("@TransactionId", PostPaymentInfo.PostPaymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", PostPaymentInfo.ModDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString, groupTrans)
            'rowCount = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   commit transaction 
                groupTrans.Commit()
                '   return without errors
                Return ""
            Else
                '   rollback transaction if there were errors
                groupTrans.Rollback()

                '   return an error message
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()
            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            End If
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddReversedPostPaymentInfo(ByVal PostPaymentInfo As PostPaymentInfo, ByVal user As String) As String
        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   do an insert
        Try

            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saTransactions (TransactionId, StuEnrollId, CampusId, TermId, ")
                .Append("   TransDate, TransCodeId, TransReference, TransDescrip, AcademicYearId, ")
                .Append("   TransAmount, TransTypeId, IsPosted, ")
                .Append("   CreateDate, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   PostPaymentId
            db.AddParameter("@TransactionId", PostPaymentInfo.PostPaymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StuEnrollId
            db.AddParameter("@StuEnrollId", PostPaymentInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampusId
            If PostPaymentInfo.CampusId = Guid.Empty.ToString Then
                db.AddParameter("@CampusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CampusId", PostPaymentInfo.CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   TermId
            If PostPaymentInfo.TermId = Guid.Empty.ToString Then
                db.AddParameter("@TermId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TermId", PostPaymentInfo.TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Date
            db.AddParameter("@TransDate", PostPaymentInfo.PostPaymentDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TransCodeId
            If PostPaymentInfo.TransCodeId = Guid.Empty.ToString Then
                db.AddParameter("@TransCodeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TransCodeId", PostPaymentInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Reference
            db.AddParameter("@TransReference", PostPaymentInfo.Reference, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Transaction Description
            db.AddParameter("@TransDescrip", PostPaymentInfo.PostPaymentDescription, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   AcademicYearId
            If PostPaymentInfo.AcademicYearId = Guid.Empty.ToString Then
                db.AddParameter("@AcademicYearId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AcademicYearId", PostPaymentInfo.AcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Amount
            db.AddParameter("@TransAmount", PostPaymentInfo.Amount, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

            '   TransTypeId
            db.AddParameter("@TransTypeId", PostPaymentInfo.TransTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   IsPosted
            db.AddParameter("@IsPosted", PostPaymentInfo.IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   create timestamp
            Dim rightNow As DateTime = Date.Now

            '   CreateDate
            db.AddParameter("@CreateDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   add payment
            '   build the query
            sb = New StringBuilder
            With sb
                .Append("INSERT saPayments (TransactionId, PaymentTypeId, ")
                '.Append("   CheckNumber, ScheduledPayment, StudentAwardId, BankAcctId, IsDeposited, ")
                'Modified by Michelle R. Rodriguez on 03/29/2005
                .Append("   CheckNumber, ScheduledPayment, BankAcctId, IsDeposited, ")
                .Append("   ModUser, ModDate) ")
                '.Append("VALUES (?,?,?,?,?,?,?,?,?,?) ")
                .Append("VALUES (?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query
            db.ClearParameters()

            '   PostPaymentId
            db.AddParameter("@TransactionId", PostPaymentInfo.PostPaymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   PaymentTypeId
            db.AddParameter("@PaymentTypeId", PostPaymentInfo.PaymentTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CheckNumber
            If PostPaymentInfo.CheckNumber = "" Then
                db.AddParameter("@CheckNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CheckNumber", PostPaymentInfo.CheckNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ScheduledPayment
            db.AddParameter("@ScheduledPayment", PostPaymentInfo.ScheduledPayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   AwardTypeId
            'If PostPaymentInfo.StudentAwardId = Guid.Empty.ToString Then
            '    db.AddParameter("@StudentAwardId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'Else
            '    db.AddParameter("@StudentAwardId", PostPaymentInfo.StudentAwardId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'End If

            '   BankAcctId
            If PostPaymentInfo.BankAcctId = Guid.Empty.ToString Then
                db.AddParameter("@BankAcctId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@BankAcctId", PostPaymentInfo.BankAcctId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   IsDeposited
            db.AddParameter("@IsDeposited", PostPaymentInfo.IsDeposited, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   add ReversedTransaction
            sb = New StringBuilder
            With sb
                .Append("INSERT saReversedTransactions (TransactionId, ReversedTransactionId, ")
                .Append("   ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?) ")
            End With

            '   add parameters values to the query
            db.ClearParameters()

            '   PostPaymentId
            db.AddParameter("@TransactionId", PostPaymentInfo.PostPaymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   OriginalTransactionId
            db.AddParameter("@OriginalTransactionId", PostPaymentInfo.OriginalTransactionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            'delete transactions from saPmtDisbRel
            sb = New StringBuilder
            With sb
                .Append("DELETE from saPmtDisbRel where TransactionId = ? ")
            End With

            '   add parameters values to the query
            db.ClearParameters()

            '   PostPaymentId
            db.AddParameter("@TransactionId", PostPaymentInfo.OriginalTransactionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            'delete transactions from saAppliedPayments
            sb = New StringBuilder
            With sb
                .Append("DELETE from saAppliedPayments where TransactionId = ? ")
            End With

            '   add parameters values to the query
            db.ClearParameters()

            '   PostPaymentId
            db.AddParameter("@TransactionId", PostPaymentInfo.OriginalTransactionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   commit transaction 
            groupTrans.Commit()

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Catch ex As Exception

            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return an error to the client   
            Return ex.Message()

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function AddPostPaymentInfo(ByVal PostPaymentInfo As PostPaymentInfo, ByVal user As String, ByVal ScheduledDisb As ArrayList, Optional ByVal appPmtColl As ArrayList = Nothing) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saTransactions (TransactionId, StuEnrollId, CampusId, TermId, ")
                .Append("   TransDate, TransCodeId, TransReference, TransDescrip, AcademicYearId, ")
                .Append("   TransAmount, TransTypeId, IsPosted, ")
                ''PaymentCode added to satransactions Table
                ''For mantis Id 15222
                .Append("   PaymentCodeId,  ")
                .Append("   FundSourceId,  ")
                .Append("   CreateDate, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   PostPaymentId
            db.AddParameter("@TransactionId", PostPaymentInfo.PostPaymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StuEnrollId
            db.AddParameter("@StuEnrollId", PostPaymentInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampusId
            If PostPaymentInfo.CampusId = Guid.Empty.ToString Then
                db.AddParameter("@CampusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CampusId", PostPaymentInfo.CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   TermId
            If PostPaymentInfo.TermId = Guid.Empty.ToString Then
                db.AddParameter("@TermId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TermId", PostPaymentInfo.TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Date
            db.AddParameter("@TransDate", PostPaymentInfo.PostPaymentDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TransCodeId
            If PostPaymentInfo.TransCodeId = Guid.Empty.ToString Then
                db.AddParameter("@TransCodeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TransCodeId", PostPaymentInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If




            '   Transaction Reference
            db.AddParameter("@TransReference", PostPaymentInfo.Reference, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Transaction Description
            db.AddParameter("@TransDescrip", PostPaymentInfo.PostPaymentDescription, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   AcademicYearId
            If PostPaymentInfo.AcademicYearId = Guid.Empty.ToString Then
                db.AddParameter("@AcademicYearId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AcademicYearId", PostPaymentInfo.AcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Amount
            db.AddParameter("@TransAmount", PostPaymentInfo.Amount, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

            '   TransTypeId
            db.AddParameter("@TransTypeId", PostPaymentInfo.TransTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   IsPosted
            db.AddParameter("@IsPosted", PostPaymentInfo.IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            ''Added by Saraswathi lakshmanan on April 16 2010
            ''For mantis case 15222
            If PostPaymentInfo.PaymentCodeID = Guid.Empty.ToString Then
                db.AddParameter("@PaymentCodeID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PaymentCodeID", PostPaymentInfo.PaymentCodeID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            If PostPaymentInfo.FundSourceID = Guid.Empty.ToString Then
                db.AddParameter("@FundSourceID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@FundSourceID", PostPaymentInfo.FundSourceID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   create timestamp
            Dim rightNow As DateTime = Date.Now

            '   CreateDate
            db.AddParameter("@CreateDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   add payment
            '   build the query
            sb = New StringBuilder
            With sb
                .Append("INSERT saPayments (TransactionId, PaymentTypeId, ")
                '.Append("   CheckNumber, ScheduledPayment, StudentAwardId, BankAcctId, IsDeposited, ")
                'Modified by Michelle R. Rodriguez on 03/29/2005
                .Append("   CheckNumber, ScheduledPayment, BankAcctId, IsDeposited, ")
                .Append("   ModUser, ModDate) ")
                '.Append("VALUES (?,?,?,?,?,?,?,?,?,?) ")
                .Append("VALUES (?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query
            db.ClearParameters()

            '   PostPaymentId
            db.AddParameter("@TransactionId", PostPaymentInfo.PostPaymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   PaymentTypeId
            db.AddParameter("@PaymentTypeId", PostPaymentInfo.PaymentTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CheckNumber
            If PostPaymentInfo.CheckNumber = "" Then
                db.AddParameter("@CheckNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CheckNumber", PostPaymentInfo.CheckNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ScheduledPayment
            db.AddParameter("@ScheduledPayment", PostPaymentInfo.ScheduledPayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   AwardTypeId
            'If PostPaymentInfo.StudentAwardId = Guid.Empty.ToString Then
            '    db.AddParameter("@StudentAwardId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'Else
            '    db.AddParameter("@StudentAwardId", PostPaymentInfo.StudentAwardId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'End If

            '   BankAcctId
            If PostPaymentInfo.BankAcctId = Guid.Empty.ToString Then
                db.AddParameter("@BankAcctId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@BankAcctId", PostPaymentInfo.BankAcctId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   IsDeposited
            db.AddParameter("@IsDeposited", PostPaymentInfo.IsDeposited, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)


            '   add/delete Payment Disbursement Relation
            If ScheduledDisb.Count > 0 Then
                '   build insert the query
                sb = New StringBuilder
                With sb
                    .Append("INSERT saPmtDisbRel (PmtDisbRelId, TransactionId, Amount,")
                    .Append("   AwardScheduleId, PayPlanScheduleId, ModUser, ModDate) ")
                    .Append("VALUES (?,?,?,?,?,?,?) ")
                End With

                '   build delete the query
                Dim sbDel As New StringBuilder
                With sbDel
                    .Append("DELETE FROM saPmtDisbRel ")
                    .Append("WHERE ")
                    .Append("        ((AwardScheduleId Is Null and ? Is Null) or AwardScheduleId=?) ")
                    .Append("AND ")
                    .Append("        ((PayPlanScheduleId Is null and ? is Null) or PayPlanScheduleId=?) ")
                    .Append("AND ")
                    .Append("        ModDate=?; ")
                    .Append("SELECT COUNT (*) FROM saPmtDisbRel WHERE AwardScheduleId=? AND PayPlanScheduleId=? ")
                End With

                '   build query to update the faStudentAwardSchedule table
                Dim sbUpdFA As New StringBuilder
                With sbUpdFA
                    .Append("UPDATE faStudentAwardSchedule ")
                    .Append("SET Reference=?, ModDate=?, ModUser=? ")
                    .Append("WHERE AwardScheduleId=? ")
                End With

                '   build query to update the faStuPaymentPlanSchedule table
                Dim sbUpdPP As New StringBuilder
                With sbUpdPP
                    .Append("UPDATE faStuPaymentPlanSchedule ")
                    .Append("SET Reference=?, ModDate=?, ModUser=? ")
                    .Append("WHERE PayPlanScheduleId=? ")
                End With

                For Each disb As ScheduledDisbInfo In ScheduledDisb
                    '   add parameters values to the query
                    db.ClearParameters()

                    '   add parameters values to the query

                    If disb.IsInDB Then

                        '   reversing a posted scheduled payment

                        '**************************************************************************
                        '**************************************************************************
                        '********************** Delete from saPmtDisbRel table ********************
                        '**************************************************************************
                        '**************************************************************************

                        '   AwardScheduleId
                        If disb.AwardScheduleId = Guid.Empty.ToString Then
                            db.AddParameter("@AwardScheduleId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Else
                            db.AddParameter("@AwardScheduleId", disb.AwardScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        End If

                        '   AwardScheduleId
                        If disb.AwardScheduleId = Guid.Empty.ToString Then
                            db.AddParameter("@AwardScheduleId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Else
                            db.AddParameter("@AwardScheduleId", disb.AwardScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        End If

                        '   PayPlanScheduleId
                        If disb.PayPlanScheduleId = Guid.Empty.ToString Then
                            db.AddParameter("@PayPlanScheduleId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Else
                            db.AddParameter("@PayPlanScheduleId", disb.PayPlanScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        End If

                        '   PayPlanScheduleId
                        If disb.PayPlanScheduleId = Guid.Empty.ToString Then
                            db.AddParameter("@PayPlanScheduleId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Else
                            db.AddParameter("@PayPlanScheduleId", disb.PayPlanScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        End If

                        '   ModDate
                        db.AddParameter("@ModDate", disb.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                        '   AwardScheduleId
                        If disb.AwardScheduleId = Guid.Empty.ToString Then
                            db.AddParameter("@AwardScheduleId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Else
                            db.AddParameter("@AwardScheduleId", disb.AwardScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        End If

                        '   PayPlanScheduleId
                        If disb.PayPlanScheduleId = Guid.Empty.ToString Then
                            db.AddParameter("@PayPlanScheduleId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Else
                            db.AddParameter("@PayPlanScheduleId", disb.PayPlanScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        End If

                        'db.RunParamSQLExecuteNoneQuery(sbDel.ToString, groupTrans)

                        '   execute the query
                        Dim rowCount As Integer = db.RunParamSQLScalar(sbDel.ToString, groupTrans)

                        '   If the row was not deleted then there was a concurrency problem
                        If Not rowCount = 0 Then

                            '   rollback transaction
                            groupTrans.Rollback()

                            '   return an error message
                            Return DALExceptions.BuildConcurrencyExceptionMessage()
                        End If


                        '   add parameters values to the query
                        db.ClearParameters()


                        '**************************************************************************
                        '**************************************************************************
                        '**** Update faStudentAwardSchedule or faStuPaymentPlanSchedule table *****
                        '**************************************************************************
                        '**************************************************************************

                        '   Scheduled Award or Scheduled Payment Plan Reference
                        db.AddParameter("@Reference", "", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                        '   ModDate
                        db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                        '   ModUser
                        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                        If disb.AwardScheduleId <> Guid.Empty.ToString Then
                            '   Update the faStudentAwardSchedule table
                            '   AwardScheduleId
                            db.AddParameter("@AwardScheduleId", disb.AwardScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                            '   execute the query
                            db.RunParamSQLExecuteNoneQuery(sbUpdFA.ToString, groupTrans)

                        Else
                            '   Update the faStuPaymentPlanSchedule table
                            '   PayPlanScheduleId
                            db.AddParameter("@PayPlanScheduleId", disb.PayPlanScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                            '   execute the query
                            db.RunParamSQLExecuteNoneQuery(sbUpdPP.ToString, groupTrans)

                        End If


                    Else

                        '**************************************************************************
                        '**************************************************************************
                        '********************** Insert into saPmtDisbRel table ********************
                        '**************************************************************************
                        '**************************************************************************

                        '   pmtDisbRelId
                        db.AddParameter("@PmtDisbRelId", disb.PmtDisbRelId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                        '   TransactionId
                        db.AddParameter("@TransactionId", disb.TransactionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                        '   Amount
                        db.AddParameter("@Amount", disb.Amount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

                        '   AwardScheduleId
                        If disb.AwardScheduleId = Guid.Empty.ToString Then
                            db.AddParameter("@AwardScheduleId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Else
                            db.AddParameter("@AwardScheduleId", disb.AwardScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        End If

                        '   PayPlanScheduleId
                        If disb.PayPlanScheduleId = Guid.Empty.ToString Then
                            db.AddParameter("@PayPlanScheduleId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Else
                            db.AddParameter("@PayPlanScheduleId", disb.PayPlanScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        End If

                        '   ModUser
                        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                        '   ModDate
                        db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                        '   execute the query
                        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)


                        '   add parameters values to the query
                        db.ClearParameters()


                        '**************************************************************************
                        '**************************************************************************
                        '**** Update faStudentAwardSchedule or faStuPaymentPlanSchedule table *****
                        '**************************************************************************
                        '**************************************************************************

                        '   Scheduled Award or Scheduled Payment Plan Reference
                        db.AddParameter("@Reference", PostPaymentInfo.Reference, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                        '   ModDate
                        db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                        '   ModUser
                        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                        If disb.AwardScheduleId <> Guid.Empty.ToString Then
                            '   Update the faStudentAwardSchedule table
                            '   AwardScheduleId
                            db.AddParameter("@AwardScheduleId", disb.AwardScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                            '   execute the query
                            db.RunParamSQLExecuteNoneQuery(sbUpdFA.ToString, groupTrans)

                        Else
                            '   Update the faStuPaymentPlanSchedule table
                            '   PayPlanScheduleId
                            db.AddParameter("@PayPlanScheduleId", disb.PayPlanScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                            '   execute the query
                            db.RunParamSQLExecuteNoneQuery(sbUpdPP.ToString, groupTrans)

                        End If
                    End If
                Next
            End If

            'Add any payment application to charges. This should only be executed if a collection of payment allocation
            'was passed to it.
            If Not appPmtColl Is Nothing Then
                Dim sbAppliedPayment As New StringBuilder

                With sbAppliedPayment
                    .Append("INSERT INTO saAppliedPayments(AppliedPmtId,TransactionId,ApplyToTransId,Amount,ModUser,ModDate) ")
                    .Append("VALUES(?,?,?,?,?,?) ")
                End With

                For Each appPmtInfo As AppliedPaymentInfo In appPmtColl
                    'Clear any params
                    db.ClearParameters()

                    'Add params
                    db.AddParameter("@AppliedPmtId", appPmtInfo.AppliedPaymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@TransId", appPmtInfo.PaymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@chargeid", appPmtInfo.ChargeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@amount", appPmtInfo.Amount, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                    db.AddParameter("@moduser", appPmtInfo.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@moddate", Utilities.GetAdvantageDBDateTime(Date.Now), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                    'Execute the query
                    db.RunParamSQLExecuteNoneQuery(sbAppliedPayment.ToString, groupTrans)

                Next
            End If


            '   commit transaction 
            groupTrans.Commit()

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()
            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            End If
            '   return an error to the client
            If ex.Message.Contains("FK_saPayments_saTransactions") Then Return "Cannot save the transaction since a similar transaction has already been posted for the same day."
            Return DALExceptions.BuildErrorMessage(ex)

        Catch ex As Exception

            '   rollback transaction if there were errors
            groupTrans.Rollback()
            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            End If
            '   return an error to the client   
            If ex.Message.Contains("FK_saPayments_saTransactions") Then Return "Cannot save the transaction since a similar transaction has already been posted for the same day."
            Return ex.Message()

        Finally
            'Close Connection
            db.CloseConnection()
        End Try


    End Function
    Public Function DeletePostPaymentInfo(ByVal postPaymentId As String, ByVal modDate As DateTime, ByVal modDate1 As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saPayments ")
                .Append("WHERE TransactionId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("Select count (*) from saPayments where TransactionId = ? ")
            End With

            '   add parameters values to the query

            '   TransactionId
            db.AddParameter("@TransactionId", postPaymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TransactionId
            db.AddParameter("@TransactionId", postPaymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If Not rowCount = 0 Then

                '   rollback transaction
                groupTrans.Rollback()

                '   return an error message
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

            '   build the query
            sb = New StringBuilder
            With sb
                .Append("DELETE FROM saTransactions ")
                .Append("WHERE PostPaymentId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("Select count (*) from saTransactions where PostPaymentId = ? ")
            End With

            '   add parameters values to the query
            db.ClearParameters()

            '   TransactionId
            db.AddParameter("@TransactionId", postPaymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TransactionId
            db.AddParameter("@TransactionId", postPaymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   commit transaction 
                groupTrans.Commit()

                '   return without errors
                Return ""
            Else

                '   rollback transaction
                groupTrans.Rollback()

                '   return an error message
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()
            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            End If
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetStudentLedger(ByVal studentId As String, ByVal StuEnrollId As String, ByVal termId As String, Optional ByVal strWhere As String = "") As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        ' Add the StudentId to the parameter list
        db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ''query Modified by Saraswathi lakshmanan on July 20 2009
        ''The payment type and the coresponding number i.e. check number etc are fetched.
        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT distinct ")
            .Append("	 -1 as RecordType,  ")
            ''Added to fix 16578
            '' .Append(" P.CheckNUmber, P.PaymentTypeId, ")
            .Append(" IsNUll((Select CheckNumber from saPayments where Transactionid=T.TransactionId  ),NUll)CheckNUmber, IsNUll((Select PaymentTypeId from saPayments where Transactionid=T.TransactionId  ),NUll) PaymentTypeId ,  ")
            .Append("    (Select TransDate from saTransactions where TransactionId=AP.ApplyToTransId) As ChargeDate, ")
            .Append("	 AP.ApplyToTransId, ")
            .Append("	 T.TransactionId,  ")
            .Append("    (Select StudentId from arStuEnrollments where StuEnrollId=T.StuEnrollId) StudentId,  ")
            .Append("    T.StuEnrollId,  ")
            .Append("    (Select PrgVerDescrip from arPrgVersions where PrgVerId=(Select PrgVerId from arStuEnrollments where StuEnrollId=T.StuEnrollId)) as EnrollmentDescrip,  ")
            .Append("    T.TransCodeId,  ")
            .Append("    (Select TransCodeDescrip from saTransCodes where TransCodeId=T.TransCodeId) As TransCodeDescrip,  ")
            .Append("    T.TransDescrip,  ")
            .Append("    T.TransDate, ")
            '.Append("    replace(CONVERT(varchar(10), T.TransDate,110),'-','/') as TransDate,  ")
            .Append("     -AP.Amount as TransAmount,  ")
            .Append("    T.TransReference,  ")
            .Append("    T.AcademicYearId,  ")
            .Append("    (Select AcademicYearDescrip from saAcademicYears where AcademicYearId=T.AcademicYearId) As AcademicYearDescrip,  ")
            .Append("    T.TermId,  ")
            .Append("    (Select TermDescrip from arTerm where TermId=T.TermId) As TermDescrip,  ")
            .Append("    T.TransTypeId,  ")
            .Append("    (Select Case T.TransTypeId When 0 then 'Charge' when 2 then 'Payment' when 1 then 'Adjustment' when 3 then 'Reversal' end) As TransTypeDescrip,  ")
            .Append("    T.Voided,  ")
            .Append("    T.ModUser,  ")
            .Append("    T.ModDate,  ")
            .Append("    CASE WHEN T.PaymentPeriodNumber IS NULL ")
            .Append("       THEN paymentPeriods.PeriodNumber  ")
            .Append("       ELSE T.PaymentPeriodNumber  ")
            .Append("    END As PaymentPeriodNumber  ")
            .Append("FROM  saTransactions T ")
            .Append(" inner join saAppliedPayments AP ON T.TransactionId=AP.TransactionId ")
            .Append(" left join saPmtPeriods paymentPeriods ON T.TransactionId=paymentPeriods.PmtPeriodId ")
            ''Added to fix 16578
            '' .Append(" ,SaPayments P ")
            .Append("WHERE T.StuEnrollId IN (Select StuEnrollId from arStuEnrollments where StudentId=?) ")
            ''Added to fix 16578
            '' .Append("And T.Transactionid=P.TransactionId ")
            '   check if the selection is "All Enrollments"
            If Not StuEnrollId = Guid.Empty.ToString Then
                .Append(" And  T.StuEnrollId = ? ")
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            '   check if the selection is "All Terms"
            If Not termId = Guid.Empty.ToString Then
                .Append(" And  T.TermId = ? ")
                db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            .Append(" And IsPosted = 1 ")
            If (strWhere <> "") Then
                .Append(strWhere)
            End If
            .Append(" And T.Voided=0 ")
            .Append("union all ")

            ' Add the StudentId to the parameter list
            db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ''Added by Saraswathi lakshmanan
            ''on Oct 27 2009
            ''To fix mantis case 
            ''17908: QA: BUG: Student balance displayed when we select balance forward is different from what we see when we select open item.
            ''The student had a partial payment applied and the remaining amount was not applied to the student . so the balance was not same for both balance forward and open item.
            ''So the open Item calculation is modified and the non applied payments are also pulled in.
            ''Additional Union query added to get the remainng non applied payment against the same transactionid.
            ''This query is similar to the first part of the query and the only difference is the ApplytoTransid is put as the Transactionid and the TransAmount is the Amount in the Transactiontable + Amount in the applied payment table. Which gives us the non applied payment.


            ''---------------

            '   with subqueries
            .Append("SELECT distinct ")
            .Append("	 -1 as RecordType,  ")
            ''Added to fix 16578
            '' .Append(" P.CheckNUmber, P.PaymentTypeId, ")
            .Append(" IsNUll((Select CheckNumber from saPayments where Transactionid=T.TransactionId  ),NUll)CheckNUmber, IsNUll((Select PaymentTypeId from saPayments where Transactionid=T.TransactionId  ),NUll) PaymentTypeId ,  ")
            .Append("    (Select TransDate from saTransactions where TransactionId=AP.TransactionId) As ChargeDate, ")
            .Append("	T.TransactionId, ")
            .Append("	 T.TransactionId,  ")
            .Append("    (Select StudentId from arStuEnrollments where StuEnrollId=T.StuEnrollId) StudentId,  ")
            .Append("    T.StuEnrollId,  ")
            .Append("    (Select PrgVerDescrip from arPrgVersions where PrgVerId=(Select PrgVerId from arStuEnrollments where StuEnrollId=T.StuEnrollId)) as EnrollmentDescrip,  ")
            .Append("    T.TransCodeId,  ")
            .Append("    (Select TransCodeDescrip from saTransCodes where TransCodeId=T.TransCodeId) As TransCodeDescrip,  ")
            .Append("    T.TransDescrip,  ")
            .Append("    T.TransDate, ")
            '.Append("    replace(CONVERT(varchar(10), T.TransDate,110),'-','/') as TransDate,  ")
            '' .Append("     -AP.Amount as TransAmount,  ")

            .Append("    +((Select TransAmount from SaTransactions where TransactionId=T.TransactionId)")
            ''To fix mantis issue 17930: QA Bug: Issue when applying payments to the same transaction multiple times. 
            ''Since two applied payments were made for the same transasction Id it was crashing. now the sum of Amount is found
            .Append("  +(Select Sum(Amount) from SaAppliedpayments where TransactionId=T.TransactionID)) as TransAmount,  ")

            .Append("    T.TransReference,  ")
            .Append("    T.AcademicYearId,  ")
            .Append("    (Select AcademicYearDescrip from saAcademicYears where AcademicYearId=T.AcademicYearId) As AcademicYearDescrip,  ")
            .Append("    T.TermId,  ")
            .Append("    (Select TermDescrip from arTerm where TermId=T.TermId) As TermDescrip,  ")
            .Append("    T.TransTypeId,  ")
            .Append("    (Select Case T.TransTypeId When 0 then 'Charge' when 2 then 'Payment' when 1 then 'Adjustment'  when 3 then 'Reversal' end) As TransTypeDescrip,  ")
            .Append("    T.Voided,  ")
            .Append("    T.ModUser,  ")
            .Append("    T.ModDate,  ")
            .Append("    CASE WHEN T.PaymentPeriodNumber IS NULL ")
            .Append("       THEN paymentPeriods.PeriodNumber  ")
            .Append("       ELSE T.PaymentPeriodNumber  ")
            .Append("    END As PaymentPeriodNumber  ")
            .Append("FROM  saTransactions T ")
            .Append(" inner join saAppliedPayments AP ON T.TransactionId=AP.TransactionId ")
            .Append(" left join saPmtPeriods paymentPeriods ON T.TransactionId=paymentPeriods.PmtPeriodId ")
            '.Append("FROM  saTransactions T, saAppliedPayments AP  ")
            ''Added to fix 16578
            '' .Append(" ,SaPayments P ")
            .Append("WHERE T.StuEnrollId IN (Select StuEnrollId from arStuEnrollments where StudentId=?) ")
            '.Append("and T.TransactionId=AP.TransactionId ")
            ''Added to fix 16578
            '' .Append("and T.Transactionid=P.TransactionId ")
            '   check if the selection is "All Enrollments"
            If Not StuEnrollId = Guid.Empty.ToString Then
                .Append(" AND  T.StuEnrollId = ? ")
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            '   check if the selection is "All Terms"
            If Not termId = Guid.Empty.ToString Then
                .Append(" AND  T.TermId = ? ")
                db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            .Append(" AND IsPosted = 1 ")
            If (strWhere <> "") Then
                .Append(strWhere)
            End If
            .Append(" AND T.Voided=0 ")
            ''Added by Saraswathi On Oct 28 2009
            ''When all the amount is applied, it gives an entry with 0 payments and 0 balance.
            ''Which is not needed
            ''So removing the entries with 0 as payment
            .Append("and ((Select TransAmount from SaTransactions where TransactionId=T.TransactionId) ")
            .Append(" +(Select Sum(Amount) from SaAppliedpayments where TransactionId=T.TransactionID))<>0.00  ")
            .Append("union all ")

            ''-----------------------


            ' Add the StudentId to the parameter list
            db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            .Append("SELECT  ")
            .Append("	 (select count(*) from saAppliedPayments where TransactionId=T.TransactionId) as RecordType, ")
            ''Added to fix 16578
            ''   .Append(" P.CheckNUmber, P.PaymentTypeId, ")
            .Append(" IsNUll((Select CheckNumber from saPayments where Transactionid=T.TransactionId  ),NUll)CheckNUmber, IsNUll((Select PaymentTypeId from saPayments where Transactionid=T.TransactionId  ),NUll) PaymentTypeId ,  ")
            .Append("     T.TransDate, ")
            .Append("	 T.TransactionId,  ")
            .Append("	 T.TransactionId,  ")
            .Append("    (Select StudentId from arStuEnrollments where StuEnrollId=T.StuEnrollId) StudentId,  ")
            .Append("    T.StuEnrollId,  ")
            .Append("    (Select PrgVerDescrip from arPrgVersions where PrgVerId=(Select PrgVerId from arStuEnrollments where StuEnrollId=T.StuEnrollId)) as EnrollmentDescrip,  ")
            .Append("    T.TransCodeId,  ")
            .Append("    (Select TransCodeDescrip from saTransCodes where TransCodeId=T.TransCodeId) As TransCodeDescrip,  ")
            .Append("    T.TransDescrip,  ")
            .Append("    replace(CONVERT(varchar(10), T.TransDate,110),'-','/') as TransDate,  ")
            .Append("     T.TransAmount,  ")
            .Append("    T.TransReference,  ")
            .Append("    T.AcademicYearId,  ")
            .Append("    (Select AcademicYearDescrip from saAcademicYears where AcademicYearId=T.AcademicYearId) As AcademicYearDescrip,  ")
            .Append("    T.TermId,  ")
            .Append("    (Select TermDescrip from arTerm where TermId=T.TermId) As TermDescrip,  ")
            .Append("    T.TransTypeId,  ")
            .Append("    (Select Case T.TransTypeId When 0 then 'Charge' when 2 then 'Payment' when 1 then 'Adjustment'  when 3 then 'Reversal' end) As TransTypeDescrip,  ")
            .Append("    T.Voided,  ")
            .Append("    T.ModUser,  ")
            .Append("    T.ModDate,  ")
            .Append("    CASE WHEN T.PaymentPeriodNumber IS NULL ")
            .Append("       THEN paymentPeriods.PeriodNumber  ")
            .Append("       ELSE T.PaymentPeriodNumber  ")
            .Append("    END As PaymentPeriodNumber  ")
            .Append("FROM  saTransactions T  ")
            .Append(" left join saPmtPeriods paymentPeriods ON T.TransactionId=paymentPeriods.PmtPeriodId ")
            ''Added to fix 16578
            '' .Append(" ,SaPayments P ")
            .Append("WHERE T.StuEnrollId IN (Select StuEnrollId from arStuEnrollments where StudentId=?) ")
            ''Added to fix 16578
            ''    .Append("and T.Transactionid=P.TransactionId ")
            '   check if the selection is "All Enrollments"
            If Not StuEnrollId = Guid.Empty.ToString Then
                .Append(" AND  T.StuEnrollId = ? ")
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            '   check if the selection is "All Terms"
            If Not termId = Guid.Empty.ToString Then
                .Append(" AND  T.TermId = ? ")
                db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            .Append(" AND IsPosted = 1 ")
            If (strWhere <> "") Then
                .Append(strWhere)
            End If
            .Append(" AND T.Voided=0 ")
            .Append("ORDER BY T.TransDate, T.ModDate, T.TransactionId ")
        End With

        '' Add the StudentId to the parameter list
        'db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '' Add the StuEnrollId only if it is a particular one
        'If Not StuEnrollId = Guid.Empty.ToString Then
        '    db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'End If

        '' Add the termId only if it is a particular one
        'If Not termId = Guid.Empty.ToString Then
        '    db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'End If

        '   Execute the query and return Dataset
        'Return db.RunParamSQLDataSet(sb.ToString)
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        '   add a column that will contain balance forward values
        Dim studentLedgerTable As DataTable = ds.Tables(0)
        studentLedgerTable.Columns.Add("Balance", GetType(String))

        '   calculate balance forward for each record in the table
        If studentLedgerTable.Rows.Count > 0 Then
            Dim runningBalance As Decimal = 0.0
            For i As Integer = 0 To studentLedgerTable.Rows.Count - 1
                Dim row As DataRow = studentLedgerTable.Rows(i)
                If row("RecordType") >= 0 Then
                    runningBalance += CType(row("TransAmount"), Decimal)
                End If
                row("Balance") = runningBalance
            Next
        End If

        '   add a column that will contain document balance values
        studentLedgerTable.Columns.Add("OpenItemBalance", GetType(String))
        Dim dv As New DataView(studentLedgerTable, "RecordType <= 0 ", "ApplyToTransId asc, TransDate asc", DataViewRowState.CurrentRows)
        '   calculate balance for each gropu of transactions
        If dv.Count > 0 Then
            Dim runningBalance As Decimal = 0.0
            Dim previousApplyToTransId As Guid = dv.Item(0).Row("ApplyToTransId")
            For i As Integer = 0 To dv.Count - 1
                Dim rowView As DataRowView = dv.Item(i)
                rowView.Row("OpenItemBalance") = String.Empty
                Dim currentApplyToTransId As Guid = rowView("ApplyToTransId")
                If Not currentApplyToTransId = previousApplyToTransId Then
                    dv.Item(i - 1).Row("OpenItemBalance") = runningBalance.ToString("c")
                    runningBalance = 0.0
                    previousApplyToTransId = currentApplyToTransId
                End If
                runningBalance += CType(rowView("TransAmount"), Decimal)
            Next
            dv.Item(dv.Count - 1).Row("OpenItemBalance") = runningBalance.ToString("c")
        End If

        '   add a column that will contain document balance values
        studentLedgerTable.Columns.Add("IsItemClosed", GetType(Boolean))
        dv = New DataView(studentLedgerTable, "RecordType <= 0 ", "ApplyToTransId asc, TransDate asc", DataViewRowState.CurrentRows)
        '   calculate balance for each gropu of transactions
        If dv.Count > 0 Then
            Dim runningBalance As Decimal = 0.0
            Dim previousApplyToTransId As Guid = Guid.Empty
            Dim currentState As Boolean
            For i As Integer = dv.Count - 1 To 0 Step -1
                Dim row As DataRow = dv.Item(i).Row
                Dim currentApplyToTransId As Guid = row("ApplyToTransId")
                If Not currentApplyToTransId = previousApplyToTransId Then
                    currentState = dv.Item(i)("OpenItemBalance") = "$0.00"
                    previousApplyToTransId = currentApplyToTransId
                End If
                dv.Item(i).Row("IsItemClosed") = currentState
            Next
            dv.Sort = "TransDate asc, ModDate asc, TransactionId"
        End If

        '   return dataset
        Return ds

    End Function
    'Added for multiple student ledger report
    Public Function GetMultipleStudentLedger(ByVal studentId As String, ByVal StuEnrollId As String, ByVal termId As String, Optional ByVal strWhere As String = "") As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        ' Add the StudentId to the parameter list
        db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT distinct ")
            .Append("	 -1 as RecordType,  ")
            .Append("    (Select TransDate from saTransactions where TransactionId=AP.ApplyToTransId) As ChargeDate, ")
            .Append("	 AP.ApplyToTransId, ")
            .Append("	 T.TransactionId,  ")
            .Append("    (Select StudentId from arStuEnrollments where StuEnrollId=T.StuEnrollId) StudentId,  ")
            .Append("    T.StuEnrollId,  ")
            .Append("    (Select PrgVerDescrip from arPrgVersions where PrgVerId=(Select PrgVerId from arStuEnrollments where StuEnrollId=T.StuEnrollId)) as EnrollmentDescrip,  ")
            .Append("    T.TransCodeId,  ")
            .Append("    (Select TransCodeDescrip from saTransCodes where TransCodeId=T.TransCodeId) As TransCodeDescrip,  ")
            .Append("    T.TransDescrip,  ")
            .Append("    replace(CONVERT(varchar(10), T.TransDate,110),'-','/') as TransDate,  ")
            .Append("    AP.Amount as TransAmount,  ")
            '.Append("    '$' + convert(varchar,-AP.Amount) as TransAmount,  ")
            .Append("    T.TransReference,  ")
            .Append("    T.AcademicYearId,  ")
            .Append("    (Select AcademicYearDescrip from saAcademicYears where AcademicYearId=T.AcademicYearId) As AcademicYearDescrip,  ")
            .Append("    T.TermId,  ")
            .Append("    (Select TermDescrip from arTerm where TermId=T.TermId) As TermDescrip,  ")
            .Append("    T.TransTypeId,  ")
            .Append("    (Select Case T.TransTypeId When 0 then 'Charge' when 2 then 'Payment' when 1 then 'Adjustment' end) As TransTypeDescrip,  ")
            .Append("    T.Voided,  ")
            .Append("    T.ModUser,  ")
            .Append("    T.ModDate  ")
            .Append("FROM  saTransactions T, saAppliedPayments AP  ")
            .Append("WHERE T.StuEnrollId IN (Select StuEnrollId from arStuEnrollments where StudentId=?) ")
            .Append("and T.TransactionId=AP.TransactionId ")
            '   check if the selection is "All Enrollments"
            If Not StuEnrollId = Guid.Empty.ToString Then
                .Append(" AND  T.StuEnrollId = ? ")
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            '   check if the selection is "All Terms"
            If Not termId = Guid.Empty.ToString Then
                .Append(" AND  T.TermId = ? ")
                db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            .Append(" AND IsPosted = 1 ")
            If (strWhere <> "") Then
                .Append(strWhere)
            End If
            .Append(" AND T.Voided=0 ")
            .Append("union all ")

            ' Add the StudentId to the parameter list
            db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            .Append("SELECT  ")
            .Append("	 (select count(*) from saAppliedPayments where TransactionId=T.TransactionId) as RecordType, ")
            .Append("     T.TransDate, ")
            .Append("	 T.TransactionId,  ")
            .Append("	 T.TransactionId,  ")
            .Append("    (Select StudentId from arStuEnrollments where StuEnrollId=T.StuEnrollId) StudentId,  ")
            .Append("    T.StuEnrollId,  ")
            .Append("    (Select PrgVerDescrip from arPrgVersions where PrgVerId=(Select PrgVerId from arStuEnrollments where StuEnrollId=T.StuEnrollId)) as EnrollmentDescrip,  ")
            .Append("    T.TransCodeId,  ")
            .Append("    (Select TransCodeDescrip from saTransCodes where TransCodeId=T.TransCodeId) As TransCodeDescrip,  ")
            .Append("    T.TransDescrip,  ")
            .Append("    replace(CONVERT(varchar(10), T.TransDate,110),'-','/') as TransDate,  ")
            .Append("    substring(convert(varchar(30),T.TransAmount),1,CHARINDEX('.',T.TransAmount)+2)as TransAmount,  ")
            '.Append("   T.TransAmount,  ")
            '.Append("    '$' + convert(varchar,T.TransAmount),  ")
            .Append("    T.TransReference,  ")
            .Append("    T.AcademicYearId,  ")
            .Append("    (Select AcademicYearDescrip from saAcademicYears where AcademicYearId=T.AcademicYearId) As AcademicYearDescrip,  ")
            .Append("    T.TermId,  ")
            .Append("    (Select TermDescrip from arTerm where TermId=T.TermId) As TermDescrip,  ")
            .Append("    T.TransTypeId,  ")
            .Append("    (Select Case T.TransTypeId When 0 then 'Charge' when 2 then 'Payment' when 1 then 'Adjustment' end) As TransTypeDescrip,  ")
            .Append("    T.Voided,  ")
            .Append("    T.ModUser,  ")
            .Append("    T.ModDate  ")
            .Append("FROM  saTransactions T  ")
            .Append("WHERE T.StuEnrollId IN (Select StuEnrollId from arStuEnrollments where StudentId=?) ")
            '   check if the selection is "All Enrollments"
            If Not StuEnrollId = Guid.Empty.ToString Then
                .Append(" AND  T.StuEnrollId = ? ")
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            '   check if the selection is "All Terms"
            If Not termId = Guid.Empty.ToString Then
                .Append(" AND  T.TermId = ? ")
                db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            .Append(" AND IsPosted = 1 ")
            If (strWhere <> "") Then
                .Append(strWhere)

            End If
            .Append(" AND T.Voided=0 ")
            .Append("ORDER BY T.TransDate, T.ModDate ")
        End With


        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        Dim MultipleStudentLedger As DataTable = ds.Tables(0)

        MultipleStudentLedger.Columns.Add("Balance", GetType(String))

        If MultipleStudentLedger.Rows.Count > 0 Then
            Dim runningBalance As Decimal = 0.0
            For i As Integer = 0 To MultipleStudentLedger.Rows.Count - 1
                Dim row As DataRow = MultipleStudentLedger.Rows(i)
                If row("RecordType") >= 0 Then
                    runningBalance += CType(row("TransAmount"), Decimal)
                End If
                row("Balance") = runningBalance
            Next
        End If

        Return ds

    End Function
    Public Function UpdateSelectedDeposits(ByVal bankAcctId As String, ByVal user As String, ByVal selectedDeposits() As String)

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the query
        Dim sb As New StringBuilder

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   do an update
        Try
            '   Insert one record per each Item in the Selected Group
            Dim i As Integer
            '   use the same timestamp to update all records
            Dim dateNow As Date = Date.Now

            For i = 0 To selectedDeposits.Length - 1



                '   build the query
                With sb
                    .Append("UPDATE saPayments ")
                    .Append(" SET BankAcctId = ?, IsDeposited = 1, ")
                    .Append("   ModUser = ?, ModDate = ? ")
                    .Append("WHERE TransactionId= ? ")
                End With

                '   add parameters values to the query
                db.AddParameter("@BankAcctId", bankAcctId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", dateNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@TransactionId", DirectCast(selectedDeposits.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Next

            '   commit transaction 
            groupTrans.Commit()

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()
            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            End If
            '   return an error to the client
            Return ex.Message

        Finally
            'Close Connection
            db.CloseConnection()
        End Try


    End Function

    Public Function UpdateSelectedLeadDeposits(ByVal user As String, ByVal selectedDeposits() As String)

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the query
        Dim sb As New StringBuilder

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   do an update
        Try
            '   Insert one record per each Item in the Selected Group
            Dim i As Integer
            '   use the same timestamp to update all records
            Dim dateNow As Date = Date.Now

            For i = 0 To selectedDeposits.Length - 1

                '   build the query
                With sb
                    .Append("UPDATE adLeadPayments SET IsDeposited = 1, ModUser = ?, ModDate = ? WHERE TransactionId= ? ")
                End With

                '   add parameters values to the query
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", dateNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@TransactionId", DirectCast(selectedDeposits.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Next

            '   commit transaction 
            groupTrans.Commit()

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()
            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            End If
            '   return an error to the client
            Return ex.Message

        Finally
            'Close Connection
            db.CloseConnection()
        End Try


    End Function

    Public Function GetAvailableDeposits(ByVal startDate As Date, ByVal endDate As Date, ByVal bankAcctId As String, ByVal campusId As String, bDeposited As Boolean) As DataSet

        'Dim db As New DataAccess
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


            ' Add BankAcct to the parameter list
            db.AddParameter("@BankAcct", bankAcctId, SqlDbType.VarChar, , ParameterDirection.Input)

            ' Add BankAcct to the parameter list
            ' db.AddParameter("@BankAcct", bankAcctId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            ' Add CampusId to the parameter list
            db.AddParameter("@CampusId", campusId, SqlDbType.VarChar, 200, ParameterDirection.Input)

            ' Add StartDate to the parameter list
            If startDate > Date.MinValue Then
                db.AddParameter("@StartDate", startDate, SqlDbType.VarChar, 200, ParameterDirection.Input)
            End If

            ' Add EndDate to the parameter list
            If endDate < Date.MaxValue Then
                db.AddParameter("@EndDate", DateAdd(DateInterval.Day, 1, endDate), SqlDbType.DateTime, , ParameterDirection.Input)
            End If

            db.AddParameter("@IsDeposited", bDeposited, SqlDbType.Bit, ParameterDirection.Input)

            Dim ds As DataSet = db.RunParamSQLDataSet_SP("usp_GetAvailableDeposits")

            Return ds

        Finally

            db.CloseConnection()
        End Try

    End Function

    Public Function GetMinAndMaxDatesFromTransactions(ByVal campusId As String) As Date()
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       Min(T.TransDate) As MinDate, ")
            .Append("       Max(T.TransDate) As MaxDate ")
            .Append("FROM   saTransactions T ")
            .Append("WHERE  CampusId=? ")
            '.Append("WHERE  T.IsPosted = 0  ")
            .Append("AND    T.Voided=0 ")
        End With

        'add CampusId parameter
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   fill date Array 
        Dim dateArray(1) As DateTime
        While dr.Read()
            If Not dr("MinDate") Is DBNull.Value Then dateArray(0) = CType(dr("MinDate"), DateTime) Else dateArray(0) = AdvantageCommonValues.FiveYearsAgo
            If Not dr("MaxDate") Is DBNull.Value Then dateArray(1) = CType(dr("MaxDate"), DateTime) Else dateArray(1) = AdvantageCommonValues.FiveYearsFromNow
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return date array
        Return dateArray

    End Function

    Public Function GetMinAndMaxDatesFromStudentAndLeadTransactions(ByVal campusId As String) As Date()

        Dim db As New DataAccess
        Dim db2 As New DataAccess

        Dim dr As OleDbDataReader
        Dim dr2 As OleDbDataReader

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try

            '==============================================================================
            'Find the min and max transactions from the saPayments table and add them to the
            'array.
            '==============================================================================
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            'build the sql query
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("       Min(T.TransDate) As MinDate, ")
                .Append("       Max(T.TransDate) As MaxDate ")
                .Append("FROM   saTransactions T ")
                .Append("WHERE  CampusId=? ")
                .Append("AND    T.Voided=0 ")
            End With

            'add CampusId parameter
            db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            dr = db.RunParamSQLDataReader(sb.ToString)

            '   fill date Array 
            Dim dateArray(1) As DateTime
            While dr.Read()
                If Not dr("MinDate") Is DBNull.Value Then dateArray(0) = CType(dr("MinDate"), DateTime) Else dateArray(0) = AdvantageCommonValues.FiveYearsAgo
                If Not dr("MaxDate") Is DBNull.Value Then dateArray(1) = CType(dr("MaxDate"), DateTime) Else dateArray(1) = DateTime.Now
            End While
            '==============================================================================


            '==============================================================================
            ' Find the min and max dates in the adLeadTransactions table, and add them to 
            ' then array if they exceed the min and max dates from the saPayments table from
            ' the query above
            '==============================================================================
            db2.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            sb.Clear()
            'build the sql query for lead transactions
            With sb
                .Append("SELECT ")
                .Append("       Min(T.TransDate) As MinDate, ")
                .Append("       Max(T.TransDate) As MaxDate ")
                .Append("FROM   adLeadTransactions T ")
                .Append("WHERE  CampusId=? ")
                .Append("AND    T.Voided=0 ")
            End With

            'add CampusId parameter
            db2.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute the query
            dr2 = db2.RunParamSQLDataReader(sb.ToString)

            'replace the min or max dates if the lead transaction are earlier or later than
            'the student transactions
            While dr2.Read()
                If Not dr2("MinDate") Is DBNull.Value Then
                    If DateTime.Compare(CType(dr2("MinDate"), DateTime), dateArray(0)) < 0 Then
                        dateArray(0) = CType(dr2("MinDate"), DateTime)
                    End If
                End If
                If Not dr2("MaxDate") Is DBNull.Value Then
                    If DateTime.Compare(CType(dr2("MaxDate"), DateTime), dateArray(1)) > 0 Then
                        dateArray(1) = CType(dr2("MaxDate"), DateTime)
                    End If
                End If
            End While
            '==============================================================================

            'Return date array
            Return dateArray

        Finally

            If dr IsNot Nothing Then
                If Not dr.IsClosed Then dr.Close()
            End If

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            If dr2 IsNot Nothing Then
                If Not dr2.IsClosed Then dr2.Close()
            End If

            If db2.Connection.State = ConnectionState.Open Then db2.CloseConnection()

        End Try

    End Function
    Public Function GetPostRefundInfo(ByVal postRefundId As String) As PostRefundInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT /*TransactionId As PostRefundId,*/ ")
            .Append("    (Select StudentId from arStuEnrollments where StuEnrollId=T.StuEnrollId) StudentId, ")
            .Append("    (Select (FirstName + ' ' + LastName) As StudentName from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=T.StuEnrollId)) StudentName, ")
            .Append("    T.StuEnrollId, ")
            .Append("    (Select PrgVerDescrip from arPrgVersions where PrgVerId=(select PrgVerId from arStuEnrollments where StuEnrollId=T.StuEnrollId)) as EnrollmentDescrip, ")
            .Append("    T.TransCodeId, ")
            .Append("    (Select TransCodeDescrip from saTransCodes where TransCodeId=T.TransCodeId) As TransCodeDescrip, ")
            .Append("    T.TransDescrip, ")
            .Append("    T.TransDate, ")
            .Append("    T.TransAmount, ")
            .Append("    T.TransReference, ")
            .Append("    T.AcademicYearId, ")
            .Append("    (Select AcademicYearDescrip from saAcademicYears where AcademicYearId=T.AcademicYearId) As AcademicYearDescrip, ")
            .Append("    T.TermId, ")
            .Append("    (Select StartDate from arTerm where TermId=T.TermId) as EarliestAllowedDate, ")
            .Append("    (Select EndDate from arTerm where TermId=T.TermId) as LatestAllowedDate, ")
            .Append("    (Select TermDescrip from arTerm where TermId=T.TermId) As TermDescrip, ")
            .Append("    T.TransTypeId, ")
            .Append("    T.IsPosted, ")
            .Append("    P.RefundTypeId, ")
            '.Append("    P.ScheduledRefund, ")
            '.Append("    P.StudentAwardId, ")
            .Append("    P.FundSourceId, ")
            .Append("    P.IsInstCharge, ")
            '.Append("    (Select AwardTypeDescrip from faAwardTypes where AwardTypeId=(Select AwardTypeId from faStudentAwards where StudentAwardId=P.StudentAwardId)) + '/' + ")
            '.Append("    (Select FundSourceDescrip from saFundSources where FundSourceId=(Select AwardTypeId from faStudentAwards where StudentAwardId=P.StudentAwardId)) + '/' + ")
            '.Append("    (Select AwardYearDescrip from faAwardYears where AwardYearId=(Select AwardYearId from faStudentAwards where StudentAwardId=P.StudentAwardId)) + '/' + ")
            '.Append("    (Select AcademicYearDescrip from saAcademicYears where AcademicYearId=(Select AcademicYearId from faStudentAwards where StudentAwardId=P.StudentAwardId)) + '/' + ")
            '.Append("    (Select Cast(GrossAmount as varchar(10)) from faStudentAwards where StudentAwardId=P.StudentAwardId) As AwardDescrip, ")
            .Append("    P.BankAcctId, ")
            .Append("    (Select BankAcctDescrip from saBankAccounts where BankAcctId=P.BankAcctId) As BankAcctDescrip, ")
            .Append("    T.ModUser, ")
            .Append("    T.ModDate, ")
            .Append("    P.ModUser as ModUser1, ")
            .Append("    P.ModDate as ModDate1 ")
            .Append("FROM  saTransactions T, saRefunds P ")
            .Append("WHERE T.TransactionId=P.TransactionId ")
            .Append(" AND  T.TransactionId= ? ")
            .Append(" AND T.Voided=0 ")
        End With

        ' Add the PostRefundId to the parameter list
        db.AddParameter("@TransactionId", postRefundId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim PostRefundInfo As New PostRefundInfo

        While dr.Read()

            '   set properties with data from DataReader
            With PostRefundInfo
                .IsInDB = True
                .PostRefundId = postRefundId
                .StudentId = CType(dr("StudentId"), Guid).ToString
                .StudentName = dr("StudentName")
                .StuEnrollId = CType(dr("StuEnrollId"), Guid).ToString
                .EnrollmentDescription = dr("EnrollmentDescrip")
                If Not dr("TransCodeId") Is DBNull.Value Then .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                If Not dr("TransCodeDescrip") Is DBNull.Value Then .TransCodeDescription = dr("TransCodeDescrip")
                .PostRefundDescription = dr("TransDescrip")
                .PostRefundDate = dr("TransDate")
                .Amount = dr("TransAmount")
                If Not dr("TransReference") Is DBNull.Value Then .Reference = dr("TransReference")
                If Not dr("AcademicYearId") Is DBNull.Value Then .AcademicYearId = CType(dr("AcademicYearId"), Guid).ToString
                If Not dr("AcademicYearDescrip") Is DBNull.Value Then .AcademicYearDescrip = dr("AcademicYearDescrip")
                If Not dr("TermId") Is DBNull.Value Then .TermId = CType(dr("TermId"), Guid).ToString
                If Not dr("TermDescrip") Is DBNull.Value Then .TermDescrip = dr("TermDescrip")
                If Not dr("EarliestAllowedDate") Is DBNull.Value Then .EarliestAllowedDate = dr("EarliestAllowedDate")
                If Not dr("LatestAllowedDate") Is DBNull.Value Then .LatestAllowedDate = dr("LatestAllowedDate")
                .TransTypeId = dr("TransTypeId")
                .IsPosted = dr("IsPosted")
                .RefundTypeId = dr("RefundTypeId")
                '.ScheduledRefund = dr("ScheduledRefund")
                'If Not dr("StudentAwardId") Is System.DBNull.Value Then .StudentAwardId = CType(dr("StudentAwardId"), Guid).ToString
                If Not dr("FundSourceId") Is DBNull.Value Then .FundSourceId = CType(dr("FundSourceId"), Guid).ToString
                .IsInstCharge = dr("IsInstCharge")
                'If Not dr("AwardDescrip") Is System.DBNull.Value Then .AwardDescrip = dr("AwardDescrip")
                If Not dr("BankAcctId") Is DBNull.Value Then .BankAcctId = CType(dr("BankAcctId"), Guid).ToString
                If Not dr("BankAcctDescrip") Is DBNull.Value Then .BankAcctDescrip = dr("BankAcctDescrip")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
                .ModUser1 = dr("ModUser1")
                .ModDate1 = dr("ModDate1")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return PostRefundInfo

    End Function
    Public Function UpdatePostRefundInfo(ByVal PostRefundInfo As PostRefundInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saTransactions Set StuEnrollId = ?, TermId = ?, ")
                .Append("   TransDate = ?, TransCodeId = ?, TransReference = ?, TransDescrip = ?, ")
                .Append("   AcademicYearId = ?, TransAmount = ?, TransTypeId = ?, IsPosted = ?, ")
                .Append("   ModUser = ?, ModDate = ? ")
                .Append("WHERE TransactionId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saTransactions where ModDate = ? ")
            End With

            '   add parameters values to the query
            '   StuEnrollId
            db.AddParameter("@StuEnrollId", PostRefundInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TermId
            If PostRefundInfo.TermId = Guid.Empty.ToString Then
                db.AddParameter("@TermId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TermId", PostRefundInfo.TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Date
            db.AddParameter("@TransDate", PostRefundInfo.PostRefundDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TransCodeId
            If PostRefundInfo.TransCodeId = Guid.Empty.ToString Then
                db.AddParameter("@TransCodeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TransCodeId", PostRefundInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Reference
            db.AddParameter("@TransReference", PostRefundInfo.Reference, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Transaction Description
            db.AddParameter("@TransDescrip", PostRefundInfo.PostRefundDescription, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   AcademicYearId
            If PostRefundInfo.AcademicYearId = Guid.Empty.ToString Then
                db.AddParameter("@AcademicYearId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AcademicYearId", PostRefundInfo.AcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Amount
            db.AddParameter("@TransAmount", -PostRefundInfo.Amount, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

            '   TransTypeId
            db.AddParameter("@TransTypeId", PostRefundInfo.TransTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   IsPosted
            db.AddParameter("@IsPosted", PostRefundInfo.IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   PostRefundId
            db.AddParameter("@TransactionId", PostRefundInfo.PostRefundId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", PostRefundInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString, groupTrans)

            '   If there were no updated rows then there was a concurrency problem
            If Not (rowCount = 1) Then

                '   rollback transaction
                groupTrans.Rollback()

                '   return an error message
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

            '
            '   update Refunds table
            '

            '   build query
            sb = New StringBuilder
            With sb
                .Append("UPDATE saRefunds Set RefundTypeId = ?, ")
                .Append("   FundSourceId = ?, IsInstCharge = ?, BankAcctId = ?, ")
                .Append("   ModUser = ?, ModDate = ? ")
                .Append("WHERE TransactionId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saRefunds where ModDate = ? ")
            End With

            '   add parameters values to the query. Clear previous list
            db.ClearParameters()

            '   RefundTypeId
            db.AddParameter("@RefundTypeId", PostRefundInfo.RefundTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ScheduledRefund
            'db.AddParameter("@ScheduledRefund", PostRefundInfo.ScheduledRefund, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   AwardTypeId
            'If PostRefundInfo.StudentAwardId = Guid.Empty.ToString Then
            '    db.AddParameter("@StudentAwardId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'Else
            '    db.AddParameter("@StudentAwardId", PostRefundInfo.StudentAwardId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'End If

            If PostRefundInfo.FundSourceId = Guid.Empty.ToString Then
                db.AddParameter("@FundSourceId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@FundSourceId", PostRefundInfo.FundSourceId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   IsInstCharge
            db.AddParameter("@IsInstCharge", PostRefundInfo.IsInstCharge, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   BankAcctId
            If PostRefundInfo.BankAcctId = Guid.Empty.ToString Then
                db.AddParameter("@BankAcctId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@BankAcctId", PostRefundInfo.BankAcctId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   PostRefundId
            db.AddParameter("@TransactionId", PostRefundInfo.PostRefundId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '   ModDate
            db.AddParameter("@Original_ModDate", PostRefundInfo.ModDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString, groupTrans)
            'rowCount = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   commit transaction 
                groupTrans.Commit()
                '   return without errors
                Return ""
            Else
                '   rollback transaction if there were errors
                groupTrans.Rollback()

                '   return an error message
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()
            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            End If
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddReversedPostRefundInfo(ByVal PostRefundInfo As PostRefundInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saTransactions (TransactionId, StuEnrollId, CampusId, TermId, ")
                .Append("   TransDate, TransCodeId, TransReference, TransDescrip, AcademicYearId, ")
                .Append("   TransAmount, TransTypeId, IsPosted, ")
                .Append("   CreateDate, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   PostRefundId
            db.AddParameter("@TransactionId", PostRefundInfo.PostRefundId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StuEnrollId
            db.AddParameter("@StuEnrollId", PostRefundInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampusId
            If PostRefundInfo.CampusId = Guid.Empty.ToString Then
                db.AddParameter("@CampusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CampusId", PostRefundInfo.CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   TermId
            If PostRefundInfo.TermId = Guid.Empty.ToString Then
                db.AddParameter("@TermId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TermId", PostRefundInfo.TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Date
            db.AddParameter("@TransDate", PostRefundInfo.PostRefundDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TransCodeId
            If PostRefundInfo.TransCodeId = Guid.Empty.ToString Then
                db.AddParameter("@TransCodeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TransCodeId", PostRefundInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Reference
            db.AddParameter("@TransReference", PostRefundInfo.Reference, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Transaction Description
            db.AddParameter("@TransDescrip", PostRefundInfo.PostRefundDescription, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   AcademicYearId
            If PostRefundInfo.AcademicYearId = Guid.Empty.ToString Then
                db.AddParameter("@AcademicYearId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AcademicYearId", PostRefundInfo.AcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Amount
            db.AddParameter("@TransAmount", -PostRefundInfo.Amount, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

            '   TransTypeId
            db.AddParameter("@TransTypeId", PostRefundInfo.TransTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   IsPosted
            db.AddParameter("@IsPosted", PostRefundInfo.IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   create timestamp
            Dim rightNow As DateTime = Date.Now

            '   CreateDate
            db.AddParameter("@CreateDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   add Refund
            '   build the query
            sb = New StringBuilder
            With sb
                .Append("INSERT saRefunds (TransactionId, RefundTypeId, ")
                .Append("   FundSourceId, ")
                .Append("   ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?) ")
            End With

            '   add parameters values to the query
            db.ClearParameters()

            '   PostRefundId
            db.AddParameter("@TransactionId", PostRefundInfo.PostRefundId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   RefundTypeId
            db.AddParameter("@RefundTypeId", PostRefundInfo.RefundTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ScheduledRefund
            'db.AddParameter("@ScheduledRefund", PostRefundInfo.ScheduledRefund, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   StudentAwardId
            'If PostRefundInfo.StudentAwardId = Guid.Empty.ToString Then
            '    db.AddParameter("@StudentAwardId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'Else
            '    db.AddParameter("@StudentAwardId", PostRefundInfo.StudentAwardId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'End If

            If PostRefundInfo.FundSourceId = Guid.Empty.ToString Then
                db.AddParameter("@FundSourceId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@FundSourceId", PostRefundInfo.FundSourceId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            ''   BankAcctId
            'If PostRefundInfo.BankAcctId = Guid.Empty.ToString Then
            '    db.AddParameter("@BankAcctId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'Else
            '    db.AddParameter("@BankAcctId", PostRefundInfo.BankAcctId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   add ReversedTransaction
            sb = New StringBuilder
            With sb
                .Append("INSERT saReversedTransactions (TransactionId, ReversedTransactionId, ")
                .Append("   ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?) ")
            End With

            '   add parameters values to the query
            db.ClearParameters()

            '   PostRefundId
            db.AddParameter("@TransactionId", PostRefundInfo.PostRefundId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   OriginalTransactionId
            db.AddParameter("@OriginalTransactionId", PostRefundInfo.OriginalTransactionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   commit transaction 
            groupTrans.Commit()

            'Close Connection
            db.CloseConnection()

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()
            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            End If
            '   return an error to the client
            'Return DALExceptions.BuildErrorMessage(ex)
            Return ex.Message

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function AddPostRefundInfo(ByVal postRefundInfo As PostRefundInfo, ByVal postedRefunds As String, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()


        '   do an insert
        Try
            Dim refunds As String() = postedRefunds.Split(",")
            For i As Integer = 0 To refunds.Length - 1
                If postedRefunds.Length > 0 Then
                    Dim refundField() = refunds(i).Split(";")
                    If i > 0 Then
                        postRefundInfo.PostRefundId = Guid.NewGuid().ToString()
                    End If
                    postRefundInfo.Amount = -Decimal.Parse(refundField(1))
                    postRefundInfo.StudentAwardId = refundField(0)
                    postRefundInfo.FundSourceId = refundField(2)
                    postRefundInfo.RefundTypeId = refundField(3)
                End If

                '   build the query
                Dim sb As New StringBuilder
                With sb
                    .Append("INSERT saTransactions (TransactionId, StuEnrollId, CampusId, TermId, ")
                    .Append("   TransDate, TransCodeId, TransReference, TransDescrip, AcademicYearId, ")
                    .Append("   TransAmount, TransTypeId, IsPosted, ")
                    .Append("   CreateDate, ModUser, ModDate) ")
                    .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                End With

                '   add parameters values to the query
                db.ClearParameters()

                '   PostRefundId
                db.AddParameter("@TransactionId", postRefundInfo.PostRefundId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   StuEnrollId
                db.AddParameter("@StuEnrollId", postRefundInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   CampusId
                If postRefundInfo.CampusId = Guid.Empty.ToString Then
                    db.AddParameter("@CampusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@CampusId", postRefundInfo.CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                '   TermId
                If postRefundInfo.TermId = Guid.Empty.ToString Then
                    db.AddParameter("@TermId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@TermId", postRefundInfo.TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                '   Transaction Date
                db.AddParameter("@TransDate", postRefundInfo.PostRefundDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   TransCodeId
                If postRefundInfo.TransCodeId = Guid.Empty.ToString Then
                    db.AddParameter("@TransCodeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@TransCodeId", postRefundInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                '   Transaction Reference
                db.AddParameter("@TransReference", postRefundInfo.Reference, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   Transaction Description
                db.AddParameter("@TransDescrip", postRefundInfo.PostRefundDescription, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   AcademicYearId
                If postRefundInfo.AcademicYearId = Guid.Empty.ToString Then
                    db.AddParameter("@AcademicYearId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@AcademicYearId", postRefundInfo.AcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                '   Transaction Amount
                db.AddParameter("@TransAmount", -postRefundInfo.Amount, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

                '   TransTypeId
                db.AddParameter("@TransTypeId", postRefundInfo.TransTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                '   IsPosted
                db.AddParameter("@IsPosted", postRefundInfo.IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                '   create timestamp
                Dim rightNow As DateTime = Date.Now

                '   CreateDate
                db.AddParameter("@CreateDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   ModDate
                db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

                '   add Refund
                '   build the query
                sb = New StringBuilder
                With sb
                    .Append("INSERT saRefunds (TransactionId, RefundTypeId, StudentAwardId, ")
                    .Append("   FundSourceId, IsInstCharge, BankAcctId, ")
                    .Append("   ModUser, ModDate) ")
                    .Append("VALUES (?,?,?,?,?,?,?,?) ")
                End With

                '   add parameters values to the query
                db.ClearParameters()

                '   PostRefundId
                db.AddParameter("@TransactionId", postRefundInfo.PostRefundId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   RefundTypeId
                db.AddParameter("@RefundTypeId", postRefundInfo.RefundTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                '   ScheduledRefund
                'db.AddParameter("@ScheduledRefund", PostRefundInfo.ScheduledRefund, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                '   studentAwardId
                If postRefundInfo.StudentAwardId = Guid.Empty.ToString Then
                    db.AddParameter("@StudentAwardId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@StudentAwardId", postRefundInfo.StudentAwardId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                If postRefundInfo.FundSourceId = Guid.Empty.ToString Then
                    db.AddParameter("@FundSourceId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@FundSourceId", postRefundInfo.FundSourceId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                '   IsInstCharge
                db.AddParameter("@IsInstCharge", postRefundInfo.IsInstCharge, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                '   BankAcctId
                If postRefundInfo.BankAcctId = Guid.Empty.ToString Then
                    db.AddParameter("@BankAcctId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@BankAcctId", postRefundInfo.BankAcctId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                '   ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   ModDate
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
            Next

            '   commit transaction 
            groupTrans.Commit()

            'Close Connection
            db.CloseConnection()

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()
            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            End If
            '   return an error to the client
            'Return DALExceptions.BuildErrorMessage(ex)
            Return ex.Message

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    ''Function Added by Kamalesh Ahuja on 15 Jan 2010 to Resolve Mantis Issue Id 12038 and 08425
    Public Function AddPostRefundInfoNew(ByVal postRefundInfo As PostRefundInfo, ByVal postedRefunds As String, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()


        '   do an insert
        Try
            Dim refunds As String() = postedRefunds.Split(":")
            For i As Integer = 0 To refunds.Length - 1
                If postedRefunds.Length > 0 Then
                    Dim refundField() = refunds(i).Split(";")
                    If i > 0 Then
                        postRefundInfo.PostRefundId = Guid.NewGuid().ToString()
                    End If
                    postRefundInfo.Amount = -Decimal.Parse(refundField(1))
                    postRefundInfo.StudentAwardId = refundField(0)
                    postRefundInfo.FundSourceId = refundField(2)
                    postRefundInfo.RefundTypeId = refundField(3)
                    postRefundInfo.AwardScheduleId = refundField(4)
                End If

                '   build the query
                Dim sb As New StringBuilder
                With sb
                    .Append("INSERT saTransactions (TransactionId, StuEnrollId, CampusId, TermId, ")
                    .Append("   TransDate, TransCodeId, TransReference, TransDescrip, AcademicYearId, ")
                    .Append("   TransAmount, TransTypeId, IsPosted, ")
                    .Append("   CreateDate, ModUser, ModDate) ")
                    .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                End With

                '   add parameters values to the query
                db.ClearParameters()

                '   PostRefundId
                db.AddParameter("@TransactionId", postRefundInfo.PostRefundId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   StuEnrollId
                db.AddParameter("@StuEnrollId", postRefundInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   CampusId
                If postRefundInfo.CampusId = Guid.Empty.ToString Then
                    db.AddParameter("@CampusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@CampusId", postRefundInfo.CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                '   TermId
                If postRefundInfo.TermId = Guid.Empty.ToString Then
                    db.AddParameter("@TermId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@TermId", postRefundInfo.TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                '   Transaction Date
                db.AddParameter("@TransDate", postRefundInfo.PostRefundDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   TransCodeId
                If postRefundInfo.TransCodeId = Guid.Empty.ToString Then
                    db.AddParameter("@TransCodeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@TransCodeId", postRefundInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                '   Transaction Reference
                db.AddParameter("@TransReference", postRefundInfo.Reference, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   Transaction Description
                db.AddParameter("@TransDescrip", postRefundInfo.PostRefundDescription, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   AcademicYearId
                If postRefundInfo.AcademicYearId = Guid.Empty.ToString Then
                    db.AddParameter("@AcademicYearId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@AcademicYearId", postRefundInfo.AcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                '   Transaction Amount
                db.AddParameter("@TransAmount", -postRefundInfo.Amount, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

                '   TransTypeId
                db.AddParameter("@TransTypeId", postRefundInfo.TransTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                '   IsPosted
                db.AddParameter("@IsPosted", postRefundInfo.IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                '   create timestamp
                Dim rightNow As DateTime = Date.Now

                '   CreateDate
                db.AddParameter("@CreateDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   ModDate
                db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

                '   add Refund
                '   build the query
                sb = New StringBuilder



                With sb
                    .Append("INSERT saRefunds (TransactionId, RefundTypeId, StudentAwardId, ")
                    .Append("   FundSourceId, IsInstCharge, BankAcctId, ")
                    .Append("   ModUser, ModDate,AwardScheduleId,RefundAmount) ")
                    .Append("VALUES (?,?,?,?,?,?,?,?,?,?); ")
                End With

                '   add parameters values to the query
                db.ClearParameters()

                '   PostRefundId
                db.AddParameter("@TransactionId", postRefundInfo.PostRefundId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   RefundTypeId
                db.AddParameter("@RefundTypeId", postRefundInfo.RefundTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                '   ScheduledRefund
                'db.AddParameter("@ScheduledRefund", PostRefundInfo.ScheduledRefund, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                '   studentAwardId
                If (postRefundInfo.StudentAwardId = Guid.Empty.ToString) Or (postRefundInfo.StudentAwardId = "") Then
                    db.AddParameter("@StudentAwardId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@StudentAwardId", postRefundInfo.StudentAwardId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                If (postRefundInfo.FundSourceId = Guid.Empty.ToString) Or (postRefundInfo.FundSourceId = "") Then
                    db.AddParameter("@FundSourceId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@FundSourceId", postRefundInfo.FundSourceId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                '   IsInstCharge
                db.AddParameter("@IsInstCharge", postRefundInfo.IsInstCharge, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                '   BankAcctId
                If (postRefundInfo.BankAcctId = Guid.Empty.ToString) Or (postRefundInfo.BankAcctId = "") Then
                    db.AddParameter("@BankAcctId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@BankAcctId", postRefundInfo.BankAcctId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                '   ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   ModDate
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                If (postRefundInfo.AwardScheduleId = Guid.Empty.ToString) Or (postRefundInfo.AwardScheduleId = "") Then
                    '   AwardScheduleId
                    db.AddParameter("@AwardScheduleId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    '   AwardScheduleId
                    db.AddParameter("@AwardScheduleId", postRefundInfo.AwardScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                '   Refund Amount
                db.AddParameter("@RefundAmount", -postRefundInfo.Amount, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)



                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
            Next

            '   commit transaction 
            groupTrans.Commit()

            'Close Connection
            db.CloseConnection()

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()
            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            End If
            '   return an error to the client
            'Return DALExceptions.BuildErrorMessage(ex)
            If ex.Message.Contains("FK_saRefunds_saTransactions") Then Return "Cannot save the transaction since a similar transaction has already been posted for the same day."
            Return ex.Message

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function DeletePostRefundInfo(ByVal postRefundId As String, ByVal modDate As DateTime, ByVal modDate1 As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saRefunds ")
                .Append("WHERE TransactionId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("Select count(*) from saRefunds where TransactionId = ? ")
            End With

            '   add parameters values to the query

            '   TransactionId
            db.AddParameter("@TransactionId", postRefundId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TransactionId
            db.AddParameter("@TransactionId", postRefundId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If Not rowCount = 0 Then

                '   rollback transaction
                groupTrans.Rollback()

                '   return an error message
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

            '   build the query
            sb = New StringBuilder
            With sb
                .Append("DELETE FROM saTransactions ")
                .Append("WHERE PostRefundId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("Select count(*) from saTransactions where PostRefundId = ? ")
            End With

            '   add parameters values to the query
            db.ClearParameters()

            '   TransactionId
            db.AddParameter("@TransactionId", postRefundId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TransactionId
            db.AddParameter("@TransactionId", postRefundId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   commit transaction 
                groupTrans.Commit()

                '   return without errors
                Return ""
            Else

                '   rollback transaction
                groupTrans.Rollback()

                '   return an error message
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()
            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            End If
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)


        Finally
            'Close Connection
            db.CloseConnection()
        End Try


    End Function
    'Public Function SaveXmlDocumentTest(ByVal guid As String, ByVal xmlAsVarChar As String) As String

    '    '   Connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   do an insert
    '    '   build the query
    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append("INSERT saXmlTests (XmlTestId, XmlAsVarChar, XmlAsText) ")
    '        .Append("VALUES (?,?,?) ")
    '    End With

    '    '   add parameters values to the query

    '    '   XmlTestId
    '    db.AddParameter("@XmlTestId", guid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    '   xmlAsVarChar
    '    db.AddParameter("@XmlAsVarChar", xmlAsVarChar, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    '   StuEnrollId
    '    db.AddParameter("@XmlAsText", xmlAsVarChar, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    '   execute the query
    '    db.RunParamSQLExecuteNoneQuery(sb.ToString)

    '    'Close Connection
    '    db.CloseConnection()

    '    '   return without errors
    '    Return ""

    '    'Close Connection
    '    db.CloseConnection()

    'End Function
    'Public Function GetXmlDocumentTest(ByVal guid As String) As System.Xml.XmlDocument

    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   build the sql query
    '    Dim sb As New System.Text.StringBuilder
    '    With sb
    '        .Append("SELECT ")
    '        '.Append("       XmlAsVarChar, ")
    '        .Append("       XmlAsText ")
    '        .Append("FROM   saXmlTests ")
    '        .Append("WHERE  XmlTestId= ? ")
    '    End With

    '    ' Add guid to the parameter list
    '    db.AddParameter("@XmlTestId", guid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    '   Execute the query
    '    Dim xmlDocument As New System.Xml.XmlDocument
    '    xmlDocument.LoadXml(db.RunParamSQLScalar(sb.ToString))

    '    'Close Connection
    '    db.CloseConnection()

    '    'Return xmlDocument
    '    Return xmlDocument

    'End Function

    'Private Sub DisplayOleDbErrorCollection(ByVal myException As OleDbException)
    '    Dim i As Integer, str As String
    '    For i = 0 To myException.Errors.Count - 1
    '        str += "Index #" + i.ToString() + ControlChars.Cr _
    '           + "Message: " + myException.Errors(i).Message + ControlChars.Cr _
    '           + "Native: " + myException.Errors(i).NativeError.ToString() + ControlChars.Cr _
    '           + "Source: " + myException.Errors(i).Source + ControlChars.Cr _
    '           + "SQL: " + myException.Errors(i).SQLState + ControlChars.Cr
    '    Next i
    'End Sub
    'Private Function ReturnSQLError(ByVal ex As OleDb.OleDbException) As String
    '    If ex.Errors(ex.Errors.Count - 1).NativeError.ToString = "547" Then Return -2
    '    If ex.Errors(ex.Errors.Count - 1).NativeError.ToString = "3621" Then Return -3
    '    Return -1
    'End Function
    ''Modified by Saraswathi On july 20 2009
    ''For mantis case 16590
    ''Include Voids is added to the search list
    ''Include voids modified to Voids list having three options shoow only voids, do not show void and show all
    ''Modified on august 18 2009 by Saraswathi lakshmanan

    Public Function SearchTransactions(ByVal campusId As String, ByVal transCodeId As String, ByVal transDateFrom As Date, ByVal transDateTo As Date, ByVal transAmountFrom As Decimal, ByVal transAmountTo As Decimal, ByVal description As String, ByVal reference As String, ByVal termId As String, ByVal academicYearId As String, ByVal charges As Boolean, ByVal payments As Boolean, ByVal debitAdjustments As Boolean, ByVal creditAdjustments As Boolean, ByVal Voidlist As Integer) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT TransactionId, ")
            .Append("    (Select StudentId from arStuEnrollments where StuEnrollId=T.StuEnrollId) StudentId, ")
            .Append("    (Select (FirstName + ' ' + LastName) As StudentName from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=T.StuEnrollId)) StudentName, ")
            .Append("    T.TransCodeId, ")
            .Append("    (Select TransCodeDescrip from saTransCodes where TransCodeId=T.TransCodeId) As TransCodeDescrip, ")
            .Append("    (Select CampGrpId from saTransCodes where TransCodeId=T.TransCodeid) As CampGrpId, ")
            .Append("    T.TransDescrip, ")
            .Append("    T.TransDate, ")
            .Append("    T.TransAmount, ")
            .Append("    T.TransReference, ")
            .Append("    T.AcademicYearId, ")
            .Append("    (Select AcademicYearDescrip from saAcademicYears where AcademicYearId=T.AcademicYearId) As AcademicYearDescrip, ")
            .Append("    T.TermId, ")
            .Append("    (Select TermDescrip from arTerm where TermId=T.TermId) As TermDescrip, ")
            .Append("    T.TransTypeId, ")
            .Append("    T.IsPosted, ")
            ''Added by Saraswathi lakshmanan on August 18 2009
            .Append("    T.Voided, ")
            .Append("    (Select count(*) from saPayments where TransactionId=T.TransactionId) As IsPayment, ")
            .Append("    (Select count(*) from saRefunds where TransactionId=T.TransactionId) As IsRefund, ")
            '.Append("    (Select Case T.TransTypeId When 0 then 'Charge' else 'Adjustment' end) As TransTypeDescrip, ")
            .Append("    (Select Case T.TransTypeId When 0 then 'Charge' when 2 then 'Payment' when 1 then 'Adjustment' end) As TransTypeDescrip, ")
            .Append("    T.ModUser ")
            .Append("FROM  saTransactions T ")
            .Append("WHERE ")
            ''Modified by Saraswathi On july 20 2009
            ''For mantis case 16590
            ''Include Voids is added to the search list
            'If IncludeVoids = True Then
            '    .Append("       T.Voided in (0,1)  ")
            'Else
            '    .Append("       T.Voided=0  ")
            'End If

            If Voidlist = 1 Then ''Show all including Voids
                .Append("       T.Voided in (0,1)  ")
            ElseIf Voidlist = 0 Then ''Do not Show voids
                .Append("       T.Voided=0  ")
            ElseIf Voidlist = 2 Then ''Show only voids
                .Append("       T.Voided=1  ")
            End If

            '   the relationship logic between checkboxes is "OR" instead of "AND". Only the first one is AND
            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "All Campus Groups"
            If Not campusId = Guid.Empty.ToString Then
                .Append(andOrOrOperator)
                .Append("T.CampusId = ? ")
                db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "All TransCodes"
            If Not transCodeId = Guid.Empty.ToString Then
                .Append(andOrOrOperator)
                .Append("T.TransCodeId = ? ")
                db.AddParameter("@TransCodeId", transCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            '   check for Transaction Date From
            If Not transDateFrom = Date.MinValue Then
                .Append(andOrOrOperator)
                .Append("T.TransDate >= ? ")
                db.AddParameter("@TransDateFrom", transDateFrom, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            '   check for Transaction Date To
            If Not transDateTo = Date.MaxValue Then
                .Append(andOrOrOperator)
                .Append("T.TransDate <= ? ")
                db.AddParameter("@TransDateTo", transDateTo, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            '   check for Transaction Amount From
            If Not transAmountFrom = 0 Then
                If transAmountFrom < 0 Then transAmountFrom = transAmountFrom * (-1.0)
                .Append(andOrOrOperator)
                .Append("ABS(T.TransAmount) >= ABS(?) ")
                db.AddParameter("@TransAmountFrom", transAmountFrom, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            '   check for Transaction Amount To
            If Not transAmountTo = 0 Then
                If transAmountTo < 0 Then transAmountTo = transAmountTo * (-1.0)
                .Append(andOrOrOperator)
                .Append("ABS(T.TransAmount) <= ABS(?) ")
                db.AddParameter("@TransAmountTo", transAmountTo, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            '   check for Description
            If Not description = "" Then
                .Append(andOrOrOperator)
                .Append("T.TransDescrip like '%' + ? + '%'")
                db.AddParameter("@Description", description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            '   check for Reference
            If Not reference = "" Then
                .Append(andOrOrOperator)
                .Append("T.TransReference like '%' + ? + '%'")
                db.AddParameter("@Reference", reference, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "All Terms"
            If Not termId = Guid.Empty.ToString Then
                .Append(andOrOrOperator)
                .Append("T.TermId = ? ")
                db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "All Academic Years"
            If Not academicYearId = Guid.Empty.ToString Then
                .Append(andOrOrOperator)
                .Append("T.AcademicYearId = ? ")
                db.AddParameter("@AcademicYearId", academicYearId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            '   1=2 forces all following relationships to be "OR"
            .Append(andOrOrOperator + "(1=2")

            '   check for charges
            If Not charges = False Then
                .Append(" OR (T.TransTypeId=0 AND T.TransAmount > 0) ")
            End If

            '   check for payments
            If Not payments = False Then
                .Append(" OR (T.TransTypeId=2 AND T.TransAmount < 0) ")
            End If

            '   check for DebitAdjustments
            If Not debitAdjustments = False Then
                .Append(" OR (T.TransTypeId=1 AND T.TransAmount > 0) ")
            End If

            '   check for CreditAdjustments
            If Not creditAdjustments = False Then
                .Append(" OR (T.TransTypeId=1 AND T.TransAmount < 0) ")
            End If

            '   Close ( .. OR  .. OR .. ) relationship
            .Append(") ")

            '   always add order
            .Append("ORDER BY T.TransDate, T.ModDate ")
        End With

        '   Execute the query and return Dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetGLDistributionsDS(ByVal transactionId As String) As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   create dataset
        Dim ds As New DataSet

        '   build select query for the GLDistributions data adapter
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       GLDistributionId, ")
            .Append("       GLDate, ")
            .Append("       GLAmount, ")
            .Append("       GLAccountId, ")
            .Append("       GLDescrip, ")
            .Append("       ViewOrder, ")
            .Append("       ModUser, ")
            .Append("       ModDate, ")
            .Append("       TransactionId ")
            .Append("FROM   saGLDistributions ")
            .Append("WHERE  (TransactionId = ?) ")
        End With

        '   build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
        sc.Parameters.Add(New OleDbParameter("TransactionId", transactionId))

        '   Create adapter to handle GLDistributions table
        Dim da As New OleDbDataAdapter(sc)

        '   Fill GLDistributions table
        da.Fill(ds, "GLDistributions")

        '   build select query for the GLAccounts data adapter
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       GLAccountId, ")
            .Append("       GLAccountCode, ")
            .Append("       GLAccountDescription ")
            .Append("FROM   saGLAccounts ")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   fill GLAccounts table
        da.Fill(ds, "GLAccounts")

        '   create primary and foreign key constraints

        '   set primary key for GLDistributions table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("GLDistributions").Columns("GLDistributionId")
        ds.Tables("GLDistributions").PrimaryKey = pk0

        '   set primary key for GLAccounts table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("GLAccounts").Columns("GLAccountId")
        ds.Tables("GLAccounts").PrimaryKey = pk1

        '   get foreign key column in GLDistributions
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("GLDistributions").Columns("GLAccountId")

        '   set foreign key in GLAccounts table
        ds.Relations.Add("GLAccountsGLDistributions", pk1, fk0)

        '   return dataset
        Return ds

    End Function
    Public Function UpdateGLDistributionsDS(ByVal ds As DataSet) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   all updates must be encapsulated as one transaction
        Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            '   build select query for the GLDistributions data adapter
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("       GLDistributionId, ")
                .Append("       GLDate, ")
                .Append("       GLAmount, ")
                .Append("       GLAccountId, ")
                .Append("       GLDescrip, ")
                .Append("       ViewOrder, ")
                .Append("       ModUser, ")
                .Append("       ModDate, ")
                .Append("       TransactionId ")
                .Append("FROM   saGLDistributions ")
                '.Append("WHERE  (TransactionId = ?) ")
            End With

            '   build select command
            Dim glDistributionsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle GLDistributions table
            Dim glDistributionsDataAdapter As New OleDbDataAdapter(glDistributionsSelectCommand)

            '   build insert, update and delete commands for GLDistributions table
            Dim cb As New OleDbCommandBuilder(glDistributionsDataAdapter)

            '   build select query for the GLAccounts data adapter
            sb = New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("       GLAccountId, ")
                .Append("       Description ")
                .Append("FROM   saGLAccounts ")
            End With

            '   build select command
            Dim glAccountsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle GLAccounts table
            Dim glAccountsDataAdapter As New OleDbDataAdapter(glAccountsSelectCommand)

            '   build insert, update and delete commands for glAccounts table
            Dim cb1 As New OleDbCommandBuilder(glAccountsDataAdapter)

            '   insert added rows in GLAccounts table
            glAccountsDataAdapter.Update(ds.Tables("GLAccounts").Select(Nothing, Nothing, DataViewRowState.Added))

            '   insert added rows in GLDistributions table
            glDistributionsDataAdapter.Update(ds.Tables("GLDistributions").Select(Nothing, Nothing, DataViewRowState.Added))

            '   delete rows in GLDistributions table
            glDistributionsDataAdapter.Update(ds.Tables("GLDistributions").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   delete rows in glAccounts table
            glAccountsDataAdapter.Update(ds.Tables("GLAccounts").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   update rows in GLDistributions table
            glDistributionsDataAdapter.Update(ds.Tables("GLDistributions").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   update rows in glAccounts table
            glAccountsDataAdapter.Update(ds.Tables("GLAccounts").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            Return ""

        Catch ex As OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally

            '   close connection
            connection.Close()
        End Try

    End Function
    Public Function GetCommonTransactionInfo(ByVal CommonTransactionId As String) As CommonTransactionInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT TransactionId As CommonTransactionId, ")
            .Append("    (Select StudentId from arStuEnrollments where StuEnrollId=T.StuEnrollId) StudentId, ")
            .Append("    (Select (FirstName + ' ' + LastName) As StudentName from arStudent where StudentId=(select StudentId from arStuEnrollments where StuEnrollId=T.StuEnrollId)) StudentName, ")
            .Append("    T.StuEnrollId, ")
            .Append("    (Select PrgVerDescrip from arPrgVersions where PrgVerId=(select PrgVerId from arStuEnrollments where StuEnrollId=T.StuEnrollId)) as EnrollmentDescrip, ")
            .Append("    T.TransCodeId, ")
            .Append("    (Select TransCodeDescrip from saTransCodes where TransCodeId=T.TransCodeId) As TransCodeDescrip, ")
            .Append("    T.TransDescrip, ")
            .Append("    T.TransDate, ")
            .Append("    T.TransAmount, ")
            .Append("    T.TransReference, ")
            .Append("    T.AcademicYearId, ")
            .Append("    (Select AcademicYearDescrip from saAcademicYears where AcademicYearId=T.AcademicYearId) As AcademicYearDescrip, ")
            .Append("    T.TermId, ")
            .Append("    (Select TermDescrip from arTerm where TermId=T.TermId) As TermDescrip, ")
            .Append("    T.TransTypeId, ")
            .Append("    T.ModUser, ")
            .Append("    T.ModDate ")
            .Append("FROM  saTransactions T ")
            .Append("WHERE T.TransactionId= ? ")
            .Append("AND T.Voided=0 ")
        End With

        ' Add the CommonTransactionId to the parameter list
        db.AddParameter("@TransactionId", CommonTransactionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim CommonTransactionInfo As New CommonTransactionInfo

        While dr.Read()

            '   set properties with data from DataReader
            With CommonTransactionInfo
                .IsInDB = True
                .CommonTransactionId = CommonTransactionId
                .StudentId = CType(dr("StudentId"), Guid).ToString
                .StudentName = dr("StudentName")
                .StuEnrollId = CType(dr("StuEnrollId"), Guid).ToString
                .EnrollmentDescription = dr("EnrollmentDescrip")
                .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                .TransCodeDescription = dr("TransCodeDescrip")
                .CommonTransactionDescription = dr("TransDescrip")
                .CommonTransactionDate = dr("TransDate")
                .Amount = dr("TransAmount")
                If Not dr("TransReference") Is DBNull.Value Then .Reference = dr("TransReference")
                If Not dr("AcademicYearId") Is DBNull.Value Then .AcademicYearId = CType(dr("AcademicYearId"), Guid).ToString
                If Not dr("AcademicYearDescrip") Is DBNull.Value Then .AcademicYearDescrip = dr("AcademicYearDescrip")
                If Not dr("TermId") Is DBNull.Value Then .TermId = CType(dr("TermId"), Guid).ToString
                If Not dr("TermDescrip") Is DBNull.Value Then .TermDescrip = dr("TermDescrip")
                .TransTypeId = dr("TransTypeId")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return CommonTransactionInfo

    End Function
    Public Function UpdateCommonTransactionInfo(ByVal commonTransactionInfo As CommonTransactionInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saTransactions Set StuEnrollId = ?, TermId = ?, ")
                .Append("   TransDate = ?, TransCodeId = ?, TransReference = ?, TransDescrip = ?, ")
                .Append("   AcademicYearId = ?, TransAmount = ?, TransTypeId = ?, IsPosted = ?, ")
                .Append("   ModUser = ?, ModDate = ? ")
                .Append("WHERE TransactionId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saTransactions where ModDate = ? ")
            End With

            '   add parameters values to the query
            '   StuEnrollId
            db.AddParameter("@StuEnrollId", commonTransactionInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TermId
            If commonTransactionInfo.TermId = Guid.Empty.ToString Then
                db.AddParameter("@TermId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TermId", commonTransactionInfo.TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Date
            db.AddParameter("@TransDate", commonTransactionInfo.CommonTransactionDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TransCodeId
            db.AddParameter("@TransCodeId", commonTransactionInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Transaction Reference
            db.AddParameter("@TransReference", commonTransactionInfo.Reference, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Transaction Description
            db.AddParameter("@TransDescrip", commonTransactionInfo.CommonTransactionDescription, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   AcademicYearId
            If commonTransactionInfo.AcademicYearId = Guid.Empty.ToString Then
                db.AddParameter("@AcademicYearId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AcademicYearId", commonTransactionInfo.AcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Amount
            db.AddParameter("@TransAmount", commonTransactionInfo.Amount, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

            '   TransTypeId
            db.AddParameter("@TransTypeId", commonTransactionInfo.TransTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   IsPosted
            db.AddParameter("@IsPosted", commonTransactionInfo.IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   CommonTransactionId
            db.AddParameter("@TransactionId", commonTransactionInfo.CommonTransactionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", commonTransactionInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddCommonTransactionInfo(ByVal commonTransactionInfo As CommonTransactionInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saTransactions (TransactionId, StuEnrollId, TermId, ")
                .Append("   TransDate, TransCodeId, TransReference, TransDescrip, AcademicYearId, ")
                .Append("   TransAmount, TransTypeId, IsPosted, ")
                .Append("   CreateDate, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   CommonTransactionId
            db.AddParameter("@TransactionId", commonTransactionInfo.CommonTransactionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StuEnrollId
            db.AddParameter("@StuEnrollId", commonTransactionInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TermId
            If commonTransactionInfo.TermId = Guid.Empty.ToString Then
                db.AddParameter("@TermId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TermId", commonTransactionInfo.TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Date
            db.AddParameter("@TransDate", commonTransactionInfo.CommonTransactionDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TransCodeId
            db.AddParameter("@TransCodeId", commonTransactionInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Transaction Reference
            db.AddParameter("@TransReference", commonTransactionInfo.Reference, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Transaction Description
            db.AddParameter("@TransDescrip", commonTransactionInfo.CommonTransactionDescription, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   AcademicYearId
            If commonTransactionInfo.AcademicYearId = Guid.Empty.ToString Then
                db.AddParameter("@AcademicYearId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AcademicYearId", commonTransactionInfo.AcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Transaction Amount
            db.AddParameter("@TransAmount", commonTransactionInfo.Amount, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

            '   TransTypeId
            db.AddParameter("@TransTypeId", commonTransactionInfo.TransTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   IsPosted
            db.AddParameter("@IsPosted", commonTransactionInfo.IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   create timestamp
            Dim rightNow As DateTime = Date.Now

            '   CreateDate
            db.AddParameter("@CreateDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteCommonTransactionInfo(ByVal commonTransactionId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saTransactions ")
                .Append("WHERE TransactionId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("Select count (*) from saTransactions where TransactionId = ? ")
            End With

            '   add parameters values to the query

            '   TransactionId
            db.AddParameter("@TransactionId", commonTransactionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TransactionId
            db.AddParameter("@TransactionId", commonTransactionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetGLDistributionBetweenDates(ByVal startDate As Date, ByVal endDate As Date) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("       TCGLA.GLAccount, ")
            .Append("       T.TransDate, ")
            .Append("       T.TransReference, ")
            .Append("       T.TransDescrip, ")
            .Append("       (SELECT CASE TypeId WHEN 0 then TransAmount*Percentage/100 ELSE TransAmount*Percentage/100 * (- 1) END) Amount ")
            .Append("FROM  ")
            .Append("       saTransCodeGLAccounts TCGLA, ")
            .Append("       saTransCodes TC, ")
            .Append("       saTransactions T ")
            .Append("WHERE  ")
            .Append("       TCGLA.TransCodeId = TC.TransCodeId ")
            .Append("AND    T.TransCodeId=TC.TransCodeId ")
            .Append("AND    T.Voided=0 ")

            If startDate > Date.MinValue Then
                .Append("AND        T.TransDate >= ? ")
            End If
            If endDate < Date.MaxValue Then
                .Append("AND        T.TransDate <= ? ")
            End If
            .Append("ORDER BY T.TransDate, T.ModDate ")
        End With

        ' Add StartDate to the parameter list
        If startDate > Date.MinValue Then
            db.AddParameter("@StartDate", startDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If

        ' Add EndDate to the parameter list
        If endDate < Date.MaxValue Then
            db.AddParameter("@EndDate", endDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If

        '   Execute the query and return Dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    'Public Function SearchGLDistributions(ByVal campGrpId As String, ByVal transDateFrom As Date, ByVal transDateTo As Date) As DataSet

    '    '   connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append("SELECT  ")
    '        .Append("        (Select '" + transDateTo.Month.ToString + "/" + transDateTo.Day.ToString + "/" + transDateTo.Year.ToString + "') Date, ")
    '        .Append("	     (Select TransCodeDescrip from saTransCodes where TransCodeId=TX.TransCodeId) TransCodeDescrip,   ")
    '        .Append("	     GLAccount,   ")
    '        .Append("        (SELECT CASE TypeId WHEN 0 then (Sum(PlusAmount) + Sum(MinusAmount))*(Percentage/100) WHEN 1 then (Sum(PlusAmount) + Sum(MinusAmount))*(Percentage/100) * (- 1) END) Amount  ")
    '        .Append(",TransCode FROM  ")
    '        .Append("(	  ")
    '        .Append("	SELECT  ")
    '        .Append("			TC.TransCodeId,  ")
    '        .Append("			TCGLA.GLAccount,   ")
    '        .Append("			TCGLA.TypeId,   ")
    '        .Append("			TCGLA.Percentage,  ")
    '        .Append("			(case  when T.TransAmount > 0 then T.TransAmount else 0.00 end) PlusAmount,  ")
    '        .Append("			(case  when T.TransAmount < 0 then T.TransAmount else 0.00 end) MinusAmount,  ")
    '        .Append("			(case  when T.TransAmount > 0 then 1 else 0 end) Signo,TC.TransCodeDescrip as TransCode  ")
    '        .Append("	  FROM    saTransactions T, saTransCodeGLAccounts TCGLA, saTransCodes TC    ")
    '        .Append("	  WHERE   TCGLA.TransCodeId=TC.TransCodeId    ")
    '        .Append("	  AND     T.TransCodeId=TC.TransCodeId    ")
    '        .Append("	  AND     T.Voided=0   ")
    '        .Append("	  AND	 TCGLA.TypeID IN (0,1)  ")

    '        '   check if the selection is "All Campus Groups"
    '        If Not campGrpId = Guid.Empty.ToString Then
    '            .Append(" AND T.CampusId IN (Select CampusId from syCmpGrpCmps where CampGrpId = ?) ")
    '            db.AddParameter("@CampGrpId", campGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        '   check for Transaction Date From
    '        If Not transDateFrom = Date.MinValue Then
    '            .Append(" AND T.TransDate >= ? ")
    '            db.AddParameter("@TransDateFrom", transDateFrom, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        End If

    '        '   check for Transaction Date To
    '        If Not transDateTo = Date.MaxValue Then
    '            .Append(" AND T.TransDate <= ? ")
    '            db.AddParameter("@TransDateTo", transDateTo, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        End If
    '        .Append(") TX    ")
    '        .Append("Group By TransCodeId, GLAccount, TypeId, Percentage, Signo ,TransCode  ")
    '        '.Append("Order By TransCodeId, GLAccount, TypeId, Percentage, Signo ")

    '        .Append("UNION ALL ")
    '        .Append("SELECT  ")
    '        .Append("		DR.DefRevenueDate as Date,  ")
    '        .Append("		(Select TransCodeDescrip from saTransCodes where TransCodeId=T.TransCodeId) TransCodeDescrip,   ")
    '        .Append("		TCGLA.GLAccount, ")
    '        .Append("		(SELECT CASE TypeId WHEN 2 THEN Sum(Amount) WHEN 3 THEN Sum(Amount)*(-1.0) END) Amount,TC.TransCodeDescrip as TransCode ")
    '        .Append("from	saDeferredRevenues DR, saTransactions T, saTransCodes TC, saTransCodeGLAccounts TCGLA  ")
    '        .Append("where	DR.TransactionId=T.TransactionId ")
    '        .Append("AND    T.Voided=0 ")
    '        .Append("AND		T.TransCodeId=TC.TransCodeId ")
    '        .Append("AND		TC.TransCodeId=TCGLA.TransCodeId ")
    '        .Append("AND		TCGLA.TypeId IN (2,3) ")

    '        '   check if the selection is "All Campus Groups"
    '        If Not campGrpId = Guid.Empty.ToString Then
    '            .Append(" AND T.CampusId IN (Select CampusId from syCmpGrpCmps where CampGrpId = ?) ")
    '            db.AddParameter("@CampGrpId", campGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        '   check for Transaction Date From
    '        If Not transDateFrom = Date.MinValue Then
    '            .Append(" AND DR.DefRevenueDate >= ? ")
    '            db.AddParameter("@TransDateFrom", transDateFrom, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        End If

    '        '   check for Transaction Date To
    '        If Not transDateTo = Date.MaxValue Then
    '            .Append(" AND DR.DefRevenueDate <= ? ")
    '            db.AddParameter("@TransDateTo", transDateTo, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        End If

    '        .Append("GROUP BY DR.DefRevenueDate, T.TransCodeId, TransCodeDescrip, GLAccount, TypeId,TC.TransCodeDescrip ")

    '    End With

    '    '   Execute the query and return Dataset
    '    Return db.RunParamSQLDataSet(sb.ToString)

    'End Function

    Public Function SearchGLDistributions_new(ByVal campGrpId As String, ByVal transDateFrom As Date, ByVal transDateTo As Date) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("  SELECT CONVERT(VARCHAR(10), [Date], 101) as [Date], TransCodeId , ")
            .Append(" GLAccount , ")
            .Append("  TypeId , ")
            .Append("  Percentage , ")
            .Append("   transtypeId , ")
            .Append("cast(SUM(creditAmount) AS DECIMAL(19,4))   CreditAmount , ")
            .Append("   cast( SUM(DebitAmount) AS DECIMAL(19,4))  DebitAmount , ")
            .Append("    TransCodeDescripSummary AS TransCodeDescrip ")
            .Append(" , '' as TransCode ")
            .Append("    FROM  glTransView2 ")
            .Append("  WHERE  1=1  ")
            '   check if the selection is "All Campus Groups"
            If Not campGrpId = Guid.Empty.ToString Then
                .Append(" And CampusId IN (Select CampusId from syCmpGrpCmps where CampGrpId = ?) ")
                db.AddParameter("@CampGrpId", campGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   check for Transaction Date From
            If Not transDateFrom = Date.MinValue Then
                .Append(" AND CONVERT(VARCHAR(10), [Date], 101) >= ? ")
                db.AddParameter("@TransDateFrom", transDateFrom, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            '   check for Transaction Date To
            If Not transDateTo = Date.MaxValue Then
                .Append(" AND CONVERT(VARCHAR(10), [Date], 101) <= ? ")
                db.AddParameter("@TransDateTo", transDateTo, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If


            .Append(" GROUP BY CONVERT(VARCHAR(10), [Date], 101) ,TransCodeId , ")
            .Append(" GLAccount, ")
            .Append("    TypeId, ")
            .Append("   Percentage, ")
            .Append("   transtypeId,")
            .Append("  TransCodeDescripSummary ,TransCode ")



            '.Append("SELECT  ")
            '.Append("        (Select '" + transDateTo.Month.ToString + "/" + transDateTo.Day.ToString + "/" + transDateTo.Year.ToString + "') Date, ")
            '.Append("	     TransCodeDescripSummary,transtypeId , TypeID , ")
            '.Append("	     GLAccount,   ")
            '.Append("        CreditAmount,DebitAmount  ")
            '.Append(",TransCode FROM  ")
            '.Append("(	Select TransTypeId , transcodeId, TypeId  , Percentage,GLAccount from GlTransView2 where 1=1 ")
            ''   check if the selection is "All Campus Groups"
            'If Not campGrpId = Guid.Empty.ToString Then
            '    .Append(" And CampusId IN (Select CampusId from syCmpGrpCmps where CampGrpId = ?) ")
            '    db.AddParameter("@CampGrpId", campGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'End If

            ''   check for Transaction Date From
            'If Not transDateFrom = Date.MinValue Then
            '    .Append(" AND Date >= ? ")
            '    db.AddParameter("@TransDateFrom", transDateFrom, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'End If

            ''   check for Transaction Date To
            'If Not transDateTo = Date.MaxValue Then
            '    .Append(" AND Date <= ? ")
            '    db.AddParameter("@TransDateTo", transDateTo, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'End If
            '.Append(") TX    ")
            '.Append("Group By TransCodeId, GLAccount, TypeId, Percentage, transtypeId  ")





            '.Append("SELECT  ")
            '.Append("        (Select '" + transDateTo.Month.ToString + "/" + transDateTo.Day.ToString + "/" + transDateTo.Year.ToString + "') Date, ")
            '.Append("	     (Select TransCodeDescrip from saTransCodes where TransCodeId=TX.TransCodeId) TransCodeDescrip,transtypeId , TypeID , ")
            '.Append("	     GLAccount,   ")
            '.Append("        (SELECT CASE TypeId WHEN 0 then (Sum(PlusAmount) + Sum(MinusAmount))*(Percentage/100) WHEN 1 then (Sum(PlusAmount) + Sum(MinusAmount))*(Percentage/100) * (- 1) END) Amount  ")
            '.Append(",TransCode FROM  ")
            '.Append("(	  ")
            '.Append("	SELECT  ")
            '.Append("			TC.TransCodeId, transtypeId,  ")
            '.Append("			TCGLA.GLAccount,   ")
            '.Append("			TCGLA.TypeId,   ")
            '.Append("			TCGLA.Percentage,  ")
            '.Append("			(case  when T.TransAmount > 0 then T.TransAmount else 0.00 end) PlusAmount,  ")
            '.Append("			(case  when T.TransAmount < 0 then T.TransAmount else 0.00 end) MinusAmount,  ")
            '.Append("			(case  when T.TransAmount > 0 then 1 else 0 end) Signo,TC.TransCodeDescrip as TransCode  ")
            '.Append("	  FROM    saTransactions T, saTransCodeGLAccounts TCGLA, saTransCodes TC    ")
            '.Append("	  WHERE   TCGLA.TransCodeId=TC.TransCodeId    ")
            '.Append("	  AND     T.TransCodeId=TC.TransCodeId    ")
            '.Append("	  AND     T.Voided=0   ")
            '.Append("	  AND	 TCGLA.TypeID IN (0,1)  ")

            ''   check if the selection is "All Campus Groups"
            'If Not campGrpId = Guid.Empty.ToString Then
            '    .Append(" AND T.CampusId IN (Select CampusId from syCmpGrpCmps where CampGrpId = ?) ")
            '    db.AddParameter("@CampGrpId", campGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'End If

            ''   check for Transaction Date From
            'If Not transDateFrom = Date.MinValue Then
            '    .Append(" AND T.TransDate >= ? ")
            '    db.AddParameter("@TransDateFrom", transDateFrom, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'End If

            ''   check for Transaction Date To
            'If Not transDateTo = Date.MaxValue Then
            '    .Append(" AND T.TransDate <= ? ")
            '    db.AddParameter("@TransDateTo", transDateTo, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'End If
            '.Append(") TX    ")
            '.Append("Group By TransCodeId, GLAccount, TypeId, Percentage, Signo ,TransCode,transtypeId  ")
            ''.Append("Order By TransCodeId, GLAccount, TypeId, Percentage, Signo ")

            '.Append("UNION ALL ")
            '.Append("SELECT  ")
            '.Append("		DR.DefRevenueDate as Date,  ")
            '.Append("		(Select TransCodeDescrip from saTransCodes where TransCodeId=T.TransCodeId) TransCodeDescrip, transtypeId ,TypeID , ")
            '.Append("		TCGLA.GLAccount, ")
            '.Append("		(SELECT CASE TypeId WHEN 2 THEN Sum(Amount) WHEN 3 THEN Sum(Amount)*(-1.0) END) Amount,TC.TransCodeDescrip as TransCode ")
            '.Append("from	saDeferredRevenues DR, saTransactions T, saTransCodes TC, saTransCodeGLAccounts TCGLA  ")
            '.Append("where	DR.TransactionId=T.TransactionId ")
            '.Append("AND    T.Voided=0 ")
            '.Append("AND		T.TransCodeId=TC.TransCodeId ")
            '.Append("AND		TC.TransCodeId=TCGLA.TransCodeId ")
            '.Append("AND		TCGLA.TypeId IN (2,3) ")

            ''   check if the selection is "All Campus Groups"
            'If Not campGrpId = Guid.Empty.ToString Then
            '    .Append(" AND T.CampusId IN (Select CampusId from syCmpGrpCmps where CampGrpId = ?) ")
            '    db.AddParameter("@CampGrpId", campGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'End If

            ''   check for Transaction Date From
            'If Not transDateFrom = Date.MinValue Then
            '    .Append(" AND DR.DefRevenueDate >= ? ")
            '    db.AddParameter("@TransDateFrom", transDateFrom, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'End If

            ''   check for Transaction Date To
            'If Not transDateTo = Date.MaxValue Then
            '    .Append(" AND DR.DefRevenueDate <= ? ")
            '    db.AddParameter("@TransDateTo", transDateTo, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'End If

            '.Append("GROUP BY DR.DefRevenueDate, T.TransCodeId, TransCodeDescrip, GLAccount, TypeId,TC.TransCodeDescrip, transtypeId   ")





        End With

        '   Execute the query and return Dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function

    Public Function SearchGlDistributionsDetails(ByVal campGrpId As String, ByVal transDateFrom As Date, ByVal transDateTo As Date) As IList(Of GeneralLedgerInfo)

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim conStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(conStr)

        '   build the sql query
        Dim sb As New StringBuilder
        With sb

            .Append("  SELECT CONVERT(VARCHAR(10), [Date], 101) as [Date], TransCodeId , ")
            .Append(" GLAccount , ")
            .Append(" [TransCodeGLAccountId] as GlAccountId, ")
            .Append("  TypeId , ")
            .Append("  Percentage , ")
            .Append("  v2.TransTypeId , ")
            .Append("  cast( CreditAmount AS DECIMAL(19,4)) CreditAmount , ")
            .Append("  cast( DebitAmount AS DECIMAL(19,4))  DebitAmount , ")
            .Append("    TransCodeDescrip  ")
            .Append(" ,TransCodeDescripSummary ")
            ' .Append("   , TransCode ")
            .Append("    FROM  glTransView2 v2 ")
            .Append("  JOIN    dbo.saTransTypes tt ON tt.TransTypeId = v2.TransTypeId ")
            .Append("  WHERE  1=1  ")
            .Append("  AND tt.Description = 'Payment' ")

            '   check if the selection is "All Campus Groups"
            If Not campGrpId = Guid.Empty.ToString Then
                .Append(" And CampusId IN (Select CampusId from syCmpGrpCmps where CampGrpId = @CampGrpId) ")
            End If

            '   check for Transaction Date From
            If Not transDateFrom = Date.MinValue Then
                .Append(" AND CONVERT(DATE,[Date]) >= CONVERT(DATE,@TransDateFrom) ")

                ' .Append(" AND CONVERT(VARCHAR(10), [Date], 101) >= @TransDateFrom ")
            End If

            '   check for Transaction Date To
            If Not transDateTo = Date.MaxValue Then
                '  .Append(" AND CONVERT(VARCHAR(10), [Date], 101) <= @TransDateTo ")

                .Append(" AND CONVERT(DATE,[Date]) < CONVERT(DATE,@TransDateTo) ")
            End If
            .Append(" order BY  [Date],TransCodeDescrip,GlAccount ")

            '.Append(" order BY  [Date],TransCode, TransCodeDescrip,GlAccount ")

        End With

        Dim command As SqlCommand = New SqlCommand(sb.ToString(), sqlconn)
        command.Parameters.AddWithValue("@CampGrpId", campGrpId)
        command.Parameters.AddWithValue("@TransDateFrom", transDateFrom)
        command.Parameters.AddWithValue("@TransDateTo", transDateTo)

        Dim outputlist As IList(Of GeneralLedgerInfo) = New List(Of GeneralLedgerInfo)()
        sqlconn.Open()
        Try
            Dim reader As SqlDataReader = command.ExecuteReader()
            While reader.Read()

                Dim trans = New GeneralLedgerInfo()
                trans.CreateDate = reader("Date")
                trans.GlAccount = reader("GLAccount")
                trans.GlAccountId = reader("GLAccountId").ToString()
                trans.Transaction = reader("TransCodeDescrip")
                trans.TransactionDescrip = reader("TransCodeDescripSummary")
                Dim cred = reader("CreditAmount")
                trans.Credit = If(IsDBNull(cred), Nothing, cred)
                Dim deb = reader("DebitAmount")
                trans.Debit = If(IsDBNull(deb), Nothing, deb)
                outputlist.Add(trans)
            End While

            Return outputlist
        Finally
            sqlconn.Close()
        End Try
    End Function


    'Public Function SearchGLDistributionsDetails(ByVal campGrpId As String, ByVal transDateFrom As Date, ByVal transDateTo As Date) As DataSet

    '    '   connect to the database
    '    Dim db As New DataAccess

    '    Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
    '    db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

    '    '   build the sql query
    '    Dim sb As New StringBuilder
    '    With sb

    '        .Append("  SELECT CONVERT(VARCHAR(10), [Date], 101) as [Date], TransCodeId , ")
    '        .Append(" GLAccount , ")
    '        .Append("  TypeId , ")
    '        .Append("  Percentage , ")
    '        .Append("   transtypeId , ")
    '        .Append("  cast( CreditAmount AS DECIMAL(19,4)) CreditAmount , ")
    '        .Append("  cast( DebitAmount AS DECIMAL(19,4))  DebitAmount , ")
    '        .Append("    TransCodeDescrip  ")
    '        .Append("   , TransCode ")
    '        .Append("    FROM  glTransView2  ")
    '        .Append("  WHERE  1=1  ")
    '        '   check if the selection is "All Campus Groups"
    '        If Not campGrpId = Guid.Empty.ToString Then
    '            .Append(" And CampusId IN (Select CampusId from syCmpGrpCmps where CampGrpId = ?) ")
    '            db.AddParameter("@CampGrpId", campGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        '   check for Transaction Date From
    '        If Not transDateFrom = Date.MinValue Then
    '            .Append(" AND CONVERT(VARCHAR(10), [Date], 101) >= ? ")
    '            db.AddParameter("@TransDateFrom", transDateFrom, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        End If

    '        '   check for Transaction Date To
    '        If Not transDateTo = Date.MaxValue Then
    '            .Append(" AND CONVERT(VARCHAR(10), [Date], 101) <= ? ")
    '            db.AddParameter("@TransDateTo", transDateTo, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        End If

    '        .Append(" order BY  [Date],TransCode, TransCodeDescrip,GlAccount ")

    '    End With

    '    '   Execute the query and return Dataset
    '    Return db.RunParamSQLDataSet(sb.ToString)

    'End Function


    Public Function GetMinAndMaxDatesFromGLDistributions() As Date()
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       Min(T.TransDate) As MinDate, ")
            .Append("       Max(T.TransDate) As MaxDate ")
            .Append("FROM   saTransactions T ")
            '.Append("WHERE  T.IsPosted = 0  ")
            .Append("where T.Voided=0 ")

        End With

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   fill date Array 
        Dim dateArray(1) As DateTime
        While dr.Read()
            If Not dr("MinDate") Is DBNull.Value Then dateArray(0) = CType(dr("MinDate"), DateTime) Else dateArray(0) = AdvantageCommonValues.MinDate
            If Not dr("MaxDate") Is DBNull.Value Then dateArray(1) = CType(dr("MaxDate"), DateTime) Else dateArray(1) = AdvantageCommonValues.MaxDate
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return date array
        Return dateArray

    End Function
    Public Function ApplyFeesByCourse(ByVal stuEnrollId As String, ByVal clsSectionId As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String

        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   BuildQueryToSelectBillingMethod
        Dim sb As New StringBuilder
        BuildQueryToSelectBillingMethod(stuEnrollId, db, sb)

        '   Execute the query
        Dim integerResult As Integer = db.RunParamSQLScalar(sb.ToString)

        '   apply course fees only if the BillingMethod is set to
        '   Courses (1) and AutoBill is True
        If Not integerResult = 3 Then Return ""

        ''' transactions displaying in the wrong credit/debit columns on the GL Distributions page
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       CF.TransCodeId, ")
            .Append("       CF.CourseFeeId, ")
            .Append("       ((Select TransCodeDescrip from saTransCodes where TransCodeId=CF.TransCodeId) + '/' + RQ.Descrip) TransCodeDescrip, ")
            .Append("       (Select Descrip from arReqs where ReqId=CF.CourseId) + ' ' + (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=?)) TransDescrip,  ")
            .Append("       (Select TermId from arClassSections where ClsSectionId= ?) TermId, ")
            .Append("       (Case LEN(CF.RateScheduleId) ")
            .Append("	        when 36 then ")
            .Append("		        (SELECT ")
            .Append("			        Case RSD.FlatAmount ")
            .Append("			            When 0.00 ")
            .Append("				            Then case RSD.UnitId ")
            .Append("						            when 0 then Rate*RQ.Credits ")
            .Append("						            when 1 then Rate*RQ.Hours ")
            .Append("                               End ")
            .Append("			            Else ")
            .Append("                           FlatAmount ")
            .Append("                   End ")
            .Append("		        FROM saRateSchedules RS, saRateScheduleDetails RSD ")
            .Append("               WHERE ")
            .Append("                       RS.RateScheduleId = RSD.RateScheduleId ")
            .Append("		        AND	    RS.RateScheduleId=CF.RateScheduleId ")
            '.Append("               AND     RSD.TuitionCategoryId = SE.TuitionCategoryId ")
            '.Append("		        AND	    (RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId)) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Hours and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId))) ")
            '.Append("               AND     (RSD.TuitionCategoryId = SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and SE.TuitionCategoryId is null)) ")
            '.Append("               AND	    (RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and Se.TuitionCategoryId is null)))) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Hours and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId or(RSD.TuitionCategoryId is null and SE.TuitionCategoryId is null))))) ")
            .Append("               AND  (RSD.TuitionCategoryId = SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)) ")
            .Append("               AND  ((RSD.FlatAmount=0.00 AND RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) or (RSD.FlatAmount=0.00 AND RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Hours and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) OR (RSD.FlatAmount>0.00 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))))) ")

            .Append("	        else ")
            .Append("	            (Case CF.UnitId ")
            .Append("			        when 0 Then CF.Amount*RQ.Credits ")
            .Append("			        when 1 then CF.Amount*RQ.Hours ")
            .Append("			        when 2 then CF.Amount ")
            .Append("		        End) ")
            .Append("	    End) TransAmount ")
            .Append("FROM saCourseFees CF, arReqs RQ, arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       CF.CourseId = RQ.ReqId ")
            '.Append("AND	CF.StartDate < GetDate() ")
            .Append("AND	CF.StartDate =	(select top 1 StartDate from saCourseFees where CourseId=CF.CourseId and StartDate <=GetDate() order by StartDate desc) ")
            .Append("AND    SE.StuEnrollId = ? ")
            .Append("AND	CF.CourseId=(Select ReqId from arClassSections where ClsSectionId= ?) ")
            .Append("AND	(((CF.RateScheduleId Is Null AND SE.TuitionCategoryId Is NULL AND CF.TuitionCategoryId Is NULL) OR (CF.RateScheduleId Is Null AND SE.TuitionCategoryId=CF.TuitionCategoryId)) OR CF.RateScheduleId Is Not Null) ")
        End With

        '   add parameters values to the query
        db.ClearParameters()

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add the ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add the ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   set time to be used on all records
        Dim rightNow = Date.Now

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   loop throughout all records
        While dr.Read()

            Dim PostChargeInfo As New PostChargeInfo

            '   set properties with data from DataReader
            With PostChargeInfo
                .StuEnrollId = stuEnrollId
                .FeeLevelId = 3 ' course fees
                .FeeId = dr("CourseFeeId")
                .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                .PostChargeDate = postFeesDate
                .PostChargeDescription = dr("TransCodeDescrip")
                If Not dr("TransAmount") Is DBNull.Value Then .Amount = dr("TransAmount") Else .Amount = 0.0
                .Reference = dr("TransDescrip")
                .TermId = CType(dr("TermId"), Guid).ToString
                .TransTypeId = 0
                .IsPosted = True
                .CampusId = campusId
                .ModUser = user
                .ModDate = rightNow
            End With

            '   add the entry to Transactions table
            Try
                '   BuildQueryToInsertPostChargeInfo
                BuildQueryToInsertPostChargeInfo(PostChargeInfo, db, sb, user, rightNow)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            Catch ex As OleDbException
                '   rollback transaction if there were errors
                groupTrans.Rollback()

                '   do not report sql lost connection
                If Not groupTrans.Connection Is Nothing Then
                    ' Report the error
                End If

                'Close Connection
                db.CloseConnection()

                '   return an error to the client
                Return DALExceptions.BuildErrorMessage(ex)
            End Try

        End While

        '   commit transaction 
        groupTrans.Commit()

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   return with no errors
        Return ""

    End Function
    Public Function ApplyFeesByCourse(ByVal termId As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String

        Dim result As Integer
        Dim Transaction As SqlTransaction
        Dim strConn As SqlConnection
        Dim strCmd As SqlCommand
        Dim strOutputParam As SqlParameter

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim ConnectionString As String = MyAdvAppSettings.AppSettings("ConnectionString")
        Try
            strConn = New SqlConnection(ConnectionString)
            strConn.Open()

            Transaction = strConn.BeginTransaction
            strCmd = New SqlCommand("USP_ApplyFeesByCourse", strConn, Transaction)
            strCmd.CommandTimeout = 0
            strCmd.CommandType = CommandType.StoredProcedure
            strOutputParam = strCmd.Parameters.Add("@TermId", SqlDbType.UniqueIdentifier)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@User", SqlDbType.VarChar)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@CampusID", SqlDbType.UniqueIdentifier)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@PostFeesDate", SqlDbType.DateTime)
            strOutputParam.Direction = ParameterDirection.Input

            strOutputParam = strCmd.Parameters.Add("@Result", OleDbType.Integer)
            strOutputParam.Direction = ParameterDirection.Output
            strCmd.Parameters("@TermId").Value = New Guid(termId)
            strCmd.Parameters("@User").Value = user
            strCmd.Parameters("@CampusID").Value = New Guid(campusId)
            strCmd.Parameters("@PostFeesDate").Value = CDate(postFeesDate)
            strCmd.ExecuteNonQuery()
            result = strCmd.Parameters("@Result").Value
            Transaction.Commit()
            If result = 0 Then
                Return "Error while inserting"
            Else
                Return ""
            End If



            ''Return 0
        Catch ex As Exception
            Transaction.Rollback()
            Return "Error while inserting"
        Finally
            strConn.Close()
        End Try


        ''   connect to the database
        'Dim db As New DataAccess
        'db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

        'Dim sb As New StringBuilder
        'With sb
        '    .Append("SELECT ")
        '    .Append("       R.StuEnrollId, ")
        '    .Append("       CF.CourseFeeId, ")
        '    .Append("       CF.TransCodeId, ")
        '    .Append("       ((Select TransCodeDescrip from saTransCodes where TransCodeId=CF.TransCodeId) + '/' + RQ.Descrip) TransCodeDescrip, ")
        '    '.Append("       (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) StudentName, ")
        '    .Append("       RQ.Descrip + ' ' + (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) TransDescrip, ")
        '    .Append("       (Case LEN(CF.RateScheduleId) ")
        '    .Append("	        when 36 then ")
        '    .Append("		        (SELECT ")
        '    .Append("			        Case RSD.FlatAmount ")
        '    .Append("			            When 0.00 ")
        '    .Append("				            Then case RSD.UnitId ")
        '    .Append("						            when 0 then Rate*RQ.Credits ")
        '    .Append("						            when 1 then Rate*RQ.Hours ")
        '    .Append("                               End ")
        '    .Append("			            Else ")
        '    .Append("                           FlatAmount ")
        '    .Append("                   End ")
        '    .Append("		        FROM saRateSchedules RS, saRateScheduleDetails RSD ")
        '    .Append("               WHERE ")
        '    .Append("                       RS.RateScheduleId = RSD.RateScheduleId ")
        '    .Append("		        AND	    RS.RateScheduleId = CF.RateScheduleId ")
        '    '.Append("              AND     RSD.TuitionCategoryId = SE.TuitionCategoryId ")
        '    '.Append("		        AND	    (RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId)) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Hours and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId))) ")
        '    '.Append("               AND     (RSD.TuitionCategoryId = SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and SE.TuitionCategoryId is null)) ")
        '    '.Append("               AND	    (RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and Se.TuitionCategoryId is null)))) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Hours and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId or(RSD.TuitionCategoryId is null and SE.TuitionCategoryId is null))))) ")
        '    .Append("               AND  (RSD.TuitionCategoryId = SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)) ")
        '    .Append("               AND  ((RSD.FlatAmount=0.00 AND RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) or (RSD.FlatAmount=0.00 AND RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Hours and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) OR (RSD.FlatAmount>0.00 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))))) ")

        '    .Append("	        else ")
        '    .Append("	            (Case CF.UnitId ")
        '    .Append("			        when 0 Then CF.Amount*RQ.Credits ")
        '    .Append("			        when 1 then CF.Amount*RQ.Hours ")
        '    .Append("			        when 2 then CF.Amount ")
        '    .Append("		        End) ")
        '    .Append("	    End) TransAmount ")
        '    '.Append("FROM   arResults R, arStuEnrollments SE, arClassSections CS, arClassSectionTerms CST,  arReqs RQ, saCourseFees CF, saBillingMethods BM ")
        '    .Append("FROM   arResults R, arStuEnrollments SE, arClassSections CS, arClassSectionTerms CST,  arReqs RQ, saCourseFees CF ")
        '    .Append("WHERE ")
        '    .Append("       R.TestId=CS.ClsSectionId ")
        '    .Append("AND	R.StuEnrollId=SE.StuEnrollId ")
        '    .Append("AND	CS.ReqId=CF.CourseId ")
        '    .Append("AND	CF.CourseId=RQ.reqId ")
        '    .Append("AND    SE.CampusId = ? ")
        '    '.Append("AND	CF.StartDate < GetDate() ")
        '    .Append("AND	CF.StartDate =	(select top 1 StartDate from saCourseFees where CourseId=CF.CourseId and StartDate <=GetDate() order by StartDate desc) ")
        '    .Append("AND	CST.ClsSectionId=CS.ClsSectionId AND CST.TermId = ? ")
        '    '.Append("AND	BM.BillingMethod*2+BM.AutoBill=2 ")
        '    .Append("AND	(((CF.RateScheduleId Is Null AND SE.TuitionCategoryId Is NULL AND CF.TuitionCategoryId Is NULL) OR (CF.RateScheduleId Is Null AND SE.TuitionCategoryId=CF.TuitionCategoryId)) OR CF.RateScheduleId Is Not Null) ")
        'End With

        '' Add CampusId to the parameter list
        'db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '' Add TermId to the parameter list
        'db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ''   Execute the query
        'Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        ''   set time to be used on all records
        'Dim rightNow = Date.Now

        ''   we must encapsulate all DB updates in one transaction
        'Dim groupTrans As OleDbTransaction = db.StartTransaction()

        ''   loop throughout all records
        'While dr.Read()

        '    Dim PostChargeInfo As New PostChargeInfo

        '    '   set properties with data from DataReader
        '    With PostChargeInfo
        '        .StuEnrollId = CType(dr("StuEnrollId"), Guid).ToString
        '        .FeeLevelId = 3 ' course fees
        '        .FeeId = CType(dr("CourseFeeId"), Guid).ToString
        '        .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
        '        .PostChargeDate = postFeesDate
        '        .PostChargeDescription = dr("TransCodeDescrip")
        '        If Not dr("TransAmount") Is System.DBNull.Value Then .Amount = dr("TransAmount") Else .Amount = 0.0
        '        .Reference = dr("TransDescrip")
        '        .TermId = termId
        '        .TransTypeId = 0
        '        .IsPosted = True
        '        .CampusId = campusId
        '        .ModUser = user
        '        .ModDate = rightNow
        '    End With

        '    '   add the entry to Transactions table
        '    Try
        '        '   BuildQueryToInsertPostChargeInfo
        '        BuildQueryToInsertPostChargeInfo(PostChargeInfo, db, sb, user, rightNow)

        '        '   execute the query
        '        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

        '    Catch ex As OleDbException
        '        '   rollback transaction if there were errors
        '        groupTrans.Rollback()

        '        '   do not report sql lost connection
        '        If Not groupTrans.Connection Is Nothing Then
        '            ' Report the error
        '        End If

        '        'Close Connection
        '        db.CloseConnection()

        '        '   return an error to the client
        '        Return DALExceptions.BuildErrorMessage(ex)
        '    End Try

        'End While

        ''   commit transaction 
        'groupTrans.Commit()

        ''Close Connection
        'db.CloseConnection()

        ''   return with no errors
        'Return ""

    End Function

    'Public Function ApplyFeesByCourseForCohortStart(ByVal termId As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String

    '    '   connect to the database
    '    Dim db As New DataAccess
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append("SELECT ")
    '        .Append("       R.StuEnrollId, ")
    '        .Append("       CF.CourseFeeId, ")
    '        .Append("       CF.TransCodeId, ")
    '        .Append("       ((Select TransCodeDescrip from saTransCodes where TransCodeId=CF.TransCodeId) + '/' + RQ.Descrip) TransCodeDescrip, ")
    '        '.Append("       (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) StudentName, ")
    '        .Append("       RQ.Descrip + ' ' + (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) TransDescrip, ")
    '        .Append("       (Case LEN(CF.RateScheduleId) ")
    '        .Append("	        when 36 then ")
    '        .Append("		        (SELECT ")
    '        .Append("			        Case RSD.FlatAmount ")
    '        .Append("			            When 0.00 ")
    '        .Append("				            Then case RSD.UnitId ")
    '        .Append("						            when 0 then Rate*RQ.Credits ")
    '        .Append("						            when 1 then Rate*RQ.Hours ")
    '        .Append("                               End ")
    '        .Append("			            Else ")
    '        .Append("                           FlatAmount ")
    '        .Append("                   End ")
    '        .Append("		        FROM saRateSchedules RS, saRateScheduleDetails RSD ")
    '        .Append("               WHERE ")
    '        .Append("                       RS.RateScheduleId = RSD.RateScheduleId ")
    '        .Append("		        AND	    RS.RateScheduleId = CF.RateScheduleId ")
    '        '.Append("              AND     RSD.TuitionCategoryId = SE.TuitionCategoryId ")
    '        '.Append("		        AND	    (RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId)) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Hours and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId))) ")
    '        '.Append("               AND     (RSD.TuitionCategoryId = SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and SE.TuitionCategoryId is null)) ")
    '        '.Append("               AND	    (RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and Se.TuitionCategoryId is null)))) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Hours and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId or(RSD.TuitionCategoryId is null and SE.TuitionCategoryId is null))))) ")
    '        .Append("               AND  (RSD.TuitionCategoryId = SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)) ")
    '        .Append("               AND  ((RSD.FlatAmount=0.00 AND RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) or (RSD.FlatAmount=0.00 AND RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Hours and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) OR (RSD.FlatAmount>0.00 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))))) ")

    '        .Append("	        else ")
    '        .Append("	            (Case CF.UnitId ")
    '        .Append("			        when 0 Then CF.Amount*RQ.Credits ")
    '        .Append("			        when 1 then CF.Amount*RQ.Hours ")
    '        .Append("			        when 2 then CF.Amount ")
    '        .Append("		        End) ")
    '        .Append("	    End) TransAmount,CS.TermId ")
    '        '.Append("FROM   arResults R, arStuEnrollments SE, arClassSections CS, arClassSectionTerms CST,  arReqs RQ, saCourseFees CF, saBillingMethods BM ")
    '        .Append("FROM   arResults R, arStuEnrollments SE, arClassSections CS, arClassSectionTerms CST,  arReqs RQ, saCourseFees CF ")
    '        .Append("WHERE ")
    '        .Append("       R.TestId=CS.ClsSectionId ")
    '        .Append("AND	R.StuEnrollId=SE.StuEnrollId ")
    '        .Append("AND	CS.ReqId=CF.CourseId ")
    '        .Append("AND	CF.CourseId=RQ.reqId ")
    '        .Append("AND    SE.CampusId = ? ")
    '        ''Added on jan 30 2009
    '        ''Added by Saraswathi to fetch the students belonging to the selected cohortstartdate only
    '        .Append(" And SE.CohortStartDate=? ")

    '        '.Append("AND	CF.StartDate < GetDate() ")
    '        .Append("AND	CF.StartDate =	(select top 1 StartDate from saCourseFees where CourseId=CF.CourseId and StartDate <=GetDate() order by StartDate desc) ")
    '        .Append("AND	CST.ClsSectionId=CS.ClsSectionId AND CST.TermId  in (select TermId from arClassSections where cohortstartdate = ?)   ")
    '        '.Append("AND	BM.BillingMethod*2+BM.AutoBill=2 ")
    '        .Append("AND	(((CF.RateScheduleId Is Null AND SE.TuitionCategoryId Is NULL AND CF.TuitionCategoryId Is NULL) OR (CF.RateScheduleId Is Null AND SE.TuitionCategoryId=CF.TuitionCategoryId)) OR CF.RateScheduleId Is Not Null) ")
    '    End With

    '    ' Add CampusId to the parameter list
    '    db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    ' Add TermId to the parameter list
    '    db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    ' Add TermId to the parameter list
    '    db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


    '    '   Execute the query
    '    Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

    '    '   set time to be used on all records
    '    Dim rightNow = Date.Now

    '    '   we must encapsulate all DB updates in one transaction
    '    Dim groupTrans As OleDbTransaction = db.StartTransaction()

    '    '   loop throughout all records
    '    While dr.Read()

    '        Dim PostChargeInfo As New PostChargeInfo

    '        '   set properties with data from DataReader
    '        With PostChargeInfo
    '            .StuEnrollId = CType(dr("StuEnrollId"), Guid).ToString
    '            .FeeLevelId = 3 ' course fees
    '            .FeeId = CType(dr("CourseFeeId"), Guid).ToString
    '            .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
    '            .PostChargeDate = postFeesDate
    '            .PostChargeDescription = dr("TransCodeDescrip")
    '            If Not dr("TransAmount") Is System.DBNull.Value Then .Amount = dr("TransAmount") Else .Amount = 0.0
    '            .Reference = dr("TransDescrip")
    '            .TermId = CType(dr("TermId"), Guid).ToString
    '            .TransTypeId = 0
    '            .IsPosted = True
    '            .CampusId = campusId
    '            .ModUser = user
    '            .ModDate = rightNow
    '        End With

    '        '   add the entry to Transactions table
    '        Try
    '            '   BuildQueryToInsertPostChargeInfo
    '            BuildQueryToInsertPostChargeInfo(PostChargeInfo, db, sb, user, rightNow)

    '            '   execute the query
    '            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

    '        Catch ex As OleDbException
    '            '   rollback transaction if there were errors
    '            groupTrans.Rollback()

    '            '   do not report sql lost connection
    '            If Not groupTrans.Connection Is Nothing Then
    '                ' Report the error
    '            End If

    '            'Close Connection
    '            db.CloseConnection()

    '            '   return an error to the client
    '            Return DALExceptions.BuildErrorMessage(ex)
    '        End Try

    '    End While

    '    '   commit transaction 
    '    groupTrans.Commit()

    '    'Close Connection
    '    db.CloseConnection()

    '    '   return with no errors
    '    Return ""

    'End Function
    Private Sub BuildQueryToInsertPostChargeInfo(ByVal postChargeInfo As PostChargeInfo, ByRef db As DataAccess, ByRef sb As StringBuilder, ByVal user As String, ByVal rightNow As DateTime)

        '   build the query
        sb = New StringBuilder
        With sb
            .Append("INSERT saTransactions (TransactionId, StuEnrollId, TermId, CampusId, ")
            .Append("   TransDate, TransCodeId, TransReference, TransDescrip, AcademicYearId, ")
            .Append("   TransAmount, TransTypeId, IsPosted, FeeLevelId, FeeId, ")
            .Append("   CreateDate, IsAutomatic, ModUser, ModDate) ")
            .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,1,?,?) ")
        End With

        '   add parameters values to the query
        db.ClearParameters()

        '   PostChargeId
        db.AddParameter("@TransactionId", postChargeInfo.PostChargeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", postChargeInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   TermId
        If postChargeInfo.TermId = Guid.Empty.ToString Then
            db.AddParameter("@TermId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@TermId", postChargeInfo.TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        '   CampusId
        If postChargeInfo.CampusId = Guid.Empty.ToString Then
            db.AddParameter("@CampusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@CampusId", postChargeInfo.CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        '   Transaction Date
        db.AddParameter("@TransDate", postChargeInfo.PostChargeDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        '   TransCodeId
        db.AddParameter("@TransCodeId", postChargeInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Transaction Reference
        db.AddParameter("@TransReference", postChargeInfo.Reference, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   Transaction Description
        db.AddParameter("@TransDescrip", postChargeInfo.PostChargeDescription, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   AcademicYearId
        If postChargeInfo.AcademicYearId = Guid.Empty.ToString Then
            db.AddParameter("@AcademicYearId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@AcademicYearId", postChargeInfo.AcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        '   Transaction Amount
        db.AddParameter("@TransAmount", postChargeInfo.Amount, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

        '   TransTypeId
        db.AddParameter("@TransTypeId", postChargeInfo.TransTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   IsPosted
        db.AddParameter("@IsPosted", postChargeInfo.IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

        '   FeeLevelId
        db.AddParameter("@FeeLevelId", postChargeInfo.FeeLevelId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   TransTypeId
        db.AddParameter("@FeeId", postChargeInfo.FeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ''   create timestamp
        'Dim rightNow As DateTime = Date.Now

        '   CreateDate
        db.AddParameter("@CreateDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        '   ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   ModDate
        db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    End Sub
    Public Function DeleteAppliedFeesByCourse(ByVal stuEnrollId As String, ByVal clsSectionId As String, ByVal user As String, ByVal campusId As String) As String

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   BuildQueryToSelectBillingMethod
        Dim sb As New StringBuilder
        BuildQueryToSelectBillingMethod(stuEnrollId, db, sb)

        '   Execute the query
        Dim integerResult As Integer = db.RunParamSQLScalar(sb.ToString)

        '   delete applied course fees only if the BillingMethod is set to
        '   Courses (1) and AutoBill is True
        If Not integerResult = 3 Then Return ""

        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       CF.TransCodeId, ")
            .Append("       (Select Descrip from arReqs where ReqId=CF.CourseId) + ' ' + (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=?)) TransDescrip,  ")
            .Append("       (Select TermId from arClassSections where ClsSectionId= ?) TermId, ")
            .Append("       (Case LEN(CF.RateScheduleId) ")
            .Append("	        when 36 then ")
            .Append("		        (SELECT ")
            .Append("			        Case RSD.FlatAmount ")
            .Append("			            When 0.00 ")
            .Append("				            Then case RSD.UnitId ")
            .Append("						            when 0 then Rate*RQ.Credits ")
            .Append("						            when 1 then Rate*RQ.Hours ")
            .Append("                               End ")
            .Append("			            Else ")
            .Append("                           FlatAmount ")
            .Append("                   End ")
            .Append("		        FROM saRateSchedules RS, saRateScheduleDetails RSD ")
            .Append("               WHERE ")
            .Append("                       RS.RateScheduleId = RSD.RateScheduleId ")
            .Append("		        AND	    RS.RateScheduleId=CF.RateScheduleId ")
            '.Append("               AND     RSD.TuitionCategoryId = SE.TuitionCategoryId ")
            '.Append("		        AND	    (RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId)) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Hours and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId))) ")
            .Append("               AND     (RSD.TuitionCategoryId = SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and SE.TuitionCategoryId is null)) ")
            .Append("               AND	    (RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and Se.TuitionCategoryId is null)))) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Hours and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId or(RSD.TuitionCategoryId is null and SE.TuitionCategoryId is null))))) ")
            .Append("	        else ")
            .Append("	            (Case CF.UnitId ")
            .Append("			        when 0 Then CF.Amount*RQ.Credits ")
            .Append("			        when 1 then CF.Amount*RQ.Hours ")
            .Append("			        when 2 then CF.Amount ")
            .Append("		        End) ")
            .Append("	    End) TransAmount ")
            .Append("FROM saCourseFees CF, arReqs RQ, arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       CF.CourseId = RQ.ReqId ")
            '.Append("AND	CF.StartDate < GetDate() ")
            .Append("AND	CF.StartDate =	(select top 1 StartDate from saCourseFees where CourseId=CF.CourseId and StartDate <=GetDate() order by StartDate desc) ")
            .Append("AND    SE.StuEnrollId = ? ")
            .Append("AND	CourseId=(Select ReqId from arClassSections where ClsSectionId= ?) ")
            .Append("AND	(((CF.RateScheduleId Is Null AND SE.TuitionCategoryId Is NULL AND CF.TuitionCategoryId Is NULL) OR (CF.RateScheduleId Is Null AND SE.TuitionCategoryId=CF.TuitionCategoryId)) OR CF.RateScheduleId Is Not Null) ")
        End With

        '   add parameters values to the query
        db.ClearParameters()

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add the ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add the ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   loop throughout all records
        While dr.Read()

            Dim PostChargeInfo As New PostChargeInfo

            '   set properties with data from DataReader
            With PostChargeInfo
                .StuEnrollId = stuEnrollId
                .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                .PostChargeDescription = dr("TransDescrip")
                .Amount = dr("TransAmount")
                .Reference = dr("TransDescrip")
                .TermId = CType(dr("TermId"), Guid).ToString
                .TransTypeId = 0
                .IsPosted = True
                .ModUser = user
            End With

            '   add the entry to Transactions table
            Try
                '   BuildQueryToDeletePostChargeInfo
                BuildQueryToDeletePostChargeInfo(PostChargeInfo, db, sb)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            Catch ex As OleDbException
                '   rollback transaction if there were errors
                groupTrans.Rollback()

                '   do not report sql lost connection
                If Not groupTrans.Connection Is Nothing Then
                    ' Report the error
                End If

                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

                '   return an error to the client
                Return DALExceptions.BuildErrorMessage(ex)
            End Try

        End While

        '   commit transaction 
        groupTrans.Commit()

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   return with no errors
        Return ""

    End Function
    Public Function DeleteAppliedFeesByCourse(ByVal termId As String, ByVal user As String, ByVal campusId As String) As String

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       R.StuEnrollId, ")
            .Append("       CF.TransCodeId, ")
            .Append("       (Select TransCodeDescrip from saTransCodes where TransCodeId=CF.TransCodeId) TransCodeDescrip, ")
            '.Append("       (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) StudentName, ")
            .Append("       RQ.Descrip + ' ' + (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) TransDescrip, ")
            .Append("       (Case LEN(CF.RateScheduleId) ")
            .Append("	        when 36 then ")
            .Append("		        (SELECT ")
            .Append("			        Case RSD.FlatAmount ")
            .Append("			            When 0.00 ")
            .Append("				            Then case RSD.UnitId ")
            .Append("						            when 0 then Rate*RQ.Credits ")
            .Append("						            when 1 then Rate*RQ.Hours ")
            .Append("                               End ")
            .Append("			            Else ")
            .Append("                           FlatAmount ")
            .Append("                   End ")
            .Append("		        FROM saRateSchedules RS, saRateScheduleDetails RSD ")
            .Append("               WHERE ")
            .Append("                       RS.RateScheduleId = RSD.RateScheduleId ")
            .Append("		        AND	    RS.RateScheduleId=CF.RateScheduleId ")
            '.Append("		        AND	    (RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Credits)) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Hours))) ")
            .Append("               AND     (RSD.TuitionCategoryId = SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and SE.TuitionCategoryId is null)) ")
            .Append("               AND	    (RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and Se.TuitionCategoryId is null)))) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Hours and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId or(RSD.TuitionCategoryId is null and SE.TuitionCategoryId is null))))) ")
            .Append("	        else ")
            .Append("	            (Case CF.UnitId ")
            .Append("			        when 0 Then CF.Amount*RQ.Credits ")
            .Append("			        when 1 then CF.Amount*RQ.Hours ")
            .Append("			        when 2 then CF.Amount ")
            .Append("		        End) ")
            .Append("	    End) TransAmount ")
            '.Append("FROM   arResults R, arStuEnrollments SE, arClassSections CS, arClassSectionTerms CST,  arReqs RQ, saCourseFees CF, saBillingMethods BM ")
            .Append("FROM   arResults R, arStuEnrollments SE, arClassSections CS, arClassSectionTerms CST,  arReqs RQ, saCourseFees CF ")
            .Append("WHERE ")
            .Append("       R.TestId=CS.ClsSectionId ")
            .Append("AND	R.StuEnrollId=SE.StuEnrollId ")
            .Append("AND	CS.ReqId=CF.CourseId ")
            .Append("AND	CF.CourseId=RQ.reqId ")
            .Append("AND    SE.CampusId = ? ")
            '.Append("AND	CF.StartDate < GetDate() ")
            .Append("AND	CF.StartDate =	(select top 1 StartDate from saCourseFees where CourseId=CF.CourseId and StartDate <=GetDate() order by StartDate desc) ")
            .Append("AND	CST.ClsSectionId=CS.ClsSectionId AND CST.TermId = ? ")
            .Append("AND	BM.BillingMethod*2+BM.AutoBill=2 ")
            .Append("AND	(((CF.RateScheduleId Is Null AND SE.TuitionCategoryId Is NULL AND CF.TuitionCategoryId Is NULL) OR (CF.RateScheduleId Is Null AND SE.TuitionCategoryId=CF.TuitionCategoryId)) OR CF.RateScheduleId Is Not Null) ")
        End With

        ' Add CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   loop throughout all records
        While dr.Read()

            Dim PostChargeInfo As New PostChargeInfo

            '   set properties with data from DataReader
            With PostChargeInfo
                .StuEnrollId = CType(dr("StuEnrollId"), Guid).ToString
                .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                .PostChargeDescription = dr("TransDescrip")
                .Amount = dr("TransAmount")
                .Reference = dr("TransDescrip")
                .TermId = termId
                .TransTypeId = 0
                .IsPosted = True
                .ModUser = user
            End With

            '   add the entry to Transactions table
            Try
                '   BuildQueryToDeletePostChargeInfo
                BuildQueryToDeletePostChargeInfo(PostChargeInfo, db, sb)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            Catch ex As OleDbException
                '   rollback transaction if there were errors
                groupTrans.Rollback()

                '   do not report sql lost connection
                If Not groupTrans.Connection Is Nothing Then
                    ' Report the error
                End If

                'Close Connection
                db.CloseConnection()

                '   return an error to the client
                Return DALExceptions.BuildErrorMessage(ex)
            End Try

        End While

        '   commit transaction 
        groupTrans.Commit()

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   return with no errors
        Return ""

    End Function
    Private Sub BuildQueryToDeletePostChargeInfo(ByVal postChargeInfo As PostChargeInfo, ByRef db As DataAccess, ByRef sb As StringBuilder)

        '   build the query
        sb = New StringBuilder
        With sb
            .Append("DELETE ")
            .Append("FROM saTransactions ")
            .Append("WHERE ")
            .Append("       StuEnrollId = ? ")
            .Append("AND    TermId = ? ")
            .Append("AND    TransCodeId = ? ")
            .Append("AND    TransReference = ? ")
            .Append("AND    TransDescrip = ? ")
            .Append("AND    TransAmount = ? ")
            .Append("AND    TransTypeId = 0 ")
            .Append("AND    IsPosted = 1 ")
            .Append("AND    saTransactions.Voided=0 ")

        End With

        '   add parameters values to the query
        db.ClearParameters()

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", postChargeInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   TermId
        db.AddParameter("@TermId", postChargeInfo.TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   TransCodeId
        db.AddParameter("@TransCodeId", postChargeInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Transaction Reference
        db.AddParameter("@TransReference", postChargeInfo.Reference, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   Transaction Description
        db.AddParameter("@TransDescrip", postChargeInfo.PostChargeDescription, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   Transaction Amount
        db.AddParameter("@TransAmount", postChargeInfo.Amount, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

    End Sub
    Public Function ApplyFeesByProgramVersion(ByVal stuEnrollId As String, ByVal dummy As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   BuildQueryToSelectBillingMethod
        Dim sb As New StringBuilder
        BuildQueryToSelectBillingMethod(stuEnrollId, db, sb)

        '   Execute the query
        Dim integerResult As Integer = db.RunParamSQLScalar(sb.ToString)

        '   apply course fees only if the BillingMethod is set to
        '   ProgramVersion (0) and AutoBill is True
        If Not integerResult = 1 Then Return ""

        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       PVF.TransCodeId, ")
            .Append("       PVF.PrgVerFeeId, ")
            .Append("       ((Select TransCodeDescrip from saTransCodes where TransCodeId=PVF.TransCodeId) + '-' + PV.PrgVerDescrip) TransCodeDescrip, ")
            '.Append("       (Select PrgVerDescrip from arPrgVersions where PrgVerId=PVF.PrgVerId) + ' ' + (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId= ?)) TransDescrip, ")
            .Append("       (Select PrgVerDescrip from arPrgVersions where PrgVerId=PVF.PrgVerId) TransDescrip, ")
            .Append("       (Case LEN(PVF.RateScheduleId) ")
            .Append("            when 36 then ")
            .Append("               (SELECT ")
            .Append("               Case RSD.FlatAmount ")
            .Append("                   When 0.00 ")
            .Append("                       Then case RSD.UnitId ")
            .Append("                               when 0 then Rate*PV.Credits ")
            .Append("                               when 1 then Rate*PV.Hours ")
            .Append("                            End ")
            .Append("				    Else ")
            .Append("                           FlatAmount ")
            .Append("                   End ")
            .Append("               FROM saRateSchedules RS, saRateScheduleDetails RSD ")
            .Append("               WHERE ")
            .Append("                       RS.RateScheduleId=RSD.RateScheduleId ")
            .Append("               AND	    RS.RateScheduleId=PVF.RateScheduleId ")
            .Append("               AND     (RSD.TuitionCategoryId = SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and SE.TuitionCategoryId is null)) ")
            '.Append("		        AND	    ((RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=PV.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and SE.TuitionCategoryId is null)))) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=PV.Hours and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and SE.TuitionCategoryId is null)))))) ")
            .Append("               AND     ((RSD.FlatAmount=0.00 AND RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=PV.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) or (RSD.FlatAmount=0.00 AND RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=PV.Hours and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) OR (RSD.FlatAmount>0.00 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=PV.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))))) ")
            .Append("            else ")
            .Append("               (Case PVF.UnitId ")
            .Append("                   when 0 Then PVF.Amount*PV.Credits ")
            .Append("                   when 1 then PVF.Amount*PV.Hours ")
            .Append("                   when 2 then PVF.Amount End) ")
            .Append("               End )TransAmount ")
            .Append("FROM   saProgramVersionFees PVF, arPrgVersions PV, arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       SE.StuEnrollId = ? ")
            .Append("AND    SE.PrgVerId=PV.PrgVerId ")
            .Append("AND    PVF.PrgVerId = PV.PrgVerId ")
            .Append("AND	(((PVF.RateScheduleId Is Null AND SE.TuitionCategoryId Is NULL AND PVF.TuitionCategoryId Is NULL) OR (PVF.RateScheduleId Is Null AND SE.TuitionCategoryId=PVF.TuitionCategoryId)) OR PVF.RateScheduleId Is Not Null) ")
        End With

        '   add parameters values to the query
        db.ClearParameters()

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '' Add stuEnrollId to the parameter list
        'db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   set time to be used on all records
        Dim rightNow = Date.Now

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   loop throughout all records
        While dr.Read()

            Dim PostChargeInfo As New PostChargeInfo

            '   set properties with data from DataReader
            With PostChargeInfo
                .StuEnrollId = stuEnrollId
                .FeeLevelId = 2 ' Program Version fees
                .FeeId = dr("PrgVerFeeId")
                .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                .PostChargeDate = postFeesDate
                .PostChargeDescription = dr("TransCodeDescrip")
                If Not dr("TransAmount") Is DBNull.Value Then .Amount = dr("TransAmount")
                .Reference = dr("TransDescrip")
                .TermId = GetTermIdForStuEnrollId(stuEnrollId)
                .TransTypeId = 0
                .IsPosted = True
                .CampusId = campusId
                .ModUser = user
                .ModDate = rightNow
            End With

            '   add the entry to Transactions table
            Try
                '   BuildQueryToInsertPostChargeInfo
                BuildQueryToInsertPostChargeInfo(PostChargeInfo, db, sb, user, rightNow)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            Catch ex As OleDbException
                '   rollback transaction if there were errors
                groupTrans.Rollback()

                '   do not report sql lost connection
                If Not groupTrans.Connection Is Nothing Then
                    ' Report the error
                End If

                'Close Connection
                db.CloseConnection()

                '   return an error to the client
                Return DALExceptions.BuildErrorMessage(ex)
            End Try

        End While

        '   commit transaction 
        groupTrans.Commit()

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   return with no errors
        Return ""

    End Function
    Public Function ApplyFeesByProgramVersion(ByVal termId As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String

        Dim result As Integer
        Dim Transaction As SqlTransaction
        Dim strConn As SqlConnection
        Dim strCmd As SqlCommand
        Dim strOutputParam As New SqlParameter

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim ConnectionString As String = MyAdvAppSettings.AppSettings("ConnectionString")
        Try
            strConn = New SqlConnection(ConnectionString)
            strConn.Open()

            Transaction = strConn.BeginTransaction
            strCmd = New SqlCommand("USP_ApplyFeesByProgramVersion", strConn, Transaction)
            strCmd.CommandTimeout = 0
            strCmd.CommandType = CommandType.StoredProcedure

            'strOutputParam = strCmd.Parameters.Add("@stuEnrollIDList", SqlDbType.VarChar)
            'strOutputParam.Direction = ParameterDirection.Input
            'strOutputParam = strCmd.Parameters.Add("@TransCodeIDList", SqlDbType.VarChar)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@TermId", SqlDbType.UniqueIdentifier)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@User", SqlDbType.VarChar)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@CampusID", SqlDbType.UniqueIdentifier)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@PostFeesDate", SqlDbType.DateTime)
            strOutputParam.Direction = ParameterDirection.Input

            strOutputParam = strCmd.Parameters.Add("@Result", OleDbType.Integer)
            strOutputParam.Direction = ParameterDirection.Output

            'strCmd.Parameters("@stuEnrollIDList").Value = StudentList
            'strCmd.Parameters("@TransCodeIDList").Value = TransCodeList

            strCmd.Parameters("@TermId").Value = New Guid(termId)
            strCmd.Parameters("@User").Value = user
            strCmd.Parameters("@CampusID").Value = New Guid(campusId)
            strCmd.Parameters("@PostFeesDate").Value = CDate(postFeesDate)
            strCmd.ExecuteNonQuery()
            result = strCmd.Parameters("@Result").Value
            Transaction.Commit()
            If result = 0 Then
                Return "Error while inserting"
            Else
                Return ""
            End If



            ''Return 0
        Catch ex As Exception
            Transaction.Rollback()
            Return "Error while inserting"
        Finally
            strConn.Close()
        End Try


    End Function
    Public Function ApplyFeesByCourseForCohortStart(ByVal termId As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String

        Dim result As Integer
        Dim transaction As SqlTransaction
        Dim strConn As SqlConnection
        Dim strCmd As SqlCommand
        Dim strOutputParam As SqlParameter

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim ConnectionString As String = MyAdvAppSettings.AppSettings("ConnectionString")
        Try
            strConn = New SqlConnection(ConnectionString)
            strConn.Open()

            transaction = strConn.BeginTransaction
            strCmd = New SqlCommand("USP_ApplyFeesByCourseForCohort", strConn, transaction)
            strCmd.CommandTimeout = 0
            strCmd.CommandType = CommandType.StoredProcedure
            strOutputParam = strCmd.Parameters.Add("@CohortStartDate", SqlDbType.DateTime)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@User", SqlDbType.VarChar)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@CampusID", SqlDbType.UniqueIdentifier)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@PostFeesDate", SqlDbType.DateTime)
            strOutputParam.Direction = ParameterDirection.Input

            strOutputParam = strCmd.Parameters.Add("@Result", OleDbType.Integer)
            strOutputParam.Direction = ParameterDirection.Output
            strCmd.Parameters("@CohortStartDate").Value = CDate(termId)
            strCmd.Parameters("@User").Value = user
            strCmd.Parameters("@CampusID").Value = New Guid(campusId)
            strCmd.Parameters("@PostFeesDate").Value = CDate(postFeesDate)
            strCmd.ExecuteNonQuery()
            result = strCmd.Parameters("@Result").Value
            transaction.Commit()
            If result = 0 Then
                Return "Error while inserting"
            Else
                Return ""
            End If



            ''Return 0
        Catch ex As Exception
            transaction.Rollback()
            Return "Error while inserting"
        Finally
            strConn.Close()
        End Try





    End Function
    Public Function ApplyFeesByProgramVersionForCohortStart(ByVal termId As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String

        Dim result As Integer
        Dim transaction As SqlTransaction
        Dim strConn As SqlConnection
        Dim strCmd As SqlCommand
        Dim strOutputParam As SqlParameter

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connectionString As String = myAdvAppSettings.AppSettings("ConnectionString")
        strConn = New SqlConnection(connectionString)
        strConn.Open()

        transaction = strConn.BeginTransaction
        Try
            strCmd = New SqlCommand("USP_ApplyFeesByProgramVersionForCohort", strConn, transaction)
            strCmd.CommandTimeout = 0
            strCmd.CommandType = CommandType.StoredProcedure
            strOutputParam = strCmd.Parameters.Add("@CohortStartDate", SqlDbType.DateTime)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@User", SqlDbType.VarChar)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@CampusID", SqlDbType.UniqueIdentifier)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@PostFeesDate", SqlDbType.DateTime)
            strOutputParam.Direction = ParameterDirection.Input

            strOutputParam = strCmd.Parameters.Add("@Result", OleDbType.Integer)
            strOutputParam.Direction = ParameterDirection.Output
            strCmd.Parameters("@CohortStartDate").Value = CDate(termId)
            strCmd.Parameters("@User").Value = user
            strCmd.Parameters("@CampusID").Value = New Guid(campusId)
            strCmd.Parameters("@PostFeesDate").Value = CDate(postFeesDate)
            strCmd.ExecuteNonQuery()
            result = strCmd.Parameters("@Result").Value
            transaction.Commit()
            If result = 0 Then
                Return "Error while inserting"
            Else
                Return ""
            End If

            ''Return 0
        Catch ex As Exception
            transaction.Rollback()
            Return "Error while inserting"
        Finally
            strConn.Close()
        End Try




    End Function

    Public Function ApplyFeesByTermForCohortStart(ByVal termId As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String
        Dim result As Integer
        Dim transaction As SqlTransaction
        Dim strConn As SqlConnection
        Dim strCmd As SqlCommand
        Dim strOutputParam As SqlParameter

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connectionString As String = myAdvAppSettings.AppSettings("ConnectionString")
        strConn = New SqlConnection(connectionString)
        strConn.Open()
        transaction = strConn.BeginTransaction
        Try
            strCmd = New SqlCommand("USP_ApplyFeesByTermForCohort", strConn, transaction)
            strCmd.CommandTimeout = 0
            strCmd.CommandType = CommandType.StoredProcedure
            strOutputParam = strCmd.Parameters.Add("@CohortStartDate", SqlDbType.DateTime)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@User", SqlDbType.VarChar)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@CampusID", SqlDbType.UniqueIdentifier)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@PostFeesDate", SqlDbType.DateTime)
            strOutputParam.Direction = ParameterDirection.Input

            strOutputParam = strCmd.Parameters.Add("@Result", OleDbType.Integer)
            strOutputParam.Direction = ParameterDirection.Output
            strCmd.Parameters("@CohortStartDate").Value = CDate(termId)
            strCmd.Parameters("@User").Value = user
            strCmd.Parameters("@CampusID").Value = New Guid(campusId)
            strCmd.Parameters("@PostFeesDate").Value = CDate(postFeesDate)
            strCmd.ExecuteNonQuery()
            result = strCmd.Parameters("@Result").Value
            transaction.Commit()
            If result = 0 Then
                Return "Error while inserting"
            Else
                Return ""
            End If

            ''Return 0
        Catch ex As Exception
            transaction.Rollback()
            Return "Error while inserting"
        Finally
            strConn.Close()
        End Try


    End Function

    Public Function DeleteAppliedFeesByProgramVersion(ByVal stuEnrollId As String, ByVal dummy As String, ByVal user As String, ByVal campusId As String) As String

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   BuildQueryToSelectBillingMethod
        Dim sb As New StringBuilder
        BuildQueryToSelectBillingMethod(stuEnrollId, db, sb)

        '   Execute the query
        Dim integerResult As Integer = db.RunParamSQLScalar(sb.ToString)

        '   delete applied fees only if the BillingMethod is set to
        '   ProgramVersion (0) and AutoBill is True
        If Not integerResult = 1 Then Return ""

        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       PVF.TransCodeId, ")
            .Append("       (Select PrgVerDescrip from arPrgVersions where PrgVerId=PVF.PrgVerId) + ' ' + (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId= ?)) TransDescrip, ")
            .Append("       (Case LEN(PVF.RateScheduleId) ")
            .Append("            when 36 then ")
            .Append("               (SELECT ")
            .Append("               Case RSD.FlatAmount ")
            .Append("                   When 0.00 ")
            .Append("                       Then case RSD.UnitId ")
            .Append("                               when 0 then Rate*PV.Credits ")
            .Append("                               when 1 then Rate*PV.Hours ")
            .Append("                            End ")
            .Append("				    Else ")
            .Append("                           FlatAmount ")
            .Append("                   End ")
            .Append("               FROM saRateSchedules RS, saRateScheduleDetails RSD ")
            .Append("               WHERE ")
            .Append("                       RS.RateScheduleId=RSD.RateScheduleId ")
            .Append("               AND	    RS.RateScheduleId=PVF.RateScheduleId ")
            .Append("               AND     RSD.TuitionCategoryId = SE.TuitionCategoryId ")
            .Append("		        AND	    (RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=PV.Credits and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId)) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=PV.Hours and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId))) ")
            .Append("            else ")
            .Append("               (Case PVF.UnitId ")
            .Append("                   when 0 Then PVF.Amount*PV.Credits ")
            .Append("                   when 1 then PVF.Amount*PV.Hours ")
            .Append("                   when 2 then PVF.Amount End) ")
            .Append("               End )TransAmount ")
            .Append("FROM   saProgramVersionFees PVF, arPrgVersions PV, arStuEnrollments SE ")
            .Append("WHERE  ")
            .Append("       SE.StuEnrollId = ? ")
            .Append("AND    SE.PrgVerId=PV.PrgVerId ")
            .Append("AND    PVF.PrgVerId = PV.PrgVerId ")
            .Append("AND	(((PVF.RateScheduleId Is Null AND SE.TuitionCategoryId Is NULL AND PVF.TuitionCategoryId Is NULL) OR (PVF.RateScheduleId Is Null AND SE.TuitionCategoryId=PVF.TuitionCategoryId)) OR PVF.RateScheduleId Is Not Null) ")
        End With

        '   add parameters values to the query
        db.ClearParameters()

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   loop throughout all records
        While dr.Read()

            Dim PostChargeInfo As New PostChargeInfo

            '   set properties with data from DataReader
            With PostChargeInfo
                .StuEnrollId = stuEnrollId
                .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                .PostChargeDescription = dr("TransDescrip")
                If Not dr("TransAmount") Is DBNull.Value Then .Amount = dr("TransAmount")
                .Reference = dr("TransDescrip")
                .TransTypeId = 0
                .IsPosted = True
                .ModUser = user
            End With

            '   add the entry to Transactions table
            Try
                '   BuildQueryToDeletePostChargeInfo
                BuildQueryToDeletePostChargeInfo(PostChargeInfo, db, sb)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            Catch ex As OleDbException
                '   rollback transaction if there were errors
                groupTrans.Rollback()

                '   do not report sql lost connection
                If Not groupTrans.Connection Is Nothing Then
                    ' Report the error
                End If

                'Close Connection
                db.CloseConnection()

                '   return an error to the client
                Return DALExceptions.BuildErrorMessage(ex)

            End Try

        End While

        '   commit transaction 
        groupTrans.Commit()

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   return with no errors
        Return ""

    End Function
    Public Function DeleteAppliedFeesByProgramVersion(ByVal termId As String, ByVal user As String, ByVal campusId As String) As String

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       R.StuEnrollId, ")
            .Append("       PVF.TransCodeId, ")
            .Append("       ((Select TransCodeDescrip from saTransCodes where TransCodeId=PVF.TransCodeId) + '-' + PV.PrgVerDescrip) TransCodeDescrip, ")
            '.Append("       (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) StudentName, ")
            .Append("       PV.PrgVerDescrip + ' ' + (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) TransDescrip, ")
            .Append("       (Case LEN(PVF.RateScheduleId) ")
            .Append("            when 36 then ")
            .Append("               (SELECT ")
            .Append("               Case RSD.FlatAmount ")
            .Append("                   When 0.00 ")
            .Append("                       Then case RSD.UnitId ")
            .Append("                               when 0 then Rate*PV.Credits ")
            .Append("                               when 1 then Rate*PV.Hours ")
            .Append("                            End ")
            .Append("				    Else ")
            .Append("                           FlatAmount ")
            .Append("                   End ")
            .Append("               FROM saRateSchedules RS, saRateScheduleDetails RSD ")
            .Append("               WHERE ")
            .Append("                       RS.RateScheduleId=RSD.RateScheduleId ")
            .Append("               AND	    RS.RateScheduleId=PVF.RateScheduleId ")
            .Append("               AND     RSD.TuitionCategoryId = SE.TuitionCategoryId ")
            .Append("		        AND	    (RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=PV.Credits and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId)) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=PV.Hours and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId))) ")
            .Append("            else ")
            .Append("               (Case PVF.UnitId ")
            .Append("                   when 0 Then PVF.Amount*PV.Credits ")
            .Append("                   when 1 then PVF.Amount*PV.Hours ")
            .Append("                   when 2 then PVF.Amount End) ")
            .Append("               End )TransAmount ")
            '.Append("FROM	arResults R, arClassSections CS, arClassSectionTerms CST,  arStuEnrollments SE, arPrgVersions PV, saProgramVersionFees PVF, saBillingMethods BM ")
            .Append("FROM	arResults R, arClassSections CS, arClassSectionTerms CST,  arStuEnrollments SE, arPrgVersions PV, saProgramVersionFees PVF ")
            .Append("WHERE ")
            .Append("       R.StuEnrollId = SE.StuEnrollId ")
            .Append("AND	R.TestId=CS.ClsSectionId ")
            .Append("AND	SE.PrgVerId=PV.PrgVerId ")
            .Append("AND	PV.PrgVerId=PVF.PrgVerId ")
            .Append("AND    SE.CampusId = ? ")
            .Append("AND	CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ")
            '.Append("AND	BM.BillingMethod*2+BM.AutoBill=0 ")
            .Append("AND	(((PVF.RateScheduleId Is Null AND SE.TuitionCategoryId Is NULL AND PVF.TuitionCategoryId Is NULL) OR (PVF.RateScheduleId Is Null AND SE.TuitionCategoryId=PVF.TuitionCategoryId)) OR PVF.RateScheduleId Is Not Null) ")
        End With

        ' Add CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   loop throughout all records
        While dr.Read()

            Dim PostChargeInfo As New PostChargeInfo

            '   set properties with data from DataReader
            With PostChargeInfo
                .StuEnrollId = CType(dr("StuEnrollId"), Guid).ToString
                .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                .PostChargeDescription = dr("TransDescrip")
                If Not dr("TransAmount") Is DBNull.Value Then .Amount = dr("TransAmount")
                .Reference = dr("TransDescrip")
                .TermId = termId
                .TransTypeId = 0
                .IsPosted = True
                .ModUser = user
            End With

            '   add the entry to Transactions table
            Try
                '   BuildQueryToDeletePostChargeInfo
                BuildQueryToDeletePostChargeInfo(PostChargeInfo, db, sb)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            Catch ex As OleDbException
                '   rollback transaction if there were errors
                groupTrans.Rollback()

                '   do not report sql lost connection
                If Not groupTrans.Connection Is Nothing Then
                    ' Report the error
                End If

                'Close Connection
                db.CloseConnection()

                '   return an error to the client
                Return DALExceptions.BuildErrorMessage(ex)
            End Try

        End While

        '   commit transaction 
        groupTrans.Commit()

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   return with no errors
        Return ""

    End Function
    Private Sub BuildQueryToSelectBillingMethod(ByVal stuEnrollId As String, ByRef db As DataAccess, ByRef sb As StringBuilder)

        '   build the sql query
        sb = New StringBuilder
        With sb
            .Append("SELECT BM.BillingMethod*2+BM.AutoBill ")
            .Append("FROM   arStuEnrollments SE, saBillingMethods BM ")
            .Append("WHERE  SE.BillingMethodId=BM.BillingMethodId ")
            .Append("AND	SE.StuEnrollId = ? ")
        End With

        '   add parameters values to the query

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    End Sub
    Public Function ApplyFeesByTerm(ByVal stuEnrollId As String, ByVal termId As String, ByVal tuitionCategoryId As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   BuildQueryToSelectBillingMethod
        Dim sb As New StringBuilder
        BuildQueryToSelectBillingMethod(stuEnrollId, db, sb)

        '   Execute the query
        Dim integerResult As Integer = db.RunParamSQLScalar(sb.ToString)

        '   apply course fees only if the BillingMethod is set to
        '   Term (2) and AutoBill is True
        If Not integerResult = 5 Then Return ""

        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       PF.TransCodeId, ")
            .Append("       ((Select TransCodeDescrip from saTransCodes where TransCodeId=PF.TransCodeId) + Case PF.ApplyTo when 0 then '- All Programs' when 1 then '-' + PT.Description when 2 then '-' + PV.PrgVerDescrip else '' end) TransCodeDescrip, ")
            .Append("       (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) StudentName, ")
            .Append("       (Select TermDescrip from arTerm where TermId=PF.TermId) TransDescrip, ")
            .Append("       T.TermDescrip, ")
            .Append("       (Case LEN(PF.RateScheduleId) ")
            .Append("           when 36 then ")
            .Append("               (SELECT ")
            .Append("                   Case RSD.FlatAmount ")
            .Append("               		When 0.00 ")
            .Append("       					Then case RSD.UnitId ")
            .Append("			                        when 0 then Rate*(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId= ? AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) ")
            .Append("			                        when 1 then Rate*(SELECT Coalesce(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId= ? AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) ")
            .Append("                                End ")
            .Append("   		            Else ")
            .Append("                           FlatAmount ")
            '.Append("                   End ")
            '.Append("       		FROM saRateSchedules RS, saRateScheduleDetails RSD ")
            '.Append("               WHERE ")
            '.Append("                   RS.RateScheduleId = RSD.RateScheduleId ")
            '.Append("               AND	RS.RateScheduleId=PF.RateScheduleId ")
            '.Append("               AND RSD.TuitionCategoryId = SE.TuitionCategoryId ")
            '.Append("	            AND (RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CS.TermId= ? ) and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId)) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CS.TermId= ? ) and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId))) ")
            .Append("                   End ")
            .Append("       		FROM saRateSchedules RS, saRateScheduleDetails RSD ")
            .Append("               WHERE ")
            .Append("                   RS.RateScheduleId = RSD.RateScheduleId ")
            .Append("               AND	RS.RateScheduleId=PF.RateScheduleId ")
            .Append("               AND  (RSD.TuitionCategoryId = SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)) ")
            .Append("               AND  ((RSD.FlatAmount=0.00 AND RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) or (RSD.FlatAmount=0.00 AND RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT Coalesce(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) OR (RSD.FlatAmount>0.00 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))))) ")

            .Append("   	 else ")
            .Append("           (Case PF.UnitId ")
            .Append("       	    when 0 Then PF.Amount* ")
            .Append("           		    (SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ ")
            .Append("                       WHERE ")
            .Append("                               R.TestId = CS.ClsSectionId ")
            .Append("                       AND	    CS.ReqId=RQ.ReqId ")
            .Append("               	    AND	    R.StuEnrollId= ? ")
            .Append("                       AND     CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ")
            .Append("                 		AND     (( ? Is NULL AND PF.TuitionCategoryId Is NULL) OR ( ? =PF.TuitionCategoryId ))) ")
            .Append("       	    when 1 then PF.Amount* ")
            .Append("           		    (SELECT SUM(RQ.Hours) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ ")
            .Append("                       WHERE ")
            .Append("                               R.TestId = CS.ClsSectionId ")
            .Append("                       AND	    CS.ReqId=RQ.ReqId ")
            .Append("                       AND     R.StuEnrollId= ? ")
            .Append("                       AND	    CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ")
            .Append("                 		AND     (( ? Is NULL AND PF.TuitionCategoryId Is NULL) OR ( ? =PF.TuitionCategoryId ))) ")
            '.Append("               when 2 then PF.Amount * (select(case when Exists(select * from arResults where StuEnrollId= ? and TestId in (select ClsSectionId from arClassSections where TermId= ? )) then 1 else 0 end)) ")
            .Append("               when 2 then PF.Amount * (select(case when Exists(select * from arResults where StuEnrollId= ? and TestId in (select ClsSectionId from arClassSections where TermId= ? AND (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) OR (SE.TuitionCategoryId=PF.TuitionCategoryId )))) then 1 else 0 end)) ")
            .Append("            End) ")
            .Append("        End) TransAmount ")
            '.Append("FROM saPeriodicFees PF, arTerm T, arStuEnrollments SE, saBillingMethods BM, syStatusCodes SC, arPrgVersions PV, arProgTypes PT ")
            .Append("FROM saPeriodicFees PF, arTerm T, arStuEnrollments SE, syStatusCodes SC, arPrgVersions PV, arProgTypes PT ")
            .Append("WHERE ")
            .Append("       PF.TermId=T.TermId ")
            .Append("AND	PF.TermId= ? ")
            .Append("AND    SE.StuEnrollId = ? ")
            .Append("AND    SE.CampusId = ? ")
            '.Append("AND	BM.BillingMethod*2+BM.AutoBill=5 ")
            .Append("AND    SE.StatusCodeId=SC.StatusCodeId ")
            .Append("AND    SC.SysStatusId In (7,9,13,20) ") ' 7=Future Start 9=Currently Attending 13=Re-entry 20=Academic Probation
            .Append("AND    SE.PrgVerId=PV.PrgVerId ")
            .Append("AND    PV.ProgTypId=PT.ProgTypId ")
            .Append("AND    ((PF.ApplyTo=0) OR (PF.ApplyTo=1 AND PV.ProgTypId=PF.ProgTypId) OR (PF.ApplyTo=2 AND PV.PrgVerId=PF.PrgVerId)) ")
        End With

        '   add parameters values to the query
        db.ClearParameters()

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Add TuitionCategoryId the parameter list
        If tuitionCategoryId = Guid.Empty.ToString Then
            db.AddParameter("@TuitionCategoryId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@TuitionCategoryId", tuitionCategoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        '   Add TuitionCategoryId the parameter list
        If tuitionCategoryId = Guid.Empty.ToString Then
            db.AddParameter("@TuitionCategoryId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@TuitionCategoryId", tuitionCategoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Add TuitionCategoryId the parameter list
        If tuitionCategoryId = Guid.Empty.ToString Then
            db.AddParameter("@TuitionCategoryId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@TuitionCategoryId", tuitionCategoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        '   Add TuitionCategoryId the parameter list
        If tuitionCategoryId = Guid.Empty.ToString Then
            db.AddParameter("@TuitionCategoryId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@TuitionCategoryId", tuitionCategoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   set time to be used on all records
        Dim rightNow = Date.Now

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   loop throughout all records
        While dr.Read()
            'do not insert transaction with null amount
            If Not dr("TransAmount") Is DBNull.Value Then
                'do not insert transactions with zero amount
                If Not CType(dr("TransAmount"), Decimal) = 0.0 Then

                    Dim PostChargeInfo As New PostChargeInfo

                    '   set properties with data from DataReader
                    With PostChargeInfo
                        .StuEnrollId = stuEnrollId
                        .FeeLevelId = 1 'Term Fee
                        .FeeId = dr("PeriodicFeeId")
                        .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                        .PostChargeDescription = dr("TransDescrip")
                        .PostChargeDate = postFeesDate
                        .Amount = dr("TransAmount")
                        .Reference = dr("TransDescrip")
                        .TermId = termId
                        .TransTypeId = 0
                        .IsPosted = True
                        .CampusId = campusId
                        .ModUser = user
                        .ModDate = rightNow
                    End With

                    '   add the entry to Transactions table
                    Try
                        '   BuildQueryToInsertPostChargeInfo
                        BuildQueryToInsertPostChargeInfo(PostChargeInfo, db, sb, user, rightNow)

                        '   execute the query
                        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

                    Catch ex As OleDbException
                        '   rollback transaction if there were errors
                        groupTrans.Rollback()

                        '   do not report sql lost connection
                        If Not groupTrans.Connection Is Nothing Then
                            ' Report the error
                        End If

                        'Close Connection
                        db.CloseConnection()

                        '   return an error to the client
                        Return DALExceptions.BuildErrorMessage(ex)
                    End Try
                End If
            End If
        End While

        '   commit transaction 
        groupTrans.Commit()

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   return with no errors
        Return ""

    End Function
    Public Function ApplyFeesByTerm(ByVal termId As String, ByVal tuitionCategoryId As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       SE.StuEnrollId, ")
            .Append("       PF.TransCodeId, ")
            .Append("       ((Select TransCodeDescrip from saTransCodes where TransCodeId=PF.TransCodeId) + Case PF.ApplyTo when 0 then '- All Programs' when 1 then '-' + PT.Description when 2 then '-' + PV.PrgVerDescrip else '' end) TransCodeDescrip, ")
            .Append("       (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) StudentName, ")
            .Append("       (Select TermDescrip from arTerm where TermId=PF.TermId) TransDescrip, ")
            .Append("       T.TermDescrip, ")
            .Append("       (Case LEN(PF.RateScheduleId) ")
            .Append("           when 36 then ")
            .Append("               (SELECT ")
            .Append("                   Case RSD.FlatAmount ")
            .Append("               		When 0.00 ")
            .Append("       					Then case RSD.UnitId ")
            '.Append("			                        when 0 then Rate*(SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CS.TermId= ? ) ")
            '.Append("			                        when 1 then Rate*(SELECT SUM(RQ.Hours) FROM arResults R, arClassSections CS, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CS.TermId= ? ) ")
            '.Append("                                End ")
            '.Append("   		            Else ")
            ''.Append("                           FlatAmount ")
            '.Append("                           FlatAmount * (select(case when Exists(select * from arResults where StuEnrollId= SE.StuEnrollId and TestId in (select ClsSectionId from arClassSections where TermId= ? )) then 1 else 0 end)) ")
            '.Append("                   End ")
            '.Append("       		FROM saRateSchedules RS, saRateScheduleDetails RSD ")
            '.Append("               WHERE ")
            '.Append("                   RS.RateScheduleId = RSD.RateScheduleId ")
            '.Append("               AND	RS.RateScheduleId=PF.RateScheduleId ")
            '.Append("               AND RSD.TuitionCategoryId = SE.TuitionCategoryId ")
            '.Append("	            AND (RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CS.TermId= ? ) and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId)) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CS.TermId= ? ) and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId))) ")
            .Append("                                  when 0 then Rate*(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) ")
            .Append("                                  when 1 then Rate*(SELECT Coalesce(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) ")
            .Append("                                End ")
            .Append("   		            Else ")
            .Append("                           FlatAmount ")
            .Append("                   End ")
            .Append("       		FROM saRateSchedules RS, saRateScheduleDetails RSD ")
            .Append("               WHERE ")
            .Append("                   RS.RateScheduleId = RSD.RateScheduleId ")
            .Append("               AND	RS.RateScheduleId=PF.RateScheduleId ")
            .Append("               AND  (RSD.TuitionCategoryId = SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)) ")
            .Append("               AND  ((RSD.FlatAmount=0.00 AND RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) or (RSD.FlatAmount=0.00 AND RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT Coalesce(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) OR (RSD.FlatAmount>0.00 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))))) ")

            .Append("   	 else ")
            .Append("           (Case PF.UnitId ")
            .Append("       	    when 0 Then PF.Amount* ")
            .Append("           		    (SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ ")
            .Append("                       WHERE ")
            .Append("                               R.TestId = CS.ClsSectionId ")
            .Append("                       AND	    CS.ReqId=RQ.ReqId ")
            .Append("               	    AND	    R.StuEnrollId=SE.StuEnrollId ")
            .Append("                       AND     CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ")
            .Append("                 		AND     (( ? Is NULL AND PF.TuitionCategoryId Is NULL) OR ( ? =PF.TuitionCategoryId ))) ")
            .Append("       	    when 1 then PF.Amount* ")
            .Append("           		    (SELECT SUM(RQ.Hours) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ ")
            .Append("                       WHERE ")
            .Append("                               R.TestId = CS.ClsSectionId ")
            .Append("                       AND	    CS.ReqId=RQ.ReqId ")
            .Append("                       AND     R.StuEnrollId=SE.StuEnrollId ")
            .Append("                       AND	    CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ")
            .Append("                 		AND     (( ? Is NULL AND PF.TuitionCategoryId Is NULL) OR ( ? =PF.TuitionCategoryId ))) ")
            '.Append("               when 2 then PF.Amount * (select(case when Exists(select * from arResults where StuEnrollId= SE.StuEnrollId and TestId in (select ClsSectionId from arClassSections where TermId= ? )) then 1 else 0 end)) ")
            .Append("                when 2 then PF.Amount * (select(case when Exists(select * from arResults where StuEnrollId= SE.StuEnrollId and TestId in (select ClsSectionId from arClassSections where TermId= ? AND (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) OR (SE.TuitionCategoryId=PF.TuitionCategoryId )))) then 1 else 0 end)) ")

            .Append("            End) ")
            .Append("        End) TransAmount ")
            '.Append("FROM saPeriodicFees PF, arTerm T, arStuEnrollments SE, saBillingMethods BM, syStatusCodes SC, arPrgVersions PV, arProgTypes PT ")
            .Append("FROM saPeriodicFees PF, arTerm T, arStuEnrollments SE, syStatusCodes SC, arPrgVersions PV, arProgTypes PT ")
            .Append("WHERE ")
            .Append("       PF.TermId=T.TermId ")
            .Append("AND	PF.TermId= ? ")
            .Append("AND    SE.CampusId = ? ")
            '.Append("AND	BM.BillingMethod*2+BM.AutoBill=4 ")
            .Append("AND    SE.StatusCodeId=SC.StatusCodeId ")
            .Append("AND    SC.SysStatusId In (7,9,13,20) ") ' 7=Future Start 9=Currently Attending 13=Re-entry 20=Academic Probation
            .Append("AND    SE.PrgVerId=PV.PrgVerId ")
            .Append("AND    PV.ProgTypId=PT.ProgTypId ")
            .Append("AND    ((PF.ApplyTo=0) OR (PF.ApplyTo=1 AND PV.ProgTypId=PF.ProgTypId) OR (PF.ApplyTo=2 AND PV.PrgVerId=PF.PrgVerId)) ")
        End With

        '   add parameters values to the query
        db.ClearParameters()

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Add TuitionCategoryId the parameter list
        If tuitionCategoryId = Guid.Empty.ToString Then
            db.AddParameter("@TuitionCategoryId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@TuitionCategoryId", tuitionCategoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        '   Add TuitionCategoryId the parameter list
        If tuitionCategoryId = Guid.Empty.ToString Then
            db.AddParameter("@TuitionCategoryId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@TuitionCategoryId", tuitionCategoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Add TuitionCategoryId the parameter list
        If tuitionCategoryId = Guid.Empty.ToString Then
            db.AddParameter("@TuitionCategoryId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@TuitionCategoryId", tuitionCategoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        '   Add TuitionCategoryId the parameter list
        If tuitionCategoryId = Guid.Empty.ToString Then
            db.AddParameter("@TuitionCategoryId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@TuitionCategoryId", tuitionCategoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   set time to be used on all records
        Dim rightNow = Date.Now

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   loop throughout all records
        While dr.Read()
            'do not insert transaction with null amount
            If Not dr("TransAmount") Is DBNull.Value Then
                'do not insert transactions with zero amount
                If Not CType(dr("TransAmount"), Decimal) = 0.0 Then

                    Dim PostChargeInfo As New PostChargeInfo

                    '   set properties with data from DataReader
                    With PostChargeInfo
                        .StuEnrollId = CType(dr("StuEnrollId"), Guid).ToString
                        .FeeLevelId = 1 'Term Fee
                        .FeeId = dr("PeriodicFeeId")
                        .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                        .PostChargeDate = postFeesDate
                        .PostChargeDescription = dr("TransCodeDescrip")
                        .Amount = dr("TransAmount")
                        .Reference = dr("TransDescrip")
                        .TermId = termId
                        .TransTypeId = 0
                        .IsPosted = True
                        .CampusId = campusId
                        .ModUser = user
                        .ModDate = rightNow
                    End With

                    '   add the entry to Transactions table
                    Try
                        '   BuildQueryToInsertPostChargeInfo
                        BuildQueryToInsertPostChargeInfo(PostChargeInfo, db, sb, user, rightNow)

                        '   execute the query
                        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

                    Catch ex As OleDbException
                        '   rollback transaction if there were errors
                        groupTrans.Rollback()

                        '   do not report sql lost connection
                        If Not groupTrans.Connection Is Nothing Then
                            ' Report the error
                        End If

                        'Close Connection
                        db.CloseConnection()

                        '   return an error to the client
                        Return DALExceptions.BuildErrorMessage(ex)
                    End Try
                End If
            End If
        End While

        '   commit transaction 
        groupTrans.Commit()

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   return with no errors
        Return ""

    End Function
    Public Function ApplyFeesByTerm(ByVal termId As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String



        Dim result As Integer
        Dim Transaction As SqlTransaction
        Dim strConn As SqlConnection
        Dim strCmd As SqlCommand
        Dim strOutputParam As SqlParameter

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim ConnectionString As String = MyAdvAppSettings.AppSettings("ConnectionString")
        Try
            strConn = New SqlConnection(ConnectionString)
            strConn.Open()

            Transaction = strConn.BeginTransaction
            strCmd = New SqlCommand("USP_ApplyFeesByTerm", strConn, Transaction)
            strCmd.CommandTimeout = 0
            strCmd.CommandType = CommandType.StoredProcedure
            strOutputParam = strCmd.Parameters.Add("@TermId", SqlDbType.UniqueIdentifier)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@User", SqlDbType.VarChar)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@CampusID", SqlDbType.UniqueIdentifier)
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@PostFeesDate", SqlDbType.DateTime)
            strOutputParam.Direction = ParameterDirection.Input

            strOutputParam = strCmd.Parameters.Add("@Result", OleDbType.Integer)
            strOutputParam.Direction = ParameterDirection.Output
            strCmd.Parameters("@TermId").Value = New Guid(termId)
            strCmd.Parameters("@User").Value = user
            strCmd.Parameters("@CampusID").Value = New Guid(campusId)
            strCmd.Parameters("@PostFeesDate").Value = CDate(postFeesDate)
            strCmd.ExecuteNonQuery()
            result = strCmd.Parameters("@Result").Value
            Transaction.Commit()
            If result = 0 Then
                Return "Error while inserting"
            Else
                Return ""
            End If



            ''Return 0
        Catch ex As Exception
            Transaction.Rollback()
            Return "Error while inserting"
        Finally
            strConn.Close()
        End Try





        ''   connect to the database
        'Dim db As New DataAccess
        'db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

        'Dim sb As StringBuilder = New StringBuilder
        'With sb
        '    .Append("SELECT ")
        '    .Append("       SE.StuEnrollId, ")
        '    .Append("       PF.PeriodicFeeId, ")
        '    .Append("       PF.TransCodeId, ")
        '    .Append("       ((Select TransCodeDescrip from saTransCodes where TransCodeId=PF.TransCodeId) + Case PF.ApplyTo when 0 then '- All Programs' when 1 then '-' + PT.Description when 2 then '-' + PV.PrgVerDescrip else '' end) TransCodeDescrip, ")
        '    .Append("       (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) StudentName, ")
        '    .Append("       (Select TermDescrip from arTerm where TermId=PF.TermId) TransDescrip, ")
        '    .Append("       T.TermDescrip, ")
        '    .Append("       (Case LEN(PF.RateScheduleId) ")
        '    .Append("           when 36 then ")
        '    .Append("               (SELECT ")
        '    .Append("                   Case RSD.FlatAmount ")
        '    .Append("               		When 0.00 ")
        '    .Append("       					Then case RSD.UnitId ")
        '    '.Append("			                        when 0 then Rate*(SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CS.TermId= ? ) ")
        '    '.Append("			                        when 1 then Rate*(SELECT SUM(RQ.Hours) FROM arResults R, arClassSections CS, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CS.TermId= ? ) ")
        '    '.Append("                                End ")
        '    '.Append("   		            Else ")
        '    '.Append("                           FlatAmount * (select(case when Exists(select * from arResults where StuEnrollId= SE.StuEnrollId and TestId in (select ClsSectionId from arClassSections where TermId= ? )) then 1 else 0 end)) ")
        '    '.Append("                   End ")
        '    '.Append("       		FROM saRateSchedules RS, saRateScheduleDetails RSD ")
        '    '.Append("               WHERE ")
        '    '.Append("                   RS.RateScheduleId = RSD.RateScheduleId ")
        '    '.Append("               AND	RS.RateScheduleId=PF.RateScheduleId ")
        '    '.Append("               AND RSD.TuitionCategoryId = SE.TuitionCategoryId ")
        '    '.Append("	            AND (RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CS.TermId= ? ) and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId)) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CS.TermId= ? ) and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId))) ")
        '    .Append("                                  when 0 then Rate*(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) ")
        '    .Append("                                  when 1 then Rate*(SELECT Coalesce(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) ")
        '    .Append("                                End ")
        '    .Append("   		            Else ")
        '    .Append("                           FlatAmount ")
        '    .Append("                   End ")
        '    .Append("       		FROM saRateSchedules RS, saRateScheduleDetails RSD ")
        '    .Append("               WHERE ")
        '    .Append("                   RS.RateScheduleId = RSD.RateScheduleId ")
        '    .Append("               AND	RS.RateScheduleId=PF.RateScheduleId ")
        '    .Append("               AND  (RSD.TuitionCategoryId = SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)) ")
        '    .Append("               AND  ((RSD.FlatAmount=0.00 AND RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) or (RSD.FlatAmount=0.00 AND RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT Coalesce(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) OR (RSD.FlatAmount>0.00 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))))) ")

        '    .Append("   	 else ")
        '    .Append("           (Case PF.UnitId ")
        '    .Append("       	    when 0 Then PF.Amount* ")
        '    .Append("           		    (SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ ")
        '    .Append("                       WHERE ")
        '    .Append("                               R.TestId = CS.ClsSectionId ")
        '    .Append("                       AND	    CS.ReqId=RQ.ReqId ")
        '    .Append("               	    AND	    R.StuEnrollId=SE.StuEnrollId ")
        '    .Append("                       AND     CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ")
        '    .Append("                 		AND     (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) OR (SE.TuitionCategoryId=PF.TuitionCategoryId ))) ")
        '    .Append("       	    when 1 then PF.Amount* ")
        '    .Append("           		    (SELECT SUM(RQ.Hours) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ ")
        '    .Append("                       WHERE ")
        '    .Append("                               R.TestId = CS.ClsSectionId ")
        '    .Append("                       AND	    CS.ReqId=RQ.ReqId ")
        '    .Append("                       AND     R.StuEnrollId=SE.StuEnrollId ")
        '    .Append("                       AND	    CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ")
        '    .Append("                 		AND     (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) OR (SE.TuitionCategoryId=PF.TuitionCategoryId ))) ")
        '    '.Append("               when 2 then PF.Amount * (select(case when Exists(select * from arResults where StuEnrollId= SE.StuEnrollId and TestId in (select ClsSectionId from arClassSections where TermId= ? )) then 1 else 0 end)) ")
        '    .Append("               when 2 then PF.Amount * (select(case when Exists(select * from arResults where StuEnrollId= SE.StuEnrollId and TestId in (select ClsSectionId from arClassSections where TermId= ? AND (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) OR (SE.TuitionCategoryId=PF.TuitionCategoryId )))) then 1 else 0 end)) ")
        '    .Append("            End) ")
        '    .Append("        End) TransAmount ")
        '    '.Append("FROM saPeriodicFees PF, arTerm T, arStuEnrollments SE, saBillingMethods BM, syStatusCodes SC, arPrgVersions PV, arProgTypes PT ")
        '    .Append("FROM saPeriodicFees PF, arTerm T, arStuEnrollments SE, syStatusCodes SC, arPrgVersions PV, arProgTypes PT ")
        '    .Append("WHERE ")
        '    .Append("       PF.TermId=T.TermId ")
        '    .Append("AND	PF.TermId= ? ")
        '    .Append("AND    SE.CampusId = ? ")
        '    '.Append("AND	BM.BillingMethod*2+BM.AutoBill=4 ")
        '    .Append("AND    SE.StatusCodeId=SC.StatusCodeId ")
        '    .Append("AND    SC.SysStatusId In (7,9,13,20) ") ' 7=Future Start 9=Currently Attending 13=Re-entry 20=Academic Probation
        '    .Append("AND    SE.PrgVerId=PV.PrgVerId ")
        '    .Append("AND    PV.ProgTypId=PT.ProgTypId ")
        '    .Append("AND    ((PF.ApplyTo=0) OR (PF.ApplyTo=1 AND PV.ProgTypId=PF.ProgTypId) OR (PF.ApplyTo=2 AND PV.PrgVerId=PF.PrgVerId)) ")
        '    .Append("AND    ((PF.TermStartDate is null) or (PF.TermStartDate = SE.ExpStartDate)) ")
        'End With

        ''   add parameters values to the query
        'db.ClearParameters()

        '' Add TermId to the parameter list
        'db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '' Add TermId to the parameter list
        'db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '' Add TermId to the parameter list
        'db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '' Add TermId to the parameter list
        'db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '' Add TermId to the parameter list
        'db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '' Add TermId to the parameter list
        'db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '' Add TermId to the parameter list
        'db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '' Add TermId to the parameter list
        'db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '' Add TermId to the parameter list
        'db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '' Add CampusId to the parameter list
        'db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ''   Execute the query
        'Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        ''   set time to be used on all records
        'Dim rightNow = Date.Now

        ''   we must encapsulate all DB updates in one transaction
        'Dim groupTrans As OleDbTransaction = db.StartTransaction()

        ''   loop throughout all records
        'While dr.Read()
        '    'do not insert transaction with null amount
        '    If Not dr("TransAmount") Is System.DBNull.Value Then
        '        'do not insert transactions with zero amount
        '        If Not CType(dr("TransAmount"), Decimal) = 0.0 Then

        '            Dim PostChargeInfo As New PostChargeInfo

        '            '   set properties with data from DataReader
        '            With PostChargeInfo
        '                .StuEnrollId = CType(dr("StuEnrollId"), Guid).ToString
        '                .FeeLevelId = 1 'Term Fee
        '                .FeeId = CType(dr("PeriodicFeeId"), Guid).ToString
        '                .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
        '                .PostChargeDescription = dr("TransCodeDescrip")
        '                .PostChargeDate = postFeesDate
        '                .Amount = dr("TransAmount")
        '                .Reference = dr("TransDescrip")
        '                .TermId = termId
        '                .TransTypeId = 0
        '                .IsPosted = True
        '                .CampusId = campusId
        '                .ModUser = user
        '                .ModDate = rightNow
        '            End With

        '            '   add the entry to Transactions table
        '            Try
        '                '   BuildQueryToInsertPostChargeInfo
        '                BuildQueryToInsertPostChargeInfo(PostChargeInfo, db, sb, user, rightNow)

        '                '   execute the query
        '                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

        '            Catch ex As OleDbException
        '                '   rollback transaction if there were errors
        '                groupTrans.Rollback()

        '                '   do not report sql lost connection
        '                If Not groupTrans.Connection Is Nothing Then
        '                    ' Report the error
        '                End If

        '                'Close Connection
        '                db.CloseConnection()

        '                '   return an error to the client
        '                Return DALExceptions.BuildErrorMessage(ex)
        '            End Try
        '        End If
        '    End If
        'End While

        ''   commit transaction 
        'groupTrans.Commit()

        ''Close Connection
        'db.CloseConnection()

        ''   return with no errors
        'Return ""

    End Function

    'Public Function ApplyFeesByTermForCohortStart(ByVal termId As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String

    '    '   connect to the database
    '    Dim db As New DataAccess
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

    '    Dim sb As StringBuilder = New StringBuilder
    '    With sb
    '        .Append("SELECT ")
    '        .Append("       SE.StuEnrollId, ")
    '        .Append("       PF.PeriodicFeeId, ")
    '        .Append("       PF.TransCodeId, ")
    '        .Append("       ((Select TransCodeDescrip from saTransCodes where TransCodeId=PF.TransCodeId) + Case PF.ApplyTo when 0 then '- All Programs' when 1 then '-' + PT.Description when 2 then '-' + PV.PrgVerDescrip else '' end) TransCodeDescrip, ")
    '        .Append("       (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) StudentName, ")
    '        .Append("       (Select TermDescrip from arTerm where TermId=PF.TermId) TransDescrip, ")
    '        .Append("       T.TermDescrip, ")
    '        .Append("       (Case LEN(PF.RateScheduleId) ")
    '        .Append("           when 36 then ")
    '        .Append("               (SELECT ")
    '        .Append("                   Case RSD.FlatAmount ")
    '        .Append("               		When 0.00 ")
    '        .Append("       					Then case RSD.UnitId ")
    '        .Append("                                  when 0 then Rate*(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId  in (select arClassSections.TermId from arClassSections,arTerm where arClassSections.TermId =arTerm.TermId and arTerm.StartDate <= '" & Date.Now.ToShortDateString & "' and arTerm.EndDate>= '" & Date.Now.ToShortDateString & "' and cohortstartdate = ?)) ")
    '        .Append("                                  when 1 then Rate*(SELECT Coalesce(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId in (select arClassSections.TermId from arClassSections,arTerm where arClassSections.TermId =arTerm.TermId and arTerm.StartDate <= '" & Date.Now.ToShortDateString & "' and arTerm.EndDate>= '" & Date.Now.ToShortDateString & "' and cohortstartdate = ?)) ")
    '        .Append("                                End ")
    '        .Append("   		            Else ")
    '        .Append("                           FlatAmount ")
    '        .Append("                   End ")
    '        .Append("       		FROM saRateSchedules RS, saRateScheduleDetails RSD ")
    '        .Append("               WHERE ")
    '        .Append("                   RS.RateScheduleId = RSD.RateScheduleId ")
    '        .Append("               AND	RS.RateScheduleId=PF.RateScheduleId ")
    '        .Append("               AND  (RSD.TuitionCategoryId = SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)) ")
    '        .Append("               AND  ((RSD.FlatAmount=0.00 AND RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId  in (select arClassSections.TermId from arClassSections,arTerm where arClassSections.TermId =arTerm.TermId and arTerm.StartDate <= '" & Date.Now.ToShortDateString & "' and arTerm.EndDate>= '" & Date.Now.ToShortDateString & "' and cohortstartdate = ?) ) and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) or (RSD.FlatAmount=0.00 AND RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT Coalesce(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId  in (select arClassSections.TermId from arClassSections,arTerm where arClassSections.TermId =arTerm.TermId and arTerm.StartDate <= '" & Date.Now.ToShortDateString & "' and arTerm.EndDate>= '" & Date.Now.ToShortDateString & "' and cohortstartdate = ?)) and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) OR (RSD.FlatAmount>0.00 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId in (select arClassSections.TermId from arClassSections,arTerm where arClassSections.TermId =arTerm.TermId and arTerm.StartDate <= '" & Date.Now.ToShortDateString & "' and arTerm.EndDate>= '" & Date.Now.ToShortDateString & "' and cohortstartdate = ?)) and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))))) ")

    '        .Append("   	 else ")
    '        .Append("           (Case PF.UnitId ")
    '        .Append("       	    when 0 Then PF.Amount* ")
    '        .Append("           		    (SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ ")
    '        .Append("                       WHERE ")
    '        .Append("                               R.TestId = CS.ClsSectionId ")
    '        .Append("                       AND	    CS.ReqId=RQ.ReqId ")
    '        .Append("               	    AND	    R.StuEnrollId=SE.StuEnrollId ")
    '        .Append("                       AND     CST.ClsSectionId=CS.ClsSectionId AND CST.TermId  in (select arClassSections.TermId from arClassSections,arTerm where arClassSections.TermId =arTerm.TermId and arTerm.StartDate <= '" & Date.Now.ToShortDateString & "' and arTerm.EndDate>= '" & Date.Now.ToShortDateString & "' and cohortstartdate = ?)   ")
    '        .Append("                 		AND     (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) OR (SE.TuitionCategoryId=PF.TuitionCategoryId ))) ")
    '        .Append("       	    when 1 then PF.Amount* ")
    '        .Append("           		    (SELECT SUM(RQ.Hours) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ ")
    '        .Append("                       WHERE ")
    '        .Append("                               R.TestId = CS.ClsSectionId ")
    '        .Append("                       AND	    CS.ReqId=RQ.ReqId ")
    '        .Append("                       AND     R.StuEnrollId=SE.StuEnrollId ")
    '        .Append("                       AND	    CST.ClsSectionId=CS.ClsSectionId AND CST.TermId  in (select arClassSections.TermId from arClassSections,arTerm where arClassSections.TermId =arTerm.TermId and arTerm.StartDate <= '" & Date.Now.ToShortDateString & "' and arTerm.EndDate>= '" & Date.Now.ToShortDateString & "' and cohortstartdate = ?)   ")
    '        .Append("                 		AND     (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) OR (SE.TuitionCategoryId=PF.TuitionCategoryId ))) ")
    '        .Append("               when 2 then PF.Amount * (select(case when Exists(select * from arResults where StuEnrollId= SE.StuEnrollId and TestId in (select ClsSectionId from arClassSections where TermId  in (select arClassSections.TermId from arClassSections,arTerm where arClassSections.TermId =arTerm.TermId and arTerm.StartDate <= '" & Date.Now.ToShortDateString & "' and arTerm.EndDate>= '" & Date.Now.ToShortDateString & "' and cohortstartdate = ?)   AND (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) OR (SE.TuitionCategoryId=PF.TuitionCategoryId )))) then 1 else 0 end)) ")
    '        .Append("            End) ")
    '        .Append("        End) TransAmount,T.TermId ")
    '        .Append("FROM saPeriodicFees PF, arTerm T, arStuEnrollments SE, syStatusCodes SC, arPrgVersions PV, arProgTypes PT ")
    '        .Append("WHERE ")
    '        .Append("       PF.TermId=T.TermId ")
    '        .Append("AND	PF.TermId in (select arClassSections.TermId from arClassSections,arTerm where arClassSections.TermId =arTerm.TermId and arTerm.StartDate <= '" & Date.Now.ToShortDateString & "' and arTerm.EndDate>= '" & Date.Now.ToShortDateString & "' and cohortstartdate = ?)   ")
    '        .Append("AND    SE.CampusId = ? ")
    '        .Append("AND    SE.StatusCodeId=SC.StatusCodeId ")
    '        .Append("AND    SC.SysStatusId In (7,9,13,20) ") ' 7=Future Start 9=Currently Attending 13=Re-entry 20=Academic Probation
    '        .Append("AND    SE.PrgVerId=PV.PrgVerId ")
    '        .Append("AND    PV.ProgTypId=PT.ProgTypId ")
    '        .Append("AND    ((PF.ApplyTo=0) OR (PF.ApplyTo=1 AND PV.ProgTypId=PF.ProgTypId) OR (PF.ApplyTo=2 AND PV.PrgVerId=PF.PrgVerId)) ")
    '        ' .Append("AND    ((PF.TermStartDate is null) or (PF.TermStartDate = SE.ExpStartDate)) ")
    '        ''MOdified by saraswathi on Jan 30 2009
    '        .Append("AND    ((PF.TermStartDate is null) or (PF.TermStartDate = SE.CohortStartDate)) and SE.CohortStartDate=? ")


    '    End With

    '    '   add parameters values to the query
    '    db.ClearParameters()

    '    ' Add TermId to the parameter list
    '    db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    ' Add TermId to the parameter list
    '    db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    ' Add TermId to the parameter list
    '    db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    ' Add TermId to the parameter list
    '    db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    ' Add TermId to the parameter list
    '    db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    ' Add TermId to the parameter list
    '    db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    ' Add TermId to the parameter list
    '    db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    ' Add TermId to the parameter list
    '    db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    ' Add TermId to the parameter list
    '    db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    ' Add CampusId to the parameter list
    '    db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    ' Add TermId to the parameter list
    '    db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


    '    '   Execute the query
    '    Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

    '    '   set time to be used on all records
    '    Dim rightNow = Date.Now

    '    '   we must encapsulate all DB updates in one transaction
    '    Dim groupTrans As OleDbTransaction = db.StartTransaction()

    '    '   loop throughout all records
    '    While dr.Read()
    '        'do not insert transaction with null amount
    '        If Not dr("TransAmount") Is System.DBNull.Value Then
    '            'do not insert transactions with zero amount
    '            If Not CType(dr("TransAmount"), Decimal) = 0.0 Then

    '                Dim PostChargeInfo As New PostChargeInfo

    '                '   set properties with data from DataReader
    '                With PostChargeInfo
    '                    .StuEnrollId = CType(dr("StuEnrollId"), Guid).ToString
    '                    .FeeLevelId = 1 'Term Fee
    '                    .FeeId = CType(dr("PeriodicFeeId"), Guid).ToString
    '                    .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
    '                    .PostChargeDescription = dr("TransCodeDescrip")
    '                    .PostChargeDate = postFeesDate
    '                    .Amount = dr("TransAmount")
    '                    .Reference = dr("TransDescrip")
    '                    .TermId = CType(dr("TermId"), Guid).ToString
    '                    .TransTypeId = 0
    '                    .IsPosted = True
    '                    .CampusId = campusId
    '                    .ModUser = user
    '                    .ModDate = rightNow
    '                End With

    '                '   add the entry to Transactions table
    '                Try
    '                    '   BuildQueryToInsertPostChargeInfo
    '                    BuildQueryToInsertPostChargeInfo(PostChargeInfo, db, sb, user, rightNow)

    '                    '   execute the query
    '                    db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

    '                Catch ex As OleDbException
    '                    '   rollback transaction if there were errors
    '                    groupTrans.Rollback()

    '                    '   do not report sql lost connection
    '                    If Not groupTrans.Connection Is Nothing Then
    '                        ' Report the error
    '                    End If

    '                    'Close Connection
    '                    db.CloseConnection()

    '                    '   return an error to the client
    '                    Return DALExceptions.BuildErrorMessage(ex)
    '                End Try
    '            End If
    '        End If
    '    End While

    '    '   commit transaction 
    '    groupTrans.Commit()

    '    'Close Connection
    '    db.CloseConnection()

    '    '   return with no errors
    '    Return ""

    'End Function
    Public Function DeleteAppliedFeesByTerm(ByVal stuEnrollId As String, ByVal termId As String, ByVal tuitionCategoryId As String, ByVal user As String, ByVal campusId As String) As String

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   BuildQueryToSelectBillingMethod
        Dim sb As New StringBuilder
        BuildQueryToSelectBillingMethod(stuEnrollId, db, sb)

        '   Execute the query
        Dim integerResult As Integer = db.RunParamSQLScalar(sb.ToString)

        '   delete applied course fees only if the BillingMethod is set to
        '   Term (0) and AutoBill is True
        If Not integerResult = 5 Then Return ""

        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       PF.TransCodeId, ")
            .Append("       (Select TermDescrip from arTerm where TermId=PF.TermId) + ' ' + (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId= ? )) TransDescrip, ")
            .Append("       T.TermDescrip, ")
            .Append("       (Case LEN(PF.RateScheduleId) ")
            .Append("           when 36 then ")
            .Append("               (SELECT ")
            .Append("                   Case RSD.FlatAmount ")
            .Append("               		When 0.00 ")
            .Append("       					Then case RSD.UnitId ")
            .Append("			                        when 0 then Rate*(SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId= ? AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) ")
            .Append("			                        when 1 then Rate*(SELECT SUM(RQ.Hours) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId= ? AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) ")
            .Append("                                End ")
            .Append("   		            Else ")
            .Append("                           FlatAmount * (select(case when Exists(select * from arResults where StuEnrollId= ? and TestId in (select ClsSectionId from arClassSections where TermId= ? )) then 1 else 0 end)) ")
            .Append("                   End ")
            .Append("       		FROM saRateSchedules RS, saRateScheduleDetails RSD ")
            .Append("               WHERE ")
            .Append("                   RS.RateScheduleId = RSD.RateScheduleId ")
            .Append("               AND	RS.RateScheduleId=PF.RateScheduleId ")
            .Append("               AND	(RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId= ? AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId=? ))) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT SUM(RQ.Hours) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId= ? AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId=? )))) ")
            .Append("   	 else ")
            .Append("           (Case PF.UnitId ")
            .Append("       	    when 0 Then PF.Amount* ")
            .Append("           		    (SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ ")
            .Append("                       WHERE ")
            .Append("                               R.TestId = CS.ClsSectionId ")
            .Append("                       AND	    CS.ReqId=RQ.ReqId ")
            .Append("               	    AND	    R.StuEnrollId= ? ")
            .Append("                       AND     CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ")
            .Append("                 		AND     (( ? Is NULL AND PF.TuitionCategoryId Is NULL) OR ( ? =PF.TuitionCategoryId ))) ")
            .Append("       	    when 1 then PF.Amount* ")
            .Append("           		    (SELECT SUM(RQ.Hours) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ ")
            .Append("                       WHERE ")
            .Append("                               R.TestId = CS.ClsSectionId ")
            .Append("                       AND	    CS.ReqId=RQ.ReqId ")
            .Append("                       AND     R.StuEnrollId= ? ")
            .Append("                       AND	    CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ")
            .Append("                 		AND     (( ? Is NULL AND PF.TuitionCategoryId Is NULL) OR ( ? =PF.TuitionCategoryId ))) ")
            .Append("               when 2 then PF.Amount * (select(case when Exists(select * from arResults where StuEnrollId= ? and TestId in (select ClsSectionId from arClassSections where TermId= ? )) then 1 else 0 end)) ")
            .Append("            End) ")
            .Append("        End) TransAmount ")
            .Append("FROM saPeriodicFees PF, arTerm T ")
            .Append("WHERE ")
            .Append("       PF.TermId=T.TermId ")
            .Append("AND	PF.TermId= ? ")
        End With

        '   add parameters values to the query
        db.ClearParameters()

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Add TuitionCategoryId the parameter list
        If tuitionCategoryId = Guid.Empty.ToString Then
            db.AddParameter("@TuitionCategoryId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@TuitionCategoryId", tuitionCategoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        '   Add TuitionCategoryId the parameter list
        If tuitionCategoryId = Guid.Empty.ToString Then
            db.AddParameter("@TuitionCategoryId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@TuitionCategoryId", tuitionCategoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Add TuitionCategoryId the parameter list
        If tuitionCategoryId = Guid.Empty.ToString Then
            db.AddParameter("@TuitionCategoryId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@TuitionCategoryId", tuitionCategoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        '   Add TuitionCategoryId the parameter list
        If tuitionCategoryId = Guid.Empty.ToString Then
            db.AddParameter("@TuitionCategoryId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@TuitionCategoryId", tuitionCategoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   loop throughout all records
        While dr.Read()

            Dim PostChargeInfo As New PostChargeInfo

            '   set properties with data from DataReader
            With PostChargeInfo
                .StuEnrollId = stuEnrollId
                .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                .PostChargeDescription = dr("TransDescrip")
                If Not dr("TransAmount") Is DBNull.Value Then .Amount = dr("TransAmount")
                .Reference = dr("TransDescrip")
                .TermId = termId
                .TransTypeId = 0
                .IsPosted = True
                .ModUser = user
            End With

            '   add the entry to Transactions table
            Try
                '   BuildQueryToDeletePostChargeInfo
                BuildQueryToDeletePostChargeInfo(PostChargeInfo, db, sb)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            Catch ex As OleDbException
                '   rollback transaction if there were errors
                groupTrans.Rollback()

                '   do not report sql lost connection
                If Not groupTrans.Connection Is Nothing Then
                    ' Report the error
                End If

                'Close Connection
                db.CloseConnection()

                '   return an error to the client
                Return DALExceptions.BuildErrorMessage(ex)
            End Try

        End While

        '   commit transaction 
        groupTrans.Commit()

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   return with no errors
        Return ""

    End Function
    Public Function DeleteAppliedFeesByTerm(ByVal termId As String, ByVal tuitionCategoryId As String, ByVal user As String, ByVal campusId As String) As String

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       SE.StuEnrollId, ")
            .Append("       PF.TransCodeId, ")
            .Append("       (Select TermDescrip from arTerm where TermId=PF.TermId) + ' ' + (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) TransDescrip, ")
            .Append("       T.TermDescrip, ")
            .Append("       (Case LEN(PF.RateScheduleId) ")
            .Append("           when 36 then ")
            .Append("               (SELECT ")
            .Append("                   Case RSD.FlatAmount ")
            .Append("               		When 0.00 ")
            .Append("       					Then case RSD.UnitId ")
            .Append("			                        when 0 then Rate*(SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) ")
            .Append("			                        when 1 then Rate*(SELECT SUM(RQ.Hours) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) ")
            .Append("                                End ")
            .Append("   		            Else ")
            .Append("                           FlatAmount * (select(case when Exists(select * from arResults where StuEnrollId= SE.StuEnrollId and TestId in (select ClsSectionId from arClassSections where TermId= ? )) then 1 else 0 end)) ")
            .Append("                   End ")
            .Append("       		FROM saRateSchedules RS, saRateScheduleDetails RSD ")
            .Append("               WHERE ")
            .Append("                   RS.RateScheduleId = RSD.RateScheduleId ")
            .Append("               AND	RS.RateScheduleId=PF.RateScheduleId ")
            .Append("               AND	(RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId=? ))) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT SUM(RQ.Hours) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId=? )))) ")
            .Append("   	 else ")
            .Append("           (Case PF.UnitId ")
            .Append("       	    when 0 Then PF.Amount* ")
            .Append("           		    (SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ ")
            .Append("                       WHERE ")
            .Append("                               R.TestId = CS.ClsSectionId ")
            .Append("                       AND	    CS.ReqId=RQ.ReqId ")
            .Append("               	    AND	    R.StuEnrollId=SE.StuEnrollId ")
            .Append("                       AND     CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ")
            .Append("                 		AND     (( ? Is NULL AND PF.TuitionCategoryId Is NULL) OR ( ? =PF.TuitionCategoryId ))) ")
            .Append("       	    when 1 then PF.Amount* ")
            .Append("           		    (SELECT SUM(RQ.Hours) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ ")
            .Append("                       WHERE ")
            .Append("                               R.TestId = CS.ClsSectionId ")
            .Append("                       AND	    CS.ReqId=RQ.ReqId ")
            .Append("                       AND     R.StuEnrollId=SE.StuEnrollId ")
            .Append("                       AND	    CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ")
            .Append("                 		AND     (( ? Is NULL AND PF.TuitionCategoryId Is NULL) OR ( ? =PF.TuitionCategoryId ))) ")
            .Append("               when 2 then PF.Amount * (select(case when Exists(select * from arResults where StuEnrollId= SE.StuEnrollId and TestId in (select ClsSectionId from arClassSections where TermId= ? )) then 1 else 0 end)) ")
            .Append("            End) ")
            .Append("        End) TransAmount ")
            '.Append("FROM saPeriodicFees PF, arTerm T, arStuEnrollments SE, saBillingMethods BM ")
            .Append("FROM saPeriodicFees PF, arTerm T, arStuEnrollments SE ")
            .Append("WHERE ")
            .Append("       PF.TermId=T.TermId ")
            .Append("AND	PF.TermId= ? ")
            .Append("AND    SE.CampusId = ? ")
            '.Append("AND	BM.BillingMethod*2+BM.AutoBill=4 ")
        End With

        '   add parameters values to the query
        db.ClearParameters()

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Add TuitionCategoryId the parameter list
        If tuitionCategoryId = Guid.Empty.ToString Then
            db.AddParameter("@TuitionCategoryId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@TuitionCategoryId", tuitionCategoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        '   Add TuitionCategoryId the parameter list
        If tuitionCategoryId = Guid.Empty.ToString Then
            db.AddParameter("@TuitionCategoryId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@TuitionCategoryId", tuitionCategoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Add TuitionCategoryId the parameter list
        If tuitionCategoryId = Guid.Empty.ToString Then
            db.AddParameter("@TuitionCategoryId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@TuitionCategoryId", tuitionCategoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        '   Add TuitionCategoryId the parameter list
        If tuitionCategoryId = Guid.Empty.ToString Then
            db.AddParameter("@TuitionCategoryId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@TuitionCategoryId", tuitionCategoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   loop throughout all records
        While dr.Read()

            Dim PostChargeInfo As New PostChargeInfo

            '   set properties with data from DataReader
            With PostChargeInfo
                .StuEnrollId = CType(dr("StuEnrollId"), Guid).ToString
                .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                .PostChargeDescription = dr("TransDescrip")
                If Not dr("TransAmount") Is DBNull.Value Then .Amount = dr("TransAmount")
                .TermId = termId
                .Reference = dr("TransDescrip")
                .TransTypeId = 0
                .IsPosted = True
                .ModUser = user
            End With

            '   add the entry to Transactions table
            Try
                '   BuildQueryToDeletePostChargeInfo
                BuildQueryToDeletePostChargeInfo(PostChargeInfo, db, sb)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            Catch ex As OleDbException
                '   rollback transaction if there were errors
                groupTrans.Rollback()

                '   do not report sql lost connection
                If Not groupTrans.Connection Is Nothing Then
                    ' Report the error
                End If

                'Close Connection
                db.CloseConnection()

                '   return an error to the client
                Return DALExceptions.BuildErrorMessage(ex)
            End Try

        End While

        '   commit transaction 
        groupTrans.Commit()

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   return with no errors
        Return ""

    End Function
    Public Function DeleteAppliedFeesByTerm(ByVal termId As String, ByVal user As String, ByVal campusId As String) As String

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       SE.StuEnrollId, ")
            .Append("       PF.TransCodeId, ")
            .Append("       (Select TermDescrip from arTerm where TermId=PF.TermId) + ' ' + (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) TransDescrip, ")
            .Append("       T.TermDescrip, ")
            .Append("       (Case LEN(PF.RateScheduleId) ")
            .Append("           when 36 then ")
            .Append("               (SELECT ")
            .Append("                   Case RSD.FlatAmount ")
            .Append("               		When 0.00 ")
            .Append("       					Then case RSD.UnitId ")
            .Append("			                        when 0 then Rate*(SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) ")
            .Append("			                        when 1 then Rate*(SELECT SUM(RQ.Hours) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) ")
            .Append("                                End ")
            .Append("   		            Else ")
            .Append("                           FlatAmount * (select(case when Exists(select * from arResults where StuEnrollId= SE.StuEnrollId and TestId in (select ClsSectionId from arClassSections where TermId= ? )) then 1 else 0 end)) ")
            .Append("                   End ")
            .Append("       		FROM saRateSchedules RS, saRateScheduleDetails RSD ")
            .Append("               WHERE ")
            .Append("                   RS.RateScheduleId = RSD.RateScheduleId ")
            .Append("               AND	RS.RateScheduleId=PF.RateScheduleId ")
            .Append("               AND	(RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId=? ))) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT SUM(RQ.Hours) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId=? )))) ")
            .Append("   	 else ")
            .Append("           (Case PF.UnitId ")
            .Append("       	    when 0 Then PF.Amount* ")
            .Append("           		    (SELECT SUM(RQ.Credits) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ ")
            .Append("                       WHERE ")
            .Append("                               R.TestId = CS.ClsSectionId ")
            .Append("                       AND	    CS.ReqId=RQ.ReqId ")
            .Append("               	    AND	    R.StuEnrollId=SE.StuEnrollId ")
            .Append("                       AND     CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ")
            .Append("                 		AND     (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) OR (SE.TuitionCategoryId=PF.TuitionCategoryId ))) ")
            .Append("       	    when 1 then PF.Amount* ")
            .Append("           		    (SELECT SUM(RQ.Hours) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ ")
            .Append("                       WHERE ")
            .Append("                               R.TestId = CS.ClsSectionId ")
            .Append("                       AND	    CS.ReqId=RQ.ReqId ")
            .Append("                       AND     R.StuEnrollId=SE.StuEnrollId ")
            .Append("                       AND	    CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ")
            .Append("                 		AND     (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) OR (SE.TuitionCategoryId=PF.TuitionCategoryId ))) ")
            .Append("               when 2 then PF.Amount * (select(case when Exists(select * from arResults where StuEnrollId= SE.StuEnrollId and TestId in (select ClsSectionId from arClassSections where TermId= ? )) then 1 else 0 end)) ")
            .Append("            End) ")
            .Append("        End) TransAmount ")
            '.Append("FROM saPeriodicFees PF, arTerm T, arStuEnrollments SE, saBillingMethods BM ")
            .Append("FROM saPeriodicFees PF, arTerm T, arStuEnrollments SE ")
            .Append("WHERE ")
            .Append("       PF.TermId=T.TermId ")
            .Append("AND	PF.TermId= ? ")
            .Append("AND    SE.CampusId = ? ")
            '.Append("AND	BM.BillingMethod*2+BM.AutoBill=4 ")
        End With

        '   add parameters values to the query
        db.ClearParameters()

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   loop throughout all records
        While dr.Read()

            Dim PostChargeInfo As New PostChargeInfo

            '   set properties with data from DataReader
            With PostChargeInfo
                .StuEnrollId = CType(dr("StuEnrollId"), Guid).ToString
                .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                .PostChargeDescription = dr("TransDescrip")
                If Not dr("TransAmount") Is DBNull.Value Then .Amount = dr("TransAmount")
                .TermId = termId
                .Reference = dr("TransDescrip")
                .TransTypeId = 0
                .IsPosted = True
                .ModUser = user
            End With

            '   add the entry to Transactions table
            Try
                '   BuildQueryToDeletePostChargeInfo
                BuildQueryToDeletePostChargeInfo(PostChargeInfo, db, sb)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            Catch ex As OleDbException
                '   rollback transaction if there were errors
                groupTrans.Rollback()

                '   do not report sql lost connection
                If Not groupTrans.Connection Is Nothing Then
                    ' Report the error
                End If

                If Not dr.IsClosed Then dr.Close()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

                '   return an error to the client
                Return DALExceptions.BuildErrorMessage(ex)
            End Try

        End While

        '   commit transaction 
        groupTrans.Commit()

        'Close Connection
        db.CloseConnection()

        '   return with no errors
        Return ""

    End Function
    Public Function GetFeesToBePostedByTermDS(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder = New StringBuilder
        With sb

            .Append("select * from (SELECT ")
            .Append("       SE.StuEnrollId,PF.PeriodicFeeId as FeeId, ")
            .Append("       PF.TransCodeId, ")
            .Append("       ((Select TransCodeDescrip from saTransCodes where TransCodeId=PF.TransCodeId) + Case PF.ApplyTo when 1 then '-' + PT.Description when 2 then '-' + PV.PrgVerDescrip else '' end) TransCodeDescrip, ")
            .Append("       (Select TransCodeDescrip from saTransCodes where TransCodeId=PF.TransCodeId) As FeeDescription, ")
            .Append("       (Case PF.ApplyTo when 0 then '-' + PV.PrgVerDescrip when 1 then '-' + PV.PrgVerDescrip when 2 then '-' + PV.PrgVerDescrip else '' end) As FeeDetailDescription, ")

            .Append("       (Select FirstName + ' ' + LastName from arStudent S where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId)) As StudentName, ")
            ''The Enrollment Status is also shown
            ''Query modified by Saraswathi
            ' .Append("       (Select StatusCodeDescrip from syStatusCodes where StatusCodeId in (Select StatusCodeID from arStuEnrollments  where StuEnrollId=SE.StuEnrollId)) as EnrollmentStatus ,")


            'include SSN or student identifier
            If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
                .Append("   (Select Coalesce(Case Len(S.SSN) When 9 then '***-**-' + SUBSTRING(SSN,6,4) else ' ' end, ' ') ")
            ElseIf CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToLower = "studentid" Then
                .Append("       (Select Coalesce(StudentNumber, ' ') ")
            Else
                .Append("   (Select Coalesce(" + CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToString + ", ' ') ")
            End If
            .Append("       from arStudent S ")
            .Append("       where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId) ")
            .Append("       ) As StudentIdentifier, ")

            .Append("       (Select TermDescrip from arTerm where TermId=PF.TermId) + ' ' + (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) TransDescrip, ")
            .Append("       T.TermDescrip, ")
            .Append("       (Case LEN(PF.RateScheduleId) ")
            .Append("           when 36 then ")
            .Append("               (SELECT ")
            .Append("                   Case RSD.FlatAmount ")
            .Append("               		When 0.00 ")
            .Append("       					Then case RSD.UnitId ")
            '.Append("                                  when 0 then Rate*(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) ")
            .Append("                                  when 0 then Rate*(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId and CST.TermId= ? AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(R.StuEnrollId,R.TestId)) ")
            '.Append("                                  when 1 then Rate*(SELECT Coalesce(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ) ")
            .Append("                                  when 1 then Rate*(SELECT Coalesce(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId and CST.TermId= ? AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(R.StuEnrollId,R.TestId)) ")
            .Append("                                End ")
            .Append("   		            Else ")
            .Append("                           FlatAmount ")
            .Append("                   End ")
            .Append("       		FROM saRateSchedules RS, saRateScheduleDetails RSD ")
            .Append("               WHERE ")
            .Append("                   RS.RateScheduleId = RSD.RateScheduleId ")
            .Append("               AND	RS.RateScheduleId=PF.RateScheduleId ")
            .Append("               AND  (RSD.TuitionCategoryId = SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)) ")
            '.Append("               AND  ((RSD.FlatAmount=0.00 AND RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CS.TermId= ? ) and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) or (RSD.FlatAmount=0.00 AND RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT Coalesce(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CS.TermId= ? ) and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))) OR (RSD.FlatAmount>0.00 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CS.TermId= ? ) and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)))))) ")
            .Append("               AND  ((RSD.FlatAmount=0.00 AND RSD.UnitId=0 AND RSD.MinUnits<=(SELECT COALESCE(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(R.StuEnrollId,R.TestId)) AND RSD.MaxUnits>=(SELECT COALESCE(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(R.StuEnrollId,R.TestId))) OR (RSD.FlatAmount=0.00 AND RSD.UnitId=1 AND RSD.MinUnits<=(SELECT COALESCE(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(R.StuEnrollId,R.TestId)) AND RSD.MaxUnits>=(SELECT COALESCE(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(R.StuEnrollId,R.TestId))) OR (RSD.FlatAmount>0.00 AND RSD.MinUnits<=(SELECT COALESCE(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(R.StuEnrollId,R.TestId)) AND RSD.MaxUnits>=(SELECT COALESCE(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(R.StuEnrollId,R.TestId))))) ")
            .Append("   	 else ")
            .Append("           (Case PF.UnitId ")
            .Append("       	    when 0 Then PF.Amount* ")
            '.Append("           		    (SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arReqs RQ ")
            .Append("           		    (SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ ")
            .Append("                       WHERE ")
            .Append("                               R.TestId = CS.ClsSectionId ")
            .Append("                       AND	    CS.ReqId=RQ.ReqId ")
            .Append("               	    AND	    R.StuEnrollId=SE.StuEnrollId ")
            .Append("                       AND     CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(R.StuEnrollId,R.TestId) ")
            .Append("                 		AND     (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) OR (SE.TuitionCategoryId=PF.TuitionCategoryId ))) ")
            .Append("       	    when 1 then PF.Amount* ")
            .Append("           		    (SELECT Coalesce(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ ")
            .Append("                       WHERE ")
            .Append("                               R.TestId = CS.ClsSectionId ")
            .Append("                       AND	    CS.ReqId=RQ.ReqId ")
            .Append("                       AND     R.StuEnrollId=SE.StuEnrollId ")
            '.Append("                       AND	    CS.TermId= ? ")
            .Append("                       AND     CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(R.StuEnrollId,R.TestId) ")
            .Append("                 		AND     (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) OR (SE.TuitionCategoryId=PF.TuitionCategoryId ))) ")
            '.Append("               when 2 then PF.Amount * (select(case when Exists(select * from arResults where StuEnrollId= SE.StuEnrollId and TestId in (select ClsSectionId from arClassSections where TermId= ? AND (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) OR (SE.TuitionCategoryId=PF.TuitionCategoryId )))) then 1 else 0 end)) ")
            .Append("               when 2 then PF.Amount * (select(case when Exists(select * from arResults where StuEnrollId= SE.StuEnrollId and TestId in (select CS1.ClsSectionId from arClassSections CS1, arClassSectionterms CST where CST.ClsSectionId=CS1.ClsSectionId AND CST.TermId= ? AND (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) OR (SE.TuitionCategoryId=PF.TuitionCategoryId )))) then 1 else 0 end)) ")
            .Append("            End) ")
            .Append("        End) TransAmount, ")
            .Append("       Convert(smalldatetime,getdate()) As TransDate, ")
            .Append("(select count(*) from saTransactions T1 where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=PF.TransCodeId and T1.TermId=T.TermId and ")
            .Append(" Voided=0 and IsAutomatic=1 and T1.FeeLevelId=1 and T1.CampusId = SE.CampusId ) As NumberOfTransactionsPosted,T.TermId ")


            '.Append("FROM saPeriodicFees PF, arTerm T, arStuEnrollments SE, saBillingMethods BM, syStatusCodes SC, arPrgVersions PV, arProgTypes PT ")
            .Append("FROM saPeriodicFees PF, arTerm T, arStuEnrollments SE, syStatusCodes SC, arPrgVersions PV, arProgTypes PT ")
            .Append("WHERE ")
            .Append("       PF.TermId=T.TermId ")
            .Append("AND	PF.TermId= ? ")
            .Append("AND    SE.CampusId = ? ")
            ''Query Modified by Saraswathi to bring the Students whose Status is FutureStart or Currently attending
            'The following line was commented to fix the mantis issue 17760 - commented by Balaji
            '.Append(" and SE.StatusCodeId in(Select StatusCodeId from syStatusCodes where StatusCodeDescrip like 'Futur%Start%' or StatusCodeDescrip like 'Curr%Att%'  or StatusCodeDescrip like 'Acad%Prob%'  )")
            'comment ends here - 17760

            '.Append("AND	BM.BillingMethod*2+BM.AutoBill=4 ")
            .Append("AND    SE.StatusCodeId=SC.StatusCodeId ")
            .Append("AND    SC.SysStatusId In (7,9,13,20) ") ' 7=Future Start 9=Currently Attending 13=Re-entry 20=Academic Probation
            .Append("AND    SE.PrgVerId=PV.PrgVerId ")
            .Append("AND    PV.ProgTypId=PT.ProgTypId ")
            .Append("AND    ((PF.ApplyTo=0) OR (PF.ApplyTo=1 AND PV.ProgTypId=PF.ProgTypId) OR (PF.ApplyTo=2 AND PV.PrgVerId=PF.PrgVerId)) ")
            .Append("AND    ((PF.TermStartDate is null) or (PF.TermStartDate = SE.ExpStartDate)) ")
            .Append("    AND	(((pf.RateScheduleId IS NULL AND SE.TuitionCategoryId IS NULL AND pf.TuitionCategoryId IS NULL) OR (pf.RateScheduleId IS NULL AND SE.TuitionCategoryId=pf.TuitionCategoryId)) OR pf.RateScheduleId IS NOT NULL) ")
            .Append(" )res WHERE res.TransAmount <>0 ")
        End With

        '   add parameters values to the query
        db.ClearParameters()

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        'db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '' Add TermId to the parameter list
        'db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '' Add TermId to the parameter list
        'db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetFeesToBePostedByTermDSForCohortStart(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder = New StringBuilder
        With sb
            .Append("select * from (SELECT ")
            .Append("       SE.StuEnrollId,PF.PeriodicFeeId as FeeId, ")
            .Append("       PF.TransCodeId, ")
            .Append("       ((Select TransCodeDescrip from saTransCodes where TransCodeId=PF.TransCodeId) + Case PF.ApplyTo when 1 then '-' + PT.Description when 2 then '-' + PV.PrgVerDescrip else '' end) TransCodeDescrip, ")
            .Append("       (Select TransCodeDescrip from saTransCodes where TransCodeId=PF.TransCodeId) As FeeDescription, ")
            .Append("       (Case PF.ApplyTo when 0 then '- All Programs' when 1 then '-' + PT.Description when 2 then '-' + PV.PrgVerDescrip else '' end) As FeeDetailDescription, ")
            ''The Enrollment Status is also shown
            ''Query modified by Saraswathi
            '.Append("       (Select StatusCodeDescrip from syStatusCodes where StatusCodeId in (Select StatusCodeID from arStuEnrollments  where StuEnrollId=SE.StuEnrollId)) as EnrollmentStatus ,")

            .Append("       (Select FirstName + ' ' + LastName from arStudent S where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId)) As StudentName, ")
            'include SSN or student identifier
            If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
                .Append("   (Select Coalesce(Case Len(S.SSN) When 9 then '***-**-' + SUBSTRING(SSN,6,4) else ' ' end, ' ') ")
            ElseIf CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToLower = "studentid" Then
                .Append("       (Select Coalesce(StudentNumber, ' ') ")
            Else
                .Append("   (Select Coalesce(" + CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToString + ", ' ') ")
            End If
            .Append("       from arStudent S ")
            .Append("       where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId) ")
            .Append("       ) As StudentIdentifier, ")

            .Append("       (Select TermDescrip from arTerm where TermId=PF.TermId) + ' ' + (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) TransDescrip, ")
            .Append("       T.TermDescrip, ")
            .Append("       (Case LEN(PF.RateScheduleId) ")
            .Append("           when 36 then ")
            .Append("               (SELECT ")
            .Append("                   Case RSD.FlatAmount ")
            .Append("               		When 0.00 ")
            .Append("       					Then case RSD.UnitId ")

            .Append("                                  when 0 then Rate*(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId and CST.TermId  in (select arClassSections.TermId from arClassSections,arTerm where arClassSections.TermId =arTerm.TermId and arTerm.StartDate <= '" & Date.Now.ToShortDateString & "' and arTerm.EndDate >= '" & Date.Now.ToShortDateString & "' and cohortstartdate = ?)   ) ")

            .Append("                                  when 1 then Rate*(SELECT Coalesce(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId and CST.TermId  in (select arClassSections.TermId from arClassSections,arTerm where arClassSections.TermId =arTerm.TermId and arTerm.StartDate <= '" & Date.Now.ToShortDateString & "' and arTerm.EndDate>= '" & Date.Now.ToShortDateString & "' and cohortstartdate = ?)   ) ")
            .Append("                                End ")
            .Append("   		            Else ")
            .Append("                           FlatAmount ")
            .Append("                   End ")
            .Append("       		FROM saRateSchedules RS, saRateScheduleDetails RSD ")
            .Append("               WHERE ")
            .Append("                   RS.RateScheduleId = RSD.RateScheduleId ")
            .Append("               AND	RS.RateScheduleId=PF.RateScheduleId ")
            .Append("               AND  (RSD.TuitionCategoryId = SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)) ")

            .Append("               AND  ((RSD.FlatAmount=0.00 AND RSD.UnitId=0 and RSD.MaxUnits=(Select MIN(MaxUnits) from saRateScheduleDetails Where MaxUnits>=(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId  in (select arClassSections.TermId from arClassSections,arTerm where arClassSections.TermId =arTerm.TermId and arTerm.StartDate <= '" & Date.Now.ToShortDateString & "' and arTerm.EndDate>= '" & Date.Now.ToShortDateString & "' and cohortstartdate = ?)   ) and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null))) AND RSD.MinUnits<=(SELECT COALESCE(SUM(RQ.Credits),0) FROM arResults R,arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId  IN (SELECT arClassSections.TermId FROM arClassSections,arTerm WHERE arClassSections.TermId =arTerm.TermId AND arTerm.StartDate <= GETDATE() AND arTerm.EndDate>= GETDATE() AND cohortstartdate = ?)) AND RSD.MaxUnits>=(SELECT COALESCE(SUM(RQ.Credits),0) FROM arResults R,arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId  IN (SELECT arClassSections.TermId FROM arClassSections,arTerm WHERE arClassSections.TermId =arTerm.TermId AND arTerm.StartDate <= GETDATE() AND arTerm.EndDate>= GETDATE() AND cohortstartdate = ?))) or (RSD.FlatAmount=0.00 AND RSD.UnitId=1 and RSD.MaxUnits=(Select Min(MaxUnits) from saRateScheduleDetails Where MaxUnits>=(SELECT Coalesce(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId   in (select arClassSections.TermId from arClassSections,arTerm where arClassSections.TermId =arTerm.TermId and arTerm.StartDate <= '" & Date.Now.ToShortDateString & "' and arTerm.EndDate>= '" & Date.Now.ToShortDateString & "' and cohortstartdate = ?)   ) and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null))) AND RSD.MinUnits<=(SELECT COALESCE(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId IN (SELECT arClassSections.TermId FROM arClassSections,arTerm WHERE arClassSections.TermId =arTerm.TermId AND arTerm.StartDate <= GETDATE() AND arTerm.EndDate>=GETDATE() AND cohortstartdate = ?)) AND RSD.MaxUnits>=(SELECT COALESCE(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId IN (SELECT arClassSections.TermId FROM arClassSections,arTerm WHERE arClassSections.TermId =arTerm.TermId AND arTerm.StartDate <= GETDATE() AND arTerm.EndDate>=GETDATE() AND cohortstartdate = ?))) OR (RSD.FlatAmount>0.00 and RSD.MaxUnits=(Select Min(MaxUnits) from saRateScheduleDetails Where MaxUnits>=(SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId in (select arClassSections.TermId from arClassSections,arTerm where arClassSections.TermId =arTerm.TermId and arTerm.StartDate <= '" & Date.Now.ToShortDateString & "' and arTerm.EndDate>= '" & Date.Now.ToShortDateString & "' and cohortstartdate = ?)   ) and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null))) AND RSD.MinUnits<=(SELECT COALESCE(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId IN (SELECT arClassSections.TermId FROM arClassSections,arTerm WHERE arClassSections.TermId =arTerm.TermId AND arTerm.StartDate <= GETDATE() AND arTerm.EndDate>= GETDATE() AND cohortstartdate = ?)) AND RSD.MaxUnits>=(SELECT COALESCE(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ WHERE R.TestId = CS.ClsSectionId AND CS.ReqId=RQ.ReqId AND R.StuEnrollId=SE.StuEnrollId AND CST.ClsSectionId=CS.ClsSectionId AND CST.TermId IN (SELECT arClassSections.TermId FROM arClassSections,arTerm WHERE arClassSections.TermId =arTerm.TermId AND arTerm.StartDate <= GETDATE() AND arTerm.EndDate>= GETDATE() AND cohortstartdate = ?))))) ")
            .Append("   	 else ")
            .Append("           (Case PF.UnitId ")
            .Append("       	    when 0 Then PF.Amount* ")

            .Append("           		    (SELECT Coalesce(SUM(RQ.Credits),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ ")
            .Append("                       WHERE ")
            .Append("                               R.TestId = CS.ClsSectionId ")
            .Append("                       AND	    CS.ReqId=RQ.ReqId ")
            .Append("               	    AND	    R.StuEnrollId=SE.StuEnrollId ")
            .Append("                       AND     CST.ClsSectionId=CS.ClsSectionId AND CST.TermId  in (select arClassSections.TermId from arClassSections,arTerm where arClassSections.TermId =arTerm.TermId and arTerm.StartDate <= '" & Date.Now.ToShortDateString & "' and arTerm.EndDate>= '" & Date.Now.ToShortDateString & "' and cohortstartdate = ?)   ")
            .Append("                 		AND     (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) OR (SE.TuitionCategoryId=PF.TuitionCategoryId ))) ")
            .Append("       	    when 1 then PF.Amount* ")
            .Append("           		    (SELECT Coalesce(SUM(RQ.Hours),0) FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ ")
            .Append("                       WHERE ")
            .Append("                               R.TestId = CS.ClsSectionId ")
            .Append("                       AND	    CS.ReqId=RQ.ReqId ")
            .Append("                       AND     R.StuEnrollId=SE.StuEnrollId ")

            .Append("                       AND     CST.ClsSectionId=CS.ClsSectionId AND CST.TermId  in (select arClassSections.TermId from arClassSections,arTerm where arClassSections.TermId =arTerm.TermId and arTerm.StartDate <= '" & Date.Now.ToShortDateString & "' and arTerm.EndDate>= '" & Date.Now.ToShortDateString & "' and cohortstartdate = ?)   ")
            .Append("                 		AND     (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) OR (SE.TuitionCategoryId=PF.TuitionCategoryId ))) ")

            .Append("               when 2 then PF.Amount * (select(case when Exists(select * from arResults where StuEnrollId= SE.StuEnrollId and TestId in (select CS1.ClsSectionId from arClassSections CS1, arClassSectionterms CST where CST.ClsSectionId=CS1.ClsSectionId AND CST.TermId in (select arClassSections.TermId from arClassSections,arTerm where arClassSections.TermId =arTerm.TermId and arTerm.StartDate <= '" & Date.Now.ToShortDateString & "' and arTerm.EndDate>= '" & Date.Now.ToShortDateString & "' and cohortstartdate = ?)  AND (( SE.TuitionCategoryId Is NULL AND PF.TuitionCategoryId Is NULL) OR (SE.TuitionCategoryId=PF.TuitionCategoryId )))) then 1 else 0 end)) ")
            .Append("            End) ")
            .Append("        End) TransAmount, ")
            .Append("       Convert(smalldatetime,getdate()) As TransDate, ")
            .Append("(select count(*) from saTransactions T1 where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=PF.TransCodeId and T1.TermId=T.TermId and ")
            .Append(" Voided=0 and IsAutomatic=1 and T1.FeeLevelId=1 and T1.CampusId = SE.CampusId ) As NumberOfTransactionsPosted,T.TermId ")

            .Append("FROM saPeriodicFees PF, arTerm T, arStuEnrollments SE, syStatusCodes SC, arPrgVersions PV, arProgTypes PT ")
            .Append("WHERE ")
            .Append("       PF.TermId=T.TermId ")
            .Append("AND	PF.TermId  in (select arClassSections.TermId from arClassSections,arTerm where arClassSections.TermId =arTerm.TermId and arTerm.StartDate <= '" & Date.Now.ToShortDateString & "' and arTerm.EndDate>= '" & Date.Now.ToShortDateString & "' and cohortstartdate = ?)   ")
            .Append("AND    SE.CampusId = ? ")
            ''Query Modified by Saraswathi to bring the Students whose Status is FutureStart or Currently attending
            '.Append(" and SE.StatusCodeId in(Select StatusCodeId from syStatusCodes where StatusCodeDescrip like 'Futur%Start%' or StatusCodeDescrip like 'Curr%Att%'  or StatusCodeDescrip like 'Acad%Prob%'  )")


            .Append("AND    SE.StatusCodeId=SC.StatusCodeId ")
            .Append("AND    SC.SysStatusId In (7,9,13,20) ") ' 7=Future Start 9=Currently Attending 13=Re-entry 20=Academic Probation
            .Append("AND    SE.PrgVerId=PV.PrgVerId ")
            .Append("AND    PV.ProgTypId=PT.ProgTypId ")
            .Append("AND    ((PF.ApplyTo=0) OR (PF.ApplyTo=1 AND PV.ProgTypId=PF.ProgTypId) OR (PF.ApplyTo=2 AND PV.PrgVerId=PF.PrgVerId)) ")
            ' .Append("AND    ((PF.TermStartDate is null) or (PF.TermStartDate = SE.ExpStartDate)) ")
            .Append("AND    ((PF.TermStartDate is null) or (PF.TermStartDate = SE.CohortStartDate))  ")

            ''Added on jan 22 2009
            ''Added by Saraswathi to fetch the students belonging to the selected cohortstartdate only
            .Append("AND SE.CohortStartDate=? ")
            .Append("AND	(((pf.RateScheduleId IS NULL AND SE.TuitionCategoryId IS NULL AND pf.TuitionCategoryId IS NULL) OR (pf.RateScheduleId IS NULL AND SE.TuitionCategoryId=pf.TuitionCategoryId)) OR pf.RateScheduleId IS NOT NULL)  ")
            .Append("AND exists( ")
            .Append("           select R.* ")
            .Append("           from arResults R, arClassSections CS, arClassSectionTerms CST ")
            .Append("           where R.TestId = CS.ClsSectionId ")
            .Append("           and R.StuEnrollId=SE.StuEnrollId ")
            .Append("           and CST.ClsSectionId=CS.ClsSectionId ")
            .Append("           and CST.TermId=PF.TermId ")
            .Append("           and CS.CampusId=? ")
            .Append("           )")
            .Append(" )res WHERE res.TransAmount <>0 ")
        End With

        '   add parameters values to the query
        db.ClearParameters()

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ''Added on jan 22 2009
        ''Added by Saraswathi to fetch the students belonging to the selected cohortstartdate only
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        '   Return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetFeesToBePostedByCourseDS(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder = New StringBuilder
        With sb
            .Append("select * from (SELECT ")
            .Append("       R.StuEnrollId,CF.CourseFeeId as FeeId, ")
            .Append("       CF.TransCodeId, ")
            .Append("       ((Select TransCodeDescrip from saTransCodes where TransCodeId=CF.TransCodeId) + '-' + RQ.Descrip) TransCodeDescrip, ")
            .Append("       (Select TransCodeDescrip from saTransCodes where TransCodeId=CF.TransCodeId) As FeeDescription, ")
            .Append("       RQ.Descrip As FeeDetailDescription, ")
            ''The Enrollment Status is also shown
            ''Query modified by Saraswathi
            ' .Append("       (Select StatusCodeDescrip from syStatusCodes where StatusCodeId in (Select StatusCodeID from arStuEnrollments  where StuEnrollId=SE.StuEnrollId)) as EnrollmentStatus ,")

            .Append("       (Select FirstName + ' ' + LastName from arStudent S where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId)) As StudentName, ")
            'include SSN or student identifier
            If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
                .Append("   (Select Coalesce(Case Len(S.SSN) When 9 then '***-**-' + SUBSTRING(SSN,6,4) else ' ' end, ' ') ")
            ElseIf CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToLower = "studentid" Then
                .Append("       (Select Coalesce(StudentNumber, ' ') ")
            Else
                .Append("   (Select Coalesce(" + CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToString + ", ' ') ")
            End If
            .Append("       from arStudent S ")
            .Append("       where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId) ")
            .Append("       ) As StudentIdentifier, ")

            .Append("       RQ.Descrip + ' ' + (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) TransDescrip, ")
            .Append("       (Case LEN(CF.RateScheduleId) ")
            .Append("	        when 36 then ")
            .Append("		        (SELECT ")
            .Append("			        Case RSD.FlatAmount ")
            .Append("			            When 0.00 ")
            .Append("				            Then case RSD.UnitId ")
            .Append("						            when 0 then Rate*RQ.Credits ")
            .Append("						            when 1 then Rate*RQ.Hours ")
            .Append("                               End ")
            .Append("			            Else ")
            .Append("                           FlatAmount ")
            .Append("                   End ")
            .Append("		        FROM saRateSchedules RS, saRateScheduleDetails RSD ")
            .Append("               WHERE ")
            .Append("                       RS.RateScheduleId = RSD.RateScheduleId ")
            .Append("		        AND	    RS.RateScheduleId=CF.RateScheduleId ")
            '.Append("               AND     RSD.TuitionCategoryId = SE.TuitionCategoryId ")
            '.Append("		        AND	    (RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId)) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Hours and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId))) ")
            .Append("               AND  (RSD.TuitionCategoryId = SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)) ")
            .Append("              AND  ((RSD.FlatAmount=0.00 AND RSD.UnitId=0  and RSD.MinUnits<=RQ.Credits and RSD.MaxUnits>=RQ.Credits) or (RSD.FlatAmount=0.00 AND RSD.UnitId=1 and RSD.MinUnits<=RQ.Hours and RSD.MaxUnits>=RQ.Hours) OR (RSD.FlatAmount>0.00 and RSD.MinUnits<=RQ.Credits and RSD.MaxUnits>=RQ.Credits))) ")

            .Append("	        else ")
            .Append("	            (Case CF.UnitId ")
            .Append("			        when 0 Then CF.Amount*RQ.Credits ")
            .Append("			        when 1 then CF.Amount*RQ.Hours ")
            .Append("			        when 2 then CF.Amount ")
            .Append("		        End) ")
            .Append("	    End) TransAmount, ")
            '.Append("1000 as TransAmount, ")
            .Append("       Convert(smalldatetime,getdate()) As TransDate, ")
            .Append("(select count(*) from saTransactions T1 where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=CF.TransCodeId and T1.TermId=CS.TermId and ")
            .Append(" Voided=0 and IsAutomatic=1 and T1.FeeLevelId=3 and T1.CampusId = SE.campusid  AND T1.TransReference=LTRIM(RTRIM(SUBSTRING( RQ.Descrip + ' ' + S.LastName + ' ' + S.FirstName,1,50)))) As NumberOfTransactionsPosted,CS.TermId ")

            '.Append("FROM   arResults R, arStuEnrollments SE, arClassSections CS, arClassSectionTerms CST,  arReqs RQ, saCourseFees CF, saBillingMethods BM ")
            '.Append("FROM   arResults R, arStuEnrollments SE, arClassSections CS, arClassSectionTerms CST,  arReqs RQ, saCourseFees CF ")
            .Append("FROM   arResults R, arStuEnrollments SE,arStudent S, arClassSections CS, arClassSectionTerms CST,  arReqs RQ, saCourseFees CF,syStatusCodes SC, arPrgVersions PV ")
            .Append("WHERE ")
            .Append("       R.TestId=CS.ClsSectionId ")
            .Append("AND	R.StuEnrollId=SE.StuEnrollId ")
            .Append("AND SE.studentID = S.studentID ")
            .Append("AND	CS.ReqId=CF.CourseId ")
            .Append("AND	CF.CourseId=RQ.reqId ")
            ''Query Modified by Saraswathi to bring the Students whose Status is FutureStart or Currently attending
            '.Append(" and SE.StatusCodeId in(Select StatusCodeId from syStatusCodes where StatusCodeDescrip like 'Futur%Start%' or StatusCodeDescrip like 'Curr%Att%'  or StatusCodeDescrip like 'Acad%Prob%'  )")
            .Append("AND    SE.StatusCodeId=SC.StatusCodeId ")
            .Append("AND    SC.SysStatusId In (7,9,13,20) ") ' 7=Future Start 9=Currently Attending 13=Re-entry 20=Academic Probation
            .Append("AND    SE.PrgVerId=PV.PrgVerId ")

            .Append("AND    SE.CampusId = ? AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(R.StuEnrollId,R.TestId) ")
            '.Append("AND	CF.StartDate < GetDate() ")
            '.Append("AND	CF.StartDate =	(select top 1 StartDate from saCourseFees where CourseId=CF.CourseId and StartDate <=GetDate() order by StartDate desc) ")
            .Append("AND EXISTS(select * from saCourseFees CF2 where CF2.CourseId=CF.CourseId and CF2.CourseFeeId=CF.CourseFeeId and CF2.StartDate=(select MAX(startdate) from saCourseFees CF3 where CF2.CourseId=CF3.CourseId and CF2.TransCodeId=CF3.TransCodeId and CF3.StartDate <= GetDate() and	(((CF3.RateScheduleId IS NULL AND SE.TuitionCategoryId IS NULL AND CF3.TuitionCategoryId IS NULL) OR (CF3.RateScheduleId IS NULL AND SE.TuitionCategoryId=CF3.TuitionCategoryId)) OR (CF3.RateScheduleId IS NOT NULL AND EXISTS(SELECT rsd.TuitionCategoryId FROM saRateScheduleDetails rsd WHERE rsd.RateScheduleId=CF3.RateScheduleId AND rsd.TuitionCategoryId=SE.TuitionCategoryId)) OR (CF3.RateScheduleId IS NOT NULL AND SE.TuitionCategoryId IS NULL AND EXISTS(SELECT rsd.* FROM saRateScheduleDetails rsd WHERE rsd.RateScheduleId=CF3.RateScheduleId AND rsd.TuitionCategoryId is null))))) ")
            .Append("AND	CST.ClsSectionId=CS.ClsSectionId AND CST.TermId = ? ")
            '.Append("AND	BM.BillingMethod*2+BM.AutoBill=2 ")
            .Append("AND	(((CF.RateScheduleId IS NULL AND SE.TuitionCategoryId IS NULL AND CF.TuitionCategoryId IS NULL) OR (CF.RateScheduleId IS NULL AND SE.TuitionCategoryId=CF.TuitionCategoryId)) OR (CF.RateScheduleId IS NOT NULL AND EXISTS(SELECT rsd.TuitionCategoryId FROM saRateScheduleDetails rsd WHERE rsd.RateScheduleId=CF.RateScheduleId AND rsd.TuitionCategoryId=SE.TuitionCategoryId)) OR(CF.RateScheduleId IS NOT NULL AND SE.TuitionCategoryId IS NULL AND EXISTS(SELECT rsd.* FROM saRateScheduleDetails rsd WHERE rsd.RateScheduleId=CF.RateScheduleId AND rsd.TuitionCategoryId is null))) ")
            .Append(" )res WHERE res.TransAmount <>0 ")
        End With

        ' Add CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return dataset
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)
        Dim dt As DataTable = ds.Tables(0)
        'Dim str As String
        'str = dt.Rows(0)("StudentName").ToString

        Return ds
    End Function

    Public Function GetFeesToBePostedByCourseDSForCohortStart(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder = New StringBuilder
        With sb
            .Append("select * from (SELECT ")
            .Append("       R.StuEnrollId,CF.CourseFeeId as FeeId,  ")
            .Append("       CF.TransCodeId, ")
            .Append("       ((Select TransCodeDescrip from saTransCodes where TransCodeId=CF.TransCodeId) + '-' + RQ.Descrip) TransCodeDescrip, ")
            .Append("       (Select TransCodeDescrip from saTransCodes where TransCodeId=CF.TransCodeId) As FeeDescription, ")
            .Append("       RQ.Descrip As FeeDetailDescription, ")
            ''The Enrollment Status is also shown
            ''Query modified by Saraswathi
            '.Append("       (Select StatusCodeDescrip from syStatusCodes where StatusCodeId in (Select StatusCodeID from arStuEnrollments  where StuEnrollId=SE.StuEnrollId)) as EnrollmentStatus ,")

            .Append("       (Select FirstName + ' ' + LastName from arStudent S where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId)) As StudentName, ")
            'include SSN or student identifier
            If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
                .Append("   (Select Coalesce(Case Len(S.SSN) When 9 then '***-**-' + SUBSTRING(SSN,6,4) else ' ' end, ' ') ")
            ElseIf CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToLower = "studentid" Then
                .Append("       (Select Coalesce(StudentNumber, ' ') ")
            Else
                .Append("   (Select Coalesce(" + CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToString + ", ' ') ")
            End If
            .Append("       from arStudent S ")
            .Append("       where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId) ")
            .Append("       ) As StudentIdentifier, ")

            .Append("       RQ.Descrip + ' ' + (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) TransDescrip, ")
            .Append("       (Case LEN(CF.RateScheduleId) ")
            .Append("	        when 36 then ")
            .Append("		        (SELECT ")
            .Append("			        Case RSD.FlatAmount ")
            .Append("			            When 0.00 ")
            .Append("				            Then case RSD.UnitId ")
            .Append("						            when 0 then Rate*RQ.Credits ")
            .Append("						            when 1 then Rate*RQ.Hours ")
            .Append("                               End ")
            .Append("			            Else ")
            .Append("                           FlatAmount ")
            .Append("                   End ")
            .Append("		        FROM saRateSchedules RS, saRateScheduleDetails RSD ")
            .Append("               WHERE ")
            .Append("                       RS.RateScheduleId = RSD.RateScheduleId ")
            .Append("		        AND	    RS.RateScheduleId=CF.RateScheduleId ")
            '.Append("               AND     RSD.TuitionCategoryId = SE.TuitionCategoryId ")
            '.Append("		        AND	    (RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Credits and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId)) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=RQ.Hours and RateScheduleId=RSD.RateScheduleId and RSD.TuitionCategoryId=SE.TuitionCategoryId))) ")
            .Append("               AND  (RSD.TuitionCategoryId = SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null)) ")
            .Append("               AND  ((RSD.FlatAmount=0.00 AND RSD.UnitId=0 and RSD.MinUnits<=RQ.Credits and RSD.MaxUnits>=RQ.Credits) or (RSD.FlatAmount=0.00 AND RSD.UnitId=1 and RSD.MinUnits<=RQ.Hours and RSD.MaxUnits>=RQ.Hours) OR (RSD.FlatAmount>0.00 and RSD.MinUnits<=RQ.Credits and RSD.MaxUnits>=RQ.Credits))) ")
            .Append("	        else ")
            .Append("	            (Case CF.UnitId ")
            .Append("			        when 0 Then CF.Amount*RQ.Credits ")
            .Append("			        when 1 then CF.Amount*RQ.Hours ")
            .Append("			        when 2 then CF.Amount ")
            .Append("		        End) ")
            .Append("	    End) TransAmount, ")
            .Append("       Convert(smalldatetime,getdate()) As TransDate, ")
            .Append("(select count(*) from saTransactions T1 where T1.StuEnrollId=SE.StuEnrollId and T1.TransCodeId=CF.TransCodeId and T1.TermId=CS.TermId and ")
            .Append(" Voided=0 and IsAutomatic=1 and T1.FeeLevelId=3 and T1.CampusId = SE.campusid  AND T1.TransReference=SUBSTRING( RQ.Descrip + ' ' + S.LastName + ' ' + S.FirstName,1,50)) As NumberOfTransactionsPosted,CS.TermId ")

            '.Append("FROM   arResults R, arStuEnrollments SE, arClassSections CS, arClassSectionTerms CST,  arReqs RQ, saCourseFees CF, saBillingMethods BM ")
            '.Append("FROM   arResults R, arStuEnrollments SE, arClassSections CS, arClassSectionTerms CST,  arReqs RQ, saCourseFees CF ")
            .Append("FROM   arResults R, arStuEnrollments SE,arStudent S, arClassSections CS, arClassSectionTerms CST,  arReqs RQ, saCourseFees CF,syStatusCodes SC ")
            .Append("WHERE ")
            .Append("       R.TestId=CS.ClsSectionId ")
            .Append("AND	R.StuEnrollId=SE.StuEnrollId ")
            .Append("AND	S.StudentId=SE.StudentId ")
            .Append("AND	CS.ReqId=CF.CourseId ")
            .Append("AND	CF.CourseId=RQ.reqId ")
            ''Query Modified by Saraswathi to bring the Students whose Status is FutureStart or Currently attending
            '.Append(" and SE.StatusCodeId in(Select StatusCodeId from syStatusCodes where StatusCodeDescrip like 'Futur%Start%' or StatusCodeDescrip like 'Curr%Att%'  or StatusCodeDescrip like 'Acad%Prob%'  )")
            .Append("AND    SE.StatusCodeId=SC.StatusCodeId ")
            .Append("AND    SC.SysStatusId In (7,9,13,20) ") ' 7=Future Start 9=Currently Attending 13=Re-entry 20=Academic Probation
            .Append("AND    SE.CampusId = ? ")
            ''Query Modified by saraswathi Lakshmanan on jan 30 2009
            ''The student cohortStartDate is also matched
            .Append(" AND	SE.CohortStartDate= ? ")
            '.Append("AND	CF.StartDate < GetDate() ")
            '.Append("AND	CF.StartDate =	(select top 1 StartDate from saCourseFees where CourseId=CF.CourseId and StartDate <=GetDate() order by StartDate desc) ")
            .Append("AND EXISTS(select * from saCourseFees CF2 where CF2.CourseId=CF.CourseId and CF2.CourseFeeId=CF.CourseFeeId and CF2.StartDate=(select MAX(startdate) from saCourseFees CF3 where CF2.CourseId=CF3.CourseId and CF2.TransCodeId=CF3.TransCodeId and CF3.StartDate <= GetDate() and	(((CF3.RateScheduleId IS NULL AND SE.TuitionCategoryId IS NULL AND CF3.TuitionCategoryId IS NULL) OR (CF3.RateScheduleId IS NULL AND SE.TuitionCategoryId=CF3.TuitionCategoryId)) OR (CF3.RateScheduleId IS NOT NULL AND EXISTS(SELECT rsd.TuitionCategoryId FROM saRateScheduleDetails rsd WHERE rsd.RateScheduleId=CF3.RateScheduleId AND rsd.TuitionCategoryId=SE.TuitionCategoryId)) OR (CF3.RateScheduleId IS NOT NULL AND SE.TuitionCategoryId IS NULL AND EXISTS(SELECT rsd.* FROM saRateScheduleDetails rsd WHERE rsd.RateScheduleId=CF3.RateScheduleId AND rsd.TuitionCategoryId is null))))) ")
            'commented by Theresa G on oct 15 2010 to remove the cohort start match for the class sections as it is not neccessary
            '.Append("AND	CST.ClsSectionId=CS.ClsSectionId AND CST.TermId in (select TermId from arClassSections where cohortstartdate = ? )")
            .Append("AND	CST.ClsSectionId=CS.ClsSectionId ")
            .Append("AND    CS.CampusID=? ")
            '.Append("AND	BM.BillingMethod*2+BM.AutoBill=2 ")
            .Append(" AND	(((CF.RateScheduleId IS NULL AND SE.TuitionCategoryId IS NULL AND CF.TuitionCategoryId IS NULL) OR (CF.RateScheduleId IS NULL AND SE.TuitionCategoryId=CF.TuitionCategoryId)) OR (CF.RateScheduleId IS NOT NULL AND EXISTS(SELECT rsd.TuitionCategoryId FROM saRateScheduleDetails rsd WHERE rsd.RateScheduleId=CF.RateScheduleId AND rsd.TuitionCategoryId=SE.TuitionCategoryId)) OR(CF.RateScheduleId IS NOT NULL AND SE.TuitionCategoryId IS NULL AND EXISTS(SELECT rsd.* FROM saRateScheduleDetails rsd WHERE rsd.RateScheduleId=CF.RateScheduleId AND rsd.TuitionCategoryId is null))) ")
            .Append(" )res WHERE res.TransAmount <>0 ")
        End With

        ' Add CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'commented by Theresa G on oct 15 2010 to remove the cohort start match for the class sections as it is not neccessary
        ' Add TermId to the parameter list
        ' db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return dataset
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)
        Return ds
    End Function
    Public Function GetFeesToBePostedByProgramVersionDS(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder = New StringBuilder
        With sb
            .Append("select * from (SELECT DISTINCT ")
            .Append("       R.StuEnrollId, PVF.PrgVerFeeId as FeeId,")
            .Append("       PVF.TransCodeId, ")
            .Append("       ((Select TransCodeDescrip from saTransCodes where TransCodeId=PVF.TransCodeId) + '-' + PV.PrgVerDescrip) TransCodeDescrip, ")
            .Append("       (Select TransCodeDescrip from saTransCodes where TransCodeId=PVF.TransCodeId) as FeeDescription, ")
            .Append("       PV.PrgVerDescrip as FeeDetailDescription, ")
            ''The Enrollment Status is also shown
            ''Query modified by Saraswathi
            ' .Append("       (Select StatusCodeDescrip from syStatusCodes where StatusCodeId in (Select StatusCodeID from arStuEnrollments  where StuEnrollId=SE.StuEnrollId)) as EnrollmentStatus, ")

            .Append("       (Select FirstName + ' ' + LastName from arStudent S where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId)) As StudentName, ")
            'include SSN or student identifier
            If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
                .Append("   (Select Coalesce(Case Len(S.SSN) When 9 then '***-**-' + SUBSTRING(SSN,6,4) else ' ' end, ' ') ")
            ElseIf CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToLower = "studentid" Then
                .Append("       (Select Coalesce(StudentNumber, ' ') ")
            Else
                .Append("   (Select Coalesce(" + CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToString + ", ' ') ")
            End If
            .Append("       from arStudent S ")
            .Append("       where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId) ")
            .Append("       ) As StudentIdentifier, ")

            .Append("       PV.PrgVerDescrip + ' ' + (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) TransDescrip, ")
            .Append("       (Case LEN(PVF.RateScheduleId) ")
            .Append("            when 36 then ")
            .Append("               (SELECT ")
            .Append("               Case RSD.FlatAmount ")
            .Append("                   When 0.00 ")
            .Append("                       Then case RSD.UnitId ")
            .Append("                               when 0 then Rate*PV.Credits ")
            .Append("                               when 1 then Rate*PV.Hours ")
            .Append("                            End ")
            .Append("				    Else ")
            .Append("                           FlatAmount ")
            .Append("                   End ")
            .Append("               FROM saRateSchedules RS, saRateScheduleDetails RSD ")
            .Append("               WHERE ")
            .Append("                       RS.RateScheduleId=RSD.RateScheduleId ")
            .Append("               AND	    RS.RateScheduleId=PVF.RateScheduleId ")
            .Append("               AND     (RSD.TuitionCategoryId = SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and SE.TuitionCategoryId is null)) ")
            '.Append("		        AND	    ((RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=PV.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and SE.TuitionCategoryId is null)))) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=PV.Hours and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and SE.TuitionCategoryId is null)))))) ")
            .Append("               AND     ((RSD.FlatAmount=0.00 AND RSD.UnitId=0 and RSD.MaxUnits=(Select Min(MaxUnits) from saRateScheduleDetails Where MaxUnits>=PV.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null))) and RSD.MinUnits<=PV.Credits and RSD.MaxUnits>=PV.Credits) or (RSD.FlatAmount=0.00 AND RSD.UnitId=1 and RSD.MaxUnits=(Select Min(MaxUnits) from saRateScheduleDetails Where MaxUnits>=PV.Hours and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null))) and RSD.MinUnits<=PV.Hours and RSD.MaxUnits>=PV.Hours ) OR (RSD.FlatAmount>0.00 and RSD.MaxUnits=(Select Min(MaxUnits) from saRateScheduleDetails Where MaxUnits>=PV.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null))) and RSD.MinUnits<=PV.Credits and RSD.MaxUnits>=PV.Credits))) ")
            .Append("            else ")
            .Append("               (Case PVF.UnitId ")
            .Append("                   when 0 Then PVF.Amount*PV.Credits ")
            .Append("                   when 1 then PVF.Amount*PV.Hours ")
            .Append("                   when 2 then PVF.Amount End) ")
            .Append("               End )TransAmount, ")
            .Append("       Convert(smalldatetime,getdate()) As TransDate, ")
            .Append("   ( SELECT    COUNT(*)       FROM      saTransactions T1  ")
            .Append(" WHERE T1.StuEnrollId = SE.StuEnrollId    AND T1.TransCodeId = PVF.TransCodeId ")
            .Append(" AND PVF.PrgVerId = SE.PrgVerId ")
            .Append(" AND T1.TermId = T.TermId ")
            .Append("   AND Voided = 0 ")
            .Append(" AND IsAutomatic = 1 ")
            .Append(" AND T1.FeeLevelId = 2 ")
            .Append(" AND T1.CampusId = SE.campusid ")
            .Append(" ) AS NumberOfTransactionsPosted,T.TermID ")


            '.Append("FROM	arResults R, arClassSections CS, arClassSectionTerms CST,  arStuEnrollments SE, arPrgVersions PV, saProgramVersionFees PVF, saBillingMethods BM ")
            .Append("FROM	arResults R, arClassSections CS, arClassSectionTerms CST,  arStuEnrollments SE, arPrgVersions PV, saProgramVersionFees PVF, arTerm T,syStatusCodes SC ")
            .Append("WHERE ")
            .Append("       R.StuEnrollId = SE.StuEnrollId ")
            .Append("AND	R.TestId=CS.ClsSectionId ")
            .Append("AND	SE.PrgVerId=PV.PrgVerId ")
            .Append("AND	PV.PrgVerId=PVF.PrgVerId ")
            ''Query Modified by Saraswathi to bring the Students whose Status is FutureStart or Currently attending
            ' .Append(" and SE.StatusCodeId in(Select StatusCodeId from syStatusCodes where StatusCodeDescrip like 'Futur%Start%' or StatusCodeDescrip like 'Curr%Att%'  or StatusCodeDescrip like 'Acad%Prob%' )")
            'modified by Theresa G on Oct 1 2010
            .Append("AND    SE.StatusCodeId=SC.StatusCodeId ")
            .Append("AND    SC.SysStatusId In (7,9,13,20) ")

            .Append("AND    SE.CampusId = ? ")
            .Append("AND	CST.ClsSectionId=CS.ClsSectionId AND CST.TermId= ? ")
            '.Append("AND	BM.BillingMethod*2+BM.AutoBill=0 ")
            '.Append("AND	(((PVF.RateScheduleId Is Null AND SE.TuitionCategoryId Is NULL AND PVF.TuitionCategoryId Is NULL) OR (PVF.RateScheduleId Is Null AND SE.TuitionCategoryId=PVF.TuitionCategoryId)) OR PVF.RateScheduleId Is Not Null) ")
            .Append("AND	(((PVF.RateScheduleId Is Null AND SE.TuitionCategoryId Is NULL AND PVF.TuitionCategoryId Is NULL) OR (PVF.RateScheduleId Is Null AND SE.TuitionCategoryId=PVF.TuitionCategoryId)) OR (PVF.RateScheduleId IS NOT NULL AND EXISTS(SELECT rsd.TuitionCategoryId FROM saRateScheduleDetails rsd WHERE rsd.RateScheduleId=PVF.RateScheduleId AND rsd.TuitionCategoryId=SE.TuitionCategoryId)) OR (PVF.RateScheduleId IS NOT NULL AND SE.TuitionCategoryId IS NULL AND EXISTS(SELECT rsd.* FROM saRateScheduleDetails rsd WHERE rsd.RateScheduleId=PVF.RateScheduleId AND rsd.TuitionCategoryId is null ))) ")
            .Append("AND    CST.TermId=T.TermId ")
            .Append("AND    SE.StartDate BETWEEN T.StartDate AND T.EndDate ")
            .Append(" )res WHERE res.TransAmount <>0 ")
        End With

        ' Add CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetFeesToBePostedByProgramVersionDSForCohortStart(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder = New StringBuilder
        With sb
            .Append("select * from (SELECT DISTINCT ")
            .Append("       R.StuEnrollId, PVF.PrgVerFeeId as FeeId, ")
            .Append("       PVF.TransCodeId, ")
            .Append("       ((Select TransCodeDescrip from saTransCodes where TransCodeId=PVF.TransCodeId) + '-' + PV.PrgVerDescrip) TransCodeDescrip, ")
            .Append("       (Select TransCodeDescrip from saTransCodes where TransCodeId=PVF.TransCodeId) as FeeDescription, ")
            .Append("       PV.PrgVerDescrip as FeeDetailDescription, ")
            ''The Enrollment Status is also shown
            ''Query modified by Saraswathi
            '.Append("       (Select StatusCodeDescrip from syStatusCodes where StatusCodeId in (Select StatusCodeID from arStuEnrollments  where StuEnrollId=SE.StuEnrollId)) as EnrollmentStatus, ")

            .Append("       (Select FirstName + ' ' + LastName from arStudent S where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId)) As StudentName, ")
            'include SSN or student identifier
            If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
                .Append("   (Select Coalesce(Case Len(S.SSN) When 9 then '***-**-' + SUBSTRING(SSN,6,4) else ' ' end, ' ') ")
            ElseIf CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToLower = "studentid" Then
                .Append("       (Select Coalesce(StudentNumber, ' ') ")
            Else
                .Append("   (Select Coalesce(" + CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToString + ", ' ') ")
            End If
            .Append("       from arStudent S ")
            .Append("       where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId) ")
            .Append("       ) As StudentIdentifier, ")

            .Append("       PV.PrgVerDescrip + ' ' + (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) TransDescrip, ")
            .Append("       (Case LEN(PVF.RateScheduleId) ")
            .Append("            when 36 then ")
            .Append("               (SELECT ")
            .Append("               Case RSD.FlatAmount ")
            .Append("                   When 0.00 ")
            .Append("                       Then case RSD.UnitId ")
            .Append("                               when 0 then Rate*PV.Credits ")
            .Append("                               when 1 then Rate*PV.Hours ")
            .Append("                            End ")
            .Append("				    Else ")
            .Append("                           FlatAmount ")
            .Append("                   End ")
            .Append("               FROM saRateSchedules RS, saRateScheduleDetails RSD ")
            .Append("               WHERE ")
            .Append("                       RS.RateScheduleId=RSD.RateScheduleId ")
            .Append("               AND	    RS.RateScheduleId=PVF.RateScheduleId ")
            .Append("               AND     (RSD.TuitionCategoryId = SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and SE.TuitionCategoryId is null)) ")
            '.Append("		        AND	    ((RSD.UnitId=0 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=PV.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and SE.TuitionCategoryId is null)))) or (RSD.UnitId=1 and RSD.MinUnits=(Select Max(MinUnits) from saRateScheduleDetails Where MinUnits<=PV.Hours and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId or (RSD.TuitionCategoryId is null and SE.TuitionCategoryId is null)))))) ")
            .Append("               AND     ((RSD.FlatAmount=0.00 AND RSD.UnitId=0 and RSD.MaxUnits=(Select Min(MaxUnits) from saRateScheduleDetails Where MaxUnits>=PV.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null))) and RSD.MinUnits <= PV.Credits AND RSD.MaxUnits >= PV.Credits ) or (RSD.FlatAmount=0.00 AND RSD.UnitId=1 and RSD.MaxUnits=(Select Min(MaxUnits) from saRateScheduleDetails Where MaxUnits>=PV.Hours and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null))) and RSD.MinUnits <= PV.Hours and RSD.MaxUnits >= PV.Hours ) OR (RSD.FlatAmount>0.00 and RSD.MaxUnits=(Select Min(MaxUnits) from saRateScheduleDetails Where MaxUnits>=PV.Credits and RateScheduleId=RSD.RateScheduleId and (RSD.TuitionCategoryId=SE.TuitionCategoryId OR (RSD.TuitionCategoryId is null AND SE.TuitionCategoryId is null))) and RSD.MinUnits <= PV.Credits and RSD.MaxUnits >= PV.Credits))) ")
            .Append("            else ")
            .Append("               (Case PVF.UnitId ")
            .Append("                   when 0 Then PVF.Amount*PV.Credits ")
            .Append("                   when 1 then PVF.Amount*PV.Hours ")
            .Append("                   when 2 then PVF.Amount End) ")
            .Append("               End )TransAmount, ")
            .Append("       Convert(smalldatetime,getdate()) As TransDate, ")
            .Append("   ( SELECT    COUNT(*)       FROM      saTransactions T1  ")
            .Append(" WHERE T1.StuEnrollId = SE.StuEnrollId    AND T1.TransCodeId = PVF.TransCodeId ")
            .Append(" AND PVF.PrgVerId = SE.PrgVerId ")
            '.Append(" AND T1.TermId = T.TermId ")
            .Append("   AND Voided = 0 ")
            .Append(" AND IsAutomatic = 1 ")
            .Append(" AND T1.FeeLevelId = 2 ")
            .Append(" AND T1.CampusId = SE.campusid ")
            .Append(" ) AS NumberOfTransactionsPosted, ")
            .Append("(SELECT TOP 1 cs.TermId ")
            .Append(" FROM arStuEnrollments s, arResults rs, arClassSections cs ")
            .Append(" WHERE s.StuEnrollId = SE.StuEnrollId	 and rs.TestId=cs.ClsSectionId		 and s.StuEnrollId=rs.StuEnrollId order by cs.StartDate	) as TERMID ")
            '.Append("FROM	arResults R, arClassSections CS, arClassSectionTerms CST,  arStuEnrollments SE, arPrgVersions PV, saProgramVersionFees PVF, saBillingMethods BM ")
            '.Append("FROM	arResults R, arClassSections CS, arClassSectionTerms CST,  arStuEnrollments SE, arPrgVersions PV, saProgramVersionFees PVF, arTerm T ")
            .Append("FROM	arResults R, arClassSections CS, arClassSectionTerms CST,  arStuEnrollments SE, arPrgVersions PV, saProgramVersionFees PVF, arTerm T ,SyStatusCodes SC ")
            .Append("WHERE ")
            .Append("       R.StuEnrollId = SE.StuEnrollId ")
            .Append("AND	R.TestId=CS.ClsSectionId ")
            .Append("AND	SE.PrgVerId=PV.PrgVerId ")
            .Append("AND	PV.PrgVerId=PVF.PrgVerId ")
            ''Query Modified by Saraswathi to bring the Students whose Status is FutureStart or Currently attending
            '.Append(" and SE.StatusCodeId in(Select StatusCodeId from syStatusCodes where StatusCodeDescrip like 'Futur%Start%' or StatusCodeDescrip like 'Curr%Att%'  or StatusCodeDescrip like 'Acad%Prob%'  )")
            '.Append(" and SE.StatusCodeId in(Select StatusCodeId from syStatusCodes where StatusCodeDescrip like 'Futur%Start%' or StatusCodeDescrip like 'Curr%Att%'  or StatusCodeDescrip like 'Acad%Prob%'  )")
            .Append("AND    SE.StatusCodeId=SC.StatusCodeId ")
            .Append("AND    SC.SysStatusId In (7,9,13,20) ") ' 7=Future Start 9=Currently Attending 13=Re-entry 20=Academic Probation
            .Append("AND    SE.CampusId = ? ")
            ''Query Modified by saraswathi Lakshmanan on jan 30 2009
            ''The student cohortStartDate is also matched
            .Append(" AND	SE.CohortStartDate= ? ")


            .Append("AND	CST.ClsSectionId=CS.ClsSectionId ")
            'commented by Theresa G on oct 15 2010 to remove the cohort start match for the class sections as it is not neccessary
            ' .Append(" AND CST.TermId in (select TermId from arClassSections where cohortstartdate = ? ) ")
            '.Append("AND	BM.BillingMethod*2+BM.AutoBill=0 ")
            .Append("AND	(((PVF.RateScheduleId Is Null AND SE.TuitionCategoryId Is NULL AND PVF.TuitionCategoryId Is NULL) OR (PVF.RateScheduleId Is Null AND SE.TuitionCategoryId=PVF.TuitionCategoryId)) OR (PVF.RateScheduleId IS NOT NULL AND EXISTS(SELECT rsd.TuitionCategoryId FROM saRateScheduleDetails rsd WHERE rsd.RateScheduleId=PVF.RateScheduleId AND rsd.TuitionCategoryId=SE.TuitionCategoryId)) OR (PVF.RateScheduleId IS NOT NULL AND SE.TuitionCategoryId IS NULL AND EXISTS(SELECT rsd.* FROM saRateScheduleDetails rsd WHERE rsd.RateScheduleId=PVF.RateScheduleId AND rsd.TuitionCategoryId is null ))) ")
            .Append("AND    CST.TermId=T.TermId ")
            .Append("AND    CS.CampusId=? ")
            .Append(" )res WHERE res.TransAmount <>0 ")
            ''   .Append("AND    SE.StartDate=T.StartDate ")
        End With

        ' Add CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'commented by Theresa G on oct 15 2010 to remove the cohort start match for the class sections as it is not neccessary
        ' Add TermId to the parameter list
        'db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAwardScheduledDisb(ByVal stuEnrollId As String, ByVal fundSourceId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder = New StringBuilder
        With sb
            '.Append("SELECT ")
            '.Append("       AwardScheduleId AS ScheduleId,")
            '.Append("       (SELECT FundSourceDescrip FROM saFundSources WHERE FundSourceId=SA.AwardTypeId) AS ScheduleDescrip, ")
            '.Append("       (SELECT AcademicYearDescrip FROM saAcademicYears WHERE AcademicYearId=SA.AcademicYearId) AS AcademicYearDescrip,")
            '.Append("       SA.AwardStartDate AS StartDate, SA.AwardEndDate AS EndDate, ExpectedDate, Amount, 1 AS IsAwardDisb, 0 as IsInDB, SAS.ModDate ")
            '.Append("FROM   faStudentAwards SA, faStudentAwardSchedule SAS ")
            '.Append("WHERE  SA.StuEnrollId=? AND SA.AwardTypeId=? ")
            '.Append("       AND SA.StudentAwardId=SAS.StudentAwardId ")
            '.Append("       AND NOT EXISTS (SELECT * FROM saPmtDisbRel WHERE AwardScheduleId=SAS.AwardScheduleId) ")
            '.Append("ORDER BY ScheduleDescrip,AcademicYearDescrip, StartDate, EndDate, ExpectedDate ")
            .Append("select ")
            .Append("		ScheduleId, ")
            .Append("		Scheduledescrip, ")
            .Append("		AcademicYearDescrip, ")
            .Append("		StartDate, ")
            .Append("		EndDate, ")
            .Append("		ExpectedDate, ")
            .Append("		Amount, ")
            .Append("		Balance, ")
            .Append("		IsAwardDisb, ")
            .Append("		IsInDB, ")
            .Append("		ModDate ")
            .Append("from ")
            .Append("( ")
            .Append("		SELECT  ")
            .Append("				AwardScheduleId AS ScheduleId, ")
            .Append("				(SELECT FundSourceDescrip FROM saFundSources WHERE FundSourceId=SA.AwardTypeId) AS ScheduleDescrip,  ")
            .Append("				(SELECT AcademicYearDescrip FROM saAcademicYears WHERE AcademicYearId=SA.AcademicYearId) AS AcademicYearDescrip, ")
            .Append("				SA.AwardStartDate AS StartDate,  ")
            .Append("				SA.AwardEndDate AS EndDate,  ")
            .Append("				ExpectedDate,  ")
            .Append("				Amount,  ")
            'this line will fail with SQL 2000 with no service pack
            '.Append("				Amount - Coalesce((Select Sum(Amount) from saPmtDisbRel PDR where AwardScheduleId=SAS.AwardScheduleId group by PDR.AwardScheduleId),0) as Balance, ")

            '''' Code commented and added by kamalesh ahuja on 02 April 2010 to resolve mantis issue id 18771 
            '.Append("		        Amount - Coalesce((Select Sum(Amount) from saPmtDisbRel PDR, saTransactions T where AwardScheduleId=SAS.AwardScheduleId and PDR.TransactionId=T.TransactionId and T.Voided=0 group by PDR.AwardScheduleId),0) as Balance, ")
            .Append("		        Amount - Coalesce((Select Sum(Amount) from saPmtDisbRel PDR, saTransactions T where AwardScheduleId=SAS.AwardScheduleId and PDR.TransactionId=T.TransactionId and T.Voided=0 group by PDR.AwardScheduleId),0) ")
            .Append("		        + Coalesce((Select Sum(TransAmount) from saTransactions T,saRefunds R where R.AwardScheduleId=SAS.AwardScheduleId and R.TransactionId=T.TransactionId and T.Voided=0 group by R.AwardScheduleId),0) as Balance, ")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            'this line won't fail in any circumstance
            '.Append("               Amount - (Select Sum(Amount) from saPmtDisbRel PDR where AwardScheduleId=SAS.AwardScheduleId group by PDR.AwardScheduleId) as Balance, ")
            .Append("				1 AS IsAwardDisb,  ")
            .Append("				0 as IsInDB,  ")
            .Append("				SAS.ModDate  ")
            .Append("		 FROM   faStudentAwards SA, faStudentAwardSchedule SAS  ")
            .Append("		 WHERE   ")
            .Append("				SA.StuEnrollId= ? ")
            If fundSourceId <> Guid.Empty.ToString() Then
                .Append("        AND    SA.AwardTypeId= ? ")
            End If
            .Append("		 AND	SA.StudentAwardId=SAS.StudentAwardId  ")
            .Append(") T ")
            .Append("where ")
            .Append("		Balance > 0 ")
            .Append("order by ScheduleDescrip, AcademicYearDescrip, StartDate, EndDate, ExpectedDate  ")
        End With

        ' Add parameters
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        If fundSourceId <> Guid.Empty.ToString() Then
            db.AddParameter("@AwardTypeId", fundSourceId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        '   Return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function

    ''Added by Saraswathi lakshmanan on April 15 2010
    ''Converted to storedProcedure

    ' New Code Added By Vijay Ramteke On June 10, 2010 For Mantis Id 18950
    Public Function GetAwardScheduledDisb_Sp(ByVal stuEnrollId As String, ByVal fundSourceId As String, ByVal CutoffDate As String) As DataSet

        '   connect to the database
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        ' Add parameters
        db.AddParameter("@StuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        If fundSourceId <> Guid.Empty.ToString() Then
            db.AddParameter("@AwardTypeId", New Guid(fundSourceId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@AwardTypeId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If
        'New Variable Added On June 09, 2010 For Mantis Id 18950
        db.AddParameter("@CutoffDate", CutoffDate, SqlDbType.DateTime, , ParameterDirection.Input)
        'New Variable Added On June 09, 2010 For Mantis Id 18950

        '' New Code Added By Vijay Ramteke On September 25, 2010 For Mantis Id 19767
        Dim usp_Name As String
        If (HttpContext.Current.Session("UserName").ToString.ToUpper = "SA" Or HttpContext.Current.Session("UserName").ToString.ToUpper = "SUPER" Or HttpContext.Current.Session("UserName").ToString.ToUpper = "SUPPORT") Then
            usp_Name = "USP_SA_GetAwardScheduledDisb_Admin"
        Else
            usp_Name = "USP_SA_GetAwardScheduledDisb"
        End If
        '   Return dataset
        Return db.RunParamSQLDataSet_SP(usp_Name)
        ''Return db.RunParamSQLDataSet_SP("USP_SA_GetAwardScheduledDisb")
        '' New Code Added By Vijay Ramteke On September 25, 2010 For Mantis Id 19767

    End Function


    Public Function GetPmtPlanScheduledDisb(ByVal stuEnrollId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder = New StringBuilder
        With sb
            '.Append("SELECT ")
            '.Append("       PayPlanScheduleId AS ScheduleId, PayPlanDescrip AS ScheduleDescrip, ")
            '.Append("       (SELECT AcademicYearDescrip FROM saAcademicYears WHERE AcademicYearId=SPP.AcademicYearId) AS AcademicYearDescrip,")
            '.Append("       SPP.PayPlanStartDate AS StartDate, SPP.PayPlanEndDate AS EndDate, ExpectedDate, Amount, 0 AS IsAwardDisb, 0 as IsInDB, PPS.ModDate ")
            '.Append("FROM   faStudentPaymentPlans SPP, faStuPaymentPlanSchedule PPS ")
            '.Append("WHERE  SPP.StuEnrollId=? ")
            '.Append("       AND SPP.PaymentPlanId = PPS.PaymentPlanId ")
            '.Append("       AND NOT EXISTS(SELECT * FROM saPmtDisbRel WHERE PayPlanScheduleId=PPS.PayPlanScheduleId) ")
            '.Append("ORDER BY ScheduleDescrip,AcademicYearDescrip, StartDate, EndDate, ExpectedDate ")
            .Append("select  ")
            .Append(" 		ScheduleId,  ")
            .Append(" 		ScheduleDescrip,  ")
            .Append(" 		AcademicYearDescrip,  ")
            .Append(" 		StartDate,  ")
            .Append(" 		EndDate,  ")
            .Append(" 		ExpectedDate,  ")
            .Append(" 		Amount,  ")
            .Append(" 		Balance,  ")
            .Append(" 		IsAwardDisb,  ")
            .Append(" 		IsInDB,  ")
            .Append(" 		ModDate  ")
            .Append(" from  ")
            .Append(" (  ")
            .Append(" 		SELECT   ")
            .Append(" 				PayPlanScheduleId AS ScheduleId,  ")
            .Append("				PayPlanDescrip As ScheduleDescrip, ")
            .Append("				(SELECT AcademicYearDescrip FROM saAcademicYears WHERE AcademicYearId=SPP.AcademicYearId) AS AcademicYearDescrip, ")
            .Append("				SPP.PayPlanStartDate AS StartDate,  ")
            .Append("				SPP.PayPlanEndDate AS EndDate,  ")
            .Append(" 				ExpectedDate,   ")
            .Append(" 				Amount,   ")
            '.Append(" 				Amount - Coalesce((Select Sum(Amount) from saPmtDisbRel PDR where PayPlanScheduleId=PPS.PayPlanScheduleId group by PDR.PayPlanScheduleId),0) as Balance,  ")
            .Append("		        Amount - Coalesce((Select Sum(Amount) from saPmtDisbRel PDR, saTransactions T where PayPlanScheduleId=PPS.PayPlanScheduleId and PDR.TransactionId=T.TransactionId and T.Voided=0 group by PDR.PayPlanScheduleId),0) as Balance, ")
            .Append(" 				0 AS IsAwardDisb,   ")
            .Append(" 				0 as IsInDB,   ")
            .Append(" 				PPS.ModDate   ")
            .Append(" 		 FROM   faStudentPaymentPlans SPP, faStuPaymentPlanSchedule PPS   ")
            .Append(" 		 WHERE    ")
            .Append(" 				SPP.StuEnrollId = ? ")
            .Append(" 		 AND	SPP.PaymentPlanId=PPS.PaymentPlanId   ")
            .Append(" ) T  ")
            .Append(" where  ")
            .Append(" 		Balance > 0  ")
            .Append(" order by ScheduleDescrip, AcademicYearDescrip, StartDate, EndDate, ExpectedDate   ")
        End With

        ' Add parameter
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    ''Added by Saraswathi lakshmanan on April 15 2010
    ''Converted to stored procedure
    Public Function GetPmtPlanScheduledDisb_sp(ByVal stuEnrollId As String) As DataSet

        '   connect to the database
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        ' Add parameter
        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   Return dataset
        Return db.RunParamSQLDataSet_SP("USP_SA_GetPmtPlanScheduledDisb")

    End Function

    Public Function GetPaymentDisbursements(ByVal transactionId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       1 AS DisbursementType,SA.AwardTypeId AS FundSourceId,PD.AwardScheduleId AS ScheduleId,")
            .Append("       (SELECT FundSourceDescrip FROM saFundSources WHERE FundSourceId=SA.AwardTypeId) AS ScheduleDescrip,")
            .Append("       (SELECT AcademicYearDescrip FROM saAcademicYears WHERE AcademicYearId=SA.AcademicYearId) AS AcademicYearDescrip,")
            .Append("       SA.AwardStartDate AS StartDate, SA.AwardEndDate AS EndDate, ExpectedDate, SAS.Amount, 1 AS IsAwardDisb, 1 as IsInDB, PD.ModDate ")
            .Append("FROM   saPmtDisbRel PD, faStudentAwardSchedule SAS, faStudentAwards SA ")
            .Append("WHERE  PD.TransactionId=? AND PD.AwardScheduleId=SAS.AwardScheduleId ")
            .Append("       AND SA.StudentAwardId=SAS.StudentAwardId ")
            .Append("UNION ALL ")
            .Append("SELECT ")
            .Append("       2 AS DisbursementType,NULL AS FundSourceId,PD.PayPlanScheduleId AS ScheduleId, PayPlanDescrip AS ScheduleDescrip,")
            .Append("       (SELECT AcademicYearDescrip FROM saAcademicYears WHERE AcademicYearId=SPP.AcademicYearId) AS AcademicYearDescrip,")
            .Append("       SPP.PayPlanStartDate AS StartDate, SPP.PayPlanEndDate AS EndDate,ExpectedDate,PPS.Amount, 0 AS IsAwardDisb, 1 as IsInDB, PD.ModDate ")
            .Append("FROM   saPmtDisbRel PD, faStudentPaymentPlans SPP, faStuPaymentPlanSchedule PPS ")
            .Append("WHERE  PD.TransactionId=? AND PD.PayPlanScheduleId=PPS.PayPlanScheduleId ")
            .Append("       AND PPS.PaymentPlanId=SPP.PaymentPlanId ")
            .Append("ORDER BY ScheduleDescrip,AcademicYearDescrip, StartDate, EndDate, ExpectedDate ")
        End With

        ' Add parameters
        db.AddParameter("@TransactionId", transactionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@TransactionId", transactionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function

    Public Function GetScheduledDisbInfo(ByVal pmtDisbRelId As String) As ScheduledDisbInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("    PD.PmtDisbRelId ")
            .Append("    PD.TransactionId, ")
            .Append("    PD.AwardScheduleId, ")
            .Append("    PD.PayPlanScheduleId, ")
            .Append("    PD.ModUser, ")
            .Append("    PD.ModDate ")
            .Append("FROM  saPmtDisbRel PD ")
            .Append("WHERE PD.pmtDisbRelId = ? ")
        End With

        ' Add the pmtDisbRelId to the parameter list
        db.AddParameter("@TransactionId", pmtDisbRelId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim ScheduledDisbInfo As New ScheduledDisbInfo

        While dr.Read()

            '   set properties with data from DataReader
            With ScheduledDisbInfo
                .IsInDB = True
                .PmtDisbRelId = pmtDisbRelId
                .TransactionId = CType(dr("TransactionId"), Guid).ToString
                If Not (dr("AwardScheduleId") Is DBNull.Value) Then .AwardScheduleId = CType(dr("AwardScheduleId"), Guid).ToString
                If Not (dr("PayPlanScheduleId") Is DBNull.Value) Then .PayPlanScheduleId = CType(dr("PayPlanScheduleId"), Guid).ToString
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return ScheduledDisbInfo

    End Function
    Public Function UpdateScheduledDisbInfo(ByVal ScheduledDisbInfo As ScheduledDisbInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saPmtDisbRel Set PmtDisbRelId = ?, TransactionId = ?, ")
                .Append("   DisbScheduleId = ?, ModUser = ?, ModDate = ? ")
                .Append("WHERE PmtDisbRelId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saPmtDisbRel where ModDate = ? ")
            End With

            '   add parameters values to the query
            '   PmtDisbRelId
            db.AddParameter("@PmtDisbRelId", ScheduledDisbInfo.PmtDisbRelId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TransactionId
            db.AddParameter("@TransactionId", ScheduledDisbInfo.TransactionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   AwardScheduleId
            If ScheduledDisbInfo.AwardScheduleId = Guid.Empty.ToString Then
                db.AddParameter("@AwardScheduleId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@AwardScheduleId", ScheduledDisbInfo.AwardScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   PayPlanScheduleId
            If ScheduledDisbInfo.PayPlanScheduleId = Guid.Empty.ToString Then
                db.AddParameter("@PayPlanScheduleId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PayPlanScheduleId", ScheduledDisbInfo.PayPlanScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   pmtDisbRelId
            db.AddParameter("@pmtDisbRelId", ScheduledDisbInfo.PmtDisbRelId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", ScheduledDisbInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                '   return an error message
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException

            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddScheduledDisbInfo(ByVal ScheduledDisbInfo As ScheduledDisbInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saPmtDisbRel (PmtDisbRelId, TransactionId, ")
                .Append("   DisbScheduleId, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   pmtDisbRelId
            db.AddParameter("@PmtDisbRelId", ScheduledDisbInfo.PmtDisbRelId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   TransactionId
            db.AddParameter("@TransactionId", ScheduledDisbInfo.TransactionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   AwardScheduleId
            If ScheduledDisbInfo.AwardScheduleId = Guid.Empty.ToString Then
                db.AddParameter("@AwardScheduleId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@AwardScheduleId", ScheduledDisbInfo.AwardScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   PayPlanScheduleId
            If ScheduledDisbInfo.PayPlanScheduleId = Guid.Empty.ToString Then
                db.AddParameter("@PayPlanScheduleId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PayPlanScheduleId", ScheduledDisbInfo.PayPlanScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   create timestamp
            Dim rightNow As DateTime = Date.Now

            ''   CreateDate
            'db.AddParameter("@CreateDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException

            '   return an error to the client
            'Return DALExceptions.BuildErrorMessage(ex)
            Return ex.Message

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteScheduledDisbInfo(ByVal pmtDisbRelId As String, ByVal modDate As DateTime, ByVal modDate1 As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saPmtDisbRel ")
                .Append("WHERE PmtDisbRelId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("Select count(*) from saPmtDisbRel where PmtDisbRelId = ? ")
            End With

            '   add parameters values to the query

            '   PmtDisbRelId
            db.AddParameter("@PmtDisbRelId", pmtDisbRelId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TransactionId
            db.AddParameter("@PmtDisbRelId", pmtDisbRelId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                '   return an error message
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException

            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetProjectedAmountByEnrollment(ByVal stuEnrollId As String) As Decimal
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New System.Text.StringBuilder
        With sb
            ''Query Modified by Saraswathi to fix issue 14483
            ''The partial payment is not added to the payment Plan

            .Append(" Select (SELECT     COALESCE(SUM(Amount), 0)  FROM   ")
            .Append(" faStudentPaymentPlans SPP, faStuPaymentPlanSchedule PPS WHERE  ")
            .Append("  SPP.StuEnrollId = ?         ")
            .Append(" AND SPP.PaymentPlanId = PPS.PaymentPlanId          ")
            .Append(" AND")
            .Append(" NOT EXISTS (SELECT * FROM saPmtDisbRel WHERE ")
            .Append(" PayPlanScheduleId = PPS.PayPlanScheduleId")
            .Append(" and PPS.Amount<=(Select Sum(Amount) from saPmtDisbRel WHERE ")
            .Append(" PayPlanScheduleId=PPS.PayPlanScheduleId  Group by PayPlanScheduleId ) )) -")
            .Append(" (Select COALESCE(SUM(CASE WHEN SP.Amount < 0 THEN SP.Amount * -1
                                       ELSE SP.Amount
                                  END), 0) from saPmtDisbRel SP,faStudentPaymentPlans SPP, faStuPaymentPlanSchedule PPS, saTransactions TR")
            .Append(" where SP.PayPlanScheduleId=PPS.PayPlanScheduleId  and ")
            .Append(" SPP.StuEnrollId = ?         ")
            .Append(" AND SPP.PaymentPlanId = PPS.PaymentPlanId AND SP.TransactionId=TR.TransactionId AND TR.Voided=0  and ")
            .Append(" PPS.Amount  >(Select Sum(Amount) from saPmtDisbRel WHERE ")
            .Append(" PayPlanScheduleId=PPS.PayPlanScheduleId  Group by PayPlanScheduleId)) AS ProjectedAmount")
            .Append(" UNION ALL ")
            .Append(" Select(SELECT    ")
            .Append(" COALESCE(SUM(Amount), 0) AS ProjectedAmount FROM      ")
            .Append(" faStudentAwards SA, faStudentAwardSchedule SAS WHERE   ")
            .Append(" SA.StuEnrollId = ?         ")
            .Append(" AND SA.StudentAwardId=SAS.StudentAwardId   ")
            .Append(" and    ")
            .Append(" NOT EXISTS (SELECT * FROM saPmtDisbRel WHERE ")
            .Append(" AwardScheduleId = SAS.AwardScheduleId")
            .Append(" and SAS.Amount<=(Select Sum(Amount) from saPmtDisbRel WHERE ")
            .Append(" AwardScheduleId=SAS.AwardScheduleId  Group by AwardScheduleId ) )) -")
            .Append(" (Select COALESCE(SUM(SP.Amount), 0) from saPmtDisbRel SP,faStudentAwards SA, faStudentAwardSchedule SAS, saTransactions TR")
            .Append(" where SP.AwardScheduleId=SAS.AwardScheduleId and ")
            .Append(" SA.StuEnrollId = ?         ")
            .Append(" AND SA.StudentAwardId=SAS.StudentAwardId AND SP.TransactionId=TR.TransactionId AND TR.Voided=0 and ")
            .Append(" SAS.Amount  >(Select Sum(Amount) from saPmtDisbRel WHERE ")
            .Append(" AwardScheduleId=SAS.AwardScheduleId  Group by AwardScheduleId)) AS ProjectedAmount")



            ' ''.Append("SELECT     COALESCE(SUM(Amount), 0) AS ProjectedAmount ")
            ' ''.Append("FROM       faStudentPaymentPlans SPP, faStuPaymentPlanSchedule PPS ")
            ' ''.Append("WHERE      SPP.StuEnrollId = ? ")
            ' ''.Append("           AND SPP.PaymentPlanId = PPS.PaymentPlanId ")
            ' ''.Append("           AND NOT EXISTS (SELECT * FROM saPmtDisbRel WHERE PayPlanScheduleId=PPS.PayPlanScheduleId) ")
            ' ''.Append(" UNION ALL ")
            ' ''.Append("SELECT     COALESCE(SUM(Amount), 0) AS ProjectedAmount ")
            ' ''.Append("FROM       faStudentAwards SA, faStudentAwardSchedule SAS ")
            ' ''.Append("WHERE      SA.StuEnrollId = ? ")
            ' ''.Append("           AND SA.StudentAwardId=SAS.StudentAwardId ")
            ' ''.Append("           AND NOT EXISTS (SELECT * FROM saPmtDisbRel WHERE AwardScheduleId=SAS.AwardScheduleId) ")
            '' ''.Append(" UNION ALL ")
            '' ''.Append("SELECT     COALESCE(SUM(T.TransAmount), 0) AS ProjectedAmount ")
            '' ''.Append("FROM       saTransactions T ")
            '' ''.Append("WHERE      T.stuEnrollId = ? ")
            '' ''.Append("           AND EXISTS (SELECT * FROM saPmtDisbRel WHERE TransactionId=T.TransactionId) ")



            '' ''.Append("SELECT ")
            '' ''.Append("       (coalesce(sum(SAS.Amount),0) + coalesce(sum(SPS.Amount),0) + coalesce((select sum(TransAmount) from saPmtDisbRel PDR, saTransactions T WHERE T.TransactionId=PDR.TransactionId AND T.StuEnrollId=?),0)) As Projected ")
            '' ''.Append("FROM ")
            '' ''.Append("       faStudentAwards SA, faStudentAwardSchedule SAS, faStudentPaymentPlans SPP, faStuPaymentPlanSchedule SPS ")
            '' ''.Append("WHERE ")
            '' ''.Append("       SA.StudentAwardId = SAS.StudentAwardId ")
            '' ''.Append("AND	SPP.PaymentPlanId = SPS.PaymentPlanId ")
            '' ''.Append("AND 	SA.StuEnrollId = ? ")
            '' ''.Append("AND 	SPP.StuEnrollId = ? ")
        End With

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        '   Execute the query
        Dim resultObj As Object
        'resultObj = db.RunParamSQLScalar(sb.ToString)
        Dim ds As DataSet
        ds = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                resultObj = ds.Tables(0).Compute("SUM(ProjectedAmount)", "")
            End If
        End If

        'Return projected value
        If resultObj Is System.DBNull.Value Then
            Return 0
        Else
            Return CType(resultObj, Decimal)
        End If

    End Function
    Public Function GetProjectedAmountForStudent(ByVal studentId As String) As Decimal
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New System.Text.StringBuilder
        With sb
            ''Query Modified by Saraswathi Lakshmanan on OCt 24-2008
            ''The partial payment is added to the projected plan
            .Append(" Select (SELECT     COALESCE(SUM(Amount), 0)  FROM   ")
            .Append(" faStudentPaymentPlans SPP, faStuPaymentPlanSchedule PPS WHERE  ")
            .Append("  SPP.StuEnrollId IN (SELECT StuEnrollId FROM arStuEnrollments WHERE StudentId = ? )          ")
            .Append(" AND SPP.PaymentPlanId = PPS.PaymentPlanId          ")
            .Append(" AND")
            .Append(" NOT EXISTS (SELECT * FROM saPmtDisbRel WHERE ")
            .Append(" PayPlanScheduleId = PPS.PayPlanScheduleId")
            .Append(" and PPS.Amount<=(Select Sum(Amount) from saPmtDisbRel WHERE ")
            .Append(" PayPlanScheduleId=PPS.PayPlanScheduleId  Group by PayPlanScheduleId ) )) -")
            .Append(" (Select COALESCE(SUM(SP.Amount), 0) from saPmtDisbRel SP,faStudentPaymentPlans SPP, faStuPaymentPlanSchedule PPS, saTransactions TR")
            .Append(" where SP.PayPlanScheduleId=PPS.PayPlanScheduleId  and ")
            .Append(" SPP.StuEnrollId IN (SELECT StuEnrollId FROM arStuEnrollments WHERE StudentId = ? )         ")
            .Append(" AND SPP.PaymentPlanId = PPS.PaymentPlanId AND SP.TransactionId=TR.TransactionId AND TR.Voided=0  and ")
            .Append(" PPS.Amount  >(Select Sum(Amount) from saPmtDisbRel WHERE ")
            .Append(" PayPlanScheduleId=PPS.PayPlanScheduleId  Group by PayPlanScheduleId)) AS ProjectedAmount")
            .Append(" UNION  ALL ")
            .Append(" Select(SELECT    ")
            .Append(" COALESCE(SUM(Amount), 0) AS ProjectedAmount FROM      ")
            .Append(" faStudentAwards SA, faStudentAwardSchedule SAS WHERE   ")
            .Append(" SA.StuEnrollId IN (SELECT StuEnrollId FROM arStuEnrollments WHERE StudentId = ? )        ")
            .Append(" AND SA.StudentAwardId=SAS.StudentAwardId   ")
            .Append(" and    ")
            .Append(" NOT EXISTS (SELECT * FROM saPmtDisbRel WHERE ")
            .Append(" AwardScheduleId = SAS.AwardScheduleId")
            .Append(" and SAS.Amount<=(Select Sum(Amount) from saPmtDisbRel WHERE ")
            .Append(" AwardScheduleId=SAS.AwardScheduleId  Group by AwardScheduleId ) )) -")
            .Append(" (Select COALESCE(SUM(SP.Amount), 0) from saPmtDisbRel SP,faStudentAwards SA, faStudentAwardSchedule SAS, saTransactions TR")
            .Append(" where SP.AwardScheduleId=SAS.AwardScheduleId  and ")
            .Append(" SA.StuEnrollId IN (SELECT StuEnrollId FROM arStuEnrollments WHERE StudentId = ? )               ")
            .Append(" AND SA.StudentAwardId=SAS.StudentAwardId AND SP.TransactionId=TR.TransactionId AND TR.Voided=0   and ")
            .Append(" SAS.Amount  >(Select Sum(Amount) from saPmtDisbRel WHERE ")
            .Append(" AwardScheduleId=SAS.AwardScheduleId  Group by AwardScheduleId)) AS ProjectedAmount")


            '.Append("SELECT     COALESCE(SUM(Amount), 0) AS ProjectedAmount ")
            '.Append("FROM       faStudentPaymentPlans SPP, faStuPaymentPlanSchedule PPS ")
            '.Append("WHERE      SPP.StuEnrollId IN (SELECT StuEnrollId FROM arStuEnrollments WHERE StudentId = ? ) ")
            '.Append("           AND SPP.PaymentPlanId = PPS.PaymentPlanId ")
            '.Append("           AND NOT EXISTS (SELECT * FROM saPmtDisbRel WHERE PayPlanScheduleId=PPS.PayPlanScheduleId) ")
            '.Append(" UNION ALL ")
            '.Append("SELECT     COALESCE(SUM(Amount), 0) AS ProjectedAmount ")
            '.Append("FROM       faStudentAwards SA, faStudentAwardSchedule SAS ")
            '.Append("WHERE      SA.StuEnrollId IN (SELECT StuEnrollId FROM arStuEnrollments WHERE StudentId = ? ) ")
            '.Append("           AND SA.StudentAwardId=SAS.StudentAwardId ")
            '.Append("           AND NOT EXISTS (SELECT * FROM saPmtDisbRel WHERE AwardScheduleId=SAS.AwardScheduleId) ")
        End With

        ' Add StuId to the parameter list
        db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim resultObj As Object
        Dim ds As DataSet
        ds = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                resultObj = ds.Tables(0).Compute("SUM(ProjectedAmount)", "")
            End If
        End If

        'Return projected value
        If resultObj Is System.DBNull.Value Then
            Return 0
        Else
            Return CType(resultObj, Decimal)
        End If

    End Function
    Public Function HasDisbursements(ByVal stuEnrollId As String) As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '.Append("SELECT (SELECT SUM(Amount)")
            '.Append("        FROM   faStudentPaymentPlans SPP, faStuPaymentPlanSchedule PPS ")
            '.Append("        WHERE  SPP.StuEnrollId = SE.StuEnrollId ")
            '.Append("               AND SPP.PaymentPlanId = PPS.PaymentPlanId ")
            '.Append("               AND NOT EXISTS(SELECT * FROM saPmtDisbRel WHERE PayPlanScheduleId=PPS.PayPlanScheduleId)) AS PPAmount, ")
            '.Append("       (SELECT SUM(Amount)")
            '.Append("        FROM   faStudentAwards SA, faStudentAwardSchedule SAS ")
            '.Append("        WHERE  SA.StuEnrollId = SE.StuEnrollId ")
            '.Append("               AND SA.StudentAwardId=SAS.StudentAwardId ")
            '.Append("               AND NOT EXISTS (SELECT * FROM saPmtDisbRel WHERE AwardScheduleId=SAS.AwardScheduleId)) AS AwardAmount ")
            '.Append("FROM   arStuEnrollments SE ")
            '.Append("WHERE  SE.StuEnrollId=? ")
            .Append("select ")
            .Append("( ")
            .Append("SELECT count(*)  ")
            .Append("FROM ")
            .Append("	( ")
            .Append("	select   ")
            'Code added by Vijay Ramteke on April 15, 2010 Mantis Id 18827
            '.Append("			Amount - Coalesce((Select Sum(Amount) from saPmtDisbrel where AwardScheduleId=SAS.AwardScheduleId),0) as Balance ")
            .Append("		        Amount - Coalesce((Select Sum(Amount) from saPmtDisbRel PDR, saTransactions T where AwardScheduleId=SAS.AwardScheduleId and PDR.TransactionId=T.TransactionId and T.Voided=0 group by PDR.AwardScheduleId),0) ")
            .Append("		        + Coalesce((Select Sum(TransAmount) from saTransactions T,saRefunds R where R.AwardScheduleId=SAS.AwardScheduleId and R.TransactionId=T.TransactionId and T.Voided=0 group by R.AwardScheduleId),0) as Balance ")
            'Code added by Vijay Ramteke on April 15, 2010 Mantis Id 18827
            .Append("	from	faStudentAwards SA, faStudentAwardSchedule SAS ")
            .Append("	where ")
            .Append("			SA.StudentAwardId=SAS.StudentAwardId ")
            .Append("	AND		SA.StuEnrollId = ? ")
            .Append("	) T where Balance <> 0.00 ")
            .Append(") As AwardAmount, ")
            .Append("( ")
            .Append("SELECT count(*)  ")
            .Append("FROM ")
            .Append("	( ")
            .Append("	select   ")
            'Code added by Vijay Ramteke on April 15, 2010 Mantis Id 18827
            '.Append("			Amount - Coalesce((Select Sum(Amount) from saPmtDisbrel where PayPlanScheduleId=PPS.PayPlanScheduleId),0) as Balance ")
            .Append("		        Amount - Coalesce((Select Sum(Amount) from saPmtDisbRel PDR, saTransactions T where PayPlanScheduleId=PPS.PayPlanScheduleId and PDR.TransactionId=T.TransactionId and T.Voided=0 group by PDR.PayPlanScheduleId),0) as Balance ")
            'Code added by Vijay Ramteke on April 15, 2010 Mantis Id 18827
            .Append("	from	faStudentPaymentPlans SPP, faStuPaymentPlanSchedule PPS ")
            .Append("	where ")
            .Append("			SPP.PaymentPlanId=PPS.PaymentPlanId ")
            .Append("	AND		SPP.StuEnrollId = ? ")
            .Append("	) T where Balance <> 0.00 ")
            .Append(") As PPAmount ")

        End With

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                If dr.IsNull("PPAmount") Then dr("PPAmount") = 0
                If dr.IsNull("AwardAmount") Then dr("AwardAmount") = 0
            End If
        End If

        Return ds.Tables(0)
    End Function
    ''Added by Saraswathi lakshmanan on April 13 2010
    ''Converted to storedProcedure
    Public Function HasDisbursements_sp(ByVal stuEnrollId As String) As DataTable
        Dim db As New SQLDataAccess
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder


        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        '   Execute the query
        ds = db.RunParamSQLDataSet_SP("USP_SA_HasDisbursements")

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                If dr.IsNull("PPAmount") Then dr("PPAmount") = 0
                If dr.IsNull("AwardAmount") Then dr("AwardAmount") = 0
            End If
        End If

        Return ds.Tables(0)
    End Function


    Private Function GetTermIdForStuEnrollId(ByVal stuEnrollId As String) As String
        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       T.TermId, ")
            .Append("       SE.ExpStartDate ")
            .Append("FROM 	arStuEnrollments SE, arTerm T ")
            .Append("WHERE ")
            .Append("       SE.ExpStartDate=T.StartDate ")
            .Append("AND	SE.StuEnrollId = ? ")
            .Append("UNION ")
            .Append("SELECT ")
            .Append("       T.TermId, ")
            .Append("       T.StartDate ")
            .Append("FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arTerm T ")
            .Append("WHERE ")
            .Append("       R.TestId=CS.ClsSectionId ")
            .Append("AND	CST.ClsSectionId=CS.ClsSectionId AND CST.TermId=T.TermId ")
            .Append("AND	R.StuEnrollId = ? ")
            .Append("ORDER BY ")
            .Append("       SE.ExpStartDate DESC ")
        End With

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim obj As Object = db.RunParamSQLScalar(sb.ToString)

        If obj Is DBNull.Value Then
            Return Guid.Empty.ToString
        Else
            Return CType(obj, Guid).ToString
        End If

    End Function
    Public Function VoidTransaction(ByVal transactionId As String, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saTransactions Set Voided=1, ")
                .Append("   ModUser = ?, ModDate = ? ")
                .Append("WHERE TransactionId = ? ")

            End With

            '   add parameters values to the query
            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   TransactionId
            db.AddParameter("@TransactionId", transactionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            db.ClearParameters()
            sb = New StringBuilder

            'We also need to remove any records from the saPmtDisbRel table with this transaction id
            With sb
                .Append("DELETE FROM saPmtDisbRel WHERE TransactionId = ? ")
            End With

            db.AddParameter("@TransactionId", transactionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   commit transaction 
            groupTrans.Commit()
            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction
            groupTrans.Rollback()
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Function ApplyLateFees(ByVal lateFees As String(), ByVal transCodeId As String, ByVal postFeesDate As Date, ByVal campusId As String, ByVal user As String) As String

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder = New StringBuilder

        With sb
            .Append("SELECT  ")
            .Append("       PPS.PayPlanScheduleId, ")
            .Append("		PP.StuEnrollId, ")
            .Append("		PP.AcademicYearId, ")
            .Append("		PP.PayPlanDescrip + ' Late Fees' As TransDescrip ")
            .Append("FROM	faStuPaymentPlanSchedule PPS, faStudentPaymentPlans PP ")
            .Append("WHERE  ")
            .Append("		PPS.PaymentPlanId=PP.PaymentPlanId ")
            .Append("AND	PayPlanScheduleId in (" + GetListOfPayPlanScheduleIds(lateFees) + ") ")
        End With

        '   Execute the query
        Dim dt As DataTable = db.RunSQLDataSet(sb.ToString).Tables(0)

        dt.Columns.Add("TransactionId", GetType(Guid))
        dt.Columns.Add("CampusId", GetType(Guid))
        dt.Columns.Add("TransAmount", GetType(Decimal))
        dt.Columns.Add("TransDate", GetType(Date))
        dt.Columns.Add("TransCodeId", GetType(Guid))
        dt.Columns.Add("TransReference", GetType(String))
        dt.Columns.Add("TransTypeId", GetType(Integer))
        dt.Columns.Add("IsPosted", GetType(Boolean))
        dt.Columns.Add("IsAutomatic", GetType(Boolean))
        dt.Columns.Add("ModUser", GetType(String))
        dt.Columns.Add("ModDate", GetType(String))
        dt.AcceptChanges()

        '   set time to be used on all records
        Dim rightNow = Date.Now

        'update table with amounts and TransCodeIds
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim row() As DataRow = dt.Select("PayPlanScheduleId='" + lateFees(i).Split(";")(0) + "'")
            row(0).SetAdded()
            row(0)("TransactionId") = Guid.NewGuid()
            row(0)("TransAmount") = Decimal.Parse(lateFees(i).Split(";")(1), NumberStyles.Currency)
            row(0)("TransDate") = postFeesDate
            row(0)("CampusId") = New Guid(campusId)
            row(0)("TransCodeId") = New Guid(transCodeId)
            row(0)("TransReference") = String.Empty
            row(0)("TransTypeId") = 0
            row(0)("IsPosted") = True
            row(0)("IsAutomatic") = True
            row(0)("ModUser") = user
            row(0)("ModDate") = rightNow
        Next

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction

        Try
            'add late fees to Transactions table

            ' Dim da As New OleDbDataAdapter("Select TransactionId, StuEnrollId, CampusId, TransDate, TransCodeId, TransReference, AcademicYearId, TransDescrip, TransAmount, TransTypeId, IsPosted, IsAutomatic, ModUser, ModDate from saTransactions ", ConfigurationManager.AppSettings("ConString"))
            Dim da As New OleDbDataAdapter("Select TransactionId, StuEnrollId, CampusId, TransDate, TransCodeId, TransReference, AcademicYearId, TransDescrip, TransAmount, TransTypeId, IsPosted, IsAutomatic, ModUser, ModDate from saTransactions ", MyAdvAppSettings.AppSettings("ConString").ToString)

            '   build insert, update and delete commands 
            Dim cb As New OleDbCommandBuilder(da)
            Dim insertCommand As OleDbCommand = cb.GetInsertCommand()
            insertCommand.Connection = New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
            insertCommand.Connection.Open()
            groupTrans = insertCommand.Connection.BeginTransaction()
            insertCommand.Transaction = groupTrans
            da.InsertCommand = insertCommand
            da.Update(dt)

            'commit transaction
            groupTrans.Commit()

            '   return with no errors
            Return ""
        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            End If

            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetListOfPayPlanScheduleIds(ByVal lateFees As String()) As String
        If lateFees.Length = 0 Then Return String.Empty
        Dim sb As New StringBuilder()
        For i As Integer = 0 To lateFees.Length - 1
            If i > 0 Then sb.Append(",")
            sb.Append("'")
            sb.Append(lateFees(i).Split(";")(0))
            sb.Append("'")
        Next
        Return sb.ToString()
    End Function

    Public Function GetDateOfLastLateFeesPosting(ByVal campusId As String) As Date
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       Max(T.TransDate) As MaxDate ")
            .Append("FROM   saTransactions T, saTransCodes TC ")
            .Append("WHERE  T.TransCodeId=TC.TransCodeId ")
            .Append("AND    CampusId=? ")
            .Append("AND    T.IsAutomatic = 1  ")
            .Append("AND    T.Voided=0 ")
            .Append("AND    TC.SysTransCodeId=7 ")
        End With

        'add CampusId parameter
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   fill date Array 
        Dim lastPostingDate As Date
        While dr.Read()
            If Not dr("MaxDate") Is DBNull.Value Then lastPostingDate = CType(dr("MaxDate"), DateTime) Else lastPostingDate = Date.MinValue
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return date array
        Return lastPostingDate

    End Function
    Public Function GetPostedFeesByTermDS(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder = New StringBuilder
        With sb
            .Append("SELECT Distinct ")
            .Append("       T1.StuEnrollId, ")
            .Append("       (Select FirstName + ' ' + LastName ")
            .Append("       from arStudent S ")
            .Append("       where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=T1.StuEnrollId)) As StudentName, ")
            .Append("       (Select Coalesce(Case Len(S.SSN) When 9 then '***-**-' + SUBSTRING(SSN,6,4) else ' ' end, ' ') ")
            .Append("       from arStudent S ")
            .Append("       where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=T1.StuEnrollId)) As StudentIdentifier, ")
            .Append("       T1.TransDescrip as FeeDescription, ")
            .Append("       T1.TransReference As FeeDetailDescription, ")
            .Append("       T1.TransDate, ")
            .Append("       T1.TransAmount ")
            .Append("from saTransactions T1, arStuEnrollments SE, arPrgVersions PV ")
            .Append("where T1.TermId=? ")
            .Append("and	IsAutomatic=1 ")
            .Append("and	T1.CampusId=? ")
            .Append("and T1.StuEnrollId=SE.StuEnrollId ")
            .Append("and SE.PrgVerId=PV.PrgVerId ")
            .Append("and T1.FeeLevelId=1 ")
        End With

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function

    Public Function GetPostedFeesByTermDSForCohortStart(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder = New StringBuilder
        With sb
            '.Append("select  ")
            '.Append("		StuEnrollId, ")
            '.Append("		TransCodeId, ")
            '.Append("		StudentName, ")
            '.Append("		StudentIdentifier, ")
            '.Append("	    (Case ApplyTo when 0 then '- All Programs' when 1 then '-' + (select Description from arProgTypes where ProgTypId=(select ProgTypId from arPrgVersions where PrgVerId=(select PrgVerId from arStuEnrollments where StuEnrollId=T.StuEnrollId))) when 2 then '-' + (select PrgVerDescrip from arPrgVersions where PrgVerId=(select top 1 PrgVerId from arStuEnrollments where StuEnrollId=T.StuEnrollId)) else '' end) As FeeDetailDescription,   ")
            '.Append("		FeeDescription, ")
            '.Append("       TransDate, ")
            '.Append("		TransAmount ")
            '.Append("from ")
            '.Append("( ")
            .Append("SELECT Distinct")
            .Append("       T1.StuEnrollId, ")
            '.Append("       PF.TransCodeId, ")
            '.Append("       PF.ApplyTo, ")
            .Append("       (Select TransCodeDescrip from saTransCodes where TransCodeId=T1.TransCodeId) As TransCodeDescrip, ")

            .Append("       (Select FirstName + ' ' + LastName from arStudent S where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=T1.StuEnrollId)) As StudentName, ")
            'include SSN or student identifier
            If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
                .Append("   (Select Coalesce(Case Len(S.SSN) When 9 then '***-**-' + SUBSTRING(SSN,6,4) else ' ' end, ' ') ")
            ElseIf CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToLower = "studentid" Then
                .Append("       (Select Coalesce(StudentNumber, ' ') ")
            Else
                .Append("   (Select Coalesce(" + CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToString + ", ' ') ")
            End If
            .Append("       from arStudent S ")
            .Append("       where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=T1.StuEnrollId) ")
            .Append("       ) As StudentIdentifier, ")
            .Append("	   T1.TransDescrip as FeeDescription, ")
            '.Append("      (Case PF.ApplyTo when 0 then '- All Programs' when 1 then '-' + PT.Description when 2 then '-' + PV.PrgVerDescrip else '' end) As FeeDetailDescription,  ")
            .Append("      T1.TransReference As FeeDetailDescription,  ")
            .Append("       T1.TransDate, ")
            .Append("	   T1.TransAmount ")
            '.Append("from saTransactions T1, saPeriodicFees PF ")
            .Append("from saTransactions T1, arStuEnrollments SE ")
            .Append("where T1.StuEnrollId=SE.StuEnrollId ")
            '.Append("		    T1.TransCodeId=PF.TransCodeId  ")
            '.Append("T1.TermId in (select arClassSections.TermId from arClassSections,arTerm where arClassSections.TermId =arTerm.TermId  ")
            '' .Append(" and arTerm.StartDate <= '" & Date.Now.ToShortDateString & "' and arTerm.EndDate>= '" & Date.Now.ToShortDateString & "'  ")
            '.Append(" and cohortstartdate = ?)  ")
            '.Append(" and        T1.TermId=PF.TermId ")
            '.Append("and		Voided=0  ")
            ''Query modified by SAraswathi Lakshmanan on Jan 30 2009
            ''The cohortStartDate is matched with the cohortstartdate of student in arstuenrollments
            .Append("and SE.CohortStartDate=? ")

            .Append("and		IsAutomatic=1 ")
            .Append("and		T1.CampusId= ? ")
            .Append("and        T1.FeeLevelId=1 ")

        End With

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add TermId to the parameter list
        'db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetPostedFeesByCourseDS(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder = New StringBuilder
        With sb
            .Append("SELECT Distinct ")
            .Append("       T1.StuEnrollId, ")
            '.Append("       CF.TransCodeId, ")
            '.Append("       (Select TransCodeDescrip from saTransCodes where TransCodeId=T1.TransCodeId) As TransCodeDescrip, ")

            .Append("       (Select FirstName + ' ' + LastName from arStudent S where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=T1.StuEnrollId)) As StudentName, ")
            'include SSN or student identifier
            If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
                .Append("   (Select Coalesce(Case Len(S.SSN) When 9 then '***-**-' + SUBSTRING(SSN,6,4) else ' ' end, ' ') ")
            ElseIf CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToLower = "studentid" Then
                .Append("       (Select Coalesce(StudentNumber, ' ') ")
            Else
                .Append("   (Select Coalesce(" + CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToString + ", ' ') ")
            End If
            .Append("       from arStudent S ")
            .Append("       where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=T1.StuEnrollId) ")
            .Append("       ) As StudentIdentifier, ")
            '.Append("	   T1.TransDescrip as FeeDescription, ")
            .Append("       (Select TransCodeDescrip from saTransCodes where TransCodeId=T1.TransCodeId) as FeeDescription, ")
            .Append("      T1.TransReference As FeeDetailDescription,  ")
            '.Append("       RQ.Descrip + ' ' + (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) TransDescrip, ")
            .Append("       T1.TransDate, ")
            .Append("       T1.TransAmount ")
            '.Append("FROM   arResults R, arStuEnrollments SE, arClassSections CS, arClassSectionTerms CST,  arReqs RQ, saCourseFees CF, saBillingMethods BM ")
            '.Append("FROM   saTransactions T1, saCourseFees CF ")
            .Append("FROM saTransactions T1 ")
            .Append("WHERE ")
            '.Append("		T1.TransCodeId=CF.TransCodeId  ")
            .Append("T1.TermId= ?  ")
            '.Append("and	Voided=0  ")
            '.Append("and	T1.TermId in (select TermId from arClassSections where reqid=CF.CourseId)  ")
            .Append("and	IsAutomatic=1 ")
            .Append("and	T1.CampusId= ? ")
            .Append("and    T1.FeeLevelId=3 ")
        End With

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return dataset
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)
        Return ds
    End Function

    Public Function GetPostedFeesByCourseDSForCohortStart(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder = New StringBuilder
        With sb
            .Append("SELECT Distinct ")
            .Append("       T1.StuEnrollId, ")
            '.Append("       CF.TransCodeId, ")
            '.Append("       ((Select TransCodeDescrip from saTransCodes where TransCodeId=CF.TransCodeId) + '-' + RQ.Descrip) TransCodeDescrip, ")

            .Append("       (Select FirstName + ' ' + LastName from arStudent S where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=T1.StuEnrollId)) As StudentName, ")
            'include SSN or student identifier
            If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
                .Append("   (Select Coalesce(Case Len(S.SSN) When 9 then '***-**-' + SUBSTRING(SSN,6,4) else ' ' end, ' ') ")
            ElseIf CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToLower = "studentid" Then
                .Append("       (Select Coalesce(StudentNumber, ' ') ")
            Else
                .Append("   (Select Coalesce(" + CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToString + ", ' ') ")
            End If
            .Append("       from arStudent S ")
            .Append("       where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=T1.StuEnrollId) ")
            .Append("       ) As StudentIdentifier, ")
            '.Append("	   T1.TransDescrip as FeeDescription, ")
            .Append("       (Select TransCodeDescrip from saTransCodes where TransCodeId=T1.TransCodeId) as FeeDescription, ")
            .Append("      T1.TransReference As FeeDetailDescription,  ")
            '.Append("       RQ.Descrip + ' ' + (Select LastName + ' ' + FirstName from arStudent where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=SE.StuEnrollId )) TransDescrip, ")
            .Append("       T1.TransDate, ")
            .Append("       T1.TransAmount ")
            '.Append("FROM   arResults R, arStuEnrollments SE, arClassSections CS, arClassSectionTerms CST,  arReqs RQ, saCourseFees CF, saBillingMethods BM ")
            .Append("FROM   saTransactions T1, arStuEnrollments SE ")
            .Append("WHERE T1.StuEnrollId=SE.StuEnrollId ")
            '.Append("		T1.TransCodeId=CF.TransCodeId  ")
            ''Cohort Start DAte removed from arclassSection Where clause to fix issue DE6233
            '.Append("and	T1.TermId in (select TermId from arClassSections where reqid=CF.CourseId )  ")
            '   .Append("and	T1.TermId in (select TermId from arClassSections where reqid=CF.CourseId and cohortstartdate = ?)  ")
            '.Append("and	Voided=0  ")
            ''Query Modified by saraswathi on Jan 30 2009
            ''CohortStartdate validation added
            '.Append("T1.StuEnrollId in (Select StuEnrollId from arStuEnrollments where StuEnrollId=T1.StuEnrollId and CohortStartDate= ? ) ")
            .Append("and SE.CohortStartDate=? ")
            .Append("and	IsAutomatic=1 ")
            .Append("and	T1.CampusId= ? ")
            .Append("and    T1.FeeLevelId=3 ")
        End With

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '' Add TermId to the parameter list
        'db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return dataset
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)
        Return ds
    End Function
    Public Function GetPostedFeesByProgramVersionDS(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder = New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       T1.StuEnrollId, ")
            '.Append("       PVF.TransCodeId, ")

            .Append("       (Select FirstName + ' ' + LastName from arStudent S where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=T1.StuEnrollId)) As StudentName, ")
            'include SSN or student identifier
            If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
                .Append("   (Select Coalesce(Case Len(S.SSN) When 9 then '***-**-' + SUBSTRING(SSN,6,4) else ' ' end, ' ') ")
            ElseIf CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToLower = "studentid" Then
                .Append("       (Select Coalesce(StudentNumber, ' ') ")
            Else
                .Append("   (Select Coalesce(" + CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToString + ", ' ') ")
            End If
            .Append("       from arStudent S ")
            .Append("       where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=T1.StuEnrollId) ")
            .Append("       ) As StudentIdentifier, ")

            '.Append("	    T1.TransDescrip as FeeDescription, ")
            .Append("       (Select TransCodeDescrip from saTransCodes where TransCodeId=T1.TransCodeId) as FeeDescription, ")

            .Append("       T1.TransReference As FeeDetailDescription,  ")
            .Append("       T1.TransDate, ")
            .Append("       T1.TransAmount ")
            .Append("FROM	saTransactions T1, arStuEnrollments SE, arPrgVersions PV, arStudent ST ")
            .Append("WHERE ")
            '.Append("		T1.TransCodeId=PVF.TransCodeId  ")
            .Append("T1.TermId= ?  ")
            '.Append("and	Voided=0  ")
            .Append("and T1.StuEnrollId=SE.StuEnrollId ")
            .Append("and SE.PrgVerId=PV.PrgVerId ")
            '.Append("and PVF.PrgVerId=PV.PrgVerId ")
            .Append("and SE.StudentId=ST.StudentId ")
            '.Append(" and T1.StuEnrollId in (Select StuEnrollId from arStuEnrollments where StuEnrollId=T1.StuEnrollId  AND PrgVerId=PVF.PrgVerId ) ")
            .Append("and	IsAutomatic=1 ")
            .Append("and	T1.CampusId= ? ")
            '.Append("and	(((PVF.RateScheduleId Is Null AND SE.TuitionCategoryId Is NULL AND PVF.TuitionCategoryId Is NULL) OR (PVF.RateScheduleId Is Null AND SE.TuitionCategoryId=PVF.TuitionCategoryId)) OR (PVF.RateScheduleId IS NOT NULL AND EXISTS(SELECT rsd.TuitionCategoryId FROM saRateScheduleDetails rsd WHERE rsd.RateScheduleId=PVF.RateScheduleId AND rsd.TuitionCategoryId=SE.TuitionCategoryId)) OR (PVF.RateScheduleId IS NOT NULL AND SE.TuitionCategoryId IS NULL AND EXISTS(SELECT rsd.* FROM saRateScheduleDetails rsd WHERE rsd.RateScheduleId=PVF.RateScheduleId AND rsd.TuitionCategoryId is null ))) ")
            .Append("and T1.FeeLevelId=2 ")
        End With

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetPostedFeesByProgramVersionDSForCohortStart(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder = New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       T1.StuEnrollId, ")
            '.Append("       PVF.TransCodeId, ")

            .Append("       (Select FirstName + ' ' + LastName from arStudent S where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=T1.StuEnrollId)) As StudentName, ")
            'include SSN or student identifier
            If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
                .Append("   (Select Coalesce(Case Len(S.SSN) When 9 then '***-**-' + SUBSTRING(SSN,6,4) else ' ' end, ' ') ")
            ElseIf CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToLower = "studentid" Then
                .Append("       (Select Coalesce(StudentNumber, ' ') ")
            Else
                .Append("   (Select Coalesce(" + CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToString + ", ' ') ")
            End If
            .Append("       from arStudent S ")
            .Append("       where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=T1.StuEnrollId) ")
            .Append("       ) As StudentIdentifier, ")

            '.Append("	    T1.TransDescrip as FeeDescription, ")
            .Append("       (Select TransCodeDescrip from saTransCodes where TransCodeId=T1.TransCodeId) as FeeDescription, ")

            .Append("       T1.TransReference As FeeDetailDescription,  ")
            .Append("       T1.TransDate, ")
            .Append("       T1.TransAmount ")
            .Append("FROM	saTransactions T1, arStuEnrollments SE ")
            .Append("WHERE ")
            .Append("		T1.StuEnrollId=SE.StuEnrollId  ")
            '.Append(" and	T1.TermId in(select TermId from arClassSections where cohortstartdate = ?)  ")
            '.Append(" and	Voided=0  ")
            ''Query Modified by saraswathi on Jan 30 2009
            ''CohortStartdate validation added
            '.Append(" and T1.StuEnrollId in (Select StuEnrollId from arStuEnrollments where StuEnrollId=T1.StuEnrollId  AND PrgVerId=PVF.PrgVerId and CohortStartDate=? ) ")
            .Append("and SE.CohortStartDate=? ")
            .Append("and	IsAutomatic=1 ")
            .Append("and	T1.CampusId= ? ")
            .Append("and    T1.FeeLevelId=2 ")
        End With

        ' Add TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ' Add TermId to the parameter list
        ' db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Sub New()

    End Sub
    'New Code Added By Vijay Ramteke On June 09, 2010
    Public Function GetAwardScheduledDisb_Sp_NoAid(ByVal stuEnrollId As String, ByVal fundSourceId As String, ByVal CutoffDate As String) As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   connect to the database
        Dim db As New SQLDataAccess

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        ' Add parameters
        db.AddParameter("@StuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        If fundSourceId <> Guid.Empty.ToString() Then
            db.AddParameter("@AwardTypeId", New Guid(fundSourceId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@AwardTypeId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If
        db.AddParameter("@CutoffDate", CutoffDate, SqlDbType.DateTime, , ParameterDirection.Input)

        '' New Code Added By Vijay Ramteke On September 25, 2010 For Mantis Id 19767
        Dim usp_Name As String
        If (HttpContext.Current.Session("UserName").ToString.ToUpper = "SA" Or HttpContext.Current.Session("UserName").ToString.ToUpper = "SUPER" Or HttpContext.Current.Session("UserName").ToString.ToUpper = "SUPPORT") Then
            usp_Name = "USP_SA_GetAwardScheduledDisb_NoAid_Admin"
        Else
            usp_Name = "USP_SA_GetAwardScheduledDisb_NoAid"
        End If
        '   Return dataset
        Return db.RunParamSQLDataSet_SP(usp_Name)
        ''Return db.RunParamSQLDataSet_SP("USP_SA_GetAwardScheduledDisb_NoAid")
        '' New Code Added By Vijay Ramteke On September 25, 2010 For Mantis Id 19767

    End Function
    'New Code Added By Vijay Ramteke On June 09, 2010

    '''Added by Saraswathi Lakshmanan on July 8 2010
    ''' ''To post the charges for the clock hour programms during student enrollment
    ''' 
    Public Function ApplyFeesByProgramVersionForCLockHourPrograms(ByVal stuEnrollId As String, ByVal user As String, ByVal campusId As String) As String

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   BuildQueryToSelectBillingMethod
        Dim sb As New StringBuilder

        sb = New StringBuilder
        With sb
            .Append(" Select SE.StuEnrollId,SE.ExpStartDate,PVF.PrgVerFeeId,PVF.TransCodeId, ")
            .Append("((Select TransCodeDescrip from saTransCodes where TransCodeId=PVF.TransCodeId) + '-' + PV.PrgVerDescrip) TransCodeDescrip, ")
            .Append("(Select PrgVerDescrip from arPrgVersions where PrgVerId=PVF.PrgVerId) TransDescrip,   ")
            .Append("(Case PVF.UnitId ")
            .Append("when 0 Then PVF.Amount*PV.Credits  ")
            .Append("when 1 then PVF.Amount*PV.Hours  ")
            .Append("when 2 then PVF.Amount End) TransAmount  ")
            .Append("from saProgramVersionFees PVF,arPrgVersions PV, arStuEnrollments SE  ")
            .Append("where SE.PrgVerId=PVF.PrgVerId and SE.StuEnrollId=? ")
            .Append(" aNd PV.PrgverID=PVF.PrgVerId and RateScheduleId is NUll ")
            ''Modified by Saraswathi lakshmanan on August 16 2010
            ''Tutio category is also compared against the student.
            ''For mantis 19531: QA: Program fees are automatically posted though there is a mismatch in the tuition category. 
            .Append(" and( ( PVF.TuitionCategoryId = SE.TuitionCategoryId )or ")
            .Append(" (PVF.TuitionCategoryId is null and SE.TuitionCategoryId is null)) ")
        End With

        '   add parameters values to the query
        db.ClearParameters()

        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   set time to be used on all records
        Dim rightNow = Date.Now

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   loop throughout all records
        While dr.Read()

            Dim PostChargeInfo As New PostChargeInfo

            '   set properties with data from DataReader
            With PostChargeInfo
                .StuEnrollId = stuEnrollId
                .FeeLevelId = 2 ' Program Version fees
                .FeeId = CType(dr("PrgVerFeeId"), Guid).ToString()
                .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                .PostChargeDate = dr("ExpStartDate")
                .PostChargeDescription = dr("TransCodeDescrip")
                If Not dr("TransAmount") Is DBNull.Value Then .Amount = dr("TransAmount")
                .Reference = dr("TransDescrip")
                .TermId = GetTermIdForStuEnrollId(stuEnrollId)
                .TransTypeId = 0
                .IsPosted = True
                .CampusId = campusId
                .ModUser = user
                .ModDate = rightNow
            End With

            '   add the entry to Transactions table
            Try
                '   BuildQueryToInsertPostChargeInfo
                BuildQueryToInsertPostChargeInfo(PostChargeInfo, db, sb, user, rightNow)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            Catch ex As OleDbException
                '   rollback transaction if there were errors
                groupTrans.Rollback()

                '   do not report sql lost connection
                If Not groupTrans.Connection Is Nothing Then
                    ' Report the error
                End If

                'Close Connection
                db.CloseConnection()

                '   return an error to the client
                Return DALExceptions.BuildErrorMessage(ex)
            End Try

        End While

        '   commit transaction 
        groupTrans.Commit()

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   return with no errors
        Return ""

    End Function
    Public Sub PostTermFees(ByVal xmlRules As String)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim db As New SQLDataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once

            db.AddParameter("@AttValues", xmlRules, SqlDbType.VarChar, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("USP_PostTermFees", Nothing)
        Catch ex As Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Sub
    Public Function CheckTransDuplicatePayment(postPaymentInfo As PostPaymentInfo) As Boolean

        '   connect to the database
        Dim intRowCount As Integer
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        ' Add parameter
        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(postPaymentInfo.StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        If postPaymentInfo.TermId = Guid.Empty.ToString Then
            db.AddParameter("@TermId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@TermId", New Guid(postPaymentInfo.TermId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If



        If postPaymentInfo.CampusId = Guid.Empty.ToString Then
            db.AddParameter("@CampusId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@CampusId", New Guid(postPaymentInfo.CampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If


        db.AddParameter("@TransDate", postPaymentInfo.PostPaymentDate, SqlDbType.DateTime, , ParameterDirection.Input)

        If postPaymentInfo.TransCodeId = Guid.Empty.ToString Then
            db.AddParameter("@TransCodeId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@TransCodeId", New Guid(postPaymentInfo.TransCodeId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If



        db.AddParameter("@TransReference", postPaymentInfo.Reference, SqlDbType.VarChar, 50, ParameterDirection.Input)

        If postPaymentInfo.AcademicYearId = Guid.Empty.ToString Then
            db.AddParameter("@AcademicYearId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@AcademicYearId", New Guid(postPaymentInfo.AcademicYearId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If




        db.AddParameter("@TransDescrip", postPaymentInfo.PostPaymentDescription, SqlDbType.VarChar, 50, ParameterDirection.Input)

        db.AddParameter("@TransAmount", postPaymentInfo.Amount, SqlDbType.Decimal, , ParameterDirection.Input)

        db.AddParameter("@TransTypeId", postPaymentInfo.TransTypeId, SqlDbType.Int, , ParameterDirection.Input)

        db.AddParameter("@IsPosted", postPaymentInfo.IsPosted, SqlDbType.Bit, , ParameterDirection.Input)
        db.AddParameter("@IsVoided", postPaymentInfo.IsVoided, SqlDbType.Bit, , ParameterDirection.Input)


        db.AddParameter("@FeeLevelId", DBNull.Value, SqlDbType.Int, , ParameterDirection.Input)



        If postPaymentInfo.FeeId = Guid.Empty.ToString Then
            db.AddParameter("@FeeId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@FeeId", New Guid(postPaymentInfo.FeeId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If


        If postPaymentInfo.PostPaymentId = Guid.Empty.ToString Then
            db.AddParameter("@PaymentCodeId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@PaymentCodeId", New Guid(postPaymentInfo.PaymentCodeID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If



        If postPaymentInfo.FundSourceID = Guid.Empty.ToString Then
            db.AddParameter("@FundSourceId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Else
            db.AddParameter("@FundSourceId", New Guid(postPaymentInfo.FundSourceID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If

        intRowCount = db.RunParamSQLScalar_SP("USP_CheckTransDuplicate")

        If intRowCount > 0 Then
            Return True
        Else
            Return False
        End If


    End Function
    Public Function CheckTransDuplicateCharge(postChargeInfo As PostChargeInfo) As Boolean

        '   connect to the database
        Dim intRowCount As Integer
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        ' Add parameter
        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(postChargeInfo.StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        If postChargeInfo.TermId = Guid.Empty.ToString Then
            db.AddParameter("@TermId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@TermId", New Guid(postChargeInfo.TermId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If



        If postChargeInfo.CampusId = Guid.Empty.ToString Then
            db.AddParameter("@CampusId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@CampusId", New Guid(postChargeInfo.CampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If


        db.AddParameter("@TransDate", postChargeInfo.PostChargeDate, SqlDbType.DateTime, , ParameterDirection.Input)

        If postChargeInfo.TransCodeId = Guid.Empty.ToString Then
            db.AddParameter("@TransCodeId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@TransCodeId", New Guid(postChargeInfo.TransCodeId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If



        db.AddParameter("@TransReference", postChargeInfo.Reference, SqlDbType.VarChar, 50, ParameterDirection.Input)

        If postChargeInfo.AcademicYearId = Guid.Empty.ToString Then
            db.AddParameter("@AcademicYearId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@AcademicYearId", New Guid(postChargeInfo.AcademicYearId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If




        db.AddParameter("@TransDescrip", postChargeInfo.PostChargeDescription, SqlDbType.VarChar, 50, ParameterDirection.Input)

        db.AddParameter("@TransAmount", postChargeInfo.Amount, SqlDbType.Decimal, , ParameterDirection.Input)

        db.AddParameter("@TransTypeId", postChargeInfo.TransTypeId, SqlDbType.Int, , ParameterDirection.Input)

        db.AddParameter("@IsPosted", postChargeInfo.IsPosted, SqlDbType.Bit, , ParameterDirection.Input)
        db.AddParameter("@IsVoided", 0, SqlDbType.Bit, , ParameterDirection.Input)


        db.AddParameter("@FeeLevelId", postChargeInfo.FeeLevelId, SqlDbType.Int, , ParameterDirection.Input)



        If postChargeInfo.FeeId = Guid.Empty.ToString Or postChargeInfo.FeeId Is Nothing Then
            db.AddParameter("@FeeId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@FeeId", New Guid(postChargeInfo.FeeId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If



        db.AddParameter("@PaymentCodeId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)


        db.AddParameter("@FundSourceId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)



        intRowCount = db.RunParamSQLScalar_SP("USP_CheckTransDuplicate")

        If intRowCount > 0 Then
            Return True
        Else
            Return False
        End If


    End Function
    Public Function CheckTransDuplicateRefund(postRefundInfo As PostRefundInfo) As Boolean

        '   connect to the database
        Dim intRowCount As Integer
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        ' Add parameter
        ' Add stuEnrollId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(postRefundInfo.StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        If postRefundInfo.TermId = Guid.Empty.ToString Then
            db.AddParameter("@TermId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@TermId", New Guid(postRefundInfo.TermId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If



        If postRefundInfo.CampusId = Guid.Empty.ToString Then
            db.AddParameter("@CampusId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@CampusId", New Guid(postRefundInfo.CampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If


        db.AddParameter("@TransDate", postRefundInfo.PostRefundDate, SqlDbType.DateTime, , ParameterDirection.Input)

        If postRefundInfo.TransCodeId = Guid.Empty.ToString Then
            db.AddParameter("@TransCodeId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@TransCodeId", New Guid(postRefundInfo.TransCodeId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If



        db.AddParameter("@TransReference", postRefundInfo.Reference, SqlDbType.VarChar, 50, ParameterDirection.Input)

        If postRefundInfo.AcademicYearId = Guid.Empty.ToString Then
            db.AddParameter("@AcademicYearId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@AcademicYearId", New Guid(postRefundInfo.AcademicYearId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If

        db.AddParameter("@TransDescrip", postRefundInfo.PostRefundDescription, SqlDbType.VarChar, 50, ParameterDirection.Input)
        db.AddParameter("@TransAmount", -postRefundInfo.Amount, SqlDbType.Decimal, , ParameterDirection.Input)
        db.AddParameter("@TransTypeId", postRefundInfo.TransTypeId, SqlDbType.Int, , ParameterDirection.Input)
        db.AddParameter("@IsPosted", postRefundInfo.IsPosted, SqlDbType.Bit, , ParameterDirection.Input)
        db.AddParameter("@IsVoided", 0, SqlDbType.Bit, , ParameterDirection.Input)
        db.AddParameter("@FeeLevelId", DBNull.Value, SqlDbType.Int, , ParameterDirection.Input)
        db.AddParameter("@FeeId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@PaymentCodeId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@FundSourceId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        intRowCount = db.RunParamSQLScalar_SP("USP_CheckTransDuplicate")

        If intRowCount > 0 Then
            Return True
        Else
            Return False
        End If


    End Function

    ''' <summary>
    ''' Search the Transaction to get the Ledge for lead not enrolled.
    ''' </summary>
    ''' <param name="campGrpId"></param>
    ''' <param name="transDateFrom"></param>
    ''' <param name="transDateTo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SearchGlForLead(ByVal campGrpId As String, ByVal transDateFrom As Date, ByVal transDateTo As Date) As IList(Of GeneralLedgerInfo)
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim sb As New StringBuilder
        With sb
            .Append(" SELECT * ")
            .Append(" FROM    ( SELECT    CONVERT(DATE,t.CreatedDate) AS [CreateDate] ")
            .Append("                    ,code.TransCodeCode + ' - ' + t.TransDescrip + ' - ' + FirstName + ' ' + l.LastName AS [Transaction] ")
            .Append("                    ,gla.GLAccount AS [GLAccount] ")
            .Append("                    ,gla.TransCodeGLAccountId AS GLAccountId ")
            .Append("                    ,code.TransCodeDescrip  ")
            .Append("                    ,( CASE WHEN tt.Description = 'Payment' ")
            .Append("                                 AND gla.TypeId = 0 THEN -(t.TransAmount) ")
            .Append("                       END ) AS Debit ")
            .Append("                    ,( CASE WHEN tt.Description = 'Payment' ")
            .Append("                                 AND gla.TypeId = 1 THEN -(t.TransAmount) ")
            .Append("                       END ) AS Credit ")
            .Append("           FROM      adLeadTransactions t ")
            .Append("           JOIN      dbo.saTransCodes code ON code.TransCodeId = t.TransCodeId ")
            .Append("           JOIN      adLeads l ON l.LeadId = t.LeadId ")
            .Append("           JOIN      dbo.saTransTypes tt ON tt.TransTypeId = t.TransTypeId ")
            .Append("           JOIN     dbo.saTransCodeGLAccounts gla ON gla.TransCodeId = code.TransCodeId ")
            .Append("           WHERE     isEnrolled = 0 ")
            .Append("                     AND code.CampGrpId = @CmpGrp ")
            .Append("                     AND CONVERT(DATE,t.CreatedDate) >= CONVERT(DATE,@BeginDate) ")
            .Append("                     AND CONVERT(DATE,t.CreatedDate) <= CONVERT(DATE,@EndDate) ")
            .Append("         ) t ")
            .Append(" WHERE   Debit IS NOT NULL ")
            .Append("         OR Credit IS NOT NULL  ")
        End With

        Dim command As SqlCommand = New SqlCommand(sb.ToString(), sqlconn)

        ' Add Parameters
        command.Parameters.AddWithValue("@CmpGrp", campGrpId)
        command.Parameters.AddWithValue("@BeginDate", transDateFrom)
        command.Parameters.AddWithValue("@EndDate", transDateTo)

        Dim list As IList(Of GeneralLedgerInfo) = New List(Of GeneralLedgerInfo)
        'Open connection
        sqlconn.Open()
        Try
            Dim sqlreader As SqlDataReader = command.ExecuteReader()
            While (sqlreader.Read())
                Dim info As GeneralLedgerInfo = New GeneralLedgerInfo()
                info.CreateDate = sqlreader("CreateDate")
                Dim credit = sqlreader("Credit")
                info.Credit = If(IsDBNull(credit), Nothing, credit)
                Dim debit = sqlreader("Debit")
                info.Debit = If(IsDBNull(debit), Nothing, debit)
                info.GlAccount = sqlreader("GLAccount")
                info.GlAccountId = sqlreader("GLAccountId").ToString()
                info.Transaction = sqlreader("Transaction")
                info.TransactionDescrip = sqlreader("TransCodeDescrip")
                list.Add(info)
            End While
            Return list
        Finally
            sqlconn.Close()
        End Try
    End Function
End Class
