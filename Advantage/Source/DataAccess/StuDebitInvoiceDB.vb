Imports FAME.Advantage.Common

Public Class StuDebitInvoiceDB
	Public Function GetStudentDebitInvoices(ByVal StartDate As String, ByVal CampusID As String, ByVal StatusId As String, ByVal ProgramId As String, ByVal ExpectDate As String) As DataSet
		Dim db As New DataAccess
		Dim sb As New System.Text.StringBuilder
		Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
		If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
			MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
		Else
			MyAdvAppSettings = New AdvAppSettings
		End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'With sb
        '.Append("SELECT * FROM ")
        '.Append("   	    (SELECT ")
        '.Append("               S.LastName,S.FirstName,S.MiddleName,S.SSN,PrgVerDescrip,S.StudentId,")
        '.Append("               SC.StatusCodeDescrip,SE.StartDate,SE.DateDetermined,SE.ExpGradDate,SE.StuEnrollId,")
        '.Append("               ((SELECT SUM(Amount) AS Balance ")
        '.Append("               FROM faStudentPaymentPlans PP,faStuPaymentPlanSchedule,arStuEnrollments E,arStudent S ")
        '.Append("               WHERE(PP.PaymentPlanId = faStuPaymentPlanSchedule.PaymentPlanId) ")
        '.Append("               AND faStuPaymentPlanSchedule.ExpectedDate <= ? ")
        ''.Append("               AND NOT EXISTS (SELECT * FROM saPmtDisbRel ")
        ''.Append("               		        WHERE PayPlanScheduleId=faStuPaymentPlanSchedule.PayPlanScheduleId) ")
        '.Append(" AND NOT EXISTS (SELECT * FROM saPmtDisbRel,saTransactions WHERE ")
        '.Append(" PayPlanScheduleId = faStuPaymentPlanSchedule.PayPlanScheduleId ")
        '.Append(" and saPmtDisbRel.TransactionID=saTransactions.TransactionID and ")
        '.Append(" faStuPaymentPlanSchedule.Amount=saPmtDisbRel.amount and saTransactions.StuEnrollId=E.StuEnrollId) ")
        '.Append("               AND PP.StuEnrollId=E.StuEnrollId AND E.StudentId=S.StudentId ")
        '.Append("               AND E.CampusId = ? AND E.StuEnrollId = SE.StuEnrollId ")
        '.Append("               GROUP BY E.StuEnrollId )")
        '.Append(" - isnull((select sum(saPmtDisbRel.amount) from saPmtDisbRel,saTransactions,faStudentPaymentPlans,faStuPaymentPlanSchedule ")
        '.Append(" where faStudentPaymentPlans.PaymentPlanId=faStuPaymentPlanSchedule.PaymentPlanId and ")
        '.Append(" saPmtDisbRel.PayPlanScheduleId = faStuPaymentPlanSchedule.PayPlanScheduleId and ")
        '.Append(" saPmtDisbRel.TransactionID=saTransactions.TransactionID and ")
        '.Append(" saTransactions.StuEnrollId = SE.StuEnrollId ")
        '.Append(" AND NOT EXISTS (SELECT * FROM saPmtDisbRel,saTransactions WHERE ")
        '.Append(" PayPlanScheduleId = faStuPaymentPlanSchedule.PayPlanScheduleId  and saPmtDisbRel.TransactionID=saTransactions.TransactionID and ")
        '.Append(" faStuPaymentPlanSchedule.Amount=saPmtDisbRel.amount and saTransactions.StuEnrollId=SE.StuEnrollId) ")
        '.Append(" AND faStuPaymentPlanSchedule.ExpectedDate <= ?  GROUP BY SE.StuEnrollId),0) ")
        '.Append(") AS Balance ")
        '.Append("           FROM arStudent S,arStuEnrollments SE,syStatusCodes SC,arPrgVersions ")
        '.Append("           WHERE SE.StatusCodeId=SC.StatusCodeId ")
        'If StatusId <> "" Then
        '    .Append(" and SC.StatusCodeId='" + StatusId + "'")
        'End If
        'If ProgramId <> "" Then
        '    .Append(" and arPrgVersions.PrgVerId='" + ProgramId + "'")
        'End If
        'If ExpectDate <> "" Then
        '    .Append(" and SE.ExpGradDate='" + ExpectDate + "'")
        'End If
        '.Append(" AND SE.PrgVerId=arPrgVersions.PrgVerId AND S.StudentId=SE.StudentId) P ")
        '.Append("WHERE Balance IS NOT NULL ")
        '.Append(" ORDER BY LastName,FirstName,MiddleName,SSN,PrgVerDescrip")
        '    .Append("SELECT * FROM    	     ")
        '    .Append("( ")
        '    .Append("	SELECT                 ")
        '    .Append("		S.LastName, ")
        '    .Append("		S.FirstName, ")
        '    .Append("		S.MiddleName, ")
        '    .Append("		S.SSN, ")
        '    .Append("		PrgVerDescrip, ")
        '    .Append("		S.StudentId, ")
        '    .Append("		SC.StatusCodeDescrip, ")
        '    .Append("		SE.StartDate, ")
        '    .Append("		SE.DateDetermined, ")
        '    .Append("		SE.ExpGradDate, ")
        '    .Append("		SE.StuEnrollId,                ")
        '    .Append("		( ")
        '    .Append("			select  ")
        '    .Append("					sum(Amount) As Amount ")
        '    .Append("			from ")
        '    .Append("			( ")
        '    .Append("				select  ")
        '    .Append("						SPS.Amount - IsNull((Select Sum(Amount) from saPmtDisbRel PDR, saTransactions T where PDR.TransactionId=T.TransactionId and T.Voided=0 and PDR.PayPlanScheduleId=SPS.PayPlanScheduleId  and T.StuEnrollid=SPP.StuEnrollid ),0) as Amount  ")
        '    .Append("				from faStudentPaymentPlans SPP, faStuPaymentPlanSchedule SPS  ")
        '    .Append("				where  ")
        '    .Append("						SPP.StuEnrollId=SE.StuEnrollId ")
        '    .Append("				and		SPP.PaymentPlanId=SPS.PaymentPlanId ")
        '    .Append("				and		SPS.ExpectedDate < ? ")
        '    .Append("			) T1 ")
        '    .Append("		) AS Balance             ")
        '    .Append("	FROM arStudent S,arStuEnrollments SE,syStatusCodes SC,arPrgVersions             ")
        '    .Append("	WHERE  ")
        '    .Append("		SE.StatusCodeId=SC.StatusCodeId   ")
        '    .Append("	AND SE.PrgVerId=arPrgVersions.PrgVerId  ")
        '    .Append("	AND S.StudentId=SE.StudentId ")
        '    .Append("   AND SE.CampusId= ? ")
        '    If StatusId <> "" Then
        '        .Append(" and SC.StatusCodeId='" + StatusId + "'")
        '    End If
        '    If ProgramId <> "" Then
        '        .Append(" and arPrgVersions.PrgVerId='" + ProgramId + "'")
        '    End If
        '    If ExpectDate <> "" Then
        '        .Append(" and SE.ExpGradDate='" + ExpectDate + "'")
        '    End If
        '    .Append(") P  ")
        '    .Append("WHERE Balance IS NOT NULL  and Balance <> 0 ")
        '    .Append("ORDER BY LastName,FirstName,MiddleName,SSN,PrgVerDescrip ")
        'End With
        'db.AddParameter("@StartDate", Convert.ToDateTime(StartDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        'db.AddParameter("@CampusId", CampusID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'ds = db.RunParamSQLDataSet(sb.ToString)

        With sb
			.Append("DECLARE @StartDate AS DATETIME " + vbCrLf)
			.Append("DECLARE @CampusId AS UNIQUEIDENTIFIER " + vbCrLf)
'            .Append("DECLARE @StatusId AS UNIQUEIDENTIFIER " + vbCrLf)
'            .Append("DECLARE @PrgVerId AS UNIQUEIDENTIFIER " + vbCrLf)
'            .Append("DECLARE @ExpGradDate AS DATETIME " + vbCrLf)
'            .Append("--DECLARE @StuEnrollIdList AS VARCHAR(MAX) " + vbCrLf)
			.Append("SET @StartDate = ? " + vbCrLf)
			.Append("SET @CampusId = ? " + vbCrLf)
'            .Append("SET @StatusId = ? " + vbCrLf)
'            .Append("SET @PrgVerId = ? " + vbCrLf)
'            .Append("SET @ExpGradDate = ? " + vbCrLf)
			.Append("SELECT AST.LastName " + vbCrLf)
			.Append("     , AST.FirstName " + vbCrLf)
			.Append("     , AST.MiddleName " + vbCrLf)
			.Append("     , AST.SSN " + vbCrLf)
			.Append("     , APV.PrgVerDescrip " + vbCrLf)
			.Append("     , AST.StudentId " + vbCrLf)
			.Append("     , SSC.StatusCodeDescrip " + vbCrLf)
			.Append("     , ASE.StartDate " + vbCrLf)
			.Append("     , ASE.DateDetermined " + vbCrLf)
			.Append("     , ASE.ExpGradDate " + vbCrLf)
			.Append("     , ASE.StuEnrollId " + vbCrLf)
			.Append("     , T.Balance AS Balance " + vbCrLf)
'            .Append("--     , T.PaymentPlanId " + vbCrLf)
			.Append("FROM arStudent AS AST " + vbCrLf)
			.Append("   INNER JOIN arStuEnrollments AS ASE ON ASE.StudentId = AST.StudentId " + vbCrLf)
			.Append("   INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId " + vbCrLf)
			.Append("   INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId " + vbCrLf)
			.Append("   INNER JOIN ( " + vbCrLf)
			.Append("               SELECT FSPP.StuEnrollId " + vbCrLf)
			.Append("                    , FSPP.PaymentPlanId " + vbCrLf)
			.Append("                    , SUM(ISNULL(FSPPS.Amount, 0) - ISNULL(SPDR2.AmountPaid, 0)) AS Balance " + vbCrLf)
			.Append("               FROM faStudentPaymentPlans AS FSPP " + vbCrLf)
			.Append("               	INNER JOIN faStuPaymentPlanSchedule AS FSPPS ON FSPPS.PaymentPlanId = FSPP.PaymentPlanId " + vbCrLf)
			.Append("               LEFT JOIN ( " + vbCrLf)
            .Append("                            SELECT CASE WHEN amountsPaid.AmountPaid < 0 THEN amountsPaid.AmountPaid * -1 " + vbCrLf)
			.Append("                                ELSE amountsPaid.AmountPaid " + vbCrLf)
			.Append("                           END AS AmountPaid " + vbCrLf)
            .Append("                           ,amountsPaid.PayPlanScheduleId" + vbCrLf)
            .Append("                           FROM   (" + vbCrLf)
			.Append("                           SELECT SPDR.PayPlanScheduleId " + vbCrLf)
			.Append("                                , SUM(ISNULL(SPDR.Amount, 0)) AS AmountPaid " + vbCrLf)
			.Append("                           FROM saPmtDisbRel AS SPDR " + vbCrLf)
			.Append("                           	INNER JOIN saTransactions AS ST ON ST.TransactionId = SPDR.TransactionId " + vbCrLf)
			.Append("                           	INNER JOIN faStuPaymentPlanSchedule AS FSPPS2 ON FSPPS2.PayPlanScheduleId = SPDR.PayPlanScheduleId " + vbCrLf)
			.Append("                           WHERE ST.Voided = 0 " + vbCrLf)
			.Append("                           GROUP BY SPDR.PayPlanScheduleId " + vbCrLf)
            .Append("                         ) AS amountsPaid " + vbCrLf)
			.Append("                         ) AS SPDR2 ON SPDR2.PayPlanScheduleId = FSPPS.PayPlanScheduleId " + vbCrLf)
			.Append("               WHERE FSPPS.ExpectedDate < @StartDate  " + vbCrLf)
			.Append("               GROUP BY FSPP.StuEnrollId " + vbCrLf)
			.Append("                      , FSPP.PaymentPlanId " + vbCrLf)
			.Append("             ) AS T ON T.StuEnrollId = ASE.StuEnrollId " + vbCrLf)
			.Append("WHERE ASE.CampusId = @CampusId " + vbCrLf)
			.Append("  AND ISNULL(T.Balance, 0) > 0 " + vbCrLf)
'            .Append("--  AND ASE.StuEnrollId IN (SELECT Val FROM [MultipleValuesForReportParameters](@StuEnrollIdList, ',', 1)) -- StuEnrollIdList  " + vbCrLf)
			If StatusId <> "" Then
				.Append("  AND SSC.StatusCodeId = ? -- @StatusId " + vbCrLf)
			End If
			If ProgramId <> "" Then
				.Append("  AND APV.PrgVerId = ? -- @PrgVerId --  ProgramId " + vbCrLf)
			End If
			If ExpectDate <> "" Then
				.Append("  AND ASE.ExpGradDate = ? --@ExpGradDate -- ExpectedDate " + vbCrLf)
			End If
			.Append("ORDER BY AST.LastName " + vbCrLf)
			.Append("       , AST.FirstName " + vbCrLf)
			.Append("       , AST.MiddleName " + vbCrLf)
			.Append("       , AST.SSN " + vbCrLf)
			.Append("       , APV.PrgVerDescrip; " + vbCrLf)
		End With
		db.AddParameter("@StartDate", Convert.ToDateTime(StartDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
		db.AddParameter("@CampusId", CampusID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
		If StatusId <> "" Then
			db.AddParameter("@StatusId", StatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
		End If
		If ProgramId <> "" Then
			db.AddParameter("@PrgVerId", ProgramId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
		End If
		If ExpectDate <> "" Then
			db.AddParameter("@ExpGradDate", Convert.ToDateTime(ExpectDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
		End If

        ds = db.RunParamSQLDataSet(sb.ToString)
		Return ds

    End Function

	Public Function GetInvoiceDS(ByVal CampusId As String, ByVal StuEnrollIdList As String, ByVal RefDate As String) As DataSet
		Dim db As New DataAccess
		Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
		If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
			MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
		Else
			MyAdvAppSettings = New AdvAppSettings
		End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        
        'With sb
		'    'OLD QUERY===========================================================
		'    '.Append("SELECT ")
		'    '.Append("(S.FirstName + ' ' + isnull(S.MiddleName,'') + ' ' + S.LastName) as Student,")
		'    '.Append("(select top 1 (Address1 + ' ' + isnull(Address2,'')) from arStudAddresses SA2 where SA2.studentid=S.StudentId order by default1 desc) as Address,")
		'    '.Append("(select top 1 city from arStudAddresses SA2 where SA2.studentid=S.StudentId order by default1 desc) as City,")
		'    '.Append("(select top 1 Zip from arStudAddresses SA2 where SA2.studentid=S.StudentId order by default1 desc) as Zip,")
		'    '.Append("(select top 1 SA.stateDescrip from arStudAddresses SA2,syStates SA where SA2.studentid=S.StudentId and SA2.StateId=SA.StateId order by SA2.default1 desc) as StateDescrip,")
		'    '.Append("StatusCodeDescrip,ExpectedDate,Amount = ")
		'    '.Append(" CASE  WHEN faStuPaymentPlanSchedule.Amount > (SELECT sum(Amount)  FROM saPmtDisbRel pmt ")
		'    '.Append(" WHERE pmt.PayPlanScheduleId=faStuPaymentPlanSchedule.PayPlanScheduleId and faStuPaymentPlanSchedule.Amount > pmt.amount)   THEN ")
		'    '.Append(" faStuPaymentPlanSchedule.Amount-(SELECT sum(Amount)  FROM saPmtDisbRel pmt ")
		'    '.Append(" WHERE pmt.PayPlanScheduleId=faStuPaymentPlanSchedule.PayPlanScheduleId and faStuPaymentPlanSchedule.Amount > pmt.amount) ")
		'    '.Append("   ELSE faStuPaymentPlanSchedule.Amount ")
		'    '.Append("  End ")
		'    '.Append(" ,PrgVerDescrip,")
		'    '.Append("E.StuEnrollId ,S.SSN,S.StudentNumber ")
		'    '.Append("FROM faStudentPaymentPlans PP,faStuPaymentPlanSchedule,arStuEnrollments E,arStudent S,syStatusCodes SC,arPrgVersions ")        ',arStudAddresses SA
		'    '.Append("WHERE PP.PaymentPlanId=faStuPaymentPlanSchedule.PaymentPlanId AND faStuPaymentPlanSchedule.ExpectedDate <= '" & RefDate & "' ")
		'    '.Append("AND NOT EXISTS (SELECT * FROM saPmtDisbRel WHERE PayPlanScheduleId=faStuPaymentPlanSchedule.PayPlanScheduleId and  faStuPaymentPlanSchedule.Amount=saPmtDisbRel.amount ) ")
		'    '.Append("AND PP.StuEnrollId=E.StuEnrollId AND E.StudentId=S.StudentId and E.StatusCodeId=SC.StatusCodeId ")
		'    '.Append("AND E.PrgVerId=arPrgVersions.PrgVerId ")
		'    '.Append("and e.campusid = '" & CampusId & "' and E.StuEnrollId in(" & StuEnrollId & ") ")
		'    '.Append("order by E.StuEnrollId,ExpectedDate")
		'    'end of old query======================================================
		'    .Append("select *  " + vbCrLf)
		'    .Append("from " + vbCrLf)
		'    .Append("( " + vbCrLf)
		'    .Append("select   " + vbCrLf)
		'    .Append("		(S.FirstName + ' ' + isnull(S.MiddleName,'') + ' ' + S.LastName) as Student, " + vbCrLf)
		'    .Append("		(select top 1 (Address1 + ' ' + isnull(Address2,'')) from arStudAddresses SA2 where SA2.studentid=S.StudentId order by default1 desc) as Address, " + vbCrLf)
		'    .Append("		(select top 1 city from arStudAddresses SA2 where SA2.studentid=S.StudentId order by default1 desc) as City, " + vbCrLf)
		'    .Append("		(select top 1 Zip from arStudAddresses SA2 where SA2.studentid=S.StudentId order by default1 desc) as Zip, " + vbCrLf)
		'    .Append("		(select top 1 SA.stateDescrip from arStudAddresses SA2,syStates SA where SA2.studentid=S.StudentId and SA2.StateId=SA.StateId order by SA2.default1 desc) as StateDescrip, " + vbCrLf)
		'    .Append("		SC.StatusCodeDescrip, " + vbCrLf)
		'    .Append("		SPS.ExpectedDate, " + vbCrLf)
		'    '.Append("		(SPS.Amount - IsNull((Select Sum(Amount) from saPmtDisbRel where PayPlanScheduleId=SPS.PayPlanScheduleId),0)) as Amount, " + vbCrLf)
		'    .Append("		(SPS.Amount - IsNull((Select Sum(Amount) from saPmtDisbRel PDR, saTransactions T where PDR.TransactionId=T.TransactionId and T.Voided=0 and PDR.PayPlanScheduleId=SPS.PayPlanScheduleId),0)) as Amount, " + vbCrLf)
		'    .Append("		PV.PrgVerDescrip, " + vbCrLf)
		'    .Append("		SE.StuEnrollId , " + vbCrLf)
		'    .Append("		S.SSN, " + vbCrLf)
		'    .Append("		S.StudentNumber,S.LastName,S.FirstName,S.MiddleName, SPP.PayPlanDescrip  " + vbCrLf)
		'    .Append("from faStudentPaymentPlans SPP, faStuPaymentPlanSchedule SPS, arStuEnrollments SE, arStudent S, syStatusCodes SC, arPrgVersions PV " + vbCrLf)
		'    .Append("where " + vbCrLf)
		'    .Append("		    SPP.PaymentPlanId=SPS.PaymentPlanId " + vbCrLf)
		'    '.Append("and		SPS.PayPlanScheduleId=PDR.PayPlanScheduleId " + vbCrLf)
		'    .Append("and		SPS.ExpectedDate <= '" & RefDate & "' " + vbCrLf)
		'    .Append("and        SE.Campusid = '" & CampusId & "' " + vbCrLf)
		'    .Append("and		SPP.StuEnrollId in (" & StuEnrollId & ") " + vbCrLf)
		'    .Append("and		SPP.StuEnrollId = SE.StuEnrollId " + vbCrLf)
		'    .Append("and		SE.StudentId=S.StudentId " + vbCrLf)
		'    .Append("and		SE.StatusCodeId=SC.StatusCodeId  " + vbCrLf)
		'    .Append("and		SE.PrgVerId=PV.PrgVerId  " + vbCrLf)
		'    '.Append("--order by SE.StuEnrollId, SPS.ExpectedDate " + vbCrLf)
		'    .Append(") T1 " + vbCrLf)
		'    .Append("Where Amount > 0.00 " + vbCrLf)
		'    '.Append("order by StuEnrollId, ExpectedDate " + vbCrLf)
		'    .Append(" ORDER BY T1.LastName,T1.FirstName,T1.MiddleName,T1.SSN,T1.PrgVerDescrip,T1.ExpectedDate " + vbCrLf)
		 With sb
			.Append("SELECT (AST.FirstName + ' ' + ISNULL(AST.MiddleName, '') + ' ' + AST.LastName) AS Student " + vbCrLf)
			.Append("     , ASA2.Address " + vbCrLf)
			.Append("     , ASA2.City " + vbCrLf)
			.Append("     , ASA2.Zip " + vbCrLf)
			.Append("     , ASA2.StateDescrip " + vbCrLf)
			.Append("     , SSC.StatusCodeDescrip " + vbCrLf)
			.Append("     , FSPPS.ExpectedDate " + vbCrLf)
			.Append("     , (ISNULL(FSPPS.Amount, 0) - ISNULL(SPDR2.AmountPaid, 0)) AS Amount " + vbCrLf)
			.Append("     , APV.PrgVerDescrip " + vbCrLf)
			.Append("     , ASE.StuEnrollId " + vbCrLf)
			.Append("     , AST.SSN " + vbCrLf)
			.Append("     , AST.StudentNumber " + vbCrLf)
			.Append("     , AST.LastName " + vbCrLf)
			.Append("     , AST.FirstName " + vbCrLf)
			.Append("     , AST.MiddleName " + vbCrLf)
			.Append("     , FSPP.PayPlanDescrip " + vbCrLf)
'	        .Append("     --, FSPP.PaymentPlanId " + vbCrLf)
'	        .Append("     --, SPDR2.PayPlanScheduleId " + vbCrLf)
			.Append("FROM faStudentPaymentPlans AS FSPP " + vbCrLf)
			.Append("    INNER JOIN faStuPaymentPlanSchedule AS FSPPS ON FSPPS.PaymentPlanId = FSPP.PaymentPlanId " + vbCrLf)
			.Append("	 INNER JOIN arStuEnrollments AS ASE ON ASE.StuEnrollId = FSPP.StuEnrollId " + vbCrLf)
			.Append("    INNER JOIN arStudent AS AST ON AST.StudentId = ASE.StudentId " + vbCrLf)
			.Append("    INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId " + vbCrLf)
			.Append("    INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId " + vbCrLf)
			.Append("    LEFT JOIN ( " + vbCrLf)
            .Append("                SELECT CASE WHEN amountsPaid.AmountPaid < 0 THEN amountsPaid.AmountPaid * -1 ELSE amountsPaid.AmountPaid END AS AmountPaid, amountsPaid.PayPlanScheduleId FROM ( " + vbCrLf)
			.Append("                SELECT SUM(ISNULL(SPDR.Amount, 0)) AS AmountPaid " + vbCrLf)
			.Append("                     , SPDR.PayPlanScheduleId " + vbCrLf)
			.Append("                FROM saPmtDisbRel AS SPDR " + vbCrLf)
			.Append("                    INNER JOIN saTransactions AS ST ON ST.TransactionId = SPDR.TransactionId " + vbCrLf)
			.Append("                    INNER JOIN faStuPaymentPlanSchedule AS FSPPS2 ON FSPPS2.PayPlanScheduleId = SPDR.PayPlanScheduleId " + vbCrLf)
            .Append("                WHERE ST.Voided = 0 " + vbCrLf)
			.Append("                GROUP BY SPDR.PayPlanScheduleId " + vbCrLf)
            .Append("                    ) AS amountsPaid " + vbCrLf)
			.Append("              ) AS SPDR2 ON SPDR2.PayPlanScheduleId = FSPPS.PayPlanScheduleId " + vbCrLf)
			.Append("    LEFT JOIN ( " + vbCrLf)
			.Append("                SELECT T.StudentId " + vbCrLf)
			.Append("                     , T.StdAddressId " + vbCrLf)
			.Append("                     , T.Address " + vbCrLf)
			.Append("                     , T.City " + vbCrLf)
			.Append("                     , T.Zip " + vbCrLf)
			.Append("                     , T.StateDescrip " + vbCrLf)
			.Append("                FROM ( " + vbCrLf)
			.Append("                      SELECT ASA.StudentId " + vbCrLf)
			.Append("                           , ASA.StdAddressId " + vbCrLf)
			.Append("                           , (Address1 + ' ' + ISNULL(Address2, '')) AS Address " + vbCrLf)
			.Append("                           , ASA.City " + vbCrLf)
			.Append("                           , ASA.Zip " + vbCrLf)
			.Append("                           , ISNULL(SS2.StateDescrip, '') AS StateDescrip " + vbCrLf)
			.Append("                           , ROW_NUMBER() OVER ( PARTITION BY ASA.StudentId ORDER BY ASA.default1 DESC ) AS RowNumber " + vbCrLf)
			.Append("                      FROM arStudAddresses AS ASA " + vbCrLf)
			.Append("                          INNER JOIN syStatuses AS SS ON SS.StatusId = ASA.StatusId " + vbCrLf)
			.Append("                          LEFT JOIN syStates AS SS2 ON SS2.StateId = ASA.StateId " + vbCrLf)
			.Append("                      WHERE SS.StatusCode = 'A' " + vbCrLf)
			.Append("                     ) AS T " + vbCrLf)
			.Append("                WHERE T.RowNumber = 1 " + vbCrLf)
			.Append("              ) AS ASA2 ON ASA2.StudentId = AST.StudentId " + vbCrLf)
			.Append("WHERE FSPPS.ExpectedDate <= ?  -- RefDate  " + vbCrLf)
			.Append("  AND ASE.CampusId = ?        -- CampusId " + vbCrLf)
			.Append("  AND ASE.StuEnrollId IN (SELECT Val FROM [MultipleValuesForReportParameters](?, ',', 1))   -- StuEnrollIdList  " + vbCrLf)
			.Append("  AND (ISNULL(FSPPS.Amount, 0) - ISNULL(SPDR2.AmountPaid, 0)) > 0.00 " + vbCrLf)
'	        .Append("--  AND	FSPP.StuEnrollId ?    -- StuEnrollId " + vbCrLf)
			.Append("ORDER BY AST.LastName " + vbCrLf)
			.Append("       , AST.FirstName " + vbCrLf)
			.Append("       , AST.MiddleName " + vbCrLf)
			.Append("       , AST.SSN " + vbCrLf)
			.Append("       , APV.PrgVerDescrip " + vbCrLf)
			.Append("       , FSPPS.ExpectedDate; " + vbCrLf)
		End With
		db.AddParameter("@StartDate", Convert.ToDateTime(RefDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
		db.AddParameter("@CampusId", Guid.Parse(CampusID), DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)	
		db.AddParameter("@StuEnrollIdList", StuEnrollIdList, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

		db.CloseConnection()

        Return ds

    End Function

End Class
