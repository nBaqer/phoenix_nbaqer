Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' PrgVerDocsDB.vb
'
' PrgVerDocsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class PrgVerDocsDB
    'Public Function GetAllPrgVerDocsDS() As DataSet

    '    '   create dataset
    '    Dim ds As New DataSet

    '    '   build select query for the ProgramVersion data adapter
    '    '   connect to the database
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '    '   build the sql query
    '    With sb
    '        .Append("SELECT ")
    '        .Append("       PV.PrgVerId, ")
    '        .Append("       (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
    '        .Append("       ((Case ST.Status when 'Active' then 1 else 0 end) * 2 + (Select case count(*) when 0 then 0 else 1 end from adPrgVerDocs where PrgVerId=PV.PrgVerId )) As ExtendedStatus, ")
    '        .Append("       PV.PrgVerDescrip ")
    '        .Append("FROM ")
    '        .Append("       arPrgVersions PV, syStatuses ST ")
    '        .Append("WHERE ")
    '        .Append("       PV.StatusId=ST.StatusId ")
    '        .Append("ORDER BY PrgVerDescrip ")
    '    End With

    '    '   build select command
    '    Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

    '    '   Create adapter to handle PrgVersions table
    '    Dim da As New OleDbDataAdapter(sc)

    '    '   Fill PrgVersions table
    '    da.Fill(ds, "PrgVersions")

    '    '   build select query for the Documents data adapter
    '    sb = New StringBuilder
    '    With sb
    '        '.Append("SELECT ")
    '        '.Append("       D.DocumentId, ")
    '        '.Append("       (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
    '        '.Append("       D.DocumentDescrip ")
    '        '.Append("FROM ")
    '        '.Append("       cmDocuments D, syStatuses ST ")
    '        '.Append("WHERE ")
    '        '.Append("       D.StatusId=ST.StatusId ")
    '        '.Append("ORDER BY D.DocumentDescrip ")

    '        .Append("SELECT ")
    '        .Append("       D.adReqId as DocumentId, ")
    '        .Append("       (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
    '        .Append("       D.Descrip as DocumentDescrip ")
    '        .Append("FROM ")
    '        .Append("       adReqs D, syStatuses ST ")
    '        .Append("WHERE ")
    '        .Append("       D.StatusId=ST.StatusId and D.adReqTypeId=3")
    '        .Append("ORDER BY D.Descrip ")
    '    End With

    '    '   modify select command
    '    da.SelectCommand.CommandText = sb.ToString

    '    '   fill Documents table
    '    da.Fill(ds, "Documents")

    '    '   build select query for the PrgVerDocs data adapter
    '    sb = New StringBuilder
    '    With sb
    '        .Append("SELECT ")
    '        .Append("       PVD.PrgVerDocId, ")
    '        .Append("       PVD.PrgVerId, ")
    '        .Append("       PVD.DocumentId, ")
    '        .Append("       PVD.Required, ")
    '        .Append("       PVD.ViewOrder, ")
    '        .Append("       PVD.ModUser, ")
    '        .Append("       PVD.ModDate ")
    '        .Append("FROM ")
    '        .Append("       adPrgVerDocs PVD ")
    '    End With

    '    '   modify select command
    '    da.SelectCommand.CommandText = sb.ToString

    '    '   fill PrgVerDocs table
    '    da.Fill(ds, "PrgVerDocs")

    '    '   create primary and foreign key constraints

    '    '   set primary key for PrgVersions table
    '    Dim pk0(0) As DataColumn
    '    pk0(0) = ds.Tables("PrgVersions").Columns("PrgVerId")
    '    ds.Tables("PrgVersions").PrimaryKey = pk0

    '    '   set primary key for Documents table
    '    Dim pk1(0) As DataColumn
    '    pk1(0) = ds.Tables("Documents").Columns("DocumentId")
    '    ds.Tables("Documents").PrimaryKey = pk1

    '    '   set foreign key column in PrgVerDocs table
    '    Dim fk0(0) As DataColumn
    '    fk0(0) = ds.Tables("PrgVerDocs").Columns("PrgVerId")

    '    '   set foreign key column in PrgVerDocs table
    '    Dim fk1(0) As DataColumn
    '    fk1(0) = ds.Tables("PrgVerDocs").Columns("DocumentId")

    '    '   set foreign keys in PrgVerDocs table
    '    ds.Relations.Add("PrgVersionsPrgVerDocs", pk0, fk0)
    '    ds.Relations.Add("DocumentsPrgVerDocs", pk1, fk1)

    '    '   return dataset
    '    Return ds

    'End Function
    'Public Function UpdatePrgVerDocsDS(ByVal ds As DataSet) As String

    '    '   all updates must be encapsulated as one transaction

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
    '    connection.Open()
    '    Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

    '    Try
    '        '   build the sql query for the PrgVerDocs data adapter
    '        Dim sb As New StringBuilder
    '        With sb
    '            .Append("SELECT ")
    '            .Append("       PVD.PrgVerDocId, ")
    '            .Append("       PVD.PrgVerId, ")
    '            .Append("       PVD.DocumentId, ")
    '            .Append("       PVD.Required, ")
    '            .Append("       PVD.ViewOrder, ")
    '            .Append("       PVD.ModUser, ")
    '            .Append("       PVD.ModDate ")
    '            .Append("FROM ")
    '            .Append("       adPrgVerDocs PVD ")
    '        End With

    '        '   build select command
    '        Dim PrgVerDocsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

    '        '   Create adapter to handle PrgVerDocDetails table
    '        Dim PrgVerDocsDataAdapter As New OleDbDataAdapter(PrgVerDocsSelectCommand)

    '        '   build insert, update and delete commands for PrgVerDocDetails table
    '        Dim cb As New OleDbCommandBuilder(PrgVerDocsDataAdapter)

    '        '   insert added rows in PrgVerDocs table
    '        PrgVerDocsDataAdapter.Update(ds.Tables("PrgVerDocs").Select(Nothing, Nothing, DataViewRowState.Added))

    '        '   delete rows in PrgVerDocs table
    '        PrgVerDocsDataAdapter.Update(ds.Tables("PrgVerDocs").Select(Nothing, Nothing, DataViewRowState.Deleted))

    '        '   update rows in PrgVerDocs table
    '        PrgVerDocsDataAdapter.Update(ds.Tables("PrgVerDocs").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

    '        '   everything went fine - commit transaction
    '        groupTrans.Commit()

    '        '   return no errors
    '        Return ""

    '    Catch ex As OleDb.OleDbException
    '        '   something went wrong
    '        '   rollback transaction
    '        groupTrans.Rollback()

    '        '   return error message
    '        Return DALExceptions.BuildErrorMessage(ex)
    '    Finally

    '        '   close connection
    '        connection.Close()
    '    End Try

    'End Function
End Class
