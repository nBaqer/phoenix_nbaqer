Imports FAME.Advantage.Common

Public Class EmployerTestDB

    Public Function GetEmployerListing(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT EmployerDescrip ")
            .Append("FROM plEmployers")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

End Class
