Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' SupervisorsDB.vb
'
' Data Access Logic for Academic Advisors and Financial Aid Advisors. 
'
' This class will be used to retrieve/update data related to Academic Advisors and Financial Aid Advisor.
' This is done because the logic is exactly the same. The difference is only on the SysRoleId used.
' SysRoleId for Academic Advisor is 4. 
' SysRoleId for Financial Aid Advisor is 7. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class SupervisorsDB
    '
    '   SupervisorsDB Class
    '

    Public Function GetSupervisorsPerCampus(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       U.UserId as SupervisorId, ")
            .Append("       U.FullName as SupervisorName ")
            .Append("FROM ")
            .Append("       syUsersRolesCampGrps URCG, syRoles R, syUsers U ")
            .Append("WHERE ")
            .Append("       R.RoleId=URCG.RoleId ")
            .Append("AND	R.SysRoleId=11 ")
            .Append("AND	URCG.UserId=U.UserId ")
            .Append("AND	CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
            .Append("ORDER BY U.FullName ")
        End With

        ' Add the CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    Public Function GetInstructorsAssignedToASpecificSupervisor(ByVal supervisorId As String, ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("		U.UserId as InstructorId, ")
            .Append("		U.FullName as InstructorName ")
            .Append("FROM	arInstructorsSupervisors [IS], syUsers U ")
            .Append("WHERE ")
            .Append("		[IS].InstructorId=U.UserId ")
            .Append("AND	[IS].SupervisorId=? ")
            .Append("ORDER BY ")
            .Append("       InstructorName ")
        End With

        ' Add the SupervisorId to the parameter list
        db.AddParameter("@Supervisor", supervisorId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    Public Function GetInstructorsNotAssignedToAnySupervisor(ByVal campusId As String, ByVal supervisorId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT  DISTINCT ")
            .Append("		U.UserId as InstructorId, ")
            .Append("		U.FullName as InstructorName  ")
            .Append("FROM	syUsersRolesCampGrps URC, syRoles R, syUsers U ")
            .Append("WHERE ")
            .Append("		URC.RoleId=R.RoleId ")
            .Append("AND		R.SysRoleId=2 ")
            .Append("AND		URC.UserId=U.UserId ")
            .Append("AND        U.Userid <> ? ")
            .Append("AND		U.CampusId= ? ")
            .Append("AND		NOT EXISTS (Select * from arInstructorsSupervisors where InstructorId=U.UserId) ")
            .Append(" and U.UserName not in ('Super','Support') " + vbCrLf)
            .Append(" ORDER BY ")
            .Append("		InstructorName ")
        End With
        ' Add the SupervisorId to the parameter list
        db.AddParameter("@Supervisor", supervisorId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add the CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    Public Function UpdateListOfInstructorsAssignedToAnSupervisor(ByVal campusId As String, ByVal supervisorId As String, ByVal user As String, ByVal selectedInstructors() As String) As String
        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            '   First we have to delete all existing selections in the arInstructorsSupervisors table
            '   build the query
            Dim sb As New System.Text.StringBuilder
            With sb
                .Append("DELETE FROM arInstructorsSupervisors WHERE SupervisorId = ? ")
            End With

            '   Add parameter Supervisor
            db.AddParameter("@Supervisor", supervisorId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            '   Insert one record per each Item in the Selected Group
            Dim now As DateTime = Date.Now
            For i As Integer = 0 To selectedInstructors.Length - 1

                '   build the query
                With sb
                    .Append("Insert arInstructorsSupervisors ")
                    .Append("       (InstructorId, SupervisorId, ModUser, ModDate) ")
                    .Append("       VALUES ")
                    .Append("       (?,?,?,?) ")
                End With

                '   add parameters values to the query
                db.AddParameter("@InstructorId", DirectCast(selectedInstructors.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@SupervisorId", supervisorId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Next

            '   commit transaction 
            groupTrans.Commit()

            '   return without errors
            Return ""

        Catch ex As OleDbException
            groupTrans.Rollback()
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
                Return ex.Message

            End If
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
End Class


