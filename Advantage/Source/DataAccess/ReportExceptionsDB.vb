Imports FAME.Advantage.Common

Public Class ReportExceptionsDB

    Public Sub RefreshLeadStatus(ByVal CampusId As String)
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim sb2 As New StringBuilder

        '   delete all records
        With sb
            .Append("delete from rptLeadStatus where CampusId=? ")
        End With
        db.AddParameter("@campusId", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()

        '   insert all Active Lead status into rptLeadStatus
        With sb2
            .Append("INSERT INTO rptLeadStatus(StatusCodeId, StatusCodeDescrip, StatusId,CampusId) ")
            .Append("   SELECT DISTINCT A.StatusCodeId AS StatusCodeId, ")
            .Append("   A.StatusCodeDescrip AS StatusCodeDescrip, C.StatusID AS StatusId,?  ")
            .Append("   FROM syStatusCodes A, sySysStatus B, syStatuses C,syStatusLevels D ")
            .Append("   WHERE A.sysStatusID = B.sysStatusID AND ")
            .Append("   A.StatusID = C.StatusID AND B.StatusLevelId = D.StatusLevelId and ")
            .Append("   C.Status = 'Active' AND D.StatusLevelId = 1 ")
            .Append("   ORDER BY A.StatusCodeDescrip")
        End With
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb2.ToString)
    End Sub

    Public Sub RefreshAdmissionsRep(ByVal CampusId As String)
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim sb2 As New StringBuilder

        '   delete all records from campusId on rptAdmissionsRep table
        With sb
            .Append("DELETE FROM rptAdmissionsRep WHERE CampusId = ? ")
        End With
        db.AddParameter("@campusId", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()


        '   insert all users from campusId with role = 'Admissions Reps' into rptAdmissionsRep
        With sb2
            .Append("INSERT INTO rptAdmissionsRep(AdmissionsRepId,AdmissionsRepDescrip,StatusId,CampusId) ")
            .Append("   SELECT DISTINCT ")
            .Append("           s1.UserId AS AdmissionsRepId,s1.Fullname AS AdmissionsRepDescrip,")
            .Append("           s6.StatusId,s1.CampusId ")
            .Append("   FROM    syUsers s1,syUsersRolesCampGrps s2,syRoles s3,sySysRoles s4,syStatuses s6 ")
            .Append("   WHERE   s1.UserId = s2.UserId And s2.RoleId = s3.RoleId ")
            .Append("           AND s3.SysRoleId = s4.SysRoleId AND s4.StatusId = s6.StatusId ")
            .Append("           AND s4.Descrip = 'Admission Reps' AND s6.Status = 'Active' ")
            '.Append("           AND s1.CampusId = ? ")
            .Append("   ORDER BY s1.Fullname ")

            ''.Append("   SELECT DISTINCT s1.UserId AS AdmissionsRepId,s1.Fullname AS AdmissionsRepDescrip,s6.StatusId,? ")
            ''.Append("   FROM syUsers s1,syUsersRolesCampGrps s2,syRoles s3,sySysRoles s4,syStatuses s6 ")
            ''.Append("   WHERE s1.UserId = s2.UserId AND s2.RoleId = s3.RoleId ")
            ''.Append("   AND s3.SysRoleId = s4.SysRoleId AND s4.StatusId = s6.StatusId ")
            ''.Append("   AND s4.SysRoleId = 3 AND s6.Status='Active' ")
            ''.Append("   ORDER BY s1.Fullname")

            '.Append("   SELECT hrEmployees.EmpID AS AdmissionsRepId, ")
            '.Append("   (hrEmployees.LastName + ', ' + hrEmployees.FirstName + ' ' +  ")
            '.Append("   hrEmployees.MI) AS AdmissionsRepDescrip ")
            '.Append("   FROM syRoles, hrEmpRoles, hrEmployees")
            '.Append("   WHERE syRoles.RoleId = hrEmpRoles.RoleId AND ")
            '.Append("   hrEmpRoles.EmpId = hrEmployees.EmpID")
            '.Append("   AND syRoles.Role = 'Admissions Reps'")
            '.Append("   ORDER BY hrEmployees.LastName")
        End With

        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb2.ToString)
    End Sub

    Public Sub RefreshInstructor()
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim sb2 As New StringBuilder

        '   delete all records in rptInstructor
        With sb
            .Append("TRUNCATE TABLE rptInstructor")
        End With
        db.RunParamSQLExecuteNoneQuery(sb.ToString)

        '   insert all employees with role = 'Instructor' into rptInstructors
        With sb2
            .Append("INSERT INTO rptInstructor(InstructorId, InstructorDescrip) ")
            .Append("   SELECT DISTINCT s1.UserId AS InstructorId,s1.Fullname AS InstructorDescrip ")
            .Append("   FROM syUsers s1,syUsersRolesCampGrps s2,syRoles s3,sySysRoles s4,syStatuses s6 ")
            .Append("   WHERE s1.UserId = s2.UserId AND s2.RoleId = s3.RoleId ")
            .Append("   AND s3.SysRoleId = s4.SysRoleId AND s4.StatusId = s6.StatusId ")
            .Append("   AND s4.SysRoleId = 2 AND s6.Status='Active' ")
            .Append("   ORDER BY s1.Fullname")

            '.Append("INSERT INTO rptInstructor(InstructorId, InstructorDescrip) ")
            '.Append("   SELECT hrEmployees.EmpID AS InstructorId, ")
            '.Append("   (hrEmployees.LastName + ', ' + hrEmployees.FirstName + ' ' +  ")
            '.Append("   hrEmployees.MI) AS InstructorDescrip ")
            '.Append("   FROM syRoles, hrEmpRoles, hrEmployees")
            '.Append("   WHERE syRoles.RoleId = hrEmpRoles.RoleId AND ")
            '.Append("   hrEmpRoles.EmpId = hrEmployees.EmpID")
            '.Append("   AND syRoles.Role = 'Instructors'")
            '.Append("   ORDER BY hrEmployees.LastName")
        End With
        db.RunParamSQLExecuteNoneQuery(sb2.ToString)
    End Sub

    Public Sub RefreshStuEnrollmentStatus()
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim sb2 As New StringBuilder

        '   delete all records
        With sb
            .Append("TRUNCATE TABLE rptStuEnrollmentStatus")
        End With
        db.RunParamSQLExecuteNoneQuery(sb.ToString)

        '   insert all Active StuEnrollment status into rptStuEnrollmentStatus
        With sb2
            .Append("INSERT INTO rptStuEnrollmentStatus(StatusCodeId, StatusCodeDescrip) ")
            .Append("   SELECT D.StatusCodeId, D.StatusCodeDescrip ")
            .Append("   FROM sySysStatus A, syStatuses B, syStatusLevels C,syStatusCodes D ")
            .Append("   WHERE A.StatusId = B.StatusId AND B.Status = 'Active' ")
            .Append("   AND A.StatusLevelId = C.StatusLevelId AND C.StatusLevelId = 2 ")
            .Append("   AND D.SysStatusId = A.SysStatusId ")
            .Append("   ORDER BY D.StatusCodeDescrip")
        End With
        db.RunParamSQLExecuteNoneQuery(sb2.ToString)
    End Sub

End Class
