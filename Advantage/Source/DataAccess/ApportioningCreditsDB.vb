﻿Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class ApportioningCreditsDB

    Public Shared Function GetFASSAP(ByVal CampusGroupId As Guid, ByVal ProgramVersionId As Guid, ByVal EnrollmentStatusId As Guid, ByVal AsOfDate As DateTime) As DataSet

        Dim ds As DataSet
        Dim db As New SQLDataAccess
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.AddParameter("@CampGrpId", CampusGroupId, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@PrgVerid", ProgramVersionId, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@StatusCodeId", EnrollmentStatusId, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@StartDate", AsOfDate, SqlDbType.DateTime, , ParameterDirection.Input)

        '   return dataset
        Try
            ds = db.RunParamSQLDataSet_SP("USP_FASAPChkResults_getList")
            Return ds
        Catch ex As Exception
            Dim x As String = ex.ToString
        End Try

    End Function



    Public Shared Function GetApportioningCreditsCount(ByVal CampusGroupId As Guid, ByVal ProgramVersionId As Guid, ByVal EnrollmentStatusId As Guid, ByVal AsOfDate As DateTime) As DataSet

        Dim ds As DataSet
        Dim db As New SQLDataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@campgrpid", CampusGroupId, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@prgverid", ProgramVersionId, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@enrollmentstatus", EnrollmentStatusId, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@asofdate", AsOfDate, SqlDbType.DateTime, , ParameterDirection.Input)

        '   return dataset
        Try
            ds = db.RunParamSQLDataSet_SP("USP_SA_GetApportionedCredits")
            Return ds
        Catch ex As Exception
            Dim x As String = ex.ToString
        End Try

    End Function

    Public Shared Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
   
End Class
