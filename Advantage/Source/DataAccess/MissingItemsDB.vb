Imports FAME.Advantage.Common

Public Class MissingItemsDB
    Public Function GetStudentList(ByVal ProgId As String, _
                                   ByVal StudentGroup As String, _
                                   Optional ByVal StatusCodeId As String = "", _
                                   Optional ByVal ShiftId As String = "", _
                                   Optional ByVal CampusId As String = "", _
                                   Optional ByVal StudentId As String = "", Optional ByVal reqType As String = "") As DataTable

        Dim sbStudentList As New StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim dsStudentsWithMissingReqsandReqGroup As New DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'dsStudentsWithMissingReqsandReqGroup = GetStudentsWithMissingRequirementsAndRequirementGroup(CampusId)
        'Dim strStudentId As String
        ''Get Students missing requirements and requirement groups and pass it as a filter
        ''for the student list
        'If dsStudentsWithMissingReqsandReqGroup.Rows.Count >= 1 Then
        '    For Each row As DataRow In dsStudentsWithMissingReqsandReqGroup.Rows
        '        strStudentId &= row("StudentId").ToString & "','"
        '    Next
        '    strStudentId = Mid(strStudentId, 1, InStrRev(strStudentId, "'") - 2)
        'End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sbStudentList
            .Append(" Select Distinct S.StudentId,S.LastName,S.FirstName, " & vbCrLf)
            .Append(" S.SSN,SE.PrgVerId,SE.StatusCodeId,PV.PrgVerDescrip,		SC.StatusCodeDescrip, " & vbCrLf)
            .Append(" (select Top 1 ShiftDescrip from arShifts where ShiftId=SE.ShiftId) as ShiftDescrip, " & vbCrLf)
            .Append(" (select Top 1 EdLvlDescrip from adEdLvls where EdLvlId=SE.EdLvlId) as PreviousEducation " & vbCrLf)
            .Append(" from arStudent S,arStuEnrollments SE,adLeadByLeadGroups LBLG,syStatusCodes SC, " & vbCrLf)
            .Append(" arPrgVersions PV,arPrograms P" & vbCrLf)
            .Append(" where S.StudentId = SE.StudentId and SE.StatusCodeId = SC.StatusCodeId and " & vbCrLf)
            .Append(" SE.StuEnrollId=LBLG.StuEnrollId and P.ProgId=PV.ProgId and " & vbCrLf)
            .Append(" PV.PrgVerId = SE.PrgVerId " & vbCrLf)
            If Not ProgId = "" Then
                .Append(" and P.ProgId = '" & ProgId & "' " & vbCrLf)
            End If
            If Not StudentGroup = "" Then
                .Append(" and LBLG.LeadGrpId = '" & StudentGroup & "' " & vbCrLf)
            End If
            If Not StatusCodeId = "" Then
                .Append(" and SE.StatusCodeId = '" & StatusCodeId & "' " & vbCrLf)
            End If
            If Not ShiftId = "" Then
                .Append(" and SE.ShiftId = '" & ShiftId & "' " & vbCrLf)
            End If
            If Not CampusId = "" Then
                .Append(" and SE.CampusId = '" & CampusId & "' " & vbCrLf)
            End If
            .Append(" and S.StudentID in ('" & StudentId & ")" & vbCrLf)
            '.Append("  and S.StudentId in 	('" & vbCrLf)
            '.Append(StudentId & vbCrLf)
            '.Append(") " & vbCrLf)
            .Append(" Order By S.FirstName " & vbCrLf)
        End With
        ds = db.RunParamSQLDataSet(sbStudentList.ToString)
        Return ds.Tables(0)
    End Function
    ''modified by saraswathi Lakshmanan to fix rally case DE5003 Name: QA: Requirement type should show only the requirements tied to the requirement type and not the other missing items.
    ''feb 4 2011
    Public Function GetRequirements(ByVal StudentId As String, Optional ByVal campusId As String = "", Optional ByVal ProgId As String = "", Optional ByVal ShiftId As String = "", Optional ByVal EnrollmentStatus As String = "", Optional ByVal reqtype As String = "") As DataTable
        Dim sbRequirements As New StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        Dim dsGetCmpGrps As New DataSet
        Dim dstGetDocumentStatus As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        Dim strCampGrpId As String
        Dim strDocumentStatus As String


        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusId)

        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        dstGetDocumentStatus = GetDocumentStatus(campusId)
        If dstGetDocumentStatus.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dstGetDocumentStatus.Tables(0).Rows
                strDocumentStatus &= row("DocStatusId").ToString & "','"
            Next
            strDocumentStatus = Mid(strDocumentStatus, 1, InStrRev(strDocumentStatus, "'") - 2)
        End If



        Dim strreqType As String = ""
        If reqtype = "Enrollment" Then
            strreqType = " and ReqforEnrollment=1  "
        ElseIf reqtype = "Financial Aid" Then
            strreqType = " and ReqforFinancialAid=1  "
        ElseIf reqtype = "Graduation" Then
            strreqType = " and ReqforGraduation=1  "
        ElseIf reqtype = "Generic Requirements" Then
            strreqType = " and ReqforEnrollment=0 and ReqforFinancialAid=0  and ReqforGraduation=0    "
        End If


        With sbRequirements
            'Mandatory Documents not approved
            .Append(" select distinct Descrip,StudentId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3)" & strreqType & "  and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement=1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("       (select Distinct DocumentId,DocStatusId,StudentId from plStudentDocs where StudentId='" & StudentId & "' ")
            If Not strDocumentStatus = "" Then
                .Append(" and DocStatusId in ('")
                .Append(strDocumentStatus)
                .Append(") ")
            End If
            .Append(" ) t3 ")
            .Append("   where t1.adReqId = t2.adReqId And t1.adReqId = t3.DocumentId ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append("   union ")
            ' Mandatory Test not passed
            .Append(" select distinct Descrip,StudentId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3)" & strreqType & "  and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement=1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("		(select EntrTestId,StudentId from adLeadEntranceTest where  StudentId='" & StudentId & "' and Pass=0) t4 ")
            .Append(" where t1.adReqId = t2.adReqId And t1.adReqId = t4.EntrTestId  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" Union ")
            ' Mandatory Test overridden
            .Append(" select distinct Descrip,StudentId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3)" & strreqType & "  and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement=1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("		(select Distinct EntrTestId,StudentId from adEntrTestOverride where Override=1 and  StudentId='" & StudentId & "' ) t4 ")
            .Append(" where t1.adReqId = t2.adReqId And t1.adReqId = t4.EntrTestId  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" Union ")
            ' Get Documents assigned directly to program version
            .Append(" select distinct Descrip,StudentId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3)" & strreqType & "  and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("   	(select Distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "' and ReqGrpId is null ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId='" & EnrollmentStatus & "' ")
            End If
            .Append("       ) t5, ")
            .Append("       (select Distinct DocumentId,DocStatusId,StudentId from plStudentDocs where StudentId='" & StudentId & "' ")
            If Not strDocumentStatus = "" Then
                .Append("  and DocStatusId in ('")
                .Append(strDocumentStatus)
                .Append(") ")
            End If
            .Append(" ) t6 ")
            .Append(" where ")
            .Append("	t1.adReqId = t2.adReqId and t1.adReqId=t5.adReqId and  ")
            .Append("   t1.adReqId = t6.DocumentId  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" Union ")
            ' Get Test assigned directly to program version
            .Append(" select distinct Descrip,StudentId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3)" & strreqType & "  and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("   	(select Distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "' and ReqGrpId is null ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" ) t5, ")
            .Append("       (select Distinct EntrTestId,StudentId from adLeadEntranceTest where StudentId='" & StudentId & "' and Pass=0) t7 ")
            .Append(" where t1.adReqId = t2.adReqId and t1.adReqId=t5.adReqId and t1.adReqId = t7.EntrTestId  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" Union ")
            .Append(" select distinct Descrip,StudentId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3)" & strreqType & "  and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("   	(select Distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "' and ReqGrpId is null ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" ) t5, ")
            .Append("       (select Distinct EntrTestId,StudentId from adEntrTestOverride where Override=1 and StudentId='" & StudentId & "') t7 ")
            .Append(" where t1.adReqId = t2.adReqId and t1.adReqId=t5.adReqId and t1.adReqId = t7.EntrTestId  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" Union ")
            ' Get docs assigned directly to lead group
            .Append(" select distinct Descrip,StudentId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3)" & strreqType & "  and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId  from adReqsEffectiveDates where getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append(" adReqLeadGroups t3, ")
            .Append("       (select Distinct DocumentId,DocStatusId,StudentId from plStudentDocs where StudentId='" & StudentId & "' ")
            If Not strDocumentStatus = "" Then
                .Append("  and DocStatusId in ('")
                .Append(strDocumentStatus)
                .Append(") ")
            End If
            .Append(" ) t4 ")
            .Append("   where ")
            .Append("       t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and  ")
            ' Requirement should not be in requirement group assigned to program version
            .Append(" t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2,arStuEnrollments s3,arStudent s4 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId=s3.PrgVerId and s3.StudentId=s4.StudentId and s4.StudentId='" & StudentId & "' and s2.adReqId is null   ")
            If Not ProgId = "" Then
                .Append(" and s3.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and s3.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and s3.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" ) ")
            .Append(" and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "' ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" ) and ")
            .Append(" t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "' and ReqGrpId is  null ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" )  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" and t3.IsRequired = 1 and ")
            .Append(" t1.adReqId = t4.DocumentId ")
            .Append("  Union ")
            'Test Requirements assigned directly to lead group that are marked as required for the lead group
            ' and the requirements should either be failed or not approved
            .Append(" select distinct Descrip,StudentId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3)" & strreqType & "  and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId  from adReqsEffectiveDates where getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("       adReqLeadGroups t3, ")
            .Append("       (select EntrTestId,StudentId from adLeadEntranceTest where StudentId='" & StudentId & "' and Pass=0) t5 ")
            .Append("       where t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            ' Requirement should not be in requirement group assigned to program version
            .Append("       t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2,arStuEnrollments s3,arStudent s4 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId=s3.PrgVerId and s3.StudentId=s4.StudentId and s4.StudentId='" & StudentId & "' and s2.adReqId is null ")
            If Not ProgId = "" Then
                .Append(" and s3.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and s3.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and s3.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" )    ")
            .Append("       and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "') and  ")
            ' Requirement should not be directly assigned to program version
            .Append("       t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "' and ReqGrpId is null ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" )  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append("       and t3.IsRequired = 1 and ")
            .Append("       t1.adReqId = t5.EntrTestId ")
            .Append("  Union ")
            'Test Requirements assigned directly to lead group that are marked as required for the lead group
            ' and the requirements should either be failed or not approved
            .Append(" select distinct Descrip,StudentId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3)" & strreqType & "  and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId  from adReqsEffectiveDates where getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("       adReqLeadGroups t3, ")
            .Append("       (select Distinct EntrTestId,StudentId from adEntrTestOverride where Override=1 and StudentId='" & StudentId & "') t5 ")
            .Append("       where t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            ' Requirement should not be in requirement group assigned to program version
            .Append("       t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2,arStuEnrollments s3,arStudent s4 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId=s3.PrgVerId and s3.StudentId=s4.StudentId and s4.StudentId='" & StudentId & "' and s2.adReqId is null ")
            If Not ProgId = "" Then
                .Append(" and s3.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and s3.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and s3.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append("           )    ")
            .Append("       and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "'  ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" ) and ")
            ' Requirement should not be directly assigned to program version
            .Append("       t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "' and ReqGrpId is null ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" )  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append("       and t3.IsRequired = 1 and ")
            .Append("       t1.adReqId = t5.EntrTestId ")
        End With
        '   return dataset
        Return db.RunParamSQLDataSet(sbRequirements.ToString).Tables(0)
    End Function
    Public Function GetDocumentStatus(ByVal campusId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid

        '   build the sql query
        With sb
            .Append(" select Distinct DocStatusId from syDocStatuses t2,sySysDocStatuses t3,syCmpGrpCmps t4 ")
            .Append(" where t2.SysDocStatusId=t3.SysDocStatusId and t2.CampGrpId = t4.CampGrpId and t4.CampusId=? and ")
            .Append(" t3.SysDocStatusId = 2 and t2.StatusId=? ")
        End With

        '   add CampusId parameter
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StatusId", strActiveGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetLeadGroupsByStudent(ByVal StudentId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid

        '   build the sql query
        With sb
            .Append(" select Distinct LeadGrpId from ")
            .Append(" adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 ")
            .Append(" where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId and t3.StudentId=? ")
        End With

        '   add CampusId parameter
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetLeadGroupsByStudent() As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid

        '   build the sql query
        With sb
            .Append(" select Distinct LeadGrpId from ")
            .Append(" adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 ")
            .Append(" where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId ")
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetLeadGroupsByStudentMissing(Optional ByVal StudentGroup As String = "") As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid

        '   build the sql query
        With sb
            .Append(" select Distinct LeadGrpId from ")
            .Append(" adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 ")
            .Append(" where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId ")
            If Not StudentGroup = "" Then
                .Append(" and t1.LeadGrpId='" & StudentGroup & "'")
            End If
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetLeadGroupsByStudentFilter(Optional ByVal filterlist As String = "") As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid

        '   build the sql query
        With sb
            .Append(" select Distinct LeadGrpId from ")
            .Append(" adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 ")
            .Append(" where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId ")
            If Not filterlist = "" Then
                .Append(" and ")
                .Append(filterlist)
            End If
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetOverridedTestByStudent(ByVal StudentId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid

        '   build the sql query
        With sb
            .Append("    select Distinct EntrTestId from adEntrTestOverRide t1,arStudent t2 ")
            .Append("    where t1.StudentId=t2.StudentId and OverRide=1 and t2.StudentId=? ")
        End With

        '   add CampusId parameter
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetOverridedTestByStudent() As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid

        '   build the sql query
        With sb
            .Append("    select Distinct EntrTestId from adEntrTestOverRide t1,arStudent t2 ")
            .Append("    where t1.StudentId=t2.StudentId and OverRide=1 ")
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetRequirementsGroupsByStudent(ByVal StudentId As String, Optional ByVal CampusId As String = "", Optional ByVal ProgId As String = "", Optional ByVal ShiftId As String = "", Optional ByVal EnrollmentStatus As String = "", Optional ByVal ReqTYpe As String = "") As DataTable
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim drowRootParent As DataRow
        Dim drowSchoolLevelSummary As DataRow
        Dim drowSchoolLevelDetails As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb, sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
        Dim drowRequirementAssignedDirectlyToPrgVersion, drowRequirementAssignedDirectlyToLeadGroup As DataRow
        Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder

        Dim dsGetEntranceTestId As New DataSet
        Dim dsGetLeadGrpId As New DataSet
        Dim dstGetRequirementsAssignedToProgramVersion As New DataSet
        Dim dstGetRequirementsDirectlyAssigned As New DataSet
        Dim dstGetLeadGroups As New DataSet
        Dim dsGetCmpGrps As New DataSet
        Dim intRowCounter As Integer
        Dim strLeadGrpId As String
        Dim dstOverrideTest As New DataSet
        Dim strEntrTestId As String
        Dim strCurrentDate As String = Date.Now.ToShortDateString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")



        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        dstGetLeadGroups = GetLeadGroupsByStudent(StudentId)
        If dstGetLeadGroups.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dstGetLeadGroups.Tables(0).Rows
                strLeadGrpId &= row("LeadGrpId").ToString & "','"
            Next
            strLeadGrpId = Mid(strLeadGrpId, 1, InStrRev(strLeadGrpId, "'") - 2)
        End If

        dstOverrideTest = GetOverridedTestByStudent(StudentId)
        If dstOverrideTest.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dstOverrideTest.Tables(0).Rows
                strEntrTestId &= row("EntrTestId").ToString & "','"
            Next
            strEntrTestId = Mid(strEntrTestId, 1, InStrRev(strEntrTestId, "'") - 2)
        End If


        Dim strreqType As String = ""
        If ReqTYpe = "Enrollment" Then
            strreqType = " and ReqforEnrollment=1  "
        ElseIf ReqTYpe = "Financial Aid" Then
            strreqType = " and ReqforFinancialAid=1  "
        ElseIf ReqTYpe = "Graduation" Then
            strreqType = " and ReqforGraduation=1  "
        ElseIf ReqTYpe = "Generic Requirements" Then
            strreqType = " and ReqforEnrollment=0 and ReqforFinancialAid=0  and ReqforGraduation=0    "
        End If


        'Query gets the requirement group summary assigned to LeadGroup and Program Version
        With sb
            .Append(" select ReqGrpId,Descrip  ")
            .Append(" from ")
            .Append(" (     ")
            'TestDocumentAttempted removed from the count query by Balaji on 4/23/2007
            .Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,IsNull(R5.TestPassed,0)+IsNULL(R5.DocsApproved,0) as AttemptedReqs,  ")
            .Append(" Case when LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            '*********************************************************************************************************************************************
            ' This Query gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted by Lead and Program Version 
            '*********************************************************************************************************************************************
            .Append(" (Select  Distinct t1.ReqGrpId,t2.Descrip,t2.CampGrpId,t3.NumReqs as Numreqs,  ")
            ''''''''''''' This Query gives the number of test passed by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId " & strreqType & "  ")
            If Not strLeadGrpId = "" Then
                .Append("  and A4.LeadGrpId in 	('")
                .Append(strLeadGrpId)
                .Append(") ")
            End If
            .Append("  and ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=1) ")
            .Append(" R1,adLeadEntranceTest R2,arStudent R3 WHERE ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.StudentId = R3.StudentId and R3.StudentId='" & StudentId & "' and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and R2.Pass=1  ")
            If Not strEntrTestId = "" Then
                .Append("  and R2.EntrTestId not in 	('")
                .Append(strEntrTestId)
                .Append(") ")
            End If
            .Append(" and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as TestPassed, ")
            ''''''''''''' This Query gives the number of documents approved by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from  ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId   " & strreqType & " ")
            If Not strLeadGrpId = "" Then
                .Append(" and A4.LeadGrpId in 	('")
                .Append(strLeadGrpId)
                .Append(") ")
            End If
            .Append(" and ")
            .Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=3) R1,plStudentDocs R2,syDocStatuses R3,arStudent R4 WHERE ")
            .Append(" R1.adReqId = R2.DocumentId and R2.StudentId=R4.StudentId and R4.StudentId='" & StudentId & "'  ")
            If Not strEntrTestId = "" Then
                .Append("  and R2.DocumentId not in 	('")
                .Append(strEntrTestId)
                .Append(") ")
            End If
            .Append(" and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as DocsApproved, ")
            .Append(" t4.LeadGrpId from adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3,adLeadGroups t4,arStuEnrollments t5,arStudent t6 ")
            .Append(",adreqs t7,adreqgrpdef t8 ")
            .Append(" where  ")
            .Append(" t1.ReqGrpId = t2.ReqGrpId and t2.ReqGrpId = t3.ReqGrpId and t3.LeadGrpId = t4.LeadGrpId and t1.PrgVerId=t5.PrgVerId and t5.StudentId=t6.StudentId and t6.StudentId='" & StudentId & "' ")
            .Append("      AND t8.ReqGrpId=t2.ReqGrpId  AND t8.adReqId= t7.adReqId  " & strreqType & "  ")

            If Not ProgId = "" Then
                .Append(" and t5.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t5.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t5.StatusCodeId='" & EnrollmentStatus & "' ")
            End If
            .Append(" and t2.IsMandatoryReqGrp <> 1 ")
            If Not strLeadGrpId = "" Then
                .Append(" and t3.LeadGrpId in 	('")
                .Append(strLeadGrpId)
                .Append(") ")
            End If
            .Append(" ) R5  ")
            'where LeadGrpId='" & LeadGrpId & "'
            If Not strCampGrpId = "" Then
                .Append("  where R5.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append("    ) R8 ")
            .Append("   where  ")
            .Append(" (AttemptedReqs < NumReqs ")
            .Append("  or  ")
            .Append(" (select Count(*) from adReqGrpDef where ReqGrpId=R8.ReqGrpId ")
            .Append(" and LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where StuEnrollId in ")
            .Append(" (select Distinct StuEnrollId from arStuEnrollments where StudentId='" & StudentId & "')) ")
            .Append("  and IsRequired=1 and  ")
            .Append(" ( ")
            ' Test should have been failed
            .Append(" adReqId in ")
            .Append(" (select distinct EntrTestId from adLeadEntranceTest where StudentId='" & StudentId & "' ")
            .Append(" and Pass=0 and EntrTestId not in (select EntrTestId from adEntrTestOverride where ")
            .Append(" StudentId='" & StudentId & "' and override=1)) ")
            .Append(" or  ")
            ' or document should be not approved
            .Append(" adReqId in ")
            .Append(" (select distinct DocumentId from plStudentDocs t1,syDocStatuses t2 where ")
            .Append(" t1.StudentId='" & StudentId & "'  and t1.DocStatusId = t2.DocStatusId and ")
            .Append(" t2.SysDocStatusId <> 1 and DocumentId not in ")
            .Append(" (select EntrTestId from adEntrTestOverride where StudentId='" & StudentId & "' ")
            .Append(" and override=1)) ")
            .Append(" or ")
            ' or requirement should be overriden
            .Append(" adReqId in ")
            .Append(" (select Distinct EntrTestId from adEntrTestOverride where StudentId='" & StudentId & "'  ")
            .Append(" and override=1))) >=1) ")
            '**************************************************************************************************************************************************************
            'End of the  Query that gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted by Lead and Program Version
            '**************************************************************************************************************************************************************
        End With
        dadNorthwind = New OleDbDataAdapter(sb.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "RequirementGroup")
        Return dstNorthwind.Tables(0)
    End Function
    Public Function GetRequirementsByReqGroupAndStudent(ByVal ReqGrpId As String, ByVal StudentId As String, Optional ByVal CampusId As String = "", Optional ByVal ReqTYpe As String = "") As DataTable
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim drowRootParent As DataRow
        Dim drowSchoolLevelSummary As DataRow
        Dim drowSchoolLevelDetails As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb, sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
        Dim drowRequirementAssignedDirectlyToPrgVersion, drowRequirementAssignedDirectlyToLeadGroup As DataRow
        Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder

        Dim dsGetEntranceTestId As New DataSet
        Dim dsGetLeadGrpId As New DataSet
        Dim dstGetRequirementsAssignedToProgramVersion As New DataSet
        Dim dstGetRequirementsDirectlyAssigned As New DataSet
        Dim dstGetLeadGroups As New DataSet
        Dim dsGetCmpGrps As New DataSet
        Dim intRowCounter As Integer
        Dim strLeadGrpId As String
        Dim dstOverrideTest As New DataSet
        Dim strEntrTestId As String
        Dim strCurrentDate As String = Date.Now.ToShortDateString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")



        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        dstGetLeadGroups = GetLeadGroupsByStudent(StudentId)
        If dstGetLeadGroups.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dstGetLeadGroups.Tables(0).Rows
                strLeadGrpId &= row("LeadGrpId").ToString & "','"
            Next
            strLeadGrpId = Mid(strLeadGrpId, 1, InStrRev(strLeadGrpId, "'") - 2)
        End If

        Dim strreqType As String = ""
        If ReqTYpe = "Enrollment" Then
            strreqType = " and ReqforEnrollment=1  "
        ElseIf ReqTYpe = "Financial Aid" Then
            strreqType = " and ReqforFinancialAid=1  "
        ElseIf ReqTYpe = "Graduation" Then
            strreqType = " and ReqforGraduation=1  "
        ElseIf ReqTYpe = "Generic Requirements" Then
            strreqType = " and ReqforEnrollment=0 and ReqforFinancialAid=0  and ReqforGraduation=0    "
        End If


        'Query gets the requirement group summary assigned to LeadGroup and Program Version
        With sb
            .Append("    select distinct t1.adReqId,Descrip,adReqTypeId,  ")
            .Append("    t6.ReqGrpId as ReqGrpId ")
            .Append("    from ")
            .Append("   (select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3)" & strreqType & "   and StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965') t1,  ")
            .Append("   (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("   (select Distinct LeadGrpId,adReqEffectiveDateId,IsRequired from adReqLeadGroups where LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "' )) t3, ")
            .Append("   (select Distinct ReqGrpId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3  where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and t3.StudentId= '" & StudentId & "' and ReqGrpId='" & ReqGrpId & "' and ReqGrpId is not null) t5, ")
            .Append("   (select Distinct adReqId,ReqGrpId,LeadGrpId from adReqGrpDef where ReqGrpId='" & ReqGrpId & "') t6, ")
            .Append("   (select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "') t7 ")
            .Append("   where t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            .Append("   t5.ReqGrpId = t6.ReqGrpId And t3.LeadGrpId = t7.LeadGrpId And t6.LeadGrpId = t7.LeadGrpId And t1.adReqId = t6.adReqId ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With
        dadNorthwind = New OleDbDataAdapter(sb.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "RequirementGroup")
        Return dstNorthwind.Tables(0)
    End Function
    Public Function GetStudentsWithMissingRequirementsAndRequirementGroup(ByVal campusId As String, Optional ByVal ProgId As String = "", Optional ByVal ShiftId As String = "", Optional ByVal EnrollmentStatus As String = "", Optional ByVal StudentGroup As String = "", Optional ByVal ReqType As String = "") As DataTable
        Dim sbRequirements As New StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim drowRootParent As DataRow
        Dim drowSchoolLevelSummary As DataRow
        Dim drowSchoolLevelDetails As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
        Dim drowRequirementAssignedDirectlyToPrgVersion, drowRequirementAssignedDirectlyToLeadGroup As DataRow
        Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder

        Dim dsGetEntranceTestId As New DataSet
        Dim dsGetLeadGrpId As New DataSet
        Dim dstGetRequirementsAssignedToProgramVersion As New DataSet
        Dim dstGetRequirementsDirectlyAssigned As New DataSet
        Dim dstGetLeadGroups As New DataSet
        Dim intRowCounter As Integer
        Dim strLeadGrpId As String
        Dim dstOverrideTest As New DataSet
        Dim strEntrTestId As String
        Dim strCurrentDate As String = Date.Now.ToShortDateString
        Dim dsGetCmpGrps As New DataSet
        Dim dstGetDocumentStatus As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        Dim strCampGrpId As String
        Dim strDocumentStatus As String

        ''MOdified by Saraswathi lakshmanan on Jan 27 2010
        ''For missing items page ADDING REQ TYPE FILTER


        Dim strreqType As String = ""
        If ReqType = "Enrollment" Then
            strreqType = " and ReqforEnrollment=1  "
        ElseIf ReqType = "Financial Aid" Then
            strreqType = " and ReqforFinancialAid=1  "
        ElseIf ReqType = "Graduation" Then
            strreqType = " and ReqforGraduation=1  "
        ElseIf ReqType = "Generic Requirements" Then
            strreqType = " and ReqforEnrollment=0 and ReqforFinancialAid=0  and ReqforGraduation=0    "
        End If







        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusId)

        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        dstGetDocumentStatus = GetDocumentStatus(campusId)
        If dstGetDocumentStatus.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dstGetDocumentStatus.Tables(0).Rows
                strDocumentStatus &= row("DocStatusId").ToString & "','"
            Next
            strDocumentStatus = Mid(strDocumentStatus, 1, InStrRev(strDocumentStatus, "'") - 2)
        End If

        dstOverrideTest = GetOverridedTestByStudent()
        If dstOverrideTest.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dstOverrideTest.Tables(0).Rows
                strEntrTestId &= row("EntrTestId").ToString & "','"
            Next
            strEntrTestId = Mid(strEntrTestId, 1, InStrRev(strEntrTestId, "'") - 2)
        End If

        If Not StudentGroup = "" Then
            dstGetLeadGroups = GetLeadGroupsByStudentMissing(StudentGroup)
        Else
            dstGetLeadGroups = GetLeadGroupsByStudentMissing("")
        End If

        If dstGetLeadGroups.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dstGetLeadGroups.Tables(0).Rows
                strLeadGrpId &= row("LeadGrpId").ToString & "','"
            Next
            strLeadGrpId = Mid(strLeadGrpId, 1, InStrRev(strLeadGrpId, "'") - 2)
        End If

        'Modified on 09/25/2007 by Balaji to bring in missing items requirement
        With sbRequirements
            'Mandatory Documents not approved
            .Append(" select distinct StudentId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3)  " & strreqType & "  and StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement=1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("       (select Distinct DocumentId,DocStatusId,StudentId from plStudentDocs  ")
            If Not strDocumentStatus = "" Then
                .Append(" where DocStatusId in ('")
                .Append(strDocumentStatus)
                .Append(") ")
            End If
            .Append(" ) t3 ")
            .Append("   where t1.adReqId = t2.adReqId And t1.adReqId = t3.DocumentId ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append("   union ")
            ' Mandatory Test not passed
            .Append(" select distinct StudentId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3)  " & strreqType & "  and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement=1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("		(select EntrTestId,StudentId from adLeadEntranceTest where  Pass=0 and StudentId is not null) t4 ")
            .Append(" where t1.adReqId = t2.adReqId And t1.adReqId = t4.EntrTestId  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" Union ")
            ' Mandatory Test overriden
            .Append(" select distinct StudentId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3)  " & strreqType & "  and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement=1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("		(select Distinct EntrTestId,StudentId from adEntrTestOverride where Override=1 and StudentId is not null) t4 ")
            .Append(" where t1.adReqId = t2.adReqId And t1.adReqId = t4.EntrTestId  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" Union ")
            ' Get Documents assigned directly to program version
            .Append(" select distinct StudentId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3)  " & strreqType & "  and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("   	(select Distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and ReqGrpId is null ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            ' Requirement should not be in requirement group assigned to program version
            .Append("       and t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2,arStuEnrollments s3,arStudent s4 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId=s3.PrgVerId and s3.StudentId=s4.StudentId and s2.adReqId is null  ")
            If Not ProgId = "" Then
                .Append(" and s3.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and s3.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and s3.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" )) t5, ")
            .Append("       (select Distinct DocumentId,DocStatusId,StudentId from plStudentDocs ")
            If Not strDocumentStatus = "" Then
                .Append("  where DocStatusId in ('")
                .Append(strDocumentStatus)
                .Append(") ")
            End If
            .Append(" ) t6 ")
            .Append(" where ")
            .Append("	t1.adReqId = t2.adReqId and t1.adReqId=t5.adReqId and  ")
            .Append("   t1.adReqId = t6.DocumentId  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" Union ")
            ' Get Test assigned directly to program version
            .Append(" select distinct StudentId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3)  " & strreqType & "  and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("   	(select Distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and ReqGrpId is null ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append("       and t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2,arStuEnrollments s3,arStudent s4 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId=s3.PrgVerId and s3.StudentId=s4.StudentId and s2.adReqId is null  ")
            If Not ProgId = "" Then
                .Append(" and s3.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and s3.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and s3.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" )) t5, ")
            .Append("       (select Distinct EntrTestId,StudentId from adLeadEntranceTest where Pass=0 and StudentId is not null) t7 ")
            .Append(" where t1.adReqId = t2.adReqId and t1.adReqId=t5.adReqId and t1.adReqId = t7.EntrTestId  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" Union ")
            ' Get Test assigned directly to program version
            .Append(" select distinct StudentId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3)  " & strreqType & "  and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("   	(select Distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and ReqGrpId is null ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append("       and t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2,arStuEnrollments s3,arStudent s4 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId=s3.PrgVerId and s3.StudentId=s4.StudentId and s2.adReqId is null  ")
            If Not ProgId = "" Then
                .Append(" and s3.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and s3.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and s3.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" )) t5, ")
            .Append("       (select Distinct EntrTestId,StudentId from adEntrTestOverride where Override=1 and StudentId is not null) t7 ")
            .Append(" where t1.adReqId = t2.adReqId and t1.adReqId=t5.adReqId and t1.adReqId = t7.EntrTestId  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" Union ")
            ' Get Test assigned directly to lead group
            .Append(" select distinct StudentId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3)  " & strreqType & "  and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId  from adReqsEffectiveDates where getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append(" adReqLeadGroups t3, ")
            .Append("       (select Distinct DocumentId,DocStatusId,StudentId from plStudentDocs ")
            If Not strDocumentStatus = "" Then
                .Append("  where DocStatusId in ('")
                .Append(strDocumentStatus)
                .Append(") ")
            End If
            .Append(" ) t4 ")
            .Append("   where ")
            .Append("       t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and  ")
            ' Requirement should not be in requirement group assigned to program version
            .Append(" t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2,arStuEnrollments s3,arStudent s4 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId=s3.PrgVerId and s3.StudentId=s4.StudentId and s2.adReqId is null   ")
            If Not ProgId = "" Then
                .Append(" and s3.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and s3.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and s3.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" ) ")
            .Append(" and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" ) and ")
            .Append(" t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and ReqGrpId is not null ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" )  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" and t3.IsRequired = 1 and ")
            .Append(" t1.adReqId = t4.DocumentId ")
            .Append("  Union ")
            'Test Requirements assigned directly to lead group that are marked as required for the lead group
            ' and the requirements should either be failed or not approved
            .Append(" select distinct StudentId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3)  " & strreqType & "  and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId  from adReqsEffectiveDates where getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("       adReqLeadGroups t3, ")
            .Append("       (select EntrTestId,StudentId from adLeadEntranceTest where Pass=0 and StudentId is not null) t5 ")
            .Append("       where t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            ' Requirement should not be in requirement group assigned to program version
            .Append("       t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2,arStuEnrollments s3,arStudent s4 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId=s3.PrgVerId and s3.StudentId=s4.StudentId and s2.adReqId is null ")
            If Not ProgId = "" Then
                .Append(" and s3.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and s3.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and s3.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" )    ")
            .Append("       and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId) and  ")
            ' Requirement should not be directly assigned to program version
            .Append("       t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and ReqGrpId is null ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" )  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append("       and t3.IsRequired = 1 and ")
            .Append("       t1.adReqId = t5.EntrTestId ")
            .Append(" Union ")
            'Test Requirements assigned directly to lead group that are marked as required for the lead group
            ' and the requirements should either be failed or not approved and overriden
            .Append(" select distinct StudentId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3)  " & strreqType & "  and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId  from adReqsEffectiveDates where getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("       adReqLeadGroups t3, ")
            .Append("       (select Distinct EntrTestId,StudentId from adEntrTestOverride where Override=1 and StudentId is not null) t5 ")
            .Append("       where t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            ' Requirement should not be in requirement group assigned to program version
            .Append("       t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2,arStuEnrollments s3,arStudent s4 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId=s3.PrgVerId and s3.StudentId=s4.StudentId and s2.adReqId is null ")
            If Not ProgId = "" Then
                .Append(" and s3.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and s3.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and s3.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append("  )    ")
            .Append("       and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId) and  ")
            ' Requirement should not be directly assigned to program version
            .Append("       t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and ReqGrpId is null")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append("   )  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append("       and t3.IsRequired = 1 and ")
            .Append("       t1.adReqId = t5.EntrTestId ")
            .Append(" Union ")
            .Append(" select Distinct StudentId  ")
            .Append(" from ")
            .Append(" (     ")
            'TestDocumentAttempted removed from the count query by Balaji on 4/23/2007
            '.Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,IsNull(R5.TestPassed,0)+IsNULL(R5.DocsApproved,0)+IsNULL(R5.TestDocumentAttempted,0)+IsNULL(R5.TestOverriden,0)+IsNULL(R5.DocumentOverriden,0) as AttemptedReqs,  ")
            .Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,IsNull(R5.TestPassed,0)+IsNULL(R5.DocsApproved,0) as AttemptedReqs,  ")
            .Append(" Case when LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else LeadGrpId end as LeadGrpId,StudentId ")
            .Append(" from ")
            '*********************************************************************************************************************************************
            ' This Query gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted by Lead and Program Version 
            '*********************************************************************************************************************************************
            .Append(" (Select  Distinct t1.ReqGrpId,t2.Descrip,t2.CampGrpId,t3.NumReqs as Numreqs,t6.StudentId,  ")
            ''''''''''''' This Query gives the number of test passed by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId ")
            If Not strLeadGrpId = "" Then
                .Append("  and A4.LeadGrpId in 	('")
                .Append(strLeadGrpId)
                .Append(") ")
            End If
            .Append("  and ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=1  " & strreqType & " ) ")
            .Append(" R1,adLeadEntranceTest R2 WHERE ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.StudentId = t6.StudentId and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and R2.Pass=1  ")
            .Append("  and R2.EntrTestId not in (select Distinct EntrTestId from adEntrTestOverRide t1   where t1.StudentId=t6.StudentId and OverRide=1) ")
            .Append(" and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as TestPassed, ")
            ''''''''''''' This Query gives the number of documents approved by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from  ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId  ")
            If Not strLeadGrpId = "" Then
                .Append(" and A4.LeadGrpId in 	('")
                .Append(strLeadGrpId)
                .Append(") ")
            End If
            .Append(" and ")
            .Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=3  " & strreqType & " ) R1,plStudentDocs R2,syDocStatuses R3 WHERE ")
            .Append(" R1.adReqId = R2.DocumentId and R2.StudentId=t6.StudentId ")
            .Append("  and R2.DocumentId not in (select Distinct EntrTestId from adEntrTestOverRide t1   where t1.StudentId=t6.StudentId and OverRide=1) ")
            .Append(" and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as DocsApproved, ")
            .Append(" t4.LeadGrpId ")
            .Append(" from adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3,adLeadGroups t4,arStuEnrollments t5,arStudent t6, syStatuses  t10   where ")
            .Append(" t1.ReqGrpId = t2.ReqGrpId and t2.ReqGrpId = t3.ReqGrpId and t3.LeadGrpId = t4.LeadGrpId and t1.PrgVerId=t5.PrgVerId and t5.StudentId=t6.StudentId ")
            .Append(" and t3.StatusId = t10.StatusId and t10.Status='Active'")
            If Not ProgId = "" Then
                .Append(" And t5.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t5.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t5.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" and t2.IsMandatoryReqGrp <> 1 ")
            If Not strLeadGrpId = "" Then
                .Append(" and t3.LeadGrpId in 	('")
                .Append(strLeadGrpId)
                .Append(") ")
            End If
            .Append(" ) R5  ")
            If Not strCampGrpId = "" Then
                .Append("  where R5.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append("    ) R8 ")
            .Append("   where  ")
            .Append(" (AttemptedReqs < NumReqs ")
            .Append("  or  ")
            .Append(" (select Count(*) from adReqGrpDef where ReqGrpId=R8.ReqGrpId ")
            .Append(" and LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where StuEnrollId in ")
            .Append(" (select Distinct StuEnrollId from arStuEnrollments where StudentId=R8.StudentId)) ")
            .Append("  and IsRequired=1 and  ")
            .Append(" ( ")
            ' Test should have been failed
            .Append(" adReqId in ")
            .Append(" (select distinct EntrTestId from adLeadEntranceTest where StudentId=R8.StudentId ")
            .Append(" and Pass=0 and EntrTestId not in (select EntrTestId from adEntrTestOverride where ")
            .Append(" StudentId=R8.StudentId and override=1)) ")
            .Append(" or  ")
            ' or document should be not approved
            .Append(" adReqId in ")
            .Append(" (select distinct DocumentId from plStudentDocs t1,syDocStatuses t2 where ")
            .Append(" t1.StudentId=R8.StudentId  and t1.DocStatusId = t2.DocStatusId and ")
            .Append(" t2.SysDocStatusId <> 1 and DocumentId not in ")
            .Append(" (select EntrTestId from adEntrTestOverride where StudentId=R8.StudentId  ")
            .Append(" and override=1)) ")
            .Append(" or ")
            ' or requirement should be overriden
            .Append(" adReqId in ")
            .Append(" (select Distinct EntrTestId from adEntrTestOverride where StudentId=R8.StudentId  ")
            .Append(" and override=1))) >=1) ")
        End With
        Return db.RunParamSQLDataSet(sbRequirements.ToString).Tables(0)
    End Function
    Public Function GetStudentsListForMissingItemsReport(ByVal campusId As String, Optional ByVal FilterList As String = "") As String
        Dim sbRequirements As New StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim drowRootParent As DataRow
        Dim drowSchoolLevelSummary As DataRow
        Dim drowSchoolLevelDetails As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
        Dim drowRequirementAssignedDirectlyToPrgVersion, drowRequirementAssignedDirectlyToLeadGroup As DataRow
        Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder

        Dim dsGetEntranceTestId As New DataSet
        Dim dsGetLeadGrpId As New DataSet
        Dim dstGetRequirementsAssignedToProgramVersion As New DataSet
        Dim dstGetRequirementsDirectlyAssigned As New DataSet
        Dim dstGetLeadGroups As New DataSet
        Dim intRowCounter As Integer
        Dim strLeadGrpId As String
        Dim dstOverrideTest As New DataSet
        Dim strEntrTestId As String
        Dim strCurrentDate As String = Date.Now.ToShortDateString
        Dim dsGetCmpGrps As New DataSet
        Dim dstGetDocumentStatus As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        Dim strCampGrpId As String
        Dim strDocumentStatus As String

        Dim strNewCampGrpID As String

        'dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusId)

        'If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
        '    For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
        '        strCampGrpId &= row("CampGrpId").ToString & "','"
        '    Next
        '    strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        'End If
        If FilterList.Contains("TypeOfReq") And FilterList.Contains("LeadGrpId") Then


            strCampGrpId = Mid(FilterList, InStr(FilterList, "(") + 1, InStr(Mid(FilterList, InStr(FilterList, "(") + 1), "AND") - 3)
            strNewCampGrpID = Mid(FilterList, InStr(FilterList, "(") + 1, InStr(Mid(FilterList, InStr(FilterList, "(") + 1), "AND") - 4) + "  ,(SELECT CAmpGrpID FROM dbo.syCampGrps WHERE IsAllCampusGrp=1 AND CampGrpCode='All'))"
            dstGetDocumentStatus = GetDocumentStatus(campusId)
            If dstGetDocumentStatus.Tables(0).Rows.Count >= 1 Then
                For Each row As DataRow In dstGetDocumentStatus.Tables(0).Rows
                    strDocumentStatus &= row("DocStatusId").ToString & "','"
                Next
                strDocumentStatus = Mid(strDocumentStatus, 1, InStrRev(strDocumentStatus, "'") - 2)
            End If

            dstOverrideTest = GetOverridedTestByStudent()
            If dstOverrideTest.Tables(0).Rows.Count >= 1 Then
                For Each row As DataRow In dstOverrideTest.Tables(0).Rows
                    strEntrTestId &= row("EntrTestId").ToString & "','"
                Next
                strEntrTestId = Mid(strEntrTestId, 1, InStrRev(strEntrTestId, "'") - 2)
            End If

            If Not FilterList = "" Then
                Dim intPosition As Integer = 0
                Dim intNextPosition As Integer = 0
                intPosition = InStr(FilterList, "adLeadGroups.LeadGrpId")
                If intPosition >= 1 Then
                    'Get the filter starting from adLeadGroups.LeadGrpId
                    Dim strTruncateFilter As String = Mid(FilterList, intPosition)
                    If InStr(FilterList, "adLeadGroups.LeadGrpId =") Then
                        intNextPosition = InStr(strTruncateFilter, "AND") - 1
                    Else
                        intNextPosition = InStr(strTruncateFilter, ")")
                    End If

                    Dim strLeadGrpFilter As String = ""
                    If intNextPosition >= 1 Then
                        strLeadGrpFilter = Mid(FilterList, intPosition, intNextPosition)
                    Else
                        intNextPosition = InStr(strTruncateFilter, "AND")
                        If intNextPosition > 0 Then
                            intNextPosition = intNextPosition - 1
                        Else
                            intNextPosition = (intPosition + 61)
                        End If

                        strLeadGrpFilter = Mid(FilterList, intPosition, intNextPosition)
                    End If
                    strLeadGrpFilter = strLeadGrpFilter.Replace("adLeadGroups", "t1")
                    dstGetLeadGroups = GetLeadGroupsByStudentFilter(strLeadGrpFilter)
                Else
                    dstGetLeadGroups = GetLeadGroupsByStudentFilter("")
                End If
            Else
                dstGetLeadGroups = GetLeadGroupsByStudentFilter("")
            End If
            If dstGetLeadGroups.Tables(0).Rows.Count >= 1 Then
                For Each row As DataRow In dstGetLeadGroups.Tables(0).Rows
                    strLeadGrpId &= row("LeadGrpId").ToString & "','"
                Next
                strLeadGrpId = Mid(strLeadGrpId, 1, InStrRev(strLeadGrpId, "'") - 2)
            End If

            If FilterList.Contains("SyTypeofRequirement.TypeOfReq") Then

                ''TypeofRequirement added by Saraswathi Lakshmanan on oct 4 2010
                Dim StrTypeofRequirement As String
                StrTypeofRequirement = Mid(FilterList, InStr(FilterList, "SyTypeofRequirement.TypeOfReq"), FilterList.Length - 1)
                Dim startPosition As Integer
                startPosition = InStr(StrTypeofRequirement, "'")

                StrTypeofRequirement = Mid(StrTypeofRequirement, startPosition + 1, StrTypeofRequirement.Length - 1)
                Dim nextpositionofEndingQuotes As Integer
                nextpositionofEndingQuotes = InStr(StrTypeofRequirement, "'")
                StrTypeofRequirement = Mid(StrTypeofRequirement, 1, nextpositionofEndingQuotes - 1)

                With sbRequirements
                    'Mandatory Documents not approved
                    .Append(" select Distinct syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId, ")
                    .Append(" (select CampDescrip from syCampuses where syCampuses.CampusId=arStuEnrollments.CampusId) as CampusDescrip, ")
                    .Append(" arStuEnrollments.StuEnrollId,getStudents.StudentId,S.LastName,S.FirstName, S.SSN,arStuEnrollments.PrgVerId,PV.PrgVerDescrip,arStuEnrollments.StatusCodeId,SC.StatusCodeDescrip, ")
                    .Append(" arStuEnrollments.ShiftId, (select Top 1 ShiftDescrip from arShifts where ShiftId=arStuEnrollments.ShiftId) as ShiftDescrip, ")
                    .Append(" arStuEnrollments.EdLvlId,(select Top 1 EdLvlDescrip from adEdLvls where EdLvlId=arStuEnrollments.EdLvlId) as EdLvlDescrip, ")
                    .Append(" adReqId,ReqDescrip,ReqType,NULL as Required,0 as Fullfilled ")
                    .Append(" from ")
                    .Append(" ( ")
                    .Append(" select distinct StudentId,Descrip as ReqDescrip,t1.adReqId,(select Descrip from adReqTypes where adReqTypeId=t1.adReqTypeId) as ReqType ")
                    .Append(" from ")
                    ''Modified by Saraswathi Lakshmanan On Oct 1 2010
                    ''Added for Document Trackking
                    ''ReqforEnrollment is added to theq query
                    .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and")
                    If StrTypeofRequirement = "Enrollment" Then
                        .Append(" ReqforEnrollment = 1 And ")
                    ElseIf StrTypeofRequirement = "Graduation" Then
                        .Append(" ReqforGraduation = 1 And ")
                    ElseIf StrTypeofRequirement = "FinancialAid" Then
                        .Append(" ReqforFinancialAid = 1 And ")
                    Else
                        .Append(" ReqforEnrollment = 0 And ReqforGraduation = 0 And ReqforFinancialAid = 0 And  ")
                    End If
                    .Append("   StatusId='" & strActiveGUID & "') t1, ")
                    .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement=1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
                    .Append("       (select Distinct DocumentId,DocStatusId,StudentId from plStudentDocs  ")
                    If Not strDocumentStatus = "" Then
                        .Append(" where DocStatusId in ('")
                        .Append(strDocumentStatus)
                        .Append(") ")
                    End If
                    .Append(" ) t3 ")
                    .Append("   where t1.adReqId = t2.adReqId And t1.adReqId = t3.DocumentId ")
                    If Not strNewCampGrpID = "" Then
                        .Append("  and  t1.CampGrpId in 	(")
                        .Append(strNewCampGrpID)
                        .Append(" ) ")
                    End If
                    .Append("   union ")
                    ' Mandatory Test not passed
                    .Append(" select distinct StudentId,Descrip  as ReqDescrip,t1.adReqId,(select Descrip from adReqTypes where adReqTypeId=t1.adReqTypeId) as ReqType ")
                    .Append(" from ")
                    ''Modified by Saraswathi Lakshmanan On Oct 1 2010
                    ''Added for Document Trackking
                    ''ReqforEnrollment is added to theq query

                    .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and ")
                    If StrTypeofRequirement = "Enrollment" Then
                        .Append(" ReqforEnrollment = 1 And ")
                    ElseIf StrTypeofRequirement = "Graduation" Then
                        .Append(" ReqforGraduation = 1 And ")
                    ElseIf StrTypeofRequirement = "FinancialAid" Then
                        .Append(" ReqforFinancialAid = 1 And ")
                    Else
                        .Append(" ReqforEnrollment = 0 And ReqforGraduation = 0 And ReqforFinancialAid = 0 And  ")
                    End If
                    .Append("  StatusId='" & strActiveGUID & "') t1, ")
                    .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement=1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
                    .Append("		(select EntrTestId,StudentId from adLeadEntranceTest where  Pass=0 and StudentId is not null) t4 ")
                    .Append(" where t1.adReqId = t2.adReqId And t1.adReqId = t4.EntrTestId  ")
                    If Not strNewCampGrpID = "" Then
                        .Append("  and  t1.CampGrpId in 	(")
                        .Append(strNewCampGrpID)
                        .Append(") ")
                    End If
                    .Append(" Union ")
                    ' Mandatory Test overriden
                    .Append(" select distinct StudentId,Descrip  as ReqDescrip,t1.adReqId,(select Descrip from adReqTypes where adReqTypeId=t1.adReqTypeId) as ReqType ")
                    .Append(" from ")
                    ''Modified by Saraswathi Lakshmanan On Oct 1 2010
                    ''Added for Document Trackking
                    ''ReqforEnrollment is added to theq query

                    .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and ")
                    If StrTypeofRequirement = "Enrollment" Then
                        .Append(" ReqforEnrollment = 1 And ")
                    ElseIf StrTypeofRequirement = "Graduation" Then
                        .Append(" ReqforGraduation = 1 And ")
                    ElseIf StrTypeofRequirement = "FinancialAid" Then
                        .Append(" ReqforFinancialAid = 1 And ")
                    Else
                        .Append(" ReqforEnrollment = 0 And ReqforGraduation = 0 And ReqforFinancialAid = 0 And  ")
                    End If
                    .Append("  StatusId='" & strActiveGUID & "') t1, ")
                    .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement=1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
                    .Append("		(select Distinct EntrTestId,StudentId from adEntrTestOverride where  Override=1 and StudentId is not null) t4 ")
                    .Append(" where t1.adReqId = t2.adReqId And t1.adReqId = t4.EntrTestId  ")
                    If Not strNewCampGrpID = "" Then
                        .Append("  and  t1.CampGrpId in 	(")
                        .Append(strNewCampGrpID)
                        .Append(") ")
                    End If
                    .Append(" Union ")
                    ' Get Documents assigned directly to program version
                    .Append(" select distinct StudentId,Descrip as ReqDescrip,t1.adReqId,(select Descrip from adReqTypes where adReqTypeId=t1.adReqTypeId) as ReqType ")
                    .Append(" from ")
                    ''Modified by Saraswathi Lakshmanan On Oct 1 2010
                    ''Added for Document Trackking
                    ''ReqforEnrollment is added to theq query
                    .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and ")
                    If StrTypeofRequirement = "Enrollment" Then
                        .Append(" ReqforEnrollment = 1 And ")
                    ElseIf StrTypeofRequirement = "Graduation" Then
                        .Append(" ReqforGraduation = 1 And ")
                    ElseIf StrTypeofRequirement = "FinancialAid" Then
                        .Append(" ReqforFinancialAid = 1 And ")
                    Else
                        .Append(" ReqforEnrollment = 0 And ReqforGraduation = 0 And ReqforFinancialAid = 0 And  ")
                    End If
                    .Append("  StatusId='" & strActiveGUID & "') t1, ")
                    .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
                    .Append("   	(select Distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and ReqGrpId is null ")
                    ' Requirement should not be in requirement group assigned to program version
                    .Append("       and t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2,arStuEnrollments s3,arStudent s4 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId=s3.PrgVerId and s3.StudentId=s4.StudentId and s2.adReqId is null)) t5,  ")
                    .Append("       (select Distinct DocumentId,DocStatusId,StudentId from plStudentDocs ")
                    If Not strDocumentStatus = "" Then
                        .Append("  where DocStatusId in ('")
                        .Append(strDocumentStatus)
                        .Append(") ")
                    End If
                    .Append(" ) t6 ")
                    .Append(" where ")
                    .Append("	t1.adReqId = t2.adReqId and t1.adReqId=t5.adReqId and  ")
                    .Append("   t1.adReqId = t6.DocumentId  ")
                    If Not strNewCampGrpID = "" Then
                        .Append("  and  t1.CampGrpId in 	(")
                        .Append(strNewCampGrpID)
                        .Append(") ")
                    End If
                    .Append(" Union ")
                    ' Get Test assigned directly to program version
                    .Append(" select distinct StudentId,Descrip as ReqDescrip,t1.adReqId,(select Descrip from adReqTypes where adReqTypeId=t1.adReqTypeId) as ReqType ")
                    .Append(" from ")
                    ''Modified by Saraswathi Lakshmanan On Oct 1 2010
                    ''Added for Document Trackking
                    ''ReqforEnrollment is added to theq query
                    .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and ")
                    If StrTypeofRequirement = "Enrollment" Then
                        .Append(" ReqforEnrollment = 1 And ")
                    ElseIf StrTypeofRequirement = "Graduation" Then
                        .Append(" ReqforGraduation = 1 And ")
                    ElseIf StrTypeofRequirement = "FinancialAid" Then
                        .Append(" ReqforFinancialAid = 1 And ")
                    Else
                        .Append(" ReqforEnrollment = 0 And ReqforGraduation = 0 And ReqforFinancialAid = 0 And  ")
                    End If
                    .Append("  StatusId='" & strActiveGUID & "') t1, ")
                    .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
                    .Append("   	(select Distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and ReqGrpId is null ")
                    ' Requirement should not be in requirement group assigned to program version
                    .Append("       and t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2,arStuEnrollments s3,arStudent s4 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId=s3.PrgVerId and s3.StudentId=s4.StudentId and s2.adReqId is null)) t5,  ")
                    .Append("       (select Distinct EntrTestId,StudentId from adLeadEntranceTest where Pass=0 and StudentId is not null) t7 ")
                    .Append(" where t1.adReqId = t2.adReqId and t1.adReqId=t5.adReqId and t1.adReqId = t7.EntrTestId  ")
                    If Not strNewCampGrpID = "" Then
                        .Append("  and  t1.CampGrpId in 	(")
                        .Append(strNewCampGrpID)
                        .Append(") ")
                    End If
                    .Append(" Union ")
                    ' Get Test assigned directly to program version
                    .Append(" select distinct StudentId,Descrip as ReqDescrip,t1.adReqId,(select Descrip from adReqTypes where adReqTypeId=t1.adReqTypeId) as ReqType ")
                    .Append(" from ")
                    ''Modified by Saraswathi Lakshmanan On Oct 1 2010
                    ''Added for Document Trackking
                    ''ReqforEnrollment is added to theq query
                    .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and ")
                    If StrTypeofRequirement = "Enrollment" Then
                        .Append(" ReqforEnrollment = 1 And ")
                    ElseIf StrTypeofRequirement = "Graduation" Then
                        .Append(" ReqforGraduation = 1 And ")
                    ElseIf StrTypeofRequirement = "FinancialAid" Then
                        .Append(" ReqforFinancialAid = 1 And ")
                    Else
                        .Append(" ReqforEnrollment = 0 And ReqforGraduation = 0 And ReqforFinancialAid = 0 And  ")
                    End If
                    .Append("  StatusId='" & strActiveGUID & "') t1, ")
                    .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
                    .Append("   	(select Distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and ReqGrpId is null ")
                    ' Requirement should not be in requirement group assigned to program version
                    .Append("       and t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2,arStuEnrollments s3,arStudent s4 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId=s3.PrgVerId and s3.StudentId=s4.StudentId and s2.adReqId is null)) t5,  ")
                    .Append("       (select Distinct EntrTestId,StudentId from adEntrTestOverride where Override=1 and StudentId is not null) t7 ")
                    .Append(" where t1.adReqId = t2.adReqId and t1.adReqId=t5.adReqId and t1.adReqId = t7.EntrTestId  ")
                    If Not strNewCampGrpID = "" Then
                        .Append("  and  t1.CampGrpId in 	(")
                        .Append(strNewCampGrpID)
                        .Append(") ")
                    End If
                    .Append(" Union ")
                    ' Get Docs assigned directly to leadgroup
                    .Append(" select distinct StudentId,Descrip as ReqDescrip,t1.adReqId,(select Descrip from adReqTypes where adReqTypeId=t1.adReqTypeId) as ReqType ")
                    .Append(" from ")
                    ''Modified by Saraswathi Lakshmanan On Oct 1 2010
                    ''Added for Document Trackking
                    ''ReqforEnrollment is added to theq query
                    .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and ")
                    If StrTypeofRequirement = "Enrollment" Then
                        .Append(" ReqforEnrollment = 1 And ")
                    ElseIf StrTypeofRequirement = "Graduation" Then
                        .Append(" ReqforGraduation = 1 And ")
                    ElseIf StrTypeofRequirement = "FinancialAid" Then
                        .Append(" ReqforFinancialAid = 1 And ")
                    Else
                        .Append(" ReqforEnrollment = 0 And ReqforGraduation = 0 And ReqforFinancialAid = 0 And  ")
                    End If
                    .Append("  StatusId='" & strActiveGUID & "') t1, ")
                    .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId  from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
                    .Append(" adReqLeadGroups t3, ")
                    .Append("       (select Distinct DocumentId,DocStatusId,StudentId from plStudentDocs ")
                    If Not strDocumentStatus = "" Then
                        .Append("  where DocStatusId in ('")
                        .Append(strDocumentStatus)
                        .Append(") ")
                    End If
                    .Append(" ) t4 ")
                    .Append("   where ")
                    .Append("       t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and  ")
                    ' Requirement should not be in requirement group assigned to program version
                    .Append(" t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2,arStuEnrollments s3,arStudent s4 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId=s3.PrgVerId and s3.StudentId=s4.StudentId and s2.adReqId is null)   ")
                    .Append(" and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId) and ")
                    .Append(" t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and ReqGrpId is null)  ")
                    If Not strNewCampGrpID = "" Then
                        .Append("  and  t1.CampGrpId in 	(")
                        .Append(strNewCampGrpID)
                        .Append(") ")
                    End If
                    .Append(" and t3.IsRequired = 1 and ")
                    .Append(" t1.adReqId = t4.DocumentId ")
                    .Append("  Union ")
                    'Test Requirements assigned directly to lead group that are marked as required for the lead group
                    ' and the requirements should either be failed or not approved
                    .Append(" select distinct StudentId,Descrip as ReqDescrip,t1.adReqId as adReqId,(select Descrip from adReqTypes where adReqTypeId=t1.adReqTypeId) as ReqType ")
                    .Append(" from ")
                    ''Modified by Saraswathi Lakshmanan On Oct 1 2010
                    ''Added for Document Trackking
                    ''ReqforEnrollment is added to theq query
                    .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and ")
                    If StrTypeofRequirement = "Enrollment" Then
                        .Append(" ReqforEnrollment = 1 And ")
                    ElseIf StrTypeofRequirement = "Graduation" Then
                        .Append(" ReqforGraduation = 1 And ")
                    ElseIf StrTypeofRequirement = "FinancialAid" Then
                        .Append(" ReqforFinancialAid = 1 And ")
                    Else
                        .Append(" ReqforEnrollment = 0 And ReqforGraduation = 0 And ReqforFinancialAid = 0 And  ")
                    End If
                    .Append("  StatusId='" & strActiveGUID & "') t1, ")
                    .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId  from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
                    .Append("       adReqLeadGroups t3, ")
                    .Append("       (select EntrTestId,StudentId from adLeadEntranceTest where Pass=0 and StudentId is not null) t5 ")
                    .Append("       where t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
                    ' Requirement should not be in requirement group assigned to program version
                    .Append("       t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2,arStuEnrollments s3,arStudent s4 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId=s3.PrgVerId and s3.StudentId=s4.StudentId and s2.adReqId is null)    ")
                    .Append("       and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId) and  ")
                    ' Requirement should not be directly assigned to program version
                    .Append("       t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and ReqGrpId is null)  ")
                    If Not strNewCampGrpID = "" Then
                        .Append("  and  t1.CampGrpId in 	(")
                        .Append(strNewCampGrpID)
                        .Append(") ")
                    End If
                    .Append("       and t3.IsRequired = 1 and ")
                    .Append("       t1.adReqId = t5.EntrTestId ")
                    .Append(" Union ")
                    'Test Requirements assigned directly to lead group that are marked as required for the lead group
                    ' and the requirements should either be failed or not approved
                    .Append(" select distinct StudentId,Descrip as ReqDescrip,t1.adReqId as adReqId,(select Descrip from adReqTypes where adReqTypeId=t1.adReqTypeId) as ReqType ")
                    .Append(" from ")
                    ''Modified by Saraswathi Lakshmanan On Oct 1 2010
                    ''Added for Document Trackking
                    ''ReqforEnrollment is added to theq query
                    .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and ")
                    If StrTypeofRequirement = "Enrollment" Then
                        .Append(" ReqforEnrollment = 1 And ")
                    ElseIf StrTypeofRequirement = "Graduation" Then
                        .Append(" ReqforGraduation = 1 And ")
                    ElseIf StrTypeofRequirement = "FinancialAid" Then
                        .Append(" ReqforFinancialAid = 1 And ")
                    Else
                        .Append(" ReqforEnrollment = 0 And ReqforGraduation = 0 And ReqforFinancialAid = 0 And  ")
                    End If
                    .Append("  StatusId='" & strActiveGUID & "') t1, ")
                    .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId  from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
                    .Append("       adReqLeadGroups t3, ")
                    .Append("       (select Distinct EntrTestId,StudentId from adEntrTestOverride where Override=1 and StudentId is not null) t5 ")
                    .Append("       where t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
                    ' Requirement should not be in requirement group assigned to program version
                    .Append("       t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2,arStuEnrollments s3,arStudent s4 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId=s3.PrgVerId and s3.StudentId=s4.StudentId and s2.adReqId is null)    ")
                    .Append("       and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId) and  ")
                    ' Requirement should not be directly assigned to program version
                    .Append("       t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and ReqGrpId is null)  ")
                    If Not strNewCampGrpID = "" Then
                        .Append("  and  t1.CampGrpId in 	(")
                        .Append(strNewCampGrpID)
                        .Append(") ")
                    End If
                    .Append("       and t3.IsRequired = 1 and ")
                    .Append("       t1.adReqId = t5.EntrTestId ")
                    .Append(" Union ")
                    .Append(" select Distinct StudentId,Descrip as ReqDescrip,ReqGrpId as adReqId,'Requirement Group' as ReqType  ")
                    .Append(" from ")
                    .Append(" (     ")
                    'TestDocumentAttempted removed from the count query by Balaji on 4/23/2007
                    '.Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,IsNull(R5.TestPassed,0)+IsNULL(R5.DocsApproved,0)+IsNULL(R5.TestDocumentAttempted,0)+IsNULL(R5.TestOverriden,0)+IsNULL(R5.DocumentOverriden,0) as AttemptedReqs,  ")
                    '.Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,IsNull(R5.TestPassed,0)+IsNULL(R5.DocsApproved,0)+IsNULL(R5.TestOverriden,0)+IsNULL(R5.DocumentOverriden,0) as AttemptedReqs,  ")
                    .Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,IsNull(R5.TestPassed,0)+IsNULL(R5.DocsApproved,0) as AttemptedReqs,  ")
                    .Append(" Case when LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else LeadGrpId end as LeadGrpId,StudentId ")
                    .Append(" from ")
                    '*********************************************************************************************************************************************
                    ' This Query gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted by Lead and Program Version 
                    '*********************************************************************************************************************************************
                    .Append(" (Select  Distinct t1.ReqGrpId,t2.Descrip,t2.CampGrpId,t3.NumReqs as Numreqs,t6.StudentId,  ")
                    ''''''''''''' This Query gives the number of test passed by the lead and grouped by lead group ''''''''
                    .Append(" (select Count(*) from ")
                    .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
                    .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
                    .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId ")
                    If Not strLeadGrpId = "" Then
                        .Append("  and A4.LeadGrpId in 	('")
                        .Append(strLeadGrpId)
                        .Append(") ")
                    End If

                    If StrTypeofRequirement = "Enrollment" Then
                        .Append(" And  ReqforEnrollment = 1  ")
                    ElseIf StrTypeofRequirement = "Graduation" Then
                        .Append(" And  ReqforGraduation = 1  ")
                    ElseIf StrTypeofRequirement = "FinancialAid" Then
                        .Append(" And ReqforFinancialAid = 1  ")
                    Else
                        .Append(" And ReqforEnrollment = 0 And ReqforGraduation = 0 And ReqforFinancialAid = 0   ")
                    End If


                    .Append("  and ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=1) ")
                    .Append(" R1,adLeadEntranceTest R2 WHERE ")
                    .Append(" R1.adReqId = R2.EntrTestId and R2.StudentId = t6.StudentId and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and R2.Pass=1  ")
                    ' If Not strEntrTestId = "" Then
                    .Append("  and R2.EntrTestId not in 	(select Distinct EntrTestId from adEntrTestOverRide t1   where t1.StudentId=t6.StudentId and OverRide=1) ")
                    '.Append(strEntrTestId)
                    '.Append(") ")
                    'End If
                    .Append(" and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as TestPassed, ")
                    '''''''''''' This Query gives the number of overriden test
                    '.Append(" (select Count(*) from ")
                    '.Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
                    '.Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
                    '.Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId ")
                    'If Not strLeadGrpId = "" Then
                    '    .Append(" and A4.LeadGrpId in 	('")
                    '    .Append(strLeadGrpId)
                    '    .Append(") ")
                    'End If
                    '.Append(" and ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=1) ")
                    '.Append(" R1,adEntrTestOverride R2 WHERE ")
                    '.Append(" R1.adReqId = R2.EntrTestId and R2.StudentId = t6.StudentId and R2.Override=1 and R1.CurrentDate >= R1.StartDate   ")
                    '.Append(" and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as TestOverriden, ")
                    '''''''''''' This Query gives the number of overriden documents
                    '.Append(" (select Count(*) from ")
                    '.Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
                    '.Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
                    '.Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId ")
                    'If Not strLeadGrpId = "" Then
                    '    .Append(" and A4.LeadGrpId in 	('")
                    '    .Append(strLeadGrpId)
                    '    .Append(") ")
                    'End If
                    '.Append(" and ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=3) ")
                    '.Append(" R1,adEntrTestOverride R2 WHERE ")
                    '.Append(" R1.adReqId = R2.EntrTestId and R2.StudentId = t6.StudentId and R2.Override=1 and R1.CurrentDate >= R1.StartDate   ")
                    '.Append(" and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as DocumentOverriden, ")
                    ''''''''''''' This Query gives the number of documents approved by the lead and grouped by lead group ''''''''
                    .Append(" (select Count(*) from  ")
                    .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
                    .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
                    .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId  ")
                    If Not strLeadGrpId = "" Then
                        .Append(" and A4.LeadGrpId in 	('")
                        .Append(strLeadGrpId)
                        .Append(") ")
                    End If
                    .Append(" and ")

                    If StrTypeofRequirement = "Enrollment" Then
                        .Append(" ReqforEnrollment = 1 And ")
                    ElseIf StrTypeofRequirement = "Graduation" Then
                        .Append(" ReqforGraduation = 1 And ")
                    ElseIf StrTypeofRequirement = "FinancialAid" Then
                        .Append(" ReqforFinancialAid = 1 And ")
                    Else
                        .Append(" ReqforEnrollment = 0 And ReqforGraduation = 0 And ReqforFinancialAid = 0 And  ")
                    End If
                    .Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=3) R1,plStudentDocs R2,syDocStatuses R3 WHERE ")
                    .Append(" R1.adReqId = R2.DocumentId and R2.StudentId=t6.StudentId ")
                    'If Not strEntrTestId = "" Then
                    .Append("  and R2.DocumentId not in 	(select Distinct EntrTestId from adEntrTestOverRide t1   where t1.StudentId=t6.StudentId and OverRide=1) ")
                    '.Append(strEntrTestId)
                    '.Append(") ")
                    'End If
                    .Append(" and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as DocsApproved, ")
                    '''''''''''' This Query gives the number of test and documents attempted(no matter pass or fail,no matter approved/not approved) grouped by lead group ''''''''
                    '.Append(" (select Count(*)  from ")
                    '.Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
                    '.Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
                    '.Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId ")
                    'If Not strLeadGrpId = "" Then
                    '    .Append(" and A4.LeadGrpId in 	('")
                    '    .Append(strLeadGrpId)
                    '    .Append(") ")
                    'End If
                    '.Append("  and ")
                    '.Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (1,3)) R1,adEntrTestOverRide R2 ")
                    '.Append(" where R1.adReqId = R2.EntrTestId and R2.StudentId = t6.StudentId and R2.StudentId is not null and R1.CurrentDate >= R1.StartDate and R2.OverRide=1 and ")
                    '.Append(" (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as TestDocumentAttempted, ")
                    .Append(" t4.LeadGrpId from adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3,adLeadGroups t4,arStuEnrollments t5,arStudent t6, syStatuses t10  where ")
                    .Append(" t1.ReqGrpId = t2.ReqGrpId and t2.ReqGrpId = t3.ReqGrpId and t3.LeadGrpId = t4.LeadGrpId and t1.PrgVerId=t5.PrgVerId and t5.StudentId=t6.StudentId ")
                    .Append(" and t3.StatusId = t10.StatusId and t10.Status='Active' ")
                    .Append(" and t2.IsMandatoryReqGrp <> 1 ")
                    If Not strLeadGrpId = "" Then
                        .Append(" and t3.LeadGrpId in 	('")
                        .Append(strLeadGrpId)
                        .Append(") ")
                    End If
                    .Append(" ) R5  ")
                    'where LeadGrpId='" & LeadGrpId & "'
                    If Not strNewCampGrpID = "" Then
                        .Append("  where R5.CampGrpId in 	(")
                        .Append(strNewCampGrpID)
                        .Append(") ")
                    End If
                    .Append("    ) R8 ")
                    .Append("   where  ")
                    .Append(" (AttemptedReqs < NumReqs ")
                    .Append("  or  ")
                    .Append(" (select Count(*) from adReqGrpDef where ReqGrpId=R8.ReqGrpId ")
                    .Append(" and LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where StuEnrollId in ")
                    .Append(" (select Distinct StuEnrollId from arStuEnrollments where StudentId=R8.StudentId)) ")
                    .Append("  and IsRequired=1 and  ")
                    .Append(" ( ")
                    ' Test should have been failed
                    .Append(" adReqId in ")
                    .Append(" (select distinct EntrTestId from adLeadEntranceTest where StudentId=R8.StudentId ")
                    .Append(" and Pass=0 and EntrTestId not in (select EntrTestId from adEntrTestOverride where ")
                    .Append(" StudentId=R8.StudentId and override=1)) ")
                    .Append(" or  ")
                    ' or document should be not approved
                    .Append(" adReqId in ")
                    .Append(" (select distinct DocumentId from plStudentDocs t1,syDocStatuses t2 where ")
                    .Append(" t1.StudentId=R8.StudentId  and t1.DocStatusId = t2.DocStatusId and ")
                    .Append(" t2.SysDocStatusId <> 1 and DocumentId not in ")
                    .Append(" (select EntrTestId from adEntrTestOverride where StudentId=R8.StudentId  ")
                    .Append(" and override=1)) ")
                    .Append(" or ")
                    ' or requirement should be overriden
                    .Append(" adReqId in ")
                    .Append(" (select Distinct EntrTestId from adEntrTestOverride where StudentId=R8.StudentId  ")
                    .Append(" and override=1))) >=1) ")
                    .Append(" ) getStudents,arStudent S,arStuEnrollments,adLeadByLeadGroups LBLG,syStatusCodes SC, ")
                    .Append("   arPrgVersions PV,arPrograms P,syCampGrps,adLeadGroups ")
                    .Append("   where getStudents.StudentId=S.StudentId and S.StudentId = arStuEnrollments.StudentId and ")
                    .Append("   arStuEnrollments.StatusCodeId = SC.StatusCodeId and arStuEnrollments.StuEnrollId=LBLG.StuEnrollId and LBLG.LeadGrpId = adLeadGroups.LeadGrpId and P.ProgId=PV.ProgId and ")
                    .Append("   PV.PrgVerId = arStuEnrollments.PrgVerId")
                    ''Commented by Saraswathi lakshmanan on feb 1 2011 for rally case DE4943 Name: QA: Campus group filter on the Missing items report is not working properly. 

                    '   .Append(" AND ArStuEnrollments.CampusId IN (SELECT campusID FROM dbo.syCmpGrpCmps WHERE CampGrpId IN ( '3b27ea95-4e7e-40da-88ba-1b115c3dbfe2' )) ")
                    .Append("  AND ArStuEnrollments.CampusId  in (")
                    .Append(strCampGrpId.Replace("t1.CampGrpId from", "t1.CampusId from"))
                    .Append("  )  ")

                    ' this part was added on 01/29/2008  by balaji
                    ' was added to filter out the requirements by program version
                    'otherwise was pulling up all missing requirements and groups irrespective of program version
                    'mantis case 12692
                    .Append("  and ")
                    .Append("   ( ")
                    .Append("       adReqId in (select distinct t1.adReqId from adReqs t1,adReqsEffectiveDates t2 ")
                    .Append("       where t1.adReqId = t2.adReqId and  MandatoryRequirement=1")
                    If StrTypeofRequirement = "Enrollment" Then
                        .Append(" And ReqforEnrollment = 1  ")
                    ElseIf StrTypeofRequirement = "Graduation" Then
                        .Append(" And ReqforGraduation = 1  ")
                    ElseIf StrTypeofRequirement = "FinancialAid" Then
                        .Append(" And ReqforFinancialAid = 1  ")
                    Else
                        .Append(" And ReqforEnrollment = 0 And ReqforGraduation = 0 And ReqforFinancialAid = 0   ")
                    End If

                    .Append(" ) ")
                    .Append("       or 	adReqId in (select distinct adReqId from adPrgVerTestDetails where ")
                    .Append("       PrgVerId=arStuEnrollments.PrgVerId and adReqId is not null) ")
                    .Append("       or 	adReqId in (select distinct ReqGrpId from adPrgVerTestDetails where ")
                    .Append("       PrgVerId=arStuEnrollments.PrgVerId and ReqGrpId is not null) ")
                    .Append("       or adReqId in (select distinct t1.adReqId from adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3 ")
                    .Append("       where t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId=t3.adReqEffectiveDateId and t3.IsRequired=1 and t3.LeadgrpId in ")
                    .Append("       (select distinct LeadGrpId from adLeadByLeadGroups where StuEnrollId= arStuEnrollments.StuEnrollId)")
                    If StrTypeofRequirement = "Enrollment" Then
                        .Append(" And ReqforEnrollment = 1  ")
                    ElseIf StrTypeofRequirement = "Graduation" Then
                        .Append(" And ReqforGraduation = 1  ")
                    ElseIf StrTypeofRequirement = "FinancialAid" Then
                        .Append(" And ReqforFinancialAid = 1  ")
                    Else
                        .Append(" And ReqforEnrollment = 0 And ReqforGraduation = 0 And ReqforFinancialAid = 0   ")
                    End If
                    .Append("  ) ")
                    .Append("   ) ")
                End With
                Return sbRequirements.ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If
    End Function

    'Public Function GetStudentsWithMissingRequirementsAndRequirementGroup_Sp(ByVal StudentID As String, ByVal campusId As String, Optional ByVal ProgId As String = "", Optional ByVal ShiftId As String = "", Optional ByVal EnrollmentStatus As String = "", Optional ByVal StudentGroup As String = "", Optional ByVal ReqType As String = "") As DataTable
    '    Dim strreqType As String = ""
    '    If ReqType = "Enrollment" Then
    '        strreqType = " and ReqforEnrollment=1  "
    '    ElseIf ReqType = "Financial Aid" Then
    '        strreqType = " and ReqforFinancialAid=1  "
    '    ElseIf ReqType = "Graduation" Then
    '        strreqType = " and ReqforGraduation=1  "
    '    ElseIf ReqType = "Generic Requirements" Then
    '        strreqType = " and ReqforEnrollment=0 and ReqforFinancialAid=0  and ReqforGraduation=0    "
    '    End If

    '    Dim db As New FAME.DataAccessLayer.SQLDataAccess
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder


    '    ' Add empId to the parameter list
    '    db.AddParameter("@CampusID", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
    '    If ProgId <> "" Then
    '        db.AddParameter("@ProgID", New Guid(ProgId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
    '    End If
    '    If ShiftId <> "" Then
    '        db.AddParameter("@ShiftID", New Guid(ShiftId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
    '    End If
    '    If EnrollmentStatus <> "" Then
    '        db.AddParameter("@StatusCodeID", New Guid(EnrollmentStatus), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
    '    End If
    '    If StudentGroup <> "" Then
    '        db.AddParameter("@LeadgrpID", New Guid(StudentGroup), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
    '    End If


    '    '   Execute the query
    '    Dim ds As New DataSet

    '    If ReqType = "Enrollment" Then
    '        ds = db.RunParamSQLDataSet_SP("USP_GetStudentGridTestRequirementDetailsByEffectiveDates_Enroll")
    '    ElseIf ReqType = "Financial Aid" Then
    '        ds = db.RunParamSQLDataSet_SP("USP_GetStudentGridTestRequirementDetailsByEffectiveDates_FinAid")
    '    ElseIf ReqType = "Graduation" Then
    '        ds = db.RunParamSQLDataSet_SP("USP_GetStudentsWithMissingRequirementsAndRequirementGroup_Grad")
    '    ElseIf ReqType = "" Then
    '        ds = db.RunParamSQLDataSet_SP("USP_GetStudentsWithMissingRequirementsAndRequirementGroup")
    '    End If

    '    Dim dt As New DataTable
    '    If ds.Tables.Count > 0 Then
    '        dt = ds.Tables(0)
    '    End If
    '    Return dt

    '    db.CloseConnection()



    'End Function
    Public Function GetStudentsWithMissingRequirementsAndRequirementGroup_Sp(ByVal campusId As String, Optional ByVal ProgId As String = "", Optional ByVal ShiftId As String = "", Optional ByVal EnrollmentStatus As String = "", Optional ByVal StudentGroup As String = "", Optional ByVal ReqType As String = "") As DataTable

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder


        ' Add empId to the parameter list
        db.AddParameter("@CampusID", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        If ProgId <> "" Then
            db.AddParameter("@ProgID", New Guid(ProgId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If
        If ShiftId <> "" Then
            db.AddParameter("@ShiftID", New Guid(ShiftId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If
        If EnrollmentStatus <> "" Then
            db.AddParameter("@StatusCodeID", New Guid(EnrollmentStatus), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If
        If StudentGroup <> "" Then
            db.AddParameter("@LeadgrpID", New Guid(StudentGroup), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If


        '   Execute the query
        Dim ds As New DataSet

        If ReqType = "Enrollment" Then
            ds = db.RunParamSQLDataSet_SP("USP_GetStudentsWithMissingRequirementsAndRequirementGroup_Enroll")
        ElseIf ReqType = "Financial Aid" Then
            ds = db.RunParamSQLDataSet_SP("USP_GetStudentsWithMissingRequirementsAndRequirementGroup_FinAid")
        ElseIf ReqType = "Graduation" Then
            ds = db.RunParamSQLDataSet_SP("USP_GetStudentsWithMissingRequirementsAndRequirementGroup_Grad")
        ElseIf ReqType = "" Then
            ds = db.RunParamSQLDataSet_SP("USP_GetStudentsWithMissingRequirementsAndRequirementGroup")
        End If

        Dim dt As New DataTable
        If ds.Tables.Count > 0 Then
            dt = ds.Tables(0)
        End If
        Return dt

        db.CloseConnection()



    End Function

    Public Function GetRequirements_Sp(ByVal studentID As String, ByVal campusId As String, Optional ByVal ProgId As String = "", Optional ByVal ShiftId As String = "", Optional ByVal EnrollmentStatus As String = "", Optional ByVal ReqType As String = "") As DataTable
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        db.AddParameter("@StudentID", New Guid(studentID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        ' Add empId to the parameter list
        db.AddParameter("@CampusID", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        'If ProgId <> "" Then
        '    db.AddParameter("@ProgID", New Guid(ProgId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        'End If
        If ProgId = String.Empty Then
            db.AddParameter("@ProgID", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@ProgID", New Guid(ProgId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If



        If ShiftId <> "" Then
            db.AddParameter("@ShiftID", New Guid(ShiftId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If
        If EnrollmentStatus <> "" Then
            db.AddParameter("@StatusCodeID", New Guid(EnrollmentStatus), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If



        '   Execute the query
        Dim ds As New DataSet

        If ReqType = "Enrollment" Then
            ds = db.RunParamSQLDataSet_SP("USP_GetStudentWithMissingRequirementsAndRequirementGroup_EnrollforaStudent")
        ElseIf ReqType = "Financial Aid" Then
            ds = db.RunParamSQLDataSet_SP("USP_GetStudentWithMissingRequirementsAndRequirementGroup_FinAidforaStudent")
        ElseIf ReqType = "Graduation" Then
            ds = db.RunParamSQLDataSet_SP("USP_GetStudentWithMissingRequirementsAndRequirementGroup_GradforaStudent")
        ElseIf ReqType = "" Then
            ds = db.RunParamSQLDataSet_SP("USP_GetStudentWithMissingRequirementsAndRequirementGroup_foraStudent")
        End If

        Dim dt As New DataTable
        If ds.Tables.Count > 0 Then
            dt = ds.Tables(0)
        End If
        Return dt

        db.CloseConnection()



    End Function

    Public Function GetRequirementsGroupsByStudent_SP(ByVal StudentId As String, Optional ByVal CampusId As String = "", Optional ByVal ProgId As String = "", Optional ByVal ShiftId As String = "", Optional ByVal EnrollmentStatus As String = "", Optional ByVal ReqTYpe As String = "") As DataTable
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        db.AddParameter("@StudentID", New Guid(StudentId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        ' Add empId to the parameter list
        db.AddParameter("@CampusID", New Guid(CampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        If ProgId <> "" Then
            db.AddParameter("@ProgID", New Guid(ProgId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If
        If ShiftId <> "" Then
            db.AddParameter("@ShiftID", New Guid(ShiftId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If
        If EnrollmentStatus <> "" Then
            db.AddParameter("@StatusCodeID", New Guid(EnrollmentStatus), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If


        '   Execute the query
        Dim ds As New DataSet

        If ReqTYpe = "Enrollment" Then
            ds = db.RunParamSQLDataSet_SP("USP_GetRequirementsGroupsByStudent_Enroll")
        ElseIf ReqTYpe = "Financial Aid" Then
            ds = db.RunParamSQLDataSet_SP("USP_GetRequirementsGroupsByStudent_FinAid")
        ElseIf ReqTYpe = "Graduation" Then
            ds = db.RunParamSQLDataSet_SP("USP_GetRequirementsGroupsByStudent_Grad")
        ElseIf ReqTYpe = "" Then
            ds = db.RunParamSQLDataSet_SP("USP_GetRequirementsGroupsByStudent")
        End If

        Dim dt As New DataTable
        If ds.Tables.Count > 0 Then
            dt = ds.Tables(0)
        End If
        Return dt

        db.CloseConnection()
    End Function

    Public Function GetRequirementsByReqGroupAndStudent_SP(ByVal ReqGrpId As String, ByVal StudentId As String, Optional ByVal CampusId As String = "", Optional ByVal ReqTYpe As String = "") As DataTable

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        db.AddParameter("@ReqGrpId", New Guid(ReqGrpId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@StudentID", New Guid(StudentId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        ' Add empId to the parameter list
        db.AddParameter("@CampusID", New Guid(CampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)


        '   Execute the query
        Dim ds As New DataSet

        If ReqTYpe = "Enrollment" Then
            ds = db.RunParamSQLDataSet_SP("USP_GetRequirementsByReqGroupAndStudent_Enroll")
        ElseIf ReqTYpe = "Financial Aid" Then
            ds = db.RunParamSQLDataSet_SP("USP_GetRequirementsByReqGroupAndStudent_FinAid")
        ElseIf ReqTYpe = "Graduation" Then
            ds = db.RunParamSQLDataSet_SP("USP_GetRequirementsByReqGroupAndStudent_Grad")
        ElseIf ReqTYpe = "" Then
            ds = db.RunParamSQLDataSet_SP("USP_GetRequirementsByReqGroupAndStudent")
        End If

        Dim dt As New DataTable
        If ds.Tables.Count > 0 Then
            dt = ds.Tables(0)
        End If
        Return dt

        db.CloseConnection()
    End Function

End Class
