Option Strict On
Public Class LeadStudentSearchDB
    Public Function BuildSearchData(ByVal LeadStudentSearchInfo As LeadStudentSearchInfo) As DataSet
        Dim ds As New DataSet
        ds = GetLeadRecords(ds)
        If LeadStudentSearchInfo.GetStudents = True Then
            ds = GetStudentRecords(ds)
        End If
        If LeadStudentSearchInfo.PhoneNumber <> "" Then
            ds = GetLeadPhoneNumbers(ds)
            ds = GetStudentPhoneNumbers(ds)
        End If
        Return ds
    End Function

    Public Function GetStudentPhoneNumbers(ByVal ds As DataSet) As DataSet
        Dim db As New DataAccess
        'Dim ds As New data
        Dim da As New OleDbDataAdapter
        Dim strSQL As New StringBuilder

        With strSQL
            .Append(" SELECT StudentPhoneId, StudentId, Phone")
            .Append(" FROM arStudentPhone")
        End With
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "StudentPhoneList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try

        db.ClearParameters()

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        strSQL.Remove(0, strSQL.Length)

        ' Return the datatable
        Return ds
    End Function
    Public Function GetLeadPhoneNumbers(ByVal ds As DataSet) As DataSet
        Dim db As New DataAccess
        'Dim ds As New data
        Dim da As New OleDbDataAdapter
        Dim strSQL As New StringBuilder

        With strSQL
            .Append(" SELECT LeadPhoneId, LeadId, Phone")
            .Append(" FROM adLeadPhone")
        End With
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "LeadPhoneList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try

        db.ClearParameters()

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        strSQL.Remove(0, strSQL.Length)
        
        ' Return the datatable
        Return ds
    End Function
    Public Function GetStudentRecords(ByVal ds As DataSet) As DataSet
        Dim db As New DataAccess
        Dim da As New OleDbDataAdapter
        Dim strSQL As New StringBuilder

        With strSQL
            .Append(" SELECT StudentId, FirstName, LastName, DOB, SSN")
            .Append(" FROM arStudent")
        End With
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "StudentRecords")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try

        db.ClearParameters()

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        strSQL.Remove(0, strSQL.Length)

        ' Return the dataset
        Return ds
    End Function
    Public Function GetLeadRecords(ByVal ds As DataSet) As DataSet
        Dim db As New DataAccess
        Dim dt As New DataTable
        Dim da As New OleDbDataAdapter
        Dim strSQL As New StringBuilder

        With strSQL
            .Append(" SELECT LeadId, FirstName, LastName, DOB, SSN")
            .Append(" FROM adLeads")
        End With
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "LeadRecords")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try

        db.ClearParameters()

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        strSQL.Remove(0, strSQL.Length)

        ' Return the dataset
        Return ds
    End Function
End Class
