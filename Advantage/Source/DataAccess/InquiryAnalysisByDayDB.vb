Imports FAME.Advantage.Common

Public Class InquiryAnalysisByDayDB

    Public Function GetLeadsByDay(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "ORDER BY " & paramInfo.OrderBy
        End If

        With sb
            .Append("SELECT")
            .Append("       adSourceCatagory.SourceCatagoryDescrip,")
            .Append("       (SELECT X.SourceTypeDescrip")
            .Append("        FROM   adSourceType X")
            .Append("        WHERE  X.SourceTypeID=adLeads.SourceTypeID) AS SourceTypeDescrip,")
            .Append("       (SELECT adSourceAdvertisement.sourceadvdescrip ")
            .Append("        FROM   adSourceAdvertisement")
            .Append("        WHERE 	SourceAdvId=adLeads.SourceAdvertisement) AS SourceAdvDescrip,")
            .Append("       adLeads.SourceDate, ")
            .Append("       COUNT(*) AS Inquiries ")
            .Append("FROM   adLeads,syCmpGrpCmps,syCampGrps,syCampuses,adSourceCatagory ")
            .Append("WHERE  syCampuses.CampusId=adLeads.CampusId")
            .Append("       AND syCmpGrpCmps.CampusId=adLeads.CampusId")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId")
            .Append("       AND adLeads.SourceDate IS NOT NULL")
            .Append("       AND adLeads.SourceCategoryID=adSourceCatagory.SourceCatagoryId")
            .Append(strWhere)
            .Append("GROUP BY 	adSourceCatagory.SourceCatagoryDescrip,adLeads.SourceCategoryID,")
            .Append("           adLeads.SourceTypeID,adLeads.SourceAdvertisement,adLeads.SourceDate	")
            .Append("ORDER BY   adSourceCatagory.SourceCatagoryDescrip,SourceTypeDescrip,SourceAdvDescrip,adLeads.SourceDate")
            .Append(strOrderBy)


            '.Append("SELECT adSourceCatagory.SourceCatagoryDescrip,adSourceType.SourceTypeDescrip,")
            '.Append("adSourceAdvertisement.sourceadvdescrip,adLeads.SourceDate, COUNT(*) AS Inquiries ")
            '.Append("FROM adSourceAdvertisement,adSourceType,adSourceCatagory,adLeads,syCmpGrpCmps,syCampGrps,syCampuses ")
            '.Append("WHERE adLeads.SourceAdvertisement=adSourceAdvertisement.sourceadvid ")
            '.Append("AND adSourceAdvertisement.sourcetypeid=adSourceType.SourceTypeID ")
            '.Append("AND adSourceType.SourceCatagoryID=adSourceCatagory.SourceCatagoryId ")
            '.Append("AND syCmpGrpCmps.CampusId = adLeads.CampusId ")
            '.Append("AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
            '.Append("AND syCampuses.CampusId=adLeads.CampusId ")
            '.Append("AND SourceDate IS NOT NULL ")
            '.Append(strWhere)
            '.Append("GROUP BY adSourceCatagory.SourceCatagoryDescrip,adSourceType.SourceTypeDescrip,")
            '.Append("adSourceAdvertisement.sourceadvdescrip,adLeads.SourceDate ")
            '.Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)
        ds.Tables(0).TableName = "InquiryByDay"

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function


    Public Function GetDailyInquiryByMonth(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        With sb
            'query with subquery
            .Append("SELECT ")
            .Append("       adLeads.SourceDate AS InquiryDate,adLeads.LastName,adLeads.FirstName,adLeads.MiddleName,")
            .Append("       adLeads.Zip,adLeads.ForeignZip,adLeads.LeadStatus,syStatusCodes.StatusCodeDescrip,")
            .Append("       adLeads.SourceAdvertisement AS AdvertisementId,")
            .Append("       (SELECT sourceadvdescrip FROM adSourceAdvertisement ")
            .Append("       WHERE sourceadvid=adLeads.SourceAdvertisement) AS AdvertisementDescrip,")
            .Append("       syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,")
            .Append("       adLeads.CampusId,syCampuses.CampDescrip ")
            .Append("FROM   adLeads, syStatusCodes, syCmpGrpCmps, syCampGrps, syCampuses ")
            .Append("WHERE  adLeads.LeadStatus = syStatusCodes.StatusCodeId ")
            .Append("       AND syCmpGrpCmps.CampusId = adLeads.CampusId ")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
            .Append("       AND syCampuses.CampusId=adLeads.CampusId ")
            .Append(strWhere)
            .Append("ORDER BY adLeads.SourceDate")
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "DailyInquiryStatusByMonth"
            'Add new columns
            ds.Tables(0).Columns.Add(New DataColumn("Year", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("Month", System.Type.GetType("System.Byte")))
            ds.Tables(0).Columns.Add(New DataColumn("MonthLabel", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("LeadName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("LeadCount", System.Type.GetType("System.Int32")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

End Class
