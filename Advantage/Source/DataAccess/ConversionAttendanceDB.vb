Imports FAME.Advantage.Common

Public Class ConversionAttendanceDB
    Public Function GetEnrollmentAttendance(ByVal stuEnrollId As String, ByVal startDate As String, ByVal endDate As String) As DataTable
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append("SELECT MeetDate,Actual,Tardy,Excused ")
            .Append("FROM atConversionAttendance ")
            .Append("WHERE StuEnrollId = ? ")
            .Append("AND MeetDate BETWEEN ? AND ? ")
            .Append("ORDER BY MeetDate ")
        End With

        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sdate", startDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@edate", endDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

        '   return DataTable
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function

    Public Function GetEnrollmentTotalAttendanceHours(ByVal stuEnrollId As String, ByVal startDate As String, ByVal endDate As String) As Decimal
        Dim db As New DataAccess
        Dim obj As System.Object

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append("SELECT SUM(Actual) ")
            .Append("FROM atConversionAttendance ")
            .Append("WHERE StuEnrollId = ? ")
            .Append("AND MeetDate BETWEEN ? AND ? ")
        End With

        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sdate", startDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@edate", endDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

        '   return DataTable
        obj = db.RunParamSQLScalar(sb.ToString)

        If obj.ToString = "" Then
            Return 0.0
        Else
            Return CDbl(obj)
        End If
    End Function
End Class
