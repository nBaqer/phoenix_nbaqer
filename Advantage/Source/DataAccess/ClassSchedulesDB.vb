Imports FAME.Advantage.Common

Public Class ClassSchedulesDB
    Public Function GetMeetings(ByVal StartDate As String, ByVal EndDate As String, ByVal CampusId As String, ByVal InstructorId As String) As DataTable
        Dim dt As New DataTable
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("select ")
            .Append("   csm.StartDate, ")
            .Append("   csm.EndDate, ")
            .Append("   wd.WorkDaysDescrip, ")
            .Append("   ti.TimeIntervalDescrip as StartTime, ")
            .Append("   ti2.TimeIntervalDescrip as EndTime, ")
            .Append("   DATEDIFF ( minute , ti.TimeIntervalDescrip , ti2.TimeIntervalDescrip ) as Minutes, ")
            .Append("   sf.ShiftDescrip, ")
            .Append("   (rq.Code + '' + tm.TermDescrip) as CodeTerm, ")
            .Append("   (select us.FullName ")
            .Append("   from syUsers us, arClassSections cs2 ")
            .Append("   where us.UserId=cs2.InstructorId ")
            .Append("   and cs2.ClsSectionId=cs.ClsSectionId) as Instructor ")


            ''Modified by Saraswathi on Feb 18 2009
            .Append("   ,(select cs2.InstructorId ")
            .Append("   from arClassSections cs2 ")
            .Append("   where  ")
            .Append("  cs2.ClsSectionId=cs.ClsSectionId) as InstructorUIDS ")

            .Append("from arClassSections cs, arClsSectMeetings csm, arReqs rq, arTerm tm, arShifts sf, ")
            .Append("   syPeriods pd, syPeriodsWorkDays pwd, plWorkDays wd, cmTimeInterval ti, cmTimeInterval ti2 ")
            .Append("where cs.ClsSectionId=csm.ClsSectionId ")
            .Append("and cs.ReqId=rq.ReqId ")
            .Append("and cs.TermId=tm.TermId ")
            .Append("and cs.ShiftId=sf.ShiftId ")
            .Append("and csm.PeriodId=pd.PeriodId ")
            .Append("and pwd.PeriodId=pd.PeriodId ")
            .Append("and pwd.WorkDayId=wd.WorkDaysId ")
            .Append("and pd.StartTimeId=ti.TimeIntervalId ")
            .Append("and pd.EndTimeId=ti2.TimeIntervalId ")
            .Append("and ( ")
            .Append("       (csm.StartDate <= ? and csm.EndDate >= ?) ")
            .Append("               or ")
            .Append("       (csm.StartDate >= ? and csm.EndDate <= ?) ")
            .Append("               or ")
            .Append("       (csm.StartDate < ? and csm.EndDate >= ? and csm.EndDate <= ?) ")
            .Append("               or ")
            .Append("       (csm.StartDate > ? and csm.StartDate < ? and csm.EndDate >= ?) ")
            .Append("               or ")
            .Append("       (csm.StartDate >= ? and csm.StartDate <= ?) ")
            .Append("   ) ")

            If CampusId <> "" Then
                .Append("and cs.CampusId = ? ")
            End If

            If InstructorId <> "" Then
                .Append("and cs.InstructorId = ? ")
            End If
        End With

        db.AddParameter("sd1", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("ed1", EndDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("sd2", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("ed2", EndDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("sd3", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("sd30", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("ed3", EndDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("sd4", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("ed4", EndDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("ed5", EndDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("sd6", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("ed6", EndDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

        If CampusId <> "" Then
            db.AddParameter("cmpid", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        If InstructorId <> "" Then
            db.AddParameter("instrid", InstructorId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        Return (db.RunParamSQLDataSet(sb.ToString)).Tables(0)


    End Function

    Public Function GetInstructorHours(ByVal StartDate As String, ByVal EndDate As String, ByVal CampusId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("select ")
            .Append("   Instructor, ")
            .Append("   count(StartDate) as Classes, ")
            .Append("   sum(Hours) as Hours ")
            .Append("from ")
            .Append("(")
            .Append("select ")
            .Append("   csm.StartDate, ")
            .Append("   csm.EndDate, ")
            .Append("   wd.WorkDaysDescrip, ")
            .Append("   ti.TimeIntervalDescrip as StartTime, ")
            .Append("   ti2.TimeIntervalDescrip as EndTime, ")
            .Append("   DATEDIFF ( Hour , ti.TimeIntervalDescrip , ti2.TimeIntervalDescrip ) as Hours, ")
            .Append("   sf.ShiftDescrip, ")
            .Append("   (rq.Code + '' + tm.TermDescrip) as CodeTerm, ")
            .Append("   (select us.FullName ")
            .Append("   from syUsers us, arClassSections cs2 ")
            .Append("   where us.UserId=cs2.InstructorId ")
            .Append("   and cs2.ClsSectionId=cs.ClsSectionId) as Instructor ")
            .Append("from arClassSections cs, arClsSectMeetings csm, arReqs rq, arTerm tm, arShifts sf, ")
            .Append("   syPeriods pd, syPeriodsWorkDays pwd, plWorkDays wd, cmTimeInterval ti, cmTimeInterval ti2 ")
            .Append("where cs.ClsSectionId=csm.ClsSectionId ")
            .Append("and cs.ReqId=rq.ReqId ")
            .Append("and cs.TermId=tm.TermId ")
            .Append("and cs.ShiftId=sf.ShiftId ")

            If CampusId <> "" Then
                .Append("and cs.CampusId = ? ")
            End If
            .Append("and csm.PeriodId=pd.PeriodId ")
            .Append("and pwd.PeriodId=pd.PeriodId ")
            .Append("and pwd.WorkDayId=wd.WorkDaysId ")
            .Append("and pd.StartTimeId=ti.TimeIntervalId ")
            .Append("and pd.EndTimeId=ti2.TimeIntervalId ")
            .Append("and ( ")
            .Append("       (csm.StartDate <= ? and csm.EndDate >= ?) ")
            .Append("               or ")
            .Append("       (csm.StartDate >= ? and csm.EndDate <= ?) ")
            .Append("   ) ")
            .Append(") R ")
            .Append("group by Instructor ")
            .Append("order by Instructor ")
        End With

        If CampusId <> "" Then
            db.AddParameter("cmpid", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        db.AddParameter("sd1", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("ed1", EndDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("sd2", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("ed2", EndDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

        Return (db.RunParamSQLDataSet(sb.ToString)).Tables(0)

    End Function
    Public Function isDayMarkedAsHoliday(ByVal dtInputDate As Date, Optional ByVal CampusId As String = "") As Boolean
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim intIsDayAHoliday As Integer = 0

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim row() As DataRow
        Dim dt As New DataTable

        dt = GetDaysMarkedAsHoliday(CampusId)
        row = dt.Select("dtHoliday='" & dtInputDate & "'")
        If row.Length >= 1 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function GetHolidaysAfterLOA(ByVal dtLOA As Date, ByVal reportDate As Date, _
                                        ByVal intMaxLimit As Integer, _
                                        ByVal intLOACase As Integer, _
                                        Optional ByVal CampusId As String = "") As Integer
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim intIsDayAHoliday As Integer = 0
        Dim dtm As DateTime
        Dim counter As Integer
        Dim row() As DataRow
        Dim dt As New DataTable
        Dim intCountOfDays As Integer = 0

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        dt = GetDaysMarkedAsHoliday(CampusId)

        Select Case intLOACase
            Case 2
                'the LOA Date falls before start date but ends before the report date
                'in this case the days from LOA End Date to report date is 
                'considered as the actual days attended by student.  

                'now code needs to check how many days are marked as holiday
                dtm = Convert.ToDateTime(dtLOA)
                For counter = 1 To intMaxLimit
                    dtm = dtm.AddDays(1)
                    row = dt.Select("dtHoliday='" & dtm & "'")
                    If row.Length >= 1 Then
                        intCountOfDays += 1
                    End If
                    If dtm = Convert.ToDateTime(reportDate) Then
                        Exit For
                    End If
                Next
            Case 4
                ' the LOA Date falls after start date but ends after the report date
                ' in this case the days from student start date to LOA start date is 
                'considered
                dtm = Convert.ToDateTime(reportDate)
                For counter = 1 To intMaxLimit
                    dtm = dtm.AddDays(1)
                    row = dt.Select("dtHoliday='" & dtm & "'")
                    If row.Length >= 1 Then
                        intCountOfDays += 1
                    End If
                    If dtm = Convert.ToDateTime(dtLOA) Then
                        Exit For
                    End If
                Next

        End Select

        Return intCountOfDays
    End Function
    Public Function GetHolidaysAfterLOAStartDateAndLOAEndDate(ByVal dtLOA As Date, ByVal dtStartDate As Date, _
                                        ByVal intMaxLimit As Integer, _
                                        ByVal dtLOAEndDate As Date, _
                                        ByVal dtEndDate As Date, _
                                        ByVal intMaxLimitByLOAEndDate As Integer, _
                                        Optional ByVal CampusId As String = "") As Integer
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim intIsDayAHoliday As Integer = 0
        Dim dtm As DateTime
        Dim counter As Integer
        Dim row() As DataRow
        Dim dt As New DataTable
        Dim intCountOfDays As Integer = 0

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        dt = GetDaysMarkedAsHoliday(CampusId)


        'the LOA Date falls between start date and report date
        'now code needs to check how many days are marked as holiday
        'in this date range

        dtm = Convert.ToDateTime(dtStartDate)
        For counter = 1 To intMaxLimit
            dtm = dtm.AddDays(1)
            row = dt.Select("dtHoliday='" & dtm & "'")
            If row.Length >= 1 Then
                intCountOfDays += 1
            End If
            If dtm = Convert.ToDateTime(dtLOA) Then
                Exit For
            End If
        Next

        dtm = Convert.ToDateTime(dtLOAEndDate)
        For counter = 1 To intMaxLimitByLOAEndDate
            dtm = dtm.AddDays(1)
            row = dt.Select("dtHoliday='" & dtm & "'")
            If row.Length >= 1 Then
                intCountOfDays += 1
            End If
            If dtm = Convert.ToDateTime(dtEndDate) Then
                Exit For
            End If
        Next
        Return intCountOfDays
    End Function
    Public Function GetDaysMarkedAsHoliday(Optional ByVal CampusId As String = "") As DataTable
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim intIsDayAHoliday As Integer = 0
        Dim arrDates As New ArrayList
        Dim dtm As DateTime
        Dim counter As Integer
        Dim Rows() As DataRow
        Dim numDays As Integer
        Dim boolIsDayMarkedAsHoliday As Boolean = False
        Dim dsDates As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append(" select Distinct H.HolidayStartDate,H.HolidayEndDate,DateDiff(day,H.HolidayStartDate,H.HolidayEndDate)+1 as NumberofDays from syHolidays H ")
            ''.Added by Sarswathi lakshmanan to fix issue 15378
            .Append(" ,syStatuses S WHERE ")
            .Append(" H.StatusId=S.StatusId and Status like 'Active' ")
            ''Added by Saraswathi lakshmanan on August 21 2009
            ''partial Holidays are not taken into considewration in Instructor Hours and Class Schedules page.
            ''For mantis case : 15387: Bug: Partial holidays are not taken into account on the Instructor hours and Class schedules page. 
            ''If the day is a all day holiday then add them to the list of holidays.
            .Append("And H.AllDay=1 ")

            If CampusId <> "" Then
                .Append(" AND (H.CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId = '")
                .Append(CampusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR     H.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
        End With
        dsDates = db.RunSQLDataSet(sb.ToString)

        Dim dt As New DataTable("HolidayDates")
        Dim row As DataRow
        dt.Columns.Add(New DataColumn("dtHoliday", System.Type.GetType("System.DateTime")))

        For Each dr As DataRow In dsDates.Tables(0).Rows
            If Not dr("HolidayStartDate") Is System.DBNull.Value And Not dr("HolidayEndDate") Is System.DBNull.Value Then
                If Convert.ToDateTime(dr("HolidayStartDate")) <= Convert.ToDateTime(dr("HolidayEndDate")) Then
                    dtm = Convert.ToDateTime(dr("HolidayStartDate"))
                    row = dt.NewRow()
                    row("dtHoliday") = dtm
                    dt.Rows.Add(row)
                    If Convert.ToDateTime(dr("HolidayStartDate")) <> Convert.ToDateTime(dr("HolidayEndDate")) Then
                        Dim intMaxLimit As Integer = 0
                        intMaxLimit = CType(dr("NumberofDays"), Integer)
                        If intMaxLimit > 1 Then
                            ''Modified by saraswathi on June 10 2009
                            ''Since, it is adding the number of days to the holidays, which is giving holiday days +1. 
                            ''Mantis Case 16405
                            For counter = 1 To intMaxLimit - 1
                                'For counter = 1 To intMaxLimit 
                                dtm = dtm.AddDays(1)
                                row = dt.NewRow()
                                row("dtHoliday") = dtm
                                dt.Rows.Add(row)
                                If dtm = Convert.ToDateTime(dr("HolidayEndDate")) Then
                                    Exit For
                                End If
                            Next
                        End If
                    End If
                End If
            End If
        Next
        Return dt
    End Function

    ''Added by Saraswathi lakshmanan on August 21 2009
    ''Find the Partial Holidays between the given dates.
    ''To fix issue 15387: Bug: Partial holidays are not taken into account on the Instructor hours and Class schedules page. 

    Public Function GetPartialHolidays(ByVal StartDate As DateTime, ByVal EndDate As DateTime, ByVal CampusId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim dsDates As DataSet
        ''Get the partial Holidays alone

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append(" select Distinct H.HolidayStartDate,H.HolidayEndDate,convert(varchar,CT1.TimeINtervalDEscrip,108) as StartTime,convert(varchar,CT2.TimeINtervalDEscrip,108) as EndTime,DateDiff(day,H.HolidayStartDate,H.HolidayEndDate)+1 as NumberofDays from syHolidays H ")
            .Append(" ,syStatuses S,cmTimeInterval CT1,cmTimeInterval CT2 WHERE ")
            .Append(" H.StatusId=S.StatusId and Status like 'Active' ")
            .Append("And H.AllDay=0 ")
            .Append(" and CT1.TimeINtervalId=StartTimeId and Ct2.TimeIntervalId=EndTimeID ")
            .Append(" and convert(varchar,HolidayStartDate,101) >=? and convert(varchar,HolidayEndDate,101)<=? ")
            If CampusId <> "" Then
                .Append(" AND (H.CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId = '")
                .Append(CampusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR     H.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
        End With
        db.AddParameter("sd1", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("ed1", EndDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)


        dsDates = db.RunParamSQLDataSet(sb.ToString)
        Dim i As Integer
        Dim dtTemp As New DataTable
        Dim row As DataRow
        dtTemp.Columns.Add(New DataColumn("HolidayDate", System.Type.GetType("System.DateTime")))
        dtTemp.Columns.Add(New DataColumn("StartTime", System.Type.GetType("System.DateTime")))
        dtTemp.Columns.Add(New DataColumn("EndTime", System.Type.GetType("System.DateTime")))
        ''Create a new table for all the dates in the partial holiday table.
        ''if the partial holiday has 3 days scheduled for holiday in the noon. Then we  are adding three dates and their timimng , instead of one record with start and end Dates, Three records will exists each denoting one day.

        If dsDates.Tables(0).Rows.Count > 0 Then
            For i = 0 To dsDates.Tables(0).Rows.Count - 1
                If CType(dsDates.Tables(0).Rows(i)("NumberofDays"), Integer) = 1 Then
                    row = dtTemp.NewRow()
                    row("HolidayDate") = dsDates.Tables(0).Rows(i)("HolidayStartDate")
                    row("StartTime") = dsDates.Tables(0).Rows(i)("StartTime")
                    row("EndTime") = dsDates.Tables(0).Rows(i)("EndTime")
                    dtTemp.Rows.Add(row)

                ElseIf CType(dsDates.Tables(0).Rows(i)("NumberofDays"), Integer) > 1 Then
                    Dim Icount As Integer
                    For Icount = 0 To CType(dsDates.Tables(0).Rows(i)("NumberofDays"), Integer) - 1
                        row = dtTemp.NewRow()
                        row("HolidayDate") = DateAdd(DateInterval.Day, Icount, CType(dsDates.Tables(0).Rows(i)("HolidayStartDate"), Date))
                        row("StartTime") = dsDates.Tables(0).Rows(i)("StartTime")
                        row("EndTime") = dsDates.Tables(0).Rows(i)("EndTime")
                        dtTemp.Rows.Add(row)
                    Next

                End If
            Next

        End If

        dtTemp.AcceptChanges()

        Return dtTemp

    End Function
End Class
