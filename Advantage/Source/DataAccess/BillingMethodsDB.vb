Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' BillingMethodsDB.vb
'
' BillingMethodsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class BillingMethodsDB
    Public Function GetAllBillingMethods(ByVal showActiveOnly As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT BM.BillingMethodId ")
            .Append("     , BM.StatusId ")
            .Append("     , BM.BillingMethodCode ")
            .Append("     , BM.BillingMethodDescrip ")
            .Append("     , BM.BillingMethod ")
            .Append("     , BM.AutoBill, ST.StatusId, ST.Status ")
            .Append("FROM     saBillingMethods BM, syStatuses ST ")
            .Append("WHERE    BM.StatusId = ST.StatusId ")
            If showActiveOnly = "True" Then
                .Append("AND    ST.Status = 'Active' ")
                .Append("ORDER BY BM.BillingMethodDescrip ")
            ElseIf showActiveOnly = "False" Then
                .Append("AND    ST.Status = 'Inactive' ")
                .Append("ORDER BY BM.BillingMethodDescrip ")
            Else
                .Append("ORDER BY ST.Status,BM.BillingMethodDescrip asc")
            End If
        End With
        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetBillingMethodInfo(ByVal BillingMethodId As String) As BillingMethodInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT BM.BillingMethodId ")
            .Append("     , BM.BillingMethodCode ")
            .Append("     , BM.StatusId ")
            .Append("     , (Select Status from syStatuses where StatusId=BM.StatusId) As Status ")
            .Append("     , BM.BillingMethodDescrip ")
            .Append("     , BM.CampGrpId ")
            .Append("     , (Select CampGrpDescrip from syCampGrps where CampGrpId=BM.CampGrpId) As CampGrpDescrip ")
            .Append("     , BM.BillingMethod ")
            .Append("     , BM.AutoBill ")
            .Append("     , BM.ModUser ")
            .Append("     , BM.ModDate ")
            .Append("     , CASE WHEN (SELECT COUNT(APV.PrgVerId) FROM arPrgVersions AS APV WHERE CONVERT(VARCHAR(50), APV.BillingMethodId) =  BM.BillingMethodId) > 0 ")
            .Append("               THEN 1 ")
            .Append("               ELSE 0 ")
            .Append("         END AS  BillingMethodInUse ")
            .Append("     , CASE WHEN (SELECT COUNT(APV.PrgVerId) FROM arPrgVersions AS APV WHERE CONVERT(VARCHAR(50), APV.BillingMethodId) =  BM.BillingMethodId) > 0 ")
            .Append("               THEN 1 ")
            .Append("               ELSE 0 ")
            .Append("       END AS  IncrementTypeInUse ")
            .Append("FROM  saBillingMethods BM ")
            .Append("WHERE BM.BillingMethodId = ? ")
        End With

        ' Add the BillingMethodId to the parameter list
        db.AddParameter("@BillingMethodId", BillingMethodId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim BillingMethodInfo As New BillingMethodInfo

        While dr.Read()

            '   set properties with data from DataReader
            With BillingMethodInfo
                .BillingMethodId = BillingMethodId
                .IsInDB = True
                .BillingMethodInUse = dr("BillingMethodInUse")
                .IncrementTypeInUse = dr("IncrementTypeInUse")
                .Code = dr("BillingMethodCode")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Description = dr("BillingMethodDescrip")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                .BillingMethod = dr("BillingMethod")
                .AutoBill = dr("AutoBill")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BillingMethodInfo
        Return BillingMethodInfo

    End Function
    Public Function UpdateBillingMethodInfo(ByVal BillingMethodInfo As BillingMethodInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saBillingMethods Set BillingMethodId = ?, BillingMethodCode = ? ")
                .Append(" , StatusId = ?, BillingMethodDescrip = ?, CampGrpId = ? ")
                .Append(" , BillingMethod = ?, AutoBill = ? ")
                .Append(" , ModUser = ?, ModDate = ? ")
                .Append("WHERE BillingMethodId = ? ")
                '.Append("AND ModDate = ? ;")
                .Append("Select count(*) from saBillingMethods where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   BillingMethodId
            db.AddParameter("@BillingMethodId", BillingMethodInfo.BillingMethodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Billing Method Code
            db.AddParameter("@BillingMethodCode", BillingMethodInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", BillingMethodInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   BillingMethodDescrip
            db.AddParameter("@BillingMethodDescrip", BillingMethodInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If BillingMethodInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", BillingMethodInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Billing Method
            db.AddParameter("@BillingMethod", BillingMethodInfo.BillingMethod, DataAccess.OleDbDataType.OleDbInteger, 2, ParameterDirection.Input)

            '   Auto Bill
            db.AddParameter("@AutoBill", BillingMethodInfo.AutoBill, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   BillingMethodId
            db.AddParameter("@Original_BillingMethodId", BillingMethodInfo.BillingMethodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            'db.AddParameter("@Original_ModDate", BillingMethodInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddBillingMethodInfo(ByVal BillingMethodInfo As BillingMethodInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saBillingMethods ")
                .Append(" ( BillingMethodId, BillingMethodCode, StatusId  ")
                .Append(" , BillingMethodDescrip, CampGrpId ")
                .Append(" , BillingMethod, AutoBill ")
                .Append(" , ModUser, ModDate )")
                .Append("VALUES (?,?,?,?,?,?, ?, ?, ? ) ")
            End With

            '   add parameters values to the query

            '   BillingMethodId
            db.AddParameter("@BillingMethodId", BillingMethodInfo.BillingMethodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   BillingMethodCode
            db.AddParameter("@BillingMethodCode", BillingMethodInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", BillingMethodInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   BillingMethodDescrip
            db.AddParameter("@BillingMethodDescrip", BillingMethodInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If BillingMethodInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", BillingMethodInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Billing Method
            db.AddParameter("@BillingMethod", BillingMethodInfo.BillingMethod, DataAccess.OleDbDataType.OleDbInteger, 2, ParameterDirection.Input)

            '   Auto Bill
            db.AddParameter("@AutoBill", BillingMethodInfo.AutoBill, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteBillingMethodInfo(ByVal BillingMethodId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saBillingMethods ")
                .Append("WHERE BillingMethodId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM saBillingMethods WHERE BillingMethodId = ? ")
            End With

            '   add parameters values to the query

            '   BillingMethodId
            db.AddParameter("@BillingMethodId", BillingMethodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   BillingMethodId
            db.AddParameter("@OriginalBillingMethodId", BillingMethodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetIncrementInfo(ByVal BillingMethodId As String) As IncrementInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT I.IncrementId ")
            .Append("     , I.BillingMethodId ")
            .Append("     , I.IncrementType ")
            .Append("     , I.IncrementName ")
            .Append("     , I.EffectiveDate ")
            .Append("     , I.ExcAbscenesPercent ")
            .Append("     , I.ModUser ")
            .Append("     , I.ModDate ")
            .Append("FROM  saIncrements AS I ")
            .Append("WHERE I.BillingMethodId = ? ")
        End With

        ' Add the BillingMethodId to the parameter list
        db.AddParameter("@BillingMethodId", BillingMethodId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim IncrementInfo As New IncrementInfo

        While dr.Read()

            '   set properties with data from DataReader
            With IncrementInfo
                .IsInDB = True
                .IncrementId = CType(dr("IncrementId"), Guid).ToString()
                .BillingMethodId = BillingMethodId
                If (dr("IncrementType") Is System.DBNull.Value) Then
                    .IncrementType = AdvantageV1.Common.IncrementType.NotApply
                Else
                    .IncrementType = dr("IncrementType")
                End If
                If Not (dr("EffectiveDate") Is System.DBNull.Value) Then
                    .EffectiveDate = dr("EffectiveDate")
                Else
                    .EffectiveDate = DateTime.MinValue
                End If
                If Not (dr("ExcAbscenesPercent") Is System.DBNull.Value) Then
                    .ExcAbsencesPercent = dr("ExcAbscenesPercent")
                Else
                    .ExcAbsencesPercent = 0.0
                End If
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return IncrementInfo
        Return IncrementInfo

    End Function
    Public Function UpdateIncrementInfo(ByVal IncrementInfo As IncrementInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE I ")
                .Append(" Set I.IncrementId = ? ")
                .Append("   , I.BillingMethodId = ? ")
                .Append("   , I.IncrementType = ? ")
                .Append("   , I.IncrementName = ? ")
                .Append("   , I.EffectiveDate = ? ")
                .Append("   , I.ExcAbscenesPercent = ? ")
                .Append("   , I.ModUser = ? ")
                .Append("   , I.ModDate = ? ")
                .Append("FROM  saIncrements AS I ")
                .Append("WHERE I.BillingMethodId = ? ; ")
                '                .Append("AND I.ModDate = ? ;")
                .Append("SELECT COUNT(*)  FROM saIncrements AS I  WHERE I.ModDate = ? ")
            End With

            '   add parameters values to the query

            '   IncrementId
            db.AddParameter("@IncrementId", IncrementInfo.IncrementId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Billing Method Code
            db.AddParameter("@BillingMethodId", IncrementInfo.BillingMethodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Increment Type
            db.AddParameter("@IncrementType", IncrementInfo.IncrementType, DataAccess.OleDbDataType.OleDbInteger, 2, ParameterDirection.Input)

            '   Increment Name
            db.AddParameter("@IncrementName", IncrementInfo.IncrementName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Effective Date
            If (IncrementInfo.EffectiveDate = DateTime.MinValue) Then
                db.AddParameter("@EffectiveDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@EffectiveDate", IncrementInfo.EffectiveDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            '   Excused Abscenes Percent

            db.AddParameter("@ExcAbscenesPercent", IncrementInfo.ExcAbsencesPercent, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   Billing Method Id for where
            db.AddParameter("@Original_BillingMethodId", IncrementInfo.BillingMethodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            'db.AddParameter("@Original_ModDate", IncrementInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddIncrementInfo(ByVal IncrementInfo As IncrementInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saIncrements ")
                .Append(" ( IncrementId ")
                .Append(" , BillingMethodId ")
                .Append(" , IncrementType ")
                .Append(" , IncrementName ")
                .Append(" , EffectiveDate ")
                .Append(" , ExcAbscenesPercent ")
                .Append(" , ModUser, ModDate ) ")
                .Append("VALUES (?, ?, ?, ?, ?, ?, ?, ? ) ")
            End With

            '   add parameters values to the query

            '   IncrementId
            db.AddParameter("@IncrementId", IncrementInfo.IncrementId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   BillingMethodCode
            db.AddParameter("@BillingMethodId", IncrementInfo.BillingMethodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Increment Type
            db.AddParameter("@IncrementType", IncrementInfo.IncrementType, DataAccess.OleDbDataType.OleDbInteger, 2, ParameterDirection.Input)

            '   Increment Name
            db.AddParameter("@IncrementName", IncrementInfo.IncrementName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Effective Date
            If (IncrementInfo.EffectiveDate = DateTime.MinValue) Then
                db.AddParameter("@EffectiveDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@EffectiveDate", IncrementInfo.EffectiveDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            '   Excused Abscenes Percent
            db.AddParameter("@ExcAbscenesPercent", IncrementInfo.ExcAbsencesPercent, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteIncrementInfo(ByVal BillingMethodId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saIncrements ")
                .Append("WHERE BillingMethodId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM saIncrements WHERE BillingMethodId = ? ")
            End With

            '   add parameters values to the query

            '   BillingMethodId
            db.AddParameter("@BillingMethodId", BillingMethodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   BillingMethodId
            db.AddParameter("@OriginalBillingMethodId", BillingMethodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function CheckIfPrgVersionsHaveABillingMethodId(ByVal BillingMethodId As String) As Boolean

        Dim ExistPrgVersionsWithGivenBillingMethodId = False

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT COUNT(APV.PrgVerId) ")
            .Append("FROM arPrgVersions AS APV ")
            .Append("WHERE APV.BillingMethodId = ? ")
        End With

        ' Add the BillingMethodId to the parameter list
        db.AddParameter("@BillingMethodId", BillingMethodId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim Qty As Integer = Int32.Parse(db.RunParamSQLScalar(sb.ToString))
        If (Qty > 0) Then
            ExistPrgVersionsWithGivenBillingMethodId = True
        Else
            ExistPrgVersionsWithGivenBillingMethodId = False
        End If

        Return ExistPrgVersionsWithGivenBillingMethodId
    End Function
    Public Function GetPmtPeriods(ByVal IncrementId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("Select PP.PmtPeriodId ")
            .Append("     , PP.IncrementId ")
            .Append("     , PP.PeriodNumber ")
            .Append("     , PP.IncrementValue ")
            .Append("     , PP.CumulativeValue ")
            .Append("     , PP.ChargeAmount ")
            .Append("     , PP.IsCharged ")
            .Append("     , PP.ModUser ")
            .Append("     , PP.ModDate ")
            .Append("     , PP.TransactionCodeId ")
            .Append("     , T.TransCodeDescrip ")
            .Append("FROM saPmtPeriods AS PP INNER JOIN saTransCodes AS T ON T.TransCodeId = PP.transactionCodeId ")
            .Append("WHERE PP.IncrementId = ? ")
            .Append("ORDER BY PP.PeriodNumber,T.TransCodeDescrip ")
        End With

        ' Add the BillingMethodId to the parameter list
        db.AddParameter("@IncrementId", IncrementId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        'Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        'While dr.Read()

        '    '   set properties with data from DataReader
        '    With PmtPeriodsIncrementsInfo
        '        .PmtPeriodsIncrementId = CType(dr("PmtPeriodsIncrementId"), Guid).ToString()
        '        .BillingMethodId = CType(dr("BillingMethodId"), Guid).ToString()
        '        .PeriodNumber = dr("PeriodNumber")
        '        .IncrementType = dr("IncrementType")
        '        .IncrementValue = dr("IncrementValue")
        '        .ChargeAmount = dr("ChargeAmount")
        '        .ModUser = dr("ModUser")
        '        .ModDate = dr("ModDate")
        '    End With
        'End While

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function UpdatePmtPeriodInfo(ByVal PmtPeriodInfo As PmtPeriodInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE PP ")
                .Append("   SET PP.PmtPeriodId = ? ")
                .Append("     , PP.IncrementId = ? ")
                .Append("     , PP.PeriodNumber = ? ")
                .Append("     , PP.IncrementValue = ? ")
                .Append("     , PP.CumulativeValue= ? ")
                .Append("     , PP.ChargeAmount = ? ")
                .Append("     , PP.IsCharged = ? ")
                .Append("     , PP.ModUser = ? ")
                .Append("     , PP.ModDate = ? ")
                .Append("     , PP.TransactionCodeId = ? ")
                .Append("FROM saPmtPeriods AS PP ")
                .Append("WHERE PP.PmtPeriodId = ? ")
                .Append("SELECT count(*) FROM saPmtPeriods AS PP WHERE PP.ModDate = ? ")
            End With

            '   add parameters values to the query
            '   PmtPeriodsIncrementId
            db.AddParameter("@PmtPeriodId", PmtPeriodInfo.PmtPeriodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   PmtPeriodsIncrementId
            db.AddParameter("@IncrementId", PmtPeriodInfo.IncrementId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   PeriodNumber
            db.AddParameter("@PeriodNumber", PmtPeriodInfo.PeriodNumber, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   IncrementValue
            db.AddParameter("@IncrementValue", PmtPeriodInfo.IncrementValue, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            '   CumulativeValue
            db.AddParameter("@CumulativeValue", PmtPeriodInfo.CumulativeValue, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            '   ChargeAmount
            db.AddParameter("@ChargeAmount", PmtPeriodInfo.ChargeAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            '   Is Charged
            db.AddParameter("@IsCharged", PmtPeriodInfo.IsCharged, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            ' transaction code id
            db.AddParameter("@TransactionCodeId", PmtPeriodInfo.TransactionCodeId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)

            '   Where parameter PmtPeriodsIncrementId
            db.AddParameter("@Original_PmtPeriodId", PmtPeriodInfo.PmtPeriodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate  PARAMETER FOR  select count query
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)


            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddPmtPeriodInfo(ByVal PmtPeriodInfo As PmtPeriodInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saPmtPeriods ")
                .Append("     ( PmtPeriodId ")
                .Append("     , IncrementId ")
                .Append("     , PeriodNumber ")
                .Append("     , IncrementValue ")
                .Append("     , CumulativeValue ")
                .Append("     , ChargeAmount ")
                .Append("     , IsCharged ")
                .Append("     , ModUser")
                .Append("     , ModDate")
                .Append("     , TransactionCodeId")
                .Append("     ) ")
                .Append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ? )")
            End With

            '   add parameters values to the query


            '   PmtPeriodsIncrementId
            db.AddParameter("@PmtPeriodId", PmtPeriodInfo.PmtPeriodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   BillingMethodId
            db.AddParameter("@IncrementId", PmtPeriodInfo.IncrementId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   PeriodNumber
            db.AddParameter("@PeriodNumber", PmtPeriodInfo.PeriodNumber, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   IncrementValue
            db.AddParameter("@IncrementValue", PmtPeriodInfo.IncrementValue, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            '   CumulativeValue
            db.AddParameter("@CumulativeValue", PmtPeriodInfo.CumulativeValue, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            '   ChargeAmount
            db.AddParameter("@ChargeAmount", PmtPeriodInfo.ChargeAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            '   Is Charged
            db.AddParameter("@IsCharged", PmtPeriodInfo.IsCharged, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   transactionCodeId
            db.AddParameter("@TransactionCodeId", PmtPeriodInfo.TransactionCodeId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function UpdatePmtPeriodsDS(ByVal ds As DataSet) As String

        '   all updates must be encapsulated as one transaction

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            '   build the sql query for the adLeadGrpReqGroups data adapter
            Dim sb As New StringBuilder
            With sb
                '   with subqueries
                .Append("Select PP.PmtPeriodId ")
                .Append("     , PP.IncrementId ")
                .Append("     , PP.PeriodNumber ")
                .Append("     , PP.IncrementValue ")
                .Append("     , PP.CumulativeValue ")
                .Append("     , PP.ChargeAmount ")
                .Append("     , PP.IsCharged ")
                .Append("     , PP.ModUser ")
                .Append("     , PP.ModDate ")
                .Append("FROM saPmtPeriods AS PP ")
            End With

            '   build select command PmtPeriod
            Dim PmtPeriodsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle PmtPeriod table
            Dim PmtPeriodsDataAdapter As New OleDbDataAdapter(PmtPeriodsSelectCommand)

            '   build insert, update and delete commands for LeadGrpReqGroups table
            Dim cb As New OleDbCommandBuilder(PmtPeriodsDataAdapter)

            '   insert added rows in PmtPeriods table
            PmtPeriodsDataAdapter.Update(ds.Tables(0).Select(Nothing, Nothing, DataViewRowState.Added))

            '   delete rows in PmtPeriods table
            PmtPeriodsDataAdapter.Update(ds.Tables(0).Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   update rows in PmtPeriods table
            PmtPeriodsDataAdapter.Update(ds.Tables(0).Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))


            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            Return ""

        Catch ex As OleDb.OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Catch ex As Exception
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return ex.Message

        Finally

            '   close connection
            connection.Close()
        End Try
    End Function
    Public Function DeletePmtPeriodInfo(ByVal PmtPeriodId As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saPmtPeriods ")
                .Append("WHERE PmtPeriodId = ? ")
                .Append("SELECT count(*) FROM saPmtPeriods WHERE PmtPeriodId = ? ")
            End With

            '   add parameters values to the query
            '   PmtPeriodId
            db.AddParameter("@PmtPeriodId", PmtPeriodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '   parameter for select count
            db.AddParameter("@OrigialPmtPeriodId", PmtPeriodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function DeletePmtPeriodsForAIncrementId(ByVal IncrementId As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saPmtPeriods ")
                .Append("WHERE IncrementId = ? ")
                .Append("SELECT count(*) FROM saPmtPeriods WHERE IncrementId = ? ")
            End With

            '   add parameters values to the query
            '   PmtPeriodId
            db.AddParameter("@PmtPeriodId", IncrementId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '   parameter for select count
            db.AddParameter("@OrigialPmtPeriodId", IncrementId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return String.Empty
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function BillingMethodCheckForChargedPaymentPeriods(ByVal BillingMethodId As String) As Boolean
        'return boolean value
        Dim db As New SQLDataAccess
        Dim result As Boolean

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

        'add Billing MethodId  parameter

        db.AddParameter("@BillingMethodId", New Guid(BillingMethodId), SqlDbType.UniqueIdentifier, ParameterDirection.Input)

        Try
            result = CType(db.RunParamSQLScalar_SP("USP_BillingMethodCheckForChargedPaymentPeriods"), Boolean)
        Catch ex As System.Exception
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return result

    End Function
End Class
