Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SGR_4YR3SummaryObjectDB

	Public Shared Function GetReportDatasetRaw(ByVal RptParamInfo As ReportParamInfoIPEDS) As DataSet

		' get list of students to include, based on report parameters
		Dim StudentList As String = IPEDSDB.GetStudentList(RptParamInfo)

		' if there are no students to include, return empty dataset
		If StudentList = "" Then
			Return New DataSet
		End If

		Dim sb As New System.Text.StringBuilder
		Dim dsRaw As New DataSet

		With sb
			' get Students
			.Append("SELECT DISTINCT arStudent.StudentId, ")

			' retrieve all other needed columns
			.Append("(SELECT syRptAgencyFldValues.RptAgencyFldValId ")
			.Append("	FROM adGenders, ")
			.Append("        syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
			.Append("	WHERE adGenders.GenderId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("         syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("         syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("         syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("         syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
			.Append("	      adGenders.GenderId = arStudent.Gender) As RptAgencyFldValId_Gender, ")
			.Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
			.Append("	FROM adGenders, ")
			.Append("        syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
			.Append("	WHERE adGenders.GenderId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("         syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("         syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("         syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("         syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
			.Append("	      adGenders.GenderId = arStudent.Gender) As GenderDescrip, ")
			.Append("(SELECT syRptAgencyFldValues.RptAgencyFldValId ")
			.Append("	FROM adEthCodes, ")
			.Append("        syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
			.Append("	WHERE adEthCodes.EthCodeId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("         syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("         syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("         syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("         syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
			.Append("	      adEthCodes.EthCodeId = arStudent.Race) As RptAgencyFldValId_EthCode, ")
			.Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
			.Append("	FROM adEthCodes, ")
			.Append("        syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
			.Append("	WHERE adEthCodes.EthCodeId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("         syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("         syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("         syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("         syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
			.Append("	      adEthCodes.EthCodeId = arStudent.Race) As EthCodeDescrip ")
			.Append("FROM ")
			.Append("arStudent ")

			' append list of appropriate students to include
			.Append("WHERE arStudent.StudentId IN (" & StudentList & ") ")

			' apply sort on Gender and Race
			.Append("ORDER BY RptAgencyFldValId_Gender, RptAgencyFldValId_EthCode ")


			.Append(";")


			' get enrollments for same set of students
			.Append("SELECT ")
			.Append("arStuEnrollments.StuEnrollId, arStuEnrollments.StudentId, ")
			.Append("arStuEnrollments.EnrollDate, arStuEnrollments.ExpGradDate, ")
			.Append("arPrgVersions.Weeks, ")
			.Append("dbo.sySysStatus.SysStatusDescrip, ")
			.Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
			.Append("	FROM arDropReasons, ")
			.Append("        syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
			.Append("	WHERE arDropReasons.DropReasonId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("         syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("         syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("         syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("         syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
			.Append("	      arDropReasons.DropReasonId = arStuEnrollments.DropReasonId) As DropReasonDescrip ")
			.Append("FROM ")
            .Append("arStuEnrollments, arPrgVersions, arPrograms, syStatusCodes, sySysStatus ")
			.Append("WHERE ")
			.Append("arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId AND ")
            .Append("arPrgVersions.ProgId = arPrograms.ProgId AND ")
            .Append("arStuEnrollments.StatusCodeId = syStatusCodes.StatusCodeId AND ")
			.Append("syStatusCodes.SysStatusId = sySysStatus.SysStatusId AND ")

            ' filter on passed-in list of programs as selected by user
            .Append("arPrograms.ProgId IN (" & RptParamInfo.FilterProgramIDs & ") AND ")

            ' append list of appropriate students to include
			.Append("arStuEnrollments.StudentId IN (" & StudentList & ") ")
		End With

		' run queries, get raw DataSet
		dsRaw = IPEDSDB.DataAccessIPEDS().RunSQLDataSet(sb.ToString)

		' set table names for returned data
		With dsRaw
			.Tables(0).TableName = TblNameStudents
			.Tables(1).TableName = TblNameEnrollments
		End With

		' return the raw DataSet
		Return dsRaw

	End Function

End Class
