Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.DataAccess.FAME.DataAccessLayer

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' GradesDB.vb
'
' GradesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class GradesDB
#Region "Get AdvAppsetting object"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region

    Public Function GetGradeSystemsDS() As DataSet


        '   create dataset
        Dim ds As New DataSet

        '   build select query for the GradeSystems data adapter
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       GS.GrdSystemId, ")
            .Append("       GS.Descrip, ")
            .Append("       GS.StatusId, ")
            .Append("       (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("	    (Select count(*) from arGradeScales where GrdSystemId=GS.GrdSystemId) as InUseByGradeScales, ")
            .Append("       GS.CampGrpId, ")
            .Append("       GS.ModUser, ")
            .Append("       GS.ModDate ")
            .Append("FROM   arGradeSystems GS, syStatuses ST ")
            .Append("WHERE  GS.StatusId = ST.StatusId ")
        End With

        '   build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(GetAdvAppSettings.AppSettings("ConString")))

        '   Create adapter to handle GradeSystems table
        Dim da As New OleDbDataAdapter(sc)

        '   Fill GradeSystems table
        da.Fill(ds, "arGradeSystems")

        '   build select query for the GradeSystemDetails data adapter
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       GSD.GrdSysDetailId, ")
            .Append("       GSD.GrdSystemId, ")
            .Append("       GSD.Grade, ")
            .Append("       GSD.IsPass, ")
            .Append("       GSD.GPA, ")
            .Append("       GSD.IsCreditsEarned, ")
            .Append("       GSD.IsCreditsAttempted, ")

            ''Added by saraswathi lakshmanan on may 04 2009
            .Append("       GSD.IsCreditsAwarded, ")

            .Append("       GSD.IsInGPA, ")
            .Append("       GSD.IsInSAP, ")
            .Append("       GSD.IsTransferGrade, ")
            .Append("       GSD.IsIncomplete, ")
            .Append("       GSD.IsDefault, ")
            .Append("       GSD.IsDrop, ")
            .Append("       GSD.ViewOrder, ")
            .Append("       GSD.ModUser, ")
            .Append("       GSD.ModDate ")
            .Append("FROM arGradeSystemDetails GSD ")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   fill GradeSystemDetails table
        da.Fill(ds, "arGradeSystemDetails")

        '   create primary and foreign key constraints

        '   set primary key for arGradeSystems table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("arGradeSystems").Columns("GrdSystemId")
        ds.Tables("arGradeSystems").PrimaryKey = pk0

        '   set primary key for arGradeSystemDetails table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("arGradeSystemDetails").Columns("GrdSysDetailId")
        ds.Tables("arGradeSystemDetails").PrimaryKey = pk1

        '   get foreign key column in arGradeSystemDetails
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("arGradeSystemDetails").Columns("GrdSystemId")

        '   set foreign key in arGradeSystemDetails table
        ds.Relations.Add(pk0, fk0)

        '   return dataset
        Return ds

    End Function
    Public Function UpdateGradeSystemsDS(ByVal ds As DataSet) As String

        '   all updates must be encapsulated as one transaction
        Dim connection As New OleDbConnection(GetAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            '   build select query for the GradeSystems data adapter
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("       GrdSystemId, ")
                .Append("       Descrip, ")
                .Append("       StatusId, ")
                .Append("       CampGrpId, ")
                .Append("       ModUser, ")
                .Append("       ModDate ")
                .Append("FROM arGradeSystems ")
            End With

            '   build select command
            Dim gradeSystemsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle GradeSystems table
            Dim gradeSystemsDataAdapter As New OleDbDataAdapter(gradeSystemsSelectCommand)

            '   build insert, update and delete commands for GradeSystems table
            Dim cb As New OleDbCommandBuilder(gradeSystemsDataAdapter)

            ''   build insert query for the GradeSystems data adapter
            'sb = New StringBuilder
            'With sb
            '    .Append("INSERT INTO ")
            '    .Append("       arGradeSystems( ")
            '    .Append("       GrdSystemId, ")
            '    .Append("       Descrip, ")
            '    .Append("       StatusId, ")
            '    .Append("       CampGrpId, ")
            '    .Append("       ModUser, ")
            '    .Append("       ModDate) ")
            '    .Append("VALUES (?, ?, ?, ?, ?, ?); ")
            '    .Append("SELECT ")
            '    .Append("       GrdSystemId, ")
            '    .Append("       Descrip, ")
            '    .Append("       StatusId, ")
            '    .Append("       CampGrpId, ")
            '    .Append("       ModUser, ")
            '    .Append("       ModDate ")
            '    .Append("FROM arGradeSystems ")
            '    .Append("WHERE ")
            '    .Append("       (GrdSystemId = ?) ")
            'End With

            ''   build insert command
            'Dim gradeSystemsInsertCommand As New OleDbCommand(sb.ToString, connection, groupTrans)
            'gradeSystemsDataAdapter.InsertCommand = gradeSystemsInsertCommand

            ''   add parameters for the insert command
            'gradeSystemsInsertCommand.Parameters.Add(New OleDbParameter("GrdSystemId", OleDbType.Guid, 16, "GrdSystemId"))
            'gradeSystemsInsertCommand.Parameters.Add(New OleDbParameter("Descrip", OleDbType.VarChar, 80, "Descrip"))
            'gradeSystemsInsertCommand.Parameters.Add(New OleDbParameter("StatusId", OleDbType.Guid, 16, "StatusId"))
            'gradeSystemsInsertCommand.Parameters.Add(New OleDbParameter("CampGrpId", OleDbType.Guid, 16, "CampGrpId"))
            'gradeSystemsInsertCommand.Parameters.Add(New OleDbParameter("ModUser", OleDbType.VarChar, 50, "ModUser"))
            'gradeSystemsInsertCommand.Parameters.Add(New OleDbParameter("ModDate", OleDbType.DBTimeStamp, 8, "ModDate"))
            'gradeSystemsInsertCommand.Parameters.Add(New OleDbParameter("Select_GrdSystemId", OleDbType.Guid, 16, "GrdSystemId"))

            ''   build update query for the GradeSystems data adapter
            'sb = New StringBuilder
            'With sb
            '    .Append("UPDATE ")
            '    .Append("       arGradeSystems ")
            '    .Append("SET ")
            '    .Append("       GrdSystemId = ?, ")
            '    .Append("       Descrip = ?, ")
            '    .Append("       StatusId = ?, ")
            '    .Append("       CampGrpId = ?, ")
            '    .Append("       ModUser = ?, ")
            '    .Append("       ModDate = ? ")
            '    .Append("WHERE ")
            '    .Append("       (GrdSystemId = ?) ")
            '    .Append("AND    (Descrip = ?) ")
            '    .Append("AND    (StatusId = ?) ")
            '    .Append("AND    (CampGrpId = ?) ")
            '    .Append("AND    (ModUser = ?) ")
            '    .Append("AND    (ModDate = ?); ")
            '    .Append("SELECT ")
            '    .Append("       GrdSystemId, ")
            '    .Append("       Descrip, ")
            '    .Append("       StatusId, ")
            '    .Append("       CampGrpId, ")
            '    .Append("       ModUser, ")
            '    .Append("       ModDate ")
            '    .Append("FROM   arGradeSystems ")
            '    .Append("WHERE ")
            '    .Append("       (GrdSystemId = ?) ")
            'End With

            ''   build update command
            'Dim gradeSystemsUpdateCommand As New OleDbCommand(sb.ToString, connection, groupTrans)
            'gradeSystemsDataAdapter.UpdateCommand = gradeSystemsUpdateCommand

            ''   add parameters for the update command
            'gradeSystemsUpdateCommand.Parameters.Add(New OleDbParameter("GrdSystemId", OleDbType.Guid, 16, "GrdSystemId"))
            'gradeSystemsUpdateCommand.Parameters.Add(New OleDbParameter("Descrip", OleDbType.VarChar, 80, "Descrip"))
            'gradeSystemsUpdateCommand.Parameters.Add(New OleDbParameter("StatusId", OleDbType.Guid, 16, "StatusId"))
            'gradeSystemsUpdateCommand.Parameters.Add(New OleDbParameter("CampGrpId", OleDbType.Guid, 16, "CampGrpId"))
            'gradeSystemsUpdateCommand.Parameters.Add(New OleDbParameter("ModUser", OleDbType.VarChar, 50, "ModUser"))
            'gradeSystemsUpdateCommand.Parameters.Add(New OleDbParameter("ModDate", OleDbType.DBTimeStamp, 8, "ModDate"))
            'gradeSystemsUpdateCommand.Parameters.Add(New OleDbParameter("Original_GrdSystemId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GrdSystemId", DataRowVersion.Original, Nothing))
            'gradeSystemsUpdateCommand.Parameters.Add(New OleDbParameter("Original_Descrip", OleDbType.VarChar, 80, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descrip", DataRowVersion.Original, Nothing))
            'gradeSystemsUpdateCommand.Parameters.Add(New OleDbParameter("Original_StatusId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "StatusId", DataRowVersion.Original, Nothing))
            'gradeSystemsUpdateCommand.Parameters.Add(New OleDbParameter("Original_CampGrpId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CampGrpId", DataRowVersion.Original, Nothing))
            'gradeSystemsUpdateCommand.Parameters.Add(New OleDbParameter("Original_ModUser", OleDbType.VarChar, 50, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ModUser", DataRowVersion.Original, Nothing))
            'gradeSystemsUpdateCommand.Parameters.Add(New OleDbParameter("Original_ModDate", OleDbType.DBTimeStamp, 8, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ModDate", DataRowVersion.Original, Nothing))
            'gradeSystemsUpdateCommand.Parameters.Add(New OleDbParameter("Select_GrdSystemId", OleDbType.Guid, 16, "GrdSystemId"))

            ''   build delete query for the GradeSystems data adapter
            'sb = New StringBuilder
            'With sb
            '    .Append("DELETE FROM arGradeSystems ")
            '    .Append("WHERE ")
            '    .Append("       (GrdSystemId = ?) ")
            '    .Append("AND    (CampGrpId = ?) ")
            '    .Append("AND    (Descrip = ?) ")
            '    .Append("AND    (StatusId = ?) ")
            '    .Append("AND    (ModUser = ?) ")
            '    .Append("AND    (ModDate = ?) ")
            'End With

            ''   build delete command
            'Dim gradeSystemsDeleteCommand As New OleDbCommand(sb.ToString, connection, groupTrans)
            'gradeSystemsDataAdapter.DeleteCommand = gradeSystemsDeleteCommand

            ''   add parameters to the delete command
            'gradeSystemsDeleteCommand.Parameters.Add(New OleDbParameter("Original_GrdSystemId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GrdSystemId", DataRowVersion.Original, Nothing))
            'gradeSystemsDeleteCommand.Parameters.Add(New OleDbParameter("Original_CampGrpId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CampGrpId", DataRowVersion.Original, Nothing))
            'gradeSystemsDeleteCommand.Parameters.Add(New OleDbParameter("Original_Descrip", OleDbType.VarChar, 80, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descrip", DataRowVersion.Original, Nothing))
            'gradeSystemsDeleteCommand.Parameters.Add(New OleDbParameter("Original_StatusId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "StatusId", DataRowVersion.Original, Nothing))
            'gradeSystemsDeleteCommand.Parameters.Add(New OleDbParameter("Original_ModUser", OleDbType.VarChar, 50, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ModUser", DataRowVersion.Original, Nothing))
            'gradeSystemsDeleteCommand.Parameters.Add(New OleDbParameter("Original_ModDate", OleDbType.DBTimeStamp, 8, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ModDate", DataRowVersion.Original, Nothing))

            '   build select query for the GradeSystemDetails data adapter
            sb = New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("       GrdSysDetailId, ")
                .Append("       GrdSystemId, ")
                .Append("       Grade, ")
                .Append("       IsPass, ")
                .Append("       GPA, ")
                .Append("       IsCreditsEarned, ")
                .Append("       IsCreditsAttempted, ")

                ''Added by saraswathi on May 04 2009
                .Append("       IsCreditsAwarded, ")

                .Append("       IsInGPA, ")
                .Append("       IsInSAP, ")
                .Append("       IsTransferGrade, ")
                .Append("       IsIncomplete, ")
                .Append("       IsDefault, ")
                .Append("       IsDrop, ")
                .Append("       ViewOrder, ")
                .Append("       ModUser, ")
                .Append("       ModDate ")
                .Append("FROM arGradeSystemDetails ")
            End With

            '   build select command
            Dim gradeSystemDetailsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle GradeSystemsDetails table
            Dim gradeSystemDetailsDataAdapter As New OleDbDataAdapter(gradeSystemDetailsSelectCommand)

            '   build insert, update and delete commands for GradeSystemDetails table
            Dim cb1 As New OleDbCommandBuilder(gradeSystemDetailsDataAdapter)

            ''   build insert query for the GradeSystemaDetails data adapter
            'sb = New StringBuilder
            'With sb
            '    .Append("INSERT INTO ")
            '    .Append("       arGradeSystemDetails (")
            '    .Append("       GrdSysDetailId, GrdSystemId, Grade, IsPass, GPA, ")
            '    .Append("       IsCreditsEarned, IsCreditsAttempted, IsInGPA, IsInSAP, ")
            '    .Append("       IsTransferGrade, IsIncomplete, IsDefault, IsDrop, ViewOrder, ")
            '    .Append("       ModUser, ModDate) ")
            '    .Append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); ")
            '    .Append("SELECT ")
            '    .Append("       GrdSysDetailId, ")
            '    .Append("       GrdSystemId, ")
            '    .Append("       Grade, ")
            '    .Append("       IsPass, ")
            '    .Append("       GPA, ")
            '    .Append("       IsCreditsEarned, ")
            '    .Append("       IsCreditsAttempted, ")
            '    .Append("       IsInGPA, ")
            '    .Append("       IsInSAP, ")
            '    .Append("       IsTransferGrade, ")
            '    .Append("       IsIncomplete, ")
            '    .Append("       IsDefault, ")
            '    .Append("       IsDrop, ")
            '    .Append("       ViewOrder, ")
            '    .Append("       ModUser, ")
            '    .Append("       ModDate ")
            '    .Append("FROM arGradeSystemDetails ")
            '    .Append("WHERE ")
            '    .Append("       (GrdSysDetailId = ?)")
            'End With

            ''   build insert command
            'Dim gradeSystemDetailsInsertCommand As New OleDbCommand(sb.ToString, connection, groupTrans)
            'gradeSystemDetailsDataAdapter.InsertCommand = gradeSystemDetailsInsertCommand

            ''   add parameters for the insert command
            'gradeSystemDetailsInsertCommand.Parameters.Add(New OleDbParameter("GrdSysDetailId", OleDbType.Guid, 16, "GrdSysDetailId"))
            'gradeSystemDetailsInsertCommand.Parameters.Add(New OleDbParameter("GrdSystemId", OleDbType.Guid, 16, "GrdSystemId"))
            'gradeSystemDetailsInsertCommand.Parameters.Add(New OleDbParameter("Grade", OleDbType.VarChar, 5, "Grade"))
            'gradeSystemDetailsInsertCommand.Parameters.Add(New OleDbParameter("IsPass", OleDbType.Boolean, 1, "IsPass"))
            'gradeSystemDetailsInsertCommand.Parameters.Add(New OleDbParameter("GPA", OleDbType.Decimal, 9, "GPA"))
            'gradeSystemDetailsInsertCommand.Parameters.Add(New OleDbParameter("IsCreditsEarned", OleDbType.Boolean, 1, "IsCreditsEarned"))
            'gradeSystemDetailsInsertCommand.Parameters.Add(New OleDbParameter("IsCreditsAttempted", OleDbType.Boolean, 1, "IsCreditsAttempted"))
            'gradeSystemDetailsInsertCommand.Parameters.Add(New OleDbParameter("IsInGPA", OleDbType.Boolean, 1, "IsInGPA"))
            'gradeSystemDetailsInsertCommand.Parameters.Add(New OleDbParameter("IsInSAP", OleDbType.Boolean, 1, "IsInSAP"))
            'gradeSystemDetailsInsertCommand.Parameters.Add(New OleDbParameter("IsTransferGrade", OleDbType.Boolean, 1, "IsTransferGrade"))
            'gradeSystemDetailsInsertCommand.Parameters.Add(New OleDbParameter("IsIncomplete", OleDbType.Boolean, 1, "IsIncomplete"))
            'gradeSystemDetailsInsertCommand.Parameters.Add(New OleDbParameter("IsDefault", OleDbType.Boolean, 1, "IsDefault"))
            'gradeSystemDetailsInsertCommand.Parameters.Add(New OleDbParameter("IsDrop", OleDbType.Boolean, 1, "IsDrop"))
            'gradeSystemDetailsInsertCommand.Parameters.Add(New OleDbParameter("ViewOrder", OleDbType.Integer, 4, "ViewOrder"))
            'gradeSystemDetailsInsertCommand.Parameters.Add(New OleDbParameter("ModUser", OleDbType.VarChar, 50, "ModUser"))
            'gradeSystemDetailsInsertCommand.Parameters.Add(New OleDbParameter("ModDate", OleDbType.DBTimeStamp, 8, "ModDate"))
            'gradeSystemDetailsInsertCommand.Parameters.Add(New OleDbParameter("Select_GrdSysDetailId", OleDbType.Guid, 16, "GrdSysDetailId"))

            ''   build update query for the GradeSystemDetails data adapter
            'sb = New StringBuilder
            'With sb
            '    .Append("UPDATE arGradeSystemDetails ")
            '    .Append("SET ")
            '    .Append("       GrdSysDetailId = ?, ")
            '    .Append("       GrdSystemId = ?, ")
            '    .Append("       Grade = ?, ")
            '    .Append("       IsPass = ?, ")
            '    .Append("       GPA = ?, ")
            '    .Append("       IsCreditsEarned = ?, ")
            '    .Append("       IsCreditsAttempted = ?, ")
            '    .Append("       IsInGPA = ?, ")
            '    .Append("       IsInSAP = ?, ")
            '    .Append("       IsTransferGrade = ?, ")
            '    .Append("       IsIncomplete = ?, ")
            '    .Append("       IsDefault = ?, ")
            '    .Append("       IsDrop = ?, ")
            '    .Append("       ViewOrder = ?, ")
            '    .Append("       ModUser = ?, ")
            '    .Append("       ModDate = ? ")
            '    .Append("WHERE ")
            '    .Append("       (GrdSysDetailId = ?) ")
            '    .Append("AND    (GPA = ? OR ? IS NULL AND GPA IS NULL) ")
            '    .Append("AND    (Grade = ?) ")
            '    .Append("AND    (GrdSystemId = ?) ")
            '    .Append("AND    (IsCreditsAttempted = ?) ")
            '    .Append("AND    (IsCreditsEarned = ?) ")
            '    .Append("AND    (IsInGPA = ?) ")
            '    .Append("AND    (IsInSAP = ?) ")
            '    .Append("AND    (IsPass = ?) ")
            '    .Append("AND    (IsTransferGrade = ?) ")
            '    .Append("AND    (IsIncomplete = ?) ")
            '    .Append("AND    (IsDefault = ?) ")
            '    .Append("AND    (IsDrop = ?) ")
            '    .Append("AND    (ViewOrder = ?) ")
            '    .Append("AND    (ModUser = ?) ")
            '    .Append("AND    (ModDate = ?); ")
            '    .Append("SELECT ")
            '    .Append("       GrdSysDetailId, ")
            '    .Append("       GrdSystemId, ")
            '    .Append("       Grade, ")
            '    .Append("       IsPass, ")
            '    .Append("       GPA, ")
            '    .Append("       IsCreditsEarned, ")
            '    .Append("       IsCreditsAttempted, ")
            '    .Append("       IsInGPA, ")
            '    .Append("       IsInSAP, ")
            '    .Append("       IsTransferGrade, ")
            '    .Append("       IsIncomplete, ")
            '    .Append("       IsDefault, ")
            '    .Append("       IsDrop, ")
            '    .Append("       ViewOrder, ")
            '    .Append("       ModUser, ")
            '    .Append("       ModDate ")
            '    .Append("FROM   arGradeSystemDetails ")
            '    .Append("WHERE  (GrdSysDetailId = ? ) ")
            'End With

            ''   build update command
            'Dim gradeSystemDetailsUpdateCommand = New OleDbCommand(sb.ToString, connection, groupTrans)
            ''Dim gradeSystemDetailsUpdateCommand As New OleDbCommand(sb.ToString, connection)

            'gradeSystemDetailsDataAdapter.UpdateCommand = gradeSystemDetailsUpdateCommand

            ''   add parameters for the update command
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("GrdSysDetailId", OleDbType.Guid, 16, "GrdSysDetailId"))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("GrdSystemId", OleDbType.Guid, 16, "GrdSystemId"))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Grade", OleDbType.VarChar, 5, "Grade"))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("IsPass", OleDbType.Boolean, 1, "IsPass"))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("GPA", OleDbType.Decimal, 9, "GPA"))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("IsCreditsEarned", OleDbType.Boolean, 1, "IsCreditsEarned"))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("IsCreditsAttempted", OleDbType.Boolean, 1, "IsCreditsAttempted"))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("IsInGPA", OleDbType.Boolean, 1, "IsInGPA"))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("IsInSAP", OleDbType.Boolean, 1, "IsInSAP"))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("IsTransferGrade", OleDbType.Boolean, 1, "IsTransferGrade"))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("IsIncomplete", OleDbType.Boolean, 1, "IsIncomplete"))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("IsDefault", OleDbType.Boolean, 1, "IsDefault"))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("IsDrop", OleDbType.Boolean, 1, "IsDrop"))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("ViewOrder", OleDbType.Integer, 4, "ViewOrder"))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("ModUser", OleDbType.VarChar, 50, "ModUser"))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("ModDate", OleDbType.DBTimeStamp, 8, "ModDate"))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_GrdSysDetailId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GrdSysDetailId", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_GPA", OleDbType.Decimal, 9, ParameterDirection.Input, False, CType(18, Byte), CType(0, Byte), "GPA", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_GPA1", OleDbType.Decimal, 9, ParameterDirection.Input, False, CType(18, Byte), CType(0, Byte), "GPA", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_Grade", OleDbType.VarChar, 5, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Grade", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_GrdSystemId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GrdSystemId", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_IsCreditsAttempted", OleDbType.Boolean, 1, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsCreditsAttempted", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_IsCreditsEarned", OleDbType.Boolean, 1, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsCreditsEarned", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_IsInGPA", OleDbType.Boolean, 1, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsInGPA", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_IsInSAP", OleDbType.Boolean, 1, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsInSAP", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_IsPass", OleDbType.Boolean, 1, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsPass", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_IsTransferGrade", OleDbType.Boolean, 1, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsTransferGrade", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_IsIncomplete", OleDbType.Boolean, 1, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsIncomplete", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_IsDefault", OleDbType.Boolean, 1, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsDefault", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_IsDrop", OleDbType.Boolean, 1, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsDrop", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_ViewOrder", OleDbType.Integer, 4, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ViewOrder", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_ModUser", OleDbType.VarChar, 50, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ModUser", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_ModDate", OleDbType.DBTimeStamp, 8, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ModDate", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Select_GrdSysDetailId", OleDbType.Guid, 16, "GrdSysDetailId"))

            ''   build delete query for the GradeSystemaDetails data adapter
            'sb = New StringBuilder
            'With sb
            '    .Append("DELETE FROM arGradeSystemDetails ")
            '    .Append("WHERE ")
            '    .Append("       (GrdSysDetailId = ?) ")
            '    .Append("AND    (GPA = ? OR ? IS NULL AND GPA IS NULL) ")
            '    .Append("AND    (Grade = ?) ")
            '    .Append("AND    (GrdSystemId = ?) ")
            '    .Append("AND    (IsCreditsAttempted = ?) ")
            '    .Append("AND    (IsCreditsEarned = ?) ")
            '    .Append("AND    (IsInGPA = ?) ")
            '    .Append("AND    (IsInSAP = ?) ")
            '    .Append("AND    (IsPass = ?) ")
            '    .Append("AND    (IsTransferGrade = ?) ")
            '    .Append("AND    (IsIncomplete = ?) ")
            '    .Append("AND    (IsDefault = ?) ")
            '    .Append("AND    (IsDrop = ?) ")
            '    .Append("AND    (ViewOrder = ?) ")
            '    .Append("AND    (ModUser = ?) ")
            '    .Append("AND    (ModDate = ?) ")
            'End With

            ''   build delete command
            'Dim gradeSystemDetailsDeleteCommand As New OleDbCommand(sb.ToString, connection, groupTrans)
            ''Dim gradeSystemDetailsDeleteCommand As New OleDbCommand(sb.ToString, connection)

            'gradeSystemDetailsDataAdapter.DeleteCommand = gradeSystemDetailsDeleteCommand

            ''   add parameters to the delete command
            'gradeSystemDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_GrdSysDetailId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GrdSysDetailId", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_GPA", OleDbType.Decimal, 9, ParameterDirection.Input, False, CType(18, Byte), CType(0, Byte), "GPA", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_GPA1", OleDbType.Decimal, 9, ParameterDirection.Input, False, CType(18, Byte), CType(0, Byte), "GPA", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_Grade", OleDbType.VarChar, 5, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Grade", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_GrdSystemId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GrdSystemId", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_IsCreditsAttempted", OleDbType.Boolean, 1, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsCreditsAttempted", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_IsCreditsEarned", OleDbType.Boolean, 1, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsCreditsEarned", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_IsInGPA", OleDbType.Boolean, 1, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsInGPA", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_IsInSAP", OleDbType.Boolean, 1, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsInSAP", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_IsPass", OleDbType.Boolean, 1, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsPass", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_IsTransferGrade", OleDbType.Boolean, 1, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsTransferGrade", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_IsIncomplete", OleDbType.Boolean, 1, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsIncomplete", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_IsDefault", OleDbType.Boolean, 1, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsDefault", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_IsDrop", OleDbType.Boolean, 1, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsDrop", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_ViewOrder", OleDbType.Integer, 4, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ViewOrder", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_ModUser", OleDbType.VarChar, 50, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ModUser", DataRowVersion.Original, Nothing))
            'gradeSystemDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_ModDate", OleDbType.DBTimeStamp, 8, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ModDate", DataRowVersion.Original, Nothing))

            '   insert added rows in GradeSystems table
            gradeSystemsDataAdapter.Update(ds.Tables("arGradeSystems").Select(Nothing, Nothing, DataViewRowState.Added))

            '   insert added rows in GradeSystemDetails table
            gradeSystemDetailsDataAdapter.Update(ds.Tables("arGradeSystemDetails").Select(Nothing, Nothing, DataViewRowState.Added))

            '   delete rows in GradeSystemDetails table
            gradeSystemDetailsDataAdapter.Update(ds.Tables("arGradeSystemDetails").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   delete rows in GradeSystems table
            gradeSystemsDataAdapter.Update(ds.Tables("arGradeSystems").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   update rows in GradeSystems table
            gradeSystemsDataAdapter.Update(ds.Tables("arGradeSystems").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   update rows in GradeSystemDetails table
            gradeSystemDetailsDataAdapter.Update(ds.Tables("arGradeSystemDetails").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            Return ""

        Catch ex As OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Catch ex As Exception
            'somethingwentwrong
            'rollbacktransaction
            groupTrans.Rollback()
 
            'returnerrormessage
            Return DALExceptions.BuildErrorMessage(ex)
        Finally

            '   close connection
            connection.Close()
        End Try

    End Function
    'Public Function GetGradeBookDisplayDS(ByVal empId As String, ByVal termId As String) As DataSet


    '    '   create dataset
    '    Dim ds As New DataSet

    '    '   build query to select GradeBookResults from a given Teacher
    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append("SELECT ")
    '        .Append("       GBR.GrdBkResultId, ")
    '        .Append("       GBR.ClsSectionId, ")
    '        .Append("       GBR.InstrGrdBkWgtDetailId, ")
    '        .Append("       (Select Descrip from arGrdBkWgtDetails where InstrGrdBkWgtDetailId=GBR.InstrGrdBkWgtDetailId) As Descrip, ")
    '        .Append("       (Select Weight from arGrdBkWgtDetails where InstrGrdBkWgtDetailId=GBR.InstrGrdBkWgtDetailId) As Weight, ")
    '        .Append("       (Select Seq from arGrdBkWgtDetails where InstrGrdBkWgtDetailId=GBR.InstrGrdBkWgtDetailId) As Seq, ")
    '        .Append("       GBR.StudentId, ")
    '        .Append("       (Select LastName + ', ' + FirstName from arStudent where StudentId=GBR.StudentId) As StudentName, ")
    '        .Append("       GBR.Score ")
    '        .Append("FROM ")
    '        .Append("       arGrdBkResults GBR, ")
    '        .Append("       arGrdBkWgtDetails IWD, ")
    '        .Append("       arStudent S ")
    '        .Append("WHERE ")
    '        .Append("       GBR.InstrGrdBkWgtDetailId = IWD.InstrGrdBkWgtDetailId ")
    '        .Append("AND ")
    '        .Append("       GBR.StudentId = S.StudentId ")
    '        .Append("AND ")
    '        .Append("       ClsSectionId In (Select ClsSectionId from arClassSections where EmpId = ? and TermId = ? ) ")
    '        .Append("ORDER BY ")
    '        .Append("       Seq, ")
    '        .Append("       GBR.InstrGrdBkWgtDetailId ")
    '    End With

    '    '   build select command
    '    Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(GetAdvAppSettings.AppSettings("ConString")))

    '    '   add EmpId parameter
    '    sc.Parameters.Add(New OleDbParameter("EmpId", empId))

    '    '   add TermId parameter   
    '    sc.Parameters.Add(New OleDbParameter("TermId", termId))


    '    '   Create adapter to handle GradeSystems table
    '    Dim da As New OleDbDataAdapter(sc)

    '    '   Fill GradeBookResults table
    '    da.Fill(ds, "GradeBookResults")

    '    '   build query to select class sections from a given instructor
    '    sb = New StringBuilder
    '    With sb
    '        .Append("SELECT ")
    '        .Append("       CS.ClsSectionId, ")
    '        .Append("       CS.GrdScaleId, ")
    '        .Append("       CS.ClsSection, ")
    '        .Append("       CS.ReqId, ")
    '        .Append("       (Select Descrip from arReqs where ReqId=CS.ReqId) As CourseDescrip ")
    '        .Append("FROM ")
    '        .Append("       arClassSections CS, ")
    '        .Append("       arClassSectionTerms CST, ")
    '        .Append("       arReqs R ")
    '        .Append("WHERE ")
    '        .Append("       CS.ReqId = R.ReqId ")
    '        .Append("AND    CS.EmpId = ? ")
    '        .Append("AND    CST.ClsSectionId=CS.ClsSectionId AND CST.TermId = ? ")
    '        .Append("ORDER BY ")
    '        .Append("       Descrip, ")
    '        .Append("       CS.ClsSection ")
    '    End With

    '    '   modify select command
    '    da.SelectCommand.CommandText = sb.ToString
    '    'da.SelectCommand.Parameters.Clear()
    '    'da.SelectCommand.Parameters.Add(New OleDbParameter("EmpId", empId))
    '    'da.SelectCommand.Parameters.Add(New OleDbParameter("TermId", termId))

    '    '   fill ClassSections table
    '    da.Fill(ds, "ClassSections")

    '    '   build query to select students from a given teacher section
    '    sb = New StringBuilder
    '    With sb
    '        .Append("SELECT DISTINCT ")
    '        .Append("       CSS.StudentId, ")
    '        .Append("       (Select LastName + ', ' + FirstName from arStudent where StudentId=CSS.StudentId) As StudentName ")
    '        .Append("FROM ")
    '        .Append("       arClsSectStudents CSS, ")
    '        .Append("       arStudent S ")
    '        .Append("WHERE ")
    '        .Append("       CSS.StudentId=S.StudentId ")
    '        .Append("AND ")
    '        .Append("       ClsSectionId In (Select ClsSectionId from arClassSections where EmpId = ? and TermId = ? ) ")
    '        .Append("ORDER BY ")
    '        .Append("       StudentName ")
    '    End With

    '    '   modify select command
    '    da.SelectCommand.CommandText = sb.ToString
    '    'da.SelectCommand.Parameters.Clear()
    '    'da.SelectCommand.Parameters.Add(New OleDbParameter("EmpId", empId))
    '    'da.SelectCommand.Parameters.Add(New OleDbParameter("TermId", termId))

    '    '   fill Students table
    '    da.Fill(ds, "Students")

    '    '   build query to select ClassSections and Students from a given teacher
    '    sb = New StringBuilder
    '    With sb
    '        .Append("SELECT ")
    '        .Append("       StudentId, ")
    '        .Append("       ClsSectionId ")
    '        .Append("FROM ")
    '        .Append("       arClsSectStudents ")
    '        .Append("WHERE ")
    '        .Append("       ClsSectionId In (Select ClsSectionId from arClassSections where EmpId = ? and TermId = ?) ")
    '    End With

    '    '   modify select command
    '    da.SelectCommand.CommandText = sb.ToString
    '    'da.SelectCommand.Parameters.Clear()
    '    'da.SelectCommand.Parameters.Add(New OleDbParameter("EmpId", empId))
    '    'da.SelectCommand.Parameters.Add(New OleDbParameter("TermId", termId))

    '    '   fill ClassSectionsStudents table
    '    da.Fill(ds, "ClassSectionsStudents")

    '    '   build query to select GradeScales used by teacher in those terms
    '    sb = New StringBuilder
    '    With sb
    '        .Append("SELECT ")
    '        .Append("       GrdScaleDetailId, ")
    '        .Append("       GrdScaleId, ")
    '        .Append("       MinVal, ")
    '        .Append("       MaxVal, ")
    '        .Append("       GrdSysDetailId ")
    '        .Append("FROM ")
    '        .Append("       arGradeScaleDetails ")
    '        .Append("WHERE ")
    '        .Append("       GrdScaleId In (Select GrdScaleId from arClassSections where EmpId = ? and TermId = ?) ")
    '    End With

    '    '   modify select command
    '    da.SelectCommand.CommandText = sb.ToString
    '    'da.SelectCommand.Parameters.Clear()
    '    'da.SelectCommand.Parameters.Add(New OleDbParameter("EmpId", empId))
    '    'da.SelectCommand.Parameters.Add(New OleDbParameter("TermId", termId))

    '    '   fill ClassSectionsStudents table
    '    da.Fill(ds, "GradeScaleDetails")

    '    '   build query to select GradeSystemDetails used by teacher in those terms
    '    sb = New StringBuilder
    '    With sb
    '        .Append("SELECT ")
    '        .Append("       GrdSysDetailId, ")
    '        .Append("       Grade ")
    '        .Append("FROM ")
    '        .Append("       arGradeSystemDetails ")
    '        .Append("WHERE ")
    '        .Append("       GrdSystemId In (Select GrdSystemId from arGradeScales where GrdScaleId in (select GrdScaleId from arClassSections where EmpId = ? and TermId = ?)) ")
    '    End With

    '    '   modify select command
    '    da.SelectCommand.CommandText = sb.ToString
    '    'da.SelectCommand.Parameters.Clear()
    '    'da.SelectCommand.Parameters.Add(New OleDbParameter("EmpId", empId))
    '    'da.SelectCommand.Parameters.Add(New OleDbParameter("TermId", termId))

    '    '   fill GradeSystemDetails table
    '    da.Fill(ds, "GradeSystemDetails")

    '    '   create primary and foreign key constraints

    '    '   set primary key for GradeBookResults table
    '    Dim pk0(0) As DataColumn
    '    pk0(0) = ds.Tables("GradeBookResults").Columns("GrdBkResultId")
    '    ds.Tables("GradeBookResults").PrimaryKey = pk0

    '    '   set primary key for ClassSections table
    '    Dim pk1(0) As DataColumn
    '    pk1(0) = ds.Tables("ClassSections").Columns("ClsSectionId")
    '    ds.Tables("ClassSections").PrimaryKey = pk1

    '    '   set primary key for Students table
    '    Dim pk2(0) As DataColumn
    '    pk2(0) = ds.Tables("Students").Columns("StudentId")
    '    ds.Tables("Students").PrimaryKey = pk2

    '    '   set primary key for GradeSystemDetails table
    '    Dim pk3(0) As DataColumn
    '    pk3(0) = ds.Tables("GradeSystemDetails").Columns("GrdSysDetailId")
    '    ds.Tables("GradeSystemDetails").PrimaryKey = pk3

    '    '   set foreign key column in GradeBookResults
    '    Dim fk0(0) As DataColumn
    '    fk0(0) = ds.Tables("GradeBookResults").Columns("ClsSectionId")

    '    '   set foreign key column in GradeBookResults
    '    Dim fk1(0) As DataColumn
    '    fk1(0) = ds.Tables("GradeBookResults").Columns("StudentId")

    '    '   set foreign key column in GradeBookResults
    '    Dim fk2(0) As DataColumn
    '    fk2(0) = ds.Tables("ClassSectionsStudents").Columns("StudentId")

    '    '   set foreign key column in GradeBookResults
    '    Dim fk3(0) As DataColumn
    '    fk3(0) = ds.Tables("GradeScaleDetails").Columns("GrdSysDetailId")

    '    '   set foreign keys in GradeBookResults table
    '    ds.Relations.Add("ClassSectionsGradeBookResults", pk1, fk0)
    '    ds.Relations.Add("StudentsGradeBookResults", pk2, fk1)
    '    ds.Relations.Add("StudentsClassSectionsStudents", pk2, fk2)
    '    ds.Relations.Add("GradeSystemDetailsGradeScaleDetails", pk3, fk3)

    '    '   return dataset
    '    Return ds

    'End Function
    Public Function GetGradeScalesDS() As DataSet


        '   create dataset
        Dim ds As New DataSet

        '   build select query for the GradeScales data adapter
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       GS.GrdScaleId, ")
            .Append("       GS.CampgrpId, ")
            .Append("       GS.Descrip, ")
            .Append("       GS.StatusId, ")
            .Append("	    (Select count(*) from arClassSections where GrdScaleId=GS.GrdScaleId) as InUseByClassSections, ")
            .Append("       (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("       GS.GrdSystemId, ")
            .Append("       GS.InstructorId, ")
            .Append("       GS.ModUser, ")
            .Append("       GS.ModDate ")
            .Append("FROM arGradeScales GS, syStatuses ST  ")
            .Append("WHERE  GS.StatusId=ST.StatusId ")
        End With

        '   build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(GetAdvAppSettings.AppSettings("ConString")))

        '   Create adapter to handle GradeScales table
        Dim da As New OleDbDataAdapter(sc)

        '   Fill GradeScales table
        da.Fill(ds, "arGradeScales")

        '   build select query for the GradeScaleDetails data adapter
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       GSD.GrdScaleDetailId, ")
            .Append("       GSD.GrdScaleId, ")
            .Append("       GSD.MinVal, ")
            .Append("       GSD.MaxVal, ")
            .Append("       (Select Grade from arGradeSystemDetails where GrdSysDetailId=GSD.GrdSysDetailId) as Grade, ")
            .Append("       (Select Grade from arGradeSystemDetails where GrdSysDetailId=GSD.GrdSysDetailId) as GradeSystemDescrip, ")
            .Append("       GSD.GrdSysDetailId, ")
            .Append("       GSD.ViewOrder, ")
            .Append("       GSD.ModUser, ")
            .Append("       GSD.ModDate ")
            .Append("FROM   arGradeScaleDetails GSD, arGradeScales GS ")
            .Append("WHERE   GSD.GrdScaleId=GS.GrdScaleId ")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   fill GradeScaleDetails table
        da.Fill(ds, "arGradeScaleDetails")

        '   build select query for the GradeSystems table
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       GS.GrdSystemId, ")
            .Append("       GS.Descrip ")
            .Append("FROM arGradeSystems GS, syStatuses ST ")
            .Append("WHERE   GS.StatusId = ST.StatusId ")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   Fill GradeSystems table
        da.Fill(ds, "arGradeSystems")

        '   build select query for the GradeSystemDetails table
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       GSD.GrdSysDetailId, ")
            .Append("       GSD.GrdSystemId, ")
            .Append("       GSD.Grade, ")
            .Append("       GSD.GPA, ")
            .Append("       GSD.ViewOrder, ")
            .Append("       GSD.ModUser, ")
            .Append("       GSD.ModDate ")
            .Append("FROM   arGradeSystemDetails GSD, arGradeSystems GS  ")
            .Append("WHERE   GSD.GrdSystemId=GS.GrdSystemId ")
            .Append("AND    Not (GSD.IsIncomplete=1 Or GSD.IsDefault=1 Or GSD.IsDrop=1) ")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   fill GradeSystemDetails table
        da.Fill(ds, "arGradeSystemDetails")

        '   create primary and foreign key constraints

        '   set primary key for arGradeScales table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("arGradeScales").Columns("GrdScaleId")
        ds.Tables("arGradeScales").PrimaryKey = pk0

        '   set primary key for arGradeScaleDetails table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("arGradeScaleDetails").Columns("GrdScaleDetailId")
        ds.Tables("arGradeScaleDetails").PrimaryKey = pk1

        '   set primary key for arGradeSystems table
        Dim pk2(0) As DataColumn
        pk2(0) = ds.Tables("arGradeSystems").Columns("GrdSystemId")
        ds.Tables("arGradeSystems").PrimaryKey = pk2

        '   set primary key for arGradeSystemDetails table
        Dim pk3(0) As DataColumn
        pk3(0) = ds.Tables("arGradeSystemDetails").Columns("GrdSysDetailId")
        ds.Tables("arGradeSystemDetails").PrimaryKey = pk3

        '   set foreign key column in arGradeScaleDetails
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("arGradeScaleDetails").Columns("GrdScaleId")

        '   set foreign key column in arGradeScaleDetails
        Dim fk1(0) As DataColumn
        fk1(0) = ds.Tables("arGradeScaleDetails").Columns("GrdSysDetailId")

        '   set foreign key column in arGradeSystemDetails
        Dim fk2(0) As DataColumn
        fk2(0) = ds.Tables("arGradeSystemDetails").Columns("GrdSystemId")

        '   set foreign key relations
        ds.Relations.Add("GradeScalesGradeScaleDetails", pk0, fk0)
        ds.Relations.Add("GradeSystemDetailsGradeScaleDetails", pk3, fk1)
        ds.Relations.Add("GradeSystemsGradeSystemDetails", pk2, fk2)

        '   return dataset
        Return ds

    End Function
    Public Function UpdateGradeScalesDS(ByVal ds As DataSet) As String

        '   all updates must be encapsulated as one transaction
        Dim connection As New OleDbConnection(GetAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            '   build select query for the GradeScales data adapter
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("       GrdScaleId, ")
                .Append("       CampGrpId, ")
                .Append("       Descrip, ")
                .Append("       StatusId, ")
                .Append("       GrdSystemId, ")
                .Append("       InstructorId, ")
                .Append("       ModUser, ")
                .Append("       ModDate ")
                .Append("FROM arGradeScales ")
            End With

            '   build select command
            Dim GradeScalesSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle GradeScales table
            Dim GradeScalesDataAdapter As New OleDbDataAdapter(GradeScalesSelectCommand)

            '   build insert, update and delete commands for GradeScales table
            'Dim cb As New OleDbCommandBuilder(GradeScalesDataAdapter)

            '   build insert query for the GradeScaleDetails data adapter
            sb = New StringBuilder
            With sb
                .Append("INSERT INTO ")
                .Append("       arGradeScales( ")
                .Append("       GrdScaleId, ")
                .Append("       CampGrpId, ")
                .Append("       Descrip, ")
                .Append("       StatusId, ")
                .Append("       GrdSystemId, ")
                .Append("       InstructorId, ")
                .Append("       ModUser, ")
                .Append("       ModDate) ")
                .Append("VALUES (?, ?, ?, ?, ?, ?, ?, ?); ")
                .Append("SELECT ")
                .Append("       GrdScaleId, ")
                .Append("       CampgrpId, ")
                .Append("       Descrip, ")
                .Append("       StatusId, ")
                .Append("       GrdSystemId, ")
                .Append("       InstructorId, ")
                .Append("       ModUser, ")
                .Append("       ModDate ")
                .Append("FROM arGradeScales ")
                .Append("WHERE ")
                .Append("       (GrdScaleId = ?) ")
            End With

            '   build insert command
            Dim GradeScalesInsertCommand As New OleDbCommand(sb.ToString, connection, groupTrans)
            GradeScalesDataAdapter.InsertCommand = GradeScalesInsertCommand

            '   add parameters for the insert command
            GradeScalesInsertCommand.Parameters.Add(New OleDbParameter("GrdScaleId", OleDbType.Guid, 16, "GrdScaleId"))
            GradeScalesInsertCommand.Parameters.Add(New OleDbParameter("CampGrpId", OleDbType.Guid, 16, "CampGrpId"))
            GradeScalesInsertCommand.Parameters.Add(New OleDbParameter("Descrip", OleDbType.VarChar, 80, "Descrip"))
            GradeScalesInsertCommand.Parameters.Add(New OleDbParameter("StatusId", OleDbType.Guid, 16, "StatusId"))
            GradeScalesInsertCommand.Parameters.Add(New OleDbParameter("GrdSystemId", OleDbType.Guid, 16, "GrdSystemId"))
            GradeScalesInsertCommand.Parameters.Add(New OleDbParameter("InstructorId", OleDbType.Guid, 16, "InstructorId"))
            GradeScalesInsertCommand.Parameters.Add(New OleDbParameter("ModUser", OleDbType.VarChar, 50, "ModUser"))
            GradeScalesInsertCommand.Parameters.Add(New OleDbParameter("ModDate", OleDbType.DBTimeStamp, 8, "ModDate"))
            GradeScalesInsertCommand.Parameters.Add(New OleDbParameter("Select_GrdScaleId", OleDbType.Guid, 16, "GrdScaleId"))

            '   build update query for the GradeScales data adapter
            sb = New StringBuilder
            With sb
                .Append("UPDATE ")
                .Append("       arGradeScales ")
                .Append("SET ")
                .Append("       GrdScaleId = ?, ")
                .Append("       CampGrpId = ?, ")
                .Append("       Descrip = ?, ")
                .Append("       StatusId = ?, ")
                .Append("       GrdSystemId = ?, ")
                .Append("       InstructorId = ?, ")
                .Append("       ModUser = ?, ")
                .Append("       ModDate = ? ")
                .Append("WHERE ")
                .Append("       (GrdScaleId = ?) ")
                .Append("AND    (CampGrpId = ?) ")
                .Append("AND    (Descrip = ?) ")
                .Append("AND    (StatusId = ?) ")
                .Append("AND    (GrdSystemId = ?) ")
                .Append("AND    (InstructorId = ? OR (InstructorId Is Null and ? Is Null)) ")
                .Append("AND    (ModUser = ?) ")
                .Append("AND    (ModDate = ?); ")
                .Append("SELECT ")
                .Append("       GrdScaleId, ")
                .Append("       CampGrpId ")
                .Append("       Descrip, ")
                .Append("       StatusId, ")
                .Append("       GrdSystemId, ")
                .Append("       InstructorId, ")
                .Append("       ModUser, ")
                .Append("       ModDate ")
                .Append("FROM arGradeScales ")
                .Append("WHERE ")
                .Append("       (GrdScaleId = ?) ")
            End With

            '   build update command
            Dim GradeScalesUpdateCommand As New OleDbCommand(sb.ToString, connection, groupTrans)
            GradeScalesDataAdapter.UpdateCommand = GradeScalesUpdateCommand

            '   add parameters for the update command
            GradeScalesUpdateCommand.Parameters.Add(New OleDbParameter("GrdScaleId", OleDbType.Guid, 16, "GrdScaleId"))
            GradeScalesUpdateCommand.Parameters.Add(New OleDbParameter("CampGrpId", OleDbType.Guid, 16, "CampGrpId"))
            GradeScalesUpdateCommand.Parameters.Add(New OleDbParameter("Descrip", OleDbType.VarChar, 80, "Descrip"))
            GradeScalesUpdateCommand.Parameters.Add(New OleDbParameter("StatusId", OleDbType.Guid, 16, "StatusId"))
            GradeScalesUpdateCommand.Parameters.Add(New OleDbParameter("GrdSystemId", OleDbType.Guid, 16, "GrdSystemId"))
            GradeScalesUpdateCommand.Parameters.Add(New OleDbParameter("InstructorId", OleDbType.Guid, 16, "InstructorId"))
            GradeScalesUpdateCommand.Parameters.Add(New OleDbParameter("ModUser", OleDbType.VarChar, 50, "ModUser"))
            GradeScalesUpdateCommand.Parameters.Add(New OleDbParameter("ModDate", OleDbType.DBTimeStamp, 8, "ModDate"))
            GradeScalesUpdateCommand.Parameters.Add(New OleDbParameter("Original_GrdScaleId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GrdScaleId", DataRowVersion.Original, Nothing))
            GradeScalesUpdateCommand.Parameters.Add(New OleDbParameter("Original_CampGrpId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CampGrpId", DataRowVersion.Original, Nothing))
            GradeScalesUpdateCommand.Parameters.Add(New OleDbParameter("Original_Descrip", OleDbType.VarChar, 80, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descrip", DataRowVersion.Original, Nothing))
            GradeScalesUpdateCommand.Parameters.Add(New OleDbParameter("Original_StatusId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "StatusId", DataRowVersion.Original, Nothing))
            GradeScalesUpdateCommand.Parameters.Add(New OleDbParameter("Original_GrdSystemId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GrdSystemId", DataRowVersion.Original, Nothing))
            GradeScalesUpdateCommand.Parameters.Add(New OleDbParameter("Original_InstructorId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "InstructorId", DataRowVersion.Original, Nothing))
            GradeScalesUpdateCommand.Parameters.Add(New OleDbParameter("Original_InstructorId1", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "InstructorId", DataRowVersion.Original, Nothing))
            GradeScalesUpdateCommand.Parameters.Add(New OleDbParameter("Original_ModUser", OleDbType.VarChar, 50, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ModUser", DataRowVersion.Original, Nothing))
            GradeScalesUpdateCommand.Parameters.Add(New OleDbParameter("Original_ModDate", OleDbType.DBTimeStamp, 8, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ModDate", DataRowVersion.Original, Nothing))
            GradeScalesUpdateCommand.Parameters.Add(New OleDbParameter("Select_GrdScaleId", OleDbType.Guid, 16, "GrdScaleId"))

            '   build delete query for the GradeScales data adapter
            sb = New StringBuilder
            With sb
                .Append("DELETE FROM arGradeScales ")
                .Append("WHERE ")
                .Append("       (GrdScaleId = ?) ")
                .Append("AND    (CampGrpId = ?) ")
                .Append("AND    (Descrip = ?) ")
                .Append("AND    (StatusId = ?) ")
                .Append("AND    (GrdSystemId = ?) ")
                .Append("AND    (InstructorId = ? OR (InstructorId Is Null and ? Is Null)) ")
                .Append("AND    (ModUser = ?) ")
                .Append("AND    (ModDate = ?) ")
            End With

            '   build delete command
            Dim GradeScalesDeleteCommand As New OleDbCommand(sb.ToString, connection, groupTrans)
            GradeScalesDataAdapter.DeleteCommand = GradeScalesDeleteCommand

            '   add parameters to the delete command
            GradeScalesDeleteCommand.Parameters.Add(New OleDbParameter("Original_GrdScaleId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GrdScaleId", DataRowVersion.Original, Nothing))
            GradeScalesDeleteCommand.Parameters.Add(New OleDbParameter("Original_CampGrpId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CampGrpId", DataRowVersion.Original, Nothing))
            GradeScalesDeleteCommand.Parameters.Add(New OleDbParameter("Original_Descrip", OleDbType.VarChar, 80, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descrip", DataRowVersion.Original, Nothing))
            GradeScalesDeleteCommand.Parameters.Add(New OleDbParameter("Original_StatusId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "StatusId", DataRowVersion.Original, Nothing))
            GradeScalesDeleteCommand.Parameters.Add(New OleDbParameter("Original_GrdSystemId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GrdSystemId", DataRowVersion.Original, Nothing))
            GradeScalesDeleteCommand.Parameters.Add(New OleDbParameter("Original_InstructorId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "InstructorId", DataRowVersion.Original, Nothing))
            GradeScalesDeleteCommand.Parameters.Add(New OleDbParameter("Original_InstructorId1", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "InstructorId", DataRowVersion.Original, Nothing))
            GradeScalesDeleteCommand.Parameters.Add(New OleDbParameter("Original_ModUser", OleDbType.VarChar, 50, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ModUser", DataRowVersion.Original, Nothing))
            GradeScalesDeleteCommand.Parameters.Add(New OleDbParameter("Original_ModDate", OleDbType.DBTimeStamp, 8, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ModDate", DataRowVersion.Original, Nothing))

            '   build select query for the GradeScaleDetails data adapter
            sb = New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("       GrdScaleDetailId, ")
                .Append("       GrdScaleId, ")
                .Append("       MinVal, ")
                .Append("       MaxVal, ")
                .Append("       GrdSysDetailId, ")
                .Append("       ViewOrder, ")
                .Append("       ModUser, ")
                .Append("       ModDate ")
                .Append("FROM arGradeScaleDetails ")
            End With

            '   build select command
            Dim GradeScaleDetailsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle GradeScalesDetails table
            Dim GradeScaleDetailsDataAdapter As New OleDbDataAdapter(GradeScaleDetailsSelectCommand)

            '   build insert, update and delete commands for GradeScaleDetails table
            'Dim cb1 As New OleDbCommandBuilder(GradeScaleDetailsDataAdapter)

            '   build insert query for the GradeScaleDetails data adapter
            sb = New StringBuilder
            With sb
                .Append("INSERT INTO ")
                .Append("       arGradeScaleDetails (")
                .Append("       GrdScaleDetailId, GrdScaleId, MinVal, MaxVal, ")
                .Append("       GrdSysDetailId, ViewOrder, ")
                .Append("       ModUser, ModDate) ")
                .Append("VALUES (?, ?, ?, ?, ?, ?, ?, ?); ")
                .Append("SELECT ")
                .Append("       GrdScaleDetailId, ")
                .Append("       GrdScaleId, ")
                .Append("       MinVal, ")
                .Append("       MaxVal, ")
                .Append("       GrdSysDetailId, ")
                .Append("       ViewOrder, ")
                .Append("       ModUser, ")
                .Append("       ModDate ")
                .Append("FROM arGradeScaleDetails ")
                .Append("WHERE ")
                .Append("       (GrdScaleDetailId = ?)")
            End With

            '   build insert command
            Dim GradeScaleDetailsInsertCommand As New OleDbCommand(sb.ToString, connection, groupTrans)
            GradeScaleDetailsDataAdapter.InsertCommand = GradeScaleDetailsInsertCommand

            '   add parameters for the insert command
            GradeScaleDetailsInsertCommand.Parameters.Add(New OleDbParameter("GrdScaleDetailId", OleDbType.Guid, 16, "GrdScaleDetailId"))
            GradeScaleDetailsInsertCommand.Parameters.Add(New OleDbParameter("GrdScaleId", OleDbType.Guid, 16, "GrdScaleId"))
            GradeScaleDetailsInsertCommand.Parameters.Add(New OleDbParameter("MinVal", OleDbType.SmallInt, 2, "MinVal"))
            GradeScaleDetailsInsertCommand.Parameters.Add(New OleDbParameter("MaxVal", OleDbType.SmallInt, 2, "MaxVal"))
            GradeScaleDetailsInsertCommand.Parameters.Add(New OleDbParameter("GrdSysDetailId", OleDbType.Guid, 16, "GrdSysDetailId"))
            GradeScaleDetailsInsertCommand.Parameters.Add(New OleDbParameter("ViewOrder", OleDbType.Integer, 4, "ViewOrder"))
            GradeScaleDetailsInsertCommand.Parameters.Add(New OleDbParameter("ModUser", OleDbType.VarChar, 50, "ModUser"))
            GradeScaleDetailsInsertCommand.Parameters.Add(New OleDbParameter("ModDate", OleDbType.DBTimeStamp, 8, "ModDate"))
            GradeScaleDetailsInsertCommand.Parameters.Add(New OleDbParameter("Select_GrdScaleDetailId", OleDbType.Guid, 16, "GrdScaleDetailId"))


            '   build update query for the GradeScaleDetails data adapter
            sb = New StringBuilder
            With sb
                .Append("UPDATE arGradeScaleDetails ")
                .Append("SET ")
                .Append("       GrdScaleDetailId = ?, ")
                .Append("       MinVal = ?, ")
                .Append("       MaxVal = ?, ")
                .Append("       GrdScaleId = ?, ")
                .Append("       GrdSysDetailId = ?, ")
                .Append("       ViewOrder = ?, ")
                .Append("       ModUser = ?, ")
                .Append("       ModDate = ? ")
                .Append("WHERE ")
                .Append("       (GrdScaleDetailId = ?) ")
                .Append("AND    (MinVal = ?) ")
                .Append("AND    (MaxVal = ?) ")
                .Append("AND    (GrdScaleId = ?) ")
                .Append("AND    (GrdSysDetailId = ?) ")
                .Append("AND    (ViewOrder = ?) ")
                .Append("AND    (ModUser = ?) ")
                .Append("AND    (ModDate = ?); ")
                .Append("SELECT ")
                .Append("       GrdScaleDetailId, ")
                .Append("       MinVal, ")
                .Append("       MaxVal, ")
                .Append("       GrdScaleId, ")
                .Append("       GrdSysDetailId, ")
                .Append("       ViewOrder, ")
                .Append("       ModUser, ")
                .Append("       ModDate ")
                .Append("FROM   arGradeScaleDetails ")
                .Append("WHERE  (GrdScaleDetailId = ? ) ")
            End With

            '   build update command
            Dim GradeScaleDetailsUpdateCommand As OleDbCommand = New OleDbCommand(sb.ToString, connection, groupTrans)
            'Dim GradeScaleDetailsUpdateCommand As New OleDbCommand(sb.ToString, connection)

            GradeScaleDetailsDataAdapter.UpdateCommand = GradeScaleDetailsUpdateCommand

            '   add parameters for the update command
            GradeScaleDetailsUpdateCommand.Parameters.Add(New OleDbParameter("GrdScaleDetailId", OleDbType.Guid, 16, "GrdScaleDetailId"))
            GradeScaleDetailsUpdateCommand.Parameters.Add(New OleDbParameter("MinVal", OleDbType.SmallInt, 2, "MinVal"))
            GradeScaleDetailsUpdateCommand.Parameters.Add(New OleDbParameter("MaxVal", OleDbType.SmallInt, 2, "MaxVal"))
            GradeScaleDetailsUpdateCommand.Parameters.Add(New OleDbParameter("GrdScaleId", OleDbType.Guid, 16, "GrdScaleId"))
            GradeScaleDetailsUpdateCommand.Parameters.Add(New OleDbParameter("GrdSysDetailId", OleDbType.Guid, 16, "GrdSysDetailId"))
            GradeScaleDetailsUpdateCommand.Parameters.Add(New OleDbParameter("ViewOrder", OleDbType.Integer, 4, "ViewOrder"))
            GradeScaleDetailsUpdateCommand.Parameters.Add(New OleDbParameter("ModUser", OleDbType.VarChar, 50, "ModUser"))
            GradeScaleDetailsUpdateCommand.Parameters.Add(New OleDbParameter("ModDate", OleDbType.DBTimeStamp, 8, "ModDate"))
            GradeScaleDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_GrdScaleDetailId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GrdScaleDetailId", DataRowVersion.Original, Nothing))
            GradeScaleDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_MinVal", OleDbType.SmallInt, 2, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MinVal", DataRowVersion.Original, Nothing))
            GradeScaleDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_MaxVal", OleDbType.SmallInt, 2, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaxVal", DataRowVersion.Original, Nothing))
            GradeScaleDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_GrdScaleId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GrdScaleId", DataRowVersion.Original, Nothing))
            GradeScaleDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_GrdSysDetailId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GrdSysDetailId", DataRowVersion.Original, Nothing))
            GradeScaleDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_ViewOrder", OleDbType.Integer, 4, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ViewOrder", DataRowVersion.Original, Nothing))
            GradeScaleDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_ModUser", OleDbType.VarChar, 50, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ModUser", DataRowVersion.Original, Nothing))
            GradeScaleDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Original_ModDate", OleDbType.DBTimeStamp, 8, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ModDate", DataRowVersion.Original, Nothing))
            GradeScaleDetailsUpdateCommand.Parameters.Add(New OleDbParameter("Select_GrdScaleDetailId", OleDbType.Guid, 16, "GrdScaleDetailId"))

            '   build delete query for the GradeScaleDetails data adapter
            sb = New StringBuilder
            With sb
                .Append("DELETE FROM arGradeScaleDetails ")
                .Append("WHERE ")
                .Append("       (GrdScaleDetailId = ?) ")
                .Append("AND    (MinVal = ?)  ")
                .Append("AND    (MaxVal = ?) ")
                .Append("AND    (GrdScaleId = ?) ")
                .Append("AND    (GrdSysDetailId = ?) ")
                .Append("AND    (ViewOrder = ?) ")
                .Append("AND    (ModUser = ?) ")
                .Append("AND    (ModDate = ?) ")
            End With

            '   build delete command
            Dim GradeScaleDetailsDeleteCommand As New OleDbCommand(sb.ToString, connection, groupTrans)
            'Dim GradeScaleDetailsDeleteCommand As New OleDbCommand(sb.ToString, connection)

            GradeScaleDetailsDataAdapter.DeleteCommand = GradeScaleDetailsDeleteCommand

            '   add parameters to the delete command
            GradeScaleDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_GrdScaleDetailId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GrdScaleDetailId", DataRowVersion.Original, Nothing))
            GradeScaleDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_MinVal", OleDbType.SmallInt, 2, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MinVal", DataRowVersion.Original, Nothing))
            GradeScaleDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_MaxVal", OleDbType.SmallInt, 2, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaxVal", DataRowVersion.Original, Nothing))
            GradeScaleDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_GrdScaleId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GrdScaleId", DataRowVersion.Original, Nothing))
            GradeScaleDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_GrdSysDetailId", OleDbType.Guid, 16, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GrdSysDetailId", DataRowVersion.Original, Nothing))
            GradeScaleDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_ViewOrder", OleDbType.Integer, 4, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ViewOrder", DataRowVersion.Original, Nothing))
            GradeScaleDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_ModUser", OleDbType.VarChar, 50, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ModUser", DataRowVersion.Original, Nothing))
            GradeScaleDetailsDeleteCommand.Parameters.Add(New OleDbParameter("Original_ModDate", OleDbType.DBTimeStamp, 8, ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ModDate", DataRowVersion.Original, Nothing))

            '   insert added rows in GradeScales table
            GradeScalesDataAdapter.Update(ds.Tables("arGradeScales").Select(Nothing, Nothing, DataViewRowState.Added))

            '   insert added rows in GradeScaleDetails table
            GradeScaleDetailsDataAdapter.Update(ds.Tables("arGradeScaleDetails").Select(Nothing, Nothing, DataViewRowState.Added))

            '   delete rows in GradeScaleDetails table
            GradeScaleDetailsDataAdapter.Update(ds.Tables("arGradeScaleDetails").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   delete rows in GradeScales table
            GradeScalesDataAdapter.Update(ds.Tables("arGradeScales").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   update rows in GradeScales table
            GradeScalesDataAdapter.Update(ds.Tables("arGradeScales").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   update rows in GradeScaleDetails table
            GradeScaleDetailsDataAdapter.Update(ds.Tables("arGradeScaleDetails").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            Return ""

        Catch ex As OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally

            '   close connection
            connection.Close()
        End Try

    End Function
    Public Function GetAllGradeSystems(ByVal showActiveOnly As Boolean) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   GS.GrdSystemId, GS.StatusId, GS.Descrip, GS.CampGrpId ")
            .Append("FROM     arGradeSystems GS, syStatuses ST ")
            .Append("WHERE    GS.StatusId = ST.StatusId ")
            '   Conditionally include only Active Items 
            If showActiveOnly Then
                .Append(" AND     ST.Status = 'Active' ")
            End If
            .Append("ORDER BY GS.Descrip ")
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllGrades(ByVal GrdSystemId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select Distinct t2.Grade,t2.GrdSysDetailId ")
            .Append(" from arGradeSystemDetails t2 ")
            .Append(" where GrdSystemId=? ")
            .Append(" order by t2.Grade ")
        End With
        db.AddParameter("@GrdSystemId", GrdSystemId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllGradesDescripSystems() As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       GS.GrdSystemId, ")
            .Append("       GS.Descrip, ")
            .Append("       GS.StatusId, ")
            .Append("       (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("       GS.CampGrpId, ")
            .Append("       GS.ModUser, ")
            .Append("       GS.ModDate ")
            .Append("FROM   arGradeSystems GS, syStatuses ST ")
            .Append("WHERE  GS.StatusId = ST.StatusId ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetGradesInfo(ByVal GrdSysDetailId As String) As GrdPostingsInfo

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select Distinct Grade,GradeDescription,Quality,GrdSystemId,GrdSysDetailId from arGradeSystemDetails where GrdSysDetailId=? ")
        End With
        ' Add the bankIdto the parameter list
        db.AddParameter("@GrdSysDetailId", GrdSysDetailId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim grdInfo As New GrdPostingsInfo

        While dr.Read()
            '   set properties with data from DataReader
            With grdInfo
                .IsInDB = True
                If Not dr("GradeDescription") Is DBNull.Value Then .GradeDescription = dr("GradeDescription") Else .GradeDescription = ""
                If Not dr("Grade") Is DBNull.Value Then .Grade = dr("Grade") Else .Grade = ""
                If Not dr("Quality") Is DBNull.Value Then .GradeQuality = dr("Quality") Else .GradeQuality = ""
                If Not dr("GrdSysDetailId") Is DBNull.Value Then .GrdSysDetailId = dr("GrdSysDetailId").ToString Else .GrdSysDetailId = ""
                If Not dr("GrdSystemId") Is DBNull.Value Then .GrdSystemId = dr("GrdSystemId").ToString Else .GrdSystemId = ""
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return grdInfo
    End Function
    Public Function GetGradesInfo_SP(ByVal GrdSysDetailId As String) As GrdPostingsInfo

        '   connect to the database
        Dim grdInfo As New GrdPostingsInfo
        Dim dr As SqlDataReader
        Dim db As New SQLDataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")


        db.AddParameter("@grdSysDetailId", New Guid(GrdSysDetailId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        dr = db.RunParamSQLDataReader_SP("dbo.usp_GetGradesInfo")
        Try
            While dr.Read()
                '   set properties with data from DataReader
                With grdInfo
                    .IsInDB = True
                    If Not dr("GradeDescription") Is DBNull.Value Then .GradeDescription = dr("GradeDescription") Else .GradeDescription = ""
                    If Not dr("Grade") Is DBNull.Value Then .Grade = dr("Grade") Else .Grade = ""
                    If Not dr("Quality") Is DBNull.Value Then .GradeQuality = dr("Quality") Else .GradeQuality = ""
                    If Not dr("GrdSysDetailId") Is DBNull.Value Then .GrdSysDetailId = dr("GrdSysDetailId").ToString Else .GrdSysDetailId = ""
                    If Not dr("GrdSystemId") Is DBNull.Value Then .GrdSystemId = dr("GrdSystemId").ToString Else .GrdSystemId = ""
                End With
            End While
        Finally
            dr.Close() 'Close Connection
        End Try

        '   Return BankInfo
        Return grdInfo
    End Function
    Public Sub UpdateGrades(ByVal GrdSysDetailId As String, ByVal Quality As String, ByVal GradeDescription As String)
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" Update arGradeSystemDetails set GradeDescription=?,Quality=? where GrdSysDetailId=? ")
        End With
        db.AddParameter("@GradeDescription", GradeDescription, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@Quality", Quality, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@GrdSysDetailId", GrdSysDetailId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)
    End Sub
    Public Function GetGradesByResultId(ByVal grdSysDetailId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       GrdSysDetailId, ")
            .Append("       Grade ")
            .Append("FROM  ")
            .Append("       arGradeSystemDetails ")
            .Append("WHERE ")
            .Append("       GrdSystemId=(Select GrdSystemId from arGradeSystemDetails where GrdSysDetailId = ? ) ")
            .Append("ORDER BY ViewOrder ")
        End With

        'add resultId to the parameter list
        db.AddParameter("@GrdSysDetailId", grdSysDetailId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    'Public Function DoGrdScalesExistFrGrdSys(ByVal GradeSystem As String) As Integer
    '    Dim ds As New DataSet
    '    Dim da As OleDbDataAdapter
    '    Dim sGrdSysDetailId As String
    '    Dim rowCount As Integer
    '    Try
    '        '   connect to the database
    '        Dim db As New DataAccess
    '        db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '        Dim sb As New System.Text.StringBuilder

    '        '   build the sql query
    '        With sb
    '            .Append("Select count(*) as Count from arGradeScales a where a.GrdSystemId = ? ")
    '        End With

    '        ' Add the PrgVerId and ChildId to the parameter list
    '        db.AddParameter("@PrgVerId", GradeSystem, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.OpenConnection()

    '        'Execute the query
    '        rowCount = db.RunParamSQLScalar(sb.ToString)


    '        'Close Connection
    '        db.CloseConnection()

    '    Catch ex As System.Exception
    '        'Throw New BaseException(ex.InnerException.ToString)
    '    End Try

    '    'Return the datatable in the dataset
    '    Return rowCount

    'End Function
    Public Function GetAllGradeScales_SP(ByVal showActiveOnly As Boolean, ByVal campusId As String) As DataTable

        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@showActiveOnly", showActiveOnly, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet("dbo.usp_GetAllGradeScales", Nothing, "SP").Tables(0)

    End Function
    Public Function GetSysGradeBookComponents() As DataSet
        Dim db As New SQLDataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Purpose of the SP: To get the Grade System Component Types
        'Create Procedure SP_GetGradeSystemComponentTypes
        'as
        'select Distinct ResourceId,Resource from syResources where resourcetypeId=10
        'order by Resource
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Dim ds As DataSet
        ds = db.RunParamSQLDataSet_SP("dbo.USP_SystemComponentTypes_GetList", Nothing)
        Try
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function GetSystemComponents(ByVal GrdComponentTypeId As String) As Integer
        Dim db As New SQLDataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Purpose of the SP: To get the System Component Types based on GrdComponentTypeId
        '    Create Procedure SP_GetSystemComponentByGradeComponents
        '    @GrdComponentTypeId varchar(36),
        '    @returnvalue int output
        '    as
        '    set @returnvalue = (select Top 1 SysComponentTypeId 
        '                       from arGrdComponentTypes 
        '                       where GrdComponentTypeId=@GrdComponentTypeId)
        '    if @returnvalue > 0
        '       begin()
        '         set @returnvalue = @returnvalue
        '       End
        '    Else
        '       begin()
        '           set @returnvalue=0
        '       End
        '    go()
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Dim intSystemComponent As Integer = 0
        ''Dim returnvalue As Integer = 0
        'db.AddParameter("@GrdComponentTypeId", GrdComponentTypeId, FAME.DataAccessLayer.SQLDataAccess.SqlDataType.SqlGuid.ToString, , ParameterDirection.Input)
        'db.AddParameter("@ReturnValue", 0, FAME.DataAccessLayer.SQLDataAccess.SqlDataType.SqlInteger, , ParameterDirection.Output)
        'Try
        '    db.RunParamSQLExecuteNoneQuery_SP("USP_Execute_SP_GetSystemComponentByGradeComponents", Nothing)

        '    intSystemComponent = Int32.Parse(db.
        'Catch ex As System.Exception
        '    Return 0
        'Finally
        '    db.CloseConnection()
        'End Try

        Using cn As New SqlConnection(GetAdvAppSettings.AppSettings("ConnectionString"))
            Using cmd As New SqlCommand("dbo.USP_SystemComponent_GetSingle")
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add(New SqlParameter("@GrdComponentTypeId", SqlDbType.VarChar))
                cmd.Parameters("@GrdComponentTypeId").Value = GrdComponentTypeId.ToString

                Dim returnparameter As SqlParameter = New SqlParameter("@ReturnValue", 0)
                returnparameter.Direction = ParameterDirection.Output
                cmd.Parameters.Add(returnparameter)


                cn.Open()
                cmd.Connection = cn
                cmd.ExecuteNonQuery()

                Dim intSysCompTypeId As Integer
                intSysCompTypeId = Int32.Parse(cmd.Parameters("@ReturnValue").Value.ToString())

                cmd.Dispose()
                cn.Dispose()

                Return intSysCompTypeId
            End Using
        End Using
    End Function
    Public Sub UpdateSysComponentTypes(ByVal GrdComponentTypeId As String, ByVal SysCompTypeId As Integer)
        Dim db As New SQLDataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'Dim intSystemComponent As Integer = 0
        Try
            db.AddParameter("@GrdComponentTypeId", New Guid(GrdComponentTypeId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@SystemComponentTypeId", SysCompTypeId, SqlDbType.Int, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_SystemComponentTypes_Update")
        Catch ex As Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Sub
    Public Sub UpdateSysComponentTypesandAddCourses(ByVal GrdComponentTypeId As String, Optional ByVal xmlCourses As String = "")
        Dim db As New SQLDataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'Dim intSystemComponent As Integer
        Try
            db.AddParameter("@GrdComponentTypeId", New Guid(GrdComponentTypeId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@SystemComponentTypeId", 612, SqlDbType.Int, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_SystemComponentTypes_Update")
        Catch ex As Exception
        Finally
            db.ClearParameters()
        End Try


        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@CourseValues", xmlCourses, SqlDbType.NText, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_Courses_Insert", Nothing)
        Catch ex As Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Sub
    Public Function GetCoursesByCampusIdandStatusId(ByVal CampusId As String, _
                                                    ByVal StatusId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As DataSet

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@CampusId", New Guid(CampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@StatusId", New Guid(StatusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_Courses_GetList", "Courses")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function GetCoursesByGrdComponentTypeId(ByVal GrdComponentTypeId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As New DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@GrdCompTypeId", New Guid(GrdComponentTypeId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_CoursesFromBridgeTable_GetList", "Courses")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function GetCountbySysCompTypeIdandCourseId(ByVal CourseId As String) As Integer
        Dim db As New SQLDataAccess
        'Dim ds As New DataSet
        Dim intRowCount As Integer


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@ReqId", New Guid(CourseId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            intRowCount = db.RunParamSQLScalar_SP("dbo.USP_DictationComponents_GetCount")
            Return intRowCount
        Catch ex As Exception
            Return 0
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function boolDictationSpeedTestComponentExist(ByVal ResourceId As Integer) As Boolean

        ' Objective : To be used for Unit Testing.
        ' This procedure will check if the component Dictation/Speed Test exist in syResources Table
        ' The resource id for Dictation/Speed Test Component is 612.

        Dim db As New SQLDataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        Dim intRecordExists As Integer = 0
        Try
            db.AddParameter("@ResourceId", ResourceId, SqlDbType.Int, , ParameterDirection.Input)
            intRecordExists = db.RunParamSQLScalar_SP("dbo.USP_DictationSpeedTestResourceId_GetSingle")
        Catch ex As Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        If intRecordExists = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    Public Function GetTestType() As DataTable
        Dim db As New SQLDataAccess
        Dim ds As DataSet

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            ds = db.RunParamSQLDataSet_SP("dbo.usp_testtype_getlist", "TestType")
            Return ds.Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function


    'Added by : Balaji
    'Modified Date : 1/14/2010
    Public Function GetCCRCoursesByCampusIdandStatusId(ByVal CampusId As String, _
                                                 ByVal StatusId As String) As DataSet

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'if exists (select * from sysobjects where type='P' and name='USP_CCR_Courses_GetList')
        '       begin
        '            drop procedure USP_CCR_Courses_GetList
        '       End
        'go
        '       Create Procedure USP_CCR_Courses_GetList
        '           @CampusId uniqueidentifier,
        '           @StatusId uniqueidentifier
        '       as
        '            Select Distinct
        '			    	t1.ReqId,t1.Descrip,t1.Code
        '            FROM 
        '			    	arReqs t1 inner join arBridge_GradeComponentTypes_Courses t2 on t1.ReqId = t2.ReqId
        '            WHERE 
        '			    	t1.ReqTypeId = 1 AND 
        '				    t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = @CampusId) AND 
        '				    t1.StatusId=@StatusId
        '           ORDER BY
        '			    	t1.Descrip
        'go
        '-- exec USP_CCR_Courses_GetList 'A78274B8-1CF0-4891-BDAC-867DF80BB6EC','F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
        '--go
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Dim db As New SQLDataAccess
        Dim ds As DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@CampusId", New Guid(CampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@StatusId", New Guid(StatusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_CCR_Courses_GetList", "Courses")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function

    Public Function GetPages(ByVal RoleId As String, ByVal ModuleResourceId As Integer, ByVal PageType As Integer,
                             ByVal intSchoolOptions As Integer, ByVal strCampusID As String) As DataTable
        'Public Function GetPages(ByVal RoleId As String, ByVal ModuleResourceId As Integer, ByVal PageType As Integer,ByVal @CampusId as String ) As DataTable
        Dim db As New SQLDataAccess
        Dim ds As DataSet
        Dim strStoredProcedureName As String


        Select Case PageType
            Case 3 'Common Tasks
                strStoredProcedureName = "dbo.USP_ManageSecurity_CommonTasks_Permissions"
            Case 4 'Maintenance
                strStoredProcedureName = "dbo.USP_ManageSecurity_Maintenance_Permissions"
            Case 5 'Reports
                strStoredProcedureName = "dbo.USP_ManageSecurity_Reports_Permissions"
            Case 394, 395, 396, 397, 689 'Tabs   '- 689 added for ipeds page - DE7659
                strStoredProcedureName = "dbo.USP_ManageSecurity_Tabs_Permissions"
            Case 999
                strStoredProcedureName = "dbo.USP_ManageSecurityForPopups_GetList"
        End Select

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")

        Try
            db.OpenConnection()
            Select Case PageType
                Case 3, 4, 5 'Common Tasks, Maintenance, and Reports
                    db.AddParameter("@RoleId", RoleId, SqlDbType.VarChar, 50, ParameterDirection.Input)
                    db.AddParameter("@ModuleResourceId", ModuleResourceId, SqlDbType.VarChar, 50, ParameterDirection.Input)
                    db.AddParameter("@SchoolEnumerator", intSchoolOptions, SqlDbType.Int, , ParameterDirection.Input)
                    db.AddParameter("@CampusId", strCampusID, SqlDbType.VarChar, 50, ParameterDirection.Input)
                Case 999
                    db.AddParameter("@RoleId", RoleId, SqlDbType.VarChar, 50, ParameterDirection.Input)
                Case Else 'Tabs
                    db.AddParameter("@RoleId", RoleId, SqlDbType.VarChar, 50, ParameterDirection.Input)
                    db.AddParameter("@ModuleResourceId", ModuleResourceId, SqlDbType.VarChar, 50, ParameterDirection.Input)
                    db.AddParameter("@TabId", PageType, SqlDbType.Int, , ParameterDirection.Input)
                    db.AddParameter("@SchoolEnumerator", intSchoolOptions, SqlDbType.Int, , ParameterDirection.Input)
                    db.AddParameter("@CampusId", strCampusID, SqlDbType.VarChar, 50, ParameterDirection.Input)
            End Select
            'DE7659
            ds = db.RunParamSQLDataSet_SP(strStoredProcedureName, "ARCommonTasks")
            Return ds.Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function AddPagesToModules(ByVal intSchoolOptions As Integer) As DataTable
        Dim db As New SQLDataAccess
        Dim ds As DataSet
        Dim strStoredProcedureName As String

        strStoredProcedureName = "dbo.USP_AddStudentPageToOtherModules"

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@SchoolEnumerator", intSchoolOptions, SqlDbType.Int, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP(strStoredProcedureName, "StudentPages")
            Return ds.Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function GetARCommonTasksChild(ByVal ParentResourceId As Integer) As DataTable
        Dim db As New SQLDataAccess
        Dim ds As DataSet

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@ParentResourceId", ParentResourceId, SqlDbType.Int, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_ManageSecurity_AR_SubMenus_GetChild", "ARCommonTasks")
            Return ds.Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function GetARCommonTasksChildDDL(ByVal ParentResourceId As Integer) As DataTable
        Dim db As New SQLDataAccess
        Dim ds As DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@ParentResourceId", ParentResourceId, SqlDbType.Int, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_ManageSecurity_AR_SubMenus_DDL", "ARCommonTasks")
            Return ds.Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    'Added by : Balaji
    'Modified Date : 1/14/2010
    Public Sub UpdateProgramCIPCode(ByVal CIPCode As String, _
                                       ByVal CredentialLevel As String, _
                                       ByVal ProgId As String, _
                                       Optional ByVal IsGEProgram As Boolean = True, _
                                       Optional ByVal Is1098T As Boolean = True)
        Dim db As New SQLDataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Purpose of the SP: To get the Grade System Component Types
        'Create Procedure SP_GetGradeSystemComponentTypes
        'as
        'select Distinct ResourceId,Resource from syResources where resourcetypeId=10
        'order by Resource
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Dim ds As New DataSet
        Try
            db.OpenConnection()
            db.AddParameter("@CIPCode", CIPCode, SqlDbType.VarChar, 6, ParameterDirection.Input)
            db.AddParameter("@CredentialLevel", CredentialLevel, SqlDbType.VarChar, 2, ParameterDirection.Input)
            db.AddParameter("@IsGEProgram", IsGEProgram, SqlDbType.Bit, , ParameterDirection.Input)
            db.AddParameter("@ProgId", ProgId, SqlDbType.VarChar, , ParameterDirection.Input)
            db.AddParameter("@Is1098T", Is1098T, SqlDbType.VarChar, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_Programs_Update")
        Catch ex As Exception
        Finally
            db.CloseConnection()
        End Try
    End Sub
    Public Function GetProgramCIPCode(ByVal ProgId As String) As DataSet

        Dim db As New SQLDataAccess
        Dim ds As DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@ProgId", ProgId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_Programs_Select", "Courses")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function BuildDSForDataExport(ByVal AwardYear As String, ByVal CampusId As String, Optional ByVal ProgramId As String = "") As DataSet

        Dim db As New SQLDataAccess
        Dim ds As DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@AwardYear", AwardYear, SqlDbType.VarChar, 50, ParameterDirection.Input)
            db.AddParameter("@CampusId", CampusId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            If ProgramId.Trim = "" Then
                db.AddParameter("@ProgramId", DBNull.Value, SqlDbType.VarChar, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ProgramId", ProgramId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            End If
            ds = db.RunParamSQLDataSet_SP("GetNSLDSExportData", "radGridDS")
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function

    Public Function GetCampusCodeFromCampusId(ByVal campusId As String) As String

        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT CampCode From SyCampuses WHERE CampusId = ?")
        End With

        'add resultId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Dim result As String = db.RunParamSQLScalar(sb.ToString()).ToString()
        Return result
    End Function

    Public Function GetProgCodeFromProgId(ByVal progId As String) As String

        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ProgCode From arPrograms WHERE ProgId = ?")
        End With

        'add resultId to the parameter list
        db.AddParameter("@ProgId", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Dim result As String = db.RunParamSQLScalar(sb.ToString()).ToString()
        Return result
    End Function


    Public Function GetRowsCount(ByVal CampusId As String, ByVal TransStartDate As Date, ByVal TransEndDate As Date, ByVal Transaction As String) As Integer
        Dim db As New SQLDataAccess
        Dim ds As DataSet



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@CampusId", New Guid(CampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@TransStartDate", TransStartDate, SqlDbType.Date, , ParameterDirection.Input)
            db.AddParameter("@TransEndDate", TransEndDate, SqlDbType.Date, , ParameterDirection.Input)
            db.AddParameter("@Transaction", Transaction, SqlDbType.VarChar, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_ApplicantLedger_ByParameters_GetList", "AppGetListofLeads")
            Dim intRowCount As Integer
            intRowCount = ds.Tables(0).Rows.Count
            Return intRowCount
        Catch ex As Exception
            Return 0
        Finally
            db.CloseConnection()
        End Try
    End Function
End Class
