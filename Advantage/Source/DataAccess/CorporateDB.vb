Imports FAME.Advantage.Common

Public Class CorporateDB
    Public Function GetCorporateInfo() As CorporateInfo
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        'Dim ds As DataSet
        'Dim dt As DataTable
        Dim CorpInfo As New CorporateInfo

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        If MyAdvAppSettings.AppSettings("CorporateName").ToString() = "" Then
            '   Retrieve information from syCampuses
            With sb
                .Append("SELECT ")
                .Append("       CampDescrip AS CorporateName,Address1,Address2,City,Zip,")
                .Append("       (SELECT StateDescrip FROM syStates WHERE StateId=C.StateId) AS State,")
                .Append("       (SELECT CountryDescrip FROM adCountries WHERE CountryId=C.CountryId) AS Country,Fax,")
                .Append("       Phone = CASE WHEN (Phone1 IS NULL) THEN (CASE WHEN (Phone2 IS NULL) THEN (CASE WHEN (Phone3 IS NULL) THEN '' ELSE Phone3 END) ELSE Phone2 END) ELSE Phone1 END ")
                .Append("FROM   syCampuses C ")
                .Append("WHERE  IsCorporate = 1 ")
            End With

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            '   Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read
                With CorpInfo
                    If Not (dr("CorporateName") Is System.DBNull.Value) Then .CorporateName = dr("CorporateName")
                    If Not (dr("Address1") Is System.DBNull.Value) Then .Address1 = dr("Address1")
                    If Not (dr("Address2") Is System.DBNull.Value) Then .Address2 = dr("Address2")
                    If Not (dr("City") Is System.DBNull.Value) Then .City = dr("City")
                    If Not (dr("Zip") Is System.DBNull.Value) Then .Zip = dr("Zip")
                    If Not (dr("State") Is System.DBNull.Value) Then .State = dr("State")
                    If Not (dr("Country") Is System.DBNull.Value) Then .Country = dr("Country")
                    If Not (dr("Phone") Is System.DBNull.Value) Then .Phone = dr("Phone")
                    If Not (dr("Fax") Is System.DBNull.Value) Then .Fax = dr("Fax")
                End With
            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Else

            '   Retrieve information from Web.Config
            With CorpInfo
                .CorporateName = MyAdvAppSettings.AppSettings("CorporateName").ToString.ToUpper
                .Address1 = MyAdvAppSettings.AppSettings("CorporateAddress1").ToString.ToUpper
                .Address2 = MyAdvAppSettings.AppSettings("CorporateAddress2").ToString.ToUpper
                .City = MyAdvAppSettings.AppSettings("CorporateCity").ToString.ToUpper
                .State = MyAdvAppSettings.AppSettings("CorporateState").ToString.ToUpper
                .Zip = MyAdvAppSettings.AppSettings("CorporateZip").ToString.ToUpper
                '   Get first phone that it is not blank
                If MyAdvAppSettings.AppSettings("CorporatePhone1").ToString <> "" Then
                    .Phone = MyAdvAppSettings.AppSettings("CorporatePhone1").ToUpper
                ElseIf MyAdvAppSettings.AppSettings("CorporatePhone2").ToString <> "" Then
                    .Phone = MyAdvAppSettings.AppSettings("CorporatePhone2").ToUpper
                ElseIf MyAdvAppSettings.AppSettings("CorporatePhone3").ToString <> "" Then
                    .Phone = MyAdvAppSettings.AppSettings("CorporatePhone3").ToUpper
                End If
                .Fax = MyAdvAppSettings.AppSettings("CorporateFax").ToString.ToUpper
                .Country = MyAdvAppSettings.AppSettings("CorporateCountry").ToString.ToUpper
            End With

        End If

        '   Return CorporateInfo
        Return CorpInfo
    End Function

    ''Added by saraswathi lakshmanan on June 17 2009
    ''The invoice address is printed in the receipts
    ''To fix mantis case 13365


    Public Function GetInvoiceAddressInfo(ByVal CampusId As String) As CorporateInfo
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        'Dim ds As DataSet
        'Dim dt As DataTable
        Dim CorpInfo As New CorporateInfo

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   Retrieve information from syCampuses
        With sb
            '    .Append("SELECT ")
            '    .Append("       CampDescrip AS CorporateName,Address1,Address2,City,Zip,")
            '    .Append("       (SELECT StateDescrip FROM syStates WHERE StateId=C.StateId) AS State,")
            '    .Append("       (SELECT CountryDescrip FROM adCountries WHERE CountryId=C.CountryId) AS Country,Fax,")
            '    .Append("       Phone = CASE WHEN (Phone1 IS NULL) THEN (CASE WHEN (Phone2 IS NULL) THEN (CASE WHEN (Phone3 IS NULL) THEN '' ELSE Phone3 END) ELSE Phone2 END) ELSE Phone1 END ")
            '    .Append("FROM   syCampuses C ")
            '.Append("WHERE  IsCorporate = 1 ")


            .Append("SELECT ")
            .Append("       CampDescrip AS CorporateName,InvAddress1,InvAddress2,InvCity,InvZip,")
            .Append("       (SELECT StateDescrip FROM syStates WHERE StateId=C.InvStateId) AS State,")
            .Append("       (SELECT CountryDescrip FROM adCountries WHERE CountryId=C.InvCountryId) AS Country,InvFax,")
            '.Append("       Phone = CASE WHEN (InvPhone1 IS NULL) THEN (CASE WHEN (InvPhone2 IS NULL) THEN (CASE WHEN (InvPhone3 IS NULL) THEN '' ELSE InvPhone3 END) ELSE InvPhone2 END) ELSE InvPhone1 END ")
            .Append("      InvPhone1 as Phone  ")
            .Append("FROM   syCampuses C  where campusId= '" + CampusId + "'")

        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read
            With CorpInfo
                If Not (dr("CorporateName") Is System.DBNull.Value) Then .CorporateName = dr("CorporateName")
                If Not (dr("InvAddress1") Is System.DBNull.Value) Then .Address1 = dr("InvAddress1")
                If Not (dr("InvAddress2") Is System.DBNull.Value) Then .Address2 = dr("InvAddress2")
                If Not (dr("InvCity") Is System.DBNull.Value) Then .City = dr("InvCity")
                If Not (dr("InvZip") Is System.DBNull.Value) Then .Zip = dr("InvZip")
                If Not (dr("State") Is System.DBNull.Value) Then .State = dr("State")
                If Not (dr("Country") Is System.DBNull.Value) Then .Country = dr("Country")
                If Not (dr("Phone") Is System.DBNull.Value) Then .Phone = dr("Phone")
                If Not (dr("InvFax") Is System.DBNull.Value) Then .Fax = dr("InvFax")
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return CorporateInfo
        Return CorpInfo
    End Function

    ''Added by saraswathi lakshmanan on June 18 2009 
    ''To check if invoice address information is available for the selected campus.

    Public Function ValidateIfInvoiceAddressAvialable(ByVal CampusId As String) As Boolean
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT ")
            .Append("  InvAddress1,InvCity,InvZip,")
            .Append("  InvStateId ")
            .Append("FROM   syCampuses  where campusId= '" + CampusId + "'")

        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If (ds.Tables(0).Rows(0)("InvAddress1") Is System.DBNull.Value) And (ds.Tables(0).Rows(0)("InvCity") Is System.DBNull.Value) And (ds.Tables(0).Rows(0)("InvZip") Is System.DBNull.Value) And (ds.Tables(0).Rows(0)("InvStateId") Is System.DBNull.Value) Then
                    Return False
                Else
                    Return True
                End If
            Else
                Return False
            End If
        Else
            Return False
        End If

        Return False
    End Function
End Class
