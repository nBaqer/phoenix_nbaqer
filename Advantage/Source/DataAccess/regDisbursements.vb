
Imports FAME.Advantage.Common

Public Class regDisbursements
    Public Function ImportDISBDataFromRegent(ByVal strXMLFile As String, ByVal CampusId As String, ByVal user As String) As String
        Dim ds As New disbursementExport
        Dim dtStudent As New DataTable
        Dim dtGetStudentEnrollment As New DataTable
        Dim dtDisbursements As New DataTable
        Dim dtSession As New DataTable
        Dim dtAward As New DataTable
        Dim dtDisbursementDetails As New DataTable
        Dim dtName As New DataTable
        Dim strSSN As String
        Dim strSessionCode As String = ""
        Dim strSessionDescription As String = ""
        Dim strEquivalentSession As String = ""
        Dim strAwardCode As String = ""
        Dim strAwardDescription As String = ""
        Dim strAwardBillCode As String = ""
        Dim strDisbDate As String = ""
        Dim strDisbTime As String = ""
        Dim decDisbAmount As Decimal = 0.0
        Dim strTermId As String = ""
        Dim strStuEnrollId As String = ""
        Dim strStudentAwardId As String = ""
        Dim strStudentName As String = ""

        ds.ReadXml(strXMLFile)

        dtStudent = ds.Tables("student")
        dtDisbursements = ds.Tables("disbursements")
        dtSession = ds.Tables("session")
        dtAward = ds.Tables("award")
        dtName = ds.Tables("name")
        dtDisbursementDetails = ds.Tables("disbursement")

        'Create a datatable to store all the disbursement data
        Dim dsImportDisbursements As New DataSet
        Dim dtImportDisbursements As New DataTable("ImportDisbursements")
        dtImportDisbursements.Columns.Add(New DataColumn("SSN", System.Type.GetType("System.String")))
        dtImportDisbursements.Columns.Add(New DataColumn("StudentAwardId", System.Type.GetType("System.String")))
        dtImportDisbursements.Columns.Add(New DataColumn("disbursementdate", System.Type.GetType("System.String")))
        dtImportDisbursements.Columns.Add(New DataColumn("disbursementtime", System.Type.GetType("System.String")))
        dtImportDisbursements.Columns.Add(New DataColumn("disbursementamount", System.Type.GetType("System.Decimal")))
        dtImportDisbursements.Columns.Add(New DataColumn("disbursementdesc", System.Type.GetType("System.String")))
        dtImportDisbursements.Columns.Add(New DataColumn("StuEnrollId", System.Type.GetType("System.String")))
        dtImportDisbursements.Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
        dtImportDisbursements.Columns.Add(New DataColumn("TermId", System.Type.GetType("System.String")))

        For Each drStudent As DataRow In dtStudent.Rows
            strSSN = drStudent("SSN").ToString
            For Each drStudentName As DataRow In dtName.Rows
                strStudentName = drStudentName("first").ToString & " " & drStudentName("last").ToString
                Exit For
            Next
            For Each drDisbursementRow As DataRow In dtDisbursements.Rows
                For Each drSessionRow As DataRow In dtSession.Rows
                    strSessionCode = drSessionRow("code").ToString
                    strSessionDescription = drSessionRow("description").ToString
                    strEquivalentSession = drSessionRow("equivalentSession").ToString
                    strTermId = GetTermBySessionCode(strSessionCode, strStudentName)
                    If strTermId.ToString.Trim.Length > 36 Then
                        AddEntryToExceptionGeneralMessage(strStudentName, strSSN, "", strTermId.ToString, strXMLFile, user)
                        Exit For
                    End If
                    For Each drAwardCodeRow As DataRow In dtAward.Rows
                        strAwardCode = drAwardCodeRow("code").ToString
                        strAwardDescription = drAwardCodeRow("description").ToString
                        strAwardBillCode = drAwardCodeRow("billcode").ToString
                        strStuEnrollId = GetStudentEnrollmentBySSN(strSSN, strAwardCode, strStudentName)
                        If strStuEnrollId.ToString.Trim.Length > 36 Then
                            AddEntryToExceptionGeneralMessage(strStudentName, strSSN, "", strStuEnrollId.ToString, strXMLFile, user)
                            Exit For
                        End If
                        strStudentAwardId = GetStudentAwardId(strStuEnrollId, strAwardCode, strAwardDescription, strStudentName)
                        If strStudentAwardId.ToString.Trim.Length > 36 Then
                            AddEntryToExceptionGeneralMessage(strStudentName, strSSN, strStuEnrollId, strStudentAwardId.ToString, strXMLFile, user)
                        End If
                        For Each drDisbursementDetailRow As DataRow In dtDisbursementDetails.Rows
                            strDisbDate = drDisbursementDetailRow("date").ToString
                            strDisbTime = drDisbursementDetailRow("time").ToString
                            decDisbAmount = CDec(drDisbursementDetailRow("disbursement_text"))
                            Dim rowImportDisbursements As DataRow
                            rowImportDisbursements = dtImportDisbursements.NewRow()
                            rowImportDisbursements("SSN") = strSSN
                            rowImportDisbursements("StudentAwardId") = strStudentAwardId
                            rowImportDisbursements("disbursementdate") = strDisbDate
                            rowImportDisbursements("disbursementtime") = strDisbTime
                            rowImportDisbursements("disbursementamount") = decDisbAmount
                            rowImportDisbursements("disbursementdesc") = strAwardDescription
                            rowImportDisbursements("StuEnrollId") = strStuEnrollId
                            rowImportDisbursements("StudentName") = strStudentName
                            rowImportDisbursements("TermId") = strTermId
                            dtImportDisbursements.Rows.Add(rowImportDisbursements)
                        Next
                    Next
                Next
            Next
        Next
        Dim strResult As String = ""
        strResult = PostDisbursements(dtImportDisbursements, strXMLFile, CampusId, user)
        If strResult.Trim = "" Then
            Return "Disbursement successfully posted"
        Else
            Return strResult
        End If
    End Function
    Public Function PostDisbursements(ByVal dtImportDisb As DataTable, ByVal filename As String, ByVal CampusId As String, ByVal user As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim sbAcademicYear As New StringBuilder
        Dim strStuEnrollId As String = ""
        Dim strAwardYear As String = ""
        Dim strAwardScheduleId As String = ""
        Dim strMessage As String = ""
        Dim strTransactionId As String = ""
        Dim strPmtDisbRelId As String = ""
        db.OpenConnection()

        For Each drImportDISB As DataRow In dtImportDisb.Rows
            If String.IsNullOrEmpty(drImportDISB("StudentAwardId")) Then
                Dim sMessage As String = "Unable to apply disbursement as student award for student " & drImportDISB("StudentName") & " cannot be empty"
                AddEntryToException(drImportDISB, sMessage.ToString, filename, user)
                Exit For
            End If
            Try
                strAwardScheduleId = GetAwardScheduleId(drImportDISB, user)
                If strAwardScheduleId.Trim.Length > 36 Then
                    strMessage &= strAwardScheduleId & vbCrLf
                    AddEntryToException(drImportDISB, strAwardScheduleId.ToString, filename, user)
                    Exit Try
                End If
                strTransactionId = InsertTransaction(drImportDISB, CampusId, user)
                If strTransactionId.Trim.Length > 36 Then
                    strMessage &= strTransactionId & vbCrLf
                    AddEntryToException(drImportDISB, strTransactionId.ToString, filename, user)
                    Exit Try
                End If
                If strAwardScheduleId.ToString.Trim.Length = 36 And strTransactionId.Trim.Length = 36 Then
                    strPmtDisbRelId = InsertPmtDisbRel(drImportDISB, strTransactionId, strAwardScheduleId, user)
                    If strPmtDisbRelId.Trim.Length > 36 Then
                        strMessage &= strPmtDisbRelId & vbCrLf
                        AddEntryToException(drImportDISB, strPmtDisbRelId.ToString, filename, user)
                        Exit Try
                    End If
                End If
            Catch ex As System.Exception
            Finally
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            End Try
        Next
        If db.Connection.State = ConnectionState.Open Then
            db.CloseConnection()
        End If
        Return strMessage
    End Function
    Private Sub AddEntryToException(ByVal dr As DataRow, ByVal ErrorMessage As String, ByVal filename As String, ByVal user As String)
        Dim sbAddException As New StringBuilder
        Dim db As New DataAccess
        db.OpenConnection()

        With sbAddException
            .Append("INSERT INTO RegentDISBImportException(SSN,StudentAwardId,disbursementdate,disbursementtime,disbursementamount,disbursementdesc,StuEnrollId,StudentName,ErrorMessage,moduser,moddate,filename) ")
            .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?) ")
        End With

        'Add params
        db.ClearParameters()
        db.AddParameter("@SSN", dr("SSN"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StudentAwardId", dr("StudentAwardId"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@disbursementdate", dr("disbursementdate"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@disbursementtime", dr("disbursementtime"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@disbursementamount", dr("disbursementamount"), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
        db.AddParameter("@disbursementdesc", dr("disbursementdesc"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", dr("StuEnrollId"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StudentName", dr("StudentName"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ErrorMessage", ErrorMessage, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@moduser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@moddate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@filename", filename, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery(sbAddException.ToString)
        Catch ex As System.Exception
        Finally
            sbAddException.Remove(0, sbAddException.Length)

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        End Try
    End Sub
    Private Sub AddEntryToExceptionGeneralMessage(ByVal StudentName As String, _
                                                  ByVal SSN As String, _
                                                  ByVal StuEnrollId As String, _
                                                  ByVal ErrorMessage As String, _
                                                  ByVal filename As String, ByVal user As String)
        Dim sbAddException As New StringBuilder
        Dim db As New DataAccess
        db.OpenConnection()

        With sbAddException
            .Append("INSERT INTO RegentDISBImportException(SSN,StudentAwardId,disbursementdate,disbursementtime,disbursementamount,disbursementdesc,StuEnrollId,StudentName,ErrorMessage,moduser,moddate,filename) ")
            .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?) ")
        End With

        'Add params
        db.ClearParameters()
        db.AddParameter("@SSN", SSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StudentAwardId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@disbursementdate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@disbursementtime", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@disbursementamount", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
        db.AddParameter("@disbursementdesc", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If StuEnrollId = "" Then
            db.AddParameter("@StuEnrollId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.AddParameter("@StudentName", StudentName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ErrorMessage", ErrorMessage, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@moduser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@moddate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@filename", filename, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(sbAddException.ToString)
        Catch ex As System.Exception
        Finally
            sbAddException.Remove(0, sbAddException.Length)

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        End Try
    End Sub
    Public Function ReturnExceptionDS(ByVal filename As String) As DataSet
        Dim sbAddException As New StringBuilder
        Dim db As New DataAccess
        Dim dsException As New DataSet
        db.OpenConnection()
        With sbAddException
            .Append(" Select * from RegentDISBImportException where filename=? ")
            .Append(" order by moddate desc ")
        End With
        db.ClearParameters()
        db.AddParameter("@filename", filename, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            dsException = db.RunParamSQLDataSet(sbAddException.ToString)
            Return dsException
        Catch ex As System.Exception
            Return Nothing
        Finally

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        End Try
    End Function
    Private Function GetStudentEnrollmentBySSN(ByVal SSN As String, ByVal AwardCode As String, ByVal studentname As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim strStuEnrollId As String = ""
        db.OpenConnection()
        With sb
            .Append(" select Distinct Top 1 t2.StuEnrollId from faStudentAwards t1,arStuEnrollments t2,arStudent t3 ")
            .Append(" where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId = t3.StudentId and t3.SSN=? ")
            .Append(" and t1.AwardCode=? ")
        End With
        db.AddParameter("@SSN", SSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@AwardCode", AwardCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strStuEnrollId = db.RunParamSQLScalar(sb.ToString).ToString
            Return strStuEnrollId
        Catch ex As System.Exception
            Return "Unable to find the enrollment information with the provided award code " & AwardCode & " for student " & studentname & vbCrLf
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Private Function GetTermBySessionCode(ByVal SessionCode As String, ByVal StudentName As String) As String
        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        Dim strTerm As String = ""
        With sb
            'With subqueries
            .Append(" select Distinct Top 1 TermId from arTerm where regentTerm=?")
        End With

        'Add the EmployerContactId the parameter list
        If SessionCode.ToString.Length = 3 Then
            SessionCode = "0" & SessionCode
        End If
        db.AddParameter("@SessionCode", SessionCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strTerm = db.RunParamSQLScalar(sb.ToString).ToString
            Return strTerm
        Catch ex As System.Exception
            Return "Unable to find the term with the provided session code " & SessionCode & " for student " & StudentName
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Private Function GetStudentAwardId(ByVal StuEnrollId As String, ByVal AwardCode As String, ByVal AwardDescription As String, ByVal StudentName As String) As String

        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        Dim strStudentAwardId As String = ""
        With sb
            'With subqueries
            .Append(" select Distinct StudentAwardId from faStudentAwards where " & vbCrLf)
            .Append(" StuEnrollId=? and AwardCode=? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@AwardCode", AwardCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@AwardDescription", AwardDescription, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strStudentAwardId = db.RunParamSQLScalar(sb.ToString).ToString
            Return strStudentAwardId
        Catch ex As System.Exception
            Return "Unable to find student award with the provided award code " & AwardCode & " and enrollment information for student " & StudentName & vbCrLf
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Public Function GetAwardScheduleId(ByVal drImportDISB As DataRow, ByVal user As String) As String
        Dim db As New DataAccess
        db.OpenConnection()
        Dim sbCheckDuplicateAwards As New StringBuilder
        Dim intDuplicates As Integer = 0
        Dim strAwardScheduleId As String = ""

        'Get Award Schedule Based on Student Award Id,Date and Amount
        With sbCheckDuplicateAwards
            .Append(" Select Distinct Top 1 AwardScheduleId from faStudentAwardSchedule where ")
            .Append(" StudentAwardID=? and ExpectedDate=? and Amount=? ")
        End With
        'Add params
        db.ClearParameters()
        db.AddParameter("@studentAwardID", drImportDISB("StudentAwardID"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ExpectedDate", drImportDISB("disbursementdate"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@Amount", Math.Abs(drImportDISB("disbursementamount")), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

        Try
            strAwardScheduleId = db.RunParamSQLScalar(sbCheckDuplicateAwards.ToString).ToString
            If strAwardScheduleId.ToString.Trim.Length = 36 Then
                Dim sb As New StringBuilder
                Dim strReference As String = "Regent - " & drImportDISB("disbursementdesc").ToString
                With sb
                    .Append(" update faStudentAwardSchedule set ExpectedDate=?,Amount=?,Reference=?,moduser=? where AwardScheduleId=? ")
                End With
                db.ClearParameters()
                db.AddParameter("@ExpectedDate", drImportDISB("disbursementdate"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@Amount", Math.Abs(drImportDISB("disbursementamount")), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@Reference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardScheduleId", strAwardScheduleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Try
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Catch ex As System.Exception
                Finally
                    sb.Remove(0, sb.Length)
                End Try
            End If
            Return strAwardScheduleId
        Catch ex As System.Exception
            strAwardScheduleId = ""
        Finally
            db.ClearParameters()
            sbCheckDuplicateAwards.Remove(0, sbCheckDuplicateAwards.Length)
        End Try

        If strAwardScheduleId = "" Then
            ''Get Award Schedule Based on Student Award Id and Amount
            With sbCheckDuplicateAwards
                .Append(" Select Distinct Top 1 AwardScheduleId from faStudentAwardSchedule where ")
                .Append(" StudentAwardID=? and Amount=? ")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@studentAwardID", drImportDISB("StudentAwardID"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Amount", Math.Abs(drImportDISB("disbursementamount")), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            Try
                strAwardScheduleId = db.RunParamSQLScalar(sbCheckDuplicateAwards.ToString).ToString
                If strAwardScheduleId.ToString.Trim.Length = 36 Then
                    Dim sb As New StringBuilder
                    Dim strReference As String = "Regent - " & drImportDISB("disbursementdesc").ToString
                    With sb
                        .Append(" update faStudentAwardSchedule set ExpectedDate=?,Amount=?,Reference=?,ModUser=? where AwardScheduleId=? ")
                    End With
                    db.ClearParameters()
                    db.AddParameter("@ExpectedDate", drImportDISB("disbursementdate"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@Amount", Math.Abs(drImportDISB("disbursementamount")), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    db.AddParameter("@Reference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardScheduleId", strAwardScheduleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Try
                        db.RunParamSQLExecuteNoneQuery(sb.ToString)
                    Catch ex As System.Exception
                    Finally
                        sb.Remove(0, sb.Length)
                    End Try
                End If
                Return strAwardScheduleId
            Catch ex As System.Exception
                strAwardScheduleId = ""
            End Try
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        If strAwardScheduleId = "" Then
            Return "Unable to find a matching disbursement based on the disbursement amount or date specified for student" & drImportDISB("StudentName")
        End If
    End Function
    Public Function InsertTransaction(ByVal drImportDISB As DataRow, ByVal CampusId As String, ByVal user As String) As String
        Dim sbTransaction As New StringBuilder
        Dim sbAcademicYear As New StringBuilder
        Dim TransactionID As String = Guid.NewGuid.ToString
        Dim strTransDescrip As String = ""
        Dim strAwardYear As String = ""
        Dim strReference As String = "Regent - " & drImportDISB("disbursementdesc").ToString
        Dim db As New DataAccess
        db.OpenConnection()

        With sbAcademicYear
            .Append(" Select Distinct AcademicYearId from saAcademicYears where SUBSTRING(AcademicYearDescrip,1,4) = ? ")
        End With
        db.ClearParameters()
        db.AddParameter("@AcademicYearDescrip", Year(drImportDISB("disbursementdate")), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strAwardYear = CType(db.RunParamSQLScalar(sbAcademicYear.ToString), Guid).ToString
        Catch ex As System.Exception
            strAwardYear = System.DBNull.Value.ToString
        End Try


        With sbTransaction
            .Append("INSERT INTO saTransactions(TransactionID,StuEnrollID,CampusId,TermId,TransDate,TransAmount,CreateDate,TransReference,TransDescrip,moduser,moddate,TransTypeID,IsPosted,IsAutomatic,AcademicYearId) ")
            .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
        End With

        'Add params
        db.ClearParameters()
        db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollID", drImportDISB("StuEnrollId"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusID", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@TermID", drImportDISB("TermId"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@TransDate", drImportDISB("disbursementdate"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@TransAmount", drImportDISB("disbursementamount"), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
        db.AddParameter("@CreateDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@TransReference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@TransDescrip", drImportDISB("disbursementdesc"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@moduser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@moddate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@TransTypeID", 2, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@IsPosted", 1, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
        db.AddParameter("@IsAutomatic", 0, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
        db.AddParameter("@AcademicYearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'execute the query
        Try
            db.RunParamSQLExecuteNoneQuery(sbTransaction.ToString)
            Return TransactionID
        Catch ex As System.Exception
            Return "Unable to add transaction for the disbursement amount " & Math.Abs(drImportDISB("disbursementamount")) & " and date specified " & drImportDISB("disbursementdate") & " for student" & drImportDISB("StudentName")
        Finally

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        End Try
    End Function
    Public Function InsertPmtDisbRel(ByVal drImportDISB As DataRow, ByVal TransactionId As String, ByVal AwardScheduleId As String, ByVal user As String) As String
        Dim sbTransaction As New StringBuilder
        Dim sbAcademicYear As New StringBuilder
        Dim strTransDescrip As String = ""
        Dim strAwardYear As String = ""
        Dim db As New DataAccess
        db.OpenConnection()
        Dim strReference As String = "Regent - " & drImportDISB("disbursementdesc").ToString

        Dim sbPmtDisbRel1 As New StringBuilder
        Dim PmtDisbRelID As String = Guid.NewGuid.ToString
        With sbPmtDisbRel1
            .Append("INSERT INTO saPmtDisbRel(PmtDisbRelID,TransactionID,Amount,StuAwardID,AwardScheduleId,moduser,moddate) ")
            .Append("VALUES(?,?,?,?,?,?,?) ")
        End With

        'Add params
        db.ClearParameters()
        db.AddParameter("@PmtDisbRelID", PmtDisbRelID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@TransactionID", TransactionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@Amount", drImportDISB("disbursementamount"), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
        db.AddParameter("@StuAwardID", drImportDISB("StudentAwardID"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@AwardScheduleId", AwardScheduleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@moduser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@moddate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(sbPmtDisbRel1.ToString)
            Return PmtDisbRelID
        Catch ex As System.Exception
            Return "Unable to apply payment for the disbursement amount " & Math.Abs(drImportDISB("disbursementamount")) & " for student" & drImportDISB("StudentName")
        Finally

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        End Try
    End Function
End Class
