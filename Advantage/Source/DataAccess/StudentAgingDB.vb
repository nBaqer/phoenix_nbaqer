Imports FAME.Advantage.Common

Public Class StudentAgingDB

#Region "Private Data Members"

    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")
    Private m_AgingDates As AgingBalance

#End Region


#Region "Public Properties"

    Public Property AgingDates() As AgingBalance
        Get
            Return m_AgingDates
        End Get
        Set(ByVal Value As AgingBalance)
            m_AgingDates = Value
        End Set
    End Property

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

#End Region


#Region "Public Methods"

    Public Function GetStudentCharges(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim refDate As String
        Dim idx As Integer
        Dim idx2 As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178
        Dim strWhereTemp As String = strWhere.ToLower
        Dim strStatusCodeIds As String = ""

        '' New Code Added By Vijay Ramteke on January 31, 2011 For RallyId DE4960
        Dim strSplit() As String
        If paramInfo.ShowUseStuCurrStatus = False Then
            Dim strTemp As String = ""
            strSplit = paramInfo.FilterList.ToLower.Replace("and", ":;").Split(";")
            Dim i As Integer
            If strSplit.Length > 0 Then
                strWhere = ""
                For i = 0 To strSplit.Length - 1
                    strTemp = strSplit(i)
                    If Not strTemp.Contains("arstuenrollments.statuscodeid") And Not strTemp.Trim = ":" Then
                        strWhere += "and " + strTemp.Replace(":", "")
                    End If
                Next
            End If
        End If
        '' New Code Added By Vijay Ramteke on January 31, 2011 For RallyId DE4960

        'get the statuscodeIds
        If strWhereTemp.ToLower.Contains("arstuenrollments.statuscodeid") Then
            If strWhereTemp.ToLower.Contains("arstuenrollments.statuscodeid in ") Then
                If strWhereTemp.Substring(strWhereTemp.ToLower.IndexOf("arstuenrollments.statuscodeid in ")).ToLower.IndexOf(" and ") >= 0 Then
                    strStatusCodeIds = strWhereTemp.Substring(strWhereTemp.ToLower.IndexOf("arstuenrollments.statuscodeid in ("), strWhereTemp.Substring(strWhereTemp.ToLower.IndexOf("arstuenrollments.statuscodeid in (")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.statuscodeid in", "").Replace(")", "").Replace("(", "")
                Else
                    strStatusCodeIds = strWhereTemp.Substring(strWhereTemp.ToLower.IndexOf("arstuenrollments.statuscodeid in (")).Replace("arstuenrollments.statuscodeid in", "").Replace(")", "").Replace("(", "")
                End If
            Else
                If strWhereTemp.Substring(strWhereTemp.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).ToLower.IndexOf(" and ") >= 0 Then
                    strStatusCodeIds = strWhereTemp.Substring(strWhereTemp.ToLower.IndexOf("arstuenrollments.statuscodeid = "), strWhereTemp.Substring(strWhereTemp.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.statuscodeid =", "").Replace(")", "").Replace("(", "")
                Else
                    strStatusCodeIds = strWhereTemp.Substring(strWhereTemp.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).Replace("arstuenrollments.statuscodeid =", "").Replace(")", "").Replace("(", "")
                End If
            End If
        End If
        'end the statuscodeIds
        '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178

        If paramInfo.FilterOther <> "" Then
            'Parse FilterOther in order to get Reference Date
            idx = paramInfo.FilterOther.IndexOf("BETWEEN")
            If idx <> -1 Then
                idx2 = paramInfo.FilterOther.IndexOf("AM")
                If idx2 <> -1 Then
                    refDate = paramInfo.FilterOther.Substring(idx + 9, idx2 + 2 - (idx + 8))
                Else
                    refDate = paramInfo.FilterOther.Substring(idx + 8)
                End If
                Dim tempDate() As String = refDate.Split(" ")
                If tempDate.GetLength(0) > 0 Then
                    refDate = tempDate(0)
                End If
            End If
        End If

        'strOrderBy &= " ORDER BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,PrgVerDescrip," & _
        '        "arStudent.LastName,arStudent.FirstName"

        If paramInfo.HideRptProgramVersion = True Then
            strOrderBy &= " ORDER BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip," & _
                            "arStudent.LastName,arStudent.FirstName"
        Else
            strOrderBy &= " ORDER BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,PrgVerDescrip," & _
                "arStudent.LastName,arStudent.FirstName"
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        ' Creating Query According to the Student Group to be shown on the Report 
        ' as per issue mention in Mantis Bug Tracker ID=15851
        With sb
            If paramInfo.ShowRptStudentGroup = True Then
                ' New Code By Vijay Ramteke on 04/03/2009 for Adding Student Lead Group Description
                .Append("SELECT syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,syCampuses.CampDescrip,arStuEnrollments.StuEnrollId,")
                '.Append("arStuEnrollments.PrgVerId,(SELECT arPrgVersions.PrgVerDescrip FROM arPrgVersions WHERE arPrgVersions.PrgVerId=arStuEnrollments.PrgVerId) AS PrgVerDescrip,")

                If paramInfo.HideRptProgramVersion = True Then
                    .Append("arStuEnrollments.PrgVerId,'' AS PrgVerDescrip,")
                Else
                    .Append("arStuEnrollments.PrgVerId,(SELECT arPrgVersions.PrgVerDescrip FROM arPrgVersions WHERE arPrgVersions.PrgVerId=arStuEnrollments.PrgVerId) AS PrgVerDescrip,")
                End If

                .Append("arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
                If StudentIdentifier = "SSN" Then
                    .Append("CASE ISNULL(arStudent.SSN,'') WHEN '' THEN 'N/A' ELSE arStudent.SSN END	AS StudentIdentifier,")
                ElseIf StudentIdentifier = "EnrollmentId" Then
                    .Append("CASE ISNULL(arStuEnrollments.EnrollmentId,'') WHEN '' THEN 'N/A' ELSE arStudent.EnrollmentId END AS StudentIdentifier,")
                ElseIf StudentIdentifier = "StudentId" Then
                    .Append("CASE ISNULL(arStudent.StudentNumber,'') WHEN '' THEN 'N/A' ELSE arStudent.StudentNumber END AS StudentIdentifier,")
                End If
                .Append("arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate <= ? AND T2.IsPosted=1 AND T2.Voided=0) AS Balance,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate >= ? AND T2.TransDate <= ? AND T2.TransAmount > 0 AND T2.IsPosted=1 AND T2.Voided=0) AS CurrentBal,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate >= ? AND T2.TransDate <= ? AND T2.TransAmount > 0 AND T2.IsPosted=1 AND T2.Voided=0) AS Bal31To60Day,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate >= ? AND T2.TransDate <= ? AND T2.TransAmount > 0 AND T2.IsPosted=1 AND T2.Voided=0) AS Bal61To90Day,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate >= ? AND T2.TransDate <= ? AND T2.TransAmount > 0 AND T2.IsPosted=1 AND T2.Voided=0) AS Bal91To120Day,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate <= ? AND T2.TransAmount > 0 AND T2.IsPosted=1 AND T2.Voided=0) AS BalOver120Day,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate >= ? AND T2.TransDate <= ? AND T2.TransAmount < 0 AND T2.IsPosted=1 AND T2.Voided=0) AS CurrentPymt,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate >= ? AND T2.TransDate <= ? AND T2.TransAmount < 0 AND T2.IsPosted=1 AND T2.Voided=0) AS Pymt31To60Day,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate >= ? AND T2.TransDate <= ? AND T2.TransAmount < 0 AND T2.IsPosted=1 AND T2.Voided=0) AS Pymt61To90Day,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate >= ? AND T2.TransDate <= ? AND T2.TransAmount < 0 AND T2.IsPosted=1 AND T2.Voided=0) AS Pymt91To120Day,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate <= ? AND T2.TransAmount < 0 AND T2.IsPosted=1 AND T2.Voided=0) AS PymtOver120Day, ")
                .Append("adLeadGroups.Descrip AS LeadGroupDescrip, 1 AS ShowLeadGroup ")

                If paramInfo.HideRptProgramVersion = True Then
                    .Append(" ,1 AS HideProgramGroup ")
                Else
                    .Append(" ,0 AS HideProgramGroup ")
                End If

                .Append("FROM adLeadGroups INNER JOIN adLeadByLeadGroups ON  adLeadGroups.LeadGrpId =  adLeadByLeadGroups.LeadGrpId ")
                .Append("RIGHT OUTER JOIN syCampGrps ")
                .Append("INNER JOIN syCmpGrpCmps ON  syCampGrps.CampGrpId =  syCmpGrpCmps.CampGrpId ")
                .Append("INNER JOIN syCampuses ON  syCmpGrpCmps.CampusId =  syCampuses.CampusId ")
                .Append("INNER JOIN arStuEnrollments ON  syCampuses.CampusId =  arStuEnrollments.CampusId ")
                .Append("INNER JOIN arStudent ON  arStudent.StudentId =  arStuEnrollments.StudentId ON adLeadByLeadGroups.StuEnrollId = arStuEnrollments.StuEnrollId ")

                ''UnCommented Code Added By Vijay Ramteke on January 31, 2011 For RallyId DE4960
                ''Code Commented By Vijay Ramteke On November 12, 2010 For Rally Id DE1244
                '' '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178
                If paramInfo.ShowUseStuCurrStatus = False And Not strStatusCodeIds = "" Then
                    .Append(" INNER JOIN syStudentStatusChanges On syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId   ")
                End If
                '' '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178
                ''Code Commented By Vijay Ramteke On November 12, 2010 For Rally Id DE1244
                'UnCommented Code Added By Vijay Ramteke on January 31, 2011 For RallyId DE4960

                .Append("WHERE syCmpGrpCmps.CampGrpId = syCampGrps.CampGrpId ")
                .Append("AND syCmpGrpCmps.CampusId=syCampuses.CampusId ")
                .Append("AND syCampuses.CampusId=arStuEnrollments.CampusId ")
                .Append("AND arStuEnrollments.StudentId=arStudent.StudentId ")
                .Append("AND adLeadGroups.UseForScheduling=1 ")
                .Append("AND arStuEnrollments.StuEnrollId IN (SELECT DISTINCT StuEnrollId FROM saTransactions WHERE TransDate <= ? AND IsPosted=1 AND saTransactions.Voided=0) ")
                .Append(" AND (SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate <=  ? AND T2.IsPosted=1 AND T2.Voided=0) <> 0 ")

                'UnCommented Code Added By Vijay Ramteke on January 31, 2011 For RallyId DE4960
                ''Code Commented By Vijay Ramteke On November 12, 2010 For Rally Id DE1244
                '' '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178
                If paramInfo.ShowUseStuCurrStatus = False And Not strStatusCodeIds = "" Then
                    .Append("  AND syStudentStatusChanges.NewStatusId in(" + strStatusCodeIds + ")  ")
                    .Append(" AND syStudentStatusChanges.ModDate <= '" + refDate + "' ")
                End If
                '' '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178
                ''Code Commented By Vijay Ramteke On November 12, 2010 For Rally Id DE1244
                'UnCommented Code Added By Vijay Ramteke on January 31, 2011 For RallyId DE4960

                .Append(strWhere)
                .Append(strOrderBy)
                ' New Code By Vijay Ramteke on 04/03/2009 for Adding Student Lead Group Description
            Else
                ' Code Commented By Vijay Ramteke on 04/03/2009 for Adding Student Lead Group Description
                ' This code is uncommented by VIjay Ramteke on 09/04/2009 as per issue mention in Mantis Bug Tracker ID=15851
                .Append("SELECT  syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,syCampuses.CampDescrip,arStuEnrollments.StuEnrollId,")
                '.Append("arStuEnrollments.PrgVerId,(SELECT arPrgVersions.PrgVerDescrip FROM arPrgVersions WHERE arPrgVersions.PrgVerId=arStuEnrollments.PrgVerId) AS PrgVerDescrip,")

                If paramInfo.HideRptProgramVersion = True Then
                    .Append("'' as PrgVerId,'' AS PrgVerDescrip,")
                Else
                    .Append("arStuEnrollments.PrgVerId,(SELECT arPrgVersions.PrgVerDescrip FROM arPrgVersions WHERE arPrgVersions.PrgVerId=arStuEnrollments.PrgVerId) AS PrgVerDescrip,")
                End If

                .Append("arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")

                If StudentIdentifier = "SSN" Then
                    .Append("CASE ISNULL(arStudent.SSN,'') WHEN '' THEN 'N/A' ELSE arStudent.SSN END	AS StudentIdentifier,")
                ElseIf StudentIdentifier = "EnrollmentId" Then
                    .Append("CASE ISNULL(arStuEnrollments.EnrollmentId,'') WHEN '' THEN 'N/A' ELSE arStudent.EnrollmentId END AS StudentIdentifier,")
                ElseIf StudentIdentifier = "StudentId" Then
                    .Append("CASE ISNULL(arStudent.StudentNumber,'') WHEN '' THEN 'N/A' ELSE arStudent.StudentNumber END AS StudentIdentifier,")
                End If

                .Append("arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate <= ? AND T2.IsPosted=1 AND T2.Voided=0) AS Balance,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate >= ? AND T2.TransDate <= ? AND T2.TransAmount > 0 AND T2.IsPosted=1 AND T2.Voided=0) AS CurrentBal,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate >= ? AND T2.TransDate <= ? AND T2.TransAmount > 0 AND T2.IsPosted=1 AND T2.Voided=0) AS Bal31To60Day,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate >= ? AND T2.TransDate <= ? AND T2.TransAmount > 0 AND T2.IsPosted=1 AND T2.Voided=0) AS Bal61To90Day,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate >= ? AND T2.TransDate <= ? AND T2.TransAmount > 0 AND T2.IsPosted=1 AND T2.Voided=0) AS Bal91To120Day,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate <= ? AND T2.TransAmount > 0 AND T2.IsPosted=1 AND T2.Voided=0) AS BalOver120Day,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate >= ? AND T2.TransDate <= ? AND T2.TransAmount < 0 AND T2.IsPosted=1 AND T2.Voided=0) AS CurrentPymt,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate >= ? AND T2.TransDate <= ? AND T2.TransAmount < 0 AND T2.IsPosted=1 AND T2.Voided=0) AS Pymt31To60Day,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate >= ? AND T2.TransDate <= ? AND T2.TransAmount < 0 AND T2.IsPosted=1 AND T2.Voided=0) AS Pymt61To90Day,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate >= ? AND T2.TransDate <= ? AND T2.TransAmount < 0 AND T2.IsPosted=1 AND T2.Voided=0) AS Pymt91To120Day,")
                .Append("(SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate <= ? AND T2.TransAmount < 0 AND T2.IsPosted=1 AND T2.Voided=0) AS PymtOver120Day, ")
                .Append("'' AS LeadGroupDescrip, 0 AS ShowLeadGroup ")

                If paramInfo.HideRptProgramVersion = True Then
                    .Append(" ,1 AS HideProgramGroup ")
                Else
                    .Append(" ,0 AS HideProgramGroup ")
                End If

                .Append("FROM syCampGrps,syCmpGrpCmps,syCampuses,arStuEnrollments,arStudent ")
                'code Commented by Priyanka on date 28th of May 2009
                '// Readded by kamalesh on 01/01/2010 to resolve mantis issue id 18290
                'commented again by theresa G on 04/27/2010 for mantis
                '18560: Student Aging Summary report is displaying some duplicates after the upgrade from 2367 to 2395 
                ''Readded By Vijay Ramteke on July 22, 2010  Form Mantis Id 19419
                ''New Code Added By Vijay ramteek On August 11, 2010 For Mantis ID 18560
                If strWhere.ToLower.Contains("adleadgroups.leadgrpid") Then
                    .Append(" ,adLeadGroups,adLeadByLeadGroups ")
                End If
                ''New Code Added By Vijay ramteek On August 11, 2010 For Mantis ID 18560
                '-

                ''Code Commented By Vijay Ramteke On November 12, 2010 For Rally Id DE1244
                '' '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178
                '--------------------------------------------------------------------------------------------------------------------
                'Troy: 6/4/2015 Commented out the code to add the syStudentStatusChanges table to the query
                'If the student has multiple status change records during the period specified the transactions returned to the main
                'query will be repeated for each status change record. For eg. if the student has the user has 2 status change records
                'for the report period, then the transactions returned will be duplicated. This is why the original query had to be
                'using a DISTINCT in it. However, the DISTINCT creates a problem for the rare occasions when a student has multiple
                'payments for the same amount on the same date for the same award such as 2 Pell payments for $700 on 4/17/2012.
                'See Rally  DE9396: QA: Some reports show only one of the payments if they are of the same amount on the same day.
                '--------------------------------------------------------------------------------------------------------------------
                'If paramInfo.ShowUseStuCurrStatus = False And Not strStatusCodeIds = "" Then
                '    .Append(" ,syStudentStatusChanges  ")
                'End If
                '' '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178
                ''Code Commented By Vijay Ramteke On November 12, 2010 For Rally Id DE1244

                .Append("WHERE syCmpGrpCmps.CampGrpId = syCampGrps.CampGrpId ")
                .Append("AND syCmpGrpCmps.CampusId=syCampuses.CampusId ")
                .Append("AND syCampuses.CampusId=arStuEnrollments.CampusId ")
                .Append("AND arStuEnrollments.StudentId=arStudent.StudentId ")

                '-code added by Priyanka on date 28th of May 2009
                '// Readded by kamalesh on 01/01/2010 to resolve mantis issue id 18290
                'commented again by theresa G on 04/27/2010 for mantis
                '18560: Student Aging Summary report is displaying some duplicates after the upgrade from 2367 to 2395 
                ''Readded By Vijay Ramteke on July 22, 2010  Form Mantis Id 19419
                ''New Code Added By Vijay ramteek On August 11, 2010 For Mantis ID 18560
                If strWhere.ToLower.Contains("adleadgroups.leadgrpid") Then
                    .Append("AND adLeadGroups.LeadGrpId = adLeadByLeadGroups.LeadGrpId ")
                    .Append("AND adLeadByLeadGroups.StuEnrollId = arStuEnrollments.StuEnrollId  ")
                    '' New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19644
                    .Append("AND adLeadGroups.UseForScheduling=1 ")
                    '' New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19644
                End If
                ''New Code Added By Vijay ramteek On August 11, 2010 For Mantis ID 18560
                '-

                ''Code Commented By Vijay Ramteke On November 12, 2010 For Rally Id DE1244
                '' '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178
                'If paramInfo.ShowUseStuCurrStatus = False And Not strStatusCodeIds = "" Then
                '    .Append(" AND syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId  ")
                '    .Append(" AND syStudentStatusChanges.NewStatusId in (" + strStatusCodeIds + ") ")
                '    .Append(" AND syStudentStatusChanges.ModDate <='" + refDate + "' ")
                '    .Append(" AND syStudentStatusChanges.NewStatusId in (")
                '    .Append(" SELECT TOP 1 NewStatusId FROM syStudentStatusChanges WHERE StuEnrollId=arStuEnrollments.StuEnrollId AND")
                '    .Append(" ModDate<='" + refDate + "' ORDER BY ModDate DESC) ")
                'End If
                If paramInfo.ShowUseStuCurrStatus = False And Not strStatusCodeIds = "" Then
                    .Append(" AND EXISTS( ")
                    .Append("       SELECT TOP 1 StudentStatusChangeId ")
                    .Append("       FROM dbo.syStudentStatusChanges ")
                    .Append("       WHERE syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId  ")
                    .Append("       AND syStudentStatusChanges.NewStatusId in (" + strStatusCodeIds + ") ")
                    .Append("       AND syStudentStatusChanges.ModDate <='" + refDate + "' ")
                    .Append("       AND syStudentStatusChanges.NewStatusId in (")
                    .Append("           SELECT TOP 1 NewStatusId FROM syStudentStatusChanges WHERE StuEnrollId=arStuEnrollments.StuEnrollId AND")
                    .Append("           ModDate<='" + refDate + "' ORDER BY ModDate DESC) ")
                    .Append("       ) ")
                End If
                '' '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178
                ''Code Commented By Vijay Ramteke On November 12, 2010 For Rally Id DE1244

                'Added By Vijay Ramteke on March 03, 2010
                'commented again by theresa G on 04/27/2010 for mantis
                '18560: Student Aging Summary report is displaying some duplicates after the upgrade from 2367 to 2395 
                '.Append("AND adLeadGroups.UseForScheduling=1 ")
                'Added By Vijay Ramteke on March 03, 2010
                .Append("AND arStuEnrollments.StuEnrollId IN (SELECT DISTINCT StuEnrollId FROM saTransactions WHERE TransDate <= ? AND IsPosted=1 AND saTransactions.Voided=0) ")
                .Append(" and (SELECT SUM(T2.TransAmount) FROM saTransactions T2 WHERE T2.StuEnrollId = arStuEnrollments.StuEnrollId AND T2.TransDate <=  ? AND T2.IsPosted=1 AND T2.Voided=0) <> 0 ")
                .Append(strWhere)
                .Append(strOrderBy)
                ' Code Commented By Vijay Ramteke on 04/03/2009 for Adding Student Lead Group Description
                ' This code is uncommented by VIjay Ramteke on 09/04/2009 as per issue mention in Mantis Bug Tracker ID=15851 
            End If
        End With

        Dim myCulture As New System.Globalization.CultureInfo("en-US", True)
        Dim dtRefDate As Date = Date.Parse(refDate, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
        'Dim dtRefDate As DateTime = Convert.ToDateTime(refDate)
        Dim agingDates As AgingBalance = GetAgingDates(dtRefDate)
        Dim refTime As String = " 11:59:59 PM"

        ' Add parameters to Parameter list
        db.AddParameter("@TransDate", dtRefDate & refTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", agingDates.CurrentStart, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", agingDates.CurrentEnd & refTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", agingDates.Start31To60DayPeriod, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", agingDates.End31To60DayPeriod & refTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", agingDates.Start61To90DayPeriod, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", agingDates.End61To90DayPeriod & refTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", agingDates.Start91To120Period, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", agingDates.End91To120Period & refTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", agingDates.Over120DayPeriod & refTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", agingDates.CurrentStart, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", agingDates.CurrentEnd & refTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", agingDates.Start31To60DayPeriod, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", agingDates.End31To60DayPeriod & refTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", agingDates.Start61To90DayPeriod, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", agingDates.End61To90DayPeriod & refTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", agingDates.Start91To120Period, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", agingDates.End91To120Period & refTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", agingDates.Over120DayPeriod & refTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", dtRefDate & refTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", dtRefDate & refTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "StudentAgingSummary"
            'Add new column.
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Public Function GetStudentPayments(ByVal StuEnrollId As String, ByVal RefDate As DateTime) As Decimal
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim obj As Object

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT SUM(TransAmount) FROM saTransactions ")
            .Append("WHERE TransAmount < 0 AND ")
            .Append("StuEnrollId = ? AND TransDate <= ? AND IsPosted=1  AND saTransactions.Voided=0")
        End With

        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", RefDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        obj = db.RunParamSQLScalar(sb.ToString)
        Dim payment As Decimal
        If Not (obj Is System.DBNull.Value) Then
            payment = obj
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return payment
    End Function


    Public Function GetStudentPayments(ByVal StuEnrollId As String) As Decimal
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim obj As Object

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT SUM(TransAmount) FROM saTransactions ")
            .Append("WHERE TransAmount < 0 AND ")
            .Append("StuEnrollId = ? AND IsPosted=1 AND saTransactions.Voided=0")
        End With

        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        obj = db.RunParamSQLScalar(sb.ToString)
        Dim payment As Decimal
        If Not (obj Is System.DBNull.Value) Then
            payment = obj
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return payment
    End Function


    Public Function GetStudentTransactions(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim refDate As String
        Dim idx As Integer
        Dim idx2 As Integer
        Dim myCulture As New System.Globalization.CultureInfo("en-US", True)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178
        Dim strWhereTemp As String = strWhere.ToLower
        Dim strStatusCodeIds As String = ""

        '' New Code Added By Vijay Ramteke on January 31, 2011 For RallyId DE4960
        Dim strSplit() As String
        If paramInfo.ShowUseStuCurrStatus = False Then
            Dim strTemp As String = ""
            strSplit = paramInfo.FilterList.ToLower.Replace("and", ":;").Split(";")
            Dim i As Integer
            If strSplit.Length > 0 Then
                strWhere = ""
                For i = 0 To strSplit.Length - 1
                    strTemp = strSplit(i)
                    If Not strTemp.Contains("arstuenrollments.statuscodeid") And Not strTemp.Trim = ":" Then
                        strWhere += "and " + strTemp.Replace(":", "")
                    End If
                Next
            End If
        End If
        '' New Code Added By Vijay Ramteke on January 31, 2011 For RallyId DE4960
        'get the statuscodeIds
        If strWhereTemp.ToLower.Contains("arstuenrollments.statuscodeid") Then
            If strWhereTemp.ToLower.Contains("arstuenrollments.statuscodeid in ") Then
                If strWhereTemp.Substring(strWhereTemp.ToLower.IndexOf("arstuenrollments.statuscodeid in ")).ToLower.IndexOf(" and ") >= 0 Then
                    strStatusCodeIds = strWhereTemp.Substring(strWhereTemp.ToLower.IndexOf("arstuenrollments.statuscodeid in ("), strWhereTemp.Substring(strWhereTemp.ToLower.IndexOf("arstuenrollments.statuscodeid in (")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.statuscodeid in", "").Replace(")", "").Replace("(", "")
                Else
                    strStatusCodeIds = strWhereTemp.Substring(strWhereTemp.ToLower.IndexOf("arstuenrollments.statuscodeid in (")).Replace("arstuenrollments.statuscodeid in", "").Replace(")", "").Replace("(", "")
                End If
            Else
                If strWhereTemp.Substring(strWhereTemp.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).ToLower.IndexOf(" and ") >= 0 Then
                    strStatusCodeIds = strWhereTemp.Substring(strWhereTemp.ToLower.IndexOf("arstuenrollments.statuscodeid = "), strWhereTemp.Substring(strWhereTemp.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.statuscodeid =", "").Replace(")", "").Replace("(", "")
                Else
                    strStatusCodeIds = strWhereTemp.Substring(strWhereTemp.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).Replace("arstuenrollments.statuscodeid =", "").Replace(")", "").Replace("(", "")
                End If
            End If
        End If
        'end the statuscodeIds
        '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178

        If paramInfo.FilterOther <> "" Then
            'Parse FilterOther in order to get Reference Date
            idx = paramInfo.FilterOther.IndexOf("BETWEEN")
            If idx <> -1 Then
                idx2 = paramInfo.FilterOther.IndexOf("AM")
                If idx2 <> -1 Then
                    refDate = paramInfo.FilterOther.Substring(idx + 9, idx2 + 2 - (idx + 8))
                Else
                    refDate = paramInfo.FilterOther.Substring(idx + 8)
                End If
                Dim tempDate() As String = refDate.Split(" ")
                If tempDate.GetLength(0) > 0 Then
                    refDate = tempDate(0)
                End If
            End If
        End If

        Dim dtRefDate As Date = Date.Parse(refDate, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
        Dim agingDates As AgingBalance = GetAgingDates(dtRefDate)

        'strOrderBy &= " ORDER BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,arPrgVersions.PrgVerDescrip," & _
        '                "arStudent.LastName,arStudent.FirstName,saTransactions.TransDate "

        If paramInfo.HideRptProgramVersion = True Then
            strOrderBy &= " ORDER BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip," & _
                        "arStudent.LastName,arStudent.FirstName,saTransactions.TransDate  "
        Else
            strOrderBy &= " ORDER BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,arPrgVersions.PrgVerDescrip," & _
                        "arStudent.LastName,arStudent.FirstName,saTransactions.TransDate "
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        'If StudentIdentifier = "SSN" Then

        ' Creating Query According to the Student Group to be shown on the Report 
        ' as per issue mention in Mantis Bug Tracker ID=15851
        With sb
            If paramInfo.ShowRptStudentGroup = True Then
                ' New Code By Vijay Ramteke on 04/03/2009 for Adding Student Lead Group Description
                .Append("SELECT syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,")
                .Append("syCampuses.CampDescrip,arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId,")

                '.Append("arPrgVersions.PrgVerDescrip,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")

                If paramInfo.HideRptProgramVersion = True Then
                    .Append(" '' as PrgVerDescrip,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
                Else
                    .Append("arPrgVersions.PrgVerDescrip,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
                End If

                If StudentIdentifier = "EnrollmentId" Then
                    .Append("arStuEnrollments.EnrollmentId AS StudentIdentifier,")
                ElseIf StudentIdentifier = "StudentId" Then
                    .Append("arStudent.StudentNumber AS StudentIdentifier,")
                Else
                    'StudentIdentifier = "SSN"
                    .Append("arStudent.SSN AS StudentIdentifier,")
                End If
                .Append("arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip,")
                .Append("saTransactions.TransDate,saTransactions.TransDescrip,adLeadGroups.Descrip AS LeadGroupDescrip, 1 AS ShowLeadGroup,saTransactions.TransAmount ")

                If paramInfo.HideRptProgramVersion = True Then
                    .Append(" ,1 AS HideProgramGroup ")
                Else
                    .Append(" ,0 AS HideProgramGroup ")
                End If

                .Append("FROM adLeadGroups INNER JOIN adLeadByLeadGroups ON  adLeadGroups.LeadGrpId = adLeadByLeadGroups.LeadGrpId ")
                .Append("RIGHT OUTER JOIN syCampGrps ")
                .Append("INNER JOIN syCmpGrpCmps ON  syCampGrps.CampGrpId =  syCmpGrpCmps.CampGrpId ")
                .Append("INNER JOIN syCampuses ON  syCmpGrpCmps.CampusId =  syCampuses.CampusId ")
                .Append("INNER JOIN arStuEnrollments ON  syCampuses.CampusId =  arStuEnrollments.CampusId ")
                .Append("INNER JOIN arPrgVersions ON  arStuEnrollments.PrgVerId =  arPrgVersions.PrgVerId ")
                .Append("INNER JOIN arStudent ON  arStuEnrollments.StudentId =  arStudent.StudentId ")
                .Append("INNER JOIN saTransactions ON  arStuEnrollments.StuEnrollId =  saTransactions.StuEnrollId ON adLeadByLeadGroups.StuEnrollId = arStuEnrollments.StuEnrollId ")

                'UnCommented Code Added By Vijay Ramteke on January 31, 2011 For RallyId DE4960
                ''Code Commented By Vijay Ramteke On November 12, 2010 For Rally Id DE1244
                '' '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178
                If paramInfo.ShowUseStuCurrStatus = False And Not strStatusCodeIds = "" Then
                    .Append(" INNER JOIN syStudentStatusChanges On syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId   ")
                End If
                '' '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178
                ''Code Commented By Vijay Ramteke On November 12, 2010 For Rally Id DE1244
                'UnCommented Code Added By Vijay Ramteke on January 31, 2011 For RallyId DE4960

                .Append("WHERE syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
                .Append("AND syCmpGrpCmps.CampusId=syCampuses.CampusId ")
                .Append("AND syCampuses.CampusId=arStuEnrollments.CampusId ")
                .Append("AND arPrgVersions.PrgVerId=arStuEnrollments.PrgVerId ")
                .Append("AND arStuEnrollments.StudentId=arStudent.StudentId ")
                .Append("AND arStuEnrollments.StuEnrollId=saTransactions.StuEnrollId ")
                .Append("AND adLeadGroups.UseForScheduling=1 ")
                .Append("AND saTransactions.TransDate <= ? AND saTransactions.IsPosted=1 ")
                .Append("AND saTransactions.Voided=0 ")
                .Append("AND (Select Sum(TransAmount) from saTransactions T where  T.Voided=0  and T.StuEnrollId=saTransactions.StuEnrollId And T.TransDate <=?) <> 0.00")

                'UnCommented Code Added By Vijay Ramteke on January 31, 2011 For RallyId DE4960
                ''Code Commented By Vijay Ramteke On November 12, 2010 For Rally Id DE1244
                '' '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178
                If paramInfo.ShowUseStuCurrStatus = False And Not strStatusCodeIds = "" Then
                    .Append("  AND syStudentStatusChanges.NewStatusId in(" + strStatusCodeIds + ")  ")
                    .Append(" AND syStudentStatusChanges.ModDate <= '" + dtRefDate + "' ")
                End If
                '' '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178
                ''Code Commented By Vijay Ramteke On November 12, 2010 For Rally Id DE1244
                'UnCommented Code Added By Vijay Ramteke on January 31, 2011 For RallyId DE4960

                .Append(strWhere)
                .Append(strOrderBy)
                ' New Code By Vijay Ramteke on 04/03/2009 for Adding Student Lead Group Description
            Else
                ' Code Commented By Vijay Ramteke on 04/03/2009 for Adding Student Lead Group Description
                ' This code is uncommented by VIjay Ramteke on 09/04/2009 as per issue mention in Mantis Bug Tracker ID=15851
                .Append("SELECT syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,")
                .Append("syCampuses.CampDescrip,arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId,")
                '.Append("arPrgVersions.PrgVerDescrip,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")

                If paramInfo.HideRptProgramVersion = True Then
                    .Append(" '' PrgVerDescrip,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
                Else
                    .Append("arPrgVersions.PrgVerDescrip,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
                End If

                If StudentIdentifier = "EnrollmentId" Then
                    .Append("arStuEnrollments.EnrollmentId AS StudentIdentifier,")
                ElseIf StudentIdentifier = "StudentId" Then
                    .Append("arStudent.StudentNumber AS StudentIdentifier,")
                Else
                    'StudentIdentifier = "SSN"
                    .Append("arStudent.SSN AS StudentIdentifier,")
                End If
                .Append("arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip,")
                .Append("saTransactions.TransDate,saTransactions.TransDescrip, '' AS LeadGroupDescrip, 0 AS ShowLeadGroup,saTransactions.TransAmount ")

                If paramInfo.HideRptProgramVersion = True Then
                    .Append(" ,1 AS HideProgramGroup ")
                Else
                    .Append(" ,0 AS HideProgramGroup ")
                End If

                .Append("FROM syCampGrps,syCmpGrpCmps,syCampuses,arStuEnrollments,arPrgVersions,arStudent,saTransactions ")
                'code Commented by Priyanka on date 28th of May 2009
                '// Readded by kamalesh on 01/01/2010 to resolve mantis issue id 18290
                'commented again by theresa G on 04/27/2010 for mantis
                '18560: Student Aging Summary report is displaying some duplicates after the upgrade from 2367 to 2395 
                ''Readded By Vijay Ramteke on July 22, 2010  Form Mantis Id 19419
                ''New Code Added By Vijay ramteek On August 11, 2010 For Mantis ID 18560
                If strWhere.ToLower.Contains("adleadgroups.leadgrpid") Then
                    .Append(" ,adLeadGroups,adLeadByLeadGroups ")
                End If
                ''New Code Added By Vijay ramteek On August 11, 2010 For Mantis ID 18560
                '---

                'UnCommented Code Added By Vijay Ramteke on January 31, 2011 For RallyId DE4960
                ''Code Commented By Vijay Ramteke On November 12, 2010 For Rally Id DE1244
                '' '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178

                '--------------------------------------------------------------------------------------------------------------------
                'Troy: 11/11/2013 Commented out the code to add the syStudentStatusChanges table to the query
                'If the student has multiple status change records during the period specified the transactions returned to the main
                'query will be repeated for each status change record. For eg. if the student has the user has 2 status change records
                'for the report period, then the transactions returned will be duplicated. This is why the original query had to be
                'using a DISTINCT in it. However, the DISTINCT creates a problem for the rare occasions when a student has multiple
                'payments for the same amount on the same date for the same award such as 2 Pell payments for $700 on 4/17/2012.
                'See Rally  DE9396: QA: Some reports show only one of the payments if they are of the same amount on the same day.
                '--------------------------------------------------------------------------------------------------------------------
                'If paramInfo.ShowUseStuCurrStatus = False And Not strStatusCodeIds = "" Then
                '    .Append(" ,syStudentStatusChanges  ")
                'End If

                '' '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178
                ''Code Commented By Vijay Ramteke On November 12, 2010 For Rally Id DE1244
                'UnCommented Code Added By Vijay Ramteke on January 31, 2011 For RallyId DE4960

                .Append("WHERE syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
                .Append("AND syCmpGrpCmps.CampusId=syCampuses.CampusId ")
                .Append("AND syCampuses.CampusId=arStuEnrollments.CampusId ")
                .Append("AND arPrgVersions.PrgVerId=arStuEnrollments.PrgVerId ")
                .Append("AND arStuEnrollments.StudentId=arStudent.StudentId ")
                .Append("AND arStuEnrollments.StuEnrollId=saTransactions.StuEnrollId ")

                '-code Commented by Priyanka on date 28th of May 2009
                '// Readded by kamalesh on 01/01/2010 to resolve mantis issue id 18290
                'commented again by theresa G on 04/27/2010 for mantis
                '18560: Student Aging Summary report is displaying some duplicates after the upgrade from 2367 to 2395 
                ''Readded By Vijay Ramteke on July 22, 2010  Form Mantis Id 19419
                ''New Code Added By Vijay ramteek On August 11, 2010 For Mantis ID 18560
                If strWhere.ToLower.Contains("adleadgroups.leadgrpid") Then
                    .Append("AND adLeadGroups.LeadGrpId = adLeadByLeadGroups.LeadGrpId ")
                    .Append("AND adLeadByLeadGroups.StuEnrollId = arStuEnrollments.StuEnrollId  ")
                    '' New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19644
                    .Append("AND adLeadGroups.UseForScheduling=1 ")
                    '' New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19644
                End If
                ''New Code Added By Vijay ramteek On August 11, 2010 For Mantis ID 18560
                ''---

                'UnCommented Code Added By Vijay Ramteke on January 31, 2011 For RallyId DE4960
                ''Code Commented By Vijay Ramteke On November 12, 2010 For Rally Id DE1244
                '' '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178


                '---------------------------------------------------------------------------------------------------------------------
                'Troy: 11/11/2013: See note above where I remove the syStudentStatusChanges table from the query
                '----------------------------------------------------------------------------------------------------------------------
                'If paramInfo.ShowUseStuCurrStatus = False And Not strStatusCodeIds = "" Then
                '    .Append(" AND syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId  ")
                '    .Append(" AND syStudentStatusChanges.NewStatusId in (" + strStatusCodeIds + ") ")
                '    .Append(" AND syStudentStatusChanges.ModDate <='" + dtRefDate + "' ")
                '    .Append(" AND syStudentStatusChanges.NewStatusId in (")
                '    .Append(" SELECT TOP 1 NewStatusId FROM syStudentStatusChanges WHERE StuEnrollId=arStuEnrollments.StuEnrollId AND")
                '    .Append(" ModDate<='" + dtRefDate + "' ORDER BY ModDate DESC) ")
                'End If


                '' '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178
                ''Code Commented By Vijay Ramteke On November 12, 2010 For Rally Id DE1244
                'UnCommented Code Added By Vijay Ramteke on January 31, 2011 For RallyId DE4960

                ' '' Code Added by Kamalesh Ahuja on 05- April 2010 to resolve Mantis Issue id 18225
                'commented again by theresa G on 04/27/2010 for mantis
                '18560: Student Aging Summary report is displaying some duplicates after the upgrade from 2367 to 2395 
                '.Append("AND adLeadGroups.UseForScheduling=1  ")
                ''''''''''''''''''''''''''''''''''''''

                .Append("AND saTransactions.TransDate <= ? AND saTransactions.IsPosted=1 ")
                .Append("AND saTransactions.Voided=0 ")
                '-Code added by Vijay Ramteke on Dec 16, 2009
                '.Append("AND (Select Sum(TransAmount) from saTransactions T where  T.Voided=0  and T.StuEnrollId=saTransactions.StuEnrollId and T.StuEnrollId=saTransactions.StuEnrollId) <> 0.00")
                .Append("AND (Select Sum(TransAmount) from saTransactions T where  T.Voided=0  and T.StuEnrollId=saTransactions.StuEnrollId and T.StuEnrollId=saTransactions.StuEnrollId And T.TransDate <=?) <> 0.00")
                '-Code added by Vijay Ramteke on Dec 16, 2009

                If paramInfo.ShowUseStuCurrStatus = False And Not strStatusCodeIds = "" Then
                    .Append(" AND EXISTS( ")
                    .Append("       SELECT TOP 1 StudentStatusChangeId ")
                    .Append("       FROM dbo.syStudentStatusChanges ")
                    .Append("       WHERE syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId  ")
                    .Append("       AND syStudentStatusChanges.NewStatusId in (" + strStatusCodeIds + ") ")
                    .Append("       AND syStudentStatusChanges.ModDate <='" + dtRefDate + "' ")
                    .Append("       AND syStudentStatusChanges.NewStatusId in (")
                    .Append("           SELECT TOP 1 NewStatusId FROM syStudentStatusChanges WHERE StuEnrollId=arStuEnrollments.StuEnrollId AND")
                    .Append("           ModDate<='" + dtRefDate + "' ORDER BY ModDate DESC) ")
                    .Append("       ) ")
                End If

                .Append(strWhere)
                .Append(strOrderBy)
                ' Code Commented By Vijay Ramteke on 04/03/2009 for Adding Student Lead Group Description
                ' This code is uncommented by VIjay Ramteke on 09/04/2009 as per issue mention in Mantis Bug Tracker ID=15851 
            End If
        End With
        'ElseIf StudentIdentifier = "EnrollmentId" Then
        '    With sb
        '        .Append("SELECT syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,")
        '        .Append("syCampuses.CampDescrip,arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId,")
        '        .Append("arPrgVersions.PrgVerDescrip,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
        '        .Append("arStuEnrollments.EnrollmentId AS StudentIdentifier,")
        '        .Append("arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip,")
        '        .Append("saTransactions.TransDate,saTransactions.TransDescrip,saTransactions.TransAmount ")
        '        .Append("FROM syCampGrps,syCmpGrpCmps,syCampuses,arStuEnrollments,arPrgVersions,arStudent,saTransactions ")
        '        .Append("WHERE syCmpGrpCmps.CampGrpId = syCampGrps.CampGrpId ")
        '        .Append("AND syCmpGrpCmps.CampusId = syCampuses.CampusId ")
        '        .Append("AND syCampuses.CampusId=arStuEnrollments.CampusId ")
        '        .Append("AND arPrgVersions.PrgVerId=arStuEnrollments.PrgVerId ")
        '        .Append("AND arStuEnrollments.StudentId = arStudent.StudentId ")
        '        .Append("AND arStuEnrollments.StuEnrollId = saTransactions.StuEnrollId ")
        '        .Append("AND saTransactions.TransDate <= ?")
        '        .Append(strWhere)
        '        .Append(strOrderBy)
        '    End With
        'ElseIf StudentIdentifier = "StudentId" Then
        '    With sb
        '        .Append("SELECT syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,")
        '        .Append("syCampuses.CampDescrip,arStuEnrollments.StuEnrollId,arStuEnrollments.PrgVerId,")
        '        .Append("arPrgVersions.PrgVerDescrip,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
        '        .Append("arStudent.StudentNumber AS StudentIdentifier,")
        '        .Append("arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip,")
        '        .Append("saTransactions.TransDate,saTransactions.TransDescrip,saTransactions.TransAmount ")
        '        .Append("FROM syCampGrps,syCmpGrpCmps,syCampuses,arStuEnrollments,arPrgVersions,arStudent,saTransactions ")
        '        .Append("WHERE syCmpGrpCmps.CampGrpId = syCampGrps.CampGrpId ")
        '        .Append("AND syCmpGrpCmps.CampusId = syCampuses.CampusId ")
        '        .Append("AND syCampuses.CampusId=arStuEnrollments.CampusId ")
        '        .Append("AND arPrgVersions.PrgVerId=arStuEnrollments.PrgVerId ")
        '        .Append("AND arStuEnrollments.StudentId = arStudent.StudentId ")
        '        .Append("AND arStuEnrollments.StuEnrollId = saTransactions.StuEnrollId ")
        '        .Append("AND saTransactions.TransDate <= ?")
        '        .Append(strWhere)
        '        .Append(strOrderBy)
        '    End With
        'End If
        '' Code Added by Kamalesh Ahuja on 05- April 2010 to resolve Mantis Issue id 18225
        Dim refTime As String = " 11:59:59 PM"
        '''''''''''''''''''''''''''''''''''''''''''''''''''

        db.AddParameter("@TransDate", dtRefDate.ToShortDateString & refTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '-Code added by Vijay Ramteke on Dec 16, 2009
        db.AddParameter("@TransDate", dtRefDate.ToShortDateString & refTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '-Code added by Vijay Ramteke on Dec 16, 2009
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "StudentAgingDetail"
            'Add columns.
            ds.Tables(0).Columns.Add(New DataColumn("CurrentBal", System.Type.GetType("System.Decimal")))
            ds.Tables(0).Columns.Add(New DataColumn("Bal31To60Day", System.Type.GetType("System.Decimal")))
            ds.Tables(0).Columns.Add(New DataColumn("Bal61To90Day", System.Type.GetType("System.Decimal")))
            ds.Tables(0).Columns.Add(New DataColumn("Bal91To120Day", System.Type.GetType("System.Decimal")))
            ds.Tables(0).Columns.Add(New DataColumn("BalOver120Day", System.Type.GetType("System.Decimal")))
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

    Private Function GetAgingDates(ByVal ReferenceDate As DateTime) As AgingBalance
        Dim aDates As New AgingBalance
        aDates.ReferenceDate = ReferenceDate
        AgingDates = aDates
        Return aDates
    End Function

#End Region


End Class
