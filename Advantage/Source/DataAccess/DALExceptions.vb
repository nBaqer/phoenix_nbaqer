Imports FAME.Advantage.Common

Public Class DALExceptions
    Public Shared Function ExceptionType(ByVal ex As OleDb.OleDbException) As String

        '   check for a Referential Integrity Exception
        If ex.Errors(ex.Errors.Count - 1).NativeError.ToString = "547" Then Return "547"

        '   check for a Unique Index Violation Exception
        If ex.Errors(ex.Errors.Count - 1).NativeError.ToString = "2601" Then Return "2601"

        '   check for a Violation of Primary Key Constraint
        If ex.Errors(ex.Errors.Count - 1).NativeError.ToString = "2627" Then Return "2627"

        '   It is other type of exception
        Return "otherError"

    End Function
    Public Shared Function ExceptionType1(ByVal ex As SqlException) As String

        '   check for a Referential Integrity Exception
        '' If ex.Errors(ex.Errors.Count - 1)..ToString = "547" Then Return "547"

        '   check for a Unique Index Violation Exception
        ' If ex.Errors(ex.Errors.Count - 1).NativeError.ToString = "2601" Then Return "2601"

        '   check for a Violation of Primary Key Constraint
        ' If ex.Errors(ex.Errors.Count - 1).NativeError.ToString = "2627" Then Return "2627"

        '   It is other type of exception
        Return "otherError"

    End Function
    Public Shared Function BuildErrorMessage(ex As OleDbException) As String

        '   this error is specific for the OLeDb Provider of MSSQL. 
        If ex.ErrorCode = -2147217873 Then
            '   get error type
            Select Case ExceptionType(ex)
                Case "547"  '   this is a referential integrity exception
                    Return BuildReferentialIntegrityExceptionMessage(New ReferentialIntegrityException("Referential Integrity Exception", ex))
                Case "2601" '   this is a unique index violation exception
                    Return BuildUniqueIndexExceptionMessage(New UniqueIndexException("Unique Index Exception", ex))
                Case "2627" '   this is a Violation of Primary Key constraint.
                    Return BuildPrimaryKeyConstraintExceptionMessage(New PrimaryKeyConstraintException("Primary Key Violation Exception", ex))
                Case Else   '   this is other OleDB exception.
                    Return ex.Message
            End Select
        Else
            Return ex.Message
        End If
    End Function
    Public Shared Function BuildErrorMessageSql1(ByVal ex As SqlException) As String

        '   this error is specific for the OLeDb Provider of MSSQL. 
        If ex.ErrorCode = -2147217873 Then
            '   get error type
            Select Case ExceptionType1(ex)
                Case "547"  '   this is a referential integrity exception
                    Return BuildReferentialIntegrityExceptionMessage(New ReferentialIntegrityException("Referential Integrity Exception", ex))
                Case "2601" '   this is a unique index violation exception
                    Return BuildUniqueIndexExceptionMessage(New UniqueIndexException("Unique Index Exception", ex))
                Case "2627" '   this is a Violation of Primary Key constraint.
                    Return BuildPrimaryKeyConstraintExceptionMessage(New PrimaryKeyConstraintException("Primary Key Violation Exception", ex))
                Case Else   '   this is other OleDB exception.
                    Return ex.Message
            End Select
        Else
            Return ex.Message
        End If
    End Function
    Public Shared Function ExceptionTypeSQL(ByVal ex As SqlClient.SqlException) As String


        For i = 0 To (ex.Errors.Count - 1)
            Select Case ex.Errors(i).Number.ToString
                '   check for a Referential Integrity Exception
                Case "547"
                    Return "547"
                    '   check for a Unique Index Violation Exception
                Case "2601"
                    Return "2601"
                    '   check for a Violation of Primary Key Constraint
                Case "2627"
                    Return "2627"
                Case Else
                    '   It is other type of exception
                    Return "otherError"
            End Select


        Next



    End Function
    Public Shared Function BuildErrorMessageSQL(ByVal ex As SqlClient.SqlException) As String

        '   this error is specific for the SQL Provider of MSSQL. 
        If ex.ErrorCode = -2146232060 Then
            '   get error type
            Select Case ExceptionTypeSQL(ex)
                Case "547"  '   this is a referential integrity exception
                    Return BuildReferentialIntegrityExceptionMessage(New ReferentialIntegrityException("Referential Integrity Exception", ex))
                Case "2601" '   this is a unique index violation exception
                    Return DALExceptions.BuildUniqueIndexExceptionMessage(New UniqueIndexException("Unique Index Exception", ex))
                Case "2627" '   this is a Violation of Primary Key constraint.
                    Return DALExceptions.BuildPrimaryKeyConstraintExceptionMessage(New PrimaryKeyConstraintException("Primary Key Violation Exception", ex))
                Case Else   '   this is other OleDB exception.
                    Return ex.Message
            End Select
        Else
            Return ex.Message
        End If
    End Function
    Public Shared Function BuildReferentialIntegrityExceptionMessage(ByVal ex As ReferentialIntegrityException) As String

        '   build message
        Dim message As New StringBuilder
        message.Append("You can not delete this item because it is being referenced in " + ChildTable(ex) + " Table.")

        '   return message
        Return message.ToString

    End Function
    Private Shared Function BuildUniqueIndexExceptionMessage(ByVal ex As UniqueIndexException) As String

        '   build message
        Dim message As New StringBuilder
        message.Append(GetIndexName(ex))

        '   return message
        Return message.ToString

    End Function
    Public Shared Function BuildConcurrencyExceptionMessage()

        '   build message
        Dim message As New StringBuilder
        message.Append("Other user just updated this record. Please get the updated record and try again ")

        '   return message
        Return message.ToString

    End Function
    Private Shared Function BuildPrimaryKeyConstraintExceptionMessage(ByVal ex As PrimaryKeyConstraintException) As String

        '   build message
        Dim message As New StringBuilder
        'message.Append("The record that you are trying to add to the " + GetTableName(ex) + " table already exists.")
        message.Append("The record that you are trying to add already exists.")
        '   return message
        Return message.ToString

    End Function
    Private Shared Function ChildTable(ByVal ex As ReferentialIntegrityException) As String
        Dim i As Integer = ex.GetBaseException.Message.LastIndexOf("', table '") + 12
        Dim j As Integer = ex.GetBaseException.Message.IndexOf("'", i + 1)
        Return ex.GetBaseException.Message.Substring(i, j - i)
    End Function
    Private Shared Function GetIndexName(ByVal ex As UniqueIndexException) As String
        Dim i As Integer = ex.GetBaseException.Message.LastIndexOf("' with unique index '") + 22
        Dim j As Integer = ex.GetBaseException.Message.IndexOf("]", i + 1)
        Return ex.GetBaseException.Message.Substring(i, j - i)
    End Function
    Private Shared Function GetTableName(ByVal ex As PrimaryKeyConstraintException) As String
        Dim i As Integer = ex.GetBaseException.Message.LastIndexOf(" key in object '") + 18
        Dim j As Integer = ex.GetBaseException.Message.IndexOf("'", i + 1)
        Return ex.GetBaseException.Message.Substring(i, j - i)
    End Function
    Public Function GetReferencedTablesByValue(ByVal tableName As String, ByVal value As String) As String
        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query to get startTime
        Dim sb As New StringBuilder
        With sb
            .Append("select ")
            .Append("		'(select convert(varchar,count(*)) from ' + ")
            .Append("		(Select name from sysobjects where id=fkc.fkeyid) +  ")
            .Append("		' where ' +  ")
            .Append("		(Select name from syscolumns where id=fkc.rkeyid and colid=fkc.rkey) + ")
            .Append("		'=''' +  ")
            .Append("		'" + value + "' + ")
            .Append("		''') + '';' + ")
            .Append("		(Select name from sysobjects where id=fkc.fkeyid) +  ")
            .Append("		'/'' + '		 ")
            .Append("from   sysforeignkeys fkc ")
            .Append("where rkeyid=(select id from sysobjects where name='" + tableName + "')")
        End With

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        sb = New StringBuilder()

        'concatenate all sql statements
        While dr.Read()
            sb.Append(dr(0))
        End While

        'execute second query
        Dim str As String = db.RunParamSQLScalar("Select " + sb.ToString + "''")

        If Not dr.IsClosed Then dr.Close()
        
        Return str
    End Function
    Public Function TimeCollisions(ByVal instructorId As String, ByVal roomId As String, ByVal workDaysId As String, ByVal startTimeId As String, ByVal endTimeId As String, ByVal startDate As Date, ByVal endDate As Date, ByVal ClsSectMeetingId As String) As String

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query to get startTime
        Dim sb As New StringBuilder
        With sb
            .Append("Select TimeIntervalDescrip from cmTimeInterval Where TimeIntervalId= ? ")
        End With

        ' Add the startTimeId to the parameter list
        db.AddParameter("@StartTimeId", startTimeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   get StartTime value
        Dim startTime As DateTime = CType(db.RunParamSQLScalar(sb.ToString), DateTime)

        '   build the sql query to get endTime
        sb = New StringBuilder
        With sb
            .Append("Select TimeIntervalDescrip from cmTimeInterval Where TimeIntervalId= ? ")
        End With

        ' Add the endTimeId to the parameter list
        db.ClearParameters()
        db.AddParameter("@EndTimeId", endTimeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   get EndTime value
        Dim endTime As DateTime = CType(db.RunParamSQLScalar(sb.ToString), DateTime)

        '   check if End Time is geater than StartTime
        Dim resultString As New StringBuilder
        If startTime >= endTime Then
            resultString.Append("Start Time is greater than End Time" + vbCrLf)
        End If


        '   build the sql query to get the WorkDay
        sb = New StringBuilder
        With sb
            .Append("Select WorkDaysDescrip from plWorkDays where WorkDaysId= ? ")
        End With

        ' Add worksDayId to the parameter list
        db.ClearParameters()
        db.AddParameter("@WorkDaysId", workDaysId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   get WorkDay value
        Dim WorkDay As String = db.RunParamSQLScalar(sb.ToString)

        '   build the sql query to check for rooms collisions
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       CS.ClsSection, ")
            '.Append("       CS.StartDate, ")
            '.Append("       CS.EndDate, ")
            .Append("       R.Descrip, ")
            .Append("       CSM.TimeIntervalId, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.TimeIntervalId) As StartTime, ")
            .Append("       CSM.EndIntervalId, ")
            .Append("	    (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.EndIntervalId) As EndTime ")
            .Append("FROM ")
            .Append("       arClsSectMeetings CSM, ")
            .Append("       arClassSections CS, ")
            .Append("       arReqs R, ")
            .Append("       cmTimeInterval TI1, ")
            .Append("       cmTimeInterval TI2 ")
            .Append("WHERE ")
            .Append("       (CSM.ClsSectionId = CS.ClsSectionId) ")
            .Append("AND    (CS.ReqId = R.ReqId) ")
            .Append("AND    (CSM.TimeIntervalId = TI1.TimeIntervalId) ")
            .Append("AND	(CSM.EndIntervalId=TI2.TimeIntervalId) ")
            '.Append("AND    (CSM.ClsSectMeetingId <> ?) ")
            .Append("AND    (CSM.ClsSectMeetingId NOT IN ( ")
            .Append("SELECT ")
            .Append(" ? ")
            .Append("UNION ")
            .Append("SELECT ")
            .Append("       ClsSectMeetingId ")
            .Append("FROM ")
            .Append("       arClsSectMeetings ")
            .Append("WHERE	ClsSectionId IN (select RightClsSectionId from arOverridenConflicts where LeftClsSectionId=(select ClsSectionId from arClsSectMeetings where ClsSectMeetingId=?)) ")
            .Append(" )) ")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            .Append("AND	(CSM.WorkDaysId= ?) ")
            .Append("AND	(CSM.RoomId= ?) ")
            .Append("AND	(?<TI2.TimeIntervalDescrip ")
            .Append("AND    TI1.TimeIntervalDescrip<?) ")
            .Append("AND	(?<CS.EndDate ")
            .Append("AND     CS.StartDate<?) ")
        End With

        ' Add workDaysId to the parameter list
        db.ClearParameters()

        ' Add ClsSectMeetingId to the parameter list
        db.AddParameter("@ClsSectMeetingId", ClsSectMeetingId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add ClsSectMeetingId to the parameter list
        db.AddParameter("@ClsSectMeetingId", ClsSectMeetingId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add WorkDays to the parameter list
        db.AddParameter("@WorkDaysId", workDaysId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add roomId to the parameter list
        db.AddParameter("@RoomId", roomId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add the startTime to the parameter list
        db.AddParameter("@StartTime", startTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add the endTime the parameter list
        db.AddParameter("@EndTime", endTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add the startDate to the parameter list
        db.AddParameter("@StartDate", startDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add the endDate the parameter list
        db.AddParameter("@EndDate", endDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   report error
        While dr.Read()
            resultString.Append("Room collision on " + WorkDay + " from " + CType(dr("StartTime"), String) + " to " + CType(dr("EndTime"), String) + " with " + dr("Descrip") + "-" + dr("ClsSection") + "." + vbCrLf)
            'resultString.Append(CType(dr("ClsSectionId"), Guid).ToString + "/" + CType(dr("TimeIntervalId"), Guid).ToString + " / " + CType(dr("EndIntervalId"), Guid).ToString)
            'resultString.Append(CType(dr("StartDate"), Date).ToString + "   " + CType(dr("EndDate"), Date).ToString)
        End While

        '   build the sql query to check for intructor collisions
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       CS.ClsSection, ")
            '.Append("       CS.StartDate, ")
            '.Append("       CS.EndDate, ")
            .Append("       R.Descrip, ")
            .Append("       CSM.TimeIntervalId, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.TimeIntervalId) As StartTime, ")
            .Append("       CSM.EndIntervalId, ")
            .Append("	    (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.EndIntervalId) As EndTime ")
            .Append("FROM  ")
            .Append("       arClsSectMeetings CSM, ")
            .Append("       arClassSections CS, ")
            .Append("       arReqs R, ")
            .Append("       cmTimeInterval TI1, ")
            .Append("       cmTimeInterval TI2 ")
            .Append("WHERE ")
            .Append("       CSM.ClsSectionId = CS.ClsSectionId ")
            .Append("AND    CS.ReqId = R.ReqId ")
            .Append("AND	CSM.TimeIntervalId=TI1.TimeIntervalId ")
            .Append("AND	CSM.EndIntervalId=TI2.TimeIntervalId ")
            '.Append("AND    (CSM.ClsSectMeetingId <> ?) ")
            .Append("AND    (CSM.ClsSectMeetingId NOT IN ( ")
            .Append("SELECT ")
            .Append(" ? ")
            .Append("UNION ")
            .Append("SELECT ")
            .Append("       ClsSectMeetingId ")
            .Append("FROM ")
            .Append("       arClsSectMeetings ")
            .Append("WHERE	ClsSectionId IN (select RightClsSectionId from arOverridenConflicts where LeftClsSectionId=(select ClsSectionId from arClsSectMeetings where ClsSectMeetingId=?)) ")
            .Append(" )) ")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            .Append("AND	CSM.WorkDaysId= ? ")
            If Not instructorId = "" Then
                .Append("AND	CS.InstructorId= ? ")
            End If
            .Append("AND	(?<TI2.TimeIntervalDescrip ")
            .Append("AND    TI1.TimeIntervalDescrip<?) ")
            .Append("AND	(?<CS.EndDate ")
            .Append("AND     CS.StartDate<?) ")
        End With

        ' Add workDaysId to the parameter list
        db.ClearParameters()

        ' Add ClsSectMeetingId to the parameter list
        db.AddParameter("@ClsSectMeetingId", ClsSectMeetingId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add ClsSectMeetingId to the parameter list
        db.AddParameter("@ClsSectMeetingId", ClsSectMeetingId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add WorkDays to the parameter list
        db.AddParameter("@WorkDaysId", workDaysId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        If Not instructorId = "" Then
            ' Add IntructorId to the parameter list
            db.AddParameter("@EmpId", instructorId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        ' Add the startTime to the parameter list
        db.AddParameter("@StartTime", startTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add the endTime the parameter list
        db.AddParameter("@EndTime", endTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add the startDate to the parameter list
        db.AddParameter("@StartDate", startDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add the endDate the parameter list
        db.AddParameter("@EndDate", endDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        '   Execute the query
        dr = db.RunParamSQLDataReader(sb.ToString)

        '   report error
        While dr.Read()
            'resultString.Append("Instructor collision on " + WorkDay + " from " + CType(dr("StartTime"), String) + " to " + CType(dr("EndTime"), String) + vbCrLf)
            resultString.Append("Instructor collision on " + WorkDay + " from " + CType(dr("StartTime"), String) + " to " + CType(dr("EndTime"), String) + " with " + dr("Descrip") + "-" + dr("ClsSection") + "." + vbCrLf)
            'resultString.Append(CType(dr("ClsSectionId"), Guid).ToString + "/" + CType(dr("TimeIntervalId"), Guid).ToString + " / " + CType(dr("EndIntervalId"), Guid).ToString)
            'resultString.Append(CType(dr("StartDate"), Date).ToString + "   " + CType(dr("EndDate"), Date).ToString)
        End While

        If Not dr.IsClosed Then dr.Close()

        Return resultString.ToString

    End Function

    Public Function TimeCollisionsByClsSectionId(ByVal instructorId As String, ByVal roomId As String, ByVal workDaysId As String, ByVal startTimeId As String, ByVal endTimeId As String, ByVal startDate As Date, ByVal endDate As Date, ByVal ClsSectionId As String) As String

        '   connect to the database
        Dim db As New DataAccess

     
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query to get startTime
        Dim sb As New StringBuilder
        With sb
            .Append("Select TimeIntervalDescrip from cmTimeInterval Where TimeIntervalId= ? ")
        End With

        ' Add the startTimeId to the parameter list
        db.AddParameter("@StartTimeId", startTimeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   get StartTime value
        Dim startTime As DateTime = CType(db.RunParamSQLScalar(sb.ToString), DateTime)

        '   build the sql query to get endTime
        sb = New StringBuilder
        With sb
            .Append("Select TimeIntervalDescrip from cmTimeInterval Where TimeIntervalId= ? ")
        End With

        ' Add the endTimeId to the parameter list
        db.ClearParameters()
        db.AddParameter("@EndTimeId", endTimeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   get EndTime value
        Dim endTime As DateTime = CType(db.RunParamSQLScalar(sb.ToString), DateTime)

        '   check if End Time is geater than StartTime
        Dim resultString As New StringBuilder
        If startTime >= endTime Then
            resultString.Append("Start Time is greater than End Time" + vbCrLf)
        End If


        '   build the sql query to get the WorkDay
        sb = New StringBuilder
        With sb
            .Append("Select WorkDaysDescrip from plWorkDays where WorkDaysId= ? ")
        End With

        ' Add worksDayId to the parameter list
        db.ClearParameters()
        db.AddParameter("@WorkDaysId", workDaysId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   get WorkDay value
        Dim WorkDay As String = db.RunParamSQLScalar(sb.ToString)

        '   build the sql query to check for rooms collisions
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       CS.ClsSection, ")
            '.Append("       CS.StartDate, ")
            '.Append("       CS.EndDate, ")
            .Append("       R.Descrip, ")
            .Append("       CSM.TimeIntervalId, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.TimeIntervalId) As StartTime, ")
            .Append("       CSM.EndIntervalId, ")
            .Append("	    (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.EndIntervalId) As EndTime ")
            .Append("FROM ")
            .Append("       arClsSectMeetings CSM, ")
            .Append("       arClassSections CS, ")
            .Append("       arReqs R, ")
            .Append("       cmTimeInterval TI1, ")
            .Append("       cmTimeInterval TI2 ")
            .Append("WHERE ")
            .Append("       (CSM.ClsSectionId = CS.ClsSectionId) ")
            .Append("AND    (CS.ReqId = R.ReqId) ")
            .Append("AND    (CSM.TimeIntervalId = TI1.TimeIntervalId) ")
            .Append("AND	(CSM.EndIntervalId=TI2.TimeIntervalId) ")
            '.Append("AND    (CSM.ClsSectMeetingId <> ?) ")
            .Append("AND    (CSM.ClsSectMeetingId NOT IN ( ")
            .Append("SELECT ")
            .Append("       ClsSectMeetingId ")
            .Append("FROM ")
            .Append("       arClsSectMeetings ")
            .Append("WHERE	ClsSectionId = ? ")
            .Append("UNION ")
            .Append("SELECT ")
            .Append("       ClsSectMeetingId ")
            .Append("FROM ")
            .Append("       arClsSectMeetings ")
            .Append("WHERE	ClsSectionId IN (select RightClsSectionId from arOverridenConflicts where LeftClsSectionId= ? ) ")
            .Append(" )) ")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            .Append("AND	(CSM.WorkDaysId= ?) ")
            .Append("AND	(CSM.RoomId= ?) ")
            .Append("AND	(?<TI2.TimeIntervalDescrip ")
            .Append("AND    TI1.TimeIntervalDescrip<?) ")
            .Append("AND	(?<CS.EndDate ")
            .Append("AND     CS.StartDate<?) ")
        End With

        ' Add workDaysId to the parameter list
        db.ClearParameters()

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add WorkDays to the parameter list
        db.AddParameter("@WorkDaysId", workDaysId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add roomId to the parameter list
        db.AddParameter("@RoomId", roomId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add the startTime to the parameter list
        db.AddParameter("@StartTime", startTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add the endTime the parameter list
        db.AddParameter("@EndTime", endTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add the startDate to the parameter list
        db.AddParameter("@StartDate", startDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add the endDate the parameter list
        db.AddParameter("@EndDate", endDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   report error
        While dr.Read()
            'resultString.Append("Room collision on " + WorkDay + " from " + CType(dr("StartTime"), String) + " to " + CType(dr("EndTime"), String) + vbCrLf)
            resultString.Append("Room collision on " + WorkDay + " from " + CType(dr("StartTime"), String) + " to " + CType(dr("EndTime"), String) + " with " + dr("Descrip") + "-" + dr("ClsSection") + "." + vbCrLf)
            'resultString.Append(CType(dr("ClsSectionId"), Guid).ToString + "/" + CType(dr("TimeIntervalId"), Guid).ToString + " / " + CType(dr("EndIntervalId"), Guid).ToString)
            'resultString.Append(CType(dr("StartDate"), Date).ToString + "   " + CType(dr("EndDate"), Date).ToString)
        End While

        '   build the sql query to check for intructor collisions
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       CS.ClsSection, ")
            '.Append("       CS.StartDate, ")
            '.Append("       CS.EndDate, ")
            .Append("       R.Descrip, ")
            .Append("       CSM.TimeIntervalId, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.TimeIntervalId) As StartTime, ")
            .Append("       CSM.EndIntervalId, ")
            .Append("	    (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.EndIntervalId) As EndTime ")
            .Append("FROM  ")
            .Append("       arClsSectMeetings CSM, ")
            .Append("       arClassSections CS, ")
            .Append("       arReqs R, ")
            .Append("       cmTimeInterval TI1, ")
            .Append("       cmTimeInterval TI2 ")
            .Append("WHERE ")
            .Append("       CSM.ClsSectionId = CS.ClsSectionId ")
            .Append("AND    CS.ReqId = R.ReqId ")
            .Append("AND	CSM.TimeIntervalId=TI1.TimeIntervalId ")
            .Append("AND	CSM.EndIntervalId=TI2.TimeIntervalId ")
            '.Append("AND    (CSM.ClsSectMeetingId <> ?) ")
            .Append("AND    (CSM.ClsSectMeetingId NOT IN ( ")
            .Append("SELECT ")
            .Append("       ClsSectMeetingId ")
            .Append("FROM ")
            .Append("       arClsSectMeetings ")
            .Append("WHERE	ClsSectionId = ? ")
            .Append("UNION ")
            .Append("SELECT ")
            .Append("       ClsSectMeetingId ")
            .Append("FROM ")
            .Append("       arClsSectMeetings ")
            .Append("WHERE	ClsSectionId IN (select RightClsSectionId from arOverridenConflicts where LeftClsSectionId= ? ) ")
            .Append(" )) ")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            .Append("AND	CSM.WorkDaysId= ? ")
            If Not instructorId = "" Then
                .Append("AND	CS.InstructorId= ? ")
            End If
            .Append("AND	(?<TI2.TimeIntervalDescrip ")
            .Append("AND    TI1.TimeIntervalDescrip<?) ")
            .Append("AND	(?<CS.EndDate ")
            .Append("AND     CS.StartDate<?) ")
        End With

        ' Add workDaysId to the parameter list
        db.ClearParameters()

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add WorkDays to the parameter list
        db.AddParameter("@WorkDaysId", workDaysId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        If Not instructorId = "" Then
            ' Add IntructorId to the parameter list
            db.AddParameter("@EmpId", instructorId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        ' Add the startTime to the parameter list
        db.AddParameter("@StartTime", startTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add the endTime the parameter list
        db.AddParameter("@EndTime", endTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add the startDate to the parameter list
        db.AddParameter("@StartDate", startDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add the endDate the parameter list
        db.AddParameter("@EndDate", endDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        '   Execute the query
        dr = db.RunParamSQLDataReader(sb.ToString)

        '   report error
        While dr.Read()
            'resultString.Append("Instructor collision on " + WorkDay + " from " + CType(dr("StartTime"), String) + " to " + CType(dr("EndTime"), String) + vbCrLf)
            resultString.Append("Instructor collision on " + WorkDay + " from " + CType(dr("StartTime"), String) + " to " + CType(dr("EndTime"), String) + " with " + dr("Descrip") + "-" + dr("ClsSection") + "." + vbCrLf)
            'resultString.Append(CType(dr("ClsSectionId"), Guid).ToString + "/" + CType(dr("TimeIntervalId"), Guid).ToString + " / " + CType(dr("EndIntervalId"), Guid).ToString)
            'resultString.Append(CType(dr("StartDate"), Date).ToString + "   " + CType(dr("EndDate"), Date).ToString)
        End While

        If Not dr.IsClosed Then dr.Close()

        Return resultString.ToString

    End Function


    ''Added by Saraswathi lakshmanan on August 14 2009
    ''To check for time collisions in Enter ClassS ections with periods page

    Public Function TimeCollisionsforClsSectionwithPeriods(ByVal instructorId As String, ByVal roomId As String, ByVal PeriodID As String, ByVal AltPeriodID As String, ByVal startDate As Date, ByVal endDate As Date, ByVal ClsSectMeetingId As String) As String
        Dim resultString As New StringBuilder
        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query to get startTime
        Dim sb As New StringBuilder


        '   build the sql query to check for rooms collisions
        sb = New StringBuilder
        With sb

            ''Query Modified by Saraswathi lakshmanan on September 18 2009 
            ''To fix mantis issue 17590,17593,17594

            .Append(" SELECT    Distinct    CS.ClsSection, (Select TermDescrip from arterm where TermId=CS.TermID)TermDescrip  ,     R.Descrip,        CSM.PeriodID,TI1.PeriodDescrip, CSM.RoomID,StartTimeID,ENDTimeID,cs.EndDate,cs.StartDate    FROM     arClsSectMeetings CSM,   ")
            .Append(" arClassSections CS,        arReqs R,        SyPeriods TI1,SyPeriodsWorkDays PWD,      cmTimeInterval TI2,cmTimeInterval TI3 WHERE ")
            .Append(" (CSM.ClsSectionId = CS.ClsSectionId) AND    (CS.ReqId = R.ReqId) AND  (TI1.PeriodID=CSM.PeriodID) ")
            .Append(" and  TI1.PEriodID=PWD.PEriodID AND (TI1.StartTimeID=TI2.TimeIntervalId) ANd (TI1.ENDTimeID=TI3.TimeIntervalId) ")
            .Append(" And(CSM.ClsSectMeetingId ")
            .Append(" NOT IN ( SELECT  ? UNION SELECT        ClsSectMeetingId  FROM  arClsSectMeetings WHERE	ClsSectionId IN ")
            .Append(" (select RightClsSectionId from arOverridenConflicts where LeftClsSectionId=(select ClsSectionId  ")
            .Append(" from arClsSectMeetings where ClsSectMeetingId=? ))  )) AND (PWD.WorkDayId in (Select WorkDayID from ")
            .Append(" syPeriodsWorkDays where PeriodId=? ")
            If AltPeriodID.ToString <> Guid.Empty.ToString Then
                .Append("  union Select WorkDayID from syPeriodsWorkDays where AltPeriodId=?  ")
            End If

            '.Append(" ) ) AND(CSM.RoomId= ?) AND  (?<=CS.EndDate AND ")
            '.Append(" CS.StartDate<=?) and")
            ''Modified By Saraswathi lakshmanan on Sept 18 2009
            ''To fix mantis case 17593
            .Append(" ) ) AND(CSM.RoomId= ?) AND  (?<=CSM.EndDate AND ")
            .Append(" CSM.StartDate<=?) and")

            .Append("( ((Select TimeIntervalDescrip from cmTimeInterval CT, syPeriods SP where SP.StartTimeId=CT.TimeIntervalId and")
            .Append(" SP.PeriodID=?) <(Select TimeIntervalDescrip from CmTimeInterval where TimeINtervalId= TI3.TimeIntervalId) )")
            .Append(" and")
            .Append(" ((Select TimeIntervalDescrip from CmTimeInterval where TimeINtervalId= TI2.TimeIntervalId)<")
            .Append(" (Select TimeIntervalDescrip from cmTimeInterval CT, syPeriods SP where SP.EndTimeId=CT.TimeIntervalId and")
            .Append(" SP.PeriodID=?)) ")
            If AltPeriodID.ToString <> Guid.Empty.ToString Then
                .Append(" or ((Select TimeIntervalDescrip from cmTimeInterval CT, syPeriods SP where SP.StartTimeId=CT.TimeIntervalId and")
                .Append(" SP.PeriodID=?) <(Select TimeIntervalDescrip from CmTimeInterval where TimeINtervalId= TI3.TimeIntervalId) )")
                .Append(" and")
                .Append(" ((Select TimeIntervalDescrip from CmTimeInterval where TimeINtervalId= TI2.TimeIntervalId)<")
                .Append(" (Select TimeIntervalDescrip from cmTimeInterval CT, syPeriods SP where SP.EndTimeId=CT.TimeIntervalId and")
                .Append(" SP.PeriodID=?)) ")
            End If
            .Append(" ) ")


        End With

        ' Add workDaysId to the parameter list
        db.ClearParameters()

        ' Add ClsSectMeetingId to the parameter list
        db.AddParameter("@ClsSectMeetingId", ClsSectMeetingId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add ClsSectMeetingId to the parameter list
        db.AddParameter("@ClsSectMeetingId", ClsSectMeetingId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        ' Add WorkDays to the parameter list
        db.AddParameter("@PeriodId", PeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        If AltPeriodID.ToString <> Guid.Empty.ToString Then
            ' Add ClsSectMeetingId to the parameter list
            db.AddParameter("@AltperiodID", AltPeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        ' Add roomId to the parameter list
        db.AddParameter("@RoomId", roomId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add the startDate to the parameter list
        db.AddParameter("@StartDate", startDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add the endDate the parameter list
        db.AddParameter("@EndDate", endDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        db.AddParameter("@PeriodId", PeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PeriodId", PeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        If AltPeriodID.ToString <> Guid.Empty.ToString Then
            db.AddParameter("@AltperiodID", AltPeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AltperiodID", AltPeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)


        '   report error
        While dr.Read()
            '  resultString.Append("Room collision on " + WorkDay + " from " + CType(dr("StartTime"), String) + " to " + CType(dr("EndTime"), String) + " with " + dr("Descrip") + "-" + dr("ClsSection") + "." + vbCrLf)
            resultString.Append("Room collision in Term: " + dr("TermDescrip") + ", For Course Name (or Course code)- " + dr("Descrip") + "-" + dr("ClsSection"))
            resultString.Append(", For Period: " + dr("PeriodDescrip") + "." + vbCrLf)
            'resultString.Append("( " + Format(CType(dr("StartDate"), Date), "MM/dd/yy") + "-" + Format(CType(dr("EndDate"), Date), "MM/dd/yy") + ")." + vbCrLf)
            'resultString.Append(CType(dr("ClsSectionId"), Guid).ToString + "/" + CType(dr("TimeIntervalId"), Guid).ToString + " / " + CType(dr("EndIntervalId"), Guid).ToString)
            'resultString.Append(CType(dr("StartDate"), Date).ToString + "   " + CType(dr("EndDate"), Date).ToString)
        End While

        '   build the sql query to check for intructor collisions
        sb = New StringBuilder
        With sb
            .Append(" SELECT    Distinct    CS.ClsSection, (Select TermDescrip from arterm where TermId=CS.TermID)TermDescrip  ,       R.Descrip,        CSM.PeriodID,TI1.PeriodDescrip, CSM.RoomID,StartTimeID,ENDTimeID,cs.EndDate,cs.StartDate    FROM     arClsSectMeetings CSM,   ")
            .Append(" arClassSections CS,        arReqs R,        SyPeriods TI1,SyPeriodsWorkDays PWD,      cmTimeInterval TI2,cmTimeInterval TI3 WHERE ")
            .Append(" (CSM.ClsSectionId = CS.ClsSectionId) AND    (CS.ReqId = R.ReqId) AND  (TI1.PeriodID=CSM.PeriodID) ")
            .Append(" and  TI1.PEriodID=PWD.PEriodID AND (TI1.StartTimeID=TI2.TimeIntervalId) ANd (TI1.ENDTimeID=TI3.TimeIntervalId) ")
            .Append(" And(CSM.ClsSectMeetingId ")
            .Append(" NOT IN ( SELECT  ? UNION SELECT        ClsSectMeetingId  FROM  arClsSectMeetings WHERE	ClsSectionId IN ")
            .Append(" (select RightClsSectionId from arOverridenConflicts where LeftClsSectionId=(select ClsSectionId  ")
            .Append(" from arClsSectMeetings where ClsSectMeetingId=? ))  )) AND (PWD.WorkDayId in (Select WorkDayID from ")
            .Append(" syPeriodsWorkDays where PeriodId=? ")
            If AltPeriodID.ToString <> Guid.Empty.ToString Then
                .Append("  union Select WorkDayID from syPeriodsWorkDays where AltPeriodId=?  ")
            End If

            .Append(" ) ) ")
            '.Append(" AND(CSM.RoomId= ?) ")
            If instructorId.ToString <> Guid.Empty.ToString Then
                .Append("AND	CS.InstructorId= ? ")
            End If
            '.Append(" AND  (?<=CS.EndDate AND ")
            '.Append(" CS.StartDate<=?) and")
            ''Modified by Saraswathi lakshmanan on Sept 18 2009
            ''to fix mantis case 17593
            .Append(" AND  (?<=CSM.EndDate AND ")
            .Append(" CSM.StartDate<=?) and")

            .Append("( ((Select TimeIntervalDescrip from cmTimeInterval CT, syPeriods SP where SP.StartTimeId=CT.TimeIntervalId and")
            .Append(" SP.PeriodID=?) <(Select TimeIntervalDescrip from CmTimeInterval where TimeINtervalId= TI3.TimeIntervalId) )")
            .Append(" and")
            .Append(" ((Select TimeIntervalDescrip from CmTimeInterval where TimeINtervalId= TI2.TimeIntervalId)<")
            .Append(" (Select TimeIntervalDescrip from cmTimeInterval CT, syPeriods SP where SP.EndTimeId=CT.TimeIntervalId and")
            .Append(" SP.PeriodID=?)) ")
            If AltPeriodID.ToString <> Guid.Empty.ToString Then
                .Append(" or ((Select TimeIntervalDescrip from cmTimeInterval CT, syPeriods SP where SP.StartTimeId=CT.TimeIntervalId and")
                .Append(" SP.PeriodID=?) <(Select TimeIntervalDescrip from CmTimeInterval where TimeINtervalId= TI3.TimeIntervalId) )")
                .Append(" and")
                .Append(" ((Select TimeIntervalDescrip from CmTimeInterval where TimeINtervalId= TI2.TimeIntervalId)<")
                .Append(" (Select TimeIntervalDescrip from cmTimeInterval CT, syPeriods SP where SP.EndTimeId=CT.TimeIntervalId and")
                .Append(" SP.PeriodID=?)) ")
            End If
            .Append(" ) ")

        End With

        ' Add workDaysId to the parameter list
        db.ClearParameters()


        ' Add ClsSectMeetingId to the parameter list
        db.AddParameter("@ClsSectMeetingId", ClsSectMeetingId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add ClsSectMeetingId to the parameter list
        db.AddParameter("@ClsSectMeetingId", ClsSectMeetingId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        ' Add WorkDays to the parameter list
        db.AddParameter("@PeriodId", PeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        If AltPeriodID.ToString <> Guid.Empty.ToString Then
            ' Add ClsSectMeetingId to the parameter list
            db.AddParameter("@AltperiodID", AltPeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        If instructorId.ToString <> Guid.Empty.ToString Then
            db.AddParameter("@InstructorID", instructorId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        ' Add the startDate to the parameter list
        db.AddParameter("@StartDate", startDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add the endDate the parameter list
        db.AddParameter("@EndDate", endDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        db.AddParameter("@PeriodId", PeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PeriodId", PeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        If AltPeriodID.ToString <> Guid.Empty.ToString Then
            db.AddParameter("@AltperiodID", AltPeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AltperiodID", AltPeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        '   Execute the query
        dr = db.RunParamSQLDataReader(sb.ToString)

        '   report error
        While dr.Read()
            resultString.Append("Instructor/ Time Collision in Term: " + dr("TermDescrip") + ", For Course Name (or Course code)- " + dr("Descrip") + "-" + dr("ClsSection") + ", For Period: " + dr("PeriodDescrip") + "." + vbCrLf)
            'resultString.Append("( " + Format(CType(dr("StartDate"), Date), "MM/dd/yy") + "-" + Format(CType(dr("EndDate"), Date), "MM/dd/yy") + ") ")

            'resultString.Append(" For " + dr("Descrip") + "-" + dr("ClsSection") + vbCrLf)
            'resultString.Append("Instructor collision on period " + dr("PeriodDescrip") + " from ")
            'resultString.Append("( " + Format(CType(dr("StartDate"), Date), "MM/dd/yy") + "-" + Format(CType(dr("EndDate"), Date), "MM/dd/yy") + ") ")

            'resultString.Append(" For " + dr("Descrip") + "-" + dr("ClsSection") + vbCrLf)

        End While

        If Not dr.IsClosed Then dr.Close()

        Return resultString.ToString

    End Function

    ''Added by Saraswathi lakshmanan on august 17 2009
    ''To get the Room Collision and instructor Collision for Enter classSections with periods, when override conflicts is checked
    Public Function TimeCollisionsByClsSectionIdforClassSecWPeriods(ByVal instructorId As String, ByVal roomId As String, ByVal PeriodID As String, ByVal AltPeriodID As String, ByVal startDate As Date, ByVal endDate As Date, ByVal ClsSectionId As String) As String

        Dim resultString As New StringBuilder
        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query to get startTime
        Dim sb As New StringBuilder
        '   build the sql query to check for rooms collisions
        '   build the sql query to check for rooms collisions
        sb = New StringBuilder
        With sb
            ''Query Modified by Saraswathi lakshmanan on September 18 2009 
            ''To fix mantis issue 17590,17593,17594

            .Append(" SELECT    Distinct    CS.ClsSection,  (Select TermDescrip from arterm where TermId=CS.TermID)TermDescrip  ,       R.Descrip,        CSM.PeriodID,TI1.PeriodDescrip, CSM.RoomID,StartTimeID,ENDTimeID,cs.EndDate,cs.StartDate    FROM     arClsSectMeetings CSM,   ")
            .Append(" arClassSections CS,        arReqs R,        SyPeriods TI1,SyPeriodsWorkDays PWD,      cmTimeInterval TI2,cmTimeInterval TI3 WHERE ")
            .Append(" (CSM.ClsSectionId = CS.ClsSectionId) AND    (CS.ReqId = R.ReqId) AND  (TI1.PeriodID=CSM.PeriodID) ")
            .Append(" and  TI1.PEriodID=PWD.PEriodID AND (TI1.StartTimeID=TI2.TimeIntervalId) ANd (TI1.ENDTimeID=TI3.TimeIntervalId) ")
            .Append(" And(CSM.ClsSectMeetingId ")
            .Append(" NOT IN (  SELECT        ClsSectMeetingId FROM  arClsSectMeetings WHERE	ClsSectionId = ? ")
            .Append("  UNION SELECT        ClsSectMeetingId  FROM  arClsSectMeetings WHERE	ClsSectionId IN ")
            .Append(" (select RightClsSectionId from arOverridenConflicts where LeftClsSectionId= ?)   )) AND (PWD.WorkDayId in (Select WorkDayID from ")
            .Append(" syPeriodsWorkDays where PeriodId=? ")
            If AltPeriodID.ToString <> Guid.Empty.ToString Then
                .Append("  union Select WorkDayID from syPeriodsWorkDays where AltPeriodId=?  ")
            End If
            ''Modified by Saraswathi lakshmanan on Sept 18 2009
            ''to fix mantis case 17593
            .Append(" ) ) AND(CSM.RoomId= ?) AND  (?<=CSM.EndDate AND ")
            .Append(" CSM.StartDate<=?) and")
            '.Append(" ) ) AND(CSM.RoomId= ?) AND  (?<=CS.EndDate AND ")
            '.Append(" CS.StartDate<=?) and")
            .Append("( ((Select TimeIntervalDescrip from cmTimeInterval CT, syPeriods SP where SP.StartTimeId=CT.TimeIntervalId and")
            .Append(" SP.PeriodID=?) <(Select TimeIntervalDescrip from CmTimeInterval where TimeINtervalId= TI3.TimeIntervalId) )")
            .Append(" and")
            .Append(" ((Select TimeIntervalDescrip from CmTimeInterval where TimeINtervalId= TI2.TimeIntervalId)<")
            .Append(" (Select TimeIntervalDescrip from cmTimeInterval CT, syPeriods SP where SP.EndTimeId=CT.TimeIntervalId and")
            .Append(" SP.PeriodID=?)) ")
            If AltPeriodID.ToString <> Guid.Empty.ToString Then
                .Append(" or ((Select TimeIntervalDescrip from cmTimeInterval CT, syPeriods SP where SP.StartTimeId=CT.TimeIntervalId and")
                .Append(" SP.PeriodID=?) <(Select TimeIntervalDescrip from CmTimeInterval where TimeINtervalId= TI3.TimeIntervalId) )")
                .Append(" and")
                .Append(" ((Select TimeIntervalDescrip from CmTimeInterval where TimeINtervalId= TI2.TimeIntervalId)<")
                .Append(" (Select TimeIntervalDescrip from cmTimeInterval CT, syPeriods SP where SP.EndTimeId=CT.TimeIntervalId and")
                .Append(" SP.PeriodID=?)) ")
            End If
            .Append(" ) ")



        End With

        ' Add workDaysId to the parameter list
        db.ClearParameters()

        ' Add ClsSectMeetingId to the parameter list
        db.AddParameter("@ClsSectMeetingId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add ClsSectMeetingId to the parameter list
        db.AddParameter("@ClsSectMeetingId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        ' Add WorkDays to the parameter list
        db.AddParameter("@PeriodId", PeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        If AltPeriodID.ToString <> Guid.Empty.ToString Then
            ' Add ClsSectMeetingId to the parameter list
            db.AddParameter("@AltperiodID", AltPeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        ' Add roomId to the parameter list
        db.AddParameter("@RoomId", roomId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add the startDate to the parameter list
        db.AddParameter("@StartDate", startDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add the endDate the parameter list
        db.AddParameter("@EndDate", endDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        db.AddParameter("@PeriodId", PeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PeriodId", PeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        If AltPeriodID.ToString <> Guid.Empty.ToString Then
            db.AddParameter("@AltperiodID", AltPeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AltperiodID", AltPeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        '   report error
        While dr.Read()

            'resultString.Append("Room collision on " + WorkDay + " from " + CType(dr("StartTime"), String) + " to " + CType(dr("EndTime"), String) + " with " + dr("Descrip") + "-" + dr("ClsSection") + "." + vbCrLf)
            ''resultString.Append("Room collision For " + dr("Descrip") + "-" + dr("ClsSection"))
            ''resultString.Append(" for period " + dr("PeriodDescrip"))
            ''resultString.Append("( " + Format(CType(dr("StartDate"), Date), "MM/dd/yy") + "-" + Format(CType(dr("EndDate"), Date), "MM/dd/yy") + ")." + vbCrLf)
            resultString.Append("Room collision in Term: " + dr("TermDescrip") + ", For Course Name (or Course code)- " + dr("Descrip") + "-" + dr("ClsSection"))
            resultString.Append(", For period " + dr("PeriodDescrip") + "." + vbCrLf)
        End While

        '   build the sql query to check for intructor collisions
        sb = New StringBuilder
        With sb
            .Append(" SELECT   Distinct     CS.ClsSection,  (Select TermDescrip from arterm where TermId=CS.TermID)TermDescrip  ,       R.Descrip,        CSM.PeriodID,TI1.PeriodDescrip, CSM.RoomID,StartTimeID,ENDTimeID,cs.EndDate,cs.StartDate    FROM     arClsSectMeetings CSM,   ")
            .Append(" arClassSections CS,        arReqs R,        SyPeriods TI1,SyPeriodsWorkDays PWD,      cmTimeInterval TI2,cmTimeInterval TI3 WHERE ")
            .Append(" (CSM.ClsSectionId = CS.ClsSectionId) AND    (CS.ReqId = R.ReqId) AND  (TI1.PeriodID=CSM.PeriodID) ")
            .Append(" and  TI1.PEriodID=PWD.PEriodID AND (TI1.StartTimeID=TI2.TimeIntervalId) ANd (TI1.ENDTimeID=TI3.TimeIntervalId)  ")
            .Append(" And(CSM.ClsSectMeetingId ")
            .Append(" NOT IN ( SELECT    ClsSectMeetingId FROM  arClsSectMeetings WHERE	ClsSectionId = ? UNION SELECT        ClsSectMeetingId  FROM  arClsSectMeetings WHERE	ClsSectionId IN ")
            .Append(" (select RightClsSectionId from arOverridenConflicts where LeftClsSectionId= ?)   )) AND (PWD.WorkDayId in (Select WorkDayID from ")
            .Append(" syPeriodsWorkDays where PeriodId=? ")
            If AltPeriodID.ToString <> Guid.Empty.ToString Then
                .Append("  union Select WorkDayID from syPeriodsWorkDays where AltPeriodId=?  ")
            End If

            .Append(" ) ) ")
            '.Append(" AND(CSM.RoomId= ?) ")
            If instructorId.ToString <> Guid.Empty.ToString Then
                .Append("AND	CS.InstructorId= ? ")
            End If
            ''Modified by Saraswathi lakshmanan on Sept 18 2009
            ''to fix mantis case 17593
            '.Append(" AND  (?<=CS.EndDate AND ")
            '.Append(" CS.StartDate<=?) and")
            .Append(" AND  (?<=CSM.EndDate AND ")
            .Append(" CSM.StartDate<=?) and")
            .Append("( ((Select TimeIntervalDescrip from cmTimeInterval CT, syPeriods SP where SP.StartTimeId=CT.TimeIntervalId and")
            .Append(" SP.PeriodID=?) <(Select TimeIntervalDescrip from CmTimeInterval where TimeINtervalId= TI3.TimeIntervalId) )")
            .Append(" and")
            .Append(" ((Select TimeIntervalDescrip from CmTimeInterval where TimeINtervalId= TI2.TimeIntervalId)<")
            .Append(" (Select TimeIntervalDescrip from cmTimeInterval CT, syPeriods SP where SP.EndTimeId=CT.TimeIntervalId and")
            .Append(" SP.PeriodID=?)) ")
            If AltPeriodID.ToString <> Guid.Empty.ToString Then
                .Append(" or ((Select TimeIntervalDescrip from cmTimeInterval CT, syPeriods SP where SP.StartTimeId=CT.TimeIntervalId and")
                .Append(" SP.PeriodID=?) <(Select TimeIntervalDescrip from CmTimeInterval where TimeINtervalId= TI3.TimeIntervalId) )")
                .Append(" and")
                .Append(" ((Select TimeIntervalDescrip from CmTimeInterval where TimeINtervalId= TI2.TimeIntervalId)<")
                .Append(" (Select TimeIntervalDescrip from cmTimeInterval CT, syPeriods SP where SP.EndTimeId=CT.TimeIntervalId and")
                .Append(" SP.PeriodID=?)) ")
            End If
            .Append(" ) ")

        End With

        ' Add workDaysId to the parameter list
        db.ClearParameters()



        ' Add ClsSectMeetingId to the parameter list
        db.AddParameter("@ClsSectMeetingId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add ClsSectMeetingId to the parameter list
        db.AddParameter("@ClsSectMeetingId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        ' Add WorkDays to the parameter list
        db.AddParameter("@PeriodId", PeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        If AltPeriodID.ToString <> Guid.Empty.ToString Then
            ' Add ClsSectMeetingId to the parameter list
            db.AddParameter("@AltperiodID", AltPeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        If instructorId.ToString <> Guid.Empty.ToString Then
            db.AddParameter("@InstructorID", instructorId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        ' Add the startDate to the parameter list
        db.AddParameter("@StartDate", startDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add the endDate the parameter list
        db.AddParameter("@EndDate", endDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        db.AddParameter("@PeriodId", PeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PeriodId", PeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        If AltPeriodID.ToString <> Guid.Empty.ToString Then
            db.AddParameter("@AltperiodID", AltPeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AltperiodID", AltPeriodID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        '   Execute the query
        dr = db.RunParamSQLDataReader(sb.ToString)


        '   report error
        While dr.Read()

            '  resultString.Append("Instructor collision on " + WorkDay + " from " + CType(dr("StartTime"), String) + " to " + CType(dr("EndTime"), String) + " with " + dr("Descrip") + "-" + dr("ClsSection") + "." + vbCrLf)
            ''resultString.Append("Instructor collision on period " + dr("PeriodDescrip") + " from ")
            ''resultString.Append("( " + Format(CType(dr("StartDate"), Date), "MM/dd/yy") + "-" + Format(CType(dr("EndDate"), Date), "MM/dd/yy") + ") ")

            ''resultString.Append(" with " + dr("Descrip") + "-" + dr("ClsSection") + vbCrLf)
            resultString.Append("Instructor/ Time collision in Term: " + dr("TermDescrip") + ", For Course Name (or Course code)- " + dr("Descrip") + "-" + dr("ClsSection") + ", For Period" + dr("PeriodDescrip") + "." + vbCrLf)
        End While

        If Not dr.IsClosed Then dr.Close()
        
        Return resultString.ToString

    End Function
#Region "Get AdvAppsetting for Manage Config entry"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
End Class
Public Class ReferentialIntegrityException
    Inherits System.Exception
    Sub New()
        MyBase.New()
    End Sub

    Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    Sub New(ByVal message As String, ByVal inner As System.Exception)
        MyBase.New(message, inner)
    End Sub
End Class
Public Class UniqueIndexException
    Inherits System.Exception
    Sub New()
        MyBase.New()
    End Sub

    Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    Sub New(ByVal message As String, ByVal inner As System.Exception)
        MyBase.New(message, inner)
    End Sub
End Class
Public Class PrimaryKeyConstraintException
    Inherits System.Exception
    Sub New()
        MyBase.New()
    End Sub

    Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    Sub New(ByVal message As String, ByVal inner As System.Exception)
        MyBase.New(message, inner)
    End Sub
End Class
