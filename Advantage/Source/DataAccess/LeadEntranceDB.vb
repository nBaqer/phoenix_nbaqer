Imports FAME.Advantage.Common

Public Class LeadEntranceDB
    Public Function GetControlCount(ByVal LeadId As String) As Integer
        '**************************************************************************************************
        'Purpose:       This Procedure Determines The Total Count OF Controls Based on The Controls Type
        '               The Controls belong to following three types - Range,List,None
        'Parameters:
        'Returns:       String
        'Notes:         This sub relies on the ResourceId and ModuleCode when
        '               when a request for the page is made.
        '               intRangeCount '- Holds the Total Count for Range Controls
        '               intListCount  '- Holds the Total Count for List Controls
        '               intNoneCount  '- Holds the Total Count for Controls with no validation
        '**************************************************************************************************
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim intTestRowCount As Integer
        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            With sb
                '.Append(" select Count(*) as TestCount from adPrgVerTestDetails where PrgVerTestId in ")
                '.Append(" (select PrgVerTestId from adPrgVerTest where PrgVerId in (select PrgVerId ")
                '.Append(" from adLeads where LeadId = ? )) and ")
                '.Append(" EntrTestId in (select EntrTestId from adLeadEntranceTest) ")
                .Append(" Select Count(*) as TestCount from adLeadEntranceTest where LeadId = ? ")
            End With
            'Execute the query
            db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            While dr.Read()
                intTestRowCount = dr("TestCount")
            End While
            dr.Close()
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            'db.CloseConnection()
            Return intTestRowCount
        Catch ex As System.Exception
        Finally
            db.CloseConnection()
        End Try
    End Function
    'Public Function GetArrayCount(ByVal LeadId As String) As Integer
    '    '**************************************************************************************************
    '    'Purpose:       This Procedure Determines The Total Count OF Controls Based on The Controls Type
    '    '               The Controls belong to following three types - Range,List,None
    '    'Parameters:
    '    'Returns:       String
    '    'Notes:         This sub relies on the ResourceId and ModuleCode when
    '    '               when a request for the page is made.
    '    '               intRangeCount '- Holds the Total Count for Range Controls
    '    '               intListCount  '- Holds the Total Count for List Controls
    '    '               intNoneCount  '- Holds the Total Count for Controls with no validation
    '    '**************************************************************************************************
    '    Dim db As New DataAccess
    '    Dim sb As New System.Text.StringBuilder
    '    Dim ds As New DataSet
    '    Dim intTestRowCount As Integer
    '    Dim strPreviousEducationId As String
    '    Try

    '        Dim MyAdvAppSettings As AdvAppSettings
    '        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '        Else
    '            MyAdvAppSettings = New AdvAppSettings
    '        End If


    '        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '        With sb
    '            .Append(" select PreviousEducation from adLeads where LeadId = ? ")
    '        End With
    '        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
    '        While dr.Read()
    '            If Not (dr("PreviousEducation") Is Guid.Empty.ToString) Then strPreviousEducationId = CType(dr("PreviousEducation"), Guid).ToString Else strPreviousEducationId = ""
    '        End While
    '        dr.Close()
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)



    '        With sb
    '            .Append(" select Count(*) as TestCount from adPrgVerTestDetails where PrgVerTestId in ")
    '            .Append(" (select PrgVerTestId from adPrgVerTest where PrgVerId in (select PrgVerId ")
    '            .Append(" from adLeads where LeadId = ? ) ")
    '            If Not strPreviousEducationId = "" Then
    '                .Append(" and PrevEduId=? ")
    '            End If
    '            .Append(" ) ")
    '        End With
    '        'Execute the query
    '        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        If Not strPreviousEducationId = "" Then
    '            db.AddParameter("@PrevEduId", strPreviousEducationId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If
    '        dr = db.RunParamSQLDataReader(sb.ToString)
    '        While dr.Read()
    '            intTestRowCount = dr("TestCount")
    '        End While
    '        dr.Close()
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)
    '        'db.CloseConnection()
    '        Return intTestRowCount
    '    Catch ex As System.Exception
    '    Finally
    '        db.CloseConnection()
    '    End Try
    'End Function
    Public Function GetAllFailedLeadNames(ByVal CampusId As String) As DataSet
        'connect to the database

        Dim ds As New DataSet
        Dim da6 As New OleDbDataAdapter

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            '.Append(" select Distinct t1.FirstName,t1.LastName,t1.LeadId from ")
            '.Append(" adLeads t1,adLeadEntranceTest t2 ")
            '.Append(" where t1.LeadId = t2.LeadId and t2.Required = 1 And t2.Pass = 0  and t1.CampusId=? ")
            '.Append(" select Distinct t2.LeadId,FirstName,MiddleName,LastName from adLeadEntranceTest t1,adLeads t2 where t1.LeadId = t2.LeadId and ")
            '.Append(" t1.Required=1 and t1.Pass=0 and t2.CampusId='" & CampusId & "'")
            '.Append(" union ")
            '.Append(" select  Distinct s3.LeadId,FirstName,MiddleName,LastName from adLeadDocsReceived s1,adReqLeadGroups s2,adLeads s3,syDocStatuses s4,sySysDocStatuses s5 ")
            '.Append(" where s1.DocumentId= s2.adReqId and s2.LeadgrpId = s3.LeadgrpId and s1.DocStatusId = s4.DocStatusId and ")
            '.Append(" s4.SysDocStatusId = s5.SysDocStatusId and s2.IsRequired=1 and s5.SysDocStatusId = 2  and s3.CampusId='" & CampusId & "'")
            '.Append(" union ")
            '.Append(" select Distinct A5.LeadId,FirstName,MiddleName,LastName ")
            '.Append(" from adReqs A1,adLeadDocsReceived A2,syDocStatuses A3,sySysDocStatuses A4,adLeads A5  ")
            '.Append(" where A1.adReqId = A2.DocumentId And A1.adReqTypeId = 3 And A1.AppliesToAll = 1 And A2.DocStatusId = A3.DocStatusId And A3.SysDocStatusId = A4.SysDocStatusId And A4.SysDocStatusId = 2 ")
            '.Append(" and A2.LeadId = A5.LeadId and A5.CampusId='" & CampusId & "'")
            '.Append(" union ")
            '.Append(" select Distinct A3.LeadId,firstname,Middlename,lastname ")
            '.Append(" from adReqs A1,adLeadEntranceTest A2,adLeads A3 ")
            '.Append(" where A1.adReqId = A2.EntrTestId And A1.adReqTypeId = 1 And A1.AppliesToAll = 1 And A2.Required = 1 ")
            '.Append(" and A2.Pass=0 and A2.LeadId = A3.LeadId and A3.CampusId='" & CampusId & "'")

            '.Append(" select t4.LeadId,t4.firstname,t4.Middlename,t4.LastName from ")
            '.Append(" syStatusCodes t1,sySysStatus t2,syStatusLevels t3,adLeads t4 ")
            '.Append(" where t1.SysStatusId = t2.SysStatusId And t2.StatusLevelId = t3.StatusLevelId ")
            '.Append(" and t2.SysStatusId <> 6 and t4.LeadStatus = t1.StatusCodeId and t4.CampusId=? ")
            '.Append(" order by t4.firstname ")


        End With
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorStudent")
        Finally
            db.CloseConnection()
        End Try
        Return ds
    End Function
    Public Function GetAllRequiredTestCount(ByVal LeadId As String) As Integer
        'connect to the database
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da6 As New OleDbDataAdapter

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append(" select Count(*) from adLeadEntranceTest where LeadId=? and Required=1 and Pass=0 ")
        End With
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.OpenConnection()
        Dim intReqCount As Integer = db.RunParamSQLScalar(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return intReqCount
    End Function
    Public Function GetAllRequiredRequirements(ByVal LeadId As String) As Integer
        'connect to the database
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da6 As New OleDbDataAdapter

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            '.Append(" select count(*) as RequiredTestCount from ( ")
            '.Append(" select LeadEntrTestId,EntrTestId from adLeadEntranceTest where LeadId= ? ")
            '.Append(" and Required=1 and Pass=0 ")
            '.Append(" union ")
            '.Append(" select Distinct t1.DocumentId,t1.DocStatusId from  ")
            '.Append(" adLeadDocsReceived t1,syDocStatuses t2,sySysDocStatuses t3 ")
            '.Append(" where t1.DocStatusId = t2.DocStatusId and t2.SysDocStatusId = t3.SysDocStatusId and  ")
            '.Append(" t3.SysDocStatusId = 2) R1 ")

            .Append(" select count(*) as RequiredTestCount from ( ")
            .Append(" select t1.EntrTestId,(select Descrip from adReqs where adReqId = t1.EntrTestId and adReqTypeId=1) as EntrTestDescrip,  ")
            .Append(" t1.MinScore,(Select MaxScore from adEntrTests where EntrTestId = t1.EntrTestId) as MaxScore,t1.Required, t1.TestTaken, t1.ActualScore, ")
            .Append(" (select OverRide from adEntrTestOverRide t2 where t2.LeadId='" & LeadId & "' and t2.EntrTestId=t1.EntrTestId) as OverRide ")
            .Append(" from adLeadEntranceTest t1 where t1.LeadId='" & LeadId & "' and t1.Required=1 and t1.Pass=0 ")
            .Append(" union ")
            ' Get List Of Pending documents that is required
            .Append(" select DocumentId,(select Descrip from adReqs where adReqId=DocumentId and adReqTypeId=3) as EntrTestDescrip, ")
            .Append(" NULL as MinScore,NULL as MaxScore,s2.IsRequired,NULL as TestTaken,NULL as ActualScore, ")
            .Append(" (select OverRide from adEntrTestOverRide t2 where  t2.LeadId='" & LeadId & "' and t2.EntrTestId=DocumentId) as OverRide ")
            .Append(" from adLeadDocsReceived s1,adReqLeadGroups s2,adLeads s3,syDocStatuses s4,sySysDocStatuses s5 ")
            .Append(" where s1.DocumentId= s2.adReqId and s2.LeadgrpId = s3.LeadgrpId and s1.DocStatusId = s4.DocStatusId and ")
            .Append(" s4.SysDocStatusId = s5.SysDocStatusId and s3.LeadId='" & LeadId & "' and s2.IsRequired=1 and s5.SysDocStatusId = 2 ")
            .Append(" union ")

            .Append("  select DocumentId,Descrip as EntrTestDescrip,NULL as MinScore,NULL as MaxScore,1 as Required,NULL as TestTaken,NULL as ActualScore, ")
            .Append(" (select OverRide from adEntrTestOverRide t2 where t2.LeadId='" & LeadId & "' and t2.EntrTestId=adReqId) as OverRide ")
            .Append(" from adReqs A1,adLeadDocsReceived A2,syDocStatuses A3,sySysDocStatuses A4 ")
            .Append(" where A1.adReqId = A2.DocumentId and A1.adReqTypeId = 3 and A1.AppliesToAll = 1 and A2.DocStatusId = A3.DocStatusId and A3.SysDocStatusId = A4.SysDocStatusId and A4.SysDocStatusId = 2 ")
            .Append(" and A2.LeadId='" & LeadId & "' union ")
            'Get list of tests that are mandatory and are pending
            .Append(" select EntrTestId,Descrip as EntrTestDescrip,NULL as MinScore,NULL as MaxScore,1 as Required,NULL as TestTaken,NULL as ActualScore, ")
            .Append(" (select OverRide from adEntrTestOverRide t2 where t2.LeadId='" & LeadId & "' and t2.EntrTestId=adReqId) as OverRide ")
            .Append(" from adReqs A1,adLeadEntranceTest A2 where A1.adReqId = A2.EntrTestId and A1.adReqTypeId = 1 and A1.AppliesToAll = 1 and A2.Required=1 ")
            .Append(" and A2.LeadId='" & LeadId & "' and A2.Pass=0 ")
            .Append(" ) R1 ")
        End With
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.OpenConnection()
        Dim intReqCount As Integer = db.RunParamSQLScalar(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return intReqCount
    End Function
    'Public Function GetControlCountAppend(ByVal LeadId As String) As Integer
    '    '**************************************************************************************************
    '    'Purpose:       This Procedure Determines The Total Count Of Tests Added After Lead Takes Up A Test
    '    'Parameters:
    '    'Returns:       String
    '    'Notes:         This sub relies on the ResourceId and ModuleCode when
    '    '               when a request for the page is made.
    '    '               intRangeCount '- Holds the Total Count for Range Controls
    '    '               intListCount  '- Holds the Total Count for List Controls
    '    '               intNoneCount  '- Holds the Total Count for Controls with no validation
    '    '**************************************************************************************************
    '    'Dim db As New DataAccess
    '    'Dim sb As New System.Text.StringBuilder
    '    'Dim ds As New DataSet
    '    'Dim intTestRowCount As Integer
    '    'Dim strPreviousEducationId As String
    '    'Try
    '    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

    '    '    'Check If Lead Has Previos Education
    '    '    'If the Lead has previous education then populate the
    '    '    'entrance test for the program version the lead is intrested in
    '    '    'and take in to account the previous education also
    '    '    With sb
    '    '        .Append(" select PreviousEducation from adLeads where LeadId = ? ")
    '    '    End With
    '    '    db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    '    Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
    '    '    While dr.Read()
    '    '        If Not (dr("PreviousEducation") Is Guid.Empty.ToString) Then strPreviousEducationId = CType(dr("PreviousEducation"), Guid).ToString Else strPreviousEducationId = ""
    '    '    End While
    '    '    dr.Close()
    '    '    db.ClearParameters()
    '    '    sb.Remove(0, sb.Length)

    '    '    With sb
    '    '        .Append(" select Count(*) as TestCount from adPrgVerTestDetails where PrgVerTestId in ")
    '    '        .Append(" (select PrgVerTestId from adPrgVerTest where PrgVerId in (select PrgVerId ")
    '    '        .Append(" from adLeads where LeadId = ? ")
    '    '        If Not strPreviousEducationId = "" Then
    '    '            .Append(" and PreviousEducation = ? ")
    '    '        End If
    '    '        .Append(" ) and PrevEduId=? ) and ")
    '    '        .Append(" EntrTestId not in (select EntrTestId from adLeadEntranceTest where LeadId=?) ")
    '    '    End With
    '    '    'Execute the query
    '    '    db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    '    If Not strPreviousEducationId = "" Then
    '    '        db.AddParameter("@PreviousEducation", strPreviousEducationId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    '        db.AddParameter("@PreviousEducation", strPreviousEducationId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    '    End If
    '    '    db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    '    dr = db.RunParamSQLDataReader(sb.ToString)
    '    '    While dr.Read()
    '    '        intTestRowCount = dr("TestCount")
    '    '    End While
    '    '    dr.Close()
    '    '    db.ClearParameters()
    '    '    sb.Remove(0, sb.Length)
    '    '    'db.CloseConnection()
    '    '    Return intTestRowCount
    '    'Catch ex As System.Exception
    '    'Finally
    '    '    db.CloseConnection()
    '    'End Try
    '    Dim db As New DataAccess
    '    Dim sb As New System.Text.StringBuilder
    '    Dim ds As New DataSet
    '    Dim strPreviousEducationId As String
    '    Dim strPrgVerId As String
    '    'Dim strPreviousEduTest As String
    '    Try
    '        'Check If Lead Has Previos Education
    '        'If the Lead has previous education then populate the
    '        'entrance test for the program version the lead is intrested in
    '        'and take in to account the previous education also
    '        With sb
    '            .Append(" select PreviousEducation,PrgVerId from adLeads where LeadId = ? ")
    '        End With
    '        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
    '        While dr.Read()
    '            If Not (dr("PreviousEducation") Is System.DBNull.Value) Then strPreviousEducationId = CType(dr("PreviousEducation"), Guid).ToString Else strPreviousEducationId = ""
    '            If Not (dr("PrgVerId") Is System.DBNull.Value) Then strPrgVerId = CType(dr("PrgVerId"), Guid).ToString Else strPrgVerId = ""
    '        End While
    '        dr.Close()
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)


    '        'If The Previous Education Is Not Blank
    '        'The Previous Education Value is hard coded to  F05E3B9B-7734-4711-A3BF-21A933384F1D as that's the 
    '        'default value if Any is selected in Program Version Entrance Test
    '        If Not strPrgVerId = "" And Not strPreviousEducationId = "" Then
    '            With sb
    '                .Append(" select Count(*) as CountEntranceTest from ")
    '                .Append(" ( select t1.EntrTestId, (select EntrTestDescrip from adEntrTests where EntrTestId = t1.EntrTestId) as EntrTestDescrip, ")
    '                .Append(" t1.MinScore,(Select MaxScore from adEntrTests where EntrTestId = t1.EntrTestId) as MaxScore, ")
    '                .Append(" t1.Required from adPrgVerTestDetails t1,adPrgVerTest t2 ")
    '                .Append(" where t1.PrgVerTestId = t2.PrgVerTestId and t2.PrevEduId = ? ")
    '                .Append(" and t2.PrgVerId = ? ")
    '                .Append(" union ")
    '                .Append(" select t1.EntrTestId, (select EntrTestDescrip from adEntrTests where EntrTestId = t1.EntrTestId) as EntrTestDescrip, ")
    '                .Append(" t1.MinScore,(Select MaxScore from adEntrTests where EntrTestId = t1.EntrTestId) as MaxScore, ")
    '                .Append(" t1.Required from adPrgVerTestDetails t1,adPrgVerTest t2 ")
    '                .Append(" where t1.PrgVerTestId = t2.PrgVerTestId and t2.PrevEduId Is NULL ")
    '                .Append(" and t2.PrgVerId = ? ) CET ")
    '            End With

    '            db.AddParameter("@PrevEduId", strPreviousEducationId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@PrgVerId", strPrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@PrgVerId", strPrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '            Dim intCount As Integer = db.RunParamSQLScalar(sb.ToString)
    '            Return intCount
    '        End If

    '        If Not strPrgVerId = "" And strPreviousEducationId = "" Then
    '            With sb
    '                .Append(" select Count(*) as CountEntranceTest from ")
    '                .Append(" ( select t1.EntrTestId, (select EntrTestDescrip from adEntrTests where EntrTestId = t1.EntrTestId) as EntrTestDescrip, ")
    '                .Append(" t1.MinScore,(Select MaxScore from adEntrTests where EntrTestId = t1.EntrTestId) as MaxScore, ")
    '                .Append(" t1.Required from adPrgVerTestDetails t1,adPrgVerTest t2 ")
    '                .Append(" where t1.PrgVerTestId = t2.PrgVerTestId And t2.PrevEduId Is NULL ")
    '                .Append(" and t2.PrgVerId = ?) CET ")
    '                '.Append(" union ")
    '                '.Append(" select t1.EntrTestId, (select EntrTestDescrip from adEntrTests where EntrTestId = t1.EntrTestId) as EntrTestDescrip, ")
    '                '.Append(" t1.MinScore,(Select MaxScore from adEntrTests where EntrTestId = t1.EntrTestId) as MaxScore, ")
    '                '.Append(" t1.Required from adPrgVerTestDetails t1,adPrgVerTest t2 ")
    '                '.Append(" where t1.PrgVerTestId = t2.PrgVerTestId and t2.PrevEduId = 'F05E3B9B-7734-4711-A3BF-21A933384F1D' ")
    '                '.Append(" and t2.PrgVerId = ? ) CET ")
    '            End With
    '            db.AddParameter("@PrgVerId", strPrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            'db.AddParameter("@PrgVerId", strPrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '            Dim intCount As Integer = db.RunParamSQLScalar(sb.ToString)
    '            Return intCount
    '        End If



    '        'If strPrgVerId <> "" Then
    '        '    With sb
    '        '        .Append(" select PrevEduId from adPrgVerTest where PrgVerId = ? ")
    '        '    End With
    '        '    db.AddParameter("@PrgVerId", strPrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        '    dr = db.RunParamSQLDataReader(sb.ToString)
    '        '    While dr.Read()
    '        '        If Not (dr("PrevEduId") Is System.DBNull.Value) Then strPreviousEduTest = CType(dr("PrevEduId"), Guid).ToString Else strPreviousEduTest = ""
    '        '    End While
    '        '    dr.Close()
    '        '    db.ClearParameters()
    '        '    sb.Remove(0, sb.Length)
    '        'Else
    '        '    strPreviousEduTest = ""
    '        'End If


    '        'With sb
    '        '    .Append(" select t1.EntrTestId, (select EntrTestDescrip from adEntrTests where EntrTestId = t1.EntrTestId) as EntrTestDescrip, ")
    '        '    .Append(" t1.MinScore,(Select MaxScore from adEntrTests where EntrTestId = t1.EntrTestId) as MaxScore, ")
    '        '    .Append(" t1.Required from adPrgVerTestDetails t1,adPrgVerTest t2,adLeads t3 where ")
    '        '    .Append(" t1.PrgVerTestId = t2.PrgVerTestId and t2.PrgVerId = t3.PrgVerId and ")
    '        '    If Not strPreviousEducationId = "" And Not strPreviousEduTest = "" Then
    '        '        .Append(" t2.PrevEduId = t3.PreviousEducation and ")
    '        '    End If
    '        '    .Append(" t3.LeadId = ?   ")
    '        'End With

    '        ''   build select command
    '        'Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(SingletonAppSettings.AppSettings("ConString")))
    '        'sc.Parameters.Add(New OleDbParameter("@LeadId", LeadId))

    '        'Dim da As New OleDbDataAdapter(sc)
    '        'da.Fill(ds, "TestDetails")
    '        'sb.Remove(0, sb.Length)
    '        'Return ds
    '        'db.CloseConnection()
    '    Catch ex As System.Exception
    '    Finally
    '        'db.CloseConnection()
    '    End Try
    'End Function
    Public Function GetLeadTestCount(ByVal LeadId As String, ByVal PrgVerId As String) As Integer
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPrgVerId As String
        'Dim strPreviousEduTest As String
        Dim intTestCount As Integer
        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            With sb
                'Put All The Entrance Test In Derived Table and Get The Count
                .Append(" select Count(*) as TestCount from ")
                .Append(" (select t1.adReqId as EntrTestId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,t2.Descrip, ")
                .Append(" t3.IsRequired as Required ")
                .Append(" from adPrgVerTestDetails t1,adReqs t2,adReqLeadGroups t3 ")
                .Append(" where t1.adReqId = t2.adReqId and t2.adReqId = t3.adReqId and t1.PrgVerId='" & PrgVerId & "' ")
                .Append(" and t2.adReqTypeId =1 ")
                .Append(" and t1.ReqGrpId is null and t3.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
                .Append(" union ")
                'Get All Requirements That Are Part Of Group and Get The Requirements Based on Lead Group Level
                .Append(" select t5.adReqId as EntrTestId,t4.ReqGrpId,t5.Descrip,t6.IsRequired as Required ")
                .Append(" from  adReqGrpDef t4,adReqs t5,adReqLeadGroups t6 ")
                .Append(" where t4.adReqId = t5.adReqId and t5.adReqId = t6.adReqId and t6.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
                .Append(" and t5.adReqTypeId =1 ")
                .Append(" and ReqGrpId in (select t1.ReqGrpId from adPrgVerTestDetails t1 where t1.PrgVerId='" & PrgVerId & "' and ReqGrpId is not null) ")
                .Append(" union ")
                'Get all test that are not part of requirement group or not assigned to a program version
                'but assigned to lead group
                .Append(" select Distinct B1.adReqId as EntrTestId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,B1.Descrip,  ")
                .Append(" B2.IsRequired as Required  from adReqs B1,adReqLeadGroups B2 where B1.adReqId = B2.adReqId ")
                .Append(" and B2.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
                .Append(" and B1.adReqTypeId=1 and B1.adReqId not in ")
                .Append(" (select adReqId from adPrgVerTestDetails where PrgVerId='" & PrgVerId & "' and adReqId is not null) ")
                .Append(" and B1.adReqId not in ")
                .Append(" (select Distinct adReqId from adReqGrpDef where ReqGrpId in (select ReqGrpId from adPrgVerTestDetails  ")
                .Append(" where PrgVerId='" & PrgVerId & "' and adReqId is NULL)) ")

                'Modified by Balaji on 08/10/2005 to address all Mandatory Requirements
                .Append(" union ")
                .Append(" select adReqId as EntrTestId,Null,Descrip,1 as Required from adReqs where adReqTypeId=1 and AppliesToAll=1 ")
                .Append(" )  ")
                .Append(" adGetAllTestByLeadAndPrgVersion ")
            End With


            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            While dr.Read()
                intTestCount = dr("TestCount")
            End While
            dr.Close()
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            'db.CloseConnection()
            Return intTestCount
        Catch ex As Exception
            Return 0
        Finally
        End Try
    End Function
    Public Function GetAllStandardLeadTestCount(ByVal LeadId As String) As Integer
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPrgVerId As String
        'Dim strPreviousEduTest As String
        Dim intTestCount As Integer
        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            With sb
                'Put All The Entrance Test In Derived Table and Get The Count
                .Append(" select Count(*) as TestCount from ")
                .Append(" (select Distinct adReqId as EntrTestId,Descrip as EntrTestDescrip,1 as Required, ")
                .Append(" (select ActualScore from adLeadEntranceTest where EntrTestId=adReqId and LeadId='" & LeadId & "') as ActualScore, ")
                .Append(" MinScore ")
                .Append(" from adReqs where AppliesToAll=1 and adReqTypeId=1 ")
                .Append(" union ")
                .Append(" select Distinct t1.adReqId as EntrTestId,t1.Descrip as EntrTestDescrip,t2.IsRequired as Required, ")
                .Append(" (select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId='" & LeadId & "') as ActualScore, ")
                .Append(" t1.MinScore ")
                .Append(" from adReqs t1,adReqLeadGroups t2,adLeads t3 ")
                .Append(" where t1.adReqId = t2.adReqId And t2.LeadGrpId = t3.LeadGrpId ")
                .Append(" and t3.LeadId='" & LeadId & "' and t1.adReqTypeId=1 ")
                .Append(" and t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) ")
                .Append(" and t1.adReqId not in (select Distinct adReqId from adReqGrpDef where ReqGrpId in (select ReqGrpId from adPrgVerTestDetails ")
                .Append(" where adReqId is NULL)) ")
                .Append(" ) ")
                .Append(" adGetAllTestByLeadAndPrgVersion ")
            End With


            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            While dr.Read()
                intTestCount = dr("TestCount")
            End While
            dr.Close()
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            'db.CloseConnection()
            Return intTestCount
        Catch ex As Exception
            Return 0
        Finally
        End Try
    End Function
    Public Function GetTestCountByLead(ByVal LeadId As String) As Integer
        '**************************************************************************************************
        'Purpose:       This Procedure Determines The Total Count OF Controls Based on The Controls Type
        '               The Controls belong to following three types - Range,List,None
        'Parameters:
        'Returns:       String
        'Notes:         This sub relies on the ResourceId and ModuleCode when
        '               when a request for the page is made.
        '               intRangeCount '- Holds the Total Count for Range Controls
        '               intListCount  '- Holds the Total Count for List Controls
        '               intNoneCount  '- Holds the Total Count for Controls with no validation
        '**************************************************************************************************
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim intLeadTestCount As Integer
        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            With sb
                .Append(" select Count(*) as LeadCount from adLeadEntranceTest where LeadId = ? ")
            End With
            'Execute the query
            db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            While dr.Read()
                intLeadTestCount = dr("LeadCount")
            End While
            dr.Close()
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            'db.CloseConnection()
            Return intLeadTestCount
        Catch ex As System.Exception
        Finally
            db.CloseConnection()
        End Try
    End Function
    'Public Function GetTestDetails(ByVal LeadId As String) As DataSet
    '    Dim db As New DataAccess
    '    Dim sb As New System.Text.StringBuilder
    '    Dim ds As New DataSet
    '    Dim strPreviousEducationId As String = ""
    '    Dim strPrgVerId As String = ""
    '    'Dim strPreviousEduTest As String

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If


    '    Try
    '        'Check If Lead Has Previos Education
    '        'If the Lead has previous education then populate the
    '        'entrance test for the program version the lead is intrested in
    '        'and take in to account the previous education also
    '        With sb
    '            .Append(" select PreviousEducation,PrgVerId from adLeads where LeadId = ? ")
    '        End With
    '        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
    '        While dr.Read()
    '            If Not (dr("PreviousEducation") Is System.DBNull.Value) Then strPreviousEducationId = CType(dr("PreviousEducation"), Guid).ToString Else strPreviousEducationId = ""
    '            If Not (dr("PrgVerId") Is System.DBNull.Value) Then strPrgVerId = CType(dr("PrgVerId"), Guid).ToString Else strPrgVerId = ""
    '        End While
    '        dr.Close()
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)


    '        'If The Previous Education Is Not Blank
    '        'The Previous Education Value is hard coded to  F05E3B9B-7734-4711-A3BF-21A933384F1D as that's the 
    '        'default value if Any is selected in Program Version Entrance Test
    '        If Not strPrgVerId = "" And Not strPreviousEducationId = "" Then
    '            With sb
    '                .Append(" select t1.EntrTestId, (select EntrTestDescrip from adEntrTests where EntrTestId = t1.EntrTestId) as EntrTestDescrip, ")
    '                .Append(" t1.MinScore,(Select MaxScore from adEntrTests where EntrTestId = t1.EntrTestId) as MaxScore, ")
    '                .Append(" t1.Required from adPrgVerTestDetails t1,adPrgVerTest t2 ")
    '                .Append(" where t1.PrgVerTestId = t2.PrgVerTestId and t2.PrevEduId = ? ")
    '                .Append(" and t2.PrgVerId = ? ")
    '                .Append(" union ")
    '                .Append(" select t1.EntrTestId, (select EntrTestDescrip from adEntrTests where EntrTestId = t1.EntrTestId) as EntrTestDescrip, ")
    '                .Append(" t1.MinScore,(Select MaxScore from adEntrTests where EntrTestId = t1.EntrTestId) as MaxScore, ")
    '                .Append(" t1.Required from adPrgVerTestDetails t1,adPrgVerTest t2 ")
    '                .Append(" where t1.PrgVerTestId = t2.PrgVerTestId and t2.PrevEduId Is NULL ")
    '                .Append(" and t2.PrgVerId = ? ")
    '            End With
    '            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
    '            sc1.Parameters.Add(New OleDbParameter("@PrevEduId", strPreviousEducationId))
    '            sc1.Parameters.Add(New OleDbParameter("@PrgVerId", strPrgVerId))
    '            sc1.Parameters.Add(New OleDbParameter("@PrgVerId", strPrgVerId))

    '            Dim da1 As New OleDbDataAdapter(sc1)
    '            da1.Fill(ds, "TestDetails")
    '            sb.Remove(0, sb.Length)
    '            Return ds
    '        End If

    '        If Not strPrgVerId = "" And strPreviousEducationId = "" Then
    '            With sb
    '                .Append(" select t1.EntrTestId, (select EntrTestDescrip from adEntrTests where EntrTestId = t1.EntrTestId) as EntrTestDescrip, ")
    '                .Append(" t1.MinScore,(Select MaxScore from adEntrTests where EntrTestId = t1.EntrTestId) as MaxScore, ")
    '                .Append(" t1.Required from adPrgVerTestDetails t1,adPrgVerTest t2 ")
    '                .Append(" where t1.PrgVerTestId = t2.PrgVerTestId And t2.PrevEduId Is NULL ")
    '                .Append(" and t2.PrgVerId = ? ")
    '                '.Append(" union ")
    '                '.Append(" select t1.EntrTestId, (select EntrTestDescrip from adEntrTests where EntrTestId = t1.EntrTestId) as EntrTestDescrip, ")
    '                '.Append(" t1.MinScore,(Select MaxScore from adEntrTests where EntrTestId = t1.EntrTestId) as MaxScore, ")
    '                '.Append(" t1.Required from adPrgVerTestDetails t1,adPrgVerTest t2 ")
    '                '.Append(" where t1.PrgVerTestId = t2.PrgVerTestId and t2.PrevEduId = 'F05E3B9B-7734-4711-A3BF-21A933384F1D' ")
    '                '.Append(" and t2.PrgVerId = ? ")
    '            End With
    '            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
    '            sc1.Parameters.Add(New OleDbParameter("@PrgVerId", strPrgVerId))
    '            'sc1.Parameters.Add(New OleDbParameter("@PrgVerId", strPrgVerId))

    '            Dim da1 As New OleDbDataAdapter(sc1)
    '            da1.Fill(ds, "TestDetails")
    '            sb.Remove(0, sb.Length)
    '            Return ds
    '        End If



    '        'If strPrgVerId <> "" Then
    '        '    With sb
    '        '        .Append(" select PrevEduId from adPrgVerTest where PrgVerId = ? ")
    '        '    End With
    '        '    db.AddParameter("@PrgVerId", strPrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        '    dr = db.RunParamSQLDataReader(sb.ToString)
    '        '    While dr.Read()
    '        '        If Not (dr("PrevEduId") Is System.DBNull.Value) Then strPreviousEduTest = CType(dr("PrevEduId"), Guid).ToString Else strPreviousEduTest = ""
    '        '    End While
    '        '    dr.Close()
    '        '    db.ClearParameters()
    '        '    sb.Remove(0, sb.Length)
    '        'Else
    '        '    strPreviousEduTest = ""
    '        'End If


    '        'With sb
    '        '    .Append(" select t1.EntrTestId, (select EntrTestDescrip from adEntrTests where EntrTestId = t1.EntrTestId) as EntrTestDescrip, ")
    '        '    .Append(" t1.MinScore,(Select MaxScore from adEntrTests where EntrTestId = t1.EntrTestId) as MaxScore, ")
    '        '    .Append(" t1.Required from adPrgVerTestDetails t1,adPrgVerTest t2,adLeads t3 where ")
    '        '    .Append(" t1.PrgVerTestId = t2.PrgVerTestId and t2.PrgVerId = t3.PrgVerId and ")
    '        '    If Not strPreviousEducationId = "" And Not strPreviousEduTest = "" Then
    '        '        .Append(" t2.PrevEduId = t3.PreviousEducation and ")
    '        '    End If
    '        '    .Append(" t3.LeadId = ?   ")
    '        'End With

    '        ''   build select command
    '        'Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(SingletonAppSettings.AppSettings("ConString")))
    '        'sc.Parameters.Add(New OleDbParameter("@LeadId", LeadId))

    '        'Dim da As New OleDbDataAdapter(sc)
    '        'da.Fill(ds, "TestDetails")
    '        'sb.Remove(0, sb.Length)
    '        'Return ds
    '        'db.CloseConnection()
    '    Catch ex As System.Exception
    '    Finally
    '        'db.CloseConnection()
    '    End Try
    'End Function
    Public Function GetLeadTestAndScores(ByVal LeadId As String, ByVal PrgVerId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Try
            With sb
                .Append(" select t1.adReqId as EntrTestId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,t2.Descrip as EntrTestDescrip, ")
                .Append(" t3.IsRequired as Required, ")
                .Append(" (select TestTaken from adLeadEntranceTest where EntrTestId = t1.adReqId and LeadId='" & LeadId & "') as TestTaken, ")
                .Append(" (select ActualScore from adLeadEntranceTest where EntrTestId = t1.adReqId and LeadId='" & LeadId & "') as ActualScore, ")
                .Append(" (select Pass from adLeadEntranceTest where EntrTestId = t1.adReqId and LeadId='" & LeadId & "') as Pass, ")
                .Append(" (select Comments from adLeadEntranceTest where EntrTestId = t1.adReqId and LeadId='" & LeadId & "') as Comments,t2.MinScore ")
                .Append(" from adPrgVerTestDetails t1,adReqs t2,adReqLeadGroups t3 ")
                .Append(" where t1.adReqId = t2.adReqId and t2.adReqId = t3.adReqId and t1.PrgVerId='" & PrgVerId & "' ")
                .Append(" and t2.adReqTypeId =1 ")
                .Append(" and t1.ReqGrpId is null and t3.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
                .Append(" union ")
                'Get All Requirements That Are Part Of Group and Get The Requirements Based on Lead Group Level
                .Append(" select t5.adReqId as EntrTestId,t4.ReqGrpId,t5.Descrip as EntrTestDescrip,t6.IsRequired as Required, ")
                .Append(" (select TestTaken from adLeadEntranceTest where EntrTestId = t5.adReqId and LeadId='" & LeadId & "') as TestTaken, ")
                .Append(" (select ActualScore from adLeadEntranceTest where EntrTestId = t5.adReqId and LeadId='" & LeadId & "') as ActualScore, ")
                .Append(" (select Pass from adLeadEntranceTest where EntrTestId = t5.adReqId and LeadId='" & LeadId & "') as Pass, ")
                .Append(" (select Comments from adLeadEntranceTest where EntrTestId = t5.adReqId and LeadId='" & LeadId & "') as Comments,t5.Minscore ")
                .Append(" from  adReqGrpDef t4,adReqs t5,adReqLeadGroups t6 ")
                .Append(" where t4.adReqId = t5.adReqId and t5.adReqId = t6.adReqId and t6.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
                .Append(" and t5.adReqTypeId =1 ")
                .Append(" and ReqGrpId in (select t1.ReqGrpId from adPrgVerTestDetails t1 where t1.PrgVerId='" & PrgVerId & "' and ReqGrpId is not null) ")
                .Append(" union ")
                'Get all requirements assigned to lead group but not assigned to program version and not 
                'part of group
                .Append(" select Distinct B1.adReqId as EntrTestId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,B1.Descrip,  ")
                .Append(" B2.IsRequired as Required,(select TestTaken from adLeadEntranceTest where EntrTestId = B1.adReqId and LeadId='" & LeadId & "') as TestTaken, ")
                .Append(" (select ActualScore from adLeadEntranceTest where EntrTestId = B1.adReqId and LeadId='" & LeadId & "') as ActualScore, ")
                .Append(" (select Pass from adLeadEntranceTest where EntrTestId = B1.adReqId and LeadId='" & LeadId & "') as Pass, ")
                .Append(" (select Comments from adLeadEntranceTest where EntrTestId = B1.adReqId and LeadId='" & LeadId & "') as Comments, ")
                .Append(" B1.MinScore ")
                .Append(" from adReqs B1,adReqLeadGroups B2 where B1.adReqId = B2.adReqId  ")
                .Append(" and B2.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
                .Append(" and B1.adReqTypeId=1 and B1.adReqId not in ")
                .Append(" (select adReqId from adPrgVerTestDetails where PrgVerId='" & PrgVerId & "'  and adReqId is not null) ")
                .Append(" and B1.adReqId not in  (select Distinct adReqId from adReqGrpDef where ReqGrpId in (select ReqGrpId from adPrgVerTestDetails ")
                .Append(" where PrgVerId='" & PrgVerId & "' and adReqId is NULL))  ")

                .Append(" union ")
                'Get All Requirements That are mandatory
                .Append(" select adReqId as EntrTestId,NULL as RepGrpId,Descrip as EntrTestDescrip,1 as Required, ")
                .Append(" (select TestTaken from adLeadEntranceTest where EntrTestId = adReqId and LeadId='" & LeadId & "') as TestTaken, ")
                .Append(" (select ActualScore from adLeadEntranceTest where EntrTestId = adReqId and LeadId='" & LeadId & "') as ActualScore, ")
                .Append(" (select Pass from adLeadEntranceTest where EntrTestId = adReqId and LeadId='" & LeadId & "') as Pass, ")
                .Append(" (select Comments from adLeadEntranceTest where EntrTestId = adReqId and LeadId='" & LeadId & "') as Comments,MinScore ")
                .Append(" from adReqs where adReqTypeId=1 and AppliesToAll=1 ")
                .Append(" order by Descrip ")
            End With

            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As System.Exception
        Finally
            'db.CloseConnection()
        End Try
        Return Nothing
    End Function
    Public Function GetStandardLeadTestAndScores(ByVal LeadId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Try
            With sb

                .Append(" select Distinct adReqId as EntrTestId,Descrip as EntrTestDescrip,1 as Required, ")
                .Append(" (select TestTaken from adLeadEntranceTest where EntrTestId = adReqId and LeadId='" & LeadId & "') as TestTaken, ")
                .Append(" (select ActualScore from adLeadEntranceTest where EntrTestId = adReqId and LeadId='" & LeadId & "') as ActualScore, ")
                .Append(" (select Pass from adLeadEntranceTest where EntrTestId = adReqId and LeadId='" & LeadId & "') as Pass, ")
                .Append(" (select Comments from adLeadEntranceTest where EntrTestId = adReqId and LeadId='" & LeadId & "') as Comments,MinScore ")
                .Append(" from adReqs where AppliesToAll=1 and adReqTypeId=1 ")
                .Append(" union ")
                .Append(" select Distinct t1.adReqId as EntrTestId,t1.Descrip as EntrTestDescrip,t2.IsRequired as Required, ")
                .Append(" (select TestTaken from adLeadEntranceTest where EntrTestId = t1.adReqId and LeadId='" & LeadId & "') as TestTaken, ")
                .Append(" (select ActualScore from adLeadEntranceTest where EntrTestId = t1.adReqId and LeadId='" & LeadId & "') as ActualScore, ")
                .Append(" (select Pass from adLeadEntranceTest where EntrTestId = t1.adReqId and LeadId='" & LeadId & "') as Pass, ")
                .Append(" (select Comments from adLeadEntranceTest where EntrTestId = t1.adReqId and LeadId='" & LeadId & "') as Comments, ")
                .Append(" t1.MinScore ")
                .Append(" from adReqs t1,adReqLeadGroups t2,adLeads t3 ")
                .Append(" where t1.adReqId = t2.adReqId And t2.LeadGrpId = t3.LeadGrpId ")
                .Append(" and t3.LeadId='" & LeadId & "' and t1.adReqTypeId=1 ")
                .Append(" and t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) ")
                .Append(" and t1.adReqId not in (select Distinct adReqId from adReqGrpDef where ReqGrpId in (select ReqGrpId from adPrgVerTestDetails ")
                .Append(" where adReqId is NULL)) ")
                .Append(" order by Descrip ")
            End With

            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As System.Exception
        Finally
            'db.CloseConnection()
        End Try
        Return Nothing
    End Function
    Public Function GetRequirementDetails(ByVal LeadId As String, ByVal PrgVerId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Try


            With sb
                .Append(" select t1.adReqId as EntrTestId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,t2.Descrip as EntrTestDescrip, ")
                .Append(" t3.IsRequired as Required, ")
                .Append(" (select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId='" & LeadId & "') as ActualScore, ")
                .Append(" t2.MinScore ")
                .Append(" from adPrgVerTestDetails t1,adReqs t2,adReqLeadGroups t3 ")
                .Append(" where t1.adReqId = t2.adReqId and t2.adReqId = t3.adReqId and t1.PrgVerId='" & PrgVerId & "' ")
                .Append(" and t2.adReqTypeId =1 ")
                .Append(" and t1.ReqGrpId is null and t3.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
                .Append(" union ")
                'Get All Requirements That Are Part Of Group and Get The Requirements Based on Lead Group Level
                .Append(" select t5.adReqId as EntrTestId,t4.ReqGrpId,t5.Descrip as EntrTestDescrip,t6.IsRequired as Required, ")
                .Append(" (select ActualScore from adLeadENtranceTest where EntrTestId=t5.adReqId and LeadId='" & LeadId & "') as ActualScore, ")
                .Append(" t5.MinScore ")
                .Append(" from  adReqGrpDef t4,adReqs t5,adReqLeadGroups t6 ")
                .Append(" where t4.adReqId = t5.adReqId and t5.adReqId = t6.adReqId and t6.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
                .Append(" and t5.adReqTypeId =1 ")
                .Append(" and ReqGrpId in (select t1.ReqGrpId from adPrgVerTestDetails t1 where t1.PrgVerId='" & PrgVerId & "' and ReqGrpId is not null) ")
                'Get all requirements that are not part of any requirement group or not assigned to program version
                'but assigned to lead group
                .Append(" union ")
                .Append(" select Distinct B1.adReqId as EntrTestId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,B1.Descrip, ")
                .Append(" B2.IsRequired as Required,(select ActualScore from adLeadENtranceTest where EntrTestId=B1.adReqId and ")
                .Append(" LeadId='" & LeadId & "') as ActualScore, ")
                .Append(" B1.MinScore from adReqs B1,adReqLeadGroups B2 where B1.adReqId = B2.adReqId ")
                .Append(" and B2.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
                .Append(" and B1.adReqTypeId=1 and B1.adReqId not in ")
                .Append(" (select adReqId from adPrgVerTestDetails where PrgVerId='" & PrgVerId & "' and adReqId is not null) ")
                .Append(" and B1.adReqId not in  ")
                .Append(" (select Distinct adReqId from adReqGrpDef where ReqGrpId in (select ReqGrpId from adPrgVerTestDetails ")
                .Append(" where PrgVerId='" & PrgVerId & "' and adReqId is NULL)) ")
                'Get all mandatory requirements
                .Append(" union ")
                .Append(" select adReqId as EntrTestId,Null as ReqGrpId,Descrip as EntrTestDescrip,1 as Required, ")
                .Append(" (select ActualScore from adLeadEntranceTest where EntrTestId=adReqId and LeadId='" & LeadId & "') as ActualScore, ")
                .Append(" MinScore from adReqs where adReqTypeId=1 and AppliesToAll=1 ")
                .Append(" order by Descrip ")
            End With

            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As System.Exception
        Finally
            'db.CloseConnection()
        End Try
        Return Nothing
    End Function
    Public Function GetGridRequirementDetails(ByVal LeadId As String, ByVal PrgVerId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Try


            With sb

                .Append(" select EntrTestId,EntrTestDescrip,Required,ActualScore,MinScore,TestTaken,Comments, ")
                .Append(" Case when OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' end as Pass from ")
                .Append(" ( ")
                .Append(" select t1.adReqId as EntrTestId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,t2.Descrip as EntrTestDescrip, ")
                .Append(" t3.IsRequired as Required, ")
                .Append(" (select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId='" & LeadId & "') as ActualScore, ")
                .Append(" (select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId='" & LeadId & "') as TestTaken, ")
                .Append("       (select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("       LeadId='" & LeadId & "') as Comments, ")
                .Append("       (select OverRide from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("       LeadId='" & LeadId & "') as OverRide, ")
                .Append(" t2.MinScore ")
                .Append(" from adPrgVerTestDetails t1,adReqs t2,adReqLeadGroups t3 ")
                .Append(" where t1.adReqId = t2.adReqId and t2.adReqId = t3.adReqId and t1.PrgVerId='" & PrgVerId & "' ")
                .Append(" and t2.adReqTypeId =1 ")
                .Append(" and t1.ReqGrpId is null and t3.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
                .Append(" union ")
                'Get All Requirements That Are Part Of Group and Get The Requirements Based on Lead Group Level
                .Append(" select t5.adReqId as EntrTestId,t4.ReqGrpId,t5.Descrip as EntrTestDescrip,t6.IsRequired as Required, ")
                .Append(" (select ActualScore from adLeadENtranceTest where EntrTestId=t5.adReqId and LeadId='" & LeadId & "') as ActualScore, ")
                .Append(" (select TestTaken from adLeadEntranceTest where EntrTestId=t5.adReqId and LeadId='" & LeadId & "') as TestTaken, ")
                .Append("       (select Comments from adLeadEntranceTest where EntrTestId=t5.adReqId and ")
                .Append("       LeadId='" & LeadId & "') as Comments, ")
                .Append("       (select OverRide from adLeadEntranceTest where EntrTestId=t5.adReqId and ")
                .Append("       LeadId='" & LeadId & "') as OverRide, ")
                .Append(" t5.MinScore ")
                .Append(" from  adReqGrpDef t4,adReqs t5,adReqLeadGroups t6 ")
                .Append(" where t4.adReqId = t5.adReqId and t5.adReqId = t6.adReqId and t6.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
                .Append(" and t5.adReqTypeId =1 ")
                .Append(" and ReqGrpId in (select t1.ReqGrpId from adPrgVerTestDetails t1 where t1.PrgVerId='" & PrgVerId & "' and ReqGrpId is not null) ")
                'Get all requirements that are not part of any requirement group or not assigned to program version
                'but assigned to lead group
                .Append(" union ")
                .Append(" select Distinct B1.adReqId as EntrTestId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,B1.Descrip, ")
                .Append(" B2.IsRequired as Required,(select ActualScore from adLeadENtranceTest where EntrTestId=B1.adReqId and ")
                .Append(" LeadId='" & LeadId & "') as ActualScore, ")
                .Append(" (select TestTaken from adLeadEntranceTest where EntrTestId=B1.adReqId and LeadId='" & LeadId & "') as TestTaken, ")
                .Append("       (select Comments from adLeadEntranceTest where EntrTestId=b1.adReqId and ")
                .Append("       LeadId='" & LeadId & "') as Comments, ")
                .Append("       (select OverRide from adLeadEntranceTest where EntrTestId=b1.adReqId and ")
                .Append("       LeadId='" & LeadId & "') as OverRide, ")
                .Append(" B1.MinScore from adReqs B1,adReqLeadGroups B2 where B1.adReqId = B2.adReqId ")
                .Append(" and B2.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
                .Append(" and B1.adReqTypeId=1 and B1.adReqId not in ")
                .Append(" (select adReqId from adPrgVerTestDetails where PrgVerId='" & PrgVerId & "' and adReqId is not null) ")
                .Append(" and B1.adReqId not in  ")
                .Append(" (select Distinct adReqId from adReqGrpDef where ReqGrpId in (select ReqGrpId from adPrgVerTestDetails ")
                .Append(" where PrgVerId='" & PrgVerId & "' and adReqId is NULL)) ")
                'Get all mandatory requirements
                .Append(" union ")
                .Append(" select adReqId as EntrTestId,Null as ReqGrpId,Descrip as EntrTestDescrip,1 as Required, ")
                .Append(" (select ActualScore from adLeadEntranceTest where EntrTestId=adReqId and LeadId='" & LeadId & "') as ActualScore, ")
                .Append(" (select TestTaken from adLeadEntranceTest where EntrTestId=adReqId and LeadId='" & LeadId & "') as TestTaken, ")
                .Append("       (select Comments from adLeadEntranceTest where EntrTestId=adReqId and ")
                .Append("       LeadId='" & LeadId & "') as Comments, ")
                .Append("       (select OverRide from adLeadEntranceTest where EntrTestId=adReqId and ")
                .Append("       LeadId='" & LeadId & "') as OverRide, ")
                .Append(" MinScore from adReqs where adReqTypeId=1 and AppliesToAll=1 ")
                .Append(" ) R1 ")
                .Append(" order by R1.EntrTestDescrip ")
            End With

            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As System.Exception
        Finally
            'db.CloseConnection()
        End Try
        Return Nothing
    End Function
    Public Function GetAllStandardRequirementDetails(ByVal LeadId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Try


            With sb
                'Get All Requirements That Are Part Of Group and Get The Requirements Based on Lead Group Level

                .Append(" select Distinct adReqId as EntrTestId,Descrip as EntrTestDescrip,1 as Required, ")
                .Append(" (select ActualScore from adLeadEntranceTest where EntrTestId=adReqId and LeadId='" & LeadId & "') as ActualScore, ")
                .Append(" MinScore ")
                .Append(" from adReqs where AppliesToAll=1 and adReqTypeId=1 ")
                .Append(" union ")
                .Append(" select Distinct t1.adReqId as EntrTestId,t1.Descrip as EntrTestDescrip,t2.IsRequired as Required, ")
                .Append(" (select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId='" & LeadId & "') as ActualScore, ")
                .Append(" t1.MinScore ")
                .Append(" from adReqs t1,adReqLeadGroups t2,adLeads t3 ")
                .Append(" where t1.adReqId = t2.adReqId And t2.LeadGrpId = t3.LeadGrpId ")
                .Append(" and t3.LeadId='" & LeadId & "' and t1.adReqTypeId=1 ")
                .Append(" and t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) ")
                .Append(" and t1.adReqId not in (select Distinct adReqId from adReqGrpDef where ReqGrpId in (select ReqGrpId from adPrgVerTestDetails ")
                .Append(" where adReqId is NULL)) ")
                .Append(" order by Descrip ")

            End With

            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As System.Exception
        Finally
            'db.CloseConnection()
        End Try
        Return Nothing
    End Function
    Public Function GetAllStandardRequirementsNoPrgVersion(ByVal LeadId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Try


            With sb
                'Get All Requirements That Are Part Of Group and Get The Requirements Based on Lead Group Level

                .Append(" select EntrTestId,EntrTestDescrip,Required, ")
                .Append(" case when ActualScore >= Minscore then 'True' else 'Fail' End as Pass,Comments,Override, ")
                .Append(" TestTaken,ActualScore,MinScore  from ")
                .Append("   ( ")
                .Append("       select Distinct adReqId as EntrTestId,Descrip as EntrTestDescrip,1 as Required,  ")
                .Append("       (select ActualScore from adLeadEntranceTest where EntrTestId=adReqId and ")
                .Append("       LeadId='" & LeadId & "') as ActualScore, ")
                .Append("       (select TestTaken from adLeadEntranceTest where EntrTestId=adReqId and ")
                .Append("       LeadId='" & LeadId & "') as TestTaken, ")
                .Append("       (select Comments from adLeadEntranceTest where EntrTestId=adReqId and ")
                .Append("       LeadId='" & LeadId & "') as Comments, ")
                .Append("       (select OverRide from adLeadEntranceTest where EntrTestId=adReqId and ")
                .Append("       LeadId='" & LeadId & "') as OverRide, ")
                .Append("       MinScore ")
                .Append("       from adReqs where AppliesToAll=1 and adReqTypeId=1 ")
                .Append("       union ")
                .Append("       select Distinct t1.adReqId as EntrTestId,t1.Descrip as EntrTestDescrip,t2.IsRequired as Required, ")
                .Append("       (select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("       LeadId='" & LeadId & "') as ActualScore, ")
                .Append("       (select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("       LeadId='" & LeadId & "') as TestTaken,  ")
                .Append("       (select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("       LeadId='" & LeadId & "') as Comments, ")
                .Append("       (select OverRide from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("       LeadId='" & LeadId & "') as OverRide, ")
                .Append("       t1.MinScore ")
                .Append("       from adReqs t1,adReqLeadGroups t2,adLeads t3 ")
                .Append("       where t1.adReqId = t2.adReqId And t2.LeadGrpId = t3.LeadGrpId ")
                .Append("       and t3.LeadId='" & LeadId & "' and t1.adReqTypeId=1 ")
                .Append("       and t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) ")
                .Append("       and t1.adReqId not in (select Distinct adReqId from adReqGrpDef where ReqGrpId ")
                .Append("       in (select ReqGrpId from adPrgVerTestDetails ")
                .Append("       where adReqId is NULL)) ")
                .Append("               ) R1 ")
                .Append("       order by EntrTestDescrip ")
            End With

            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As System.Exception
        Finally
            'db.CloseConnection()
        End Try
        Return Nothing
    End Function
    Public Function GetAllStandardRequirementsByEffectiveDates(ByVal LeadId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Dim CurrentDate As Date = Date.Now.ToShortDateString
        Try


            With sb
                .Append(" select Distinct ")
                .Append(" adReqId,  ")
                .Append(" Descrip,  ")
                .Append(" StartDate,")
                .Append(" EndDate, ")
                .Append(" ActualScore, ")
                .Append(" TestTaken, ")
                .Append(" Comments, ")
                .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append(" Minscore, ")
                .Append(" Required, ")
                .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass ")
                .Append(" from ")
                .Append("   (   ")

                'Get List Of Requirements that are mandatory for all requirement
                .Append("       select  ")
                .Append("           t1.adReqId, ")
                .Append("           t1.Descrip, ")
                .Append("           '" & CurrentDate & "' as CurrentDate, ")
                .Append("           t2.StartDate, ")
                .Append("           t2.EndDate, ")
                .Append("           (select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as ActualScore, ")
                .Append("           (select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as TestTaken, ")
                .Append("           (select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as Comments, ")
                .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("                    LeadId='" & LeadId & "') ")
                .Append("            as override, ")
                .Append("           t2.Minscore, ")
                .Append("           1 as Required ")
                .Append("       from ")
                .Append("           adReqs t1, ")
                .Append("           adReqsEffectiveDates t2 ")
                .Append("       where  ")
                .Append("           t1.adReqId = t2.adReqId and  ")
                .Append("           t1.adReqTypeId = 1 and  ")
                .Append("           t2.MandatoryRequirement=1 ")
                .Append(" ) ")
                .Append("       R1 where R1.CurrentDate >= R1.StartDate and ")
                .Append("       (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union  ")

                'Get List Of Test that has been assigned to lead group but not part of requirement group
                'or not assigned to a program version
                .Append(" select  ")
                .Append(" adReqId,  ")
                .Append(" Descrip,  ")
                .Append(" StartDate,")
                .Append(" EndDate, ")
                .Append(" ActualScore, ")
                .Append(" TestTaken, ")
                .Append(" Comments, ")
                .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append(" Minscore, ")
                .Append(" Required, ")
                .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass ")
                .Append(" from ")
                .Append("   (   ")
                .Append("       select  ")
                .Append("           t1.adReqId, ")
                .Append("           t1.Descrip, ")
                .Append("           '" & CurrentDate & "' as CurrentDate, ")
                .Append("           t2.StartDate, ")
                .Append("           t2.EndDate, ")
                .Append("           (select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as ActualScore, ")
                .Append("           (select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as TestTaken, ")
                .Append("           (select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as Comments, ")
                .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("                    LeadId='" & LeadId & "') ")
                .Append("            as override, ")
                .Append("           t2.Minscore, ")
                .Append("           t3.IsRequired as Required ")
                .Append("       from ")
                .Append("           adReqs t1, ")
                .Append("           adReqsEffectiveDates t2, ")
                .Append("           adReqLeadGroups t3 ")
                .Append("       where  ")
                .Append("           t1.adReqId = t2.adReqId and ")
                .Append("           t1.adreqTypeId = 1 and  ")
                .Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
                .Append("           t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and ")
                .Append("           t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and  ")
                .Append("           t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 ")
                .Append("                              where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null) ")
                .Append(" ) ")
                .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")

            End With
            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As System.Exception
        Finally
        End Try
        Return Nothing
    End Function
    Public Function GetAllStandardRequirementsByEffectiveDatesAndPrgVersion(ByVal LeadId As String, ByVal PrgVerId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Dim currentDate As Date = Date.Now.ToShortDateString
        Try


            With sb
                .Append(" select  ")
                .Append(" adReqId,  ")
                .Append(" Descrip,  ")
                .Append(" StartDate,")
                .Append(" EndDate, ")
                .Append(" ActualScore, ")
                .Append(" TestTaken, ")
                .Append(" Comments, ")
                .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append(" Minscore, ")
                .Append(" Required, ")
                .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass ")
                .Append(" from ")
                .Append("   (   ")

                'Get List Of Requirements that are mandatory for all requirement
                .Append("       select  ")
                .Append("           t1.adReqId, ")
                .Append("           t1.Descrip, ")
                .Append("           '" & currentDate & "' as CurrentDate, ")
                .Append("           t2.StartDate, ")
                .Append("           t2.EndDate, ")
                .Append("           (select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as ActualScore, ")
                .Append("           (select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as TestTaken, ")
                .Append("           (select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as Comments, ")
                .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("                    LeadId='" & LeadId & "') ")
                .Append("            as override, ")
                .Append("           t2.Minscore, ")
                .Append("           1 as Required ")
                .Append("       from ")
                .Append("           adReqs t1, ")
                .Append("           adReqsEffectiveDates t2 ")
                .Append("       where  ")
                .Append("           t1.adReqId = t2.adReqId and  ")
                .Append("           t1.adReqTypeId = 1 and  ")
                .Append("           t2.MandatoryRequirement=1 ")
                .Append(" ) ")
                .Append("       R1 where R1.CurrentDate >= R1.StartDate and ")
                .Append("       (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union  ")

                'Get List Of Test that has been assigned to lead group but not part of requirement group
                'or not assigned to a program version
                .Append(" select  ")
                .Append(" adReqId,  ")
                .Append(" Descrip,  ")
                .Append(" StartDate,")
                .Append(" EndDate, ")
                .Append(" ActualScore, ")
                .Append(" TestTaken, ")
                .Append(" Comments, ")
                .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append(" Minscore, ")
                .Append(" Required, ")
                .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass ")
                .Append(" from ")
                .Append("   (   ")
                .Append("       select  ")
                .Append("           t1.adReqId, ")
                .Append("           t1.Descrip, ")
                .Append("           '" & currentDate & "' as CurrentDate,  ")
                .Append("           t2.StartDate, ")
                .Append("           t2.EndDate, ")
                .Append("           (select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as ActualScore, ")
                .Append("           (select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as TestTaken, ")
                .Append("           (select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as Comments, ")
                .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("                    LeadId='" & LeadId & "') ")
                .Append("            as override, ")
                .Append("           t2.Minscore, ")
                .Append("           t3.IsRequired as Required ")
                .Append("       from ")
                .Append("           adReqs t1, ")
                .Append("           adReqsEffectiveDates t2, ")
                .Append("           adReqLeadGroups t3, ")
                .Append("           adLeads t4 ")
                .Append("       where  ")
                .Append("           t1.adReqId = t2.adReqId and ")
                .Append("           t1.adreqTypeId = 1 and  ")
                .Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
                .Append("           t3.LeadGrpId = t4.LeadGrpId and t4.LeadId='" & LeadId & "' and ")
                .Append("           t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and  ")
                .Append("           t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 ")
                .Append("                              where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null) ")
                .Append(" ) ")
                .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")
                .Append(" Union ")
                'Get List Of Requirements assigned to program version
                'Get List Of Test that has been assigned to lead group but not part of requirement group
                'or not assigned to a program version
                .Append(" select  ")
                .Append(" adReqId,  ")
                .Append(" Descrip,  ")
                .Append(" StartDate,")
                .Append(" EndDate, ")
                .Append(" ActualScore, ")
                .Append(" TestTaken, ")
                .Append(" Comments, ")
                .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append(" Minscore, ")
                .Append(" Required, ")
                .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass ")
                .Append(" from ")
                .Append("   (   ")
                .Append("       select  ")
                .Append("           t1.adReqId, ")
                .Append("           t1.Descrip, ")
                .Append("           '" & currentDate & "' as CurrentDate, ")
                .Append("           t2.StartDate, ")
                .Append("           t2.EndDate, ")
                .Append("           (select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as ActualScore, ")
                .Append("           (select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as TestTaken, ")
                .Append("           (select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as Comments, ")
                .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("                    LeadId='" & LeadId & "') ")
                .Append("            as override, ")
                .Append("           t2.Minscore, ")
                .Append("           t3.IsRequired as Required ")
                .Append("       from ")
                .Append("           adReqs t1, ")
                .Append("           adReqsEffectiveDates t2, ")
                .Append("           adReqLeadGroups t3,adLeads t4, ")
                .Append("           adPrgVerTestDetails t5 ")
                .Append("       where  ")
                .Append("           t1.adReqId = t2.adReqId and ")
                .Append("           t1.adreqTypeId = 1 and      ")
                .Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
                .Append("           t3.LeadGrpId = t4.LeadGrpId and t4.LeadId='" & LeadId & "' and ")
                .Append("           t1.adReqId = t5.adReqId and t1.PrgVerId = '" & PrgVerId & "' and ")
                .Append("           t5.ReqGrpId Is Null  ")
                .Append(" ) ")
                .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")

                .Append(" Union ")
                'Get List Of Requirements assigned to program version and requirements that are part of a group
                .Append(" select  ")
                .Append(" adReqId,  ")
                .Append(" Descrip,  ")
                .Append(" StartDate,")
                .Append(" EndDate, ")
                .Append(" ActualScore, ")
                .Append(" TestTaken, ")
                .Append(" Comments, ")
                .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append(" Minscore, ")
                .Append(" Required, ")
                .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass ")
                .Append(" from ")
                .Append("   (   ")
                .Append("       select  ")
                .Append("           t1.adReqId, ")
                .Append("           t1.Descrip, ")
                .Append("           '" & currentDate & "' as CurrentDate, ")
                .Append("           t2.StartDate, ")
                .Append("           t2.EndDate, ")
                .Append("           (select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as ActualScore, ")
                .Append("           (select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as TestTaken, ")
                .Append("           (select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as Comments, ")
                .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("                    LeadId='" & LeadId & "') ")
                .Append("            as override, ")
                .Append("           t2.Minscore, ")
                .Append("           t3.IsRequired as Required ")
                .Append("       from ")
                .Append("           adReqs t1, ")
                .Append("           adReqsEffectiveDates t2, ")
                .Append("           adReqLeadGroups t3,adLeads t4, ")
                .Append("           adPrgVerTestDetails t5 ")
                .Append("       where  ")
                .Append("           t1.adReqId = t2.adReqId and ")
                .Append("           t1.adreqTypeId = 1 and      ")
                .Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
                .Append("           t3.LeadGrpId = t4.LeadGrpId and t4.LeadId='" & LeadId & "' and ")
                .Append("           t1.adReqId = t5.adReqId and t1.PrgVerId = '" & PrgVerId & "' and ")
                .Append("          t5.ReqGrpId in (select t1.ReqGrpId from adPrgVerTestDetails t1 where t1.PrgVerId='" & PrgVerId & "' and ReqGrpId is not null)  ")
                .Append(" ) ")
                .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")
            End With
            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As System.Exception
        Finally
        End Try
        Return Nothing
    End Function
    Public Function GetDocumentsByPrgVersionLeadGroup(ByVal LeadId As String, ByVal PrgVerId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Try
            With sb
                .Append(" select t1.adReqId as EntrTestId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,t2.Descrip as EntrTestDescrip, ")
                .Append(" t3.IsRequired as Required, ")
                .Append(" (select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId='" & LeadId & "') as ActualScore, ")
                .Append(" t2.MinScore ")
                .Append(" from adPrgVerTestDetails t1,adReqs t2,adReqLeadGroups t3 ")
                .Append(" where t1.adReqId = t2.adReqId and t2.adReqId = t3.adReqId and t1.PrgVerId='" & PrgVerId & "' ")
                .Append(" and t2.adReqTypeId = 3 ")
                .Append(" and t1.ReqGrpId is null and t3.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
                .Append(" union ")
                'Get All Requirements That Are Part Of Group and Get The Requirements Based on Lead Group Level
                .Append(" select t5.adReqId as EntrTestId,t4.ReqGrpId,t5.Descrip as EntrTestDescrip,t6.IsRequired as Required, ")
                .Append(" (select ActualScore from adLeadENtranceTest where EntrTestId=t5.adReqId and LeadId='" & LeadId & "') as ActualScore, ")
                .Append(" t5.MinScore ")
                .Append(" from  adReqGrpDef t4,adReqs t5,adReqLeadGroups t6 ")
                .Append(" where t4.adReqId = t5.adReqId and t5.adReqId = t6.adReqId and t6.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
                .Append(" and t5.adReqTypeId = 3 ")
                .Append(" and ReqGrpId in (select t1.ReqGrpId from adPrgVerTestDetails t1 where t1.PrgVerId='" & PrgVerId & "' and ReqGrpId is not null) ")
                .Append(" order by Descrip ")
            End With

            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As System.Exception
        Finally
            'db.CloseConnection()
        End Try
        Return Nothing
    End Function
    Public Function getPrgVersionByLead(ByVal LeadId As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        Dim PrgVerId As String
        'Dim strPreviousEduTest As String
        With sb
            .Append(" select PrgVerId from adLeads where LeadId = ? ")
        End With
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            If Not (dr("PrgVerId") Is System.DBNull.Value) Then PrgVerId = CType(dr("PrgVerId"), Guid).ToString Else PrgVerId = ""
        End While
        dr.Close()
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        If PrgVerId = "" Then
            Return ""
        Else
            Return PrgVerId
        End If
    End Function
    Public Function getDocumentStatusByDocument(ByVal LeadId As String, ByVal DocumentId As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        Dim strDocStatusId As String = ""
        'Dim strPreviousEduTest As String

        With sb
            .Append(" select Distinct DocStatusId from adLeadDocsReceived where LeadId = ? and DocumentId=?")
        End With
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@DocumentId", DocumentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            If Not (dr("DocStatusId") Is System.DBNull.Value) Then strDocStatusId = CType(dr("DocStatusId"), Guid).ToString Else strDocStatusId = ""
        End While
        dr.Close()
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        If strDocStatusId = "" Then
            Return ""
        Else
            Return strDocStatusId
        End If
    End Function
    Public Function getPrgVersionByStudent(ByVal StudentId As String) As Integer
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim PrgVerId As String
        'Dim strPreviousEduTest As String
        With sb
            .Append(" select Count(*) from arStuEnrollments where StudentId = ? ")
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Dim intCount As Integer = db.RunParamSQLScalar(sb.ToString)
        Return intCount
        db.ClearParameters()
        sb.Remove(0, sb.Length)


    End Function
    'Public Function GetScores(ByVal LeadId As String) As DataSet

    '    Dim sb As New StringBuilder
    '    Dim db As New DataAccess
    '    Dim ds As New DataSet

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If


    '    With sb
    '        .Append(" select t1.EntrTestId, (select Descrip from adReqs where adReqId = t1.EntrTestId and adReqTypeId=1) as EntrTestDescrip, ")
    '        .Append(" t1.MinScore,(Select MaxScore from adEntrTests where EntrTestId = t1.EntrTestId) as MaxScore, ")
    '        .Append(" t1.Required, ")
    '        .Append(" t1.TestTaken, ")
    '        .Append(" t1.ActualScore, ")
    '        .Append(" (select OverRide from adEntrTestOverRide t2 where t2.LeadId=? and t2.EntrTestId=t1.EntrTestId) as OverRide ")
    '        .Append(" from adLeadEntranceTest t1 where t1.LeadId=? and t1.Required=1 and t1.Pass=0 ")
    '    End With

    '    'Build select command
    '    Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
    '    sc.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '    sc.Parameters.Add(New OleDbParameter("@LeadId", LeadId))

    '    Dim da As New OleDbDataAdapter(sc)
    '    da.Fill(ds, "LeadEntranceTest")
    '    sb.Remove(0, sb.Length)

    '    'With sb
    '    '    .Append(" select t1.EntrTestId, t1.OverRide ")
    '    '    .Append(" from adEntrTestOverRide t1 where t1.LeadId=? ")
    '    'End With
    '    'Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(SingletonAppSettings.AppSettings("ConString")))
    '    'sc1.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '    'da.Fill(ds, "OverRide")
    '    'sb.Remove(0, sb.Length)

    '    Return ds
    'End Function
    'Public Function GetOverRideScores(ByVal LeadId As String) As DataSet

    '    Dim sb As New StringBuilder
    '    Dim db As New DataAccess
    '    Dim ds As New DataSet

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If


    '    With sb
    '        '.Append(" select t1.EntrTestId, (select Descrip from adReqs where adReqId = t1.EntrTestId and adReqTypeId=1) as EntrTestDescrip, ")
    '        '.Append(" t1.MinScore,(Select MaxScore from adEntrTests where EntrTestId = t1.EntrTestId) as MaxScore, ")
    '        '.Append(" t1.Required, ")
    '        '.Append(" t1.TestTaken, ")
    '        '.Append(" t1.ActualScore, ")
    '        '.Append(" (select OverRide from adEntrTestOverRide t2 where t2.LeadId=? and t2.EntrTestId=t1.EntrTestId) as OverRide ")
    '        '.Append(" from adLeadEntranceTest t1 where t1.LeadId=? and t1.Required=1 and t1.Pass=0 ")

    '        'Commented By Balaji on 08/22/2005
    '        '.Append(" select t1.EntrTestId,(select Descrip from adReqs where adReqId = t1.EntrTestId and adReqTypeId=1) as EntrTestDescrip,  ")
    '        '.Append(" t1.MinScore,(Select MaxScore from adEntrTests where EntrTestId = t1.EntrTestId) as MaxScore,t1.Required, t1.TestTaken, t1.ActualScore, ")
    '        '.Append(" (select OverRide from adEntrTestOverRide t2 where t2.LeadId='" & LeadId & "' and t2.EntrTestId=t1.EntrTestId) as OverRide ")
    '        '.Append(" from adLeadEntranceTest t1 where t1.LeadId='" & LeadId & "' and t1.Required=1 and t1.Pass=0 ")
    '        '.Append(" union ")
    '        '' Get List Of Pending documents that is required
    '        '.Append(" select DocumentId,(select Descrip from adReqs where adReqId=DocumentId and adReqTypeId=3) as EntrTestDescrip, ")
    '        '.Append(" NULL as MinScore,NULL as MaxScore,s2.IsRequired,NULL as TestTaken,NULL as ActualScore, ")
    '        '.Append(" (select OverRide from adEntrTestOverRide t2 where  t2.LeadId='" & LeadId & "' and t2.EntrTestId=DocumentId) as OverRide ")
    '        '.Append(" from adLeadDocsReceived s1,adReqLeadGroups s2,adLeads s3,syDocStatuses s4,sySysDocStatuses s5 ")
    '        '.Append(" where s1.DocumentId= s2.adReqId and s2.LeadgrpId = s3.LeadgrpId and s1.DocStatusId = s4.DocStatusId and ")
    '        '.Append(" s4.SysDocStatusId = s5.SysDocStatusId and s3.LeadId='" & LeadId & "' and s2.IsRequired=1 and s5.SysDocStatusId = 2 ")
    '        '.Append(" union ")

    '        '.Append("  select DocumentId,Descrip as EntrTestDescrip,NULL as MinScore,NULL as MaxScore,1 as Required,NULL as TestTaken,NULL as ActualScore, ")
    '        '.Append(" (select OverRide from adEntrTestOverRide t2 where t2.LeadId='" & LeadId & "' and t2.EntrTestId=adReqId) as OverRide ")
    '        '.Append(" from adReqs A1,adLeadDocsReceived A2,syDocStatuses A3,sySysDocStatuses A4 ")
    '        '.Append(" where A1.adReqId = A2.DocumentId and A1.adReqTypeId = 3 and A1.AppliesToAll = 1 and A2.DocStatusId = A3.DocStatusId and A3.SysDocStatusId = A4.SysDocStatusId and A4.SysDocStatusId = 2 ")
    '        '.Append(" and A2.LeadId='" & LeadId & "' union ")
    '        ''Get list of tests that are mandatory and are pending
    '        '.Append(" select EntrTestId,Descrip as EntrTestDescrip,NULL as MinScore,NULL as MaxScore,1 as Required,NULL as TestTaken,NULL as ActualScore, ")
    '        '.Append(" (select OverRide from adEntrTestOverRide t2 where t2.LeadId='" & LeadId & "' and t2.EntrTestId=adReqId) as OverRide ")
    '        '.Append(" from adReqs A1,adLeadEntranceTest A2 where A1.adReqId = A2.EntrTestId and A1.adReqTypeId = 1 and A1.AppliesToAll = 1 and A2.Required=1 ")
    '        '.Append(" and A2.LeadId='" & LeadId & "' and A2.Pass=0 ")


    '        .Append(" select t1.EntrTestId,(select Descrip from adReqs where adReqId = t1.EntrTestId and adReqTypeId=1) as EntrTestDescrip, ")
    '        .Append(" t1.MinScore,(Select MaxScore from adEntrTests where EntrTestId = t1.EntrTestId) as MaxScore,t1.Required, t1.TestTaken, t1.ActualScore, ")
    '        .Append(" (select OverRide from adEntrTestOverRide t2 where t2.LeadId='" & LeadId & "' and t2.EntrTestId=t1.EntrTestId) as OverRide ")
    '        .Append(" from adLeadEntranceTest t1 where t1.LeadId='" & LeadId & "' and t1.Required=1 and t1.Pass=0 ")
    '        .Append(" union ")
    '        'Get List Of Pending documents that is required
    '        .Append(" select DocumentId,(select Descrip from adReqs where adReqId=DocumentId and adReqTypeId=3) as EntrTestDescrip, ")
    '        .Append(" NULL as MinScore,NULL as MaxScore,s2.IsRequired,NULL as TestTaken,NULL as ActualScore, ")
    '        .Append(" (select OverRide from adEntrTestOverRide t2 where  t2.LeadId='" & LeadId & "' and t2.EntrTestId=DocumentId) as OverRide ")
    '        .Append(" from adLeadDocsReceived s1,adReqLeadGroups s2,adLeads s3,syDocStatuses s4,sySysDocStatuses s5 ")
    '        .Append(" where s1.DocumentId= s2.adReqId and s2.LeadgrpId = s3.LeadgrpId and s1.DocStatusId = s4.DocStatusId and ")
    '        .Append(" s4.SysDocStatusId = s5.SysDocStatusId and s3.LeadId='" & LeadId & "' and s2.IsRequired=1 and s5.SysDocStatusId = 2 ")
    '        .Append(" union ")
    '        .Append(" select adReqId,Descrip as EntrTestDescrip,NULL as MinScore,NULL as MaxScore,1 as Required,NULL as TestTaken,NULL as ActualScore, ")
    '        .Append(" (select OverRide from adEntrTestOverRide t2 where t2.LeadId='" & LeadId & "' and t2.EntrTestId=adReqId) as OverRide ")
    '        .Append(" from adReqs A1 where A1.adReqTypeId = 3 and  ")
    '        .Append(" A1.AppliesToAll = 1 and adReqId not in  ")
    '        .Append(" (select DocumentId from adLeadDocsReceived where LeadId='" & LeadId & "') ")
    '        .Append(" union ")
    '        'Get list of tests that are mandatory and are pending
    '        .Append(" select adReqId,Descrip as EntrTestDescrip,NULL as MinScore,NULL as MaxScore,1 as Required,NULL as TestTaken,NULL as ActualScore, ")
    '        .Append(" (select OverRide from adEntrTestOverRide t2 where t2.LeadId='" & LeadId & "' and t2.EntrTestId=adReqId) as OverRide ")
    '        .Append(" from adReqs A1 where A1.adReqTypeId = 1 and ")
    '        .Append(" A1.AppliesToAll = 1 and adReqId not in ")
    '        .Append(" (select EntrTestId from adLeadEntranceTest where ")
    '        .Append(" LeadId='" & LeadId & "') ")
    '    End With

    '    'Build select command
    '    Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
    '    'sc.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '    'sc.Parameters.Add(New OleDbParameter("@LeadId", LeadId))

    '    Dim da As New OleDbDataAdapter(sc)
    '    da.Fill(ds, "LeadEntranceTest")
    '    sb.Remove(0, sb.Length)

    '    'With sb
    '    '    .Append(" select t1.EntrTestId, t1.OverRide ")
    '    '    .Append(" from adEntrTestOverRide t1 where t1.LeadId=? ")
    '    'End With
    '    'Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(SingletonAppSettings.AppSettings("ConString")))
    '    'sc1.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '    'da.Fill(ds, "OverRide")
    '    'sb.Remove(0, sb.Length)

    '    Return ds
    'End Function
    Public Function GetOverRideCount(ByVal LeadId As String) As Integer

        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append(" Select Count(*) from adEntrTestOverRide where LeadId=? ")
        End With
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Dim intCount As Integer = db.RunParamSQLScalar(sb.ToString)
        Return intCount

    End Function
    'Public Function GetScoreByTest(ByVal LeadId As String) As DataSet
    '    Dim db As New DataAccess
    '    Dim sb As New System.Text.StringBuilder
    '    Dim ds As New DataSet
    '    Dim strPreviousEducationId As String
    '    Dim strPrgVerId As String
    '    'Dim strPreviousEduTest As String

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If


    '    Try
    '        'Check If Lead Has Previos Education
    '        'If the Lead has previous education then populate the
    '        'entrance test for the program version the lead is intrested in
    '        'and take in to account the previous education also
    '        With sb
    '            .Append(" select PreviousEducation,PrgVerId from adLeads where LeadId = ? ")
    '        End With
    '        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
    '        While dr.Read()
    '            If Not (dr("PreviousEducation") Is System.DBNull.Value) Then strPreviousEducationId = CType(dr("PreviousEducation"), Guid).ToString Else strPreviousEducationId = ""
    '            If Not (dr("PrgVerId") Is System.DBNull.Value) Then strPrgVerId = CType(dr("PrgVerId"), Guid).ToString Else strPrgVerId = ""
    '        End While
    '        dr.Close()
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)


    '        'If The Previous Education Is Not Blank
    '        'The Previous Education Value is hard coded to  F05E3B9B-7734-4711-A3BF-21A933384F1D as that's the 
    '        'default value if Any is selected in Program Version Entrance Test
    '        If Not strPrgVerId = "" And Not strPreviousEducationId = "" Then
    '            With sb
    '                .Append(" select t1.EntrTestId, (select EntrTestDescrip from adEntrTests where EntrTestId = t1.EntrTestId) as EntrTestDescrip, ")
    '                .Append(" t1.MinScore,(Select MaxScore from adEntrTests where EntrTestId = t1.EntrTestId) as MaxScore, ")
    '                .Append(" t1.Required, ")
    '                .Append(" (select TestTaken from adLeadEntranceTest where EntrTestId = t1.EntrTestId and LeadId=?) as TestTaken, ")
    '                .Append(" (select ActualScore from adLeadEntranceTest where EntrTestId = t1.EntrTestId and LeadId=?) as ActualScore, ")
    '                .Append(" (select Pass from adLeadEntranceTest where EntrTestId = t1.EntrTestId and LeadId=?) as Pass, ")
    '                .Append(" (select Comments from adLeadEntranceTest where EntrTestId = t1.EntrTestId and LeadId=?) as Comments ")
    '                .Append(" from adPrgVerTestDetails t1,adPrgVerTest t2 ")
    '                .Append(" where t1.PrgVerTestId = t2.PrgVerTestId and t2.PrevEduId = ? ")
    '                .Append(" and t2.PrgVerId = ? ")
    '                .Append(" union ")
    '                .Append(" select t1.EntrTestId, (select EntrTestDescrip from adEntrTests where EntrTestId = t1.EntrTestId) as EntrTestDescrip, ")
    '                .Append(" t1.MinScore,(Select MaxScore from adEntrTests where EntrTestId = t1.EntrTestId) as MaxScore, ")
    '                .Append(" t1.Required, ")
    '                .Append(" (select TestTaken from adLeadEntranceTest where EntrTestId = t1.EntrTestId and LeadId=?) as TestTaken, ")
    '                .Append(" (select ActualScore from adLeadEntranceTest where EntrTestId = t1.EntrTestId and LeadId=?) as ActualScore, ")
    '                .Append(" (select Pass from adLeadEntranceTest where EntrTestId = t1.EntrTestId and LeadId=?) as Pass, ")
    '                .Append(" (select Comments from adLeadEntranceTest where EntrTestId = t1.EntrTestId and LeadId=?) as Comments ")
    '                .Append(" from adPrgVerTestDetails t1,adPrgVerTest t2 ")
    '                .Append(" where t1.PrgVerTestId = t2.PrgVerTestId and t2.PrevEduId Is Null ")
    '                .Append(" and t2.PrgVerId = ? ")
    '            End With
    '            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
    '            sc1.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '            sc1.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '            sc1.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '            sc1.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '            sc1.Parameters.Add(New OleDbParameter("@PrevEduId", strPreviousEducationId))
    '            sc1.Parameters.Add(New OleDbParameter("@PrgVerId", strPrgVerId))
    '            sc1.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '            sc1.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '            sc1.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '            sc1.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '            sc1.Parameters.Add(New OleDbParameter("@PrgVerId", strPrgVerId))

    '            Dim da1 As New OleDbDataAdapter(sc1)
    '            da1.Fill(ds, "TestDetails")
    '            sb.Remove(0, sb.Length)
    '            Return ds
    '        End If

    '        If Not strPrgVerId = "" And strPreviousEducationId = "" Then
    '            With sb
    '                .Append(" select t1.EntrTestId, (select EntrTestDescrip from adEntrTests where EntrTestId = t1.EntrTestId) as EntrTestDescrip, ")
    '                .Append(" t1.MinScore,(Select MaxScore from adEntrTests where EntrTestId = t1.EntrTestId) as MaxScore, ")
    '                .Append(" t1.Required, ")
    '                .Append(" (select TestTaken from adLeadEntranceTest where EntrTestId = t1.EntrTestId and LeadId=?) as TestTaken, ")
    '                .Append(" (select ActualScore from adLeadEntranceTest where EntrTestId = t1.EntrTestId and LeadId=?) as ActualScore, ")
    '                .Append(" (select Pass from adLeadEntranceTest where EntrTestId = t1.EntrTestId and LeadId=?) as Pass, ")
    '                .Append(" (select Comments from adLeadEntranceTest where EntrTestId = t1.EntrTestId and LeadId=?) as Comments ")
    '                .Append(" from adPrgVerTestDetails t1,adPrgVerTest t2 ")
    '                .Append(" where t1.PrgVerTestId = t2.PrgVerTestId And t2.PrevEduId Is NULL ")
    '                .Append(" and t2.PrgVerId = ? ")
    '                '.Append(" union ")
    '                '.Append(" select t1.EntrTestId, (select EntrTestDescrip from adEntrTests where EntrTestId = t1.EntrTestId) as EntrTestDescrip, ")
    '                '.Append(" t1.MinScore,(Select MaxScore from adEntrTests where EntrTestId = t1.EntrTestId) as MaxScore, ")
    '                '.Append(" t1.Required, ")
    '                '.Append(" (select TestTaken from adLeadEntranceTest where EntrTestId = t1.EntrTestId and LeadId=?) as TestTaken, ")
    '                '.Append(" (select ActualScore from adLeadEntranceTest where EntrTestId = t1.EntrTestId and LeadId=?) as ActualScore, ")
    '                '.Append(" (select Pass from adLeadEntranceTest where EntrTestId = t1.EntrTestId and LeadId=?) as Pass, ")
    '                '.Append(" (select Comments from adLeadEntranceTest where EntrTestId = t1.EntrTestId and LeadId=?) as Comments ")
    '                '.Append(" from adPrgVerTestDetails t1,adPrgVerTest t2 ")
    '                '.Append(" where t1.PrgVerTestId = t2.PrgVerTestId and t2.PrevEduId = 'F05E3B9B-7734-4711-A3BF-21A933384F1D' ")
    '                '.Append(" and t2.PrgVerId = ? ")
    '            End With
    '            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
    '            sc1.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '            sc1.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '            sc1.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '            sc1.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '            sc1.Parameters.Add(New OleDbParameter("@PrgVerId", strPrgVerId))
    '            'sc1.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '            'sc1.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '            'sc1.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '            'sc1.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '            'sc1.Parameters.Add(New OleDbParameter("@PrgVerId", strPrgVerId))
    '            Dim da1 As New OleDbDataAdapter(sc1)
    '            da1.Fill(ds, "TestDetails")
    '            sb.Remove(0, sb.Length)
    '            Return ds
    '        End If

    '    Catch ex As System.Exception
    '    Finally
    '        'db.CloseConnection()
    '    End Try
    '    Return Nothing
    'End Function
    'Public Function GetTestDetailsAppend(ByVal LeadId As String) As DataSet
    '    Dim db As New DataAccess
    '    Dim sb As New System.Text.StringBuilder
    '    Dim ds As New DataSet
    '    Dim strPreviousEducationId As String = ""

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If


    '    Try
    '        'Check If Lead Has Previos Education
    '        'If the Lead has previous education then populate the
    '        'entrance test for the program version the lead is intrested in
    '        'and take in to account the previous education also
    '        With sb
    '            .Append(" select PreviousEducation from adLeads where LeadId = ? ")
    '        End With
    '        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
    '        While dr.Read()
    '            If Not (CType(dr("PreviousEducation"), Guid).ToString = Guid.Empty.ToString) Then strPreviousEducationId = CType(dr("PreviousEducation"), Guid).ToString Else strPreviousEducationId = ""
    '        End While
    '        dr.Close()
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)

    '        With sb
    '            .Append(" select t1.EntrTestId, (select EntrTestDescrip from adEntrTests where EntrTestId = t1.EntrTestId) as EntrTestDescrip, ")
    '            .Append(" t1.MinScore,(Select MaxScore from adEntrTests where EntrTestId = t1.EntrTestId) as MaxScore, ")
    '            .Append(" t1.Required from adPrgVerTestDetails t1,adPrgVerTest t2,adLeads t3 where ")
    '            .Append(" t1.PrgVerTestId = t2.PrgVerTestId and t2.PrgVerId = t3.PrgVerId and ")
    '            If Not strPreviousEducationId = "" Then
    '                .Append(" t2.PrevEduId = t3.PreviousEducation and ")
    '            End If
    '            .Append(" t3.LeadId = ?   ")
    '            .Append(" and t1.EntrTestId not in (select EntrTestId from adLeadEntranceTest where LeadId=?) ")
    '        End With

    '        '   build select command
    '        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
    '        sc.Parameters.Add(New OleDbParameter("@LeadId", LeadId))
    '        sc.Parameters.Add(New OleDbParameter("@LeadId", LeadId))

    '        Dim da As New OleDbDataAdapter(sc)
    '        da.Fill(ds, "TestDetailsAppend")
    '        sb.Remove(0, sb.Length)
    '        Return ds
    '        'db.CloseConnection()
    '    Catch ex As System.Exception
    '    Finally
    '        'db.CloseConnection()
    '    End Try
    '    Return Nothing
    'End Function
    'Public Function GetLeadEntranceTestDetail(ByVal LeadId As String) As DataSet
    '    Dim db As New DataAccess
    '    Dim sb As New System.Text.StringBuilder
    '    Dim ds As New DataSet

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If


    '    Try
    '        'db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '        With sb
    '            .Append(" select t1.LeadId,t1.EntrTestId,t1.Comments, ")
    '            .Append(" (select EntrTestDescrip from adEntrTests where EntrTestId=t1.EntrTestId) as EntrTestDescrip, ")
    '            .Append(" t1.Pass,t1.TestTaken,t1.ActualScore,t1.MinScore,t1.MaxScore, ")
    '            .Append(" t1.ViewOrder from adLeadEntranceTest t1 where t1.LeadId = ? ")
    '            .Append(" order by LeadId,ViewOrder ")
    '        End With

    '        '   build select command
    '        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
    '        sc.Parameters.Add(New OleDbParameter("@LeadId", LeadId))

    '        Dim da As New OleDbDataAdapter(sc)
    '        da.Fill(ds, "LeadEntranceTest")
    '        sb.Remove(0, sb.Length)

    '        Return ds
    '        'db.CloseConnection()
    '    Catch ex As System.Exception
    '    Finally
    '        'db.CloseConnection()
    '    End Try
    '    Return Nothing
    'End Function
    Public Function InsertValues(ByVal LeadId As String, ByVal selectedEntrTestId() As String, ByVal selectedRequired() As String, ByVal selectedPass() As String, ByVal selectedTest() As String, ByVal selectedActual() As String, ByVal selectedMin() As String, ByVal selectedComments() As String, ByVal user As String) As Integer
        '   connect to the database
        Dim db As New DataAccess
        Dim sb As New StringBuilder


        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb1 As New StringBuilder
        With sb1
            .Append("Delete from adLeadEntranceTest where LeadId = ? ")
        End With
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'execute query
        db.RunParamSQLExecuteNoneQuery(sb1.ToString)
        db.ClearParameters()
        sb1.Remove(0, sb1.Length)

        '   Insert one record per each Item in the Selected Group
        Dim i, j, k As Integer
        Dim x, y, z As Integer
        Try
            Dim intLeadCount As Integer
            Dim intRequiredField As Integer

            intLeadCount = selectedEntrTestId.Length
            While x < intLeadCount
                With sb
                    .Append("INSERT INTO adLeadEntranceTest(LeadId,EntrTestId,Required,Pass,TestTaken,ActualScore,MinScore,Comments,ViewOrder,ModUser,ModDate) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?)")
                End With
                'add parameters
                Dim strActual As String = DirectCast(selectedActual.GetValue(x), String)
                Dim strMin As String = DirectCast(selectedMin.GetValue(x), String)
                Dim strPass As Integer
                Dim strInsertTest As String = DirectCast(selectedTest.GetValue(x), String)
                Dim intActual As Integer
                Dim intMinScore As Integer
                Try
                    intActual = selectedActual.GetValue(x)
                Catch ex As System.Exception
                    intActual = 0
                End Try

                Try
                    intMinScore = selectedMin.GetValue(x)
                Catch ex As System.Exception
                    intMinScore = 0
                End Try

                If intActual > intMinScore Then
                    strPass = 1
                Else
                    strPass = 0
                End If

                'If Trim(strActual) >= Trim(strMin) Then
                '    strPass = 1
                'Else
                '    strPass = 0
                'End If

                If Len(strActual) = 0 Then
                    strActual = 0
                End If

                If DirectCast(selectedRequired.GetValue(x), String) = "1" Then
                    intRequiredField = 1
                Else
                    intRequiredField = 0
                End If

                db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@EntrTestId", DirectCast(selectedEntrTestId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Required", intRequiredField, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
                db.AddParameter("@Pass", strPass, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
                db.AddParameter("@TestTaken", DirectCast(selectedTest.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ActualScore", strActual, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@MinScore", DirectCast(selectedMin.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Comments", DirectCast(selectedComments.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ViewOrder", x, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                'Insert into the database only if the completed date was put in
                If Len(strInsertTest) >= 1 Then
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                End If

                db.ClearParameters()
                sb.Remove(0, sb.Length)

                'Increment value of x and y
                x = x + 1
            End While
            Return 0
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function EntranceTestOverRide(ByVal LeadId As String, ByVal selectedEntrTestId() As String, ByVal selectedOverRide() As String, ByVal User As String) As Integer
        '   connect to the database
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb1 As New StringBuilder
        With sb1
            .Append("Delete from adEntrTestOverRide where LeadId = ? ")
        End With
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'execute query
        db.RunParamSQLExecuteNoneQuery(sb1.ToString)
        db.ClearParameters()
        sb1.Remove(0, sb1.Length)

        '   Insert one record per each Item in the Selected Group
        Dim i, j, k As Integer
        Dim x, y, z As Integer
        Try
            Dim intLeadCount As Integer
            Dim strRequiredField As String

            intLeadCount = selectedEntrTestId.Length
            While x < intLeadCount
                With sb
                    .Append("INSERT INTO adEntrTestOverRide(EntrTestOverRideId,LeadId,EntrTestId,OverRide,ModUser,ModDate) ")
                    .Append("VALUES(?,?,?,?,?,?)")
                End With
                'add parameters
                'Dim strPass As Integer

                If DirectCast(selectedOverRide.GetValue(x), String) = "Yes" Then
                    strRequiredField = "Yes"
                Else
                    strRequiredField = "No"
                End If

                db.AddParameter("@EntrTestOverRideId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@EntrTestId", DirectCast(selectedEntrTestId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@OverRide", strRequiredField, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                'execute query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)

                'With sb
                '    .Append(" update adLeadEntranceTest set Pass=1 where EntrTestId=? and LeadId=? ")
                'End With
                'db.AddParameter("@EntrTestId", DirectCast(selectedEntrTestId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'If strRequiredField = "Yes" Then
                '    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                'End If
                'db.ClearParameters()
                'sb.Remove(0, sb.Length)

                'Increment value of x and y
                x = x + 1
            End While
            Return 0
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetAllStandardDocumentsByEffectiveDates(ByVal LeadId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String
        Dim currentDate As Date = Date.Now.ToShortDateString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Try


            With sb
                .Append(" select Distinct adReqId as DocumentId,descrip as DocumentDescrip ")
                .Append(" from ")
                .Append(" ( ")
                .Append(" select  ")
                .Append(" adReqId,  ")
                .Append(" Descrip,  ")
                .Append(" StartDate,")
                .Append(" EndDate, ")
                .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append(" Required ")
                .Append(" from ")
                .Append("   (   ")

                'Get List Of Requirements that are mandatory for all requirement
                .Append("       select  ")
                .Append("           t1.adReqId, ")
                .Append("           t1.Descrip, ")
                .Append("           '" & currentDate & "' as CurrentDate, ")
                .Append("           t2.StartDate, ")
                .Append("           t2.EndDate, ")
                .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("                    LeadId='" & LeadId & "') ")
                .Append("            as override, ")
                .Append("           1 as Required ")
                .Append("       from ")
                .Append("           adReqs t1, ")
                .Append("           adReqsEffectiveDates t2 ")
                .Append("       where  ")
                .Append("           t1.adReqId = t2.adReqId and  ")
                .Append("           t1.adReqTypeId = 3 and  ")
                .Append("           t2.MandatoryRequirement=1 ")
                .Append(" ) ")
                .Append("       R1 where R1.CurrentDate >= R1.StartDate and ")
                .Append("       (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union  ")

                'Get List Of Test that has been assigned to lead group but not part of requirement group
                'or not assigned to a program version
                .Append(" select  ")
                .Append(" adReqId,  ")
                .Append(" Descrip,  ")
                .Append(" StartDate,")
                .Append(" EndDate, ")
                .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append(" Required ")
                .Append(" from ")
                .Append("   (   ")
                .Append("       select  ")
                .Append("           t1.adReqId, ")
                .Append("           t1.Descrip, ")
                .Append("           '" & currentDate & "' as CurrentDate, ")
                .Append("           t2.StartDate, ")
                .Append("           t2.EndDate, ")
                .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("                    LeadId='" & LeadId & "') ")
                .Append("            as override, ")
                .Append("           t3.IsRequired as Required ")
                .Append("       from ")
                .Append("           adReqs t1, ")
                .Append("           adReqsEffectiveDates t2, ")
                .Append("           adReqLeadGroups t3 ")
                .Append("       where  ")
                .Append("           t1.adReqId = t2.adReqId and ")
                .Append("           t1.adreqTypeId = 3 and  ")
                .Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
                .Append("           t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
                .Append("           t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and  ")
                .Append("           t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 ")
                .Append("                              where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null) ")
                .Append(" ) ")
                .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")
                .Append(" ) R2 ")

            End With
            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As System.Exception
        Finally
        End Try
        Return Nothing
    End Function
    Public Function GetAllStandardDocumentsByStudentEnrollment(ByVal StudentId As String, ByVal ModuleId As Integer) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String
        Dim strcurrentDate As Date = Date.Now.ToShortDateString
        Try



            With sb
                .Append(" select Distinct ")
                .Append("              adReqId as DocumentId, ")
                .Append("               Descrip as DocumentDescrip ")
                .Append("              from  ")
                .Append("              ( ")
                ' Get Requirement Group and requirements of mandatory requirement
                .Append("               	select  ")
                .Append("               		adReqId, ")
                .Append("              		Descrip, ")
                .Append("             		ReqGrpId, ")
                .Append("            		StartDate, ")
                .Append("             		EndDate, ")
                .Append("            		Required,ModuleId ")
                .Append("           	from  ")
                .Append("            		( ")
                .Append("          		select ")
                .Append("          		t1.adReqId, ")
                .Append("          				t1.Descrip, ")
                .Append("         				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("         					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("         					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                .Append("         				'" & strcurrentDate & "' as CurrentDate, ")
                .Append("          				t2.StartDate, ")
                .Append("            				t2.EndDate, ")
                .Append("           				t2.Minscore, ")
                .Append("            				1 as Required,t1.ModuleId ")
                .Append("          				from  ")
                .Append("             				adReqs t1, ")
                .Append("            				adReqsEffectiveDates t2 ")
                .Append("            				where  ")
                .Append("           				t1.adReqId = t2.adReqId and ")
                .Append("            				t2.MandatoryRequirement=1 and ")
                .Append("            				t1.adReqTypeId in (3) ")
                .Append("            				) ")
                .Append("             				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("             		union ")
                '-- Get Requirements assigned to a lead groups,but not assigned 
                '--  directly to a program version
                '-- or not assigned to a requirement group 
                .Append("               	select  ")
                .Append("               		adReqId, ")
                .Append("              		Descrip, ")
                .Append("             		ReqGrpId, ")
                .Append("            		StartDate, ")
                .Append("             		EndDate, ")
                .Append("            		Required,ModuleId ")
                .Append("           	from  ")
                .Append("            		( ")
                .Append("          		select ")
                .Append("          		t1.adReqId, ")
                .Append("          				t1.Descrip, ")
                .Append("         				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("         					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("         					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                .Append("         				'" & strcurrentDate & "' as CurrentDate, ")
                .Append("          				t2.StartDate, ")
                .Append("            				t2.EndDate, ")
                .Append("           				t2.Minscore, ")
                .Append("            				t3.Isrequired as Required,t1.ModuleId from ")
                .Append("            							adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3 ")
                .Append("          						where ")
                .Append("         							t1.adReqId = t2.adReqId and ")
                .Append("         							t1.adreqTypeId in (3) and ")
                .Append("         							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  and ")
                .Append("      						       t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where StudentId='" & StudentId & "') ")
                .Append("      							and t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId in (select distinct prgverId from arStuEnrollments where StudentId='" & StudentId & "')) and  ")
                .Append("     							t1.adReqId not in (select Distinct adReqId from adReqGrpDef f1,adLeadByLeadGroups f2 ")
                .Append("     							where f1.LeadGrpId = f2.LeadGrpId and f2.StudentId='" & StudentId & "' ) ")
                .Append("     					) ")
                .Append("     					R1 where R1.CurrentDate >= R1.StartDate and ")
                .Append("    					(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("    				union ")

                ''-- Requirements assigned to a lead group and part of requirement group but not assigned to a program version
                '-- balaji 09/13/2005


                .Append("               	select  ")
                .Append("               		adReqId, ")
                .Append("              		Descrip, ")
                .Append("             		ReqGrpId, ")
                .Append("            		StartDate, ")
                .Append("             		EndDate, ")
                .Append("            		Required,ModuleId ")
                .Append("           	from  ")
                .Append("            		( ")
                .Append("          		select ")
                .Append("          		t1.adReqId, ")
                .Append("          				t1.Descrip, ")
                .Append("         				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("         					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("         					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                .Append("         				'" & strcurrentDate & "' as CurrentDate, ")
                .Append("          				t2.StartDate, ")
                .Append("            				t2.EndDate, ")
                .Append("           				t2.Minscore, ")
                .Append("            				t3.Isrequired as Required,t1.ModuleId ")
                .Append("             						from  ")
                .Append("          							adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3,adReqGrpDef t4 ")
                .Append("             						where ")
                .Append("            							t1.adReqId = t2.adReqId and ")
                .Append("            							t1.adreqTypeId in (3) and ")
                .Append("            							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  and ")
                .Append("                                       t1.adReqId = t4.adReqId and t3.LeadGrpId = t4.LeadGrpId and ")
                .Append("          						    t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where StudentId='" & StudentId & "' ) and ")
                .Append("          						    t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId in (select distinct prgverId from arStuEnrollments where StudentId='" & StudentId & "')) and  ")
                .Append("           						    t1.adReqId not in (select Distinct f1.adReqId from adReqGrpDef f1,adLeadByLeadGroups f2,adPrgVerTestDetails f3 ")
                .Append("           						                    where f1.LeadGrpId = f2.LeadGrpId and f1.reqGrpId=f3.ReqGrpId and f2.StudentId='" & StudentId & "' ")
                .Append("          					                        and f3.prgVerId in (select distinct prgverId from arStuEnrollments where StudentId='" & StudentId & "') and f3.ReqGrpId is not null) ")
                .Append("       					) ")
                .Append("        					R1 where R1.CurrentDate >= R1.StartDate and ")
                .Append("        					(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("       				union ")


                ' Requirements assigned to a program version
                .Append("               	select  ")
                .Append("               		adReqId, ")
                .Append("              		Descrip, ")
                .Append("             		ReqGrpId, ")
                .Append("            		StartDate, ")
                .Append("             		EndDate, ")
                .Append("            		Required,ModuleId ")
                .Append("           	from  ")
                .Append("            		( ")
                .Append("          		select ")
                .Append("          		t1.adReqId, ")
                .Append("          				t1.Descrip, ")
                .Append("         				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("         					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("         					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                .Append("         				'" & strcurrentDate & "' as CurrentDate, ")
                .Append("          				t2.StartDate, ")
                .Append("            				t2.EndDate, ")
                .Append("           				t2.Minscore, ")
                .Append("            				t3.IsRequired as Required,t1.ModuleId ")
                .Append("             					from  ")
                .Append("          							adReqs t1, ")
                .Append("          							adReqsEffectiveDates t2, ")
                .Append("          							adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
                .Append("          						where ")
                .Append("        						t1.adReqId = t2.adReqId and ")
                .Append("       							t1.adreqTypeId in (3) and ")
                .Append("      							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId ")
                .Append("     							and t1.adReqId = t5.adReqId and t5.PrgVerId in (select distinct prgverId from arStuEnrollments where StudentId='" & StudentId & "')   and ")
                .Append("     					       t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where ")
                .Append("    						StudentId='" & StudentId & "' ) ")
                .Append("    							and t5.adReqId is not null ")
                .Append("    						) ")
                .Append("    						R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("   					union ")

                'Get Requirement groups assigned to a program version and all requirements part of group

                .Append("               	select  ")
                .Append("               		adReqId, ")
                .Append("              		Descrip, ")
                .Append("             		ReqGrpId, ")
                .Append("            		StartDate, ")
                .Append("             		EndDate, ")
                .Append("            		Required,ModuleId ")
                .Append("           	from  ")
                .Append("            		( ")
                .Append("          		select ")
                .Append("          		t1.adReqId, ")
                .Append("          				t1.Descrip, ")
                .Append("         				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("         					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("         					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                .Append("         				'" & strcurrentDate & "' as CurrentDate, ")
                .Append("          				t2.StartDate, ")
                .Append("            				t2.EndDate, ")
                .Append("           				t2.Minscore, ")
                .Append("            				t3.IsRequired as Required,t1.ModuleId ")
                .Append("              					from  ")
                .Append("              						adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3,adPrgVerTestDetails t5,adReqGrpDef t6,adLeadByLeadGroups t7 ")
                .Append("               					where ")
                .Append("             						t1.adReqId = t2.adReqId and ")
                .Append("            					t1.adreqTypeId in (3) and ")
                .Append("            					t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and  t3.LeadGrpId = t7.LeadGrpId and ")
                .Append("            					t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
                .Append("           					and t7.StudentId='" & StudentId & "'  and t5.PrgVerId in (select distinct prgverId from arStuEnrollments where StudentId='" & StudentId & "')  ")
                .Append("           		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("            ) ")
                .Append("            R2 where R2.ModuleId =  ? ")

            End With
            db.AddParameter("@ModuleId", ModuleId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(SingletonAppSettings.AppSettings("ConString")))

            'Dim da1 As New OleDbDataAdapter(sc1)
            'da1.Fill(ds, "TestDetails")
            'sb.Remove(0, sb.Length)
            Return db.RunParamSQLDataSet(sb.ToString)
        Finally
        End Try
    End Function

    Public Function GetAllStandardDocumentsByEffectiveDatesAndPrgVersion(ByVal LeadId As String, ByVal PrgVerId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String
        Dim strcurrentDate As Date = Date.Now.ToShortDateString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Try

            With sb
                .Append(" select Distinct ")
                .Append(" adReqId as DocumentId, ")
                .Append(" Descrip as DocumentDescrip ")
                .Append(" from  ")
                .Append(" ( ")
                ' Get Requirement Group and requirements of mandatory requirement
                .Append(" 	select  ")
                .Append(" 		adReqId, ")
                .Append("		Descrip, ")
                .Append("		ReqGrpId, ")
                .Append("		StartDate, ")
                .Append("		EndDate, ")
                .Append("		ActualScore, ")
                .Append("		TestTaken, ")
                .Append("		Comments, ")
                .Append("		OverRide, ")
                .Append("		Minscore, ")
                .Append("		Required, ")
                .Append("		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		DocSubmittedCount, ")
                .Append("		DocStatusDescrip ")
                .Append("	from  ")
                .Append("		( ")
                .Append("		select ")
                .Append("		t1.adReqId, ")
                .Append("				t1.Descrip, ")
                .Append("				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                .Append("				'" & strcurrentDate & "' as CurrentDate, ")
                .Append("				t2.StartDate, ")
                .Append("				t2.EndDate, ")
                .Append("				(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("					LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("				(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("					LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("				(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("					LeadId='" & LeadId & "' ) as Comments, ")
                .Append("				       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("				        LeadId='" & LeadId & "' ) as override, ")
                .Append("				(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("					LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("				t2.Minscore, ")
                .Append("				1 as Required, ")
                .Append("				(select Distinct s1.SysDocStatusId from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("				s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("				from  ")
                .Append("				adReqs t1, ")
                .Append("				adReqsEffectiveDates t2 ")
                .Append("				where  ")
                .Append("				t1.adReqId = t2.adReqId and ")
                .Append("				t2.MandatoryRequirement=1 and ")
                .Append("				t1.adReqTypeId in (3) ")
                .Append("				) ")
                .Append("				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("		union ")
                ' Get Requirements assigned to a lead groups,but not assigned 
                '  directly to a program version
                ' or not assigned to a requirement group 
                .Append(" 	select  ")
                .Append(" 		adReqId, ")
                .Append("		Descrip, ")
                .Append("		ReqGrpId, ")
                .Append("		StartDate, ")
                .Append("		EndDate, ")
                .Append("		ActualScore, ")
                .Append("		TestTaken, ")
                .Append("		Comments, ")
                .Append("		OverRide, ")
                .Append("		Minscore, ")
                .Append("		Required, ")
                .Append("		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		DocSubmittedCount, ")
                .Append("		DocStatusDescrip ")
                .Append("		from  ")
                .Append("				( ")
                .Append("						select ")
                .Append("							t1.adReqId, ")
                .Append("							t1.Descrip, ")
                .Append("							'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append("							'" & strcurrentDate & "' as CurrentDate, ")
                .Append("							t2.StartDate, ")
                .Append("							t2.EndDate, ")
                .Append("							(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("								LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("							(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("								LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("							(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("								LeadId='" & LeadId & "' ) as Comments, ")
                .Append("							       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("							        LeadId='" & LeadId & "' ) ")
                .Append("							as override, ")
                .Append("							(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("								LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("							t2.Minscore, ")
                .Append("							t3.Isrequired as Required, ")
                .Append("							(select Distinct s1.SysDocStatusId from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("							s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("						from  ")
                .Append("							adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3 ")
                .Append("						where ")
                .Append("							t1.adReqId = t2.adReqId and ")
                .Append("							t1.adreqTypeId in (3) and ")
                .Append("							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  and ")
                .Append("						       t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' ) ")
                .Append("							and t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId = '" & PrgVerId & "' ) and  ")
                .Append("							t1.adReqId not in (select Distinct adReqId from adReqGrpDef f1,adLeadByLeadGroups f2 ")
                .Append("							where f1.LeadGrpId = f2.LeadGrpId and f2.LeadId='" & LeadId & "' ) ")
                .Append("					) ")
                .Append("					R1 where R1.CurrentDate >= R1.StartDate and ")
                .Append("					(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("				union ")

                '' Requirements assigned to a lead group and part of requirement group but not assigned to a program version
                '' balaji 09/13/2005


                .Append(" 	select  ")
                .Append(" 		adReqId, ")
                .Append("		Descrip, ")
                .Append("		ReqGrpId, ")
                .Append("		StartDate, ")
                .Append("		EndDate, ")
                .Append("		ActualScore, ")
                .Append("		TestTaken, ")
                .Append("		Comments, ")
                .Append("		OverRide, ")
                .Append("		Minscore, ")
                .Append("		Required, ")
                .Append("		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		DocSubmittedCount, ")
                .Append("		DocStatusDescrip ")
                .Append("		from  ")
                .Append("				( ")
                .Append("						select ")
                .Append("							t1.adReqId, ")
                .Append("							t1.Descrip, ")
                .Append("							case when t4.ReqGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t4.ReqGrpId end as ReqGrpId, ")
                .Append("							'" & strcurrentDate & "' as CurrentDate, ")
                .Append("							t2.StartDate, ")
                .Append("							t2.EndDate, ")
                .Append("							(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("								LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("							(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("								LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("							(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("								LeadId='" & LeadId & "' ) as Comments, ")
                .Append("							       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("							        LeadId='" & LeadId & "' ) ")
                .Append("							as override, ")
                .Append("							(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("								LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("							t2.Minscore, ")
                .Append("							t3.Isrequired as Required, ")
                .Append("							(select Distinct s1.SysDocStatusId from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("							s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("						from  ")
                .Append("							adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3,adReqGrpDef t4 ")
                .Append("						where ")
                .Append("							t1.adReqId = t2.adReqId and ")
                .Append("							t1.adreqTypeId in (3) and ")
                .Append("							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  and ")
                .Append("                           t1.adReqId = t4.adReqId and t3.LeadGrpId = t4.LeadGrpId and ")
                .Append("						    t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' ) and ")
                .Append("						    t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId = '" & PrgVerId & "') and  ")
                .Append("						    t1.adReqId not in (select Distinct f1.adReqId from adReqGrpDef f1,adLeadByLeadGroups f2,adPrgVerTestDetails f3 ")
                .Append("						                    where f1.LeadGrpId = f2.LeadGrpId and f1.reqGrpId=f3.ReqGrpId and f2.LeadId='" & LeadId & "' ")
                .Append("					                        and f3.prgVerId='" & PrgVerId & "' and f3.ReqGrpId is not null) ")
                .Append("					) ")
                .Append("					R1 where R1.CurrentDate >= R1.StartDate and ")
                .Append("					(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("				union ")


                '' Requirements assigned to a program version
                .Append("			select  ")
                .Append("					adReqId, ")
                .Append("					Descrip, ")
                .Append("					ReqGrpId, ")
                .Append("					StartDate, ")
                .Append("					EndDate, ")
                .Append("					ActualScore, ")
                .Append("					TestTaken, ")
                .Append("						Comments, ")
                .Append("						OverRide, ")
                .Append("						Minscore, ")
                .Append("						Required, ")
                .Append("						Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("						DocSubmittedCount, ")
                .Append("						DocStatusDescrip ")
                .Append("				from  ")
                .Append("					( ")
                .Append("						select ")
                .Append("							t1.adReqId, ")
                .Append("							t1.Descrip, ")
                .Append("						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("						'" & strcurrentDate & "' as CurrentDate, ")
                .Append("						t2.StartDate, ")
                .Append("						t2.EndDate, ")
                .Append("						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("							LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("					(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("							LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("							LeadId='" & LeadId & "' ) as Comments, ")
                .Append("					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("						        LeadId='" & LeadId & "' ) ")
                .Append("						as override, ")
                .Append("						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("							LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("					t2.Minscore, ")
                .Append("						t3.IsRequired as Required, ")
                .Append("						(select Distinct s1.SysDocStatusId from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("						s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("					from  ")
                .Append("							adReqs t1, ")
                .Append("							adReqsEffectiveDates t2, ")
                .Append("							adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
                .Append("						where ")
                .Append("						t1.adReqId = t2.adReqId and ")
                .Append("							t1.adreqTypeId in (3) and ")
                .Append("							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId ")
                .Append("							and t1.adReqId = t5.adReqId and t5.PrgVerId = '" & PrgVerId & "'   and ")
                .Append("					       t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where ")
                .Append("						LeadId='" & LeadId & "' ) ")
                .Append("							and t5.adReqId is not null ")
                .Append("						) ")
                .Append("						R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("					union ")

                'Get Requirement groups assigned to a program version and all requirements part of group

                .Append("			select  ")
                .Append("					adReqId, ")
                .Append("					Descrip, ")
                .Append("					ReqGrpId, ")
                .Append("					StartDate, ")
                .Append("					EndDate, ")
                .Append("					ActualScore, ")
                .Append("					TestTaken, ")
                .Append("						Comments, ")
                .Append("						OverRide, ")
                .Append("						Minscore, ")
                .Append("						Required, ")
                .Append("						Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("						DocSubmittedCount, ")
                .Append("						DocStatusDescrip ")
                .Append("				from  ")
                .Append("					( ")
                .Append("						select ")
                .Append("							t1.adReqId, ")
                .Append("							t1.Descrip, ")
                .Append("						t5.ReqGrpId as reqGrpId, ")
                .Append("						'" & strcurrentDate & "' as CurrentDate, ")
                .Append("						t2.StartDate, ")
                .Append("						t2.EndDate, ")
                .Append("						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("							LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("					(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("							LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("							LeadId='" & LeadId & "' ) as Comments, ")
                .Append("					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("						        LeadId='" & LeadId & "' ) ")
                .Append("						as override, ")
                .Append("						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("							LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("					t2.Minscore, ")
                .Append("						t3.IsRequired as Required, ")
                .Append("						(select Distinct s1.SysDocStatusId from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("						s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("					from  ")
                .Append("						adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3,adPrgVerTestDetails t5,adReqGrpDef t6,adLeadByLeadGroups t7 ")
                .Append("					where ")
                .Append("						t1.adReqId = t2.adReqId and ")
                .Append("					t1.adreqTypeId in (3) and ")
                .Append("					t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and  t3.LeadGrpId = t7.LeadGrpId and ")
                .Append("					t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
                .Append("					and t7.LeadId='" & LeadId & "'  and t5.PrgVerId = '" & PrgVerId & "'  ")
                .Append("		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" ) ")
                .Append(" R2 ")
                '   .Append(" ) R3 where R3.Required =1  and  R3.DocStat = 'NotApproved' and R3.OverRide='False' ")
            End With
            'commented by balaji on 09/12/2005

            ' With sb


            'Get All Requirements That Are Not Part Of Any Group
            '
            '    .Append(" select Distinct ")
            '    .Append(" adReqId as DocumentId, ")
            '    .Append(" Descrip as DocumentDescrip ")
            '    '.Append(" ReqGrpId, ")
            '    '.Append(" StartDate, ")
            '    '.Append(" EndDate, ")
            '    '.Append(" ActualScore, ")
            '    '.Append(" TestTaken, ")
            '    '.Append(" Comments, ")
            '    '.Append(" Case when OverRide>=1 then 'True' else 'False' end as OverRide, ")
            '    '.Append(" Minscore, ")
            '    '.Append(" Required, ")
            '    '.Append(" DocSubmittedCount, ")
            '    '.Append(" DocStatusDescrip, ")
            '    '.Append(" Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount, ")
            '    '.Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass ")
            '    .Append(" from  ")
            '    .Append(" ( ")
            '    .Append(" select  ")
            '    .Append(" adReqId, ")
            '    .Append(" Descrip, ")
            '    .Append(" ReqGrpId, ")
            '    .Append(" StartDate, ")
            '    .Append(" EndDate, ")
            '    .Append(" ActualScore, ")
            '    .Append(" TestTaken, ")
            '    .Append(" Comments, ")
            '   .Append(" OverRide, ")
            '    .Append(" Minscore, ")
            '    .Append(" Required, ")
            '    .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
            '    .Append(" DocSubmittedCount, ")
            '    .Append(" DocStatusDescrip ")
            '    .Append(" from  ")
            '    .Append(" ( ")
            '    ' Get Requirement Group and requirements of mandatory requirement
            '    .Append(" select ")
            '    .Append("	t1.adReqId, ")
            '    .Append("	t1.Descrip, ")
            '    .Append("	case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
            '    .Append("		(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
            '    .Append("		else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
            '    .Append("	'" & currentDate & "' as CurrentDate, ")
            '    .Append("	t2.StartDate, ")
            '    .Append("	t2.EndDate, ")
            '    .Append("	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("		LeadId='" & LeadId & "' ) as ActualScore,  ")
            '    .Append("	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("		LeadId='" & LeadId & "' ) as TestTaken,  ")
            '    .Append("	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            '    .Append("		LeadId='" & LeadId & "' ) as Comments,  ")
            '    .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            '    .Append("                    LeadId='" & LeadId & "') ")
            '    .Append("            as override, ")
            '    .Append("	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            '    .Append("		LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
            '    .Append("	t2.Minscore, ")
            '    .Append("	1 as Required, ")
            '    .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            '    .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            '    .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            '    .Append(" from  ")
            '    .Append("	adReqs t1, ")
            '    .Append("	adReqsEffectiveDates t2 ")
            '    .Append(" where  ")
            '    .Append("	t1.adReqId = t2.adReqId and ")
            '    .Append("	t2.MandatoryRequirement=1 and ")
            '    .Append("	t1.adReqTypeId in (3) ")
            '    .Append(" ) ")
            '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            '    .Append(" union ")
            '    ' Get Requirements assigned to a lead groups,but not assigned directly to a program version
            '    ' or not assigned to a requirement group 
            '    .Append(" select  ")
            '    .Append(" adReqId, ")
            '    .Append(" Descrip, ")
            '    .Append(" ReqGrpId, ")
            '    .Append(" StartDate, ")
            '    .Append(" EndDate, ")
            '    .Append(" ActualScore, ")
            '    .Append(" TestTaken, ")
            '    .Append(" Comments, ")
            ' .Append(" OverRide, ")
            '    .Append(" Minscore, ")
            '    .Append(" Required, ")
            '    .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
            '    .Append(" DocSubmittedCount, ")
            '    .Append(" DocStatusDescrip ")
            '    .Append(" from  ")
            '    .Append(" ( ")
            '    ' Get Requirement Group and requirements of mandatory requirement
            '    .Append(" select ")
            '    .Append("	t1.adReqId, ")
            '    .Append("	t1.Descrip, ")
            '    .Append("	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            '    .Append("	'" & currentDate & "' as CurrentDate, ")
            '    .Append("	t2.StartDate, ")
            '    .Append("	t2.EndDate, ")
            '    .Append("	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("		LeadId='" & LeadId & "' ) as ActualScore,  ")
            '    .Append("	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("		LeadId='" & LeadId & "' ) as TestTaken,  ")
            '    .Append("	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            '    .Append("		LeadId='" & LeadId & "' ) as Comments,  ")
            '    .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            '    .Append("                    LeadId='" & LeadId & "') ")
            '    .Append("            as override, ")
            '    .Append("	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            '    .Append("		LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
            '    .Append("	t2.Minscore, ")
            '    .Append("	t3.Isrequired as Required, ")
            '    .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            '    .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            '    .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            '    .Append(" from  ")
            '    .Append("	adReqs t1, ")
            '    .Append("		adReqsEffectiveDates t2, ")
            '    .Append("		adReqLeadGroups t3 ")
            '    .Append("	where ")
            '    .Append("		t1.adReqId = t2.adReqId and ")
            '    .Append("		t1.adreqTypeId in (3) and ")
            '    .Append("		t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            '    .Append("       t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            '    .Append("		t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and  ")
            '    .Append("		t1.adReqId not in (select Distinct adReqId from adReqGrpDef) ")
            '    .Append(" ) ")
            '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            '    .Append(" union ")

            '    ' Requirements assigned to a program version and not part of any requirement group
            '    .Append(" select  ")
            '    .Append(" adReqId, ")
            '    .Append(" Descrip, ")
            '    .Append(" ReqGrpId, ")
            '    .Append(" StartDate, ")
            '    .Append(" EndDate, ")
            '    .Append(" ActualScore, ")
            '    .Append(" TestTaken, ")
            '    .Append(" Comments, ")
            '  .Append(" OverRide, ")
            '    .Append(" Minscore, ")
            '    .Append(" Required, ")
            '    .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
            '    .Append(" DocSubmittedCount, ")
            '    .Append(" DocStatusDescrip ")
            '    .Append(" from  ")
            '    .Append(" ( ")
            '    ' Get Requirement Group and requirements of mandatory requirement
            '    .Append(" select ")
            '    .Append("	t1.adReqId, ")
            '    .Append("	t1.Descrip, ")
            '    .Append(" case when t5.ReqGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t5.ReqGrpId End as reqGrpId,  ")
            '    .Append("	'" & currentDate & "' as CurrentDate, ")
            '    .Append("	t2.StartDate, ")
            '    .Append("	t2.EndDate, ")
            '    .Append("	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("		LeadId='" & LeadId & "' ) as ActualScore,  ")
            '    .Append("	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("		LeadId='" & LeadId & "' ) as TestTaken,  ")
            '    .Append("	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            '    .Append("		LeadId='" & LeadId & "' ) as Comments,  ")
            '    .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            '    .Append("                    LeadId='" & LeadId & "') ")
            '    .Append("            as override, ")
            '    .Append("	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            '    .Append("		LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
            '    .Append("	t2.Minscore, ")
            '    .Append("	t3.IsRequired as Required, ")
            '    .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            '    .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            '    .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            '    .Append(" from  ")
            '    .Append("	adReqs t1, ")
            '    .Append("		adReqsEffectiveDates t2, ")
            '    .Append("		adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
            '    .Append("	where ")
            '    .Append("		t1.adReqId = t2.adReqId and ")
            '    .Append("		t1.adreqTypeId in (3) and ")
            '    .Append("		t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId ")
            '    .Append("		and t1.adReqId = t5.adReqId and t5.PrgVerId='" & PrgVerId & "'  and ")
            '    .Append("       t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            '    .Append("		t1.adReqId not in (select Distinct adReqId from adReqGrpDef) ")
            '    .Append(" ) ")
            '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            '    .Append(" union ")
            '    ' Get Requirements assigned to a lead groups,but not assigned directly to a program version
            '    ' or not assigned to a requirement group that is part of a program version
            '    .Append(" select  ")
            '    .Append(" adReqId, ")
            '    .Append(" Descrip, ")
            '    .Append(" ReqGrpId, ")
            '    .Append(" StartDate, ")
            '    .Append(" EndDate, ")
            '    .Append(" ActualScore, ")
            '    .Append(" TestTaken, ")
            '    .Append(" Comments, ")
            '  .Append(" OverRide, ")
            '    .Append(" Minscore, ")
            '    .Append(" Required, ")
            '    .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
            '    .Append(" DocSubmittedCount, ")
            '    .Append(" DocStatusDescrip ")
            '    .Append(" from  ")
            '    .Append(" ( ")
            '    ' Get Requirement Group and requirements of mandatory requirement
            '    .Append(" select ")
            '    .Append("	t1.adReqId, ")
            '    .Append("	t1.Descrip, ")
            '    .Append(" case when t5.ReqGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t5.ReqGrpId End as reqGrpId,  ")
            '    .Append("	'" & currentDate & "' as CurrentDate, ")
            '    .Append("	t2.StartDate, ")
            '    .Append("	t2.EndDate, ")
            '    .Append("	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("		LeadId='" & LeadId & "' ) as ActualScore,  ")
            '    .Append("	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("		LeadId='" & LeadId & "' ) as TestTaken,  ")
            '    .Append("	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            '    .Append("		LeadId='" & LeadId & "' ) as Comments,  ")
            '    .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            '    .Append("                    LeadId='" & LeadId & "') ")
            '    .Append("            as override, ")
            '    .Append("	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            '    .Append("		LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
            '    .Append("	t2.Minscore, ")
            '    .Append("	t3.IsRequired as Required, ")
            '    .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            '    .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            '    .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            '    .Append(" from  ")
            '    .Append("	adReqs t1, ")
            '    .Append("		adReqsEffectiveDates t2, ")
            '    .Append("		adReqLeadGroups t3,adReqGrpDef t5 ")
            '    .Append(" where ")
            '    .Append("	t1.adReqId = t2.adReqId and ")
            '    .Append("	t1.adreqTypeId in (3) and ")
            '    .Append("	t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t1.adReqId = t5.adReqId and ")
            '    .Append("           t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            '    .Append("	t1.adReqId = t5.adReqId and ")
            '    .Append("	t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and ")
            '    .Append("	t5.adReqId not in (select Distinct s1.ReqGrpId from adReqGrpDef s1,adPrgVerTestDetails s2 ")
            '    .Append("			     where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null) ")
            '    .Append("	) ")
            '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            '    ' requirements part of a requirement group assigned to program version 
            '    .Append(" union ")
            '    .Append(" select  ")
            '    .Append(" adReqId, ")
            '    .Append(" Descrip, ")
            '    .Append(" ReqGrpId, ")
            '    .Append(" StartDate, ")
            '    .Append(" EndDate, ")
            '    .Append(" ActualScore, ")
            '    .Append(" TestTaken, ")
            '    .Append(" Comments, ")
            '    .Append(" OverRide, ")
            '    .Append(" Minscore, ")
            '    .Append(" Required, ")
            '    .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
            '    .Append(" DocSubmittedCount,DocStatusDescrip ")
            '    .Append(" from  ")
            '    .Append(" ( ")
            '    '' Get Requirement Group and requirements of mandatory requirement
            '    .Append(" select ")
            '    .Append("	t1.adReqId, ")
            '    .Append("	t1.Descrip, ")
            '    .Append(" case when t5.ReqGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t5.ReqGrpId End as reqGrpId,  ")
            '    .Append("	'" & currentDate & "' as CurrentDate, ")
            '    .Append("	t2.StartDate, ")
            '    .Append("	t2.EndDate, ")
            '    .Append("	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("		LeadId='" & LeadId & "' ) as ActualScore,  ")
            '    .Append("	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("		LeadId='" & LeadId & "' ) as TestTaken,  ")
            '    .Append("	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            '    .Append("		LeadId='" & LeadId & "' ) as Comments,  ")
            '    .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            '    .Append("                    LeadId='" & LeadId & "') ")
            '    .Append("            as override, ")
            '    .Append("	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            '    .Append("		LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
            '    .Append("	t2.Minscore, ")
            '    .Append("	t3.IsRequired as Required, ")
            '    .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            '    .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            '    .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            '    .Append(" from  ")
            '    .Append("	adReqs t1, ")
            '    .Append("		adReqsEffectiveDates t2, ")
            '    .Append("		adReqLeadGroups t3,adReqGrpDef t5  ")
            '    .Append("	where  ")
            '    .Append("		t1.adReqId = t2.adReqId and  ")
            '    .Append("		t1.adreqTypeId in (3) and   ")
            '    .Append("		t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t1.adReqId = t5.adReqId and  ")
            '    .Append("           t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            '    .Append("		t5.ReqGrpId in (select Distinct s1.ReqGrpId from adReqGrpDef s1,adPrgVerTestDetails s2  ")
            '    .Append("				     where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null)  ")
            '    .Append("	)  ")
            '    .Append("	R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)  ")
            '    .Append("	) R2 ")
            'End With

            'With sb
            '    .Append(" select  ")
            '    .Append(" adReqId as DocumentId,  ")
            '    .Append(" Descrip as DocumentDescrip,  ")
            '    .Append(" StartDate,")
            '    .Append(" EndDate, ")
            '    .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
            '    .Append(" Required ")
            '    .Append(" from ")
            '    .Append("   (   ")

            '    'Get List Of Requirements that are mandatory for all requirement
            '    .Append("       select  ")
            '    .Append("           t1.adReqId, ")
            '    .Append("           t1.Descrip, ")
            '    .Append("           Convert(char(10),getdate(),101) as CurrentDate, ")
            '    .Append("           t2.StartDate, ")
            '    .Append("           t2.EndDate, ")
            '    .Append("           (select OverRide from adLeadDocsReceived where DocumentId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as OverRide, ")
            '    .Append("           1 as Required ")
            '    .Append("       from ")
            '    .Append("           adReqs t1, ")
            '    .Append("           adReqsEffectiveDates t2 ")
            '    .Append("       where  ")
            '    .Append("           t1.adReqId = t2.adReqId and  ")
            '    .Append("           t1.adReqTypeId = 3 and  ")
            '    .Append("           t2.MandatoryRequirement=1 ")
            '    .Append(" ) ")
            '    .Append("       R1 where R1.CurrentDate >= R1.StartDate and ")
            '    .Append("       (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            '    .Append(" union  ")

            '    'Get List Of Test that has been assigned to lead group but not part of requirement group
            '    'or not assigned to a program version
            '    .Append(" select  ")
            '    .Append(" adReqId,  ")
            '    .Append(" Descrip,  ")
            '    .Append(" StartDate,")
            '    .Append(" EndDate, ")
            '    .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
            '    .Append(" Required ")
            '    .Append(" from ")
            '    .Append("   (   ")
            '    .Append("       select  ")
            '    .Append("           t1.adReqId, ")
            '    .Append("           t1.Descrip, ")
            '    .Append("           Convert(char(10),getdate(),101) as CurrentDate, ")
            '    .Append("           t2.StartDate, ")
            '    .Append("           t2.EndDate, ")
            '    .Append("           (select OverRide from adLeadDocsReceived where DocumentId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as OverRide, ")
            '    .Append("           t3.IsRequired as Required ")
            '    .Append("       from ")
            '    .Append("           adReqs t1, ")
            '    .Append("           adReqsEffectiveDates t2, ")
            '    .Append("           adReqLeadGroups t3, ")
            '    .Append("           adLeads t4 ")
            '    .Append("       where  ")
            '    .Append("           t1.adReqId = t2.adReqId and ")
            '    .Append("           t1.adreqTypeId = 3 and  ")
            '    .Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            '    .Append("           t3.LeadGrpId = t4.LeadGrpId and t4.LeadId='" & LeadId & "' and ")
            '    .Append("           t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and  ")
            '    .Append("           t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 ")
            '    .Append("                              where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null) ")
            '    .Append(" ) ")
            '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")
            '    .Append(" Union ")
            '    'Get List Of Requirements assigned to program version
            '    'Get List Of Test that has been assigned to lead group but not part of requirement group
            '    'or not assigned to a program version
            '    .Append(" select  ")
            '    .Append(" adReqId,  ")
            '    .Append(" Descrip,  ")
            '    .Append(" StartDate,")
            '    .Append(" EndDate, ")
            '    .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
            '    .Append(" Required, ")
            '    .Append(" from ")
            '    .Append("   (   ")
            '    .Append("       select  ")
            '    .Append("           t1.adReqId, ")
            '    .Append("           t1.Descrip, ")
            '    .Append("           Convert(char(10),getdate(),101) as CurrentDate, ")
            '    .Append("           t2.StartDate, ")
            '    .Append("           t2.EndDate, ")
            '    .Append("           (select OverRide from adLeadDocsReceived where DocumentId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as OverRide, ")
            '    .Append("           t2.Minscore, ")
            '    .Append("           t3.IsRequired as Required ")
            '    .Append("       from ")
            '    .Append("           adReqs t1, ")
            '    .Append("           adReqsEffectiveDates t2, ")
            '    .Append("           adReqLeadGroups t3,adLeads t4, ")
            '    .Append("           adPrgVerTestDetails t5 ")
            '    .Append("       where  ")
            '    .Append("           t1.adReqId = t2.adReqId and ")
            '    .Append("           t1.adreqTypeId = 3 and      ")
            '    .Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            '    .Append("           t3.LeadGrpId = t4.LeadGrpId and t4.LeadId='" & LeadId & "' and ")
            '    .Append("           t1.adReqId = t5.adReqId and t1.PrgVerId = '" & PrgVerId & "' and ")
            '    .Append("           t5.ReqGrpId Is Null  ")
            '    .Append(" ) ")
            '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")

            '    .Append(" Union ")
            '    'Get List Of Requirements assigned to program version and requirements that are part of a group
            '    .Append(" select  ")
            '    .Append(" adReqId,  ")
            '    .Append(" Descrip,  ")
            '    .Append(" StartDate,")
            '    .Append(" EndDate, ")
            '    .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
            '    .Append(" Required ")
            '    .Append(" from ")
            '    .Append("   (   ")
            '    .Append("       select  ")
            '    .Append("           t1.adReqId, ")
            '    .Append("           t1.Descrip, ")
            '    .Append("           Convert(char(10),getdate(),101) as CurrentDate, ")
            '    .Append("           t2.StartDate, ")
            '    .Append("           t2.EndDate, ")
            '    .Append("           (select OverRide from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as OverRide, ")
            '    .Append("           t3.IsRequired as Required ")
            '    .Append("       from ")
            '    .Append("           adReqs t1, ")
            '    .Append("           adReqsEffectiveDates t2, ")
            '    .Append("           adReqLeadGroups t3,adLeads t4, ")
            '    .Append("           adPrgVerTestDetails t5 ")
            '    .Append("       where  ")
            '    .Append("           t1.adReqId = t2.adReqId and ")
            '    .Append("           t1.adreqTypeId = 3 and      ")
            '    .Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            '    .Append("           t3.LeadGrpId = t4.LeadGrpId and t4.LeadId='" & LeadId & "' and ")
            '    .Append("           t1.adReqId = t5.adReqId and t1.PrgVerId = '" & PrgVerId & "' and ")
            '    .Append("          t5.ReqGrpId in (select t1.ReqGrpId from adPrgVerTestDetails t1 where t1.PrgVerId='" & PrgVerId & "' and ReqGrpId is not null)  ")
            '    .Append(" ) ")
            '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")
            'End With
            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As System.Exception
        Finally
        End Try
        Return Nothing
    End Function
    Public Function CheckIfLeadHasApprovedRequiredDocumentsNoProgramVersion(ByVal LeadId As String) As Integer
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String
        Dim currentDate As Date = Date.Now.ToShortDateString
        Try

            With sb
                .Append(" select Count(*) as CountOfLeadFailedRequiredDocuments from ")
                .Append(" ( ")
                .Append(" select  ")
                .Append(" adReqId,  ")
                .Append(" Descrip,  ")
                .Append(" StartDate,")
                .Append(" EndDate, ")
                .Append(" case when DocStatusId=1 then 'Approved' else 'Not Approved' end as DocStatus, ")
                .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append(" Required ")
                .Append(" from ")
                .Append("   (   ")

                'Get List Of Requirements that are mandatory for all requirement
                .Append("       select  ")
                .Append("           t1.adReqId, ")
                .Append("           t1.Descrip, ")
                .Append("           '" & currentDate & "' as CurrentDate, ")
                .Append("           t2.StartDate, ")
                .Append("           t2.EndDate, ")
                .Append(" (select s3.SysDocStatusId from adLeadDocsReceived s1,syDocStatuses s2,sySysDocStatuses s3 ")
                .Append("  where s1.DocStatusId = s2.DocStatusId And s2.SysDocStatusId = s3.SysDocStatusId ")
                .Append("  and s1.documentId = t1.adReqId and s1.LeadId='" & LeadId & "') as DocStatusId, ")
                .Append("           (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as OverRide, ")
                .Append("           1 as Required ")
                .Append("       from ")
                .Append("           adReqs t1, ")
                .Append("           adReqsEffectiveDates t2 ")
                .Append("       where  ")
                .Append("           t1.adReqId = t2.adReqId and  ")
                .Append("           t1.adReqTypeId = 3 and  ")
                .Append("           t2.MandatoryRequirement=1 ")
                .Append(" ) ")
                .Append("       R1 where R1.CurrentDate >= R1.StartDate and ")
                .Append("       (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union  ")

                'Get List Of Test that has been assigned to lead group but not part of requirement group
                'or not assigned to a program version
                .Append(" select  ")
                .Append(" adReqId,  ")
                .Append(" Descrip,  ")
                .Append(" StartDate,")
                .Append(" EndDate, ")
                .Append(" case when DocStatusId=1 then 'Approved' else 'Not Approved' end as DocStatus, ")
                .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append(" Required ")
                .Append(" from ")
                .Append("   (   ")
                .Append("       select  ")
                .Append("           t1.adReqId, ")
                .Append("           t1.Descrip, ")
                .Append("           '" & currentDate & "' as CurrentDate, ")
                .Append("           t2.StartDate, ")
                .Append("           t2.EndDate, ")
                .Append(" (select s3.SysDocStatusId from adLeadDocsReceived s1,syDocStatuses s2,sySysDocStatuses s3 ")
                .Append("  where s1.DocStatusId = s2.DocStatusId And s2.SysDocStatusId = s3.SysDocStatusId ")
                .Append("  and s1.documentId = t1.adReqId and s1.LeadId='" & LeadId & "') as DocStatusId, ")
                .Append("           (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as OverRide, ")
                .Append("           t3.IsRequired as Required ")
                .Append("       from ")
                .Append("           adReqs t1, ")
                .Append("           adReqsEffectiveDates t2, ")
                .Append("           adReqLeadGroups t3 ")
                .Append("       where  ")
                .Append("           t1.adReqId = t2.adReqId and ")
                .Append("           t1.adreqTypeId = 3 and  ")
                .Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
                .Append("           t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
                .Append("           t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and  ")
                .Append("           t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 ")
                .Append("                              where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null) ")
                .Append(" ) ")
                .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")
                .Append(" ) R2 ")
                .Append(" where R2.Required = 1 and R2.DocStatus = 'Not Approved' and R2.OverRide='False' ")
            End With

            Dim intLeadCount As Integer = db.RunParamSQLScalar(sb.ToString)

            'Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(SingletonAppSettings.AppSettings("ConString")))
            'Dim da1 As New OleDbDataAdapter(sc1)
            'da1.Fill(ds, "TestDetails")
            'sb.Remove(0, sb.Length)
            Return intLeadCount
        Catch ex As System.Exception
        Finally
        End Try
    End Function
    Public Function CheckIfLeadHasPassedRequiredTestNoProgramVersion(ByVal LeadId As String) As Integer
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String
        Dim currentDate As Date = Date.Now.ToShortDateString
        Try

            With sb
                .Append(" select Count(*) as CountOfLeadFailedRequiredTest from ")
                .Append(" ( ")
                .Append(" select  ")
                .Append(" adReqId,  ")
                .Append(" Descrip,  ")
                .Append(" StartDate,")
                .Append(" EndDate, ")
                .Append(" ActualScore, ")
                .Append(" TestTaken, ")
                .Append(" Comments, ")
                .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append(" Minscore, ")
                .Append(" Required, ")
                .Append(" Case  when ActualScore >= Minscore   then 'True' else 'False' End as Pass ")
                .Append(" from ")
                .Append("   (   ")

                'Get List Of Requirements that are mandatory for all requirement
                .Append("       select  ")
                .Append("           t1.adReqId, ")
                .Append("           t1.Descrip, ")
                .Append("           '" & currentDate & "' as CurrentDate, ")
                .Append("           t2.StartDate, ")
                .Append("           t2.EndDate, ")
                .Append("           (select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as ActualScore, ")
                .Append("           (select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as TestTaken, ")
                .Append("           (select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as Comments, ")
                .Append("           (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as OverRide, ")
                .Append("           t2.Minscore, ")
                .Append("           1 as Required ")
                .Append("       from ")
                .Append("           adReqs t1, ")
                .Append("           adReqsEffectiveDates t2 ")
                .Append("       where  ")
                .Append("           t1.adReqId = t2.adReqId and  ")
                .Append("           t1.adReqTypeId = 1 and  ")
                .Append("           t2.MandatoryRequirement=1 ")
                .Append(" ) ")
                .Append("       R1 where R1.CurrentDate >= R1.StartDate and ")
                .Append("       (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union  ")

                'Get List Of Test that has been assigned to lead group but not part of requirement group
                'or not assigned to a program version
                .Append(" select  ")
                .Append(" adReqId,  ")
                .Append(" Descrip,  ")
                .Append(" StartDate,")
                .Append(" EndDate, ")
                .Append(" ActualScore, ")
                .Append(" TestTaken, ")
                .Append(" Comments, ")
                .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append(" Minscore, ")
                .Append(" Required, ")
                .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass ")
                .Append(" from ")
                .Append("   (   ")
                .Append("       select  ")
                .Append("           t1.adReqId, ")
                .Append("           t1.Descrip, ")
                .Append("           '" & currentDate & "' as CurrentDate, ")
                .Append("           t2.StartDate, ")
                .Append("           t2.EndDate, ")
                .Append("           (select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as ActualScore, ")
                .Append("           (select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as TestTaken, ")
                .Append("           (select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as Comments, ")
                .Append("           (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as OverRide, ")
                .Append("           t2.Minscore, ")
                .Append("           t3.IsRequired as Required ")
                .Append("       from ")
                .Append("           adReqs t1, ")
                .Append("           adReqsEffectiveDates t2, ")
                .Append("           adReqLeadGroups t3 ")
                .Append("       where  ")
                .Append("           t1.adReqId = t2.adReqId and ")
                .Append("           t1.adreqTypeId = 1 and  ")
                .Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
                .Append("           t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
                .Append("           t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and  ")
                .Append("           t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 ")
                .Append("                              where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null) ")
                .Append(" ) ")
                .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")
                .Append(" ) R2 ")
                .Append(" where R2.Required =1 and R2.Pass = 'False' and R2.OverRide='False' ")
            End With

            Dim intLeadCount As Integer = db.RunParamSQLScalar(sb.ToString)

            'Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(SingletonAppSettings.AppSettings("ConString")))
            'Dim da1 As New OleDbDataAdapter(sc1)
            'da1.Fill(ds, "TestDetails")
            'sb.Remove(0, sb.Length)
            Return intLeadCount
        Catch ex As System.Exception
        Finally
        End Try
    End Function
    Public Function CheckIfLeadHasPassedRequiredTestWithProgramVersion(ByVal LeadId As String, ByVal PrgVerId As String) As Integer
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String
        Dim strcurrentDate As Date = Date.Now.ToShortDateString
        Try


            'With sb
            '    .Append("select count(*) from ")
            '    .Append(" ( ")
            '    .Append(" select  ")
            '    .Append(" adReqId,  ")
            '    .Append(" Descrip,  ")
            '    .Append(" StartDate,")
            '    .Append(" EndDate, ")
            '    .Append(" ActualScore, ")
            '    .Append(" TestTaken, ")
            '    .Append(" Comments, ")
            '    .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
            '    .Append(" Minscore, ")
            '    .Append(" Required, ")
            '    .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass ")
            '    .Append(" from ")
            '    .Append("   (   ")

            '    'Get List Of Requirements that are mandatory for all requirement
            '    .Append("       select  ")
            '    .Append("           t1.adReqId, ")
            '    .Append("           t1.Descrip, ")
            '    .Append("           '" & currentDate & "' as CurrentDate, ")
            '    .Append("           t2.StartDate, ")
            '    .Append("           t2.EndDate, ")
            '    .Append("           (select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as ActualScore, ")
            '    .Append("           (select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as TestTaken, ")
            '    .Append("           (select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as Comments, ")
            '    .Append("           (select OverRide from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as OverRide, ")
            '    .Append("           t2.Minscore, ")
            '    .Append("           1 as Required ")
            '    .Append("       from ")
            '    .Append("           adReqs t1, ")
            '    .Append("           adReqsEffectiveDates t2 ")
            '    .Append("       where  ")
            '    .Append("           t1.adReqId = t2.adReqId and  ")
            '    .Append("           t1.adReqTypeId = 1 and  ")
            '    .Append("           t2.MandatoryRequirement=1 ")
            '    .Append(" ) ")
            '    .Append("       R1 where R1.CurrentDate >= R1.StartDate and ")
            '    .Append("       (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            '    .Append(" union  ")

            '    'Get List Of Test that has been assigned to lead group but not part of requirement group
            '    'or not assigned to a program version
            '    .Append(" select  ")
            '    .Append(" adReqId,  ")
            '    .Append(" Descrip,  ")
            '    .Append(" StartDate,")
            '    .Append(" EndDate, ")
            '    .Append(" ActualScore, ")
            '    .Append(" TestTaken, ")
            '    .Append(" Comments, ")
            '    .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
            '    .Append(" Minscore, ")
            '    .Append(" Required, ")
            '    .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass ")
            '    .Append(" from ")
            '    .Append("   (   ")
            '    .Append("       select  ")
            '    .Append("           t1.adReqId, ")
            '    .Append("           t1.Descrip, ")
            '    .Append("           '" & currentDate & "' as CurrentDate, ")
            '    .Append("           t2.StartDate, ")
            '    .Append("           t2.EndDate, ")
            '    .Append("           (select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as ActualScore, ")
            '    .Append("           (select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as TestTaken, ")
            '    .Append("           (select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as Comments, ")
            '    .Append("           (select OverRide from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as OverRide, ")
            '    .Append("           t2.Minscore, ")
            '    .Append("           t3.IsRequired as Required ")
            '    .Append("       from ")
            '    .Append("           adReqs t1, ")
            '    .Append("           adReqsEffectiveDates t2, ")
            '    .Append("           adReqLeadGroups t3 ")
            '    .Append("       where  ")
            '    .Append("           t1.adReqId = t2.adReqId and ")
            '    .Append("           t1.adreqTypeId = 1 and  ")
            '    .Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            '    .Append("           t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            '    .Append("           t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and  ")
            '    .Append("           t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 ")
            '    .Append("                              where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null) ")
            '    .Append(" ) ")
            '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")
            '    .Append(" Union ")
            '    'Get List Of Requirements assigned to program version
            '    'Get List Of Test that has been assigned to lead group but not part of requirement group
            '    'or not assigned to a program version
            '    .Append(" select  ")
            '    .Append(" adReqId,  ")
            '    .Append(" Descrip,  ")
            '    .Append(" StartDate,")
            '    .Append(" EndDate, ")
            '    .Append(" ActualScore, ")
            '    .Append(" TestTaken, ")
            '    .Append(" Comments, ")
            '    .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
            '    .Append(" Minscore, ")
            '    .Append(" Required, ")
            '    .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass ")
            '    .Append(" from ")
            '    .Append("   (   ")
            '    .Append("       select  ")
            '    .Append("           t1.adReqId, ")
            '    .Append("           t1.Descrip, ")
            '    .Append("           '" & currentDate & "' as CurrentDate, ")
            '    .Append("           t2.StartDate, ")
            '    .Append("           t2.EndDate, ")
            '    .Append("           (select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as ActualScore, ")
            '    .Append("           (select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as TestTaken, ")
            '    .Append("           (select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as Comments, ")
            '    .Append("           (select OverRide from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as OverRide, ")
            '    .Append("           t2.Minscore, ")
            '    .Append("           t3.IsRequired as Required ")
            '    .Append("       from ")
            '    .Append("           adReqs t1, ")
            '    .Append("           adReqsEffectiveDates t2, ")
            '    .Append("           adReqLeadGroups t3, ")
            '    .Append("           adPrgVerTestDetails t5 ")
            '    .Append("       where  ")
            '    .Append("           t1.adReqId = t2.adReqId and ")
            '    .Append("           t1.adreqTypeId = 1 and      ")
            '    .Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            '    .Append("           t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            '    .Append("           t1.adReqId = t5.adReqId and t1.PrgVerId = '" & PrgVerId & "' and ")
            '    .Append("           t5.ReqGrpId Is Null  ")
            '    .Append(" ) ")
            '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")

            '    .Append(" Union ")
            '    'Get List Of Requirements assigned to program version and requirements that are part of a group
            '    .Append(" select  ")
            '    .Append(" adReqId,  ")
            '    .Append(" Descrip,  ")
            '    .Append(" StartDate,")
            '    .Append(" EndDate, ")
            '    .Append(" ActualScore, ")
            '    .Append(" TestTaken, ")
            '    .Append(" Comments, ")
            '    .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
            '    .Append(" Minscore, ")
            '    .Append(" Required, ")
            '    .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass ")
            '    .Append(" from ")
            '    .Append("   (   ")
            '    .Append("       select  ")
            '    .Append("           t1.adReqId, ")
            '    .Append("           t1.Descrip, ")
            '    .Append("           '" & currentDate & "' as CurrentDate, ")
            '    .Append("           t2.StartDate, ")
            '    .Append("           t2.EndDate, ")
            '    .Append("           (select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as ActualScore, ")
            '    .Append("           (select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as TestTaken, ")
            '    .Append("           (select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as Comments, ")
            '    .Append("           (select OverRide from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as OverRide, ")
            '    .Append("           t2.Minscore, ")
            '    .Append("           t3.IsRequired as Required ")
            '    .Append("       from ")
            '    .Append("           adReqs t1, ")
            '    .Append("           adReqsEffectiveDates t2, ")
            '    .Append("           adReqLeadGroups t3, ")
            '    .Append("           adPrgVerTestDetails t5 ")
            '    .Append("       where  ")
            '    .Append("           t1.adReqId = t2.adReqId and ")
            '    .Append("           t1.adreqTypeId = 1 and      ")
            '    .Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            '    .Append("           t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            '    .Append("           t1.adReqId = t5.adReqId and t1.PrgVerId = '" & PrgVerId & "' and ")
            '    .Append("          t5.ReqGrpId in (select t1.ReqGrpId from adPrgVerTestDetails t1 where t1.PrgVerId='" & PrgVerId & "' and ReqGrpId is not null)  ")
            '    .Append(" ) ")
            '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")
            '    .Append(" ) R2 ")
            '    .Append(" where R2.Required = 1 and R2.Pass = 'False' and R2.OverRide='False' ")
            'End With

            'Above Code Commented By Balaji on 09/12/2005
            With sb
                .Append(" select OverRide,Count(*) as OverRideCount from ")
                .Append(" ( ")
                .Append(" select Distinct ")
                .Append(" adReqId, ")
                .Append(" Descrip, ")
                ' .Append(" ReqGrpId, ")
                ' .Append(" StartDate,  ")
                '.Append(" EndDate, ")
                '.Append(" ActualScore, ")
                '.Append(" TestTaken, ")
                '.Append(" Comments, ")
                .Append(" Case when OverRide>=1 then 'True' else 'False' end as OverRide, ")
                '.Append(" Minscore, ")
                .Append(" Required,Pass ")
                '.Append(" DocSubmittedCount, ")
                ' .Append(" DocStatusDescrip, ")
                '.Append(" Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount ")
                .Append(" from  ")
                .Append(" ( ")
                ' Get Requirement Group and requirements of mandatory requirement
                .Append(" 	select  ")
                .Append(" 		adReqId, ")
                .Append("		Descrip, ")
                .Append("		ReqGrpId, ")
                .Append("		StartDate, ")
                .Append("		EndDate, ")
                .Append("		ActualScore, ")
                .Append("		TestTaken, ")
                .Append("		Comments, ")
                .Append("		OverRide, ")
                .Append("		Minscore, ")
                .Append("		Required, ")
                .Append("		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		DocSubmittedCount, ")
                .Append("		DocStatusDescrip ")
                .Append("	from  ")
                .Append("		( ")
                .Append("		select ")
                .Append("		t1.adReqId, ")
                .Append("				t1.Descrip, ")
                .Append("				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                .Append("				'" & strcurrentDate & "' as CurrentDate, ")
                .Append("				t2.StartDate, ")
                .Append("				t2.EndDate, ")
                .Append("				(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("					LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("				(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("					LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("				(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("					LeadId='" & LeadId & "' ) as Comments, ")
                .Append("				       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("				        LeadId='" & LeadId & "' ) as override, ")
                .Append("				(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("					LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("				t2.Minscore, ")
                .Append("				1 as Required, ")
                .Append("				(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("				s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("				from  ")
                .Append("				adReqs t1, ")
                .Append("				adReqsEffectiveDates t2 ")
                .Append("				where  ")
                .Append("				t1.adReqId = t2.adReqId and ")
                .Append("				t2.MandatoryRequirement=1 and ")
                .Append("				t1.adReqTypeId in (1) ")
                .Append("				) ")
                .Append("				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("		union ")
                ' Get Requirements assigned to a lead groups,but not assigned 
                '  directly to a program version
                ' or not assigned to a requirement group 
                .Append(" 	select  ")
                .Append(" 		adReqId, ")
                .Append("		Descrip, ")
                .Append("		ReqGrpId, ")
                .Append("		StartDate, ")
                .Append("		EndDate, ")
                .Append("		ActualScore, ")
                .Append("		TestTaken, ")
                .Append("		Comments, ")
                .Append("		OverRide, ")
                .Append("		Minscore, ")
                .Append("		Required, ")
                .Append("		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		DocSubmittedCount, ")
                .Append("		DocStatusDescrip ")
                .Append("		from  ")
                .Append("				( ")
                .Append("						select ")
                .Append("							t1.adReqId, ")
                .Append("							t1.Descrip, ")
                .Append("							'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append("							'" & strcurrentDate & "' as CurrentDate, ")
                .Append("							t2.StartDate, ")
                .Append("							t2.EndDate, ")
                .Append("							(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("								LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("							(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("								LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("							(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("								LeadId='" & LeadId & "' ) as Comments, ")
                .Append("							       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("							        LeadId='" & LeadId & "' ) ")
                .Append("							as override, ")
                .Append("							(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("								LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("							t2.Minscore, ")
                .Append("							t3.Isrequired as Required, ")
                .Append("							(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("							s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("						from  ")
                .Append("							adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3 ")
                .Append("						where ")
                .Append("							t1.adReqId = t2.adReqId and ")
                .Append("							t1.adreqTypeId in (1) and ")
                .Append("							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  and ")
                .Append("						       t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' ) ")
                .Append("							and t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId = '" & PrgVerId & "' ) and  ")
                .Append("							t1.adReqId not in (select Distinct adReqId from adReqGrpDef f1,adLeadByLeadGroups f2 ")
                .Append("							where f1.LeadGrpId = f2.LeadGrpId and f2.LeadId='" & LeadId & "' ) ")
                .Append("					) ")
                .Append("					R1 where R1.CurrentDate >= R1.StartDate and ")
                .Append("					(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("				union ")

                '' Requirements assigned to a lead group and part of requirement group but not assigned to a program version
                '' balaji 09/13/2005


                .Append(" 	select  ")
                .Append(" 		adReqId, ")
                .Append("		Descrip, ")
                .Append("		ReqGrpId, ")
                .Append("		StartDate, ")
                .Append("		EndDate, ")
                .Append("		ActualScore, ")
                .Append("		TestTaken, ")
                .Append("		Comments, ")
                .Append("		OverRide, ")
                .Append("		Minscore, ")
                .Append("		Required, ")
                .Append("		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		DocSubmittedCount, ")
                .Append("		DocStatusDescrip ")
                .Append("		from  ")
                .Append("				( ")
                .Append("						select ")
                .Append("							t1.adReqId, ")
                .Append("							t1.Descrip, ")
                .Append("							case when t4.ReqGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t4.ReqGrpId end as ReqGrpId, ")
                .Append("							'" & strcurrentDate & "' as CurrentDate, ")
                .Append("							t2.StartDate, ")
                .Append("							t2.EndDate, ")
                .Append("							(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("								LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("							(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("								LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("							(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("								LeadId='" & LeadId & "' ) as Comments, ")
                .Append("							       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("							        LeadId='" & LeadId & "' ) ")
                .Append("							as override, ")
                .Append("							(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("								LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("							t2.Minscore, ")
                .Append("							t3.Isrequired as Required, ")
                .Append("							(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("							s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("						from  ")
                .Append("							adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3,adReqGrpDef t4 ")
                .Append("						where ")
                .Append("							t1.adReqId = t2.adReqId and ")
                .Append("							t1.adreqTypeId in (1) and ")
                .Append("							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  and ")
                .Append("                           t1.adReqId = t4.adReqId and t3.LeadGrpId = t4.LeadGrpId and ")
                .Append("						    t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' ) and ")
                .Append("						    t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId = '" & PrgVerId & "') and  ")
                .Append("						    t1.adReqId not in (select Distinct f1.adReqId from adReqGrpDef f1,adLeadByLeadGroups f2,adPrgVerTestDetails f3 ")
                .Append("						                    where f1.LeadGrpId = f2.LeadGrpId and f1.reqGrpId=f3.ReqGrpId and f2.LeadId='" & LeadId & "' ")
                .Append("					                        and f3.prgVerId='" & PrgVerId & "' and f3.ReqGrpId is not null) ")
                .Append("					) ")
                .Append("					R1 where R1.CurrentDate >= R1.StartDate and ")
                .Append("					(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("				union ")


                '' Requirements assigned to a program version
                .Append("			select  ")
                .Append("					adReqId, ")
                .Append("					Descrip, ")
                .Append("					ReqGrpId, ")
                .Append("					StartDate, ")
                .Append("					EndDate, ")
                .Append("					ActualScore, ")
                .Append("					TestTaken, ")
                .Append("						Comments, ")
                .Append("						OverRide, ")
                .Append("						Minscore, ")
                .Append("						Required, ")
                .Append("						Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("						DocSubmittedCount, ")
                .Append("						DocStatusDescrip ")
                .Append("				from  ")
                .Append("					( ")
                .Append("						select ")
                .Append("							t1.adReqId, ")
                .Append("							t1.Descrip, ")
                .Append("						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("						'" & strcurrentDate & "' as CurrentDate, ")
                .Append("						t2.StartDate, ")
                .Append("						t2.EndDate, ")
                .Append("						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("							LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("					(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("							LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("							LeadId='" & LeadId & "' ) as Comments, ")
                .Append("					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("						        LeadId='" & LeadId & "' ) ")
                .Append("						as override, ")
                .Append("						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("							LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("					t2.Minscore, ")
                .Append("						t3.IsRequired as Required, ")
                .Append("						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("						s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("					from  ")
                .Append("							adReqs t1, ")
                .Append("							adReqsEffectiveDates t2, ")
                .Append("							adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
                .Append("						where ")
                .Append("						t1.adReqId = t2.adReqId and ")
                .Append("							t1.adreqTypeId in (1) and ")
                .Append("							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId ")
                .Append("							and t1.adReqId = t5.adReqId and t5.PrgVerId = '" & PrgVerId & "'   and ")
                .Append("					       t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where ")
                .Append("						LeadId='" & LeadId & "' ) ")
                .Append("							and t5.adReqId is not null ")
                .Append("						) ")
                .Append("						R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("					union ")

                'Get Requirement groups assigned to a program version and all requirements part of group

                .Append("			select  ")
                .Append("					adReqId, ")
                .Append("					Descrip, ")
                .Append("					ReqGrpId, ")
                .Append("					StartDate, ")
                .Append("					EndDate, ")
                .Append("					ActualScore, ")
                .Append("					TestTaken, ")
                .Append("						Comments, ")
                .Append("						OverRide, ")
                .Append("						Minscore, ")
                .Append("						Required, ")
                .Append("						Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("						DocSubmittedCount, ")
                .Append("						DocStatusDescrip ")
                .Append("				from  ")
                .Append("					( ")
                .Append("						select ")
                .Append("							t1.adReqId, ")
                .Append("							t1.Descrip, ")
                .Append("						t5.ReqGrpId as reqGrpId, ")
                .Append("						'" & strcurrentDate & "' as CurrentDate, ")
                .Append("						t2.StartDate, ")
                .Append("						t2.EndDate, ")
                .Append("						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("							LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("					(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("							LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("							LeadId='" & LeadId & "' ) as Comments, ")
                .Append("					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("						        LeadId='" & LeadId & "' ) ")
                .Append("						as override, ")
                .Append("						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("							LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("					t2.Minscore, ")
                .Append("						t3.IsRequired as Required, ")
                .Append("						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("						s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("					from  ")
                .Append("						adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3,adPrgVerTestDetails t5,adReqGrpDef t6,adLeadByLeadGroups t7 ")
                .Append("					where ")
                .Append("						t1.adReqId = t2.adReqId and ")
                .Append("					t1.adreqTypeId in (1) and ")
                .Append("					t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and  t3.LeadGrpId = t7.LeadGrpId and ")
                .Append("					t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
                .Append("					and t7.LeadId='" & LeadId & "'  and t5.PrgVerId = '" & PrgVerId & "'  ")
                .Append("		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" ) ")
                .Append(" R2 ")
                .Append(" where R2.Required = 1 and R2.Pass = 'False' ")
                .Append(" ) R3 group by OverRide Having R3.OverRide='False' ")
            End With

            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            Dim studentinfo As New PlacementInfo
            Dim intLeadCount As Integer
            While dr.Read()
                If Not (dr("OverRideCount") Is System.DBNull.Value) Then intLeadCount = CType(dr("OverRideCount"), Integer) Else intLeadCount = 0
            End While
            Return intLeadCount
        Catch ex As System.Exception
        Finally
        End Try
    End Function
    Public Function CheckIfDocumentsApprovedNoProgramVersion(ByVal LeadId As String) As Integer
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String
        Dim currentDate As Date = Date.Now.ToShortDateString
        Try


            With sb
                .Append(" select count(*) from ")
                .Append("   ( ")
                .Append(" select  ")
                .Append(" adReqId  as DocumentId,  ")
                .Append(" Descrip  as DocumentDescrip,  ")
                .Append(" StartDate,")
                .Append(" EndDate, ")
                .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append(" Required, ")
                .Append(" Case when DocumentStatus=1 then 'Approved' else 'NotApproved' end as DocumentStatus ")
                .Append(" from ")
                .Append("   (   ")

                'Get List Of Requirements that are mandatory for all requirement
                .Append("       select  ")
                .Append("           t1.adReqId, ")
                .Append("           t1.Descrip, ")
                .Append("           '" & currentDate & "' as CurrentDate, ")
                .Append("           t2.StartDate, ")
                .Append("           t2.EndDate, ")
                .Append("           (select OverRide from adLeadDocsReceived where DocumentId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as OverRide, ")
                .Append("           1 as Required, ")
                .Append("           (select Distinct s3.SysDocStatusId from adLeadDocsReceived s1,syDocStatuses s2,sySysDocStatuses s3 ")
                .Append("           where s1.DocStatusId = s2.DocStatusId And s2.SysDocStatusId = s3.SysDocStatusId and s3.SysDocStatusId = 1 and s1.LeadId='" & LeadId & "') as DocumentStatus ")
                .Append("       from ")
                .Append("           adReqs t1, ")
                .Append("           adReqsEffectiveDates t2 ")
                .Append("       where  ")
                .Append("           t1.adReqId = t2.adReqId and  ")
                .Append("           t1.adReqTypeId = 3 and  ")
                .Append("           t2.MandatoryRequirement=1 ")
                .Append(" ) ")
                .Append("       R1 where R1.CurrentDate >= R1.StartDate and ")
                .Append("       (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union  ")

                'Get List Of Test that has been assigned to lead group but not part of requirement group
                'or not assigned to a program version
                .Append(" select  ")
                .Append(" adReqId,  ")
                .Append(" Descrip,  ")
                .Append(" StartDate,")
                .Append(" EndDate, ")
                .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append(" Required, ")
                .Append(" Case when DocumentStatus=1 then 'Approved' else 'NotApproved' end as DocumentStatus ")
                .Append(" from ")
                .Append("   (   ")
                .Append("       select  ")
                .Append("           t1.adReqId, ")
                .Append("           t1.Descrip, ")
                .Append("           '" & currentDate & "' as CurrentDate, ")
                .Append("           t2.StartDate, ")
                .Append("           t2.EndDate, ")
                .Append("           (select OverRide from adLeadDocsReceived where DocumentId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as OverRide, ")
                .Append("           t3.IsRequired as Required, ")
                .Append("           (select Distinct s3.SysDocStatusId from adLeadDocsReceived s1,syDocStatuses s2,sySysDocStatuses s3 ")
                .Append("           where s1.DocStatusId = s2.DocStatusId And s2.SysDocStatusId = s3.SysDocStatusId and s3.SysDocStatusId = 1 and s1.LeadId='" & LeadId & "') as DocumentStatus ")
                .Append("       from ")
                .Append("           adReqs t1, ")
                .Append("           adReqsEffectiveDates t2, ")
                .Append("           adReqLeadGroups t3, ")
                .Append("           adLeads t4 ")
                .Append("       where  ")
                .Append("           t1.adReqId = t2.adReqId and ")
                .Append("           t1.adreqTypeId = 3 and  ")
                .Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
                .Append("           t3.LeadGrpId = t4.LeadGrpId and t4.LeadId='" & LeadId & "' and ")
                .Append("           t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and  ")
                .Append("           t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 ")
                .Append("                              where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null) ")
                .Append(" ) ")
                .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")
                .Append(" ) R2 ")
                .Append(" where R2.Required =1 and R2.DocumentStatus = 'NotApproved' and R2.OverRide='False' ")
            End With

            Dim intNotApprovedCount As Integer = db.RunParamSQLScalar(sb.ToString)
            Return intNotApprovedCount

        Catch ex As System.Exception
        Finally
        End Try
    End Function
    Public Function CheckAllApprovedDocumentsWithPrgVersion(ByVal LeadId As String, ByVal PrgVerId As String) As Integer
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String
        Dim strcurrentDate As Date = Date.Now.ToShortDateString
        Try


            With sb
                '.Append(" select count(*) from ")
                '.Append(" ( ")
                '.Append(" select  ")
                '.Append(" adReqId as DocumentId,  ")
                '.Append(" Descrip as DocumentDescrip,  ")
                '.Append(" StartDate,")
                '.Append(" EndDate, ")
                '.Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                '.Append(" Required, ")
                '.Append(" Case when DocumentStatus=1 then 'Approved' else 'NotApproved' end as DocumentStatus ")
                '.Append(" from ")
                '.Append("   (   ")

                ''Get List Of Requirements that are mandatory for all requirement
                '.Append("       select  ")
                '.Append("           t1.adReqId, ")
                '.Append("           t1.Descrip, ")
                '.Append("           '" & currentDate & "' as CurrentDate, ")
                '.Append("           t2.StartDate, ")
                '.Append("           t2.EndDate, ")
                '.Append("           (select OverRide from adLeadDocsReceived where DocumentId=t1.adReqId and ")
                '.Append("           LeadId='" & LeadId & "') as OverRide, ")
                '.Append("           1 as Required, ")
                '.Append("           (select Distinct s3.SysDocStatusId from adLeadDocsReceived s1,syDocStatuses s2,sySysDocStatuses s3 ")
                '.Append("           where s1.DocStatusId = s2.DocStatusId And s2.SysDocStatusId = s3.SysDocStatusId and s3.SysDocStatusId = 1 and s1.LeadId='" & LeadId & "') as DocumentStatus ")
                '.Append("       from ")
                '.Append("           adReqs t1, ")
                '.Append("           adReqsEffectiveDates t2 ")
                '.Append("       where  ")
                '.Append("           t1.adReqId = t2.adReqId and  ")
                '.Append("           t1.adReqTypeId = 3 and  ")
                '.Append("           t2.MandatoryRequirement=1 ")
                '.Append(" ) ")
                '.Append("       R1 where R1.CurrentDate >= R1.StartDate and ")
                '.Append("       (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                '.Append(" union  ")

                ''Get List Of Test that has been assigned to lead group but not part of requirement group
                ''or not assigned to a program version
                '.Append(" select  ")
                '.Append(" adReqId,  ")
                '.Append(" Descrip,  ")
                '.Append(" StartDate,")
                '.Append(" EndDate, ")
                '.Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                '.Append(" Required, ")
                '.Append(" Case when DocumentStatus=1 then 'Approved' else 'NotApproved' end as DocumentStatus ")
                '.Append(" from ")
                '.Append("   (   ")
                '.Append("       select  ")
                '.Append("           t1.adReqId, ")
                '.Append("           t1.Descrip, ")
                '.Append("           '" & currentDate & "' as CurrentDate, ")
                '.Append("           t2.StartDate, ")
                '.Append("           t2.EndDate, ")
                '.Append("           (select OverRide from adLeadDocsReceived where DocumentId=t1.adReqId and ")
                '.Append("           LeadId='" & LeadId & "') as OverRide, ")
                '.Append("           t3.IsRequired as Required, ")
                '.Append("           (select Distinct s3.SysDocStatusId from adLeadDocsReceived s1,syDocStatuses s2,sySysDocStatuses s3 ")
                '.Append("           where s1.DocStatusId = s2.DocStatusId And s2.SysDocStatusId = s3.SysDocStatusId and s3.SysDocStatusId = 1 and s1.LeadId='" & LeadId & "') as DocumentStatus ")
                '.Append("       from ")
                '.Append("           adReqs t1, ")
                '.Append("           adReqsEffectiveDates t2, ")
                '.Append("           adReqLeadGroups t3 ")

                '.Append("       where  ")
                '.Append("           t1.adReqId = t2.adReqId and ")
                '.Append("           t1.adreqTypeId = 3 and  ")
                '.Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
                '.Append("           t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
                '.Append("           t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and  ")
                '.Append("           t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 ")
                '.Append("                              where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null) ")
                '.Append(" ) ")
                '.Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")
                '.Append(" Union ")
                ''Get List Of Requirements assigned to program version
                ''Get List Of Test that has been assigned to lead group but not part of requirement group
                ''or not assigned to a program version
                '.Append(" select  ")
                '.Append(" adReqId,  ")
                '.Append(" Descrip,  ")
                '.Append(" StartDate,")
                '.Append(" EndDate, ")
                '.Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                '.Append(" Required, ")
                '.Append(" Case when DocumentStatus=1 then 'Approved' else 'NotApproved' end as DocumentStatus ")
                '.Append(" from ")
                '.Append("   (   ")
                '.Append("       select  ")
                '.Append("           t1.adReqId, ")
                '.Append("           t1.Descrip, ")
                '.Append("           '" & currentDate & "' as CurrentDate, ")
                '.Append("           t2.StartDate, ")
                '.Append("           t2.EndDate, ")
                '.Append("           (select OverRide from adLeadDocsReceived where DocumentId=t1.adReqId and ")
                '.Append("           LeadId='" & LeadId & "') as OverRide, ")
                '.Append("           t2.Minscore, ")
                '.Append("           t3.IsRequired as Required, ")
                '.Append("           (select Distinct s3.SysDocStatusId from adLeadDocsReceived s1,syDocStatuses s2,sySysDocStatuses s3 ")
                '.Append("           where s1.DocStatusId = s2.DocStatusId And s2.SysDocStatusId = s3.SysDocStatusId and s3.SysDocStatusId = 1 and s1.LeadId='" & LeadId & "') as DocumentStatus ")
                '.Append("       from ")
                '.Append("           adReqs t1, ")
                '.Append("           adReqsEffectiveDates t2, ")
                '.Append("           adReqLeadGroups t3, ")
                '.Append("           adPrgVerTestDetails t5 ")
                '.Append("       where  ")
                '.Append("           t1.adReqId = t2.adReqId and ")
                '.Append("           t1.adreqTypeId = 3 and      ")
                '.Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
                '.Append("           t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
                '.Append("           t1.adReqId = t5.adReqId and t1.PrgVerId = '" & PrgVerId & "' and ")
                '.Append("           t5.ReqGrpId Is Null  ")
                '.Append(" ) ")
                '.Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")

                '.Append(" Union ")
                ''Get List Of Requirements assigned to program version and requirements that are part of a group
                '.Append(" select  ")
                '.Append(" adReqId,  ")
                '.Append(" Descrip,  ")
                '.Append(" StartDate,")
                '.Append(" EndDate, ")
                '.Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                '.Append(" Required, ")
                '.Append(" Case when DocumentStatus=1 then 'Approved' else 'NotApproved' end as DocumentStatus ")
                '.Append(" from ")
                '.Append("   (   ")
                '.Append("       select  ")
                '.Append("           t1.adReqId, ")
                '.Append("           t1.Descrip, ")
                '.Append("           '" & currentDate & "' as CurrentDate, ")
                '.Append("           t2.StartDate, ")
                '.Append("           t2.EndDate, ")
                '.Append("           (select OverRide from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                '.Append("           LeadId='" & LeadId & "') as OverRide, ")
                '.Append("           t3.IsRequired as Required, ")
                '.Append("           (select Distinct s3.SysDocStatusId from adLeadDocsReceived s1,syDocStatuses s2,sySysDocStatuses s3 ")
                '.Append("           where s1.DocStatusId = s2.DocStatusId And s2.SysDocStatusId = s3.SysDocStatusId and s3.SysDocStatusId = 1 and s1.LeadId='" & LeadId & "') as DocumentStatus ")
                '.Append("       from ")
                '.Append("           adReqs t1, ")
                '.Append("           adReqsEffectiveDates t2, ")
                '.Append("           adReqLeadGroups t3, ")
                '.Append("           adPrgVerTestDetails t5 ")
                '.Append("       where  ")
                '.Append("           t1.adReqId = t2.adReqId and ")
                '.Append("           t1.adreqTypeId = 3 and      ")
                '.Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
                '.Append("           t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
                '.Append("           t1.adReqId = t5.adReqId and t1.PrgVerId = '" & PrgVerId & "' and ")
                '.Append("          t5.ReqGrpId in (select t1.ReqGrpId from adPrgVerTestDetails t1 where t1.PrgVerId='" & PrgVerId & "' and ReqGrpId is not null)  ")
                '.Append(" ) ")
                '.Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")
                '.Append(" ) R2 ")
                '.Append(" where R2.Required =1 and R2.DocumentStatus = 'NotApproved' and R2.OverRide='False' ")

                .Append(" select Count(*) from ")
                .Append(" ( ")
                .Append(" select Distinct ")
                .Append(" adReqId, ")
                .Append(" Descrip, ")
                ' .Append(" ReqGrpId, ")
                ' .Append(" StartDate,  ")
                '.Append(" EndDate, ")
                '.Append(" ActualScore, ")
                '.Append(" TestTaken, ")
                '.Append(" Comments, ")
                .Append(" Case when OverRide>=1 then 'True' else 'False' end as OverRide, ")
                '.Append(" Minscore, ")
                .Append(" Required,Pass, ")
                '.Append(" DocSubmittedCount, ")
                ' .Append(" DocStatusDescrip, ")
                .Append(" Case when DocStatusDescrip=1 then 'Approved' else 'NotApproved' end as DocStat ")
                .Append(" from  ")
                .Append(" ( ")
                ' Get Requirement Group and requirements of mandatory requirement
                .Append(" 	select  ")
                .Append(" 		adReqId, ")
                .Append("		Descrip, ")
                .Append("		ReqGrpId, ")
                .Append("		StartDate, ")
                .Append("		EndDate, ")
                .Append("		ActualScore, ")
                .Append("		TestTaken, ")
                .Append("		Comments, ")
                .Append("		OverRide, ")
                .Append("		Minscore, ")
                .Append("		Required, ")
                .Append("		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		DocSubmittedCount, ")
                .Append("		DocStatusDescrip ")
                .Append("	from  ")
                .Append("		( ")
                .Append("		select ")
                .Append("		t1.adReqId, ")
                .Append("				t1.Descrip, ")
                .Append("				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                .Append("				'" & strcurrentDate & "' as CurrentDate, ")
                .Append("				t2.StartDate, ")
                .Append("				t2.EndDate, ")
                .Append("				(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("					LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("				(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("					LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("				(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("					LeadId='" & LeadId & "' ) as Comments, ")
                .Append("				       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("				        LeadId='" & LeadId & "' ) as override, ")
                .Append("				(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("					LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("				t2.Minscore, ")
                .Append("				1 as Required, ")
                .Append("				(select Distinct s1.SysDocStatusId from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("				s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("				from  ")
                .Append("				adReqs t1, ")
                .Append("				adReqsEffectiveDates t2 ")
                .Append("				where  ")
                .Append("				t1.adReqId = t2.adReqId and ")
                .Append("				t2.MandatoryRequirement=1 and ")
                .Append("				t1.adReqTypeId in (3) ")
                .Append("				) ")
                .Append("				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("		union ")
                ' Get Requirements assigned to a lead groups,but not assigned 
                '  directly to a program version
                ' or not assigned to a requirement group 
                .Append(" 	select  ")
                .Append(" 		adReqId, ")
                .Append("		Descrip, ")
                .Append("		ReqGrpId, ")
                .Append("		StartDate, ")
                .Append("		EndDate, ")
                .Append("		ActualScore, ")
                .Append("		TestTaken, ")
                .Append("		Comments, ")
                .Append("		OverRide, ")
                .Append("		Minscore, ")
                .Append("		Required, ")
                .Append("		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		DocSubmittedCount, ")
                .Append("		DocStatusDescrip ")
                .Append("		from  ")
                .Append("				( ")
                .Append("						select ")
                .Append("							t1.adReqId, ")
                .Append("							t1.Descrip, ")
                .Append("							'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append("							'" & strcurrentDate & "' as CurrentDate, ")
                .Append("							t2.StartDate, ")
                .Append("							t2.EndDate, ")
                .Append("							(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("								LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("							(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("								LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("							(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("								LeadId='" & LeadId & "' ) as Comments, ")
                .Append("							       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("							        LeadId='" & LeadId & "' ) ")
                .Append("							as override, ")
                .Append("							(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("								LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("							t2.Minscore, ")
                .Append("							t3.Isrequired as Required, ")
                .Append("							(select Distinct s1.SysDocStatusId from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("							s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("						from  ")
                .Append("							adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3 ")
                .Append("						where ")
                .Append("							t1.adReqId = t2.adReqId and ")
                .Append("							t1.adreqTypeId in (3) and ")
                .Append("							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  and ")
                .Append("						       t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' ) ")
                .Append("							and t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId = '" & PrgVerId & "' ) and  ")
                .Append("							t1.adReqId not in (select Distinct adReqId from adReqGrpDef f1,adLeadByLeadGroups f2 ")
                .Append("							where f1.LeadGrpId = f2.LeadGrpId and f2.LeadId='" & LeadId & "' ) ")
                .Append("					) ")
                .Append("					R1 where R1.CurrentDate >= R1.StartDate and ")
                .Append("					(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("				union ")

                '' Requirements assigned to a lead group and part of requirement group but not assigned to a program version
                '' balaji 09/13/2005


                .Append(" 	select  ")
                .Append(" 		adReqId, ")
                .Append("		Descrip, ")
                .Append("		ReqGrpId, ")
                .Append("		StartDate, ")
                .Append("		EndDate, ")
                .Append("		ActualScore, ")
                .Append("		TestTaken, ")
                .Append("		Comments, ")
                .Append("		OverRide, ")
                .Append("		Minscore, ")
                .Append("		Required, ")
                .Append("		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		DocSubmittedCount, ")
                .Append("		DocStatusDescrip ")
                .Append("		from  ")
                .Append("				( ")
                .Append("						select ")
                .Append("							t1.adReqId, ")
                .Append("							t1.Descrip, ")
                .Append("							case when t4.ReqGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t4.ReqGrpId end as ReqGrpId, ")
                .Append("							'" & strcurrentDate & "' as CurrentDate, ")
                .Append("							t2.StartDate, ")
                .Append("							t2.EndDate, ")
                .Append("							(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("								LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("							(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("								LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("							(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("								LeadId='" & LeadId & "' ) as Comments, ")
                .Append("							       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("							        LeadId='" & LeadId & "' ) ")
                .Append("							as override, ")
                .Append("							(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("								LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("							t2.Minscore, ")
                .Append("							t3.Isrequired as Required, ")
                .Append("							(select Distinct s1.SysDocStatusId from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("							s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("						from  ")
                .Append("							adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3,adReqGrpDef t4 ")
                .Append("						where ")
                .Append("							t1.adReqId = t2.adReqId and ")
                .Append("							t1.adreqTypeId in (3) and ")
                .Append("							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  and ")
                .Append("                           t1.adReqId = t4.adReqId and t3.LeadGrpId = t4.LeadGrpId and ")
                .Append("						    t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' ) and ")
                .Append("						    t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId = '" & PrgVerId & "') and  ")
                .Append("						    t1.adReqId not in (select Distinct f1.adReqId from adReqGrpDef f1,adLeadByLeadGroups f2,adPrgVerTestDetails f3 ")
                .Append("						                    where f1.LeadGrpId = f2.LeadGrpId and f1.reqGrpId=f3.ReqGrpId and f2.LeadId='" & LeadId & "' ")
                .Append("					                        and f3.prgVerId='" & PrgVerId & "' and f3.ReqGrpId is not null) ")
                .Append("					) ")
                .Append("					R1 where R1.CurrentDate >= R1.StartDate and ")
                .Append("					(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("				union ")


                '' Requirements assigned to a program version
                .Append("			select  ")
                .Append("					adReqId, ")
                .Append("					Descrip, ")
                .Append("					ReqGrpId, ")
                .Append("					StartDate, ")
                .Append("					EndDate, ")
                .Append("					ActualScore, ")
                .Append("					TestTaken, ")
                .Append("						Comments, ")
                .Append("						OverRide, ")
                .Append("						Minscore, ")
                .Append("						Required, ")
                .Append("						Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("						DocSubmittedCount, ")
                .Append("						DocStatusDescrip ")
                .Append("				from  ")
                .Append("					( ")
                .Append("						select ")
                .Append("							t1.adReqId, ")
                .Append("							t1.Descrip, ")
                .Append("						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("						'" & strcurrentDate & "' as CurrentDate, ")
                .Append("						t2.StartDate, ")
                .Append("						t2.EndDate, ")
                .Append("						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("							LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("					(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("							LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("							LeadId='" & LeadId & "' ) as Comments, ")
                .Append("					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("						        LeadId='" & LeadId & "' ) ")
                .Append("						as override, ")
                .Append("						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("							LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("					t2.Minscore, ")
                .Append("						t3.IsRequired as Required, ")
                .Append("						(select Distinct s1.SysDocStatusId from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("						s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("					from  ")
                .Append("							adReqs t1, ")
                .Append("							adReqsEffectiveDates t2, ")
                .Append("							adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
                .Append("						where ")
                .Append("						t1.adReqId = t2.adReqId and ")
                .Append("							t1.adreqTypeId in (3) and ")
                .Append("							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId ")
                .Append("							and t1.adReqId = t5.adReqId and t5.PrgVerId = '" & PrgVerId & "'   and ")
                .Append("					       t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where ")
                .Append("						LeadId='" & LeadId & "' ) ")
                .Append("							and t5.adReqId is not null ")
                .Append("						) ")
                .Append("						R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("					union ")

                'Get Requirement groups assigned to a program version and all requirements part of group

                .Append("			select  ")
                .Append("					adReqId, ")
                .Append("					Descrip, ")
                .Append("					ReqGrpId, ")
                .Append("					StartDate, ")
                .Append("					EndDate, ")
                .Append("					ActualScore, ")
                .Append("					TestTaken, ")
                .Append("						Comments, ")
                .Append("						OverRide, ")
                .Append("						Minscore, ")
                .Append("						Required, ")
                .Append("						Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("						DocSubmittedCount, ")
                .Append("						DocStatusDescrip ")
                .Append("				from  ")
                .Append("					( ")
                .Append("						select ")
                .Append("							t1.adReqId, ")
                .Append("							t1.Descrip, ")
                .Append("						t5.ReqGrpId as reqGrpId, ")
                .Append("						'" & strcurrentDate & "' as CurrentDate, ")
                .Append("						t2.StartDate, ")
                .Append("						t2.EndDate, ")
                .Append("						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("							LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("					(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("							LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("							LeadId='" & LeadId & "' ) as Comments, ")
                .Append("					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("						        LeadId='" & LeadId & "' ) ")
                .Append("						as override, ")
                .Append("						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("							LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("					t2.Minscore, ")
                .Append("						t3.IsRequired as Required, ")
                .Append("						(select Distinct s1.SysDocStatusId from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("						s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("					from  ")
                .Append("						adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3,adPrgVerTestDetails t5,adReqGrpDef t6,adLeadByLeadGroups t7 ")
                .Append("					where ")
                .Append("						t1.adReqId = t2.adReqId and ")
                .Append("					t1.adreqTypeId in (3) and ")
                .Append("					t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and  t3.LeadGrpId = t7.LeadGrpId and ")
                .Append("					t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
                .Append("					and t7.LeadId='" & LeadId & "'  and t5.PrgVerId = '" & PrgVerId & "'  ")
                .Append("		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" ) ")
                .Append(" R2 ")
                .Append(" ) R3 where R3.Required =1  and  R3.DocStat = 'NotApproved' and R3.OverRide='False' ")
            End With

            Dim intLeadCount As Integer = db.RunParamSQLScalar(sb.ToString)
            Return intLeadCount
        Catch ex As System.Exception
        Finally
        End Try
    End Function
    Public Function GetGridRequirementDetailsByEffectiveDates(ByVal LeadId As String, ByVal PrgVerId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String
        Dim strCurrentdate As Date = Date.Now.ToShortDateString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Try
            'commented by balaji on 09/12/2005
            With sb
                .Append(" select Distinct ")
                .Append(" adReqId, ")
                .Append(" Descrip, ")
                .Append(" ActualScore, ")
                .Append(" TestTaken, ")
                .Append(" Comments, ")
                .Append(" Case when OverRide>=1 then 'True' else 'False' end as OverRide, ")
                .Append(" Case when (select count(*) from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') >=1 then ")
                .Append(" (select MinScore from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') ")
                .Append(" else MinScore end as MinScore, ")
                .Append(" Required,Pass, ")
                .Append(" Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount ")
                .Append(" from  ")
                .Append(" ( ")
                ' Get Requirement Group and requirements of mandatory requirement
                .Append(" 	select  ")
                .Append(" 		adReqId, ")
                .Append("		Descrip, ")
                .Append("		ReqGrpId, ")
                .Append("		StartDate, ")
                .Append("		EndDate, ")
                .Append("		ActualScore, ")
                .Append("		TestTaken, ")
                .Append("		Comments, ")
                .Append("		OverRide, ")
                .Append("		Minscore, ")
                .Append("		Required, ")
                .Append("		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		DocSubmittedCount, ")
                .Append("		DocStatusDescrip ")
                .Append("	from  ")
                .Append("		( ")
                .Append("		select ")
                .Append("		t1.adReqId, ")
                .Append("				t1.Descrip, ")
                .Append("				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                .Append("				'" & strCurrentdate & "' as CurrentDate, ")
                .Append("				t2.StartDate, ")
                .Append("				t2.EndDate, ")
                .Append("				(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("					LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("				(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("					LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("				(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("					LeadId='" & LeadId & "' ) as Comments, ")
                .Append("				       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("				        LeadId='" & LeadId & "' ) as override, ")
                .Append("				(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("					LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("				t2.Minscore, ")
                .Append("				1 as Required, ")
                .Append("				(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("				s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("				from  ")
                .Append("				adReqs t1, ")
                .Append("				adReqsEffectiveDates t2 ")
                .Append("				where  ")
                .Append("				t1.adReqId = t2.adReqId and ")
                .Append("				t2.MandatoryRequirement=1 and ")
                .Append("				t1.adReqTypeId in (1) ")
                .Append("				) ")
                .Append("				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("		union ")
                ' Get Requirements assigned to a lead groups,but not assigned 
                '  directly to a program version
                ' or not assigned to a requirement group 
                .Append(" 	select  ")
                .Append(" 		adReqId, ")
                .Append("		Descrip, ")
                .Append("		ReqGrpId, ")
                .Append("		StartDate, ")
                .Append("		EndDate, ")
                .Append("		ActualScore, ")
                .Append("		TestTaken, ")
                .Append("		Comments, ")
                .Append("		OverRide, ")
                .Append("		Minscore, ")
                .Append("		Required, ")
                .Append("		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		DocSubmittedCount, ")
                .Append("		DocStatusDescrip ")
                .Append("		from  ")
                .Append("				( ")
                .Append("						select ")
                .Append("							t1.adReqId, ")
                .Append("							t1.Descrip, ")
                .Append("							'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append("							'" & strCurrentdate & "' as CurrentDate, ")
                .Append("							t2.StartDate, ")
                .Append("							t2.EndDate, ")
                .Append("							(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("								LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("							(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("								LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("							(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("								LeadId='" & LeadId & "' ) as Comments, ")
                .Append("							       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("							        LeadId='" & LeadId & "' ) ")
                .Append("							as override, ")
                .Append("							(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("								LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("							t2.Minscore, ")
                .Append("							t3.Isrequired as Required, ")
                .Append("							(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("							s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("						from  ")
                .Append("							adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3 ")
                .Append("						where ")
                .Append("							t1.adReqId = t2.adReqId and ")
                .Append("							t1.adreqTypeId in (1) and ")
                .Append("							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  and ")
                .Append("						       t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' ) ")
                .Append("							and t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId = '" & PrgVerId & "' ) and  ")
                .Append("							t1.adReqId not in (select Distinct adReqId from adReqGrpDef f1,adLeadByLeadGroups f2 ")
                .Append("							where f1.LeadGrpId = f2.LeadGrpId and f2.LeadId='" & LeadId & "' ) ")
                .Append("					) ")
                .Append("					R1 where R1.CurrentDate >= R1.StartDate and ")
                .Append("					(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("				union ")
                '' Requirements assigned to a lead group and part of requirement group but not assigned to a program version
                '' balaji 09/13/2005
                .Append(" 	select  ")
                .Append(" 		adReqId, ")
                .Append("		Descrip, ")
                .Append("		ReqGrpId, ")
                .Append("		StartDate, ")
                .Append("		EndDate, ")
                .Append("		ActualScore, ")
                .Append("		TestTaken, ")
                .Append("		Comments, ")
                .Append("		OverRide, ")
                .Append("		Minscore, ")
                .Append("		Required, ")
                .Append("		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		DocSubmittedCount, ")
                .Append("		DocStatusDescrip ")
                .Append("		from  ")
                .Append("				( ")
                .Append("						select ")
                .Append("							t1.adReqId, ")
                .Append("							t1.Descrip, ")
                .Append("							case when t4.ReqGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t4.ReqGrpId end as ReqGrpId, ")
                .Append("							'" & strCurrentdate & "' as CurrentDate, ")
                .Append("							t2.StartDate, ")
                .Append("							t2.EndDate, ")
                .Append("							(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("								LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("							(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("								LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("							(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("								LeadId='" & LeadId & "' ) as Comments, ")
                .Append("							       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("							        LeadId='" & LeadId & "' ) ")
                .Append("							as override, ")
                .Append("							(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("								LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("							t2.Minscore, ")
                .Append("							t3.Isrequired as Required, ")
                .Append("							(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("							s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("						from  ")
                .Append("							adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3,adReqGrpDef t4 ")
                .Append("						where ")
                .Append("							t1.adReqId = t2.adReqId and ")
                .Append("							t1.adreqTypeId in (1) and ")
                .Append("							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  and ")
                .Append("                           t1.adReqId = t4.adReqId and t3.LeadGrpId = t4.LeadGrpId and ")
                .Append("						    t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' ) and ")
                .Append("						    t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId = '" & PrgVerId & "') and  ")
                .Append("						    t1.adReqId not in (select Distinct f1.adReqId from adReqGrpDef f1,adLeadByLeadGroups f2,adPrgVerTestDetails f3 ")
                .Append("						                    where f1.LeadGrpId = f2.LeadGrpId and f1.reqGrpId=f3.ReqGrpId and f2.LeadId='" & LeadId & "' ")
                .Append("					                        and f3.prgVerId='" & PrgVerId & "' and f3.ReqGrpId is not null) ")
                .Append("					) ")
                .Append("					R1 where R1.CurrentDate >= R1.StartDate and ")
                .Append("					(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("				union ")
                '' Requirements assigned to a program version
                .Append("			select  ")
                .Append("					adReqId, ")
                .Append("					Descrip, ")
                .Append("					ReqGrpId, ")
                .Append("					StartDate, ")
                .Append("					EndDate, ")
                .Append("					ActualScore, ")
                .Append("					TestTaken, ")
                .Append("						Comments, ")
                .Append("						OverRide, ")
                .Append("						Minscore, ")
                .Append("						Required, ")
                .Append("						Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("						DocSubmittedCount, ")
                .Append("						DocStatusDescrip ")
                .Append("				from  ")
                .Append("					( ")
                .Append("						select ")
                .Append("							t1.adReqId, ")
                .Append("							t1.Descrip, ")
                .Append("						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("						'" & strCurrentdate & "' as CurrentDate, ")
                .Append("						t2.StartDate, ")
                .Append("						t2.EndDate, ")
                .Append("						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("							LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("					(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("							LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("							LeadId='" & LeadId & "' ) as Comments, ")
                .Append("					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("						        LeadId='" & LeadId & "' ) ")
                .Append("						as override, ")
                .Append("						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("							LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("					t2.Minscore, ")
                .Append("						t3.IsRequired as Required, ")
                .Append("						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("						s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("					from  ")
                .Append("							adReqs t1, ")
                .Append("							adReqsEffectiveDates t2, ")
                .Append("							adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
                .Append("						where ")
                .Append("						t1.adReqId = t2.adReqId and ")
                .Append("							t1.adreqTypeId in (1) and ")
                .Append("							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId ")
                .Append("							and t1.adReqId = t5.adReqId and t5.PrgVerId = '" & PrgVerId & "'   and ")
                .Append("					       t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where ")
                .Append("						LeadId='" & LeadId & "' ) ")
                .Append("							and t5.adReqId is not null ")
                .Append("						) ")
                .Append("						R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("					union ")
                'Get Requirement groups assigned to a program version and all requirements part of group
                .Append("			select  ")
                .Append("					adReqId, ")
                .Append("					Descrip, ")
                .Append("					ReqGrpId, ")
                .Append("					StartDate, ")
                .Append("					EndDate, ")
                .Append("					ActualScore, ")
                .Append("					TestTaken, ")
                .Append("						Comments, ")
                .Append("						OverRide, ")
                .Append("						Minscore, ")
                .Append("						Required, ")
                .Append("						Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("						DocSubmittedCount, ")
                .Append("						DocStatusDescrip ")
                .Append("				from  ")
                .Append("					( ")
                .Append("						select ")
                .Append("							t1.adReqId, ")
                .Append("							t1.Descrip, ")
                .Append("						t5.ReqGrpId as reqGrpId, ")
                .Append("						'" & strCurrentdate & "' as CurrentDate, ")
                .Append("						t2.StartDate, ")
                .Append("						t2.EndDate, ")
                .Append("						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("							LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("					(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("							LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("							LeadId='" & LeadId & "' ) as Comments, ")
                .Append("					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("						        LeadId='" & LeadId & "' ) ")
                .Append("						as override, ")
                .Append("						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("							LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("					t2.Minscore, ")
                .Append("						t3.IsRequired as Required, ")
                .Append("						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("						s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("					from  ")
                .Append("						adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3,adPrgVerTestDetails t5,adReqGrpDef t6,adLeadByLeadGroups t7 ")
                .Append("					where ")
                .Append("						t1.adReqId = t2.adReqId and ")
                .Append("					t1.adreqTypeId in (1) and ")
                .Append("					t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and  t3.LeadGrpId = t7.LeadGrpId and ")
                .Append("					t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
                .Append("					and t7.LeadId='" & LeadId & "'  and t5.PrgVerId = '" & PrgVerId & "'  ")
                .Append("		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" ) ")
                .Append(" R2 ")
            End With
            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As System.Exception
        Finally
            'db.CloseConnection()
        End Try
        Return Nothing
    End Function
    Public Function GetAllStandardRequirementsByEffectiveDatesForOverride(ByVal LeadId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String
        Dim CurrentDate As Date = Date.Now.ToShortDateString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Try


            With sb
                .Append(" select distinct adReqId,Descrip,OverRide,Pass,DocumentStatus,ReqTypeDescrip ")
                .Append(" from ( ")
                .Append(" select Distinct ")
                .Append(" adReqId,  ")
                .Append(" Descrip,  ")
                .Append(" StartDate,")
                .Append(" EndDate, ")
                .Append(" ActualScore, ")
                .Append(" TestTaken, ")
                .Append(" Comments, ")
                .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append(" Minscore, ")
                .Append(" Required, ")
                .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
                .Append(" case when DocStatusDescrip='Approved' then 'Yes' else 'No' end as DocumentStatus, ")
                .Append(" ReqTypeDescrip ")
                .Append(" from ")
                .Append("   (   ")
                'Get List Of Requirements that are mandatory for all requirement
                .Append("       select  ")
                .Append("           t1.adReqId, ")
                .Append("           t1.Descrip, ")
                .Append("           '" & CurrentDate & "' as CurrentDate, ")
                .Append("           t2.StartDate, ")
                .Append("           t2.EndDate, ")
                .Append("           (select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as ActualScore, ")
                .Append("           (select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as TestTaken, ")
                .Append("           (select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as Comments, ")
                .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("                    LeadId='" & LeadId & "') ")
                .Append("            as override, ")
                .Append("           t2.Minscore, ")
                .Append("           1 as Required, ")
                .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
                .Append(" t4.Descrip as ReqTypeDescrip ")
                .Append("       from ")
                .Append("           adReqs t1, ")
                .Append("           adReqsEffectiveDates t2,adReqTypes t4 ")
                .Append("       where  ")
                .Append("           t1.adReqId = t2.adReqId and t1.adReqTypeId = t4.adReqTypeId and ")
                .Append("           t1.adReqTypeId in (1,3) and  ")
                .Append("           t2.MandatoryRequirement=1 ")
                .Append(" ) ")
                .Append("       R1 where R1.CurrentDate >= R1.StartDate and ")
                .Append("       (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union  ")
                'Get List Of Test that has been assigned to lead group but not part of requirement group
                'or not assigned to a program version
                .Append(" select  ")
                .Append(" adReqId,  ")
                .Append(" Descrip,  ")
                .Append(" StartDate,")
                .Append(" EndDate, ")
                .Append(" ActualScore, ")
                .Append(" TestTaken, ")
                .Append(" Comments, ")
                .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append(" Minscore, ")
                .Append(" Required, ")
                .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
                .Append(" DocStatusDescrip,ReqTypeDescrip ")
                .Append(" from ")
                .Append("   (   ")
                .Append("       select  ")
                .Append("           t1.adReqId, ")
                .Append("           t1.Descrip, ")
                .Append("           '" & CurrentDate & "' as CurrentDate, ")
                .Append("           t2.StartDate, ")
                .Append("           t2.EndDate, ")
                .Append("           (select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as ActualScore, ")
                .Append("           (select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as TestTaken, ")
                .Append("           (select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("           LeadId='" & LeadId & "') as Comments, ")
                .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("                    LeadId='" & LeadId & "') ")
                .Append("            as override, ")
                .Append("           t2.Minscore, ")
                .Append("           t3.IsRequired as Required, ")
                .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
                .Append(" t4.Descrip as ReqTypeDescrip ")
                .Append("       from ")
                .Append("           adReqs t1, ")
                .Append("           adReqsEffectiveDates t2, ")
                .Append("           adReqLeadGroups t3,adReqTypes t4 ")
                .Append("       where  ")
                .Append("           t1.adReqId = t2.adReqId and ")
                .Append("           t1.adreqTypeId in (1,3) and t1.adReqTypeId = t4.adReqTypeId and  ")
                .Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
                .Append("           t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and ")
                .Append("           t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and  ")
                .Append("           t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 ")
                .Append("                              where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null) ")
                .Append(" ) ")
                .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")
                .Append(" ) R2 ")
            End With
            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As System.Exception
        Finally
        End Try
        Return Nothing
    End Function
    Public Function GetGridRequirementDetailsByEffectiveDatesForOverRide(ByVal LeadId As String, ByVal PrgVerId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String
        Dim Currentdate As Date = Date.Now.ToShortDateString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Try

            With sb
                'Get All Requirements That Are Not Part Of Any Group
                .Append(" select Distinct ")
                .Append(" adReqId, ")
                .Append(" Descrip, ")
                .Append(" Case when OverRide>=1 then 'True' else 'False' end as OverRide, ")
                .Append(" case when DocStatusDescrip='Approved' then 'Yes' else 'No' end as DocumentStatus, ")
                .Append(" ReqTypeDescrip, ")
                .Append(" Case  when ActualScore >= Minscore   then 'True' else 'False' End as Pass ")
                .Append(" from  ")
                .Append(" ( ")
                .Append(" select  ")
                .Append(" adReqId, ")
                .Append(" Descrip, ")
                .Append(" ReqGrpId, ")
                .Append(" StartDate, ")
                .Append(" EndDate, ")
                .Append(" ActualScore, ")
                .Append(" TestTaken, ")
                .Append(" Comments, ")
                .Append(" OverRide, ")
                .Append(" Minscore, ")
                .Append(" Required, ")
                .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
                .Append(" DocSubmittedCount, ")
                .Append(" DocStatusDescrip,ReqTypeDescrip ")
                .Append(" from  ")
                .Append(" ( ")
                ' Get Requirement Group and requirements of mandatory requirement
                .Append(" select ")
                .Append("	t1.adReqId, ")
                .Append("	t1.Descrip, ")
                .Append("	case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("		(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("		else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                .Append("	'" & Currentdate & "' as CurrentDate, ")
                .Append("	t2.StartDate, ")
                .Append("	t2.EndDate, ")
                .Append("	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("		LeadId='" & LeadId & "' ) as ActualScore,  ")
                .Append("	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("		LeadId='" & LeadId & "' ) as TestTaken,  ")
                .Append("	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("		LeadId='" & LeadId & "' ) as Comments,  ")
                .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("                    LeadId='" & LeadId & "') ")
                .Append("            as override, ")
                .Append("	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("		LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("	t2.Minscore, ")
                .Append("	1 as Required, ")
                .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t4.Descrip as ReqTypeDescrip ")
                .Append(" from  ")
                .Append("	adReqs t1, ")
                .Append("	adReqsEffectiveDates t2,adReqTypes t4 ")
                .Append(" where  ")
                .Append("	t1.adReqId = t2.adReqId and ")
                .Append("	t2.MandatoryRequirement=1 and t1.adReqTypeId = t4.adReqTypeId and ")
                .Append("	t1.adReqTypeId in (1,3) ")
                .Append(" ) ")
                .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                ' Get Requirements assigned to a lead groups,but not assigned directly to a program version
                ' or not assigned to a requirement group 
                .Append(" select  ")
                .Append(" adReqId, ")
                .Append(" Descrip, ")
                .Append(" ReqGrpId, ")
                .Append(" StartDate, ")
                .Append(" EndDate, ")
                .Append(" ActualScore, ")
                .Append(" TestTaken, ")
                .Append(" Comments, ")
                .Append(" OverRide, ")
                .Append(" Minscore, ")
                .Append(" Required, ")
                .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
                .Append(" DocSubmittedCount, ")
                .Append(" DocStatusDescrip,ReqTypeDescrip ")
                .Append(" from  ")
                .Append(" ( ")
                ' Get Requirement Group and requirements of mandatory requirement
                .Append(" select ")
                .Append("	t1.adReqId, ")
                .Append("	t1.Descrip, ")
                .Append("	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append("	'" & Currentdate & "' as CurrentDate, ")
                .Append("	t2.StartDate, ")
                .Append("	t2.EndDate, ")
                .Append("	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("		LeadId='" & LeadId & "' ) as ActualScore,  ")
                .Append("	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("		LeadId='" & LeadId & "' ) as TestTaken,  ")
                .Append("	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("		LeadId='" & LeadId & "' ) as Comments,  ")
                .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("                    LeadId='" & LeadId & "') ")
                .Append("            as override, ")
                .Append("	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("		LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("	t2.Minscore, ")
                .Append("	t3.Isrequired as Required, ")
                .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t4.Descrip as ReqTypeDescrip ")
                .Append(" from  ")
                .Append("	adReqs t1, ")
                .Append("		adReqsEffectiveDates t2, ")
                .Append("		adReqLeadGroups t3,adReqTypes t4 ")
                .Append("	where ")
                .Append("		t1.adReqId = t2.adReqId and ")
                .Append("		t1.adreqTypeId in (1,3) and ")
                .Append("		t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t1.adReqTypeId = t4.adReqTypeId and ")
                .Append("		t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
                .Append("		t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and  ")
                .Append("		t1.adReqId not in (select Distinct adReqId from adReqGrpDef) ")
                .Append(" ) ")
                .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")

                ' Requirements assigned to a program version and not part of any requirement group
                .Append(" select  ")
                .Append(" adReqId, ")
                .Append(" Descrip, ")
                .Append(" ReqGrpId, ")
                .Append(" StartDate, ")
                .Append(" EndDate, ")
                .Append(" ActualScore, ")
                .Append(" TestTaken, ")
                .Append(" Comments, ")
                .Append(" OverRide, ")
                .Append(" Minscore, ")
                .Append(" Required, ")
                .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
                .Append(" DocSubmittedCount, ")
                .Append(" DocStatusDescrip,ReqTypeDescrip ")
                .Append(" from  ")
                .Append(" ( ")
                ' Get Requirement Group and requirements of mandatory requirement
                .Append(" select ")
                .Append("	t1.adReqId, ")
                .Append("	t1.Descrip, ")
                .Append(" case when t5.ReqGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t5.ReqGrpId End as reqGrpId,  ")
                .Append("	'" & Currentdate & "' as CurrentDate, ")
                .Append("	t2.StartDate, ")
                .Append("	t2.EndDate, ")
                .Append("	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("		LeadId='" & LeadId & "' ) as ActualScore,  ")
                .Append("	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("		LeadId='" & LeadId & "' ) as TestTaken,  ")
                .Append("	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("		LeadId='" & LeadId & "' ) as Comments,  ")
                .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("                    LeadId='" & LeadId & "') ")
                .Append("            as override, ")
                .Append("	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("		LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("	t2.Minscore, ")
                .Append("	t3.IsRequired as Required, ")
                .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t6.Descrip as ReqTypeDescrip ")
                .Append(" from  ")
                .Append("	adReqs t1, ")
                .Append("		adReqsEffectiveDates t2, ")
                .Append("		adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5,adReqTypes t6 ")
                .Append("	where ")
                .Append("		t1.adReqId = t2.adReqId and ")
                .Append("		t1.adreqTypeId in (1,3) and t1.adReqTypeId = t6.adReqTypeId and ")
                .Append("		t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId ")
                .Append("		and t1.adReqId = t5.adReqId and t5.PrgVerId='" & PrgVerId & "'  and ")
                .Append("		t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
                .Append("		t1.adReqId not in (select Distinct adReqId from adReqGrpDef) ")
                .Append(" ) ")
                .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                ' Get Requirements assigned to a lead groups,but not assigned directly to a program version
                ' or not assigned to a requirement group that is part of a program version
                .Append(" select  ")
                .Append(" adReqId, ")
                .Append(" Descrip, ")
                .Append(" ReqGrpId, ")
                .Append(" StartDate, ")
                .Append(" EndDate, ")
                .Append(" ActualScore, ")
                .Append(" TestTaken, ")
                .Append(" Comments, ")
                .Append(" OverRide, ")
                .Append(" Minscore, ")
                .Append(" Required, ")
                .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
                .Append(" DocSubmittedCount, ")
                .Append(" DocStatusDescrip,ReqTypeDescrip ")
                .Append(" from  ")
                .Append(" ( ")
                ' Get Requirement Group and requirements of mandatory requirement
                .Append(" select ")
                .Append("	t1.adReqId, ")
                .Append("	t1.Descrip, ")
                .Append(" case when t5.ReqGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t5.ReqGrpId End as reqGrpId,  ")
                .Append("	'" & Currentdate & "' as CurrentDate, ")
                .Append("	t2.StartDate, ")
                .Append("	t2.EndDate, ")
                .Append("	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("		LeadId='" & LeadId & "' ) as ActualScore,  ")
                .Append("	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("		LeadId='" & LeadId & "' ) as TestTaken,  ")
                .Append("	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("		LeadId='" & LeadId & "' ) as Comments,  ")
                .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("                    LeadId='" & LeadId & "') ")
                .Append("            as override, ")
                .Append("	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("		LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("	t2.Minscore, ")
                .Append("	t3.IsRequired as Required, ")
                .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t6.Descrip as ReqTypeDescrip ")
                .Append(" from  ")
                .Append("	adReqs t1, ")
                .Append("		adReqsEffectiveDates t2, ")
                .Append("		adReqLeadGroups t3,adLeads t4,adReqGrpDef t5,adReqTypes t6 ")
                .Append(" where ")
                .Append("	t1.adReqId = t2.adReqId and t1.adReqTypeId = t6.adReqTypeId and  ")
                .Append("	t1.adreqTypeId in (1,3) and ")
                .Append("	t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t1.adReqId = t5.adReqId and ")
                .Append("		t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
                .Append("	t1.adReqId = t5.adReqId and ")
                .Append("	t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and ")
                .Append("	t5.adReqId not in (select Distinct s1.ReqGrpId from adReqGrpDef s1,adPrgVerTestDetails s2 ")
                .Append("			     where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null) ")
                .Append("	) ")
                .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                ' requirements part of a requirement group assigned to program version 
                .Append(" union ")
                .Append(" select  ")
                .Append(" adReqId, ")
                .Append(" Descrip, ")
                .Append(" ReqGrpId, ")
                .Append(" StartDate, ")
                .Append(" EndDate, ")
                .Append(" ActualScore, ")
                .Append(" TestTaken, ")
                .Append(" Comments, ")
                .Append(" OverRide, ")
                .Append(" Minscore, ")
                .Append(" Required, ")
                .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
                .Append(" DocSubmittedCount,DocStatusDescrip,ReqTypeDescrip ")
                .Append(" from  ")
                .Append(" ( ")
                '' Get Requirement Group and requirements of mandatory requirement
                .Append(" select ")
                .Append("	t1.adReqId, ")
                .Append("	t1.Descrip, ")
                .Append(" case when t5.ReqGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t5.ReqGrpId End as reqGrpId,  ")
                .Append("	'" & Currentdate & "' as CurrentDate, ")
                .Append("	t2.StartDate, ")
                .Append("	t2.EndDate, ")
                .Append("	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("		LeadId='" & LeadId & "' ) as ActualScore,  ")
                .Append("	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("		LeadId='" & LeadId & "' ) as TestTaken,  ")
                .Append("	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("		LeadId='" & LeadId & "' ) as Comments,  ")
                .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("                    LeadId='" & LeadId & "') ")
                .Append("            as override, ")
                .Append("	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("		LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("	t2.Minscore, ")
                .Append("	t3.IsRequired as Required, ")
                .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip,t6.Descrip as ReqTypeDescrip ")
                .Append(" from  ")
                .Append("	adReqs t1, ")
                .Append("		adReqsEffectiveDates t2, ")
                .Append("		adReqLeadGroups t3,adLeads t4,adReqGrpDef t5,adReqTypes t6  ")
                .Append("	where  ")
                .Append("		t1.adReqId = t2.adReqId and  ")
                .Append("		t1.adreqTypeId in (1,3) and  t1.adReqTypeId = t6.adReqTypeId and ")
                .Append("		t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t1.adReqId = t5.adReqId and  ")
                .Append("		t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
                .Append("		t5.ReqGrpId in (select Distinct s1.ReqGrpId from adReqGrpDef s1,adPrgVerTestDetails s2  ")
                .Append("				     where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null)  ")
                .Append("	)  ")
                .Append("	R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)  ")
                .Append("	) R2 ")
            End With

            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As System.Exception
        Finally
            'db.CloseConnection()
        End Try
        Return Nothing
    End Function
    Public Function GetReqsByRequirementgroup(ByVal ReqGrpId As String, ByVal Status As String) As DataSet

        Dim strCurrentDate As Date = Date.Now.ToShortDateString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Dim sb As New StringBuilder
        With sb
            .Append("  select Distinct adReqId,Descrip,ReqTypeDescrip,LeadGroupDescrip,StartDate,EndDate,Required ")
            .Append("  from ")
            .Append("  (   ")
            .Append("                      select  ")
            .Append("                          t2.adReqId,t2.Descrip,t4.Descrip as ReqTypeDescrip,t5.Descrip as LeadGroupDescrip, ")
            .Append("                           '" & strCurrentDate & "' as CurrentDate,  ")
            .Append("                           t6.StartDate, ")
            .Append("                           t6.EndDate, ")
            .Append("                           (select Distinct IsRequired from adReqLeadGroups where adReqEffectiveDateId=t6.adReqEffectiveDateId and LeadGrpId=t5.LeadGrpId) as Required ")
            .Append("                       from ")
            .Append("                          adReqGrpDef t1,adReqs t2,adReqGroups t3,adReqTypes t4, ")
            .Append(" 			    adLeadGroups t5,adReqsEffectiveDates t6 , adLeadGrpReqGroups t7, syStatuses t10 ")
            .Append(" 			where  ")
            .Append(" 				t1.adReqId=t2.adReqId and t1.ReqGrpId = t3.ReqGrpId and ")
            .Append(" 				t2.adReqTypeId = t4.adReqTypeId and t1.LeadGrpId = t5.LeadGrpId ")
            .Append(" 				and t2.adReqId = t6.adReqId  ")
            .Append(" 				and t1.ReqGrpId = '" & ReqGrpId & "' and t6.MandatoryRequirement <> 1 ")
            .Append(" 				and t1.LeadGrpId = t7.LeadGrpId And  t1.ReqGrpId = t7.ReqGrpId ")
            .Append("               and t7.StatusId = t10.StatusId ")
            If Status = "Active" Then
                .Append(" And t10.Status = 'Active' ")
            ElseIf Status = "Inactive" Then
                .Append(" and t10.Status = 'Inactive' ")
            End If
            .Append(") ")
            .Append("                       R1 where R1.CurrentDate >= R1.StartDate And ")
            .Append("                       (R1.CurrentDate <= R1.EndDate Or R1.EndDate Is NULL) ")


        End With
        Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
        Dim ds As New DataSet
        Dim da1 As New OleDbDataAdapter(sc1)
        da1.Fill(ds, "TestDetails")
        sb.Remove(0, sb.Length)
        Return ds
    End Function

    Public Function DeleteReqGrpDef(ByVal LeadgrpId As String, ByVal ReqGrpId As String) As Integer
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        With sb
            .Append(" Delete from adReqGrpDef where LeadGrpId='" & LeadgrpId & "' and ReqGrpId = '" & ReqGrpId & "' ")
        End With
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        With sb
            .Append(" Delete from adLeadGrpReqGroups where LeadGrpId='" & LeadgrpId & "' and ReqGrpId = '" & ReqGrpId & "' ")
        End With
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)
        Return 0
    End Function
    Public Function GetAllStandardDocumentsByEffectiveDatesByStatus(ByVal LeadId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String
        Dim currentDate As Date = Date.Now.ToShortDateString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Try


            With sb
                .Append(" select Distinct adReqId,descrip as DocumentDescrip,Required,DocStatusDescrip, ")
                .Append(" case when (select count(*) from adLeadDocsReceived where DocumentId=adReqId and LeadId='" & LeadId & "') >=1 then 'Yes' else 'No' end as DocSubmitted ")
                .Append(" from ")
                .Append(" ( ")
                .Append(" select  ")
                .Append(" adReqId,  ")
                .Append(" Descrip,  ")
                .Append(" StartDate,")
                .Append(" EndDate, ")
                .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append(" Required,DocStatusDescrip, ")
                .Append(" from ")
                .Append("   (   ")

                'Get List Of Requirements that are mandatory for all requirement
                .Append("       select  ")
                .Append("           t1.adReqId, ")
                .Append("           t1.Descrip, ")
                .Append("           '" & currentDate & "' as CurrentDate, ")
                .Append("           t2.StartDate, ")
                .Append("           t2.EndDate, ")
                .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("                    LeadId='" & LeadId & "') ")
                .Append("            as override, ")
                .Append("           1 as Required, ")
                .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("       from ")
                .Append("           adReqs t1, ")
                .Append("           adReqsEffectiveDates t2 ")
                .Append("       where  ")
                .Append("           t1.adReqId = t2.adReqId and  ")
                .Append("           t1.adReqTypeId = 3 and  ")
                .Append("           t2.MandatoryRequirement=1 ")
                .Append(" ) ")
                .Append("       R1 where R1.CurrentDate >= R1.StartDate and ")
                .Append("       (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union  ")

                'Get List Of Test that has been assigned to lead group but not part of requirement group
                'or not assigned to a program version
                .Append(" select  ")
                .Append(" adReqId,  ")
                .Append(" Descrip,  ")
                .Append(" StartDate,")
                .Append(" EndDate, ")
                .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
                .Append(" Required,DocStatusDescrip ")
                .Append(" from ")
                .Append("   (   ")
                .Append("       select  ")
                .Append("           t1.adReqId, ")
                .Append("           t1.Descrip, ")
                .Append("           '" & currentDate & "' as CurrentDate, ")
                .Append("           t2.StartDate, ")
                .Append("           t2.EndDate, ")
                .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("                    LeadId='" & LeadId & "') ")
                .Append("            as override, ")
                .Append("           t3.IsRequired as Required, ")
                .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("       from ")
                .Append("           adReqs t1, ")
                .Append("           adReqsEffectiveDates t2, ")
                .Append("           adReqLeadGroups t3 ")
                .Append("       where  ")
                .Append("           t1.adReqId = t2.adReqId and ")
                .Append("           t1.adreqTypeId = 3 and  ")
                .Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
                .Append("           t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
                .Append("           t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and  ")
                .Append("           t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 ")
                .Append("                              where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null) ")
                .Append(" ) ")
                .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")
                .Append(" ) R2 ")

            End With
            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As System.Exception
        Finally
        End Try
        Return Nothing
    End Function
    Public Function GetAllStandardDocumentsByEffectiveDatesAndPrgVersionByStatus(ByVal LeadId As String, ByVal PrgVerId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        'Dim strPreviousEduTest As String
        Dim strcurrentDate As Date = Date.Now.ToShortDateString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Try

            With sb
                .Append(" select Distinct ")
                .Append(" adReqId as DocumentId, ")
                .Append(" Descrip as DocumentDescrip,Required as Required, ")
                .Append(" case when DocStatusDescrip=1 then 'Approved' else 'NotApproved' end as DocStatusDescrip")
                .Append(" from  ")
                .Append(" ( ")
                ' Get Requirement Group and requirements of mandatory requirement
                .Append(" 	select  ")
                .Append(" 		adReqId, ")
                .Append("		Descrip, ")
                .Append("		ReqGrpId, ")
                .Append("		StartDate, ")
                .Append("		EndDate, ")
                .Append("		ActualScore, ")
                .Append("		TestTaken, ")
                .Append("		Comments, ")
                .Append("		OverRide, ")
                .Append("		Minscore, ")
                .Append("		Required, ")
                .Append("		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		DocSubmittedCount, ")
                .Append("		DocStatusDescrip ")
                .Append("	from  ")
                .Append("		( ")
                .Append("		select ")
                .Append("		t1.adReqId, ")
                .Append("				t1.Descrip, ")
                .Append("				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append("					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
                .Append("					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                .Append("				'" & strcurrentDate & "' as CurrentDate, ")
                .Append("				t2.StartDate, ")
                .Append("				t2.EndDate, ")
                .Append("				(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("					LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("				(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("					LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("				(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("					LeadId='" & LeadId & "' ) as Comments, ")
                .Append("				       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("				        LeadId='" & LeadId & "' ) as override, ")
                .Append("				(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("					LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("				t2.Minscore, ")
                .Append("				1 as Required, ")
                .Append("				(select Distinct s1.SysDocStatusId from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("				s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("				from  ")
                .Append("				adReqs t1, ")
                .Append("				adReqsEffectiveDates t2 ")
                .Append("				where  ")
                .Append("				t1.adReqId = t2.adReqId and ")
                .Append("				t2.MandatoryRequirement=1 and ")
                .Append("				t1.adReqTypeId in (3) ")
                .Append("				) ")
                .Append("				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("		union ")
                ' Get Requirements assigned to a lead groups,but not assigned 
                '  directly to a program version
                ' or not assigned to a requirement group 
                .Append(" 	select  ")
                .Append(" 		adReqId, ")
                .Append("		Descrip, ")
                .Append("		ReqGrpId, ")
                .Append("		StartDate, ")
                .Append("		EndDate, ")
                .Append("		ActualScore, ")
                .Append("		TestTaken, ")
                .Append("		Comments, ")
                .Append("		OverRide, ")
                .Append("		Minscore, ")
                .Append("		Required, ")
                .Append("		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		DocSubmittedCount, ")
                .Append("		DocStatusDescrip ")
                .Append("		from  ")
                .Append("				( ")
                .Append("						select ")
                .Append("							t1.adReqId, ")
                .Append("							t1.Descrip, ")
                .Append("							'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append("							'" & strcurrentDate & "' as CurrentDate, ")
                .Append("							t2.StartDate, ")
                .Append("							t2.EndDate, ")
                .Append("							(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("								LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("							(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("								LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("							(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("								LeadId='" & LeadId & "' ) as Comments, ")
                .Append("							       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("							        LeadId='" & LeadId & "' ) ")
                .Append("							as override, ")
                .Append("							(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("								LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("							t2.Minscore, ")
                .Append("							t3.Isrequired as Required, ")
                .Append("							(select Distinct s1.SysDocStatusId from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("							s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("						from  ")
                .Append("							adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3 ")
                .Append("						where ")
                .Append("							t1.adReqId = t2.adReqId and ")
                .Append("							t1.adreqTypeId in (3) and ")
                .Append("							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  and ")
                .Append("						       t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' ) ")
                .Append("							and t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId = '" & PrgVerId & "' ) and  ")
                .Append("							t1.adReqId not in (select Distinct adReqId from adReqGrpDef f1,adLeadByLeadGroups f2 ")
                .Append("							where f1.LeadGrpId = f2.LeadGrpId and f2.LeadId='" & LeadId & "' ) ")
                .Append("					) ")
                .Append("					R1 where R1.CurrentDate >= R1.StartDate and ")
                .Append("					(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("				union ")

                '' Requirements assigned to a lead group and part of requirement group but not assigned to a program version
                '' balaji 09/13/2005


                .Append(" 	select  ")
                .Append(" 		adReqId, ")
                .Append("		Descrip, ")
                .Append("		ReqGrpId, ")
                .Append("		StartDate, ")
                .Append("		EndDate, ")
                .Append("		ActualScore, ")
                .Append("		TestTaken, ")
                .Append("		Comments, ")
                .Append("		OverRide, ")
                .Append("		Minscore, ")
                .Append("		Required, ")
                .Append("		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("		DocSubmittedCount, ")
                .Append("		DocStatusDescrip ")
                .Append("		from  ")
                .Append("				( ")
                .Append("						select ")
                .Append("							t1.adReqId, ")
                .Append("							t1.Descrip, ")
                .Append("							case when t4.ReqGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t4.ReqGrpId end as ReqGrpId, ")
                .Append("							'" & strcurrentDate & "' as CurrentDate, ")
                .Append("							t2.StartDate, ")
                .Append("							t2.EndDate, ")
                .Append("							(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("								LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("							(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("								LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("							(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("								LeadId='" & LeadId & "' ) as Comments, ")
                .Append("							       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("							        LeadId='" & LeadId & "' ) ")
                .Append("							as override, ")
                .Append("							(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("								LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("							t2.Minscore, ")
                .Append("							t3.Isrequired as Required, ")
                .Append("							(select Distinct s1.SysDocStatusId from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("							s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("						from  ")
                .Append("							adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3,adReqGrpDef t4 ")
                .Append("						where ")
                .Append("							t1.adReqId = t2.adReqId and ")
                .Append("							t1.adreqTypeId in (3) and ")
                .Append("							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  and ")
                .Append("                           t1.adReqId = t4.adReqId and t3.LeadGrpId = t4.LeadGrpId and ")
                .Append("						    t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' ) and ")
                .Append("						    t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId = '" & PrgVerId & "') and  ")
                .Append("						    t1.adReqId not in (select Distinct f1.adReqId from adReqGrpDef f1,adLeadByLeadGroups f2,adPrgVerTestDetails f3 ")
                .Append("						                    where f1.LeadGrpId = f2.LeadGrpId and f1.reqGrpId=f3.ReqGrpId and f2.LeadId='" & LeadId & "' ")
                .Append("					                        and f3.prgVerId='" & PrgVerId & "' and f3.ReqGrpId is not null) ")
                .Append("					) ")
                .Append("					R1 where R1.CurrentDate >= R1.StartDate and ")
                .Append("					(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("				union ")


                '' Requirements assigned to a program version
                .Append("			select  ")
                .Append("					adReqId, ")
                .Append("					Descrip, ")
                .Append("					ReqGrpId, ")
                .Append("					StartDate, ")
                .Append("					EndDate, ")
                .Append("					ActualScore, ")
                .Append("					TestTaken, ")
                .Append("						Comments, ")
                .Append("						OverRide, ")
                .Append("						Minscore, ")
                .Append("						Required, ")
                .Append("						Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("						DocSubmittedCount, ")
                .Append("						DocStatusDescrip ")
                .Append("				from  ")
                .Append("					( ")
                .Append("						select ")
                .Append("							t1.adReqId, ")
                .Append("							t1.Descrip, ")
                .Append("						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
                .Append("						'" & strcurrentDate & "' as CurrentDate, ")
                .Append("						t2.StartDate, ")
                .Append("						t2.EndDate, ")
                .Append("						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("							LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("					(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("							LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("							LeadId='" & LeadId & "' ) as Comments, ")
                .Append("					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("						        LeadId='" & LeadId & "' ) ")
                .Append("						as override, ")
                .Append("						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("							LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("					t2.Minscore, ")
                .Append("						t3.IsRequired as Required, ")
                .Append("						(select Distinct s1.SysDocStatusId from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("						s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("					from  ")
                .Append("							adReqs t1, ")
                .Append("							adReqsEffectiveDates t2, ")
                .Append("							adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
                .Append("						where ")
                .Append("						t1.adReqId = t2.adReqId and ")
                .Append("							t1.adreqTypeId in (3) and ")
                .Append("							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId ")
                .Append("							and t1.adReqId = t5.adReqId and t5.PrgVerId = '" & PrgVerId & "'   and ")
                .Append("					       t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where ")
                .Append("						LeadId='" & LeadId & "' ) ")
                .Append("							and t5.adReqId is not null ")
                .Append("						) ")
                .Append("						R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append("					union ")

                'Get Requirement groups assigned to a program version and all requirements part of group

                .Append("			select  ")
                .Append("					adReqId, ")
                .Append("					Descrip, ")
                .Append("					ReqGrpId, ")
                .Append("					StartDate, ")
                .Append("					EndDate, ")
                .Append("					ActualScore, ")
                .Append("					TestTaken, ")
                .Append("						Comments, ")
                .Append("						OverRide, ")
                .Append("						Minscore, ")
                .Append("						Required, ")
                .Append("						Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
                .Append("						DocSubmittedCount, ")
                .Append("						DocStatusDescrip ")
                .Append("				from  ")
                .Append("					( ")
                .Append("						select ")
                .Append("							t1.adReqId, ")
                .Append("							t1.Descrip, ")
                .Append("						t5.ReqGrpId as reqGrpId, ")
                .Append("						'" & strcurrentDate & "' as CurrentDate, ")
                .Append("						t2.StartDate, ")
                .Append("						t2.EndDate, ")
                .Append("						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("							LeadId='" & LeadId & "' ) as ActualScore, ")
                .Append("					(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
                .Append("							LeadId='" & LeadId & "' ) as TestTaken, ")
                .Append("						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
                .Append("							LeadId='" & LeadId & "' ) as Comments, ")
                .Append("					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
                .Append("						        LeadId='" & LeadId & "' ) ")
                .Append("						as override, ")
                .Append("						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
                .Append("							LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
                .Append("					t2.Minscore, ")
                .Append("						t3.IsRequired as Required, ")
                .Append("						(select Distinct s1.SysDocStatusId from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("						s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
                .Append("					from  ")
                .Append("						adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3,adPrgVerTestDetails t5,adReqGrpDef t6,adLeadByLeadGroups t7 ")
                .Append("					where ")
                .Append("						t1.adReqId = t2.adReqId and ")
                .Append("					t1.adreqTypeId in (3) and ")
                .Append("					t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and  t3.LeadGrpId = t7.LeadGrpId and ")
                .Append("					t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
                .Append("					and t7.LeadId='" & LeadId & "'  and t5.PrgVerId = '" & PrgVerId & "'  ")
                .Append("		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" ) ")
                .Append(" R2 ")
            End With

            'With sb


            '    'Get All Requirements That Are Not Part Of Any Group
            '    '
            '    .Append(" select Distinct ")
            '    .Append(" adReqId, ")
            '    .Append(" Descrip as DocumentDescrip,Required,DocStatusDescrip, ")
            '    .Append(" case when (select count(*) from adLeadDocsReceived where DocumentId=adReqId and LeadId='" & LeadId & "') >=1 then 'Yes' else 'No' end as DocSubmitted ")
            '    '.Append(" ReqGrpId, ")
            '    '.Append(" StartDate, ")
            '    '.Append(" EndDate, ")
            '    '.Append(" ActualScore, ")
            '    '.Append(" TestTaken, ")
            '    '.Append(" Comments, ")
            '    '.Append(" Case when OverRide>=1 then 'True' else 'False' end as OverRide, ")
            '    '.Append(" Minscore, ")
            '    '.Append(" Required, ")
            '    '.Append(" DocSubmittedCount, ")
            '    '.Append(" DocStatusDescrip, ")
            '    '.Append(" Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount, ")
            '    '.Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass ")
            '    .Append(" from  ")
            '    .Append(" ( ")
            '    .Append(" select  ")
            '    .Append(" adReqId, ")
            '    .Append(" Descrip, ")
            '    .Append(" ReqGrpId, ")
            '    .Append(" StartDate, ")
            '    .Append(" EndDate, ")
            '    .Append(" ActualScore, ")
            '    .Append(" TestTaken, ")
            '    .Append(" Comments, ")
            '    .Append(" OverRide, ")
            '    .Append(" Minscore, ")
            '    .Append(" Required, ")
            '    .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
            '    .Append(" DocSubmittedCount, ")
            '    .Append(" DocStatusDescrip ")
            '    .Append(" from  ")
            '    .Append(" ( ")
            '    ' Get Requirement Group and requirements of mandatory requirement
            '    .Append(" select ")
            '    .Append("	t1.adReqId, ")
            '    .Append("	t1.Descrip, ")
            '    .Append("	case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
            '    .Append("		(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
            '    .Append("		else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
            '    .Append("	'" & currentDate & "' as CurrentDate, ")
            '    .Append("	t2.StartDate, ")
            '    .Append("	t2.EndDate, ")
            '    .Append("	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("		LeadId='" & LeadId & "' ) as ActualScore,  ")
            '    .Append("	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("		LeadId='" & LeadId & "' ) as TestTaken,  ")
            '    .Append("	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            '    .Append("		LeadId='" & LeadId & "' ) as Comments,  ")
            '    .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            '    .Append("                    LeadId='" & LeadId & "') ")
            '    .Append("            as override, ")
            '    .Append("	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            '    .Append("		LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
            '    .Append("	t2.Minscore, ")
            '    .Append("	1 as Required, ")
            '    .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            '    .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            '    .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            '    .Append(" from  ")
            '    .Append("	adReqs t1, ")
            '    .Append("	adReqsEffectiveDates t2 ")
            '    .Append(" where  ")
            '    .Append("	t1.adReqId = t2.adReqId and ")
            '    .Append("	t2.MandatoryRequirement=1 and ")
            '    .Append("	t1.adReqTypeId in (3) ")
            '    .Append(" ) ")
            '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            '    .Append(" union ")
            '    ' Get Requirements assigned to a lead groups,but not assigned directly to a program version
            '    ' or not assigned to a requirement group 
            '    .Append(" select  ")
            '    .Append(" adReqId, ")
            '    .Append(" Descrip, ")
            '    .Append(" ReqGrpId, ")
            '    .Append(" StartDate, ")
            '    .Append(" EndDate, ")
            '    .Append(" ActualScore, ")
            '    .Append(" TestTaken, ")
            '    .Append(" Comments, ")
            '    .Append(" OverRide, ")
            '    .Append(" Minscore, ")
            '    .Append(" Required, ")
            '    .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
            '    .Append(" DocSubmittedCount, ")
            '    .Append(" DocStatusDescrip ")
            '    .Append(" from  ")
            '    .Append(" ( ")
            '    ' Get Requirement Group and requirements of mandatory requirement
            '    .Append(" select ")
            '    .Append("	t1.adReqId, ")
            '    .Append("	t1.Descrip, ")
            '    .Append("	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            '    .Append("	'" & currentDate & "' as CurrentDate, ")
            '    .Append("	t2.StartDate, ")
            '    .Append("	t2.EndDate, ")
            '    .Append("	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("		LeadId='" & LeadId & "' ) as ActualScore,  ")
            '    .Append("	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("		LeadId='" & LeadId & "' ) as TestTaken,  ")
            '    .Append("	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            '    .Append("		LeadId='" & LeadId & "' ) as Comments,  ")
            '    .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            '    .Append("                    LeadId='" & LeadId & "') ")
            '    .Append("            as override, ")
            '    .Append("	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            '    .Append("		LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
            '    .Append("	t2.Minscore, ")
            '    .Append("	t3.Isrequired as Required, ")
            '    .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            '    .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            '    .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            '    .Append(" from  ")
            '    .Append("	adReqs t1, ")
            '    .Append("		adReqsEffectiveDates t2, ")
            '    .Append("		adReqLeadGroups t3 ")
            '    .Append("	where ")
            '    .Append("		t1.adReqId = t2.adReqId and ")
            '    .Append("		t1.adreqTypeId in (3) and ")
            '    .Append("		t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            '    .Append("       t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            '    .Append("		t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and  ")
            '    .Append("		t1.adReqId not in (select Distinct adReqId from adReqGrpDef) ")
            '    .Append(" ) ")
            '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            '    .Append(" union ")

            '    ' Requirements assigned to a program version and not part of any requirement group
            '    .Append(" select  ")
            '    .Append(" adReqId, ")
            '    .Append(" Descrip, ")
            '    .Append(" ReqGrpId, ")
            '    .Append(" StartDate, ")
            '    .Append(" EndDate, ")
            '    .Append(" ActualScore, ")
            '    .Append(" TestTaken, ")
            '    .Append(" Comments, ")
            '    .Append(" OverRide, ")
            '    .Append(" Minscore, ")
            '    .Append(" Required, ")
            '    .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
            '    .Append(" DocSubmittedCount, ")
            '    .Append(" DocStatusDescrip ")
            '    .Append(" from  ")
            '    .Append(" ( ")
            '    ' Get Requirement Group and requirements of mandatory requirement
            '    .Append(" select ")
            '    .Append("	t1.adReqId, ")
            '    .Append("	t1.Descrip, ")
            '    .Append(" case when t5.ReqGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t5.ReqGrpId End as reqGrpId,  ")
            '    .Append("	'" & currentDate & "' as CurrentDate, ")
            '    .Append("	t2.StartDate, ")
            '    .Append("	t2.EndDate, ")
            '    .Append("	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("		LeadId='" & LeadId & "' ) as ActualScore,  ")
            '    .Append("	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("		LeadId='" & LeadId & "' ) as TestTaken,  ")
            '    .Append("	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            '    .Append("		LeadId='" & LeadId & "' ) as Comments,  ")
            '    .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            '    .Append("                    LeadId='" & LeadId & "') ")
            '    .Append("            as override, ")
            '    .Append("	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            '    .Append("		LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
            '    .Append("	t2.Minscore, ")
            '    .Append("	t3.IsRequired as Required, ")
            '    .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            '    .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            '    .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            '    .Append(" from  ")
            '    .Append("	adReqs t1, ")
            '    .Append("		adReqsEffectiveDates t2, ")
            '    .Append("		adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
            '    .Append("	where ")
            '    .Append("		t1.adReqId = t2.adReqId and ")
            '    .Append("		t1.adreqTypeId in (3) and ")
            '    .Append("		t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId ")
            '    .Append("		and t1.adReqId = t5.adReqId and t5.PrgVerId='" & PrgVerId & "'  and ")
            '    .Append("       t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            '    .Append("		t1.adReqId not in (select Distinct adReqId from adReqGrpDef) ")
            '    .Append(" ) ")
            '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            '    .Append(" union ")
            '    ' Get Requirements assigned to a lead groups,but not assigned directly to a program version
            '    ' or not assigned to a requirement group that is part of a program version
            '    .Append(" select  ")
            '    .Append(" adReqId, ")
            '    .Append(" Descrip, ")
            '    .Append(" ReqGrpId, ")
            '    .Append(" StartDate, ")
            '    .Append(" EndDate, ")
            '    .Append(" ActualScore, ")
            '    .Append(" TestTaken, ")
            '    .Append(" Comments, ")
            '    .Append(" OverRide, ")
            '    .Append(" Minscore, ")
            '    .Append(" Required, ")
            '    .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
            '    .Append(" DocSubmittedCount, ")
            '    .Append(" DocStatusDescrip ")
            '    .Append(" from  ")
            '    .Append(" ( ")
            '    ' Get Requirement Group and requirements of mandatory requirement
            '    .Append(" select ")
            '    .Append("	t1.adReqId, ")
            '    .Append("	t1.Descrip, ")
            '    .Append(" case when t5.ReqGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t5.ReqGrpId End as reqGrpId,  ")
            '    .Append("	'" & currentDate & "' as CurrentDate, ")
            '    .Append("	t2.StartDate, ")
            '    .Append("	t2.EndDate, ")
            '    .Append("	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("		LeadId='" & LeadId & "' ) as ActualScore,  ")
            '    .Append("	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("		LeadId='" & LeadId & "' ) as TestTaken,  ")
            '    .Append("	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            '    .Append("		LeadId='" & LeadId & "' ) as Comments,  ")
            '    .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            '    .Append("                    LeadId='" & LeadId & "') ")
            '    .Append("            as override, ")
            '    .Append("	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            '    .Append("		LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
            '    .Append("	t2.Minscore, ")
            '    .Append("	t3.IsRequired as Required, ")
            '    .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            '    .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            '    .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            '    .Append(" from  ")
            '    .Append("	adReqs t1, ")
            '    .Append("		adReqsEffectiveDates t2, ")
            '    .Append("		adReqLeadGroups t3,adReqGrpDef t5 ")
            '    .Append(" where ")
            '    .Append("	t1.adReqId = t2.adReqId and ")
            '    .Append("	t1.adreqTypeId in (3) and ")
            '    .Append("	t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t1.adReqId = t5.adReqId and ")
            '    .Append("           t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            '    .Append("	t1.adReqId = t5.adReqId and ")
            '    .Append("	t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and ")
            '    .Append("	t5.adReqId not in (select Distinct s1.ReqGrpId from adReqGrpDef s1,adPrgVerTestDetails s2 ")
            '    .Append("			     where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null) ")
            '    .Append("	) ")
            '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            '    ' requirements part of a requirement group assigned to program version 
            '    .Append(" union ")
            '    .Append(" select  ")
            '    .Append(" adReqId, ")
            '    .Append(" Descrip, ")
            '    .Append(" ReqGrpId, ")
            '    .Append(" StartDate, ")
            '    .Append(" EndDate, ")
            '    .Append(" ActualScore, ")
            '    .Append(" TestTaken, ")
            '    .Append(" Comments, ")
            '    .Append(" OverRide, ")
            '    .Append(" Minscore, ")
            '    .Append(" Required, ")
            '    .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
            '    .Append(" DocSubmittedCount,DocStatusDescrip ")
            '    .Append(" from  ")
            '    .Append(" ( ")
            '    '' Get Requirement Group and requirements of mandatory requirement
            '    .Append(" select ")
            '    .Append("	t1.adReqId, ")
            '    .Append("	t1.Descrip, ")
            '    .Append(" case when t5.ReqGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t5.ReqGrpId End as reqGrpId,  ")
            '    .Append("	'" & currentDate & "' as CurrentDate, ")
            '    .Append("	t2.StartDate, ")
            '    .Append("	t2.EndDate, ")
            '    .Append("	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("		LeadId='" & LeadId & "' ) as ActualScore,  ")
            '    .Append("	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("		LeadId='" & LeadId & "' ) as TestTaken,  ")
            '    .Append("	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            '    .Append("		LeadId='" & LeadId & "' ) as Comments,  ")
            '    .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            '    .Append("                    LeadId='" & LeadId & "') ")
            '    .Append("            as override, ")
            '    .Append("	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            '    .Append("		LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
            '    .Append("	t2.Minscore, ")
            '    .Append("	t3.IsRequired as Required, ")
            '    .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            '    .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            '    .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            '    .Append(" from  ")
            '    .Append("	adReqs t1, ")
            '    .Append("		adReqsEffectiveDates t2, ")
            '    .Append("		adReqLeadGroups t3,adReqGrpDef t5  ")
            '    .Append("	where  ")
            '    .Append("		t1.adReqId = t2.adReqId and  ")
            '    .Append("		t1.adreqTypeId in (3) and   ")
            '    .Append("		t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t1.adReqId = t5.adReqId and  ")
            '    .Append("           t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            '    .Append("		t5.ReqGrpId in (select Distinct s1.ReqGrpId from adReqGrpDef s1,adPrgVerTestDetails s2  ")
            '    .Append("				     where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null)  ")
            '    .Append("	)  ")
            '    .Append("	R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)  ")
            '    .Append("	) R2 ")
            'End With

            'With sb
            '    .Append(" select  ")
            '    .Append(" adReqId as DocumentId,  ")
            '    .Append(" Descrip as DocumentDescrip,  ")
            '    .Append(" StartDate,")
            '    .Append(" EndDate, ")
            '    .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
            '    .Append(" Required ")
            '    .Append(" from ")
            '    .Append("   (   ")

            '    'Get List Of Requirements that are mandatory for all requirement
            '    .Append("       select  ")
            '    .Append("           t1.adReqId, ")
            '    .Append("           t1.Descrip, ")
            '    .Append("           Convert(char(10),getdate(),101) as CurrentDate, ")
            '    .Append("           t2.StartDate, ")
            '    .Append("           t2.EndDate, ")
            '    .Append("           (select OverRide from adLeadDocsReceived where DocumentId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as OverRide, ")
            '    .Append("           1 as Required ")
            '    .Append("       from ")
            '    .Append("           adReqs t1, ")
            '    .Append("           adReqsEffectiveDates t2 ")
            '    .Append("       where  ")
            '    .Append("           t1.adReqId = t2.adReqId and  ")
            '    .Append("           t1.adReqTypeId = 3 and  ")
            '    .Append("           t2.MandatoryRequirement=1 ")
            '    .Append(" ) ")
            '    .Append("       R1 where R1.CurrentDate >= R1.StartDate and ")
            '    .Append("       (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            '    .Append(" union  ")

            '    'Get List Of Test that has been assigned to lead group but not part of requirement group
            '    'or not assigned to a program version
            '    .Append(" select  ")
            '    .Append(" adReqId,  ")
            '    .Append(" Descrip,  ")
            '    .Append(" StartDate,")
            '    .Append(" EndDate, ")
            '    .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
            '    .Append(" Required ")
            '    .Append(" from ")
            '    .Append("   (   ")
            '    .Append("       select  ")
            '    .Append("           t1.adReqId, ")
            '    .Append("           t1.Descrip, ")
            '    .Append("           Convert(char(10),getdate(),101) as CurrentDate, ")
            '    .Append("           t2.StartDate, ")
            '    .Append("           t2.EndDate, ")
            '    .Append("           (select OverRide from adLeadDocsReceived where DocumentId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as OverRide, ")
            '    .Append("           t3.IsRequired as Required ")
            '    .Append("       from ")
            '    .Append("           adReqs t1, ")
            '    .Append("           adReqsEffectiveDates t2, ")
            '    .Append("           adReqLeadGroups t3, ")
            '    .Append("           adLeads t4 ")
            '    .Append("       where  ")
            '    .Append("           t1.adReqId = t2.adReqId and ")
            '    .Append("           t1.adreqTypeId = 3 and  ")
            '    .Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            '    .Append("           t3.LeadGrpId = t4.LeadGrpId and t4.LeadId='" & LeadId & "' and ")
            '    .Append("           t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and  ")
            '    .Append("           t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 ")
            '    .Append("                              where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null) ")
            '    .Append(" ) ")
            '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")
            '    .Append(" Union ")
            '    'Get List Of Requirements assigned to program version
            '    'Get List Of Test that has been assigned to lead group but not part of requirement group
            '    'or not assigned to a program version
            '    .Append(" select  ")
            '    .Append(" adReqId,  ")
            '    .Append(" Descrip,  ")
            '    .Append(" StartDate,")
            '    .Append(" EndDate, ")
            '    .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
            '    .Append(" Required, ")
            '    .Append(" from ")
            '    .Append("   (   ")
            '    .Append("       select  ")
            '    .Append("           t1.adReqId, ")
            '    .Append("           t1.Descrip, ")
            '    .Append("           Convert(char(10),getdate(),101) as CurrentDate, ")
            '    .Append("           t2.StartDate, ")
            '    .Append("           t2.EndDate, ")
            '    .Append("           (select OverRide from adLeadDocsReceived where DocumentId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as OverRide, ")
            '    .Append("           t2.Minscore, ")
            '    .Append("           t3.IsRequired as Required ")
            '    .Append("       from ")
            '    .Append("           adReqs t1, ")
            '    .Append("           adReqsEffectiveDates t2, ")
            '    .Append("           adReqLeadGroups t3,adLeads t4, ")
            '    .Append("           adPrgVerTestDetails t5 ")
            '    .Append("       where  ")
            '    .Append("           t1.adReqId = t2.adReqId and ")
            '    .Append("           t1.adreqTypeId = 3 and      ")
            '    .Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            '    .Append("           t3.LeadGrpId = t4.LeadGrpId and t4.LeadId='" & LeadId & "' and ")
            '    .Append("           t1.adReqId = t5.adReqId and t1.PrgVerId = '" & PrgVerId & "' and ")
            '    .Append("           t5.ReqGrpId Is Null  ")
            '    .Append(" ) ")
            '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")

            '    .Append(" Union ")
            '    'Get List Of Requirements assigned to program version and requirements that are part of a group
            '    .Append(" select  ")
            '    .Append(" adReqId,  ")
            '    .Append(" Descrip,  ")
            '    .Append(" StartDate,")
            '    .Append(" EndDate, ")
            '    .Append(" Case When OverRide=1 then 'True' else 'False' end as OverRide, ")
            '    .Append(" Required ")
            '    .Append(" from ")
            '    .Append("   (   ")
            '    .Append("       select  ")
            '    .Append("           t1.adReqId, ")
            '    .Append("           t1.Descrip, ")
            '    .Append("           Convert(char(10),getdate(),101) as CurrentDate, ")
            '    .Append("           t2.StartDate, ")
            '    .Append("           t2.EndDate, ")
            '    .Append("           (select OverRide from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            '    .Append("           LeadId='" & LeadId & "') as OverRide, ")
            '    .Append("           t3.IsRequired as Required ")
            '    .Append("       from ")
            '    .Append("           adReqs t1, ")
            '    .Append("           adReqsEffectiveDates t2, ")
            '    .Append("           adReqLeadGroups t3,adLeads t4, ")
            '    .Append("           adPrgVerTestDetails t5 ")
            '    .Append("       where  ")
            '    .Append("           t1.adReqId = t2.adReqId and ")
            '    .Append("           t1.adreqTypeId = 3 and      ")
            '    .Append("           t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            '    .Append("           t3.LeadGrpId = t4.LeadGrpId and t4.LeadId='" & LeadId & "' and ")
            '    .Append("           t1.adReqId = t5.adReqId and t1.PrgVerId = '" & PrgVerId & "' and ")
            '    .Append("          t5.ReqGrpId in (select t1.ReqGrpId from adPrgVerTestDetails t1 where t1.PrgVerId='" & PrgVerId & "' and ReqGrpId is not null)  ")
            '    .Append(" ) ")
            '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")
            'End With
            Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

            Dim da1 As New OleDbDataAdapter(sc1)
            da1.Fill(ds, "TestDetails")
            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As System.Exception
        Finally
        End Try
        Return Nothing
    End Function
    Public Function GetPendingReqsByLeadandPrgVersion(ByVal LeadId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        'Dim strPreviousEducationId As String
        Dim strPreviousEduTest As String
        Dim strEnrollDate As Date = Date.Now.ToShortDateString
        Try
            With sb
                .Append(" Select distinct ")
                .Append(" adReqId,Descrip,ReqGrpId,StartDate,EndDate, ")
                .Append(" Required,case when DocStatusDescrip='Approved' then 'Yes' else 'No' end as DocStatusDescrip, ")
                .Append(" ReqTypeDescrip, ")
                .Append(" Case when ActualScore >= MinScore then 'True' else 'False' end as Pass, ")
                .Append(" Case when (select Override from adEntrTestOverRide where EntrTestId=adReqId and LeadId='" & LeadId & "') = 1 then 'True' else 'False' end as OverRide ")
                .Append(" from ")
                .Append(" ( ")
                'Get Requirement Group and requirements of mandatory requirement
                .Append(" select  ")
                .Append(" distinct adReqId,Descrip,ReqGrpId,StartDate, ")
                .Append(" EndDate,Required,DocStatusDescrip,ReqTypeDescrip,ActualScore,MinScore ")
                .Append(" from ")
                .Append(" ( ")
                .Append(" Select Distinct ")
                .Append(" t1.adReqId,t1.Descrip,case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
                .Append(" (select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
                .Append(" '" & strEnrollDate & "' as CurrentDate, ")
                .Append("			 t2.StartDate, ")
                .Append("			 t2.EndDate, ")
                .Append("			 1 as Required, ")
                .Append("			 (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("			 where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("			 s3.LeadId = '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
                .Append("		 t3.Descrip as ReqTypeDescrip, ")
                .Append("		 (select ActualScore from adLeadEntranceTest where LeadId='" & LeadId & "' and EntrTestId=t1.adReqId) as ActualScore, ")
                .Append("		 (select MinScore from adLeadEntranceTest where LeadId='" & LeadId & "' and EntrTestId=t1.adReqId) as MinScore ")
                .Append(" from  ")
                .Append(" adReqs t1, ")
                .Append(" adReqsEffectiveDates t2,adReqTypes t3 ")
                .Append(" where ")
                .Append(" t1.adReqId = t2.adReqId And t1.adReqTypeId = t3.adReqTypeId And ")
                .Append(" t2.MandatoryRequirement = 1 And ")
                .Append(" t1.adReqTypeId in (1,3) ")
                .Append(" ) ")
                .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                ' Get The Requirements that was assigned to a program version
                .Append(" select distinct adReqId,Descrip,ReqGrpId,StartDate,EndDate,Required,DocStatusDescrip,ReqTypeDescrip,ActualScore, ")
                .Append(" MinScore from ")
                .Append(" ( ")
                .Append(" Select Distinct ")
                .Append(" t1.adReqId, ")
                .Append(" t1.Descrip, ")
                .Append(" '00000000-0000-0000-0000-000000000000' as reqGrpId,'" & strEnrollDate & "' as CurrentDate, ")
                .Append(" t2.StartDate,t2.EndDate, ")
                .Append(" 1 as Required, ")
                .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append(" s3.LeadId =  '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
                .Append(" t6.Descrip  as ReqTypeDescrip, ")
                .Append(" (select ActualScore from adLeadEntranceTest where LeadId='" & LeadId & "' and EntrTestId=t1.adReqId) as ActualScore, ")
                .Append(" (select MinScore from adLeadEntranceTest where LeadId='" & LeadId & "' and EntrTestId=t1.adReqId) as MinScore ")
                .Append(" from ")
                .Append(" adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5,adReqTypes t6 ")
                .Append(" where ")
                .Append(" t1.adReqId = t2.adReqId and ")
                .Append(" t2.MandatoryRequirement <> 1 and ")
                .Append(" t1.adReqTypeId in (1,3) and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
                .Append(" t4.PrgVerId = t5.PrgVerId ")
                .Append(" and t1.adReqId = t5.adReqId and t1.adReqTypeId = t6.adReqTypeId and  ")
                .Append(" t5.PrgVerId in (select distinct PrgVerId from adLeads where LeadId='" & LeadId & "')  and ")
                .Append(" t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') ")
                .Append(" and t5.adReqId is not null ")
                .Append(" ) ")
                .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                ' Get Requirements === Requirement Group assigned to Program Version
                .Append(" select ")
                .Append(" distinct ")
                .Append(" adReqId,Descrip,ReqGrpId,StartDate,EndDate,Required,DocStatusDescrip,ReqTypeDescrip, ")
                .Append(" ActualScore,MinScore from ")
                .Append(" ( ")
                .Append(" Select Distinct t1.adReqId,t1.Descrip,'00000000-0000-0000-0000-000000000000' as reqGrpId,'" & strEnrollDate & "' as CurrentDate, ")
                .Append(" t2.StartDate,t2.EndDate,1 as Required,(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append(" s3.LeadId =  '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
                .Append(" t8.Descrip as ReqTypeDescrip, ")
                .Append(" (select ActualScore from adLeadEntranceTest where LeadId='" & LeadId & "' and EntrTestId=t1.adReqId) as ActualScore, ")
                .Append(" (select MinScore from adLeadEntranceTest where LeadId='" & LeadId & "' and EntrTestId=t1.adReqId) as MinScore ")
                .Append(" from ")
                .Append(" adReqs t1,adReqsEffectiveDates t2, ")
                .Append(" adReqLeadGroups t3,adPrgVerTestDetails t5,adReqGrpDef t6,adLeadByLeadGroups t7,adReqTypes t8 where ")
                .Append(" t1.adReqId = t2.adReqId and t1.adReqTypeId in (1,3) and ")
                .Append(" t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and ")
                .Append(" t3.LeadGrpId = t7.LeadGrpId and ")
                .Append(" t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t1.adReqTypeId = t8.adReqTypeId and  ")
                .Append(" t5.ReqGrpId is not null and t7.LeadId ='" & LeadId & "'  and ")
                .Append(" t5.PrgVerId in (select distinct PrgVerId from adLeads where LeadId='" & LeadId & "') ")
                .Append(" ) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" union ")
                'Get Requirements that were assigned to lead group but not part of any requirement group
                .Append("Select distinct ")
                .Append("   adReqId, ")
                .Append("   Descrip, ")
                .Append("   ReqGrpId, ")
                .Append("   StartDate, ")
                .Append("   EndDate, ")
                .Append("	    Required, ")
                .Append("    DocStatusDescrip, ")
                .Append(" ReqTypeDescrip, ")
                .Append(" ActualScore,  ")
                .Append(" MinScore ")
                .Append(" from ")
                .Append("  ( ")
                ' Get Requirement Group and requirements that are assigned to a lead group
                ' and part of a requirement group
                .Append(" Select distinct ")
                .Append("   	t1.adReqId, ")
                .Append("   	t1.Descrip, ")
                .Append(" '00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
                .Append(" '" & strEnrollDate & "' as CurrentDate, ")
                .Append("   	t2.StartDate, ")
                .Append("  	t2.EndDate, ")
                .Append("       	t3.IsRequired as Required, ")
                .Append("  (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
                .Append("  where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
                .Append("	          s3.LeadId =  '" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
                .Append(" t4.Descrip as ReqTypeDescrip, ")
                .Append(" (select ActualScore from adLeadEntranceTest where LeadId='" & LeadId & "' and EntrTestId=t1.adReqId) as ActualScore, ")
                .Append(" (select MinScore from adLeadEntranceTest where LeadId='" & LeadId & "' and EntrTestId=t1.adReqId) as MinScore ")
                .Append("  from ")
                .Append("          	adReqs t1, ")
                .Append("        	adReqsEffectiveDates t2, ")
                .Append("       adReqLeadGroups t3,adReqTypes t4 ")
                .Append(" where ")
                .Append(" t1.adReqId = t2.adReqId And t2.adReqEffectiveDateId = t3.adReqEffectiveDateId ")
                .Append("	       and t1.adReqTypeId = t4.adReqTypeId and ")
                .Append("	       t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId in (select distinct PrgVerId from adLeads where LeadId='" & LeadId & "')")
                .Append(" and s2.adReqId is null) ")
                .Append("		and t3.LeadGrpId in ")
                .Append("		(select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and ")
                .Append(" t1.adReqTypeId in (1,3) and t2.MandatoryRequirement <> 1 and ")
                .Append(" 		t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails  ")
                .Append(" where PrgVerId in (select distinct PrgVerId from adLeads where ")
                .Append(" LeadId='" & LeadId & "')) ")
                .Append("   ) ")
                .Append("  R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
                .Append(" ) R2 Order by ReqTypeDescrip,Descrip ")
            End With
            Return db.RunParamSQLDataSet(sb.ToString)
        Catch ex As System.Exception
        End Try
        Return Nothing
    End Function


    Public Function GetPendingReqsByLeadandPrgVersion_SP(ByVal LeadId As String) As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        ' Add empId to the parameter list
        db.AddParameter("@LeadId", New Guid(LeadId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As New DataSet

        ds = db.RunParamSQLDataSet_SP("USP_GetPendingReqsByLeadandPrgVersion_Enroll")
        Return ds
        db.CloseConnection()


    End Function
End Class

