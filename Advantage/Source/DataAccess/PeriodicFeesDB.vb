Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' PeriodicFeesDB.vb
'
' PeriodicFeesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class PeriodicFeesDB
    Public Function GetAllPeriodicFees(ByVal showActiveOnly As Boolean) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   PF.PeriodicFeeId, ")
            .Append("         PF.StatusId, ")
            .Append("         PF.TermId, ")
            .Append("         PF.TransCodeId, ")
            .Append("         PF.TuitionCategoryId, ")
            .Append("         PF.Amount, ")
            .Append("         PF.RateScheduleId, ")
            .Append("         PF.UnitId, ")
            .Append("         PF.ApplyTo, ")
            .Append("         PF.ProgTypId, ")
            .Append("         PF.PrgVerId ")
            .Append("FROM     saPeriodicFees PF, syStatuses ST ")
            .Append("WHERE    PF.StatusId = ST.StatusId ")
            If showActiveOnly Then
                .Append(" AND     ST.Status = 'Active' ")
            End If
        End With
        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetFutureTermsInWhichToApplyThisTransCodeId(ByVal termId As String, _
                                                                ByVal transCodeId As String, _
                                                                ByVal campusId As String, _
                                                                ByVal selApplyToThesePrograms As String, _
                                                                Optional ByVal PrgVerId As String = "") As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select Distinct")
            .Append("		T.TermId, ")
            .Append("		T.TermDescrip, ")
            .Append("		T.StartDate,T.ProgId	 ")
            '.Append(" (select Status	 from	syStatuses where Statusid = T.Statusid) as Status ")
            .Append("from	arTerm T ,syStatuses S ")
            .Append("where T.StatusId=S.StatusId and S.Status='Active' and ")
            If selApplyToThesePrograms = "2" And Not PrgVerId = "" Then
                .Append(" (T.ProgId is Null or T.ProgId in (Select Distinct ProgId from ")
                .Append(" arPrgVersions where PrgVerId=?)) and ")
            End If
            .Append("		T.StartDate > (Select StartDate from arTerm where TermId = ? )		 ")
            .Append("and	T.TermId not in (Select TermId from saPeriodicFees where TransCodeId = ? ) ")
            If campusId <> "" Then
                .Append("AND (T.CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId = '")
                .Append(campusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR     T.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
            .Append("order by ")
            .Append("	T.StartDate,T.TermDescrip ")
        End With


        If selApplyToThesePrograms = "2" And Not PrgVerId = "" Then
            'Add the PrgVerId parameter to the parameter list
            db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        ' Add the TermId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add the TransCodeId to the parameter list
        db.AddParameter("@TransCodeId", transCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetPeriodicFeeInfo(ByVal PeriodicFeeId As String) As PeriodicFeeInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT PF.PeriodicFeeId, ")
            .Append("    PF.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=PF.StatusId) Status, ")
            .Append("    PF.TermId, ")
            .Append("    (Select TermDescrip from arTerm where TermId=PF.TermId) Term, ")
            .Append("    PF.TransCodeId, ")
            .Append("    (Select TransCodeDescrip from saTransCodes where TransCodeId=PF.TransCodeId) TransCode, ")
            .Append("    PF.TuitionCategoryId, ")
            .Append("    (Select TuitionCategoryDescrip from saTuitionCategories where TuitionCategoryId=PF.TuitionCategoryId) TuitionCategory, ")
            .Append("    PF.Amount, ")
            .Append("    PF.RateScheduleId, ")
            .Append("    (Select RateScheduleDescrip from saRateSchedules where RateScheduleId=PF.RateScheduleId) RateSchedule, ")
            .Append("    PF.UnitId, ")
            .Append("    PF.ApplyTo, ")
            .Append("    PF.ProgTypId, ")
            .Append(" (SELECT    Description FROM arProgTypes   WHERE ProgTypId = PF.ProgTypId) ProgType ,")
            .Append("    PF.PrgVerId, ")
            .Append("(SELECT    PrgVerDescrip   FROM      arPrgVersions   WHERE     PrgVerId = PF.PrgVerId) ProgVerDescrip ,")
            .Append("    PF.TermStartDate, ")
            .Append("    PF.ModUser, ")
            .Append("    PF.ModDate ")
            .Append("FROM  saPeriodicFees PF ")
            .Append("WHERE PF.PeriodicFeeId= ? ")
        End With

        ' Add the PeriodicFeeId to the parameter list
        db.AddParameter("@PeriodicFeeId", PeriodicFeeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim PeriodicFeeInfo As New PeriodicFeeInfo

        While dr.Read()

            '   set properties with data from DataReader
            With PeriodicFeeInfo
                .PeriodicFeeId = PeriodicFeeId
                .IsInDB = True
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                If Not (dr("TermId") Is System.DBNull.Value) Then .TermId = CType(dr("TermId"), Guid).ToString
                If Not (dr("Term") Is System.DBNull.Value) Then .Term = dr("Term")
                If Not (dr("TransCodeId") Is System.DBNull.Value) Then .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                If Not (dr("TransCode") Is System.DBNull.Value) Then .TransCode = dr("TransCode")
                If Not (dr("TuitionCategoryId") Is System.DBNull.Value) Then .TuitionCategoryId = CType(dr("TuitionCategoryId"), Guid).ToString
                If Not (dr("TuitionCategory") Is System.DBNull.Value) Then .TuitionCategory = dr("TuitionCategory")
                .Amount = dr("Amount")
                If Not (dr("RateScheduleId") Is System.DBNull.Value) Then .RateScheduleId = CType(dr("RateScheduleId"), Guid).ToString
                If Not (dr("RateSchedule") Is System.DBNull.Value) Then .rateSchedule = dr("RateSchedule")
                .UnitId = CType(dr("UnitId"), UnitType)

                .ApplyTo = CType(dr("ApplyTo"), ApplyFeeToPrograms)
                Select Case .ApplyTo
                    Case 1
                        .ProgTypeId = CType(dr("ProgTypId"), Guid).ToString
                        .ProgType = CType(dr("ProgType"), String).ToString
                    Case 2
                        .PrgVerId = CType(dr("PrgVerId"), Guid).ToString
                        .PrgVerDescrip = CType(dr("ProgVerDescrip"), String).ToString
                End Select
                If Not (dr("TermStartDate") Is System.DBNull.Value) Then .TermStartDate = dr("TermStartDate")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return PeriodicFeeInfo
        Return PeriodicFeeInfo

    End Function
    Public Function UpdatePeriodicFeeInfo(ByVal PeriodicFeeInfo As PeriodicFeeInfo, ByVal user As String, ByVal terms() As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saPeriodicFees Set PeriodicFeeId = ?, ")
                .Append(" StatusId = ?, TermId = ?, ")
                .Append(" TransCodeId = ?, TuitionCategoryId = ?, ")
                .Append(" Amount = ?, RateScheduleId = ?, UnitId = ?, ")
                .Append(" ApplyTo = ?, ProgTypId = ?, PrgVerId = ?, TermStartDate = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE PeriodicFeeId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saPeriodicFees where ModDate = ? ")
            End With

            '   add parameters values to the query
            Dim now As DateTime = Date.Now
            BuildParameterListForInsertPeriodicFeeQuery(db, PeriodicFeeInfo, user, now)

            '   PeriodicFeeId
            db.AddParameter("@PeriodicFeeId", PeriodicFeeInfo.PeriodicFeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", PeriodicFeeInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString, groupTrans)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then

                'insert PeriodicFees records for future terms
                If terms.Length > 0 Then InsertPeriodicFeesForFutureTerms(db, PeriodicFeeInfo, user, terms, groupTrans)

                '   commit transaction 
                groupTrans.Commit()

                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Catch ex As Exception

            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return an error to the client
            Throw New Exception("Error Updating Term Fees", ex.InnerException)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddPeriodicFeeInfo(ByVal PeriodicFeeInfo As PeriodicFeeInfo, ByVal user As String, ByVal terms() As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   do an insert
        Try
            '   add parameters values to the query
            BuildParameterListForInsertPeriodicFeeQuery(db, PeriodicFeeInfo, user, Date.Now)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(BuildInsertQueryForPeriodicFees(), groupTrans)

            'insert PeriodicFees records for future terms
            If terms.Length > 0 Then InsertPeriodicFeesForFutureTerms(db, PeriodicFeeInfo, user, terms, groupTrans)

            '   commit transaction 
            groupTrans.Commit()

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Catch ex As Exception

            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return an error to the client
            Throw New Exception("Error Inserting Term Fees", ex.InnerException)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Private Function BuildInsertQueryForPeriodicFees() As String
        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("INSERT saPeriodicFees (PeriodicFeeId, StatusId, ")
            .Append("   TermId, ")
            .Append("   TransCodeId, TuitionCategoryId, ")
            .Append("   Amount, RateScheduleId, UnitId, ")
            .Append("   ApplyTo, ProgTypId, PrgVerId, TermStartDate, ")
            .Append("   ModUser, ModDate) ")
            .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
        End With
        Return sb.ToString()
    End Function
    Private Sub InsertPeriodicFeesForFutureTerms(ByVal db As DataAccess, ByVal periodicFeeInfo As PeriodicFeeInfo, ByVal user As String, ByVal terms() As String, ByVal groupTrans As OleDbTransaction)
        Dim now As DateTime = Date.Now
        Dim sqlCommand As String = BuildInsertQueryForPeriodicFees()
        For i As Integer = 0 To terms.Length - 1
            periodicFeeInfo.TermId = terms(i)
            periodicFeeInfo.PeriodicFeeId = Guid.NewGuid().ToString
            BuildParameterListForInsertPeriodicFeeQuery(db, periodicFeeInfo, user, now)
            db.RunParamSQLExecuteNoneQuery(sqlCommand, groupTrans)
        Next
    End Sub
    Private Sub BuildParameterListForInsertPeriodicFeeQuery(ByVal db As DataAccess, ByVal periodicFeeInfo As PeriodicFeeInfo, ByVal user As String, ByVal now As DateTime)
        '   add parameters values to the query
        db.ClearParameters()

        '   PeriodicFeeId
        db.AddParameter("@PeriodicFeeId", periodicFeeInfo.PeriodicFeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StatusId
        db.AddParameter("@StatusId", periodicFeeInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   TermId
        db.AddParameter("@TermId", periodicFeeInfo.TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   TransCodeId
        If periodicFeeInfo.TransCodeId = Guid.Empty.ToString Then
            db.AddParameter("@TransCodeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@TransCodeId", periodicFeeInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        '   TuitionCategoryId
        If periodicFeeInfo.TuitionCategoryId = Guid.Empty.ToString Then
            db.AddParameter("@TuitionCategoryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@TuitionCategoryId", periodicFeeInfo.TuitionCategoryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        '   Amount
        db.AddParameter("@Amount", periodicFeeInfo.Amount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

        '   RateScheduleId
        If periodicFeeInfo.RateScheduleId = Guid.Empty.ToString Then
            db.AddParameter("@RateScheduleId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@RateScheduleId", periodicFeeInfo.RateScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        '   Unit
        db.AddParameter("@UnitId", periodicFeeInfo.UnitId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   ApplyTo
        db.AddParameter("@ApplyTo", periodicFeeInfo.ApplyTo, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   ProgTypeId
        If periodicFeeInfo.ProgTypeId = Guid.Empty.ToString Then
            db.AddParameter("@ProgTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@ProgTypeId", periodicFeeInfo.ProgTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        '   PrgVerId
        If periodicFeeInfo.PrgVerId = Guid.Empty.ToString Then
            db.AddParameter("@PrgVerId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@PrgVerId", periodicFeeInfo.PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        '   TermStartDate
        If periodicFeeInfo.TermStartDate = Date.MinValue Then
            db.AddParameter("@TermStartDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        Else
            db.AddParameter("@TermStartDate", periodicFeeInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If

        '   ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   ModDate
        db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    End Sub
    Public Function DeletePeriodicFeeInfo(ByVal PeriodicFeeId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saPeriodicFees ")
                .Append("WHERE PeriodicFeeId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM saPeriodicFees WHERE PeriodicFeeId = ? ")
            End With

            '   add parameters values to the query

            '   PeriodicFeeId
            db.AddParameter("@PeriodicFeeId", PeriodicFeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   PeriodicFeeId
            db.AddParameter("@PeriodicFeeId", PeriodicFeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetTransCodesByTerm(ByVal termId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   PF.PeriodicFeeId, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("         PF.TermId, ")
            .Append("         (Select TermDescrip from arTerm where TermId=PF.TermId) As Term, ")
            .Append("         PF.TransCodeId, ")
            .Append("         (Select TransCodeDescrip from saTransCodes where TransCodeId=PF.TransCodeId) As TransCodeDescrip, ")
            .Append("         PF.TuitionCategoryId, ")
            .Append("         PF.Amount, ")
            .Append("         PF.RateScheduleId, ")
            .Append("         PF.UnitId, ")
            .Append("         PF.ApplyTo, ")
            .Append("         PF.ProgTypId, ")
            .Append("         PF.PrgVerId, ")
            .Append("         PF.TermStartDate ")
            '.Append("         (Select count(*) from saTransactions where TermId=?) As NumberOfTransactionsPosted ")
            .Append("FROM     saPeriodicFees PF, syStatuses ST ")
            .Append("WHERE    PF.StatusId=ST.StatusId ")
            .Append("AND      PF.TermId = ? ")
            .Append("ORDER BY TransCodeDescrip ")
        End With

        ' Add the courseId to the parameter list
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class

