Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class PrgVerDetailsDB
    Public Function GetAllPrgVerDS(ByVal PrgVerId As String, Optional ByVal campusid As String = "") As DataSet

        '   create dataset
        Dim ds As New DataSet
        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusid)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        '   build select query for the ProgramVersion data adapter
        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            With sb
                .Append("select JC.PrgVerId,JC.PrgVerDescrip ")
                .Append("FROM    arPrgVersions JC,syStatuses ST ")
                .Append("WHERE   JC.StatusId = ST.StatusId ")
                .Append(" AND    ST.Status = 'Active' ")
                .Append(" Order By JC.PrgVerDescrip")
            End With
        End With

        '   build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

        '   Create adapter to handle PrgVersions table
        Dim da As New OleDbDataAdapter(sc)

        '   Fill PrgVersions table
        da.Fill(ds, "PrgVersions")

        '   build select query for the Documents data adapter
        sb = New StringBuilder

        'Commented out by BN 09/09/05 to accomodate the effective dates issue
        'With sb
        '    .Append("SELECT t1.adReqId,t1.Descrip,t1.Code,t1.adReqTypeId,t2.MinScore ")
        '    .Append("FROM adReqs t1, adReqsEffectiveDates t2 WHERE ")
        '    .Append("t1.adReqId not in (Select adReqId from adPrgVerTestDetails t3 where PrgVerId = ? and adReqId is not null) ")
        '    .Append("and t1.AppliesToAll = 0 and t1.adReqId = t2.adReqId ")
        '    .Append("ORDER BY t1.Descrip")

        'End With

        Dim strCurrentDate As Date = Date.Now.ToShortDateString

        With sb
            .Append(" select Distinct  ")
            .Append("	adReqId,Descrip,Code,adReqTypeId,MinScore,CampGrpId from  ")
            .Append("	( ")
            .Append("	select Distinct ")
            .Append("              adReqId,  ")
            .Append("              Descrip,  ")
            .Append("              StartDate, ")
            .Append("              EndDate, ")
            .Append("              Minscore,  ")
            .Append("              Required, ")
            .Append("	       adReqTypeId, ")
            .Append("		Code,CampGrpId ")
            .Append("              from ")
            'Get List Of Requirements that are mandatory for all requirement
            .Append("                ( select ")
            .Append("                        t1.adReqId, ")
            .Append("                        t1.Descrip, ")
            .Append("			   t1.code, ")
            .Append("                        '" & strCurrentDate & "' as CurrentDate, ")
            .Append("                        t2.StartDate, ")
            .Append("                        t2.EndDate, ")
            .Append("                        t2.Minscore, ")
            .Append("                        1 as Required, ")
            .Append("		t3.adReqTypeId,t1.CampGrpId ")
            .Append("                    from ")
            .Append("                        adReqs t1, ")
            .Append("                        adReqsEffectiveDates t2,adReqTypes t3 ")
            .Append("                    where  ")
            .Append("                        t1.adReqId = t2.adReqId and t1.adReqTypeId = t3.adReqTypeId and ")
            .Append("                        t1.adReqTypeId in (1,3) and  ")
            .Append("                        t2.MandatoryRequirement <> 1 ")
            ' ''modified by Saraswathi Lakshmanan on Sept 15 2010
            ' ''To show only the Documents mapped to Admissions Module ie 2
            ' ''For Document Tracking
            '.Append("                     and   t1.ReqforEnrollment=1  ")
            .Append("			   and t1.adReqId not in  ")
            .Append("				(Select adReqId from adPrgVerTestDetails t3 where PrgVerId = ? ")
            .Append("				and adReqId is not null) ")
            .Append("              ) ")
            .Append("                    R1 where R1.CurrentDate >= R1.StartDate and ")
            .Append("                    (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("	) R2 ")
            If Not strCampGrpId = "" Then
                .Append("  WHERE R2.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" Order by Descrip ")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        da.SelectCommand.Parameters.Clear()
        da.SelectCommand.Parameters.Add(New OleDbParameter("ProgVerId", PrgVerId))


        '   fill Documents table
        da.Fill(ds, "AvailReqs")

        '   build select query for the PrgVerDocs data adapter
        sb = New StringBuilder
        With sb
            .Append("SELECT t1.ReqGrpId,t1.Descrip,t1.Code ")
            .Append("FROM adReqGroups t1 WHERE ")
            .Append("t1.ReqGrpId not in (Select ReqGrpId from adPrgVerTestDetails t3 where PrgVerId = ? and ReqGrpId is not null) ")
            .Append("and t1.IsMandatoryReqGrp = 0 ")
            If Not strCampGrpId = "" Then
                .Append("  and t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append("ORDER BY t1.Descrip")

        End With


        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        da.SelectCommand.Parameters.Clear()
        da.SelectCommand.Parameters.Add(New OleDbParameter("ProgVerId", PrgVerId))

        '   fill PrgVerDocs table
        da.Fill(ds, "AvailReqGrps")

        '   build select query for the PrgVerDocs data adapter
        sb = New StringBuilder

        'Commented out by BN 09/09/05 to accomodate the effective dates issue
        'With sb

        '    .Append("SELECT a.*, (Select b.adReqTypeId from adReqs b where b.adReqId = a.adReqId)as adReqTypeId ")
        '    .Append("FROM adPrgVerTestDetails a ")
        '    .Append("WHERE a.PrgVerId = ? ")

        'End With

        With sb
            .Append(" select Distinct  ")
            .Append("	adReqId,Descrip,Code,adReqTypeId,MinScore,ReqGrpId,CampGrpId from  ")
            .Append("	( ")
            .Append("	select Distinct ")
            .Append("              adReqId,  ")
            .Append("              Descrip,  ")
            .Append("              StartDate, ")
            .Append("              EndDate, ")
            .Append("              Minscore,  ")
            .Append("              Required, ")
            .Append("	       adReqTypeId, ")
            .Append("		   Code, ")
            .Append("          null as ReqGrpId,CampGrpId ")
            .Append("              from ")
            'Get List Of Requirements that are mandatory for all requirement
            .Append("                ( select ")
            .Append("                        t1.adReqId, ")
            .Append("                        t1.Descrip, ")
            .Append("			   t1.code, ")
            .Append("                        '" & strCurrentDate & "' as CurrentDate, ")
            .Append("                        t2.StartDate, ")
            .Append("                        t2.EndDate, ")
            .Append("                        t2.Minscore, ")
            .Append("                        1 as Required, ")
            .Append("		t3.adReqTypeId,t1.CampGrpId as CampGrpId ")
            .Append("                    from ")
            .Append("                        adReqs t1, ")
            .Append("                        adReqsEffectiveDates t2,adReqTypes t3 ")
            .Append("                    where  ")
            .Append("                        t1.adReqId = t2.adReqId and t1.adReqTypeId = t3.adReqTypeId and ")
            .Append("                        t1.adReqTypeId in (1,3) and  ")
            .Append("                        t2.MandatoryRequirement <> 1 ")
            .Append("			   and t1.adReqId in  ")
            .Append("				(Select adReqId from adPrgVerTestDetails t3 where PrgVerId = ? ")
            .Append("				and adReqId is not null) ")
            .Append("              ) ")
            .Append("                    R1 where R1.CurrentDate >= R1.StartDate and ")
            .Append("                    (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("UNION ")
            .Append("Select Distinct Null as adReqId, ")
            .Append("Descrip, ")
            .Append("Null as StartDate, ")
            .Append("Null as EndDate, ")
            .Append("0 as MinScore, ")
            .Append("Null as Required, ")
            .Append("Null as adReqTypeId, ")
            .Append("Code, ")
            .Append("t1.ReqGrpId,t1.CampGrpId as CampGrpId ")
            .Append("from adReqGroups t1, adPrgVerTestDetails t2 ")
            .Append("where t1.ReqGrpId = t2.ReqGrpId and t2.PrgVerId = ? ")
            .Append("and t2.adReqId is null and t2.ReqGrpId is not null ")
            .Append("	) R2 ")
            If Not strCampGrpId = "" Then
                .Append("  WHERE R2.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" order by Descrip ")
        End With

        '.Append("SELECT a.*, b.adReqTypeId ")
        '.Append("FROM adPrgVerTestDetails a, adReqs b ")
        '.Append("WHERE a.PrgVerId = ? and a.adReqId = b.adReqId ")

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        da.SelectCommand.Parameters.Clear()
        da.SelectCommand.Parameters.Add(New OleDbParameter("ProgVerId", PrgVerId))
        da.SelectCommand.Parameters.Add(New OleDbParameter("PrgVerId", PrgVerId))

        '   fill PrgVerDocs table
        da.Fill(ds, "Selected")
        '   create primary and foreign key constraints

        '   set primary key for PrgVersions table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("PrgVersions").Columns("PrgVerId")
        ds.Tables("PrgVersions").PrimaryKey = pk0

        '   set primary key for adReqs table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("AvailReqs").Columns("adReqId")
        ds.Tables("AvailReqs").PrimaryKey = pk1

        '   set primary key for adReqGrps table
        Dim pk2(0) As DataColumn
        pk2(0) = ds.Tables("AvailReqGrps").Columns("ReqGrpId")
        ds.Tables("AvailReqGrps").PrimaryKey = pk2

        '   set foreign key column in PrgVerDocs table
        'Dim fk0(0) As DataColumn
        'fk0(0) = ds.Tables("Selected").Columns("PrgVerTestDetailId")
        'ds.Tables("Selected").PrimaryKey = fk0
        ''   set foreign key column in PrgVerDocs table
        'Dim fk1(0) As DataColumn
        'fk1(0) = ds.Tables("PrgVerDocs").Columns("DocumentId")

        ''   set foreign keys in PrgVerDocs table
        'ds.Relations.Add("PrgVersionsPrgVerDocs", pk0, fk0)
        'ds.Relations.Add("DocumentsPrgVerDocs", pk1, fk1)

        '   return dataset
        Return ds

    End Function
    'Public Function GetAllPrgVersions() As DataSet

    '    'connect to the database
    '    Dim db As New DataAccess
    '    Dim sb As New System.Text.StringBuilder
    '    Dim da As New OleDbDataAdapter
    '    Dim ds As New DataSet


    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '    Try
    '        With sb

    '            .Append("select JC.PrgVerId,JC.PrgVerDescrip ")
    '            .Append("FROM    arPrgVersions JC,syStatuses ST ")
    '            .Append("WHERE   JC.StatusId = ST.StatusId ")
    '            .Append(" AND    ST.Status = 'Active' ")
    '            .Append(" Order By JC.PrgVerDescrip")

    '        End With

    '        '   Execute the query
    '        db.AddParameter("@edate1", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        db.AddParameter("@edate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        db.OpenConnection()
    '        da = db.RunParamSQLDataAdapter(sb.ToString)
    '        da.Fill(ds, "ProgVer")

    '        'Close Connection
    '        db.CloseConnection()

    '    Catch ex As System.Exception
    '        Throw New BaseException(ex.Message)
    '    End Try

    '    'Return the datatable in the dataset
    '    Return ds

    'End Function
    Public Function GetAllPrgVersions(Optional ByVal campusId As String = "") As DataSet

        'connect to the database
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim da As New OleDbDataAdapter
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            With sb

                .Append("select JC.PrgVerId,JC.PrgVerDescrip, ")
                .Append(" case when (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=JC.ProgId) is null  then ")
                .Append(" JC.PrgVerDescrip ")
                .Append(" else ")
                .Append(" JC.PrgVerDescrip + ' (' + (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=JC.ProgId) + ')'  ")
                .Append(" end as PrgVerShiftDescrip  ")
                .Append("FROM    arPrgVersions JC,syStatuses ST ")
                .Append("WHERE   JC.StatusId = ST.StatusId ")
                .Append(" AND    ST.Status = 'Active' ")

                If campusId <> "" Then
                    .Append("AND (JC.CampGrpId IN(SELECT CampGrpId ")
                    .Append("FROM syCmpGrpCmps ")
                    .Append("WHERE CampusId = ? ")
                    .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                    .Append("OR JC.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                End If


                .Append(" Order By JC.PrgVerDescrip")

            End With

            '   Execute the query
            'db.AddParameter("@edate1", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'db.AddParameter("@edate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            If campusId <> "" Then
                db.AddParameter("cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "ProgVer")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function


    Public Function GetReqName(ByVal ReqId As String) As String
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT Descrip ")
            .Append("FROM adReqs ")
            .Append("WHERE adReqId = ?")
        End With

        db.AddParameter("@ReqID", ReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Return DirectCast(db.RunParamSQLScalar(sb.ToString), String)
    End Function
    Public Function GetReqGrpName(ByVal ReqGrpId As String) As String
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT Descrip ")
            .Append("FROM adReqGroups ")
            .Append("WHERE ReqGrpId = ?")
        End With

        db.AddParameter("@ReqGrpID", ReqGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Return DirectCast(db.RunParamSQLScalar(sb.ToString), String)
    End Function
    Public Function GetReqsForReqGroup(ByVal ReqGrpId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT t1.adReqId, t2.Descrip,t2.adReqTypeId ")
            .Append("FROM adReqGrpDef t1, adReqs t2 ")
            .Append("WHERE t1.adReqId=t2.adReqId ")
            .Append("AND t1.ReqGrpId = ? ")

        End With

        db.AddParameter("@reqgrpid", ReqGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Function AssignReqsToPrgVer(ByVal ChildInfoObj As ReqInfo, ByVal ID As String, ByVal user As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim childID As String
        Dim childTyp As String
        Dim childSeq As Integer
        Dim req As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        'childID = ChildInfoObj.ChildId
        '' childTyp = ChildInfoObj.ChildType
        'childSeq = ChildInfoObj.ChildSeq
        ' req = ChildInfoObj.Req


        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


        With sb
            .Append("INSERT INTO adPrgVerTestDetails(PrgVerId,ReqGrpId,adReqId,MinScore,ModUser,ModDate) ")
            .Append("VALUES(?,?,?,?,?,?)")
        End With

        db.AddParameter("@coursegrpid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        If ChildInfoObj.ReqGrpId = "" Then
            db.AddParameter("@ReqGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@ReqGrpId", ChildInfoObj.ReqGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If ChildInfoObj.ReqId = "" Then
            db.AddParameter("@ReqId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@ReqId", ChildInfoObj.ReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        'db.AddParameter("@childseq", childSeq, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@req", ChildInfoObj.MinScore, DataAccess.OleDbDataType.OleDbDecimal, 4, ParameterDirection.Input)
        '   ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   ModDate
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)



        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function

    'Public Function UpdatePrgVerDef(ByVal ChildInfoObj As ReqInfo, ByVal ID As String, ByVal user As String)
    '    Dim db As New DataAccess
    '    Dim sb As New System.Text.StringBuilder
    '    Dim ds As DataSet
    '    Dim childID As String
    '    Dim childTyp As String
    '    Dim childSeq As Integer
    '    Dim req As Integer



    '    'Set the connection string
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")


    '    With sb
    '        .Append("UPDATE adPrgVerTestDetails Set Sequence = ?, ")
    '        .Append("ModUser=?, ")
    '        .Append("ModDate=? ")
    '        .Append("WHERE  adReqId = ? and ReqGrpId = ?")
    '        .Append("AND ModDate = ? ;")
    '        .Append("Select count(*) from adReqGrpDef where adReqId = ? and ReqGrpId = ? and ModDate = ? ")

    '        db.AddParameter("@childseq", childSeq, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        '   ModDate
    '        Dim now As Date = Date.Now
    '        db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        db.AddParameter("@childid", childID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@ReqGrpId", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        '   ModDate
    '        db.AddParameter("@Original_ModDate", ChildInfoObj.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        db.AddParameter("@childid2", childID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@ReqGrpId2", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        '   ModDate
    '        db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '    End With



    '    db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '    db.ClearParameters()
    '    sb.Remove(0, sb.Length)

    '    'Close Connection
    '    db.CloseConnection()
    'End Function

    Public Function DeleteReqsFrmPrgVer(ByVal ChildInfoObj As ReqInfo, ByVal ID As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        If ChildInfoObj.ReqGrpId <> "" Then
            With sb
                .Append("DELETE FROM adPrgVerTestDetails ")
                .Append("WHERE PrgVerId = ? ")
                .Append("AND ReqGrpId = ? ")
            End With
            db.AddParameter("@ReqGrpId", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@childid", ChildInfoObj.ReqGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ElseIf ChildInfoObj.ReqId <> "" Then
            With sb
                .Append("DELETE FROM adPrgVerTestDetails ")
                .Append("WHERE PrgVerId = ? ")
                .Append("AND adReqId = ? ")
            End With
            db.AddParameter("@ReqGrpId", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@childid", ChildInfoObj.ReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If





        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function
End Class
