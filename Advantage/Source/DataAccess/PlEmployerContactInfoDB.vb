Imports FAME.Advantage.Common

'=================================================
'FAME.AdvantageV1.BusinessFacade
'
'plEmployerInfoDB.vb
'
'Placement Employer Info DataAccess Interface
'
'=================================================
'CopyRight (c) 2003-2004 FAME Inc.
'
'All Rights Reserved
'=================================================

Public Class PlEmployerContactInfoDB
    Public Function UpdateEmployerInfo(ByVal employerinfo As plEmployerContact, ByVal User As String) As String

        'Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE plEmployerContact Set LastName=?,FirstName=?,MiddleName=?, ")
                .Append(" StatusId=?,PrefixId=?,SuffixId=?,TitleId=?, ")
                .Append(" WorkPhone=?,HomePhone=?,CellPhone=?,Beeper=?,")
                .Append(" WorkEmail=?,HomeEmail=?, ")
                .Append(" ModUser=?, ")
                .Append(" ModDate=?, ")
                .Append(" Address1=?,Address2=?,City=?,State=?,Zip=?,Country=?, ")
                .Append(" WorkExt=?,WorkBestTime=?,HomeBestTime=?,CellBestTime=?,PinNumber=?,Notes=?,AddressTypeID=?,EmployerId=?, ")
                .Append(" ForeignHomePhone=?,ForeignCellPhone=?,ForeignZip=?,ForeignWorkPhone=?,OtherState=? ")
                .Append(" Where EmployerContactId=? ")
            End With

            'Add Parameters To Query

            'LastName
            If employerinfo.LastName = "" Then
                db.AddParameter("@LastName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@LastName", employerinfo.LastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'FirstName
            If employerinfo.FirstName = "" Then
                db.AddParameter("@FirstName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@FirstName", employerinfo.FirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'MiddleName
            If employerinfo.MiddleName = "" Then
                db.AddParameter("@MiddleName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@MiddleName", employerinfo.MiddleName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'StatusId
            If employerinfo.Status = Guid.Empty.ToString Or employerinfo.Status = "" Then
                db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StatusId", employerinfo.Status, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Prefix
            If employerinfo.Prefix = Guid.Empty.ToString Or employerinfo.Prefix = "" Then
                db.AddParameter("@PrefixId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PrefixId", employerinfo.Prefix, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Suffix
            If employerinfo.Suffix = Guid.Empty.ToString Or employerinfo.Suffix = "" Then
                db.AddParameter("@SuffixId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@SuffixId", employerinfo.Suffix, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Title
            If employerinfo.Title = Guid.Empty.ToString Or employerinfo.Title = "" Then
                db.AddParameter("@TitleId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@TitleId", employerinfo.Title, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'WorkPhone
            If employerinfo.WorkPhone = "" Then
                db.AddParameter("@WorkPhone", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@WorkPhone", employerinfo.WorkPhone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'HomePhone
            If employerinfo.HomePhone = "" Then
                db.AddParameter("@HomePhone", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@HomePhone", employerinfo.HomePhone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'CellPhone
            If employerinfo.CellPhone = "" Then
                db.AddParameter("@CellPhone", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CellPhone", employerinfo.CellPhone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Beeper
            If employerinfo.Beeper = "" Then
                db.AddParameter("@Beeper", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Beeper", employerinfo.Beeper, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'WorkEmail
            If employerinfo.WorkEmail = "" Then
                db.AddParameter("@WorkEmail", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@WorkEmail", employerinfo.WorkEmail, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'HomeEmail
            If employerinfo.HomeEmail = "" Then
                db.AddParameter("@HomeEmail", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@HomeEmail", employerinfo.HomeEmail, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ModUser
            db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", employerinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'Address1
            If employerinfo.Address1 = "" Then
                db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Address1", employerinfo.Address1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Address2
            If employerinfo.Address2 = "" Then
                db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", employerinfo.Address2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'City
            If employerinfo.City = "" Then
                db.AddParameter("@city", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@city", employerinfo.City, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'State
            If employerinfo.State = "" Or employerinfo.State = Guid.Empty.ToString Then
                db.AddParameter("@State", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@State", employerinfo.State, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Zip
            If employerinfo.Zip = "" Then
                db.AddParameter("@Zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Zip", employerinfo.Zip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Country
            If employerinfo.Country = Guid.Empty.ToString Or employerinfo.Country = "" Then
                db.AddParameter("@Country", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Country", employerinfo.Country, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'WorkExtension
            If employerinfo.WorkExt = "" Then
                db.AddParameter("@WorkExt", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@WorkExt", employerinfo.WorkExt, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'WorkBestTime
            If employerinfo.WorkBestTime = "" Then
                db.AddParameter("@WorkBestTime", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@WorkBestTime", employerinfo.WorkBestTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'HomeBestTime
            If employerinfo.HomeBestTime = "" Then
                db.AddParameter("@HomeBestTime", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@HomeBestTime", employerinfo.HomeBestTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'CellBestTime
            If employerinfo.CellBestTime = "" Then
                db.AddParameter("@CellBestTime", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CellBestTime", employerinfo.CellBestTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Pin Number
            If employerinfo.PinNumber = "" Then
                db.AddParameter("@PinNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PinNumber", employerinfo.PinNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Notes
            If employerinfo.Notes = "" Then
                db.AddParameter("@Notes", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@Notes", employerinfo.Notes, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If

            'Address Type
            If employerinfo.AddressTypeId = Guid.Empty.ToString Or employerinfo.AddressTypeId = "" Then
                db.AddParameter("@AddressTypeID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AddressTypeID", employerinfo.AddressTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'EmployerID
            If employerinfo.EmployerId = Guid.Empty.ToString Or employerinfo.EmployerId = "" Then
                db.AddParameter("@EmployerId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@EmployerId", employerinfo.EmployerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            db.AddParameter("@ForeignHomePhone", employerinfo.ForeignHomePhone, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@ForeignCellPhone", employerinfo.ForeignCellPhone, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@ForeignZip", employerinfo.ForeignZip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@ForeignWorkPhone", employerinfo.ForeignWorkPhone, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            If employerinfo.OtherState = "" Then
                db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@OtherState", employerinfo.OtherState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'EmployerContactId
            db.AddParameter("@EmployerContactId", employerinfo.EmployerContactId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            'Retun Without Errors
            Return ""
        Catch ex As System.Exception
            'Return an Error To Client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Function AddEmployerInfo(ByVal employerinfo As plEmployerContact, ByVal user As String) As String

        'Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("Insert plEmployerContact(EmployerContactId,")
                .Append(" LastName,FirstName,MiddleName, ")
                .Append(" StatusId,PrefixId,SuffixId,TitleId, ")
                .Append(" WorkPhone,HomePhone,cellPhone,Beeper, ")
                .Append(" WorkEmail,HomeEmail,ModUser,ModDate, ")
                .Append(" Address1,Address2,City,State,Zip,Country, ")
                .Append(" WorkExt,WorkBestTime,HomeBestTime,CellBestTime, ")
                .Append(" PinNumber,Notes,AddressTypeID,EmployerId,ForeignHomePhone,ForeignCellPhone,ForeignZip,ForeignWorkPhone,OtherState) ")
                .Append(" Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            'Add Parameters To Query
            'EmployerId
            db.AddParameter("@EmployerContactId", employerinfo.EmployerContactId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'LastName
            If employerinfo.LastName = "" Then
                db.AddParameter("@LastName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@LastName", employerinfo.LastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'FirstName
            If employerinfo.FirstName = "" Then
                db.AddParameter("@FirstName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@FirstName", employerinfo.FirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'MiddleName
            If employerinfo.MiddleName = "" Then
                db.AddParameter("@MiddleName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@MiddleName", employerinfo.MiddleName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'StatusId
            If employerinfo.Status = Guid.Empty.ToString Or employerinfo.Status = "" Then
                db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StatusId", employerinfo.Status, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Prefix
            If employerinfo.Prefix = Guid.Empty.ToString Or employerinfo.Prefix = "" Then
                db.AddParameter("@PrefixId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PrefixId", employerinfo.Prefix, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Suffix
            If employerinfo.Suffix = Guid.Empty.ToString Or employerinfo.Suffix = "" Then
                db.AddParameter("@SuffixId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@SuffixId", employerinfo.Suffix, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Title
            If employerinfo.Title = Guid.Empty.ToString Or employerinfo.Title = "" Then
                db.AddParameter("@TitleId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@TitleId", employerinfo.Title, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'WorkPhone
            If employerinfo.WorkPhone = "" Then
                db.AddParameter("@WorkPhone", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@WorkPhone", employerinfo.WorkPhone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'HomePhone
            If employerinfo.HomePhone = "" Then
                db.AddParameter("@HomePhone", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@HomePhone", employerinfo.HomePhone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'CellPhone
            If employerinfo.CellPhone = "" Then
                db.AddParameter("@CellPhone", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CellPhone", employerinfo.CellPhone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Beeper
            If employerinfo.Beeper = "" Then
                db.AddParameter("@Beeper", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Beeper", employerinfo.Beeper, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'WorkEmail
            If employerinfo.WorkEmail = "" Then
                db.AddParameter("@WorkEmail", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@WorkEmail", employerinfo.WorkEmail, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'HomeEmail
            If employerinfo.HomeEmail = "" Then
                db.AddParameter("@HomeEmail", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@HomeEmail", employerinfo.HomeEmail, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", employerinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'Address1
            If employerinfo.Address1 = "" Then
                db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Address1", employerinfo.Address1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Address2
            If employerinfo.Address2 = "" Then
                db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", employerinfo.Address2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'City
            If employerinfo.City = "" Then
                db.AddParameter("@city", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@city", employerinfo.City, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'State
            If employerinfo.State = "" Or employerinfo.State = Guid.Empty.ToString Then
                db.AddParameter("@State", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@State", employerinfo.State, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Zip
            If employerinfo.Zip = "" Then
                db.AddParameter("@Zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Zip", employerinfo.Zip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Country
            If employerinfo.Country = Guid.Empty.ToString Or employerinfo.Country = "" Then
                db.AddParameter("@Country", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Country", employerinfo.Country, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'WorkExtension
            If employerinfo.WorkExt = "" Then
                db.AddParameter("@WorkExt", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@WorkExt", employerinfo.WorkExt, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'WorkBestTime
            If employerinfo.WorkBestTime = "" Then
                db.AddParameter("@WorkBestTime", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@WorkBestTime", employerinfo.WorkBestTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'HomeBestTime
            If employerinfo.HomeBestTime = "" Then
                db.AddParameter("@HomeBestTime", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@HomeBestTime", employerinfo.HomeBestTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'CellBestTime
            If employerinfo.CellBestTime = "" Then
                db.AddParameter("@CellBestTime", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CellBestTime", employerinfo.CellBestTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Pin Number
            If employerinfo.PinNumber = "" Then
                db.AddParameter("@PinNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PinNumber", employerinfo.PinNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Notes
            If employerinfo.Notes = "" Then
                db.AddParameter("@Notes", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@Notes", employerinfo.Notes, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If

            'Address Type
            If employerinfo.AddressTypeId = Guid.Empty.ToString Or employerinfo.AddressTypeId = "" Then
                db.AddParameter("@AddressTypeID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AddressTypeID", employerinfo.AddressTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'EmployerID
            If employerinfo.EmployerId = Guid.Empty.ToString Or employerinfo.EmployerId = "" Then
                db.AddParameter("@EmployerId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@EmployerId", employerinfo.EmployerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            db.AddParameter("@ForeignHomePhone", employerinfo.ForeignHomePhone, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@ForeignCellPhone", employerinfo.ForeignCellPhone, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@ForeignZip", employerinfo.ForeignZip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@ForeignWorkPhone", employerinfo.ForeignWorkPhone, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            If employerinfo.OtherState = "" Then
                db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@OtherState", employerinfo.OtherState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""
            'Catch ex As System.Exception
            '    'Return an Error To Client
            '    Return -1
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Private Function GetEmployerInfo(ByVal EmployerInfo As plEmployerInfo) As Integer

    End Function

    Public Function DeleteEmployerInfo(ByVal EmployerId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM plEmployerContact ")
                .Append("WHERE EmployerContactId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM plEmployerContact WHERE EmployerContactId = ? ")
            End With


            'EmployerContactId
            db.AddParameter("@EmployerContactId", EmployerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate 
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'EmployerContactId
            db.AddParameter("@EmployerContactId", EmployerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            'execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            'If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try


    End Function

    Public Function GetEmployerContactInfo(ByVal EmployerId As String) As plEmployerContact

        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("SELECT     B.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=B.StatusId) as Status, ")
            .Append("    B.LastName, ")
            .Append("    B.FirstName, ")
            .Append("    B.MiddleName, ")
            .Append("    B.PrefixId, ")
            .Append("   (Select PrefixDescrip from syPrefixes where PrefixId=B.PrefixId) As Prefix, ")
            .Append(" B.SuffixId, ")
            .Append("   (Select SuffixDescrip from sySuffixes where SuffixId=B.SuffixId) Suffix, ")
            .Append(" B.TitleId, ")
            .Append(" B.HomePhone, ")
            .Append(" B.WorkPhone, ")
            .Append(" B.CellPhone, ")
            .Append(" B.Beeper, ")
            .Append(" B.WorkEmail, ")
            .Append(" B.HomeEmail, ")
            .Append(" B.Address1,B.Address2,B.City,B.State As StateId,B.Zip, ")
            .Append("   (Select StateDescrip from syStates where StateId=B.State) As State, ")
            .Append(" B.Country As CountryId,B.WorkExt,B.WorkBestTime,B.HomeBestTime,B.CellBestTime, ")
            .Append("   (Select CountryDescrip from AdCountries where CountryId=B.Country) As Country, ")
            .Append(" B.PinNumber,B.Notes,B.AddressTypeID,B.ForeignHomePhone,B.ForeignCellPhone, ")
            .Append("   (Select AddressDescrip from plAddressTypes where AddressTypeId=B.AddressTypeId) As AddressType, ")
            .Append(" B.ForeignZip,B.ForeignWorkPhone,B.OtherState,B.ModDate ")
            .Append("FROM  plEmployerContact B ")
            .Append("WHERE B.EmployerContactId= ? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@EmployerContactId", EmployerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim employerinfo As New plEmployerContact

        While dr.Read()

            'set properties with data from DataReader
            With employerinfo
                .IsInDB = True
                .EmployerContactId = EmployerId
                If Not (dr("StatusId") Is System.DBNull.Value) Then .StatusId = CType(dr("StatusId"), Guid).ToString
                If Not (dr("PrefixId") Is System.DBNull.Value) Then .PrefixID = CType(dr("PrefixId"), Guid).ToString
                If Not (dr("Prefix") Is System.DBNull.Value) Then .Prefix = dr("Prefix")
                If Not (dr("SuffixId") Is System.DBNull.Value) Then .SuffixId = CType(dr("SuffixId"), Guid).ToString
                If Not (dr("Suffix") Is System.DBNull.Value) Then .Suffix = dr("Suffix")
                If Not (dr("TitleId") Is System.DBNull.Value) Then .Title = dr("TitleId") Else .Title = ""
                If Not (dr("WorkEmail") Is System.DBNull.Value) Then .WorkEmail = dr("WorkEmail") Else .WorkEmail = ""
                If Not (dr("HomeEmail") Is System.DBNull.Value) Then .HomeEmail = dr("HomeEmail") Else .HomeEmail = ""
                If Not (dr("WorkPhone") Is System.DBNull.Value) Then .WorkPhone = dr("WorkPhone") Else .WorkPhone = ""
                If Not (dr("HomePhone") Is System.DBNull.Value) Then .HomePhone = dr("HomePhone") Else .HomePhone = ""
                If Not (dr("CellPhone") Is System.DBNull.Value) Then .CellPhone = dr("CellPhone") Else .CellPhone = ""
                If Not (dr("Beeper") Is System.DBNull.Value) Then .Beeper = dr("Beeper") Else .Beeper = ""
                If Not (dr("FirstName") Is System.DBNull.Value) Then .FirstName = dr("FirstName") Else .FirstName = ""
                If Not (dr("LastName") Is System.DBNull.Value) Then .LastName = dr("lastname") Else .LastName = ""
                If Not (dr("MiddleName") Is System.DBNull.Value) Then .MiddleName = dr("MiddleName") Else .MiddleName = ""
                If Not (dr("Address1") Is System.DBNull.Value) Then .Address1 = dr("Address1").ToString Else .Address1 = ""
                If Not (dr("Address2") Is System.DBNull.Value) Then .Address2 = dr("Address2").ToString Else .Address2 = ""
                If Not (dr("City") Is System.DBNull.Value) Then .City = dr("City").ToString Else .City = ""
                If Not (dr("StateId") Is System.DBNull.Value) Then .StateId = CType(dr("StateId"), Guid).ToString
                If Not (dr("State") Is System.DBNull.Value) Then .State = dr("State")
                If Not (dr("Zip") Is System.DBNull.Value) Then .Zip = dr("zip").ToString Else .Zip = ""
                If Not (dr("CountryId") Is System.DBNull.Value) Then .CountryId = CType(dr("CountryId"), Guid).ToString
                If Not (dr("Country") Is System.DBNull.Value) Then .Country = dr("Country")
                If Not (dr("WorkExt") Is System.DBNull.Value) Then .WorkExt = dr("WorkExt").ToString Else .WorkExt = ""
                If Not (dr("WorkBestTime") Is System.DBNull.Value) Then .WorkBestTime = dr("WorkBestTime").ToString Else .WorkBestTime = ""
                If Not (dr("HomeBestTime") Is System.DBNull.Value) Then .HomeBestTime = dr("HomeBestTime").ToString Else .HomeBestTime = ""
                If Not (dr("CellBestTime") Is System.DBNull.Value) Then .CellBestTime = dr("CellBestTime").ToString Else .CellBestTime = ""
                If Not (dr("PinNumber") Is System.DBNull.Value) Then .PinNumber = dr("PinNumber") Else .PinNumber = ""
                If Not (dr("Notes") Is System.DBNull.Value) Then .Notes = dr("Notes") Else .Notes = ""
                If Not (dr("AddressTypeId") Is System.DBNull.Value) Then .AddressTypeId = CType(dr("AddressTypeId"), Guid).ToString
                If Not (dr("AddressType") Is System.DBNull.Value) Then .AddressType = dr("AddressType")
                If Not (dr("ForeignHomePhone") Is System.DBNull.Value) Then .ForeignHomePhone = CType(dr("ForeignHomePhone"), String).ToString
                If Not (dr("ForeignCellPhone") Is System.DBNull.Value) Then .ForeignCellPhone = CType(dr("ForeignCellPhone"), String).ToString
                If Not (dr("ForeignZip") Is System.DBNull.Value) Then .ForeignZip = CType(dr("ForeignZip"), String).ToString Else .ForeignZip = ""
                If Not (dr("ForeignWorkPhone") Is System.DBNull.Value) Then .ForeignWorkPhone = CType(dr("ForeignWorkPhone"), String).ToString
                If Not (dr("OtherState") Is System.DBNull.Value) Then .OtherState = CType(dr("OtherState"), String).ToString Else .OtherState = ""
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")

            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return employerinfo
    End Function
    Public Function GetAllTitles() As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   PR.TitleId, PR.TitleDescrip ")
            .Append("FROM     adTitles PR, syStatuses ST ")
            .Append("WHERE    PR.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY PR.TitleDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllEmployerContacts(ByVal showActiveOnly As String, ByVal EmployerId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   BC.LastName,BC.FirstName,BC.MiddleName,BC.EmployerContactId,ST.StatusId,ST.Status ")
            .Append("FROM     plEmployerContact BC, syStatuses ST ")
            .Append("WHERE    BC.StatusId = ST.StatusId and BC.EmployerId = ? ")
            '   Conditionally include only Active Items 
            If showActiveOnly = "True" Then
                .Append("AND    ST.Status = 'Active' ")
                .Append("ORDER BY BC.FirstName ")
            ElseIf showActiveOnly = "False" Then
                .Append("AND    ST.Status = 'Inactive' ")
                .Append("ORDER BY BC.FirstName ")
            Else
                .Append("ORDER BY ST.Status,BC.FirstName asc")
            End If
        End With

        '   return dataset
        db.AddParameter("@EmployerId", EmployerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllEmployerContactsByEmployerId(ByVal showActiveOnly As Boolean, ByVal EmployerId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            '   build the sql query
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT   BC.LastName,BC.FirstName,BC.MiddleName,BC.EmployerContactId ")
                .Append("FROM     plEmployerContact BC, syStatuses ST ")
                .Append("WHERE    BC.StatusId = ST.StatusId and BC.EmployerId= ? ")
                '   Conditionally include only Active Items 
                If showActiveOnly Then
                    .Append(" AND     ST.Status = 'Active' ")
                Else
                    .Append(" AND ST.Status = 'Inactive' ")
                End If
                .Append("ORDER BY BC.FirstName ")
            End With
            db.AddParameter("@EmployerId", EmployerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   return dataset
            db.OpenConnection()
            Return db.RunParamSQLDataSet(sb.ToString)
        Finally
            db.CloseConnection()
        End Try
    End Function
End Class
