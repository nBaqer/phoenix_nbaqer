Imports System.Data
Imports System.Data.OleDb
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.DataAccess.FAME.DataAccessLayer

Public Class TranscriptDB
    ''' <summary>
    ''' Store the connection string for this class
    ''' </summary>
    ''' <remarks></remarks>
    Private ReadOnly conString As String

    ''' <summary>
    ''' Application Setting for get some other different values from config.
    ''' </summary>
    ''' <remarks></remarks>                                                                                                             
    Private ReadOnly myAdvAppSettings As AdvAppSettings

    Sub New()
        myAdvAppSettings = AdvAppSettings.GetAppSettings()
        conString = myAdvAppSettings.AppSettings("ConString")
    End Sub

    Public Function GetTranscriptByEnrollment(ByVal StuEnrollId As String, ByVal PrgVerId As String, Optional ByVal ExcludeNullGrades As Boolean = False) As DataSet
        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        Dim strNoNullGrd1, strNoNullGrd2 As String
        If ExcludeNullGrades Then
            strNoNullGrd1 = "AND A.GrdSysDetailId IS NOT NULL "
            strNoNullGrd2 = "AND D.GrdSysDetailId IS NOT NULL "
        End If

        'build the sql query
        Dim sb As New StringBuilder
        'Query Modified on 03/06/2005 To Get The Grade Based on ClassSection and not on StuEnrollId
        With sb
            .Append(" select Distinct A.GrdSysDetailId,A.TestId,B.ClsSection,C.Code,C.Descrip,B.StartDate, ")
            .Append(" (select Grade from arGradeSystemDetails where GrdSysDetailId = A.GrdSysDetailId) as Grades,A.StuEnrollId,B.TermId,")
            .Append(" (select TermDescrip from arTerm where TermId=B.TermId) as Term ")
            .Append(" from arResults A,arClassSections B,arReqs C ")
            .Append(" where StuEnrollId = ? ")
            .Append(" and A.TestId = B.ClsSectionId and B.ReqId = C.ReqId ")
            .Append(strNoNullGrd1)
            .Append(" Union ")
            .Append(" select Distinct D.GrdSysDetailId,D.TestId,C.ClsSection,B.Code,B.Descrip,C.StartDate, ")
            .Append(" (select Grade from arGradeSystemDetails where GrdSysDetailId = D.GrdSysDetailId) as Grades, ")
            .Append(" D.StuEnrollId,C.TermId, ")
            .Append(" (select TermDescrip from arTerm where TermId = C.TermId) as Term ")
            .Append(" from arProgVerDef A,arReqs B,arClassSections C,arResults D,arStuEnrollments E ")
            .Append(" where A.ReqId = B.ReqId And B.ReqId = C.ReqId And C.ClsSectionId = D.TestId and D.StuEnrollId = E.StuEnrollId ")
            .Append("  and A.PrgVerId = ? and E.StuEnrollId = ? ")
            .Append(strNoNullGrd2)
            '.Append(" and D.StudentId = (select Distinct StudentId from arStuEnrollments where StuEnrollId = ?)")
            .Append(" order by StartDate desc ")
        End With


        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetTranscriptGradeByEnrollment(ByVal StuEnrollId As String, ByVal PrgVerId As String, ByVal StudentId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = conString 'MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder

        With sb
            .Append(" select Distinct ")
            .Append(" A.TestId, ")
            .Append(" B.ClsSection, ")
            .Append(" C.Code, ")
            .Append(" C.Descrip, ")
            .Append(" B.StartDate,  ")
            .Append(" A.StuEnrollId, ")
            .Append(" B.TermId, ")
            .Append(" (select TermDescrip from arTerm where TermId=B.TermId) as Term, ")
            .Append(" tblDerived.Grade ")
            .Append(" from ")
            .Append(" arResults A, ")
            .Append(" arClassSections B, ")
            .Append(" arReqs C, ")
            .Append("	(select ")
            .Append("		t1.GrdSysDetailId, ")
            .Append("		t1.TestId, 	   ")
            .Append("		t2.Grade	   ")
            .Append("		from    	   ")
            .Append("		arResults t1,	   ")
            .Append("		arGradeSystemDetails t2 ")
            .Append("		where 		   ")
            .Append("		t1.GrdSysDetailId = t2.GrdSysDetailId and ")
            .Append("       TestId in (select Distinct TestId from arResults where StuEnrollId=?) and ")
            .Append("		StuEnrollId in ")
            .Append("			(select StuEnrollId from arStuEnrollments ")
            .Append("			where StudentId=? ) ")
            .Append("	)  ")
            .Append("	tblDerived  ")
            .Append(" where ")
            .Append(" StuEnrollId = ? and ")
            .Append(" A.TestId = B.ClsSectionId and ")
            .Append(" B.ReqId = C.ReqId  and ")
            .Append(" A.TestId = tblDerived.TestId ")
            .Append(" union ")

            .Append(" select Distinct ")
            .Append("	D.TestId, ")
            .Append("	C.ClsSection, ")
            .Append("	B.Code, ")
            .Append("	B.Descrip, ")
            .Append("	C.StartDate, ")
            .Append("	D.StuEnrollId, ")
            .Append("	C.TermId, ")
            .Append("	(select TermDescrip from arTerm where TermId = C.TermId) as Term, ")
            .Append("	tblDerived.Grade ")
            .Append(" from  ")
            .Append("	arProgVerDef A, ")
            .Append("	arReqs B, ")
            .Append("	arClassSections C, ")
            .Append("	arResults D, ")
            .Append("	arStuEnrollments E, ")
            .Append("	(select ")
            .Append("		t1.GrdSysDetailId, ")
            .Append("		t1.TestId, ")
            .Append("		t2.Grade ")
            .Append("		from ")
            .Append("		arResults t1, ")
            .Append("		arGradeSystemDetails t2 ")
            .Append("		where ")
            .Append("		t1.GrdSysDetailId = t2.GrdSysDetailId and ")
            .Append("       TestId in (select Distinct TestId from arResults where StuEnrollId=?) and ")
            .Append("		StuEnrollId in ")
            .Append("			(select StuEnrollId from arStuEnrollments ")
            .Append("			where StudentId=? ) ")
            .Append("		) ")
            .Append("	tblDerived ")
            .Append(" where  ")
            .Append("	A.ReqId = B.ReqId And ")
            .Append("	B.ReqId = C.ReqId And ")
            .Append("	C.ClsSectionId = D.TestId and ")
            .Append("	D.StuEnrollId = E.StuEnrollId and ")
            .Append("	D.TestId = tblDerived.TestId ")
            .Append("	and A.PrgVerId = ? ")
            .Append("	and E.StuEnrollId = ? ")
        End With


        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetGraduateAuditByEnrollment(ByVal StuEnrollId As String, ByVal PrgVerId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess
        Dim intCommaOccurance As Integer

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder

        With sb
            .Append(" select Distinct A.GrdSysDetailId,A.TestId,B.ClsSection,C.Code,C.Descrip,B.StartDate, ")
            .Append(" (select Grade from arGradeSystemDetails where GrdSysDetailId = A.GrdSysDetailId) as Grades,A.StuEnrollId,B.TermId,")
            .Append(" (select TermDescrip from arTerm where TermId=B.TermId) as Term ")
            .Append(" from arResults A,arClassSections B,arReqs C ")
            .Append(" where StuEnrollId = ? ")
            .Append(" and A.TestId = B.ClsSectionId and B.ReqId = C.ReqId and C.IsAttendanceOnly = 0 ")
            .Append(" Union ")
            .Append(" select Distinct D.GrdSysDetailId,D.TestId,C.ClsSection,B.Code,B.Descrip,C.StartDate, ")
            .Append(" (select Grade from arGradeSystemDetails where GrdSysDetailId = D.GrdSysDetailId) as Grades, ")
            .Append(" D.StuEnrollId,C.TermId, ")
            .Append(" (select TermDescrip from arTerm where TermId = C.TermId) as Term ")
            .Append(" from arProgVerDef A,arReqs B,arClassSections C,arResults D,arStuEnrollments E ")
            .Append(" where A.ReqId = B.ReqId And B.ReqId = C.ReqId And C.ClsSectionId = D.TestId and D.StuEnrollId = E.StuEnrollId and B.IsAttendanceOnly = 0 ")
            .Append(" and A.PrgVerId = ? and E.StuEnrollId =? ")
            '.Append(" and D.StudentId = (select Distinct StudentId from arStuEnrollments where StuEnrollId = ?)")
            .Append(" order by StartDate desc ")
        End With
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'return dataset
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim strCourses As String
        While dr.Read()
            strCourses &= dr("Descrip") & "','"
        End While
        If Not dr.IsClosed Then dr.Close()
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        intCommaOccurance = InStrRev(strCourses, ",", -1)
        If intCommaOccurance >= 1 Then
            strCourses = Mid(strCourses, 1, intCommaOccurance - 2)
        Else
            strCourses = "None"
        End If
        With sb
            .Append(" select Distinct B.Code as Code,B.Descrip as Descrip, ")
            .Append(" (select Grade from arGradeSystemDetails where GrdSysDetailId = D.GrdSysDetailId) as Grades, ")
            .Append(" (select Distinct GrdSystemId from arGradeSystemDetails where GrdSysDetailId = A.GrdSysDetailId) as GrdSystemId ")
            .Append(" from arProgVerDef A,arReqs B,arClassSections C,arResults D,arStuEnrollments E ")
            .Append(" where A.ReqId = B.ReqId And B.ReqId = C.ReqId And C.ClsSectionId = D.TestId and D.StuEnrollId = E.StuEnrollId and B.IsAttendanceOnly = 0 ")
            .Append(" and A.PrgVerId = ? and E.StuEnrollId = ? ")
            '.Append(" and D.StudentId = (select Distinct StudentId from arStuEnrollments where StuEnrollId = ?)")
            .Append(" union ")
            .Append(" SELECT  Distinct C.Code as Code,C.Descrip as Descrip, Null as Grades, ")
            .Append(" Null as GrdSystemId ")
            .Append(" FROM arPrgVersions A,arProgVerDef B,arReqs C  ")
            .Append(" where A.PrgVerId = B.PrgVerId And B.ReqId = C.ReqId and C.IsAttendanceOnly = 0 and A.PrgVerId = ? and ")
            .Append(" C.Descrip not in ('")
            .Append(strCourses)
            .Append("')")
            .Append(" order by Grades,Descrip asc")
        End With
        ' db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim ds As DataSet
        ds = db.RunParamSQLDataSet(sb.ToString)

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds

    End Function
    Public Function GetGraduateAuditByEnrollmentCourseGroup(ByVal StuEnrollId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess
        Dim prgVerId As String
        Dim intCommaOccurance As Integer

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        Dim sb2 As New StringBuilder
        With sb2
            .Append("select distinct prgverid from arStuEnrollments where StuEnrollid=?")
        End With
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Dim drPrg As OleDbDataReader = db.RunParamSQLDataReader(sb2.ToString)
        While drPrg.Read()
            prgVerId = CType(drPrg("prgverid"), Guid).ToString
        End While
        If Not drPrg.IsClosed Then drPrg.Close()
        db.ClearParameters()
        sb2.Remove(0, sb2.Length)

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select Distinct A.GrdSysDetailId,A.TestId,B.ClsSection,C.Code,C.Descrip,B.StartDate, ")
            .Append(" (select Grade from arGradeSystemDetails where GrdSysDetailId = A.GrdSysDetailId) as Grades,A.StuEnrollId,B.TermId,")
            .Append(" (select TermDescrip from arTerm where TermId=B.TermId) as Term ")
            .Append(" from arResults A,arClassSections B,arReqs C ")
            .Append(" where StuEnrollId = ? ")
            .Append(" and A.TestId = B.ClsSectionId and B.ReqId = C.ReqId ")
            .Append(" Union ")
            .Append(" select Distinct D.GrdSysDetailId,D.TestId,C.ClsSection,B.Code,B.Descrip,C.StartDate, ")
            .Append(" (select Grade from arGradeSystemDetails where GrdSysDetailId = D.GrdSysDetailId) as Grades, ")
            .Append(" D.StuEnrollId,C.TermId, ")
            .Append(" (select TermDescrip from arTerm where TermId = C.TermId) as Term ")
            .Append(" from arProgVerDef A,arReqs B,arClassSections C,arResults D,arStuEnrollments E ")
            .Append(" where A.ReqId = B.ReqId And B.ReqId = C.ReqId And C.ClsSectionId = D.TestId and D.StuEnrollId = E.StuEnrollId ")
            .Append(" and A.PrgVerId = ? and E.StuEnrollId =? ")
            .Append(" order by StartDate desc ")
        End With
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'return dataset
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim strCourses As String
        While dr.Read()
            strCourses &= dr("Descrip") & "','"
        End While
        If Not dr.IsClosed Then dr.Close()
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        intCommaOccurance = InStrRev(strCourses, ",", -1)
        If intCommaOccurance >= 1 Then
            strCourses = Mid(strCourses, 1, intCommaOccurance - 2)
        Else
            strCourses = "None"
        End If
        With sb

            .Append("  select Distinct B.Code as Code,B.Descrip as Descrip,B.Credits,(select Grade ")
            .Append(" from arGradeSystemDetails where GrdSysDetailId = D.GrdSysDetailId) as Grades, ")
            .Append(" (select Distinct GrdSystemId from arGradeSystemDetails where GrdSysDetailId = A.GrdSysDetailId) as GrdSystemId, ")
            .Append(" B.ReqId,  C.ClsSectionId,  (Select Case IsPass when 1 then 'Yes' else 'No' End from arGradeSystemDetails where GrdSysDetailId = ")
            .Append(" D.GrdSysDetailId) as IsPass,  ")
            .Append(" (select Distinct Credits from arReqs t1,arResults t2,arClassSections t3,arGradeSystemDetails t4  ")
            .Append(" where t1.ReqId = t3.ReqId And t2.TestId = t3.ClsSectionId And t2.GrdSysDetailId = t4.GrdSysDetailId  and t4.IsCreditsAttempted = 1 and ")
            .Append(" t1.ReqId=A.ReqId) as CreditsAttempted,  ")
            .Append(" (select Distinct Credits from arReqs t1,arResults t2,arClassSections t3,arGradeSystemDetails t4  where t1.ReqId = t3.ReqId And t2.TestId = t3.ClsSectionId And t2.GrdSysDetailId = t4.GrdSysDetailId  ")
            .Append(" and t4.IsCreditsEarned = 1 and t1.ReqId=A.ReqId) as CreditsEarned  from arProgVerDef A,arReqs B,arClassSections C,arResults D,arStuEnrollments E  ")
            .Append(" where A.ReqId = B.ReqId And B.ReqId = C.ReqId And C.ClsSectionId = D.TestId and D.StuEnrollId=E.StuEnrollId  and A.PrgVerId = ? ")
            .Append(" and E.StuEnrollId =? ")
            .Append("  union ")
            .Append(" SELECT  Distinct C.Code as Code,C.Descrip as Descrip, C.Credits,Null as Grades,  Null as GrdSystemId ")
            .Append(" ,C.ReqId,Null as ClsSectionId,Null as IsPass,Null as CreditsAttempted,Null as CreditsEarned  FROM arPrgVersions A,arProgVerDef B,arReqs C   where A.PrgVerId = B.PrgVerId And B.ReqId = C.ReqId and A.PrgVerId = ? ")
            .Append(" and  C.Descrip not in ('")
            .Append(strCourses)
            .Append("')")
            .Append("  order by Grades,Descrip asc ")

        End With
        '   build select command
        Dim ds As New DataSet
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(conString)) '(MyAdvAppSettings.AppSettings("ConString")))
        sc.Parameters.Add(New OleDbParameter("@PrgVerId", prgVerId))
        sc.Parameters.Add(New OleDbParameter("@StuEnrollId", StuEnrollId))
        sc.Parameters.Add(New OleDbParameter("@PrgVerId", prgVerId))

        Dim da As New OleDbDataAdapter(sc)
        da.Fill(ds, "arReqs")
        sb.Remove(0, sb.Length)

        With sb
            .Append("  Select Distinct A.GrpId,A.ReqId,(select Descrip from arReqs ")
            .Append("  where ReqId=A.ReqId) as Course,")
            .Append("  (Select Credits from arReqs where ReqId=A.ReqId) as Credits,")
            .Append("   (select Distinct Grade from arReqs t1,arResults t2,arClassSections t3,arGradeSystemDetails t4 ")
            .Append("   where t1.ReqId=t3.ReqId and t2.TestId = t3.ClsSectionId and  t2.GrdSysDetailId = t4.GrdSysDetailId  and t2.StuEnrollId = ?")
            .Append("   and t1.ReqId=A.ReqId) as Grades, ")
            .Append(" (select Distinct Case IsPass when 1 then 'Yes' else 'No' End from arReqs t1,arResults t2,arClassSections t3,arGradeSystemDetails t4 ")
            .Append(" where t1.ReqId=t3.ReqId and t3.ReqId=A.ReqId and t2.TestId = t3.ClsSectionId and  t2.GrdSysDetailId = t4.GrdSysDetailId  and t2.StuEnrollId =?) as Pass,  ")
            .Append(" (select Distinct Credits from arReqs t1,arResults t2,arClassSections t3,arGradeSystemDetails t4 ")
            .Append(" where t1.ReqId = t3.ReqId And t3.ReqId = A.ReqId And t2.TestId = t3.ClsSectionId And t2.GrdSysDetailId = t4.GrdSysDetailId ")
            .Append(" and t4.IsCreditsAttempted = 1 and t2.StuEnrollId=?) as CreditsAttempted, ")
            .Append(" (select Distinct Credits from arReqs t1,arResults t2,arClassSections t3,arGradeSystemDetails t4 ")
            .Append(" where t1.ReqId = t3.ReqId And t3.ReqId = A.ReqId And t2.TestId = t3.ClsSectionId And t2.GrdSysDetailId = t4.GrdSysDetailId ")
            .Append(" and t4.IsCreditsEarned = 1 and t2.StuEnrollId=?) as CreditsEarned ")
            .Append("  from arReqGrpDef A ")

        End With

        Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(conString)) ' (MyAdvAppSettings.AppSettings("ConString")))
        sc1.Parameters.Add(New OleDbParameter("@StuEnrollId", StuEnrollId))
        sc1.Parameters.Add(New OleDbParameter("@StuEnrollId", StuEnrollId))
        sc1.Parameters.Add(New OleDbParameter("@StuEnrollId", StuEnrollId))
        sc1.Parameters.Add(New OleDbParameter("@StuEnrollId", StuEnrollId))


        Dim da1 As New OleDbDataAdapter(sc1)
        da1.Fill(ds, "Orders")
        sb.Remove(0, sb.Length)
        Try
            ds.Relations.Add("Orders", ds.Tables("arReqs").Columns("ReqId"), ds.Tables("Orders").Columns("GrpId"))
        Catch ex As Exception
        End Try


        Dim cmdSelectAuthors As OleDbCommand
        Dim dtrAuthors As OleDbDataReader
        Dim intGrpCount As Integer
        With sb
            .Append("  Select Distinct A.GrpId,A.ReqId,(select Descrip from arReqs ")
            .Append("  where ReqId=A.ReqId) as Course,")
            .Append("  (Select Credits from arReqs where ReqId=A.ReqId) as Credits,")
            .Append("   (select Distinct Grade from arReqs t1,arResults t2,arClassSections t3,arGradeSystemDetails t4 ")
            .Append("   where t1.ReqId=t3.ReqId and t2.TestId = t3.ClsSectionId and  t2.GrdSysDetailId = t4.GrdSysDetailId  and t2.StuEnrollId ='" & StuEnrollId & "'")
            .Append(" and t1.ReqId=A.ReqId) as Grades, ")
            .Append(" (select Distinct Case IsPass when 1 then 'Yes' else 'No' End from arReqs t1,arResults t2,arClassSections t3,arGradeSystemDetails t4 ")
            .Append(" where t1.ReqId=t3.ReqId and t3.ReqId=A.ReqId and t2.TestId = t3.ClsSectionId and  t2.GrdSysDetailId = t4.GrdSysDetailId  and t2.StuEnrollId ='" & StuEnrollId & "'")
            '.Append(StuEnrollId)
            .Append(" ) as Pass,  ")
            .Append(" (select Distinct Credits from arReqs t1,arResults t2,arClassSections t3,arGradeSystemDetails t4 ")
            .Append(" where t1.ReqId = t3.ReqId And t3.ReqId = A.ReqId And t2.TestId = t3.ClsSectionId And t2.GrdSysDetailId = t4.GrdSysDetailId ")
            .Append(" and t4.IsCreditsAttempted = 1 and t2.StuEnrollId ='" & StuEnrollId & "'")
            .Append(" ) as CreditsAttempted, ")
            .Append(" (select Distinct Credits from arReqs t1,arResults t2,arClassSections t3,arGradeSystemDetails t4 ")
            .Append(" where t1.ReqId = t3.ReqId And t3.ReqId = A.ReqId And t2.TestId = t3.ClsSectionId And t2.GrdSysDetailId = t4.GrdSysDetailId ")
            .Append(" and t4.IsCreditsEarned = 1 and t2.StuEnrollId ='" & StuEnrollId & "'")
            .Append(" ) as CreditsEarned ")
            .Append(" from arReqGrpDef A ")
        End With

        Dim conPubs As OleDbConnection
        conPubs = New OleDbConnection(conString) '(MyAdvAppSettings.AppSettings("ConString"))
        conPubs.Open()

        cmdSelectAuthors = New OleDbCommand("Select Distinct GrpId From arReqGrpDef", conPubs)
        dtrAuthors = cmdSelectAuthors.ExecuteReader()
        While dtrAuthors.Read()
            intGrpCount += 1
        End While
        If Not dtrAuthors.IsClosed Then dtrAuthors.Close()


        Dim strChild As Integer
        Dim strOrders As Object
        Dim strFillData As String
        Dim strOldData As String
        Dim i As Integer = 1
        For i = 1 To intGrpCount
            strChild = i
            'strOrders = "strOrders" & strChild.ToString
            strFillData = "strFillData" & strChild.ToString


            strOrders = New OleDbDataAdapter(sb.ToString, New OleDbConnection(conString)) '(MyAdvAppSettings.AppSettings("ConString")))
            strOrders.Fill(ds, strFillData)
            If strChild = 1 Then
                Try
                    ds.Relations.Add(strFillData, ds.Tables("Orders").Columns("ReqID"), ds.Tables(strFillData).Columns("GrpID"))
                Catch ex As Exception
                End Try
            Else
                Try
                    ds.Relations.Add(strFillData, ds.Tables(strOldData).Columns("ReqID"), ds.Tables(strFillData).Columns("GrpID"))
                Catch ex As Exception
                End Try
            End If
            strOldData = strFillData

        Next

        'return dataset
        Return ds
    End Function
    Public Function GetGraduateAuditGradeByEnrollmentCourseGroup(ByVal StuEnrollId As String, ByVal StudentId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess
        Dim prgVerId As String
        Dim intCommaOccurance As Integer

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        Dim sb2 As New StringBuilder
        With sb2
            .Append("select distinct prgverid from arStuEnrollments where StuEnrollid=?")
        End With
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Dim drPrg As OleDbDataReader = db.RunParamSQLDataReader(sb2.ToString)
        While drPrg.Read()
            prgVerId = CType(drPrg("prgverid"), Guid).ToString
        End While
        If Not drPrg.IsClosed Then drPrg.Close()
        db.ClearParameters()
        sb2.Remove(0, sb2.Length)

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select Distinct A.GrdSysDetailId,A.TestId,B.ClsSection,C.Code,C.Descrip,B.StartDate, ")
            .Append(" (select Grade from arGradeSystemDetails where GrdSysDetailId = A.GrdSysDetailId) as Grades,A.StuEnrollId,B.TermId,")
            .Append(" (select TermDescrip from arTerm where TermId=B.TermId) as Term ")
            .Append(" from arResults A,arClassSections B,arReqs C, ")
            .Append(" (select t1.GrdSysDetailId,t1.TestId,t2.Grade  from ")
            .Append(" arResults t1,arGradeSystemDetails t2   where  ")
            .Append(" t1.GrdSysDetailId = t2.GrdSysDetailId and  TestId in (select Distinct TestId from arResults where StuEnrollId=?) ")
            .Append(" and  StuEnrollId in (select StuEnrollId from arStuEnrollments where StudentId=?)) tblDerived ")
            .Append(" where StuEnrollId = ? ")
            .Append(" and A.TestId = B.ClsSectionId and B.ReqId = C.ReqId ")
            .Append(" and tblDerived.TestId = A.TestId ")
            .Append(" Union ")
            .Append(" select Distinct D.GrdSysDetailId,D.TestId,C.ClsSection,B.Code,B.Descrip,C.StartDate, ")
            .Append(" (select Grade from arGradeSystemDetails where GrdSysDetailId = D.GrdSysDetailId) as Grades, ")
            .Append(" D.StuEnrollId,C.TermId, ")
            .Append(" (select TermDescrip from arTerm where TermId = C.TermId) as Term ")
            .Append(" from arProgVerDef A,arReqs B,arClassSections C,arResults D,arStuEnrollments E, ")
            .Append(" (select t1.GrdSysDetailId,t1.TestId,t2.Grade  from ")
            .Append(" arResults t1,arGradeSystemDetails t2   where  ")
            .Append(" t1.GrdSysDetailId = t2.GrdSysDetailId and  TestId in (select Distinct TestId from arResults where StuEnrollId=?) ")
            .Append(" and  StuEnrollId in (select StuEnrollId from arStuEnrollments where StudentId=?)) tblDerived ")
            .Append(" where A.ReqId = B.ReqId And B.ReqId = C.ReqId And C.ClsSectionId = D.TestId and D.StuEnrollId = E.StuEnrollId and D.TestId=tblDerived.TestId ")
            .Append(" and A.PrgVerId = ? and E.StuEnrollId =? ")
            .Append(" order by StartDate desc ")
        End With
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'return dataset
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim strCourses As String
        While dr.Read()
            strCourses &= dr("Descrip") & "','"
        End While
        If Not dr.IsClosed Then dr.Close()
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        intCommaOccurance = InStrRev(strCourses, ",", -1)
        If intCommaOccurance >= 1 Then
            strCourses = Mid(strCourses, 1, intCommaOccurance - 2)
        Else
            strCourses = "None"
        End If
        With sb

            .Append("  select Distinct B.Code as Code,B.Descrip as Descrip,B.Credits, ")
            .Append(" tblDerived.Grade as Grades,tblDerived.GrdSystemId, ")
            .Append(" B.ReqId,  C.ClsSectionId,  (Select Case IsPass when 1 then 'Yes' else 'No' End from arGradeSystemDetails where GrdSysDetailId = ")
            .Append(" D.GrdSysDetailId) as IsPass,  ")
            .Append(" (select Distinct Credits from arReqs t1,arResults t2,arClassSections t3  where ")
            .Append(" t1.ReqId = t3.ReqId And t2.TestId = t3.ClsSectionId And t2.GrdSysDetailId = tblDerived.GrdSysDetailId  and tblDerived.IsCreditsAttempted = 1 and  ")
            .Append(" t1.ReqId=A.ReqId) as Attempted,  ")
            .Append(" (select Distinct Credits from arReqs t1,arResults t2,arClassSections t3  where ")
            .Append(" t1.ReqId = t3.ReqId And t2.TestId = t3.ClsSectionId And t2.GrdSysDetailId = tblDerived.GrdSysDetailId  and tblDerived.IsCreditsEarned = 1 and  ")
            .Append(" t1.ReqId=A.ReqId) as Earned  ")
            .Append("  from arProgVerDef A,arReqs B,arClassSections C,arResults D,arStuEnrollments E,  ")
            .Append(" (select t1.GrdSysDetailId,t1.TestId,t2.Grade,t2.GrdSystemId,t2.IsCreditsAttempted,t2.IsCreditsEarned  from ")
            .Append(" arResults t1,arGradeSystemDetails t2   where  ")
            .Append(" t1.GrdSysDetailId = t2.GrdSysDetailId and  TestId in (select Distinct TestId from arResults where StuEnrollId=?) ")
            .Append(" and  StuEnrollId in (select StuEnrollId from arStuEnrollments where StudentId=?)) tblDerived ")
            .Append(" where A.ReqId = B.ReqId And B.ReqId = C.ReqId And C.ClsSectionId = D.TestId and D.StuEnrollId=E.StuEnrollId and tblDerived.TestId = D.TestId and A.PrgVerId = ? ")
            .Append(" and E.StuEnrollId =? ")
            .Append("  union ")
            .Append(" SELECT  Distinct C.Code as Code,C.Descrip as Descrip, C.Credits,Null as Grades,  Null as GrdSystemId ")
            .Append(" ,C.ReqId,Null as ClsSectionId,Null as IsPass,Null as CreditsAttempted,Null as CreditsEarned  FROM arPrgVersions A,arProgVerDef B,arReqs C   where A.PrgVerId = B.PrgVerId And B.ReqId = C.ReqId and A.PrgVerId = ? ")
            .Append(" and  C.Descrip not in ('")
            .Append(strCourses)
            .Append("')")
            .Append("  order by Grades,Descrip asc ")

        End With
        '   build select command
        Dim ds As New DataSet
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(conString)) '(MyAdvAppSettings.AppSettings("ConString")))
        sc.Parameters.Add(New OleDbParameter("@StuEnrollId", StuEnrollId))
        sc.Parameters.Add(New OleDbParameter("@StudentId", StudentId))
        sc.Parameters.Add(New OleDbParameter("@PrgVerId", prgVerId))
        sc.Parameters.Add(New OleDbParameter("@StuEnrollId", StuEnrollId))
        sc.Parameters.Add(New OleDbParameter("@PrgVerId", prgVerId))

        Dim da As New OleDbDataAdapter(sc)
        da.Fill(ds, "arReqs")
        sb.Remove(0, sb.Length)

        With sb
            .Append(" Select Distinct  ")
            .Append("	A.GrpId, ")
            .Append("	A.ReqId, ")
            .Append("	(select Descrip from arReqs where ReqId=A.ReqId) as Course, ")
            .Append("	(Select Credits from arReqs where ReqId=A.ReqId) as Credits, ")
            .Append(" (select Distinct Grade ")
            .Append("		from  ")
            .Append("		arReqs t1,arResults t2,arClassSections t3,arGradeSystemDetails t4, ")
            .Append("		(select Distinct GrdSysDetailId,StuEnrollId,TestId from arResults where StuEnrollId in  ")
            .Append("		(select StuEnrollId from arStuEnrollments where StudentId=?) ")
            .Append("		 and GrdSysDetailId is not null ")
            .Append("		) t5 ")
            .Append("		where t1.ReqId = t3.ReqId and t5.TestId = t2.TestId ")
            .Append("		And  ")
            .Append("		t3.ReqId = A.ReqId ")
            .Append("		And t2.TestId = t3.ClsSectionId And  ")
            .Append("		t5.GrdSysDetailId = t4.GrdSysDetailId ")
            .Append("		)  ")
            .Append("	as Grades, ")

            .Append(" (select Distinct  Case IsPass when 1 then 'Yes' else 'No' End  from ")
            .Append("	arReqs t1,arResults t2,arClassSections t3,arGradeSystemDetails t4, ")
            .Append("	(select Distinct GrdSysDetailId,TestId from arResults where StuEnrollId in ")
            .Append("	(select StuEnrollId from arStuEnrollments where StudentId=?) ")
            .Append("	 and GrdSysDetailId is not null ")
            .Append("	) t5 ")
            .Append("	where t1.ReqId = t3.ReqId And t5.TestId = t2.TestId and")
            .Append("	t3.ReqId = A.ReqId And t2.TestId = t3.ClsSectionId And ")
            .Append("	t5.GrdSysDetailId = t4.GrdSysDetailId and ")
            .Append("	t2.StuEnrollId =?) as Pass, ")

            .Append(" (select Distinct Credits  from ")
            .Append("  arReqs t1,arResults t2,arClassSections t3,arGradeSystemDetails t4, ")
            .Append(" (select Distinct GrdSysDetailId,TestId from arResults where StuEnrollId in ")
            .Append(" (select StuEnrollId from arStuEnrollments where StudentId=?) ")
            .Append("  and GrdSysDetailId is not null ")
            .Append("  ) t5 ")
            .Append("  where   t1.ReqId = t3.ReqId and t5.TestId = t2.TestId and t3.ReqId = A.ReqId And t2.TestId = t3.ClsSectionId And t5.GrdSysDetailId = t4.GrdSysDetailId ")
            .Append("  and t4.IsCreditsEarned = 1 and ")
            .Append("  t2.StuEnrollId =? ) as Earned, ")

            .Append(" (select Distinct Credits  from ")
            .Append("  arReqs t1,arResults t2,arClassSections t3,arGradeSystemDetails t4, ")
            .Append(" (select Distinct GrdSysDetailId,TestId from arResults where StuEnrollId in ")
            .Append(" (select StuEnrollId from arStuEnrollments where StudentId=?) ")
            .Append("  and GrdSysDetailId is not null ")
            .Append("  ) t5 ")
            .Append("  where   t1.ReqId = t3.ReqId And t5.TestId=t2.TestId and t3.ReqId = A.ReqId And t2.TestId = t3.ClsSectionId And t5.GrdSysDetailId = t4.GrdSysDetailId ")
            .Append("  and t4.IsCreditsAttempted = 1 and ")
            .Append("  t2.StuEnrollId =? ) as Attempted ")
            .Append(" from arReqGrpDef A  ")
        End With

        Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(conString)) 'MyAdvAppSettings.AppSettings("ConString")))
        sc1.Parameters.Add(New OleDbParameter("@StudentId", StudentId))

        sc1.Parameters.Add(New OleDbParameter("@StudentId", StudentId))
        sc1.Parameters.Add(New OleDbParameter("@StuEnrollId", StuEnrollId))

        sc1.Parameters.Add(New OleDbParameter("@StudentId", StudentId))
        sc1.Parameters.Add(New OleDbParameter("@StuEnrollId", StuEnrollId))

        sc1.Parameters.Add(New OleDbParameter("@StudentId", StudentId))
        sc1.Parameters.Add(New OleDbParameter("@StuEnrollId", StuEnrollId))


        Dim da1 As New OleDbDataAdapter(sc1)
        da1.Fill(ds, "Orders")
        sb.Remove(0, sb.Length)
        Try
            ds.Relations.Add("Orders", ds.Tables("arReqs").Columns("ReqId"), ds.Tables("Orders").Columns("GrpId"))
        Catch ex As Exception
        End Try


        Dim cmdSelectAuthors As OleDbCommand
        Dim dtrAuthors As OleDbDataReader
        Dim intGrpCount As Integer
        With sb
            .Append(" Select Distinct  ")
            .Append("	A.GrpId, ")
            .Append("	A.ReqId, ")
            .Append("	(select Descrip from arReqs where ReqId=A.ReqId) as Course, ")
            .Append("	(Select Credits from arReqs where ReqId=A.ReqId) as Credits, ")
            .Append(" (select Distinct Grade ")
            .Append("		from  ")
            .Append("		arReqs t1,arResults t2,arClassSections t3,arGradeSystemDetails t4, ")
            .Append("		(select Distinct GrdSysDetailId,StuEnrollId,TestId from arResults where StuEnrollId in  ")
            .Append("		(select StuEnrollId from arStuEnrollments where StudentId='" & StudentId & "'" & ") and GrdSysDetailId is not null ")
            .Append("		) t5 ")
            .Append("		where t1.ReqId = t3.ReqId and t2.TestId=t5.TestId  ")
            .Append("		And  ")
            .Append("		t3.ReqId = A.ReqId ")
            .Append("		And t2.TestId = t3.ClsSectionId And  ")
            .Append("		t5.GrdSysDetailId = t4.GrdSysDetailId ")
            .Append("		)  ")
            .Append("	as Grades, ")

            .Append(" (select Distinct  Case IsPass when 1 then 'Yes' else 'No' End  from ")
            .Append("	arReqs t1,arResults t2,arClassSections t3,arGradeSystemDetails t4, ")
            .Append("	(select Distinct GrdSysDetailId,TestId from arResults where StuEnrollId in ")
            .Append("	(select StuEnrollId from arStuEnrollments where StudentId='" & StudentId & "'" & ") and GrdSysDetailId is not null ")
            .Append("	) t5 ")
            .Append("	where t1.ReqId = t3.ReqId And t2.TestId=t5.TestId and ")
            .Append("	t3.ReqId = A.ReqId And t2.TestId = t3.ClsSectionId And ")
            .Append("	t5.GrdSysDetailId = t4.GrdSysDetailId and ")
            .Append("	t2.StuEnrollId ='" & StuEnrollId & "'" & ") as Pass, ")
            .Append(" (select Distinct Credits  from ")
            .Append("  arReqs t1,arResults t2,arClassSections t3,arGradeSystemDetails t4, ")
            .Append(" (select Distinct GrdSysDetailId,TestId from arResults where StuEnrollId in ")
            .Append(" (select StuEnrollId from arStuEnrollments where StudentId='" & StudentId & "'" & ") and GrdSysDetailId is not null ")
            .Append("  ) t5 ")
            .Append("  where   t1.ReqId = t3.ReqId and t2.TestId=t5.TestId And t3.ReqId = A.ReqId And t2.TestId = t3.ClsSectionId And t5.GrdSysDetailId = t4.GrdSysDetailId ")
            .Append("  and t4.IsCreditsEarned = 1 and ")
            .Append("  t2.StuEnrollId ='" & StuEnrollId & "'" & ") as Earned, ")

            .Append(" (select Distinct Credits  from ")
            .Append("  arReqs t1,arResults t2,arClassSections t3,arGradeSystemDetails t4, ")
            .Append(" (select Distinct GrdSysDetailId,TestId from arResults where StuEnrollId in ")
            .Append(" (select StuEnrollId from arStuEnrollments where StudentId='" & StudentId & "'" & ") and GrdSysDetailId is not null ")
            .Append("  ) t5 ")
            .Append("  where   t1.ReqId = t3.ReqId and  t2.TestId = t5.TestId And t3.ReqId = A.ReqId And t2.TestId = t3.ClsSectionId And t5.GrdSysDetailId = t4.GrdSysDetailId ")
            .Append("  and t4.IsCreditsAttempted = 1 and ")
            .Append("  t2.StuEnrollId ='" & StuEnrollId & "'" & ") as Attempted ")
            .Append(" from arReqGrpDef A  ")
        End With

        Dim conPubs As OleDbConnection
        conPubs = New OleDbConnection(conString) 'MyAdvAppSettings.AppSettings("ConString"))
        conPubs.Open()

        cmdSelectAuthors = New OleDbCommand("Select Distinct GrpId From arReqGrpDef", conPubs)
        dtrAuthors = cmdSelectAuthors.ExecuteReader()
        While dtrAuthors.Read()
            intGrpCount += 1
        End While

        If Not dr.IsClosed Then dr.Close()


        Dim strChild As Integer
        Dim strOrders As Object
        Dim strFillData As String
        Dim strOldData As String
        Dim i As Integer = 1
        For i = 1 To intGrpCount
            strChild = i
            strOrders = "strOrders" & strChild.ToString
            strFillData = "strFillData" & strChild.ToString


            strOrders = New OleDbDataAdapter(sb.ToString, New OleDbConnection(conString)) '(MyAdvAppSettings.AppSettings("ConString")))
            strOrders.Fill(ds, strFillData)
            If strChild = 1 Then
                Try
                    ds.Relations.Add(strFillData, ds.Tables("Orders").Columns("ReqID"), ds.Tables(strFillData).Columns("GrpID"))
                Catch ex As Exception
                End Try
            Else
                Try
                    ds.Relations.Add(strFillData, ds.Tables(strOldData).Columns("ReqID"), ds.Tables(strFillData).Columns("GrpID"))
                Catch ex As Exception
                End Try
            End If
            strOldData = strFillData

        Next

        'return dataset
        Return ds
    End Function
    Public Function GetDefaultGradeByGradeSystem(ByVal GrdSystemId As String) As String
        Dim db As New DataAccess

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select Grade from arGradeSystemDetails where GrdSystemId = ? and IsDefault=1 ")
        End With

        'Add Parameter
        db.AddParameter("@GrdSystemId", GrdSystemId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim strDefaultGrade As String = String.Empty
        While dr.Read()
            strDefaultGrade = dr("Grade")
        End While
        If Not dr.IsClosed Then dr.Close()

        Return strDefaultGrade
    End Function
    Public Function GetCoursesByEnrollment(ByVal PrgVerId As String, ByVal DescCollection As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" SELECT  A.PrgVerDescrip, A.PrgVerId, B.ReqId, C.Descrip, C.Code ")
            .Append(" FROM    arPrgVersions A,arProgVerDef B,arReqs C  ")
            .Append(" where   ")
            .Append(" A.PrgVerId = B.PrgVerId And B.ReqId = C.ReqId and A.PrgVerId = ? and ")
            .Append(" C.Descrip not in ('")
            .Append(DescCollection)
            .Append("')")
            .Append(" order by C.Descrip ")
        End With

        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetEnrollment(ByVal StudentId As String, Optional ByVal campusId As String = Nothing) As DataSet
        Dim db As New DataAccess

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select A.PrgVerId,StuEnrollId,P.PrgVerDescrip + ' (' + S.StatusCodeDescrip + ')' AS PrgVerDescrip, p.ProgramRegistrationType ")
            .Append(" from arStuEnrollments A, syStatusCodes S, arPrgVersions P ")
            .Append(" where A.StatusCodeId=S.StatusCodeId ")
            .Append(" and A.PrgVerId = P.PrgVerId ")
            .Append(" and StudentId = ? ")
            If Not campusId Is Nothing Then
                .Append(" and CampusId = ? ")
            End If
            'Modified by Michelle R. Rodriguez on 03/17/2006
            '.Append(" order by A.EnrollDate DESC ")
            .Append(" order by A.ExpStartDate DESC,PrgVerDescrip ")
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If Not campusId Is Nothing Then
            db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function IsEnrollmentInSchool(ByVal stuEnrollId As String) As Boolean
        Dim db As New DataAccess


        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" SELECT  ss.InSchool ")
            .Append(" FROM    dbo.arStuEnrollments e ")
            .Append(" INNER JOIN dbo.syStatusCodes sc ON e.StatusCodeId = sc.StatusCodeId ")
            .Append(" INNER JOIN dbo.sySysStatus ss ON sc.SysStatusId = ss.SysStatusId ")
            .Append(" WHERE   StuEnrollId = ? ")
        End With
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim bIsInSchool As Boolean = True

        Try
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                bIsInSchool = dr("InSchool")
            End While

            Return bIsInSchool

        Catch ex As Exception
            Return False

        End Try

    End Function
    Public Function GetSummary(ByVal StuEnrollId As String, ByVal PrgVerId As String, Optional ByVal ExcludeNullGrades As Boolean = False) As TranscriptInfo
        '   connect to the database
        Dim db As New DataAccess

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder

        'Modified on 05/18/2005 by Michelle R. Rodriguez
        Dim strExcludeNullGrd1, strExcludeNullGrd2 As String
        If ExcludeNullGrades Then
            strExcludeNullGrd1 = "AND C.GrdSysDetailId IS NOT NULL "
        End If

        With sb

            .Append(" select Sum(B.Credits) as CreditsEarned ")
            .Append(" from arClassSections A ,arReqs B,arResults C,arGradeSystemDetails D,arStuEnrollments E where  ")
            .Append(" A.ReqId = B.ReqId and A.ClsSectionId = C.TestId and C.GrdSysDetailId = D.GrdSysDetailId and ")
            .Append(" B.ReqId in ")
            .Append(" (select ReqId from arProgVerDef where PrgVerId = ?) ")
            .Append(" and D.IsCreditsEarned = 1 ")
            .Append(" and C.StuEnrollId = E.StuEnrollId and E.StuEnrollId = ? ")
            .Append(strExcludeNullGrd1)
        End With

        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim summary As New TranscriptInfo
        While dr.Read()
            With summary
                If Not (dr("CreditsEarned") Is DBNull.Value) Then .TotalCreditsEarned = dr("CreditsEarned") Else .TotalCreditsEarned = 0
            End With
        End While
        If Not dr.IsClosed Then dr.Close()
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        With sb
            .Append(" select Sum(B.Credits) as CreditsAttempted ")
            .Append(" from arClassSections A ,arReqs B,arResults C,arGradeSystemDetails D,arStuEnrollments E where  ")
            .Append(" A.ReqId = B.ReqId and A.ClsSectionId = C.TestId and C.GrdSysDetailId = D.GrdSysDetailId and ")
            .Append(" B.ReqId in ")
            .Append(" (select ReqId from arProgVerDef where PrgVerId = ?) ")
            .Append(" and D.IsCreditsAttempted = 1 ")
            .Append(" and C.StuEnrollId = E.StuEnrollId and E.StuEnrollId = ? ")
            .Append(strExcludeNullGrd1)
        End With
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        dr = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            With summary
                If Not (dr("CreditsAttempted") Is DBNull.Value) Then .TotalCreditsAttempted = dr("CreditsAttempted") Else .TotalCreditsAttempted = 0

            End With
        End While
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        With sb
            .Append(" select Sum(D.GPA) as GPA ")
            .Append(" from arClassSections A ,arReqs B,arResults C,arGradeSystemDetails D,arStuEnrollments E where  ")
            .Append(" A.ReqId = B.ReqId and A.ClsSectionId = C.TestId and C.GrdSysDetailId = D.GrdSysDetailId and ")
            .Append(" B.ReqId in ")
            .Append(" (select ReqId from arProgVerDef where PrgVerId = ?) ")
            .Append(" and D.IsInGPA = 1  ")
            .Append(" and C.StuEnrollId = E.StuEnrollId and E.StuEnrollId = ? ")
            'Modified on 05/18/2005 by Michelle R. Rodriguez
            .Append(strExcludeNullGrd1)
            'and C.StudentId =
            '.Append(" (select Distinct StudentId from arStuEnrollments where StuEnrollId = ?) ")
        End With
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        dr = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            With summary
                If Not (dr("GPA") Is DBNull.Value) Then
                    .GPAAverageValue = dr("GPA")
                Else
                    .GPAAverageValue = 0
                End If
            End With
        End While
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Modified by Michelle R. Rodriguez on 05/18/2005
        If ExcludeNullGrades Then
            strExcludeNullGrd1 = "AND A.GrdSysDetailId IS NOT NULL "
            strExcludeNullGrd2 = "AND D.GrdSysDetailId IS NOT NULL"
        End If

        With sb

            .Append(" select Count(t2.Code) as NumberOfClasses from ")
            .Append(" (select Distinct A.GrdSysDetailId,A.TestId,B.ClsSection,C.Code,C.Descrip,B.StartDate, ")
            .Append(" (select Grade from arGradeSystemDetails where GrdSysDetailId = A.GrdSysDetailId) as Grades,A.StuEnrollId,B.TermId, ")
            .Append(" (select TermDescrip from arTerm where TermId=B.TermId) as Term ")
            .Append(" from arResults A,arClassSections B,arReqs C ")
            .Append(" where StuEnrollId = ? ")
            .Append(" and A.TestId = B.ClsSectionId and B.ReqId = C.ReqId ")
            'Added by Balaji on 7.18.2013
            .Append(" and A.IsCourseCompleted=1 ")
            'Modified by Michelle R. Rodriguez on 05/18/2005 to exclude NULL grades.
            .Append(strExcludeNullGrd1)
            .Append(" Union ")
            .Append(" select Distinct D.GrdSysDetailId,D.TestId,C.ClsSection,B.Code,B.Descrip,C.StartDate, ")
            .Append(" (select Grade from arGradeSystemDetails where GrdSysDetailId = D.GrdSysDetailId) as Grades, ")
            .Append(" D.StuEnrollId,C.TermId, ")
            .Append(" (select TermDescrip from arTerm where TermId = C.TermId) as Term ")
            .Append(" from arProgVerDef A,arReqs B,arClassSections C,arResults D,arStuEnrollments E ")
            .Append(" where A.ReqId = B.ReqId And B.ReqId = C.ReqId And C.ClsSectionId = D.TestId and D.StuEnrollId = E.StuEnrollId ")
            .Append(" and A.PrgVerId = ? and E.StuEnrollId = ? ")
            .Append(" and D.StuEnrollId = ? ")
            'Added by Balaji on 7.18.2013
            .Append(" and D.IsCourseCompleted=1 ")
            .Append(strExcludeNullGrd2)
            .Append(" ) t2  ")
        End With
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        dr = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            With summary
                If Not (dr("NumberOfClasses") Is DBNull.Value) Then .TotalClasses = dr("NumberOfClasses") Else .TotalClasses = 0
            End With
        End While
        If Not dr.IsClosed Then dr.Close()
        db.ClearParameters()
        sb.Remove(0, sb.Length)


        Dim sb35 As New StringBuilder
        With sb35
            .Append(" select case IsMakingSAP when 1  then 'Yes' else 'No'  end as MakingSAP from arSAPChkResults where StuEnrollId = ?  and Previewsapcheck=0 and DatePerformed = (select Max(DatePerformed) from arSAPChkResults where StuEnrollId=? and Previewsapcheck=0) ")
        End With
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr35 As OleDbDataReader = db.RunParamSQLDataReader(sb35.ToString)
        While dr35.Read()
            With summary
                Try
                    .IsMakingSAP = IIf(dr35("MakingSAP").ToString() = "Yes", True, False)
                Catch ex As Exception
                    .IsMakingSAP = False
                End Try
            End With
        End While
        If Not dr35.IsClosed Then dr35.Close()
        db.ClearParameters()
        sb.Remove(0, sb.Length)
        Return summary
    End Function
    Public Function GetSummaryFromSP(ByVal stuEnrollIdList As String) As TranscriptInfo

        Dim coursesTakenTableName As String
        Dim gPASummaryTableName As String
        Dim termId As Guid? = Nothing
        Dim showMultipleEnrollments As Boolean = False
        Dim ds As New DataSet()
        Dim dr As DataRow
        Dim summary As New TranscriptInfo

        '   connect to the database
        Using connection As New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
            '  Perpare GPA 
            connection.Open()

            Using sqlCommand As New SqlCommand("Usp_TR_Sub03_PrepareGPA", connection)
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.Add(New SqlParameter("@StuEnrollIdList", SqlDbType.NVarChar)).Value = stuEnrollIdList

                Dim paramCoursesTakenTableName As SqlParameter = New SqlParameter("@CoursesTakenTableName", SqlDbType.NVarChar, 200)
                paramCoursesTakenTableName.Direction = ParameterDirection.Output
                sqlCommand.Parameters.Add(paramCoursesTakenTableName)

                Dim paramGPASummaryTableName As SqlParameter = New SqlParameter("@GPASummaryTableName", SqlDbType.NVarChar, 200)
                paramGPASummaryTableName.Direction = ParameterDirection.Output
                sqlCommand.Parameters.Add(paramGPASummaryTableName)
                sqlCommand.CommandTimeout = 90
                sqlCommand.ExecuteNonQuery()

                coursesTakenTableName = sqlCommand.Parameters("@CoursesTakenTableName").Value
                gPASummaryTableName = sqlCommand.Parameters("@GPASummaryTableName").Value
            End Using

            '   Get totales
            Using sqlCommand As New SqlCommand("USP_TR_Sub07_TotalCourses", connection)
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.Add(New SqlParameter("@CoursesTakenTableName", SqlDbType.NVarChar, 200)).Value = coursesTakenTableName
                sqlCommand.Parameters.Add(New SqlParameter("@GPASummaryTableName", SqlDbType.NVarChar, 200)).Value = gPASummaryTableName
                sqlCommand.Parameters.Add(New SqlParameter("@StuEnrollIdList", SqlDbType.NVarChar)).Value = stuEnrollIdList
                sqlCommand.Parameters.Add(New SqlParameter("@TermId", SqlDbType.UniqueIdentifier)).Value = termId
                sqlCommand.Parameters.Add(New SqlParameter("@ShowMultipleEnrollments", SqlDbType.Bit)).Value = showMultipleEnrollments
                Dim mySqlDataAdapter As New SqlDataAdapter(sqlCommand)
                mySqlDataAdapter.Fill(ds)
            End Using

            If (ds.Tables(0).Rows.Count > 0) Then
                dr = ds.Tables(0).Rows(0)
                If (LCase(dr("GradesFormat").ToString()) = "letter") Then
                    summary.GPAAverageLable = "Cumulative GPA:"
                Else
                    summary.GPAAverageLable = "Average:"
                End If

                summary.PrgVersionTrackCredits = dr("PrgVersionTrackCredits")
                If (LCase(dr("GPAMethod").ToString()) = "weightedavg" _
                    And summary.PrgVersionTrackCredits = True) Then
                    summary.GPAAverageValue = IIf(IsDBNull(dr("EnrollmentWeightedGPA")), 0.0, Decimal.Parse(dr("EnrollmentWeightedGPA").ToString()))
                Else
                    summary.GPAAverageValue = IIf(IsDBNull(dr("EnrollmentSimpleGPA")), 0.0, Decimal.Parse(dr("EnrollmentSimpleGPA").ToString()))
                End If
                summary.TotalCreditsAttempted = Decimal.Parse(dr("CreditsAttempted"))
                summary.TotalHoursScheduled = Decimal.Parse(dr("ScheduleDays"))
                summary.TotalCreditsEarned = IIf(IsDBNull(dr("CreditsEarned")), 0.0, Decimal.Parse(dr("CreditsEarned").ToString()))
                summary.TotalHoursEarned = IIf(IsDBNull(dr("ActualDay")), 0.0, Decimal.Parse(dr("ActualDay").ToString()))
                summary.GradePoints = IIf(IsDBNull(dr("GradePoints")), 0.0, Decimal.Parse(dr("GradePoints").ToString()))

                summary.TotalClasses = dr("CoursesTaked")
                If (Integer.Parse(dr("IsMakingSAP")) = 0) Then
                    summary.IsMakingSAP = False
                Else
                    summary.IsMakingSAP = True
                End If
                summary.AcademicType = dr("AcademicType")
                summary.PrgVersionTrackCredits = dr("PrgVersionTrackCredits")
            End If

            '   delete temp tables
            Using sqlCommand As New SqlCommand("USP_TR_Sub03_DropPreparedGPATable", connection)
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.Add(New SqlParameter("@TableName", SqlDbType.NVarChar, 200)).Value = coursesTakenTableName
                sqlCommand.ExecuteNonQuery()
            End Using
            Using sqlCommand As New SqlCommand("USP_TR_Sub03_DropPreparedGPATable", connection)
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.Add(New SqlParameter("@TableName", SqlDbType.NVarChar, 200)).Value = gPASummaryTableName
                sqlCommand.ExecuteNonQuery()
            End Using
            connection.Close()
        End Using

        ds.Clear()

        Return summary
    End Function

    Public Function GetParentChildLevel() As Integer
        Dim cmdSelectAuthors As OleDbCommand
        Dim dtrAuthors As OleDbDataReader
        Dim intGrpCount As Integer
        Dim db As New DataAccess

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        Dim conPubs As OleDbConnection
        conPubs = New OleDbConnection(conString) '(MyAdvAppSettings.AppSettings("ConString"))
        conPubs.Open()

        cmdSelectAuthors = New OleDbCommand("Select Distinct GrpId From arReqGrpDef", conPubs)
        dtrAuthors = cmdSelectAuthors.ExecuteReader()
        While dtrAuthors.Read()
            intGrpCount += 1
        End While
        Return intGrpCount
    End Function
    Public Function GetGraduateAuditByEnrollment(ByVal StuEnrollId As String, Optional ByVal termEndDate As DateTime = #1/1/1001#, Optional ByVal termCond As String = "", Optional ByVal classCond As String = "", Optional ByVal campusId As String = "") As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim sb As New StringBuilder
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        'Get the direct children of the prog version associated with the enrollment passed in
        With sb
            .Append("SELECT distinct t400.ReqId as ReqId,t3.Descrip As Req,t3.Code As Code,t3.Credits, t3.Hours, t3.ReqTypeId, ")
            .Append("t400.Credits as DefCredits, t600.Credits as ProgCredits,t600.Hours as ProgHours,")
            .Append("t600.PrgVerDescrip,t400.ReqSeq,t600.IsContinuingEd,t600.PrgVerId,t3.FinAidCredits,t100.StuEnrollId    ")
            'Added By Vijay Ramteke on Feb 16, 2010
            .Append("       , t3.Hours As ScheduledHours   ")
            'Added By Vijay Ramteke in Feb 16, 2010
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600 ")
            '.Append(" ,syStatuses t700 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            ' .Append("t3.StatusId = t700.StatusId AND ")
            '.Append("t700.StatusId='" & strActiveGUID & "'" & " AND ")
            .Append("t100.StuEnrollId =  ? ")
            .Append("ORDER BY t400.ReqSeq ")
        End With

        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "DirectChildren")
            sb.Remove(0, sb.Length)
            db.ClearParameters()
        Catch ex As Exception
            Throw ex
        End Try

        'Get the results for classes in the prog version for the enrollment passed in.
        With sb
            .Append("select R.TermId , R.TermDescrip ")
            .Append(", ISNULL(ATES.DescripXTranscript, '') AS DescripXTranscript ")
            .Append(", R.ReqId, R.Code, R.Descrip, R.Credits, R.Hours, R.CourseCategoryId, R.StartDate, R.EndDate, R.ClassStartDate, R.ClassEndDate ")
            .Append(", R.GrdSysDetailId, R.TestId, R.ResultId, R.Grade, R.IsPass, R.GPA, R.IsCreditsAttempted, R.IsCreditsEarned, R.IsInGPA, R.IsInSAP,  R.IsDrop ")
            .Append(", R.CourseCategory, R.Score, R.FinAidCredits, R.DateIssue, R.DropDate, R.ScheduledHours, R.IsTransferGrade  ")
            .Append("FROM (    ")
            .Append("SELECT DISTINCT ")
            .Append("       t4.TermId, t3.TermDescrip, ")
            .Append("       t4.ReqId, t2.Code, t2.Descrip AS Descrip, ")
            .Append("       t2.Credits, t2.Hours, t2.CourseCategoryId, t3.StartDate AS StartDate, t3.EndDate AS EndDate, t4.StartDate as ClassStartDate, t4.EndDate as ClassEndDate, ")
            .Append("       t1.GrdSysDetailId, t1.TestId, t1.ResultId, ")
            .Append("       (Select Grade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId)as Grade, ")
            .Append("       (Select IsPass from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsPass, ")
            .Append("       (Select isnull(GPA,0) from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as GPA, ")
            .Append("       (Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsAttempted, ")
            .Append("       (Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsEarned, ")
            .Append("       (Select IsInGPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInGPA, ")
            .Append("       (Select IsInSAP from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInSAP, ")
            .Append("       (Select IsDrop from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsDrop, ")
            .Append("       (Select Descrip from arCourseCategories where CourseCategoryId = t2.CourseCategoryId) as CourseCategory, t1.Score, t2.FinAidCredits,  ")
            .Append("       Case(t2.IsExternship) When 1 then (select max(AttendedDate) from arExternshipAttendance where arExternshipAttendance.StuEnrollId = t1.StuEnrollId) else t4.EndDate end AS DateIssue, t1.DateDetermined as DropDate ")
            .Append("       , t2.Hours As ScheduledHours ")
            .Append("       , (Select IsTransferGrade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsTransferGrade ")
            .Append("       , t1.StuEnrollId  ")
            .Append("FROM   arResults t1, arReqs t2, arTerm t3, arClassSections t4, arClassSectionTerms t5, arStuEnrollments t6 ")
            .Append("WHERE  t1.TestId = t4.ClsSectionId ")
            .Append("       AND t4.ClsSectionId = t5.ClsSectionId ")
            .Append("       AND t5.TermId = t3.TermId ")
            .Append("       AND t4.ReqId = t2.ReqId ")
            .Append("       AND t1.StuEnrollId = t6.StuEnrollId ")
            If Not myAdvAppSettings.AppSettings("GradesFormat", campusId).ToString.ToLower = "numeric" Then
                .Append(" AND  ")
                'code added by balaji on 4/08/2009 to fix issue with credits per service mantis:15477 
                'modification starts here
                .Append(" (t1.GrdSysDetailId is not null or (t1.GrdSysDetailId is null and t1.isClinicsSatisfied = 1) ")
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    .Append(" or (t1.GrdSysDetailId is null  and  (t1.isClinicsSatisfied = 0 or t1.IsClinicsSatisfied is NULL)   and (select count(*) from argrdBkResults where StuEnrollId = t1.StuEnrollId and ClsSectionId=t1.TestId and Score is not null) >=1) ")
                End If
                .Append(" ) ")
                'ends here
            Else
                .Append(" AND ")
                'code added by balaji on 4/08/2009 to fix issue with credits per service mantis:15477 
                'modification starts here
                .Append(" (t1.Score is not null or (t1.score is null and t1.isClinicsSatisfied=1) ")
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    .Append(" or (t1.score is null and  (t1.isClinicsSatisfied=0 or t1.IsClinicsSatisfied is NULL)  and (select count(*) from argrdBkResults where StuEnrollId = t1.StuEnrollId and ClsSectionId=t1.TestId and Score is not null) >=1) ")
                End If
                .Append(" ) ")
                'ends here
            End If
            .Append("       AND t1.StuEnrollId = ? ")
            If myAdvAppSettings.AppSettings("SchedulingMethod", campusId) <> "ModuleStart" Then
                If Not (termEndDate = #1/1/1001#) Then
                    .Append("AND t3.EndDate <= ? ")
                End If
            End If
            'Code added by Vijay Ramteke on May, 07 2009
            If termCond <> "" Then
                .Append(" " & termCond.ToLower.Replace("arterm", "t3") & " ")
            End If
            If classCond <> "" Then
                .Append(" " & classCond.ToLower.Replace("arclasssections", "t4") & " ")
            End If
            'Code added by Vijay Ramteke on May, 07 2009

            'If we are dealing with a module start school the class might be shared between starts.
            'In this case the class would show up multiple times on the student's transcript - once
            'for each start that it is related to. As a temporary fix we are addding a filter to only
            'return the results for the start of the student. Remember that this is just a temporary 
            'fix. In the long term we will have to store the PK from arClassSectionTerms table that
            'tells us exactly which class and start the student is doing th class for. This is
            'important because the student might fail a course and has to retake it in a different 
            'start from the one that he started out with.
            'If SingletonAppSettings.AppSettings("SchedulingMethod", CampusId) = "ModuleStart" Then
            '    .Append("   AND t6.ExpStartDate = t3.StartDate ")
            'End If
            .Append("UNION ")
            .Append("SELECT DISTINCT ")
            .Append("       t10.TermId, t30.TermDescrip, ")
            .Append("       t10.ReqId, t20.Code, t20.Descrip AS Descrip, ")
            .Append("       t20.Credits, t20.Hours, t20.CourseCategoryId, t30.StartDate AS StartDate, t30.EndDate AS EndDate, '1/1/1900' as ClassStartDate, '1/1/1900' as ClassEndDate, ")
            .Append("       t10.GrdSysDetailId, '{00000000-0000-0000-0000-000000000000}' AS TestId, t10.TransferId AS ResultId, ")
            .Append("       (Select Grade from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId)as Grade, ")
            .Append("       (Select IsPass from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsPass, ")
            .Append("       (Select isnull(GPA,0) from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as GPA, ")
            .Append("       (Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsCreditsAttempted, ")
            .Append("       (Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsCreditsEarned, ")
            .Append("       (Select IsInGPA from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsInGPA, ")
            .Append("       (Select IsInSAP from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsInSAP, ")
            .Append("       (Select IsDrop from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsDrop, ")
            .Append("       (Select Descrip from arCourseCategories where CourseCategoryId = t20.CourseCategoryId) as CourseCategory, t10.Score, t20.FinAidCredits, ")
            'Code Added By Vijay Ramteke on May, 11 2009
            '.Append("       t30.EndDate AS DateIssue,t30.EndDate AS DropDate      ")
            .Append("       Case(t20.IsExternship) When 1 then (select max(AttendedDate) from arExternshipAttendance where arExternshipAttendance.StuEnrollId = t10.StuEnrollId) else t30.EndDate end AS DateIssue, t30.EndDate AS DropDate  ")
            'Code Added By Vijay Ramteke on May, 11 2009
            'Added By Vijay Ramteke on Feb 16, 2010
            .Append("       , t20.Hours As ScheduledHours ")
            'Added By Vijay Ramteke in Feb 16, 2010
            ''New Code Added By Vijay Ramteke For Rally Id 1393 on January 05, 2011
            .Append("       , (Select IsTransferGrade from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsTransferGrade ")
            .Append("       , t10.StuEnrollId  ")
            ''New Code Added By Vijay Ramteke For Rally Id 1393 on January 05, 2011
            .Append("FROM   arTransferGrades t10, arReqs t20, arTerm t30 ")
            .Append("WHERE  t10.TermId = t30.TermId ")
            .Append("       AND t10.ReqId = t20.ReqId ")
            If Not myAdvAppSettings.AppSettings("GradesFormat", campusId).ToString.ToLower = "numeric" Then
                .Append(" AND  ")
                'code added by balaji on 4/08/2009 to fix issue with credits per service mantis:15477 
                'modification starts here
                .Append(" (t10.GrdSysDetailId is not null or (t10.GrdSysDetailId is null and t10.isClinicsSatisfied = 1) ")
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    .Append(" or (t10.GrdSysDetailId is null  and  (t10.isClinicsSatisfied=0 or t10.IsClinicsSatisfied is NULL)   and (select count(*) from argrdBkResults where StuEnrollId=t10.StuEnrollId and ClsSectionId in (select Distinct ClsSectionId from arClassSections where ReqId = t10.ReqId and TermId = t10.TermId) and Score is not null) >= 1) ")
                End If
                .Append(" ) ")
                'ends here
            Else
                .Append(" AND ")
                'code added by balaji on 4/08/2009 to fix issue with credits per service mantis:15477 
                'modification starts here
                .Append(" (t10.Score is not null or (t10.score is null and t10.isClinicsSatisfied=1) ")
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    .Append(" or (t10.score is null and  (t10.isClinicsSatisfied = 0 or t10.IsClinicsSatisfied is NULL)  and (select count(*) from argrdBkResults where StuEnrollId = t10.StuEnrollId and ClsSectionId in (select Distinct ClsSectionId from arClassSections where ReqId = t10.ReqId and TermId = t10.TermId) and Score is not null) >=1) ")
                End If
                .Append(" ) ")
                'ends here
            End If
            .Append("       AND t10.StuEnrollId = ? ")
            If myAdvAppSettings.AppSettings("SchedulingMethod", campusId) <> "ModuleStart" Then
                If Not (termEndDate = #1/1/1001#) Then
                    .Append("AND t30.EndDate <= ? ")
                End If
            End If
            'Code added by Vijay Ramteke on May, 07 2009
            If termCond <> "" Then
                .Append(" " & termCond.ToLower.Replace("arterm", "t30") & " ")
            End If
            If classCond <> "" Then
                .Append(" " & classCond.ToLower.Replace("arclasssections", "t30") & " ")
            End If
            'Code added by Vijay Ramteke on May, 07 2009

            '.Append(" union ")
            '.Append(" select distinct t4.TermId,t3.TermDescrip,S.ReqId,t2.Code,t2.Descrip AS Descrip,       t2.Credits, ")
            '.Append(" t2.Hours,t2.CourseCategoryId,t3.StartDate AS StartDate,t3.EndDate AS EndDate,       t1.GrdSysDetailId,t1.TestId,t1.ResultId, ")
            '.Append(" (Select Grade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId)as Grade,    ")
            '.Append(" (Select IsPass from ")
            '.Append(" arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsPass,       (Select isnull(GPA,0) from arGradeSystemDetails ")
            '.Append(" where GrdSysDetailId = t1.GrdSysDetailId) as GPA,       (Select IsCreditsAttempted from arGradeSystemDetails where ")
            '.Append(" GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsAttempted,       (Select IsCreditsEarned from arGradeSystemDetails ")
            '.Append(" where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsEarned,       (Select IsInGPA from arGradeSystemDetails where ")
            '.Append(" GrdSysDetailId = t1.GrdSysDetailId) as IsInGPA,       (Select IsDrop from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) ")
            '.Append(" as IsDrop,       (Select Descrip from arCourseCategories where CourseCategoryId = t2.CourseCategoryId) as CourseCategory,t1.Score,t2.FinAidCredits   ")
            '.Append(" from ")
            '.Append(" (SELECT distinct t700.ReqId as EqReqId,t3.ReqId,t100.StuEnrollId  FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600 ")
            '.Append(" ,arCourseEquivalent t700 ")
            '.Append(" WHERE(t100.StudentId = t500.StudentId) ")
            '.Append(" AND t3.ReqId = t400.ReqId AND t100.PrgVerId = t400.PrgVerId AND t100.PrgVerId = t600.PrgVerId ")
            '.Append(" AND t3.Reqid=t700.EquivReqId AND t100.StuEnrollId =  ? ")
            '.Append("   and t3.Reqid not in ")
            '.Append(" (select ReqId from arResults a ,arClassSections b where a.TestId=b.ClsSectionId ")
            '.Append(" and StuEnrollId =  ? and ReqId=t400.ReqId)) S, arResults t1, arReqs t2, ")
            '.Append(" arTerm t3, arClassSections t4, arClassSectionTerms t5, arStuEnrollments t6  WHERE  t1.TestId = t4.ClsSectionId  ")
            '.Append(" AND t4.ClsSectionId = t5.ClsSectionId       AND t5.TermId = t3.TermId       AND t4.ReqId = t2.ReqId       AND ")
            '.Append(" t1.StuEnrollId = t6.StuEnrollId And t1.GrdSysDetailId Is Not null And t1.StuEnrollId = S.StuEnrollId ")
            '.Append(" and t2.Reqid=S.EqReqId ")
            .Append(") AS R  ")
            .Append(" LEFT OUTER JOIN arTermEnrollSummary AS ATES ON R.TermId = ATES.TermId AND R.StuEnrollId = ATES.StuEnrollId ")
            .Append(" ORDER BY StartDate desc, EndDate, TermDescrip ")
        End With

        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If myAdvAppSettings.AppSettings("SchedulingMethod", campusId) <> "ModuleStart" Then
            If Not (termEndDate = #1/1/1001#) Then
                db.AddParameter("@EndDate1", termEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
        End If
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If myAdvAppSettings.AppSettings("SchedulingMethod", campusId) <> "ModuleStart" Then
            If Not (termEndDate = #1/1/1001#) Then
                db.AddParameter("@EndDate", termEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
        End If
        'db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "Results")
            Dim stuStartDate As String = String.Empty
            Dim aRows() As DataRow


            'modified by Theresa G on 5/29/09 for mantis 16376: 2.2.0: Course is shown in the completed section even before the results are transferred. 
            '----------------------------------------------------------------------------------------------------------------------------------------
            Dim labCount As Integer
            Dim labCountAttempted As Integer
            For Each row As DataRow In ds.Tables("Results").Rows
                'labCount = 0
                'labCountAttempted = 0
                If myAdvAppSettings.AppSettings("SchedulingMethod", campusId) = "ModuleStart" Then
                    If row("TestId").ToString <> "00000000-0000-0000-0000-000000000000" Then
                        If (IsDuplicateClassSection(row("TestId").ToString) = True) Then
                            stuStartDate = GetStudentStartDate(StuEnrollId)
                            aRows = ds.Tables("Results").Select("TestId='" & row("TestId").ToString & "' AND StartDate=#" & CDate(stuStartDate) & "#")
                            If aRows.Length > 0 Then
                                If (stuStartDate <> CDate(row("StartDate")).ToShortDateString) Then
                                    row.Delete()
                                End If
                            Else
                                'aRows = ds.Tables(0).Select("TestId='" & row("TestId").ToString & "'")
                                aRows = ds.Tables("Results").Select("TestId='" & row("TestId").ToString & "' AND StartDate <> #" & row("StartDate") & "#")
                                If (aRows.Length > 0) Then
                                    row.Delete()
                                End If
                            End If
                        End If
                    End If
                End If
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    labCount = (New TransferGradeDB).LabWorkOrLabHourCourseCount(row("ReqId").ToString)
                    If labCount <= 0 And row("GrdSysDetailId").ToString = "" Then row.Delete()
                    If labCount > 0 Then
                        labCountAttempted = (New ExamsDB).GetClinicServicesAndHoursAttemptedByStudent(StuEnrollId, row("Reqid").ToString)
                        If labCountAttempted <= 0 Then row.Delete()
                    End If

                End If

            Next
            ds.AcceptChanges()
            '----------------------------------------------------------------------------------------------------------------------------------------

            sb.Remove(0, sb.Length)
            db.ClearParameters()
        Catch ex As Exception
            Throw ex
        End Try

        'Get the direct children for all groups used in the program version irregardless of level
        With sb
            .Append("SELECT rg.GrpId,rg.ReqId,ar.Descrip as Req,ar.Code as Code,ar.Credits,ar.ReqTypeId,rg.ReqSeq,ar.Hours,stu.PrgVerId,ar.FinAidCredits,stu.StuEnrollid     ")
            'Added By Vijay Ramteke on Feb 16, 2010
            .Append("       , ar.Hours As ScheduledHours   ")
            'Added By Vijay Ramteke in Feb 16, 2010
            .Append("FROM arReqGrpDef rg, arReqs ar, arStuEnrollments stu ")
            .Append("WHERE rg.ReqId=ar.ReqId and stu.StuEnrollId = ? ")
            .Append("AND EXISTS( ")
            .Append("SELECT distinct t4.ReqId ")
            .Append("FROM arReqs t4 ")
            .Append("WHERE t4.ReqTypeId=2 ")
            .Append("AND t4.ReqId=rg.GrpId ")
            .Append("AND EXISTS( ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t400.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t400.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t700.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t700.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append(" SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t800.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t800.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t900.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t900.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t1000.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t1000.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t1100.ReqId as ReqId, t3.Descrip As Req ")
            .Append(" FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000, arReqGrpDef t1100 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t1000.ReqId = t1100.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t1100.ReqId=t4.ReqId ")
            .Append(")) ")
            .Append("ORDER BY rg.ReqSeq ")


        End With
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "GroupChildren")
            sb.Remove(0, sb.Length)
            db.ClearParameters()
        Catch ex As Exception
            Throw ex
        End Try

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    Public Function GetGraduateAuditByEnrollmentForCourseEquivalent(ByVal StuEnrollId As String, Optional ByVal termEndDate As DateTime = #1/1/1001#, Optional ByVal termCond As String = "", Optional ByVal classCond As String = "", Optional ByVal Campusid As String = "") As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim sb As New StringBuilder

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        'Get the direct children of the prog version associated with the enrollment passed in
        With sb
            .Append("SELECT distinct t400.ReqId as ReqId,t3.Descrip As Req,t3.Code As Code,t3.Credits, t3.Hours, t3.ReqTypeId, ")
            .Append("t400.Credits as DefCredits, t600.Credits as ProgCredits,t600.Hours as ProgHours,")
            .Append("t600.PrgVerDescrip,t400.ReqSeq,t600.IsContinuingEd,t600.PrgVerId,t3.FinAidCredits,t100.StuEnrollId    ")
            .Append("       , t3.Hours As ScheduledHours   ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.IsAttendanceOnly = 0 AND ")
            .Append("t100.StuEnrollId =  ? ")
            .Append("ORDER BY t400.ReqSeq ")
        End With

        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "DirectChildren")
            sb.Remove(0, sb.Length)
            db.ClearParameters()
        Catch ex As Exception
            Throw ex
        End Try

        'Get the results for classes in the prog version for the enrollment passed in.
        With sb
            .Append("select R.TermId , R.TermDescrip ")
            .Append(", ISNULL(ATES.DescripXTranscript, '') AS DescripXTranscript ")
            .Append(", R.ReqId, R.Code, R.Descrip, R.Credits, R.Hours, R.CourseCategoryId, R.StartDate, R.EndDate, R.ClassStartDate, R.ClassEndDate ")
            .Append(", R.GrdSysDetailId, R.TestId, R.ResultId, R.Grade, R.IsPass, R.GPA, R.IsCreditsAttempted, R.IsCreditsEarned, R.IsInGPA, R.IsInSAP, R.IsDrop ")
            .Append(", R.CourseCategory, R.Score, R.FinAidCredits, R.DateIssue, R.DropDate, R.ScheduledHours, R.IsTransferGrade  ")
            .Append("FROM (    ")
            .Append("SELECT DISTINCT ")
            .Append("       t4.TermId, t3.TermDescrip, ")
            .Append("       t4.ReqId, t2.Code, t2.Descrip AS Descrip, ")
            .Append("       t2.Credits, t2.Hours, t2.CourseCategoryId, t3.StartDate AS StartDate, t3.EndDate AS EndDate, t4.StartDate as ClassStartDate, t4.EndDate as ClassEndDate, ")
            .Append("       t1.GrdSysDetailId, t1.TestId, t1.ResultId, ")
            .Append("       (Select Grade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as Grade, ")
            .Append("       (Select IsPass from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsPass, ")
            .Append("       (Select isnull(GPA,0) from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as GPA, ")
            .Append("       (Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsAttempted, ")
            .Append("       (Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsEarned, ")
            .Append("       (Select IsInGPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInGPA, ")
            .Append("       (Select IsInSAP from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInSAP, ")
            .Append("       (Select IsDrop from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsDrop, ")
            .Append("       (Select Descrip from arCourseCategories where CourseCategoryId = t2.CourseCategoryId) as CourseCategory, t1.Score, t2.FinAidCredits,    ")
            .Append("       Case(t2.IsExternship) When 1 then (select max(AttendedDate) from arExternshipAttendance where arExternshipAttendance.StuEnrollId = t1.StuEnrollId) else t4.EndDate end AS DateIssue, t1.DateDetermined as DropDate  ")
            .Append("       , t2.Hours As ScheduledHours   ")
            .Append("       , (Select IsTransferGrade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsTransferGrade ")
            .Append("       , t1.StuEnrollId  ")
            .Append("FROM   arResults t1, arReqs t2, arTerm t3, arClassSections t4, arClassSectionTerms t5, arStuEnrollments t6  ")
            .Append("WHERE  t1.TestId = t4.ClsSectionId ")
            .Append("       AND t4.ClsSectionId = t5.ClsSectionId ")
            .Append("       AND t5.TermId = t3.TermId ")
            .Append("       AND t4.ReqId = t2.ReqId ")
            .Append("       AND t1.StuEnrollId = t6.StuEnrollId ")
            .Append("       AND t2.IsAttendanceOnly = 0 ")
            If Not myAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                .Append(" AND  ")
                .Append(" (t1.GrdSysDetailId is not null or (t1.GrdSysDetailId is null and t1.isClinicsSatisfied=1) ")
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    .Append(" or (t1.GrdSysDetailId is null  and  (t1.isClinicsSatisfied=0 or t1.IsClinicsSatisfied is NULL)   and (select count(*) from argrdBkResults where StuEnrollId = t1.StuEnrollId and ClsSectionId = t1.TestId and Score is not null) >= 1) ")
                End If
                .Append(" ) ")
                'ends here
            Else
                .Append(" AND ")
                'code added by balaji on 4/08/2009 to fix issue with credits per service mantis:15477 
                'modification starts here
                .Append(" (t1.Score is not null or (t1.score is null and t1.isClinicsSatisfied=1) ")
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    .Append(" or (t1.score is null and  (t1.isClinicsSatisfied=0 or t1.IsClinicsSatisfied is NULL)  and (select count(*) from argrdBkResults where StuEnrollId = t1.StuEnrollId and ClsSectionId = t1.TestId and Score is not null) >= 1) ")
                End If
                .Append(" ) ")
                'ends here
            End If
            .Append("       AND t1.StuEnrollId = ? ")
            If myAdvAppSettings.AppSettings("SchedulingMethod", Campusid) <> "ModuleStart" Then
                If Not (termEndDate = #1/1/1001#) Then
                    .Append("AND t3.EndDate <= ? ")
                End If
            End If
            'Code added by Vijay Ramteke on May, 07 2009
            If termCond <> "" Then
                .Append(" " & termCond.ToLower.Replace("arterm", "t3") & " ")
            End If
            If classCond <> "" Then
                .Append(" " & classCond.ToLower.Replace("arclasssections", "t4") & " ")
            End If
            'Code added by Vijay Ramteke on May, 07 2009

            'If we are dealing with a module start school the class might be shared between starts.
            'In this case the class would show up multiple times on the student's transcript - once
            'for each start that it is related to. As a temporary fix we are addding a filter to only
            'return the results for the start of the student. Remember that this is just a temporary 
            'fix. In the long term we will have to store the PK from arClassSectionTerms table that
            'tells us exactly which class and start the student is doing th class for. This is
            'important because the student might fail a course and has to retake it in a different 
            'start from the one that he started out with.
            'If SingletonAppSettings.AppSettings("SchedulingMethod", CampusId) = "ModuleStart" Then
            '    .Append("   AND t6.ExpStartDate = t3.StartDate ")
            'End If
            .Append("UNION ")
            .Append("SELECT DISTINCT ")
            .Append("       t10.TermId, t30.TermDescrip, ")
            .Append("       t10.ReqId, t20.Code, t20.Descrip AS Descrip,")
            .Append("       t20.Credits, t20.Hours, t20.CourseCategoryId, t30.StartDate AS StartDate, t30.EndDate AS EndDate, '1/1/1900' as ClassStartDate, '1/1/1900' as ClassEndDate, ")
            .Append("       t10.GrdSysDetailId, '{00000000-0000-0000-0000-000000000000}' AS TestId, t10.TransferId AS ResultId,")
            .Append("       (Select Grade from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId)as Grade, ")
            .Append("       (Select IsPass from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsPass, ")
            .Append("       (Select isnull(GPA,0) from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as GPA,       ")
            .Append("       (Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsCreditsAttempted, ")
            .Append("       (Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsCreditsEarned,       ")
            .Append("       (Select IsInGPA from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsInGPA,       ")
            .Append("       (Select IsInSAP from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsInSAP,       ")
            .Append("       (Select IsDrop from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsDrop,  ")
            .Append("       (Select Top 1 Descrip from arCourseCategories where CourseCategoryId = t20.CourseCategoryId) as CourseCategory, t10.Score as Score, t20.FinAidCredits, ")
            .Append("       Case(t20.IsExternship) When 1 then (select max(AttendedDate) from arExternshipAttendance where arExternshipAttendance.StuEnrollId = t10.StuEnrollId) else t30.EndDate end AS DateIssue, t30.EndDate AS DropDate ")
            .Append("       , t20.Hours As ScheduledHours ")
            .Append("      , (Select IsTransferGrade from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsTransferGrade ")
            .Append("       , t10.StuEnrollId  ")
            .Append("FROM   arTransferGrades t10, arReqs t20, arTerm t30 ")
            .Append("WHERE  t10.TermId = t30.TermId")
            .Append("       AND t10.ReqId = t20.ReqId ")
            .Append("       AND t20.IsAttendanceOnly = 0 ")
            If Not myAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                .Append(" AND  ")
                'code added by balaji on 4/08/2009 to fix issue with credits per service mantis:15477 
                'modification starts here
                .Append(" (t10.GrdSysDetailId is not null or (t10.GrdSysDetailId is null and t10.isClinicsSatisfied = 1) ")
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    .Append(" or (t10.GrdSysDetailId is null  and  (t10.isClinicsSatisfied = 0 or t10.IsClinicsSatisfied is NULL)   and (select count(*) from argrdBkResults where StuEnrollId = t10.StuEnrollId and ClsSectionId in (select Distinct ClsSectionId from arClassSections where ReqId = t10.ReqId and TermId = t10.TermId) and Score is not null) >= 1) ")
                End If
                .Append(" ) ")
                'ends here
            Else
                .Append(" AND ")
                'code added by balaji on 4/08/2009 to fix issue with credits per service mantis:15477 
                'modification starts here
                .Append(" (t10.Score is not null or (t10.score is null and t10.isClinicsSatisfied=1) ")
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    .Append(" or (t10.score is null and  (t10.isClinicsSatisfied = 0 or t10.IsClinicsSatisfied is NULL)  and (select count(*) from argrdBkResults where StuEnrollId = t10.StuEnrollId and ClsSectionId in (select Distinct ClsSectionId from arClassSections where ReqId = t10.ReqId and TermId = t10.TermId) and Score is not null) >= 1) ")
                End If
                .Append(" ) ")
                'ends here
            End If
            'End If
            .Append("       AND t10.StuEnrollId = ? ")
            If myAdvAppSettings.AppSettings("SchedulingMethod", Campusid) <> "ModuleStart" Then
                If Not (termEndDate = #1/1/1001#) Then
                    .Append("AND t30.EndDate <= ? ")
                End If
            End If
            'Code added by Vijay Ramteke on May, 07 2009
            If termCond <> "" Then
                .Append(" " & termCond.ToLower.Replace("arterm", "t30") & " ")
            End If
            If classCond <> "" Then
                .Append(" " & classCond.ToLower.Replace("arclasssections", "t30") & " ")
            End If
            'Code added by Vijay Ramteke on May, 07 2009

            .Append("UNION ")
            .Append("SELECT DISTINCT ")
            .Append("       t4.TermId, t3.TermDescrip, ")
            .Append("       S.ReqId, t2.Code, t2.Descrip AS Descrip,   ")
            .Append("       t2.Credits, t2.Hours, t2.CourseCategoryId, t3.StartDate AS StartDate, t3.EndDate AS EndDate, t4.StartDate as ClassStartDate, t4.EndDate  as ClassEndDate,  ")
            .Append("       t1.GrdSysDetailId, t1.TestId, t1.ResultId, ")
            .Append("       (Select Top 1 Grade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId)as Grade, ")
            .Append("       (Select Top 1 IsPass from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsPass, ")
            .Append("       (Select Top 1 isnull(GPA,0) from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as GPA, ")
            .Append("       (Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsAttempted, ")
            .Append("       (Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsEarned, ")
            .Append("       (Select Top 1 IsInGPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInGPA, ")
            .Append("       (Select Top 1 IsInSAP from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInSAP, ")
            .Append("       (Select Top 1 IsDrop from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsDrop, ")
            .Append("       (Select Top 1 Descrip from arCourseCategories where CourseCategoryId = t2.CourseCategoryId) as CourseCategory, t1.Score, t2.FinAidCredits, ")
            .Append("       Case(t2.IsExternship) When 1 then (select max(AttendedDate) from arExternshipAttendance where arExternshipAttendance.StuEnrollId = t1.StuEnrollId) else t4.EndDate end AS DateIssue, t1.DateDetermined AS DropDate ")
            .Append("       , t2.Hours As ScheduledHours   ")
            .Append("       , (Select IsTransferGrade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsTransferGrade ")
            .Append("       , t1.StuEnrollId  ")
            .Append("FROM ")
            .Append("    (SELECT distinct ")
            .Append("            t700.ReqId as EqReqId, t3.ReqId, t100.StuEnrollId  ")
            .Append("     FROM arStuEnrollments t100, arReqs t3, arProgVerDef t400, adLeads t500, arPrgVersions t600, arCourseEquivalent t700  ")
            .Append("     WHERE(t100.StudentId = t500.StudentId) ")
            .Append("       AND t3.ReqId = t400.ReqId AND t100.PrgVerId = t400.PrgVerId AND t100.PrgVerId = t600.PrgVerId ")
            .Append("       AND t3.Reqid = t700.EquivReqId AND t100.StuEnrollId =  ? ")
            .Append("       AND t3.IsAttendanceOnly = 0 ")
            .Append("       AND t3.Reqid not in ")
            .Append("                          (select ReqId from arResults a ,arClassSections b where a.TestId = b.ClsSectionId ")
            .Append("                                        AND StuEnrollId =  ? and ReqId = t400.ReqId) ")
            .Append("    ) AS S, arResults t1, arReqs t2, arTerm t3, arClassSections t4, arClassSectionTerms t5, arStuEnrollments t6 ")
            .Append("WHERE  t1.TestId = t4.ClsSectionId  ")
            .Append("  AND t4.ClsSectionId = t5.ClsSectionId ")
            .Append("  AND t5.TermId = t3.TermId ")
            .Append("  AND t4.ReqId = t2.ReqId ")
            .Append("  AND t1.StuEnrollId = t6.StuEnrollId  ")
            If Not myAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                .Append("  AND t1.GrdSysDetailId IS NOT NULL")
            Else
                .Append("  AND t1.Score is NOT NULL ")
            End If
            .Append(" And t1.StuEnrollId = S.StuEnrollId ")
            .Append(" and t2.Reqid = S.EqReqId ")
            If termCond <> "" Then
                .Append(" " & termCond.ToLower.Replace("arterm", "t3") & " ")
            End If
            If classCond <> "" Then
                .Append(" " & classCond.ToLower.Replace("arclasssections", "t4") & " ")
            End If
            .Append(") AS R  ")
            .Append(" LEFT OUTER JOIN arTermEnrollSummary AS ATES ON R.TermId = ATES.TermId AND R.StuEnrollId = ATES.StuEnrollId ")
            .Append(" ORDER BY StartDate desc, EndDate, TermDescrip")
        End With

        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If myAdvAppSettings.AppSettings("SchedulingMethod", Campusid) <> "ModuleStart" Then
            If Not (termEndDate = #1/1/1001#) Then
                db.AddParameter("@EndDate1", termEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
        End If
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If myAdvAppSettings.AppSettings("SchedulingMethod", Campusid) <> "ModuleStart" Then
            If Not (termEndDate = #1/1/1001#) Then
                db.AddParameter("@EndDate", termEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
        End If
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "Results")
            Dim stuStartDate As String = String.Empty
            Dim aRows() As DataRow
            'modified by Theresa G on 5/29/09 for mantis 16376: 2.2.0: Course is shown in the completed section even before the results are transferred. 
            '----------------------------------------------------------------------------------------------------------------------------------------
            For Each row As DataRow In ds.Tables("Results").Rows
                If myAdvAppSettings.AppSettings("SchedulingMethod", Campusid) = "ModuleStart" Then
                    If row("TestId").ToString <> "00000000-0000-0000-0000-000000000000" Then
                        If (IsDuplicateClassSection(row("TestId").ToString) = True) Then
                            stuStartDate = GetStudentStartDate(StuEnrollId)
                            aRows = ds.Tables("Results").Select("TestId='" & row("TestId").ToString & "' AND StartDate=#" & CDate(stuStartDate) & "#")
                            If aRows.Length > 0 Then
                                If (stuStartDate <> CDate(row("StartDate")).ToShortDateString) Then
                                    row.Delete()
                                End If
                            Else
                                'aRows = ds.Tables(0).Select("TestId='" & row("TestId").ToString & "'")
                                aRows = ds.Tables("Results").Select("TestId='" & row("TestId").ToString & "' AND StartDate <> #" & row("StartDate") & "#")
                                If (aRows.Length > 0) Then
                                    row.Delete()
                                End If
                            End If
                        End If
                    End If
                End If

            Next
            ds.AcceptChanges()
            '----------------------------------------------------------------------------------------------------------------------------------------

            sb.Remove(0, sb.Length)
            db.ClearParameters()
        Catch ex As Exception
            Throw ex
        End Try

        'Get the direct children for all groups used in the program version irregardless of level
        With sb
            .Append("SELECT rg.GrpId,rg.ReqId,ar.Descrip as Req,ar.Code as Code,ar.Credits,ar.ReqTypeId,rg.ReqSeq,ar.Hours,stu.PrgVerId,ar.FinAidCredits,stu.StuEnrollid     ")
            'Added By Vijay Ramteke on Feb 16, 2010
            .Append("       , ar.Hours As ScheduledHours   ")
            'Added By Vijay Ramteke in Feb 16, 2010
            .Append("FROM arReqGrpDef rg, arReqs ar, arStuEnrollments stu ")
            .Append("WHERE rg.ReqId=ar.ReqId and stu.StuEnrollId = ? ")
            .Append("AND ar.IsAttendanceOnly = 0 ")
            .Append("AND EXISTS( ")
            .Append("SELECT distinct t4.ReqId ")
            .Append("FROM arReqs t4 ")
            .Append("WHERE t4.ReqTypeId=2 ")
            .Append("AND t4.ReqId=rg.GrpId ")
            .Append("AND t4.IsAttendanceOnly = 0 ")
            .Append("AND EXISTS( ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t400.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t400.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t700.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t700.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append(" SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t800.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t800.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t900.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t900.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t1000.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t1000.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t1100.ReqId as ReqId, t3.Descrip As Req ")
            .Append(" FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000, arReqGrpDef t1100 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t1000.ReqId = t1100.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t1100.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append(")) ")
            .Append("ORDER BY rg.ReqSeq ")


        End With
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "GroupChildren")
            sb.Remove(0, sb.Length)
            db.ClearParameters()
        Catch ex As Exception
            Throw ex
        End Try

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    Public Function GetGraduateAuditByEnrollmentForCourseEquivalent_SP(ByVal StuEnrollId As String, Optional ByVal termEndDate As DateTime = #1/1/1001#, Optional ByVal termCond As String = "", Optional ByVal classCond As String = "") As DataSet
        'Dim db As New DataAccess
        'Dim da As New OleDbDataAdapter
        'Dim sb As New StringBuilder
        'Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim DirectChildren As New DataTable
        'Dim results As New DataTable
        'Dim Groupchildren As New DataTable
        Dim ds As DataSet

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        ds = GetTranscriptLetter(StuEnrollId, termCond, classCond)
        ds.Tables(0).TableName = "DirectChildren"
        ds.Tables(1).TableName = "Results"
        ds.Tables(2).TableName = "Groupchildren"
        'DirectChildren = GetDirectChildrenForTranscript(StuEnrollId)
        'ds.Tables.Add(DirectChildren.Copy())
        'ds.Tables(0).TableName = "DirectChildren"

        'results = GetResultsForTranscript(StuEnrollId, termCond, classCond)
        'ds.Tables.Add(results.Copy())
        'ds.Tables(1).TableName = "Results"
        Try

            'Dim stuStartDate As String = String.Empty
            'Dim aRows() As DataRow
            'modified by Theresa G on 5/29/09 for mantis 16376: 2.2.0: Course is shown in the completed section even before the results are transferred. 
            '----------------------------------------------------------------------------------------------------------------------------------------
            Dim labCount As Integer = 0
            Dim labCountAttempted As Integer = 0
            If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                For Each row As DataRow In ds.Tables("Results").Rows
                    'labCount = 0
                    'labCountAttempted = 0

                    labCount = (New TransferGradeDB).LabWorkOrLabHourCourseCount_SP(row("ReqId").ToString)
                    If labCount <= 0 And row("GrdSysDetailId").ToString = "" Then row.Delete()
                    If labCount > 0 Then
                        labCountAttempted = (New ExamsDB).GetClinicServicesAndHoursAttemptedByStudent_SP(StuEnrollId, row("Reqid").ToString)
                        If labCountAttempted <= 0 Then row.Delete()
                    End If



                Next
            End If
            ds.AcceptChanges()
            '----------------------------------------------------------------------------------------------------------------------------------------


        Catch ex As Exception
            Throw ex
        End Try

        Return ds
    End Function

    Public Function GetGraduateAuditByEnrollmentForNumeric(ByVal StuEnrollId As String, Optional ByVal termEndDate As DateTime = #1/1/1001#, Optional ByVal termCond As String = "", Optional ByVal classCond As String = "", Optional ByVal campusId As String = "") As DataSet
        ' New Optional Parameter add termCond and classCond by Vijay Ramteke on May, 06 2009
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim sb As New StringBuilder
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        'Get the direct children of the prog version associated with the enrollment passed in
        With sb
            .Append("SELECT distinct t400.ReqId as ReqId,t3.Descrip As Req,t3.Code As Code,t3.Credits, t3.Hours, t3.ReqTypeId, ")
            .Append("t400.Credits as DefCredits, t600.Credits as ProgCredits,t600.Hours as ProgHours,")
            .Append("t600.PrgVerDescrip,t400.ReqSeq,t600.IsContinuingEd,t600.PrgVerId,t100.StuEnrollId  ")
            'Added By Vijay Ramteke on Feb 16, 2010
            .Append("       , t3.Hours As ScheduledHours   ")
            'Added By Vijay Ramteke in Feb 16, 2010
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600 ")
            '.Append(" ,syStatuses t700 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.IsAttendanceOnly = 0 AND ")
            ' .Append("t3.StatusId = t700.StatusId AND ")
            '.Append("t700.StatusId='" & strActiveGUID & "'" & " AND ")
            .Append("t100.StuEnrollId =  ? ")
            .Append("ORDER BY t400.ReqSeq ")
        End With

        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "DirectChildren")
            sb.Remove(0, sb.Length)
            db.ClearParameters()
        Catch ex As Exception
            Throw ex
        End Try

        'Get the results for classes in the prog version for the enrollment passed in.
        With sb
            .Append("select R.TermId , R.TermDescrip ")
            .Append(", ISNULL(ATES.DescripXTranscript, '') AS DescripXTranscript ")
            .Append(", R.ReqId, R.Code, R.Descrip, R.Credits, R.Hours, R.CourseCategoryId, R.StartDate, R.EndDate, R.ClassStartDate, R.ClassEndDate ")
            .Append(", R.GrdSysDetailId, R.TestId, R.ResultId, R.Grade, R.IsPass, R.GPA, R.IsCreditsAttempted, R.IsCreditsEarned, R.IsInGPA, R.IsInSAP, R.IsDrop ")
            .Append(", R.CourseCategory, R.Score, R.FinAidCredits, R.DateIssue, R.DropDate, R.ScheduledHours, R.IsTransferGrade  ")
            .Append("FROM (    ")
            .Append("SELECT DISTINCT ")
            .Append("       t4.TermId, t3.TermDescrip, ")
            .Append("       t4.ReqId, t2.Code, t2.Descrip AS Descrip, ")
            .Append("       t2.Credits, t2.Hours, t2.CourseCategoryId, t3.StartDate AS StartDate, t3.EndDate AS EndDate, t4.StartDate as ClassStartDate, t4.EndDate as ClassEndDate, ")
            .Append("       t1.GrdSysDetailId, t1.TestId, t1.ResultId, ")
            .Append("       (Select Grade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as Grade, ")
            .Append("       (Select IsPass from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsPass, ")
            .Append("       (Select isnull(GPA,0) from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as GPA, ")
            .Append("       Case when t1.GrdSysDetailId is NULL Then 1 else (Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) end as IsCreditsAttempted, ")
            .Append("       Case when t1.GrdSysDetailId is NULL Then 1 else (Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) end as IsCreditsEarned, ")
            .Append("       (Select IsInGPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInGPA, ")
            .Append("       (Select IsInSAP from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInSAP, ")
            .Append("       (Select IsDrop from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsDrop, ")
            .Append("       (Select  Distinct Top 1 Descrip from arCourseCategories where CourseCategoryId = t2.CourseCategoryId) as CourseCategory, t1.Score as Score, t2.FinAidCredits, ")
            .Append("       Case(t2.IsExternship) When 1 then (select max(AttendedDate) from arExternshipAttendance where arExternshipAttendance.StuEnrollId = t1.StuEnrollId) else t4.EndDate end AS DateIssue, t1.DateDetermined as DropDate  ")
            .Append("       , t2.Hours As ScheduledHours   ")
            .Append("       , (Select IsTransferGrade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsTransferGrade ")
            .Append("       , t1.StuEnrollId  ")
            .Append("FROM   arResults t1, arReqs t2, arTerm t3, arClassSections t4, arClassSectionTerms t5, arStuEnrollments t6 ")
            .Append("WHERE  t1.TestId = t4.ClsSectionId ")
            .Append("       AND t4.ClsSectionId = t5.ClsSectionId ")
            .Append("       AND t5.TermId = t3.TermId ")
            .Append("       AND t4.ReqId = t2.ReqId ")
            .Append("       AND t2.IsAttendanceOnly = 0 ")
            '------rolling back this change to fix this issue 15277: 2.0.0: Data on the transcript page is incorrect for a numeric school.  - Theresa
            .Append("       AND t1.StuEnrollId = t6.StuEnrollId  ")
            'code added by balaji on 4/08/2009 to fix issue with credits per service mantis:15477 
            'modification starts here
            .Append("      AND (t1.Score is not null or (t1.score is null and t1.isClinicsSatisfied = 1) ")
            If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                .Append(" or (t1.score is null and  (t1.isClinicsSatisfied=0 or t1.IsClinicsSatisfied is NULL)  and (select count(*) from argrdBkResults where StuEnrollId = t1.StuEnrollId and ClsSectionId = t1.TestId and Score is not null) >= 1) ")
            End If
            .Append(" ) ")
            'modification ends here

            '.Append("       AND (t1.GrdSysDetailId is not null  or t1.Score,t2.FinAidCredits   is not null) ")
            .Append("       AND t1.StuEnrollId = ? ")
            If myAdvAppSettings.AppSettings("SchedulingMethod", campusId) <> "ModuleStart" Then
                If Not (termEndDate = #1/1/1001#) Then
                    .Append("AND t3.EndDate <= ? ")
                End If
            End If
            'Code added by Vijay Ramteke on May, 07 2009
            If termCond <> "" Then
                .Append(" " & termCond.ToLower.Replace("arterm", "t3") & " ")
            End If
            If classCond <> "" Then
                .Append(" " & classCond.ToLower.Replace("arclasssections", "t4") & " ")
            End If
            'Code added by Vijay Ramteke on May, 07 2009
            .Append("UNION ")
            .Append("SELECT DISTINCT ")
            .Append("       t10.TermId, t30.TermDescrip, ")
            .Append("       t10.ReqId, t20.Code, t20.Descrip AS Descrip,")
            .Append("       t20.Credits, t20.Hours, t20.CourseCategoryId, t30.StartDate AS StartDate, t30.EndDate AS EndDate, '1/1/1900' as ClassStartDate, '1/1/1900' as ClassEndDate, ")
            .Append("       t10.GrdSysDetailId, '{00000000-0000-0000-0000-000000000000}' AS TestId, t10.TransferId AS ResultId,")
            .Append("       (Select Grade from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId)as Grade, ")
            .Append("       (Select IsPass from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsPass, ")
            .Append("       (Select isnull(GPA,0) from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as GPA,       ")
            .Append("       (Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsCreditsAttempted, ")
            .Append("       (Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsCreditsEarned,       ")
            .Append("       (Select IsInGPA from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsInGPA,       ")
            .Append("       (Select IsInSAP from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsInSAP, ")
            .Append("       (Select IsDrop from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsDrop,  ")
            .Append("       (Select Top 1 Descrip from arCourseCategories where CourseCategoryId = t20.CourseCategoryId) as CourseCategory, t10.Score as Score, t20.FinAidCredits, ")
            '.Append("     t30.EndDate AS DateIssue,t30.EndDate AS DropDate      ")
            .Append("       Case(t20.IsExternship) When 1 then (select max(AttendedDate) from arExternshipAttendance where arExternshipAttendance.StuEnrollId = t10.StuEnrollId) else t30.EndDate end AS DateIssue, t30.EndDate AS DropDate ")
            'Added By Vijay Ramteke on Feb 16, 2010
            .Append("       , t20.Hours As ScheduledHours ")
            'Added By Vijay Ramteke in Feb 16, 2010
            ''New Code Added By Vijay Ramteke For Rally Id 1393 on December 22, 2010
            .Append("       , (Select IsTransferGrade from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsTransferGrade ")
            .Append("       , t10.StuEnrollId  ")
            ''New Code Added By Vijay Ramteke For Rally Id 1393 on December 22, 2010
            .Append("FROM   arTransferGrades t10, arReqs t20, arTerm t30 ")
            .Append("WHERE  t10.TermId = t30.TermId")
            '.Append("       AND t10.ReqId = t20.ReqId ")
            '------rolling back this change to fix this issue 15277: 2.0.0: Data on the transcript page is incorrect for a numeric school.  - Theresa
            .Append("       AND t10.ReqId = t20.ReqId ")
            .Append("       AND t20.IsAttendanceOnly = 0 and ")
            'code added by balaji on 4/08/2009 to fix issue with credits per service mantis:15477 
            'modification starts here
            .Append(" (t10.Score is not null or (t10.score is null and t10.isClinicsSatisfied = 1) ")
            If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                .Append(" or (t10.score is null and  (t10.isClinicsSatisfied = 0 or t10.IsClinicsSatisfied is NULL)  and (select count(*) from argrdBkResults where StuEnrollId = t10.StuEnrollId and ClsSectionId in (select Distinct ClsSectionId from arClassSections where ReqId = t10.ReqId and TermId = t10.TermId) and Score is not null) >=1) ")
            End If
            .Append(" ) ")
            'modification ends here
            .Append("       AND t10.StuEnrollId = ? ")
            If myAdvAppSettings.AppSettings("SchedulingMethod", campusId) <> "ModuleStart" Then
                If Not (termEndDate = #1/1/1001#) Then
                    .Append("AND t30.EndDate <= ? ")
                End If
            End If
            'Code added by Vijay Ramteke on May, 07 2009
            If termCond <> "" Then
                .Append(" " & termCond.ToLower.Replace("arterm", "t30") & " ")
            End If
            If classCond <> "" Then
                .Append(" " & classCond.ToLower.Replace("arclasssections", "t30") & " ")
            End If
            'Code added by Vijay Ramteke on May, 07 2009
            .Append(") AS R  ")
            .Append(" LEFT OUTER JOIN arTermEnrollSummary AS ATES ON R.TermId = ATES.TermId AND R.StuEnrollId = ATES.StuEnrollId ")
            .Append(" ORDER BY StartDate desc, EndDate, TermDescrip")
        End With
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If myAdvAppSettings.AppSettings("SchedulingMethod", campusId) <> "ModuleStart" Then
            If Not (termEndDate = #1/1/1001#) Then
                db.AddParameter("@EndDate1", termEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
        End If
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If myAdvAppSettings.AppSettings("SchedulingMethod", campusId) <> "ModuleStart" Then
            If Not (termEndDate = #1/1/1001#) Then
                db.AddParameter("@EndDate", termEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
        End If

        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "Results")
            Dim stuStartDate As String
            Dim aRows() As DataRow
            'modified by Theresa G on 5/29/09 for mantis 16376: 2.2.0: Course is shown in the completed section even before the results are transferred. 
            '----------------------------------------------------------------------------------------------------------------------------------------
            Dim labCount As Integer
            Dim labCountAttempted As Integer
            For Each row As DataRow In ds.Tables("Results").Rows
                'labCount = 0
                'labCountAttempted = 0
                If myAdvAppSettings.AppSettings("SchedulingMethod", campusId) = "ModuleStart" Then
                    If row("TestId").ToString <> "00000000-0000-0000-0000-000000000000" Then
                        If (IsDuplicateClassSection(row("TestId").ToString) = True) Then
                            stuStartDate = GetStudentStartDate(StuEnrollId)
                            aRows = ds.Tables("Results").Select("TestId='" & row("TestId").ToString & "' AND StartDate=#" & CDate(stuStartDate) & "#")
                            If aRows.Length > 0 Then
                                If (stuStartDate <> CDate(row("StartDate")).ToShortDateString) Then
                                    row.Delete()
                                End If
                            Else
                                'aRows = ds.Tables(0).Select("TestId='" & row("TestId").ToString & "'")
                                aRows = ds.Tables("Results").Select("TestId='" & row("TestId").ToString & "' AND StartDate <> #" & row("StartDate") & "#")
                                If (aRows.Length > 0) Then
                                    row.Delete()
                                End If
                            End If
                        End If
                    End If
                End If
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    labCount = (New TransferGradeDB).LabWorkOrLabHourCourseCount(row("ReqId").ToString)
                    If labCount <= 0 And row("GrdSysDetailId").ToString = "" Then row.Delete()
                    If labCount > 0 Then
                        labCountAttempted = (New ExamsDB).GetClinicServicesAndHoursAttemptedByStudent(StuEnrollId, row("Reqid").ToString)
                        If labCountAttempted <= 0 Then row.Delete()
                    End If

                End If

            Next
            ds.AcceptChanges()
            '----------------------------------------------------------------------------------------------------------------------------------------

            sb.Remove(0, sb.Length)
            db.ClearParameters()
        Catch ex As Exception
            Throw ex
        End Try

        'Get the direct children for all groups used in the program version irregardless of level
        With sb
            .Append("SELECT rg.GrpId,rg.ReqId,ar.Descrip as Req,ar.Code as Code,ar.Credits,ar.ReqTypeId,rg.ReqSeq,ar.Hours,stu.PrgVerId,ar.FinAidCredits,stu.StuEnrollid      ")
            'Added By Vijay Ramteke on Feb 16, 2010
            .Append("       , ar.Hours As ScheduledHours   ")
            'Added By Vijay Ramteke in Feb 16, 2010
            .Append("FROM arReqGrpDef rg, arReqs ar, arStuEnrollments stu ")
            .Append("WHERE rg.ReqId=ar.ReqId and ar.IsAttendanceOnly = 0 and stu.StuEnrollId = ? ")
            .Append("AND EXISTS( ")
            .Append("SELECT distinct t4.ReqId ")
            .Append("FROM arReqs t4 ")
            .Append("WHERE t4.ReqTypeId=2 and t4.IsAttendanceOnly = 0 ")
            .Append("AND t4.ReqId=rg.GrpId ")
            .Append("AND EXISTS( ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t400.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t400.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t700.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t700.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append(" SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t800.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t800.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t900.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t900.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t1000.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t1000.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t1100.ReqId as ReqId, t3.Descrip As Req ")
            .Append(" FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000, arReqGrpDef t1100 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t1000.ReqId = t1100.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t1100.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append(")) ")
            .Append("ORDER BY rg.ReqSeq ")


        End With
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "GroupChildren")
            sb.Remove(0, sb.Length)
            db.ClearParameters()
        Catch ex As Exception
            Throw ex
        End Try

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function


    Public Function GetGraduateAuditByEnrollmentForNumericForCourseEquivalent(ByVal StuEnrollId As String, Optional ByVal termEndDate As DateTime = #1/1/1001#, Optional ByVal termCond As String = "", Optional ByVal classCond As String = "", Optional ByVal campusId As String = "") As DataSet
        ' New Optional Parameter add termCond and classCond by Vijay Ramteke on May, 06 2009
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim sb As New StringBuilder
        'Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        'Get the direct children of the prog version associated with the enrollment passed in
        With sb
            .Append("SELECT distinct t400.ReqId as ReqId,t3.Descrip As Req,t3.Code As Code,t3.Credits, t3.Hours, t3.ReqTypeId, ")
            .Append("t400.Credits as DefCredits, t600.Credits as ProgCredits,t600.Hours as ProgHours,")
            .Append("t600.PrgVerDescrip,t400.ReqSeq,t600.IsContinuingEd,t600.PrgVerId,t3.FinAidCredits,t100.StuEnrollId  ")
            'Added By Vijay Ramteke on Feb 16, 2010
            .Append("       , t3.Hours As ScheduledHours   ")
            'Added By Vijay Ramteke in Feb 16, 2010
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600 ")
            '.Append(" ,syStatuses t700 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.IsAttendanceOnly = 0 AND ")
            ' .Append("t3.StatusId = t700.StatusId AND ")
            '.Append("t700.StatusId='" & strActiveGUID & "'" & " AND ")
            .Append("t100.StuEnrollId =  ? ")
            .Append("ORDER BY t400.ReqSeq ")
        End With

        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "DirectChildren")
            sb.Remove(0, sb.Length)
            db.ClearParameters()
        Catch ex As Exception
            Throw ex
        End Try

        'Get the results for classes in the prog version for the enrollment passed in.
        With sb
            .Append("select R.TermId , R.TermDescrip ")
            .Append(", ISNULL(ATES.DescripXTranscript, '') AS DescripXTranscript ")
            .Append(", R.ReqId, R.Code, R.Descrip, R.Credits, R.Hours, R.CourseCategoryId, R.StartDate, R.EndDate, R.ClassStartDate, R.ClassEndDate ")
            .Append(", R.GrdSysDetailId, R.TestId, R.ResultId, R.Grade, R.IsPass, R.GPA, R.IsCreditsAttempted, R.IsCreditsEarned, R.IsInGPA, R.IsInSAP, R.IsDrop ")
            .Append(", R.CourseCategory, R.Score, R.FinAidCredits, R.DateIssue, R.DropDate, R.ScheduledHours, R.IsTransferGrade  ")
            .Append("FROM (    ")
            .Append("SELECT DISTINCT ")
            .Append("       t4.TermId, t3.TermDescrip, ")
            .Append("       t4.ReqId, t2.Code, t2.Descrip AS Descrip, ")
            .Append("       t2.Credits, t2.Hours, t2.CourseCategoryId, t3.StartDate AS StartDate, t3.EndDate AS EndDate, t4.StartDate as ClassStartDate, t4.EndDate  as ClassEndDate, ")
            .Append("       t1.GrdSysDetailId, t1.TestId, t1.ResultId, ")
            .Append("       (Select Grade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId)as Grade, ")
            .Append("       (Select IsPass from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsPass, ")
            .Append("       (Select isnull(GPA,0) from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as GPA,       ")
            .Append("       Case when t1.GrdSysDetailId is NULL Then 1 else (Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) end as IsCreditsAttempted, ")
            .Append("       Case when t1.GrdSysDetailId is NULL Then 1 else (Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) end as IsCreditsEarned,  ")
            .Append("       ISNULL((Select IsInGPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId),0) as IsInGPA, ")
            .Append("       ISNULL((Select IsInSAP from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId),0) as IsInSAP, ")
            .Append("       ISNULL((Select IsDrop from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId),0) as IsDrop, ")
            .Append("       (Select Top 1 Descrip from arCourseCategories where CourseCategoryId = t2.CourseCategoryId) as CourseCategory, t1.Score, t2.FinAidCredits, ")
            'Code Added By Vijay Ramteke on May, 11 2009
            '.Append("       t4.EndDate AS DateIssue ,t1.DateDetermined as DropDate        ")
            .Append("       Case(t2.IsExternship) When 1 then (select max(AttendedDate) from arExternshipAttendance where arExternshipAttendance.StuEnrollId = t1.StuEnrollId) else t4.EndDate end AS DateIssue, t1.DateDetermined as DropDate ")
            'Code Added By Vijay Ramteke on May, 11 2009
            'Added By Vijay Ramteke on Feb 16, 2010
            .Append("       , t2.Hours As ScheduledHours ")
            'Added By Vijay Ramteke in Feb 16, 2010
            ''New Code Added By Vijay Ramteke For Rally Id 1393 on December 22, 2010
            .Append("       , (Select IsTransferGrade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsTransferGrade ")
            .Append("       , t1.StuEnrollId  ")
            ''New Code Added By Vijay Ramteke For Rally Id 1393 on December 22, 2010
            .Append("FROM   arResults t1, arReqs t2, arTerm t3, arClassSections t4, arClassSectionTerms t5, arStuEnrollments t6 ")
            .Append("WHERE  t1.TestId = t4.ClsSectionId ")
            .Append("       AND t4.ClsSectionId = t5.ClsSectionId ")
            .Append("       AND t5.TermId = t3.TermId ")
            .Append("       AND t4.ReqId = t2.ReqId ")
            .Append("       AND t1.StuEnrollId = t6.StuEnrollId ")
            .Append("       AND t2.IsAttendanceOnly = 0 ")
            '------rolling back this change to fix this issue 15277: 2.0.0: Data on the transcript page is incorrect for a numeric school.  - Theresa
            'code added by balaji on 4/08/2009 to fix issue with credits per service mantis:15477 
            'modification starts here
            .Append("      AND (t1.Score is not null or (t1.score is null and t1.isClinicsSatisfied=1) ")
            If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                .Append(" or (t1.score is null and  (t1.isClinicsSatisfied = 0 or t1.IsClinicsSatisfied is NULL)  and (select count(*) from argrdBkResults where StuEnrollId = t1.StuEnrollId and ClsSectionId=t1.TestId and Score is not null) >=1) ")
            End If
            .Append(" ) ")
            'modification ends here
            '.Append("       AND (t1.GrdSysDetailId is not null  or t1.Score,t2.FinAidCredits   is not null) ")
            .Append("       AND t1.StuEnrollId = ? ")
            If myAdvAppSettings.AppSettings("SchedulingMethod", campusId) <> "ModuleStart" Then
                If Not (termEndDate = #1/1/1001#) Then
                    .Append("AND t3.EndDate <= ? ")
                End If
                'Code added by Vijay Ramteke on May, 07 2009
                If termCond <> "" Then
                    .Append(" " & termCond.ToLower.Replace("arterm", "t3") & " ")
                End If
                If classCond <> "" Then
                    .Append(" " & classCond.ToLower.Replace("arclasssections", "t4") & " ")
                End If
                'Code added by Vijay Ramteke on May, 07 2009
            End If
            .Append("UNION ")
            .Append("SELECT DISTINCT ")
            .Append("       t10.TermId,t30.TermDescrip, ")
            .Append("       t10.ReqId, t20.Code, t20.Descrip AS Descrip, ")
            .Append("       t20.Credits, t20.Hours, t20.CourseCategoryId, t30.StartDate AS StartDate, t30.EndDate AS EndDate, '1/1/1900' as ClassStartDate, '1/1/1900' as ClassEndDate, ")
            .Append("       t10.GrdSysDetailId, '{00000000-0000-0000-0000-000000000000}' AS TestId, t10.TransferId AS ResultId, ")
            .Append("       (Select Grade from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId)as Grade,  ")
            .Append("       (Select IsPass from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsPass, ")
            .Append("       (Select isnull(GPA,0) from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as GPA, ")
            .Append("       Case when t10.GrdSysDetailId is NULL Then 1 else (Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) end as IsCreditsAttempted, ")
            .Append("       Case when t10.GrdSysDetailId is NULL Then 1 else (Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) end as IsCreditsEarned,  ")
            .Append("       ISNULL((Select IsInGPA from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId),0) as IsInGPA, ")
            .Append("       ISNULL((Select IsInSAP from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId),0) as IsInSAP, ")
            .Append("       ISNULL((Select IsDrop from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId),0) as IsDrop, ")
            .Append("       (Select Top 1 Descrip from arCourseCategories where CourseCategoryId = t20.CourseCategoryId) as CourseCategory, t10.Score, t20.FinAidCredits,     ")
            .Append("       Case(t20.IsExternship) When 1 then (select max(AttendedDate) from arExternshipAttendance where arExternshipAttendance.StuEnrollId = t10.StuEnrollId) else t30.EndDate end AS DateIssue, t30.EndDate AS DropDate ")
            .Append("       , t20.Hours As ScheduledHours  ")
            .Append("       , (Select IsTransferGrade from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsTransferGrade ")
            .Append("       , t10.StuEnrollId  ")
            .Append("FROM   arTransferGrades t10, arReqs t20, arTerm t30 ")
            .Append("WHERE  t10.TermId = t30.TermId")
            '------rolling back this change to fix this issue 15277: 2.0.0: Data on the transcript page is incorrect for a numeric school.  - Theresa
            .Append("       AND t10.ReqId = t20.ReqId and t20.IsAttendanceOnly = 0  and  ")
            .Append(" (t10.Score is not null or (t10.score is null and t10.isClinicsSatisfied=1) ")
            If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                .Append(" or (t10.score is null and  (t10.isClinicsSatisfied=0 or t10.IsClinicsSatisfied is NULL)  and (select count(*) from argrdBkResults where StuEnrollId=t10.StuEnrollId and ClsSectionId in (select Distinct ClsSectionId from arClassSections where ReqId=t10.ReqId and TermId=t10.TermId) and Score is not null) >=1) ")
            End If
            .Append(" ) ")
            'modification ends here
            .Append("       AND t10.StuEnrollId = ? ")
            If termCond <> "" Then
                .Append(" " & termCond.ToLower.Replace("arterm", "t30") & " ")
            End If
            If classCond <> "" Then
                .Append(" " & classCond.ToLower.Replace("arclasssections", "t30") & " ")
            End If
            .Append("UNION ")
            .Append("SELECT DISTINCT ")
            .Append("       t4.TermId, t3.TermDescrip, ")
            .Append("       S.ReqId, t2.Code, t2.Descrip AS Descrip,   ")
            .Append("       t2.Credits, t2.Hours, t2.CourseCategoryId, t3.StartDate AS StartDate, t3.EndDate AS EndDate, t4.StartDate as ClassStartDate, t4.EndDate  as ClassEndDate,  ")
            .Append("       t1.GrdSysDetailId, t1.TestId, t1.ResultId, ")
            .Append("       (Select Top 1 Grade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId)as Grade, ")
            .Append("       (Select Top 1 IsPass from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsPass, ")
            .Append("       (Select Top 1 isnull(GPA,0) from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as GPA, ")
            .Append("       Case when t1.GrdSysDetailId is NULL Then 1 else (Select Top 1 IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) end as IsCreditsAttempted, ")
            .Append("       Case when t1.GrdSysDetailId is NULL Then 1 else (Select Top 1 IsCreditsEarned from arGradeSystemDetails  where GrdSysDetailId = t1.GrdSysDetailId) end as IsCreditsEarned, ")
            .Append("       ISNULL((Select Top 1 IsInGPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId),0) as IsInGPA, ")
            .Append("       ISNULL((Select IsInSAP from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId),0) as IsInSAP, ")
            .Append("       ISNULL((Select Top 1 IsDrop from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId),0) as IsDrop, ")
            .Append("       (Select Top 1 Descrip from arCourseCategories where CourseCategoryId = t2.CourseCategoryId) as CourseCategory, t1.Score, t2.FinAidCredits, ")
            .Append("       Case(t2.IsExternship) When 1 then (select max(AttendedDate) from arExternshipAttendance where arExternshipAttendance.StuEnrollId = t1.StuEnrollId) else t4.EndDate end AS DateIssue, t1.DateDetermined as DropDate  ")
            .Append("       , t2.Hours As ScheduledHours   ")
            .Append("       , (Select Top 1 IsTransferGrade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsTransferGrade ")
            .Append("       , t1.StuEnrollId  ")
            .Append("FROM ")
            .Append("    (SELECT distinct ")
            .Append("            t700.ReqId as EqReqId, t3.ReqId, t100.StuEnrollId  ")
            .Append("     FROM arStuEnrollments t100, arReqs t3, arProgVerDef t400, adLeads t500, arPrgVersions t600, arCourseEquivalent t700  ")
            .Append("     WHERE(t100.StudentId = t500.StudentId) ")
            .Append("       AND t3.ReqId = t400.ReqId AND t100.PrgVerId = t400.PrgVerId AND t100.PrgVerId = t600.PrgVerId ")
            .Append("       AND t3.Reqid = t700.EquivReqId AND t100.StuEnrollId =  ? ")
            .Append("       AND t3.IsAttendanceOnly = 0 ")
            .Append("       AND t3.Reqid not in ")
            .Append("                          (select ReqId from arResults a ,arClassSections b where a.TestId = b.ClsSectionId ")
            .Append("                                        AND StuEnrollId =  ? and ReqId=t400.ReqId) ")
            .Append("    ) AS S, arResults t1, arReqs t2, arTerm t3, arClassSections t4, arClassSectionTerms t5, arStuEnrollments t6 ")
            .Append("WHERE  t1.TestId = t4.ClsSectionId  ")
            .Append("  AND t4.ClsSectionId = t5.ClsSectionId ")
            .Append("  AND t5.TermId = t3.TermId ")
            .Append("  AND t4.ReqId = t2.ReqId ")
            .Append("  AND t1.StuEnrollId = t6.StuEnrollId And t1.GrdSysDetailId Is Not null And t1.StuEnrollId = S.StuEnrollId   ")
            .Append("  AND (t1.Score is not null or (t1.score is null and t1.isClinicsSatisfied = 1) ")
            If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                .Append(" or (t1.score is null and  (t1.isClinicsSatisfied = 0 or t1.IsClinicsSatisfied is NULL)  and (select count(*) from argrdBkResults where StuEnrollId = t1.StuEnrollId and ClsSectionId = t1.TestId and Score is not null) >=1) ")
            End If

            .Append(" ) ")
            'modification ends here
            .Append(" and t2.Reqid = S.EqReqId ")
            If myAdvAppSettings.AppSettings("SchedulingMethod", campusId) <> "ModuleStart" Then
                If Not (termEndDate = #1/1/1001#) Then
                    .Append("AND t3.EndDate <= ? ")
                End If
            End If
            'Code added by Vijay Ramteke on May, 07 2009
            If termCond <> "" Then
                .Append(" " & termCond.ToLower.Replace("arterm", "t3") & " ")
            End If
            If classCond <> "" Then
                .Append(" " & classCond.ToLower.Replace("arclasssections", "t4") & " ")
            End If
            'Code added by Vijay Ramteke on May, 07 2009
            .Append(") AS R  ")
            .Append(" LEFT OUTER JOIN arTermEnrollSummary AS ATES ON R.TermId = ATES.TermId AND R.StuEnrollId = ATES.StuEnrollId ")
            .Append(" ORDER BY StartDate desc, EndDate, TermDescrip")
        End With
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If myAdvAppSettings.AppSettings("SchedulingMethod", campusId) <> "ModuleStart" Then
            If Not (termEndDate = #1/1/1001#) Then
                db.AddParameter("@EndDate1", termEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
        End If
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If myAdvAppSettings.AppSettings("SchedulingMethod", campusId) <> "ModuleStart" Then
            If Not (termEndDate = #1/1/1001#) Then
                db.AddParameter("@EndDate", termEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
        End If
        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "Results")
            Dim stuStartDate As String
            Dim aRows() As DataRow
            'modified by Theresa G on 5/29/09 for mantis 16376: 2.2.0: Course is shown in the completed section even before the results are transferred. 
            '----------------------------------------------------------------------------------------------------------------------------------------
            Dim labCount As Integer
            Dim labCountAttempted As Integer
            For Each row As DataRow In ds.Tables("Results").Rows

                If myAdvAppSettings.AppSettings("SchedulingMethod", campusId) = "ModuleStart" Then
                    If row("TestId").ToString <> "00000000-0000-0000-0000-000000000000" Then
                        If (IsDuplicateClassSection(row("TestId").ToString) = True) Then
                            stuStartDate = GetStudentStartDate(StuEnrollId)
                            aRows = ds.Tables("Results").Select("TestId='" & row("TestId").ToString & "' AND StartDate=#" & CDate(stuStartDate) & "#")
                            If aRows.Length > 0 Then
                                If (stuStartDate <> CDate(row("StartDate")).ToShortDateString) Then
                                    row.Delete()
                                End If
                            Else
                                'aRows = ds.Tables(0).Select("TestId='" & row("TestId").ToString & "'")
                                aRows = ds.Tables("Results").Select("TestId='" & row("TestId").ToString & "' AND StartDate <> #" & row("StartDate") & "#")
                                If (aRows.Length > 0) Then
                                    row.Delete()
                                End If
                            End If
                        End If
                    End If
                End If
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    labCount = (New TransferGradeDB).LabWorkOrLabHourCourseCount(row("ReqId").ToString)
                    If labCount <= 0 And row("GrdSysDetailId").ToString = "" Then row.Delete()
                    If labCount > 0 Then
                        labCountAttempted = (New ExamsDB).GetClinicServicesAndHoursAttemptedByStudent(StuEnrollId, row("Reqid").ToString)
                        If labCountAttempted <= 0 Then row.Delete()
                    End If

                End If

            Next
            ds.AcceptChanges()
            '----------------------------------------------------------------------------------------------------------------------------------------

            sb.Remove(0, sb.Length)
            db.ClearParameters()
        Catch ex As Exception
            Throw ex
        End Try

        'Get the direct children for all groups used in the program version irregardless of level
        With sb
            .Append("SELECT rg.GrpId,rg.ReqId,ar.Descrip as Req,ar.Code as Code,ar.Credits,ar.ReqTypeId,rg.ReqSeq,ar.Hours,stu.PrgVerId,ar.FinAidCredits,stu.StuEnrollid      ")
            'Added By Vijay Ramteke on Feb 16, 2010
            .Append("       , ar.Hours As ScheduledHours   ")
            'Added By Vijay Ramteke in Feb 16, 2010
            .Append("FROM arReqGrpDef rg, arReqs ar, arStuEnrollments stu ")
            .Append("WHERE rg.ReqId=ar.ReqId and ar.IsAttendanceOnly = 0 and stu.StuEnrollId = ? ")
            .Append("AND EXISTS( ")
            .Append("SELECT distinct t4.ReqId ")
            .Append("FROM arReqs t4 ")
            .Append("WHERE t4.ReqTypeId=2 ")
            .Append("AND t4.ReqId=rg.GrpId ")
            .Append("AND t4.IsAttendanceOnly = 0 ")
            .Append("AND EXISTS( ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t400.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t400.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t700.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t700.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append(" SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t800.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t800.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t900.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t900.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t1000.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t1000.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t1100.ReqId as ReqId, t3.Descrip As Req ")
            .Append(" FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000, arReqGrpDef t1100 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t1000.ReqId = t1100.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t1100.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append(")) ")
            .Append("ORDER BY rg.ReqSeq ")


        End With
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "GroupChildren")
            sb.Remove(0, sb.Length)
            db.ClearParameters()
        Catch ex As Exception
            Throw ex
        End Try

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Private Function GetTranscript(ByVal StuEnrollId As String, ByVal termCond As String, ByVal classCond As String) As DataSet
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        If termCond = String.Empty Then
            db.AddParameter("@termId", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        Else
            If termCond.Contains("AND  arterm.termid =") Then
                termCond = termCond.Replace("AND  arterm.termid =", "").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("'", "")
            Else
                termCond = termCond.Replace("AND  arterm.termid in", "").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("'", "")
            End If
            db.AddParameter("@termId", termCond, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        End If
        If classCond = String.Empty Then
            db.AddParameter("@clsStartDate", "1/1/1900", SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@clsEndDate", "1/1/2100", SqlDbType.DateTime, , ParameterDirection.Input)
        Else
            Dim strDate As String = classCond.Replace("AND  arclasssections.startdate>=", "").Replace("AND  arclasssections.enddate<=", ",").Replace(" ", "").Replace("'", "")
            Dim strArr() As String
            strArr = strDate.Split(",")
            db.AddParameter("@clsStartDate", CDate(strArr(0)), SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@clsEndDate", CDate(strArr(1)), SqlDbType.DateTime, , ParameterDirection.Input)
        End If
        If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GetTranscript_addcreditsbyservice")
        Else
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GetTranscript")
        End If

        Try
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function

    Private Function GetTranscriptCeNumeric(ByVal StuEnrollId As String, ByVal termCond As String, ByVal classCond As String) As DataSet
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        If termCond = String.Empty Then
            db.AddParameter("@termId", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        Else
            If termCond.Contains("AND  arterm.termid =") Then
                termCond = termCond.Replace("AND  arterm.termid =", "").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("'", "")
            Else
                termCond = termCond.Replace("AND  arterm.termid in", "").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("'", "")
            End If
            db.AddParameter("@termId", termCond, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        End If
        If classCond = String.Empty Then
            db.AddParameter("@clsStartDate", "1/1/1900", SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@clsEndDate", "1/1/2100", SqlDbType.DateTime, , ParameterDirection.Input)
        Else
            Dim strDate As String = classCond.Replace("AND  arclasssections.startdate>=", "").Replace("AND  arclasssections.enddate<=", ",").Replace(" ", "").Replace("'", "")
            Dim strArr() As String
            strArr = strDate.Split(",")
            db.AddParameter("@clsStartDate", CDate(strArr(0)), SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@clsEndDate", CDate(strArr(1)), SqlDbType.DateTime, , ParameterDirection.Input)
        End If
        If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GetTranscriptCENumeric_addcreditsbyservice")
        Else
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GetTranscriptCENumeric")
        End If

        Try
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function

    Private Function GetTranscriptCELetter(ByVal StuEnrollId As String, ByVal termCond As String, ByVal classCond As String) As DataSet
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        If termCond = String.Empty Then
            db.AddParameter("@termId", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        Else
            If termCond.Contains("AND  arterm.termid =") Then
                termCond = termCond.Replace("AND  arterm.termid =", "").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("'", "")
            Else
                termCond = termCond.Replace("AND  arterm.termid in", "").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("'", "")
            End If
            db.AddParameter("@termId", termCond, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        End If
        If classCond = String.Empty Then
            db.AddParameter("@clsStartDate", "1/1/1900", SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@clsEndDate", "1/1/2100", SqlDbType.DateTime, , ParameterDirection.Input)
        Else
            Dim strDate As String = classCond.Replace("AND  arclasssections.startdate>=", "").Replace("AND  arclasssections.enddate<=", ",").Replace(" ", "").Replace("'", "")
            Dim strArr() As String
            strArr = strDate.Split(",")
            db.AddParameter("@clsStartDate", CDate(strArr(0)), SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@clsEndDate", CDate(strArr(1)), SqlDbType.DateTime, , ParameterDirection.Input)
        End If
        If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GetTranscriptCELetter_addcreditsbyservice")
        Else
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GetTranscriptCELetter")
        End If

        Try
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Private Function GetTranscriptLetter(ByVal StuEnrollId As String, ByVal termCond As String, ByVal classCond As String) As DataSet
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        If termCond = String.Empty Then
            db.AddParameter("@termId", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        Else
            If termCond.Contains("AND  arterm.termid =") Then
                termCond = termCond.Replace("AND  arterm.termid =", "").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("'", "")
            Else
                termCond = termCond.Replace("AND  arterm.termid in", "").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("'", "")
            End If
            db.AddParameter("@termId", termCond, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        End If
        If classCond = String.Empty Then
            db.AddParameter("@clsStartDate", "1/1/1900", SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@clsEndDate", "1/1/2100", SqlDbType.DateTime, , ParameterDirection.Input)
        Else
            Dim strDate As String = classCond.Replace("AND  arclasssections.startdate>=", "").Replace("AND  arclasssections.enddate<=", ",").Replace(" ", "").Replace("'", "")
            Dim strArr() As String
            strArr = strDate.Split(",")
            db.AddParameter("@clsStartDate", CDate(strArr(0)), SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@clsEndDate", CDate(strArr(1)), SqlDbType.DateTime, , ParameterDirection.Input)
        End If
        If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GetTranscriptLetter_addcreditsbyservice")
        Else
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GetTranscriptLetter")
        End If

        Try
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function GetDTResultsLetter(ByVal StuEnrollId As String, ByVal termCond As String, ByVal classCond As String, Optional ByVal Campusid As String = "") As DataTable
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        If termCond = String.Empty Then
            db.AddParameter("@termId", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        Else
            If termCond.Contains("AND  arterm.termid =") Then
                termCond = termCond.Replace("AND  arterm.termid =", "").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("'", "")
            Else
                termCond = termCond.Replace("AND  arterm.termid in", "").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("'", "")
            End If
            db.AddParameter("@termId", termCond, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        End If
        If classCond = String.Empty Then
            db.AddParameter("@clsStartDate", "1/1/1900", SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@clsEndDate", "1/1/2100", SqlDbType.DateTime, , ParameterDirection.Input)
        Else
            Dim strDate As String = classCond.Replace("AND  arclasssections.startdate>=", "").Replace("AND  arclasssections.enddate<=", ",").Replace(" ", "").Replace("'", "")
            Dim strArr() As String
            strArr = strDate.Split(",")
            db.AddParameter("@clsStartDate", CDate(strArr(0)), SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@clsEndDate", CDate(strArr(1)), SqlDbType.DateTime, , ParameterDirection.Input)
        End If
        If myAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GetDTResultsForTranscriptNumeric", "Results")
            ' If (ds.Tables.Count = 0 OrElse IsNothing(ds.Tables("Results"))) Then
            '    Throw New ApplicationException("dbo.USP_GetDTResultsForTranscriptNumeric does not return the expected table")
            'End If
        Else
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GetDTResultsForTranscriptLetter", "Results")
        End If


        Try

            'Dim stuStartDate As String = String.Empty
            'Dim aRows() As DataRow
            'modified by Theresa G on 5/29/09 for mantis 16376: 2.2.0: Course is shown in the completed section even before the results are transferred. 
            '----------------------------------------------------------------------------------------------------------------------------------------
            Dim labCount As Integer
            Dim labCountAttempted As Integer

            If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" AndAlso ds.Tables.Count > 0 AndAlso ds.Tables("Results").Rows.Count >= 1 Then
                For Each row As DataRow In ds.Tables("Results").Rows
                    'labCount = 0
                    'labCountAttempted = 0

                    labCount = (New TransferGradeDB).LabWorkOrLabHourCourseCount_SP(row("ReqId").ToString)
                    If labCount <= 0 And row("GrdSysDetailId").ToString = "" Then row.Delete()
                    If labCount > 0 Then
                        labCountAttempted = (New ExamsDB).GetClinicServicesAndHoursAttemptedByStudent_SP(StuEnrollId, row("Reqid").ToString)
                        If labCountAttempted <= 0 Then row.Delete()
                    End If
                Next
            End If
            ds.AcceptChanges()
            '----------------------------------------------------------------------------------------------------------------------------------------
            Return ds.Tables(0)
        Finally
            db.CloseConnection()
        End Try



    End Function

    Public Function GrdOverRide_SP(ByVal PrgVerId As String, ByVal Reqid As String, ByVal sStdGrade As String) As Boolean
        Dim rtn As Boolean
        Dim db As New SQLDataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@reqId", New Guid(Reqid), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@prgVerId", New Guid(PrgVerId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@grade", sStdGrade, SqlDbType.VarChar, 100, ParameterDirection.Input)
        Try
            rtn = CType(db.RunParamSQLScalar_SP("dbo.usp_DoesPrgVerHaveGrdOverride"), Boolean)
            Return rtn
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try

    End Function

    Public Function GetEnrollmentCanChangeGrade(ByVal stuEnrollId As String, ByVal userId As String) As Boolean
        Dim rtn As Boolean
        Dim db As New SQLDataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@StuEnrollId", stuEnrollId, SqlDbType.VarChar, , ParameterDirection.Input)
        db.AddParameter("@UserId", userId, SqlDbType.VarChar, , ParameterDirection.Input)

        Try
            rtn = CType(db.RunParamSQLScalar_SP("dbo.GetEnrollmentChangeGradeEnabled"), Boolean)
            Return rtn
        Catch ex As Exception
            Return False
        Finally
            db.CloseConnection()
        End Try

    End Function

    Public Function GetGraduateAuditByEnrollmentForNumericForCourseEquivalent_SP(ByVal StuEnrollId As String, Optional ByVal termEndDate As DateTime = #1/1/1001#, Optional ByVal termCond As String = "", Optional ByVal classCond As String = "") As DataSet

        Dim ds As DataSet

        ds = GetTranscript(StuEnrollId, termCond, classCond)
        ds.Tables(0).TableName = "DirectChildren"
        ds.Tables(1).TableName = "Results"
        ds.Tables(2).TableName = "Groupchildren"
        Try

            'Dim stuStartDate As String = String.Empty
            'Dim aRows() As DataRow
            'modified by Theresa G on 5/29/09 for mantis 16376: 2.2.0: Course is shown in the completed section even before the results are transferred. 
            '----------------------------------------------------------------------------------------------------------------------------------------
            Dim labCount As Integer
            Dim labCountAttempted As Integer
            If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                For Each row As DataRow In ds.Tables("Results").Rows

                    labCount = (New TransferGradeDB).LabWorkOrLabHourCourseCount_SP(row("ReqId").ToString)
                    If labCount <= 0 And row("GrdSysDetailId").ToString = "" Then row.Delete()
                    If labCount > 0 Then
                        labCountAttempted = (New ExamsDB).GetClinicServicesAndHoursAttemptedByStudent_SP(StuEnrollId, row("Reqid").ToString)
                        If labCountAttempted <= 0 Then row.Delete()
                    End If
                Next
            End If
            ds.AcceptChanges()
            '----------------------------------------------------------------------------------------------------------------------------------------


        Catch ex As Exception
            Throw ex
        End Try

        Return ds
    End Function
    Public Function GetGraduateAuditByCEProgEnrollment(ByVal StuEnrollId As String, Optional ByVal termEndDate As DateTime = #1/1/1001#, Optional ByVal termCond As String = "", Optional ByVal classCond As String = "", Optional ByVal Campusid As String = "") As DataSet
        ' New Optional Parameter add termCond and classCond by Vijay Ramteke on May, 06 2009
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim sb As New StringBuilder

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        'Special case:  Since Continuing Education (CE) programs do not have a definition,
        '               need to get from arResults and arTransferGrades only those courses the student has taken 
        '               or is currently registered for
        With sb
            .Append("SELECT ")
            .Append("       C.ReqId,R.Descrip AS Req,R.Code,R.Credits,R.Hours,")
            .Append("       R.ReqTypeId,0 AS DefCredits, 0 AS ProgCredits,")
            .Append("       0 AS ProgHours,PV.PrgVerDescrip,PV.IsContinuingEd,PV.PrgVerId,R.FinAidCredits    ")
            'Added By Vijay Ramteke on Feb 16, 2010
            .Append("       , R.Hours As ScheduledHours   ")
            'Added By Vijay Ramteke in Feb 16, 2010
            .Append("FROM   arResults T,arClassSections C,arReqs R,arStuEnrollments E,arPrgVersions PV ")
            .Append("WHERE  T.StuEnrollId=? AND T.TestId=C.ClsSectionId AND C.ReqId=R.ReqId ")
            .Append("       AND E.StuEnrollId=T.StuEnrollId AND PV.PrgVerId=E.PrgVerId ")
            .Append("       AND R.IsAttendanceOnly = 0 ")
            .Append("UNION ALL ")
            .Append("SELECT ")
            .Append("       G.ReqId,R.Descrip AS Req,R.Code,R.Credits,R.Hours,")
            .Append("       R.ReqTypeId,0 AS DefCredits, 0 AS ProgCredits,")
            .Append("       0 AS ProgHours,PV.PrgVerDescrip,PV.IsContinuingEd,PV.PrgVerId ,R.FinAidCredits    ")
            'Added By Vijay Ramteke on Feb 16, 2010
            .Append("       , R.Hours As ScheduledHours   ")
            'Added By Vijay Ramteke in Feb 16, 2010
            .Append("FROM   arTransferGrades G,arReqs R,arStuEnrollments E,arPrgVersions PV ")
            .Append("WHERE  G.StuEnrollId=?")
            .Append("       AND G.ReqId=R.ReqId AND E.StuEnrollId=G.StuEnrollId AND PV.PrgVerId=E.PrgVerId ")
            .Append("       AND R.IsAttendanceOnly = 0 ")
        End With

        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "DirectChildren")
            sb.Remove(0, sb.Length)
            db.ClearParameters()
        Catch ex As Exception
            Throw ex
        End Try


        'Get the results for classes regardless of the program version.
        With sb
            .Append("select R.TermId , R.TermDescrip ")
            .Append(", ISNULL(ATES.DescripXTranscript, '') AS DescripXTranscript ")
            .Append(", R.ReqId, R.Code, R.Descrip, R.Credits, R.Hours, R.CourseCategoryId, R.StartDate, R.EndDate, R.ClassStartDate, R.ClassEndDate ")
            .Append(", R.GrdSysDetailId, R.TestId, R.ResultId, R.Grade, R.IsPass, R.GPA, R.IsCreditsAttempted, R.IsCreditsEarned, R.IsInGPA, R.IsInSAP, R.IsDrop ")
            .Append(", R.CourseCategory, R.Score, R.FinAidCredits, R.DateIssue, R.DropDate, R.ScheduledHours, R.IsTransferGrade  ")
            .Append("FROM (    ")
            .Append("SELECT DISTINCT ")
            .Append("       t4.TermId,t3.TermDescrip, ")
            .Append("       t4.ReqId,t2.Code,t2.Descrip AS Descrip, ")
            .Append("       t2.Credits, t2.Hours, t2.CourseCategoryId, t3.StartDate AS StartDate ,t3.EndDate AS EndDate, t4.StartDate as ClassStartDate, t4.EndDate  as ClassEndDate, ")
            .Append("       t1.GrdSysDetailId, t1.TestId, t1.ResultId, ")
            .Append("       (Select Grade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId)as Grade, ")
            .Append("       (Select IsPass from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsPass, ")
            .Append("       (Select GPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as GPA, ")
            .Append("       (Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsAttempted, ")
            .Append("       (Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsEarned, ")
            .Append("       (Select IsInGPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInGPA, ")
            .Append("       (Select IsInSAP from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInSAP, ")
            .Append("       (Select IsDrop from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsDrop, ")
            .Append("       (Select Descrip from arCourseCategories where CourseCategoryId = t2.CourseCategoryId) as CourseCategory,t1.Score,t2.FinAidCredits, ")
            .Append("       Case(t2.IsExternship) When 1 then (select max(AttendedDate) from arExternshipAttendance where arExternshipAttendance.StuEnrollId = t1.StuEnrollId) else t4.EndDate end AS DateIssue, t1.DateDetermined as DropDate ")
            .Append("       , t2.Hours As ScheduledHours   ")
            .Append("       , (Select IsTransferGrade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsTransferGrade ")
            .Append("       , t1.StuEnrollId  ")
            '.Append("       ,(Select IsInSAP from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInSAP ")
            .Append("FROM   arResults t1, arReqs t2, arTerm t3, arClassSections t4, arClassSectionTerms t5 ")
            .Append("WHERE  t1.TestId = t4.ClsSectionId ")
            .Append("       AND t4.ClsSectionId = t5.ClsSectionId ")
            .Append("       AND t5.TermId = t3.TermId ")
            .Append("       AND t4.ReqId = t2.ReqId ")
            .Append("       AND t2.IsAttendanceOnly = 0 ")
            If Not myAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                .Append(" AND  ")
                'code added by balaji on 4/08/2009 to fix issue with credits per service mantis:15477 
                'modification starts here
                .Append(" (t1.GrdSysDetailId is not null or (t1.GrdSysDetailId is null and t1.isClinicsSatisfied = 1) ")
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    .Append(" or (t1.GrdSysDetailId is null  and  (t1.isClinicsSatisfied=0 or t1.IsClinicsSatisfied is NULL)   and (select count(*) from argrdBkResults where StuEnrollId = t1.StuEnrollId and ClsSectionid = t1.TestId and Score is not null) >= 1) ")
                End If
                .Append(" ) ")
                'ends here
            Else
                .Append(" AND ")
                'code added by balaji on 4/08/2009 to fix issue with credits per service mantis:15477 
                'modification starts here
                .Append(" (t1.Score is not null or (t1.score is null and t1.isClinicsSatisfied=1) ")
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    .Append(" or (t1.score is null and  (t1.isClinicsSatisfied=0 or t1.IsClinicsSatisfied is NULL)  and (select count(*) from argrdBkResults where StuEnrollId = t1.StuEnrollId and ClsSectionid = t1.TestId and Score is not null) >= 1) ")
                End If
                .Append(" ) ")
                'ends here
            End If
            .Append("       AND t1.StuEnrollId = ? ")
            If myAdvAppSettings.AppSettings("SchedulingMethod", Campusid) <> "ModuleStart" Then
                If Not (termEndDate = #1/1/1001#) Then
                    .Append("AND t3.EndDate <= ? ")
                End If
            End If
            'Code added by Vijay Ramteke on May, 07 2009
            If termCond <> "" Then
                .Append(" " & termCond.ToLower.Replace("arterm", "t3") & " ")
            End If
            If classCond <> "" Then
                .Append(" " & classCond.ToLower.Replace("arclasssections", "t4") & " ")
            End If
            'Code added by Vijay Ramteke on May, 07 2009
            .Append("UNION ")
            .Append("SELECT DISTINCT ")
            .Append("       t10.TermId, t30.TermDescrip, ")
            .Append("       t10.ReqId, t20.Code, t20.Descrip AS Descrip, ")
            .Append("       t20.Credits, t20.Hours, t20.CourseCategoryId, t30.StartDate AS StartDate, t30.EndDate AS EndDate, '1/1/1900' as ClassStartDate, '1/1/1900' as ClassEndDate, ")
            .Append("       t10.GrdSysDetailId, '{00000000-0000-0000-0000-000000000000}' AS TestId, t10.TransferId AS ResultId, ")
            .Append("       (Select Grade from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId)as Grade, ")
            .Append("       (Select IsPass from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsPass, ")
            .Append("       (Select GPA from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as GPA,")
            .Append("       (Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsCreditsAttempted, ")
            .Append("       (Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsCreditsEarned, ")
            .Append("       (Select IsInGPA from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsInGPA, ")
            .Append("       (Select IsInSAP from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsInSAP, ")
            .Append("       (Select IsDrop from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsDrop, ")
            .Append("       (Select Descrip from arCourseCategories where CourseCategoryId = t20.CourseCategoryId) as CourseCategory, t10.Score as Score, t20.FinAidCredits, ")
            'Code Added By Vijay Ramteke on May, 11 2009
            '.Append("       t30.EndDate AS DateIssue ,t30.EndDate AS DropDate     ")
            .Append("       Case(t20.IsExternship) When 1 then (select max(AttendedDate) from arExternshipAttendance where arExternshipAttendance.StuEnrollId = t10.StuEnrollId) else t30.EndDate end AS DateIssue, t30.EndDate AS DropDate ")
            'Code Added By Vijay Ramteke on May, 11 2009
            'Added By Vijay Ramteke on Feb 16, 2010
            .Append("       , t20.Hours As ScheduledHours ")
            'Added By Vijay Ramteke in Feb 16, 2010
            ''New Code Added By Vijay Ramteke For Rally Id 1393 on January 05, 2011
            .Append("       , (Select IsTransferGrade from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsTransferGrade ")
            .Append("       , t10.StuEnrollId  ")
            ''New Code Added By Vijay Ramteke For Rally Id 1393 on January 05, 2011
            .Append("FROM   arTransferGrades t10,arReqs t20,arTerm t30 ")
            .Append("WHERE  t10.TermId = t30.TermId ")
            .Append("       AND t10.ReqId = t20.ReqId ")
            .Append("       AND t20.IsAttendanceOnly = 0 ")
            If Not myAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                .Append(" AND  ")
                'code added by balaji on 4/08/2009 to fix issue with credits per service mantis:15477 
                'modification starts here
                .Append(" (t10.GrdSysDetailId is not null or (t10.GrdSysDetailId is null and t10.isClinicsSatisfied = 1) ")
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    .Append(" or (t10.GrdSysDetailId is null  and  (t10.isClinicsSatisfied=0 or t10.IsClinicsSatisfied is NULL)   and (select count(*) from argrdBkResults where StuEnrollId = t10.StuEnrollId and ClsSectionId in (select Distinct ClsSectionId from arClassSections where ReqId = t10.ReqId and TermId = t10.TermId) and Score is not null) >=1) ")
                End If
                .Append(" ) ")
                'ends here
            Else
                .Append(" AND ")
                'code added by balaji on 4/08/2009 to fix issue with credits per service mantis:15477 
                'modification starts here
                .Append(" (t10.Score is not null or (t10.score is null and t10.isClinicsSatisfied=1) ")
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    .Append(" or (t10.score is null and  (t10.isClinicsSatisfied = 0 or t10.IsClinicsSatisfied is NULL)  and (select count(*) from argrdBkResults where StuEnrollId = t10.StuEnrollId and ClsSectionId in (select Distinct ClsSectionId from arClassSections where ReqId = t10.ReqId and TermId =t 10.TermId) and Score is not null) >= 1) ")
                End If
                .Append(" ) ")
                'ends here
            End If
            .Append("       AND t10.StuEnrollId = ? ")
            If myAdvAppSettings.AppSettings("SchedulingMethod", Campusid) <> "ModuleStart" Then
                If Not (termEndDate = #1/1/1001#) Then
                    .Append("AND t30.EndDate <= ? ")
                End If
            End If
            'Code added by Vijay Ramteke on May, 07 2009
            If termCond <> "" Then
                .Append(" " & termCond.ToLower.Replace("arterm", "t30") & " ")
            End If
            If classCond <> "" Then
                .Append(" " & classCond.ToLower.Replace("arclasssections", "t30") & " ")
            End If
            'Code added by Vijay Ramteke on May, 07 2009
            .Append(") AS R  ")
            .Append(" LEFT OUTER JOIN arTermEnrollSummary AS ATES ON R.TermId = ATES.TermId AND R.StuEnrollId = ATES.StuEnrollId ")
            .Append(" ORDER BY StartDate desc, EndDate, TermDescrip")
        End With

        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If myAdvAppSettings.AppSettings("SchedulingMethod", Campusid) <> "ModuleStart" Then
            If Not (termEndDate = #1/1/1001#) Then
                db.AddParameter("@EndDate", termEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
        End If
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If myAdvAppSettings.AppSettings("SchedulingMethod", Campusid) <> "ModuleStart" Then
            If Not (termEndDate = #1/1/1001#) Then
                db.AddParameter("@EndDate", termEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
        End If

        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "Results")
            Dim stuStartDate As String
            Dim aRows() As DataRow
            'modified by Theresa G on 5/29/09 for mantis 16376: 2.2.0: Course is shown in the completed section even before the results are transferred. 
            '----------------------------------------------------------------------------------------------------------------------------------------
            Dim labCount As Integer
            Dim labCountAttempted As Integer
            For Each row As DataRow In ds.Tables("Results").Rows

                If myAdvAppSettings.AppSettings("SchedulingMethod", Campusid) = "ModuleStart" Then
                    If row("TestId").ToString <> "00000000-0000-0000-0000-000000000000" Then
                        If (IsDuplicateClassSection(row("TestId").ToString) = True) Then
                            stuStartDate = GetStudentStartDate(StuEnrollId)
                            aRows = ds.Tables("Results").Select("TestId='" & row("TestId").ToString & "' AND StartDate=#" & CDate(stuStartDate) & "#")
                            If aRows.Length > 0 Then
                                If (stuStartDate <> CDate(row("StartDate")).ToShortDateString) Then
                                    row.Delete()
                                End If
                            Else
                                'aRows = ds.Tables(0).Select("TestId='" & row("TestId").ToString & "'")
                                aRows = ds.Tables("Results").Select("TestId='" & row("TestId").ToString & "' AND StartDate <> #" & row("StartDate") & "#")
                                If (aRows.Length > 0) Then
                                    row.Delete()
                                End If
                            End If
                        End If
                    End If
                End If
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    labCount = (New TransferGradeDB).LabWorkOrLabHourCourseCount(row("ReqId").ToString)
                    If labCount <= 0 And row("GrdSysDetailId").ToString = "" Then row.Delete()
                    If labCount > 0 Then
                        labCountAttempted = (New ExamsDB).GetClinicServicesAndHoursAttemptedByStudent(StuEnrollId, row("Reqid").ToString)
                        If labCountAttempted <= 0 Then row.Delete()
                    End If

                End If

            Next
            ds.AcceptChanges()
            '----------------------------------------------------------------------------------------------------------------------------------------
            sb.Remove(0, sb.Length)
            db.ClearParameters()
        Catch ex As Exception
            Throw ex
        End Try


        'This table should always be empty because no course group should exist for CE programs.
        'Get the direct children for all groups that might exist.
        With sb
            .Append("SELECT rg.GrpId,rg.ReqId,ar.Descrip as Req,ar.Code as Code,ar.Credits,ar.ReqTypeId,rg.ReqSeq,ar.Hours,ar.FinAidCredits,stu.StuEnrollid      ")
            'Added By Vijay Ramteke on Feb 16, 2010
            .Append("       , ar.Hours As ScheduledHours   ")
            'Added By Vijay Ramteke in Feb 16, 2010
            .Append("FROM arReqGrpDef rg, arReqs ar, arStuEnrollments stu ")
            .Append("WHERE rg.ReqId=ar.ReqId  and stu.StuEnrollId = ? ")
            .Append("and ar.IsAttendanceOnly = 0 ")
            .Append("AND EXISTS( ")
            .Append("SELECT distinct t4.ReqId ")
            .Append("FROM arReqs t4 ")
            .Append("WHERE t4.ReqTypeId=2 ")
            .Append("AND t4.ReqId=rg.GrpId ")
            .Append("AND t4.IsAttendanceOnly = 0 ")
            .Append("AND EXISTS( ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t400.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t400.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t700.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t700.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append(" SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t800.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t800.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t900.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t900.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t1000.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t1000.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t1100.ReqId as ReqId, t3.Descrip As Req ")
            .Append(" FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000, arReqGrpDef t1100 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t1000.ReqId = t1100.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t1100.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append(")) ")
            .Append("ORDER BY rg.ReqSeq ")
        End With

        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "GroupChildren")
            sb.Remove(0, sb.Length)
            db.ClearParameters()
        Catch ex As Exception
            Throw ex
        End Try

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    Public Function GetGraduateAuditByCEProgEnrollmentForCourseEquivalent(ByVal StuEnrollId As String, Optional ByVal termEndDate As DateTime = #1/1/1001#, Optional ByVal termCond As String = "", Optional ByVal classCond As String = "", Optional ByVal Campusid As String = "") As DataSet
        ' New Optional Parameter add termCond and classCond by Vijay Ramteke on May, 06 2009
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim sb As New StringBuilder

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        'Special case:  Since Continuing Education (CE) programs do not have a definition,
        '               need to get from arResults and arTransferGrades only those courses the student has taken 
        '               or is currently registered for
        With sb
            .Append("SELECT ")
            .Append("       distinct C.ReqId,R.Descrip AS Req,R.Code,R.Credits,R.Hours,")
            .Append("       R.ReqTypeId,0 AS DefCredits, 0 AS ProgCredits,")
            .Append("       0 AS ProgHours,PV.PrgVerDescrip,PV.IsContinuingEd,PV.PrgVerId,R.FinAidCredits,E.StuEnrollId    ")
            'Added By Vijay Ramteke on Feb 16, 2010
            .Append("       , R.Hours As ScheduledHours   ")
            'Added By Vijay Ramteke in Feb 16, 2010
            .Append("FROM   arResults T,arClassSections C,arReqs R,arStuEnrollments E,arPrgVersions PV ")
            .Append("WHERE  T.StuEnrollId=? AND T.TestId=C.ClsSectionId AND C.ReqId=R.ReqId ")
            .Append("       AND E.StuEnrollId=T.StuEnrollId AND PV.PrgVerId=E.PrgVerId ")
            .Append("       AND R.IsAttendanceOnly = 0 ")
            If Not myAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                .Append(" and R.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId=? and GrdSysDetailId is not null) ")
            Else
                .Append(" and R.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId=? and Score is not null) ")
            End If
            .Append("UNION ALL ")
            .Append("SELECT ")
            .Append("       distinct G.ReqId,R.Descrip AS Req,R.Code,R.Credits,R.Hours,")
            .Append("       R.ReqTypeId,0 AS DefCredits, 0 AS ProgCredits,")
            .Append("       0 AS ProgHours,PV.PrgVerDescrip,PV.IsContinuingEd,PV.PrgVerId ,R.FinAidCredits,E.StuEnrollId    ")
            'Added By Vijay Ramteke on Feb 16, 2010
            .Append("       , R.Hours As ScheduledHours   ")
            'Added By Vijay Ramteke in Feb 16, 2010
            .Append("FROM   arTransferGrades G,arReqs R,arStuEnrollments E,arPrgVersions PV ")
            .Append("WHERE  G.StuEnrollId=?")
            .Append("       AND G.ReqId=R.ReqId AND E.StuEnrollId=G.StuEnrollId AND PV.PrgVerId=E.PrgVerId ")
            .Append("       AND R.IsAttendanceOnly = 0 ")
        End With

        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "DirectChildren")
            sb.Remove(0, sb.Length)
            db.ClearParameters()
        Catch ex As Exception
            Throw ex
        End Try


        'Get the results for classes regardless of the program version.
        With sb
            .Append("select R.TermId , R.TermDescrip ")
            .Append(", ISNULL(ATES.DescripXTranscript, '') AS DescripXTranscript ")
            .Append(", R.ReqId, R.Code, R.Descrip, R.Credits, R.Hours, R.CourseCategoryId, R.StartDate, R.EndDate, R.ClassStartDate, R.ClassEndDate ")
            .Append(", R.GrdSysDetailId, R.TestId, R.ResultId, R.Grade, R.IsPass, R.GPA, R.IsCreditsAttempted, R.IsCreditsEarned, R.IsInGPA, R.IsInSAP, R.IsDrop ")
            .Append(", R.CourseCategory, R.Score, R.FinAidCredits, R.DateIssue, R.DropDate, R.ScheduledHours, R.IsTransferGrade  ")
            .Append("FROM (    ")
            .Append("SELECT DISTINCT ")
            .Append("       t4.TermId, t3.TermDescrip, ")
            .Append("       t4.ReqId, t2.Code, t2.Descrip AS Descrip, ")
            .Append("       t2.Credits, t2.Hours, t2.CourseCategoryId, t3.StartDate AS StartDate, t3.EndDate AS EndDate, t4.StartDate as ClassStartDate, t4.EndDate as ClassEndDate, ")
            .Append("       t1.GrdSysDetailId, t1.TestId, t1.ResultId, ")
            .Append("       (Select Grade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as Grade,")
            .Append("       (Select IsPass from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsPass,")
            .Append("       (Select isnull(GPA,0) from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as GPA, ")
            .Append("       (Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsAttempted, ")
            .Append("       (Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsEarned, ")
            .Append("       (Select IsInGPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInGPA, ")
            .Append("       (Select IsInSAP from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInSAP, ")
            .Append("       (Select IsDrop from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsDrop, ")
            .Append("       (Select Descrip from arCourseCategories where CourseCategoryId = t2.CourseCategoryId) as CourseCategory, t1.Score, t2.FinAidCredits,  ")
            .Append("       Case(t2.IsExternship) When 1 then (select max(AttendedDate) from arExternshipAttendance where arExternshipAttendance.StuEnrollId = t1.StuEnrollId) else t4.EndDate end AS DateIssue, t1.DateDetermined as DropDate ")
            .Append("       , t2.Hours As ScheduledHours ")
            .Append("       , (Select IsTransferGrade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsTransferGrade ")
            .Append("       , t1.StuEnrollId  ")
            .Append("FROM   arResults t1, arReqs t2, arTerm t3, arClassSections t4, arClassSectionTerms t5 ")
            .Append("WHERE  t1.TestId = t4.ClsSectionId ")
            .Append("       AND t4.ClsSectionId = t5.ClsSectionId ")
            .Append("       AND t5.TermId = t3.TermId ")
            .Append("       AND t4.ReqId = t2.ReqId ")
            .Append("       AND t2.IsAttendanceOnly = 0 ")
            If Not myAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                .Append(" AND  ")
                'code added by balaji on 4/08/2009 to fix issue with credits per service mantis:15477 
                'modification starts here
                .Append(" (t1.GrdSysDetailId is not null or (t1.GrdSysDetailId is null and t1.isClinicsSatisfied=1) ")
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    .Append(" or (t1.GrdSysDetailId is null  and  (t1.isClinicsSatisfied=0 or t1.IsClinicsSatisfied is NULL)   and (select count(*) from argrdBkResults where StuEnrollId = t1.StuEnrollId and ClsSectionId = t1.TestId and Score is not null) >= 1) ")
                End If
                .Append(" ) ")
                'ends here
            Else
                .Append(" AND ")
                'code added by balaji on 4/08/2009 to fix issue with credits per service mantis:15477 
                'modification starts here
                .Append(" (t1.Score is not null or (t1.score is null and t1.isClinicsSatisfied = 1) ")
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    .Append(" or (t1.score is null and  (t1.isClinicsSatisfied=0 or t1.IsClinicsSatisfied is NULL)  and (select count(*) from argrdBkResults where StuEnrollId = t1.StuEnrollId and ClsSectionId = t1.TestId and Score is not null) >= 1) ")
                End If
                .Append(" ) ")
                'ends here
            End If
            .Append("       AND t1.StuEnrollId = ? ")
            If Not myAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                .Append(" and t2.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId = ? and GrdSysDetailId is not null) ")
            Else
                .Append(" and t2.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId = ? and Score is not null) ")
            End If
            If myAdvAppSettings.AppSettings("SchedulingMethod", Campusid) <> "ModuleStart" Then
                If Not (termEndDate = #1/1/1001#) Then
                    .Append("AND t3.EndDate <= ? ")
                End If
            End If
            'Code added by Vijay Ramteke on May, 07 2009
            If termCond <> "" Then
                .Append(" " & termCond.ToLower.Replace("arterm", "t3") & " ")
            End If
            If classCond <> "" Then
                .Append(" " & classCond.ToLower.Replace("arclasssections", "t4") & " ")
            End If
            'Code added by Vijay Ramteke on May, 07 2009
            .Append("UNION ")
            .Append("SELECT DISTINCT ")
            .Append("       t10.TermId, t30.TermDescrip, ")
            .Append("       t10.ReqId, t20.Code, t20.Descrip AS Descrip, ")
            .Append("       t20.Credits, t20.Hours, t20.CourseCategoryId, t30.StartDate AS StartDate, t30.EndDate AS EndDate, '1/1/1900' as ClassStartDate, '1/1/1900' as ClassEndDate, ")
            .Append("       t10.GrdSysDetailId, '{00000000-0000-0000-0000-000000000000}' AS TestId, t10.TransferId AS ResultId, ")
            .Append("       (Select Grade from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId)as Grade, ")
            .Append("       (Select IsPass from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsPass, ")
            .Append("       (Select isnull(GPA,0) from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as GPA, ")
            .Append("       (Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsCreditsAttempted, ")
            .Append("       (Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsCreditsEarned, ")
            .Append("       (Select IsInGPA from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsInGPA, ")
            .Append("       (Select IsInSAP from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsInSAP, ")
            .Append("       (Select IsDrop from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsDrop, ")
            .Append("       (Select Descrip from arCourseCategories where CourseCategoryId = t20.CourseCategoryId) as CourseCategory, t10.Score, t20.FinAidCredits, ")
            'Code Added By Vijay Ramteke on May, 11 2009
            '.Append("       t30.EndDate AS DateIssue,t30.EndDate AS DropDate      ")
            .Append("       Case(t20.IsExternship) When 1 then (select max(AttendedDate) from arExternshipAttendance where arExternshipAttendance.StuEnrollId = t10.StuEnrollId) else t30.EndDate end AS DateIssue, t30.EndDate AS DropDate ")
            'Code Added By Vijay Ramteke on May, 11 2009
            'Added By Vijay Ramteke on Feb 16, 2010
            .Append("       , t20.Hours As ScheduledHours ")
            'Added By Vijay Ramteke in Feb 16, 2010
            ''New Code Added By Vijay Ramteke For Rally Id 1393 on January 05, 2011
            .Append("       , (Select IsTransferGrade from arGradeSystemDetails where GrdSysDetailId = t10.GrdSysDetailId) as IsTransferGrade ")
            .Append("       , t10.StuEnrollId  ")
            ''New Code Added By Vijay Ramteke For Rally Id 1393 on January 05, 2011
            .Append("FROM   arTransferGrades t10,arReqs t20,arTerm t30 ")
            .Append("WHERE  t10.TermId = t30.TermId ")
            .Append("       AND t10.ReqId = t20.ReqId ")
            .Append("       AND t20.IsAttendanceOnly = 0 ")
            If Not myAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                .Append(" AND  ")
                'code added by balaji on 4/08/2009 to fix issue with credits per service mantis:15477 
                'modification starts here
                .Append(" (t10.GrdSysDetailId is not null or (t10.GrdSysDetailId is null and t10.isClinicsSatisfied = 1) ")
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    .Append(" or (t10.GrdSysDetailId is null  and  (t10.isClinicsSatisfied=0 or t10.IsClinicsSatisfied is NULL)   and (select count(*) from argrdBkResults where StuEnrollId = t10.StuEnrollId and ClsSectionId in (select Distinct ClsSectionId from arClassSections where ReqId = t10.ReqId and TermId = t10.TermId) and Score is not null) >= 1) ")
                End If
                .Append(" ) ")
                'ends here
            Else
                .Append(" AND ")
                'code added by balaji on 4/08/2009 to fix issue with credits per service mantis:15477 
                'modification starts here
                .Append(" (t10.Score is not null or (t10.score is null and t10.isClinicsSatisfied=1) ")
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    .Append(" or (t10.score is null and  (t10.isClinicsSatisfied = 0 or t10.IsClinicsSatisfied is NULL)  and (select count(*) from argrdBkResults where StuEnrollId = t10.StuEnrollId and ClsSectionId in (select Distinct ClsSectionId from arClassSections where ReqId = t10.ReqId and TermId = t10.TermId) and Score is not null) >=1) ")
                End If
                .Append(" ) ")
                'ends here
            End If
            .Append("       AND t10.StuEnrollId = ? ")
            If myAdvAppSettings.AppSettings("SchedulingMethod", Campusid) <> "ModuleStart" Then
                If Not (termEndDate = #1/1/1001#) Then
                    .Append("AND t30.EndDate <= ? ")
                End If
            End If
            'Code added by Vijay Ramteke on May, 07 2009
            If termCond <> "" Then
                .Append(" " & termCond.ToLower.Replace("arterm", "t30") & " ")
            End If
            If classCond <> "" Then
                .Append(" " & classCond.ToLower.Replace("arclasssections", "t30") & " ")
            End If
            'Code added by Vijay Ramteke on May, 07 2009

            .Append("UNION ")
            .Append("SELECT DISTINCT ")
            .Append("       t4.TermId, t3.TermDescrip, ")
            .Append("       S.ReqId, t2.Code, t2.Descrip AS Descrip,   ")
            .Append("       t2.Credits, t2.Hours, t2.CourseCategoryId, t3.StartDate AS StartDate, t3.EndDate AS EndDate, t4.StartDate as ClassStartDate, t4.EndDate  as ClassEndDate,  ")
            .Append("       t1.GrdSysDetailId, t1.TestId, t1.ResultId, ")
            .Append("       (Select Top 1 Grade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId)as Grade, ")
            .Append("       (Select Top 1 IsPass from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsPass, ")
            .Append("       (Select Top 1 isnull(GPA,0) from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as GPA, ")
            .Append("       (Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsAttempted, ")
            .Append("       (Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsEarned, ")
            .Append("       (Select Top 1 IsInGPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInGPA, ")
            .Append("       (Select Top 1 IsInSAP from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInSAP, ")
            .Append("       (Select Top 1 IsDrop from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsDrop, ")
            .Append("       (Select Top 1 Descrip from arCourseCategories where CourseCategoryId = t2.CourseCategoryId) as CourseCategory, t1.Score, t2.FinAidCredits, ")
            'Code Added By Vijay Ramteke on May, 11 2009
            '.Append("       t4.EndDate AS DateIssue ,t1.DateDetermined as DropDate        ")
            .Append("       Case(t2.IsExternship) When 1 then (select max(AttendedDate) from arExternshipAttendance where arExternshipAttendance.StuEnrollId = t1.StuEnrollId) else t4.EndDate end AS DateIssue, t1.DateDetermined as DropDate ")
            'Code Added By Vijay Ramteke on May, 11 2009
            'Added By Vijay Ramteke on Feb 16, 2010
            .Append("       , t2.Hours As ScheduledHours  ")
            'Added By Vijay Ramteke in Feb 16, 2010
            ''New Code Added By Vijay Ramteke For Rally Id 1393 on January 05, 2011
            .Append("       , (Select IsTransferGrade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsTransferGrade ")
            .Append("       , t1.StuEnrollId  ")
            ''New Code Added By Vijay Ramteke For Rally Id 1393 on January 05, 2011
            .Append("FROM ")
            .Append("    (SELECT distinct ")
            .Append("            t700.ReqId as EqReqId, t3.ReqId, t100.StuEnrollId  ")
            .Append("     FROM arStuEnrollments t100, arReqs t3, arProgVerDef t400, adLeads t500, arPrgVersions t600, arCourseEquivalent t700  ")
            .Append("     WHERE(t100.StudentId = t500.StudentId) ")
            .Append("       AND t3.ReqId = t400.ReqId AND t100.PrgVerId = t400.PrgVerId AND t100.PrgVerId = t600.PrgVerId ")
            .Append("       AND t3.Reqid = t700.EquivReqId AND t100.StuEnrollId =  ? ")
            .Append("       AND t3.Reqid not in ")
            .Append("                          (select ReqId from arResults a ,arClassSections b where a.TestId = b.ClsSectionId ")
            .Append("                                        AND StuEnrollId =  ? and ReqId = t400.ReqId) ")
            .Append("    ) AS S, arResults t1, arReqs t2, arTerm t3, arClassSections t4, arClassSectionTerms t5, arStuEnrollments t6 ")
            .Append("WHERE  t1.TestId = t4.ClsSectionId  ")
            .Append("  AND t4.ClsSectionId = t5.ClsSectionId ")
            .Append("  AND t5.TermId = t3.TermId ")
            .Append("  AND t4.ReqId = t2.ReqId ")
            .Append("  AND t1.StuEnrollId = t6.StuEnrollId  ")
            .Append("  AND t2.IsAttendanceOnly = 0 ")
            If Not myAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
                .Append(" AND  ")
                'code added by balaji on 4/08/2009 to fix issue with credits per service mantis:15477 
                'modification starts here
                .Append(" (t1.GrdSysDetailId is not null or (t1.GrdSysDetailId is null and t1.isClinicsSatisfied=1) ")
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    .Append(" or (t1.GrdSysDetailId is null  and  (t1.isClinicsSatisfied=0 or t1.IsClinicsSatisfied is NULL)   and (select count(*) from argrdBkResults where StuEnrollId = t1.StuEnrollId and ClsSectionId = t1.TestId and Score is not null) >= 1) ")
                End If
                .Append(" ) ")
                'ends here
            Else
                .Append(" AND ")
                'code added by balaji on 4/08/2009 to fix issue with credits per service mantis:15477 
                'modification starts here
                .Append(" (t1.Score is not null or (t1.score is null and t1.isClinicsSatisfied=1) ")
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    .Append(" or (t1.score is null and  (t1.isClinicsSatisfied = 0 or t1.IsClinicsSatisfied is NULL)  and (select count(*) from argrdBkResults where StuEnrollId = t1.StuEnrollId and ClsSectionId = t1.TestId and Score is not null) >= 1) ")
                End If
                .Append(" ) ")
                'ends here
            End If
            .Append(" And t1.StuEnrollId = S.StuEnrollId ")
            .Append(" and t2.Reqid = S.EqReqId ")
            'Code added by Vijay Ramteke on May, 07 2009
            If termCond <> "" Then
                .Append(" " & termCond.ToLower.Replace("arterm", "t3") & " ")
            End If
            If classCond <> "" Then
                .Append(" " & classCond.ToLower.Replace("arclasssections", "t4") & " ")
            End If
            'Code added by Vijay Ramteke on May, 07 2009
            .Append(") AS R  ")
            .Append(" LEFT OUTER JOIN arTermEnrollSummary AS ATES ON R.TermId = ATES.TermId AND R.StuEnrollId = ATES.StuEnrollId ")
            .Append(" ORDER BY StartDate desc, EndDate, TermDescrip")
        End With

        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If myAdvAppSettings.AppSettings("SchedulingMethod", Campusid) <> "ModuleStart" Then
            If Not (termEndDate = #1/1/1001#) Then
                db.AddParameter("@EndDate", termEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
        End If
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If myAdvAppSettings.AppSettings("SchedulingMethod", Campusid) <> "ModuleStart" Then
            If Not (termEndDate = #1/1/1001#) Then
                db.AddParameter("@EndDate", termEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
        End If
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "Results")
            Dim stuStartDate As String
            Dim aRows() As DataRow
            'modified by Theresa G on 5/29/09 for mantis 16376: 2.2.0: Course is shown in the completed section even before the results are transferred. 
            '----------------------------------------------------------------------------------------------------------------------------------------
            Dim labCount As Integer
            Dim labCountAttempted As Integer
            For Each row As DataRow In ds.Tables("Results").Rows

                If myAdvAppSettings.AppSettings("SchedulingMethod", Campusid) = "ModuleStart" Then
                    If row("TestId").ToString <> "00000000-0000-0000-0000-000000000000" Then
                        If (IsDuplicateClassSection(row("TestId").ToString) = True) Then
                            stuStartDate = GetStudentStartDate(StuEnrollId)
                            aRows = ds.Tables("Results").Select("TestId='" & row("TestId").ToString & "' AND StartDate=#" & CDate(stuStartDate) & "#")
                            If aRows.Length > 0 Then
                                If (stuStartDate <> CDate(row("StartDate")).ToShortDateString) Then
                                    row.Delete()
                                End If
                            Else
                                'aRows = ds.Tables(0).Select("TestId='" & row("TestId").ToString & "'")
                                aRows = ds.Tables("Results").Select("TestId='" & row("TestId").ToString & "' AND StartDate <> #" & row("StartDate") & "#")
                                If (aRows.Length > 0) Then
                                    row.Delete()
                                End If
                            End If
                        End If
                    End If
                End If
                If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                    labCount = (New TransferGradeDB).LabWorkOrLabHourCourseCount(row("ReqId").ToString)
                    If labCount <= 0 And row("GrdSysDetailId").ToString = "" Then row.Delete()
                    If labCount > 0 Then
                        labCountAttempted = (New ExamsDB).GetClinicServicesAndHoursAttemptedByStudent(StuEnrollId, row("Reqid").ToString)
                        If labCountAttempted <= 0 Then row.Delete()
                    End If

                End If

            Next
            ds.AcceptChanges()
            '----------------------------------------------------------------------------------------------------------------------------------------
            sb.Remove(0, sb.Length)
            db.ClearParameters()
        Catch ex As Exception
            Throw ex
        End Try


        'This table should always be empty because no course group should exist for CE programs.
        'Get the direct children for all groups that might exist.
        With sb
            .Append("SELECT rg.GrpId,rg.ReqId,ar.Descrip as Req,ar.Code as Code,ar.Credits,ar.ReqTypeId,rg.ReqSeq,ar.Hours,ar.FinAidCredits,stu.StuEnrollid      ")
            'Added By Vijay Ramteke on Feb 16, 2010
            .Append("       , ar.Hours As ScheduledHours   ")
            'Added By Vijay Ramteke in Feb 16, 2010
            .Append("FROM arReqGrpDef rg, arReqs ar, arStuEnrollments stu ")
            .Append("WHERE rg.ReqId=ar.ReqId  and stu.StuEnrollId = ? ")
            .Append("and ar.IsAttendanceOnly = 0 ")
            .Append("AND EXISTS( ")
            .Append("SELECT distinct t4.ReqId ")
            .Append("FROM arReqs t4 ")
            .Append("WHERE t4.ReqTypeId=2 ")
            .Append("AND t4.ReqId=rg.GrpId ")
            .Append("AND t4.IsAttendanceOnly = 0 ")
            .Append("AND EXISTS( ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t400.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t400.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t700.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t700.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append(" SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t800.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t800.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t900.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t900.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t1000.ReqId as ReqId, t3.Descrip As Req ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t1000.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t1100.ReqId as ReqId, t3.Descrip As Req ")
            .Append(" FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000, arReqGrpDef t1100 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append(" t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t1000.ReqId = t1100.GrpId AND ")
            .Append("t100.StuEnrollId = ? AND ")
            .Append("t1100.ReqId=t4.ReqId AND ")
            .Append("t3.IsAttendanceOnly = 0 ")
            .Append(")) ")
            .Append("ORDER BY rg.ReqSeq ")
        End With

        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "GroupChildren")
            sb.Remove(0, sb.Length)
            db.ClearParameters()
        Catch ex As Exception
            Throw ex
        End Try
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    Public Function GetGraduateAuditByCEProgEnrollmentForCourseEquivalent_SP(ByVal StuEnrollId As String, Optional ByVal termEndDate As DateTime = #1/1/1001#, Optional ByVal termCond As String = "", Optional ByVal classCond As String = "", Optional ByVal Campusid As String = "") As DataSet

        Dim ds As DataSet

        If myAdvAppSettings.AppSettings("GradesFormat", Campusid).ToString.ToLower = "numeric" Then
            ds = GetTranscriptCeNumeric(StuEnrollId, termCond, classCond)
        Else
            ds = GetTranscriptCELetter(StuEnrollId, termCond, classCond)
        End If

        ds.Tables(0).TableName = "DirectChildren"
        ds.Tables(1).TableName = "Results"
        ds.Tables(2).TableName = "Groupchildren"

        Try

            'Dim stuStartDate As String = String.Empty
            'Dim aRows() As DataRow
            'modified by Theresa G on 5/29/09 for mantis 16376: 2.2.0: Course is shown in the completed section even before the results are transferred. 
            '----------------------------------------------------------------------------------------------------------------------------------------
            Dim labCount As Integer
            Dim labCountAttempted As Integer
            If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                For Each row As DataRow In ds.Tables("Results").Rows

                    labCount = (New TransferGradeDB).LabWorkOrLabHourCourseCount_SP(row("ReqId").ToString)
                    If labCount <= 0 And row("GrdSysDetailId").ToString = "" Then row.Delete()
                    If labCount > 0 Then
                        labCountAttempted = (New ExamsDB).GetClinicServicesAndHoursAttemptedByStudent_SP(StuEnrollId, row("Reqid").ToString)
                        If labCountAttempted <= 0 Then row.Delete()
                    End If



                Next
            End If
            ds.AcceptChanges()
            '----------------------------------------------------------------------------------------------------------------------------------------


        Catch ex As Exception
            Throw ex
        End Try

        'Get the direct children for all groups used in the program version irregardless of level

        'Groupchildren = GetGroupChildrenForTranscript(StuEnrollId)
        'ds.Tables.Add(Groupchildren.Copy())
        'ds.Tables(2).TableName = "Groupchildren"
        Return ds
    End Function

    Public Function GetReqsForCourseGroup(ByVal courseGrpId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT t1.ReqId,t2.Code,t2.Credits,t2.Descrip,t2.ReqTypeId,t2.Hours ")
            .Append("FROM arReqGrpDef t1, arReqs t2 ")
            .Append("WHERE t1.ReqId=t2.ReqId ")
            .Append("AND t1.GrpId = ? ")
            .Append("UNION ")
            .Append("SELECT t3.ReqId,t2.Code, t2.Credits,t2.Descrip,t2.ReqTypeId,t2.Hours ")
            .Append("FROM arReqGrpDef t1, arReqs t2, arReqGrpDef t3 ")
            .Append("WHERE t3.ReqId=t2.ReqId ")
            .Append("AND t1.ReqId=t3.GrpId ")
            .Append("AND t1.GrpId = ? ")
            .Append("UNION ")
            .Append("SELECT t4.ReqId,t2.Code, t2.Credits,t2.Descrip,t2.ReqTypeId,t2.Hours ")
            .Append("FROM arReqGrpDef t1, arReqs t2, arReqGrpDef t3, arReqGrpDef t4 ")
            .Append("WHERE t4.ReqId=t2.ReqId ")
            .Append("AND t1.ReqId=t3.GrpId ")
            .Append("AND t3.ReqId=t4.GrpId ")
            .Append("AND t1.GrpId = ? ")
            .Append("UNION ")
            .Append("SELECT t5.ReqId,t2.Code, t2.Credits,t2.Descrip,t2.ReqTypeId,t2.Hours ")
            .Append("FROM arReqGrpDef t1, arReqs t2, arReqGrpDef t3, arReqGrpDef t4, arReqGrpDef t5 ")
            .Append("WHERE t5.ReqId=t2.ReqId ")
            .Append("AND t1.ReqId=t3.GrpId ")
            .Append("AND t3.ReqId=t4.GrpId ")
            .Append("AND t4.ReqId=t5.GrpId ")
            .Append("AND t1.GrpId = ? ")
            .Append("UNION ")
            .Append("SELECT t6.ReqId,t2.Code, t2.Credits,t2.Descrip,t2.ReqTypeId,t2.Hours ")
            .Append("FROM arReqGrpDef t1, arReqs t2, arReqGrpDef t3, arReqGrpDef t4, arReqGrpDef t5, arReqGrpDef t6 ")
            .Append("WHERE t6.ReqId=t2.ReqId ")
            .Append("AND t1.ReqId=t3.GrpId ")
            .Append("AND t3.ReqId=t4.GrpId ")
            .Append("AND t4.ReqId=t5.GrpId ")
            .Append("AND t5.ReqId=t6.GrpId ")
            .Append("AND t1.GrpId = ? ")
            .Append("UNION ")
            .Append("SELECT t7.ReqId,t2.Code, t2.Credits,t2.Descrip,t2.ReqTypeId,t2.Hours ")
            .Append("FROM arReqGrpDef t1, arReqs t2, arReqGrpDef t3, arReqGrpDef t4, arReqGrpDef t5, arReqGrpDef t6, arReqGrpDef t7 ")
            .Append("WHERE t7.ReqId=t2.ReqId ")
            .Append("AND t1.ReqId=t3.GrpId ")
            .Append("AND t3.ReqId=t4.GrpId ")
            .Append("AND t4.ReqId=t5.GrpId ")
            .Append("AND t5.ReqId=t6.GrpId ")
            .Append("AND t6.ReqId=t7.GrpId ")
            .Append("AND t1.GrpId = ? ")
            .Append("UNION ")
            .Append("SELECT t8.ReqId,t2.Code, t2.Credits,t2.Descrip,t2.ReqTypeId,t2.Hours ")
            .Append("FROM arReqGrpDef t1, arReqs t2, arReqGrpDef t3, arReqGrpDef t4, arReqGrpDef t5, arReqGrpDef t6, arReqGrpDef t7, arReqGrpDef t8 ")
            .Append("WHERE t8.ReqId=t2.ReqId ")
            .Append("AND t1.ReqId=t3.GrpId ")
            .Append("AND t3.ReqId=t4.GrpId ")
            .Append("AND t4.ReqId=t5.GrpId ")
            .Append("AND t5.ReqId=t6.GrpId ")
            .Append("AND t6.ReqId=t7.GrpId ")
            .Append("AND t7.ReqId=t8.GrpId ")
            .Append("AND t1.GrpId = ? ")
        End With

        db.AddParameter("@csrgrpid", courseGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@csrgrpid", courseGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@csrgrpid", courseGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@csrgrpid", courseGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@csrgrpid", courseGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@csrgrpid", courseGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@csrgrpid", courseGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetScheduledCourses(ByVal stuEnrollId As String, ByVal campusId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")
        With sb
            '   build query to get scheduled courses
            .Append("SELECT A.TestId,B.StartDate AS ClassStart,E.Descrip,E.Credits,E.Code,'' AS TimeAndRoom,")
            .Append("       (SELECT FullName FROM syUsers WHERE UserId=B.InstructorId) AS InstructorName,C.StartDate,")
            .Append("       (SELECT Descrip FROM arGradeSystemDetails WHERE GrdSysDetailId=A.GrdSysDetailId) AS Grade ")
            .Append(" ,(select CampDescrip from syCampuses where CampusId=B.CampusId) as CampDescrip,E.ReqId ")
            .Append("FROM   arResults A,arClassSections B,arTerm C,arClassSectionTerms X,syStatuses D,arReqs E ")
            .Append("WHERE  A.StuEnrollId=? ")
            .Append("       AND A.TestId=B.ClsSectionId ")
            .Append("       AND B.ClsSectionId=X.ClsSectionId")
            .Append("       AND X.TermId=C.TermId")
            .Append("       AND C.StatusId=D.StatusId")        'AND D.Status='Active' 
            .Append("       AND B.ReqId=E.ReqId ")

            'Added by Balaji on 7.18.2013
            .Append(" AND (A.IsCourseCompleted = 0 ")

            If Not myAdvAppSettings.AppSettings("GradesFormat", campusId).ToString.ToLower = "numeric" Then

                .Append(" OR ")
                .Append(" ((A.Score is NULL or GrdSysDetailId is NULL) and (A.isClinicsSatisfied=0 or A.isClinicsSatisfied is NULL))) ")
            Else

                .Append(" OR ")
                .Append(" ((A.Score is NULL or GrdSysDetailId is NULL) and (A.isClinicsSatisfied=0 or A.isClinicsSatisfied is NULL))) ")
            End If
            'If Not SingletonAppSettings.AppSettings("GradesFormat",CampusId).ToString.ToLower = "numeric" Then
            '    .Append(" and E.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId=? and GrdSysDetailId is not null) ")
            'Else
            '    .Append(" and E.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId=? and Score is not null) ")
            'End If
            .Append("ORDER BY C.StartDate ,B.StartDate,E.Code,E.Descrip;")
        End With
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            Dim ds As DataSet
            Dim stuStartDate As String
            Dim aRows() As DataRow
            ds = db.RunParamSQLDataSet(sb.ToString)
            If myAdvAppSettings.AppSettings("SchedulingMethod") = "ModuleStart" Then
                For Each row As DataRow In ds.Tables(0).Rows
                    If (IsDuplicateClassSection(row("TestId").ToString) = True) Then
                        stuStartDate = GetStudentStartDate(stuEnrollId)
                        aRows = ds.Tables(0).Select("TestId='" & row("TestId").ToString & "' AND StartDate=#" & CDate(stuStartDate) & "#")
                        If aRows.Length > 0 Then
                            If (stuStartDate <> CDate(row("StartDate")).ToShortDateString) Then
                                row.Delete()
                            End If
                        Else
                            'aRows = ds.Tables(0).Select("TestId='" & row("TestId").ToString & "'")
                            aRows = ds.Tables(0).Select("TestId='" & row("TestId").ToString & "' AND StartDate <> #" & row("StartDate") & "#")
                            If (aRows.Length > 0) Then
                                row.Delete()
                            End If
                        End If
                    End If
                Next
                ds.AcceptChanges()
            End If

            'For numeric schools, if there is a clinic service course check to see if all components
            'under the course has been graded.if all lab work has been completed remove the course
            'from scheduled courses dataset

            'applied to both letter and numeric school on 05/21/2009
            'If Not SingletonAppSettings.AppSettings("GradesFormat",CampusId).ToString.ToLower = "numeric" Then
            For Each row As DataRow In ds.Tables(0).Rows
                Dim boolIsCourseALab As Boolean
                boolIsCourseALab = (New TransferGradeDB).isCourseALabWorkOrLabHourCourse(row("ReqId").ToString)
                If boolIsCourseALab = True Then
                    Dim sb1 As New StringBuilder
                    Dim intRowCount As Integer
                    Dim intRowExist As Integer
                    'Check if any lab work components exists in arGrdBkResults table

                    db.ClearParameters()
                    With sb1
                        .Append("select Count(*) as CountofComponentsNotGraded from arGrdBkResults where ClsSectionId=? and StuEnrollId=? ")
                    End With
                    db.AddParameter("@ClsSectionId", row("TestId").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Try
                        intRowExist = db.RunParamSQLScalar(sb1.ToString)

                    Catch ex As Exception
                        intRowExist = 0
                    End Try
                    sb1.Remove(0, sb1.Length)
                    db.ClearParameters()

                    'Dim boolIsCourseACombination As Boolean
                    Dim intCourseACombination As Integer = 0
                    With sb1
                        .Append(" select distinct Count(GC.Descrip) from arGrdBkWeights GBW,arGrdComponentTypes GC, arGrdBkWgtDetails GD where ")
                        .Append(" GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId And GC.GrdComponentTypeId = GD.GrdComponentTypeId ")
                        .Append(" and GBW.ReqId = ? and GC.SysComponentTypeID is not null  ")
                        .Append(" and GC.SysComponentTypeID in (499,501,502,533,544) ")
                    End With
                    db.AddParameter("@ReqId", row("ReqId").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Try
                        intCourseACombination = db.RunParamSQLScalar(sb1.ToString)
                    Catch ex As Exception
                        intCourseACombination = 0
                    End Try
                    sb1.Remove(0, sb1.Length)
                    db.ClearParameters()

                    Dim dr10 As OleDbDataReader
                    Dim boolNotSatisfied As Boolean = False
                    If intCourseACombination = 0 Then 'Only when course has lab work/lab hour and not part of a combination
                        With sb1
                            .Append(" select Required,sum(score) as totalscore ")
                            .Append(" 					from ")
                            .Append(" 					( ")
                            .Append(" 					   select ClsSectionId,t1.StuEnrollId,t1.InstrGrdBkWgtDetailId, ")
                            .Append(" 					   ( ")
                            .Append(" 							select  ")
                            .Append("            						Sum(GBWD.Number) as Required ")
                            .Append(" 					from	arGrdComponentTypes GCT, arGrdBkWgtDetails GBWD, ")
                            .Append(" 									arGrdBkWeights GBW,arClassSections CS,arResults R   ")
                            .Append(" 							where  ")
                            .Append(" 									GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId  and ")
                            .Append(" 									GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId	and ")
                            .Append(" 									GBW.ReqId=CS.ReqId and CS.StartDate >= GBW.EffectiveDate ")
                            .Append(" 									and		GBWD.Number >= 1  ")
                            .Append(" 									and CS.clsSectionId = R.TestId  and ")
                            .Append(" 									R.TestId=t1.ClsSectionId and ")
                            .Append(" 									R.StuEnrollId=t1.StuEnrollId ")
                            .Append(" 									and GBW.EffectiveDate = ")
                            .Append(" 									(select max(arGrdBkWeights.EffectiveDate)  ")
                            .Append(" 									from arGrdBkWeights where ReqId=CS.ReqId)  ")
                            .Append(" 						) as Required, ")
                            .Append(" 						t1.score ")
                            .Append(" 					from arGrdBkResults t1 ")
                            .Append(" 						where t1.score is not null  ")
                            .Append(" 						and ClsSectionId=? ")
                            .Append(" 						and StuEnrollId=? ")
                            .Append(" 						group by ClsSectionId,t1.StuEnrollId,t1.InstrGrdBkWgtDetailId,t1.score ")
                            .Append(" 					)  R1 group by required ")

                        End With
                        db.AddParameter("@ClsSectionId", row("TestId").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        Try
                            dr10 = db.RunParamSQLDataReader(sb1.ToString)
                            While dr10.Read()
                                Dim decRequired As Decimal = 0.0
                                Dim decCompleted As Decimal = 0.0
                                If Not dr10("required") Is DBNull.Value Then decRequired = dr10("required") Else decRequired = 0
                                If Not dr10("TotalScore") Is DBNull.Value Then decCompleted = dr10("TotalScore") Else decCompleted = 0
                                If decRequired > decCompleted Then
                                    boolNotSatisfied = True
                                Else
                                    boolNotSatisfied = False
                                End If

                            End While
                            If Not dr10.IsClosed Then dr10.Close()

                        Catch ex As Exception

                        End Try
                        sb1.Remove(0, sb1.Length)
                        db.ClearParameters()

                        With sb1
                            .Append("select Count(*) as CountofComponentsNotGraded from arGrdBkResults where ClsSectionId=? and StuEnrollId=? and Score is NULL")
                        End With
                        db.AddParameter("@ClsSectionId", row("TestId").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        Try
                            intRowCount = db.RunParamSQLScalar(sb1.ToString)
                            sb1.Remove(0, sb1.Length)
                            db.ClearParameters()
                            If boolNotSatisfied = False And intRowExist >= 1 And intRowCount = 0 Then ' if count is 0, then means all labwork for the course has been completed and should not show up in scheduled courses grid
                                'Check if the course is already scheduled
                                Dim sb2 As New StringBuilder
                                Dim intSchedCount As Integer = 0
                                With sb2
                                    .Append("select Count(*) as CountofComponentsNotGraded from arResults where TestId=? and StuEnrollId=? and IsCourseCompleted=0")
                                End With
                                db.AddParameter("@ClsSectionId", row("TestId").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                intSchedCount = db.RunParamSQLScalar(sb2.ToString)
                                sb2.Remove(0, sb2.Length)
                                db.ClearParameters()
                                If intSchedCount = 0 Then row.Delete() 'if student is not scheduled delete row
                            End If
                        Catch ex As Exception
                            intRowCount = 0
                        End Try
                    Else

                    End If

                End If
            Next
            ds.AcceptChanges()
            'End If

            If ds.Tables.Count = 1 Then
                ds.Tables(0).TableName = "ScheduledCourses"
                If ds.Tables(0).Rows.Count > 0 Then
                    'Reuse code to get the Meeting Info
                    Dim dt As DataTable
                    dt = (New StuClassScheduleDB).GetClassMeetings(GetClsSectionList(ds.Tables(0)))
                    ds.Tables.Add(dt.Copy)
                    ds.Tables(1).TableName = "ClsSectMeetings"
                    Dim dc As DataColumn = ds.Tables(1).Columns.Add("TestId", Type.GetType("System.String"))
                    dc.AllowDBNull = True
                    dc.Expression = "ClsSectionId"
                End If
            End If
            Return ds
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function IsDuplicateClassSection(ByVal clsSectionId As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder


        db.ConnectionString = conString
        With sb
            .Append(" select ClsSectionId,Count(*) from arClassSectionTerms where ClsSectionId= ?   group by ClsSectionId having count(*)>1 ")
        End With
        db.AddParameter("@clsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Dim ds As New DataSet
        ds = db.RunParamSQLDataSet(sb.ToString)
        If (ds.Tables(0).Rows.Count > 0) Then
            Return True
        End If
        Return False
    End Function

    Private Function GetStudentStartDate(ByVal stuEnrollID) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT ExpStartDate FROM arStuEnrollments WHERE StuEnrollId= ? ")
        End With
        db.AddParameter("@StuEnrollId", stuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLScalar(sb.ToString)
    End Function

    Private Function GetClsSectionList(ByVal dt As DataTable) As String
        Dim clsSectList As String = ""
        For Each dr As DataRow In dt.Rows
            clsSectList &= "'" & dr("TestId").ToString & "',"
        Next
        If clsSectList <> "" Then
            'remove the last comma
            clsSectList = clsSectList.Substring(0, clsSectList.Length - 1)
        End If
        Return clsSectList
    End Function

    'Public Function GetRemainingCourses(ByVal stuEnrollId As String, ByVal campusId As String) As DataSet
    Public Function GetRemainingCourses(ByVal stuEnrollId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT * FROM ( ")
            .Append("   SELECT DISTINCT t100.StuEnrollId,t100.StartDate,t100.ExpStartDate,t500.LastName,t500.FirstName,t100.ExpGradDate,t100.PrgVerId,t600.PrgVerDescrip,t400.ReqId as ReqId,t3.Descrip As Req,t3.Code As Code,t3.Credits,t3.Hours,IsRequired = (CASE t400.IsRequired WHEN 0 THEN 'False' ELSE 'True' END) ")
            .Append(" ,(select CampDescrip from syCampuses where CampusId=t100.CampusId) as CampDescrip ")
            .Append(" ,isnull(t400.GrdSysDetailId,'00000000-0000-0000-0000-000000000000') as OverRide ")
            .Append("   FROM arStuEnrollments t100,arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, syStatuses t700 ")
            .Append("   WHERE t100.StudentId=t500.StudentId AND t100.PrgVerId=t600.PrgVerId AND t100.PrgVerId=t400.PrgVerId ")
            .Append("   AND t3.ReqId=t400.ReqId AND t3.ReqTypeId=1 ")
            .Append("   AND t100.StuEnrollId=? AND t3.StatusId=t700.StatusId  ")
            '.Append(" AND t700.Status='Active' ")
            .Append("   UNION ")
            .Append("   SELECT DISTINCT t100.StuEnrollId,t100.StartDate,t100.ExpStartDate,t500.LastName, t500.FirstName,t100.ExpGradDate,t100.PrgVerId,t600.PrgVerDescrip,t700.ReqId as ReqId,t3.Descrip As Req,t3.Code As Code,t3.Credits,t3.Hours,IsRequired = (CASE t400.IsRequired WHEN 0 THEN 'False' ELSE 'True' END) ")
            .Append(" ,(select CampDescrip from syCampuses where CampusId=t100.CampusId) as CampDescrip ")
            .Append(" ,isnull(t400.GrdSysDetailId,'00000000-0000-0000-0000-000000000000') as OverRide ")
            .Append("   FROM arStuEnrollments t100,arReqs t3,arProgVerDef t400,adLeads t500,arPrgVersions t600,arReqGrpDef t700, syStatuses t800 ")
            .Append("   WHERE t100.StudentId=t500.StudentId AND t100.PrgVerId=t600.PrgVerId AND t100.PrgVerId=t400.PrgVerId ")
            .Append("   AND t400.ReqId=t700.GrpId AND t3.ReqId=t700.ReqId AND t3.ReqTypeId=1 ")
            .Append("   AND t100.StuEnrollId=? AND t3.StatusId=t800.StatusId  ")
            '.Append(" AND t800.Status='Active' ")
            .Append("   UNION ")
            .Append("   SELECT DISTINCT t100.StuEnrollId,t100.StartDate,t100.ExpStartDate,t500.LastName,t500.FirstName,t100.ExpGradDate,t100.PrgVerId,t600.PrgVerDescrip,t800.ReqId as ReqId,t3.Descrip As Req,t3.Code As Code,t3.Credits,t3.Hours,IsRequired = (CASE t400.IsRequired WHEN 0 THEN 'False' ELSE 'True' END) ")
            .Append(" ,(select CampDescrip from syCampuses where CampusId=t100.CampusId) as CampDescrip ")
            .Append(" ,isnull(t400.GrdSysDetailId,'00000000-0000-0000-0000-000000000000') as OverRide ")
            .Append("   FROM arStuEnrollments t100,arReqs t3,arProgVerDef t400,adLeads t500,arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, syStatuses t900 ")
            .Append("   WHERE t100.StudentId=t500.StudentId AND t100.PrgVerId=t600.PrgVerId AND t100.PrgVerId=t400.PrgVerId ")
            .Append("   AND t400.ReqId=t700.GrpId AND t700.ReqId=t800.GrpId AND t3.ReqId=t800.ReqId AND t3.ReqTypeId=1 ")
            .Append("   AND t100.StuEnrollId=? AND t3.StatusId=t900.StatusId  ")
            '.Append(" AND t900.Status='Active' ")
            .Append("   UNION ")
            .Append("   SELECT DISTINCT t100.StuEnrollId,t100.StartDate,t100.ExpStartDate,t500.LastName,t500.FirstName,t100.ExpGradDate,t100.PrgVerId,t600.PrgVerDescrip,t900.ReqId as ReqId,t3.Descrip As Req,t3.Code As Code,t3.Credits,t3.Hours,IsRequired = (CASE t400.IsRequired WHEN 0 THEN 'False' ELSE 'True' END) ")
            .Append(" ,(select CampDescrip from syCampuses where CampusId=t100.CampusId) as CampDescrip ")
            .Append(" ,isnull(t400.GrdSysDetailId,'00000000-0000-0000-0000-000000000000') as OverRide ")
            .Append("   FROM arStuEnrollments t100,arReqs t3,arProgVerDef t400,adLeads t500,arPrgVersions t600,arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, syStatuses t1000 ")
            .Append("   WHERE t100.StudentId=t500.StudentId AND t100.PrgVerId=t600.PrgVerId AND t100.PrgVerId=t400.PrgVerId ")
            .Append("   AND t400.ReqId=t700.GrpId AND t700.ReqId=t800.GrpId AND t800.ReqId=t900.GrpId ")
            .Append("   AND t3.ReqId=t900.ReqId AND t3.ReqTypeId=1 ")
            .Append("   AND t100.StuEnrollId=? AND t3.StatusId=t1000.StatusId  ")
            '.Append(" AND t1000.Status='Active' ")
            .Append("   UNION ")
            .Append("   SELECT DISTINCT t100.StuEnrollId,t100.StartDate,t100.ExpStartDate,t500.LastName,t500.FirstName,t100.ExpGradDate,t100.PrgVerId,t600.PrgVerDescrip,t1000.ReqId as ReqId,t3.Descrip As Req,t3.Code As Code,t3.Credits,t3.Hours,IsRequired = (CASE t400.IsRequired WHEN 0 THEN 'False' ELSE 'True' END) ")
            .Append(" ,(select CampDescrip from syCampuses where CampusId=t100.CampusId) as CampDescrip ")
            .Append(" ,isnull(t400.GrdSysDetailId,'00000000-0000-0000-0000-000000000000') as OverRide ")
            .Append("   FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000, syStatuses t1100 ")
            .Append("   WHERE t100.StudentId=t500.StudentId AND t100.PrgVerId = t600.PrgVerId AND t100.PrgVerId=t400.PrgVerId ")
            .Append("   AND t400.ReqId=t700.GrpId AND t700.ReqId=t800.GrpId AND t800.ReqId=t900.GrpId ")
            .Append("   AND t900.ReqId=t1000.GrpId AND t3.ReqId=t1000.ReqId AND t3.ReqTypeId=1 ")
            .Append("   AND t100.StuEnrollId=? AND t3.StatusId=t1100.StatusId ")
            '.Append(" AND t1100.Status='Active' ")
            .Append("   UNION ")
            .Append("   SELECT DISTINCT t100.StuEnrollId,t100.StartDate,t100.ExpStartDate, t500.LastName, t500.FirstName, t100.ExpGradDate, t100.PrgVerId,t600.PrgVerDescrip,t1100.ReqId as ReqId, t3.Descrip As Req, t3.Code As Code,t3.Credits,t3.Hours,IsRequired = (CASE t400.IsRequired WHEN 0 THEN 'False' ELSE 'True' END) ")
            .Append(" ,(select CampDescrip from syCampuses where CampusId=t100.CampusId) as CampDescrip ")
            .Append(" ,isnull(t400.GrdSysDetailId,'00000000-0000-0000-0000-000000000000') as OverRide ")
            .Append("   FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, adLeads t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000, arReqGrpDef t1100, syStatuses t1200 ")
            .Append("   WHERE t100.StudentId=t500.StudentId AND t100.PrgVerId=t600.PrgVerId AND t100.PrgVerId=t400.PrgVerId ")
            .Append("   AND t400.ReqId=t700.GrpId AND t700.ReqId=t800.GrpId AND t800.ReqId=t900.GrpId ")
            .Append("   AND t900.ReqId=t1000.GrpId AND t1000.ReqId=t1100.GrpId AND t3.ReqId=t1100.ReqId AND t3.ReqTypeId=1 ")
            .Append("   AND t100.StuEnrollId=? AND t3.StatusId=t1200.StatusId ")
            '.Append("   AND t1200.Status='Active' ")
            .Append("   ) P ")
            .Append("WHERE NOT EXISTS (SELECT * FROM ")
            .Append("		            (SELECT DISTINCT t4.TermId, t3.TermDescrip,t4.ReqId,t2.Code,t2.Descrip,t2.Credits,t2.Hours,t3.StartDate,t3.EndDate,t1.GrdSysDetailId, ")
            .Append("		            (SELECT Grade FROM arGradeSystemDetails WHERE GrdSysDetailId=t1.GrdSysDetailId)as Grade, ")
            '.Append("		            (SELECT IsPass FROM arGradeSystemDetails WHERE GrdSysDetailId=t1.GrdSysDetailId) as IsPass, ")
            .Append(" Case when (select GrdSysDetailId from arProgVerDef where ReqId=P.ReqId and PrgVerId=P.PrgVerId) is not null then ")
            .Append(" (0) ")
            .Append(" else ")
            .Append(" (SELECT IsPass FROM arGradeSystemDetails WHERE GrdSysDetailId=t1.GrdSysDetailId ) ")
            .Append(" end as ISPass, ")

            .Append("		            (SELECT GPA FROM arGradeSystemDetails WHERE GrdSysDetailId=t1.GrdSysDetailId) as GPA, ")
            .Append("		            (SELECT IsCreditsAttempted FROM arGradeSystemDetails WHERE GrdSysDetailId=t1.GrdSysDetailId) as IsCreditsAttempted, ")
            .Append("		            (SELECT IsCreditsEarned FROM arGradeSystemDetails WHERE GrdSysDetailId=t1.GrdSysDetailId) as IsCreditsEarned, ")
            .Append("		            (SELECT IsInGPA FROM arGradeSystemDetails WHERE GrdSysDetailId=t1.GrdSysDetailId) as IsInGPA, t1.StuEnrollId  ,t1.IsCourseCompleted")
            .Append("		            FROM arResults t1,arReqs t2,arTerm t3,arClassSections t4 ")
            .Append("                   WHERE t1.TestId=t4.ClsSectionId And t4.TermId=t3.TermId And t4.ReqId=t2.ReqId ")
            .Append("		            AND t1.GrdSysDetailId IS NOT NULL ")
            .Append("		            AND t1.StuEnrollId=? ")
            .Append("                   UNION ")
            .Append("		            SELECT DISTINCT t10.TermId,t30.TermDescrip,t10.ReqId,t20.Code,t20.Descrip,t20.Credits,t20.Hours,t30.StartDate,t30.EndDate,t10.GrdSysDetailId, ")
            .Append("		            (SELECT Grade FROM arGradeSystemDetails WHERE GrdSysDetailId=t10.GrdSysDetailId) as Grade, ")
            '.Append("		            (SELECT IsPass FROM arGradeSystemDetails WHERE GrdSysDetailId=t10.GrdSysDetailId) as IsPass, ")
            .Append(" Case when (select GrdSysDetailId from arProgVerDef where ReqId=P.ReqId and PrgVerId=P.PrgVerId) is not null then ")
            .Append(" (0) ")
            .Append(" else ")
            .Append(" (SELECT IsPass FROM arGradeSystemDetails WHERE GrdSysDetailId=t10.GrdSysDetailId ) ")
            .Append(" end as ISPass, ")
            .Append("		            (SELECT GPA FROM arGradeSystemDetails WHERE GrdSysDetailId=t10.GrdSysDetailId) as GPA, ")
            .Append("		            (SELECT IsCreditsAttempted FROM arGradeSystemDetails WHERE GrdSysDetailId=t10.GrdSysDetailId) as IsCreditsAttempted, ")
            .Append("		            (SELECT IsCreditsEarned FROM arGradeSystemDetails WHERE GrdSysDetailId=t10.GrdSysDetailId) as IsCreditsEarned, ")
            .Append("		            (SELECT IsInGPA FROM arGradeSystemDetails WHERE GrdSysDetailId=t10.GrdSysDetailId) as IsInGPA, t10.StuEnrollId,t10.IsCourseCompleted ")
            .Append("		            FROM arTransferGrades t10,arReqs t20,arTerm t30 ")
            .Append("                   WHERE t10.TermId=t30.TermId And t10.ReqId=t20.ReqId ")
            .Append("		            AND t10.GrdSysDetailId IS NOT NULL ")
            .Append("		            AND t10.StuEnrollId=? ")
            .Append(" union ")
            .Append(" select distinct t4.TermId,t3.TermDescrip,S.ReqId,t2.Code,t2.Descrip,       t2.Credits, ")
            .Append(" t2.Hours,t3.StartDate,t3.EndDate,       t1.GrdSysDetailId, ")
            .Append(" (Select Grade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId)as Grade,    ")
            .Append(" (Select IsPass from ")
            .Append(" arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsPass,       (Select isnull(GPA,0) from arGradeSystemDetails ")
            .Append(" where GrdSysDetailId = t1.GrdSysDetailId) as GPA,       (Select IsCreditsAttempted from arGradeSystemDetails where ")
            .Append(" GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsAttempted,       (Select IsCreditsEarned from arGradeSystemDetails ")
            .Append(" where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsEarned,       (Select IsInGPA from arGradeSystemDetails where ")
            .Append(" GrdSysDetailId = t1.GrdSysDetailId) as IsInGPA,  t1.stuEnrollId ,t1.IsCourseCompleted ")
            .Append(" from ")
            .Append(" (SELECT distinct t700.ReqId as EqReqId,t3.ReqId,t100.StuEnrollId  FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, ")
            .Append(" adLeads t500,arPrgVersions t600  ")
            .Append(" ,arCourseEquivalent t700 ")
            .Append(" WHERE t100.StudentId = t500.StudentId ")
            .Append(" AND t3.ReqId = t400.ReqId AND t100.PrgVerId = t400.PrgVerId AND t100.PrgVerId = t600.PrgVerId ")
            .Append(" AND t3.Reqid=t700.EquivReqId AND t100.StuEnrollId =  ? ")
            .Append(" and t3.Reqid not in ")
            .Append(" (select ReqId from arResults a ,arClassSections b where a.TestId=b.ClsSectionId ")
            .Append(" and StuEnrollId =  ? and ReqId=t400.ReqId)) S, arResults t1, arReqs t2, ")
            .Append(" arTerm t3, arClassSections t4, arClassSectionTerms t5, arStuEnrollments t6  WHERE  t1.TestId = t4.ClsSectionId ")
            .Append(" AND t4.ClsSectionId = t5.ClsSectionId       AND t5.TermId = t3.TermId       AND t4.ReqId = t2.ReqId       AND ")
            .Append(" t1.StuEnrollId = t6.StuEnrollId And t1.GrdSysDetailId Is Not null And t1.StuEnrollId = S.StuEnrollId ")
            .Append(" and t2.Reqid=S.EqReqId ")
            'This is for records in arResults that have a course equivalent
            .Append("UNION ")
            .Append("SELECT DISTINCT t4.TermId,t3.TermDescrip,t5.EquivReqId,t2.Code,t2.Descrip,t2.Credits,t2.Hours,t3.StartDate,t3.EndDate,t1.GrdSysDetailId, ")
            .Append("( SELECT Grade FROM arGradeSystemDetails WHERE GrdSysDetailId = t1.GrdSysDetailId) AS Grade, ")
            .Append(" CASE WHEN ( SELECT  GrdSysDetailId FROM arProgVerDef WHERE   ReqId = P.ReqId AND PrgVerId = P.PrgVerId) IS NOT NULL THEN ( 0 ) ")
            .Append(" ELSE ( SELECT  IsPass FROM    arGradeSystemDetails WHERE   GrdSysDetailId = t1.GrdSysDetailId) ")
            .Append("END AS ISPass, ")
            .Append(" ( SELECT GPA FROM arGradeSystemDetails WHERE GrdSysDetailId = t1.GrdSysDetailId) AS GPA, ")
            .Append(" ( SELECT IsCreditsAttempted FROM arGradeSystemDetails WHERE GrdSysDetailId = t1.GrdSysDetailId) AS IsCreditsAttempted,")
            .Append("( SELECT IsCreditsEarned FROM arGradeSystemDetails WHERE GrdSysDetailId = t1.GrdSysDetailId ) AS IsCreditsEarned,")
            .Append("( SELECT IsInGPA FROM arGradeSystemDetails WHERE GrdSysDetailId = t1.GrdSysDetailId) AS IsInGPA, ")
            .Append("t1.StuEnrollId ,t1.IsCourseCompleted ")
            .Append("FROM      arResults t1,arReqs t2,arTerm t3,arClassSections t4,dbo.arCourseEquivalent t5 ")
            .Append("WHERE     t1.TestId = t4.ClsSectionId AND t4.TermId = t3.TermId AND t4.ReqId = t2.ReqId AND t1.GrdSysDetailId IS NOT NULL AND t4.ReqId=t5.ReqId AND t1.StuEnrollId = ? ")
            'This handles records in arTransferGrades with course equivalents
            .Append("UNION ")
            .Append("SELECT DISTINCT t10.TermId,t30.TermDescrip,t40.EquivReqId,t20.Code,t20.Descrip,t20.Credits,t20.Hours,t30.StartDate,t30.EndDate,t10.GrdSysDetailId,")
            .Append(" ( SELECT Grade FROM arGradeSystemDetails WHERE GrdSysDetailId = t10.GrdSysDetailId) AS Grade,")
            .Append("CASE WHEN ( SELECT GrdSysDetailId FROM arProgVerDef WHERE ReqId = P.ReqId AND PrgVerId = P.PrgVerId) IS NOT NULL THEN ( 0 ) ")
            .Append("ELSE ( SELECT IsPass FROM arGradeSystemDetails WHERE GrdSysDetailId = t10.GrdSysDetailId) ")
            .Append("END AS ISPass, ")
            .Append("( SELECT GPA FROM arGradeSystemDetails WHERE GrdSysDetailId = t10.GrdSysDetailId) AS GPA,")
            .Append("( SELECT IsCreditsAttempted FROM arGradeSystemDetails WHERE GrdSysDetailId = t10.GrdSysDetailId) AS IsCreditsAttempted,")
            .Append("( SELECT IsCreditsEarned FROM arGradeSystemDetails WHERE GrdSysDetailId = t10.GrdSysDetailId) AS IsCreditsEarned,")
            .Append("( SELECT    IsInGPA FROM arGradeSystemDetails WHERE GrdSysDetailId = t10.GrdSysDetailId) AS IsInGPA, ")
            .Append("t10.StuEnrollId ,t10.IsCourseCompleted ")
            .Append("FROM arTransferGrades t10,arReqs t20,arTerm t30,dbo.arCourseEquivalent t40 ")
            .Append(" WHERE t10.TermId = t30.TermId AND t10.ReqId = t20.ReqId AND t10.GrdSysDetailId IS NOT NULL AND t10.ReqId=t40.ReqId AND t10.StuEnrollId = ? ")
            .Append(" ) A ")
            .Append("		          WHERE P.StuEnrollId=A.StuEnrollId AND P.ReqId=A.ReqId AND ((A.ISPass = 1 AND A.IsCourseCompleted = 1) OR A.IsCourseCompleted = 0)) ")
            .Append(" and NOT EXISTS (SELECT * ")
            .Append("		        FROM arResults A,arClassSections B,arTerm C,syStatuses D,arReqs E ")
            .Append("		        WHERE A.StuEnrollId=? AND A.GrdSysDetailId IS NULL AND A.TestId=B.ClsSectionId ")
            ' AND B.CampusId=? ")
            '.Append("		        AND B.TermId=C.TermId AND C.StartDate <= '" & Date.Now & "' AND C.EndDate >= '" & Date.Now & "'")
            .Append("		        AND B.TermId=C.TermId AND C.StatusId=D.StatusId ")    'AND D.Status='Active' 
            .Append("		        AND B.ReqId=E.ReqId AND A.StuEnrollId=P.StuEnrollId AND P.ReqId=B.ReqId) ")
            .Append("ORDER BY P.CampDescrip,P.Code,P.Req,P.PrgVerDescrip")
        End With

        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query       
        db.OpenConnection()

        Try
            Dim ds As DataSet
            ds = db.RunParamSQLDataSet(sb.ToString)

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return ds

        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw ex
            Else
                Throw ex.InnerException
            End If
        End Try
    End Function

    Public Function GetRemainingCourses_sp(ByVal stuEnrollId As String, Optional ByVal termId As String = "") As DataSet

        'for some reason this term id is being passed from the page to the facade to the data layer with tic marks around the value - remove them before using
        termId = If(termId = String.Empty, "00000000-0000-0000-0000-000000000000", termId.Substring(1, termId.Length - 2))
        Dim myDataSet As New DataSet()
        Using myConnection As New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
            Using myCommand As New SqlCommand("USP_GetRemainingCourses", myConnection)
                myCommand.CommandType = CommandType.StoredProcedure
                myCommand.Parameters.Add(New SqlParameter("@StuEnrollId", SqlDbType.UniqueIdentifier)).Value = New Guid(stuEnrollId)
                myCommand.Parameters.Add(New SqlParameter("@TermId", SqlDbType.UniqueIdentifier)).Value = New Guid(termId)
                Dim mySqlDataAdapter As New SqlDataAdapter(myCommand)
                mySqlDataAdapter.Fill(myDataSet)

            End Using
        End Using
        Return myDataSet
    End Function


    Public Function IsContinuingEdPrgVersion(ByVal stuEnrollId As String) As Boolean
        'connect to the database
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT ")
            .Append("       IsContinuingEd ")
            .Append("FROM   arPrgVersions ")
            .Append("WHERE  prgVerId=(SELECT prgVerId FROM arStuEnrollments WHERE StuEnrollId=?) ")
        End With

        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query       
        db.OpenConnection()

        Try
            Dim s As String
            s = db.RunParamSQLScalar(sb.ToString)

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return s

        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw ex
            Else
                Throw ex.InnerException
            End If
        End Try
    End Function

    Public Function GetRemainingCoursesForContinuingEdProgVersion(ByVal stuEnrollId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        With sb
            '   Build query that:
            '       1. retrieves all courses regardless of the program version they belong to.
            '       2. excludes courses already taken with a passing grade.
            '       3. excludes courses already scheduled for.
            .Append("SELECT * FROM ( ")
            .Append("           SELECT DISTINCT E.StuEnrollId,S.LastName,S.FirstName,E.ExpGradDate,E.PrgVerId,P.PrgVerDescrip, ")
            .Append("           R.ReqId as ReqId,R.Descrip As Req,R.Code As Code,R.Credits,R.Hours,'False' AS IsRequired,E.StartDate,E.ExpStartDate  ")
            .Append(" ,(select CampDescrip from syCampuses where CampusId=E.CampusId) as CampDescrip ")
            .Append("           FROM arStuEnrollments E,adLeads S,arPrgVersions P,arReqs R,syStatuses A ")
            .Append("           WHERE E.StudentId = S.StudentId And E.PrgVerId=P.PrgVerId ")
            .Append("           AND E.StuEnrollId=? AND R.StatusId=A.StatusId AND A.Status='Active') P ")
            '-- This portion gets the courses that the student has already taken with a passing grade
            .Append("WHERE NOT EXISTS ( ")
            .Append("                   SELECT * FROM  ")
            .Append("                       (SELECT DISTINCT ")
            .Append("                           t4.TermId,t3.TermDescrip,t4.ReqId,t2.Code,t2.Descrip,t2.Credits,t2.Hours,t3.StartDate, t3.EndDate, t1.GrdSysDetailId, ")
            .Append("                           (SELECT Grade FROM arGradeSystemDetails WHERE GrdSysDetailId=t1.GrdSysDetailId)as Grade,  ")
            .Append("                           (SELECT IsPass FROM arGradeSystemDetails WHERE GrdSysDetailId=t1.GrdSysDetailId) as IsPass,  ")
            .Append("                           (SELECT GPA FROM arGradeSystemDetails WHERE GrdSysDetailId=t1.GrdSysDetailId) as GPA,  ")
            .Append("                           (SELECT IsCreditsAttempted FROM arGradeSystemDetails WHERE GrdSysDetailId=t1.GrdSysDetailId) as IsCreditsAttempted,  ")
            .Append("                           (SELECT IsCreditsEarned FROM arGradeSystemDetails WHERE GrdSysDetailId=t1.GrdSysDetailId) as IsCreditsEarned,  ")
            .Append("                           (SELECT IsInGPA FROM arGradeSystemDetails WHERE GrdSysDetailId=t1.GrdSysDetailId) as IsInGPA, t1.StuEnrollId  ")
            .Append("                       FROM arResults t1,arReqs t2,arTerm t3,arClassSections t4  ")
            .Append("                       WHERE t1.TestId = t4.ClsSectionId And t4.TermId = t3.TermId And t4.ReqId = t2.ReqId ")
            .Append("                           AND t1.GrdSysDetailId IS NOT NULL ")
            .Append("                           AND t1.StuEnrollId=? ")
            .Append("                       UNION  ")
            .Append("                       SELECT DISTINCT ")
            .Append("                           t10.TermId,t30.TermDescrip,t10.ReqId,t20.Code,t20.Descrip,t20.Credits,t20.Hours,t30.StartDate,t30.EndDate,t10.GrdSysDetailId,")
            .Append("                           (SELECT Grade FROM arGradeSystemDetails WHERE GrdSysDetailId=t10.GrdSysDetailId) as Grade,  ")
            .Append("                           (SELECT IsPass FROM arGradeSystemDetails WHERE GrdSysDetailId=t10.GrdSysDetailId) as IsPass,  ")
            .Append("                           (SELECT GPA FROM arGradeSystemDetails WHERE GrdSysDetailId=t10.GrdSysDetailId) as GPA,  ")
            .Append("                           (SELECT IsCreditsAttempted FROM arGradeSystemDetails WHERE GrdSysDetailId=t10.GrdSysDetailId) as IsCreditsAttempted, ")
            .Append("                           (SELECT IsCreditsEarned FROM arGradeSystemDetails WHERE GrdSysDetailId=t10.GrdSysDetailId) as IsCreditsEarned,  ")
            .Append("                           (SELECT IsInGPA FROM arGradeSystemDetails WHERE GrdSysDetailId=t10.GrdSysDetailId) as IsInGPA, t10.StuEnrollId  ")
            .Append("                       FROM arTransferGrades t10,arReqs t20,arTerm t30  ")
            .Append("                       WHERE t10.TermId = t30.TermId And t10.ReqId = t20.ReqId ")
            .Append("                           AND t10.GrdSysDetailId IS NOT NULL  ")
            .Append("                           AND t10.StuEnrollId=? ) A  ")
            .Append("               WHERE P.StuEnrollId=A.StuEnrollId AND P.ReqId=A.ReqId AND A.IsPass=1) ")
            '-- This portion gets the courses the student is already scheduled for
            .Append("AND NOT EXISTS ( ")
            .Append("               SELECT * FROM arResults A,arClassSections B,arTerm C,syStatuses D,arReqs E ")
            .Append("               WHERE A.StuEnrollId=? ")
            .Append("                   AND A.GrdSysDetailId IS NULL ")
            .Append("                   AND A.TestId=B.ClsSectionId ")
            '.Append("                   AND B.CampusId=? ")
            .Append("                   AND B.TermId=C.TermId AND C.StatusId=D.StatusId ")
            .Append("                   AND B.ReqId=E.ReqId AND A.StuEnrollId=P.StuEnrollId AND P.ReqId=B.ReqId) ")
            .Append("ORDER BY CampDescrip,P.Code,P.Req,P.PrgVerDescrip ")
        End With

        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query       
        db.OpenConnection()

        Try
            Dim ds As DataSet
            ds = db.RunParamSQLDataSet(sb.ToString)

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return ds

        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw ex
            Else
                Throw ex.InnerException
            End If
        End Try
    End Function

    Public Function GetRemainingCoursesForContinuingEdProgVersion_sp(ByVal stuEnrollId As String) As DataSet
        Dim myConnection As SqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim myCommand As SqlCommand = New SqlCommand("USP_GetRemainingCoursesForContinuingEdProgVersion", myConnection)
        Dim myDataSet As New DataSet()
        myCommand.CommandType = CommandType.StoredProcedure
        myCommand.Parameters.Add(New SqlParameter("@StuEnrollId", SqlDbType.UniqueIdentifier)).Value = New Guid(stuEnrollId)
        Dim mySqlDataAdapter As New SqlDataAdapter(myCommand)
        Try
            mySqlDataAdapter.Fill(myDataSet)
            Return myDataSet

        Finally
            myCommand.Connection.Close()
        End Try
    End Function

    Public Function UpdateSingleGrade(ByVal resultId As String, ByVal testId As String, ByVal grdSysDetailId As String, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess
        db.ConnectionString = conString

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            Select Case testId
                Case "00000000-0000-0000-0000-000000000000"
                    With sb
                        .Append("UPDATE arTransferGrades ")
                        .Append(" Set GrdSysDetailId = ?, ")
                        .Append("IsCourseCompleted = 1 ,")
                        .Append("IsGradeOverridden = 1 ,")
                        .Append("GradeOverriddenBy= ?, ")
                        .Append("GradeOverriddenDate= ?, ")
                        .Append(" ModUser = ?, ModDate = ? ")
                        .Append("WHERE TransferId = ? ")
                    End With
                Case Else
                    With sb
                        .Append("UPDATE arResults ")
                        .Append(" Set GrdSysDetailId = ?, ")
                        .Append("IsCourseCompleted = 1 ,")
                        .Append("IsGradeOverridden = 1 ,")
                        .Append("GradeOverriddenBy= ?, ")
                        .Append("GradeOverriddenDate= ?, ")
                        .Append(" ModUser = ?, ModDate = ? ")
                        .Append("WHERE ResultId = ? ")
                    End With
            End Select

            '   add parameters values to the query

            '   GrdSysDetailId
            db.AddParameter("@GrdSysDetailId", grdSysDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            db.AddParameter("@GradeOverriddenBy", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@GradeOverriddenDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate

            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ResultId or TransferId
            db.AddParameter("@ResultIdOrTransferId", resultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            'Return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Function UpdateSingleGradeTermAllEnrollments(ByVal resultId As String, ByRef TermId As String, ByVal reqId As String, ByVal testId As String, ByVal grdSysDetailId As String, ByVal user As String, ByVal reqDescr As String, ByVal bTermChanged As Boolean, ByVal stuEnrollId As String) As String

        Dim sb1 As New StringBuilder
        Dim isNotTransfer As Boolean = True

        '   Connect to the database
        Dim db As New DataAccess
        db.ConnectionString = conString


        If testId = "00000000-0000-0000-0000-000000000000" Then
            testId = Nothing
            isNotTransfer = False
        End If

        If String.IsNullOrEmpty(TermId) Then
            TermId = Nothing
        End If

        If String.IsNullOrEmpty(grdSysDetailId) Then
            grdSysDetailId = Nothing
        End If

        If isNotTransfer And bTermChanged Then
            Dim attCnt As Integer = 0
            With sb1
                .Append("select COUNT(*) from atClsSectAttendance where ClsSectionId=? AND StuEnrollId=(select stuenrollid from arResults where resultid=?)")
            End With
            db.AddParameter("@testId", testId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@resultId", resultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            Try
                attCnt = db.RunParamSQLScalar(sb1.ToString)
                If attCnt > 0 Then Return "You cannot change the term,because attendance has been already posted."
            Catch ex As Exception
                Return DALExceptions.BuildErrorMessage(ex)
            Finally
                db.ClearParameters()

            End Try

        End If

        sb1.Clear()
        Dim weightingLevel As String = ""
        If Not myAdvAppSettings.AppSettings("GradeBookWeightingLevel") Is Nothing Then
            weightingLevel = myAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower()
        End If

        Dim isClassInTerm As Boolean = True
        If Not reqId = "" And isNotTransfer Then
            With sb1
                .Append("Select Top 1 ClsSectionId from arClassSections where TermId=? and ReqId=? ")
            End With
            db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ReqId", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            Try
                Dim ret As Object

                ret = (db.RunParamSQLScalar(sb1.ToString))
                If ret Is Nothing Then
                    isClassInTerm = False
                End If

            Catch ex As Exception
                isClassInTerm = False
            Finally
                sb1.Remove(0, sb1.Length)
                db.ClearParameters()
            End Try
        End If

        If Not isClassInTerm Then
            Return "There is no class for " & reqDescr & " in this term."

        Else
            '   ResultId or TransferId
            db.AddParameter("@ResultIdOrTransferId", resultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ReqId", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@TestId", testId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@GrdSysDetailId", grdSysDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@GradBookWeightingLevel", weightingLevel, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@User", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            Try
                db.RunParamSQLDataSetUsingSP("dbo.UpdateTranscriptCompletedClass")
                Return ""
            Catch ex As Exception
                Return DALExceptions.BuildErrorMessage(ex)
            Finally
                db.CloseConnection()
            End Try

        End If

    End Function


    Public Function DeleteSingleGrade(ByVal resultId As String, ByVal testId As String, ByVal user As String, ByVal stuEnrollId As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            Select Case testId
                Case "00000000-0000-0000-0000-000000000000"
                    With sb
                        .Append("DELETE FROM arTransferGrades ")
                        .Append("WHERE TransferId = ? ")
                    End With
                Case Else
                    With sb
                        .Append("DELETE FROM arResults ")
                        .Append("WHERE ResultId = ? ;")
                        .Append("delete from dbo.arGrdBkResults where ")
                        .Append(" ClsSectionId= ? and stuEnrollId= ? ;")
                        .Append("delete from dbo.atClsSectAttendance where ")
                        .Append(" ClsSectionId= ? and stuEnrollId= ? ;")
                    End With
            End Select

            '   add parameters values to the query

            '   ResultId or TransferId
            db.AddParameter("@ResultIdOrTransferId", resultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            If testId <> "00000000-0000-0000-0000-000000000000" Then
                db.AddParameter("@TestId", testId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@TestId", testId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            'Return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    ''' <summary>
    ''' Delete Grade in all enrollments
    ''' </summary>
    ''' <param name="resultId"></param>
    ''' <param name="testId"></param>
    ''' <param name="user"></param>
    ''' <param name="stuEnrollId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DeleteSingleGradeAllEnrollments(resultId As String, testId As String, user As String, stuEnrollId As String) As String

        If testId = "00000000-0000-0000-0000-000000000000" Then
            testId = Nothing
        End If

        '   Connect to the database
        Dim connectionString = myAdvAppSettings.AppSettings("ConnectionString")
        Dim conn = New SqlConnection(connectionString)
        Dim comm = New SqlCommand("dbo.DeleteTranscriptCompletedClass", conn)
        comm.CommandType = CommandType.StoredProcedure

        comm.Parameters.AddWithValue("@ResultIdOrTransferId", resultId)
        comm.Parameters.AddWithValue("@TestId", testId)
        comm.Parameters.AddWithValue("@StuEnrollId", stuEnrollId)

        conn.Open()
        Try
            comm.ExecuteNonQuery()
            Return String.Empty
        Finally
            conn.Close()
        End Try
    End Function

    Public Function UpdateSingleClassSectionId(ByVal resultId As String, ByVal testId As String, ByVal clsSectionIdOrTermId As String, ByVal user As String, Optional ByVal reqId As String = "", Optional ByVal reqDesc As String = "") As String

        '   Connect to the database
        Dim db As New DataAccess

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            Dim sb1 As New StringBuilder
            Dim strClassSectionId As String = ""
            'added by Theresa G on Sep 13 2010 to fix mantis
            '19727: The attendance already posted is removed for a class section of the course on transcript tab. 
            If testId <> "00000000-0000-0000-0000-000000000000" Then
                Dim attCnt As Integer = 0
                With sb1
                    .Append("select COUNT(*) from atClsSectAttendance where ClsSectionId=? AND StuEnrollId=(select stuenrollid from arResults where resultid=?)")
                End With
                db.AddParameter("@testId", testId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@resultId", resultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                Try
                    attCnt = db.RunParamSQLScalar(sb1.ToString)
                    If attCnt > 0 Then Return "You cannot change the class section,because attendance has been already posted."
                Catch ex As Exception
                    strClassSectionId = 0
                Finally
                    sb1.Remove(0, sb1.Length)
                    db.ClearParameters()
                End Try

            End If

            If Not reqId = "" Then
                With sb1
                    .Append("Select Top 1 ClsSectionId from arClassSections where TermId=? and ReqId=? ")
                End With
                db.AddParameter("@TermId", clsSectionIdOrTermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ReqId", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                Try
                    strClassSectionId = (db.RunParamSQLScalar(sb1.ToString)).ToString
                Catch ex As Exception
                    strClassSectionId = ""
                Finally
                    sb1.Remove(0, sb1.Length)
                    db.ClearParameters()
                End Try
            End If
            If testId <> "00000000-0000-0000-0000-000000000000" And strClassSectionId = "" Then
                Return "There is no class for " & reqDesc & " in this term."

            End If
            Select Case testId
                Case "00000000-0000-0000-0000-000000000000"
                    With sb
                        .Append("UPDATE arTransferGrades ")
                        .Append(" Set TermId = ?, ")
                        .Append(" ModUser = ?, ModDate = ? ")
                        .Append("WHERE TransferId = ? ")
                    End With
                Case Else
                    With sb
                        .Append("UPDATE arResults ")
                        .Append(" Set TestId = ?, ")
                        .Append(" ModUser = ?, ModDate = ? ")
                        .Append("WHERE ResultId = ? ; ")
                        If myAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" Then
                            .Append("UPDATE arGrdBkResults ")
                            .Append(" Set ClsSectionId = ?, ")
                            .Append(" ModUser = ?, ModDate = ? ")
                            .Append(" WHERE ClsSectionId = ? and StuEnrollid =(select stuEnrollid from arResults where resultId = ? ) ; ")
                        End If

                    End With
            End Select

            '   add parameters values to the query

            '   ClsSectionId
            Dim now As Date = Date.Now
            Select Case testId
                Case "00000000-0000-0000-0000-000000000000"
                    db.AddParameter("@ClssectionIdOrTermId", clsSectionIdOrTermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    '   ModDate

                    db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                    '   ResultId or TransferId
                    db.AddParameter("@ResultIdOrTransferId", resultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Case Else
                    db.AddParameter("@ClssectionIdOrTermId", strClassSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    '   ModDate
                    db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                    '   ResultId or TransferId
                    db.AddParameter("@ResultIdOrTransferId", resultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    'update arGrdBkResults
                    If myAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" Then

                        db.AddParameter("@ClssectionIdOrTermId", strClassSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                        '   ModDate
                        db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                        '   ResultId or TransferId
                        db.AddParameter("@ClsSectionId", testId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                        db.AddParameter("@ResultIdOrTransferId", resultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    End If
            End Select



            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            'Return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetAllTermsInWhichAReqHasBeenTaught(ByVal resultId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       CS.ClsSectionId, ")
            .Append("       T.TermDescrip, ")
            .Append("       T.StartDate ")
            .Append("FROM 	arClassSections CS, arTerm T ")
            .Append("WHERE ")
            .Append("       CS.TermId=T.TermId ")
            .Append("AND	ReqId=(select ReqId from arClassSections where ClsSectionId=(Select TestId from arResults where ResultId = ? )) ")
            .Append("ORDER BY ")
            .Append("       T.StartDate ")
        End With

        '   ResultId
        db.AddParameter("@ResultId", resultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetStudentName(ByVal StuEnrollId As String) As String

        'connect to the database
        Dim db As New DataAccess
        Dim strSSN, strStudentID As String
        Dim strFirstName, strLastName, strMiddleName As String

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("SELECT * from adLeads where StudentId in  ")
            .Append("(Select StudentId from arStuEnrollments where StuEnrollId = ?) ")

        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@StudentId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)


        While dr.Read()

            'set properties with data from DataReader
            strFirstName = dr("FirstName").ToString()
            strLastName = dr("LastName").ToString()
            strMiddleName = dr("MiddleName").ToString()
            If Not (dr("SSN") Is DBNull.Value) Then
                strSSN = dr("SSN").ToString()
            End If
            If Not (dr("StudentNumber") Is DBNull.Value) Then
                strStudentID = dr("StudentNumber")
            End If

        End While
        If Not dr.IsClosed Then dr.Close()
        db.ClearParameters()
        sb.Remove(0, sb.Length)


        'Close(Connection)
        db.CloseConnection()

        'Return BankInfo
        Return strFirstName & ";" & strLastName & ";" & strMiddleName & ";" & strSSN & ";" & strStudentID
    End Function
    Public Function GetPrograms(ByVal campusId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            .Append("SELECT DISTINCT pr.ProgId,")
            ''the Shift id added to the program description

            .Append(" case when (select ShiftDescrip from arShifts where arShifts.shiftid=Pr.shiftid ) is null  then ")
            .Append(" Pr.ProgDescrip ")
            .Append(" else ")
            .Append(" Pr.ProgDescrip + ' (' + (select ShiftDescrip from arShifts where arShifts.shiftid=Pr.shiftid) + ')'  ")
            .Append(" end as ProgDescrip  ")

            .Append(" ,pr.ProgCode  ")
            .Append("            FROM arPrograms pr  ")
            .Append("            WHERE (CampGrpId IN(SELECT CampGrpId  ")
            .Append("            FROM syCmpGrpCmps WHERE CampusId = ? ")
            .Append("            AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL'))  ")
            .Append("            OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL'))  ")
            .Append("            ORDER BY ProgDescrip ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@campid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)

    End Function

    Public Function GetTerms(ByVal campusId As String, ByVal progId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")
        'build the sql query
        With sb
            'With subqueries
            .Append("SELECT DISTINCT tm.TermId,TermDescrip,tm.StartDate  ")
            .Append("FROM arTerm tm, syStatuses st  ")
            .Append("WHERE tm.StatusId=st.StatusId ")
            .Append("AND st.Status='Active' ")
            .Append("AND (tm.CampGrpId IN(SELECT CampGrpId ")
            .Append("FROM syCmpGrpCmps ")
            .Append("WHERE CampusId = ? ")
            .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            If progId <> "" Then
                .Append("AND (tm.ProgId = ? OR tm.ProgId IS NULL) ")
            End If
            .Append("ORDER BY tm.StartDate,TermDescrip ")
        End With

        'Add the campusid the parameter list
        db.AddParameter("@campid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If progId <> "" Then
            db.AddParameter("@progid", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)

    End Function

    Public Function GetAvailableClassesRestrictedByStudentStart(ByVal campusId As String, ByVal progId As String, ByVal termId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            .Append("SELECT R.ClsSectionId,R.ClsSection,R.ReqId,R.ShiftId,R.Code,R.Descrip,R.StartDate,R.EndDate,R.MaxStud-R.Registered AS Available,R.TermId ")
            .Append("FROM ")
            .Append("(SELECT cs.ClsSectionId,cs.ClsSection,cs.MaxStud,cs.ReqId,cs.ShiftId,rq.Code,rq.Descrip,cs.StartDate,cs.EndDate,tm.TermId, ")
            .Append("   (SELECT COUNT(*) ")
            .Append("    FROM arResults ar ")
            .Append("    WHERE ar.TestId = cs.ClsSectionId) AS Registered ")
            .Append("FROM arClassSections cs, arClassSectionTerms ct, arTerm tm, arReqs rq ")
            .Append("WHERE cs.ClsSectionId=ct.ClsSectionId ")
            .Append("AND ct.TermId=tm.TermId ")
            .Append("AND cs.ReqId=rq.ReqId ")
            .Append("AND cs.EndDate > GetDate() ")
            .Append("AND cs.CampusId = ? ")
            .Append("AND (cs.StudentStartDate IS NOT NULL  or cs.LeadGrpId is not null or cs.CohortStartDate is not null)")

            If progId <> "" Then
                .Append("AND (tm.ProgId = ? or tm.ProgId is null) ")
            End If
            If termId <> "" Then
                .Append("AND ct.TermId = ? ")
            End If

            .Append(") R ")
            .Append("WHERE R.MaxStud-R.Registered > 0 ")
            .Append("ORDER BY R.EndDate DESC ")

        End With

        db.AddParameter("@campid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If progId <> "" Then
            db.AddParameter("@progid", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If termId <> "" Then
            db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)

    End Function

    Public Function GetAvailableClassesRestrictedByStudentStart_SP(ByVal campusId As String, ByVal progId As String, ByVal termId As String) As DataTable
        Dim db As New SQLDataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        If termId = "" And progId = "" Then
            db.AddParameter("@campusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Return db.RunParamSQLDataSet_SP("dbo.usp_GetAvailableClassesRestrictedByStudentStart").Tables(0)
        ElseIf progId <> "" And termId = "" Then
            db.AddParameter("@campusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@progId", New Guid(progId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Return db.RunParamSQLDataSet_SP("dbo.usp_GetAvailableClassesRestrictedByStudentStartWithProgram").Tables(0)
            db.CloseConnection()
        ElseIf progId = "" And termId <> "" Then
            db.AddParameter("@campusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@termId", New Guid(termId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Return db.RunParamSQLDataSet_SP("dbo.usp_GetAvailableClassesRestrictedByStudentStartWithTerm").Tables(0)
            db.CloseConnection()
        ElseIf progId <> "" And termId <> "" Then
            db.AddParameter("@campusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@progId", New Guid(progId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@termId", New Guid(termId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Return db.RunParamSQLDataSet_SP("dbo.usp_GetAvailableClassesRestrictedByStudentStartWithProgramAndTerm").Tables(0)
            db.CloseConnection()
        End If


    End Function
    Public Function GetAvailableStudentsForClassRestrictedByStartDate(ByVal clsSectionId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            .Append("SELECT se.StuEnrollId,se.ShiftId,pg.ProgDescrip,st.LastName,st.FirstName,pv.PrgVerId,pg.ProgId ")
            .Append("FROM arStuEnrollments se, arPrgVersions pv,arPrograms pg, adLeads st, syStatusCodes sc ")
            .Append("WHERE se.PrgVerId=pv.PrgVerId ")
            .Append("AND se.StudentId=st.StudentId ")
            .Append("AND pv.ProgId=pg.ProgId ")
            .Append("AND se.StatusCodeId=sc.StatusCodeId ")
            .Append("AND sc.SysStatusId IN(7,9,13,20) ")
            .Append("AND( pv.ProgId=(SELECT tm.ProgId ")
            .Append("               FROM arClassSections cs, arClassSectionTerms ct, arTerm tm  ,arProgVerDef pvd   ")
            .Append("               WHERE cs.ClsSectionId=ct.ClsSectionId ")
            .Append("               AND ct.TermId=tm.TermId ")
            .Append("     and Pv.PrgVerId=Pvd.PrgVerId and Pvd.Reqid=cs.Reqid ")
            .Append("               AND cs.ClsSectionId = ?) ")
            .Append(" or (SELECT isnull(cast(tm.ProgId as char(50)),'')  FROM arClassSections cs, arClassSectionTerms ct, arTerm tm   ,arProgVerDef pvd  ")
            .Append(" WHERE cs.ClsSectionId = ct.ClsSectionId And ct.TermId = TM.TermId ")
            .Append("     and Pv.PrgVerId=Pvd.PrgVerId and Pvd.Reqid=cs.Reqid ")
            .Append(" AND cs.ClsSectionId = ? ) = '' ) ")
            .Append(" AND ( se.ExpStartDate=(SELECT StudentStartDate ")
            .Append("                       FROM arClassSections ")
            .Append("                       WHERE ClsSectionId = ?) ")
            .Append(" or se.stuEnrollid in (select stuEnrollid from adLeadByLeadGroups a,arClassSections b ")
            .Append(" where a.LeadGrpId=b.LeadGrpId and b.ClsSectionId =  ? ) ")
            .Append(" or se.CohortStartDate=(select CohortStartDate ")
            .Append("                        from arClassSections ")
            .Append("                        where ClsSectionId = ?) ")
            .Append("      ) ")


            'for equivalent courses
            .Append(" and se.stuenrollid not in ")
            .Append(" (select stuenrollid from arResults,arGradesystemdetails  where ")
            .Append(" testid in (select ClsSectionId from arclassSections where reqid in( ")
            .Append(" Select equivreqid ")
            .Append(" from arCourseequivalent where reqid =(select reqid from arClassSections ")
            .Append(" where ClsSectionid= ? ))) and arResults.GrdSysDetailid=arGradesystemdetails.GrdSysDetailid and arGradesystemdetails.IsPass=1 ) ")

            ''If a student has been scheduled for a classsection then do not bring that stuydent for the same cklass section irrespective of whether the student passes or Fails
            ''For mantis issue 17539: QA: Student failing a course is showing up on the run schedule page to be scheduled in the same class section again.
            .Append("  and se.StuEnrollId not in  ")
            .Append(" (Select StuEnrollid from arresults where TestId=?) ")
            .Append(" ORDER BY se.ExpGradDate,st.LastName,st.FirstName ")
        End With

        db.AddParameter("@clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@clssectid2", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@clssectid2", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@clssectid2", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@clssectid3", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@clssectid3", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
        '
    End Function


    Public Function GetAvailableStudentsForClassRestrictedByStartDate_SP(ByVal clsSectionId As String, ByVal leadGrpId As String, campusid As String) As DataTable
        Dim db As New SQLDataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@clsSectionId", New Guid(clsSectionId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@leadGrpId", New Guid(leadGrpId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@campusid", New Guid(campusid), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Try
            Return db.RunParamSQLDataSet_SP("dbo.usp_GetAvailableStudentsForClassRestrictedByStartDate").Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try


    End Function

    Public Function GetAvailableStudentsForStudentGroups(ByVal clsSectionId As String, ByVal ParentClsSection As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            .Append("SELECT se.StuEnrollId,se.ShiftId,pg.ProgDescrip,st.LastName,st.FirstName,pv.PrgVerId,pg.ProgId ")
            .Append("FROM arStuEnrollments se, arPrgVersions pv,arPrograms pg, adLeads st, syStatusCodes sc ")
            .Append("WHERE se.PrgVerId=pv.PrgVerId ")
            .Append("AND se.StudentId=st.StudentId ")
            .Append("AND pv.ProgId=pg.ProgId ")
            .Append("AND se.StatusCodeId=sc.StatusCodeId ")
            .Append("AND sc.SysStatusId IN(7,9,13,20) ")
            .Append("AND( pv.ProgId=(SELECT tm.ProgId ")
            .Append("               FROM arClassSections cs, arClassSectionTerms ct, arTerm tm  ,arProgVerDef pvd   ")
            .Append("               WHERE cs.ClsSectionId=ct.ClsSectionId ")
            .Append("               AND ct.TermId=tm.TermId ")
            .Append("     and Pv.PrgVerId=Pvd.PrgVerId and Pvd.Reqid=cs.Reqid ")
            .Append("               AND cs.ClsSectionId = ?) ")
            .Append(" or (SELECT isnull(cast(tm.ProgId as char(50)),'')  FROM arClassSections cs, arClassSectionTerms ct, arTerm tm   ,arProgVerDef pvd  ")
            .Append(" WHERE cs.ClsSectionId = ct.ClsSectionId And ct.TermId = TM.TermId ")
            .Append("     and Pv.PrgVerId=Pvd.PrgVerId and Pvd.Reqid=cs.Reqid ")
            .Append(" AND cs.ClsSectionId = ? ) = '' ) ")
            .Append(" AND  se.ExpStartDate <> (SELECT StudentStartDate ")
            .Append("                       FROM arClassSections ")
            .Append("                       WHERE ClsSectionId = ?) ")
            .Append(" and  se.stuEnrollid in (select stuEnrollid from adLeadByLeadGroups a,arClassSections b ")
            .Append(" where a.LeadGrpId=b.LeadGrpId and b.ClsSectionId =  ? ) ")
            .Append(" ORDER BY se.ExpGradDate,st.LastName,st.FirstName ")
        End With

        db.AddParameter("@clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@clssectid2", ParentClsSection, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@clssectid2", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@clssectid3", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)

    End Function
    Public Function GetAvailableStudentsForClassRestrictedByStartDate(ByVal clsSectionId As String, ByVal ReqId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            .Append("SELECT se.StuEnrollId,se.ShiftId,pg.ProgDescrip,st.LastName,st.FirstName,pv.PrgVerId,pg.ProgId ")
            .Append("FROM arStuEnrollments se, arPrgVersions pv,arPrograms pg, adLeads st, syStatusCodes sc ")
            .Append("WHERE se.PrgVerId=pv.PrgVerId ")
            .Append("AND se.StudentId=st.StudentId ")
            .Append("AND pv.ProgId=pg.ProgId ")
            .Append("AND se.StatusCodeId=sc.StatusCodeId ")
            .Append("AND sc.SysStatusId IN(7,9,13,20) ")
            .Append("AND( pv.ProgId=(SELECT tm.ProgId ")
            .Append("               FROM arClassSections cs, arClassSectionTerms ct, arTerm tm ,arProgVerDef pvd ")
            .Append("               WHERE cs.ClsSectionId=ct.ClsSectionId ")
            .Append("               AND ct.TermId=tm.TermId ")
            .Append(" and Pv.PrgVerId=Pvd.PrgVerId and Pvd.Reqid=cs.Reqid ")
            .Append("               AND cs.ClsSectionId = ?) ")
            .Append(" or (SELECT isnull(cast(tm.ProgId as char(50)),'')  FROM arClassSections cs, arClassSectionTerms ct, arTerm tm ,arProgVerDef pvd  ")
            .Append(" WHERE cs.ClsSectionId = ct.ClsSectionId And ct.TermId = TM.TermId ")
            .Append(" and Pv.PrgVerId=Pvd.PrgVerId and Pvd.Reqid=cs.Reqid ")
            .Append(" AND cs.ClsSectionId = ? ) ='' ) ")
            .Append(" AND ( se.ExpStartDate=(SELECT StudentStartDate ")
            .Append("                       FROM arClassSections ")
            .Append("                       WHERE ClsSectionId = ?) ")
            .Append(" or se.stuEnrollid in (select stuEnrollid from adLeadByLeadGroups a,arClassSections b ")
            .Append(" where a.LeadGrpId=b.LeadGrpId and b.ClsSectionId =  ? )) ")

            'check for equivalent courses

            .Append(" and ?  not in ")
            .Append(" (select ClsSectionId  from arClassSections where Reqid in( ")
            .Append(" select c.EquivReqid from arResults a,arClassSections b,arCourseEquivalent  c where ")
            .Append(" a.TestId=b.ClsSectionId and a.StuEnrollId = se.StuEnrollId ")
            .Append(" and b.Reqid=c.Reqid and c.EquivReqid = ? ")
            .Append(" ))  ")

            .Append(" ORDER BY se.ExpGradDate,st.LastName,st.FirstName ")
        End With

        db.AddParameter("@clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@clssectid2", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@clssectid2", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@clssectid2", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@Reqid", ReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@clssectid3", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)

    End Function

    Public Function GetScheduleCount(ByVal clsSectionId As String, ByVal stuEnrollId As String, Optional ByVal Campusid As String = "") As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim count As Integer

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb

            .Append(" SELECT count(ar.StuEnrollId) as count ")
            .Append("               FROM arResults ar, arClassSections sn ")
            .Append("               WHERE ar.TestId=sn.ClsSectionId and ")
            If myAdvAppSettings.AppSettings("GradesFormat", Campusid).ToLower = "numeric" Then
                .Append(" ar.Score is null  ")
            Else
                .Append(" ar.GrdSysDetailId is null  ")
            End If

            .Append("               AND ar.StuEnrollId= ? ")
            .Append("               AND sn.ReqId = (SELECT ReqId ")
            .Append("                               FROM arClassSections ")
            .Append("                               WHERE ClsSectionId = ?)  ")

        End With

        db.AddParameter("@stuEnrollID", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.AddParameter("@clssectid2", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        count = db.RunParamSQLScalar(sb.ToString)

        Return count

    End Function

    Public Function RegisterStudentsForClasses(ByVal dtStudentClasses As DataTable, ByVal modUser As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dr As DataRow
        Dim result As String
        'Dim regDB As New RegisterDB
        db.OpenConnection()
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        Try

            For Each dr In dtStudentClasses.Rows
                With sb
                    .Append("INSERT INTO arResults(StuEnrollId,TestId,ModUser,ModDate) ")
                    .Append("VALUES(?,?,?,?) ")
                End With

                Dim sDate As DateTime = Utilities.GetAdvantageDBDateTime(Date.Now)

                db.AddParameter("sid", dr("StuEnrollId"), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("testid", dr("ClsSectionId"), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("user", modUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("date", sDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                db.ClearParameters()
                sb.Remove(0, sb.Length)


            Next


            groupTrans.Commit()
            For Each dr In dtStudentClasses.Rows
                'If SingletonAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "true" Or _
                'SingletonAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "yes" Then
                result = RegisterStudentForGradeBookComponents(dr("StuEnrollId").ToString, dr("ClsSectionId").ToString, modUser, Date.Now)
                'End If
            Next
            'If we are dealing with a school that wants to ignore the prereqs for Run Schedules
            'then we should also update any expected grad date where it is less than the end of
            'the last class that the student is scheduled for.
            If myAdvAppSettings.AppSettings("IgnorePrereqsForRunSchedules").ToLower = "true" Then
                UpdateExpectedGradDateToLastClassEndDate()
            End If
            Return ""
        Catch ex As Exception
            groupTrans.Rollback()
            If ex.InnerException Is Nothing Then
                Return ex.Message
            Else
                Return ex.InnerException.Message
            End If
        Finally
            db.CloseConnection()
        End Try
    End Function

    Public Function RegisterStudentForGradeBookComponents(ByVal StuEnrollId As String, ByVal ClsSectionId As String, ByVal user As String, ByVal rightNow As Date) As String
        Dim db As New DataAccess

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            'Modified by Balaji on 04/03/2009 to fix issue 13655
            'When a student is registered for a class, records were inserted in to arResults table for all schools
            'For Ross Type School (defined by ShowRossOnlyTabs entry in web.config), in addition to inserting records in to arresults, records were inserted into arGrdBkResults table
            'It was realized later that for non-ross type schools, if student is registered in an Externship class then record needs to be inserted into 
            'arGrdBkResults table, otherwise records will not show up in Post Externship Attendance and Attendance cannot be posted for the externship hours attended by the student
            If myAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "true" Or
              myAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "yes" Then
                With sb
                    .Append("insert into arGrdBkResults (GrdBkResultId, ClsSectionId, InstrGrdBkWgtDetailId, Comments, StuEnrollId, ModUser, ModDate) ")
                    .Append("select ")
                    .Append("		NewId() as GrdBkResultId,  ")
                    .Append("		CS.ClsSectionId, ")
                    .Append("		GBWD.InstrGrdBkWgtDetailId, ")
                    .Append("       GCT.Descrip, ")
                    .Append("		R.StuEnrollId, ")
                    .Append("		?, ")
                    .Append("		? ")
                    .Append("from	arResults R, arClassSections CS, arGrdBkWeights GBW, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT  ")
                    .Append("where	R.StuEnrollId=? ")
                    .Append("and		R.TestId=CS.ClsSectionId  ")
                    .Append("and		CS.ReqId=GBW.ReqId ")
                    .Append("and		GBW.InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId  ")
                    .Append("and		GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId  ")
                    .Append("and		GBW.EffectiveDate = (Select max(EffectiveDate) from arGrdBkWeights where ReqId=GBW.ReqId and EffectiveDate <= CS.StartDate) ")
                    .Append("and		not exists (select *  ")
                    .Append("					from arGrdBkResults  ")
                    .Append("					where ClsSectionId=CS.ClsSectionId ")
                    .Append("					and	  InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
                    .Append("					and	  StuEnrollId=?) ")
                    .Append("and    CS.ClsSectionId= ? ")
                End With
            Else
                With sb
                    .Append("insert into arGrdBkResults (GrdBkResultId, ClsSectionId, InstrGrdBkWgtDetailId, Comments, StuEnrollId, ModUser, ModDate) ")
                    .Append("select ")
                    .Append("		NewId() as GrdBkResultId,  ")
                    .Append("		CS.ClsSectionId, ")
                    .Append("		GBWD.InstrGrdBkWgtDetailId, ")
                    .Append("       GCT.Descrip, ")
                    .Append("		R.StuEnrollId, ")
                    .Append("		?, ")
                    .Append("		? ")
                    .Append("from	arResults R, arClassSections CS, arGrdBkWeights GBW, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT,arReqs RQ  ")
                    .Append("where	R.StuEnrollId=? ")
                    .Append("and		R.TestId=CS.ClsSectionId  ")
                    .Append("and		CS.ReqId=GBW.ReqId ")
                    .Append("and		GBW.InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId  ")
                    .Append("and		GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId and CS.ReqId=RQ.ReqId and RQ.IsExternShip=1 ")
                    .Append("and		GBW.EffectiveDate = (Select max(EffectiveDate) from arGrdBkWeights where ReqId=GBW.ReqId and EffectiveDate <= CS.StartDate) ")
                    .Append("and		not exists (select *  ")
                    .Append("					from arGrdBkResults  ")
                    .Append("					where ClsSectionId=CS.ClsSectionId ")
                    .Append("					and	  InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
                    .Append("					and	  StuEnrollId=?) ")
                    .Append("and    CS.ClsSectionId= ? ")
                End With
            End If
            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'stuEnrollId
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'stuEnrollId
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'stuEnrollId
            db.AddParameter("@ClsSectionId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   commit transaction
            groupTrans.Commit()

            '   return with no errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            ''   do not report sql lost connection
            'If Not groupTrans.Connection Is Nothing Then
            '    '   report an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
            'End If

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Sub UpdateExpectedGradDateToLastClassEndDate()
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("update arStuEnrollments ")
            .Append("set ExpGradDate=( ")
            .Append("                   select max(cs.EndDate) ")
            .Append("                   from arResults rs, arClassSections cs ")
            .Append("                   where rs.TestId=cs.ClsSectionId ")
            .Append("                   and rs.StuEnrollId=arStuEnrollments.StuEnrollId ")
            .Append("                ) ")
            .Append("from arStuEnrollments, syStatusCodes sc ")
            .Append("where arStuEnrollments.StatusCodeId=sc.StatusCodeId ")
            .Append("and sc.SysStatusId=9 ")
            .Append("and arStuEnrollments.ExpGradDate < (  ")
            .Append("                                       select max(cs.EndDate) ")
            .Append("                                       from arResults rs, arClassSections cs ")
            .Append("                                       where rs.TestId=cs.ClsSectionId ")
            .Append("                                       and rs.StuEnrollId=arStuEnrollments.StuEnrollId ")
            .Append("                                   ) ")
        End With

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

    End Sub

    Public Function GetGradeDescriptionsForEnrollment(ByVal stuEnrollId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("SELECT Distinct gsd.Grade,gsd.GPA, ")
            .Append("   (SELECT TOP 1 Cast(MinVal AS Varchar) + ' - ' + Cast(MaxVal AS varchar) ")
            .Append("    FROM arGradeScaleDetails gscd, arGradeScales gsc ")
            .Append("    WHERE gscd.GrdScaleId=gsc.GrdScaleId ")
            .Append("    AND gscd.GrdSysDetailId=gsd.GrdSysDetailId ")
            .Append("    AND gsc.GrdSystemId=gs.GrdSystemId) AS Range, ")
            .Append("    gsd.GradeDescription,gsd.quality ")
            .Append("FROM arStuEnrollments se, arPrgVersions pv, arGradeSystems gs, arGradeSystemDetails gsd ")
            .Append("WHERE StuEnrollId = ? ")
            .Append("AND se.PrgVerId=pv.PrgVerId ")
            .Append("AND pv.GrdSystemId=gs.GrdSystemId ")
            .Append("AND gs.GrdSystemId=gsd.GrdSystemId ")
            .Append(" order by gsd.GPA desc,gsd.Grade")
        End With

        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)

    End Function
    Public Function GetGradeDescriptionsForEnrollmentNotNULL(ByVal stuEnrollId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("SELECT Distinct gsd.Grade, ")
            .Append("    gsd.GradeDescription ")
            .Append("FROM arStuEnrollments se, arPrgVersions pv, arGradeSystems gs, arGradeSystemDetails gsd ")
            .Append("WHERE StuEnrollId = ? ")
            .Append("AND se.PrgVerId=pv.PrgVerId ")
            .Append("AND pv.GrdSystemId=gs.GrdSystemId ")
            .Append("AND gs.GrdSystemId=gsd.GrdSystemId AND gsd.GradeDescription is NOT NULL AND LEN(gsd.GradeDescription)>0 ")
            .Append(" order by gsd.Grade")
        End With

        db.AddParameter("@sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)

    End Function

    Public Function GetGradeDescriptionsForTranscript_SP(ByVal stuEnrollId As String) As DataSet
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@stuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        ds = db.RunParamSQLDataSet_SP("dbo.USP_GetGradeDescriptionsForTranscript")
        Try
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function

    Public Function OverrideGrd(ByVal PrgVerId As String, ByVal ReqId As String) As String
        'Dim ds As New DataSet
        'Dim da As OleDbDataAdapter

        Dim sGrdSysDetailId As String

        Try

            'Dim MyAdvAppSettings As AdvAppSettings
            'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            'Else
            '    MyAdvAppSettings = New AdvAppSettings
            'End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("select isnull(GrdSysDetailId,'00000000-0000-0000-0000-000000000000') as GrdSysDetailId from arProgVerDef where ReqId= ? and PrgVerId= ? ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ReqId", ReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            db.OpenConnection()
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                sGrdSysDetailId = dr("GrdSysDetailId").ToString

            End While

            'ds = db.RunParamSQLDataSet(sb.ToString)
            'sb.Remove(0, sb.Length)


            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception

        End Try

        'Return the datatable in the dataset
        Return sGrdSysDetailId

    End Function

    Public Sub UpdateOverRideGrade(ByVal GrdSysDetailId As String, ByVal PrgVerId As String, ByVal ReqId As String)


        Try

            'Dim MyAdvAppSettings As AdvAppSettings
            'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            'Else
            '    MyAdvAppSettings = New AdvAppSettings
            'End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("update arProgVerDef set GrdSysDetailId = ?  where ReqId= ? and PrgVerId= ? ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            If GrdSysDetailId = "" Then
                db.AddParameter("@GrdSysDetailId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@GrdSysDetailId", GrdSysDetailId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            db.AddParameter("@ReqId", ReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            db.OpenConnection()
            db.RunParamSQLExecuteNoneQuery(sb.ToString()) 'Execute the query
            db.CloseConnection()

        Catch ex As Exception

        End Try

        'Return the datatable in the dataset


    End Sub

    Public Function GetNumberOfClassesScheduledForStudent(ByVal stuEnrollId As String) As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim count As Integer

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb

            .Append(" SELECT count(ResultId) as count ")
            .Append(" FROM arResults ")
            .Append(" WHERE StuEnrollId=? ")
            .Append(" AND Score IS NULL ")
            .Append(" AND GrdSysDetailId IS NULL ")
        End With

        db.AddParameter("@stuEnrollID", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        count = db.RunParamSQLScalar(sb.ToString)

        Return count
    End Function
    Public Function GetEquivalentCourseDescrip(ByVal Reqid As String, ByVal stuEnrollID As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append(" select isnull(Descrip,'') as Descrip from arCourseEquivalent,arReqs where ")
            .Append(" arCourseEquivalent.Reqid=arReqs.Reqid and ")
            .Append(" EquivReqid= ? ")
            .Append(" and EquivReqid not in (select reqid from arClassSections a,arResults b where ")
            .Append(" a.ClsSectionId=b.TestId and StuEnrollId= ? ")
            .Append(" and Reqid= ? ")
            .Append(" UNION select reqid from arTransferGrades  where  ")
            .Append(" StuEnrollId= ?  and Reqid= ? ")
            .Append(" )")
        End With
        db.AddParameter("@ReqId", Reqid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ReqId", Reqid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ReqId", Reqid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLScalar(sb.ToString)
    End Function
    Public Function GetCourseDescrip(ByVal Reqid As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("select Descrip from arReqs where Reqid = ? ")
        End With
        db.AddParameter("@ReqId", Reqid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLScalar(sb.ToString)
    End Function
    Public Function GetCourseDescrip_SP(ByVal Reqid As String) As String
        Dim rtn As Object
        Dim db As New SQLDataAccess

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@reqid", New Guid(Reqid), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Try
            rtn = db.RunParamSQLScalar_SP("dbo.USP_GetCourseDescrip")
            Return rtn.ToString()
        Catch ex As Exception
        Finally
            db.CloseConnection()
        End Try
    End Function

    Public Function GetEquivalentReqId(ByVal Reqid As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim rtn As Object

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append(" select Reqid from arCourseEquivalent where EquivReqid= ? ")
        End With
        db.AddParameter("@ReqId", Reqid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        rtn = db.RunParamSQLScalar(sb.ToString)
        If rtn Is Nothing Then Return ""
        Return rtn.ToString
    End Function
    Public Function GetEquivalentReqId_SP(ByVal reqid As String) As String
        Dim rtn As Object
        Dim db As New SQLDataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@reqid", New Guid(reqid), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Try
            rtn = db.RunParamSQLScalar_SP("dbo.USP_GetEquivalentReqId")
            If rtn Is Nothing Then
                Return ""
            Else
                Return rtn.ToString()
            End If
        Catch ex As Exception
        Finally
            db.CloseConnection()
        End Try
        Return rtn.ToString
    End Function

    Public Sub UnRegisterCohortStartDateClasses(ByVal campusId As String, ByVal progId As String)
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dt As DataTable
        Dim stuEnrollId As String
        Dim stuCohortStartDate As String

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("select distinct rs.StuEnrollId,cs.CohortStartDate ")
            .Append("from arResults rs, arGradeSystemDetails gsd, arClassSections cs,arStuEnrollments se, arPrgVersions pv ")
            .Append("where rs.GrdSysDetailId=gsd.GrdSysDetailId ")
            .Append("and rs.TestId=cs.ClsSectionId ")
            .Append("and rs.StuEnrollId=se.StuEnrollId ")
            .Append("and cs.CohortStartDate=se.CohortStartDate ")
            .Append("and se.PrgVerId=pv.PrgVerId ")
            .Append("and gsd.IsPass=0 ")
            .Append("and se.CampusId = ? ")

            If progId <> "" Then
                .Append("AND pv.ProgId = ?  ")
            End If

        End With

        'Add the campusid the parameter list
        db.AddParameter("@campid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        If progId <> "" Then
            db.AddParameter("@progid", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        dt = (db.RunParamSQLDataSet(sb.ToString)).Tables(0)

        db.ClearParameters()
        sb.Remove(0, sb.Length - 1)


        'Process any records returned
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                'Store the StuEnrollId and CohortStartDate as we will need them later to
                'update the enrollments tab to the new CohortStartDate
                stuEnrollId = dr("StuEnrollId").ToString
                stuCohortStartDate = dr("CohortStartDate")

                'Unregister this enrollment for any classes that does not have a grade posted
                Dim sb2 As New StringBuilder
                Dim sb3 As New StringBuilder


                With sb2
                    .Append("delete from arResults ")
                    .Append("where StuEnrollId = ? ")
                    .Append("and GrdSysDetailId is null ")
                End With

                db.AddParameter("@stuenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb2.ToString)

                db.ClearParameters()

                'Update the enrollment cohort start date to the next available one
                With sb3
                    .Append("update arStuEnrollments ")
                    .Append("set CohortStartDate = (select min(cs.CohortStartDate) ")
                    .Append("                       from arTerm tm, arClassSections cs ")
                    .Append("                       where tm.ProgId=(select pv.ProgId ")
                    .Append("                                        from arPrgVersions pv, arPrograms pg ")
                    .Append("                                        where pv.ProgId=pg.ProgId ")
                    .Append("                                        and pv.PrgVerId=arStuEnrollments.PrgVerId) ")
                    .Append("                       and tm.TermId=cs.TermId ")
                    .Append("                       and cs.CohortStartDate > ? ")
                    .Append("                       and exists( ")
                    .Append("                                   select top 1 ReqId,EndDate ")
                    .Append("                                   from ")
                    .Append("                                   (select rs.TestId,gsd.Grade,cs.EndDate,cs.ReqId ")
                    .Append("                                    from arResults rs, arGradeSystemDetails gsd, arClassSections cs, arStuEnrollments se ")
                    .Append("                                    where rs.StuEnrollId = ? ")
                    .Append("                                    and rs.GrdSysDetailId=gsd.GrdSysDetailId ")
                    .Append("                                    and gsd.IsPass=0 ")
                    .Append("                                    and rs.TestId=cs.ClsSectionId ")
                    .Append("                                    and rs.StuEnrollId=se.StuEnrollId ")
                    .Append("                                    and cs.CohortStartDate=se.CohortStartDate) R ")
                    .Append("                                    where R.ReqId=cs.ReqId ")
                    .Append("                                    and cs.StartDate > R.EndDate ")
                    .Append("                                    order by EndDate desc ")
                    .Append("                                   ) ")
                    .Append("                       ) ")
                    .Append("where StuEnrollId = ? ")
                End With
                db.AddParameter("@cohortstartdate", stuCohortStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@stuenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@stuenrollid2", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb3.ToString)

                db.ClearParameters()


            Next
        End If



    End Sub

    ''Optional Parameter added by Saraswathi
    ''To remove the details of the oldProgramversion from arResults which are not graded

    Public Function ReScheduleStudentWithCohortStartDate(ByVal stuEnrollId As String, ByVal newCohortStartDate As String, ByVal modUser As String, ByVal campusId As String, Optional ByVal OldStuEnrollId As String = "") As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        'Dim dr As DataRow
        'Dim result As String
        'Dim regDB As New RegisterDB
        db.OpenConnection()
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        Try
            'First, delete any classes that the student is enrolled in that is not yet graded
            With sb
                .Append("delete from arResults ")

                ''Added by Saraswathi
                If OldStuEnrollId = "" Then
                    .Append(" where StuEnrollId=? ")
                Else
                    .Append(" where StuEnrollId in(?, ? ) ")

                End If
                .Append("and GrdSysDetailId is null ")
            End With

            db.AddParameter("@stuenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            ''Added by Saraswathi
            If Not OldStuEnrollId = "" Then
                db.AddParameter("@oldstuenrollid", OldStuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            End If
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Second, register the student in any classes for the new cohort start date that he/she 
            'has not already passed. Make certain to restrict the classes based on the campus, shift
            'and program that the student is in.
            With sb
                .Append("insert into arResults(StuEnrollId,TestId,ModUser,ModDate) ")
                .Append("(select distinct se.StuEnrollId,cs.ClsSectionid,?,convert(smalldatetime,getdate()) ")
                .Append("from arStuEnrollments se, arClassSections cs, arReqs rq, arTerm tm ")
                .Append("where cs.ReqId=rq.ReqId ")
                .Append("and se.StuEnrollId=? ")
                .Append("and cs.CohortStartDate=? ")
                .Append("and cs.CampusId = ? ")
                .Append("and se.ShiftId=cs.ShiftId ")
                .Append("and cs.TermId=tm.TermId ")
                .Append("and ( ")
                .Append("       tm.ProgId is null ")
                .Append("               or ")
                .Append("       tm.ProgId=(select pg.ProgId from arPrgVersions pv, arPrograms pg where pv.ProgId=pg.ProgId and pv.PrgVerId=se.PrgVerId ) ")
                .Append("       ) ")
                .Append("and not exists(( ")
                .Append("               select rs.StuEnrollId ")
                .Append("               from arResults rs, arClassSections cs2, arGradeSystemDetails gsd ")
                .Append("               where rs.TestId=cs2.ClsSectionId ")
                .Append("               and rs.GrdSysDetailId=gsd.GrdSysDetailId ")
                .Append("               and rs.StuEnrollId = ? ")
                .Append("               and gsd.IsPass=1 ")
                .Append("               and cs2.ReqId=rq.ReqId ")
                .Append("               ) ")

                ''added By saraswathi lakshmanan
                .Append("             Union (  select rs.StuEnrollId ")
                .Append("               from arTransferGrades rs,  arGradeSystemDetails gsd ")
                .Append("               where   ")
                .Append("             rs.GrdSysDetailId=gsd.GrdSysDetailId ")
                .Append("               and rs.StuEnrollId = ? ")
                .Append("               and gsd.IsPass=1 ")
                .Append("               and rs.ReqId=rq.ReqId ")
                .Append("               ) ))")


            End With

            db.AddParameter("@moduser", modUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@stuenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@csdate", newCohortStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@campusid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@stuenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@stuenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Third, update the student expected grad date to the end date of the last
            'class if it is less than that. We should not update it if it is greater
            'This is because that date could reflect externships etc. However, the
            'exp grad date should definitely not be less than the last class that the
            'student is scheduled for.
            With sb
                .Append("update arStuEnrollments ")
                .Append("set ExpGradDate=( ")
                .Append("                   select max(cs.EndDate) ")
                .Append("                   from arResults rs, arClassSections cs ")
                .Append("                   where rs.TestId=cs.ClsSectionId ")
                .Append("                   and rs.StuEnrollId=arStuEnrollments.StuEnrollId ")
                .Append("                ) ")
                .Append("from arStuEnrollments ")
                .Append("where arStuEnrollments.StuEnrollId = ? ")
            End With

            db.AddParameter("@stuenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Next we need to update the cohort start date itself
            With sb
                .Append("update arStuEnrollments ")
                .Append("set CohortStartDate = ? ")
                .Append("where StuEnrollId = ? ")
            End With

            db.AddParameter("@csdate", newCohortStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@stuenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)


            groupTrans.Commit()
            Return ""
        Catch ex As Exception
            groupTrans.Rollback()
            If ex.InnerException Is Nothing Then
                Return ex.Message
            Else
                Return ex.InnerException.Message
            End If
        Finally
            db.CloseConnection()
        End Try

    End Function

    Public Function GetExpGradDate(ByVal stuEnrollId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim obj As Object


        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            .Append("select ExpGradDate from arStuEnrollments where StuEnrollId = ? ")
        End With

        db.AddParameter("@stuEnrollID", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        obj = db.RunParamSQLScalar(sb.ToString)

        If Not obj Is DBNull.Value Then
            Return CDate(obj).ToShortDateString
        Else
            Return ""
        End If

    End Function

    Public Function IsStudentCurrentlyInSchool(ByVal stuEnrollId) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim bln As Boolean
        Dim cnt As Integer

        bln = False


        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            .Append("select count(*) ")
            .Append("from arStuEnrollments se, syStatusCodes sc ")
            .Append("where StuEnrollId = ? ")
            .Append("and se.StatusCodeId=sc.StatusCodeId ")
            ''Inschool Status added by saraswathi lakshmanan on June 12 2009
            ''7-FutureStart, 9-Currently attending, 10-LOA, Suapended 11, 20-Academic probabtion, 21- Suspension
            ''for mantis case 14829
            .Append("and sc.SysStatusId in(7,9,20,10,11,21,22) ")
        End With

        db.AddParameter("@stuEnrollID", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        cnt = CInt(db.RunParamSQLScalar(sb.ToString))

        If cnt > 0 Then
            Return True
        Else
            Return False
        End If




    End Function
    ' Added for Multiple Student Attendance
    Public Function GetHours(ByVal stuEnrollId As String, ByVal Otherwhere As String) As DataSet

        Dim ds As DataSet
        Dim db As New DataAccess

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder

        With sb

            .Append("   select S.firstname, replace(CONVERT(varchar(10), SCA.RecordDate,110),'-','/') as Date,  ")
            .Append("   SE.StuEnrollID, SS.ScheduleId,  SCA.RecordDate,SCA.SchedHours, SCA.ActualHours as Hours,SCA.IsTardy as Tardy,  ")
            .Append("   SCA.ModUser, SCA.ModDate,   SS.StartDate,SS.EndDate, SS.Active,  PS.UseFlexTime,  ")
            .Append("   PV.UseTimeClock,AUT.UnitTypeDescrip,'attendance' as Source,  SCA.PostByException   ")
            .Append("   from arStudentClockAttendance SCA, arStudentSchedules SS,arProgSchedules PS, arPrgVersions PV,  ")
            .Append("   arPrograms P, arStuEnrollments SE,adLeads S, arAttUnitType AUT where  ")
            .Append("   SCA.StuEnrollId = SS.StuEnrollId  and SCA.ScheduleId = SS.ScheduleId   ")
            .Append("   and SS.StuEnrollId = SE.StuEnrollId   and SE.StudentId = S.StudentId  ")
            .Append("   and SCA.ScheduleId = SS.ScheduleId  and SCA.ScheduleId = PS.ScheduleId  ")
            .Append("   and PS.PrgVerId = PV.PrgVerId  and PV.ProgId = P.ProgId   ")
            .Append("   and PV.UnitTypeId = AUT.UnitTypeId and SS.Active = 1 and SCA.ActualHours <> 9999  ")
            .Append("   and se.stuEnrollid = ? and SCA.RecordDate < '")
            .Append(Otherwhere)
            .Append("'")
            .Append("   union  ")
            .Append("   select   ")
            .Append("   S.firstname, replace(CONVERT(varchar(10), SCA.RecordDate,110),'-','/') as Date,  ")
            .Append("   SE.StuEnrollID,   SS.ScheduleId,   ASCA.MeetDate, SCA.ActualHours as Hours ,  ")
            .Append("   ASCA.Schedule, ASCA.Tardy as Tardy,'' as ModUser,    '' as ModDate,    SS.StartDate,  ")
            .Append("   SS.EndDate,    SS.Active,    PS.UseFlexTime,PV.UseTimeClock,   AUT.UnitTypeDescrip,  ")
            .Append("   'conversion' as Source, SCA.PostByException from   ")
            .Append("   atConversionAttendance ASCA ,arStudentSchedules SS, arProgSchedules PS,  arPrgVersions PV,  ")
            .Append("   arPrograms P,  arStuEnrollments SE, adLeads S,  arAttUnitType AUT ,arStudentClockAttendance SCA  ")
            .Append("   where SCA.StuEnrollId = SS.StuEnrollId and SS.StuEnrollId = SE.StuEnrollId  ")
            .Append("   and SE.StudentId = S.StudentId and PS.PrgVerId = PV.PrgVerId  ")
            .Append("   and PV.ProgId = P.ProgId  and PV.UnitTypeId = AUT.UnitTypeId  ")
            .Append("   and SS.Active = 1 and SCA.ActualHours <> 9999  ")
            .Append("   and se.stuEnrollid = ?  and SCA.RecordDate < '")
            .Append(Otherwhere)
            .Append("'")
            .Append("   order by SCA.Date  ")




        End With
        db.AddParameter("@stuEnrollID", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollID", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)



        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds
        'GetStudentAttendance(ds)

    End Function

    ''Reschedule Reason added to table arstuReschReason
    Public Function AddRescheduleReason(ByVal stuEnrollId As String, ByVal user As String, ByVal reschReason As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.OpenConnection()
        db.ConnectionString = conString

        Try
            With sb
                ''                .Append("insert into arStuReschReason(ReschReasonTypeId,Descrip,StuEnrollId,CampusId,ModUser,ModDate) ")
                .Append("insert into arStuReschReason(ReschReasonTypeId,Descrip,StuEnrollId,ModUser,ModDate) ")
                .Append("Values(?,?,?,?,?) ")
            End With

            db.AddParameter("@ReschReasonTypeId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@reasonDescrip", reschReason, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@stuenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'db.AddParameter("@campusid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            Return ""
        Catch ex As OleDbException
            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        Return ""

    End Function
    Public Function getCredits(ByVal prgVerid As String) As Decimal
        Dim db As New DataAccess
        Dim rtn As Decimal
        Dim sb As New StringBuilder

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            .Append("select credits from arPrgVersions where PrgVerid = ? ")
        End With

        db.AddParameter("@prgVerId", prgVerid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        rtn = CDec(db.RunParamSQLScalar(sb.ToString))
        Return rtn
    End Function
    Public Function getHours(ByVal reqid As String) As Decimal
        Dim db As New DataAccess
        Dim rtn As Decimal
        Dim sb As New StringBuilder


        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            .Append("select hours from arReqs where reqid = ? ")
        End With

        db.AddParameter("@reqid", reqid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        rtn = CDec(db.RunParamSQLScalar(sb.ToString))
        Return rtn
    End Function
    Public Function ComputeWithCreditsPerService(ByVal StuEnrollId As String, ByVal addcreditsbyservice As String, ByVal CourseId As String) As Decimal
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim decCreditsEarned As Decimal = 0.0

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("       select  " + vbCrLf)
            .Append("       			Sum(GBR.Score) as LabsCompletedByStudent, " + vbCrLf)
            .Append("                   (select Distinct isClinicsSatisfied from arresults where StuEnrollId=? " + vbCrLf)
            .Append("                   and TestId in (select ClsSectionId from arClassSections where ReqId=?)) as isClinicsSatisfied, " + vbCrLf)
            .Append(" GBR.InstrGrdBkWgtDetailId,GBWD.Number as RequiredNumberofLabs,GBWD.CreditsPerService " + vbCrLf)
            .Append("                   from " + vbCrLf)
            .Append("                   			(select * from arGrdBkResults where score is not null and clsSectionId in (select distinct ClsSectionId from arClassSections where ReqId=?)) GBR,  arGrdBkWeights GBW, " + vbCrLf)
            .Append("                   			arGrdBkWgtDetails GBWD,arGrdComponentTypes GDT " + vbCrLf)
            .Append("                   where " + vbCrLf)
            .Append("                   			GBR.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId and " + vbCrLf)
            .Append("                   			GBW.InstrGrdBkWgtId = GBWD.InstrGrdBkWgtId and  " + vbCrLf)
            .Append("                   GBWD.GrdComponentTypeId = GDT.GrdComponentTypeId " + vbCrLf)
            .Append("                   			and GBR.Score is not null and StuEnrollId=?" + vbCrLf)
            .Append("                   Group by " + vbCrLf)
            .Append("                   GBR.InstrGrdBkWgtDetailId, GBWD.Number,GBWD.CreditsPerService " + vbCrLf)
        End With
        db.AddParameter("@stuenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@reqid", CourseId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@reqid", CourseId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)
        'Logic : This function computes the credits earned by a student for a clinic service class and will be shown in the transcript.
        'The credits per service is driven by setting in web.config, if addcreditsbyservice is set to yes, then credits per service 
        'will be multiplied against the completed services and will be taken as credits attempted.The reason why its done on the fly
        'is some schools require students to take services at intervals and don't expect them to take all at once.so schools
        'need to keep track of how much they have earned as they keep doing the services.
        If addcreditsbyservice.ToString.ToLower = "yes" Then
            For Each dr As DataRow In ds.Tables(0).Rows
                If Not dr("CreditsPerService") Is DBNull.Value Then
                    If Not dr("LabsCompletedByStudent") Is DBNull.Value Then
                        decCreditsEarned += CType(dr("LabsCompletedByStudent") * dr("CreditsPerService"), Decimal)
                    End If
                Else
                    If Not dr("isClinicsSatisfied") Is DBNull.Value Then
                        If (dr("isClinicsSatisfied") = 1 Or dr("isClinicsSatisfied") = True) Then
                            'if course is satisfied, take credits for course add it to credits earned
                            Dim sb1 As New StringBuilder
                            db.ClearParameters()
                            With sb1
                                .Append(" select Distinct Credits from arReqs where ReqId=?")
                            End With
                            db.AddParameter("@ReqId", CourseId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            decCreditsEarned += CType(db.RunParamSQLScalar(sb1.ToString), Decimal)
                            db.ClearParameters()
                            sb1.Remove(0, sb1.Length)
                            'Once credits are earned, then the credits for the course has to be added just once 
                            'if the course is satisfied, exit for loop
                            Exit For
                        Else
                            decCreditsEarned += 0
                        End If
                    Else
                        decCreditsEarned += 0
                    End If
                End If
            Next
        Else
            For Each dr As DataRow In ds.Tables(0).Rows
                If Not dr("isClinicsSatisfied") Is DBNull.Value Then
                    If (dr("isClinicsSatisfied") = 1 Or dr("isClinicsSatisfied") = True) Then
                        Dim sb1 As New StringBuilder
                        db.ClearParameters()
                        With sb1
                            .Append(" select Distinct Credits from arReqs where ReqId=?")
                        End With
                        db.AddParameter("@ReqId", CourseId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        decCreditsEarned += CType(db.RunParamSQLScalar(sb1.ToString), Decimal)
                        db.ClearParameters()
                        sb1.Remove(0, sb1.Length)
                        'Once credits are earned, then the credits for the course has to be added just once 
                        'if the course is satisfied
                        Exit For
                    End If
                Else
                    decCreditsEarned += 0
                End If
            Next
        End If
        Return decCreditsEarned
    End Function
    Private Function GetCreditsForACourse(ByVal reqId As String) As Decimal
        Dim rtn As Decimal
        Dim db As New SQLDataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@reqId", New Guid(reqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        rtn = CType(db.RunParamSQLScalar_SP("dbo.USP_GetCreditsForACourse"), Decimal)
        Try
            Return rtn
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function ComputeWithCreditsPerService_SP(ByVal StuEnrollId As String, ByVal CourseId As String) As Decimal
        Dim ds As DataSet
        Dim decCreditsEarned As Decimal = 0.0
        Dim db As New SQLDataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")


        db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@reqId", New Guid(CourseId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet_SP("dbo.usp_ComputeWithCreditsPerService")
        If myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
            For Each dr As DataRow In ds.Tables(0).Rows
                If Not dr("CreditsPerService") Is DBNull.Value Then
                    If Not dr("LabsCompletedByStudent") Is DBNull.Value Then
                        decCreditsEarned += CType(dr("LabsCompletedByStudent") * dr("CreditsPerService"), Decimal)
                    End If
                Else
                    If Not dr("isClinicsSatisfied") Is DBNull.Value Then
                        If (dr("isClinicsSatisfied") = 1 Or dr("isClinicsSatisfied") = True) Then
                            'if course is satisfied, take credits for course add it to credits earned

                            decCreditsEarned += GetCreditsForACourse(CourseId)

                            'Once credits are earned, then the credits for the course has to be added just once 
                            'if the course is satisfied, exit for loop
                            Exit For
                        Else
                            decCreditsEarned += 0
                        End If
                    Else
                        decCreditsEarned += 0
                    End If
                End If
            Next
        Else
            For Each dr As DataRow In ds.Tables(0).Rows
                If Not dr("isClinicsSatisfied") Is DBNull.Value Then
                    If (dr("isClinicsSatisfied") = 1 Or dr("isClinicsSatisfied") = True) Then


                        decCreditsEarned += GetCreditsForACourse(CourseId)

                        'Once credits are earned, then the credits for the course has to be added just once 
                        'if the course is satisfied
                        Exit For
                    End If
                Else
                    decCreditsEarned += 0
                End If
            Next
        End If


        Try
            Return decCreditsEarned
        Catch ex As Exception
        Finally
            db.CloseConnection()
        End Try


    End Function

    ''Function to call the stored Procedure added by Saraswathi lakshmanan on April 4th 2009
    Public Function IsCourseCombinationandPass(ByVal StuEnrollId As String, ByVal ClsSectionId As String) As Boolean
        Dim transaction As OleDbTransaction
        Dim returnValue As Integer
        Dim strConn As OleDbConnection
        Dim strCmd As OleDbCommand
        Dim strOutputParam As OleDbParameter

        strConn = New OleDbConnection(myAdvAppSettings.AppSettings("ConString"))
        strConn.Open()
        transaction = strConn.BeginTransaction
        Try
            strCmd = New OleDbCommand("Sp_CoursehasLabOrExternAndIsPass", strConn, transaction)
            strCmd.CommandType = CommandType.StoredProcedure
            strOutputParam = strCmd.Parameters.Add("@StuEnrollId", OleDbType.Guid.ToString())
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@ClsSectionId", OleDbType.Guid.ToString())
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@returnvalue", OleDbType.Integer)
            strOutputParam.Direction = ParameterDirection.Output
            strCmd.Parameters("@StuEnrollId").Value = StuEnrollId
            strCmd.Parameters("@ClsSectionId").Value = ClsSectionId
            strCmd.Parameters("@returnvalue").Value = 0
            strCmd.ExecuteNonQuery()
            returnValue = strCmd.Parameters("@returnvalue").Value

            transaction.Commit()
            Return (returnValue > 0)
        Catch ex As Exception
            transaction.Rollback()
            Return False
        Finally
            strConn.Close()
        End Try
    End Function
    ''Function added by saraswathi to find the grades from the gradesystems where isTransferGrade is true and Credits earned is not checked and is pass is checked.

    Function GetGradesSysDetailId() As DataSet
        Dim db As New DataAccess
        Dim ds As DataSet
        'Dim da As New OleDbDataAdapter
        Dim sb As New StringBuilder

        sb.Append(" Select GrdSysDetailId from arGradeSystemDetails where IsTransferGrade=1 and IsPass=1 and IsCreditsEarned=0 ")
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds
    End Function

    Public Function GetEquivalentReqIdDS(ByVal Reqid As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim ds As DataSet

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append(" select Distinct EquivReqid from arCourseEquivalent where Reqid= ? ")
        End With
        db.AddParameter("@ReqId", Reqid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            ds = db.RunParamSQLDataSet(sb.ToString)
            Return ds
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Function GetEquivalentReqIdDS_SP(ByVal Reqid As String) As DataTable

        Dim ds As DataSet
        Dim db As New SQLDataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@reqid", New Guid(Reqid), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Try
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GetEquivalentReqIdDS")
            Return ds.Tables(0)
        Catch ex As Exception
        Finally
            db.CloseConnection()
        End Try

    End Function
    ''To Highligt the courses not mapped to the prgversion.

    Public Function GetCoursesinaProgVersion(ByVal StuEnrollId As String, ByVal PrgVerID As String, Optional ByVal IncludeCourseEquivalents As Boolean = False) As DataTable
        Dim ds As DataSet
        Dim dsresult As DataSet
        Dim db As New SQLDataAccess

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Try
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GetCampuIDForGivenEnrollment")

            Dim CampusID As String

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    CampusID = ds.Tables(0).Rows(0)(0).ToString
                Else
                    Return New DataTable
                End If
            Else
                Return New DataTable
            End If

            If CampusID <> "" Then

                db.ClearParameters()

                db.AddParameter("@CampusID", New Guid(CampusID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@PrgVerID", New Guid(PrgVerID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@IncludeCourseEquivalents", IncludeCourseEquivalents, SqlDbType.Bit, , ParameterDirection.Input)

                dsresult = db.RunParamSQLDataSet_SP("dbo.Sp_GetReqsForProgramVersion")
                If dsresult.Tables.Count > 0 Then
                    Return dsresult.Tables(0)
                End If
            End If
        Catch ex As Exception

        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function isCourseCompleted(ByVal ResultId As String) As Boolean

        Dim db As New SQLDataAccess

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@ResultId", New Guid(ResultId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Try
            Dim intTotalRows As Integer
            intTotalRows = db.RunParamSQLScalar_SP("dbo.USP_IsCourseCompleted")
            If intTotalRows >= 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function getExternshipCourses(ByVal StuEnrollId As String) As DataTable

        Dim db As New SQLDataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Try
            Dim dt As DataTable = db.RunParamSQLDataSet_SP("USP_GetExternshipCourses_ForStudentEnrollment", "getExternshipCourses").Tables(0)
            Return dt
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function

    '' Get record TermEnrollSummary or Create record from arTermEnrollSummary table
    Public Function GetTermEnrollSummary(ByVal pTermId As String, ByVal pStuEnrollId As String, ByVal pUser As String) As DataSet

        '   connect to the database
        Dim ds As New DataSet
        Dim dr As SqlDataAdapter

        Dim db As New SQLDataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")

        Try
            db.AddParameter("@TermId", New Guid(pTermId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", New Guid(pStuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@User", pUser, SqlDbType.NVarChar, 50, ParameterDirection.Input)

            db.OpenConnection()
            dr = db.RunParamSQLDataAdapter_SP("dbo.USP_GetTermEnrollSummary")
            dr.Fill(ds, "TermEnrollSummary")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds
    End Function
    '' Update record TermEnrollSummary in arTermEnrollSummary table
    Public Function UpdateTermEnrollSummary(ByVal termEnrollSummary As TermEnrollSummary) As String
        'return boolean value
        Dim db As New SQLDataAccess
        Dim result As String
        '   connect to the database
        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")

        Try
            db.AddParameter("@TermId", New Guid(termEnrollSummary.TermId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", New Guid(termEnrollSummary.StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@DescripXTranscript", termEnrollSummary.DescripXTranscript, SqlDbType.NVarChar, 250, ParameterDirection.Input)
            db.AddParameter("@ModUser", termEnrollSummary.ModUser, SqlDbType.NVarChar, 50, ParameterDirection.Input)
            db.AddParameter("@ModDate", termEnrollSummary.ModDate, SqlDbType.DateTime, , ParameterDirection.Input)

            db.OpenConnection()
            result = CType(db.RunParamSQLScalar_SP("dbo.USP_UpdateTermEnrollSummary"), String)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return result
    End Function

    Public Function IsEnrollmentForCEProgramVersion(ByVal stuEnrollId As String) As Boolean
        Dim db As New DataAccess
        Dim rtnBoolean As Boolean
        Dim sb As New StringBuilder
        Dim intRes As Integer

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            .Append("SELECT pv.IsContinuingEd ")
            .Append("FROM arPrgVersions pv, dbo.arStuEnrollments se ")
            .Append("WHERE pv.PrgVerId = se.PrgVerId ")
            .Append("AND se.StuEnrollId = ? ")
        End With

        db.AddParameter("@stuenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Return db.RunParamSQLScalar(sb.ToString)

    End Function

    Public Function IsStudentRegisteredForClasses(ByVal StuEnrollId As String) As Boolean
        Dim db As New DataAccess
        Dim rtnBoolean As Boolean
        Dim sb As New StringBuilder
        Dim intRes As Integer

        db.ConnectionString = conString ' MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            .Append("SELECT COUNT(*) AS Num ")
            .Append("FROM dbo.arResults ")
            .Append("WHERE StuEnrollId = ? ")

        End With

        db.AddParameter("@stuenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        intRes = CInt(db.RunParamSQLScalar(sb.ToString))

        If intRes > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
End Class
