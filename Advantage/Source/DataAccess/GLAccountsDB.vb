Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' GLAccountsDB.vb
'
' GLAccountsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class GLAccountsDB

#Region "Get AdvAppsetting object"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region

    Public Function GetAllGLAccounts(ByVal showActiveOnly As Boolean) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   GLA.GLAccountId, GLA.StatusId, GLA.GLAccountCode, GLA.GLAccountDescription ")
            .Append("FROM     saGLAccounts GLA, syStatuses ST ")
            .Append("WHERE    GLA.StatusId = ST.StatusId ")
            If showActiveOnly Then
                .Append(" AND     ST.Status = 'Active' ")
            End If
            .Append("ORDER BY GLA.GLAccountDescription ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetGLAccountInfo(ByVal GLAccountId As String) As GLAccountInfo

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT GLA.GLAccountId, ")
            .Append("    GLA.GLAccountCode, ")
            .Append("    GLA.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=GLA.StatusId) As Status, ")
            .Append("    GLA.GLAccountDescription, ")
            .Append("    GLA.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=GLA.CampGrpId) As CampGrpDescrip, ")
            .Append("    GLA.ModUser, ")
            .Append("    GLA.ModDate ")
            .Append("FROM  saGLAccounts GLA ")
            .Append("WHERE GLA.GLAccountId= ? ")
        End With

        ' Add the GLAccountId to the parameter list
        db.AddParameter("@GLAccountId", GLAccountId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim GLAccountInfo As New GLAccountInfo

        While dr.Read()

            '   set properties with data from DataReader
            With GLAccountInfo
                .GLAccountId = GLAccountId
                .IsInDB = True
                .GLAccountCode = dr("GLAccountCode")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .GLAccountDescription = dr("GLAccountDescription")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return GLAccountInfo

    End Function
    Public Function UpdateGLAccountInfo(ByVal GLAccountInfo As GLAccountInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saGLAccounts Set GLAccountId = ?, GLAccountCode = ?, ")
                .Append(" StatusId = ?, GLAccountDescription = ?, CampGrpId = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE GLAccountId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saGLAccounts where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   GLAccountId
            db.AddParameter("@GLAccountId", GLAccountInfo.GLAccountId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   GLAccountCode
            db.AddParameter("@GLAccountCode", GLAccountInfo.GLAccountCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", GLAccountInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   GLAccountDescription
            db.AddParameter("@GLAccountDescription", GLAccountInfo.GLAccountDescription, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If GLAccountInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", GLAccountInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   GLAccountId
            db.AddParameter("@AdmDepositId", GLAccountInfo.GLAccountId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", GLAccountInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddGLAccountInfo(ByVal GLAccountInfo As GLAccountInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saGLAccounts (GLAccountId, GLAccountCode, StatusId, ")
                .Append("   GLAccountDescription, CampGrpId, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   GLAccountId
            db.AddParameter("@GLAccountId", GLAccountInfo.GLAccountId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   GLAccountCode
            db.AddParameter("@GLAccountCode", GLAccountInfo.GLAccountCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", GLAccountInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   GLAccountDescription
            db.AddParameter("@GLAccountDescription", GLAccountInfo.GLAccountDescription, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If GLAccountInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", GLAccountInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteGLAccountInfo(ByVal GLAccountId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saGLAccounts ")
                .Append("WHERE GLAccountId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM saGLAccounts WHERE GLAccountId = ? ")
            End With

            '   add parameters values to the query

            '   GLAccountId
            db.AddParameter("@GLAccountId", GLAccountId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   GLAccountId
            db.AddParameter("@GLAccountId", GLAccountId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
End Class
