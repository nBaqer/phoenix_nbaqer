Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.DataAccess.FAME.DataAccessLayer


Public Class SAPCheckDB

    Public Function GetPrgVerSAP(ByVal stuEnrollId As String) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim da1 As OleDbDataAdapter
        Dim ds As New DataSet
        'Dim count As Integer

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        ''Include Transfer Hours added by Saraswathi lakshmanan
        ''On June 16 2010

        With sb
            .Append("SELECT  t1.PrgVerId, isnull(t1.SAPId,t2.sapid) as sapId, t2.Credits, t2.PrgVerDescrip, t3.TrigUnitTypId, t3.TrigOffsetTypId, t2.PrgVerDescrip,  ")
            .Append(" t3.MinTermGPA,t3.TermGPAOver,isnull(t3.TrackExternAttendance,0) as TrackExternAttendance,IsNull(t3.IncludeTransferHours,0) as IncludeTransferHours,isnull(t1.TransferHours,0) as TransferHours,")
            .Append("(select UnitTypedescrip from arAttUnitType where UnitTypeId=t2.UnitTypeId) as attendanceType,")
            .Append(" (Select arPrograms.ACId from arprograms, syAcademicCalendars  where	arPrograms.ProgId=t2.ProgId  and syAcademicCalendars.ACId=arPrograms.ACId  and	ACDescrip like 'Clock%') ")
            .Append(" as ClockHrProgram ")
            .Append(" FROM  arStuEnrollments t1, arPrgVersions t2, arSAP t3 ")
            .Append("WHERE t1.StuEnrollId = ? and t1.PrgVerId = t2.PrgVerId and isnull(t1.SAPId,t2.sapId) = t3.SAPId and t3.TrigUnitTypId is not null ")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@studentid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        '   Execute the query       
        db.OpenConnection()
        da1 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da1.Fill(ds, "PrgVersions")
        Catch ex As System.Exception
        End Try
        sb.Remove(0, sb.Length)
        db.ClearParameters()
        'Close Connection
        db.CloseConnection()
        Return ds
    End Function

    Public Function GetSAPPolicyDetails(ByVal sapId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet
        'Dim dr As OleDbDataReader

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT t2.SAPDetailId, t2.QualMinValue, t2.QuantMinValue,t2.QuantMinUnitTypId,t2.QualMinTypId, ")
            .Append("  ROW_NUMBER() OVER ( ORDER BY t2.TrigOffsetSeq, t2.TrigValue ) AS Seq, ")
            '.Append(" t2.Seq, ")
            '.Append(" (Select count(*) from arSAPDetails where TrigOffsetSeq<=t2.TrigOffsetSeq and TrigValue<=t2.TrigValue and SAPId=T2.SAPId) As Seq, ")
            '.Append("  Case len(TrigOffsetSeq) when 1 then (Select count(*) from arSAPDetails where TrigOffsetSeq<=t2.TrigOffsetSeq and ((TrigOffsetSeq*100 + TrigValue)<=(t2.TrigOffsetSeq*100 + t2.TrigValue)) and SAPId=T2.SAPId) else (Select count(*) from arSAPDetails where TrigValue <= t2.TrigValue and SAPId=T2.SAPId) end As Seq, ")
            '.Append(" (Select count(*) from arSAPDetails where TrigOffsetSeq<=t2.TrigOffsetSeq and ((TrigOffsetSeq*100 + TrigValue)<=(t2.TrigOffsetSeq*100 + t2.TrigValue)) and SAPId=T2.SAPId) As Seq, ")
            .Append("t2.TrigValue AS TrigValue,t2.TrigUnitTypId,t2.TrigOffsetTypId,t2.TrigOffsetSeq,t2.ConsequenceTypId, t2.MinCredsCompltd, t2.MinAttendanceValue, t3.TrigUnitTypDescrip, t4.TrigOffTypDescrip, t5.QuantMinTypDesc, t6.QualMinTypDesc, t7.ConseqTypDesc ")
            .Append("FROM arSAPDetails t2, arTrigUnitTyps t3, arTrigOffsetTyps t4, arQuantMinUnitTyps t5, arQualMinTyps t6, arConsequenceTyps t7  ")
            .Append("WHERE t2.SAPId = ? and t2.TrigUnitTypId = t3.TrigUnitTypId and t2.TrigOffsetTypId = t4.TrigOffsetTypId and t2.QuantMinUnitTypId = t5.QuantMinUnitTypId and t2.QualMinTypId = t6.QualMinTypId and t2.ConsequenceTypId = t7.ConsequenceTypId ")
            .Append("ORDER BY t2.TrigOffsetSeq ,t2.TrigValue  ")
        End With
        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@SAPId", sapId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)


        'Close Connection
        db.CloseConnection()

        Return ds

    End Function

    Public Function GetSAPPolicyInstuctionDetails(ByVal sapDetailId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        'Dim dr As OleDbDataReader

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        With sb

            .Append("SELECT  SAPQuantInsTypeID ,QuantMinValue,SAPDetailId,arSAPQuantByInstruction.InstructionTypeId,InstructionTypeDescrip ")
            .Append(" FROM arSAPQuantByInstruction,arInstructionType     WHERE arSAPQuantByInstruction.InstructionTypeId=arInstructionType.InstructionTypeId AND   SAPDetailId =  ? ")
        End With
        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@SAPDetailId", sapDetailId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)


        'Close Connection
        db.CloseConnection()

        Return ds.Tables(0)

    End Function

    Public Function GetTrigToBeFired(ByVal stuEnrollId As String) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        'Dim da1 As New OleDbDataAdapter
        Dim ds As DataSet
        'Dim count As Integer

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT  t1.Period ")
            .Append("FROM  arSAPChkResults t1 ")
            .Append("WHERE t1.StuEnrollId = ? and  t1.PreviewSapCheck = 0 ")
            .Append("ORDER BY t1.Period desc ")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@studentid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)


        'Close Connection
        db.CloseConnection()

        Return ds
    End Function
    Public Function GetTrigFASAPToBeFired(ByVal stuEnrollId As String) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        'Dim da1 As New OleDbDataAdapter
        Dim ds As DataSet
        'Dim count As Integer

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT  t1.Period ")
            .Append("FROM  arFASAPChkResults t1 ")
            .Append("WHERE t1.StuEnrollId = ? and  t1.PreviewSapCheck = 0 ")
            .Append("ORDER BY t1.Period desc ")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@studentid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)


        'Close Connection
        db.CloseConnection()

        Return ds
    End Function
    Public Function GetCredsAttempted(ByVal StuEnrollId As String, ByVal ProgVerId As String, Optional ByVal TermEDate As Date = #1/1/1001#) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim dr As OleDbDataReader

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        If TermEDate = #1/1/1001# Then
            'With sb
            '    .Append("SELECT t4.Credits, ")
            '    .Append("t1.GrdSystemId, t5.IsCreditsAttempted ")
            '    .Append("FROM arPrgVersions t1, arStuProgVer t2, arResults t3, arReqs t4, arGradeSystemDetails t5, arTerm t6 WHERE ")
            '    .Append("t2.PrgVerId = t1.PrgVerId And t2.StudentId = t3.StudentId and ")
            '    .Append("t3.ReqId = t4.ReqId and t1.GrdSystemId = t5.GrdSystemId and t3.TermId = t6.TermId and ")
            '    .Append("t2.PrgVerId = ? and t2.StudentId = ?  and t3.Grade = t5.Grade and t5.IsCreditsAttempted = 1 ")
            '    .Append("ORDER BY t6.StartDate ")
            'End With
            With sb
                .Append("SELECT sum(t4.Credits) as CredsAttmptd  ")
                .Append("FROM  arResults t3, arClassSections t7, arReqs t4, arGradeSystemDetails t5 WHERE  ")
                .Append("t3.TestId = t7.ClsSectionId and t7.ReqId = t4.ReqId  and ")
                .Append("t3.StuEnrollId = ? and t3.GrdSysDetailId = t5.GrdSysDetailId and t5.IsCreditsAttempted = 1  ")
            End With
            ' Add the StudentId and PrgVerId to the parameter list
            'db.AddParameter("@SAPId", ProgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@SAPId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Else
            'With sb
            '    .Append("SELECT t4.Credits, ")
            '    .Append("t1.GrdSystemId, t5.IsCreditsAttempted ")
            '    .Append("FROM arPrgVersions t1, arStuProgVer t2, arResults t3, arReqs t4, arGradeSystemDetails t5, arTerm t6 WHERE ")
            '    .Append("t2.PrgVerId = t1.PrgVerId And t2.StudentId = t3.StudentId and ")
            '    .Append("t3.ReqId = t4.ReqId and t1.GrdSystemId = t5.GrdSystemId and ")
            '    .Append("t2.PrgVerId = ? and t2.StudentId = ?  and t3.Grade = t5.Grade and t5.IsCreditsAttempted = 1 and ")
            '    .Append("t6.TermId = t3.TermId ")
            '    If TermEDate <> #1/1/1001# Then
            '        .Append("and t3.TermId in (Select TermId from arTerm where EndDate <= ?)")
            '    End If
            'End With
            With sb
                .Append("SELECT sum(t4.Credits) as CredsAttmptd  ")
                .Append("FROM  arResults t3, arClassSections t7, arReqs t4, arGradeSystemDetails t5, arTerm t6 WHERE  ")
                .Append("t3.TestId = t7.ClsSectionId and t7.ReqId = t4.ReqId  and  t6.TermId = t7.TermId and t6.EndDate <= ? and ")
                .Append("t3.StuEnrollId = ? and t3.GrdSysDetailId = t5.GrdSysDetailId and t5.IsCreditsAttempted = 1 ")
            End With
            If TermEDate <> #1/1/1001# Then
                '  Dim TermGuid As Guid = XmlConvert.ToGuid(TermId)
                db.AddParameter("@Term", TermEDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            ' Add the StudentId and PrgVerId to the parameter list
            db.AddParameter("@StudenId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        End If
        Try
            ds = db.RunParamSQLDataSet(sb.ToString)
        Catch ex As System.Exception

        End Try
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        Return ds

    End Function

    Public Function GetCredsCompltd(ByVal StuEnrollId As String, ByVal ProgVerId As String, Optional ByVal TermEDate As Date = #1/1/1001#) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim dr As OleDbDataReader

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        If TermEDate = #1/1/1001# Then
            'With sb
            '    .Append("SELECT t4.Credits, ")
            '    .Append("t1.GrdSystemId, t5.IsCreditsEarned ")
            '    .Append("FROM arPrgVersions t1, arStuProgVer t2, arResults t3, arReqs t4, arGradeSystemDetails t5, arTerm t6 WHERE ")
            '    .Append("t2.PrgVerId = t1.PrgVerId And t2.StudentId = t3.StudentId and ")
            '    .Append("t3.ReqId = t4.ReqId and t1.GrdSystemId = t5.GrdSystemId and ")
            '    .Append("t2.PrgVerId = ? and t2.StudentId = ?  and t3.Grade = t5.Grade and t5.IsCreditsEarned = 1 and ")
            '    .Append("t6.TermId = t3.TermId ")
            '    .Append("ORDER BY t6.StartDate ")
            'End With
            With sb
                .Append("SELECT sum(t4.Credits) as CredsCompltd  ")
                .Append("FROM  arResults t3, arClassSections t7, arReqs t4, arGradeSystemDetails t5 WHERE  ")
                .Append("t3.TestId = t7.ClsSectionId and t7.ReqId = t4.ReqId  and ")
                .Append("t3.StuEnrollId = ? and t3.GrdSysDetailId = t5.GrdSysDetailId and t5.IsCreditsEarned = 1  ")
            End With
            ' Add the StudentId and PrgVerId to the parameter list
            db.AddParameter("@SAPId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Else
            'With sb
            '    .Append("SELECT t4.Credits, ")
            '    .Append("t1.GrdSystemId, t5.IsCreditsEarned ")
            '    .Append("FROM arPrgVersions t1, arStuProgVer t2, arResults t3, arReqs t4, arGradeSystemDetails t5, arTerm t6 WHERE ")
            '    .Append("t2.PrgVerId = t1.PrgVerId And t2.StudentId = t3.StudentId and ")
            '    .Append("t3.ReqId = t4.ReqId and t1.GrdSystemId = t5.GrdSystemId and ")
            '    .Append("t2.PrgVerId = ? and t2.StudentId = ?  and t3.Grade = t5.Grade and t5.IsCreditsEarned = 1 and ")
            '    .Append("t6.TermId = t3.TermId ")
            '    If TermEDate <> #1/1/1001# Then
            '        .Append("and t3.TermId in (Select TermId from arTerm where EndDate <= ?)")
            '    End If
            'End With
            With sb
                .Append("SELECT sum(t4.Credits) as CredsCompltd  ")
                .Append("FROM  arResults t3, arClassSections t7, arReqs t4, arGradeSystemDetails t5, arTerm t6 WHERE  ")
                .Append("t3.TestId = t7.ClsSectionId and t7.ReqId = t4.ReqId  and  t6.TermId = t7.TermId and t6.EndDate <= ? and ")
                .Append("t3.StuEnrollId = ? and t3.GrdSysDetailId = t5.GrdSysDetailId and t5.IsCreditsEarned = 1  ")
            End With

            If TermEDate <> #1/1/1001# Then
                '  Dim TermGuid As Guid = XmlConvert.ToGuid(TermId)
                db.AddParameter("@Term", TermEDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            ' Add the StudentId and PrgVerId to the parameter list
            db.AddParameter("@SAPId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        End If
        db.OpenConnection()
        Try
            ds = db.RunParamSQLDataSet(sb.ToString)
        Catch ex As System.Exception

        End Try
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        Return ds

    End Function

    Public Function GetCredsCompltd1(ByVal StudentId As String, ByVal ProgVerId As String, Optional ByVal TermEDate As Date = #1/1/1001#) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim dr As OleDbDataReader

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT t4.Credits ")
            .Append("t1.GrdSystemId, t5.IsCreditsEarned ")
            .Append("FROM arPrgVersions t1, arStuProgVer t2, arResults t3, arReqs t4, arGradeSystemDetails t5, arTerm t6 WHERE ")
            .Append("t2.PrgVerId = t1.PrgVerId And t2.StudentId = t3.StudentId and ")
            .Append("t3.ReqId = t4.ReqId and t1.GrdSystemId = t5.GrdSystemId and ")
            .Append("t2.PrgVerId = ? and t2.StudentId = ?  and t3.Grade = t5.Grade and t5.IsCreditsEarned = 1 ")
            .Append("t6.arTerm = t3.TermId ")
            If TermEDate <> #1/1/1001# Then
                .Append("and t6.EndDate <= ? ")
            End If
        End With
        ' Add the StudentId and PrgVerId to the parameter list
        db.AddParameter("@SAPId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@SAPId", ProgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        If TermEDate <> #1/1/1001# Then
            '  Dim TermGuid As Guid = XmlConvert.ToGuid(TermId)
            db.AddParameter("@Term", TermEDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If

        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        Return ds

    End Function

    Public Function GetCoursesAttempted(ByVal StudentId As String, ByVal ProgVerId As String, Optional ByVal TermEDate As Date = #1/1/1001#) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim dr As OleDbDataReader

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT t3.ReqId ")
            .Append("t1.GrdSystemId, t5.IsCreditsEarned ")
            .Append("FROM arPrgVersions t1, arStuProgVer t2, arResults t3,arGradeSystemDetails t5 WHERE ")
            .Append("t2.PrgVerId = t1.PrgVerId And t2.StudentId = t3.StudentId and ")
            .Append("t1.GrdSystemId = t5.GrdSystemId and ")
            .Append("t2.PrgVerId = ? and t2.StudentId = ?  and t3.Grade = t5.Grade and t5.IsCreditsAttempted = 1 ")
        End With
        ' Add the StudentId and PrgVerId to the parameter list
        db.AddParameter("@SAPId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@SAPId", ProgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        Return ds

    End Function

    Public Function GetCoursesCompltd(ByVal StudentId As String, ByVal ProgVerId As String, Optional ByVal TermEDate As Date = #1/1/1001#) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim dr As OleDbDataReader

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT t3.ReqId ")
            .Append("t1.GrdSystemId, t5.IsCreditsEarned ")
            .Append("FROM arPrgVersions t1, arStuProgVer t2, arResults t3,arGradeSystemDetails t5 WHERE ")
            .Append("t2.PrgVerId = t1.PrgVerId And t2.StudentId = t3.StudentId and ")
            .Append("t1.GrdSystemId = t5.GrdSystemId and ")
            .Append("t2.PrgVerId = ? and t2.StudentId = ?  and t3.Grade = t5.Grade and t5.IsCreditsEarned = 1 ")
        End With
        ' Add the StudentId and PrgVerId to the parameter list
        db.AddParameter("@SAPId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@SAPId", ProgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        Return ds

    End Function

    Public Function GetGPAInfo(ByVal StuEnrollId As String, ByVal ProgVerId As String, ByVal TrigUnit As String, Optional ByVal TermEDate As Date = #1/1/1001#) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim dr As OleDbDataReader

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        If (TrigUnit = "Credits") Then
            'With sb
            '    .Append("SELECT t5.GPA, t4.Credits, t6.StartDate ")
            '    .Append("FROM arPrgVersions t1, arStuProgVer t2, arResults t3, arReqs t4, arGradeSystemDetails t5, arTerm t6 WHERE ")
            '    .Append("t2.PrgVerId = t1.PrgVerId And t2.StudentId = t3.StudentId and ")
            '    .Append("t3.ReqId = t4.ReqId and t1.GrdSystemId = t5.GrdSystemId and ")
            '    .Append("t2.PrgVerId = ? and t2.StudentId = ?  and t3.Grade = t5.Grade and t5.IsInGPA = 1 ")
            '    .Append("and t6.TermId = t3.TermId ")
            '    .Append("ORDER BY t6.StartDate ")
            'End With
            With sb
                .Append("SELECT t5.IsInGPA, t5.GPA, t4.Credits, t6.StartDate, t6.TermId as Term ")
                .Append("FROM arPrgVersions t1, arStuEnrollments t2, arResults t3, arReqs t4, arGradeSystemDetails t5, arTerm t6, arClassSections t7 WHERE ")
                .Append("t2.PrgVerId = t1.PrgVerId And ")
                .Append("t3.TestId = t7.ClsSectionId and t7.ReqId = t4.ReqId and t1.GrdSystemId = t5.GrdSystemId and ")
                .Append("t3.StuEnrollId = t2.StuEnrollId and t3.StuEnrollId = ?  and t3.GrdSysDetailId = t5.GrdSysDetailId and t5.IsInGPA = 1 ")
                .Append("and t7.TermId = t6.TermId  ")
                .Append("ORDER BY t6.StartDate ")
            End With

        Else

            With sb
                .Append("SELECT t5.IsInGPA, t5.GPA, t4.Credits, t6.StartDate, t6.TermId as Term ")
                .Append("FROM arPrgVersions t1, arStuEnrollments t2, arResults t3, arReqs t4, arGradeSystemDetails t5, arTerm t6, arClassSections t7 WHERE ")
                .Append("t2.PrgVerId = t1.PrgVerId And ")
                .Append("t3.TestId = t7.ClsSectionId and t7.ReqId = t4.ReqId and t1.GrdSystemId = t5.GrdSystemId and ")
                .Append("t3.StuEnrollId = t2.StuEnrollId and t3.StuEnrollId = ?  and t3.GrdSysDetailId = t5.GrdSysDetailId and t5.IsInGPA = 1 ")
                .Append("and t7.TermId = t6.TermId and t6.EndDate <= ? ")
                .Append("ORDER BY t6.StartDate ")
            End With
        End If

        ' Add the StudentId and PrgVerId to the parameter list
        db.AddParameter("@SAPId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        If TermEDate <> #1/1/1001# Then
            '  Dim TermGuid As Guid = XmlConvert.ToGuid(TermId)
            db.AddParameter("@Term", TermEDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If
        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        Return ds

    End Function

    Public Function GetStuTerms(ByVal StudentId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet
        Dim dr As OleDbDataReader

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        'With sb
        '    .Append("SELECT DISTINCT t1.TermId, t2.StartDate, t2.EndDate ")
        '    .Append("FROM arResults t1, arTerm t2 WHERE ")
        '    .Append("t1.StudentId = ? and t1.TermId = t2.TermId ")
        '    .Append("ORDER BY t2.StartDate ")
        'End With
        With sb
            .Append("SELECT DISTINCT t3.TermId, t2.StartDate, t2.EndDate ")
            .Append("FROM  arResults t1, arTerm t2, arClassSections t3 WHERE ")
            .Append("t1.StuEnrollId = ? and t1.TestId = t3.ClsSectionId and t3.TermId = t2.TermId ")
            .Append(" Union ")
            .Append(" SELECT DISTINCT t1.TermId, t2.StartDate, t2.EndDate FROM  arTransferGrades t1, arTerm t2, arReqs t3 ")
            .Append(" WHERE t1.StuEnrollId = ? and t1.Reqid = t3.Reqid and t2.TermId = t1.TermId ")
            .Append("ORDER BY t2.StartDate ")
        End With
        ' Add the StudentId and PrgVerId to the parameter list
        db.AddParameter("@SAPId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@SAPId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        Return ds

    End Function

    Public Function GetStuTermsfromSDate(ByVal StuEnrollId As String, ByVal SDate As Date, ByVal OffsetDate As Date, Optional ByVal campusId As String = "") As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim dr As OleDbDataReader

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'With sb
        '    .Append("SELECT DISTINCT t1.TermId, t2.StartDate, t2.EndDate ")
        '    .Append("FROM arResults t1, arTerm t2 WHERE ")
        '    .Append("t1.StuEnrollId = ? and t1.TermId = t2.TermId and t2.EndDate between ? and ? ")
        '    .Append("ORDER BY t2.StartDate ")
        'End With

        With sb

            .Append("SELECT DISTINCT t3.TermId, t2.StartDate, t2.EndDate ")
            .Append("FROM arResults t1, arTerm t2, arClassSections t3 WHERE ")
            .Append(" t1.StuEnrollId = ? ")
            .Append("and t1.TestId = t3.ClsSectionId and t3.TermId = t2.TermId ")
            If MyAdvAppSettings.AppSettings("SchedulingMethod", campusId) <> "ModuleStart" Then
                .Append("and t2.EndDate between ? and ? ")
            End If
            .Append(" Union ")
            .Append("SELECT DISTINCT t2.TermId, t2.StartDate, t2.EndDate ")
            .Append("FROM arTransferGrades t1, arTerm t2, arReqs t3 WHERE ")
            .Append(" t1.StuEnrollId = ? ")
            .Append("and t1.TermId = t2.TermId and t1.ReqId = t3.ReqId ")
            If MyAdvAppSettings.AppSettings("SchedulingMethod", campusId) <> "ModuleStart" Then
                .Append("and t2.EndDate between ? and ? ")
            End If
            .Append("ORDER BY t2.StartDate ")
        End With

        ' Add the StudentId and PrgVerId to the parameter list
        db.AddParameter("@SAPId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If MyAdvAppSettings.AppSettings("SchedulingMethod", campusId) <> "ModuleStart" Then
            db.AddParameter("@SAPId", SDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@SAPId", OffsetDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If
        db.AddParameter("@SAPId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If MyAdvAppSettings.AppSettings("SchedulingMethod", campusId) <> "ModuleStart" Then
            db.AddParameter("@SAPId", SDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@SAPId", OffsetDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If

        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        Return ds

    End Function

    Public Function GetTermResults(ByVal TermId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim dr As OleDbDataReader

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT  t1.TermId, t2.StartDate, t2.EndDate ")
            .Append("FROM arResults t1, arTerm t2 WHERE ")
            .Append("t2.StudentId = ? and t1.TermId = t2.TermId ")
        End With
        ' Add the StudentId and PrgVerId to the parameter list
        db.AddParameter("@SAPId", TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        Return ds

    End Function

    Public Function GetStuStartDate(ByVal StuEnrollId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet
        Dim dr As OleDbDataReader

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT t2.StuEnrollId, t2.StartDate ")
            .Append("FROM arStuEnrollments t2 ")
            .Append("WHERE t2.StuEnrollId = ?  ")

        End With
        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@SAPId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        Return ds

    End Function

    Public Function AddTrigToDB(ByVal sapChkInfo As SAPCheckInfo, ByVal user As String) As String
        Try
            Dim db As New DataAccess
            Dim sb As New StringBuilder

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

            'Set the connection string
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            With sb
                .Append("Delete from arSAPChkResults where StuEnrollId = ? and PreviewSapCheck = 1 and Period = ?; ")
                .Append("INSERT INTO arSAPChkResults ")
                .Append("(StuEnrollId, Period, IsMakingSAP, HrsAttended, HrsEarned, CredsAttempted, CredsEarned, GPA, DatePerformed,Comments,SAPDetailId,ModUser,ModDate,Attendance,CumFinAidCredits,PercentCompleted,checkpointdate,TermStartDate,PreviewSapCheck) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With
            db.AddParameter("@StudentId", sapChkInfo.StudentId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            db.AddParameter("@Period", sapChkInfo.Period, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@StudentId", sapChkInfo.StudentId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            db.AddParameter("@Period", sapChkInfo.Period, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@IsMakingSAP", sapChkInfo.IsMakingSAP, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@HrsAttended", sapChkInfo.HrsAttended, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@HrsEarned", sapChkInfo.HrsEarned, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            db.AddParameter("@CredsAttmptd", sapChkInfo.CredsAttempted, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@CredsEarned", sapChkInfo.CredsEarned, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@GPA", sapChkInfo.GPA, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@DatePerf", sapChkInfo.DatePerf, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Comments", sapChkInfo.Comments, DataAccess.OleDbDataType.OleDbString, 1000, ParameterDirection.Input)
            db.AddParameter("@SAPDetailId", sapChkInfo.SAPDetailId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)


            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Attendance", sapChkInfo.PercAttendance, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@CumFinAid", sapChkInfo.FinCredsEarned, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@PercentCompleted", sapChkInfo.PercCredsEarned, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            If sapChkInfo.CheckPointDate = #12:00:00 AM# Then
                db.AddParameter("@CheckPointDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@CheckPointDate", sapChkInfo.CheckPointDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            If sapChkInfo.TermStartDate = #12:00:00 AM# Then
                db.AddParameter("@TermStartDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@TermStartDate", sapChkInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            If sapChkInfo.IsMakingSAP Then
                db.AddParameter("@PreviewSapCheck", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            Else
                db.AddParameter("@PreviewSapCheck", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()
            'End If
        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try
    End Function

    Public Function AddTrigFASAPToDB(ByVal SAPChkInfo As SAPCheckInfo, ByVal user As String) As String
        Try
            Dim db As New DataAccess
            Dim sb As New StringBuilder

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            With sb
                .Append("Delete from arFASAPChkResults where StuEnrollId = ? and PreviewSapCheck = 1 and Period = ?; ")
                .Append("INSERT INTO arFASAPChkResults ")
                .Append("(StuEnrollId, Period, IsMakingSAP, CredsAttempted, CredsEarned, GPA, DatePerformed,Comments,SAPDetailId,ModUser,ModDate,Attendance,CumFinAidCredits,PercentCompleted,checkpointdate,TermStartDate,PreviewSapCheck) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With
            db.AddParameter("@StudentId", SAPChkInfo.StudentId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            db.AddParameter("@Period", SAPChkInfo.Period, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@StudentId", SAPChkInfo.StudentId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            db.AddParameter("@Period", SAPChkInfo.Period, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@IsMakingSAP", SAPChkInfo.IsMakingSAP, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@CredsAttmptd", SAPChkInfo.CredsAttempted, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@CredsEarned", SAPChkInfo.CredsEarned, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@GPA", SAPChkInfo.GPA, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@DatePerf", SAPChkInfo.DatePerf, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Comments", SAPChkInfo.Comments, DataAccess.OleDbDataType.OleDbString, 1000, ParameterDirection.Input)
            db.AddParameter("@SAPDetailId", SAPChkInfo.SAPDetailId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)


            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Attendance", SAPChkInfo.PercAttendance, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@CumFinAid", SAPChkInfo.FinCredsEarned, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@PercentCompleted", SAPChkInfo.PercCredsEarned, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            If SAPChkInfo.CheckPointDate = #12:00:00 AM# Then
                db.AddParameter("@CheckPointDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@CheckPointDate", SAPChkInfo.CheckPointDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            If SAPChkInfo.TermStartDate = #12:00:00 AM# Then
                db.AddParameter("@TermStartDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@TermStartDate", SAPChkInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            db.AddParameter("@PreviewSapCheck", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()
            'End If
        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try
    End Function

    Public Function AddTrigFASAPToDB(ByVal SAPChkInfo As SAPCheckInfo, ByVal user As String, SAPInfo As SAPInfo) As String
        Try
            Dim db As New DataAccess
            Dim sb As New StringBuilder

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            'Set the connection string
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            'If (SAPChkInfo.IsMakingSAP = True) Then
            '    With sb
            '        .Append("delete from arSAPChkResults where StuEnrollId = ? ")
            '    End With
            '    db.AddParameter("@StuEnrollId", SAPChkInfo.StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    db.RunParamSQLExecuteNoneQuery(sb.ToString)
            '    'Execute the query
            '    'Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            '    'Dim intSAPCount As Integer
            '    'While dr.Read
            '    '    intSAPCount = dr("Count")
            '    'End While
            '    db.ClearParameters()
            '    sb.Remove(0, sb.Length)
            'End If
            ' If intSAPCount < 1 Then
            With sb
                .Append("Delete from arFASAPChkResults where StuEnrollId = ? and PreviewSapCheck = 1 and Period = ?; ")
                .Append("INSERT INTO arFASAPChkResults ")
                .Append("(StuEnrollId, Period, IsMakingSAP, CredsAttempted, CredsEarned, GPA, DatePerformed,Comments,SAPDetailId,ModUser,ModDate,Attendance,CumFinAidCredits,PercentCompleted,checkpointdate,TermStartDate,PreviewSapCheck) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); ")
                .Append("Delete from arSAPQuantResults where StuEnrollId = ? and SapDetailId= ? ;")
                For i As Integer = 0 To SAPInfo.QuantMinValueByInsTypes.Count - 1
                    .Append("Insert into arSAPQuantResults ")
                    .Append("(stuenrollid,instructiontypeid,sapdetailid,percentcompleted)")
                    .Append("Values(?,?,?,?);")
                Next

            End With
            db.AddParameter("@StudentId", SAPChkInfo.StudentId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            db.AddParameter("@Period", SAPChkInfo.Period, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@StudentId", SAPChkInfo.StudentId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            db.AddParameter("@Period", SAPChkInfo.Period, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@IsMakingSAP", SAPChkInfo.IsMakingSAP, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@CredsAttmptd", SAPChkInfo.CredsAttempted, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@CredsEarned", SAPChkInfo.CredsEarned, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@GPA", SAPChkInfo.GPA, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@DatePerf", SAPChkInfo.DatePerf, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Comments", SAPChkInfo.Comments, DataAccess.OleDbDataType.OleDbString, 1000, ParameterDirection.Input)
            db.AddParameter("@SAPDetailId", SAPChkInfo.SAPDetailId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)


            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Attendance", SAPChkInfo.PercAttendance, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@CumFinAid", SAPChkInfo.FinCredsEarned, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@PercentCompleted", SAPChkInfo.PercCredsEarned, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            If SAPChkInfo.CheckPointDate = #12:00:00 AM# Then
                db.AddParameter("@CheckPointDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@CheckPointDate", SAPChkInfo.CheckPointDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            If SAPChkInfo.TermStartDate = #12:00:00 AM# Then
                db.AddParameter("@TermStartDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@TermStartDate", SAPChkInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            db.AddParameter("@PreviewSapCheck", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)


            db.AddParameter("@StudentId", SAPChkInfo.StudentId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            db.AddParameter("@SAPDetailId", SAPChkInfo.SAPDetailId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)

            For i As Integer = 0 To SAPInfo.QuantMinValueByInsTypes.Count - 1
                db.AddParameter("@StudentId", SAPChkInfo.StudentId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
                db.AddParameter("@InstructionTypeId", SAPInfo.QuantMinValueByInsTypes(i).InstructionTypeId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
                db.AddParameter("@SAPDetailId", SAPChkInfo.SAPDetailId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
                db.AddParameter("@PercCompltd", SAPInfo.QuantMinValueByInsTypes(i).PercCompltd, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            Next
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()
            'End If
        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try
    End Function

    Public Function AddTrigToDB(ByVal sapChkInfo As SAPCheckInfo, ByVal user As String, SAPInfo As SAPInfo) As String
        Try
            Dim db As New DataAccess
            Dim sb As New StringBuilder

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            'Set the connection string
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            'If (SAPChkInfo.IsMakingSAP = True) Then
            '    With sb
            '        .Append("delete from arSAPChkResults where StuEnrollId = ? ")
            '    End With
            '    db.AddParameter("@StuEnrollId", SAPChkInfo.StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    db.RunParamSQLExecuteNoneQuery(sb.ToString)
            '    'Execute the query
            '    'Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            '    'Dim intSAPCount As Integer
            '    'While dr.Read
            '    '    intSAPCount = dr("Count")
            '    'End While
            '    db.ClearParameters()
            '    sb.Remove(0, sb.Length)
            'End If
            ' If intSAPCount < 1 Then
            With sb
                .Append("Delete from arSAPChkResults where StuEnrollId = ? and PreviewSapCheck = 1 and Period = ?; ")
                .Append("INSERT INTO arSAPChkResults ")
                .Append("(StuEnrollId, Period, IsMakingSAP, CredsAttempted, CredsEarned, GPA, DatePerformed,Comments,SAPDetailId,ModUser,ModDate,Attendance,CumFinAidCredits,PercentCompleted,checkpointdate,TermStartDate,PreviewSapCheck) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); ")
                .Append("Delete from arSAPQuantResults where StuEnrollId = ? and SapDetailId= ? ;")
                For i As Integer = 0 To SAPInfo.QuantMinValueByInsTypes.Count - 1
                    .Append("Insert into arSAPQuantResults ")
                    .Append("(stuenrollid,instructiontypeid,sapdetailid,percentcompleted)")
                    .Append("Values(?,?,?,?);")
                Next

            End With
            db.AddParameter("@StudentId", sapChkInfo.StudentId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            db.AddParameter("@Period", sapChkInfo.Period, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@StudentId", sapChkInfo.StudentId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            db.AddParameter("@Period", sapChkInfo.Period, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@IsMakingSAP", sapChkInfo.IsMakingSAP, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@CredsAttmptd", sapChkInfo.CredsAttempted, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@CredsEarned", sapChkInfo.CredsEarned, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@GPA", sapChkInfo.GPA, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@DatePerf", sapChkInfo.DatePerf, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Comments", sapChkInfo.Comments, DataAccess.OleDbDataType.OleDbString, 1000, ParameterDirection.Input)
            db.AddParameter("@SAPDetailId", sapChkInfo.SAPDetailId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)


            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Attendance", sapChkInfo.PercAttendance, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@CumFinAid", sapChkInfo.FinCredsEarned, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@PercentCompleted", sapChkInfo.PercCredsEarned, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            If sapChkInfo.CheckPointDate = #12:00:00 AM# Then
                db.AddParameter("@CheckPointDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@CheckPointDate", sapChkInfo.CheckPointDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            If sapChkInfo.TermStartDate = #12:00:00 AM# Then
                db.AddParameter("@TermStartDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@TermStartDate", sapChkInfo.TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            If sapChkInfo.IsMakingSAP Then
                db.AddParameter("@PreviewSapCheck", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            Else
                db.AddParameter("@PreviewSapCheck", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If

            db.AddParameter("@StudentId", sapChkInfo.StudentId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            db.AddParameter("@SAPDetailId", sapChkInfo.SAPDetailId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)

            For i As Integer = 0 To SAPInfo.QuantMinValueByInsTypes.Count - 1
                db.AddParameter("@StudentId", sapChkInfo.StudentId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
                db.AddParameter("@InstructionTypeId", SAPInfo.QuantMinValueByInsTypes(i).InstructionTypeId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
                db.AddParameter("@SAPDetailId", sapChkInfo.SAPDetailId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
                db.AddParameter("@PercCompltd", SAPInfo.QuantMinValueByInsTypes(i).PercCompltd, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            Next
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()
            'End If
        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try
    End Function

    Public Function GetSAPDataForCurrentGPA() As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT StuEnrollId,SSN,LastName as 'Last Name',FirstName as 'First Name',PrgVerDescrip As Program, NPL, MTF, CreditsAttempted as UA, ")
            .Append("CreditsEarned as UC, (CreditsAttempted/NPL)*100 As PP, GPA ")
            .Append("FROM ")
            .Append("(SELECT se.StuEnrollId,st.SSN,st.LastName,st.FirstName,pv.PrgVerDescrip,ss.SysStatusDescrip, ")
            .Append("pv.Credits as NPL, pv.Credits * 1.5 as MTF, ")
            .Append("(SELECT SUM(Credits) ")
            .Append("FROM ")
            .Append("(SELECT distinct t1.StuEnrollId,t4.TermId,t3.TermDescrip, t4.ReqId, t2.Credits, t3.StartDate, t3.EndDate, t1.TestId, t1.GrdSysDetailId, ")
            .Append("(Select Grade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId)as Grade, ")
            .Append("(Select IsPass from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsPass, ")
            .Append("(Select GPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as GPA, ")
            .Append("(Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsAttempted, ")
            .Append("(Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsEarned, ")
            .Append("(Select IsInGPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInGPA ")
            .Append("FROM arResults t1, arReqs t2, arTerm t3, arClassSections t4 ")
            .Append("WHERE t1.TestId = t4.ClsSectionId And t4.TermId = t3.TermId And t4.ReqId = t2.ReqId ")
            .Append("AND t1.GrdSysDetailId is not null ")
            .Append("AND EXISTS( ")
            .Append("SELECT distinct t100.StuEnrollId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t100.StuEnrollId =  se.StuEnrollId ")
            .Append("AND t400.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t100.StuEnrollId =  se.StuEnrollId ")
            .Append("AND t700.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t100.StuEnrollId =  se.StuEnrollId ")
            .Append("AND t800.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t100.StuEnrollId =  se.StuEnrollId ")
            .Append("AND t900.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t100.StuEnrollId =  se.StuEnrollId ")
            .Append("AND t1000.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000, arReqGrpDef t1100 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t1000.ReqId = t1100.GrpId AND ")
            .Append("t100.StuEnrollId =  se.StuEnrollId ")
            .Append("AND t1100.ReqId=t4.ReqId ")
            .Append(") ")
            .Append("AND t1.StuEnrollId IN( ")
            .Append("SELECT StuEnrollId ")
            .Append("FROM arStuEnrollments ")
            .Append("WHERE StudentId = (SELECT StudentId FROM arStuEnrollments ")
            .Append("WHERE StuEnrollId = se.StuEnrollId))) as d1 ")
            .Append("WHERE d1.IsCreditsAttempted=1) As CreditsAttempted, ")
            .Append("(SELECT SUM(Credits) ")
            .Append("FROM ")
            .Append("(SELECT distinct t1.StuEnrollId,t4.TermId,t3.TermDescrip, t4.ReqId, t2.Credits, t3.StartDate, t3.EndDate, t1.TestId, t1.GrdSysDetailId, ")
            .Append("(Select Grade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId)as Grade, ")
            .Append("(Select IsPass from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsPass, ")
            .Append("(Select GPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as GPA, ")
            .Append("(Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsAttempted, ")
            .Append("(Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsEarned, ")
            .Append("(Select IsInGPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInGPA ")
            .Append("FROM arResults t1, arReqs t2, arTerm t3, arClassSections t4 ")
            .Append("WHERE t1.TestId = t4.ClsSectionId And t4.TermId = t3.TermId And t4.ReqId = t2.ReqId ")
            .Append("AND t1.GrdSysDetailId is not null ")
            .Append("AND EXISTS( ")
            .Append("SELECT distinct t100.StuEnrollId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t100.StuEnrollId =  se.StuEnrollId ")
            .Append("AND t400.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t100.StuEnrollId =  se.StuEnrollId ")
            .Append("AND t700.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t100.StuEnrollId =  se.StuEnrollId ")
            .Append("AND t800.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t100.StuEnrollId =  se.StuEnrollId ")
            .Append("AND t900.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t100.StuEnrollId =  se.StuEnrollId ")
            .Append("AND t1000.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000, arReqGrpDef t1100 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t1000.ReqId = t1100.GrpId AND ")
            .Append("t100.StuEnrollId =  se.StuEnrollId ")
            .Append("AND t1100.ReqId=t4.ReqId ")
            .Append(") ")
            .Append("AND t1.StuEnrollId IN( ")
            .Append("SELECT StuEnrollId ")
            .Append("FROM arStuEnrollments ")
            .Append("WHERE StudentId = (SELECT StudentId FROM arStuEnrollments ")
            .Append("WHERE StuEnrollId = se.StuEnrollId))) as d2 ")
            .Append("WHERE d2.IsCreditsEarned=1) As CreditsEarned, ")
            .Append("(SELECT (SUM(Credits*GPA)/SUM(Credits)) ")
            .Append("FROM ")
            .Append("(SELECT distinct t1.StuEnrollId,t4.TermId,t3.TermDescrip, t4.ReqId, t2.Credits, t3.StartDate, t3.EndDate, t1.TestId, t1.GrdSysDetailId, ")
            .Append("(Select Grade from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId)as Grade, ")
            .Append("(Select IsPass from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsPass, ")
            .Append("(Select GPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as GPA, ")
            .Append("(Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsAttempted, ")
            .Append("(Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsCreditsEarned, ")
            .Append("(Select IsInGPA from arGradeSystemDetails where GrdSysDetailId = t1.GrdSysDetailId) as IsInGPA ")
            .Append("FROM arResults t1, arReqs t2, arTerm t3, arClassSections t4 ")
            .Append("WHERE t1.TestId = t4.ClsSectionId And t4.TermId = t3.TermId And t4.ReqId = t2.ReqId ")
            .Append("AND t1.GrdSysDetailId is not null ")
            .Append("AND EXISTS( ")
            .Append("SELECT distinct t100.StuEnrollId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t100.StuEnrollId =  se.StuEnrollId ")
            .Append("AND t400.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t100.StuEnrollId =  se.StuEnrollId ")
            .Append("AND t700.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t100.StuEnrollId =  se.StuEnrollId ")
            .Append("AND t800.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t100.StuEnrollId =  se.StuEnrollId ")
            .Append("AND t900.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t100.StuEnrollId =  se.StuEnrollId ")
            .Append("AND t1000.ReqId=t4.ReqId ")
            .Append("UNION ")
            .Append("SELECT distinct t100.StuEnrollId ")
            .Append("FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000, arReqGrpDef t1100 ")
            .Append("WHERE t100.StudentId=t500.StudentId AND ")
            .Append("t3.ReqId = t400.ReqId AND ")
            .Append("t100.PrgVerId = t400.PrgVerId AND ")
            .Append("t100.PrgVerId = t600.PrgVerId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t1000.ReqId = t1100.GrpId AND ")
            .Append("t100.StuEnrollId =  se.StuEnrollId ")
            .Append("AND t1100.ReqId=t4.ReqId ")
            .Append(") ")
            .Append("AND t1.StuEnrollId IN( ")
            .Append("SELECT StuEnrollId ")
            .Append("FROM arStuEnrollments ")
            .Append("WHERE StudentId = (SELECT StudentId FROM arStuEnrollments ")
            .Append("WHERE StuEnrollId = se.StuEnrollId))) as d3 ")
            .Append("WHERE d3.IsInGPA=1) As GPA ")
            .Append("FROM arStuEnrollments se, arStudent st, arPrgVersions pv, syStatusCodes sc, sySysStatus ss ")
            .Append("WHERE se.StudentId=st.StudentId ")
            .Append("AND se.PrgVerId=pv.PrgVerId ")
            .Append("AND se.StatusCodeId = sc.StatusCodeId ")
            .Append("AND sc.SysStatusId=ss.SysStatusId ")
            .Append("AND ss.SysStatusId NOT IN(12,14) ")
            .Append(") as d4 ")
        End With

        Try
            Return db.RunSQLDataSet(sb.ToString).Tables(0)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function DoesSAPIncrementHasResultsForEnrollment(ByVal increment As Integer, ByVal stuEnrollId As String) As Boolean
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New System.Text.StringBuilder
        Dim i As Integer
        Dim obj As System.Object

        With sb
            .Append("SELECT Count(*) ")
            .Append("FROM arSAPChkResults ")
            .Append("WHERE Period = ? ")
            .Append("AND StuEnrollId = ? ")
        End With

        db.AddParameter("@increment", increment, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@stuenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        obj = db.RunParamSQLScalar(sb.ToString)
        If IsDBNull(obj) Then
            Return False
        Else
            i = CInt(obj)
            If i = 0 Then
                Return False
            Else
                Return True
            End If
        End If

    End Function

    'Public Sub AddFASAPChkResult(ByVal sapChkResult As SAPChkResultInfo)
    '    Dim db As New DataAccess

    '    Dim myAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        myAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder

    '    With sb
    '        .Append("INSERT INTO arFASAPChkResults(StuEnrollId,Period,IsMakingSAP,CredsAttempted,CredsEarned,GPA,DatePerformed,Comments,ModDate,ModUser) ")
    '        .Append("VALUES(?,?,?,?,?,?,?,?,?,?)")
    '    End With

    '    db.AddParameter("@stuenrollid", sapChkResult.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@period", sapChkResult.Increment, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
    '    If sapChkResult.IsMakingSAP Then
    '        db.AddParameter("@makingsap", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
    '    Else
    '        db.AddParameter("@makingsap", 0, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
    '    End If
    '    db.AddParameter("@credsatt", sapChkResult.CreditsAttempted, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '    db.AddParameter("@credsearn", sapChkResult.CreditsEarned, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '    db.AddParameter("@gpa", sapChkResult.GPA, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '    db.AddParameter("@dateperf", sapChkResult.DatePerformed, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '    db.AddParameter("@comments", sapChkResult.Comments, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
    '    db.AddParameter("@moddate", sapChkResult.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '    db.AddParameter("@moduser", sapChkResult.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '    Try
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '    Catch ex As Exception
    '        If ex.InnerException Is Nothing Then
    '            Throw ex
    '        Else
    '            Throw (ex.InnerException)
    '        End If
    '    End Try

    'End Sub

    'Public Sub AddSAPChkResult(ByVal sapChkResult As SAPChkResultInfo)
    '    Dim db As New DataAccess

    '    Dim myAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        myAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder

    '    With sb
    '        .Append("INSERT INTO arSAPChkResults(StuEnrollId,Period,IsMakingSAP,CredsAttempted,CredsEarned,GPA,DatePerformed,Comments,ModDate,ModUser) ")
    '        .Append("VALUES(?,?,?,?,?,?,?,?,?,?)")
    '    End With

    '    db.AddParameter("@stuenrollid", sapChkResult.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@period", sapChkResult.Increment, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
    '    If sapChkResult.IsMakingSAP Then
    '        db.AddParameter("@makingsap", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
    '    Else
    '        db.AddParameter("@makingsap", 0, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
    '    End If
    '    db.AddParameter("@credsatt", sapChkResult.CreditsAttempted, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '    db.AddParameter("@credsearn", sapChkResult.CreditsEarned, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '    db.AddParameter("@gpa", sapChkResult.GPA, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '    db.AddParameter("@dateperf", sapChkResult.DatePerformed, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '    db.AddParameter("@comments", sapChkResult.Comments, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
    '    db.AddParameter("@moddate", sapChkResult.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '    db.AddParameter("@moduser", sapChkResult.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '    Try
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '    Catch ex As Exception
    '        If ex.InnerException Is Nothing Then
    '            Throw ex
    '        Else
    '            Throw (ex.InnerException)
    '        End If
    '    End Try

    'End Sub
    Public Function GetSAPChkResultInfo(ByVal stuEnrollId As String, ByVal increment As Integer) As SAPChkResultInfo
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT GPA,CredsEarned,CredsAttempted ")
            .Append("FROM arSAPChkResults ")
            .Append("WHERE StuEnrollId = ? ")
            .Append("AND Period = ? ")
        End With

        db.AddParameter("@stuid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@period", increment, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim sapChk As New SAPChkResultInfo

        While dr.Read()

            '   set properties with data from DataReader
            With sapChk
                .GPA = dr("GPA")
                .CreditsEarned = dr("CredsEarned")
                .CreditsAttempted = dr("CredsAttempted")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()

        Return sapChk

    End Function
    Public Function GetEnrollmentCurrentStatus(ByVal stuEnrollId As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim sStatus As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT StatusCodeId ")
            .Append("FROM arStuEnrollments ")
            .Append("WHERE StuEnrollId = ? ")
        End With

        db.AddParameter("@stuid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Try
            sStatus = db.RunParamSQLScalar(sb.ToString).ToString()
            Return sStatus
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw ex
            Else
                Throw (ex.InnerException)
            End If
        End Try

    End Function
    Public Sub ChangeEnrollmentStatusFromAcademicProbationToCurrentlyAttending(ByVal stuEnrollId As String, ByVal campusId As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim sStatus As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("UPDATE arStuEnrollments ")
            .Append("SET StatusCodeId = (SELECT Top 1 StatusCodeId FROM syStatusCodes WHERE SysStatusId = 9 and CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?)) ")
            .Append("WHERE StuEnrollId = ? ")
        End With

        db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@stuid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw ex
            Else
                Throw (ex.InnerException)
            End If
        End Try
    End Sub
    'Public Sub ChangeEnrollmentStatusFromAcademicProbationToLastCurrentlyAttendingStatus(ByVal stuEnrollId As String, ByVal campusId As String)
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim strStatus As String = ""
    '    Dim dr As OleDbDataReader

    '    Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
    '    db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

    '    'This query will get the last status that was mapped to "currently attending" before being placed on probation.
    '    With sb
    '        .Append(" select Distinct t1.StatusId,t1.ModDate,t2.StatusCodeDescrip " + vbCrLf)
    '        .Append(" from " + vbCrLf)
    '        .Append("   (select distinct StuEnrollId,NewStatusId as StatusId,ModDate from syStudentStatusChanges " + vbCrLf)
    '        '.Append("   union select distinct StuEnrollId,OrigStatusId as StatusId,ModDate from syStudentStatusChanges " + vbCrLf)
    '        .Append("   )  t1,syStatusCodes t2 where t1.StuEnrollId=? and t1.StatusId=t2.StatusCodeId  " + vbCrLf)
    '        .Append(" and t2.SysStatusId not in (7,8,10,11,12,19,21,14,20) " + vbCrLf)
    '        .Append("   order by t1.ModDate desc ")
    '    End With

    '    db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '    Try
    '        dr = db.RunParamSQLDataReader(sb.ToString)
    '        While dr.Read()
    '            strStatus = CType(dr("StatusId"), Guid).ToString
    '            Exit While
    '        End While
    '    Catch ex As Exception
    '        strStatus = ""
    '    Finally
    '        dr.Close()
    '    End Try

    '    db.ClearParameters()
    '    sb.Remove(0, sb.Length)



    '    If Not strStatus = "" Then
    '        With sb
    '            .Append("UPDATE arStuEnrollments ")
    '            .Append("SET StatusCodeId = ? ")
    '            .Append("WHERE StuEnrollId = ? ")
    '        End With
    '        db.AddParameter("@StatusCodeId", strStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        Try
    '            db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '        Catch ex As System.Exception
    '        Finally
    '            db.CloseConnection()
    '        End Try
    '    Else
    '        'If data not found in syStudentStatusChanges then set the student back to Currently Attending
    '        With sb
    '            .Append("UPDATE arStuEnrollments ")
    '            .Append("SET StatusCodeId = (SELECT Top 1 StatusCodeId FROM syStatusCodes WHERE SysStatusId = 9 and CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?)) ")
    '            .Append("WHERE StuEnrollId = ? ")
    '        End With
    '        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        Try
    '            db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '        Catch ex As Exception
    '        Finally
    '            db.CloseConnection()
    '        End Try
    '    End If
    'End Sub

    Public Function GetOffsetDateForCreditsPolicy(ByVal StuEnrollId As String) As String
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim sdate As String
        Dim offsetDate As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("select top 1(t2.EndDate), t1.* from arResults t1, arClassSections t2 ")
                .Append("where t1.StuEnrollId = ? ")
                .Append("and t1.TestId = t2.ClsSectionId ")
                .Append("order by t2.EndDate desc ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ReqId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            db.OpenConnection()
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                offsetDate = dr("EndDate").ToString
            End While

            'ds = db.RunParamSQLDataSet(sb.ToString)
            'sb.Remove(0, sb.Length)


            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return offsetDate

    End Function
    Public Function DoesStudentHaveLOA(ByVal StuEnrollId As String, ByVal StartDate As Date, ByVal OffsetDate As Date) As String
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New System.Text.StringBuilder
        Dim i As Integer
        Dim obj As System.Object
        Dim NoOfLOADays As Integer
        Dim LOAReturn As String
        Dim LOAStart As String
        Dim count As Integer

        Try
            With sb
                .Append("SELECT Count(*) as Count, LOAReturnDate, StartDate ")
                .Append("FROM arStudentLOAs ")
                .Append("WHERE (LOAReturnDate >= ? and StartDate <= ?)  ")
                .Append("AND StuEnrollId = ? ")
                .Append("GROUP BY LOAReturnDate, StartDate")
            End With

            db.AddParameter("@stuenrollid", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            db.AddParameter("@increment", OffsetDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            db.AddParameter("@stuenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                LOAReturn = dr("LOAReturnDate").ToString
                LOAStart = dr("StartDate").ToString
                count = dr("Count").ToString
            End While


            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return LOAStart & "," & LOAReturn & "," & count

    End Function
    Public Function GetClockScheduledHours(ByVal stuEnrollId As String) As Integer
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New System.Text.StringBuilder
        Dim i As Integer


        Try
            With sb
                .Append("Select sum(schedHours) as TotSchedHours from arStudentClockAttendance where StuEnrollId = ? ")
            End With

            db.AddParameter("@stuenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            i = db.RunParamSQLScalar(sb.ToString())



            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return i
    End Function

    Public Function GetClockActualHours(ByVal stuEnrollId As String) As Integer
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New System.Text.StringBuilder
        Dim i As Integer


        Try
            With sb
                .Append("Select sum(ActualHours) as TotActualHours from arStudentClockAttendance where StuEnrollId = ? ")
            End With

            db.AddParameter("@stuenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            i = db.RunParamSQLScalar(sb.ToString())



            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return i
    End Function
    Public Function GetWorkUnitResults(ByVal strStuEnrollId As String, ByVal cutOffDate As Date) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT ")
            .Append("   GBCR.ReqId,GBCR.TermId,GCT.Descrip,GBCR.Score,GBCR.MinResult as PassingGrade, ")
            .Append("   GBCR.Required,GBCR.MustPass, ")
            .Append("   (CASE ")
            .Append("       WHEN isnull(GBCR.Score,0) > MinResult THEN NULL ")
            .Append("       WHEN isnull(GBCR.Score,0) = MinResult THEN 0 ")
            .Append("       WHEN MinResult > isnull(GBCR.Score,0) THEN (MinResult - isnull(GBCR.Score,0)) ")
            .Append("    END) AS Remaining ,")
            .Append("(Select Code from arReqs where ReqId=GBCR.ReqId) as CourseCode, ")
            .Append(" (select Descrip from arReqs  where ReqId=GBCR.ReqId) as CourseDescrip,SysComponentTypeId ")
            .Append(" FROM arGrdBkConversionResults GBCR, arGrdComponentTypes GCT, arTransferGrades TG,arTerm ")
            .Append("WHERE GBCR.GrdComponentTypeId=GCT.GrdComponentTypeId and ")
            .Append("	GBCR.StuEnrollId= ? ")
            .Append(" and    GBCR.TermId=TG.TermId and  GBCR.ReqId = TG.ReqId And GBCR.StuEnrollId = TG.StuEnrollId  and TG.TermId=arTerm.TermId  and	")
            .Append(" arTerm.StartDate <= ? and  GBCR.MinResult is not null ")

            .Append(" Union ")
            .Append("SELECT ")
            .Append("   ReqId,TermId,Descrip,Score,MinResult as PassingGrade,Required,MustPass, ")
            .Append("   (CASE ")
            .Append("     WHEN isnull(Score,0) > MinResult THEN NULL ")
            .Append("     WHEN isnull(Score,0) = MinResult THEN 0 ")
            .Append("     WHEN MinResult > isnull(Score,0) THEN (MinResult - isnull(Score,0)) ")
            .Append("    END) AS Remaining,CourseCode,CourseDescrip,SysComponentTypeId ")
            .Append("   FROM ")
            .Append("   (SELECT ")
            .Append("    CS.ReqId,CS.TermId,GCT.Descrip, ")
            .Append("     (CASE GCT.SysComponentTypeId ")
            .Append("       WHEN 544 THEN (SELECT SUM(HoursAttended) FROM arExternshipAttendance WHERE StuEnrollId=?) ")
            .Append("       ELSE GBRS.Score ")
            .Append("       END ")
            .Append("     ) AS Score, ")
            .Append("    (CASE GCT.SysComponentTypeId ")
            .Append("       WHEN 500 THEN GBWD.Number ")
            .Append("       WHEN 503 THEN GBWD.Number ")
            .Append("       ELSE (SELECT MIN(MinVal) ")
            .Append("             FROM arGradeScaleDetails GSD, arGradeSystemDetails GSS ")
            .Append("             WHERE GSD.GrdSysDetailId=GSS.GrdSysDetailId ")
            .Append("             AND GSS.IsPass=1) ")
            .Append("       END ")
            .Append("       )	AS MinResult, ")
            .Append("       GBWD.Required, GBWD.MustPass ,")
            .Append(" (Select Code from arReqs where ReqId=CS.ReqId) as CourseCode, ")
            .Append(" (select Descrip from arReqs  where ReqId=CS.ReqId) as CourseDescrip,SysComponentTypeId ")
            .Append("       FROM arGrdBkResults GBRS, arClassSections CS, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT,arTerm    ")
            .Append(" where   		CS.ClsSectionId=GBRS.ClsSectionId   and	GBRS.StuEnrollId= ? ")
            .Append(" and CS.TermId=arTerm.TermId  and	arTerm.StartDate <=  ? ")
            .Append("       AND GBRS.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
            .Append("       AND GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId ) P  where MinResult is not null ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("CutOffDate", cutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("CutOffDate", cutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function
    Public Function GetResultsByEnrollment(ByVal strStuEnrollId As String, ByVal cutOffDate As Date) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet

        With sb
            .Append(" SELECT DISTINCT        t4.TermId,t3.TermDescrip,t4.ReqId,t2.Code,t2.Descrip AS Descrip,       t2.Credits,t2.Hours,t2.CourseCategoryId, ")
            .Append(" t3.StartDate AS StartDate,t3.EndDate AS EndDate,      (select B.GrdSysDetailId from ")
            .Append(" arResults A,arGradeSystemDetails B,arGradeScaleDetails  C  where B.GrdSysDetailId=C.GrdSysDetailId and A.Score >=C.MinVal and  ")
            .Append(" A.Score <=C.MaxVal and A.ResultId=t1.ResultId) as GrdSysDetailId,t1.TestId,t1.ResultId, ")
            .Append(" (select B.Grade from arResults A,arGradeSystemDetails B,arGradeScaleDetails  C  where B.GrdSysDetailId=C.GrdSysDetailId and A.Score >=C.MinVal ")
            .Append(" and A.Score <=C.MaxVal and A.ResultId=t1.ResultId) as Grade,   (select B.IsPass from arResults A,arGradeSystemDetails B,arGradeScaleDetails  C ")
            .Append(" where B.GrdSysDetailId=C.GrdSysDetailId and A.Score >=C.MinVal and A.Score <=C.MaxVal and A.ResultId=t1.ResultId) as IsPass, ")
            .Append(" (select isnull(B.GPA,0) from arResults A,arGradeSystemDetails B,arGradeScaleDetails  C  where B.GrdSysDetailId=C.GrdSysDetailId and ")
            .Append(" A.Score >=C.MinVal and A.Score <=C.MaxVal and A.ResultId=t1.ResultId) as GPA,(select B.IsCreditsAttempted from arResults A,arGradeSystemDetails B, ")
            .Append(" arGradeScaleDetails  C  where B.GrdSysDetailId=C.GrdSysDetailId and A.Score >=C.MinVal and A.Score <=C.MaxVal and A.ResultId=t1.ResultId) as IsCreditsAttempted, ")
            .Append(" (select B.IsCreditsEarned from arResults A,arGradeSystemDetails B,arGradeScaleDetails  C  where B.GrdSysDetailId=C.GrdSysDetailId and A.Score >=C.MinVal ")
            .Append(" and A.Score <=C.MaxVal and A.ResultId=t1.ResultId) as IsCreditsEarned,(select B.IsInGPA from arResults A,arGradeSystemDetails B,arGradeScaleDetails  C ")
            .Append(" where B.GrdSysDetailId=C.GrdSysDetailId and A.Score >=C.MinVal and A.Score <=C.MaxVal and A.ResultId=t1.ResultId) as IsInGPA, ")
            .Append(" (select B.IsDrop from arResults A,arGradeSystemDetails B,arGradeScaleDetails  C  where B.GrdSysDetailId=C.GrdSysDetailId and A.Score >=C.MinVal ")
            .Append(" and A.Score <=C.MaxVal and A.ResultId=t1.ResultId) as IsDrop, ")
            .Append("       (Select Descrip from arCourseCategories where CourseCategoryId = t2.CourseCategoryId) as CourseCategory,t1.Score  ")
            .Append("FROM   arResults t1, arReqs t2, arTerm t3, arClassSections t4, arClassSectionTerms t5, arStuEnrollments t6  ")
            .Append("WHERE  t1.TestId = t4.ClsSectionId")
            .Append("       AND t4.ClsSectionId = t5.ClsSectionId")
            .Append("       AND t5.TermId = t3.TermId")
            .Append("       AND t4.ReqId = t2.ReqId")
            .Append("       AND t1.StuEnrollId = t6.StuEnrollId")
            '.Append("       AND (t1.GrdSysDetailId is not null  or t1.Score is not null) ")
            .Append("       AND t1.StuEnrollId = ? ")

            .Append(" AND t3.EndDate <= ? ")

            .Append(" UNION ")
            .Append(" SELECT DISTINCT ")
            .Append("       t10.TermId,t30.TermDescrip,t10.ReqId,t20.Code,t20.Descrip AS Descrip,")
            .Append("       t20.Credits,t20.Hours,t20.CourseCategoryId,t30.StartDate AS StartDate,t30.EndDate AS EndDate,")
            .Append(" (select B.GrdSysDetailId from arTransferGrades A,arGradeSystemDetails B,arGradeScaleDetails  C ")
            .Append(" where B.GrdSysDetailId=C.GrdSysDetailId and A.Score >=C.MinVal and A.Score <=C.MaxVal and A.TransferId=t10.TransferId) as GrdSysDetailId,")
            .Append("'{00000000-0000-0000-0000-000000000000}' AS TestId,t10.TransferId AS ResultId,")
            .Append(" (select B.Grade from arTransferGrades A,arGradeSystemDetails B,arGradeScaleDetails  C ")
            .Append(" where B.GrdSysDetailId=C.GrdSysDetailId and A.Score >=C.MinVal and A.Score <=C.MaxVal and A.TransferId=t10.TransferId) as Grade,")
            .Append(" (select B.IsPass from arTransferGrades A,arGradeSystemDetails B,arGradeScaleDetails  C ")
            .Append(" where B.GrdSysDetailId=C.GrdSysDetailId and A.Score >=C.MinVal and A.Score <=C.MaxVal and A.TransferId=t10.TransferId) as IsPass,")
            .Append("(select isnull(B.GPA,0) from arTransferGrades A,arGradeSystemDetails B,arGradeScaleDetails  C ")
            .Append(" where B.GrdSysDetailId=C.GrdSysDetailId and A.Score >=C.MinVal and A.Score <=C.MaxVal and A.TransferId=t10.TransferId) as GPA,")
            .Append("(select B.IsCreditsAttempted from arTransferGrades A,arGradeSystemDetails B,arGradeScaleDetails  C ")
            .Append(" where B.GrdSysDetailId=C.GrdSysDetailId and A.Score >=C.MinVal and A.Score <=C.MaxVal and A.TransferId=t10.TransferId) as IsCreditsAttempted,")
            .Append("(select B.IsCreditsEarned from arTransferGrades A,arGradeSystemDetails B,arGradeScaleDetails  C ")
            .Append(" where B.GrdSysDetailId=C.GrdSysDetailId and A.Score >=C.MinVal and A.Score <=C.MaxVal and A.TransferId=t10.TransferId) as IsCreditsEarned,")
            .Append("(select B.IsInGPA from arTransferGrades A,arGradeSystemDetails B,arGradeScaleDetails  C ")
            .Append(" where B.GrdSysDetailId=C.GrdSysDetailId and A.Score >=C.MinVal and A.Score <=C.MaxVal and A.TransferId=t10.TransferId) as IsInGPA,")
            .Append(" (select B.IsDrop from arTransferGrades A,arGradeSystemDetails B,arGradeScaleDetails  C ")
            .Append(" where B.GrdSysDetailId=C.GrdSysDetailId and A.Score >=C.MinVal and A.Score <=C.MaxVal and A.TransferId=t10.TransferId) as IsDrop,")
            .Append("       (Select Descrip from arCourseCategories where CourseCategoryId = t20.CourseCategoryId) as CourseCategory,t10.Score  ")
            .Append("FROM   arTransferGrades t10, arReqs t20, arTerm t30 ")
            .Append("WHERE  t10.TermId = t30.TermId")
            .Append("       AND t10.ReqId = t20.ReqId ")
            .Append("       AND t10.StuEnrollId = ? ")
            .Append(" AND t30.EndDate <= ? ")


            .Append(" ORDER BY StartDate desc,EndDate,TermDescrip")
        End With
        db.AddParameter("@StdId", strStuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.AddParameter("@EndDate1", cutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        db.AddParameter("@StdId", strStuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.AddParameter("@EndDate", cutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Function GetCourseAverage(ByVal stuEnrollId As String, ByVal termId As String, ByVal reqid As String) As Decimal
        Dim sb As New StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append(" select isnull(score,0) from arResults where stuEnrollId= ? ")
            .Append(" and Testid in (select clsSectionid from arClasssections where ")
            .Append(" Termid= ?  and reqid= ? )")
            .Append(" union ")
            .Append(" select isnull(score,0) from arTransferGrades where stuEnrollId= ? ")
            .Append(" and TermId= ?  and  reqid= ? ")
        End With
        db.AddParameter("@StdId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@termId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@reqId", reqid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@termId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@reqId", reqid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        Dim d As Decimal
        d = CDec(db.RunParamSQLScalar(sb.ToString))

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return d
    End Function
    Public Function GetCourseAverage(ByVal stuEnrollId As String, ByVal clsSectId As String) As Decimal
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append(" select isnull(score,0) from arResults where stuEnrollId= ? ")
            .Append(" and Testid= ? ")
            .Append(" union ")
            .Append(" select isnull(a.score,0) from arTransferGrades a, arClassSections b where a.stuEnrollId= ? ")
            .Append(" and a.TermId=b.TermId and a.Reqid=b.Reqid and b.ClsSectionId= ? ")
        End With
        db.AddParameter("@StdId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@clsSect", clsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StdId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@clsSect", clsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        Dim d As Decimal
        d = CDec(db.RunParamSQLScalar(sb.ToString))

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return d
    End Function

    Public Function GetCourseAverage_Sp(ByVal stuEnrollId As String, ByVal clsSectId As String) As Decimal
        'Dim db As New DataAccess
        Dim Transaction As OleDbTransaction
        Dim ReturnValue As Decimal = 0.0
        'Dim returnoutputparam As OleDbParameter
        Dim strConn As OleDbConnection
        Dim strCmd As OleDbCommand
        Dim strOutputParam As OleDbParameter

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            strConn = New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
            strConn.Open()

            strCmd = New OleDbCommand("USP_AR_GetCourseAverage_forStdandClsSect", strConn, Transaction)
            strCmd.CommandType = CommandType.StoredProcedure
            strOutputParam = strCmd.Parameters.Add("@StuEnrollId", OleDbType.Guid.ToString())
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@ClsSectionId", OleDbType.Guid.ToString())
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.Add("@returnvalue", OleDbType.Integer)
            strOutputParam.Direction = ParameterDirection.Output
            strCmd.Parameters("@StuEnrollId").Value = stuEnrollId
            strCmd.Parameters("@ClsSectionId").Value = clsSectId
            strCmd.Parameters("@returnvalue").Value = 0
            strCmd.ExecuteNonQuery()
            ReturnValue = strCmd.Parameters("@returnvalue").Value

            ''Return 0
        Catch ex As Exception
            Transaction.Rollback()
            Return CDec(ReturnValue)
        Finally
            strConn.Close()
        End Try

        Return CDec(ReturnValue)

    End Function

    Public Function PerformFASAPCheck(ByVal dtStudentClasses As DataTable) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dr As DataRow

        db.OpenConnection()
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Dim myAdvAppSettings As AdvAppSettings = New AdvAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Try

            For Each dr In dtStudentClasses.Rows
                With sb
                    .Append("Update arFASAPChkResults set PreviewSAPCheck = 0, DatePerformed= ? where stuEnrollid = ? and Period = ? ")
                End With

                Dim sDate As DateTime = Utilities.GetAdvantageDBDateTime(Date.Now)

                db.AddParameter("datePerformed", sDate, DataAccess.OleDbDataType.OleDbDateTime, ParameterDirection.Input)
                db.AddParameter("sid", dr("StuEnrollId"), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("period", dr("Period"), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Next


            groupTrans.Commit()
            For Each dr In dtStudentClasses.Rows
                TerminateFASAPORProbateStudent(dr("StuEnrollId").ToString, dr("ConsequenceTypId").ToString, dr("modUser").ToString, dr("CampusId").ToString)

            Next
            'If we are dealing with a school that wants to ignore the prereqs for Run Schedules
            'then we should also update any expected grad date where it is less than the end of
            'the last class that the student is scheduled for.
            Return ""
        Catch ex As Exception
            groupTrans.Rollback()
            If ex.InnerException Is Nothing Then
                Return ex.Message
            Else
                Return ex.InnerException.Message
            End If
        Finally
            db.CloseConnection()
        End Try

    End Function
    Public Function PerformSAPCheck(ByVal dtStudentClasses As DataTable) As String
        'Dim db As New DataAccess
        'Dim sb As New StringBuilder
        'Dim dr As DataRow
        'Dim result As String
        ' Dim regDB As New RegisterDB
        ' db.OpenConnection()
        'Dim groupTrans As OleDbTransaction = db.StartTransaction()

        'Dim myAdvAppSettings As AdvAppSettings = New AdvAppSettings()
        ' db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        'Try

        'For Each dr In dtStudentClasses.Rows
        '    With sb
        '        .Append("Update arSAPChkResults set PreviewSAPCheck = 0, DatePerformed= ? where stuEnrollid = ? and Period = ? ")
        '    End With

        '    Dim sDate As DateTime = Utilities.GetAdvantageDBDateTime(Date.Now)

        '    db.AddParameter("datePerformed", sDate, DataAccess.OleDbDataType.OleDbDateTime, ParameterDirection.Input)
        '    db.AddParameter("sid", dr("StuEnrollId"), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '    db.AddParameter("period", dr("Period"), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '    db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
        '    db.ClearParameters()
        '    sb.Remove(0, sb.Length)
        'Next


        'groupTrans.Commit()
        For Each dr As DataRow In dtStudentClasses.Rows
            TerminateOrProbateStudent(dr("StuEnrollId").ToString, dr("ConsequenceTypId").ToString, dr("modUser").ToString, dr("CampusId").ToString, dr("Period"))
        Next

        'If we are dealing with a school that wants to ignore the prerequisite for Run Schedules
        'then we should also update any expected grad date where it is less than the end of
        'the last class that the student is scheduled for.
        Return String.Empty
        'Catch ex As Exception
        '    ' groupTrans.Rollback()
        '    Return If(ex.InnerException Is Nothing, ex.Message, ex.InnerException.Message)
        'End Try

    End Function

    Private Sub TerminateFASAPORProbateStudent(ByVal stuEnrollId As String, ByVal ConsequenceTypId As String, ByVal user As String, ByVal campusId As String)
        '***********************************************************************************************************************
        'If the student enrollment is NOT making SAP then based on the policy details for the increment that the SAP
        'check was done we need to update the enrollment status to one of the following:
        '   (1) Academic probation
        '       If enrollment status is NOT academic probation then we can simply go ahead and change the enrollment
        '       status to academic probation.
        '       
        '       If enrollment status is already academic probation then we have to check if the enrollment has been
        '       on probation longer than then school SAP policy states. The school must specify how long an enrollment
        '       can be on academic probation. It cannot be for an indefinite period.
        '           If the permitted period has been exceeded then the enrollment should be terminated
        '           Else we don't need to do anything. We can simply leave the status as academic probation.
        '   (2) Terminate the student
        '
        '
        'If the student enrollment is maing SAP we need to check if the enrollment status is academic probation.
        'If it is then we need to update the status to the status code that corresponds to the currently attending
        'system status.
        '***************************************************************************************************************************

        'Not making SAP

        'Policy states that enrollment should be placed on probation
        If ConsequenceTypId = 1 Then
            'Place student on probation. Note that because we are using state objects we can simply try to place the student
            'on probation. If he/she is already on probation then the state object will handle that appropriately.
            Dim db As BatchStatusChangeDB = New BatchStatusChangeDB()

            If Not IsEnrollmentStatusAcademicProbation(stuEnrollId) Then
                Dim change As StudentChangeHistoryObj = New StudentChangeHistoryObj()
                change.CampusId = campusId
                change.StuEnrollId = stuEnrollId
                change.IsRevelsal = 0
                change.DropReasonGuid = Nothing
                change.Reason = "Not making FA SAP"
                change.StartDate = Date.Now
                change.EndDate = Nothing
                change.ProbWarningTypeId = 1
                change.ModUser = user
                change.ModDate = Date.Now
                change.DateOfChange = Date.Now
                change.Lda = Nothing
                db.PlaceStudentOnProbation(change)
            End If



        ElseIf ConsequenceTypId = 2 Then
            'Policy states that enrollment should be terminated
            Dim db As BatchStatusChangeDB = New BatchStatusChangeDB()

            Dim dropStudent As New TerminateStudent
            Dim dsDropStatuses As DataSet
            Dim dropStatusCodeId As String
            'We need the StatusCodeId that corresponds to the system status of DROP. Note that our architecture allows the school
            'to map more than one such status to the system status. This means that we will have to modify the UI to allow the
            'user to indicate that a status that is being mapped to our DROP status should be used when terminating an enrollment
            'as a result of not making SAP. For now we will simply use the first one in the DataSet returned with drop statuses.
            'Note that we also need the user to specify which drop reason should be used when terminating an enrollment as a
            'result of not making SAP. For now we will simply pass an empty string.
            dsDropStatuses = dropStudent.GetDropStatuses
            dropStatusCodeId = dsDropStatuses.Tables(0).Rows(0)("StatusCodeId").ToString

            Dim objDB As New StatusChangeHistoryDB
            Dim db1 As New ArStudentStatusChangeDb
            Dim record As New StudentChangeHistoryObj
            'Update the student enrollment status to 'dropped' and the student status
            'to 'inactive'
            'Get last state
            record.OrigStatusGuid = db1.GetPreviousStatusChange(stuEnrollId)
            record.StuEnrollId = stuEnrollId
            record.DateOfChange = Date.Now
            record.NewStatusGuid = dropStatusCodeId
            record.DropReasonGuid = Nothing
            record.DateOfReenrollment = Nothing
            record.ModUser = user
            record.Lda = objDB.GetLdaDate(stuEnrollId)
            db.TerminateStudent(record)


            'dropStudent.TerminateStudent(stuEnrollId, "", Date.Now.ToShortDateString, dropStatusCodeId, "", user)

        End If




    End Sub

    'Private Sub Drop(ByVal studentChangeHistoryObj As StudentChangeHistoryObj, ByVal empty As String)
    '    Throw New NotImplementedException()
    'End Sub

    Private Sub TerminateOrProbateStudent(ByVal stuEnrollId As String, ByVal ConsequenceTypId As String, ByVal user As String, ByVal campusId As String, period As Integer)
        '***********************************************************************************************************************
        'If the student enrollment is NOT making SAP then based on the policy details for the increment that the SAP
        'check was done we need to update the enrollment status to one of the following:
        '   (1) Academic probation
        '       If enrollment status is NOT academic probation then we can simply go ahead and change the enrollment
        '       status to academic probation.
        '       
        '       If enrollment status is already academic probation then we have to check if the enrollment has been
        '       on probation longer than then school SAP policy states. The school must specify how long an enrollment
        '       can be on academic probation. It cannot be for an indefinite period.
        '           If the permitted period has been exceeded then the enrollment should be terminated
        '           Else we don't need to do anything. We can simply leave the status as academic probation.
        '   (2) Terminate the student
        '
        '
        'If the student enrollment is maing SAP we need to check if the enrollment status is academic probation.
        'If it is then we need to update the status to the status code that corresponds to the currently attending
        'system status.
        '***************************************************************************************************************************

        'Not making SAP

        'Policy states that enrollment should be placed on probation
        If ConsequenceTypId = 1 Then
            'Place student on probation. Note that because we are using state objects we can simply try to place the student
            'on probation. If he/she is already on probation then the state object will handle that appropriately.
            Dim db As BatchStatusChangeDB = New BatchStatusChangeDB()

            If Not IsEnrollmentStatusAcademicProbation(stuEnrollId) Then
                Dim change As StudentChangeHistoryObj = New StudentChangeHistoryObj()
                change.CampusId = campusId
                change.StuEnrollId = stuEnrollId
                change.IsRevelsal = 0
                change.DropReasonGuid = Nothing
                change.Reason = "Not making SAP"
                change.StartDate = Date.Now
                change.EndDate = Nothing
                change.ProbWarningTypeId = 1
                change.ModUser = user
                change.ModDate = Date.Now
                change.DateOfChange = Date.Now
                change.Lda = Nothing
                change.SapPeriod = period
                change.NewSysStatusId = 20
                change.NewStatusGuid = db.GetSchoolDefinedStatusGuid(20, campusId)
                change.OrigStatusGuid = db.GetActualEnrollmentSchoolStatusGuid(stuEnrollId)
                db.PlaceStudentOnProbation(change)
                'db.PlaceStudentOnProbation(stuEnrollId, "Not making SAP", Date.Now.ToShortDateString, "", "1", user)
            Else
                UpdateSapCheckResult(stuEnrollId, period)
            End If



        ElseIf ConsequenceTypId = 2 Then
            'Policy states that enrollment should be terminated
            Dim db As BatchStatusChangeDB = New BatchStatusChangeDB()
            Dim dropStudent As New TerminateStudent
            Dim dsDropStatuses As DataSet
            Dim dropStatusCodeId As String
            'We need the StatusCodeId that corresponds to the system status of DROP. Note that our architecture allows the school
            'to map more than one such status to the system status. This means that we will have to modify the UI to allow the
            'user to indicate that a status that is being mapped to our DROP status should be used when terminating an enrollment
            'as a result of not making SAP. For now we will simply use the first one in the DataSet returned with drop statuses.
            'Note that we also need the user to specify which drop reason should be used when terminating an enrollment as a
            'result of not making SAP. For now we will simply pass an empty string.
            dsDropStatuses = dropStudent.GetDropStatuses
            dropStatusCodeId = dsDropStatuses.Tables(0).Rows(0)("StatusCodeId").ToString

            Dim objDB As New StatusChangeHistoryDB
            Dim db1 As New ArStudentStatusChangeDb
            Dim record As New StudentChangeHistoryObj
            'Update the student enrollment status to 'dropped' and the student status
            'to 'inactive'
            'Get last state
            record.OrigStatusGuid = db1.GetPreviousStatusChange(stuEnrollId)
            record.CampusId = campusId
            record.StuEnrollId = stuEnrollId
            record.IsRevelsal = 0
            record.DateOfChange = Date.Now
            record.DateDetermined = DateTime.Now
            record.DateOfReenrollment = Nothing
            record.NewStatusGuid = dropStatusCodeId
            record.DropReasonGuid = Nothing
            record.ModDate = DateTime.Now
            record.ModUser = user
            record.Lda = objDB.GetLdaDate(stuEnrollId)
            record.SapPeriod = period
            record.NewSysStatusId = 12
            record.NewStatusGuid = db.GetSchoolDefinedStatusGuid(12, campusId)
            record.OrigStatusGuid = db.GetActualEnrollmentSchoolStatusGuid(stuEnrollId)
            db.TerminateStudent(record)
            'dropStudent.TerminateStudent(stuEnrollId, "", Date.Now.ToShortDateString, dropStatusCodeId, "", user)
        Else
            UpdateSapCheckResult(stuEnrollId, period)
        End If

        'Troy: We need to update PreviewSAPCheck for all ealrier periods for this enrollment
        If period > 1 Then
            For n = 1 To period - 1
                UpdateSapCheckResult(stuEnrollId, n)
            Next
        End If

    End Sub

    ''' <summary>
    ''' This put to 0 the PreviewSAPCheck flag that was raised when the previous SAP button was pressed.
    ''' </summary>
    ''' <param name="stuEnrollId"></param>
    ''' <param name="period"></param>
    ''' <remarks></remarks>
    Private Sub UpdateSapCheckResult(ByVal stuEnrollId As String, period As Integer)
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Const sql As String = "UPDATE arSAPChkResults SET PreviewSAPCheck = 0, DatePerformed= @DatePerformed WHERE stuEnrollid = @StdEnroll and Period = @Period "
        Dim command As SqlCommand = New SqlCommand(sql, sqlconn)
        command.CommandType = CommandType.Text
        command.Parameters.AddWithValue("@DatePerformed", DateTime.Now)
        command.Parameters.AddWithValue("@StdEnroll", stuEnrollId)
        command.Parameters.AddWithValue("@Period", period)
        sqlconn.Open()
        Try
            command.ExecuteNonQuery()
        Finally
            sqlconn.Close()
        End Try
    End Sub


    Private Function IsEnrollmentStatusAcademicProbation(ByVal stuEnrollId As String) As Boolean
        Dim sapChkDB As New SAPCheckDB
        Dim probDB As New ProbationDB
        Dim probStatus As String
        Dim curStatus As String

        probStatus = probDB.GetAcademicProbationStatus()
        curStatus = sapChkDB.GetEnrollmentCurrentStatus(stuEnrollId)

        If probStatus = curStatus Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function CheckFASAPDetailsExists(ByVal prgVerId As String) As Boolean
        Dim db As New SQLDataAccess
        Dim RowCount As Decimal
        db.OpenConnection()
        If prgVerId = String.Empty Then
            db.AddParameter("@prgVerId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@prgVerId", New Guid(prgVerId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If
        RowCount = db.RunParamSQLScalar_SP("dbo.usp_GetFASAPDetailsCount")
        db.CloseConnection()
        If RowCount > 0 Then
            Return True
        Else
            Return False
        End If



    End Function
    Public Function CheckSAPDetailsExists(ByVal prgVerId As String) As Boolean
        Dim db As New SQLDataAccess
        Dim RowCount As Decimal
        db.OpenConnection()
        If prgVerId = String.Empty Then
            db.AddParameter("@prgVerId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@prgVerId", New Guid(prgVerId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If
        RowCount = db.RunParamSQLScalar_SP("dbo.usp_GetSAPDetailsCount")
        db.CloseConnection()
        If RowCount > 0 Then
            Return True
        Else
            Return False
        End If



    End Function


    Public Function GetFasapStudentInformation(ByVal enrollmentId As String) As List(Of StudentEnrollmentFasapDto)
        Dim list As List(Of StudentEnrollmentFasapDto) = New List(Of StudentEnrollmentFasapDto)()
        Dim sb As New StringBuilder
        With sb
            .Append(" SELECT ROW_NUMBER() OVER ( ORDER BY det.TrigOffsetSeq, det.TrigValue ) as Increment")
            .Append(" , typ.TrigUnitTypDescrip, sap.DatePerformed, sap.IsMakingSAP, sap.Comments ")
            .Append(" FROM arSAPChkResults sap ")
            .Append(" JOIN arSAPDetails det ON det.SAPDetailId = sap.SAPDetailId ")
            .Append(" JOIN arTrigUnitTyps typ ON typ.TrigUnitTypId = det.TrigUnitTypId ")
            .Append(" WHERE sap.StuEnrollId = ? ")
            .Append(" ORDER BY sap.Period ")
        End With

        Dim enGuid As Guid = Guid.Parse(enrollmentId)
        Dim db As New DataAccess
        db.AddParameter("@StuEnrollId", enGuid, DataAccess.OleDbDataType.OleDbGuid, ParameterDirection.Input)
        'db.OpenConnection()
        Try
            Dim reader As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString())
            'If reader.HasRows Then 
            Do While reader.Read()
                Dim dto = New StudentEnrollmentFasapDto()
                dto.Increment = reader.GetInt64(0).ToString()
                dto.TriggeredAt = reader.GetString(1)
                dto.sapcheckDate = reader.GetDateTime(2)
                dto.IsMakingSap = reader.GetBoolean(3)
                dto.IsMakSap = IIf(dto.IsMakingSap = True, "YES", "NO")
                Dim comment = reader(4)
                If IsDBNull(comment) Then
                    comment = String.Empty
                End If
                dto.Reason = comment.ToString()
                list.Add(dto)
            Loop
            ' End If
        Finally
            ' db.CloseConnection()
        End Try
        Return list
    End Function
    Public Function CheckForSAPDependency(ByVal SAPId As String) As Boolean
        Dim db As New SQLDataAccess
        Dim RowCount As Decimal
        db.OpenConnection()
        db.AddParameter("@SAPId", New Guid(SAPId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        RowCount = db.RunParamSQLScalar_SP("dbo.USP_ValidateSAPDependency")
        db.CloseConnection()
        If RowCount > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function CheckForFASAPDependency(ByVal SAPId As String) As Boolean
        Dim db As New SQLDataAccess
        Dim RowCount As Decimal
        db.OpenConnection()
        db.AddParameter("@SAPId", New Guid(SAPId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        RowCount = db.RunParamSQLScalar_SP("dbo.USP_ValidateFASAPDependency")
        db.CloseConnection()
        If RowCount > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function GetStudentSAPIncrementsAlreadyRun(ByVal stuEnrollId As String) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        'Dim da1 As New OleDbDataAdapter
        Dim ds As DataSet
        'Dim count As Integer

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT  t1.Period ")
            .Append("FROM  arSAPChkResults t1 ")
            .Append("WHERE t1.StuEnrollId = ? and  t1.PreviewSapCheck = 0 ")
            .Append("ORDER BY t1.Period ")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@studentid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)


        'Close Connection
        db.CloseConnection()

        Return ds
    End Function
End Class
