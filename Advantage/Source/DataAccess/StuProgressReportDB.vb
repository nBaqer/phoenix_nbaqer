Imports System.Xml
Imports System.Linq
Imports FAME.Advantage.Common

Public Class StuProgressReportDB

#Region "Private Data Members"
    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")
    Private m_MinimumGPA As String = MyAdvAppSettings.AppSettings("MinimumGPA")
    Private m_GradeCourseRepetitionsMethod As String = MyAdvAppSettings.AppSettings("GradeCourseRepetitionsMethod")
    Private m_ShowProgramOnTranscript As Boolean = MyAdvAppSettings.AppSettings("ShowProgramOnTranscript")
    Private m_IncludeHoursForFailedGrade As String = MyAdvAppSettings.AppSettings("IncludeHoursForFailingGrade")


#End Region


#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

    Public ReadOnly Property MinimumGPA() As String
        Get
            Return m_MinimumGPA
        End Get
    End Property

    Public ReadOnly Property GradeCourseRepetitionsMethod() As String
        Get
            Return m_GradeCourseRepetitionsMethod
        End Get
    End Property

    Public ReadOnly Property ShowProgramOnTranscript() As Boolean
        Get
            Return m_ShowProgramOnTranscript
        End Get
    End Property

    Public ReadOnly Property IncludeHoursForFailedGrade() As Boolean
        Get
            If m_IncludeHoursForFailedGrade.ToLower = "true" Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

#End Region

#Region "Public Methods"

    Public Function GetEnrollment(ByRef paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strStuEnrollId As String
        Dim iRows As Integer
        Dim m_SchoolLogo As Byte()
        Dim rptDB As New ReportsDB

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        strStuEnrollId = GetStuEnrollId(paramInfo)

        'Dim m_StudentIdentifier As String = SingletonAppSettings.AppSettings("StudentIdentifier")
        Dim strStudentId As String
        If StudentIdentifier = "SSN" Then
            strStudentId = "A.SSN AS StudentNumber, "
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStudentId = "arStuEnrollments.EnrollmentId AS StudentNumber, "
        ElseIf StudentIdentifier = "StudentId" Then
            strStudentId = "A.StudentNumber AS StudentNumber, "
        End If

        With sb
            .Append("SELECT DISTINCT")
            .Append("       arStuEnrollments.StuEnrollId,")
            .Append("       (SELECT ProgDescrip FROM arPrograms X,arPrgVersions Y WHERE Y.PrgVerId=arStuEnrollments.PrgVerId AND Y.ProgId=X.ProgId) AS ProgramDescrip," & strStudentId)
            '.Append("       A.StudentNumber,")

            .Append("       arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip,")
            .Append("       (SELECT SysStatusId FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS SysStatusId, ")
            .Append("       arStuEnrollments.StartDate,arStuEnrollments.ExpGradDate,")
            .Append("       ISNULL((SELECT A.LastName + ', ' + A.FirstName +  ' ' + A.MiddleName), (SELECT A.LastName + ', ' + A.FirstName )) AS StudentName, ")
            .Append("       C.CampDescrip, ")
            .Append("       (SELECT MAX(StartDate) FROM arStuProbWarnings SP WHERE SP.StuEnrollId=arStuEnrollments.StuEnrollId AND SP.ProbWarningTypeId=1) AS BeginProbationDate,")
            .Append("	    (SELECT Max(StartDate) FROM arStudentLOAs SL WHERE SL.StuEnrollId=arStuEnrollments.StuEnrollId) AS BeginLeaveDate, ")
            .Append("       (SELECT Max(EndDate) FROM arStudentLOAs SL WHERE SL.StuEnrollId=arStuEnrollments.StuEnrollId) AS EndLeaveDate,")
            .Append("       arStuEnrollments.LDA, arStuEnrollments.DateDetermined, ")
            .Append("       (SELECT MAX(StartDate) FROM arStdSuspensions SS WHERE SS.StuEnrollId=arStuEnrollments.StuEnrollId) AS BeginSuspensionDate, ")
            .Append("       (SELECT MAX(EndDate) FROM arStdSuspensions SS WHERE SS.StuEnrollId=arStuEnrollments.StuEnrollId) AS EndSuspensionDate ,")
            .Append(" (SELECT UnitTypeDescrip FROM arPrograms X,arPrgVersions Y,arAttUnitType Z ")
            .Append(" WHERE Y.PrgVerId=arStuEnrollments.PrgVerId AND Y.ProgId=X.ProgId and Y.UnitTypeid=Z.UnitTypeid) AS UnitTypeDescrip ")
            .Append("FROM   arStuEnrollments,arStudent A, syCampuses C  ")
            .Append("WHERE  arStuEnrollments.StudentId=A.StudentId ")
            .Append("AND arStuEnrollments.StuEnrollId=? ")
            .Append("AND arStuEnrollments.CampusId=C.CampusId ")

        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "SummaryInfo"
            'Need to add these fields as they are to link the tables in report.
            ds.Tables(0).Columns.Add(New DataColumn("StrStuEnrollId", System.Type.GetType("System.String")))

            'Need this field for the Cutoff date so we can easily extract it for the report
            ds.Tables(0).Columns.Add(New DataColumn("CutOffDate", System.Type.GetType("System.String")))
            If paramInfo.CutoffDate = "" Then
                paramInfo.CutoffDate = (New TermsDB).GetTermInfo(paramInfo.OrderBy.Substring(paramInfo.OrderBy.IndexOf("'")).Replace("'", "")).EndDate
            End If

            ds.Tables(0).Rows(0)("CutOffDate") = paramInfo.CutoffDate
            ds.Tables(0).Columns.Add(New DataColumn("SchoolLogo", System.Type.GetType("System.Byte[]")))
            m_SchoolLogo = rptDB.GetSchoolLogo(True).Image
            ds.Tables(0).Rows(0)("SchoolLogo") = m_SchoolLogo
            ds.Tables(0).Columns.Add("CorporateName", System.Type.GetType("System.String"))
            ds.Tables(0).Columns.Add("FullAddress", System.Type.GetType("System.String"))
            ds.Tables(0).Columns.Add("Phone", System.Type.GetType("System.String"))
            ds.Tables(0).Columns.Add("Fax", System.Type.GetType("System.String"))
            ds.Tables(0).Columns.Add("Website", System.Type.GetType("System.String"))

            iRows = ds.Tables(0).Rows.Count
            'Add new table.
            Dim dt As New DataTable("GeneralInfo")
            dt.Columns.Add(New DataColumn("StuEnrollId", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("StartDate", System.Type.GetType("System.DateTime")))
            dt.Columns.Add(New DataColumn("LastDateAttended", System.Type.GetType("System.DateTime")))
            dt.Columns.Add(New DataColumn("ExpectedGradDate", System.Type.GetType("System.DateTime")))
            dt.Columns.Add(New DataColumn("OverallAverage", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TotalCost", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CurrentBalance", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("WeeklySchedHours", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TotalDaysAttended", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("ScheduledDays", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("DaysAbsent", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("MakeupDays", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("AttendancePercent", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("StudentGroups", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Attendance", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("AttendanceType", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("RossType", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ShowWorkUnits", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ShowGroupWorkUnits", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ShowStudentsignature", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ShowSchoolsignature", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ShowTermOrModule", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("GradeType", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ByClassAttendanceType", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ClockHourSchool", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ShowCurrentBalance", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ShowTotalCost", System.Type.GetType("System.String")))



            ds.Tables.Add(dt)

            'Add new table.
            'dt = New DataTable("ModuleCoursesResults")
            'dt.Columns.Add(New DataColumn("StuEnrollId", System.Type.GetType("System.String")))
            'dt.Columns.Add(New DataColumn("TermId", System.Type.GetType("System.String")))
            'dt.Columns.Add(New DataColumn("TermDescrip", System.Type.GetType("System.String")))
            'dt.Columns.Add(New DataColumn("StartDate", System.Type.GetType("System.DateTime")))
            'dt.Columns.Add(New DataColumn("EndDate", System.Type.GetType("System.DateTime")))
            'dt.Columns.Add(New DataColumn("Code", System.Type.GetType("System.String")))
            'dt.Columns.Add(New DataColumn("Descrip", System.Type.GetType("System.String")))
            'dt.Columns.Add(New DataColumn("ReqId", System.Type.GetType("System.String")))
            'ds.Tables.Add(dt)

        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Public Function GetEnrollmentList(ByRef paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strWhere As String
        Dim strTempWhere As String = ""
        Dim strTerm As String = ""
        Dim iRows As Integer
        Dim m_SchoolLogo As Byte()
        Dim rptDB As New ReportsDB

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Dim m_StudentIdentifier As String = SingletonAppSettings.AppSettings("StudentIdentifier")
        Dim strStudentId As String
        If StudentIdentifier = "SSN" Then
            strStudentId = "A.SSN AS StudentNumber, "
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStudentId = "arStuEnrollments.EnrollmentId AS StudentNumber, "
        ElseIf StudentIdentifier = "StudentId" Then
            strStudentId = "A.StudentNumber AS StudentNumber, "
        End If

        'Get ProgVerId and Where Clause from paramInfo.FilterList
        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        'Get StudentId and rest of Where Clause from paramInfo.FilterOther


        strTempWhere = strWhere
        strTempWhere = strTempWhere.ToLower.Replace("and", ";")
        strWhere = ""
        Dim strArr As String() = strTempWhere.Remove(strTempWhere.IndexOf(";"), 1).Split(";")

        For i As Integer = 0 To strArr.Length - 1
            If strArr(i).ToLower.IndexOf("arterm") > 0 Then
                strTerm &= " AND " & strArr(i)
            Else
                strWhere &= " AND " & strArr(i)
            End If
        Next
        If paramInfo.FilterOther <> "" Then
            If InStr(paramInfo.FilterOther, "PM") > 0 Then
                strWhere &= " AND " & paramInfo.FilterOther.Substring(0, InStr(paramInfo.FilterOther, "PM") + 2)
            End If
        End If

        'If a student group is selected it affects the basic query that we need to
        'run because each enrollment can belong to one or more student groups.
        'If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
        With sb
            .Append("SELECT DISTINCT")
            .Append("       arStuEnrollments.StuEnrollId,")
            .Append("       (SELECT ProgDescrip FROM arPrograms X,arPrgVersions Y WHERE Y.PrgVerId=arStuEnrollments.PrgVerId AND Y.ProgId=X.ProgId) AS ProgramDescrip," & strStudentId)
            '.Append("       A.StudentNumber,")
            .Append("       arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip,")
            .Append("       (SELECT SysStatusId FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS SysStatusId, ")
            .Append("       arStuEnrollments.StartDate,arStuEnrollments.ExpGradDate,")
            .Append("       ISNULL((SELECT A.LastName + ', ' + A.FirstName +  ' ' + A.MiddleName), (SELECT A.LastName + ', ' + A.FirstName )) AS StudentName, ")
            .Append("       syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,C.CampDescrip, ")
            .Append("       (SELECT MAX(StartDate) FROM arStuProbWarnings SP WHERE SP.StuEnrollId=arStuEnrollments.StuEnrollId AND SP.ProbWarningTypeId=1) AS BeginProbationDate,")
            .Append("	    (SELECT Max(StartDate) FROM arStudentLOAs SL WHERE SL.StuEnrollId=arStuEnrollments.StuEnrollId) AS BeginLeaveDate, ")
            .Append("       (SELECT Max(EndDate) FROM arStudentLOAs SL WHERE SL.StuEnrollId=arStuEnrollments.StuEnrollId) AS EndLeaveDate,")
            .Append("       arStuEnrollments.LDA, arStuEnrollments.DateDetermined, ")
            .Append("       (SELECT MAX(StartDate) FROM arStdSuspensions SS WHERE SS.StuEnrollId=arStuEnrollments.StuEnrollId) AS BeginSuspensionDate, ")
            .Append("       (SELECT MAX(EndDate) FROM arStdSuspensions SS WHERE SS.StuEnrollId=arStuEnrollments.StuEnrollId) AS EndSuspensionDate, ")
            .Append(" (SELECT UnitTypeDescrip FROM arPrograms X,arPrgVersions Y,arAttUnitType Z ")
            .Append(" WHERE Y.PrgVerId=arStuEnrollments.PrgVerId AND Y.ProgId=X.ProgId and Y.UnitTypeid=Z.UnitTypeid) AS UnitTypeDescrip,A.FirstName,A.LastName,A.MiddleName  ")
            .Append("FROM   arStuEnrollments,arStudent A, syCampuses C, syCmpGrpCmps,syCampGrps  ")

            If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                .Append(",adLeadByLeadGroups, adLeadGroups ")
            End If

            .Append("WHERE  arStuEnrollments.StudentId=A.StudentId ")
            .Append("AND arStuEnrollments.CampusId=C.CampusId ")
            .Append("AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId ")
            .Append("AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId ")
            .Append("AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
            '.Append("AND syCampGrps.CampGrpDescrip <> 'All' ")

            If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                .Append("AND adLeadByLeadGroups.LeadGrpId=adLeadGroups.LeadGrpId ")
                .Append("AND adLeadByLeadGroups.StuEnrollId=arStuEnrollments.StuEnrollId ")
            End If

            .Append(strWhere)
            If paramInfo.OrderBy <> "" Then
                .Append(" order by " & paramInfo.OrderBy.Replace("arStudent", "A"))
            End If
        End With



        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)
        If strTerm <> "" Then
            paramInfo.CutoffDate = (New TermsDB).GetTermInfo(strTerm.Substring(strTerm.IndexOf("'")).Replace("'", "")).EndDate
            'paramInfo.CutoffDate=(New ter
        End If

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "SummaryInfo"
            'Need to add these fields as they are to link the tables in report.
            ds.Tables(0).Columns.Add(New DataColumn("StrStuEnrollId", System.Type.GetType("System.String")))

            'Need this field for the Cutoff date so we can easily extract it for the report
            ds.Tables(0).Columns.Add(New DataColumn("CutOffDate", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("SchoolLogo", System.Type.GetType("System.Byte[]")))
            m_SchoolLogo = rptDB.GetSchoolLogo(True).Image
            If ds.Tables(0).Rows.Count > 0 Then ds.Tables(0).Rows(0)("SchoolLogo") = m_SchoolLogo
            ds.Tables(0).Columns.Add("CorporateName", System.Type.GetType("System.String"))
            ds.Tables(0).Columns.Add("FullAddress", System.Type.GetType("System.String"))
            ds.Tables(0).Columns.Add("Phone", System.Type.GetType("System.String"))
            ds.Tables(0).Columns.Add("Fax", System.Type.GetType("System.String"))
            ds.Tables(0).Columns.Add("Website", System.Type.GetType("System.String"))
            'Add the CutOffDate to each row in the DataTable
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                ds.Tables(0).Rows(i)("CutOffDate") = paramInfo.CutoffDate
            Next


            'Add new table.
            Dim dt As New DataTable("GeneralInfo")
            dt.Columns.Add(New DataColumn("StuEnrollId", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("StartDate", System.Type.GetType("System.DateTime")))
            dt.Columns.Add(New DataColumn("LastDateAttended", System.Type.GetType("System.DateTime")))
            dt.Columns.Add(New DataColumn("ExpectedGradDate", System.Type.GetType("System.DateTime")))
            dt.Columns.Add(New DataColumn("OverallAverage", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TotalCost", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CurrentBalance", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("WeeklySchedHours", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("TotalDaysAttended", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("ScheduledDays", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("DaysAbsent", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("MakeupDays", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("AttendancePercent", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("Attendance", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("StudentGroups", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("AttendanceType", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("RossType", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ShowWorkUnits", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ShowGroupWorkUnits", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ShowStudentsignature", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ShowSchoolsignature", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("GradeType", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ByClassAttendanceType", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ClockHourSchool", System.Type.GetType("System.String")))
            ds.Tables.Add(dt)

        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds


    End Function
    Public Function GetEnrollmentList_sp(ByRef paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim strWhere As String
        Dim strTempWhere As String = ""
        Dim strOrder(2) As String
        Dim strTerm As String = ""
        Dim iRows As Integer
        Dim m_SchoolLogo As Byte()
        Dim rptDB As New ReportsDB
        Dim db As New SQLDataAccess
        'db.ConnectionString = SingletonAppSettings.AppSettings("ConnectionString")
        'Dim m_StudentIdentifier As String = SingletonAppSettings.AppSettings("StudentIdentifier")
        Dim strStudentId As String
        If StudentIdentifier = "SSN" Then
            strStudentId = "A.SSN AS StudentNumber, "
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStudentId = "arStuEnrollments.EnrollmentId AS StudentNumber, "
        ElseIf StudentIdentifier = "StudentId" Then
            strStudentId = "A.StudentNumber AS StudentNumber, "
        End If

        'Get ProgVerId and Where Clause from paramInfo.FilterList
        If paramInfo.FilterList <> "" Then
            strWhere &= " and " & paramInfo.FilterList
        End If


        '********************************************************************************'
        Dim campGrpId As String = String.Empty
        Dim prgVerId As String = String.Empty
        Dim statusCodeId As String = String.Empty
        Dim leadGrpId As String = String.Empty
        Dim strStuEnrollid As String = String.Empty
        '********************************************************************************'


        'Get StudentId and rest of Where Clause from paramInfo.FilterOther


        strTempWhere = strWhere
        strTempWhere = strTempWhere.ToLower.Replace("and", ";")
        strWhere = ""
        Dim strArr As String() = strTempWhere.Remove(strTempWhere.IndexOf(";"), 1).Split(";")

        For i As Integer = 0 To strArr.Length - 1
            If strArr(i).ToLower.IndexOf("arterm") > 0 Then
                strTerm &= " and " & strArr(i)
            Else
                strWhere &= " and " & strArr(i)
            End If
        Next
        If paramInfo.FilterOther <> "" Then
            If InStr(paramInfo.FilterOther, "PM") > 0 Then
                strWhere &= " and " & paramInfo.FilterOther.Substring(0, InStr(paramInfo.FilterOther, "PM") + 2)
            End If
        End If
        If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
            leadGrpId = True
        End If

        '********************************************************************************'
        'get the values of the parameters
        If paramInfo.FilterList.ToLower.Contains("arstuenrollments.stuenrollid") Then
            strStuEnrollid = paramInfo.FilterList.ToLower.Replace("arstuenrollments.stuenrollid =", "").Replace("'", "").Trim()
            db.AddParameter("@stuEnrollId", New Guid(strStuEnrollid), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.usp_GetEnrollment")
        Else
            campGrpId = strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in (")).ToLower.IndexOf(" and ")).Replace("t1.campgrpid in", "").Replace(")", "").Replace("(", "")

            'get the prgverid
            If strWhere.ToLower.Contains("arstuenrollments.prgverid in ") Then
                If strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid in ")).ToLower.IndexOf(" and ") >= 0 Then
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid in (")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.prgverid in", "").Replace(")", "").Replace("(", "")
                Else
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid in (")).Replace("arstuenrollments.prgverid in", "").Replace(")", "").Replace("(", "")
                End If
            Else
                If strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid = ")).ToLower.IndexOf(" and ") >= 0 Then
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid = "), strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid = ")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.prgverid =", "").Replace(")", "").Replace("(", "")
                Else
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid = ")).Replace("arstuenrollments.prgverid =", "").Replace(")", "").Replace("(", "")
                End If
            End If
            'end the prgverid

            'get the statuscode
            If strWhere.ToLower.Contains("arstuenrollments.statuscodeid") Then
                If strWhere.Contains("arstuenrollments.statuscodeid in ") Then
                    If strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid in ")).ToLower.IndexOf(" and ") >= 0 Then
                        statusCodeId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid in (")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.statuscodeid in", "").Replace(")", "").Replace("(", "")
                    Else
                        statusCodeId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid in (")).Replace("arstuenrollments.statuscodeid in", "").Replace(")", "").Replace("(", "")
                    End If
                Else
                    If strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).ToLower.IndexOf(" and ") >= 0 Then
                        statusCodeId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid = "), strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.statuscodeid =", "").Replace(")", "").Replace("(", "")
                    Else
                        statusCodeId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).Replace("arstuenrollments.statuscodeid =", "").Replace(")", "").Replace("(", "")
                    End If
                End If
            End If
            'end the statuscode

            'get the leadGrpid
            If strWhere.ToLower.Contains("adleadgroups.leadgrpid") Then
                If strWhere.Contains("adleadgroups.leadgrpid in ") Then
                    If strWhere.Substring(strWhere.ToLower.IndexOf("adleadgroups.leadgrpid in ")).ToLower.IndexOf(" and ") >= 0 Then
                        leadGrpId = strWhere.Substring(strWhere.ToLower.IndexOf("adleadgroups.leadgrpid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("adleadgroups.leadgrpid in (")).ToLower.IndexOf(" and ")).Replace("adleadgroups.leadgrpid in", "").Replace(")", "").Replace("(", "")
                    Else
                        leadGrpId = strWhere.Substring(strWhere.ToLower.IndexOf("adleadgroups.leadgrpid in (")).Replace("adleadgroups.leadgrpid in", "").Replace(")", "").Replace("(", "")
                    End If
                Else
                    If strWhere.Substring(strWhere.ToLower.IndexOf("adleadgroups.leadgrpid = ")).ToLower.IndexOf(" and ") >= 0 Then
                        leadGrpId = strWhere.Substring(strWhere.ToLower.IndexOf("adleadgroups.leadgrpid = "), strWhere.Substring(strWhere.ToLower.IndexOf("adleadgroups.leadgrpid = ")).ToLower.IndexOf(" and ")).Replace("adleadgroups.leadgrpid =", "").Replace(")", "").Replace("(", "")
                    Else
                        leadGrpId = strWhere.Substring(strWhere.ToLower.IndexOf("adleadgroups.leadgrpid = ")).Replace("adleadgroups.leadgrpid =", "").Replace(")", "").Replace("(", "")
                    End If
                End If
            End If

            'end the leadgrpid

            '********************************************************************************'
            campGrpId = campGrpId.Replace(" ", "").Replace("'", "")
            'campGrpId = "'" + campGrpId + "'"
            prgVerId = prgVerId.Replace(" ", "").Replace("'", "")
            'prgVerId = "'" + prgVerId + "'"

            If paramInfo.GroupByClass = True Then
                If leadGrpId = String.Empty Then
                    db.AddParameter("@campGrpId", campGrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    db.AddParameter("@campusId", New Guid(paramInfo.CampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                    db.AddParameter("@prgVerId", prgVerId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    If statusCodeId = String.Empty Then
                        db.AddParameter("@statusCodeId", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    Else
                        statusCodeId = statusCodeId.Replace(" ", "").Replace("'", "")
                        'statusCodeId = "'" + statusCodeId + "'"
                        db.AddParameter("@statusCodeId", statusCodeId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    End If
                    If strTerm = String.Empty Then
                        db.AddParameter("@termId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@termId", New Guid(strTerm.Substring(strTerm.IndexOf("'")).Replace("'", "")), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                    End If


                    ds = db.RunParamSQLDataSet_SP("dbo.usp_GetEnrollmentListWithNoLeadGrps_GroupByClass")
                Else
                    db.AddParameter("@campGrpId", campGrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    db.AddParameter("@campusId", New Guid(paramInfo.CampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                    db.AddParameter("@prgVerId", prgVerId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    If statusCodeId = String.Empty Then
                        db.AddParameter("@statusCodeId", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    Else
                        statusCodeId = statusCodeId.Replace(" ", "").Replace("'", "")
                        'statusCodeId = "'" + statusCodeId + "'"
                        db.AddParameter("@statusCodeId", statusCodeId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    End If
                    If strTerm = String.Empty Then
                        db.AddParameter("@termId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@termId", New Guid(strTerm.Substring(strTerm.IndexOf("'")).Replace("'", "")), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                    End If
                    leadGrpId = leadGrpId.Replace(" ", "").Replace("'", "")
                    db.AddParameter("@leadGrpId", leadGrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    ds = db.RunParamSQLDataSet_SP("dbo.usp_GetEnrollmentListWithLeadGrps_GroupByClass")

                End If
            Else
                If leadGrpId = String.Empty Then
                    db.AddParameter("@campGrpId", campGrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    db.AddParameter("@campusId", New Guid(paramInfo.CampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                    db.AddParameter("@prgVerId", prgVerId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    If statusCodeId = String.Empty Then
                        db.AddParameter("@statusCodeId", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    Else
                        statusCodeId = statusCodeId.Replace(" ", "").Replace("'", "")
                        'statusCodeId = "'" + statusCodeId + "'"
                        db.AddParameter("@statusCodeId", statusCodeId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    End If

                    ds = db.RunParamSQLDataSet_SP("dbo.usp_GetEnrollmentListWithNoLeadGrps")
                Else
                    db.AddParameter("@campGrpId", campGrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    db.AddParameter("@campusId", New Guid(paramInfo.CampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                    db.AddParameter("@prgVerId", prgVerId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    If statusCodeId = String.Empty Then
                        db.AddParameter("@statusCodeId", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    Else
                        statusCodeId = statusCodeId.Replace(" ", "").Replace("'", "")
                        'statusCodeId = "'" + statusCodeId + "'"
                        db.AddParameter("@statusCodeId", statusCodeId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    End If
                    leadGrpId = leadGrpId.Replace(" ", "").Replace("'", "")
                    db.AddParameter("@leadGrpId", leadGrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    ds = db.RunParamSQLDataSet_SP("dbo.usp_GetEnrollmentListWithLeadGrps")

                End If
            End If






        End If
        If paramInfo.OrderBy <> "" Then
            strOrder = GetOrderArray(paramInfo.OrderBy.Replace("arStudent.", ""))
        Else
            strOrder(0) = "LastName"
            strOrder(1) = "LastName"
            strOrder(2) = "LastName"
            'strOrder = paramInfo.OrderBy.Replace("arStudent.", "")

        End If



        If strTerm <> "" Then
            paramInfo.CutoffDate = (New TermsDB).GetTermEndDate_SP(strTerm.Substring(strTerm.IndexOf("'")).Replace("'", ""))
            'paramInfo.CutoffDate=(New ter
        End If
        'Dim gradDate As DateTime = CDate("2/28/2010")
        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "SummaryInfo"

            Dim gradDate1 As DateTime
            Dim gradDate2 As DateTime
            Dim str As String = String.Empty
            Dim SummaryInfo As DataTable = ds.Tables("SummaryInfo")
            ' SummaryInfo.DefaultView.Sort = strOrder
            ds.Tables.RemoveAt(0)

            If paramInfo.FilterOther.ToLower.Contains("arstuenrollments.expgraddate") Then

                '*****************************************************************'
                'this varies depending upon the condition

                If paramInfo.FilterOther.ToUpper.IndexOf("NOT BETWEEN") >= 0 Then
                    '"arStuEnrollments.ExpGradDate NOT BETWEEN '2/28/2010 12:00:00 AM' AND '2/28/2010 11:59:59 PM'
                    str = paramInfo.FilterOther.Substring(0, paramInfo.FilterOther.LastIndexOf("'"))
                    str = str.Replace("arStuEnrollments.ExpGradDate NOT BETWEEN ", "")
                    gradDate1 = str.Substring(1, str.IndexOf("AND") - 3).Trim
                    gradDate2 = str.Substring(str.IndexOf("AND")).Trim.Replace("AND", "").Replace("'", "").Trim
                    Dim query = From order In SummaryInfo.AsEnumerable() _
                    Where order.Field(Of DateTime)("ExpGradDate") < gradDate1 Or order.Field(Of DateTime)("ExpGradDate") > gradDate2 Select order Order By order.Field(Of String)(strOrder(0)), order.Field(Of String)(strOrder(1)), order.Field(Of String)(strOrder(2))

                    If query.Count > 0 Then
                        SummaryInfo = query.CopyToDataTable()
                    Else
                        SummaryInfo = New DataTable()
                    End If
                ElseIf paramInfo.FilterOther.ToUpper.IndexOf("BETWEEN") >= 0 Then
                    str = paramInfo.FilterOther.Substring(0, paramInfo.FilterOther.LastIndexOf("'"))
                    str = str.Replace("arStuEnrollments.ExpGradDate BETWEEN ", "")
                    gradDate1 = str.Substring(1, str.IndexOf("AND") - 3).Trim
                    gradDate2 = str.Substring(str.IndexOf("AND")).Trim.Replace("AND", "").Replace("'", "").Trim
                    Dim query = From order In SummaryInfo.AsEnumerable() _
                    Where order.Field(Of DateTime)("ExpGradDate") >= gradDate1 And order.Field(Of DateTime)("ExpGradDate") <= gradDate2 Select order Order By order.Field(Of String)(strOrder(0)), order.Field(Of String)(strOrder(1)), order.Field(Of String)(strOrder(2)) 'Select order Order By order.Field(Of String)("MiddleName"), order.Field(Of String)("LastName")
                    If query.Count > 0 Then
                        SummaryInfo = query.CopyToDataTable()
                    Else
                        SummaryInfo = New DataTable()
                    End If

                ElseIf paramInfo.FilterOther.IndexOf(">") >= 0 Then
                    ''arStuEnrollments.ExpGradDate > '2/28/2010 11:59:59 PM'
                    str = paramInfo.FilterOther.Substring(0, paramInfo.FilterOther.LastIndexOf("'"))
                    str = str.Replace("arStuEnrollments.ExpGradDate > ", "").Replace("'", "").Trim
                    gradDate1 = str
                    Dim query = From order In SummaryInfo.AsEnumerable() _
                   Where order.Field(Of DateTime)("ExpGradDate") > gradDate1 Select order Order By order.Field(Of String)(strOrder(0)), order.Field(Of String)(strOrder(1)), order.Field(Of String)(strOrder(2)) 'Select order Order By order.Field(Of String)("MiddleName"), order.Field(Of String)("LastName")
                    If query.Count > 0 Then
                        SummaryInfo = query.CopyToDataTable()
                    Else
                        SummaryInfo = New DataTable()
                    End If
                ElseIf paramInfo.FilterOther.IndexOf("<") >= 0 Then
                    str = paramInfo.FilterOther.Substring(0, paramInfo.FilterOther.LastIndexOf("'"))
                    str = str.Replace("arStuEnrollments.ExpGradDate < ", "").Replace("'", "").Trim
                    gradDate1 = str
                    Dim query = From order In SummaryInfo.AsEnumerable() _
                   Where order.Field(Of DateTime)("ExpGradDate") < gradDate1 Select order Order By order.Field(Of String)(strOrder(0)), order.Field(Of String)(strOrder(1)), order.Field(Of String)(strOrder(2)) 'Select order Order By order.Field(Of String)("MiddleName"), order.Field(Of String)("LastName")
                    If query.Count > 0 Then
                        SummaryInfo = query.CopyToDataTable()
                    Else
                        SummaryInfo = New DataTable()
                    End If
                End If

                '********************************************************************'

            Else
                Dim query = From order In SummaryInfo.AsEnumerable() _
                Select order Order By order.Field(Of String)(strOrder(0)), order.Field(Of String)(strOrder(1)), order.Field(Of String)(strOrder(2))

                If query.Count > 0 Then
                    SummaryInfo = query.CopyToDataTable()
                Else
                    SummaryInfo = New DataTable()
                End If
            End If
            ds.Tables.Add(SummaryInfo)
            ds.Tables(0).TableName = "SummaryInfo"

            'Need to add these fields as they are to link the tables in report.
            ds.Tables(0).Columns.Add(New DataColumn("StrStuEnrollId", System.Type.GetType("System.String")))

            'Need this field for the Cutoff date so we can easily extract it for the report
            ds.Tables(0).Columns.Add(New DataColumn("CutOffDate", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("SchoolLogo", System.Type.GetType("System.Byte[]")))
            m_SchoolLogo = rptDB.GetSchoolLogo(True).Image
            If ds.Tables(0).Rows.Count > 0 Then ds.Tables(0).Rows(0)("SchoolLogo") = m_SchoolLogo
            ds.Tables(0).Columns.Add("CorporateName", System.Type.GetType("System.String"))
            ds.Tables(0).Columns.Add("FullAddress", System.Type.GetType("System.String"))
            ds.Tables(0).Columns.Add("Address", System.Type.GetType("System.String"))
            ds.Tables(0).Columns.Add("Phone", System.Type.GetType("System.String"))
            ds.Tables(0).Columns.Add("Fax", System.Type.GetType("System.String"))
            ds.Tables(0).Columns.Add("Website", System.Type.GetType("System.String"))
            ds.Tables(0).Columns.Add("GroupByClass", System.Type.GetType("System.Boolean"))
            ds.Tables(0).Columns.Add("SuppressStudentAddress", System.Type.GetType("System.String"))
            ds.Tables(0).Columns.Add("SuppressStudentPhone", System.Type.GetType("System.String"))
            ds.Tables(0).Columns.Add("SuppressOverallAverage", System.Type.GetType("System.String"))
            ds.Tables(0).Columns.Add("ShowOfficialTitle", System.Type.GetType("System.Boolean"))
            ''New Code Added By Vijay Ramteke On August 17, 2010 For Mantis Id 19545
            ds.Tables(0).Columns.Add("TranscriptAuthznTitle", System.Type.GetType("System.String"))
            ds.Tables(0).Columns.Add("TranscriptAuthznName", System.Type.GetType("System.String"))
            ''New Code Added By Vijay Ramteke On August 17, 2010 For Mantis Id 19545
            'Add the CutOffDate to each row in the DataTable
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                ds.Tables(0).Rows(i)("CutOffDate") = paramInfo.CutoffDate
                ds.Tables(0).Rows(i)("GroupByClass") = paramInfo.GroupByClass
            Next


            'Add new table.
            Dim dt As New DataTable("GeneralInfo")
            dt = GetGeneralInfoForMultipleStudents_sp(campGrpId, prgVerId, statusCodeId, leadGrpId, paramInfo.CutoffDate, paramInfo.FilterOther, strOrder, strStuEnrollid, paramInfo.CampusId)
            'dt.Columns.Add(New DataColumn("StuEnrollId", System.Type.GetType("System.String")))
            'dt.Columns.Add(New DataColumn("StartDate", System.Type.GetType("System.DateTime")))
            dt.Columns.Add(New DataColumn("LastDateAttended", System.Type.GetType("System.DateTime")))
            dt.Columns.Add(New DataColumn("ExpectedGradDate", System.Type.GetType("System.DateTime")))
            dt.Columns.Add(New DataColumn("OverallAverage", System.Type.GetType("System.Decimal")))
            'dt.Columns.Add(New DataColumn("TotalCost", System.Type.GetType("System.Decimal")))
            'dt.Columns.Add(New DataColumn("CurrentBalance", System.Type.GetType("System.Decimal")))
            'dt.Columns.Add(New DataColumn("WeeklySchedHours", System.Type.GetType("System.Decimal")))
            'dt.Columns.Add(New DataColumn("TotalDaysAttended", System.Type.GetType("System.Decimal")))
            'dt.Columns.Add(New DataColumn("ScheduledDays", System.Type.GetType("System.Decimal")))
            'dt.Columns.Add(New DataColumn("DaysAbsent", System.Type.GetType("System.Decimal")))
            'dt.Columns.Add(New DataColumn("MakeupDays", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("AttendancePercent", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("Attendance", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("StudentGroups", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("AttendanceType", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("RossType", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ShowWorkUnits", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ShowGroupWorkUnits", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ShowStudentsignature", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ShowSchoolsignature", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ShowTermOrModule", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("GradeType", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ByClassAttendanceType", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ClockHourSchool", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ShowCurrentBalance", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("ShowTotalCost", System.Type.GetType("System.String")))
            '' New Code Added By Vijay Ramteke On August 17, 2010 For Mantis Id 19545
            dt.Columns.Add(New DataColumn("ShowLegalDisclaimer", System.Type.GetType("System.Int32")))
            '' New Code Added By Vijay Ramteke On August 17, 2010 For Mantis Id 19545
            ds.Tables.Add(dt.Copy)
            ds.Tables(1).TableName = "GeneralInfo"

        End If

        Try
            Return ds
        Catch ex As Exception
        Finally
            db.CloseConnection()
        End Try



    End Function


    Public Function GetGeneralInfo(ByVal paramInfo As ReportParamInfo) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strStuEnrollId As String
        Dim strCutOffDate As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        strStuEnrollId = GetStuEnrollId(paramInfo)
        'strCutOffDate = GetCutOffDate(paramInfo)
        strCutOffDate = paramInfo.CutoffDate
        Dim refTime As String = " 11:59:59 PM"
        With sb
            .Append("SELECT ")
            .Append("   StuEnrollId,StartDate,LDA,ExpGradDate, ")
            .Append("   (SELECT SUM(ST.TransAmount) ")
            .Append("    FROM saTransactions ST, saTransCodes SC ")
            .Append("    WHERE ST.StuEnrollId=? AND ST.TranscodeId=SC.TransCodeId ")
            .Append("    AND ST.Voided = 0 AND SC.IsInstCharge = 1   and ST.Transtypeid in (0,1) AND TransDate <= ?) AS TotalCost, ")
            .Append("   (SELECT SUM(TransAmount) ")
            .Append("    FROM saTransactions ")
            .Append("    WHERE StuEnrollId=? AND Voided = 0 AND TransDate <= ?) AS CurrentBalance, ")
            .Append("   (SELECT SUM(Actual) ")
            .Append("    FROM atConversionAttendance ")
            .Append("    WHERE StuEnrollId=? ")
            .Append("    AND Actual > 0.0 AND MeetDate <= ?) AS TotalDaysAttended, ")
            .Append("   (SELECT SUM(Schedule) ")
            .Append("    FROM atConversionAttendance ")
            .Append("    WHERE StuEnrollId=? ")
            .Append("    AND Schedule > 0.0 AND MeetDate <= ?) AS ScheduledDays, ")
            .Append("   (SELECT SUM(Schedule-Actual) ")
            .Append("    FROM atConversionAttendance ")
            .Append("    WHERE StuEnrollId=? ")
            .Append("    AND Schedule > 0 AND MeetDate <= ?) AS DaysAbsent, ")
            .Append("   (SELECT SUM(Actual) ")
            .Append("    FROM atConversionAttendance ")
            .Append("    WHERE StuEnrollId=? ")
            .Append("    AND Actual > 0 ")
            .Append("    AND Schedule = 0 AND MeetDate <= ?) AS MakeupDays, ")
            .Append("   (SELECT SUM(PSD.Total) ")
            .Append("    FROM arStudentSchedules SS, arProgScheduleDetails PSD ")
            .Append("    WHERE StuEnrollId=? ")
            .Append("    AND SS.ScheduleId=PSD.ScheduleId) AS WeeklySchedHours ")
            .Append("FROM   arStuEnrollments ")
            .Append("WHERE  StuEnrollId=? ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        db.AddParameter("sid1", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("cd1", strCutOffDate & refTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.AddParameter("sid2", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("cd2", strCutOffDate & refTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.AddParameter("sid3", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("cd3", strCutOffDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.AddParameter("sid4", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("cd4", strCutOffDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.AddParameter("sid5", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("cd5", strCutOffDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.AddParameter("sid6", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("cd6", strCutOffDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.AddParameter("sid7", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("sid8", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function

    Public Function GetGeneralInfoForMultipleStudents(ByVal paramInfo As ReportParamInfo) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strWhere As String
        Dim strTempWhere As String = ""
        Dim strTerm As String = ""
        Dim strCutOffDate As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Commented out for testing purposes only
        strCutOffDate = paramInfo.CutoffDate

        'Get ProgVerId and Where Clause from paramInfo.FilterList
        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If



        strTempWhere = strWhere
        strTempWhere = strTempWhere.ToLower.Replace("and", ";")
        strWhere = ""
        Dim strArr As String() = strTempWhere.Remove(strTempWhere.IndexOf(";"), 1).Split(";")

        For i As Integer = 0 To strArr.Length - 1
            If strArr(i).ToLower.IndexOf("arterm") > 0 Then
                strTerm &= " AND " & strArr(i)
            Else
                strWhere &= " AND " & strArr(i)
            End If
        Next

        If paramInfo.FilterOther <> "" Then
            If InStr(paramInfo.FilterOther, "PM") > 0 Then
                strWhere &= " AND " & paramInfo.FilterOther.Substring(0, InStr(paramInfo.FilterOther, "PM") + 2)
            End If
        End If
        With sb
            .Append("SELECT ")
            .Append("   StuEnrollId,StartDate,LDA,ExpGradDate, ")
            .Append("   (SELECT SUM(ST.TransAmount) ")
            .Append("    FROM saTransactions ST, saTransCodes SC ")
            .Append("    WHERE ST.StuEnrollId=SE.StuEnrollId AND ST.TranscodeId=SC.TransCodeId ")
            .Append("    AND ST.Voided = 0 AND SC.IsInstCharge = 1   and ST.Transtypeid in (0,1) AND TransDate <= ?) AS TotalCost, ")
            .Append("   (SELECT SUM(TransAmount) ")
            .Append("    FROM saTransactions ")
            .Append("    WHERE StuEnrollId=SE.StuEnrollId AND Voided = 0 AND TransDate <= ?) AS CurrentBalance, ")
            .Append("   (SELECT SUM(Actual) ")
            .Append("    FROM atConversionAttendance ")
            .Append("    WHERE StuEnrollId=SE.StuEnrollId ")
            .Append("    AND Actual > 0.0 AND MeetDate <= ?) AS TotalDaysAttended, ")
            .Append("   (SELECT SUM(Schedule) ")
            .Append("    FROM atConversionAttendance ")
            .Append("    WHERE StuEnrollId=SE.StuEnrollId ")
            .Append("    AND Schedule > 0.0 AND MeetDate <= ?) AS ScheduledDays, ")
            .Append("   (SELECT SUM(Schedule-Actual) ")
            .Append("    FROM atConversionAttendance ")
            .Append("    WHERE StuEnrollId=SE.StuEnrollId ")
            .Append("    AND Schedule > 0 AND MeetDate <= ?) AS DaysAbsent, ")
            .Append("   (SELECT SUM(Actual) ")
            .Append("    FROM atConversionAttendance ")
            .Append("    WHERE StuEnrollId=SE.StuEnrollId ")
            .Append("    AND Actual > 0 ")
            .Append("    AND Schedule = 0 AND MeetDate <= ?) AS MakeupDays, ")
            .Append("   (SELECT SUM(PSD.Total) ")
            .Append("    FROM arStudentSchedules SS, arProgScheduleDetails PSD ")
            .Append("    WHERE StuEnrollId=SE.StuEnrollId ")
            .Append("    AND SS.ScheduleId=PSD.ScheduleId) AS WeeklySchedHours ")
            .Append("FROM   arStuEnrollments SE,arStudent ")
            .Append("WHERE   SE.StudentId=arStudent.StudentId  and EXISTS (SELECT DISTINCT arStuEnrollments.StuEnrollId ")
            .Append("              FROM arStuEnrollments, arStudent A, syCampuses C, syCmpGrpCmps,syCampGrps  ")

            If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                .Append(",adLeadByLeadGroups, adLeadGroups ")
            End If

            .Append("              WHERE arStuEnrollments.StudentId=A.StudentId ")
            .Append("              AND arStuEnrollments.CampusId=C.CampusId ")
            .Append("              AND arStuEnrollments.StuEnrollId=SE.StuEnrollId ")
            .Append("              AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId ")
            .Append("              AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")

            If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                .Append("AND adLeadByLeadGroups.LeadGrpId=adLeadGroups.LeadGrpId ")
                .Append("AND adLeadByLeadGroups.StuEnrollId=arStuEnrollments.StuEnrollId ")
            End If

            .Append(strWhere)
            .Append(") ")
            If paramInfo.OrderBy <> "" Then
                .Append(" order by " & paramInfo.OrderBy.ToString)
            End If
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        db.AddParameter("cd1", strCutOffDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("cd2", strCutOffDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("cd3", strCutOffDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("cd4", strCutOffDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("cd5", strCutOffDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("cd6", strCutOffDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function
    Public Function GetGeneralInfoForMultipleStudents_sp(ByVal campGrpId As String, ByVal prgVerId As String, ByVal statuscodeId As String, ByVal leadgrpId As String, ByVal cutOffDate As Date, ByVal filterOthers As String, ByVal strOrder As String(), Optional ByVal stuenrollid As String = "", Optional ByVal campusId As String = "") As DataTable
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
        Dim rtnDataTable As New DataTable
        Dim refTime As String = " 11:59:59 PM"
        If stuenrollid <> "" Then
            db.AddParameter("@stuEnrollId", New Guid(stuenrollid), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@cutOffDate", cutOffDate & refTime, SqlDbType.DateTime, , ParameterDirection.Input)
            rtnDataTable = db.RunParamSQLDataSet_SP("dbo.usp_GetGeneralInfo").Tables(0)
        Else


            If leadgrpId = String.Empty Then
                db.AddParameter("@campGrpId", campGrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                db.AddParameter("@campusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@prgVerId", prgVerId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                If statuscodeId = String.Empty Then
                    db.AddParameter("@statusCodeId", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                Else
                    statuscodeId = statuscodeId.Replace(" ", "").Replace("'", "")
                    'statusCodeId = "'" + statusCodeId + "'"
                    db.AddParameter("@statusCodeId", statuscodeId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                End If
                db.AddParameter("@cutOffDate", cutOffDate & refTime, SqlDbType.DateTime, , ParameterDirection.Input)
                rtnDataTable = db.RunParamSQLDataSet_SP("dbo.usp_GetGeneralInfoForMultipleStudentsWithNoLeadGrps").Tables(0)
            Else
                db.AddParameter("@campGrpId", campGrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                db.AddParameter("@campusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@prgVerId", prgVerId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                If statuscodeId = String.Empty Then
                    db.AddParameter("@statusCodeId", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                Else
                    statuscodeId = statuscodeId.Replace(" ", "").Replace("'", "")
                    'statusCodeId = "'" + statusCodeId + "'"
                    db.AddParameter("@statusCodeId", statuscodeId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                End If
                db.AddParameter("@cutOffDate", cutOffDate & refTime, SqlDbType.DateTime, , ParameterDirection.Input)
                leadgrpId = leadgrpId.Replace(" ", "").Replace("'", "")
                db.AddParameter("@leadGrpId", leadgrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                rtnDataTable = db.RunParamSQLDataSet_SP("dbo.usp_GetGeneralInfoForMultipleStudentsWithLeadGrps").Tables(0)

            End If
        End If
        If filterOthers.ToLower.Contains("arstuenrollments.expgraddate") Then
            Dim gradDate1 As DateTime
            Dim gradDate2 As DateTime
            Dim str As String = String.Empty

            '*****************************************************************'
            'this varies depending upon the condition

            If filterOthers.ToUpper.IndexOf("NOT BETWEEN") >= 0 Then
                '"arStuEnrollments.ExpGradDate NOT BETWEEN '2/28/2010 12:00:00 AM' AND '2/28/2010 11:59:59 PM'
                str = filterOthers.Substring(0, filterOthers.LastIndexOf("'"))
                str = str.Replace("arStuEnrollments.ExpGradDate NOT BETWEEN ", "")
                gradDate1 = str.Substring(1, str.IndexOf("AND") - 3).Trim
                gradDate2 = str.Substring(str.IndexOf("AND")).Trim.Replace("AND", "").Replace("'", "").Trim
                Dim query = From order In rtnDataTable.AsEnumerable() _
                Where order.Field(Of DateTime)("ExpGradDate") < gradDate1 Or order.Field(Of DateTime)("ExpGradDate") > gradDate2 Select order Order By order.Field(Of String)(strOrder(0)), order.Field(Of String)(strOrder(1)), order.Field(Of String)(strOrder(2))
                If query.Count > 0 Then
                    rtnDataTable = query.CopyToDataTable()
                Else
                    rtnDataTable = New DataTable()
                End If
            ElseIf filterOthers.ToUpper.IndexOf("BETWEEN") >= 0 Then
                str = filterOthers.Substring(0, filterOthers.LastIndexOf("'"))
                str = str.Replace("arStuEnrollments.ExpGradDate BETWEEN ", "")
                gradDate1 = str.Substring(1, str.IndexOf("AND") - 3).Trim
                gradDate2 = str.Substring(str.IndexOf("AND")).Trim.Replace("AND", "").Replace("'", "").Trim
                Dim query = From order In rtnDataTable.AsEnumerable() _
                Where order.Field(Of DateTime)("ExpGradDate") >= gradDate1 And order.Field(Of DateTime)("ExpGradDate") <= gradDate2 Select order Order By order.Field(Of String)(strOrder(0)), order.Field(Of String)(strOrder(1)), order.Field(Of String)(strOrder(2))
                If query.Count > 0 Then
                    rtnDataTable = query.CopyToDataTable()
                Else
                    rtnDataTable = New DataTable()
                End If

            ElseIf filterOthers.IndexOf(">") >= 0 Then
                ''arStuEnrollments.ExpGradDate > '2/28/2010 11:59:59 PM'
                str = filterOthers.Substring(0, filterOthers.LastIndexOf("'"))
                str = str.Replace("arStuEnrollments.ExpGradDate > ", "").Replace("'", "").Trim
                gradDate1 = str
                Dim query = From order In rtnDataTable.AsEnumerable() _
               Where order.Field(Of DateTime)("ExpGradDate") > gradDate1 Select order Order By order.Field(Of String)(strOrder(0)), order.Field(Of String)(strOrder(1)), order.Field(Of String)(strOrder(2))
                If query.Count > 0 Then
                    rtnDataTable = query.CopyToDataTable()
                Else
                    rtnDataTable = New DataTable()
                End If
            ElseIf filterOthers.IndexOf("<") >= 0 Then
                str = filterOthers.Substring(0, filterOthers.LastIndexOf("'"))
                str = str.Replace("arStuEnrollments.ExpGradDate < ", "").Replace("'", "").Trim
                gradDate1 = str
                Dim query = From order In rtnDataTable.AsEnumerable() _
               Where order.Field(Of DateTime)("ExpGradDate") < gradDate1 Select order Order By order.Field(Of String)(strOrder(0)), order.Field(Of String)(strOrder(1)), order.Field(Of String)(strOrder(2))
                If query.Count > 0 Then
                    rtnDataTable = query.CopyToDataTable()
                Else
                    rtnDataTable = New DataTable()
                End If
            End If
        Else
            Dim query = From order In rtnDataTable.AsEnumerable() _
        Select order Order By order.Field(Of String)(strOrder(0)), order.Field(Of String)(strOrder(1)), order.Field(Of String)(strOrder(2))

            If query.Count > 0 Then
                rtnDataTable = query.CopyToDataTable()
            Else
                rtnDataTable = New DataTable()
            End If
        End If
        Try
            Return rtnDataTable
        Catch ex As Exception
        Finally
            db.CloseConnection()
        End Try


    End Function


    Public Function GetModulesCoursesResults(Optional ByVal paramInfo As ReportParamInfo = Nothing, Optional ByVal StuEnrollId As String = "") As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strStuEnrollId As String
        Dim strCutOffDate As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If StuEnrollId = "" Then
            strStuEnrollId = GetStuEnrollId(paramInfo)
            'strCutOffDate = GetCutOffDate(paramInfo)
            strCutOffDate = paramInfo.CutoffDate
        Else
            strStuEnrollId = StuEnrollId
        End If



        With sb
            .Append("select * from ")
            .Append("(select tg.StuEnrollId,tg.TermId,tm.TermDescrip,")
            If MyAdvAppSettings.AppSettings("GradeRounding").ToLower = "yes" Then
                .Append("round(tg.Score,0) as score,")
            Else
                .Append("tg.Score as score,")
            End If
            .Append("       tm.StartDate,tm.EndDate,rq.Code,rq.Descrip,tg.ReqId,rq.Credits,rq.FinAidCredits,'" & Guid.Empty.ToString & "' as ClsSectionId, ")
            .Append(" (select Grade from arGradesystemdetails where GrdSysdetailid=tg.GrdSysdetailid) as Grade, ")
            .Append(" (select IsPass from arGradesystemdetails where GrdSysdetailid=tg.GrdSysdetailid) as IsPass, ")
            .Append(" (select IsCreditsAttempted from arGradesystemdetails where GrdSysdetailid=tg.GrdSysdetailid) as IsCreditsAttempted,")
            .Append(" (select IsCreditsEarned from arGradesystemdetails where GrdSysdetailid=tg.GrdSysdetailid) as IsCreditsEarned, ")
            .Append(" rq.IsAttendanceOnly ")
            .Append(" from arTransferGrades tg, arTerm tm, arReqs rq ")
            .Append("where StuEnrollId=? ")
            .Append("and tg.TermId=tm.TermId ")
            .Append("and tg.ReqId=rq.ReqId ")
            'If Not paramInfo Is Nothing Then
            '    .Append("and tm.StartDate <= ? ")
            'End If
            ''Modified by Saraswathi lakshmanan on June 9 2010
            ''If not paraminfo nothing added, to fix clock hour issue BSA Transfer Hour 11355
            If Not paramInfo Is Nothing Then
                If paramInfo.OrderBy = "" Then
                    .Append("and tm.StartDate <= ? ")
                Else
                    .Append(paramInfo.OrderBy.ToLower.Replace("arterm", "tm"))
                End If
            End If


            .Append("UNION ")

            .Append("select ar.StuEnrollId,cs.TermId,tm2.TermDescrip,")
            If MyAdvAppSettings.AppSettings("GradeRounding").ToLower = "yes" Then
                .Append("round(ar.Score,0) as score,")
            Else
                .Append("ar.Score as score,")
            End If
            .Append("       tm2.StartDate,tm2.EndDate,rq2.Code,rq2.Descrip,cs.ReqId,rq2.Credits,rq2.FinAidCredits ,cs.ClsSectionid, ")
            .Append(" (select Grade from arGradesystemdetails where GrdSysdetailid=ar.GrdSysdetailid) as Grade, ")
            .Append(" (select IsPass from arGradesystemdetails where GrdSysdetailid=ar.GrdSysdetailid) as IsPass, ")
            .Append(" (select IsCreditsAttempted from arGradesystemdetails where GrdSysdetailid=ar.GrdSysdetailid) as IsCreditsAttempted,")
            .Append(" (select IsCreditsEarned from arGradesystemdetails where GrdSysdetailid=ar.GrdSysdetailid) as IsCreditsEarned, ")
            .Append(" rq2.IsAttendanceOnly ")
            .Append(" from arResults ar, arClassSections cs, arTerm tm2, arReqs rq2 ")
            .Append(" where StuEnrollId=? ")
            .Append("and ar.TestId=cs.ClsSectionId ")
            .Append("and cs.TermId=tm2.TermId ")
            .Append("and cs.ReqId=rq2.ReqId ")
            'If Not paramInfo Is Nothing Then
            '    .Append("and tm2.StartDate <= ? ")
            'End If
            If Not paramInfo Is Nothing Then
                If paramInfo.OrderBy = "" Then
                    .Append("and tm2.StartDate <= ? ")
                Else
                    .Append(paramInfo.OrderBy.ToLower.Replace("arterm", "tm2"))
                End If
            End If

            .Append(") P")
            .Append(" order by StartDate,Grade desc,Score desc ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'If Not paramInfo Is Nothing Then
        '    db.AddParameter("sd", strCutOffDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'End If
        If Not paramInfo Is Nothing Then
            If paramInfo.OrderBy = "" Then
                db.AddParameter("sd", strCutOffDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
        End If
        db.AddParameter("sid2", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'If Not paramInfo Is Nothing Then
        '    db.AddParameter("sd2", strCutOffDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'End If
        If Not paramInfo Is Nothing Then
            If paramInfo.OrderBy = "" Then
                db.AddParameter("sd", strCutOffDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
        End If

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function

    Public Function GetModulesCoursesResultsForMultipleStudents(ByVal paramInfo As ReportParamInfo) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strStuEnrollId As String
        Dim strWhere As String
        Dim strTempWhere As String = ""
        Dim strTerm As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Get ProgVerId and Where Clause from paramInfo.FilterList
        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If
        strTempWhere = strWhere
        strTempWhere = strTempWhere.ToLower.Replace("and", ";")
        strWhere = ""
        Dim strArr As String() = strTempWhere.Remove(strTempWhere.IndexOf(";"), 1).Split(";")

        For i As Integer = 0 To strArr.Length - 1
            If strArr(i).ToLower.IndexOf("arterm") > 0 Then
                strTerm &= " AND " & strArr(i)
            Else
                strWhere &= " AND " & strArr(i)
            End If
        Next


        If paramInfo.FilterOther <> "" Then
            If InStr(paramInfo.FilterOther, "PM") > 0 Then
                strWhere &= " AND " & paramInfo.FilterOther.Substring(0, InStr(paramInfo.FilterOther, "PM") + 2)
            End If
        End If

        With sb
            .Append("select * from ")
            .Append("(select tg.StuEnrollId,tg.TermId,tm.TermDescrip,")
            If MyAdvAppSettings.AppSettings("GradeRounding").ToLower = "yes" Then
                .Append("round(tg.Score,0) as score,")
            Else
                .Append("tg.Score as score,")
            End If
            .Append("       tm.StartDate,tm.EndDate,rq.Code,rq.Descrip,tg.ReqId,rq.Credits,rq.FinAidCredits ,'" & Guid.Empty.ToString & "' as ClsSectionId, ")
            .Append(" (select Grade from arGradesystemdetails where GrdSysdetailid=tg.GrdSysdetailid) as Grade, ")
            .Append(" (select IsPass from arGradesystemdetails where GrdSysdetailid=tg.GrdSysdetailid) as IsPass, ")
            .Append(" (select IsCreditsAttempted from arGradesystemdetails where GrdSysdetailid=tg.GrdSysdetailid) as IsCreditsAttempted,")
            .Append(" (select IsCreditsEarned from arGradesystemdetails where GrdSysdetailid=tg.GrdSysdetailid) as IsCreditsEarned ")
            .Append(" from arTransferGrades tg, arTerm tm, arReqs rq ")
            .Append("where tg.TermId=tm.TermId ")
            .Append("and tg.ReqId=rq.ReqId ")

            If strTerm <> "" Then
                .Append(strTerm.ToLower.Replace("arterm", "tm"))
            Else
                .Append("and tm.StartDate <= ?  ")
            End If

            .Append(" UNION ")

            .Append("select ar.StuEnrollId,cs.TermId,tm2.TermDescrip,")
            If MyAdvAppSettings.AppSettings("GradeRounding").ToLower = "yes" Then
                .Append("round(ar.Score,0) as score,")
            Else
                .Append("ar.Score as score,")
            End If
            .Append("       tm2.StartDate,tm2.EndDate,rq2.Code,rq2.Descrip,cs.ReqId,rq2.Credits,rq2.FinAidCredits  ,cs.ClsSectionid, ")
            .Append(" (select Grade from arGradesystemdetails where GrdSysdetailid=ar.GrdSysdetailid) as Grade, ")
            .Append(" (select IsPass from arGradesystemdetails where GrdSysdetailid=ar.GrdSysdetailid) as IsPass, ")
            .Append(" (select IsCreditsAttempted from arGradesystemdetails where GrdSysdetailid=ar.GrdSysdetailid) as IsCreditsAttempted,")
            .Append(" (select IsCreditsEarned from arGradesystemdetails where GrdSysdetailid=ar.GrdSysdetailid) as IsCreditsEarned ")
            .Append(" from arResults ar, arClassSections cs, arTerm tm2, arReqs rq2 ")
            .Append("where ar.TestId=cs.ClsSectionId ")
            .Append("and cs.TermId=tm2.TermId ")
            .Append("and cs.ReqId=rq2.ReqId ")
            If strTerm <> "" Then
                .Append(strTerm.ToLower.Replace("arterm", "tm2"))
            Else
                .Append("and tm2.StartDate <= ? ")
            End If

            .Append(") P ")
            .Append("WHERE EXISTS (SELECT DISTINCT arStuEnrollments.StuEnrollId ")
            .Append("              FROM arStuEnrollments, arStudent A, syCampuses C, syCmpGrpCmps,syCampGrps  ")

            If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                .Append(",adLeadByLeadGroups, adLeadGroups ")
            End If

            .Append("              WHERE arStuEnrollments.StudentId=A.StudentId ")
            .Append("              AND arStuEnrollments.CampusId=C.CampusId ")
            .Append("              AND arStuEnrollments.StuEnrollId=P.StuEnrollId ")
            .Append("              AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId ")
            .Append("              AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")

            If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                .Append("AND adLeadByLeadGroups.LeadGrpId=adLeadGroups.LeadGrpId ")
                .Append("AND adLeadByLeadGroups.StuEnrollId=arStuEnrollments.StuEnrollId ")
            End If

            .Append(strWhere)
            .Append(") ")
            .Append(" order by StartDate,Grade desc,Score desc ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        If strTerm = "" Then
            db.AddParameter("cd", paramInfo.CutoffDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("cd2", paramInfo.CutoffDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function
    Public Function GetModulesCoursesResultsForMultipleStudents_SP(ByVal paramInfo As ReportParamInfo) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim strWhere As String
        Dim strTempWhere As String = ""
        Dim strTerm As String = ""
        Dim iRows As Integer
        Dim m_SchoolLogo As Byte()
        Dim rptDB As New ReportsDB
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

        'Get ProgVerId and Where Clause from paramInfo.FilterList
        If paramInfo.FilterList <> "" Then
            strWhere &= " and " & paramInfo.FilterList
        End If


        '********************************************************************************'
        Dim campGrpId As String = String.Empty
        Dim prgVerId As String = String.Empty
        Dim statusCodeId As String = String.Empty
        Dim leadGrpId As String = String.Empty
        Dim strStuEnrollid As String = String.Empty
        '********************************************************************************'


        'Get StudentId and rest of Where Clause from paramInfo.FilterOther


        strTempWhere = strWhere
        strTempWhere = strTempWhere.ToLower.Replace("and", ";")
        strWhere = ""
        Dim strArr As String() = strTempWhere.Remove(strTempWhere.IndexOf(";"), 1).Split(";")

        For i As Integer = 0 To strArr.Length - 1
            If strArr(i).ToLower.IndexOf("arterm") > 0 Then
                strTerm &= " and " & strArr(i)
            Else
                strWhere &= " and " & strArr(i)
            End If
        Next
        If paramInfo.FilterOther <> "" Then
            If InStr(paramInfo.FilterOther, "PM") > 0 Then
                strWhere &= " and " & paramInfo.FilterOther.Substring(0, InStr(paramInfo.FilterOther, "PM") + 2)
            End If
        End If
        If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
            leadGrpId = True
        End If

        If strTerm <> "" Then strTerm = strTerm.Substring(strTerm.IndexOf("'")).Replace("'", "")


        '********************************************************************************'
        'get the values of the parameters
        If strWhere.ToLower.Contains("arstuenrollments.stuenrollid") Then
            strStuEnrollid = strWhere.ToLower.Replace("and   arstuenrollments.stuenrollid =", "").Replace("'", "").Trim()
            db.AddParameter("@stuEnrollId", New Guid(strStuEnrollid), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@termDate", paramInfo.CutoffDate, SqlDbType.DateTime, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.usp_GetModulesCoursesResults")
        Else



            campGrpId = strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in (")).ToLower.IndexOf(" and ")).Replace("t1.campgrpid in", "").Replace(")", "").Replace("(", "")

            'get the prgverid
            If strWhere.ToLower.Contains("arstuenrollments.prgverid in ") Then
                If strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid in ")).ToLower.IndexOf(" and ") >= 0 Then
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid in (")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.prgverid in", "").Replace(")", "").Replace("(", "")
                Else
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid in (")).Replace("arstuenrollments.prgverid in", "").Replace(")", "").Replace("(", "")
                End If
            Else
                If strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid = ")).ToLower.IndexOf(" and ") >= 0 Then
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid = "), strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid = ")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.prgverid =", "").Replace(")", "").Replace("(", "")
                Else
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid = ")).Replace("arstuenrollments.prgverid =", "").Replace(")", "").Replace("(", "")
                End If
            End If
            'end the prgverid

            'get the statuscode
            If strWhere.ToLower.Contains("arstuenrollments.statuscodeid") Then
                If strWhere.Contains("arstuenrollments.statuscodeid in ") Then
                    If strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid in ")).ToLower.IndexOf(" and ") >= 0 Then
                        statusCodeId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid in (")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.statuscodeid in", "").Replace(")", "").Replace("(", "")
                    Else
                        statusCodeId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid in (")).Replace("arstuenrollments.statuscodeid in", "").Replace(")", "").Replace("(", "")
                    End If
                Else
                    If strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).ToLower.IndexOf(" and ") >= 0 Then
                        statusCodeId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid = "), strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.statuscodeid =", "").Replace(")", "").Replace("(", "")
                    Else
                        statusCodeId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).Replace("arstuenrollments.statuscodeid =", "").Replace(")", "").Replace("(", "")
                    End If
                End If
            End If
            'end the statuscode

            'get the leadGrpid
            If strWhere.ToLower.Contains("adleadgroups.leadgrpid") Then
                If strWhere.Contains("adleadgroups.leadgrpid in ") Then
                    If strWhere.Substring(strWhere.ToLower.IndexOf("adleadgroups.leadgrpid in ")).ToLower.IndexOf(" and ") >= 0 Then
                        leadGrpId = strWhere.Substring(strWhere.ToLower.IndexOf("adleadgroups.leadgrpid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("adleadgroups.leadgrpid in (")).ToLower.IndexOf(" and ")).Replace("adleadgroups.leadgrpid in", "").Replace(")", "").Replace("(", "")
                    Else
                        leadGrpId = strWhere.Substring(strWhere.ToLower.IndexOf("adleadgroups.leadgrpid in (")).Replace("adleadgroups.leadgrpid in", "").Replace(")", "").Replace("(", "")
                    End If
                Else
                    If strWhere.Substring(strWhere.ToLower.IndexOf("adleadgroups.leadgrpid = ")).ToLower.IndexOf(" and ") >= 0 Then
                        leadGrpId = strWhere.Substring(strWhere.ToLower.IndexOf("adleadgroups.leadgrpid = "), strWhere.Substring(strWhere.ToLower.IndexOf("adleadgroups.leadgrpid = ")).ToLower.IndexOf(" and ")).Replace("adleadgroups.leadgrpid =", "").Replace(")", "").Replace("(", "")
                    Else
                        leadGrpId = strWhere.Substring(strWhere.ToLower.IndexOf("adleadgroups.leadgrpid = ")).Replace("adleadgroups.leadgrpid =", "").Replace(")", "").Replace("(", "")
                    End If
                End If
            End If

            'end the leadgrpid

            '********************************************************************************'
            campGrpId = campGrpId.Replace(" ", "").Replace("'", "")
            'campGrpId = "'" + campGrpId + "'"
            prgVerId = prgVerId.Replace(" ", "").Replace("'", "")
            'prgVerId = "'" + prgVerId + "'"


            If paramInfo.GroupByClass = True Then
                If leadGrpId = String.Empty Then
                    db.AddParameter("@campGrpId", campGrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    db.AddParameter("@prgVerId", prgVerId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    If statusCodeId = String.Empty Then
                        db.AddParameter("@statusCodeId", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    Else
                        statusCodeId = statusCodeId.Replace(" ", "").Replace("'", "")
                        'statusCodeId = "'" + statusCodeId + "'"
                        db.AddParameter("@statusCodeId", statusCodeId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    End If
                    If strTerm <> "" Then
                        db.AddParameter("@termId", New Guid(strTerm), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@termId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                    End If

                    ds = db.RunParamSQLDataSet_SP("dbo.usp_GetModulesCoursesResultsForMultipleStudents_GroupByClass")
                Else
                    db.AddParameter("@campGrpId", campGrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    db.AddParameter("@prgVerId", prgVerId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    If statusCodeId = String.Empty Then
                        db.AddParameter("@statusCodeId", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    Else
                        statusCodeId = statusCodeId.Replace(" ", "").Replace("'", "")
                        'statusCodeId = "'" + statusCodeId + "'"
                        db.AddParameter("@statusCodeId", statusCodeId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    End If
                    If strTerm <> "" Then
                        db.AddParameter("@termId", New Guid(strTerm), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@termId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                    End If
                    leadGrpId = leadGrpId.Replace(" ", "").Replace("'", "")
                    db.AddParameter("@leadGrpId", leadGrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    ds = db.RunParamSQLDataSet_SP("dbo.usp_GetModulesCoursesResultsForMultipleStudentsWithLeadGrps_GroupByClass")

                End If
            Else
                If leadGrpId = String.Empty Then
                    db.AddParameter("@campGrpId", campGrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    db.AddParameter("@prgVerId", prgVerId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    If statusCodeId = String.Empty Then
                        db.AddParameter("@statusCodeId", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    Else
                        statusCodeId = statusCodeId.Replace(" ", "").Replace("'", "")
                        'statusCodeId = "'" + statusCodeId + "'"
                        db.AddParameter("@statusCodeId", statusCodeId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    End If
                    If strTerm <> "" Then
                        db.AddParameter("@termId", New Guid(strTerm), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                        db.AddParameter("@termDate", DBNull.Value, SqlDbType.DateTime, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@termId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                        db.AddParameter("@termDate", paramInfo.CutoffDate, SqlDbType.DateTime, , ParameterDirection.Input)
                    End If

                    ds = db.RunParamSQLDataSet_SP("dbo.usp_GetModulesCoursesResultsForMultipleStudents")
                Else
                    db.AddParameter("@campGrpId", campGrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    db.AddParameter("@prgVerId", prgVerId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    If statusCodeId = String.Empty Then
                        db.AddParameter("@statusCodeId", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    Else
                        statusCodeId = statusCodeId.Replace(" ", "").Replace("'", "")
                        'statusCodeId = "'" + statusCodeId + "'"
                        db.AddParameter("@statusCodeId", statusCodeId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    End If
                    If strTerm <> "" Then
                        db.AddParameter("@termId", New Guid(strTerm), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                        db.AddParameter("@termDate", DBNull.Value, SqlDbType.DateTime, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@termId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                        db.AddParameter("@termDate", paramInfo.CutoffDate, SqlDbType.DateTime, , ParameterDirection.Input)
                    End If
                    leadGrpId = leadGrpId.Replace(" ", "").Replace("'", "")
                    db.AddParameter("@leadGrpId", leadGrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
                    ds = db.RunParamSQLDataSet_SP("dbo.usp_GetModulesCoursesResultsForMultipleStudentsWithLeadGrps")

                End If
            End If
        End If








        Dim SummaryInfo As DataTable = ds.Tables(0)

        If paramInfo.FilterOther.ToLower.Contains("arstuenrollments.expgraddate") Then
            Dim gradDate1 As DateTime
            Dim gradDate2 As DateTime
            Dim str As String = String.Empty


            '*****************************************************************'
            'this varies depending upon the condition

            If paramInfo.FilterOther.ToUpper.IndexOf("NOT BETWEEN") >= 0 Then
                '"arStuEnrollments.ExpGradDate NOT BETWEEN '2/28/2010 12:00:00 AM' AND '2/28/2010 11:59:59 PM'
                str = paramInfo.FilterOther.Substring(0, paramInfo.FilterOther.LastIndexOf("'"))
                str = str.Replace("arStuEnrollments.ExpGradDate NOT BETWEEN ", "")
                gradDate1 = str.Substring(1, str.IndexOf("AND") - 3).Trim
                gradDate2 = str.Substring(str.IndexOf("AND")).Trim.Replace("AND", "").Replace("'", "").Trim
                Dim query = From order In SummaryInfo.AsEnumerable() _
                Where order.Field(Of DateTime)("ExpGradDate") < gradDate1 Or order.Field(Of DateTime)("ExpGradDate") > gradDate2
                If query.Count > 0 Then
                    SummaryInfo = query.CopyToDataTable()
                Else
                    SummaryInfo = New DataTable()
                End If
            ElseIf paramInfo.FilterOther.ToUpper.IndexOf("BETWEEN") >= 0 Then
                str = paramInfo.FilterOther.Substring(0, paramInfo.FilterOther.LastIndexOf("'"))
                str = str.Replace("arStuEnrollments.ExpGradDate BETWEEN ", "")
                gradDate1 = str.Substring(1, str.IndexOf("AND") - 3).Trim
                gradDate2 = str.Substring(str.IndexOf("AND")).Trim.Replace("AND", "").Replace("'", "").Trim
                Dim query = From order In SummaryInfo.AsEnumerable() _
                Where order.Field(Of DateTime)("ExpGradDate") >= gradDate1 And order.Field(Of DateTime)("ExpGradDate") <= gradDate2
                If query.Count > 0 Then
                    SummaryInfo = query.CopyToDataTable()
                Else
                    SummaryInfo = New DataTable()
                End If

            ElseIf paramInfo.FilterOther.IndexOf(">") >= 0 Then
                ''arStuEnrollments.ExpGradDate > '2/28/2010 11:59:59 PM'
                str = paramInfo.FilterOther.Substring(0, paramInfo.FilterOther.LastIndexOf("'"))
                str = str.Replace("arStuEnrollments.ExpGradDate > ", "").Replace("'", "").Trim
                gradDate1 = str
                Dim query = From order In SummaryInfo.AsEnumerable() _
               Where order.Field(Of DateTime)("ExpGradDate") > gradDate1
                If query.Count > 0 Then
                    SummaryInfo = query.CopyToDataTable()
                Else
                    SummaryInfo = New DataTable()
                End If
            ElseIf paramInfo.FilterOther.IndexOf("<") >= 0 Then
                str = paramInfo.FilterOther.Substring(0, paramInfo.FilterOther.LastIndexOf("'"))
                str = str.Replace("arStuEnrollments.ExpGradDate < ", "").Replace("'", "").Trim
                gradDate1 = str
                Dim query = From order In SummaryInfo.AsEnumerable() _
               Where order.Field(Of DateTime)("ExpGradDate") < gradDate1
                If query.Count > 0 Then
                    SummaryInfo = query.CopyToDataTable()
                Else
                    SummaryInfo = New DataTable()
                End If
            End If
        End If
        '********************************************************************'
        Try
            Return SummaryInfo
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try





    End Function

    Public Function GetWorkUnitResults(Optional ByVal paramInfo As ReportParamInfo = Nothing, Optional ByVal StuEnrollId As String = "") As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strStuEnrollId As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If StuEnrollId = "" Then
            strStuEnrollId = GetStuEnrollId(paramInfo)
        Else
            strStuEnrollId = StuEnrollId
        End If


        With sb
            .Append("SELECT ")
            .Append("   ConversionResultid,GBCR.ReqId,GBCR.TermId,GCT.Descrip + (case when GBCR.ResNum =0 then '' else cast(GBCR.ResNum as char) end) as Descrip ,GBCR.Score,GBCR.MinResult, ")
            .Append("   GBCR.Required,GBCR.MustPass, ")
            .Append(" (case when GCT.SysComponentTypeId in(500,503,544) then ")
            .Append(" (CASE      WHEN ISNULL(Score,0) > MinResult THEN NULL      WHEN ISNULL(Score,0) = MinResult ")
            .Append(" THEN 0      WHEN MinResult > ISNULL(Score,0) THEN (MinResult - ISNULL(Score,0))     END) ")
            .Append(" else null ")
            .Append("    END) AS Remaining,GCT.SysComponentTypeId as WorkUnitType,(select Resource from syResources where Resourceid=GCT.SysComponentTypeId) as WorkUnitDescrip,null as InstrGrdBkWgtDetailId ")
            .Append("FROM arGrdBkConversionResults GBCR, arGrdComponentTypes GCT ")
            .Append("WHERE GBCR.GrdComponentTypeId=GCT.GrdComponentTypeId ")
            .Append("AND GBCR.StuEnrollId=? ")
            .Append("UNION ")
            .Append("SELECT ")
            .Append("   grdBkResultid,ReqId,TermId,Descrip,Score,MinResult,Required,MustPass, ")
            .Append(" (case when WorkUnitType in(500,503,544) then ")
            .Append(" (CASE      WHEN ISNULL(Score,0) > MinResult THEN NULL      WHEN ISNULL(Score,0) = MinResult ")
            .Append(" THEN 0      WHEN MinResult > ISNULL(Score,0) THEN (MinResult - ISNULL(Score,0))     END) ")
            .Append(" else null ")
            .Append("    END) AS Remaining,WorkUnitType,WorkUnitDescrip,InstrGrdBkWgtDetailId ")
            .Append("   FROM ")
            .Append("   (SELECT ")
            .Append("      CS.ReqId,CS.TermId,GCT.Descrip + (case when GBRS.ResNum =0 then '' else cast(GBRS.ResNum as char) end) as Descrip , ")
            .Append("     (CASE GCT.SysComponentTypeId ")
            .Append("       WHEN 544 THEN (SELECT SUM(HoursAttended) FROM arExternshipAttendance WHERE StuEnrollId=?) ")
            .Append("       ELSE GBRS.Score ")
            .Append("       END ")
            .Append("     ) AS Score, ")
            .Append("    (CASE GCT.SysComponentTypeId ")
            .Append("       WHEN 500 THEN GBWD.Number ")
            .Append("       WHEN 503 THEN GBWD.Number ")
            .Append("       WHEN 544 THEN GBWD.Number ")
            .Append("       ELSE (SELECT MIN(MinVal) ")
            .Append("             FROM arGradeScaleDetails GSD, arGradeSystemDetails GSS ")
            .Append("             WHERE GSD.GrdSysDetailId=GSS.GrdSysDetailId ")
            .Append("             AND GSS.IsPass=1) ")
            .Append("       END ")
            .Append("       )	AS MinResult, ")
            .Append("       GBWD.Required, GBWD.MustPass,GCT.SysComponentTypeId as WorkUnitType,(select Resource from syResources where Resourceid=GCT.SysComponentTypeId) as WorkUnitDescrip,GBRS.grdBkResultid,GBRS.InstrGrdBkWgtDetailId     ")
            .Append("       FROM arGrdBkResults GBRS, arClassSections CS, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT ")
            .Append("       WHERE GBRS.StuEnrollId=? ")
            .Append("       AND GBRS.ClsSectionId=CS.ClsSectionId ")
            .Append("       AND GBRS.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
            .Append("       AND GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId) P ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function
    Public Function GetWorkUnitResultsForProgressDisplay(Optional ByVal paramInfo As ReportParamInfo = Nothing, Optional ByVal StuEnrollId As String = "") As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strStuEnrollId As String
        Dim dtAttempted As DataTable
        Dim dtNotAttempted As DataTable
        Dim dtNeverAttempted As DataTable
        Dim dtAll As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If StuEnrollId = "" Then
            strStuEnrollId = GetStuEnrollId(paramInfo)
        Else
            strStuEnrollId = StuEnrollId
        End If
        Dim RossType As String = MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower.ToString()

        With sb
            .Append("SELECT ")
            .Append("   ConversionResultid,GBCR.ReqId,GBCR.TermId,rtrim(GCT.Descrip) + (case when GBCR.ResNum  in (0,1) then '' else cast(GBCR.ResNum as char) end) as Descrip ,GCT.Descrip as Des,GBCR.Score,GBCR.MinResult, ")
            .Append("   GBCR.Required,GBCR.MustPass, ")
            .Append(" (case when GCT.SysComponentTypeId in(500,503,544) then ")
            .Append(" (CASE      WHEN ISNULL(Score,0) > MinResult THEN NULL      WHEN ISNULL(Score,0) = MinResult ")
            .Append(" THEN 0      WHEN MinResult > ISNULL(Score,0) THEN (MinResult - ISNULL(Score,0))     END) ")
            .Append(" else null ")
            .Append("    END) AS Remaining,GCT.SysComponentTypeId as WorkUnitType,(select Resource from syResources where Resourceid=GCT.SysComponentTypeId) as WorkUnitDescrip,null as InstrGrdBkWgtDetailId,GBCR.StuEnrollid,GBCR.ResNum,'" & RossType & "' as RossType  ")
            .Append("FROM arGrdBkConversionResults GBCR, arGrdComponentTypes GCT ")
            .Append("WHERE GBCR.GrdComponentTypeId=GCT.GrdComponentTypeId ")
            .Append("AND GBCR.StuEnrollId=? ")
            'If SingletonAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" Then
            '    .Append(" and GBCR.resnum <> 0 ")
            'End If
            .Append("UNION ")
            .Append("SELECT ")
            .Append("   grdBkResultid,ReqId,TermId,")
            If MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" Then
                .Append("Descrip,Des,")
            Else
                .Append("isnull(grdWgtDescrip,Descrip) as Descrip,Descrip as Des,")
            End If
            'Descrip,
            .Append(" Score, MinResult, Required, MustPass, ")
            .Append(" (case when WorkUnitType in(500,503,544) then ")
            .Append(" (CASE      WHEN ISNULL(Score,0) > MinResult THEN NULL      WHEN ISNULL(Score,0) = MinResult ")
            .Append(" THEN 0      WHEN MinResult > ISNULL(Score,0) THEN (MinResult - ISNULL(Score,0))     END) ")
            .Append(" else null ")
            .Append("    END) AS Remaining,WorkUnitType,WorkUnitDescrip,InstrGrdBkWgtDetailId,StuEnrollid,ResNum,'" & RossType & "' as RossType  ")
            .Append("   FROM ")
            .Append("   (SELECT ")
            .Append("      CS.ReqId,CS.TermId,")
            '.Append(" GCT.Descrip + (case when GBRS.ResNum =0 then '' else cast(GBRS.ResNum as char) end) as Descrip , ")
            .Append(" (case when (select distinct number from arGrdBkWgtDetails where InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId) = 1 then ")
            .Append(" GCT.Descrip ")
            .Append(" else ")
            .Append(" rtrim(GCT.Descrip) + (case when GBRS.ResNum =0 then '' else cast(GBRS.ResNum as char) end) ")
            .Append(" end) ")
            .Append(" as Descrip,")
            .Append("     (CASE GCT.SysComponentTypeId ")
            .Append("       WHEN 544 THEN (SELECT SUM(HoursAttended) FROM arExternshipAttendance WHERE StuEnrollId=?) ")
            .Append("       ELSE GBRS.Score ")
            .Append("       END ")
            .Append("     ) AS Score, ")
            .Append("    (CASE GCT.SysComponentTypeId ")
            .Append("       WHEN 500 THEN GBWD.Number ")
            .Append("       WHEN 503 THEN GBWD.Number ")
            .Append("       WHEN 544 THEN GBWD.Number ")
            .Append("       ELSE (SELECT MIN(MinVal) ")
            .Append("             FROM arGradeScaleDetails GSD, arGradeSystemDetails GSS,arClassSEctions CSR    ")
            .Append("             WHERE GSD.GrdSysDetailId=GSS.GrdSysDetailId ")
            .Append("             AND GSS.IsPass=1  and GSD.GrdScaleId=CSR.GrdScaleId and CSR.ClsSectionId=GBRS.ClsSectionId ) ")
            .Append("       END ")
            .Append("       )	AS MinResult, ")
            .Append("       GBWD.Required, GBWD.MustPass,GCT.SysComponentTypeId as WorkUnitType,(select Resource from syResources where Resourceid=GCT.SysComponentTypeId) as WorkUnitDescrip,GBRS.grdBkResultid,GBRS.InstrGrdBkWgtDetailId,GBRS.StuEnrollid,GCT.Descrip as Des,GBRS.ResNum,GBWD.Descrip as grdWgtDescrip        ")
            .Append("       FROM arGrdBkResults GBRS, arClassSections CS, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT ")
            .Append("       WHERE GBRS.StuEnrollId=? ")
            .Append("       AND GBRS.ClsSectionId=CS.ClsSectionId ")
            .Append("       AND GBRS.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
            'If SingletonAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" Then
            '    .Append(" and GBRS.resnum <> 0 ")
            'End If
            .Append("       AND GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId) P where WorkUnitType not in(500,503,544,533) ")

            .Append(" Union ")
            .Append(" SELECT    '00000000-0000-0000-0000-000000000000' as grdBkResultid,")
            .Append(" ReqId,TermId,")
            If MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" Then
                .Append("Descrip,Des,")
            Else
                .Append("isnull(grdWgtDescrip,Descrip)  as Descrip,Descrip as Des,")
            End If
            .Append(" sum(score) as score,isnull(MinResult,0) as MinResult,Required,MustPass,")
            .Append(" (CASE      WHEN sum(ISNULL(Score,0)) > MinResult THEN NULL      WHEN sum(ISNULL(Score,0)) = MinResult ")
            .Append(" THEN 0      WHEN MinResult > sum(ISNULL(Score,0)) THEN (MinResult - sum(ISNULL(Score,0)))     END) as remaining, ")
            .Append(" WorkUnitType, WorkUnitDescrip, InstrGrdBkWgtDetailId, StuEnrollId, ResNum ,'" & RossType & "' as RossType  ")
            .Append(" from ")
            .Append(" (SELECT       CS.ReqId,CS.TermId, ")
            '.Append(" GCT.Descrip + (case when GBRS.ResNum =0 then '' else cast(GBRS.ResNum as char) end) as Descrip ,  ")
            .Append(" (case when (select distinct number from arGrdBkWgtDetails where InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId) = 1 then ")
            .Append(" GCT.Descrip ")
            .Append(" else ")
            .Append(" rtrim(GCT.Descrip) + (case when GBRS.ResNum =0 then '' else cast(GBRS.ResNum as char) end) ")
            .Append(" end) ")
            .Append(" as Descrip,")
            .Append(" (CASE GCT.SysComponentTypeId        WHEN 544 THEN (SELECT SUM(HoursAttended) FROM arExternshipAttendance WHERE StuEnrollId= ? )  ")
            .Append(" ELSE GBRS.Score        END      ) AS Score,     (CASE GCT.SysComponentTypeId        WHEN 500 THEN GBWD.Number   ")
            .Append(" WHEN 503 THEN GBWD.Number        WHEN 544 THEN GBWD.Number        ELSE (SELECT MIN(MinVal)      ")
            .Append(" FROM arGradeScaleDetails GSD, arGradeSystemDetails GSS,arClassSEctions CSR                 WHERE GSD.GrdSysDetailId=GSS.GrdSysDetailId          ")
            .Append(" AND GSS.IsPass=1  and GSD.GrdScaleId=CSR.GrdScaleId and CSR.ClsSectionId=GBRS.ClsSectionId)        END        )	AS MinResult,        GBWD.Required, GBWD.MustPass,GCT.SysComponentTypeId as WorkUnitType, ")
            .Append(" (select Resource from syResources where Resourceid=GCT.SysComponentTypeId) as WorkUnitDescrip,GBRS.grdBkResultid, ")
            .Append(" GBRS.InstrGrdBkWgtDetailId,GBRS.StuEnrollid,GCT.Descrip as Des,GBRS.ResNum,GBWD.Descrip as grdWgtDescrip   ")
            .Append(" FROM arGrdBkResults GBRS, arClassSections CS, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT     ")
            .Append(" WHERE GBRS.StuEnrollId= ? ")
            .Append(" AND GBRS.ClsSectionId=CS.ClsSectionId        AND GBRS.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
            'If SingletonAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" Then
            '    .Append(" and GBRS.resnum <> 0 ")
            'End If
            .Append(" AND GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId) P where WorkUnitType  in(500,503,544,533) ")

            .Append(" group by ")
            .Append(" ReqId,TermId,Descrip,Des,MinResult,Required,MustPass,")
            If MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "instructorlevel" Then
                .Append("isnull(grdWgtDescrip,Descrip) ,")
            End If
            .Append("WorkUnitType, WorkUnitDescrip, InstrGrdBkWgtDetailId, StuEnrollId, ResNum order by Descrip ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)
        dtAttempted = ds.Tables(0)
        dtNotAttempted = GetWorkUnitResultsNotAttempted(dtAttempted, strStuEnrollId)
        If MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" Then
            dtNeverAttempted = GetWorkUnitResultsNeverAttemptedForCourseLevel(dtAttempted, strStuEnrollId)
        Else
            dtNeverAttempted = GetWorkUnitResultsNeverAttemptedForInstructorLevel(dtAttempted, strStuEnrollId)
        End If

        dtAll = dtAttempted.Copy()
        dtAll.Merge(dtNotAttempted)
        dtAll.Merge(dtNeverAttempted)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return OrderDataTable(dtAll)

    End Function
    Public Function GetWorkUnitResultsForProgressDisplay_SP(Optional ByVal paramInfo As ReportParamInfo = Nothing, Optional ByVal StuEnrollId As String = "") As DataTable

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
        Dim ds As DataSet
        Dim strStuEnrollId As String
        Dim dtAttempted As DataTable
        Dim dtNotAttempted As DataTable
        Dim dtNeverAttempted As DataTable
        Dim dtAll As DataTable
        If StuEnrollId = "" Then
            strStuEnrollId = GetStuEnrollId(paramInfo)
        Else
            strStuEnrollId = StuEnrollId
        End If


        If MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" Then

            db.AddParameter("@stuEnrollId", New Guid(strStuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.usp_GetWorkUnitResultsForProgressDisplayForCourseLevel")
        Else
            db.AddParameter("@stuEnrollId", New Guid(strStuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.usp_GetWorkUnitResultsForProgressDisplayForInstructorLevel")
        End If


        dtAttempted = ds.Tables(0)
        dtNotAttempted = GetWorkUnitResultsNotAttempted_SP(dtAttempted, strStuEnrollId)
        If MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" Then
            dtNeverAttempted = GetWorkUnitResultsNeverAttemptedForCourseLevel_SP(dtAttempted, strStuEnrollId)
        Else
            dtNeverAttempted = GetWorkUnitResultsNeverAttemptedForInstructorLevel_SP(dtAttempted, strStuEnrollId)
        End If

        dtAll = dtAttempted.Copy()
        dtAll.Merge(dtNotAttempted)
        dtAll.Merge(dtNeverAttempted)

        Try
            Return OrderDataTable(dtAll)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try



    End Function

    Private Function OrderDataTable(ByVal dt As DataTable) As DataTable
        Dim arrWU() As DataRow
        Dim rw As DataRow
        Dim rtnTable As New DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        rtnTable = dt.Clone
        If dt.Rows.Count > 0 Then
            If MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" Then
                arrWU = dt.Select("", "WorkUnitType,Des,ResNum")
            Else
                arrWU = dt.Select("", "WorkUnitType,Descrip")
            End If
            If arrWU.Length > 0 Then

                For i As Integer = 0 To arrWU.Length - 1
                    rw = rtnTable.NewRow
                    rw("ReqId") = arrWU(i)("ReqId")
                    rw("TermId") = arrWU(i)("TermId")
                    rw("Descrip") = arrWU(i)("Descrip")
                    rw("Score") = arrWU(i)("Score")
                    rw("MinResult") = arrWU(i)("MinResult")
                    rw("Required") = arrWU(i)("Required")
                    rw("MustPass") = arrWU(i)("MustPass")
                    rw("Remaining") = arrWU(i)("Remaining")
                    rw("WorkUnitType") = arrWU(i)("WorkUnitType")
                    rw("WorkUnitDescrip") = arrWU(i)("WorkUnitDescrip")
                    rw("InstrGrdBkWgtDetailId") = arrWU(i)("InstrGrdBkWgtDetailId")
                    rw("stuEnrollId") = arrWU(i)("stuEnrollId")
                    rw("RossType") = MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower.ToString()
                    'rtnTable.ImportRow(rw)
                    rtnTable.Rows.Add(rw)
                Next
            End If
        End If
        Return rtnTable
    End Function

    Public Function GetWorkUnitResultsNotAttempted(ByVal dtWU As DataTable, ByVal stuEnrollId As String) As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim rtnTable As New DataTable
        Dim rw As DataRow
        Dim numCnt As Integer = 0
        rtnTable = dtWU.Clone
        For i As Integer = 0 To dtWU.Rows.Count - 1
            Dim arrWU As DataRow
            arrWU = dtWU.Rows(i)
            If arrWU("InstrGrdBkWgtDetailId").ToString <> "" Then
                Dim cnt As Attempted = GetAttempted(stuEnrollId, arrWU("InstrGrdBkWgtDetailId").ToString, arrWU("Reqid").ToString, arrWU("TermId").ToString)
                Dim arr() As DataRow = rtnTable.Select("stuEnrollId='" & stuEnrollId & "' and InstrGrdBkWgtDetailId ='" & arrWU("InstrGrdBkWgtDetailId").ToString & "' and TermId ='" & arrWU("TermId").ToString & "' and Reqid= '" & arrWU("reqid").ToString & "'")
                If cnt.num > 1 And arr.Length = 0 And arrWU("WorkUnitType") <> 544 And arrWU("WorkUnitType") <> 503 And arrWU("WorkUnitType") <> 500 Then
                    '                For j As Integer = cnt.num - cnt.attempt To cnt.num
                    For j As Integer = cnt.attempt + 1 To cnt.num
                        rw = rtnTable.NewRow
                        rw("ReqId") = arrWU("ReqId")
                        rw("TermId") = arrWU("TermId")
                        If MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" Then
                            rw("Descrip") = arrWU("Des").ToString.Trim() & j
                        Else
                            rw("Descrip") = arrWU("Des").ToString.Trim().Substring(0, arrWU("Des").ToString.Trim().Length - j.ToString.Length) & j
                        End If



                        'rw("Score") = 0
                        rw("MinResult") = arrWU("MinResult")
                        rw("Required") = arrWU("Required")
                        rw("MustPass") = arrWU("MustPass")
                        If arrWU("WorkUnitType") = 500 Or arrWU("WorkUnitType") = 503 Or arrWU("WorkUnitType") = 544 Then
                            rw("Remaining") = arrWU("MinResult")
                        End If
                        rw("WorkUnitType") = arrWU("WorkUnitType")
                        rw("WorkUnitDescrip") = arrWU("WorkUnitDescrip")
                        rw("InstrGrdBkWgtDetailId") = arrWU("InstrGrdBkWgtDetailId")
                        rw("stuEnrollId") = arrWU("stuEnrollId")
                        rw("Des") = arrWU("Des")
                        rw("resNum") = arrWU("resNum")
                        rw("RossType") = arrWU("RossType")
                        'rtnTable.ImportRow(rw)
                        rtnTable.Rows.Add(rw)
                    Next
                End If
            End If
        Next
        rtnTable.DefaultView.Sort = "Score,Descrip"
        Return rtnTable
    End Function
    Public Function GetWorkUnitResultsNotAttempted_SP(ByVal dtWU As DataTable, ByVal stuEnrollId As String) As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim rtnTable As New DataTable
        Dim rw As DataRow
        Dim numCnt As Integer = 0
        rtnTable = dtWU.Clone
        For i As Integer = 0 To dtWU.Rows.Count - 1
            Dim arrWU As DataRow
            arrWU = dtWU.Rows(i)
            If arrWU("InstrGrdBkWgtDetailId").ToString <> "" Then
                Dim cnt As Attempted = GetAttempted_SP(stuEnrollId, arrWU("InstrGrdBkWgtDetailId").ToString, arrWU("Reqid").ToString, arrWU("TermId").ToString)
                Dim arr() As DataRow = rtnTable.Select("stuEnrollId='" & stuEnrollId & "' and InstrGrdBkWgtDetailId ='" & arrWU("InstrGrdBkWgtDetailId").ToString & "' and TermId ='" & arrWU("TermId").ToString & "' and Reqid= '" & arrWU("reqid").ToString & "'")
                If cnt.num > 1 And arr.Length = 0 And arrWU("WorkUnitType") <> 544 And arrWU("WorkUnitType") <> 503 And arrWU("WorkUnitType") <> 500 Then
                    '                For j As Integer = cnt.num - cnt.attempt To cnt.num
                    For j As Integer = cnt.attempt + 1 To cnt.num
                        rw = rtnTable.NewRow
                        rw("ReqId") = arrWU("ReqId")
                        rw("TermId") = arrWU("TermId")
                        If MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" Then
                            rw("Descrip") = arrWU("Des").ToString.Trim() & j
                        Else
                            rw("Descrip") = arrWU("Des").ToString.Trim().Substring(0, arrWU("Des").ToString.Trim().Length - j.ToString.Length) & j
                        End If



                        'rw("Score") = 0
                        rw("MinResult") = arrWU("MinResult")
                        rw("Required") = arrWU("Required")
                        rw("MustPass") = arrWU("MustPass")
                        If arrWU("WorkUnitType") = 500 Or arrWU("WorkUnitType") = 503 Or arrWU("WorkUnitType") = 544 Then
                            rw("Remaining") = arrWU("MinResult")
                        End If
                        rw("WorkUnitType") = arrWU("WorkUnitType")
                        rw("WorkUnitDescrip") = arrWU("WorkUnitDescrip")
                        rw("InstrGrdBkWgtDetailId") = arrWU("InstrGrdBkWgtDetailId")
                        rw("stuEnrollId") = arrWU("stuEnrollId")
                        rw("Des") = arrWU("Des")
                        rw("resNum") = arrWU("resNum")
                        rw("RossType") = MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower.ToString()
                        'rtnTable.ImportRow(rw)
                        rtnTable.Rows.Add(rw)
                    Next
                End If
            End If
        Next
        rtnTable.DefaultView.Sort = "Score,Descrip"
        Return rtnTable
    End Function
    Public Function GetWorkUnitResultsNeverAttemptedForCourseLevel(ByVal dtWU As DataTable, ByVal stuEnrollId As String) As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim dt As New DataTable
        Dim rtnTable As New DataTable
        Dim rw As DataRow
        Dim arrWU As DataRow
        rtnTable = dtWU.Clone
        dt = GetNeverAttemptedWUForCourseLevel(stuEnrollId)
        If dt.Rows.Count > 0 Then

            For i As Integer = 0 To dt.Rows.Count - 1
                arrWU = dt.Rows(i)
                If arrWU("WorkUnitType") = 500 Or arrWU("WorkUnitType") = 503 Or arrWU("WorkUnitType") = 544 Then
                    rw = rtnTable.NewRow
                    rw("ReqId") = arrWU("ReqId")
                    rw("TermId") = arrWU("TermId")
                    rw("Descrip") = arrWU("Descrip").ToString.Trim()
                    'rw("Score") = 0
                    rw("MinResult") = arrWU("MinResult")
                    rw("Required") = arrWU("Required")
                    rw("MustPass") = arrWU("MustPass")
                    If arrWU("WorkUnitType") = 500 Or arrWU("WorkUnitType") = 503 Or arrWU("WorkUnitType") = 544 Then
                        rw("Remaining") = arrWU("MinResult")
                    End If
                    rw("WorkUnitType") = arrWU("WorkUnitType")
                    rw("WorkUnitDescrip") = arrWU("WorkUnitDescrip")
                    rw("InstrGrdBkWgtDetailId") = arrWU("InstrGrdBkWgtDetailId")
                    rw("stuEnrollId") = arrWU("stuEnrollId")
                    rw("Des") = arrWU("Descrip")
                    rw("resNum") = 1
                    rw("RossType") = MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower.ToString()
                    'rtnTable.ImportRow(rw)
                    rtnTable.Rows.Add(rw)
                Else
                    For j As Integer = 0 To arrWU("number") - 1
                        rw = rtnTable.NewRow
                        rw("ReqId") = arrWU("ReqId")
                        rw("TermId") = arrWU("TermId")
                        If arrWU("number") > 1 Then
                            rw("Descrip") = arrWU("Descrip").ToString.Trim() & j + 1
                        Else
                            rw("Descrip") = arrWU("Descrip").ToString.Trim()
                        End If

                        'rw("Score") = 0
                        rw("MinResult") = arrWU("MinResult")
                        rw("Required") = arrWU("Required")
                        rw("MustPass") = arrWU("MustPass")
                        If arrWU("WorkUnitType") = 500 Or arrWU("WorkUnitType") = 503 Or arrWU("WorkUnitType") = 544 Then
                            rw("Remaining") = arrWU("MinResult")
                        End If
                        rw("WorkUnitType") = arrWU("WorkUnitType")
                        rw("WorkUnitDescrip") = arrWU("WorkUnitDescrip")
                        rw("InstrGrdBkWgtDetailId") = arrWU("InstrGrdBkWgtDetailId")
                        rw("stuEnrollId") = arrWU("stuEnrollId")
                        rw("Des") = arrWU("Descrip")
                        rw("resNum") = j + 1
                        rw("RossType") = MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower.ToString()
                        'rtnTable.ImportRow(rw)
                        rtnTable.Rows.Add(rw)
                    Next
                End If


            Next

        End If


        Return rtnTable
    End Function
    Public Function GetWorkUnitResultsNeverAttemptedForCourseLevel_SP(ByVal dtWU As DataTable, ByVal stuEnrollId As String) As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim dt As New DataTable
        Dim rtnTable As New DataTable
        Dim rw As DataRow
        Dim arrWU As DataRow
        rtnTable = dtWU.Clone
        dt = GetNeverAttemptedWUForCourseLevel_SP(stuEnrollId)
        If dt.Rows.Count > 0 Then

            For i As Integer = 0 To dt.Rows.Count - 1
                arrWU = dt.Rows(i)
                If arrWU("WorkUnitType") = 500 Or arrWU("WorkUnitType") = 503 Or arrWU("WorkUnitType") = 544 Then
                    rw = rtnTable.NewRow
                    rw("ReqId") = arrWU("ReqId")
                    rw("TermId") = arrWU("TermId")
                    rw("Descrip") = arrWU("Descrip").ToString.Trim()
                    'rw("Score") = 0
                    rw("MinResult") = arrWU("MinResult")
                    rw("Required") = arrWU("Required")
                    rw("MustPass") = arrWU("MustPass")
                    If arrWU("WorkUnitType") = 500 Or arrWU("WorkUnitType") = 503 Or arrWU("WorkUnitType") = 544 Then
                        rw("Remaining") = arrWU("MinResult")
                    End If
                    rw("WorkUnitType") = arrWU("WorkUnitType")
                    rw("WorkUnitDescrip") = arrWU("WorkUnitDescrip")
                    rw("InstrGrdBkWgtDetailId") = arrWU("InstrGrdBkWgtDetailId")
                    rw("stuEnrollId") = arrWU("stuEnrollId")
                    rw("Des") = arrWU("Descrip")
                    rw("resNum") = 1
                    rw("RossType") = MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower.ToString()
                    'rtnTable.ImportRow(rw)
                    rtnTable.Rows.Add(rw)
                Else
                    For j As Integer = 0 To arrWU("number") - 1
                        rw = rtnTable.NewRow
                        rw("ReqId") = arrWU("ReqId")
                        rw("TermId") = arrWU("TermId")
                        If arrWU("number") > 1 Then
                            rw("Descrip") = arrWU("Descrip").ToString.Trim() & j + 1
                        Else
                            rw("Descrip") = arrWU("Descrip").ToString.Trim()
                        End If

                        'rw("Score") = 0
                        rw("MinResult") = arrWU("MinResult")
                        rw("Required") = arrWU("Required")
                        rw("MustPass") = arrWU("MustPass")
                        If arrWU("WorkUnitType") = 500 Or arrWU("WorkUnitType") = 503 Or arrWU("WorkUnitType") = 544 Then
                            rw("Remaining") = arrWU("MinResult")
                        End If
                        rw("WorkUnitType") = arrWU("WorkUnitType")
                        rw("WorkUnitDescrip") = arrWU("WorkUnitDescrip")
                        rw("InstrGrdBkWgtDetailId") = arrWU("InstrGrdBkWgtDetailId")
                        rw("stuEnrollId") = arrWU("stuEnrollId")
                        rw("Des") = arrWU("Descrip")
                        rw("resNum") = j + 1
                        rw("RossType") = MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower.ToString()
                        'rtnTable.ImportRow(rw)
                        rtnTable.Rows.Add(rw)
                    Next
                End If


            Next

        End If


        Return rtnTable
    End Function
    Public Function GetWorkUnitResultsNeverAttemptedForInstructorLevel(ByVal dtWU As DataTable, ByVal stuEnrollId As String) As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim dt As New DataTable
        Dim rtnTable As New DataTable
        Dim rw As DataRow
        Dim arrWU As DataRow
        rtnTable = dtWU.Clone
        dt = GetNeverAttemptedWUForInstructorLevel(stuEnrollId)
        If dt.Rows.Count > 0 Then

            For i As Integer = 0 To dt.Rows.Count - 1

                arrWU = dt.Rows(i)
                If arrWU("WorkUnitType") = 500 Or arrWU("WorkUnitType") = 503 Or arrWU("WorkUnitType") = 544 Or arrWU("IsExternShip") = 0 Then
                    rw = rtnTable.NewRow
                    rw("ReqId") = arrWU("ReqId")
                    rw("TermId") = arrWU("TermId")
                    rw("Descrip") = arrWU("Descrip").ToString.Trim()
                    'rw("Score") = 0
                    rw("MinResult") = arrWU("MinResult")
                    rw("Required") = arrWU("Required")
                    rw("MustPass") = arrWU("MustPass")
                    If arrWU("WorkUnitType") = 500 Or arrWU("WorkUnitType") = 503 Or arrWU("WorkUnitType") = 544 Then
                        rw("Remaining") = arrWU("MinResult")
                    End If
                    rw("WorkUnitType") = arrWU("WorkUnitType")
                    rw("WorkUnitDescrip") = arrWU("WorkUnitDescrip")
                    rw("InstrGrdBkWgtDetailId") = arrWU("InstrGrdBkWgtDetailId")
                    rw("stuEnrollId") = arrWU("stuEnrollId")
                    rw("Des") = arrWU("Descrip")
                    rw("resNum") = 1
                    rw("RossType") = MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower.ToString()
                    rtnTable.Rows.Add(rw)
                Else
                    For j As Integer = 0 To arrWU("number") - 1
                        rw = rtnTable.NewRow
                        rw("ReqId") = arrWU("ReqId")
                        rw("TermId") = arrWU("TermId")
                        If arrWU("number") > 1 Then
                            rw("Descrip") = arrWU("Descrip").ToString.Trim() & j + 1
                        Else
                            rw("Descrip") = arrWU("Descrip").ToString.Trim()
                        End If

                        'rw("Score") = 0
                        rw("MinResult") = arrWU("MinResult")
                        rw("Required") = arrWU("Required")
                        rw("MustPass") = arrWU("MustPass")
                        If arrWU("WorkUnitType") = 500 Or arrWU("WorkUnitType") = 503 Or arrWU("WorkUnitType") = 544 Then
                            rw("Remaining") = arrWU("MinResult")
                        End If
                        rw("WorkUnitType") = arrWU("WorkUnitType")
                        rw("WorkUnitDescrip") = arrWU("WorkUnitDescrip")
                        rw("InstrGrdBkWgtDetailId") = arrWU("InstrGrdBkWgtDetailId")
                        rw("stuEnrollId") = arrWU("stuEnrollId")
                        rw("Des") = arrWU("Descrip")
                        rw("resNum") = j + 1
                        rw("RossType") = MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower.ToString()
                        'rtnTable.ImportRow(rw)
                        rtnTable.Rows.Add(rw)

                    Next

                End If
                'rtnTable.ImportRow(rw)


            Next

        End If


        Return rtnTable
    End Function
    Public Function GetWorkUnitResultsNeverAttemptedForInstructorLevel_SP(ByVal dtWU As DataTable, ByVal stuEnrollId As String) As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim dt As New DataTable
        Dim rtnTable As New DataTable
        Dim rw As DataRow
        Dim arrWU As DataRow
        rtnTable = dtWU.Clone
        dt = GetNeverAttemptedWUForInstructorLevel_SP(stuEnrollId)
        If dt.Rows.Count > 0 Then

            For i As Integer = 0 To dt.Rows.Count - 1

                arrWU = dt.Rows(i)
                If arrWU("WorkUnitType") = 500 Or arrWU("WorkUnitType") = 503 Or arrWU("WorkUnitType") = 544 Or arrWU("IsExternShip") = 0 Then
                    rw = rtnTable.NewRow
                    rw("ReqId") = arrWU("ReqId")
                    rw("TermId") = arrWU("TermId")
                    rw("Descrip") = arrWU("Descrip").ToString.Trim()
                    'rw("Score") = 0
                    rw("MinResult") = arrWU("MinResult")
                    rw("Required") = arrWU("Required")
                    rw("MustPass") = arrWU("MustPass")
                    If arrWU("WorkUnitType") = 500 Or arrWU("WorkUnitType") = 503 Or arrWU("WorkUnitType") = 544 Then
                        rw("Remaining") = arrWU("MinResult")
                    End If
                    rw("WorkUnitType") = arrWU("WorkUnitType")
                    rw("WorkUnitDescrip") = arrWU("WorkUnitDescrip")
                    rw("InstrGrdBkWgtDetailId") = arrWU("InstrGrdBkWgtDetailId")
                    rw("stuEnrollId") = arrWU("stuEnrollId")
                    rw("Des") = arrWU("Descrip")
                    rw("resNum") = 1
                    rw("RossType") = MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower.ToString()
                    rtnTable.Rows.Add(rw)
                Else
                    For j As Integer = 0 To arrWU("number") - 1
                        rw = rtnTable.NewRow
                        rw("ReqId") = arrWU("ReqId")
                        rw("TermId") = arrWU("TermId")
                        If arrWU("number") > 1 Then
                            rw("Descrip") = arrWU("Descrip").ToString.Trim() & j + 1
                        Else
                            rw("Descrip") = arrWU("Descrip").ToString.Trim()
                        End If

                        'rw("Score") = 0
                        rw("MinResult") = arrWU("MinResult")
                        rw("Required") = arrWU("Required")
                        rw("MustPass") = arrWU("MustPass")
                        If arrWU("WorkUnitType") = 500 Or arrWU("WorkUnitType") = 503 Or arrWU("WorkUnitType") = 544 Then
                            rw("Remaining") = arrWU("MinResult")
                        End If
                        rw("WorkUnitType") = arrWU("WorkUnitType")
                        rw("WorkUnitDescrip") = arrWU("WorkUnitDescrip")
                        rw("InstrGrdBkWgtDetailId") = arrWU("InstrGrdBkWgtDetailId")
                        rw("stuEnrollId") = arrWU("stuEnrollId")
                        rw("Des") = arrWU("Descrip")
                        rw("resNum") = j + 1
                        rw("RossType") = MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower.ToString()
                        'rtnTable.ImportRow(rw)
                        rtnTable.Rows.Add(rw)

                    Next

                End If
                'rtnTable.ImportRow(rw)


            Next

        End If


        Return rtnTable
    End Function
    Public Function GetNeverAttemptedWUForCourseLevel(ByVal stuEnrollId As String) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append(" select d.reqid,d.termId,e.Descrip, ")
            .Append("(CASE e.SysComponentTypeId    ")
            .Append(" WHEN 500 THEN a.Number  ")
            .Append(" WHEN 503 THEN a.Number  ")
            .Append(" WHEN 544 THEN a.Number ")
            .Append(" ELSE (SELECT MIN(MinVal)              FROM arGradeScaleDetails GSD, arGradeSystemDetails GSS   ")
            .Append(" WHERE GSD.GrdSysDetailId=GSS.GrdSysDetailId              AND GSS.IsPass=1)        END        )	AS MinResult, ")
            .Append(" a.required, a.mustpass, e.SysComponentTypeId as WorkUnitType, a.number,(select Resource from syResources where Resourceid=e.SysComponentTypeId) as WorkUnitDescrip, ")
            .Append("a.InstrGrdBkWgtDetailId,c.stuenrollid    ")
            .Append(" from arGrdBkWgtDetails a,arGrdBkWeights b,arResults c,arClassSections d,arGrdComponentTypes e ")
            .Append(" where a.InstrGrdBkWgtId=b.InstrGrdBkWgtId and  ")
            .Append(" b.Reqid = d.reqid And c.testid = d.clsSectionid ")
            .Append(" and e.GrdComponentTypeId=a.GrdComponentTypeId ")
            .Append(" and b.EffectiveDate<=d.StartDate ")
            .Append(" and c.StuEnrollid= ? ")
            .Append(" and a.number is not null ")
            .Append(" and a.InstrGrdBkWgtDetailId not in (select InstrGrdBkWgtDetailId from arGrdBkResults ")
            '.Append(" where Resnum <> 0 and StuEnrollId= ?   ")
            .Append(" where  StuEnrollId= ?   ")
            .Append(" and clsSectionid in (select clsSectionid from arClassSections where Reqid = d.reqid ")
            .Append(" and Termid = d.TermId )) ")
            .Append(" and b.EffectiveDate = ")
            .Append(" (SELECT Distinct Top 1 A.EffectiveDate ")
            .Append(" FROM          arGrdBkWeights A,arClassSections B  ")
            .Append(" WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=d.ClsSectionId order by A.EffectiveDate desc) ")
            .Append(" order by d.reqid,e.SysComponentTypeId,e.Descrip ")

        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()
        db.AddParameter("sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)

    End Function
    Public Function GetNeverAttemptedWUForCourseLevel_SP(ByVal stuEnrollId As String) As DataTable
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@stuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet_SP("usp_GetNeverAttemptedWUForCourseLevel")

        Try
            Return ds.Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try



    End Function
    Public Function GetNeverAttemptedWUForInstructorLevel(ByVal stuEnrollId As String) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append(" select d.reqid,d.termId,a.Descrip, ")
            .Append("(CASE e.SysComponentTypeId    ")
            .Append(" WHEN 500 THEN a.Number  ")
            .Append(" WHEN 503 THEN a.Number  ")
            .Append(" WHEN 544 THEN a.Number ")
            .Append(" ELSE (SELECT MIN(MinVal)              FROM arGradeScaleDetails GSD, arGradeSystemDetails GSS   ")
            .Append(" WHERE GSD.GrdSysDetailId=GSS.GrdSysDetailId              AND GSS.IsPass=1)        END        )	AS MinResult, ")
            .Append(" a.required, a.mustpass, e.SysComponentTypeId as WorkUnitType, a.number,(select Resource from syResources where Resourceid=e.SysComponentTypeId) as WorkUnitDescrip, ")
            .Append(" a.InstrGrdBkWgtDetailId, c.stuenrollid,0 as IsExternShip ")
            .Append(" from arGrdBkWgtDetails a,arGrdBkWeights b,arResults c,arClassSections d,arGrdComponentTypes e ")
            .Append(" where a.InstrGrdBkWgtId=b.InstrGrdBkWgtId and  ")
            .Append(" c.testid = d.clsSectionid ")
            .Append(" and b.InstrGrdBkWgtId=d.InstrGrdBkWgtId ")
            .Append(" and e.GrdComponentTypeId=a.GrdComponentTypeId ")
            .Append(" and c.StuEnrollid= ? ")
            .Append(" and a.InstrGrdBkWgtDetailId not in (select InstrGrdBkWgtDetailId from arGrdBkResults ")
            .Append(" where StuEnrollId= ? ")
            .Append(" and clsSectionid in (select clsSectionid from arClassSections where Reqid = d.reqid and Termid = d.TermId )) ")
            .Append(" Union ")
            .Append(" select d.reqid,d.termId,e.Descrip, ")
            .Append("(CASE e.SysComponentTypeId    ")
            .Append(" WHEN 500 THEN a.Number  ")
            .Append(" WHEN 503 THEN a.Number  ")
            .Append(" WHEN 544 THEN a.Number ")
            .Append(" ELSE (SELECT MIN(MinVal)              FROM arGradeScaleDetails GSD, arGradeSystemDetails GSS   ")
            .Append(" WHERE GSD.GrdSysDetailId=GSS.GrdSysDetailId              AND GSS.IsPass=1)        END        )	AS MinResult, ")
            .Append(" a.required, a.mustpass, e.SysComponentTypeId as WorkUnitType, a.number,(select Resource from syResources where Resourceid=e.SysComponentTypeId) as WorkUnitDescrip, ")
            .Append("a.InstrGrdBkWgtDetailId,c.stuenrollid, f.IsExternship   ")
            .Append(" from arGrdBkWgtDetails a,arGrdBkWeights b,arResults c,arClassSections d,arGrdComponentTypes e,arReqs f ")
            .Append(" where a.InstrGrdBkWgtId=b.InstrGrdBkWgtId and  ")
            .Append(" b.Reqid = d.reqid And c.testid = d.clsSectionid ")
            .Append(" and e.GrdComponentTypeId=a.GrdComponentTypeId ")
            .Append(" and d.Reqid=f.Reqid and f.IsExternship=1 ")
            .Append(" and b.EffectiveDate<=d.StartDate ")
            .Append(" and c.StuEnrollid= ? ")
            .Append(" and a.number is not null ")
            .Append(" and a.InstrGrdBkWgtDetailId not in (select InstrGrdBkWgtDetailId from arGrdBkResults ")
            '.Append(" where Resnum <> 0 and StuEnrollId= ?   ")
            .Append(" where  StuEnrollId= ?   ")
            .Append(" and clsSectionid in (select clsSectionid from arClassSections where Reqid = d.reqid ")
            .Append(" and Termid = d.TermId )) ")
            .Append(" and b.EffectiveDate = ")
            .Append(" (SELECT Distinct Top 1 A.EffectiveDate ")
            .Append(" FROM          arGrdBkWeights A,arClassSections B  ")
            .Append(" WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=d.ClsSectionId order by A.EffectiveDate desc) ")



        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()
        db.AddParameter("sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)

    End Function
    Public Function GetNeverAttemptedWUForInstructorLevel_SP(ByVal stuEnrollId As String) As DataTable


        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")


        db.AddParameter("@stuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        'add cutOff date parameter


        ds = db.RunParamSQLDataSet_SP("dbo.usp_GetNeverAttemptedWUForInstructorLevel")

        Try
            Return ds.Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try




    End Function


    Public Class Attempted
        Public num As Integer
        Public attempt As Integer
    End Class
    Public Function GetAttempted(ByVal stuEnrollId As String, ByVal grdId As String, ByVal Reqid As String, ByVal Termid As String) As Attempted

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim rtn As New Attempted
        With sb
            .Append(" select cast(isnull(number ,0) as int) as num,  ")
            .Append(" isnull((select count(InstrGrdBkWgtDetailId) from arGrdBkResults where StuEnrollId= ? ")
            .Append(" and InstrGrdBkWgtDetailId= ? ")
            'If SingletonAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" Then
            '    .Append(" and resnum <> 0 ")
            'End If
            '.Append(" and clsSectionid in (select clsSectionid from arClassSections where Reqid = ? and Termid = ? ) ")
            .Append(" group by InstrGrdBkWgtDetailId),0) as attempted from  ")
            .Append(" arGrdBkWgtDetails where InstrGrdBkWgtDetailId= ? ")

        End With


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()
        db.AddParameter("sid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("grdId", grdId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'db.AddParameter("reqId", Reqid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'db.AddParameter("termId", Termid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("grdId", grdId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)
        If ds.Tables(0).Rows.Count > 0 Then
            rtn.num = ds.Tables(0).Rows(0)("num")
            rtn.attempt = ds.Tables(0).Rows(0)("attempted")

        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return rtn

    End Function

    Public Function GetAttempted_SP(ByVal stuEnrollId As String, ByVal grdId As String, ByVal Reqid As String, ByVal Termid As String) As Attempted
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
        Dim rtn As New Attempted


        db.AddParameter("@stuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@instrGrdBkWgtDetailId", New Guid(grdId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)



        ds = db.RunParamSQLDataSet_SP("dbo.usp_GetAttempted")
        If ds.Tables(0).Rows.Count > 0 Then
            rtn.num = ds.Tables(0).Rows(0)("num")
            rtn.attempt = ds.Tables(0).Rows(0)("attempted")

        End If
        Try
            Return rtn
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try



    End Function

    Public Function GetWorkUnitResultsForMultipleStudents(ByVal paramInfo As ReportParamInfo) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strStuEnrollId As String
        Dim strWhere As String
        Dim strCutOffDate As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Commented out for testing purposes only
        strCutOffDate = paramInfo.CutoffDate

        'Get ProgVerId and Where Clause from paramInfo.FilterList
        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            If InStr(paramInfo.FilterOther, "PM") > 0 Then
                strWhere &= " AND " & paramInfo.FilterOther.Substring(0, InStr(paramInfo.FilterOther, "PM") + 2)
            End If
        End If

        With sb
            .Append("SELECT * FROM ( ")
            .Append("SELECT ")
            .Append("   GBCR.StuEnrollId,GBCR.ReqId,GBCR.TermId,GCT.Descrip,GBCR.Score,GBCR.MinResult, ")
            .Append("   GBCR.Required,GBCR.MustPass, ")
            .Append("   (CASE ")
            .Append("       WHEN ISNULL(Score,0) > MinResult THEN NULL ")
            .Append("       WHEN ISNULL(Score,0) = MinResult THEN 0 ")
            .Append("       WHEN MinResult > ISNULL(Score,0) THEN (MinResult - ISNULL(Score,0)) ")
            .Append("    END) AS Remaining,GCT.SysComponentTypeId as WorkUnitType,(select Resource from syResources where Resourceid=GCT.SysComponentTypeId) as WorkUnitDescrip ")
            .Append("FROM arGrdBkConversionResults GBCR, arGrdComponentTypes GCT ")
            .Append("WHERE GBCR.GrdComponentTypeId=GCT.GrdComponentTypeId ")
            '.Append("AND GBCR.StuEnrollId=? ")
            .Append("UNION ")
            .Append("SELECT ")
            .Append("   StuEnrollId,ReqId,TermId,Descrip,Score,MinResult,Required,MustPass, ")
            .Append("   (CASE ")
            .Append("     WHEN ISNULL(Score,0) > MinResult THEN NULL ")
            .Append("     WHEN ISNULL(Score,0) = MinResult THEN 0 ")
            .Append("     WHEN MinResult > ISNULL(Score,0) THEN (MinResult - ISNULL(Score,0)) ")
            .Append("    END) AS Remaining,WorkUnitType,WorkUnitDescrip  ")
            .Append("   FROM ")
            .Append("   (SELECT ")
            .Append("     GBRS.StuEnrollId,CS.ReqId,CS.TermId,GCT.Descrip, ")
            .Append("     (CASE GCT.SysComponentTypeId ")
            .Append("       WHEN 544 THEN (SELECT SUM(HoursAttended) FROM arExternshipAttendance WHERE StuEnrollId=GBRS.StuEnrollId) ")
            .Append("       ELSE GBRS.Score ")
            .Append("       END ")
            .Append("     ) AS Score, ")
            .Append("    (CASE GCT.SysComponentTypeId ")
            .Append("       WHEN 500 THEN GBWD.Number ")
            .Append("       WHEN 503 THEN GBWD.Number ")
            .Append("       WHEN 544 THEN GBWD.Number ")
            .Append("       ELSE (SELECT MIN(MinVal) ")
            .Append("             FROM arGradeScaleDetails GSD, arGradeSystemDetails GSS ")
            .Append("             WHERE GSD.GrdSysDetailId=GSS.GrdSysDetailId ")
            .Append("             AND GSS.IsPass=1) ")
            .Append("       END ")
            .Append("       )	AS MinResult, ")
            .Append("       GBWD.Required, GBWD.MustPass,GCT.SysComponentTypeId as WorkUnitType,(select Resource from syResources where Resourceid=GCT.SysComponentTypeId) as WorkUnitDescrip    ")
            .Append("       FROM arGrdBkResults GBRS, arClassSections CS, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT ")
            .Append("       WHERE GBRS.ClsSectionId=CS.ClsSectionId ")
            .Append("       AND GBRS.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
            .Append("       AND GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId) P ")
            .Append(") R ")
            .Append("WHERE EXISTS (SELECT DISTINCT arStuEnrollments.StuEnrollId ")
            .Append("              FROM arStuEnrollments, arStudent A, syCampuses C, syCmpGrpCmps,syCampGrps  ")

            If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                .Append(",adLeadByLeadGroups, adLeadGroups ")
            End If

            .Append("              WHERE arStuEnrollments.StudentId=A.StudentId ")
            .Append("              AND arStuEnrollments.CampusId=C.CampusId ")
            .Append("              AND arStuEnrollments.StuEnrollId=R.StuEnrollId ")
            .Append("              AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId ")
            .Append("              AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")

            If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                .Append("AND adLeadByLeadGroups.LeadGrpId=adLeadGroups.LeadGrpId ")
                .Append("AND adLeadByLeadGroups.StuEnrollId=arStuEnrollments.StuEnrollId ")
            End If

            .Append(strWhere)
            .Append(") ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()


        ds = db.RunSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function
    Public Function GetMultipleStudents(ByVal paramInfo As ReportParamInfo) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strStuEnrollId As String
        Dim strWhere As String
        Dim strCutOffDate As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Commented out for testing purposes only
        strCutOffDate = paramInfo.CutoffDate

        'Get ProgVerId and Where Clause from paramInfo.FilterList
        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            If InStr(paramInfo.FilterOther, "PM") > 0 Then
                strWhere &= " AND " & paramInfo.FilterOther.Substring(0, InStr(paramInfo.FilterOther, "PM") + 2)
            End If
        End If

        With sb
            .Append("SELECT * FROM ( ")
            .Append("SELECT ")
            .Append("  distinct GBCR.StuEnrollId ")
            .Append(" FROM arGrdBkConversionResults GBCR, arGrdComponentTypes GCT ")
            .Append("WHERE GBCR.GrdComponentTypeId=GCT.GrdComponentTypeId ")
            '.Append("AND GBCR.StuEnrollId=? ")
            .Append("UNION ")
            .Append("SELECT ")
            .Append("   distinct StuEnrollId ")
            .Append("       FROM arGrdBkResults GBRS, arClassSections CS, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT ")
            .Append("       WHERE GBRS.ClsSectionId=CS.ClsSectionId ")
            .Append("       AND GBRS.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
            .Append("       AND GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId  ")
            .Append(") R ")
            .Append("WHERE EXISTS (SELECT DISTINCT arStuEnrollments.StuEnrollId ")
            .Append("              FROM arStuEnrollments, arStudent A, syCampuses C, syCmpGrpCmps,syCampGrps  ")

            If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                .Append(",adLeadByLeadGroups, adLeadGroups ")
            End If

            .Append("              WHERE arStuEnrollments.StudentId=A.StudentId ")
            .Append("              AND arStuEnrollments.CampusId=C.CampusId ")
            .Append("              AND arStuEnrollments.StuEnrollId=R.StuEnrollId ")
            .Append("              AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId ")
            .Append("              AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")

            If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                .Append("AND adLeadByLeadGroups.LeadGrpId=adLeadGroups.LeadGrpId ")
                .Append("AND adLeadByLeadGroups.StuEnrollId=arStuEnrollments.StuEnrollId ")
            End If

            .Append(strWhere)
            .Append(") ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()


        ds = db.RunSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function

    Public Function GetMinPassingScore() As Decimal
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("select min(GSD.MinVal) ")
            .Append("from arGradeScaleDetails GSD, arGradeSystemDetails GSY ")
            .Append("where GSD.GrdSysDetailId=GSY.GrdSysDetailId ")
            .Append("and GSY.IsPass=1 ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()


        Return CDec(db.RunSQLScalar(sb.ToString))


    End Function
    Public Function GetWeightsForCourse(ByVal Reqid As String) As Integer
        Dim sb As New System.Text.StringBuilder
        Dim db As New SQLDataAccess
        Dim RowCount As Integer = 0
        db.OpenConnection()
        db.AddParameter("@ReqId", New Guid(Reqid), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        RowCount = db.RunParamSQLScalar_SP("USP_GetWeightsForCourse")
        db.CloseConnection()
        Return RowCount

    End Function
    Public Function GetMinPassingScore(ByVal clsSectionId As String) As Decimal

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If clsSectionId = "00000000-0000-0000-0000-000000000000" Then
            Return GetMinPassingScore
        Else
            Dim sb As New System.Text.StringBuilder
            Dim db As New DataAccess
            With sb
                .Append("select min(GSD.MinVal) ")
                .Append("from arGradeScaleDetails GSD, arGradeSystemDetails GSY,arClassSections CSR ")
                .Append("where GSD.GrdSysDetailId=GSY.GrdSysDetailId ")
                .Append("and GSY.IsPass=1  and GSD.GrdScaleId=CSR.GrdScaleId and CSR.GrdScaleId=GSD.GrdScaleId and CSR.ClsSectionId = ? ")
            End With
            db.AddParameter("clsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            Dim d As Decimal
            d = CDec(db.RunParamSQLScalar(sb.ToString))

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return d
        End If




    End Function
    ''Added by Saraswathi Lakshmanan
    Public Function GetMinPassingScore_Sp() As Decimal
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim score As Decimal = 0

        Try
            ds = db.RunParamSQLDataSet("dbo.USP_AR_GetMinPassingScore", Nothing, "SP")
            score = ds.Tables(0).Rows(0)(0).ToString()
        Catch ex As System.Exception

        Finally
            db.Dispose()
        End Try

        Return score

    End Function


    Public Function GetStudentGroups(ByVal paramInfo As ReportParamInfo) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strStuEnrollId As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        strStuEnrollId = GetStuEnrollId(paramInfo)

        With sb
            .Append("select LG.Descrip ")
            .Append("from adLeadByLeadGroups LBG, adLeadGroups LG ")
            .Append("where LBG.LeadGrpId=LG.LeadGrpId ")
            .Append("and StuEnrollId=? ")
            .Append("order by LG.Descrip ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)

    End Function
    Public Function GetStudentGroups_SP(ByVal strStuEnrollId As String) As DataTable
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@stuEnrollId", New Guid(strStuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        'add cutOff date parameter


        ds = db.RunParamSQLDataSet_SP("dbo.usp_GetStudentGroups")

        Try
            Return ds.Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try


    End Function
    Public Function GetStudentGroups(ByVal strStuEnrollId As String) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("select LG.Descrip ")
            .Append("from adLeadByLeadGroups LBG, adLeadGroups LG ")
            .Append("where LBG.LeadGrpId=LG.LeadGrpId ")
            .Append("and StuEnrollId=? ")
            .Append("order by LG.Descrip ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        db.AddParameter("sid", strStuEnrollId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)

    End Function

    Public Function GetStudentGroupsForEnrollment(ByVal StuEnrollId As String) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("select LG.Descrip ")
            .Append("from adLeadByLeadGroups LBG, adLeadGroups LG ")
            .Append("where LBG.LeadGrpId=LG.LeadGrpId ")
            .Append("and StuEnrollId=? ")
            .Append("order by LG.Descrip ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        db.AddParameter("sid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)

    End Function


    Public Function GetStudentGroupsForMultipleStudents(ByVal paramInfo As ReportParamInfo) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strStuEnrollId As String

        With sb
            .Append("select LBG.StuEnrollId,LG.Descrip ")
            .Append("from adLeadByLeadGroups LBG, adLeadGroups LG ")
            .Append("where LBG.LeadGrpId=LG.LeadGrpId ")
            .Append("order by LG.Descrip ")

        End With


    End Function



#End Region

#Region "Private Methods"

    Public Function GetCutOffDate(ByVal rptParamInfo As ReportParamInfo) As String
        'The FilterOther property has the enrollment and term start cutoff 
        'The format is like this 'arStuEnrollments.StuEnrollId='xxxx' AND arTerm.StartDate='xxxx'
        Dim strArr() As String
        Dim strItem As String

        strArr = rptParamInfo.FilterOther.ToString.Split("AND")
        strArr = strArr(1).Split("=")
        Return Trim(Replace(strArr(1), "'", ""))
    End Function

    Private Function GetStuEnrollId(ByVal rptparaminfo As ReportParamInfo) As String
        'The FilterOther property has the enrollment and term start cutoff 
        'The format is like this 'arStuEnrollments.StuEnrollId='xxxx' AND arTerm.StartDate='xxxx'
        Dim strArr() As String
        Dim strItem As String

        strArr = rptparaminfo.FilterOther.ToString.Split("AND")
        strArr = strArr(0).Split("=")
        strItem = Trim(Replace(strArr(1), "'", ""))
        Return XmlConvert.ToGuid(strItem).ToString
    End Function
#End Region

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

    Private Function GetOrderArray(ByVal str As String) As String()
        Dim rtn(2) As String
        Dim result() As String = str.Split(",")
        If result.Length = 3 Then
            rtn(0) = result(0)
            rtn(1) = result(1)
            rtn(2) = result(2)

        ElseIf result.Length = 2 Then
            rtn(0) = result(0)
            rtn(1) = result(1)
            rtn(2) = result(1)
        Else
            rtn(0) = result(0)
            rtn(1) = result(0)
            rtn(2) = result(0)
        End If
        Return rtn
    End Function
    Public Function GetClockHourProgramVersions_SP(ByVal prgverId As String) As String
        Dim ds As DataSet
        Dim db As New SQLDataAccess
        Dim rtn As String = String.Empty
        db.AddParameter("@prgVerId", prgverId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet_SP("dbo.usp_GetClockHourProgramVersions")
        If ds.Tables(0).Rows.Count > 0 Then
            rtn = "Please unselect these clock hour programVersions"
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                rtn = rtn & vbCrLf & ds.Tables(0).Rows(i)(0)
            Next
        End If
        Return rtn
    End Function
    Public Function GetStudentAddress(ByVal stuEnrollId As String) As CorporateInfo

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim CorpInfo As New CorporateInfo
        Dim dr As SqlDataReader
        Dim db As New SQLDataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("stuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)





        'ds = db.RunParamSQLDataSet_SP("dbo.usp_GetEnrollmentListWithNoLeadGrps")

        '   Execute the query
        Try
            dr = db.RunParamSQLDataReader_SP("dbo.usp_GetStudentAddress")

            While dr.Read
                With CorpInfo
                    If Not (dr("Address1") Is System.DBNull.Value) Then .Address1 = dr("Address1")
                    If Not (dr("Address2") Is System.DBNull.Value) Then .Address2 = dr("Address2")
                    If Not (dr("City") Is System.DBNull.Value) Then .City = dr("City")
                    If Not (dr("State") Is System.DBNull.Value) Then .State = dr("State")
                    If Not (dr("OtherState") Is System.DBNull.Value) Then .State = dr("OtherState")
                    If Not (dr("Zip") Is System.DBNull.Value) Then .Zip = dr("Zip")
                    If Not (dr("ForeignZip") Is System.DBNull.Value) Then .ForeignZip = dr("ForeignZip")
                    If Not (dr("Country") Is System.DBNull.Value) Then .Country = dr("Country")
                    If Not (dr("Phone") Is System.DBNull.Value) Then .Phone = dr("Phone")
                    If Not (dr("ForeignPhone") Is System.DBNull.Value) Then .Phone = dr("ForeignPhone")
                End With
            End While


            'Close Connection
        Catch ex As System.Exception
        Finally
            dr.Close()
            db.CloseConnection()
        End Try

        '   Return CorporateInfo
        Return CorpInfo

    End Function

End Class
