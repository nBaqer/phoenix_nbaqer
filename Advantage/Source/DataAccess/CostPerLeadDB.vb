Imports FAME.Advantage.Common

Public Class CostPerLeadDB

    Public Function GetCostPerLeadAnalysis(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim sb2 As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String = ""
        'Dim strOrderBy As String
        Dim strCmpGrp As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strCmpGrp &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        'If paramInfo.OrderBy <> "" Then
        '    strOrderBy &= "ORDER BY " & paramInfo.OrderBy
        'End If

        With sb
            .Append("SELECT COUNT(*) FROM adLeads,syCmpGrpCmps,syCampGrps,syCampuses ")
            .Append("WHERE SourceAdvertisement=adSourceAdvertisement.sourceadvid ")
            .Append("AND syCmpGrpCmps.CampusId=adLeads.CampusId ")
            .Append("AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
            .Append("AND syCampuses.CampusId=adLeads.CampusId ")
            .Append(strCmpGrp)
            '.Append(strOrderBy)
        End With

        With sb2
            .Append("SELECT adSourceCatagory.SourceCatagoryDescrip,adSourceType.SourceTypeDescrip,")
            .Append("adSourceAdvertisement.sourceadvdescrip,adSourceAdvertisement.cost,")
            .Append("adSourceAdvertisement.startdate,adSourceAdvertisement.enddate,")
            .Append("adAdvInterval.AdvIntervalDescrip,syStatuses.Status,")
            .Append("(" & sb.ToString & ") AS NumberOfLeads ")
            .Append("FROM adSourceAdvertisement,adSourceType,adSourceCatagory,adAdvInterval,syStatuses ")
            .Append("WHERE adSourceAdvertisement.sourcetypeid=adSourceType.SourceTypeID ")
            .Append("AND adSourceType.SourceCatagoryID=adSourceCatagory.SourceCatagoryId ")
            .Append("AND adAdvInterval.AdvIntervalId=adSourceAdvertisement.AdvIntervalId ")
            .Append("AND adAdvInterval.StatusId=syStatuses.StatusId ")
            .Append(strWhere)
            .Append(" ORDER BY adSourceCatagory.SourceCatagoryDescrip,adSourceType.SourceTypeDescrip,adSourceAdvertisement.sourceadvdescrip")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb2.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "CostPerLeadAnalysis"
            'Add new column
            ds.Tables(0).Columns.Add(New DataColumn("CostPerLead", System.Type.GetType("System.Decimal")))
            If ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    If Not (dr("NumberOfLeads") Is System.DBNull.Value) Then
                        If dr("NumberOfLeads") <> 0 Then
                            dr("CostPerLead") = dr("cost") / dr("NumberOfLeads")
                        Else
                            dr("CostPerLead") = 0
                        End If
                    End If
                Next
            End If
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

End Class
