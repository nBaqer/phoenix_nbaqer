﻿Imports FAME.Advantage.Common
Public Class EDDataXfer
    Public m_ExceptionMessage As String


#Region " Methods"
    Public Function GetAwardTypeID(ByRef db As DataAccess, ByRef AwardTypeID As String, ByVal strFund As String, ByVal strMsgType As String) As String
        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbAwardTypeID As New System.Text.StringBuilder(1000)
        Dim blnYYYY As Boolean = False
        Dim strFN As String

        'GetAwardTypeID = False
        m_ExceptionMessage = ""
        Try
            '   connect to the database
            'If Not IsNumeric(strFund) Then
            '    Return "Unable to get award type as fund value provided is invalid"
            '    Exit Function
            'End If
            If strMsgType.ToUpper = "PELL" Then
                If strFund = "" Or strFund.ToUpper = "P" Then
                    strFN = "PELL"
                ElseIf strFund = "TT" Then
                    strFN = "TEACH"
                ElseIf strFund = "A" Then
                    strFN = "ACG"
                ElseIf strFund = "S" Or strFund = "T" Then
                    strFN = "SMART"
                End If
            Else
                If strFund = "P" Then
                    strFN = "DL - PLUS"
                ElseIf strFund = "S" Then
                    strFN = "DL - SUB"
                ElseIf strFund = "U" Then
                    strFN = "DL - UNSUB"
                Else
                    strFN = ""
                End If
            End If


            sbAwardTypeID = New StringBuilder
            With sbAwardTypeID
                .Append("SELECT FundSourceId, FundSourceCode, AwardYear, advFundSourceId, ")
                .Append("(CASE WHEN (SELECT COUNT(*) FROM saFundSources LEFT OUTER JOIN syAdvFundSources ON saFundSources.AdvFundSourceId=syAdvFundSources.AdvFundSourceId WHERE  syAdvFundSources.Descrip= ? ) > 1 THEN 'True' ELSE 'False' END) AS useAwdyr ")
                .Append("FROM  saFundSources ")
                .Append("WHERE (advFundSourceId = ")
                .Append("(SELECT TOP 1 AdvFundSourceId ")
                .Append("FROM syAdvFundSources ")
                .Append("WHERE (Descrip = ?))) ")
                .Append("ORDER BY AwardYear ASC ")
            End With
            db.ClearParameters()
            db.AddParameter("@advFundSourceId", strFN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@advFundSourceId", strFN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            dr = db.RunParamSQLDataReader(sbAwardTypeID.ToString())
            'check for exception - no data
            If Not (dr.HasRows) Then
                'EXCEPTION REPORT for this query
                Return "Unable to match fundsource records for the given fund type "
                Exit Function
            End If
            While dr.Read()
                If Not (dr("FundSourceId") Is System.DBNull.Value) Then
                    If dr("useAwdyr") = "False" Then  'only one record
                        AwardTypeID = dr("FundSourceId").ToString
                    Else
                        'If Not (dr("AwardYear") Is System.DBNull.Value) Then blnYYYY = (strAwdyr = dr("AwardYear"))
                        AwardTypeID = dr("FundSourceId").ToString
                        'If blnYYYY Then Exit While
                    End If
                End If
            End While
            Return ""
        Finally
            dr.Close()
        End Try
    End Function
    Public Function GetFundSourceDetails(ByRef db As DataAccess, ByRef FundSourceDesc As String, ByRef FundSourceId As String, ByVal strFund As String, ByVal strMsgType As String) As String
        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbAwardTypeID As New System.Text.StringBuilder(1000)
        Dim blnYYYY As Boolean = False
        Dim strFN As String

        'GetAwardTypeID = False
        m_ExceptionMessage = ""
        Try
            '   connect to the database
            'If Not IsNumeric(strFund) Then
            '    Return "Unable to get award type as fund value provided is invalid"
            '    Exit Function
            'End If
            If strMsgType.ToUpper = "PELL" Then
                If strFund = "" Or strFund.ToUpper = "P" Then
                    strFN = "PELL"
                ElseIf strFund = "TT" Then
                    strFN = "TEACH"
                ElseIf strFund = "A" Then
                    strFN = "ACG"
                ElseIf strFund = "S" Or strFund = "T" Then
                    strFN = "SMART"
                End If
            Else
                If strFund = "P" Then
                    strFN = "DL - PLUS"
                ElseIf strFund = "S" Then
                    strFN = "DL - SUB"
                ElseIf strFund = "U" Then
                    strFN = "DL - UNSUB"
                Else
                    strFN = ""
                End If
            End If

            sbAwardTypeID = New StringBuilder
            With sbAwardTypeID
                .Append("SELECT FundSourceId, FundSourceDescrip ")
                .Append("FROM  saFundSources ")
                .Append("WHERE (advFundSourceId = ")
                .Append("(SELECT TOP 1 AdvFundSourceId ")
                .Append("FROM syAdvFundSources ")
                .Append("WHERE (Descrip = ?))) ")
                .Append("ORDER BY AwardYear ASC ")
            End With
            db.ClearParameters()
            db.AddParameter("@advFundSourceId", strFN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'db.AddParameter("@advFundSourceId", strFN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            dr = db.RunParamSQLDataReader(sbAwardTypeID.ToString())
            'check for exception - no data
            If Not (dr.HasRows) Then
                'EXCEPTION REPORT for this query
                Return "Unable to match fundsource records for the given fund type "
                Exit Function
            End If
            While dr.Read()
                FundSourceDesc = dr("FundSourceDescrip").ToString
                FundSourceId = dr("FundSourceId").ToString
            End While
            Return ""
        Finally
            dr.Close()
        End Try
    End Function
    Public Function GetFundSourceID(ByRef db As DataAccess, ByRef FundSourceID As String, ByRef RefundTypeID As Integer, ByVal strFund As String) As String
        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbAwardTypeID As New System.Text.StringBuilder(1000)
        Dim intFund As Integer

        'GetFundSourceID = False
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(strFund) Then
            Return "Unable to get fund source as the provided fund type is not found"
            Exit Function
        End If
        Try
            '   connect to the database
            If Not IsNumeric(strFund) Then
                Return "Unable to get fund source as the provided fund type must be a number"
                Exit Function
            End If
            intFund = CInt(strFund)
            sbAwardTypeID = New StringBuilder
            With sbAwardTypeID
                .Append("SELECT Top 1  FundSourceId, advFundSourceId, AwardTypeId ")
                .Append("FROM saFundSources ")
                .Append("WHERE advFundSourceId = ")
                .Append("(SELECT TOP 1 AdvFundSourceId ")
                .Append("FROM syAdvFundSources ")
                .Append("WHERE AdvFundSourceId = ?) ")
            End With
            db.ClearParameters()
            db.AddParameter("@advFundSourceId", intFund, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            '   Execute the query
            dr = db.RunParamSQLDataReader(sbAwardTypeID.ToString())
            'check for exception - no data
            If Not (dr.HasRows) Then
                Return "Unable to match fund source data found for the given fund type"
                Exit Function
            End If
            While dr.Read()
                If Not (dr("FundSourceId") Is System.DBNull.Value) Then
                    FundSourceID = dr("FundSourceId").ToString
                    If Not (dr("AwardTypeId") Is System.DBNull.Value) Then
                        RefundTypeID = dr("advFundSourceId").ToString
                    End If
                    Return ""
                End If
            End While
        Finally
            dr.Close()
        End Try
        'Return GetFundSourceID
    End Function
    Public Function GetAwardScheduleID(ByRef db As DataAccess, ByRef AwardScheduleID As String, ByVal strStudentAwardID As String, Optional ByVal StudentName As String = "") As String
        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbScheduleID As New System.Text.StringBuilder(1000)

        'GetAwardScheduleID = False
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(strStudentAwardID) Then
            Return "Unable to get award schedule as student award provided for student " & StudentName & " was not found"
            Exit Function
        End If
        '   connect to the database
        sbScheduleID = New StringBuilder
        With sbScheduleID
            .Append(" SELECT Top 1 ")
            .Append(" StudentAwardId, ExpectedDate, AwardScheduleID ")
            .Append(" FROM  faStudentAwardSchedule ")
            .Append(" WHERE(StudentAwardId =  ? )")
            .Append(" ORDER BY ExpectedDate ASC ")
        End With
        Try
            db.ClearParameters()
            db.AddParameter("@StudentAwardID", strStudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            dr = db.RunParamSQLDataReader(sbScheduleID.ToString())
            'check for exception - no data
            If Not (dr.HasRows) Then
                'EXCEPTION REPORT for this query
                Return "Unable to match award schedule data for the student " & StudentName
                Exit Function
            End If
            While dr.Read()
                If Not (dr("AwardScheduleID") Is System.DBNull.Value) Then
                    AwardScheduleID = dr("AwardScheduleID").ToString
                    Return ""
                End If
            End While
        Catch ex As System.Exception
            Return "Unable to match award schedule data for the student " & StudentName
            Exit Function
        Finally
            dr.Close()
        End Try
        'Return GetAwardScheduleID
    End Function
    Public Function GetStudentAwardID(ByRef db As DataAccess, ByRef StudentAwardID As String, ByVal strStuEnrollID As String, ByVal strFAID As String, Optional ByVal StudentName As String = "") As String
        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbAwardID As New System.Text.StringBuilder(1000)

        If String.IsNullOrEmpty(strFAID) Then
            Return "Unable to get student award for student " & StudentName & " as the FAID provided is empty"
            Exit Function
        End If

        If String.IsNullOrEmpty(strStuEnrollID) Then
            Return "Unable to find student enrollment information for the student " & StudentName
            Exit Function
        End If

        sbAwardID = New StringBuilder
        With sbAwardID
            .Append(" SELECT ")
            .Append(" StudentAwardId, fa_id, StuEnrollId ")
            .Append(" FROM  faStudentAwards ")
            .Append(" WHERE(faStudentAwards.fa_id  = ? ) AND (StuEnrollId = ? ) ")
        End With

        Try
            db.ClearParameters()
            db.AddParameter("@FAID", strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@STUENROLLID", strStuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            dr = db.RunParamSQLDataReader(sbAwardID.ToString())
            'check for exception - no data
            If Not (dr.HasRows) Then
                Return ("Unable to find student awards for the student " & StudentName & " and FA Identifier")
                Exit Function
            End If
            While dr.Read()
                If Not (dr("StudentAwardID") Is System.DBNull.Value) Then
                    StudentAwardID = dr("StudentAwardID").ToString
                    Return ""
                Else
                    Return "Unable to find a matching student award information for the student" & StudentName
                    Exit Function
                End If
            End While
        Catch ex As System.Exception
            Return "Unable to find student awards for the student " & StudentName & " and FA Identifier"
            Exit Function
        Finally
            dr.Close()
        End Try
    End Function
    Public Function GetStuEnrollIDbySSN(ByRef db As DataAccess, ByRef stuEnrollID As String, ByVal strMsgSSN As String) As String
        'returns the StuEnrollmentID given a valid SSN, else returns blank string
        Dim ExceptionObject As System.Exception = Nothing
        Dim dr As OleDbDataReader = Nothing
        Dim sbEnrollID As New System.Text.StringBuilder(1000)
        Dim strStudentID As String = ""
        Dim strEnrollId As String = ""
        Dim ds As New DataSet
        GetStuEnrollIDbySSN = False
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(strMsgSSN) Then
            Return "Student enrollment information not found as ssn is empty"
            Exit Function
        End If
        Try
            sbEnrollID = New StringBuilder
            With sbEnrollID
                .Append(" SELECT Distinct ")
                .Append(" StudentID ")
                .Append(" FROM ")
                .Append(" arStudent ")
                .Append(" WHERE ")
                .Append(" SSN = ? ")
            End With
            db.ClearParameters()
            db.AddParameter("@SSN", strMsgSSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            Try
                strStudentID = CType(db.RunParamSQLScalar(sbEnrollID.ToString), Guid).ToString
                If Not strStudentID = "" And Not strStudentID = Guid.Empty.ToString Then
                    Dim sbID As New StringBuilder
                    With sbID
                        .Append("SELECT ")
                        .Append(" SE.StuEnrollId, SC.SysStatusId ")
                        .Append("FROM ")
                        .Append(" arStuEnrollments SE, syStatusCodes SC ")
                        .Append("WHERE SE.StatusCodeId=SC.StatusCodeId AND ")
                        .Append(" SE.studentId = ?")
                        .Append(" order by EnrollDate Desc ")
                    End With
                    db.ClearParameters()
                    db.AddParameter("@StudentId", strStudentID, DataAccess.OleDbDataType.OleDbString, 0, ParameterDirection.Input)
                    Try
                        ''strEnrollId = CType(db.RunParamSQLScalar(sbID.ToString), Guid).ToString
                        ds = db.RunParamSQLDataSet(sbID.ToString)

                        Dim tempDR() As DataRow

                        tempDR = ds.Tables(0).Select("SysStatusId=9")
                        If (tempDR.Length > 0) Then
                            strEnrollId = tempDR(0)("StuEnrollId").ToString
                        Else
                            strEnrollId = ds.Tables(0).Rows(0)("StuEnrollId").ToString
                        End If

                        If Not strEnrollId = "" And Not strEnrollId = Guid.Empty.ToString Then
                            Return strEnrollId
                            Exit Function
                        Else
                            Return "Student enrollment information not found for SSN:" & strMsgSSN
                            Exit Function
                        End If
                    Catch ex As System.Exception
                        Return "Student enrollment information not found for SSN:" & strMsgSSN
                        Exit Function
                    End Try
                Else
                    Return "Student not found for the given SSN:" & strMsgSSN
                    Exit Function
                End If
            Catch ex As System.Exception
                Return "Student not found for the given SSN:" & strMsgSSN
                Exit Function
            End Try
        Catch ex As System.Exception
            Return "Student not found for the given SSN:" & strMsgSSN
            Exit Function
        End Try
    End Function
    Public Function GetStuCampusIDbySSN(ByRef db As DataAccess, ByRef stuCampusID As String, ByVal strMsgSSN As String) As String
        'returns the StuEnrollmentID given a valid SSN, else returns blank string
        Dim ExceptionObject As System.Exception = Nothing
        Dim dr As OleDbDataReader = Nothing
        Dim sbEnrollID As New System.Text.StringBuilder(1000)
        Dim strStudentID As String = ""
        Dim ds As New DataSet
        Dim strCampusId As String = ""
        GetStuCampusIDbySSN = False
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(strMsgSSN) Then
            Return "Student campus information not found as SSN is empty"
            Exit Function
        End If
        Try
            sbEnrollID = New StringBuilder
            With sbEnrollID
                .Append(" SELECT Distinct ")
                .Append(" StudentID ")
                .Append(" FROM ")
                .Append(" arStudent ")
                .Append(" WHERE ")
                .Append(" SSN = ? ")
            End With
            db.ClearParameters()
            db.AddParameter("@SSN", strMsgSSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            Try
                strStudentID = CType(db.RunParamSQLScalar(sbEnrollID.ToString), Guid).ToString
                If Not strStudentID = "" And Not strStudentID = Guid.Empty.ToString Then
                    Dim sbID As New StringBuilder
                    With sbID
                        .Append("SELECT ")
                        .Append(" SE.CampusId, SC.SysStatusId ")
                        .Append("FROM ")
                        .Append(" arStuEnrollments SE, syStatusCodes SC ")
                        .Append("WHERE SE.StatusCodeId=SC.StatusCodeId AND ")
                        .Append(" SE.studentId = ?")
                        .Append(" order by EnrollDate Desc ")
                    End With
                    db.ClearParameters()
                    db.AddParameter("@StudentId", strStudentID, DataAccess.OleDbDataType.OleDbString, 0, ParameterDirection.Input)
                    Try
                        'strCampusId = CType(db.RunParamSQLScalar(sbID.ToString), Guid).ToString
                        ds = db.RunParamSQLDataSet(sbID.ToString)

                        Dim tempDR() As DataRow

                        tempDR = ds.Tables(0).Select("SysStatusId=9")
                        If (tempDR.Length > 0) Then
                            strCampusId = tempDR(0)("CampusId").ToString
                        Else
                            strCampusId = ds.Tables(0).Rows(0)("CampusId").ToString
                        End If

                        If Not strCampusId = "" And Not strCampusId = Guid.Empty.ToString Then
                            Return strCampusId
                            Exit Function
                        Else
                            Return "Student campus information not found for SSN:" & strMsgSSN
                            Exit Function
                        End If
                    Catch ex As System.Exception
                        Return "Student campus information not found for SSN:" & strMsgSSN
                        Exit Function
                    End Try
                Else
                    Return "Student not found for the given SSN:" & strMsgSSN
                    Exit Function
                End If
            Catch ex As System.Exception
                Return "Student not found for the given SSN:" & strMsgSSN
                Exit Function
            End Try
        Catch ex As System.Exception
            Return "Student not found for the given SSN:" & strMsgSSN
            Exit Function
        End Try
    End Function
    Public Function GetStudentInfo(ByVal strMsgSSN As String) As String
        Dim ExceptionObject As System.Exception = Nothing
        Dim dr As OleDbDataReader = Nothing
        Dim sb As New StringBuilder
        Dim dt As New DataTable
        Dim db As New DataAccess
        Dim strStudentName As String = ""

        With sb
            .Append(" Select Distinct FirstName,LastName from arStudent where SSN=? ")
        End With
        '   Execute the query
        Try
            db.ClearParameters()
            db.AddParameter("@SSN", strMsgSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            dr = db.RunParamSQLDataReader(sb.ToString())
            'check for exception - no data
            If Not (dr.HasRows) Then
                Return "Student not found for the given SSN: " & strMsgSSN
                Exit Function
            End If
            While dr.Read()
                strStudentName = dr("FirstName").ToString + " " + dr("LastName").ToString
                Return strStudentName
            End While
        Catch ex As System.Exception
            Return "Student not found for the given SSN: " & strMsgSSN
            Exit Function
        Finally
            dr.Close()
        End Try
    End Function
    Public Function Add_saTransaction(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef TransactionID As String, ByVal StuEnrollID As String, ByVal StuCampusID As String, ByVal msgEntry As EDMessageInfo, ByVal FundSourceDesc As String, Optional ByVal StudentName As String = "", Optional ByVal SourceToTarget As String = "") As String
        Dim sbTransaction As New System.Text.StringBuilder(1000)
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(StuEnrollID) Then
            Return "Unable to match financial aid data with student " & StudentName & " enrollment information"
            Exit Function
        End If


        'TransactionID = Guid.NewGuid.ToString

        Dim strReference As String

        Dim sbFundDescrip As New StringBuilder
        Dim strTransDescrip As String
        Dim dbAmount As Double = 0.0
        Dim sbAcademicYear As New StringBuilder

        strTransDescrip = "EdExpress"
        If Not FundSourceDesc = "" Then
            strTransDescrip &= "-" & FundSourceDesc
        End If

        strReference = "EdExpress"
        If msgEntry.strMsgType.ToUpper = "PELL" Then
            strReference &= "-" & FormatDate(msgEntry.strDisbursementDate)
        Else
            strReference &= "-" & FormatDate(msgEntry.strActualDisbursementDate)
        End If
        'If Not FundSourceDesc = "" Then
        '    strReference &= "-" & FundSourceDesc
        'End If
        Try
            If msgEntry.strMsgType.ToUpper = "PELL" Then
                dbAmount = Convert.ToDouble(msgEntry.strAcceptedDisbursementAmount)
            Else
                dbAmount = Convert.ToDouble(msgEntry.strActualDisbursementNetAmount)
            End If

        Catch ex As Exception
            dbAmount = 0.0
        End Try



        Try
            Dim TransTypeID As Integer = 2
            Dim IsPosted As Boolean = True
            Dim IsAutomatic As Boolean = False
            Dim intDuplicates As Integer = 0
            Dim sbDuplicates As New StringBuilder
            Dim sbgetTransactionId As New StringBuilder

            If Not TransactionID + "" = "" Then
                With sbTransaction
                    .Append("Update saTransactions set StuEnrollID=?,CampusId=?,TransDate=?,TransAmount=?,TransReference=?,TransDescrip=?,moduser=?,moddate=?,TransTypeID=?,IsPosted=?,IsAutomatic=?,AcademicYearId=? ")
                    .Append(" Where TransactionID=? ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@CampusId", StuCampusID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If msgEntry.strMsgType.ToUpper = "PELL" Then
                    db.AddParameter("@TransDate", FormatDate(msgEntry.strDisbursementDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Else
                    db.AddParameter("@TransDate", FormatDate(msgEntry.strActualDisbursementDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                db.AddParameter("@TransAmount", dbAmount * (-1.0), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@TransReference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@TransDescrip", strTransDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@TransTypeID", TransTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@IsPosted", IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@IsAutomatic", IsAutomatic, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@AcademicYearId", msgEntry.strAcademicYearId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                TransactionID = Guid.NewGuid.ToString
                With sbTransaction
                    .Append("INSERT INTO saTransactions(TransactionID,StuEnrollID,CampusId,TransDate,TransAmount,CreateDate,TransReference,TransDescrip,moduser,moddate,TransTypeID,IsPosted,IsAutomatic,AcademicYearId) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@CampusId", StuCampusID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If msgEntry.strMsgType.ToUpper = "PELL" Then
                    db.AddParameter("@TransDate", FormatDate(msgEntry.strDisbursementDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Else
                    db.AddParameter("@TransDate", FormatDate(msgEntry.strActualDisbursementDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                db.AddParameter("@TransAmount", dbAmount * (-1.0), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@CreateDate", DateTime.Now(), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@TransReference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@TransDescrip", strTransDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@TransTypeID", TransTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@IsPosted", IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@IsAutomatic", IsAutomatic, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@AcademicYearId", msgEntry.strAcademicYearId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'execute the query
            Try
                db.RunParamSQLExecuteNoneQuery(sbTransaction.ToString)
                Return ""
                Exit Function
            Catch ex As System.Exception
                Return "Unable to apply payment for " & StudentName
                Exit Function
            End Try
        Finally
        End Try
    End Function
    Public Function Add_saPayments(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal TransactionID As String, ByVal msgEntry As EDMessageInfo, Optional ByVal StudentName As String = "") As String
        Dim sbPayments As New System.Text.StringBuilder(1000)
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(TransactionID) Then
            Return "Unable to apply payments for student " & StudentName & " as transaction is not found"
            Exit Function
        End If

        Dim intDuplicates As Integer = 0
        Dim sbAwardScheduleId As New StringBuilder
        Dim sbDuplicates As New StringBuilder
        Dim sbgetTransactionId As New StringBuilder
        Dim PaymentTypeID As Integer = 2
        Dim SchedulePayment As Boolean = False
        Dim IsDeposited As Boolean = False
        With sbDuplicates
            .Append(" select Count(*) from saPayments where TransactionId=? and PaymentTypeId=?")
        End With
        db.ClearParameters()
        db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PaymentTypeID", PaymentTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        Try
            intDuplicates = db.RunParamSQLScalar(sbDuplicates.ToString)
            If intDuplicates >= 1 Then
                Return ""
                Exit Function
            End If
        Catch ex As System.Exception
        End Try

        If intDuplicates = 0 Then
            Try
                With sbPayments
                    .Append("INSERT INTO saPayments(TransactionID, CheckNumber, ScheduledPayment, PaymentTypeID, IsDeposited, moduser, moddate) ")
                    .Append("VALUES(?,?,?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@CheckNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ScheduledPayment", SchedulePayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@PaymentTypeID", PaymentTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@IsDeposited", SchedulePayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sbPayments.ToString)
            Catch ex As System.Exception
                'Can put a message here but may disrupt the flow of RCVD Messages
                'so do nothing in the exception
                Return "Unable to post payment for " & StudentName
                Exit Function
            End Try
        End If
        'Return Add_saPayments
    End Function

    Public Function Add_saPmtDisbRel(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef PmtDisbRelID As String, ByVal TransactionID As String, ByVal StuAwardID As String, ByVal StuAwardScheduleID As String, ByVal msgEntry As EDMessageInfo, Optional ByVal StudentName As String = "") As String
        Dim sbPmtDisbRel As New System.Text.StringBuilder(1000)
        Dim sbPmtDisbRel1 As New System.Text.StringBuilder(1000)
        Dim intDisbCount As Integer = 0
        Dim dbAmount As Double
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(TransactionID) Then
            Return "Unable to add payment disbursement for student" & StudentName & " as provided transaction Id is not found"
            Exit Function
        End If
        If String.IsNullOrEmpty(StuAwardID) Then
            Return "Unable to add payment disbursement as student award is missing for student " & StudentName
            Exit Function
        End If
        PmtDisbRelID = Guid.NewGuid.ToString
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        'Dim myconn As New OleDbConnection(ConfigurationManager.AppSettings("ConString"))
        Dim myconn As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString").ToString)
        myconn.Open()
        Try
            If msgEntry.strMsgType.ToUpper = "PELL" Then
                dbAmount = Convert.ToDouble(msgEntry.strAcceptedDisbursementAmount)
            Else
                dbAmount = Convert.ToDouble(msgEntry.strActualDisbursementNetAmount)
            End If

        Catch ex As Exception
            dbAmount = 0.0
        End Try

        Try
            With sbPmtDisbRel
                .Append("Select Count(*) from saPmtDisbRel where TransactionID=? and Amount=? and StuAwardID=? ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Amount", dbAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                intDisbCount = db.RunParamSQLScalar(sbPmtDisbRel.ToString)
                sbPmtDisbRel.Remove(0, sbPmtDisbRel.Length)
                If intDisbCount >= 1 Then
                    With sbPmtDisbRel
                        .Append("Select PmtDisbRelId from saPmtDisbRel where TransactionID=? and Amount=? and StuAwardID=? and AwardScheduleId=?")
                    End With
                    'Add params
                    db.ClearParameters()
                    db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@Amount", dbAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardScheduleID", StuAwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Try
                        PmtDisbRelID = CType(db.RunParamSQLScalar(sbPmtDisbRel.ToString), Guid).ToString
                        sbPmtDisbRel.Remove(0, sbPmtDisbRel.Length)
                        sbPmtDisbRel.Append("Update saPmtDisbRel Set TransactionID=?,Amount=?,StuAwardID=?,AwardScheduleId=?,moduser=?,moddate=? ")
                        sbPmtDisbRel.Append(" Where PmtDisbRelId=? ")

                        db.ClearParameters()

                        db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@Amount", dbAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                        db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@AwardScheduleId", StuAwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                        db.AddParameter("@PmtDisbRelID", PmtDisbRelID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.RunParamSQLExecuteNoneQuery(sbPmtDisbRel.ToString)
                        Return ""
                        Exit Function
                    Catch ex As System.Exception
                    End Try
                End If
            Catch ex As System.Exception
                Return "Unable to apply payment for student " & StudentName
                Exit Function
            End Try

            With sbPmtDisbRel1
                .Append("INSERT INTO saPmtDisbRel(PmtDisbRelID,TransactionID,Amount,StuAwardID,AwardScheduleId,moduser,moddate) ")
                .Append("VALUES(?,?,?,?,?,?,?) ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@PmtDisbRelID", PmtDisbRelID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Amount", dbAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AwardScheduleId", StuAwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sbPmtDisbRel1.ToString)
                Return ""
                Exit Function
            Catch ex As System.Exception
                Dim sbGetAmount As New StringBuilder
                Dim strGetAmount As String = ""
                Dim sMessage As String = ""
                With sbGetAmount
                    .Append(" select Top 1 Amount from faStudentAwardSchedule where StudentAwardId=? and (Reference is Null or Reference='') order by ExpectedDate ")
                End With
                db.ClearParameters()
                db.AddParameter("@StudentAwardId", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Try
                    strGetAmount = CType(db.RunParamSQLScalar(sbGetAmount.ToString), Decimal).ToString
                Catch ex9 As Exception
                    strGetAmount = ""
                End Try
                sMessage = "Unable to apply payment as the disbursement amount did not match for student " & StudentName & "." & vbLf
                If Not strGetAmount = "" Then
                    sMessage &= "Disbursement amount in Advantage : " & CType(strGetAmount, Decimal).ToString("#0.00")
                End If
                Return sMessage
                Exit Function
            End Try
        Finally
        End Try
        'Return Add_saPmtDisbRel
    End Function
    Public Function Add_faAwardScheduleForPell(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef AwardScheduleID As String, ByRef TransactionID As String, ByVal StudentAwardID As String, ByVal msgEntry As EDMessageInfo, Optional ByVal StudentName As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal ExceptionGUID As String = "") As String
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwards As New StringBuilder
        Dim sbAwardScheduleId As New StringBuilder
        Dim intDuplicates As Integer = 0
        Dim dr As OleDbDataReader
        m_ExceptionMessage = ""
        'Add_faAwardSchedule = False
        If String.IsNullOrEmpty(StudentAwardID) Then
            Return "Unable to add disbursement as student award for student " & StudentName & " cannot be empty"
            Exit Function
        End If

        AwardScheduleID = Guid.NewGuid.ToString
        Try
            With sbCheckDuplicateAwards
                .Append(" Select Count(*) from faStudentAwardSchedule where ")
                .Append(" StudentAwardID=? and DisbursementNumber=? and Amount=? ")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@DisbursementNumber", msgEntry.strDisbursementNumber.Trim(), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Amount", IIf(msgEntry.strAcceptedDisbursementAmount.Trim() = "", 0.0, msgEntry.strAcceptedDisbursementAmount), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            Try
                intDuplicates = db.RunParamSQLScalar(sbCheckDuplicateAwards.ToString)
                If intDuplicates >= 1 Then
                    With sbAwardScheduleId
                        .Append(" Select AwardScheduleID,TransactionId from faStudentAwardSchedule where ")
                        .Append(" StudentAwardID=? and DisbursementNumber=? and Amount=? ")
                    End With
                    'Add params
                    db.ClearParameters()
                    db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@DisbursementNumber", msgEntry.strDisbursementNumber.Trim(), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@Amount", IIf(msgEntry.strAcceptedDisbursementAmount.Trim() = "", 0.0, msgEntry.strAcceptedDisbursementAmount), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    dr = db.RunParamSQLDataReader(sbAwardScheduleId.ToString)
                    While dr.Read()
                        AwardScheduleID = dr("AwardScheduleID").ToString
                        TransactionID = dr("TransactionId").ToString
                    End While
                    dr.Close()
                    Return ""
                    Exit Function
                End If
            Catch ex As System.Exception
                intDuplicates = 0
                dr.Close()
            End Try

            With sbAwardSchedule
                .Append("INSERT INTO faStudentAwardSchedule(AwardScheduleID,StudentAwardID,ExpectedDate,Amount,Reference,moduser,moddate,recid,DisbursementNumber) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?) ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@AwardScheduleID", AwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ExpectedDate", FormatDate(msgEntry.strDisbursementDate.Trim()), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Amount", IIf(msgEntry.strAcceptedDisbursementAmount.Trim() = "", 0.0, msgEntry.strAcceptedDisbursementAmount), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@Reference", "EDExpress-" & FormatDate(msgEntry.strDisbursementDate.Trim()), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@moddate", "", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@DisbursementNumber", msgEntry.strDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Try
                db.RunParamFLSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                TransactionID = ""
                Return ""
                Exit Function
            Catch ex As System.Exception
                Return "Unable to add disbursement for the selected student award for student" & StudentName
                Exit Function
            End Try
        Finally

        End Try
    End Function
    Public Function Update_faAwardScheduleForPell(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef AwardScheduleID As String, ByRef TransactionID As String) As String
        Dim sbAwardSchedule As New StringBuilder
        Try
            With sbAwardSchedule
                .Append("Update faStudentAwardSchedule Set TransactionId=? ")
                .Append(" Where AwardScheduleID=? ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AwardScheduleID", AwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                db.RunParamFLSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                Return ""
                Exit Function
            Catch ex As System.Exception
                Return "Unable to update transaction id for the selected award schedule "
                Exit Function
            End Try
        Finally

        End Try
    End Function
    Public Function Add_faAwardScheduleForDL(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef AwardScheduleID As String, ByRef TransactionID As String, ByVal StudentAwardID As String, ByVal msgEntry As EDMessageInfo, Optional ByVal StudentName As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal ExceptionGUID As String = "") As String
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbAwardScheduleId As New StringBuilder
        Dim sbCheckDuplicateAwards As New StringBuilder
        Dim sbCheckDuplicateAwardsNoDate As New StringBuilder
        Dim intDuplicatesNoDate As Integer = 0
        Dim intDuplicates As Integer = 0
        Dim dr As OleDbDataReader
        Dim intDisbNum As Integer = 0
        Dim intLoop As Integer = 1
        m_ExceptionMessage = ""
        'Add_faAwardSchedule = False
        If String.IsNullOrEmpty(StudentAwardID) Then
            Return "Unable to add disbursement as student award for student " & StudentName & " cannot be empty"
            Exit Function
        End If

        AwardScheduleID = Guid.NewGuid.ToString
        Try
            If msgEntry.strDatabaseIndicator.ToUpper = "M" Then
                intDisbNum = Convert.ToInt32(msgEntry.strActualDisbursementNumber)
                With sbCheckDuplicateAwards
                    .Append(" Select AwardScheduleID,TransactionId from faStudentAwardSchedule where ")
                    .Append(" StudentAwardID=?  Order By ExpectedDate Asc")
                End With
                db.ClearParameters()
                db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                dr = db.RunParamSQLDataReader(sbCheckDuplicateAwards.ToString)
                While dr.Read()
                    If intLoop = intDisbNum Then
                        AwardScheduleID = dr("AwardScheduleID").ToString
                        TransactionID = dr("TransactionId").ToString
                        Exit While
                    End If
                    intLoop += 1
                End While
                dr.Close()
                Return ""
                Exit Function
            Else
                With sbCheckDuplicateAwards
                    .Append(" Select Count(*) from faStudentAwardSchedule where ")
                    .Append(" StudentAwardID=? ")
                    .Append(" and DisbursementNumber=?")
                    .Append(" and Amount=? ")
                End With
                'Add params
                db.ClearParameters()
                db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If msgEntry.strDatabaseIndicator.ToUpper = "N" Then
                    db.AddParameter("@DisbursementNumber", msgEntry.strAnticipatedDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@Amount", IIf(msgEntry.strAnticipatedDisbursementNetAmount.Trim() = "", 0.0, msgEntry.strAnticipatedDisbursementNetAmount), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                Else
                    db.AddParameter("@DisbursementNumber", msgEntry.strActualDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@Amount", IIf(msgEntry.strActualDisbursementNetAmount.Trim() = "", 0.0, msgEntry.strActualDisbursementNetAmount), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                End If

                Try
                    intDuplicates = db.RunParamSQLScalar(sbCheckDuplicateAwards.ToString)
                    If intDuplicates >= 1 Then
                        With sbAwardScheduleId
                            .Append(" Select AwardScheduleID,TransactionId from faStudentAwardSchedule where ")
                            .Append(" StudentAwardID=? and DisbursementNumber=? and Amount=? ")
                        End With
                        'Add params
                        db.ClearParameters()
                        db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@DisbursementNumber", msgEntry.strAnticipatedDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@Amount", IIf(msgEntry.strAnticipatedDisbursementNetAmount.Trim() = "", 0.0, msgEntry.strAnticipatedDisbursementNetAmount), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                        dr = db.RunParamSQLDataReader(sbAwardScheduleId.ToString)
                        While dr.Read()
                            AwardScheduleID = dr("AwardScheduleID").ToString
                            TransactionID = dr("TransactionId").ToString
                        End While
                        dr.Close()
                        Return ""
                        Exit Function
                    End If
                Catch ex As System.Exception
                    intDuplicates = 0
                End Try
            End If


            With sbAwardSchedule
                .Append("INSERT INTO faStudentAwardSchedule(AwardScheduleID,StudentAwardID,ExpectedDate,Amount,Reference,moduser,moddate,recid,DisbursementNumber) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?) ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@AwardScheduleID", AwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            If msgEntry.strDatabaseIndicator.ToUpper = "N" Then
                db.AddParameter("@ExpectedDate", FormatDate(msgEntry.strAnticipatedDisbursementDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@Amount", IIf(msgEntry.strAnticipatedDisbursementNetAmount.Trim() = "", 0.0, msgEntry.strAnticipatedDisbursementNetAmount), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            Else
                db.AddParameter("@ExpectedDate", FormatDate(msgEntry.strActualDisbursementDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@Amount", IIf(msgEntry.strActualDisbursementNetAmount.Trim() = "", 0.0, msgEntry.strActualDisbursementNetAmount), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            End If
            If msgEntry.strDatabaseIndicator.ToUpper = "N" Then
                db.AddParameter("@Reference", "EDExpress-" & FormatDate(msgEntry.strAnticipatedDisbursementDate), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Reference", "EDExpress-" & FormatDate(msgEntry.strActualDisbursementDate), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@recid", "", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If msgEntry.strDatabaseIndicator.ToUpper = "N" Then
                db.AddParameter("@DisbursementNumber", msgEntry.strAnticipatedDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@DisbursementNumber", msgEntry.strActualDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            Try
                db.RunParamFLSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                TransactionID = ""
                Return ""
                Exit Function
            Catch ex As System.Exception
                Return "Unable to add disbursement for the selected student award for student" & StudentName
                Exit Function
            End Try

        Finally
        End Try
    End Function
    Public Function Update_faAwardScheduleForDL(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef AwardScheduleID As String, ByRef TransactionID As String) As String
        Dim sbAwardSchedule As New StringBuilder
        Try
            With sbAwardSchedule
                .Append("Update faStudentAwardSchedule Set TransactionId=? ")
                .Append(" Where AwardScheduleID=? ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AwardScheduleID", AwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                db.RunParamFLSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                Return ""
                Exit Function
            Catch ex As System.Exception
                Return "Unable to update transaction id for the selected award schedule "
                Exit Function
            End Try
        Finally

        End Try
    End Function
    Public Function BuildExceptionReport(ByRef db As DataAccess, ByVal ErrMsg As String, ByVal FileName As String, ByRef myMsgCollection As EDCollectionMessageInfos, ByVal MsgType As String, ByRef intNPDM As Integer, ByRef intNPDD As Integer, ByVal drP As DataRow, Optional ByVal drD1() As DataRow = Nothing, Optional ByVal drD2() As DataRow = Nothing, Optional ByVal ExceptionGUID As String = "", Optional ByRef intNPAD As Integer = 0) As String

        ''drD1 is used for Pell and Anticipated Direct Loan
        ''drD2 is used for Actual Direct Loan

        Dim sbParent As New System.Text.StringBuilder
        Dim sbChild As New StringBuilder
        Dim msgEntryParent As EDMessageInfo
        Dim msgEntryChild As EDMessageInfo
        Dim strExceptionReportId As String
        strExceptionReportId = Guid.NewGuid.ToString
        Try
            If MsgType = "PELL" Then
                intNPDM += 1
                msgEntryParent = myMsgCollection.Items(Convert.ToInt32(drP("MCID").ToString))
                With sbParent
                    .Append("INSERT INTO syEDExpressExceptionReportPell_ACG_SMART_Teach_StudentTable(ExceptionReportId,DbIn,Filter,AwardAmount,AwardId,GrantType,AddDate,AddTime,OriginalSSN,UpdateDate,UpdateTime,OriginationStatus,AcademicYearId,AwardYearStartDate,AwardYearEndDate,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                    'Add params
                    db.ClearParameters()
                    db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@DbIn", msgEntryParent.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@Filter", msgEntryParent.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    If msgEntryParent.strDatabaseIndicator.ToUpper = "T" Then
                        db.AddParameter("@AwardAmount", msgEntryParent.strAwardAmountForEntireYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@AwardAmount", msgEntryParent.strAwardAmountForEntireSchoolYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    db.AddParameter("@AwardId", msgEntryParent.strAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@GrantType", IIf(msgEntryParent.strGrandType = "TT", "", msgEntryParent.strGrandType), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    If msgEntryParent.strDatabaseIndicator.ToUpper = "T" Then
                        db.AddParameter("@AddDate", msgEntryParent.strPellAddDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@AddDate", msgEntryParent.strTeachAddDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    If msgEntryParent.strDatabaseIndicator.ToUpper = "T" Then
                        db.AddParameter("@AddTime", msgEntryParent.strPellAddTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@AddTime", msgEntryParent.strTeachAddTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If

                    db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    If msgEntryParent.strDatabaseIndicator.ToUpper = "T" Then
                        db.AddParameter("@UpdateDate", msgEntryParent.strPellUpdateDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@UpdateDate", msgEntryParent.strTeachUpdateDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    If msgEntryParent.strDatabaseIndicator.ToUpper = "T" Then
                        db.AddParameter("@UpdateTime", msgEntryParent.strPellUpdateTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@UpdateTime", msgEntryParent.strTeachUpdateTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    db.AddParameter("@OriginationStatus", msgEntryParent.strOriginationStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AcademicYearId", msgEntryParent.strAcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardYearStartDate", msgEntryParent.strAwardYearStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardYearEndDate", msgEntryParent.strAwardYearEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End With
                Try
                    db.RunParamSQLExecuteNoneQuery(sbParent.ToString)
                Catch ex As System.Exception
                End Try
                sbParent = sbParent.Remove(0, sbParent.Length)
                If Not drD1 Is Nothing Then
                    For Each drT As DataRow In drD1
                        intNPDD += 1
                        msgEntryChild = myMsgCollection.Items(Convert.ToInt32(drT("MCID").ToString))
                        With sbChild
                            .Append("INSERT INTO syEDExpressExceptionReportPell_ACG_SMART_Teach_DisbursementTable(DetailId,ExceptionReportId,DbIn,Filter,AccDisbAmount,DisbDate,DisbNum,DisbRelIndi,DusbSeqNum,SimittedDisbAmount,ActionStatusDisb,OriginalSSN,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                            .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                            'Add params
                            db.ClearParameters()
                            db.AddParameter("@DetailId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DbIn", msgEntryChild.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@Filter", msgEntryChild.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@AccDisbAmount", msgEntryChild.strAcceptedDisbursementAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DisbDate", msgEntryChild.strDisbursementDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DisbNum", msgEntryChild.strDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DisbRelIndi", msgEntryChild.strDisbursementRealeaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@DusbSeqNum", msgEntryChild.strDisbursementSequenceNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@SimittedDisbAmount", msgEntryChild.strSubmittedDisbursementAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@ActionStatusDisb", msgEntryChild.strActionStatusDisbursement, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                            db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        End With
                        Try
                            db.RunParamSQLExecuteNoneQuery(sbChild.ToString)
                        Catch ex As System.Exception
                        End Try
                        sbChild = sbChild.Remove(0, sbChild.Length)
                    Next
                End If

            Else
                msgEntryParent = myMsgCollection.Items(Convert.ToInt32(drP("MCID").ToString))
                intNPDM += 1
                With sbParent
                    .Append("INSERT INTO syEDExpressExceptionReportDirectLoan_StudentTable(ExceptionReportId,DbIn,Filter,AddDate,AddTime,LoanAmtApproved,LoanFeePer,LoanId,LoanPeriodEndDate,LoanPeriodStartDate,LoanStatus,LoanType,MPNStatus,OriginalSSN,UpdateDate,UpdateTime,AcademicYearId,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                    'Add params
                    db.ClearParameters()
                    db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@DbIn", msgEntryParent.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@Filter", msgEntryParent.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AddDate", msgEntryParent.strLoanAddDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AddTime", msgEntryParent.strLoanAddTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@LoanAmtApproved", msgEntryParent.strLoanAmountApproved, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@LoanFeePer", msgEntryParent.strLoanFeePercentage, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@LoanId", msgEntryParent.strLoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@LoanPeriodEndDate", msgEntryParent.strLoanPeriodEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@LoanPeriodStartDate", msgEntryParent.strLoanPeriodStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@LoanStatus", msgEntryParent.strLoanStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@LoanType", msgEntryParent.strLoanType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@MPNStatus", msgEntryParent.strMPNStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@UpdateDate", msgEntryParent.strLoanUpdateDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@UpdateTime", msgEntryParent.strLoanUpdateTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AcademicYearId", msgEntryParent.strAcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End With
                Try
                    db.RunParamSQLExecuteNoneQuery(sbParent.ToString)
                Catch ex As System.Exception
                End Try
                sbParent = sbParent.Remove(0, sbParent.Length)
                If Not drD1 Is Nothing Then
                    For Each drT1 As DataRow In drD1
                        msgEntryChild = myMsgCollection.Items(Convert.ToInt32(drT1("MCID").ToString))
                        If msgEntryChild.blnIsParsed Then
                            intNPDD += 1
                            With sbChild
                                .Append("INSERT INTO syEDExpressExceptionReportDirectLoan_DisbursementTable(DetailId,ExceptionReportId,DbIn,Filter,DisbDate,DisbGrossAmount,DisbIntRebAmount,DisbLoanFeeAmount,DisbNetAdjAmount,DisbNetAmount,DisbNum,DisbSeqNum,DisbStatus_RelInd,DisbType,LoanId,OriginalSSN,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                                'Add params
                                db.ClearParameters()
                                db.AddParameter("@DetailId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DbIn", msgEntryChild.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@Filter", msgEntryChild.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbDate", msgEntryChild.strAnticipatedDisbursementDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbGrossAmount", msgEntryChild.strAnticipatedDisbursementGrossAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbIntRebAmount", msgEntryChild.strAnticipatedDisbursementInterestRebateAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbLoanFeeAmount", msgEntryChild.strAnticipatedDisbursementFeeAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbNetAdjAmount", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbNetAmount", msgEntryChild.strAnticipatedDisbursementNetAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbNum", msgEntryChild.strAnticipatedDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbSeqNum", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbStatus_RelInd", msgEntryChild.strAnticipatedDisbursementRealeaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbType", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@LoanId", msgEntryChild.strLoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                                db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            End With
                            Try
                                db.RunParamSQLExecuteNoneQuery(sbChild.ToString)
                            Catch ex As System.Exception
                            End Try
                        End If
                        sbChild = sbChild.Remove(0, sbChild.Length)
                    Next
                End If
                If Not drD2 Is Nothing Then
                    For Each drT2 As DataRow In drD2
                        msgEntryChild = myMsgCollection.Items(Convert.ToInt32(drT2("MCID").ToString))
                        If msgEntryChild.blnIsParsed Then
                            intNPAD += 1
                            With sbChild
                                .Append("INSERT INTO syEDExpressExceptionReportDirectLoan_DisbursementTable(DetailId,ExceptionReportId,DbIn,Filter,DisbDate,DisbGrossAmount,DisbIntRebAmount,DisbLoanFeeAmount,DisbNetAdjAmount,DisbNetAmount,DisbNum,DisbSeqNum,DisbStatus_RelInd,DisbType,LoanId,OriginalSSN,ModDate,ErrorMsg,FileName,ExceptionGUID) ")
                                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                                'Add params
                                db.ClearParameters()
                                db.AddParameter("@DetailId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@ExceptionReportId", strExceptionReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DbIn", msgEntryChild.strDatabaseIndicator, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@Filter", msgEntryChild.strFilter, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbDate", msgEntryChild.strActualDisbursementDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbGrossAmount", msgEntryChild.strActualDisbursementGrossAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbIntRebAmount", msgEntryChild.strActualDisbursementInterestRebateAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbLoanFeeAmount", msgEntryChild.strActualDisbursementLoanFeeAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbNetAdjAmount", msgEntryChild.strActualDisbursementNetAdjustmentAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbNetAmount", msgEntryChild.strActualDisbursementNetAmount, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbNum", msgEntryChild.strActualDisbursementNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbSeqNum", msgEntryChild.strActualDisbursementSequenceNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbStatus_RelInd", msgEntryChild.strActualDisbursementStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@DisbType", msgEntryChild.strActualDisbursementType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@LoanId", msgEntryChild.strLoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@OriginalSSN", msgEntryParent.strOriginalSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                                db.AddParameter("@ErrorMsg", ErrMsg, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            End With
                            Try
                                db.RunParamSQLExecuteNoneQuery(sbChild.ToString)
                            Catch ex As System.Exception
                            End Try
                        End If
                        sbChild = sbChild.Remove(0, sbChild.Length)
                    Next
                End If
            End If
        Finally
        End Try
    End Function
    Public Function ValidateDate(ByRef DateToFormat As String) As String
        Dim dt As DateTime
        Dim strDD As String
        Dim strMM As String
        Dim strYYYY As String

        Try
            strYYYY = DateToFormat.Substring(0, 4)
            strMM = DateToFormat.Substring(4, 2)
            strDD = DateToFormat.Substring(6, 2)
            dt = Convert.ToDateTime(strMM + "/" + strDD + "/" + strYYYY)
            Return dt.ToString("MM/dd/yyyy")
        Catch ex As Exception
            Return ""
        End Try
    End Function
    Public Sub DeleteFromExceptionReport(ByRef db As DataAccess, ByVal FileName As String, ByVal MsgType As String, ByVal ExceptionID As String)
        Dim sbDelete As New System.Text.StringBuilder(1000)
        Try
            With sbDelete
                If MsgType.ToUpper = "PELL" Then
                    .Append("Delete from  syEDExpressExceptionReportPell_ACG_SMART_Teach_StudentTable where FileName=?  and ExceptionGUID <> ? ")
                    .Append("Delete from  syEDExpressExceptionReportPell_ACG_SMART_Teach_DisbursementTable where FileName=? and ExceptionGUID <> ? ")
                Else
                    .Append("Delete from  syEDExpressExceptionReportDirectLoan_StudentTable where FileName=? and ExceptionGUID<>? ")
                    .Append("Delete from  syEDExpressExceptionReportDirectLoan_DisbursementTable where FileName=? and ExceptionGUID<>? ")
                End If
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ExceptionGUID", ExceptionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ExceptionGUID", ExceptionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sbDelete.ToString)
                Exit Sub
            Catch ex As System.Exception
            End Try
        Finally
        End Try
    End Sub
    Public Sub DeleteFromExceptionReport(ByRef db As DataAccess, ByVal FileName As String, ByVal MsgType As String)
        Dim sbDelete As New System.Text.StringBuilder(1000)
        Try
            With sbDelete
                If MsgType.ToUpper = "PELL" Then
                    .Append("Delete from  syEDExpressExceptionReportPell_ACG_SMART_Teach_StudentTable where FileName=? ")
                    .Append("Delete from  syEDExpressExceptionReportPell_ACG_SMART_Teach_DisbursementTable where FileName=? ")
                Else
                    .Append("Delete from  syEDExpressExceptionReportDirectLoan_StudentTable where FileName=? ")
                    .Append("Delete from  syEDExpressExceptionReportDirectLoan_DisbursementTable where FileName=? ")
                End If
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sbDelete.ToString)
                Exit Sub
            Catch ex As System.Exception
            End Try
        Finally
        End Try
    End Sub
    Public Function GetMatchingStudentAwards(ByRef db As DataAccess, ByVal strStuEnrollID As String, ByVal strAwardId As String, ByVal strFundDescrip As String, ByVal GrossAmount As String, Optional ByVal AwardStartDate As String = "", Optional ByVal AwardEndDate As String = "", Optional ByVal StudentName As String = "", Optional ByVal FileType As String = "") As String
        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbAwardID As New System.Text.StringBuilder(1000)
        Dim sbAcademicYear As New StringBuilder
        Dim StudentAwardID As String
        Dim strFN As String

        If String.IsNullOrEmpty(strAwardId) Then
            Return "Unable to get student award for student " & StudentName & " as the fund provided is empty"
            Exit Function
        End If
        If String.IsNullOrEmpty(strStuEnrollID) Then
            Return "Unable to find student enrollment information for the student" & StudentName
            Exit Function
        End If
        If FileType.ToUpper = "PELL" Then
            If strFundDescrip = "" Or strFundDescrip.ToUpper = "P" Then
                strFN = "PELL"
            ElseIf strFundDescrip = "TT" Then
                strFN = "TEACH"
            ElseIf strFundDescrip = "A" Then
                strFN = "ACG"
            ElseIf strFundDescrip = "S" Or strFundDescrip = "T" Then
                strFN = "SMART"
            End If
        Else
            If strFundDescrip = "P" Then
                strFN = "DL - PLUS"
            ElseIf strFundDescrip = "S" Then
                strFN = "DL - SUB"
            ElseIf strFundDescrip = "U" Then
                strFN = "DL - UNSUB"
            Else
                strFN = ""
            End If
        End If


        sbAwardID = New StringBuilder
        With sbAwardID
            .Append(" select Distinct Top 1 StudentAwardId from faStudentAwards where StuEnrollId=? and ")
            .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources, syAdvFundSources where syAdvFundSources.AdvFundSourceId=saFundSources.AdvFundSourceId AND syAdvFundSources.Descrip=?)  ")
            '.Append(" and AcademicYearId = ? ")
            '.Append(" and GrossAmount=? ")
            ''Commented on Aug 05, 2009
            ''If FileType.ToUpper = "PELL" Then
            ''    .Append(" and FA_ID =? ")
            ''End If
            ''If FileType = "DL" Then
            ''    .Append(" and LoanId=? ")
            ''End If
            ''Commented on Aug 05, 2009
            .Append(" and FA_ID =? ")
            '.Append(" and AwardStartDate=? ")
            '.Append(" and AwardEndDate=? ")
        End With
        Try
            db.ClearParameters()
            db.AddParameter("@STUENROLLID", strStuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FundDescrip", strFN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'db.AddParameter("@GrossAmount", GrossAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            ''Commented on Aug 05, 2009
            ''If FileType.ToUpper = "PELL" Then
            ''    db.AddParameter("@FA_ID", strAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            ''End If
            ''If FileType = "DL" Then
            ''    db.AddParameter("@LoanId", strAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            ''End If
            ''Commented on Aug 05, 2009
            db.AddParameter("@FA_ID", strAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'db.AddParameter("@AwardStartDate", AwardStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'db.AddParameter("@AwardEndDate", AwardEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            dr = db.RunParamSQLDataReader(sbAwardID.ToString())
            If Not (dr.HasRows) Then
                Return ""
                Exit Function
            End If
            While dr.Read()
                Try
                    StudentAwardID = dr("StudentAwardID").ToString
                    Return StudentAwardID
                Catch ex As System.Exception
                    Return ""
                    Exit Function
                End Try
            End While
        Catch ex As System.Exception
            Return ""
            Exit Function
        Finally
            dr.Close()
        End Try
    End Function
    Public Function Add_faStudentAwardsFromPell(ByRef groupTrans As OleDbTransaction, ByRef db As DataAccess, ByRef StudentAwardID As String, ByVal StuEnrollID As String, ByVal msgEntry As EDMessageInfo, ByVal DisbNumber As String, Optional ByVal strStudentName As String = "", Optional ByVal SourceToTarget As String = "") As String
        Dim sbStudentAwards As New System.Text.StringBuilder(1000)
        Dim sbUpdateStudentAwards As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwards As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchool As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchoolDiffLoanPeriods As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchoolWithDiffGrossAmount As New System.Text.StringBuilder(1000)


        Dim AwardTypeID As String = " "
        Dim getAwardTypeMessage As String = ""
        Dim Disbursements As Integer = 0
        Dim LoanID As String = ""
        Dim intDuplicates As Integer = 0
        Dim intDuplicatesBySchool As Integer = 0
        Dim intDuplicatesBySchoolDiffLoanPeriods As Integer = 0
        Dim intDuplicatesBySchoolDiffGrossAmount As Integer = 0
        Dim intDuplicatesBySchoolSameAwardYearDiffLoanPeriods As Integer = 0

        Dim sbAcademicYear As New StringBuilder
        'Dim strAwardYear As String
        'Dim dr As OleDbDataReader
        m_ExceptionMessage = ""

        'db.OpenConnection()

        If String.IsNullOrEmpty(StuEnrollID) Or StuEnrollID = Guid.Empty.ToString Then
            Return "Unable to find enrollment information for the student" & strStudentName
            Exit Function
        End If

        'Look for matching records
        Try
            ''StudentAwardID = GetMatchingStudentAwards(db, StuEnrollID, msgEntry.strAwardId, msgEntry.strGrandType, msgEntry.strAwardAmountForEntireYear, "", "", strStudentName, msgEntry.strMsgType)
            If msgEntry.strDatabaseIndicator = "T" Then
                StudentAwardID = GetMatchingStudentAwards(db, StuEnrollID, msgEntry.strAwardId, msgEntry.strGrandType, msgEntry.strAwardAmountForEntireYear, msgEntry.strAwardYearStartDate, msgEntry.strAwardYearEndDate, strStudentName, msgEntry.strMsgType)
            Else
                StudentAwardID = GetMatchingStudentAwards(db, StuEnrollID, msgEntry.strAwardId, msgEntry.strGrandType, msgEntry.strAwardAmountForEntireSchoolYear, msgEntry.strAwardYearStartDate, msgEntry.strAwardYearEndDate, strStudentName, msgEntry.strMsgType)
            End If

            If Not StudentAwardID = "" Then
                'Update existing record with this FAID
                With sbUpdateStudentAwards
                    .Append("Update faStudentAwards set AcademicYearId=?,fa_id=?,GrossAmount=?,AwardStartDate=?,AwardEndDate=?,moduser=?,moddate=?,Disbursements=?,LoanId=? where StudentAwardId=? ")
                End With
                db.ClearParameters()
                'db.AddParameter("@FA_ID", msgEntry.strAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                'db.AddParameter("@StudentAwardId", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AcademicYearId", msgEntry.strAcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@fa_id", msgEntry.strAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If msgEntry.strDatabaseIndicator = "T" Then
                    db.AddParameter("@GrossAmount", msgEntry.strAwardAmountForEntireYear.Trim(), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                Else
                    db.AddParameter("@GrossAmount", msgEntry.strAwardAmountForEntireSchoolYear.Trim(), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                End If
                db.AddParameter("@AwardStartDate", msgEntry.strAwardYearStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardEndDate", msgEntry.strAwardYearEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@Disbursements", DisbNumber, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@LoanId", "", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Try
                    db.RunParamSQLExecuteNoneQuery(sbUpdateStudentAwards.ToString)
                    Return StudentAwardID
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to match/update FAID with student awards for student " & strStudentName
                    Exit Function
                End Try
            Else
                StudentAwardID = Guid.NewGuid.ToString
            End If
        Catch ex As System.Exception
            StudentAwardID = Guid.NewGuid.ToString
        End Try

        'Before Creating new awards check to see if the Award Start Date falls after the AwardsCuttOffDate
        'in web.config
        Try
            getAwardTypeMessage = GetAwardTypeID(db, AwardTypeID, msgEntry.strGrandType, msgEntry.strMsgType)
            If Not getAwardTypeMessage = "" Then
                Return "Unable to add student awards for student " & strStudentName & " as the provided award type was not found"
                Exit Function
            End If

            With sbStudentAwards
                .Append("INSERT INTO faStudentAwards(StudentAwardID,StuEnrollID,AwardTypeID,AcademicYearId,fa_id,GrossAmount,AwardStartDate,AwardEndDate,LoanFees,moduser,moddate,Disbursements,LoanId) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AwardTypeID", AwardTypeID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AcademicYearId", msgEntry.strAcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@fa_id", msgEntry.strAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If msgEntry.strDatabaseIndicator = "T" Then
                db.AddParameter("@GrossAmount", msgEntry.strAwardAmountForEntireYear.Trim(), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            Else
                db.AddParameter("@GrossAmount", msgEntry.strAwardAmountForEntireSchoolYear.Trim(), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            End If
            db.AddParameter("@AwardStartDate", msgEntry.strAwardYearStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AwardEndDate", msgEntry.strAwardYearEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@LoanFees", 0.0, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Disbursements", DisbNumber, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@LoanId", "", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   execute the query
            Try
                db.RunParamSQLExecuteNoneQuery(sbStudentAwards.ToString)
                Return StudentAwardID
                Exit Function
            Catch ex As System.Exception
                Return "Unable to create student awards for student " & strStudentName
                'db.CloseConnection()
                Exit Function
            End Try
            '' ''End If
        Catch ex As System.Exception
            Return "Unable to create student award for student " & strStudentName
            Exit Function
        Finally
        End Try
    End Function
    Public Function Check_StudentSchedulePell(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal StudentAwardID As String, ByVal myMsgCollection As EDCollectionMessageInfos, ByVal drTempRows() As DataRow) As String
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim dsTemp As New DataSet
        Dim msgEntry As EDMessageInfo
        Dim strAwardScheduleId As String = ""
        Dim strDisbNum As String = ""
        ''  Dim drTemp() As DataRow
        Try
            For Each drC As DataRow In drTempRows
                msgEntry = myMsgCollection.Items(Convert.ToInt32(drC("MCID").ToString))
                If strDisbNum = "" Then
                    strDisbNum = msgEntry.strDisbursementNumber
                Else
                    strDisbNum = strDisbNum & "," & msgEntry.strDisbursementNumber
                End If

            Next
            With sbAwardSchedule
                .Append(" Delete From faStudentAwardSchedule where ")
                .Append(" StudentAwardID=?  and DisbursementNumber Not In ( " & strDisbNum & " )")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                Exit Function
            Catch ex As System.Exception
            End Try
        Finally
        End Try
    End Function
    Public Function Check_StudentScheduleDL(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal StudentAwardID As String, ByVal myMsgCollection As EDCollectionMessageInfos, ByVal drTempRows() As DataRow) As String
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim dsTemp As New DataSet
        Dim msgEntry As EDMessageInfo
        Dim strAwardScheduleId As String = ""
        Dim strDisbNum As String = ""
        '' Dim drTemp() As DataRow
        Try
            For Each drC As DataRow In drTempRows
                msgEntry = myMsgCollection.Items(Convert.ToInt32(drC("MCID").ToString))
                If strDisbNum = "" Then
                    strDisbNum = msgEntry.strAnticipatedDisbursementNumber
                Else
                    strDisbNum = strDisbNum & "," & msgEntry.strAnticipatedDisbursementNumber
                End If

            Next
            With sbAwardSchedule
                .Append(" Delete From faStudentAwardSchedule where ")
                .Append(" StudentAwardID=?  and DisbursementNumber Not In ( " & strDisbNum & " )")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                Exit Function
            Catch ex As System.Exception
            End Try
        Finally
        End Try
    End Function
    Public Function Check_TransactionPosted(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal StudentAwardScheduleID As String, ByRef TransactionID As String) As Boolean
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim dsTemp As New DataSet
        ''  Dim msgEntry As EDMessageInfo
        Try
            With sbAwardSchedule
                .Append(" Select TransactionId from saPmtDisbRel where ")
                .Append(" AwardScheduleId=? ")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@AwardScheduleId", StudentAwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                dsTemp = db.RunParamSQLDataSet(sbAwardSchedule.ToString)
                If dsTemp.Tables(0).Rows.Count = 0 Then
                    TransactionID = dsTemp.Tables(0).Rows(0)("TransactionId").ToString
                    Return False
                    Exit Function
                Else
                    Return True
                    Exit Function
                End If
            Catch ex As System.Exception
            End Try
        Finally
        End Try
    End Function
    Public Function Add_faStudentAwardsFromDL(ByRef groupTrans As OleDbTransaction, ByRef db As DataAccess, ByRef StudentAwardID As String, ByVal StuEnrollID As String, ByVal msgEntry As EDMessageInfo, ByVal DisbNumber As String, Optional ByVal strStudentName As String = "", Optional ByVal SourceToTarget As String = "") As String
        Dim sbStudentAwards As New System.Text.StringBuilder(1000)
        Dim sbUpdateStudentAwards As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwards As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchool As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchoolDiffLoanPeriods As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchoolWithDiffGrossAmount As New System.Text.StringBuilder(1000)
        '' Dim dr As OleDbDataReader

        Dim AwardTypeID As String = " "
        Dim getAwardTypeMessage As String = ""
        Dim Disbursements As Integer = 0
        Dim LoanID As String = ""
        Dim intDuplicates As Integer = 0
        Dim intDuplicatesBySchool As Integer = 0
        Dim intDuplicatesBySchoolDiffLoanPeriods As Integer = 0
        Dim intDuplicatesBySchoolDiffGrossAmount As Integer = 0
        Dim intDuplicatesBySchoolSameAwardYearDiffLoanPeriods As Integer = 0

        Dim sbAcademicYear As New StringBuilder
        m_ExceptionMessage = ""

        'db.OpenConnection()

        If String.IsNullOrEmpty(StuEnrollID) Or StuEnrollID = Guid.Empty.ToString Then
            Return "Unable to find enrollment information for the student" & strStudentName
            Exit Function
        End If
        Dim dbAmount As Double = 0.0
        Dim dbFeePer As Double = 0.0
        Dim dbLoanFees As Double = 0.0
        Try
            dbAmount = Convert.ToDouble(msgEntry.strLoanAmountApproved)
        Catch ex As Exception
            dbAmount = 0.0
        End Try
        Try
            dbFeePer = Convert.ToDouble(msgEntry.strLoanFeePercentage)
        Catch ex As Exception
            dbFeePer = 0.0
        End Try
        dbLoanFees = (dbAmount * dbFeePer) / 100
        'Look for matching records
        Try

            StudentAwardID = GetMatchingStudentAwards(db, StuEnrollID, msgEntry.strLoanId, msgEntry.strLoanType, msgEntry.strLoanAmountApproved, msgEntry.strAwardYearStartDate, msgEntry.strAwardYearEndDate, strStudentName, msgEntry.strMsgType)

            If Not StudentAwardID = "" Then
                'Update existing record with this FAID
                With sbUpdateStudentAwards
                    .Append("Update faStudentAwards set AcademicYearId=?,GrossAmount=?,LoanFees=?,AwardStartDate=?,AwardEndDate=?,moduser=?,moddate=?, ")
                    If Not DisbNumber = "0" Then
                        .Append(" Disbursements=?, ")
                    End If
                    .Append(" fa_id=? where StudentAwardId=? ")
                End With
                db.ClearParameters()
                db.AddParameter("@AcademicYearId", msgEntry.strAcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@GrossAmount", dbAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@LoanFees", dbLoanFees, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@AwardStartDate", msgEntry.strAwardYearStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardEndDate", msgEntry.strAwardYearEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If Not DisbNumber = "0" Then
                    db.AddParameter("@Disbursements", DisbNumber, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                End If
                db.AddParameter("@fa_id", msgEntry.strLoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Try
                    db.RunParamSQLExecuteNoneQuery(sbUpdateStudentAwards.ToString)
                    Return StudentAwardID
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to match/update FAID with student awards for student " & strStudentName
                    Exit Function
                End Try
            Else
                StudentAwardID = Guid.NewGuid.ToString
            End If
        Catch ex As System.Exception
            StudentAwardID = Guid.NewGuid.ToString
        End Try

        'Before Creating new awards check to see if the Award Start Date falls after the AwardsCuttOffDate
        'in web.config
        Try

            getAwardTypeMessage = GetAwardTypeID(db, AwardTypeID, msgEntry.strLoanType, msgEntry.strMsgType)

            If Not getAwardTypeMessage = "" Then
                Return "Unable to add student awards for student " & strStudentName & " as the provided award type was not found"
                Exit Function
            End If

            With sbStudentAwards
                .Append("INSERT INTO faStudentAwards(StudentAwardID,StuEnrollID,AwardTypeID,AcademicYearId,fa_id,GrossAmount,LoanFees,AwardStartDate,AwardEndDate,moduser,moddate,Disbursements,LoanId) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AwardTypeID", AwardTypeID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AcademicYearId", msgEntry.strAcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@fa_id", msgEntry.strLoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@GrossAmount", dbAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@LoanFees", dbLoanFees, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@AwardStartDate", msgEntry.strAwardYearStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AwardEndDate", msgEntry.strAwardYearEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Disbursements", DisbNumber, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@LoanId", LoanID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   execute the query
            Try
                db.RunParamSQLExecuteNoneQuery(sbStudentAwards.ToString)
                Return StudentAwardID
                Exit Function
            Catch ex As System.Exception
                Return "Unable to create student awards for student " & strStudentName
                'db.CloseConnection()
                Exit Function
            End Try
            '' ''End If
        Catch ex As System.Exception
            Return "Unable to create student award for student " & strStudentName
            Exit Function
        Finally
        End Try
    End Function

    Public Function FormatDate(ByRef DateToFormat As String) As String
        Dim strDD As String
        Dim strMM As String
        Dim strYYYY As String
        Try
            strYYYY = DateToFormat.Substring(0, 4)
            strMM = DateToFormat.Substring(4, 2)
            strDD = DateToFormat.Substring(6, 2)
            Return strMM + "/" + strDD + "/" + strYYYY
        Catch ex As Exception
            Return DateToFormat
        End Try
    End Function
    Public Function FormatTime(ByRef TimeToFormat As String) As String
        Dim strHH As String
        Dim strMM As String
        Dim strSS As String
        Try
            strHH = TimeToFormat.Substring(0, 2)
            strMM = TimeToFormat.Substring(2, 2)
            strSS = TimeToFormat.Substring(4, 2)
            Return strHH + ":" + strMM + ":" + strSS
        Catch ex As Exception
            Return TimeToFormat
        End Try
    End Function
    Public Function getExceptionReport(ByVal ExceptionGUID As String, ByVal MsgType As String) As DataSet
        Dim db As New DataAccess
        Dim sbException As New StringBuilder
        Dim ds As New DataSet
        Dim dsTemp As New DataSet
        Dim dsR As New DataSet
        Dim dtParent As New DataTable("Parent")
        Dim dtChild As New DataTable("Child")
        ''   Dim drTemp() As DataRow
        Dim drParent As DataRow
        Dim drChild As DataRow
        Dim strERID_Old As String = ""
        Dim strERID_New As String = ""
        Dim strGrantType As String = ""
        ds = Nothing

        Dim dbValue As Double = 0.0

        dtParent.Columns.Add("SSN", GetType(String))
        dtParent.Columns.Add("Campus", GetType(String))
        dtParent.Columns.Add("Amount", GetType(String))
        dtParent.Columns.Add("AwardIdLoanId", GetType(String))
        dtParent.Columns.Add("GrantTypeLoanType", GetType(String))
        dtParent.Columns.Add("FileName", GetType(String))
        dtParent.Columns.Add("Message", GetType(String))
        dtParent.Columns.Add("ModDate", GetType(DateTime))

        dtChild.Columns.Add("SSN", GetType(String))
        dtChild.Columns.Add("Campus", GetType(String))
        dtChild.Columns.Add("DisbDate", GetType(String))
        dtChild.Columns.Add("AccDisbAmtDisbGrossAmt", GetType(String))
        dtChild.Columns.Add("SubDisbAmtDisbNetAmt", GetType(String))
        dtChild.Columns.Add("FileName", GetType(String))
        dtChild.Columns.Add("Message", GetType(String))
        dtChild.Columns.Add("ModDate", GetType(DateTime))

        If MsgType = "PELL" Then

            Try
                With sbException
                    .Append(" Select PT.ExceptionReportId,PT.DbIn, PT.OriginalSSN ,PT.AwardAmount,PT.AwardId,PT.GrantType,PT.FileName,PT.ErrorMsg,PT.ModDate, ")
                    .Append(" (SELECT TOP 1 C.CampDescrip FROM arStuEnrollments SE INNER JOIN arStudent S ON SE.StudentId = S.StudentId INNER JOIN syCampuses C ON SE.CampusId=C.CampusId WHERE PT.OriginalSSN=S.SSN  order by EnrollDate Desc ) AS CampDescrip ")
                    .Append(" From syEDExpressExceptionReportPell_ACG_SMART_Teach_StudentTable PT Where PT.ExceptionGUID='" & ExceptionGUID & "' Order By PT.OriginalSSN")
                End With
                ds = db.RunParamSQLDataSet(sbException.ToString)
                For Each dr As DataRow In ds.Tables(0).Rows
                    drParent = dtParent.NewRow()
                    drParent("SSN") = dr("OriginalSSN").ToString
                    drParent("Campus") = dr("CampDescrip").ToString
                    Try
                        dbValue = Convert.ToDouble(dr("AwardAmount").ToString)
                    Catch ex As Exception
                        dbValue = 0.0
                    End Try
                    drParent("Amount") = dbValue.ToString("0.00")
                    drParent("AwardIdLoanId") = dr("AwardId").ToString
                    If dr("DbIn").ToString.ToUpper = "H" Then
                        strGrantType = "TEACH"
                    Else
                        strGrantType = IIf(dr("GrantType").ToString = "" Or dr("GrantType").ToString = "P", "PELL", IIf(dr("GrantType").ToString = "A", "ACG", IIf(dr("GrantType").ToString = "S" Or dr("GrantType").ToString = "T", "SMART", "")))
                    End If
                    drParent("GrantTypeLoanType") = strGrantType
                    drParent("FileName") = dr("FileName").ToString
                    drParent("Message") = dr("ErrorMsg").ToString
                    drParent("ModDate") = dr("ModDate").ToString
                    dtParent.Rows.Add(drParent)
                    sbException = sbException.Remove(0, sbException.Length)
                    With sbException
                        .Append(" Select CT.DbIn, CT.OriginalSSN ,CT.DisbDate,CT.AccDisbAmount,CT.SimittedDisbAmount,CT.FileName,CT.ErrorMsg,CT.ModDate, ")
                        .Append(" (SELECT TOP 1 C.CampDescrip FROM arStuEnrollments SE INNER JOIN arStudent S ON SE.StudentId = S.StudentId INNER JOIN syCampuses C ON SE.CampusId=C.CampusId WHERE CT.OriginalSSN=S.SSN  order by EnrollDate Desc ) AS CampDescrip ")
                        .Append(" From syEDExpressExceptionReportPell_ACG_SMART_Teach_DisbursementTable CT Where CT.ExceptionReportId='" & dr("ExceptionReportId").ToString & "' Order By CT.OriginalSSN")
                    End With
                    dsTemp = db.RunParamSQLDataSet(sbException.ToString)
                    For Each drC As DataRow In dsTemp.Tables(0).Rows
                        drChild = dtChild.NewRow()
                        drChild("SSN") = dr("OriginalSSN").ToString
                        drChild("Campus") = dr("CampDescrip").ToString
                        'drChild("DisbDate") = FormatDate(drC("DisbDate").ToString)
                        drChild("DisbDate") = drC("DisbDate").ToString
                        Try
                            dbValue = Convert.ToDouble(drC("AccDisbAmount").ToString)
                        Catch ex As Exception
                            dbValue = 0.0
                        End Try
                        drChild("AccDisbAmtDisbGrossAmt") = dbValue.ToString("0.00")
                        Try
                            dbValue = Convert.ToDouble(drC("SimittedDisbAmount").ToString)
                        Catch ex As Exception
                            dbValue = 0.0
                        End Try
                        drChild("SubDisbAmtDisbNetAmt") = dbValue.ToString("0.00")
                        drChild("FileName") = drC("FileName").ToString
                        drChild("Message") = drC("ErrorMsg").ToString
                        drChild("ModDate") = drC("ModDate").ToString
                        dtChild.Rows.Add(drChild)
                    Next
                Next
                dsR.Tables.Add(dtParent)
                dsR.Tables.Add(dtChild)
                Return dsR
            Catch ex As Exception
                Return dsR
            End Try
        Else
            Try
                With sbException
                    .Append(" Select PT.ExceptionReportId,PT.DbIn, PT.OriginalSSN ,PT.LoanAmtApproved,PT.LoanId,PT.LoanType,PT.FileName,PT.ErrorMsg,PT.ModDate, ")
                    .Append(" (SELECT TOP 1 C.CampDescrip FROM arStuEnrollments SE INNER JOIN arStudent S ON SE.StudentId = S.StudentId INNER JOIN syCampuses C ON SE.CampusId=C.CampusId WHERE PT.OriginalSSN=S.SSN  order by EnrollDate Desc ) AS CampDescrip ")
                    .Append(" From syEDExpressExceptionReportDirectLoan_StudentTable PT Where PT.ExceptionGUID='" & ExceptionGUID & "' Order By PT.OriginalSSN")
                End With
                ds = db.RunParamSQLDataSet(sbException.ToString)
                For Each dr As DataRow In ds.Tables(0).Rows
                    drParent = dtParent.NewRow()
                    drParent("SSN") = dr("OriginalSSN").ToString
                    Try
                        dbValue = Convert.ToDouble(dr("LoanAmtApproved").ToString)
                    Catch ex As Exception
                        dbValue = 0.0
                    End Try
                    drParent("Amount") = dbValue.ToString("0.00")
                    drParent("Campus") = dr("CampDescrip").ToString
                    drParent("AwardIdLoanId") = dr("LoanId").ToString
                    drParent("GrantTypeLoanType") = IIf(dr("LoanType").ToString = "P", "DL - PLUS", IIf(dr("LoanType").ToString = "S", "DL - SUB", IIf(dr("LoanType").ToString = "U", "DL - UNSUB", "")))
                    drParent("FileName") = dr("FileName").ToString
                    drParent("Message") = dr("ErrorMsg").ToString
                    drParent("ModDate") = dr("ModDate").ToString
                    dtParent.Rows.Add(drParent)
                    sbException = sbException.Remove(0, sbException.Length)
                    With sbException
                        .Append(" Select CT.DbIn, CT.OriginalSSN ,CT.DisbDate,CT.DisbGrossAmount,CT.DisbNetAmount,CT.FileName,CT.ErrorMsg,CT.ModDate, ")
                        .Append(" (SELECT TOP 1 C.CampDescrip FROM arStuEnrollments SE INNER JOIN arStudent S ON SE.StudentId = S.StudentId INNER JOIN syCampuses C ON SE.CampusId=C.CampusId WHERE CT.OriginalSSN=S.SSN  order by EnrollDate Desc ) AS CampDescrip ")
                        .Append(" From syEDExpressExceptionReportDirectLoan_DisbursementTable CT Where CT.ExceptionReportId='" & dr("ExceptionReportId").ToString & "' Order By CT.OriginalSSN")
                    End With
                    dsTemp = db.RunParamSQLDataSet(sbException.ToString)
                    For Each drC As DataRow In dsTemp.Tables(0).Rows
                        drChild = dtChild.NewRow()
                        drChild("SSN") = dr("OriginalSSN").ToString
                        drChild("Campus") = dr("CampDescrip").ToString
                        'drChild("DisbDate") = FormatDate(drC("DisbDate").ToString)
                        drChild("DisbDate") = drC("DisbDate").ToString
                        Try
                            dbValue = Convert.ToDouble(drC("DisbGrossAmount").ToString)
                        Catch ex As Exception
                            dbValue = 0.0
                        End Try
                        drChild("AccDisbAmtDisbGrossAmt") = dbValue.ToString("0.00")
                        Try
                            dbValue = Convert.ToDouble(drC("DisbNetAmount").ToString)
                        Catch ex As Exception
                            dbValue = 0.0
                        End Try
                        drChild("SubDisbAmtDisbNetAmt") = dbValue.ToString("0.00")
                        drChild("FileName") = drC("FileName").ToString
                        drChild("Message") = drC("ErrorMsg").ToString
                        drChild("ModDate") = drC("ModDate").ToString
                        dtChild.Rows.Add(drChild)
                    Next
                Next
                dsR.Tables.Add(dtParent)
                dsR.Tables.Add(dtChild)
                Return dsR
            Catch ex As Exception
                Return dsR
            End Try
        End If
    End Function
#End Region
End Class
