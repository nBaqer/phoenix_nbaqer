Imports System.Data
Imports System.Data.OleDb
Imports System.Text

Public Class BasisDetailDB
    Public Sub AddBasisDetail(ByVal BasisDetailInfo As BasisDetailInfo)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("INSERT INTO cmBasisDetail")
            .Append("(BasisDetailId, BasisDetailValue, BasisWhenId)")
            .Append(" VALUES (?,?,?)")
        End With
        db.AddParameter("@Param1", BasisDetailInfo.BasisDetailId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param2", BasisDetailInfo.BasisDetailValue, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@Param3", BasisDetailInfo.BasisWhenId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex.InnerException)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Sub

    Public Sub UpdateBasisDetail(ByVal BasisDetailInfo As BasisDetailInfo)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("UPDATE cmBasisDetail SET ")
            .Append("BasisDetailValue = ?, BasisWhenId = ?")
            .Append(" WHERE BasisDetailId = ? ")
        End With
        db.AddParameter("@Param2", BasisDetailInfo.BasisDetailValue, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@Param3", BasisDetailInfo.BasisWhenId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param1", BasisDetailInfo.BasisDetailId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
    End Sub

    Public Sub DeleteBasisDetail(ByVal BasisDetailID As Guid)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("DELETE FROM cmBasisDetail WHERE BasisDetailId = ?")
        End With
        db.AddParameter("@Param1", BasisDetailID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Sub

    Public Function GetDropDownList(ByVal LangId As Int32) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da1 As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder
        With strSQLString
            .Append("SELECT a.BasisID, b.Caption FROM cmBasis a, syTblCaptions b")
            .Append(" WHERE a.TblId = b.TblId AND ")
            .Append(" b.LangId = ?")
        End With
        db.OpenConnection()
        db.AddParameter("@Param1", LangId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        Try
            da1 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da1.Fill(ds, "BasisList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Dim da2 As New OleDbDataAdapter
        strSQLString.Remove(0, strSQLString.Length)
        db.ClearParameters()

        With strSQLString
            .Append("SELECT a.BasisWhenId, a.BasisId, b.Caption FROM cmBasisWhen a, syFldCaptions b ")
            .Append("WHERE a.FldId = b.FldId AND ")
            .Append("b.LangId = ?")
        End With
        db.AddParameter("@Param1", LangId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        Try
            da2 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da2.Fill(ds, "BasisWhenList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)
        db.ClearParameters()
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetDataList(ByVal LangId As Int32) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        Dim ds As New DataSet
        With strSQL
            .Append("SELECT  a.BasisDetailId, a.BasisDetailValue, a.BasisWhenId, b.BasisId, c.Caption, e.Caption FROM cmBasisDetail a, cmBasisWhen b, syFldCaptions c, cmBasis d, syTblCaptions e ")
            .Append("Where a.BasisWhenId = B.BasisWhenId and ")
            .Append("b.FldId = c.FldId and ")
            .Append("c.LangId = ? and ")
            .Append("b.BasisId = d.BasisId and ")
            .Append("d.TblId = e.TblId and ")
            .Append("e.LangId = ?")
        End With
        db.OpenConnection()
        db.AddParameter("@Param1", LangId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        db.AddParameter("@Param2", LangId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        Try
            ds = db.RunParamSQLDataSet(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.CloseConnection()
        db.ClearParameters()
        Return ds
    End Function
End Class
