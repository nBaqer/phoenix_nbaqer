Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFE_AllGObjectDB

	Public Shared Function GetReportDatasetRaw(ByVal RptParamInfo As ReportParamInfoIPEDS) As DataSet

		' get list of students to include, based on report parameters
		Dim StudentList As String = IPEDSDB.GetStudentList(RptParamInfo)

		' if there are no students to include, return empty dataset
		If StudentList = "" Then
			Return New DataSet
		End If

		Dim sb As New System.Text.StringBuilder
		Dim dsRaw As New DataSet
		Dim drRaw As DataRow
		Dim dtAuditHist As DataTable
		Dim EnrollmentList As String, NewValueList As String

		With sb
			' get list of Students
			.Append("SELECT DISTINCT arStudent.StudentId, ")

			' Retrieve Student Identifier
			.Append(IPEDSDB.GetSQL_StudentIdentifier & ", ")

			' retrieve all other needed columns
			.Append("arStudent.LastName, arStudent.FirstName, arStudent.MiddleName FROM arStudent ")

			' append list of appropriate students to include
			.Append("WHERE arStudent.StudentId IN (" & StudentList & ") ")

			' apply sort from report param info - either Student Identifier or Last Name
			.Append("ORDER BY " & IPEDSDB.GetSQL_StudentSort(RptParamInfo))


			.Append(";")


			' get list of Student Enrollments
			.Append("SELECT ")

			' retrieve needed columns
			.Append("arStuEnrollments.StuEnrollId, arStuEnrollments.StudentId, ")
			.Append("arStuEnrollments.EnrollDate, arStuEnrollments.StatusCodeId, ")
			.Append("sySysStatus.SysStatusDescrip, arStuEnrollments.DropReasonId, ")
			.Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
			.Append("	FROM arDropReasons, ")
			.Append("		 syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
			.Append("	WHERE arDropReasons.DropReasonId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("	      syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("	      syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("	      syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("	      syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
			.Append("	      arDropReasons.DropReasonId = arStuEnrollments.DropReasonId) AS DropReasonDescrip, ")
			.Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
			.Append("	FROM arAttendTypes, ")
			.Append("		 syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
			.Append("	WHERE arAttendTypes.AttendTypeId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("	      syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("	      syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("	      syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("	      syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
			.Append("	      arAttendTypes.AttendTypeId = arStuEnrollments.AttendTypeId) AS AttendTypeDescrip ")
			.Append("FROM ")
            .Append("arStuEnrollments, arPrgVersions, arPrograms, syStatusCodes, sySysStatus ")

			' establish necessary relationships
			.Append("WHERE ")
			.Append("arStuEnrollments.StatusCodeId = syStatusCodes.StatusCodeId AND ")
            .Append("syStatusCodes.SysStatusId = sySysStatus.SysStatusId AND ")
            .Append("arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId AND ")
            .Append("arPrgVersions.ProgId = arPrograms.ProgId AND ")

            ' filter on passed-in list of programs as selected by user
            .Append("arPrograms.ProgId IN (" & RptParamInfo.FilterProgramIDs & ") AND ")

			' append list of appropriate students to include
			.Append("arStuEnrollments.StudentId IN (" & StudentList & ") ")
		End With

		' run query, add returned data to raw dataset
		dsRaw = IPEDSDB.DataAccessIPEDS().RunParamSQLDataSet(sb.ToString)

		' set table names for each part of returned data
		With dsRaw
			.Tables(0).TableName = TblNameStudents
			.Tables(1).TableName = TblNameEnrollments
		End With



		' Get needed Student-related audit history records and add to raw dataset
		' Restrict audit history records to events which:
		'	- pertain to table arStudents, and
		'	- where either an insert or an update, and
		'	- pertain to data relating to the list of students included this report
		dtAuditHist = _
			IPEDSDB.GetAuditHistRecords("arStudent", _
										AuditHistEvents.InsertOrUpdate, _
										, _
										StudentList)
		dtAuditHist.TableName = "AuditHistStudents"
		dsRaw.Tables.Add(dtAuditHist.Copy)



		' Get needed Enrollment-related audit history records and add to raw dataset
		' Restrict audit history records to events which:
		'	- pertain to table arStuEnrollments, and
		'	- where either an insert or an update, and
		'	- pertain to data relating to the list of student enrollments used by this
		'	  report, and
		'	- pertain to column arStuEnrollments.StatusCodeId or arStuEnrollments.AttendTypeId

		' build list of EnrollmentIds to use when getting audit history records
		With sb
			.Remove(0, .Length)
			For Each drRaw In dsRaw.Tables(TblNameEnrollments).Rows
				.Append(drRaw("StuEnrollId").ToString & ",")
			Next
		End With
		EnrollmentList = sb.ToString.TrimEnd(",".ToCharArray)

		' get list of StatusCodeIds which map to "Graduated" in sySysStatus, to use
		'	when getting audit history records
		NewValueList = GetIdsGraduated()

		' get data using lists from above, and add to raw dataset
		dtAuditHist = _
			IPEDSDB.GetAuditHistRecords("arStuEnrollments", _
										AuditHistEvents.InsertOrUpdate, _
										, _
										EnrollmentList, _
										"StatusCodeId, AttendTypeId")
		dtAuditHist.TableName = "AuditHistEnrollments"
		dsRaw.Tables.Add(dtAuditHist.Copy)


		' return raw dataset
		Return dsRaw

	End Function

	Public Shared Function GetIdsGraduated() As String
	' Builds list of StatusCodeIds which map to "Graduated" in sySysStatus

		Dim sb As New System.Text.StringBuilder
		Dim dtIds As DataTable
		Dim drIds As DataRow

		' Build SQL query
		With sb
			.Append("SELECT ")
			.Append("syStatusCodes.StatusCodeId ")
			.Append("FROM ")
			.Append("syStatusCodes, sySysStatus ")
			.Append("WHERE ")
			.Append("syStatusCodes.SysStatusId = sySysStatus.SysStatusId AND ")
			.Append("sySysStatus.SysStatusDescrip LIKE 'Graduated'")
		End With

		' run query, save data to DataTable
		dtIds = IPEDSDB.DataAccessIPEDS().RunSQLDataSet(sb.ToString).Tables(0).Copy

		' build and return list of StatusCodeIds 
		With sb
			.Remove(0, .Length)
			For Each drIds In dtIds.Rows
				.Append(drIds("StatusCodeId").ToString & ",")
			Next
		End With

		Return sb.ToString.TrimEnd(",".ToCharArray)	' get rid of extra "," at end
	End Function

	Public Shared Function GetIdsDropped() As String
	' Builds list of StatusCodeIds which map to "Dropped" in sySysStatus

		Dim sb As New System.Text.StringBuilder
		Dim dtIds As DataTable
		Dim drIds As DataRow

		' Build SQL query
		With sb
			.Append("SELECT ")
			.Append("syStatusCodes.StatusCodeId ")
			.Append("FROM ")
			.Append("syStatusCodes, sySysStatus ")
			.Append("WHERE ")
			.Append("syStatusCodes.SysStatusId = sySysStatus.SysStatusId AND ")
			.Append("sySysStatus.SysStatusDescrip LIKE 'Dropped'")
		End With

		' run query, save data to DataTable
		dtIds = IPEDSDB.DataAccessIPEDS().RunSQLDataSet(sb.ToString).Tables(0).Copy

		' build and return list of StatusCodeIds 
		With sb
			.Remove(0, .Length)
			For Each drIds In dtIds.Rows
				.Append(drIds("StatusCodeId").ToString & ",")
			Next
		End With

		Return sb.ToString.TrimEnd(",".ToCharArray)	' get rid of extra "," at end
	End Function

	Public Shared Function GetIdsFullTime() As String
	' Builds list of Attendance Type Ids which map to "Full Time" in IPEDS

		Dim sb As New System.Text.StringBuilder
		Dim dtIds As DataTable
		Dim drIds As DataRow

		' Build SQL query
		With sb
			.Append("SELECT ")
			.Append("arAttendTypes.AttendTypeId ")
			.Append("FROM ")
			.Append("arAttendTypes, ")
			.Append("syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
			.Append("WHERE ")
			.Append("arAttendTypes.AttendTypeId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
			.Append("syRptAgencyFldValues.AgencyDescrip LIKE 'Full Time'")
		End With

		' run query, save data to DataTable
		dtIds = IPEDSDB.DataAccessIPEDS().RunSQLDataSet(sb.ToString).Tables(0).Copy

		' build and return list of Attendance Type ids
		With sb
			.Remove(0, .Length)
			For Each drIds In dtIds.Rows
				.Append(drIds("AttendTypeId").ToString & ",")
			Next
		End With

		Return sb.ToString.TrimEnd(",".ToCharArray)	' get rid of extra "," at end
	End Function

	Public Shared Function GetIdsPartTime() As String
	' Builds list of Attendance Type Ids which map to "Part Time" in IPEDS

		Dim sb As New System.Text.StringBuilder
		Dim dtIds As DataTable
		Dim drIds As DataRow

		' Build SQL query
		With sb
			.Append("SELECT ")
			.Append("arAttendTypes.AttendTypeId ")
			.Append("FROM ")
			.Append("arAttendTypes, ")
			.Append("syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
			.Append("WHERE ")
			.Append("arAttendTypes.AttendTypeId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
			.Append("syRptAgencyFldValues.AgencyDescrip LIKE 'Part Time'")
		End With

		' run query, save data to DataTable
		dtIds = IPEDSDB.DataAccessIPEDS().RunSQLDataSet(sb.ToString).Tables(0).Copy

		' build and return list of Attendance Type ids
		With sb
			.Remove(0, .Length)
			For Each drIds In dtIds.Rows
				.Append(drIds("AttendTypeId").ToString & ",")
			Next
		End With

		Return sb.ToString.TrimEnd(",".ToCharArray)	' get rid of extra "," at end
	End Function

End Class
