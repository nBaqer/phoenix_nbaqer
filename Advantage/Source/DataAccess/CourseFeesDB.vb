' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' CourseFeesDB.vb
'
' CourseFeesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class CourseFeesDB
    Public Function GetAllCourseFees(ByVal showActiveOnly As Boolean) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   CF.CourseFeeId, ")
            .Append("         CF.StatusId, ")
            .Append("         CF.CourseId, ")
            .Append("         CF.TransCodeId, ")
            .Append("         CF.TuitionCategoryId, ")
            .Append("         CF.Amount, ")
            .Append("         CF.RateScheduleId, ")
            .Append("         CF.UnitId, ")
            .Append("         CF.StartDate ")
            .Append("FROM     saCourseFees CF, syStatuses ST ")
            .Append("WHERE    CF.StatusId = ST.StatusId ")
            If showActiveOnly Then
                .Append(" AND     ST.Status = 'Active' ")
            End If
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetCourseFeeInfo(ByVal CourseFeeId As String) As CourseFeeInfo

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT CF.CourseFeeId, ")
            .Append("    CF.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=CF.StatusId) Status, ")
            .Append("    CF.CourseId, ")
            .Append("    (Select Descrip from arReqs where ReqId=CF.CourseId) Course, ")
            .Append("    CF.TransCodeId, ")
            .Append("    (Select TransCodeDescrip from saTransCodes where TransCodeId=CF.TransCodeId) TransCode, ")
            .Append("    CF.TuitionCategoryId, ")
            .Append("    (Select TuitionCategoryDescrip from saTuitionCategories where TuitionCategoryId=CF.TuitionCategoryId) TuitionCategory, ")
            .Append("    CF.Amount, ")
            .Append("    CF.RateScheduleId, ")
            .Append("    (Select RateScheduleDescrip from saRateSchedules where RateScheduleId=CF.RateScheduleId) RateSchedule, ")
            .Append("    CF.UnitId, ")
            .Append("    CF.StartDate, ")
            .Append("    CF.ModUser, ")
            .Append("    CF.ModDate ")
            .Append("FROM  saCourseFees CF ")
            .Append("WHERE CF.CourseFeeId= ? ")
        End With

        ' Add the CourseFeeId to the parameter list
        db.AddParameter("@CourseFeeId", CourseFeeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim CourseFeeInfo As New CourseFeeInfo

        While dr.Read()

            '   set properties with data from DataReader
            With CourseFeeInfo
                .CourseFeeId = CourseFeeId
                .IsInDB = True
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                If Not (dr("CourseId") Is System.DBNull.Value) Then .CourseId = CType(dr("CourseId"), Guid).ToString
                If Not (dr("Course") Is System.DBNull.Value) Then .Course = dr("Course")
                If Not (dr("TransCodeId") Is System.DBNull.Value) Then .TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                If Not (dr("TransCode") Is System.DBNull.Value) Then .TransCode = dr("TransCode")
                If Not (dr("TuitionCategoryId") Is System.DBNull.Value) Then .TuitionCategoryId = CType(dr("TuitionCategoryId"), Guid).ToString
                If Not (dr("TuitionCategory") Is System.DBNull.Value) Then .TuitionCategory = dr("TuitionCategory")
                .Amount = dr("Amount")
                If Not (dr("RateScheduleId") Is System.DBNull.Value) Then .RateScheduleId = CType(dr("RateScheduleId"), Guid).ToString
                If Not (dr("RateSchedule") Is System.DBNull.Value) Then .rateSchedule = dr("RateSchedule")
                If Not (dr("StartDate") Is System.DBNull.Value) Then .StartDate = dr("StartDate")
                .UnitId = CType(dr("UnitId"), UnitType)
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return CourseFeeInfo
        Return CourseFeeInfo

    End Function
    Public Function UpdateCourseFeeInfo(ByVal CourseFeeInfo As CourseFeeInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saCourseFees Set CourseFeeId = ?, ")
                .Append(" StatusId = ?, ")
                .Append(" CourseId = ?, TransCodeId = ?, TuitionCategoryId = ?, ")
                .Append(" Amount = ?, RateScheduleId = ?, UnitId = ?, ")
                .Append(" StartDate = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE CourseFeeId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saCourseFees where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   CourseFeeId
            db.AddParameter("@CourseFeeId", CourseFeeInfo.CourseFeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", CourseFeeInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CourseId
            If CourseFeeInfo.CourseId = Guid.Empty.ToString Then
                db.AddParameter("@CourseId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CourseId", CourseFeeInfo.CourseId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   TransCodeId
            If CourseFeeInfo.TransCodeId = Guid.Empty.ToString Then
                db.AddParameter("@TransCodeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@TransCodeId", CourseFeeInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   TuitionCategoryId
            If CourseFeeInfo.TuitionCategoryId = Guid.Empty.ToString Then
                db.AddParameter("@TuitionCategoryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@TuitionCategoryId", CourseFeeInfo.TuitionCategoryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Amount
            db.AddParameter("@Amount", CourseFeeInfo.Amount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            '   RateScheduleId
            If CourseFeeInfo.RateScheduleId = Guid.Empty.ToString Then
                db.AddParameter("@RateScheduleId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@RateScheduleId", CourseFeeInfo.RateScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Unit
            db.AddParameter("@UnitId", CourseFeeInfo.UnitId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   Startdate
            db.AddParameter("@StartDate", CourseFeeInfo.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   CourseFeeId
            db.AddParameter("@AdmDepositId", CourseFeeInfo.CourseFeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", CourseFeeInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddCourseFeeInfo(ByVal CourseFeeInfo As CourseFeeInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saCourseFees (CourseFeeId, StatusId, ")
                .Append("   CourseId, TransCodeId, TuitionCategoryId, ")
                .Append("   Amount, RateScheduleId, UnitId, ")
                .Append("   StartDate, ")
                .Append("   ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   CourseFeeId
            db.AddParameter("@CourseFeeId", CourseFeeInfo.CourseFeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", CourseFeeInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CourseId
            If CourseFeeInfo.CourseId = Guid.Empty.ToString Then
                db.AddParameter("@CourseId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CourseId", CourseFeeInfo.CourseId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   TransCodeId
            If CourseFeeInfo.TransCodeId = Guid.Empty.ToString Then
                db.AddParameter("@TransCodeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@TransCodeId", CourseFeeInfo.TransCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   TuitionCategoryId
            If CourseFeeInfo.TuitionCategoryId = Guid.Empty.ToString Then
                db.AddParameter("@TuitionCategoryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@TuitionCategoryId", CourseFeeInfo.TuitionCategoryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Amount
            db.AddParameter("@Amount", CourseFeeInfo.Amount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            '   RateScheduleId
            If CourseFeeInfo.RateScheduleId = Guid.Empty.ToString Then
                db.AddParameter("@RateScheduleId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@RateScheduleId", CourseFeeInfo.RateScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Unit
            db.AddParameter("@UnitId", CourseFeeInfo.UnitId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   Startdate
            db.AddParameter("@StartDate", CourseFeeInfo.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteCourseFeeInfo(ByVal CourseFeeId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saCourseFees ")
                .Append("WHERE CourseFeeId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM saCourseFees WHERE CourseFeeId = ? ")
            End With

            '   add parameters values to the query

            '   CourseFeeId
            db.AddParameter("@CourseFeeId", CourseFeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   CourseFeeId
            db.AddParameter("@CourseFeeId", CourseFeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetBillingCodesNotAssgd(ByVal CourseID As String) As DataSet
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim sb As New System.Text.StringBuilder


        'Set the connection string
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT t1.TransCodeId,t1.TransCodeDescrip ")
            .Append("FROM saTransCodes t1 ")
            .Append("WHERE NOT EXISTS ")
            .Append("(SELECT t2.TransCodeId FROM saCourseFees t2 where t2.CourseId = ? and t2.TransCodeId = t1.TransCodeId)")
            .Append("ORDER BY t1.TransCodeDescrip")
        End With
        ' Add the CourseId and ChildId to the parameter list
        db.AddParameter("@courseId", CourseID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds
    End Function
    Public Function GetCourseFeeInfoTable(ByVal CourseID As String) As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim sb As New System.Text.StringBuilder

        'Set the connection string
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT t1.TransCodeId,Amount,Seq ")
            .Append("FROM saCourseFees t1 ")
            .Append("WHERE t1.CourseId = ? ")
        End With
        ' Add the CourseId to the parameter list
        db.AddParameter("@CourseId", CourseID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the datatable
        Return ds.Tables(0)
    End Function

    Public Function GetBillingCodeInfo() As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim da As OleDbDataAdapter

     
        Try
            'Set the connection string
            db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

            'Build query to obtain coursegrpids and coursegrpdescrips from the arCourseGrps table.
            With sb
                .Append("SELECT t1.TransCodeId,TransCodeDescrip ")
                .Append("FROM saTransCodes t1 ")
            End With

            '   Execute the query
            ds = db.RunSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable
        Return ds.Tables(0)
    End Function

    Public Function LoadDS(ByVal CourseID As String) As DataSet

        Dim LoadedDS As New DataSet
        Dim dt1 As New DataTable
        Dim dt2 As New DataTable
        Dim dt3 As New DataTable
        Dim row As DataRow
        Dim isreq As Boolean

        dt1 = GetCourseFeeInfoTable(CourseID).Copy
        dt1.TableName = "CourseFeeInfo"
        LoadedDS.Tables.Add(dt1)


        dt2 = GetBillingCodeInfo().Copy
        dt2.TableName = "BillingCodes"
        LoadedDS.Tables.Add(dt2)


        'return dataset to facade
        Return LoadedDS

    End Function

    Public Function InsertCourseFeeToDB(ByVal CourseFeeObj As CourseFeeInfo, ByVal CourseID As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim TransCodeID As String
        Dim Amount As Decimal
        Dim Seq As Integer

      

        TransCodeID = CourseFeeObj.TransCodeId
        Amount = CourseFeeObj.Amount
        'Seq = CourseFeeObj.Seq

        'Set the connection string
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("INSERT INTO saCourseFees(CourseId,TransCodeId,Amount,Seq) ")
            .Append("VALUES(?,?,?,?)")

            db.AddParameter("@courseid", CourseID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@transcodeid", TransCodeID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@amount", Amount, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
            db.AddParameter("@seq", Seq, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        End With

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function

    Public Function UpdateCourseFeeInDB(ByVal CourseFeeObj As CourseFeeInfo, ByVal CourseID As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim childID As String
        Dim childTyp As String
        Dim childSeq As Integer
        Dim req As Integer

    
        'Set the connection string
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")


        With sb
            .Append("UPDATE saCourseFees Set Seq = ?, Amount = ? ")
            .Append("WHERE  TransCodeId = ? and CourseId = ?")

            'db.AddParameter("@seq", CourseFeeObj.Seq, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@amount", CourseFeeObj.Amount, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
            db.AddParameter("@transcodeid", CourseFeeObj.TransCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@courseid", CourseID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        End With

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function

    Public Function DeleteCourseFeeFrmDB(ByVal CourseFeeObj As CourseFeeInfo, ByVal CourseID As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet


        'Set the connection string
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("DELETE FROM saCourseFees ")
            .Append("WHERE CourseId = ? ")
            .Append("AND TransCodeId = ? ")
        End With

        db.AddParameter("@courseid", CourseID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@transcodeid", CourseFeeObj.TransCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function
    Public Function GetTransCodesByCourse(ByVal courseId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

     
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   CF.CourseFeeId, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("         CF.CourseId, ")
            .Append("         (Select Descrip from arReqs where ReqId=CF.CourseId) As Course, ")
            .Append("         CF.TransCodeId, ")
            .Append("         (Select TransCodeDescrip from saTransCodes where TransCodeId=CF.TransCodeId) As TransCodeDescrip, ")
            .Append("         CF.TuitionCategoryId, ")
            .Append("         CF.Amount, ")
            .Append("         CF.RateScheduleId, ")
            .Append("         CF.UnitId, ")
            .Append("         CF.StartDate ")
            .Append("FROM     saCourseFees CF, syStatuses ST ")
            .Append("WHERE    CF.StatusId=ST.StatusId ")
            .Append("AND      CF.CourseId = ? ")
            .Append("ORDER BY TransCodeDescrip, StartDate ")
        End With

        ' Add the courseId to the parameter list
        db.AddParameter("@CourseId", courseId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
#Region "Get AdvAppsetting for Manage Config entry"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
End Class
