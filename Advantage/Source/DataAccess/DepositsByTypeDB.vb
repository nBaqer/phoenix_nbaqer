Imports FAME.Advantage.Common

Public Class DepositsByTypeDB


#Region "Private Data Members"
    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")

#End Region


#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

#End Region


#Region "Public Methods"

    Public Function GetDepositsByType(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String = ""
        Dim strWhere1 As String = ""
        Dim strOrderBy As String = ""
        Dim strFrom As String = ""
        Dim strFrom1 As String = ""
        Dim strStudentId As String = ""
        Dim strFundSourceSelected As Boolean

        strFrom = "FROM saTransactions,arStuEnrollments B,arStudent,saPayments,syCmpGrpCmps E,syCampuses F,syCampGrps"

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
            'If paramInfo.FilterList.IndexOf("FundSourceId") > -1 Then
            If strWhere.IndexOf("FundSourceId") > -1 Then
                strFundSourceSelected = True
                strFrom1 &= ",faStudentAwards,saFundSources,faStudentAwardSchedule SAS,saPmtDisbRel PDR "
                strWhere1 &= " AND PDR.TransactionId=saTransactions.TransactionId " & _
                            " AND saTransactions.Voided=0 " & _
                            "AND PDR.AwardScheduleId=SAS.AwardScheduleId " & _
                            "AND SAS.StudentAwardId=faStudentAwards.StudentAwardId " & _
                            "AND faStudentAwards.AwardTypeId=saFundSources.FundSourceId "
            Else
                strFrom1 &= " "
                strWhere1 &= " "
                strFundSourceSelected = False
            End If
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        If StudentIdentifier = "SSN" Then
            strStudentId = "arStudent.SSN AS StudentIdentifier,"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStudentId = "B.EnrollmentId AS StudentIdentifier,"
        ElseIf StudentIdentifier = "StudentId" Then
            strStudentId = "arStudent.StudentNumber AS StudentIdentifier,"
        End If

        With sb
            .Append(" SELECT  ")
            .Append(" saTransactions.StuEnrollId, arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
            .Append(strStudentId)
            If strWhere.IndexOf("FundSourceId") > -1 Then
                .Append("   saTransactions.TransDescrip,PDR.Amount AS TransAmount,saTransactions.TransDate,")
            Else
                .Append("   saTransactions.TransDescrip,saTransactions.TransAmount* (-1) AS TransAmount,saTransactions.TransDate,")
            End If
            .Append("   (SELECT AcademicYearDescrip FROM saAcademicYears WHERE saTransactions.AcademicYearId=AcademicYearId) AS AcademicYearDescrip,")
            .Append("   saPayments.PaymentTypeId,")
            .Append("   CASE saPayments.PaymentTypeId ")
            .Append("       WHEN 1 THEN 'Cash' ")
            .Append("       WHEN 2 THEN 'Check' ")
            .Append("       WHEN 3 THEN 'Credit Card' ")
            .Append("       WHEN 4 THEN 'EFT' ")
            .Append("       WHEN 5 THEN 'Money Order' ")
            .Append("       WHEN 6 THEN 'Non_Cash' ")
            .Append("       ELSE 'Unknown' ")
            .Append("   END AS PaymentTypeDescrip, ")
            .Append("   saPayments.CheckNumber,")
            .Append("   (SELECT TOP 1 FS.FundSourceDescrip ")
            .Append("       FROM saPmtDisbRel PDR,faStudentAwards SA,faStudentAwardSchedule SAS,saFundSources FS ")
            .Append("       WHERE PDR.TransactionId=saTransactions.TransactionId ")
            .Append("       AND PDR.AwardScheduleId=SAS.AwardScheduleId ")
            .Append("       AND SAS.StudentAwardId=SA.StudentAwardId ")
            .Append("       AND FS.FundSourceId=SA.AwardTypeId) AS AwardTypeDescrip,")
            .Append("   (SELECT TOP 1 AY.AcademicYearDescrip ")
            .Append("       FROM saPmtDisbRel PDR,faStudentAwards SA,faStudentAwardSchedule SAS,saAcademicYears AY ")
            .Append("       WHERE PDR.TransactionId=saTransactions.TransactionId ")
            .Append("       AND PDR.AwardScheduleId=SAS.AwardScheduleId ")
            .Append("       AND SAS.StudentAwardId=SA.StudentAwardId ")
            .Append("       AND AY.AcademicYearId=SA.AcademicYearId) AS AwardYearDescrip,")
            .Append("   syCampGrps.CampGrpDescrip,F.CampDescrip,saTransactions.TransCodeId,saTransactions.TransactionId ")
            .Append(strFrom)
            .Append(strFrom1)
            .Append(" WHERE saTransactions.StuEnrollId=B.StuEnrollId AND B.StudentId=arStudent.StudentId ")
            .Append("   AND saTransactions.TransactionId=saPayments.TransactionId AND saTransactions.IsPosted=1 ")
            .Append("   AND B.CampusId=F.CampusId AND F.CampusId=E.CampusId AND E.CampGrpId=syCampGrps.CampGrpId ")
            .Append("   AND saTransactions.Voided=0 ")
            .Append(strWhere)
            .Append(strWhere1)
            .Append(" UNION ")
            .Append("SELECT ")
            .Append("   saTransactions.StuEnrollId, arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
            .Append(strStudentId)
            If strWhere.IndexOf("FundSourceId") > -1 Then
                .Append("   saTransactions.TransDescrip,saTransactions.TransAmount AS TransAmount,saTransactions.TransDate,")
            Else
                .Append("   saTransactions.TransDescrip,saTransactions.TransAmount* (-1) AS TransAmount,saTransactions.TransDate,")
            End If
            .Append("   (SELECT AcademicYearDescrip FROM saAcademicYears WHERE saTransactions.AcademicYearId=AcademicYearId) AS AcademicYearDescrip,")
            .Append("   saPayments.PaymentTypeId,")
            .Append("   CASE saPayments.PaymentTypeId ")
            .Append("       WHEN 1 THEN 'Cash' ")
            .Append("       WHEN 2 THEN 'Check' ")
            .Append("       WHEN 3 THEN 'Credit Card' ")
            .Append("       WHEN 4 THEN 'EFT' ")
            .Append("       WHEN 5 THEN 'Money Order' ")
            .Append("       WHEN 6 THEN 'Non_Cash' ")
            .Append("       ELSE 'Unknown' ")
            .Append("   END AS PaymentTypeDescrip, ")
            .Append("   saPayments.CheckNumber,")
            .Append("   NULL AS AwardTypeDescrip ,	 ")
            .Append("   NULL AS AwardYearDescrip ,	 ")
            .Append("   syCampGrps.CampGrpDescrip,F.CampDescrip,saTransactions.TransCodeId,saTransactions.TransactionId  ")
            .Append(strFrom)
            .Append(" ,faStudentPaymentPlans PP ,saFundSources ,	saPmtDisbRel PDR, faStuPaymentPlanSchedule PPS  ")
            .Append("WHERE saTransactions.StuEnrollId=B.StuEnrollId AND B.StudentId=arStudent.StudentId ")
            .Append("   AND saTransactions.TransactionId=saPayments.TransactionId AND saTransactions.IsPosted=1 ")
            .Append("   AND B.CampusId=F.CampusId AND F.CampusId=E.CampusId AND E.CampGrpId=syCampGrps.CampGrpId ")
            .Append("   AND saTransactions.Voided=0 ")
            .Append(strWhere)
            .Append(" AND PDR.TransactionId = saTransactions.TransactionId ")
            .Append(" AND B.StuEnrollId = PP.StuEnrollId ")
            .Append(" AND PPS.PaymentPlanId=PP.PaymentPlanId ")
            .Append(" AND PDR.PayPlanScheduleId=PPS.PayPlanScheduleId ")
            .Append(" AND saTransactions.FundSourceId=saFundSources.FundSourceId ")
            .Append(" UNION ")
            .Append("SELECT ")
            .Append("   saTransactions.StuEnrollId, arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
            .Append(strStudentId)
            If strWhere.IndexOf("FundSourceId") > -1 Then
                .Append("   saTransactions.TransDescrip,saTransactions.TransAmount AS TransAmount,saTransactions.TransDate,")
            Else
                .Append("   saTransactions.TransDescrip,saTransactions.TransAmount* (-1) AS TransAmount,saTransactions.TransDate,")
            End If
            .Append("   (SELECT AcademicYearDescrip FROM saAcademicYears WHERE saTransactions.AcademicYearId=AcademicYearId) AS AcademicYearDescrip,")
            .Append("   saPayments.PaymentTypeId,")
            .Append("   CASE saPayments.PaymentTypeId ")
            .Append("       WHEN 1 THEN 'Cash' ")
            .Append("       WHEN 2 THEN 'Check' ")
            .Append("       WHEN 3 THEN 'Credit Card' ")
            .Append("       WHEN 4 THEN 'EFT' ")
            .Append("       WHEN 5 THEN 'Money Order' ")
            .Append("       WHEN 6 THEN 'Non_Cash' ")
            .Append("       ELSE 'Unknown' ")
            .Append("   END AS PaymentTypeDescrip, ")
            .Append("   saPayments.CheckNumber,")
            .Append("   NULL AS AwardTypeDescrip ,	 ")
            .Append("   NULL AS AwardYearDescrip ,	 ")
            .Append("   syCampGrps.CampGrpDescrip,F.CampDescrip,saTransactions.TransCodeId,saTransactions.TransactionId  ")
            .Append(strFrom)
            .Append(" ,saFundSources ")
            .Append("WHERE saTransactions.StuEnrollId=B.StuEnrollId AND B.StudentId=arStudent.StudentId ")
            .Append("   AND saTransactions.TransactionId=saPayments.TransactionId AND saTransactions.IsPosted=1 ")
            .Append("   AND B.CampusId=F.CampusId AND F.CampusId=E.CampusId AND E.CampGrpId=syCampGrps.CampGrpId ")
            .Append("   AND saTransactions.Voided=0 ")
            .Append(strWhere)
            .Append(" AND saTransactions.FundSourceId=saFundSources.FundSourceId  ")
            .Append(" AND NOT EXISTS(SELECT * FROM saPmtDisbRel PDR	WHERE PDR.TransactionId=saTransactions.TransactionId ) ")
            '.Append(" UNION ")
            '.Append("SELECT ")
            '.Append("   saTransactions.StuEnrollId, arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
            '.Append(strStudentId)
            'If strWhere.IndexOf("FundSourceId") > -1 Then
            '    .Append("   saTransactions.TransDescrip,saTransactions.TransAmount AS TransAmount,saTransactions.TransDate,")
            'Else
            '    .Append("   saTransactions.TransDescrip,saTransactions.TransAmount* (-1) AS TransAmount,saTransactions.TransDate,")
            'End If
            '.Append("   (SELECT AcademicYearDescrip FROM saAcademicYears WHERE saTransactions.AcademicYearId=AcademicYearId) AS AcademicYearDescrip,")
            '.Append("   saPayments.PaymentTypeId,")
            '.Append("   CASE saPayments.PaymentTypeId ")
            '.Append("       WHEN 1 THEN 'Cash' ")
            '.Append("       WHEN 2 THEN 'Check' ")
            '.Append("       WHEN 3 THEN 'Credit Card' ")
            '.Append("       WHEN 4 THEN 'EFT' ")
            '.Append("       WHEN 5 THEN 'Money Order' ")
            '.Append("       WHEN 6 THEN 'Non_Cash' ")
            '.Append("       ELSE 'Unknown' ")
            '.Append("   END AS PaymentTypeDescrip, ")
            '.Append("   saPayments.CheckNumber,")
            '.Append("   NULL AS AwardTypeDescrip ,	 ")
            '.Append("   NULL AS AwardYearDescrip ,	 ")
            '.Append("   syCampGrps.CampGrpDescrip,F.CampDescrip ")
            '.Append(strFrom)
            '.Append(" WHERE saTransactions.StuEnrollId=B.StuEnrollId AND B.StudentId=arStudent.StudentId ")
            '.Append("   AND saTransactions.TransactionId=saPayments.TransactionId AND saTransactions.IsPosted=1 ")
            '.Append("   AND B.CampusId=F.CampusId AND F.CampusId=E.CampusId AND E.CampGrpId=syCampGrps.CampGrpId ")
            '.Append("   AND saTransactions.Voided=0 ")
            '.Append(strWhere)
            '.Append(" AND saTransactions.FundSourceId Is NULL  ")
            '.Append(" ORDER BY CampGrpDescrip,CampDescrip,saPayments.PaymentTypeId,PaymentTypeDescrip,LastName,FirstName")
            .Append(" ORDER BY CampGrpDescrip,CampDescrip,saPayments.PaymentTypeId,PaymentTypeDescrip")
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "DepositsByType"
            'Add new columns
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("PaymentNumber", System.Type.GetType("System.String")))
            '
            'Create table to store Total Amounts per Campus Groups and Campuses.
            Dim dt As New DataTable("TotalAmounts")
            dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampGrpTotal", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CampusTotal", System.Type.GetType("System.Decimal")))
            ds.Tables.Add(dt)
            '
            'Get tables with details and summary by deposit types and fund sources
            Dim ds2 As DataSet = GetFundTypeSummary(strWhere, strFundSourceSelected)
            If ds2.Tables.Count > 0 Then
                ds.Tables.Add(ds2.Tables(0).Copy)
            End If
            'If ds2.Tables.Count > 1 Then
            '    Dim dt2 As DataTable = ds2.Tables(0).Copy
            '    Dim dt3 As DataTable = ds2.Tables(1).Copy
            '    ds.Tables.Add(dt2)
            '    ds.Tables.Add(dt3)
            'End If
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Public Function GetFundTypeSummary(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strFrom As String = ""
        Dim strWhere As String = ""
        Dim strOrderBy As String = ""

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
            If strWhere.IndexOf("FundSourceId") > -1 Then
                strWhere &= " AND PDR.TransactionId=saTransactions.TransactionId " & _
                            " AND saTransactions.Voided=0 " & _
                            "AND PDR.AwardScheduleId=SAS.AwardScheduleId " & _
                            "AND SAS.StudentAwardId=faStudentAwards.StudentAwardId " & _
                            "AND faStudentAwards.AwardTypeId=saFundSources.FundSourceId "
            End If
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If


        strFrom = "FROM saPayments,saTransactions,arStuEnrollments,syCmpGrpCmps,syCampGrps,syCampuses"

        If strWhere.IndexOf("AwardTypeId") > -1 Then
            strFrom &= ",faStudentAwards,saFundSources,faStudentAwardSchedule SAS,saPmtDisbRel PDR "
        Else
            strFrom &= " "
        End If


        With sb
            .Append("SELECT ")
            .Append("       REPLACE(CampGrpDescrip,'''','') as CampGrpDescrip,REPLACE(CampusDescrip,'''','') AS CampusDescrip,PaymentTypeId,PaymentTypeDescrip,AwardTypeDescrip,")
            .Append("       COUNT(*) AS PymtCount,SUM(TransAmount) AS PymtSum ")
            .Append("FROM (")
            .Append("       SELECT ")
            .Append("           syCampGrps.CampGrpDescrip,syCampuses.CampDescrip AS CampusDescrip,")
            .Append("           saPayments.PaymentTypeId,")
            .Append("           CASE saPayments.PaymentTypeId ")
            .Append("               WHEN 1 THEN 'Cash' ")
            .Append("               WHEN 2 THEN 'Check' ")
            .Append("               WHEN 3 THEN 'Credit Card' ")
            .Append("               WHEN 4 THEN 'EFT' ")
            .Append("               WHEN 5 THEN 'Money Order' ")
            .Append("               WHEN 6 THEN 'Non_Cash' ")
            .Append("               ELSE 'Unknown' ")
            .Append("           END AS PaymentTypeDescrip, ")
            '.Append("           (SELECT TOP 1 FS.FundSourceDescrip ")
            '.Append("               FROM saPmtDisbRel PDR, faStudentAwards SA, faStudentAwardSchedule SAS,saFundSources FS ")
            '.Append("               WHERE PDR.TransactionId = saPayments.TransactionId ")
            '.Append("               AND PDR.AwardScheduleId=SAS.AwardScheduleId ")
            '.Append("               AND SAS.StudentAwardId=SA.StudentAwardId ")
            '.Append("               AND FS.FundSourceId=SA.AwardTypeId) AS AwardTypeDescrip,")
            .Append("           (SELECT FS.FundSourceDescrip ")
            .Append("            FROM saFundSources FS ")
            .Append("            WHERE FS.FundSourceId = saTransactions.FundSourceId) AS AwardTypeDescrip, ")
            .Append("           saTransactions.TransAmount * (-1) AS TransAmount ")
            .Append(strFrom)
            .Append("       WHERE saPayments.TransactionId=saTransactions.TransactionId AND saTransactions.IsPosted=1 ")
            .Append("           AND saTransactions.Voided=0 ")
            .Append("           AND saTransactions.StuEnrollId=arStuEnrollments.StuEnrollId ")
            .Append("           AND arStuEnrollments.CampusId=syCmpGrpCmps.CampusId ")
            .Append("           AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
            .Append("           AND syCmpGrpCmps.CampusId=syCampuses.CampusId ")
            .Append(strWhere)
            '.Append("ORDER BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,saPayments.PaymentTypeId,PaymentTypeDescrip,AwardTypeDescrip ")
            .Append(" ) tblDerived ")
            .Append("GROUP BY CampGrpDescrip,CampusDescrip,PaymentTypeId,PaymentTypeDescrip,AwardTypeDescrip ")
            .Append("ORDER BY CampGrpDescrip,CampusDescrip,PaymentTypeId,PaymentTypeDescrip,AwardTypeDescrip ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "FundTypeSummary"
            'Add new table
            Dim dt As New DataTable("Totals")
            dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("TotalCount", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("TotalSum", System.Type.GetType("System.Decimal")))
            ds.Tables.Add(dt)
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function


#End Region


#Region "Private Methods"

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

    Private Function GetFundTypeSummary(ByVal WhereClause As String, FundSourceSelected As Boolean) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strFrom As String
        Dim strFrom1 As String = ""
        Dim strWhere1 As String = ""

        strFrom = "FROM saPayments,saTransactions,arStuEnrollments,syCmpGrpCmps,syCampGrps"

        If FundSourceSelected Then
            strFrom1 &= ",faStudentAwards,saFundSources,faStudentAwardSchedule SAS,saPmtDisbRel PDR "
            strWhere1 &= " AND PDR.TransactionId=saTransactions.TransactionId " & _
                               " AND saTransactions.Voided=0 " & _
                               "AND PDR.AwardScheduleId=SAS.AwardScheduleId " & _
                               "AND SAS.StudentAwardId=faStudentAwards.StudentAwardId " & _
                               "AND faStudentAwards.AwardTypeId=saFundSources.FundSourceId "
        Else
            strFrom1 &= " "
            strWhere1 &= " "
        End If

        With sb
            .Append("SELECT DISTINCT ")
            .Append("       PaymentTypeId,PaymentTypeDescrip,AwardTypeDescrip,")
            .Append("       COUNT(*) AS PymtCount,SUM(TransAmount) AS PymtSum ")
            .Append("FROM (SELECT ")
            .Append("       saPayments.PaymentTypeId,")
            .Append("       CASE saPayments.PaymentTypeId ")
            .Append("           WHEN 1 THEN 'Cash' ")
            .Append("           WHEN 2 THEN 'Check' ")
            .Append("           WHEN 3 THEN 'Credit Card' ")
            .Append("           WHEN 4 THEN 'EFT' ")
            .Append("           WHEN 5 THEN 'Money Order' ")
            .Append("           WHEN 6 THEN 'Non_Cash' ")
            .Append("           ELSE 'Unknown' ")
            .Append("       END AS PaymentTypeDescrip, ")
            .Append("       (SELECT TOP 1 FS.FundSourceDescrip ")
            .Append("           FROM saPmtDisbRel PDR, faStudentAwards SA, faStudentAwardSchedule SAS,saFundSources FS ")
            .Append("           WHERE PDR.TransactionId = saPayments.TransactionId ")
            .Append("           AND PDR.AwardScheduleId=SAS.AwardScheduleId ")
            .Append("           AND SAS.StudentAwardId=SA.StudentAwardId ")
            .Append("           AND FS.FundSourceId=SA.AwardTypeId) AS AwardTypeDescrip,")
            .Append("       saTransactions.TransAmount * (-1) AS TransAmount ")
            .Append(strFrom)
            .Append(strFrom1)
            .Append(" WHERE saPayments.TransactionId=saTransactions.TransactionId AND saTransactions.IsPosted=1 ")
            .Append("       AND saTransactions.StuEnrollId=arStuEnrollments.StuEnrollId ")
            .Append("       AND arStuEnrollments.CampusId=syCmpGrpCmps.CampusId ")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
            .Append("       AND saTransactions.Voided=0 ")
            .Append(WhereClause)
            .Append(strWhere1)
            '.Append("ORDER BY saPayments.PaymentTypeId,PaymentTypeDescrip,AwardTypeDescrip ")
            .Append(" ) tblDerived ")
            .Append("GROUP BY PaymentTypeId,PaymentTypeDescrip,AwardTypeDescrip ")
            .Append("ORDER BY PaymentTypeId,PaymentTypeDescrip,AwardTypeDescrip")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "FundTypeSummary"      '"FundTypeDetail"
            'Add new table
            'Dim dt As New DataTable("FundTypeSummary")
            'dt.Columns.Add(New DataColumn("PaymentTypeDescrip", System.Type.GetType("System.String")))
            'dt.Columns.Add(New DataColumn("AwardTypeDescrip", System.Type.GetType("System.String")))
            'dt.Columns.Add(New DataColumn("PymtCount", System.Type.GetType("System.Int32")))
            'dt.Columns.Add(New DataColumn("PymtSum", System.Type.GetType("System.Decimal")))
            'ds.Tables.Add(dt)
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

#End Region


End Class
