Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class NonCourseReqDB
    Public Function GetDataListValues(ByRef ReqTypeId As Integer) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim objEmployeeList As New OleDbDataAdapter
        With sb
            .Append("Select a.ReqId, a.Descrip, a.StatusId from arReqs a, arReqTypes b ")
            .Append(" where (b.ReqTypeId = ?) ")
            .Append(" and (a.ReqTypeId = b.ReqTypeId) ")
        End With
        db.AddParameter("@ReqTypeId", ReqTypeId, DataAccess.OleDbDataType.OleDbInteger, 16, ParameterDirection.Input)

        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        Return ds
    End Function

    Public Function GetAllReqTypes() As DataSet
        Dim ds As DataSet

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.ReqTypeId,Descrip ")
                .Append("FROM arReqTypes t1 ")
            End With

            '   Execute the query
            ds = db.RunSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
End Class
