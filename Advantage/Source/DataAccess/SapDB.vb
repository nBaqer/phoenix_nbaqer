Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class SapDB


    Public Function GetSAPPolicyTrigs(ByVal SAPId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim dr As OleDbDataReader

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT t1.SAPDetailId, ")
            '.Append("       (Select count(*) from arSAPDetails where TrigOffsetSeq<=t1.TrigOffsetSeq and ((TrigOffsetSeq*100 + TrigValue)<=(t1.TrigOffsetSeq*100 + t1.TrigValue)) and SAPId=T1.SAPId) As Seq, ")
            '.Append("       Case len(TrigOffsetSeq) when 1 then (Select count(*) from arSAPDetails where TrigOffsetSeq<=t1.TrigOffsetSeq and ((TrigOffsetSeq*100 + TrigValue)<=(t1.TrigOffsetSeq*100 + t1.TrigValue)) and SAPId=?) else (Select count(*) from arSAPDetails where TrigValue <= t1.TrigValue and SAPId=?) end As Seq, ")
            .Append(" ROW_NUMBER() OVER ( ORDER BY t1.TrigOffsetSeq, t1.TrigValue ) AS Seq,")
            .Append("       t1.TrigValue, ")
            .Append("       t1.TrigUnitTypId, ")
            .Append("       t1.TrigOffsetTypId, ")
            .Append("       t1.TrigOffsetSeq, ")
            .Append("       t2.TrigUnitTypDescrip, ")
            .Append("       t3.TrigOffTypDescrip ")
            .Append("FROM arSAPDetails t1, arTrigUnitTyps t2, arTrigOffsetTyps t3 ")
            .Append("WHERE t1.SAPId = ? ")
            .Append("and    t1.TrigUnitTypId = t2.TrigUnitTypId ")
            .Append("and    t1.TrigOffsetTypId = t3.TrigOffsetTypId ")
            '.Append("ORDER BY t1.Seq ")
            .Append("ORDER BY t1.TrigOffsetSeq, t1.TrigValue ")
        End With
        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@SAPId", SAPId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)


        'Close Connection
        db.CloseConnection()

        Return ds

    End Function
    Public Function GetTrigUnits() As DataSet
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New System.Text.StringBuilder

        With sb
            .Append("SELECT t1.TrigUnitTypId,TrigUnitTypDescrip ")
            .Append("FROM arTrigUnitTyps t1 ")
            'where TrigUnitTypId IN(1,2,3,4,5) ")
        End With
        'With sb
        '    .Append("SELECT  DISTINCT t1.TrigUnit, t1.SAPDetailId  ")
        '    .Append("FROM arSAPDetails t1 ")
        'End With


        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds
    End Function

    Public Function GetTrigOffTyps() As DataSet
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New System.Text.StringBuilder

        With sb
            .Append("SELECT t1.TrigOffsetTypId,TrigOffTypDescrip ")
            .Append("FROM arTrigOffsetTyps t1 where t1.TrigOffsetTypId in (2,3) ")
        End With
        'With sb
        '    .Append("SELECT DISTINCT t1.TrigOffsetTyp, t1.SAPDetailId  ")
        '    .Append("FROM arSAPDetails t1 ")
        'End With


        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds
    End Function

    Public Function GetTrigOffTyps(TrigUnitTypId As Integer) As DataSet
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New System.Text.StringBuilder

        With sb
            If TrigUnitTypId = 3 Or TrigUnitTypId = 4 Or TrigUnitTypId = 5 Then
                .Append("SELECT t1.TrigOffsetTypId,TrigOffTypDescrip ")
                .Append("FROM arTrigOffsetTyps t1 where t1.TrigOffsetTypId in (3) ")
            Else
                .Append("SELECT t1.TrigOffsetTypId,TrigOffTypDescrip ")
                .Append("FROM arTrigOffsetTyps t1 where t1.TrigOffsetTypId in (2,3) ")
            End If
        End With
        'With sb
        '    .Append("SELECT DISTINCT t1.TrigOffsetTyp, t1.SAPDetailId  ")
        '    .Append("FROM arSAPDetails t1 ")
        'End With


        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds
    End Function

    Public Function GetQuantMinUnitTyps() As DataSet
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New System.Text.StringBuilder
        With sb
            .Append("SELECT t1.QuantMinUnitTypId,QuantMinTypDesc ")
            .Append("FROM arQuantMinUnitTyps t1 where t1.QuantMinUnitTypId in(1,3)  ")
        End With
        'With sb
        '    .Append("SELECT DISTINCT t1.QuantMinUnitType, t1.SAPDetailId  ")
        '    .Append("FROM arSAPDetails t1 ")
        'End With


        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds
    End Function
    Public Function GetQualTyps(Optional ByVal qualMinType As Integer = 1) As DataSet
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New System.Text.StringBuilder
        With sb
            .Append("SELECT t1.QualMinTypId,QualMinTypDesc ")
            .Append("FROM arQualMinTyps t1 where t1.QualMinTypId = ? ")
        End With

        'With sb
        '    .Append("SELECT DISTINCT t1.QualType, t1.SAPDetailId  ")
        '    .Append("FROM arSAPDetails t1 ")
        'End With


        '   Execute the query
        db.AddParameter("q1", qualMinType, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds
    End Function

    Public Function GetConsequences() As DataSet
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New System.Text.StringBuilder

        With sb
            .Append("SELECT t1.ConsequenceTypId,ConseqTypDesc ")
            .Append("FROM arConsequenceTyps t1 ")
        End With

        'With sb
        '    .Append("SELECT DISTINCT t1.Consequence,t1.SAPDetailId  ")
        '    .Append("FROM arSAPDetails t1 ")
        'End With


        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds
    End Function
    Public Function GetSAPInfo(ByVal SAPDetailId As String) As DataSet
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        With sb
            .Append("Select t1.SAPDetailId, ")
            .Append("       t1.QualMinValue, ")
            .Append("       t1.QuantMinValue, ")
            .Append("       t1.QuantMinUnitTypId, ")
            .Append("       t1.QualMinTypId, ")
            '.Append("       (Select count(*) from arSAPDetails where TrigOffsetSeq<=t1.TrigOffsetSeq and ((TrigOffsetSeq*100 + TrigValue)<=(t1.TrigOffsetSeq*100 + t1.TrigValue)) and SAPId=T1.SAPId) As Seq, ")
            '.Append("       Case len(TrigOffsetSeq) when 1 then (Select count(*) from arSAPDetails where TrigOffsetSeq<=t1.TrigOffsetSeq and ((TrigOffsetSeq*100 + TrigValue)<=(t1.TrigOffsetSeq*100 + t1.TrigValue)) and SAPId=T1.SAPId) else (Select count(*) from arSAPDetails where TrigValue <= t1.TrigValue and SAPId=T1.SAPId) end As Seq, ")
            .Append("ROW_NUMBER()OVER (ORDER BY t1.TrigOffsetSeq,t1.TrigValue) AS Seq, ")
            .Append("       t1.TrigValue, ")
            .Append("       t1.TrigUnitTypId, ")
            .Append("       t1.TrigOffsetTypId, ")
            .Append("       t1.TrigOffsetSeq, ")
            .Append("       t1.ConsequenceTypId, ")
            .Append("       t1.MinCredsCompltd, ")
            .Append("       t1.MinAttendanceValue, ")
            .Append("       t1.SAPId, ")
            .Append("       t2.TrigUnitTypDescrip, ")
            .Append("       t3.TrigOffTypDescrip, ")
            .Append("       t4.QuantMinTypDesc, ")
            .Append("       t5.QualMinTypDesc, ")
            .Append("       t6.ConseqTypDesc, t1.Accuracy, ")
            .Append(" IsSAPPolicyUsed = (CASE WHEN (SELECT COUNT(*) FROM arSAPChkResults WHERE SAPDetailId = ? and PreviewSapCheck = 0 )> 0 THEN 'True' ELSE 'False' END) ")
            .Append("FROM arSAPDetails t1, arTrigUnitTyps t2, arTrigOffsetTyps t3, arQuantMinUnitTyps t4, arQualMinTyps t5, arConsequenceTyps t6 ")
            .Append("WHERE t1.SAPDetailId = ? ")
            .Append("and    t1.TrigUnitTypId = t2.TrigUnitTypId ")
            .Append("and    t1.TrigOffsetTypId = t3.TrigOffsetTypId ")
            .Append("and    t1.QuantMinUnitTypId = t4.QuantMinUnitTypId ")
            .Append("and    t1.QualMinTypId = t5.QualMinTypId ")
            .Append("and    t1.ConsequenceTypId = t6.ConsequenceTypId ")
            .Append(" ORDER BY TrigOffsetSeq,TrigValue ")
        End With

        db.AddParameter("@SAPDetailId1", SAPDetailId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.AddParameter("@SAPDetailId", SAPDetailId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            '   Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()
        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try
        Return ds
    End Function

    Public Function GetSAPInfo_SP(ByVal SAPDetailId As String) As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
        Dim ds As DataSet

        db.AddParameter("@SAPDetailId", New Guid(SAPDetailId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet_SP("dbo.usp_GetSAPDetails")

        Try
            Return ds

        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try

    End Function

    Public Function AddSAPDetails(ByVal SAPDetails As SAPInfo) As String

        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            With sb
                .Append("INSERT INTO arSAPDetails ")
                '.Append("(SAPDetailId, SapId, Seq, TrigValue, TrigUnitTypId, TrigOffsetTypId, TrigOffsetSeq,QualMinValue,QuantMinValue,QuantMinUnitTypId,QualMinTypId,MinCredsCompltd,ConsequenceTypId,Accuracy) ")
                .Append("(SAPDetailId, SapId, TrigValue, TrigUnitTypId, TrigOffsetTypId, TrigOffsetSeq,QualMinValue,QuantMinValue,QuantMinUnitTypId,QualMinTypId,MinAttendanceValue,MinCredsCompltd,ConsequenceTypId,Accuracy) ")
                '.Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?) ; ")

            End With
            db.AddParameter("@SAPDetailId", SAPDetails.SAPDetailId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            db.AddParameter("@SAPId", SAPDetails.SAPId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            'db.AddParameter("@Seq", SAPDetails.Seq, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@TrigValue", SAPDetails.TrigValue, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@TrigUnit", SAPDetails.TrigUnitTypId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@TrigOffsetTyp", SAPDetails.TrigOffsetTypId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)


            'If the triggoffsetseq is 0 it means that we really should insert null in the db
            If SAPDetails.TrigOffsetSeq = 0 Then
                db.AddParameter("@TrigOffsetSeq", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@TrigOffsetSeq", SAPDetails.TrigOffsetSeq, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            End If

            db.AddParameter("@QualMinValue", SAPDetails.QualMinValue, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            If SAPDetails.OverallAttendance = True Then
                db.AddParameter("@QuantMinValue", SAPDetails.QuantMinValue, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            Else
                db.AddParameter("@QuantMinValue", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            End If

            db.AddParameter("@QuantMinUnitType", SAPDetails.QuantMinUnitTypId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@QualMinType", SAPDetails.QualMinTypId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@MinAttendance", SAPDetails.MinAttendanceValue, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@MinCredsCompltd", SAPDetails.MinCredsCompltd, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@Consequence", SAPDetails.ConsequenceTypId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@Accuracy", SAPDetails.Accuracy, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            If SAPDetails.OverallAttendance = False Then

                DeleteSAPDetailsQuantByInstruction(SAPDetails.SAPDetailId)
                AddSAPDetailsQuantByInstruction(SAPDetails)


            End If

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddSAPDetailsQuantByInstruction(SAPDetails) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        For i As Integer = 0 To SAPDetails.QuantMinValueByInsTypes.Count - 1

            With sb
                .Append(" INSERT INTO dbo.arSAPQuantByInstruction ")
                .Append(" ( SAPQuantInsTypeID ,QuantMinValue,SAPDetailId,InstructionTypeId,moddate)")
                .Append("VALUES")
                .Append("(?,?,?,?,?)")
            End With
            db.AddParameter("@SAPQuantInsTypeID", SAPDetails.QuantMinValueByInsTypes(i).SAPQuantInsTypeID, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            db.AddParameter("@QuantMinValue", SAPDetails.QuantMinValueByInsTypes(i).QuantMinValue, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@SAPDetailId", SAPDetails.QuantMinValueByInsTypes(i).SAPDetailId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            db.AddParameter("@InstructionTypeId", SAPDetails.QuantMinValueByInsTypes(i).InstructionTypeId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            db.AddParameter("@moddate", SAPDetails.QuantMinValueByInsTypes(i).moddate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

        Next

    End Function



    Public Function UpdateSAPDetails(ByVal SAPDetails As SAPInfo) As String
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            With strSQL
                .Append("UPDATE arSAPDetails SET ")
                '.Append("Seq = ?")
                .Append("TrigValue = ?")
                .Append(",TrigUnitTypId = ?")
                .Append(",TrigOffsetTypId = ?")
                .Append(",TrigOffsetSeq = ?")
                .Append(",QualMinValue = ?")
                .Append(",QuantMinValue = ?")
                .Append(",QuantMinUnitTypId = ?")
                .Append(",QualMinTypId = ?")
                .Append(",MinCredsCompltd = ?")
                .Append(",MinAttendanceValue = ?")
                .Append(",ConsequenceTypId = ?")
                .Append(",Accuracy=? ")
                .Append(" WHERE SAPDetailId = ? ")
            End With
            'db.AddParameter("@Seq", SAPDetails.Seq, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@TrigValue", SAPDetails.TrigValue, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@TrigUnit", SAPDetails.TrigUnitTypId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@TrigOffsetTyp", SAPDetails.TrigOffsetTypId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            'If the triggoffsetseq is 0 it means that we really should insert null in the db
            If SAPDetails.TrigOffsetSeq = 0 Then
                db.AddParameter("@TrigOffsetSeq", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@TrigOffsetSeq", SAPDetails.TrigOffsetSeq, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            End If
            db.AddParameter("@QualMinValue", SAPDetails.QualMinValue, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            If SAPDetails.OverallAttendance = True Then
                db.AddParameter("@QuantMinValue", SAPDetails.QuantMinValue, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            Else
                db.AddParameter("@QuantMinValue", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            End If

            db.AddParameter("@QuantMinUnitType", SAPDetails.QuantMinUnitTypId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@QualMinType", SAPDetails.QualMinTypId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@MinCredsCompltd", SAPDetails.MinCredsCompltd, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@MinAttendance", SAPDetails.MinAttendanceValue, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@Consequence", SAPDetails.ConsequenceTypId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@Accuracy", SAPDetails.Accuracy, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@SAPDetailId", SAPDetails.SAPDetailId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
            db.ClearParameters()
            strSQL.Remove(0, strSQL.Length)
            DeleteSAPDetailsQuantByInstruction(SAPDetails.SAPDetailId)
            If SAPDetails.OverallAttendance = False Then
                AddSAPDetailsQuantByInstruction(SAPDetails)
            End If
            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Function DeleteSAPDetails(ByVal SAPDetailId As Guid) As String
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        Try
            DeleteSAPDetailsQuantByInstruction(SAPDetailId)
            With strSQL
                .Append("DELETE FROM arSAPDetails WHERE SAPDetailId = ?")
            End With

            db.AddParameter("@SAPDetailId", SAPDetailId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)

            'return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'db.AddParameter("@SAPDetailId", SAPDetailId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
        'db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        'db.ClearParameters()
        'strSQL.Remove(0, strSQL.Length)

    End Function


    Public Function DeleteSAPDetailsQuantByInstruction(ByVal SAPDetailId As Guid) As String
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        Try
            With strSQL
                .Append("DELETE FROM arSAPQuantByInstruction  WHERE SAPDetailId = ?")
            End With

            db.AddParameter("@SAPDetailId", SAPDetailId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)

            'return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'db.AddParameter("@SAPDetailId", SAPDetailId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
        'db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        'db.ClearParameters()
        'strSQL.Remove(0, strSQL.Length)

    End Function

    Public Function GetSAPPolicyName(ByVal sapPolicyID As String) As String
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT SAPDescrip ")
            .Append("FROM arSAP ")
            .Append("WHERE SAPId = ?")
        End With

        db.AddParameter("@SAPID", sapPolicyID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Return DirectCast(db.RunParamSQLScalar(sb.ToString), String)
    End Function
    Public Function GetValidationSequence(ByVal SAPId As String) As String
        Dim dt As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        With sb
            .Append("Select SAPDetailId, ")
            '.Append("       (Select count(*) from arSAPDetails where TrigOffsetSeq<=t1.TrigOffsetSeq and ((TrigOffsetSeq*100 + TrigValue)<=(t1.TrigOffsetSeq*100 + t1.TrigValue)) and SAPId= ?) As Seq, ")
            '.Append("       Case len(TrigOffsetSeq) when 1 then (Select count(*) from arSAPDetails where TrigOffsetSeq<=t1.TrigOffsetSeq and ((TrigOffsetSeq*100 + TrigValue)<=(t1.TrigOffsetSeq*100 + t1.TrigValue)) and SAPId=?) else (Select count(*) from arSAPDetails where TrigValue <= t1.TrigValue and SAPId=?) end As Seq, ")
            .Append(" ROW_NUMBER() OVER ( ORDER BY t1.TrigOffsetSeq, t1.TrigValue ) AS Seq,")
            .Append("       TrigValue, ")
            .Append("       TrigUnitTypId, ")
            .Append("       TrigOffsetTypId, ")
            .Append("       TrigOffsetSeq ")
            .Append("FROM   arSAPDetails t1 ")
            .Append("WHERE  SAPId = ? ")
            '.Append("ORDER BY Seq, TrigOffsetTypId, TrigValue ")
            .Append("ORDER BY TrigOffsetSeq, TrigValue ")

        End With

        'db.AddParameter("@SAPId", SAPId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@SAPId", SAPId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@SAPId", SAPId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            '   Execute the query
            dt = db.RunParamSQLDataSet(sb.ToString).Tables(0)
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()
        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        '   build validation sequence string
        For Each row As DataRow In dt.Rows
            'If Not row.IsNull("TrigOffsetSeq") Then
            '    sb.Append(CType(row("Seq") * 10000000 + row("TrigOffsetSeq") * 100000 + row("TrigOffsetTypId") * 10000 + row("TrigValue") * 10 + row("TrigUnitTypId"), String).ToString)
            'Else
            '    sb.Append(CType(row("Seq") * 10000000 + 0 * 100000 + row("TrigOffsetTypId") * 10000 + row("TrigValue") * 10 + row("TrigUnitTypId"), String).ToString)
            'End If
            If Not row.IsNull("TrigOffsetSeq") Then
                'sb.Append(CType(row("TrigOffsetSeq") * 100000 + row("TrigOffsetTypId") * 10000 + row("TrigValue") * 10 + row("TrigUnitTypId"), String).ToString)
                sb.Append(CType(row("TrigOffsetSeq") * 1000000000 + row("TrigOffsetTypId") * 100000000 + row("TrigValue") * 100000 + row("TrigUnitTypId"), String).ToString)
            Else
                'sb.Append(CType(row("TrigOffsetTypId") * 10000 + row("TrigValue") * 10 + row("TrigUnitTypId"), String).ToString)
                sb.Append(CType(row("TrigOffsetTypId") * 100000000 + row("TrigValue") * 100000 + row("TrigUnitTypId"), String).ToString)
            End If

            sb.Append(";")
        Next

        'remove the last ';'
        If sb.Length > 1 Then sb.Remove(sb.Length - 1, 1)

        '   return string
        Return sb.ToString
    End Function
    Public Function InsertSAPPolicy(ByVal SAPPolicy As SAPInfo, ByVal user As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim clssectid As String
        Dim StudentId As String
        Dim GrdBkWgtDetailId As String
        Dim score As Integer
        Dim Comments As String
        Dim sBuildSAP As String = CheckStr(SAPPolicy.Descrip)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try

            With sb
                .Append("INSERT INTO arSAP(SAPId,SAPCode,SAPDescrip,TrigUnitTypId,TrigOffsetTypId,StatusId,CampGrpId,ModUser,ModDate,TerminationProbationCnt,MinTermGPA,TermGPAOver,TrackExternAttendance,IncludeTransferHours, FaSapPolicy, PayOnProbation) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
            End With
            db.AddParameter("@sapid", SAPPolicy.SAPId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.AddParameter("@code", SAPPolicy.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            If SAPPolicy.FaSapPolicy = 1 Then
                db.AddParameter("@descrip", sBuildSAP, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@descrip", SAPPolicy.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            db.AddParameter("@TrigUnitTypId", SAPPolicy.TrigUnitTypId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
            db.AddParameter("@TrigOffsetTypId", SAPPolicy.TrigOffsetTypId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)

            db.AddParameter("@statusid", SAPPolicy.StatusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            db.AddParameter("@campgrpid", SAPPolicy.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            If (SAPPolicy.TerminateProbCnt = 0) Then
                db.AddParameter("@TerCnt", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Else
                db.AddParameter("@TerCnt", SAPPolicy.TerminateProbCnt, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End If
            If (SAPPolicy.MinTermGPA = 0) Then
                db.AddParameter("@MinTermGPA", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Else
                db.AddParameter("@MinTermGPA", SAPPolicy.MinTermGPA, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End If
            If (SAPPolicy.TermGPAOver = 0) Then
                db.AddParameter("@TermGPAOver", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Else
                db.AddParameter("@TermGPAOver", SAPPolicy.TermGPAOver, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End If
            db.AddParameter("@TrackExternAttendance", SAPPolicy.TrackExternAttendance, DataAccess.OleDbDataType.OleDbBoolean, 16, ParameterDirection.Input)
            ''Added by Saraswathi lakshamanan on June 8 2010 for transfer Hours 11355
            db.AddParameter("@IncludeTransferHours", SAPPolicy.IncludeTransferHours, DataAccess.OleDbDataType.OleDbBoolean, 16, ParameterDirection.Input)
            ''US4487 by Tatiana in DB field
            db.AddParameter("@FaSapPolicy", SAPPolicy.FaSapPolicy, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@PayOnProbation", SAPPolicy.PayOnProbation, DataAccess.OleDbDataType.OleDbBoolean, 16, ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Private Function CheckStr(ByVal Item As String) As String
        Dim iPos = InStr(Item, "(")
        If iPos > 0 Then
            Return Item
        Else
            Item = Item & " (Title IV)"
            Return Item
        End If

    End Function

    Public Function UpdateSAPPolicy(ByVal SAPPolicy As SAPInfo, ByVal user As String) As String

        Dim SqlConnection As SqlConnection
        Dim mTransaction As SqlTransaction
        Dim rowCt As Integer
        Dim sBuildSAP As String = CheckStr(SAPPolicy.Descrip)



        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Dim connectionString = MyAdvAppSettings.AppSettings("ConnectionString").ToString

        SqlConnection = New SqlConnection(connectionString)
        If SqlConnection.State = ConnectionState.Open Then SqlConnection.Close()
        SqlConnection.Open()

        Try
            Dim todaysdate As String = String.Format("{0:MM/dd/yyyy HH:mm:ss}", DateTime.Now)


            'Begin Transation
            'The File Been Modifyed 
            mTransaction = SqlConnection.BeginTransaction()

            Dim myCommand As New SqlCommand("UPDATE arSAP SET SAPCode = @SAPCode,  SAPDescrip=@SAPDescrip, " &
                                            " TrigUnitTypId = @TrigUnitTypId,  TrigOffsetTypId  = @TrigOffsetTypId," &
                                           " StatusId= @StatusId, CampGrpId =@CampGrpId,   ModUser=@ModUser,  ModDate=@ModDate,  TerminationProbationCnt=@TerminationProbationCnt, " &
                                           " MinTermGPA=@MinTermGPA, TermGPAOver=@TermGPAOver, TrackExternAttendance=@TrackExternAttendance, " &
                                           " IncludeTransferHours= @IncludeTransferHours, FaSapPolicy=@FaSapPolicy, PayOnProbation=@PayOnProbation  WHERE SAPId = @SAPId; " &
                                           " SELECT COUNT(*) FROM arSAP WHERE ModDate=@ModDate", SqlConnection)
            myCommand.Parameters.AddWithValue("@SAPCode", SAPPolicy.Code)
            If SAPPolicy.FaSapPolicy = 1 Then
                myCommand.Parameters.AddWithValue("@SAPDescrip", sBuildSAP)
            Else
                myCommand.Parameters.AddWithValue("@SAPDescrip", SAPPolicy.Descrip)
            End If

            myCommand.Parameters.AddWithValue("@TrigUnitTypId", SAPPolicy.TrigUnitTypId)
            myCommand.Parameters.AddWithValue("@TrigOffsetTypId", SAPPolicy.TrigOffsetTypId)
            myCommand.Parameters.AddWithValue("@SAPId", SAPPolicy.SAPId)
            myCommand.Parameters.AddWithValue("@FaSapPolicy", SAPPolicy.FaSapPolicy)
            If (SAPPolicy.PayOnProbation Is Nothing) Then
                myCommand.Parameters.AddWithValue("@PayOnProbation", System.DBNull.Value)
            Else
                myCommand.Parameters.AddWithValue("@PayOnProbation", SAPPolicy.PayOnProbation)
            End If
            myCommand.Parameters.AddWithValue("@StatusId", SAPPolicy.StatusId)
            myCommand.Parameters.AddWithValue("@CampGrpId", SAPPolicy.CampGrpId)
            If (SAPPolicy.TerminateProbCnt = 0) Then
                myCommand.Parameters.AddWithValue("@TerminationProbationCnt", System.DBNull.Value)
            Else
                myCommand.Parameters.AddWithValue("@TerminationProbationCnt", SAPPolicy.TerminateProbCnt)
            End If

            myCommand.Parameters.AddWithValue("@TrackExternAttendance", SAPPolicy.TrackExternAttendance)
            myCommand.Parameters.AddWithValue("@IncludeTransferHours", SAPPolicy.IncludeTransferHours)
            If (SAPPolicy.MinTermGPA = 0) Then
                myCommand.Parameters.AddWithValue("@MinTermGPA", System.DBNull.Value)
            Else
                myCommand.Parameters.AddWithValue("@MinTermGPA", SAPPolicy.MinTermGPA)
            End If
            If (SAPPolicy.TermGPAOver = 0) Then
                myCommand.Parameters.AddWithValue("@TermGPAOver", System.DBNull.Value)
            Else
                myCommand.Parameters.AddWithValue("@TermGPAOver", SAPPolicy.TermGPAOver)
            End If
            myCommand.Parameters.AddWithValue("@ModUser", user)
            myCommand.Parameters.AddWithValue("@ModDate", todaysdate)

            myCommand.CommandType = CommandType.Text


            If mTransaction IsNot Nothing Then
                myCommand.Transaction = mTransaction
            End If

            rowCt = Convert.ToInt32(myCommand.ExecuteScalar())
            mTransaction.Commit()

            'If there were no updated rows then there was a concurrency problem
            If rowCt = 1 Then
                '   return NO errors
                Return ""
                myCommand.Dispose()
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
                myCommand.Dispose()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            mTransaction.Rollback()
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            mTransaction.Dispose()

            'Close Connection
            If SqlConnection.State = ConnectionState.Open Then SqlConnection.Close()
        End Try

    End Function

    Public Function DeleteSAPPolicy(ByVal SAPPolicyId As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim clssectid As String
        Dim StudentId As String
        Dim GrdBkWgtDetailId As String
        Dim score As Integer
        Dim Comments As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'clssectid = StdGrdObj.ClsSectId
        'StudentId = StdGrdObj.StudentId
        'GrdBkWgtDetailId = StdGrdObj.GrdBkWgtDetailId


        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            With sb
                .Append("DELETE FROM arSAP ")
                .Append("WHERE SAPId = ? ")


            End With

            db.AddParameter("@coursegrpid", SAPPolicyId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetSAPPolicy(ByVal SAPId As String) As SAPInfo

        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select SAPId,SAPCode,SAPDescrip,TrigUnitTypId,TrigOffsetTypId,StatusId,CampGrpId, ")
            .Append("ModDate,ModUser,TerminationProbationCnt,MinTermGPA,TermGPAOver,isnull(TrackExternAttendance,0) as TrackExternAttendance,Isnull(IncludeTransferHours,0) as IncludeTransferHours, FaSapPolicy, PayOnProbation from arSAP where SAPId=? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@CourseID1", SAPId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim SAPObj As New SAPInfo

        While dr.Read()
            With SAPObj
                '.StudentId = dr("StudentId")
                .SAPId = dr("SAPId")
                .Code = dr("SAPCode").ToString
                .Descrip = dr("SAPDescrip").ToString
                If Not (dr("TrigUnitTypId") Is System.DBNull.Value) Then .TrigUnitTypId = dr("TrigUnitTypId") Else .TrigUnitTypId = 0
                If Not (dr("TrigOffsetTypId") Is System.DBNull.Value) Then .TrigOffsetTypId = dr("TrigOffsetTypId") Else .TrigOffsetTypId = 0
                '.TrigUnitTypId = dr("TrigUnitTypId")
                '.TrigOffsetTypId = dr("TrigOffsetTypId")
                .StatusId = dr("StatusId")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = dr("CampGrpId").ToString Else .CampGrpId = ""
                'get ModUser
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate") Else .ModDate = Date.MinValue
                If Not (dr("TerminationProbationCnt") Is System.DBNull.Value) Then .TerminateProbCnt = dr("TerminationProbationCnt")
                If Not (dr("MinTermGPA") Is System.DBNull.Value) Then .MinTermGPA = dr("MinTermGPA")
                If Not (dr("TermGPAOver") Is System.DBNull.Value) Then .TermGPAOver = dr("TermGPAOver")
                .TrackExternAttendance = dr("TrackExternAttendance")
                .IncludeTransferHours = dr("IncludeTransferHours")
                If Not (dr("FaSapPolicy") Is System.DBNull.Value) Then .FaSapPolicy = If(CType(dr("FaSapPolicy"), Boolean) = True, 1, 0) Else .FaSapPolicy = 0
                If Not (dr("PayOnProbation") Is System.DBNull.Value) Then .PayOnProbation = CType(dr("PayOnProbation"), Boolean)

                .ModUser = dr("ModUser")
            End With
        End While

        SAPObj.TitleIVSAPCustomVerbiage = GetSapNoticeCustomMessages(SAPId)

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return SAPObj
    End Function

    Public Function InsertSAPCustomVerbiage(ByVal ListOfSapCustomVerbiage As List(Of SAPCustomVerbiage), ByVal userId As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try

            Dim activeStatusId = ""
            Dim activeStatusQuery As New StringBuilder
            Dim userGuid = Guid.Parse(userId)
            With activeStatusQuery
                .Append("SELECT StatusId FROM dbo.syStatuses WHERE UPPER(StatusCode) = 'A'")
            End With
            Dim activeStatusIdObject = db.RunParamSQLScalar(activeStatusQuery.ToString)
            activeStatusId = Convert.ToString(activeStatusIdObject)
            db.ClearParameters()

            For Each scv As SAPCustomVerbiage In ListOfSapCustomVerbiage
                Dim titleIVSapStatusCodeId = 0
                Dim titleIVSAPStatusQuery As New StringBuilder
                Dim existingCustomVerbiageQuery As New StringBuilder
                Dim code = scv.Code.Trim()
                With titleIVSAPStatusQuery
                    .Append("SELECT TOP 1 Id FROM dbo.syTitleIVSapStatus WHERE Code = ? AND StatusId = ?")
                End With

                db.AddParameter("@code", code, DataAccess.OleDbDataType.OleDbString, code.Length, ParameterDirection.Input)
                db.AddParameter("@statusId", activeStatusId, DataAccess.OleDbDataType.OleDbString, activeStatusId.Length, ParameterDirection.Input)

                Dim titleIVSAPStatusObject = db.RunParamSQLScalar(titleIVSAPStatusQuery.ToString)
                titleIVSapStatusCodeId = Convert.ToInt32(titleIVSAPStatusObject)
                db.ClearParameters()
                titleIVSAPStatusQuery.Remove(0, titleIVSAPStatusQuery.Length)


                With sb
                    .Append("INSERT INTO syTitleIVSapCustomVerbiage(SAPId,TitleIVSapStatusId,Message,StatusId,CreatedById,CreatedDate) ")
                    .Append("VALUES(?,?,?,?,?,?)")
                End With
                db.AddParameter("@sapid", scv.SapId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                db.AddParameter("@titleIVSapStatusCode", titleIVSapStatusCodeId, DataAccess.OleDbDataType.OleDbInteger, ParameterDirection.Input)
                db.AddParameter("@message", scv.Message, DataAccess.OleDbDataType.OleDbString, scv.Message.Length, ParameterDirection.Input)
                db.AddParameter("@statusId", activeStatusId, DataAccess.OleDbDataType.OleDbString, activeStatusId.Length, ParameterDirection.Input)
                db.AddParameter("@userId", userGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                db.AddParameter("@createdDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, ParameterDirection.Input)

                'Execute the query
                db.OpenConnection()
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)

            Next

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteSAPCustomVerbiage(ByVal SAPPolicyId As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            With sb
                .Append("DELETE FROM syTitleIVSapCustomVerbiage ")
                .Append("WHERE SAPId = ? ")


            End With

            db.AddParameter("@coursegrpid", SAPPolicyId, DataAccess.OleDbDataType.OleDbString, SAPPolicyId.Length, ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function SetSAPCustomVerbiageStatus(ByVal customVerbiage As SAPCustomVerbiage, Optional ByVal sapStatusId As Integer = 0) As String

        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim inactiveStatusId = ""
        Dim inactiveStatusQuery As New StringBuilder

        With inactiveStatusQuery
            .Append("SELECT StatusId FROM dbo.syStatuses WHERE UPPER(StatusCode) = 'I'")
        End With

        Try
            Dim inactiveStatusIdObject = db.RunParamSQLScalar(inactiveStatusQuery.ToString)
            inactiveStatusId = Convert.ToString(inactiveStatusIdObject)
            db.ClearParameters()
        Catch ex As Exception
        End Try

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            If (sapStatusId = 0) Then
                Dim activeStatusId = ""
                Dim activeStatusQuery As New StringBuilder
                With activeStatusQuery
                    .Append("SELECT StatusId FROM dbo.syStatuses WHERE UPPER(StatusCode) = 'A'")
                End With
                Dim activeStatusIdObject = db.RunParamSQLScalar(activeStatusQuery.ToString)
                activeStatusId = Convert.ToString(activeStatusIdObject)
                db.ClearParameters()

                Dim titleIVSAPStatusQuery As New StringBuilder
                Dim code = customVerbiage.Code.Trim()

                With titleIVSAPStatusQuery
                    .Append("SELECT TOP 1 Id FROM dbo.syTitleIVSapStatus WHERE Code = ? AND StatusId = ?")
                End With

                db.AddParameter("@code", code, DataAccess.OleDbDataType.OleDbString, code.Length, ParameterDirection.Input)
                db.AddParameter("@statusId", activeStatusId, DataAccess.OleDbDataType.OleDbString, activeStatusId.Length, ParameterDirection.Input)

                Dim titleIVSAPStatusObject = db.RunParamSQLScalar(titleIVSAPStatusQuery.ToString)
                sapStatusId = Convert.ToInt32(titleIVSAPStatusObject)
                db.ClearParameters()

            End If

            Dim existingCustomVerbiageQuery As New StringBuilder
            With existingCustomVerbiageQuery
                .Append("UPDATE dbo.syTitleIVSapCustomVerbiage SET StatusId = ? WHERE SapId = ? AND TitleIVSapStatusId = ?")
            End With
            db.AddParameter("@statusId", inactiveStatusId, DataAccess.OleDbDataType.OleDbString, inactiveStatusId.Length, ParameterDirection.Input)
            db.AddParameter("@sapid", customVerbiage.SapId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.AddParameter("@sapStatusId", sapStatusId, DataAccess.OleDbDataType.OleDbInteger, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(existingCustomVerbiageQuery.ToString)
            db.ClearParameters()
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function UpdateSAPCustomVerbiage(ByVal ListOfSapCustomVerbiage As List(Of SAPCustomVerbiage), ByVal userId As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim activeStatusId = ""
        Dim activeStatusQuery As New StringBuilder

        With activeStatusQuery
            .Append("SELECT StatusId FROM dbo.syStatuses WHERE UPPER(StatusCode) = 'A'")
        End With

        Try
            Dim activeStatusIdObject = db.RunParamSQLScalar(activeStatusQuery.ToString)
            activeStatusId = Convert.ToString(activeStatusIdObject)
            db.ClearParameters()
        Catch ex As Exception
        End Try

        Try

            For Each scv As SAPCustomVerbiage In ListOfSapCustomVerbiage
                Dim titleIVSapStatusCodeId = 0
                Dim titleIVSAPStatusQuery As New StringBuilder
                Dim existingCustomVerbiageQuery As New StringBuilder
                Dim code = scv.Code.Trim()

                With titleIVSAPStatusQuery
                    .Append("SELECT TOP 1 Id FROM dbo.syTitleIVSapStatus WHERE Code = ? AND StatusId = ?")
                End With

                db.AddParameter("@code", code, DataAccess.OleDbDataType.OleDbString, code.Length, ParameterDirection.Input)
                db.AddParameter("@statusId", activeStatusId, DataAccess.OleDbDataType.OleDbString, activeStatusId.Length, ParameterDirection.Input)

                Try
                    Dim titleIVSAPStatusObject = db.RunParamSQLScalar(titleIVSAPStatusQuery.ToString)
                    titleIVSapStatusCodeId = Convert.ToInt32(titleIVSAPStatusObject)
                    db.ClearParameters()
                Catch ex As Exception
                End Try

                SetSAPCustomVerbiageStatus(scv, titleIVSapStatusCodeId)

                With sb
                    .Append("INSERT INTO syTitleIVSapCustomVerbiage(SAPId,TitleIVSapStatusId,Message,StatusId,CreatedById,CreatedDate) ")
                    .Append("VALUES(?,?,?,?,?,?)")
                End With
                db.AddParameter("@sapid", scv.SapId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                db.AddParameter("@titleIVSapStatusCode", titleIVSapStatusCodeId, DataAccess.OleDbDataType.OleDbInteger, ParameterDirection.Input)
                db.AddParameter("@message", scv.Message, DataAccess.OleDbDataType.OleDbString, scv.Message.Length, ParameterDirection.Input)
                db.AddParameter("@statusId", activeStatusId, DataAccess.OleDbDataType.OleDbString, activeStatusId.Length, ParameterDirection.Input)
                db.AddParameter("@userId", userId, DataAccess.OleDbDataType.OleDbString, userId.Length, ParameterDirection.Input)
                db.AddParameter("@createdDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, ParameterDirection.Input)

                'Execute the query
                db.OpenConnection()
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)

            Next

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetSapNoticeDefaultMessages() As Dictionary(Of String, String)

        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim activeStatusId = ""
        Dim activeStatusQuery As New StringBuilder

        With activeStatusQuery
            .Append("SELECT StatusId FROM dbo.syStatuses WHERE UPPER(StatusCode) = 'A'")
        End With

        Try
            Dim activeStatusIdObject = db.RunParamSQLScalar(activeStatusQuery.ToString)
            activeStatusId = Convert.ToString(activeStatusIdObject)
            db.ClearParameters()
        Catch ex As Exception
        End Try


        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("SELECT Code, DefaultMessage ")
            .Append("FROM(SELECT Code,DefaultMessage,StatusId, ROW_NUMBER() OVER (PARTITION BY CODE, StatusId ORDER BY CreatedDate) AS Occurence FROM dbo.syTitleIVSapStatus) AS defaultMessagesWithCode WHERE defaultMessagesWithCode.Occurence = 1 AND defaultMessagesWithCode.StatusId = ? ")

        End With

        db.AddParameter("@statusId", activeStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim defaultMessages As New Dictionary(Of String, String)

        While dr.Read()
            With defaultMessages
                .Add(dr("Code").ToString(), dr("DefaultMessage").ToString())
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return defaultMessages
    End Function


    Public Function GetSapNoticeCustomMessages(ByVal SAPId As String) As List(Of SAPCustomVerbiage)

        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim activeStatusId = ""
        Dim activeStatusQuery As New StringBuilder

        With activeStatusQuery
            .Append("SELECT StatusId FROM dbo.syStatuses WHERE UPPER(StatusCode) = 'A'")
        End With

        Try
            Dim activeStatusIdObject = db.RunParamSQLScalar(activeStatusQuery.ToString)
            activeStatusId = Convert.ToString(activeStatusIdObject)
            db.ClearParameters()
        Catch ex As Exception
        End Try


        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("SELECT SapId,Code,Message ")
            .Append("FROM(SELECT scv.SapId, ss.Code, scv.Message, scv.StatusId, ROW_NUMBER() OVER (PARTITION BY scv.SapId, scv.TitleIVSapStatusId, scv.StatusId ORDER BY scv.CreatedDate) AS Occurence FROM dbo.syTitleIVSapCustomVerbiage scv INNER JOIN dbo.syTitleIVSapStatus ss on ss.Id = scv.TitleIVSapStatusId WHERE scv.SapId = ?) AS defaultMessagesWithCode WHERE defaultMessagesWithCode.Occurence = 1 AND defaultMessagesWithCode.StatusId = ? ")

        End With
        db.AddParameter("@sapId", SAPId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@statusId", activeStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim customMessages As New List(Of SAPCustomVerbiage)

        While dr.Read()
            With customMessages
                .Add(New SAPCustomVerbiage With {.SAPId = dr("SapId"), .Code = dr("Code"), .Message = dr("Message")})
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return customMessages
    End Function
    Public Function GetSAPPolicyMinAttendanceValue(ByVal SAPPolicyID As String) As Decimal
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        Try
            With sb
                .Append("SELECT ")
                .Append("       SUM(MinAttendanceValue) ")
                .Append("FROM   arSAPDetails ")
                .Append("WHERE  SAPId=?")
            End With

            db.AddParameter("@SAPId", SAPPolicyID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Dim result As Object = db.RunParamSQLScalar(sb.ToString)

            If result Is System.DBNull.Value Then
                Return 0
            Else
                Return Decimal.Parse(result)
            End If


        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetHoursForExternships(ByVal stuEnrollId As String, ByVal cutOffDate As DateTime) As Decimal()
        Dim db As New DataAccess
        Dim rtn(2) As Decimal

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb

            .Append(" select   ")
            .Append("		Coalesce(GBWD.Number, 0.00) as RequiredHours ")
            .Append(" from	arStuEnrollments SE, arGrdBkResults GBR, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT,arClassSections CS     ")
            .Append(" where	   ")
            .Append("		SE.StuEnrollId=GBR.StuEnrollId   ")
            .Append("and	GBR.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId   ")
            .Append("and	GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId   ")
            .Append("and	GCT.SysComponentTypeId=544   ")
            .Append("and	SE.StuEnrollId = ? ")
            .Append("and (select PrgVerId from arStuEnrollments where StuEnrollId=GBR.StuEnrollId) in ")
            .Append(" (select PrgVerId from arProgVerDef where ReqId=(Select Top 1 GrpId from arReqGrpDef where	ReqId=(Select ReqId from arGrdBkWeights where InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId   and GCT.SysComponentTypeId=544 ))  ")
            .Append(" Union Select PrgVerId from arProgVerDef where ReqId=(Select ReqId from arGrdBkWeights where InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId and GCT.SysComponentTypeId=544) ")
            .Append(" ) and GBR.ClsSectionId=CS.ClsSectionId and CS.StartDate<= ?  ")
            .Append("union all  ")
            .Append("select   ")
            .Append("		Coalesce(GBWD.Number, 0.00)  as RequiredHours  ")

            .Append("from	arGrdBkConversionResults GBCR, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT   ")
            .Append("where	   ")
            .Append("	GBCR.GrdComponentTypeId=GCT.GrdComponentTypeId   ")
            .Append("and	GBCR.GrdComponentTypeId=GBWD.GrdComponentTypeId   ")
            .Append("and	GCT.SysComponentTypeId=544   ")
            .Append("and	GBCR.StuEnrollId = ? ")
            .Append("and (select PrgVerId from arStuEnrollments where StuEnrollId=GBCR.StuEnrollId) in (select PrgVerId from arProgVerDef where ReqId=(Select Top 1 GrpId from arReqGrpDef where	ReqId=(Select ReqId from arGrdBkWeights where InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId   and GCT.SysComponentTypeId=544 ))  ")
            .Append(" Union Select PrgVerId from arProgVerDef where ReqId=(Select ReqId from arGrdBkWeights where InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId and GCT.SysComponentTypeId=544) ")
            .Append(" ) ")
        End With

        'StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@CutOffDate", cutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        'StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'Required Hours of externship
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString())
        Dim SchedHrs As Decimal = 0.0
        Dim completedHrs As Decimal = 0.0
        If dr.Read() Then
            SchedHrs = dr(0)
        End If
        dr.Close()
        If SchedHrs > 0 Then
            completedHrs = GetCompletedHoursForExternships(stuEnrollId, cutOffDate)
        End If
        rtn(0) = SchedHrs
        rtn(1) = completedHrs

        Return rtn

    End Function
    Public Function GetCompletedHoursForExternships(ByVal stuEnrollId As String, ByVal cutOffDate As DateTime) As Decimal
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append(" select isnull(sum(HoursAttended),0) as CompletedHours from arExternshipAttendance,arGrdComponentTypes where ")
            .Append(" arExternshipAttendance.stuEnrollid= ? ")
            .Append(" and arExternshipAttendance.GrdComponentTypeid=arGrdComponentTypes.GrdComponentTypeid ")
            .Append(" and arGrdComponentTypes.SysComponentTypeId=544 and attendedDate <= ? ")

        End With

        'StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'cutOffDate
        db.AddParameter("@CutOffDate", cutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        'Required Hours of externship
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString())
        Dim firstRecord As Decimal = 0.0
        If dr.Read() Then
            firstRecord = dr(0)
        End If
        dr.Close()
        Return firstRecord

    End Function
End Class
