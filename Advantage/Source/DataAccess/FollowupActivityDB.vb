'Option Strict On

Imports System.Data
Imports System.Data.OleDb
Imports System.Text

Public Class FollowupActivityDB
    Public Sub AddFollowupActivity(ByVal ActivityAssignment As ActivityAssignmentInfo)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("INSERT INTO cmActivityAssignment")
            .Append("(ActivityAssignmentId, EmployeeId, StudentId, ActivityId, Subject, DueDate,")
            .Append(" StartTime, EndTime, ActivityStatusId, PriorityId, Comments, ParentId, DateCreated)")
            .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)")
        End With
        db.AddParameter("@activityassignmentid", ActivityAssignment.ActivityAssignmentId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@employeeid", ActivityAssignment.EmployeeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@studentid", ActivityAssignment.StudentId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@activityid", ActivityAssignment.ActivityId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@subject", ActivityAssignment.Subject, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@duedate", ActivityAssignment.DueDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@starttime", ActivityAssignment.StartTime, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@endtime", ActivityAssignment.EndTime, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@activitystatusid", 1, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        db.AddParameter("@priorityid", ActivityAssignment.PriorityId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@comments", ActivityAssignment.Comments, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@parentid", ActivityAssignment.ParentId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@datecreated", Now, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

        ' The ChildId for the parent is the ActivitityAssignmentId of the record we just wrote above
        Dim ChildId As New Guid
        ChildId = ActivityAssignment.ActivityAssignmentId
        ' The parent record we are updating is the Guid stored in the ParentId of the record we just wrote above
        Dim UpdateAssignmentId As New Guid
        UpdateAssignmentId = ActivityAssignment.ParentId

        'The next step is to update the parents record with the ChildId
        Dim db2 As New DataAccess
        Dim strSQL2 As New StringBuilder
        With strSQL2
            .Append("UPDATE cmActivityAssignment SET ")
            .Append("ChildId = ?")
            .Append(" WHERE ActivityAssignmentId = ? ")
        End With
        db2.AddParameter("@childid", ChildId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db2.AddParameter("@activityassignmentid", UpdateAssignmentId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db2.RunParamSQLExecuteNoneQuery(strSQL2.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db2.ClearParameters()
        strSQL2.Remove(0, strSQL.Length)

    End Sub


    Public Function GetDropDownList(ByVal EmployeeId As Guid) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da1 As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder
        With strSQLString
            .Append("Select StudentID, LastName, FirstName from arStudent")
        End With
        db.OpenConnection()
        Try
            da1 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da1.Fill(ds, "StudentList")
        Catch ex As System.Exception

        End Try
        Dim da2 As New OleDbDataAdapter
        strSQLString.Remove(0, strSQLString.Length)

        With strSQLString
            .Append("Select PriorityID, PriorityDescrip from cmPriority")
        End With
        'db.OpenConnection()
        Try
            da2 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da2.Fill(ds, "PriorityList")
        Catch ex As System.Exception

        End Try

        strSQLString.Remove(0, strSQLString.Length)

        Dim da3 As New OleDbDataAdapter

        With strSQLString
            .Append("Select ActivityID, ActivityDescrip from cmActivities")
        End With
        Try
            da3 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da3.Fill(ds, "ActivityList")
        Catch ex As System.Exception

        End Try

        strSQLString.Remove(0, strSQLString.Length)

        Dim da4 As New OleDbDataAdapter

        With strSQLString
            .Append("Select ActivityStatusID, ActivityStatusDescrip from cmActivityStatus")
        End With
        Try
            da4 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da4.Fill(ds, "ActivityStatusList")
        Catch ex As System.Exception

        End Try

        strSQLString.Remove(0, strSQLString.Length)

        Dim da5 As New OleDbDataAdapter

        With strSQLString
            .Append("Select TimeIntervalID, TimeIntervalDescrip from cmTimeInterval order by TimeIntervalDescrip")
        End With
        Try
            da5 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da5.Fill(ds, "TimeIntervalList")
        Catch ex As System.Exception

        End Try

        strSQLString.Remove(0, strSQLString.Length)

        Dim da6 As New OleDbDataAdapter

        With strSQLString
            .Append("SELECT DISTINCT b.EmpId, c.LastName, c.FirstName ")
            .Append("FROM hrEmpAssActGrps a, hrEmpRoles b, hrEmployees c ")
            .Append("WHERE (a.EmpId = ?) and ")
            .Append("(b.EmpId = c.EmpId) and ")
            .Append("(a.RoleId = b.RoleId) ")
        End With
        db.AddParameter("@EmpId", EmployeeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            da6 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da6.Fill(ds, "ActivityEmpList")
        Catch ex As System.Exception

        End Try

        strSQLString.Remove(0, strSQLString.Length)
        db.CloseConnection()
        Return ds
    End Function
    Public Sub UpdateActivityAssignment(ByVal ActivityAssignmentId As Guid, ByVal ChildId As Guid)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("UPDATE cmActivityAssignment SET ")
            .Append("ChildId = ?")
            .Append(" WHERE ActivityAssignmentId = ? ")
        End With
        'db.OpenConnection()
        db.AddParameter("@childid", ChildId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@activityassignmentid", ActivityAssignmentId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
        'db.CloseConnection()
    End Sub
    Public Function GetOneActivityAssignment(ByVal ParentID As Guid) As Guid
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim row As DataRow
        Dim tbl As New DataTable
        Dim ChildId As New Guid
        Dim strSQLString As New StringBuilder
        With strSQLString
            .Append("Select * from cmActivityAssignment")
            .Append(" where ActivityAssignmentId = ? ")
        End With
        Dim objSingleAssignment As New OleDbDataAdapter
        db.OpenConnection()
        db.AddParameter("@AssignmentId", ParentID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            objSingleAssignment = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        objSingleAssignment.Fill(ds, "SingleAssignment")
        db.ClearParameters()
        If ds.Tables("SingleAssignment").Rows.Count > 0 Then
            For Each row In ds.Tables("SingleAssignment").Rows
                ChildId = DirectCast(row("ActivityAssignmentId"), Guid)
            Next
        End If
        db.CloseConnection()
        Return ChildId
    End Function
End Class
