﻿
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' InstructionTypeDB.vb
'
' InstructionTypeDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class InstructionTypeDB

    '08/25/2011 JRobinson Clock Hour Project - Add new methods to support Instruction Type

    ' 08/03/2011 JRobinson Clock Hour Project - Add new method to remove Default for Instruction Types
    Public Function RemoveDefaultFromInstructionTypes(ByVal InstructionTypeId As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            With sb
                .Append("UPDATE arInstructionType ")
                .Append("  SET IsDefault = 0 ")
                .Append("WHERE InstructionTypeId <> ? ")
                .Append("AND IsDefault = 1 ")
            End With

            db.AddParameter("@InstructionTypeId", InstructionTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            sb.Clear()

            With sb
                .Append("SELECT count(*) FROM arInstructionType WHERE IsDefault = 1 ")
            End With

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return "No Instruction Type has been selected as the default"
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function CheckInstructionTypeDup(ByVal InstructionTypeId As String, ByVal InstructionTypeCode As String, ByVal InstructionTypeDescrip As String) As String
        'Dim db As New FAME.DataAccessLayer.SQLDataAccess
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try
            If InstructionTypeId = "00000000-0000-0000-0000-000000000000" Then
                sb.Append("SELECT count(*) FROM arInstructionType WHERE InstructionTypeCode = ? OR InstructionTypeDescrip = ? ")
                db.AddParameter("@InstructionTypeCode", InstructionTypeCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@InstructionTypeDescrip", InstructionTypeDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                sb.Append("SELECT count(*) FROM arInstructionType WHERE (InstructionTypeCode = ? OR InstructionTypeDescrip = ? ) AND InstructionTypeId <> ? ")
                db.AddParameter("@InstructionTypeCode", InstructionTypeCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@InstructionTypeDescrip", InstructionTypeDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@InstructionTypeId", New Guid(InstructionTypeId), DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            End If


            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return "Instruction Type Code or Description already exists"
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
        '    If InstructionTypeId = "00000000-0000-0000-0000-000000000000" Then
        '        sb.Append("SELECT count(*) FROM arInstructionType WHERE InstructionTypeCode = ? OR InstructionTypeDescrip = ? ")
        '        db.AddParameter("@InstructionTypeCode", InstructionTypeCode, SqlDbType.VarChar, 10, ParameterDirection.Input)
        '        db.AddParameter("@InstructionTypeDescrip", InstructionTypeDescrip, SqlDbType.VarChar, 50, ParameterDirection.Input)
        '    Else
        '        'sb.Append("SELECT count(*) FROM arInstructionType WHERE (InstructionTypeCode = ? AND InstructionTypeId <> ? ) OR (InstructionTypeDescrip = ? AND InstructionTypeId <> ? ) ")
        '        sb.Append("SELECT count(*) FROM arInstructionType WHERE (InstructionTypeCode = ? AND InstructionTypeId <> ? ) ")
        '        db.AddParameter("@InstructionTypeCode", InstructionTypeCode, SqlDbType.VarChar, 10, ParameterDirection.Input)
        '        db.AddParameter("@InstructionTypeId", New Guid(InstructionTypeId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '        'db.AddParameter("@InstructionTypeDescrip", InstructionTypeDescrip, SqlDbType.VarChar, 50, ParameterDirection.Input)
        '        'db.AddParameter("@InstructionTypeId1", New Guid(InstructionTypeId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        '    End If


        '    '   execute the query
        '    Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

        '    '   
        '    If rowCount = 0 Then
        '        '   return without errors
        '        Return ""
        '    Else
        '        Return "Instruction Type Code or Description already exists"
        '    End If

        'Catch ex As System.Exception
        '    Throw New BaseException(ex.Message)
        'Finally
        '    db.ClearParameters()
        '    db.CloseConnection()
        'End Try

    End Function

    Public Function GetInstructionTypesByStatus(ByVal Status As String) As DataTable

        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")


        db.AddParameter("@Status", Status, SqlDbType.VarChar, 50, ParameterDirection.Input)

        Try
            ds = db.RunParamSQLDataSet_SP("dbo.USP_AR_InstructionType_GetList")
        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return ds.Tables(0)
    End Function

    Public Function GetAllInstructionTypeIds() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT  InstructionTypeId,  ")
            .Append("        InstructionTypeCode, ")
            .Append("        InstructionTypeDescrip, ")
            .Append("        IT.StatusID, ")
            .Append("        (Case ST.Status when 'Active' then 1 else 0 end) As Status ")
            .Append("FROM     arInstructionType IT, syStatuses ST ")
            .Append("WHERE    IT.StatusId = ST.StatusId ")
            .Append("ORDER BY ST.Status,IT.InstructionTypeDescrip asc")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function

    Public Function GetInstructionTypeInfo(ByVal InstructionTypeId As String) As InstructionTypeInfo

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")



        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT  InstructionTypeId,  ")
            .Append("    InstructionTypeCode, ")
            .Append("    InstructionTypeDescrip, ")
            .Append("    StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=IT.StatusId) As Status, ")
            .Append("    CampGrpId, ")
            .Append("    IsDefault, ")
            .Append("    ModUser, ")
            .Append("    ModDate ")
            .Append("FROM  arInstructionType IT ")
            .Append("WHERE InstructionTypeId= ? ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@InstructionTypeId", New Guid(InstructionTypeId), DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)

        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim InstructionTypeInfo As New InstructionTypeInfo

        While dr.Read()

            '   set properties with data from DataReader
            With InstructionTypeInfo
                .InstructionTypeId = InstructionTypeId
                .OrigInstructionTypeId = InstructionTypeId
                .InstructionTypeCode = dr("InstructionTypeCode")
                .InstructionTypeDescrip = dr("InstructionTypeDescrip")
                .IsInDB = True
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("IsDefault") Is System.DBNull.Value) Then
                    .IsDefault = CType(dr("IsDefault"), Boolean).ToString
                Else
                    .IsDefault = False
                End If
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()

        '   Return InstructionTypeInfo
        Return InstructionTypeInfo

    End Function

    Public Function UpdateInstructionTypeInfo(ByVal InstructionTypeInfo As InstructionTypeInfo, ByVal UserName As String) As String
        Dim db As New SQLDataAccess
        Dim sb As New System.Text.StringBuilder
        Dim dt As New DataTable

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            'Set the connection string
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            db.AddParameter("@InstructionTypeId", New Guid(InstructionTypeInfo.InstructionTypeId.ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@InstructionTypeCode", InstructionTypeInfo.InstructionTypeCode, SqlDbType.VarChar, 10, ParameterDirection.Input)
            db.AddParameter("@InstructionTypeDescrip", InstructionTypeInfo.InstructionTypeDescrip, SqlDbType.VarChar, 50, ParameterDirection.Input)
            db.AddParameter("@StatusId", New Guid(InstructionTypeInfo.StatusId.ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@IsDefault", InstructionTypeInfo.IsDefault, SqlDbType.Bit, , ParameterDirection.Input)
            db.AddParameter("@UserName", UserName, SqlDbType.VarChar, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_AR_InstructionType_Update")


        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try

    End Function

    Public Function AddInstructionTypeInfo(ByVal InstructionTypeInfo As InstructionTypeInfo, ByVal UserName As String) As String

        Dim db As New SQLDataAccess
        Dim sb As New System.Text.StringBuilder
        Dim dt As New DataTable


        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            'Set the connection string
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            db.AddParameter("@InstructionTypeId", New Guid(InstructionTypeInfo.InstructionTypeId.ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@InstructionTypeCode", InstructionTypeInfo.InstructionTypeCode, SqlDbType.VarChar, 10, ParameterDirection.Input)
            db.AddParameter("@InstructionTypeDescrip", InstructionTypeInfo.InstructionTypeDescrip, SqlDbType.VarChar, 50, ParameterDirection.Input)
            db.AddParameter("@IsDefault", InstructionTypeInfo.IsDefault, SqlDbType.Bit, , ParameterDirection.Input)
            db.AddParameter("@UserName", UserName, SqlDbType.VarChar, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_AR_InstructionType_Insert")

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try



    End Function

    Public Function DeleteInstructionTypeInfo(ByVal strInstructionTypeId As String, ByVal UserName As String) As String

        Dim db As New SQLDataAccess
        Dim sb As New System.Text.StringBuilder
        Dim dt As New DataTable
        Dim rtn As String = String.Empty

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            'Set the connection string
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


            db.AddParameter("@InstructionTypeId", New Guid(strInstructionTypeId.ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_AR_InstructionType_Delete")

        Catch ex As System.Exception
            'Throw New BaseException(ex.Message)
            Return ex.Message
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return rtn
    End Function

    Public Function GetDefaultInstructionTypeId() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT TOP 1 InstructionTypeId AS InstructionTypeId, InstructionTypeDescrip AS InstructionTypeDescrip FROM dbo.arInstructionType WHERE IsDefault=1  ")
        End With

        Return db.RunSQLDataSet(sb.ToString)

    End Function

End Class
