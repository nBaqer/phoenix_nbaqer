Imports FAME.Advantage.Common

Public Class EmployeeSearchDB

    Public Function EmployeeSearchResults(ByVal strEmpID As String, ByVal strLastName As String, ByVal strFirstName As String, ByVal strSSN As String, ByVal strStatus As String, ByVal campusId As String) As DataSet

        '   connect to the database
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb

            .Append(" SELECT   EmpID, ")
            .Append(" LastName,FirstName, ")
            .Append(" SSN,StatusID ")
            .Append(" FROM  hrEmployees  where CampusId=? ")

            'use the campusId 
            Dim andOrOrOperator As String = " AND "
            db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            If Not strEmpID = "" Then
                .Append(andOrOrOperator)
                .Append(" EmpID = ? ")
                db.AddParameter("@EmpID", strEmpID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            '   check if the selection is "All Campus Groups"
            If Not strLastName = "" Then
                .Append(andOrOrOperator)
                .Append(" LastName like '%' + ? + '%'")
                db.AddParameter("@LastName", strLastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            If Not strFirstName = "" Then
                .Append(andOrOrOperator)
                .Append(" FirstName like '%' + ? + '%'")
                db.AddParameter("@FirstName", strFirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            If Not strSSN = "" Then
                .Append(andOrOrOperator)
                .Append(" SSN like  '%' + ? + '%'")
                db.AddParameter("@SSN", strSSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            If Not strStatus = Guid.Empty.ToString Then
                .Append(andOrOrOperator)
                .Append(" StatusID = ? ")
                db.AddParameter("@StatusID", strStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If
            .Append(" ORDER BY LastName ")
        End With
        'Execute the query and return Dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function GetEmployeeID(ByVal EmployeeCode As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim EmployeeID As String
        Dim EmployeeDescrip As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'connect to the database

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            'With subqueries
            .Append("SELECT     B.EmpID,B.FirstName + ' ' + B.LastName As EmployeeDescrip ")
            .Append("FROM  hrEmployees B ")
            .Append("WHERE B.Code = ? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@code", EmployeeCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read()
            'set properties with data from DataReader
            EmployeeID = dr("EmpID").ToString
            EmployeeDescrip = dr("EmployeeDescrip")
        End While

        If Not dr.IsClosed Then dr.Close()

        If EmployeeID <> "" Then
            Return EmployeeID & "," & EmployeeDescrip
        Else
            Return "None"
        End If
    End Function

    Public Function GetEmployeeCount(ByVal EmployeeCode As String) As Integer
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim intCount As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'connect to the database

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        With sb
            'With subqueries
            .Append("SELECT  Count(*) as EmployeeCount ")
            .Append("FROM  hrEmployees B ")
            .Append("WHERE B.Code = ? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@code", EmployeeCode, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read()
            'set properties with data from DataReader
            intCount = dr("EmployeeCount")
        End While

        If Not dr.IsClosed Then dr.Close()

        Return intCount
    End Function
    Public Function GetAllRolesUsedByEmployees() As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New System.Text.StringBuilder
        With sb
            .Append("SELECT     Distinct ")
            .Append("           R.RoleId, R.Role ")
            .Append("FROM       syRoles R ")
            '.Append("WHERE      R.RoleId=ER.RoleId ")
            .Append("ORDER BY   R.Role")
        End With

        '   Execute the query
        Dim ds As DataSet = db.RunSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    Public Function GetAllEmployeeEmailsByEmployeeRole(ByVal rolesIdList(,) As String) As DataSet

        '   if the roles list is ... return nothing
        If rolesIdList.GetLength(1) = 0 Then Return Nothing

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build a string with all roleIds
        For i As Integer = 0 To rolesIdList.GetLength(1) - 1
            If i > 0 Then sb.Append(",")
            sb.Append("'")
            sb.Append(rolesIdList(0, i))
            sb.Append("'")
        Next
        Dim rolesList As String = sb.ToString

        '   build the sql query
        sb = New StringBuilder
        With sb
            '.Append("SELECT ")
            '.Append("           count(*) As Count ")
            '.Append("FROM       hrEmpContactInfo EC, hrEmpRoles ER ")
            '.Append("WHERE      EC.EmpId=ER.EmpId ")
            '.Append("AND        Email is not Null ")
            '.Append("AND        ER.RoleId IN (" + rolesList + ") ")
            .Append("SELECT     Distinct ")
            .Append("           Coalesce(EC.WorkEmail, EC.HomeEmail) as Email, ")
            .Append("           (E.FirstName + ' ' + E.LastName) Name, ")
            .Append("FROM       hrEmpContactInfo EC, syUsersRolesCmpGrps ER, hrEmployees E ")
            .Append("WHERE      EC.EmpId=ER.EmpId ")
            .Append("AND        ER.EmpId=E.EmpId ")
            .Append("AND        (EC.WorkEmail is not null or EC.HomeEmail is not null) ")
            .Append("AND        ER.RoleId IN (" + rolesList + ") ")
            '.Append("ORDER BY   Email")
        End With

        '   Execute the query
        Return db.RunSQLDataSet(sb.ToString)

        ''   Execute the query
        'Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString)

        ''   get the record count in order to set the maximum dimension of the emailList 
        'dr.Read()
        'Dim emailList(CType(dr("Count"), Integer)) As String

        ''   build an string array with emails. No duplicate Emails
        'dr.NextResult()
        'Dim j As Integer = 0
        'While dr.Read
        '    emailList(j) = dr("Email")
        '    j += 1
        'End While

        ''   resize the array to adjust for duplicated Emails
        'If j > 0 Then
        '    ReDim Preserve emailList(j - 1)
        'Else
        '    emailList = Nothing
        'End If

        ''Close Connection
        'db.CloseConnection()

        ''Return string array
        'Return emailList

    End Function
End Class
