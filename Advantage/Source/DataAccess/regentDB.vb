Imports FAME.AdvantageV1.Common.RegentAdvantageIntegration
Imports System.Data
Imports System.Xml
Imports System.IO
Imports FAME.Advantage.Common

Public Class regentDB
#Region "XML Functions"
    Public Sub GenerateXMLForStudentCreation(ByVal StudentId As String, ByVal CampusId As String)
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'In advantage students are created when a lead is enrolled.At that 
        'point of time award,academic and financial aid data is not available
        'so these elements are left out of student tag during serialization.

        'However the code will be added to Student Awards,academic session
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Dim dtgetStudentDetails As New DataTable
        Dim dtgetStudentAddress As New DataTable
        Dim dtgetStudentPhone As New DataTable
        Dim dtGetEmail As New DataTable
        Dim dtGetActivity As New DataTable
        Dim dtgetStudentMissingRequirement As New DataTable
        Dim dtgetStudentAcademic As New DataTable
        Dim strPhone As String = ""

        'Create instance of Students,StudentType,addressType,nameType objects
        Dim regStudents As students = New students()
        Dim regStudent As studentType = New studentType
        Dim regAddressType As addressType = New addressType
        Dim regNameType As nameType = New nameType
        Dim regEmailType As emailType = New emailType
        Dim regDemographicType As demographicType = New demographicType
        Dim regTrackingType As trackingType = New trackingType
        Dim regActivityType As activityType = New activityType
        Dim regCommentType As commentType = New commentType
        Dim regWorkEmailType As emailType = New emailType
        Dim regFinancialAidType As financialAidType = New financialAidType
        Dim regAcademicType As academicType = New academicType

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Create instances of Student,address and name types 
        regStudents.student = Array.CreateInstance(regStudent.GetType, 1)
        regStudent.address = Array.CreateInstance(regAddressType.GetType, 1)
        regStudent.name = Array.CreateInstance(regNameType.GetType, 1)
        regStudent.email = Array.CreateInstance(regEmailType.GetType, 2)


        regStudent.comment = Array.CreateInstance(regCommentType.GetType, 1)
        regStudent.financialAid = Array.CreateInstance(regFinancialAidType.GetType, 1)
        'regStudent.academic = Array.CreateInstance(regAcademicType.GetType, 1)

        dtgetStudentDetails = getStudentDetails(StudentId)
        dtgetStudentAddress = getStudentAddress(StudentId)
        dtgetStudentPhone = getStudentPhone(StudentId)
        dtgetStudentMissingRequirement = getAllMissingRequirements(StudentId, CampusId)
        dtGetActivity = getStudentActivityData(StudentId)
        dtgetStudentAcademic = getStudentAcademicData(StudentId)


        Dim intCountVal As Integer = 0
        Try
            intCountVal = dtgetStudentAcademic.Rows.Count
        Catch ex As System.Exception
        End Try
        If intCountVal >= 1 Then
            regStudent.academic = Array.CreateInstance(regAcademicType.GetType, intCountVal)
        End If



        If dtgetStudentPhone.Rows.Count >= 1 Then
            strPhone = CType(dtgetStudentPhone.Rows(0)("Phone"), String)
        End If
        Try
            regAddressType = PopulateStudentAddress(dtgetStudentAddress, strPhone)
        Catch ex As System.Exception
        End Try
        Try
            regNameType = PopulateStudentName(dtgetStudentDetails)
        Catch ex As System.Exception
        End Try
        Try
            regEmailType = PopulateEmail(dtgetStudentDetails)
        Catch ex As System.Exception
        End Try
        Try
            regWorkEmailType = PopulateWORKEmail(dtgetStudentDetails)
        Catch ex As System.Exception
        End Try
        Try
            regDemographicType = PopulateDemographicData(dtgetStudentDetails)
        Catch ex As System.Exception
        End Try
        Try
            regCommentType = PopulateComment(dtgetStudentDetails)
        Catch ex As System.Exception
        End Try
        'Try
        '    If dtgetStudentAcademic.Rows.Count >= 1 Then
        '        regAcademicType = PopulateAcademic(dtgetStudentAcademic)
        '    End If
        'Catch ex As System.Exception
        'End Try
        'regTrackingType = PopulateDocumentTracking(dtgetStudentMissingRequirement)
        ' regFinancialAidType = PopulateFinancialAid(dtgetStudentDetails)



        Dim dr As DataRow
        Dim strSession As String
        dr = dtgetStudentDetails.Rows(0)
        Dim strNumberOfCampus As String = MyAdvAppSettings.AppSettings("NumberofCampuses")
        If strNumberOfCampus.Length < 10 Then
            strNumberOfCampus = "0" + strNumberOfCampus.ToString
        End If
        ' If Not dr("SessionApplied") Is System.DBNull.Value Then regTrackingType.session = dr("SessionApplied") Else regTrackingType.session = "NA"
        If Not dr("SessionApplied") Is System.DBNull.Value Then regActivityType.session = dr("SessionApplied") Else regActivityType.session = "NA"


        Dim intMissingRequirement As Integer = 0
        Dim intActivity As Integer = 0
        Try
            intMissingRequirement = dtgetStudentMissingRequirement.Rows.Count
        Catch ex As System.Exception
            intMissingRequirement = 0
        End Try

        Try
            intActivity = dtGetActivity.Rows.Count
        Catch ex As System.Exception
            intActivity = 0
        End Try
        Dim strSSN As String = ""
        Dim strGradDate As String = ""
        With regStudent
            .name(0) = regNameType
            Try
                .address(0) = regAddressType
            Catch ex As System.Exception
            End Try
            Try
                .email(0) = regEmailType
            Catch ex As System.Exception
            End Try
            Try
                .email(1) = regWorkEmailType
            Catch ex As System.Exception
            End Try
            Try
                .demographic = regDemographicType
            Catch ex As System.Exception
            End Try
            ' .financialAid(0) = regFinancialAidType
            Try
                If dtgetStudentAcademic.Rows.Count >= 1 Then
                    Dim intGetRowCount As Integer = 0
                    Dim z As Integer
                    intGetRowCount = dtgetStudentAcademic.Rows.Count
                    For z = 0 To intGetRowCount - 1
                        Try
                            .academic(z) = PopulateAcademic(dtgetStudentAcademic, z)
                        Catch ex As System.Exception
                        End Try
                    Next
                End If
            Catch ex As System.Exception
            End Try
            Try
                If intMissingRequirement >= 1 Then
                    Dim intCounter As Integer = 0
                    regStudent.tracking = Array.CreateInstance(regTrackingType.GetType, intMissingRequirement)
                    For intCounter = 0 To intMissingRequirement - 1
                        regTrackingType = PopulateDocumentTracking(dtgetStudentMissingRequirement, dr("SessionApplied"), intCounter)
                        .tracking(intCounter) = regTrackingType
                    Next
                End If
            Catch ex As System.Exception
            End Try
            Try
                If intActivity >= 1 Then
                    Dim intCounter As Integer = 0
                    regStudent.activity = Array.CreateInstance(regActivityType.GetType, intActivity)
                    For intCounter = 0 To intActivity - 1
                        regActivityType = PopulateActivity(dtGetActivity, dr("SessionApplied"), intCounter)
                        .activity(intCounter) = regActivityType
                    Next
                End If
            Catch ex As System.Exception
            End Try
            Try
                .comment(0) = regCommentType
            Catch ex As System.Exception
            End Try


            .id = dr("SSN")
            strSSN = dr("SSN")

            Try
                strGradDate = CDate(dr("GraduationDate")).ToString("yyyy-MM-dd")
            Catch ex As System.Exception
                strGradDate = ""
            End Try
            If Not dr("GraduationDate") Is System.DBNull.Value Then
                .graduationDate = CDate(dr("GraduationDate")).ToString("yyyy-MM-dd")
                .graduationDateSpecified = True
            Else
                .graduationDate = ""
                .graduationDateSpecified = False
            End If

            .status = "AC"
            If Not dr("StateOfResidence") Is System.DBNull.Value Then .stateOfResidence = dr("StateOfResidence") Else .stateOfResidence = ""
            .institutionCode = strNumberOfCampus
            If Not dr("SessionApplied") Is System.DBNull.Value Then .sessionApplied = dr("SessionApplied") Else .sessionApplied = ""
            strSession = .sessionApplied
            .sessionRemaining = "0.00"
            If Not dr("SessionStartTerm") Is System.DBNull.Value Then .sessionStart = dr("SessionStartTerm") Else .sessionStart = ""
            If Not dr("SessionEndTerm") Is System.DBNull.Value Then .sessionEnd = dr("SessionEndTerm") Else .sessionEnd = ""
            .eligibleCode = "A0"
            If Not dr("SessionApplied") Is System.DBNull.Value Then .eligibleSession = dr("SessionApplied") Else .eligibleSession = ""
            .hoursRemaining = "0"
            If Not dr("CurriculumCode") Is System.DBNull.Value Then .curriculumCode = dr("CurriculumCode") Else .curriculumCode = ""
            .miscellaneousCode1 = ""
            .miscellaneousCode2 = ""
            .miscellaneousCode3 = ""
            .miscellaneousCode4 = ""
            ' If Not dr("EntranceInterviewDate") Is System.DBNull.Value Then .entranceInterviewDate = CDate(dr("EntranceInterviewDate")).ToString("yyyy-MM-dd") Else .entranceInterviewDate = ""
            'If Not dr("ExitInterviewDate") Is System.DBNull.Value Then .exitInterviewDate = CDate(dr("ExitInterviewDate")).ToString("yyyy-MM-dd") Else .exitInterviewDate = ""
            .acgEligibilityReasonCode = "00"
            'If Not dr("HighSchoolProgramCode") Is System.DBNull.Value Then .highSchoolProgramCode = dr("HighSchoolProgramCode") Else .highSchoolProgramCode = ""
            .nameType = "STUD"
            .nameCode = "MSTR"
            .addressType = "HOME"
            .addressCode = "HOME"
            .emailType = "HOME"
            .emailCode = "HOME"
            If Not dr("lenderId") Is System.DBNull.Value Then .lenderId = dr("lenderId") Else .lenderId = ""
        End With
        regStudents.student(0) = regStudent

        Dim serializer As System.Xml.Serialization.XmlSerializer
        Dim getFileName As String = AdvantageCommonValues.getStudentBatchFileName()
        Dim file As System.IO.FileStream = New System.IO.FileStream(getFileName, IO.FileMode.OpenOrCreate)
        serializer = New System.Xml.Serialization.XmlSerializer(GetType(students))
        serializer.Serialize(file, regStudents)
        file.Close()

        'Try
        '    Dim xmlDoc As XmlDocument
        '    xmlDoc.Load(getFileName)
        '    xmlDoc = AppendGraduationDate(xmlDoc, strGradDate, strSSN)
        '    xmlDoc.Save(getFileName)
        'Catch ex As System.Exception
        'End Try
    End Sub
    Public Sub UpdateXMLForStudentCreation(ByVal StudentId As String, _
                                           ByVal strFileName As String, ByVal campusId As String)
        Dim dtgetStudentDetails As New DataTable
        Dim dtgetStudentAddress As New DataTable
        Dim dtgetStudentPhone As New DataTable
        Dim dtGetAcademicData As New DataTable
        Dim dtGetEmail As New DataTable
        Dim dtAwardData As New DataTable
        Dim dtGetActivity As New DataTable
        Dim strPhone As String = ""
        Dim dtgetStudentMissingRequirement As New DataTable
        dtgetStudentMissingRequirement = getAllMissingRequirements(StudentId, campusId)
        dtgetStudentDetails = getStudentDetails(StudentId)
        dtgetStudentAddress = getStudentAddress(StudentId)
        dtgetStudentPhone = getStudentPhone(StudentId)
        dtAwardData = getStudentAwardData(StudentId)
        dtGetAcademicData = getStudentAcademicData(StudentId)
        dtGetActivity = getStudentActivityData(StudentId)

        If dtgetStudentPhone.Rows.Count >= 1 Then
            strPhone = CType(dtgetStudentPhone.Rows(0)("Phone"), String)
        End If

        Dim drSession As DataRow
        Dim strSession As String = ""

        drSession = dtgetStudentDetails.Rows(0)
        strSession = drSession("SessionApplied").ToString

        Dim xmlDoc As XmlDocument = New XmlDocument()
        If (File.Exists(strFileName)) Then
            xmlDoc.Load(strFileName)
            Dim elmRoot As XmlElement = xmlDoc.DocumentElement
            Try
                AppendStudentAttribute(xmlDoc, elmRoot, dtgetStudentDetails)
            Catch ex As System.Exception
            End Try
            Try
                AppendNameAttribute(xmlDoc, elmRoot, dtgetStudentDetails)
            Catch ex As System.Exception
            End Try
            Try
                AppendAddressAttribute(xmlDoc, elmRoot, dtgetStudentAddress, strPhone)
            Catch ex As System.Exception
            End Try
            Try
                AppendEmailAttribute(xmlDoc, elmRoot, dtgetStudentDetails)
            Catch ex As System.Exception
            End Try
            Try
                AppendWorkAttribute(xmlDoc, elmRoot, dtgetStudentDetails)
            Catch ex As System.Exception
            End Try
            Try
                AppendDemographicAttribute(xmlDoc, elmRoot, dtgetStudentDetails)
            Catch ex As System.Exception
            End Try
            Try
                AppendAcademicAttribute(xmlDoc, elmRoot, dtGetAcademicData)
            Catch ex As System.Exception
            End Try
            Try
                AppendDocumentAttribute(xmlDoc, elmRoot, dtgetStudentMissingRequirement, strSession)
            Catch ex As System.Exception
            End Try
            Try
                AppendCommentAttribute(xmlDoc, elmRoot, dtgetStudentDetails)
            Catch ex As System.Exception
            End Try
            xmlDoc.Save(strFileName)
        End If
    End Sub
    Private Function AppendStudentAttribute(ByVal xmlDoc As XmlDocument, ByVal elmRoot As XmlElement, ByVal dtStudentDetails As DataTable) As XmlDocument
        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "student", "http://www.regenteducation.com/carbon/batchload")
        Dim dr As DataRow
        Dim strCurrCode As String = ""
        Dim strDegreeCode As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim strNumberOfCampus As String = MyAdvAppSettings.AppSettings("numberofcampuses")
        dr = dtStudentDetails.Rows(0)
        If Not dr("CurriculumCode") Is System.DBNull.Value Then strCurrCode = Mid(dr("CurriculumCode"), 1, 6) Else strCurrCode = ""
        With elmNew
            .SetAttribute("id", dr("SSN"))
            If Not dr("GraduationDate") Is System.DBNull.Value Then
                .SetAttribute("graduationDate", CDate(dr("GraduationDate")).ToString("yyyy-MM-dd"))
                '.SetAttribute("graduationDateSpecified", True)
            Else
                .SetAttribute("graduationDate", "")
                ' .SetAttribute("graduationDateSpecified", False)
            End If
            If strNumberOfCampus.Length = 1 Then
                strNumberOfCampus = "0" & strNumberOfCampus
            End If

            .SetAttribute("status", "AC")
            If Not dr("StateOfResidence") Is System.DBNull.Value Then .SetAttribute("stateOfResidence", dr("StateOfResidence")) Else .SetAttribute("stateOfResidence", "")
            .SetAttribute("institutionCode", strNumberOfCampus)
            If Not dr("SessionApplied") Is System.DBNull.Value Then .SetAttribute("sessionApplied", dr("SessionApplied")) Else .SetAttribute("sessionApplied", "")
            '.SetAttribute("strSession", dr("SessionApplied"))
            .SetAttribute("sessionRemaining", "0.00")
            If Not dr("SessionStartTerm") Is System.DBNull.Value Then .SetAttribute("sessionStart", dr("SessionStartTerm")) Else .SetAttribute("sessionStart", "")
            If Not dr("SessionEndTerm") Is System.DBNull.Value Then .SetAttribute("sessionEnd", dr("SessionEndTerm")) Else .SetAttribute("sessionEnd", "")
            .SetAttribute("eligibleCode", "A0")
            If Not dr("SessionApplied") Is System.DBNull.Value Then .SetAttribute("eligibleSession", dr("SessionApplied")) Else .SetAttribute("eligibleSession", "")
            .SetAttribute("hoursRemaining", "0")
            If Not dr("CurriculumCode") Is System.DBNull.Value Then .SetAttribute("curriculumCode", strCurrCode) Else .SetAttribute("curriculumCode", "")
            .SetAttribute("miscellaneousCode1", "")
            .SetAttribute("miscellaneousCode2", "")
            .SetAttribute("miscellaneousCode3", "")
            .SetAttribute("miscellaneousCode4", "")
            ' If Not dr("EntranceInterviewDate") Is System.DBNull.Value Then .SetAttribute("entranceInterviewDate", CDate(dr("EntranceInterviewDate")).ToString("yyyy-MM-dd")) Else .SetAttribute("entranceInterviewDate", "")
            'If Not dr("ExitInterviewDate") Is System.DBNull.Value Then .SetAttribute("exitInterviewDate", CDate(dr("ExitInterviewDate")).ToString("yyyy-MM-dd")) Else .SetAttribute("exitInterviewDate", "")
            .SetAttribute("acgEligibilityReasonCode", "00")
            'If Not dr("HighSchoolProgramCode") Is System.DBNull.Value Then .SetAttribute("highSchoolProgramCode", Mid(dr("HighSchoolProgramCode"), 1, 6)) Else .SetAttribute("highSchoolProgramCode", "")
            .SetAttribute("nameType", "STUD")
            .SetAttribute("nameCode", "MSTR")
            .SetAttribute("addressType", "HOME")
            .SetAttribute("addressCode", "HOME")
            .SetAttribute("emailType", "HOME")
            .SetAttribute("emailCode", "HOME")
            If Not dr("lenderId") Is System.DBNull.Value Then .SetAttribute("lenderId", Mid(dr("lenderId"), 1, 6)) Else .SetAttribute("lenderId", "")
        End With
        elmRoot.AppendChild(elmNew)
    End Function
    Private Function AppendStudentAttributeToExistingStudents(ByVal xmlDoc As XmlDocument, ByVal elmRoot As XmlElement, ByVal dtStudentDetails As DataTable) As XmlDocument
        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "Student", "http://www.regenteducation.com/carbon/batchload")
        Dim dr As DataRow

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim strNumberOfCampus As String = MyAdvAppSettings.AppSettings("numberofcampuses")
        dr = dtStudentDetails.Rows(0)
        'elmNew.SetAttribute("id", dr("SSN"))
        'elmNew.SetAttribute("graduationDate", dr("GraduationDate"))
        'elmNew.SetAttribute("status", "AC")
        'elmNew.SetAttribute("stateOfResidence", Mid(dr("StateOfResidence"), 1, 2))
        'elmNew.SetAttribute("institutionCode", strNumberOfCampus)
        'elmNew.SetAttribute("sessionApplied", dr("SessionApplied"))
        'elmNew.SetAttribute("sessionRemaining", "0.00")
        'elmNew.SetAttribute("sessionStart", dr("SessionStartTerm"))
        'elmNew.SetAttribute("sessionEnd", dr("SessionEndTerm"))
        'elmNew.SetAttribute("eligibleCode", "A0")
        'elmNew.SetAttribute("eligibleSession", dr("SessionApplied"))
        'elmNew.SetAttribute("hoursRemaining", "")
        'elmNew.SetAttribute("curriculumCode", Mid(dr("CurriculumCode"), 1, 6))
        'elmNew.SetAttribute("miscellaneousCode1", "")
        'elmNew.SetAttribute("miscellaneousCode2", "")
        'elmNew.SetAttribute("miscellaneousCode3", "")
        'elmNew.SetAttribute("miscellaneousCode4", "")
        'elmNew.SetAttribute("entranceInterviewDate", dr("EntranceInterviewDate"))
        'elmNew.SetAttribute("exitInterviewDate", dr("ExitInterviewDate"))
        'elmNew.SetAttribute("acgEligibilityReasonCode ", "")
        'elmNew.SetAttribute("highSchoolProgramCode", Mid(dr("HighSchoolProgramCode"), 1, 6))
        'elmNew.SetAttribute("nameType", "STUD")
        'elmNew.SetAttribute("nameCode", "MSTR")
        'elmNew.SetAttribute("addressType", "STUD")
        'elmNew.SetAttribute("addressCode", "MSTR")
        'elmNew.SetAttribute("emailType", "STUD")
        'elmNew.SetAttribute("emailCode", "MSTR")
        'elmNew.SetAttribute("lenderId", Mid(dr("lenderId"), 1, 6))
        With elmNew
            .SetAttribute("id", dr("SSN"))
            If Not dr("GraduationDate") Is System.DBNull.Value Then
                .SetAttribute("graduationDate", CDate(dr("GraduationDate")).ToString("yyyy-MM-dd"))
                '.SetAttribute("graduationDateSpecified", True)
            Else
                .SetAttribute("graduationDate", "")
                '.SetAttribute("graduationDateSpecified", False)
            End If
            If strNumberOfCampus.Length = 1 Then
                strNumberOfCampus = "0" & strNumberOfCampus
            End If
            .SetAttribute("status", "AC")
            If Not dr("StateOfResidence") Is System.DBNull.Value Then .SetAttribute("stateOfResidence", dr("StateOfResidence")) Else .SetAttribute("stateOfResidence", "")
            .SetAttribute("institutionCode", strNumberOfCampus)
            If Not dr("SessionApplied") Is System.DBNull.Value Then .SetAttribute("sessionApplied", dr("SessionApplied")) Else .SetAttribute("sessionApplied", "")
            .SetAttribute("strSession", dr("SessionApplied"))
            .SetAttribute("sessionRemaining", "0.00")
            If Not dr("SessionStartTerm") Is System.DBNull.Value Then .SetAttribute("sessionStart", dr("SessionStartTerm")) Else .SetAttribute("sessionStart", "")
            If Not dr("SessionEndTerm") Is System.DBNull.Value Then .SetAttribute("sessionEnd", dr("SessionEndTerm")) Else .SetAttribute("sessionEnd", "")
            .SetAttribute("eligibleCode", "A0")
            If Not dr("SessionApplied") Is System.DBNull.Value Then .SetAttribute("eligibleSession", dr("SessionApplied")) Else .SetAttribute("eligibleSession", "")
            .SetAttribute("hoursRemaining", "0")
            If Not dr("CurriculumCode") Is System.DBNull.Value Then .SetAttribute("curriculumCode", dr("CurriculumCode")) Else .SetAttribute("curriculumCode", "")
            .SetAttribute("miscellaneousCode1", "")
            .SetAttribute("miscellaneousCode2", "")
            .SetAttribute("miscellaneousCode3", "")
            .SetAttribute("miscellaneousCode4", "")
            ' If Not dr("EntranceInterviewDate") Is System.DBNull.Value Then .SetAttribute("entranceInterviewDate", CDate(dr("EntranceInterviewDate")).ToString("yyyy-MM-dd")) Else .SetAttribute("entranceInterviewDate", "")
            'If Not dr("ExitInterviewDate") Is System.DBNull.Value Then .SetAttribute("exitInterviewDate", CDate(dr("ExitInterviewDate")).ToString("yyyy-MM-dd")) Else .SetAttribute("exitInterviewDate", "")
            .SetAttribute("acgEligibilityReasonCode", "00")
            'If Not dr("HighSchoolProgramCode") Is System.DBNull.Value Then .SetAttribute("highSchoolProgramCode", Mid(dr("HighSchoolProgramCode"), 1, 6)) Else .SetAttribute("highSchoolProgramCode", "")
            .SetAttribute("nameType", "STUD")
            .SetAttribute("nameCode", "MSTR")
            .SetAttribute("addressType", "HOME")
            .SetAttribute("addressCode", "HOME")
            .SetAttribute("emailType", "HOME")
            .SetAttribute("emailCode", "HOME")
            If Not dr("lenderId") Is System.DBNull.Value Then .SetAttribute("lenderId", Mid(dr("lenderId"), 1, 6)) Else .SetAttribute("lenderId", "")
        End With
        elmRoot.AppendChild(elmNew)
        Return xmlDoc
    End Function
    Private Function AppendNameAttribute(ByVal xmlDoc As XmlDocument, ByVal elmRoot As XmlElement, ByVal dtStudentDetails As DataTable) As XmlDocument
        Dim dr As DataRow
        dr = dtStudentDetails.Rows(0)
        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "name", "http://www.regenteducation.com/carbon/batchload")
        With elmNew
            .SetAttribute("type", "STUD")
            .SetAttribute("code", "MSTR")
            .SetAttribute("firstName", Mid(dr("FirstName"), 1, 20))
            If Not dr("middlename") Is System.DBNull.Value Then
                .SetAttribute("middleName", Mid(dr("middleName").ToString, 1, 20))
            Else
                .SetAttribute("middleName", "")
            End If
            .SetAttribute("lastName", Mid(dr("LastName"), 1, 20))
            .SetAttribute("informalName", Mid(dr("LastName"), 1, 20))
            .SetAttribute("title", "")
            If Not dr("Prefix") Is System.DBNull.Value Then
                .SetAttribute("prefix", Mid(dr("Prefix").ToString, 1, 4))
            Else
                .SetAttribute("prefix", "")
            End If
            If Not dr("Suffix") Is System.DBNull.Value Then
                .SetAttribute("suffix", Mid(dr("Suffix").ToString, 1, 4))
            Else
                .SetAttribute("suffix", "")
            End If
        End With
        elmRoot.LastChild.AppendChild(elmNew)
        Return xmlDoc
    End Function
    Private Sub AppendAddressAttribute(ByVal xmlDoc As XmlDocument, _
                                       ByVal elmRoot As XmlElement, _
                                       ByVal dtStudentDetails As DataTable, _
                                       ByVal strPhone As String)
        Dim dr As DataRow
        Try
            dr = dtStudentDetails.Rows(0)
        Catch ex As System.Exception
            Exit Sub
        End Try

        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "address", "http://www.regenteducation.com/carbon/batchload")
        With elmNew
            'If Not dr("AddressDescrip") Is System.DBNull.Value Then .type = dr("AddressDescrip") Else .type = "HOME"
            'If Not dr("AddressCode") Is System.DBNull.Value Then .code = dr("AddressCode") Else .code = "HOME"
            'If Not dr("Address1") Is System.DBNull.Value Then .line1 = Mid(dr("Address1"), 1, 30) Else .line1 = ""
            'If Not dr("Address2") Is System.DBNull.Value Then .line2 = Mid(dr("Address2"), 1, 30) Else .line2 = ""
            '.line3 = ""
            '.line4 = ""
            'If Not dr("City") Is System.DBNull.Value Then .city = Mid(dr("City"), 1, 20) Else .city = ""
            'If Not dr("StateDescrip") Is System.DBNull.Value Then .state = Mid(dr("StateDescrip"), 1, 2) Else .state = ""
            'If Not dr("Zip") Is System.DBNull.Value Then .zip = dr("Zip") Else .zip = ""
            'If Not dr("CountryCode") Is System.DBNull.Value Then .countryCode = Mid(dr("CountryCode"), 1, 4) Else .countyCode = ""
            'If Not dr("CountyCode") Is System.DBNull.Value Then .countyCode = Mid(dr("CountyCode"), 1, 4) Else .countyCode = ""
            '.status = "AC"
            'If Not dr("telephone1") Is System.DBNull.Value Then .telephone1 = dr("telephone1") Else .telephone1 = ""
            'If Not dr("telephone2") Is System.DBNull.Value Then .telephone2 = dr("telephone2") Else .telephone2 = ""
            'If Not dr("fax") Is System.DBNull.Value Then .fax = dr("fax") Else .fax = ""

            If Not dr("AddressDescrip") Is System.DBNull.Value Then .SetAttribute("type", dr("AddressDescrip")) Else .SetAttribute("type", "HOME")
            If Not dr("AddressCode") Is System.DBNull.Value Then .SetAttribute("code", dr("AddressCode")) Else .SetAttribute("code", "HOME")
            If Not dr("Address1") Is System.DBNull.Value Then .SetAttribute("line1", Mid(dr("Address1"), 1, 30)) Else .SetAttribute("line1", "")
            If Not dr("Address2") Is System.DBNull.Value Then .SetAttribute("line2", Mid(dr("Address2"), 1, 30)) Else .SetAttribute("line2", "")
            .SetAttribute("line3", "")
            .SetAttribute("line4", "")
            If Not dr("City") Is System.DBNull.Value Then .SetAttribute("city", Mid(dr("City"), 1, 20)) Else .SetAttribute("city", "")
            If Not dr("StateDescrip") Is System.DBNull.Value Then .SetAttribute("state", Mid(dr("StateDescrip"), 1, 2)) Else .SetAttribute("state", "")
            If Not dr("Zip") Is System.DBNull.Value Then .SetAttribute("zip", dr("Zip")) Else .SetAttribute("zip", "")
            If Not dr("CountryCode") Is System.DBNull.Value Then .SetAttribute("countryCode", Mid(dr("CountryCode"), 1, 4)) Else .SetAttribute("countyCode", "")
            If Not dr("CountyCode") Is System.DBNull.Value Then .SetAttribute("countyCode", Mid(dr("CountyCode"), 1, 4)) Else .SetAttribute("countyCode", "")
            .SetAttribute("status", "AC")
            If Not dr("telephone1") Is System.DBNull.Value Then .SetAttribute("telephone1", dr("telephone1")) Else .SetAttribute("telephone1", "")
            If Not dr("telephone2") Is System.DBNull.Value Then .SetAttribute("telephone2", dr("telephone2")) Else .SetAttribute("telephone2", "")
            If Not dr("fax") Is System.DBNull.Value Then .SetAttribute("fax", dr("fax")) Else .SetAttribute("fax", "")
        End With
        elmRoot.LastChild.AppendChild(elmNew)
    End Sub
    Private Sub AppendDocumentAttribute(ByVal xmlDoc As XmlDocument, _
                                        ByVal elmRoot As XmlElement, _
                                        ByVal dtDoc As DataTable, ByVal strSession As String)
        If dtDoc.Rows.Count = 0 Then
            Exit Sub
        End If
        For Each dr As DataRow In dtDoc.Rows
            Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "tracking", "http://www.regenteducation.com/carbon/batchload")
            With elmNew
                .SetAttribute("session", strSession)
                If Not dr("trackCode") Is System.DBNull.Value Then
                    .SetAttribute("trackCode", Mid(dr("trackCode"), 1, 2))
                Else
                    .SetAttribute("trackCode", "")
                End If
                If Not dr("DueDate") Is System.DBNull.Value Then
                    .SetAttribute("dueDate", CDate(dr("DueDate")).ToString("yyyy-MM-dd"))
                Else
                    .SetAttribute("dueDate", "")
                End If
                If Not dr("transdate") Is System.DBNull.Value Then
                    .SetAttribute("notifiedDate", CDate(dr("transdate")).ToString("yyyy-MM-dd"))
                    ' .SetAttribute("notifiedDateSpecified", True)
                Else
                    .SetAttribute("notifiedDate", "")
                    ' .SetAttribute("notifiedDateSpecified", False)
                End If
                If Not dr("completedDate") Is System.DBNull.Value Then
                    .SetAttribute("completedDate", CDate(dr("completedDate")).ToString("yyyy-MM-dd"))
                    '.SetAttribute("completedDateSpecified", True)
                Else
                    .SetAttribute("completedDate", "")
                    ' .SetAttribute("completedDateSpecified", False)
                End If
                If Not dr("notificationcode") Is System.DBNull.Value Then
                    .SetAttribute("notationCode", dr("notificationcode"))
                Else
                    .SetAttribute("notationCode", "")
                End If
                If Not dr("note") Is System.DBNull.Value Then
                    .SetAttribute("note", dr("note"))
                Else
                    .SetAttribute("note", "")
                End If
            End With
            elmRoot.LastChild.AppendChild(elmNew)
        Next
    End Sub
    Private Sub AppendCommentAttribute(ByVal xmlDoc As XmlDocument, _
                                       ByVal elmRoot As XmlElement, _
                                       ByVal dtDoc As DataTable)
        Dim dr As DataRow
        dr = dtDoc.Rows(0)
        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "comment", "http://www.regenteducation.com/carbon/batchload")
        With elmNew
            Dim objCommentType As New commentType
            .SetAttribute("session", dr("SessionApplied"))
            .SetAttribute("blockNumber", dr("BlockNumber"))
            If Not dr("Comments") Is System.DBNull.Value Then .SetAttribute("comment", dr("Comments")) Else .SetAttribute("comment", "")
            If Not dr("CommentType") Is System.DBNull.Value Then .SetAttribute("type", dr("CommentType")) Else .SetAttribute("type", "")
        End With
        elmRoot.LastChild.AppendChild(elmNew)

    End Sub
    Private Sub AppendEmailAttribute(ByVal xmlDoc As XmlDocument, _
                                     ByVal elmRoot As XmlElement, _
                                     ByVal dtEmail As DataTable)
        Dim dr As DataRow
        dr = dtEmail.Rows(0)
        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "email", "http://www.regenteducation.com/carbon/batchload")
        With elmNew
            If Not dr("HomeEmail") Is System.DBNull.Value Then
                .SetAttribute("address", dr("HomeEmail").ToString)
                .SetAttribute("type", "HOME")
                .SetAttribute("code", "HOME")
            Else
                .SetAttribute("address", "")
                .SetAttribute("type", "HOME")
                .SetAttribute("code", "HOME")
            End If
        End With
        elmRoot.LastChild.AppendChild(elmNew)
    End Sub
    Private Sub AppendWorkAttribute(ByVal xmlDoc As XmlDocument, _
                                    ByVal elmRoot As XmlElement, _
                                    ByVal dtEmail As DataTable)
        Dim dr As DataRow
        dr = dtEmail.Rows(0)
        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "email", "http://www.regenteducation.com/carbon/batchload")
        With elmNew

            If Not dr("WorkEmail") Is System.DBNull.Value Then
                .SetAttribute("address", dr("WorkEmail").ToString)
                .SetAttribute("type", "WORK")
                .SetAttribute("code", "WORK")
            Else
                .SetAttribute("address", "")
                .SetAttribute("type", "WORK")
                .SetAttribute("code", "WORK")
            End If
        End With
        elmRoot.LastChild.AppendChild(elmNew)
    End Sub
    Private Sub AppendDemographicAttribute(ByVal xmlDoc As XmlDocument, _
                                           ByVal elmRoot As XmlElement, _
                                           ByVal dtDemraphic As DataTable)
        Dim dr As DataRow
        dr = dtDemraphic.Rows(0)
        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "demographic", "http://www.regenteducation.com/carbon/batchload")
        With elmNew
            .SetAttribute("alternateId", "")
            If Not dr("DOB") Is System.DBNull.Value Then .SetAttribute("dateOfBirth", CDate(dr("DOB")).ToString("yyyy-MM-dd")) Else .SetAttribute("dateOfBirth", "")
            If Not dr("Sex") Is System.DBNull.Value Then .SetAttribute("sex", dr("sex")) Else .SetAttribute("sex", "")
            If Not dr("ssn") Is System.DBNull.Value Then .SetAttribute("ssn", dr("ssn")) Else .SetAttribute("ssn", "")
            If Not dr("maritalstatus") Is System.DBNull.Value Then .SetAttribute("maritalStatus", dr("maritalstatus")) Else .SetAttribute("maritalStatus", "")
            If Not dr("citizen") Is System.DBNull.Value Then .SetAttribute("citizenship", dr("citizen")) Else .SetAttribute("citizenship", "")
            If Not dr("race") Is System.DBNull.Value Then .SetAttribute("ethnicity", dr("race")) Else .SetAttribute("ethnicity", "")
            .SetAttribute("handicap", "")
            '.SetAttribute("deceasedDate", "")
            .SetAttribute("pin", "")
            .SetAttribute("status", "AC")
            .SetAttribute("miscellaneous1", "")
            .SetAttribute("miscellaneous2", "")
            .SetAttribute("miscellaneous3", "")
            .SetAttribute("miscellaneous4", "")
            .SetAttribute("miscellaneous5", "")
            .SetAttribute("miscellaneous6", "")
            .SetAttribute("miscellaneous7", "")
            .SetAttribute("miscellaneous8", "")
            .SetAttribute("miscellaneous9", "")
            .SetAttribute("miscellaneous10", "")
            .SetAttribute("miscellaneous11", "")
            .SetAttribute("miscellaneous12", "")
            .SetAttribute("miscellaneous13", "")
            .SetAttribute("miscellaneous14", "")
            .SetAttribute("miscellaneous15", "")
            .SetAttribute("miscellaneous16", "")
            .SetAttribute("miscellaneous17", "")
            .SetAttribute("miscellaneous18", "")
            .SetAttribute("miscellaneous19", "")
            .SetAttribute("miscellaneous20", "")
            .SetAttribute("miscellaneous21", "")
            .SetAttribute("miscellaneous22", "")
            .SetAttribute("miscellaneous23", "")
            .SetAttribute("miscellaneous24", "")
            .SetAttribute("miscellaneous25", "")
            .SetAttribute("miscellaneous26", "")
            .SetAttribute("miscellaneous27", "")
            .SetAttribute("miscellaneous28", "")
            .SetAttribute("miscellaneous29", "")
            .SetAttribute("miscellaneous30", "")
        End With
        elmRoot.LastChild.AppendChild(elmNew)
    End Sub
    Private Sub AppendFinancialAidAttribute(ByVal xmlDoc As XmlDocument, _
                                            ByVal elmRoot As XmlElement, _
                                            ByVal dtFinAid As DataTable)
        Dim dr As DataRow
        dr = dtFinAid.Rows(0)
        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "financialAid", "http://www.regenteducation.com/carbon/batchload")
        With elmNew
            .SetAttribute("sessionStart", "")
            .SetAttribute("sessionEnd", "")
            .SetAttribute("status", "")
            .SetAttribute("freezeStatus", "")
            .SetAttribute("budgetCode", "")
            .SetAttribute("budgetAmount", "0.00")
            .SetAttribute("budgetMonths", "0")
            .SetAttribute("childcareAmount", "")
            .SetAttribute("supplementalAmount", "0.00")
            .SetAttribute("programCost", "0.00")
            .SetAttribute("packageCode", "0.00")
            .SetAttribute("additionalAmount", "0.00")
            .SetAttribute("scheduleCosts", "0.00")
            .SetAttribute("enrollmentCode1", "")
            .SetAttribute("budgetCode1", "")
            .SetAttribute("enrollmentCode2", "")
            .SetAttribute("budgetCode2", "")
            .SetAttribute("enrollmentCode3", "")
            .SetAttribute("budgetCode3", "")
            .SetAttribute("enrollmentCode4", "")
            .SetAttribute("budgetCode4", "")
            .SetAttribute("enrollmentCode5", "")
            .SetAttribute("budgetCode5", "")
            .SetAttribute("parentIncome", "")
            .SetAttribute("studentIncome", "")
            .SetAttribute("numberOfDependents", "")
            .SetAttribute("parentContribution", "")
            .SetAttribute("studentContribution", "")
            .SetAttribute("parentContributionIM", "")
            .SetAttribute("studentContributionIM", "")
            .SetAttribute("efc", "")
            .SetAttribute("costOfEducation", "")
            .SetAttribute("enrollmentCode", "")
            .SetAttribute("sarId", "")
            .SetAttribute("hoursYear", "0.00")
            .SetAttribute("hoursExp", "0.00")
            .SetAttribute("weeksYear", "0")
            .SetAttribute("weeksExp", "0")
            .SetAttribute("codCitizenshipCode", "")
            .SetAttribute("needAmount", "0.00")
            .SetAttribute("institutionalNeedAmount", "0.00")
            .SetAttribute("afdcOrTanf", "")
            .SetAttribute("dependencyStatus", "")
            .SetAttribute("yearInSchool", "")
            .SetAttribute("parentOrStudentMaritalStatus", "")
            .SetAttribute("pellEligibility", "")
            .SetAttribute("coaOverride", "")
        End With
        elmRoot.LastChild.AppendChild(elmNew)
    End Sub
    Private Sub AppendAwardAttribute(ByVal xmlDoc As XmlDocument, _
                                     ByVal elmRoot As XmlElement, _
                                     ByVal dtAward As DataTable)
        Dim dr As DataRow
        dr = dtAward.Rows(0)
        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "award", "http://www.regenteducation.com/carbon/batchload")
        With elmNew
            If Not dr("session") Is System.DBNull.Value Then .SetAttribute("session", dr("session")) Else .SetAttribute("session", "")
            If Not dr("code") Is System.DBNull.Value Then .SetAttribute("code", dr("code")) Else .SetAttribute("code", "")
            If Not dr("status") Is System.DBNull.Value Then .SetAttribute("status", dr("status")) Else .SetAttribute("status", "")
            If Not dr("amount") Is System.DBNull.Value Then .SetAttribute("amount", dr("amount")) Else .SetAttribute("amount", "")
            If Not dr("subCode") Is System.DBNull.Value Then .SetAttribute("subCode", dr("subCode")) Else .SetAttribute("subCode", "")
            .SetAttribute("miscellaneousCode1", "")
            .SetAttribute("miscellaneousCode2", "")
            If Not dr("ActivityDate") Is System.DBNull.Value Then .SetAttribute("ActivityDate", FormatDateTime(dr("ActivityDate"), DateFormat.ShortDate)) Else .SetAttribute("ActivityDate", "")
        End With
        elmRoot.LastChild.AppendChild(elmNew)
    End Sub
    Private Sub AppendTrackingAttribute(ByVal xmlDoc As XmlDocument, _
                                        ByVal elmRoot As XmlElement, _
                                        ByVal dtTracking As DataTable)
        Dim dr As DataRow
        dr = dtTracking.Rows(0)
        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "tracking", "http://www.regenteducation.com/carbon/batchload")
        With elmNew
            .SetAttribute("session", "")
            .SetAttribute("trackCode", "")
            .SetAttribute("dueDate", "")
            .SetAttribute("notifiedDate", "")
            .SetAttribute("completedDate", "")
            .SetAttribute("notationCode", "")
        End With
        elmRoot.LastChild.AppendChild(elmNew)
    End Sub
    Public Sub AddAddressXML(ByVal StudentId As String, _
                             ByVal filename As String, _
                             ByVal StudentAddressId As String)

        Dim xmlDoc As XmlDocument = New XmlDocument()
        Dim dtStudentAddress As DataTable
        Dim dr As DataRow
        Dim strSSN As String = ""
        dtStudentAddress = getStudentAddress(StudentAddressId, "Append")

        Try
            dr = dtStudentAddress.Rows(0)
        Catch ex As System.Exception
            Exit Sub
        End Try
        'If file with todays date exists then open the file and add the address info
        'If not then create the student tag and then add address to it
        If (File.Exists(filename)) Then
            xmlDoc.Load(filename)
            strSSN = getSSN(StudentId)
            xmlDoc = AddAddressForExistingStudent(xmlDoc, dr, strSSN, StudentId)
            xmlDoc.Save(filename)
        Else
            'If file not found then create file with student data and then add address data 
            strSSN = CreateStudentDataForUpdates(StudentId, "")
            If File.Exists(filename) Then
                xmlDoc.Load(filename)
                xmlDoc = AddAddressForExistingStudent(xmlDoc, dr, strSSN, StudentId)
                xmlDoc.Save(filename)
            End If
        End If
    End Sub
    Private Function AppendGraduationDate(ByVal xmlDoc As XmlDocument, ByVal graduationDate As String, ByVal strSSN As String) As XmlDocument
        'Get a reference to the root node
        Dim elmRoot As XmlElement = xmlDoc.DocumentElement
        Dim boolStudentFound As Boolean = False
        'Create a list of the student
        Dim lstStudents As XmlNodeList = xmlDoc.GetElementsByTagName("student")
        Dim intStudentTagCount As Integer = 0
        Do While intStudentTagCount < lstStudents.Count
            Dim curAttributes As XmlAttributeCollection = lstStudents(intStudentTagCount).Attributes
            Dim intAttrCount As Integer = 0
            Do While intAttrCount < curAttributes.Count
                'If any one of the student tag has a ssn="123456789" then add the address tag to it
                If Not curAttributes("id").InnerText Is Nothing AndAlso curAttributes("id").InnerText.ToString.Trim = strSSN.ToString.Trim Then
                    boolStudentFound = True
                    xmlDoc.DocumentElement.SetAttribute("graduationDate", graduationDate)
                    Exit Do
                End If
                intAttrCount += 1
            Loop
            intStudentTagCount += 1
        Loop

    End Function
    Public Function AddAddressForExistingStudent(ByVal xmlDoc As XmlDocument, ByVal dr As DataRow, ByVal strSSN As String, ByVal Studentid As String) As XmlDocument
        'Get a reference to the root node
        Dim elmRoot As XmlElement = xmlDoc.DocumentElement
        Dim boolStudentFound As Boolean = False
        Dim boolAddressTagExists As Boolean = False
        'Create a list of the student
        Dim lstStudents As XmlNodeList = xmlDoc.GetElementsByTagName("student")
        ' visit each Student
        For Each node As XmlNode In lstStudents
            Dim lstChildren As XmlNodeList = node.ChildNodes
            ' Visit each child node 
            For Each dir As XmlNode In lstChildren
                If dir.LocalName.ToString.Trim = "address" And Not node.Attributes("id").Value.ToString.Trim = "" And node.Attributes("id").Value.ToString.Trim = strSSN.ToString Then
                    boolStudentFound = True
                    boolAddressTagExists = True
                    If dir.Attributes("type").Value.ToString.Trim = dr("AddressDescrip").ToString.Trim And _
                       dir.Attributes("code").Value.ToString.Trim = dr("AddressCode").ToString.Trim Then
                        With dir
                            If Not dr("Address1") Is System.DBNull.Value Then .Attributes("line1").Value = Mid(dr("Address1"), 1, 30) Else .Attributes("line1").Value = ""
                            If Not dr("Address2") Is System.DBNull.Value Then .Attributes("line2").Value = Mid(dr("Address2"), 1, 30) Else .Attributes("line2").Value = ""
                            .Attributes("line3").Value = ""
                            .Attributes("line4").Value = ""
                            If Not dr("City") Is System.DBNull.Value Then .Attributes("city").Value = Mid(dr("City"), 1, 20) Else .Attributes("city").Value = ""
                            If Not dr("StateDescrip") Is System.DBNull.Value Then .Attributes("state").Value = Mid(dr("StateDescrip"), 1, 2) Else .Attributes("state").Value = ""
                            If Not dr("Zip") Is System.DBNull.Value Then .Attributes("zip").Value = dr("Zip") Else .Attributes("zip").Value = ""
                            If Not dr("CountryCode") Is System.DBNull.Value Then .Attributes("countryCode").Value = Mid(dr("CountryCode"), 1, 4) Else .Attributes("countyCode").Value = ""
                            If Not dr("CountyCode") Is System.DBNull.Value Then .Attributes("countyCode").Value = Mid(dr("CountryCode"), 1, 4) Else .Attributes("countyCode").Value = ""
                            .Attributes("status").Value = "AC"
                            If Not dr("telephone1") Is System.DBNull.Value Then .Attributes("telephone1").Value = dr("telephone1") Else .Attributes("telephone1").Value = ""
                            If Not dr("telephone2") Is System.DBNull.Value Then .Attributes("telephone2").Value = dr("telephone2") Else .Attributes("telephone2").Value = ""
                            If Not dr("fax") Is System.DBNull.Value Then .Attributes("fax").Value = dr("fax") Else .Attributes("fax").Value = ""
                        End With
                    Else
                        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "address", "http://www.regenteducation.com/carbon/batchload")
                        With elmNew
                            If Not dr("AddressDescrip") Is System.DBNull.Value Then .SetAttribute("type", dr("AddressDescrip").ToString.Trim) Else .SetAttribute("type", "HOME")
                            If Not dr("AddressCode") Is System.DBNull.Value Then .SetAttribute("code", dr("AddressCode").ToString.Trim) Else .SetAttribute("code", "HOME")
                            If Not dr("Address1") Is System.DBNull.Value Then .SetAttribute("line1", Mid(dr("Address1"), 1, 30)) Else .SetAttribute("line1", "")
                            If Not dr("Address2") Is System.DBNull.Value Then .SetAttribute("line2", Mid(dr("Address2"), 1, 30)) Else .SetAttribute("line2", "")
                            .SetAttribute("line3", "")
                            .SetAttribute("line4", "")
                            If Not dr("City") Is System.DBNull.Value Then .SetAttribute("city", Mid(dr("City"), 1, 20)) Else .SetAttribute("city", "")
                            If Not dr("StateDescrip") Is System.DBNull.Value Then .SetAttribute("state", Mid(dr("StateDescrip"), 1, 2)) Else .SetAttribute("state", "")
                            If Not dr("Zip") Is System.DBNull.Value Then .SetAttribute("zip", dr("Zip")) Else .SetAttribute("zip", "")
                            If Not dr("CountryCode") Is System.DBNull.Value Then .SetAttribute("countryCode", Mid(dr("CountryCode"), 1, 4)) Else .SetAttribute("countyCode", "")
                            If Not dr("CountyCode") Is System.DBNull.Value Then .SetAttribute("countyCode", Mid(dr("CountryCode"), 1, 4)) Else .SetAttribute("countyCode", "")
                            .SetAttribute("status", "AC")
                            If Not dr("telephone1") Is System.DBNull.Value Then .SetAttribute("telephone1", dr("telephone1")) Else .SetAttribute("telephone1", "")
                            If Not dr("telephone2") Is System.DBNull.Value Then .SetAttribute("telephone2", dr("telephone2")) Else .SetAttribute("telephone2", "")
                            If Not dr("fax") Is System.DBNull.Value Then .SetAttribute("fax", dr("fax")) Else .SetAttribute("fax", "")
                        End With
                        node.InsertAfter(elmNew, dir)
                    End If
                    'With dir
                    '    .Attributes("session").Value

                    Exit For
                End If
            Next
        Next
        If boolAddressTagExists = False Then
            lstStudents = xmlDoc.GetElementsByTagName("student")
            Dim intStudentTagCount As Integer = 0
            Do While intStudentTagCount < lstStudents.Count
                Dim curAttributes As XmlAttributeCollection = lstStudents(intStudentTagCount).Attributes
                Dim intAttrCount As Integer = 0
                Do While intAttrCount < curAttributes.Count
                    If Not curAttributes("id").InnerText Is Nothing AndAlso curAttributes("id").InnerText.ToString.Trim = strSSN.ToString.Trim Then
                        boolStudentFound = True
                        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "address", "http://www.regenteducation.com/carbon/batchload")
                        With elmNew
                            If Not dr("AddressDescrip") Is System.DBNull.Value Then .SetAttribute("type", "HOME") Else .SetAttribute("type", "HOME")
                            If Not dr("AddressCode") Is System.DBNull.Value Then .SetAttribute("code", "HOME") Else .SetAttribute("code", "HOME")
                            If Not dr("Address1") Is System.DBNull.Value Then .SetAttribute("line1", Mid(dr("Address1"), 1, 30)) Else .SetAttribute("line1", "")
                            If Not dr("Address2") Is System.DBNull.Value Then .SetAttribute("line2", Mid(dr("Address2"), 1, 30)) Else .SetAttribute("line2", "")
                            .SetAttribute("line3", "")
                            .SetAttribute("line4", "")
                            If Not dr("City") Is System.DBNull.Value Then .SetAttribute("city", Mid(dr("City"), 1, 20)) Else .SetAttribute("city", "")
                            If Not dr("StateDescrip") Is System.DBNull.Value Then .SetAttribute("state", Mid(dr("StateDescrip"), 1, 2)) Else .SetAttribute("state", "")
                            If Not dr("Zip") Is System.DBNull.Value Then .SetAttribute("zip", dr("Zip")) Else .SetAttribute("zip", "")
                            If Not dr("CountryCode") Is System.DBNull.Value Then .SetAttribute("countryCode", Mid(dr("CountryCode"), 1, 4)) Else .SetAttribute("countyCode", "")
                            If Not dr("CountyCode") Is System.DBNull.Value Then .SetAttribute("countyCode", Mid(dr("CountryCode"), 1, 4)) Else .SetAttribute("countyCode", "")
                            .SetAttribute("status", "AC")
                            If Not dr("telephone1") Is System.DBNull.Value Then .SetAttribute("telephone1", dr("telephone1")) Else .SetAttribute("telephone1", "")
                            If Not dr("telephone2") Is System.DBNull.Value Then .SetAttribute("telephone2", dr("telephone2")) Else .SetAttribute("telephone2", "")
                            If Not dr("fax") Is System.DBNull.Value Then .SetAttribute("fax", dr("fax")) Else .SetAttribute("fax", "")
                        End With
                        lstStudents(intStudentTagCount).AppendChild(elmNew)
                        boolAddressTagExists = True
                        Exit Do
                    End If
                    intAttrCount += 1
                Loop
                intStudentTagCount += 1
            Loop
        End If
        'If student data is not found in the XML file then 
        ' create the student tag and then add address tag to it.
        If boolStudentFound = False Then
            Dim elmRoot1 As XmlElement = xmlDoc.DocumentElement
            Dim dtgetStudentDetails As New DataTable
            dtgetStudentDetails = getStudentDetails(Studentid)
            xmlDoc = AppendStudentAttributeToExistingStudents(xmlDoc, elmRoot1, dtgetStudentDetails)
            xmlDoc = AppendNameAttribute(xmlDoc, elmRoot1, dtgetStudentDetails)
            'As Student is created now add the address
            lstStudents = xmlDoc.GetElementsByTagName("student")
            ' visit each Student
            For Each node As XmlNode In lstStudents
                Dim lstChildren As XmlNodeList = node.ChildNodes
                ' Visit each child node 
                For Each dir As XmlNode In lstChildren
                    If dir.LocalName.ToString.Trim = "name" And Not node.Attributes("id").Value.ToString.Trim = "" And node.Attributes("id").Value.ToString.Trim = strSSN.ToString Then
                        boolStudentFound = True
                        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "address", "http://www.regenteducation.com/carbon/batchload")
                        With elmNew
                            If Not dr("AddressDescrip") Is System.DBNull.Value Then .SetAttribute("type", "HOME") Else .SetAttribute("type", "HOME")
                            If Not dr("AddressCode") Is System.DBNull.Value Then .SetAttribute("code", "HOME") Else .SetAttribute("code", "HOME")
                            If Not dr("Address1") Is System.DBNull.Value Then .SetAttribute("line1", Mid(dr("Address1"), 1, 30)) Else .SetAttribute("line1", "")
                            If Not dr("Address2") Is System.DBNull.Value Then .SetAttribute("line2", Mid(dr("Address2"), 1, 30)) Else .SetAttribute("line2", "")
                            .SetAttribute("line3", "")
                            .SetAttribute("line4", "")
                            If Not dr("City") Is System.DBNull.Value Then .SetAttribute("city", Mid(dr("City"), 1, 20)) Else .SetAttribute("city", "")
                            If Not dr("StateDescrip") Is System.DBNull.Value Then .SetAttribute("state", Mid(dr("StateDescrip"), 1, 2)) Else .SetAttribute("state", "")
                            If Not dr("Zip") Is System.DBNull.Value Then .SetAttribute("zip", dr("Zip")) Else .SetAttribute("zip", "")
                            If Not dr("CountryCode") Is System.DBNull.Value Then .SetAttribute("countryCode", Mid(dr("CountryCode"), 1, 4)) Else .SetAttribute("countyCode", "")
                            If Not dr("CountyCode") Is System.DBNull.Value Then .SetAttribute("countyCode", Mid(dr("CountryCode"), 1, 4)) Else .SetAttribute("countyCode", "")
                            .SetAttribute("status", "AC")
                            If Not dr("telephone1") Is System.DBNull.Value Then .SetAttribute("telephone1", dr("telephone1")) Else .SetAttribute("telephone1", "")
                            If Not dr("telephone2") Is System.DBNull.Value Then .SetAttribute("telephone2", dr("telephone2")) Else .SetAttribute("telephone2", "")
                            If Not dr("fax") Is System.DBNull.Value Then .SetAttribute("fax", dr("fax")) Else .SetAttribute("fax", "")
                        End With
                        node.InsertAfter(elmNew, dir)
                        Exit For
                    End If
                Next
            Next
        End If
        Return xmlDoc
    End Function
    Public Sub AddAwardXML(ByVal StudentId As String, _
                           ByVal filename As String, _
                           Optional ByVal StudentAwardId As String = "")
        Dim xmlDoc As XmlDocument = New XmlDocument()
        Dim dtAwardData As DataTable
        Dim dr As DataRow
        Dim strSSN As String = ""
        'Value of StudentId is passed to StudentAwardId paramater
        'Value of StudentAwardId is passed using the studentId parameter
        'get data based on student award id, the value for student award id is passed through the variable
        'StudentId
        dtAwardData = getStudentAwardData(StudentAwardId, "Append")
        Try
            dr = dtAwardData.Rows(0)
        Catch ex As System.Exception
            Exit Sub
        End Try
        If (File.Exists(filename)) Then
            xmlDoc.Load(filename)
            strSSN = dr("SSN")
            xmlDoc = AddAwardForExistingStudent(xmlDoc, dr, strSSN, StudentId)
            xmlDoc.Save(filename)
        Else
            'If file not found then create file with student data and then add address data 
            strSSN = CreateStudentDataForUpdates(StudentId, "")
            If File.Exists(filename) Then
                xmlDoc.Load(filename)
                xmlDoc = AddAwardForExistingStudent(xmlDoc, dr, strSSN, StudentId)
                xmlDoc.Save(filename)
            End If
        End If
    End Sub
    Private Function AddAwardForExistingStudent(ByVal xmlDoc As XmlDocument, ByVal dr As DataRow, ByVal strSSN As String, ByVal StudentId As String) As XmlDocument
        'Get a reference to the root node
        Dim elmRoot As XmlElement = xmlDoc.DocumentElement
        Dim boolStudentFound As Boolean = False
        Dim boolAwardTagExists As Boolean = False
        'Create a list of the student
        Dim lstStudents As XmlNodeList = xmlDoc.GetElementsByTagName("student")
        ' visit each Student
        For Each node As XmlNode In lstStudents
            Dim lstChildren As XmlNodeList = node.ChildNodes
            ' Visit each child node 
            For Each dir As XmlNode In lstChildren
                If dir.LocalName.ToString.Trim = "award" And Not node.Attributes("id").Value.ToString.Trim = "" And node.Attributes("id").Value.ToString.Trim = strSSN.ToString Then
                    boolStudentFound = True
                    boolAwardTagExists = True
                    If dir.Attributes("code").Value.ToString.Trim = dr("code").ToString.Trim And _
                       dir.Attributes("subCode").Value.ToString.Trim = dr("subCode").ToString.Trim Then
                        With dir
                            If Not dr("session") Is System.DBNull.Value Then .Attributes("session").Value = Mid(dr("session"), 1, 4) Else .Attributes("session").Value = ""
                            If Not dr("code") Is System.DBNull.Value Then .Attributes("code").Value = Mid(dr("code"), 1, 2) Else .Attributes("code").Value = ""
                            If Not dr("status") Is System.DBNull.Value Then .Attributes("status").Value = Mid(dr("status"), 1, 1) Else .Attributes("status").Value = ""
                            If Not dr("amount") Is System.DBNull.Value Then .Attributes("amount").Value = dr("amount") Else .Attributes("amount").Value = ""
                            If Not dr("subCode") Is System.DBNull.Value Then .Attributes("subCode").Value = Mid(dr("subCode"), 1, 6) Else .Attributes("subCode").Value = ""
                            .Attributes("miscellaneousCode1").Value = ""
                            .Attributes("miscellaneousCode2").Value = ""
                            If Not dr("ActivityDate") Is System.DBNull.Value Then .Attributes("activityDate").Value = CDate(dr("ActivityDate")).ToString("yyyy-MM-dd") Else .Attributes("activityDate").Value = ""
                        End With
                    Else
                        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "award", "http://www.regenteducation.com/carbon/batchload")
                        With elmNew
                            If Not dr("session") Is System.DBNull.Value Then .SetAttribute("session", Mid(dr("session"), 1, 4)) Else .SetAttribute("session", "")
                            If Not dr("code") Is System.DBNull.Value Then .SetAttribute("code", Mid(dr("code"), 1, 2)) Else .SetAttribute("code", "")
                            If Not dr("status") Is System.DBNull.Value Then .SetAttribute("status", Mid(dr("status"), 1, 1)) Else .SetAttribute("status", "")
                            If Not dr("amount") Is System.DBNull.Value Then .SetAttribute("amount", dr("amount")) Else .SetAttribute("amount", "")
                            If Not dr("subCode") Is System.DBNull.Value Then .SetAttribute("subCode", Mid(dr("subCode"), 1, 6)) Else .SetAttribute("subCode", "")
                            .SetAttribute("miscellaneousCode1", "")
                            .SetAttribute("miscellaneousCode2", "")
                            If Not dr("ActivityDate") Is System.DBNull.Value Then .SetAttribute("activityDate", CDate(dr("ActivityDate")).ToString("yyyy-MM-dd")) Else .SetAttribute("activityDate", "")
                        End With
                        node.InsertAfter(elmNew, dir)
                    End If

                    Exit For
                End If
            Next
        Next

        'If Award tag is not found then we need to add the award tag
        If boolAwardTagExists = False Then
            lstStudents = xmlDoc.GetElementsByTagName("student")
            Dim intStudentTagCount As Integer = 0
            Do While intStudentTagCount < lstStudents.Count
                Dim curAttributes As XmlAttributeCollection = lstStudents(intStudentTagCount).Attributes
                Dim intAttrCount As Integer = 0
                Do While intAttrCount < curAttributes.Count
                    If Not curAttributes("id").InnerText Is Nothing AndAlso curAttributes("id").InnerText.ToString.Trim = strSSN.ToString.Trim Then
                        boolStudentFound = True
                        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "award", "http://www.regenteducation.com/carbon/batchload")
                        With elmNew
                            If Not dr("session") Is System.DBNull.Value Then .SetAttribute("session", Mid(dr("session"), 1, 4)) Else .SetAttribute("session", "")
                            If Not dr("code") Is System.DBNull.Value Then .SetAttribute("code", Mid(dr("code"), 1, 2)) Else .SetAttribute("code", "")
                            If Not dr("status") Is System.DBNull.Value Then .SetAttribute("status", Mid(dr("status"), 1, 1)) Else .SetAttribute("status", "")
                            If Not dr("amount") Is System.DBNull.Value Then .SetAttribute("amount", dr("amount")) Else .SetAttribute("amount", "")
                            If Not dr("subCode") Is System.DBNull.Value Then .SetAttribute("subCode", Mid(dr("subCode"), 1, 6)) Else .SetAttribute("subCode", "")
                            .SetAttribute("miscellaneousCode1", "")
                            .SetAttribute("miscellaneousCode2", "")
                            If Not dr("ActivityDate") Is System.DBNull.Value Then .SetAttribute("activityDate", CDate(dr("ActivityDate")).ToString("yyyy-MM-dd")) Else .SetAttribute("activityDate", "")
                        End With
                        lstStudents(intStudentTagCount).AppendChild(elmNew)
                        boolAwardTagExists = True
                        Exit Do
                    End If
                    intAttrCount += 1
                Loop
                intStudentTagCount += 1
            Loop
        End If

        'If student data is not found in the XML file then 
        ' create the student tag and then add address tag to it.
        If boolStudentFound = False Then
            Dim elmRoot1 As XmlElement = xmlDoc.DocumentElement
            Dim dtgetStudentDetails As New DataTable
            dtgetStudentDetails = getStudentDetails(StudentId)
            xmlDoc = AppendStudentAttributeToExistingStudents(xmlDoc, elmRoot1, dtgetStudentDetails)
            xmlDoc = AppendNameAttribute(xmlDoc, elmRoot1, dtgetStudentDetails)
            'As Student is created now add the address
            lstStudents = xmlDoc.GetElementsByTagName("student")
            ' visit each Student
            For Each node As XmlNode In lstStudents
                Dim lstChildren As XmlNodeList = node.ChildNodes
                ' Visit each child node 
                For Each dir As XmlNode In lstChildren
                    If dir.LocalName.ToString.Trim = "name" And Not node.Attributes("id").Value.ToString.Trim = "" And node.Attributes("id").Value.ToString.Trim = strSSN.ToString Then
                        boolStudentFound = True
                        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "award", "http://www.regenteducation.com/carbon/batchload")
                        With elmNew
                            If Not dr("session") Is System.DBNull.Value Then .SetAttribute("session", Mid(dr("session"), 1, 4)) Else .SetAttribute("session", "")
                            If Not dr("code") Is System.DBNull.Value Then .SetAttribute("code", Mid(dr("code"), 1, 2)) Else .SetAttribute("code", "")
                            If Not dr("status") Is System.DBNull.Value Then .SetAttribute("status", Mid(dr("status"), 1, 1)) Else .SetAttribute("status", "")
                            If Not dr("amount") Is System.DBNull.Value Then .SetAttribute("amount", dr("amount")) Else .SetAttribute("amount", "")
                            If Not dr("subCode") Is System.DBNull.Value Then .SetAttribute("subCode", Mid(dr("subCode"), 1, 6)) Else .SetAttribute("subCode", "")
                            .SetAttribute("miscellaneousCode1", "")
                            .SetAttribute("miscellaneousCode2", "")
                            If Not dr("activityDate") Is System.DBNull.Value Then .SetAttribute("activityDate", CDate(dr("ActivityDate")).ToString("yyyy-MM-dd")) Else .SetAttribute("activityDate", "")
                        End With
                        node.InsertAfter(elmNew, dir)
                        Exit For
                    End If
                Next
            Next
        End If
        Return xmlDoc
    End Function
    Public Sub AddAcademicXML(ByVal StudentId As String, _
                              ByVal filename As String, _
                              ByVal StudentAcademicId As String, _
                              Optional ByVal CreditsAttempted As Decimal = 0.0, _
                              Optional ByVal CreditsEarned As Decimal = 0.0, _
                              Optional ByVal GPA As Decimal = 0.0)

        Dim xmlDoc As XmlDocument = New XmlDocument()
        Dim dtAcademicData As DataTable
        Dim dr As DataRow
        Dim strSSN As String = ""
        dtAcademicData = getStudentAcademicData(StudentId, "Append")
        Try
            dr = dtAcademicData.Rows(0)
        Catch ex As System.Exception
            Exit Sub
        End Try
        If (File.Exists(filename)) Then
            xmlDoc.Load(filename)
            strSSN = getSSN(StudentId)
            xmlDoc = AddAcademicForExistingStudent(xmlDoc, dtAcademicData, dr, strSSN, StudentId, CreditsAttempted, CreditsEarned, GPA)
            xmlDoc.Save(filename)
        Else
            'If file not found then create file with student data and then add address data 
            strSSN = CreateStudentDataForUpdates(StudentId, "")
            If File.Exists(filename) Then
                xmlDoc.Load(filename)
                xmlDoc = AddAcademicForExistingStudent(xmlDoc, dtAcademicData, dr, strSSN, StudentId, CreditsAttempted, CreditsEarned, GPA)
                xmlDoc.Save(filename)
            End If
        End If
    End Sub
    Private Function AddAcademicForExistingStudent(ByVal xmlDoc As XmlDocument, ByVal dt As DataTable, _
                                                   ByVal dr1 As DataRow, _
                                                   ByVal strSSN As String, _
                                                   ByVal Studentid As String, _
                                                   Optional ByVal CreditsAttempted As Decimal = 0.0, _
                                                   Optional ByVal CreditsEarned As Decimal = 0.0, _
                                                   Optional ByVal GPA As Decimal = 0.0) As XmlDocument

        Dim elmRoot As XmlElement = xmlDoc.DocumentElement
        Dim boolStudentFound As Boolean = False
        Dim boolActivityTagExists As Boolean = False
        Dim strSession As String = ""
        'Create a list of the student
        Dim lstStudents As XmlNodeList = xmlDoc.GetElementsByTagName("student")
        ' visit each Student
        For Each node As XmlNode In lstStudents
            Dim lstChildren As XmlNodeList = node.ChildNodes
            ' Visit each child node 
            For Each dir As XmlNode In lstChildren

                If dir.LocalName.ToString.Trim = "academic" And Not node.Attributes("id").Value.ToString.Trim = "" And node.Attributes("id").Value.ToString.Trim = strSSN.ToString Then
                    Try
                        strSession = node.Attributes("sessionApplied").Value.ToString.Trim
                    Catch ex As System.Exception
                        strSession = ""
                    End Try
                    boolStudentFound = True
                    boolActivityTagExists = True
                    For Each dr As DataRow In dt.Rows

                        If dir.Attributes("session").Value.ToString.Trim = dr("Session").ToString.Trim And _
                           dir.Attributes("degreeCode").Value.ToString.Trim = dr("degreecode").ToString.Trim And _
                           dir.Attributes("curriculumCode").Value.ToString.Trim = dr("curriculumCode").ToString.Trim Then
                            With dir
                                .Attributes("sessionHoursAttempted").Value = CreditsAttempted
                                .Attributes("sessionHoursEarned").Value = CreditsEarned
                                .Attributes("sessionGpa").Value = GPA
                            End With
                        Else
                            If dir.LocalName.ToString.Trim = "demographic" Then
                                Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "academic", "http://www.regenteducation.com/carbon/batchload")
                                With elmNew
                                    .SetAttribute("session", dr("Session"))
                                    If Not dr("degreecode") Is System.DBNull.Value Then .SetAttribute("degreeCode", dr("degreecode").ToString) Else .SetAttribute("degreeCode", "")
                                    If Not dr("curriculumCode") Is System.DBNull.Value Then .SetAttribute("curriculumCode", dr("curriculumCode").ToString) Else .SetAttribute("curriculumCode", "")
                                    .SetAttribute("appealCode", "")
                                    ' .SetAttribute("appealdate", "")
                                    .SetAttribute("sessionHoursAttempted", CreditsAttempted)
                                    .SetAttribute("sessionHoursEarned", CreditsEarned)
                                    .SetAttribute("sessionHoursFunded", "0")
                                    .SetAttribute("academicStatus", "")
                                    .SetAttribute("sessionGpa", GPA)
                                    .SetAttribute("cumulativeGpa", "0")
                                    .SetAttribute("cumulativeQpa", "0")
                                    '.SetAttribute("miscellaneousCode1", "0")
                                    .SetAttribute("miscellaneousCode2", "0")
                                    .SetAttribute("withdrawalCode", "0")
                                    '.SetAttribute("withdrawalDate", "")
                                    .SetAttribute("cumulativeTransferHours", "")
                                    '.SetAttribute("cipCode", "0")
                                    .SetAttribute("gradeLevel", "0")
                                    .SetAttribute("overrideHours", "0")
                                    .SetAttribute("hoursAdjustment", "0")
                                    .SetAttribute("miscellaneousHours1", "0")
                                    .SetAttribute("miscellaneousHours2", "0")
                                End With
                                node.InsertAfter(elmNew, dir)
                            End If
                        End If
                    Next
                End If
            Next
        Next

        'If Award tag is not found then we need to add the award tag
        If boolActivityTagExists = False Then
            lstStudents = xmlDoc.GetElementsByTagName("student")
            Dim intStudentTagCount As Integer = 0
            'Do While intStudentTagCount < lstStudents.Count
            '    Dim curAttributes As XmlAttributeCollection = lstStudents(intStudentTagCount).Attributes
            Dim intAttrCount As Integer = 0
            '    Do While intAttrCount < curAttributes.Count
            '        If Not curAttributes("id").InnerText Is Nothing AndAlso curAttributes("id").InnerText.ToString.Trim = strSSN.ToString.Trim Then
            For Each node As XmlNode In lstStudents
                Dim lstChildren As XmlNodeList = node.ChildNodes
                ' Visit each child node 
                For Each dir As XmlNode In lstChildren

                    ' If dir.LocalName.ToString.Trim = "academic" And Not node.Attributes("id").Value.ToString.Trim = "" And node.Attributes("id").Value.ToString.Trim = strSSN.ToString Then
                    Try
                        strSession = node.Attributes("sessionApplied").Value.ToString.Trim
                    Catch ex As System.Exception
                        strSession = ""
                    End Try
                    boolStudentFound = True
                    If dir.LocalName.ToString.Trim = "demographic" Then
                        For Each dr As DataRow In dt.Rows
                            Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "academic", "http://www.regenteducation.com/carbon/batchload")
                            With elmNew
                                .SetAttribute("session", dr("Session"))
                                If Not dr("degreecode") Is System.DBNull.Value Then .SetAttribute("degreeCode", dr("degreecode").ToString) Else .SetAttribute("degreeCode", "")
                                If Not dr("curriculumCode") Is System.DBNull.Value Then .SetAttribute("curriculumCode", dr("curriculumCode").ToString) Else .SetAttribute("curriculumCode", "")
                                .SetAttribute("appealCode", "")
                                '.SetAttribute("appealdate", "")
                                .SetAttribute("sessionHoursAttempted", CreditsAttempted)
                                .SetAttribute("sessionHoursEarned", CreditsEarned)
                                .SetAttribute("sessionHoursFunded", "0")
                                .SetAttribute("academicStatus", "")
                                .SetAttribute("sessionGpa", GPA)
                                .SetAttribute("cumulativeGpa", "0")
                                .SetAttribute("cumulativeQpa", "0")
                                '.SetAttribute("miscellaneousCode1", "0")
                                .SetAttribute("miscellaneousCode2", "0")
                                .SetAttribute("withdrawalCode", "")
                                ' .SetAttribute("withdrawalDate", "")
                                .SetAttribute("cumulativeTransferHours", "0")
                                '.SetAttribute("cipCode", "0")
                                .SetAttribute("gradeLevel", "0")
                                .SetAttribute("overrideHours", "0")
                                .SetAttribute("hoursAdjustment", "0")
                                .SetAttribute("miscellaneousHours1", "0")
                                .SetAttribute("miscellaneousHours2", "0")
                            End With
                            node.InsertAfter(elmNew, dir)
                        Next
                        boolActivityTagExists = True
                        Return xmlDoc
                        Exit Function
                    End If
                Next
            Next
        End If

        'If student data is not found in the XML file then 
        ' create the student tag and then add address tag to it.
        If boolStudentFound = False Then
            Dim elmRoot1 As XmlElement = xmlDoc.DocumentElement
            Dim dtgetStudentDetails As New DataTable
            dtgetStudentDetails = getStudentDetails(Studentid)
            xmlDoc = AppendStudentAttributeToExistingStudents(xmlDoc, elmRoot1, dtgetStudentDetails)
            xmlDoc = AppendNameAttribute(xmlDoc, elmRoot1, dtgetStudentDetails)
            'As Student is created now add the address
            lstStudents = xmlDoc.GetElementsByTagName("student")
            ' visit each Student
            For Each node As XmlNode In lstStudents
                Dim lstChildren As XmlNodeList = node.ChildNodes
                ' Visit each child node 
                For Each dir As XmlNode In lstChildren
                    If dir.LocalName.ToString.Trim = "name" And Not node.Attributes("id").Value.ToString.Trim = "" And node.Attributes("id").Value.ToString.Trim = strSSN.ToString Then
                        Try
                            strSession = node.Attributes("sessionApplied").Value.ToString.Trim
                        Catch ex As System.Exception
                            strSession = ""
                        End Try
                        boolStudentFound = True
                        For Each dr As DataRow In dt.Rows
                            Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "academic", "http://www.regenteducation.com/carbon/batchload")
                            With elmNew
                                .SetAttribute("session", dr("Session"))
                                If Not dr("degreecode") Is System.DBNull.Value Then .SetAttribute("degreeCode", dr("degreecode").ToString) Else .SetAttribute("degreeCode", "")
                                If Not dr("curriculumCode") Is System.DBNull.Value Then .SetAttribute("curriculumCode", dr("curriculumCode").ToString) Else .SetAttribute("curriculumCode", "")
                                .SetAttribute("appealCode", "")
                                ' .SetAttribute("appealdate", "")
                                .SetAttribute("sessionHoursAttempted", CreditsAttempted)
                                .SetAttribute("sessionHoursEarned", CreditsEarned)
                                .SetAttribute("sessionHoursFunded", "0")
                                .SetAttribute("academicStatus", "")
                                .SetAttribute("sessionGpa", GPA)
                                .SetAttribute("cumulativeGpa", "0")
                                .SetAttribute("cumulativeQpa", "0")
                                '.SetAttribute("miscellaneousCode1", "0")
                                .SetAttribute("miscellaneousCode2", "0")
                                .SetAttribute("withdrawalCode", "0")
                                '.SetAttribute("withdrawalDate", "")
                                .SetAttribute("cumulativeTransferHours", "0")
                                ' .SetAttribute("cipCode", "0")
                                .SetAttribute("gradeLevel", "0")
                                .SetAttribute("overrideHours", "0")
                                .SetAttribute("hoursAdjustment", "0")
                                .SetAttribute("miscellaneousHours1", "0")
                                .SetAttribute("miscellaneousHours2", "0")
                            End With
                            node.InsertAfter(elmNew, dir)
                        Next
                        Exit For
                    End If
                Next
            Next
        End If
        Return xmlDoc
    End Function
    Private Sub AppendAcademicAttribute(ByVal xmlDoc As XmlDocument, _
                                        ByVal elmRoot As XmlElement, _
                                        ByVal dtAcademicData As DataTable)
        Dim dr As DataRow
        Try
            dr = dtAcademicData.Rows(0)
        Catch ex As System.Exception
            Exit Sub
        End Try

        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "academic", "http://www.regenteducation.com/carbon/batchload")
        With elmNew
            If Not dr("session") Is System.DBNull.Value Then .SetAttribute("session", dr("session")) Else .SetAttribute("session", "")
            If Not dr("DegreeCode") Is System.DBNull.Value Then .SetAttribute("DegreeCode", dr("DegreeCode")) Else .SetAttribute("DegreeCode", "")
            If Not dr("CurriculumCode") Is System.DBNull.Value Then .SetAttribute("CurriculumCode", dr("CurriculumCode")) Else .SetAttribute("CurriculumCode", "")
            .SetAttribute("appealCode", "")
            .SetAttribute("appealdate", "")
            .SetAttribute("sessionHoursAttempted", "")
            .SetAttribute("sessionHoursEarned", "")
            .SetAttribute("sessionHoursFunded", "")
            If Not dr("academicStatus") Is System.DBNull.Value Then .SetAttribute("academicStatus", dr("academicStatus").ToString) Else .SetAttribute("academicStatus", "")
            .SetAttribute("sessionGpa", "")
            .SetAttribute("cumulativeGpa", "")
            .SetAttribute("cumulativeQpa", "")
            .SetAttribute("withdrawalCode", "")
            .SetAttribute("withdrawalDate", "")
            .SetAttribute("miscellaneousCode1", "")
            .SetAttribute("miscellaneousCode2", "")
            .SetAttribute("cumulativeTransferHours", "")
            .SetAttribute("cipCode", "")
            .SetAttribute("gradeLevel", "")
            .SetAttribute("overrideHours", "")
            .SetAttribute("hoursAdjustment", "")
            .SetAttribute("miscellaneousHours1", "")
            .SetAttribute("miscellaneousHours2", "")
        End With
        elmRoot.LastChild.AppendChild(elmNew)
    End Sub
    Public Sub AddTrackingXML(ByVal StudentId As String, _
                              ByVal filename As String, _
                              ByVal StudentTrackId As String)

        Dim xmlDoc As XmlDocument = New XmlDocument()
        Dim dtTrackingData As DataTable
        Dim dr As DataRow
        Dim strSSN As String = ""
        dtTrackingData = getStudentTrackingData(StudentTrackId)

        Try
            dr = dtTrackingData.Rows(0)
        Catch ex As System.Exception
            Exit Sub
        End Try
        If (File.Exists(filename)) Then
            xmlDoc.Load(filename)
            strSSN = getSSN(StudentId)
            xmlDoc = AddDocumentTracking(xmlDoc, dr, strSSN, StudentId)
            xmlDoc.Save(filename)
        Else
            'If file not found then create file with student data and then add address data 
            strSSN = CreateStudentDataForUpdates(StudentId, "")
            If File.Exists(filename) Then
                xmlDoc.Load(filename)
                xmlDoc = AddDocumentTracking(xmlDoc, dr, strSSN, StudentId)
                xmlDoc.Save(filename)
            End If
        End If
    End Sub
    Private Function AddDocumentTracking(ByVal xmlDoc As XmlDocument, _
                                         ByVal dr As DataRow, _
                                         ByVal strSSN As String, _
                                         ByVal Studentid As String) As XmlDocument
        'Get a reference to the root node
        'Dim elmRoot As XmlElement = xmlDoc.DocumentElement
        ''Create a list of the student
        'Dim lstStudents As XmlNodeList = xmlDoc.GetElementsByTagName("student")
        'Dim intStudentTagCount As Integer = 0
        'Dim boolStudentFound As Boolean = False
        'Do While intStudentTagCount < lstStudents.Count
        '    Dim curAttributes As XmlAttributeCollection = lstStudents(intStudentTagCount).Attributes
        '    Dim intAttrCount As Integer = 0
        '    Dim strSession As String = ""
        '    Try
        '        strSession = curAttributes("sessionApplied").InnerText.ToString()
        '    Catch ex As System.Exception
        '        strSession = "NA"
        '    End Try
        '    Do While intAttrCount < curAttributes.Count
        '        If Not curAttributes("id").InnerText Is Nothing AndAlso curAttributes("id").InnerText.ToString.Trim = strSSN.ToString.Trim Then
        '            boolStudentFound = True
        '            Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "tracking", "http://www.regenteducation.com/carbon/batchload")
        '            With elmNew
        '                .SetAttribute("session", strSession)
        '                If Not dr("trackCode") Is System.DBNull.Value Then
        '                    .SetAttribute("trackCode", Mid(dr("trackCode"), 1, 2))
        '                Else
        '                    .SetAttribute("trackCode", "")
        '                End If
        '                If Not dr("DueDate") Is System.DBNull.Value Then
        '                    .SetAttribute("dueDate", CDate(dr("DueDate")).ToString("yyyy-MM-dd"))
        '                Else
        '                    .SetAttribute("dueDate", "")
        '                End If
        '                If Not dr("notifiedDate") Is System.DBNull.Value Then
        '                    .SetAttribute("notifiedDate", CDate(dr("notifiedDate")).ToString("yyyy-MM-dd"))
        '                Else
        '                    .SetAttribute("notifiedDate", "")
        '                End If
        '                If Not dr("completedDate") Is System.DBNull.Value Then
        '                    .SetAttribute("completedDate", CDate(dr("completedDate")).ToString("yyyy-MM-dd"))
        '                Else
        '                    .SetAttribute("completedDate", "")
        '                End If
        '                If Not dr("notationCode") Is System.DBNull.Value Then
        '                    .SetAttribute("notationCode", dr("notationCode"))
        '                Else
        '                    .SetAttribute("trackCode", "")
        '                End If
        '                If Not dr("Description") Is System.DBNull.Value Then
        '                    .SetAttribute("note", dr("Description"))
        '                Else
        '                    .SetAttribute("note", "")
        '                End If
        '            End With
        '            lstStudents(intStudentTagCount).AppendChild(elmNew)
        '            Exit Do
        '        End If
        '        intAttrCount += 1
        '    Loop
        '    intStudentTagCount += 1
        'Loop
        'If boolStudentFound = False Then
        '    Dim elmRoot1 As XmlElement = xmlDoc.DocumentElement
        '    Dim dtgetStudentDetails As New DataTable
        '    dtgetStudentDetails = getStudentDetails(Studentid)
        '    xmlDoc = AppendStudentAttributeToExistingStudents(xmlDoc, elmRoot1, dtgetStudentDetails)
        'End If
        Dim elmRoot As XmlElement = xmlDoc.DocumentElement
        Dim boolStudentFound As Boolean = False
        Dim boolActivityTagExists As Boolean = False
        Dim strSession As String = ""
        'Create a list of the student
        Dim lstStudents As XmlNodeList = xmlDoc.GetElementsByTagName("student")
        ' visit each Student
        For Each node As XmlNode In lstStudents
            Dim lstChildren As XmlNodeList = node.ChildNodes
            ' Visit each child node 
            For Each dir As XmlNode In lstChildren
                If dir.LocalName.ToString.Trim = "tracking" And Not node.Attributes("id").Value.ToString.Trim = "" And node.Attributes("id").Value.ToString.Trim = strSSN.ToString Then
                    Try
                        strSession = node.Attributes("sessionApplied").Value.ToString.Trim
                    Catch ex As System.Exception
                        strSession = ""
                    End Try
                    boolStudentFound = True
                    boolActivityTagExists = True
                    If dir.Attributes("session").Value.ToString.Trim = strSession.Trim And _
                       dir.Attributes("trackCode").Value.ToString.Trim = dr("trackCode").ToString.Trim And _
                       dir.Attributes("note").Value.ToString.Trim = dr("Description").ToString.Trim Then
                        With dir
                            .Attributes("session").Value = strSession
                            If Not dr("trackCode") Is System.DBNull.Value Then
                                .Attributes("trackCode").Value = Mid(dr("trackCode"), 1, 2)
                            Else
                                .Attributes("trackCode").Value = ""
                            End If
                            If Not dr("dueDate") Is System.DBNull.Value Then
                                .Attributes("dueDate").Value = CDate(dr("dueDate")).ToString("yyyy-MM-dd")
                            Else
                                .Attributes("dueDate").Value = ""
                            End If
                            If Not dr("notifiedDate") Is System.DBNull.Value Then
                                .Attributes("notifiedDate").Value = CDate(dr("notifiedDate")).ToString("yyyy-MM-dd")
                            Else
                                .Attributes("notifiedDate").Value = ""
                            End If
                            If Not dr("completedDate") Is System.DBNull.Value Then
                                .Attributes("completedDate").Value = CDate(dr("completedDate")).ToString("yyyy-MM-dd")
                            Else
                                .Attributes("completedDate").Value = ""
                            End If
                            If Not dr("Notificationcode") Is System.DBNull.Value Then
                                .Attributes("notationCode").Value = dr("Notificationcode")
                            Else
                                .Attributes("notationCode").Value = ""
                            End If
                            If Not dr("Description") Is System.DBNull.Value Then
                                .Attributes("note").Value = dr("Description")
                            Else
                                .Attributes("note").Value = ""
                            End If
                        End With
                    Else
                        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "tracking", "http://www.regenteducation.com/carbon/batchload")
                        With elmNew
                            .SetAttribute("session", strSession)
                            If Not dr("trackCode") Is System.DBNull.Value Then
                                .SetAttribute("trackCode", Mid(dr("trackCode"), 1, 2))
                            Else
                                .SetAttribute("trackCode", "")
                            End If
                            If Not dr("DueDate") Is System.DBNull.Value Then
                                .SetAttribute("dueDate", CDate(dr("DueDate")).ToString("yyyy-MM-dd"))
                            Else
                                .SetAttribute("dueDate", "")
                            End If
                            If Not dr("notifiedDate") Is System.DBNull.Value Then
                                .SetAttribute("notifiedDate", CDate(dr("notifiedDate")).ToString("yyyy-MM-dd"))
                            Else
                                .SetAttribute("notifiedDate", "")
                            End If
                            If Not dr("completedDate") Is System.DBNull.Value Then
                                .SetAttribute("completedDate", CDate(dr("completedDate")).ToString("yyyy-MM-dd"))
                            Else
                                .SetAttribute("completedDate", "")
                            End If
                            If Not dr("Notificationcode") Is System.DBNull.Value Then
                                .SetAttribute("notationCode", dr("Notificationcode"))
                            Else
                                .SetAttribute("notationCode", "")
                            End If
                            If Not dr("Description") Is System.DBNull.Value Then
                                .SetAttribute("note", dr("Description"))
                            Else
                                .SetAttribute("note", "")
                            End If
                        End With
                        node.InsertAfter(elmNew, dir)
                    End If
                    Exit For
                End If
            Next
        Next

        'If Award tag is not found then we need to add the award tag
        If boolActivityTagExists = False Then
            lstStudents = xmlDoc.GetElementsByTagName("student")
            Dim intStudentTagCount As Integer = 0
            Do While intStudentTagCount < lstStudents.Count
                Dim curAttributes As XmlAttributeCollection = lstStudents(intStudentTagCount).Attributes
                Dim intAttrCount As Integer = 0
                Do While intAttrCount < curAttributes.Count
                    If Not curAttributes("id").InnerText Is Nothing AndAlso curAttributes("id").InnerText.ToString.Trim = strSSN.ToString.Trim Then
                        Try
                            strSession = curAttributes("sessionApplied").InnerText.ToString.Trim
                        Catch ex As System.Exception
                            strSession = ""
                        End Try
                        boolStudentFound = True
                        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "tracking", "http://www.regenteducation.com/carbon/batchload")
                        With elmNew
                            .SetAttribute("session", strSession)
                            If Not dr("trackCode") Is System.DBNull.Value Then
                                .SetAttribute("trackCode", Mid(dr("trackCode"), 1, 2))
                            Else
                                .SetAttribute("trackCode", "")
                            End If
                            If Not dr("DueDate") Is System.DBNull.Value Then
                                .SetAttribute("dueDate", CDate(dr("DueDate")).ToString("yyyy-MM-dd"))
                            Else
                                .SetAttribute("dueDate", "")
                            End If
                            If Not dr("notifiedDate") Is System.DBNull.Value Then
                                .SetAttribute("notifiedDate", CDate(dr("notifiedDate")).ToString("yyyy-MM-dd"))
                            Else
                                .SetAttribute("notifiedDate", "")
                            End If
                            If Not dr("completedDate") Is System.DBNull.Value Then
                                .SetAttribute("completedDate", CDate(dr("completedDate")).ToString("yyyy-MM-dd"))
                            Else
                                .SetAttribute("completedDate", "")
                            End If
                            If Not dr("Notificationcode") Is System.DBNull.Value Then
                                .SetAttribute("notationCode", dr("Notificationcode"))
                            Else
                                .SetAttribute("notationCode", "")
                            End If
                            If Not dr("Description") Is System.DBNull.Value Then
                                .SetAttribute("note", dr("Description"))
                            Else
                                .SetAttribute("note", "")
                            End If
                        End With
                        lstStudents(intStudentTagCount).AppendChild(elmNew)
                        boolActivityTagExists = True
                        Exit Do
                    End If
                    intAttrCount += 1
                Loop
                intStudentTagCount += 1
            Loop
        End If

        'If student data is not found in the XML file then 
        ' create the student tag and then add address tag to it.
        If boolStudentFound = False Then
            Dim elmRoot1 As XmlElement = xmlDoc.DocumentElement
            Dim dtgetStudentDetails As New DataTable
            dtgetStudentDetails = getStudentDetails(Studentid)
            xmlDoc = AppendStudentAttributeToExistingStudents(xmlDoc, elmRoot1, dtgetStudentDetails)
            xmlDoc = AppendNameAttribute(xmlDoc, elmRoot1, dtgetStudentDetails)
            'As Student is created now add the address
            lstStudents = xmlDoc.GetElementsByTagName("student")
            ' visit each Student
            For Each node As XmlNode In lstStudents
                Dim lstChildren As XmlNodeList = node.ChildNodes
                ' Visit each child node 
                For Each dir As XmlNode In lstChildren
                    If dir.LocalName.ToString.Trim = "name" And Not node.Attributes("id").Value.ToString.Trim = "" And node.Attributes("id").Value.ToString.Trim = strSSN.ToString Then
                        Try
                            strSession = node.Attributes("sessionApplied").Value.ToString.Trim
                        Catch ex As System.Exception
                            strSession = ""
                        End Try
                        boolStudentFound = True
                        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "tracking", "http://www.regenteducation.com/carbon/batchload")
                        With elmNew
                            .SetAttribute("session", strSession)
                            If Not dr("trackCode") Is System.DBNull.Value Then
                                .SetAttribute("trackCode", Mid(dr("trackCode"), 1, 2))
                            Else
                                .SetAttribute("trackCode", "")
                            End If
                            If Not dr("DueDate") Is System.DBNull.Value Then
                                .SetAttribute("dueDate", CDate(dr("DueDate")).ToString("yyyy-MM-dd"))
                            Else
                                .SetAttribute("dueDate", "")
                            End If
                            If Not dr("notifiedDate") Is System.DBNull.Value Then
                                .SetAttribute("notifiedDate", CDate(dr("notifiedDate")).ToString("yyyy-MM-dd"))
                            Else
                                .SetAttribute("notifiedDate", "")
                            End If
                            If Not dr("completedDate") Is System.DBNull.Value Then
                                .SetAttribute("completedDate", CDate(dr("completedDate")).ToString("yyyy-MM-dd"))
                            Else
                                .SetAttribute("completedDate", "")
                            End If
                            If Not dr("Notificationcode") Is System.DBNull.Value Then
                                .SetAttribute("notationCode", dr("Notificationcode"))
                            Else
                                .SetAttribute("notationCode", "")
                            End If
                            If Not dr("Description") Is System.DBNull.Value Then
                                .SetAttribute("note", dr("Description"))
                            Else
                                .SetAttribute("note", "")
                            End If
                        End With
                        node.InsertAfter(elmNew, dir)
                        Exit For
                    End If
                Next
            Next
        End If
        Return xmlDoc
    End Function
    Public Sub AddFinancialAidXML(ByVal StudentId As String, _
                                  ByVal filename As String, _
                                  ByVal StudentFinAidId As String)

        Dim xmlDoc As XmlDocument = New XmlDocument()
        Dim dtFinAidData As DataTable
        Dim strSSN As String = ""
        Dim dr As DataRow
        dtFinAidData = getStudentFinAidData(StudentFinAidId, "Append")

        Try
            dr = dtFinAidData.Rows(0)
        Catch ex As System.Exception
            Exit Sub
        End Try
        If (File.Exists(filename)) Then
            xmlDoc.Load(filename)
            strSSN = getSSN(StudentId)
            xmlDoc = AddFinancialAidDataForExistingStudent(xmlDoc, dr, strSSN, StudentId)
            xmlDoc.Save(filename)
        Else
            'If file not found then create file with student data and then add address data 
            strSSN = CreateStudentDataForUpdates(StudentId, "")
            If File.Exists(filename) Then
                xmlDoc.Load(filename)
                xmlDoc = AddFinancialAidDataForExistingStudent(xmlDoc, dr, strSSN, StudentId)
                xmlDoc.Save(filename)
            End If
        End If
    End Sub
    Private Function AddFinancialAidDataForExistingStudent(ByVal xmlDoc As XmlDocument, _
                                                           ByVal dr As DataRow, _
                                                           ByVal strSSN As String, _
                                                           ByVal Studentid As String) As XmlDocument
        'Get a reference to the root node
        'Dim elmRoot As XmlElement = xmlDoc.DocumentElement
        ''Create a list of the student
        'Dim lstStudents As XmlNodeList = xmlDoc.GetElementsByTagName("student")
        'Dim intStudentTagCount As Integer = 0
        'Dim boolStudentFound As Boolean = False
        'Do While intStudentTagCount < lstStudents.Count
        '    Dim curAttributes As XmlAttributeCollection = lstStudents(intStudentTagCount).Attributes
        '    Dim intAttrCount As Integer = 0
        '    Do While intAttrCount < curAttributes.Count
        '        'Dim graddateattr As XmlAttribute = curAttributes("ssn")
        '        'If any one of the student tag has a ssn="123456789" then add the address tag to it
        '        If Not curAttributes("id").InnerText Is Nothing AndAlso curAttributes("id").InnerText.ToString.Trim = strSSN.ToString.Trim Then
        '            boolStudentFound = True
        '            Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "financialAid", "http://www.regenteducation.com/carbon/batchload")
        '            With elmNew
        '                '.SetAttribute("sessionStart", "")
        '                '.SetAttribute("sessionEnd", "")
        '                '.SetAttribute("status", "")
        '                If Not dr("SessionStartTerm") Is System.DBNull.Value Then .SetAttribute("sessionStart", dr("SessionStartTerm")) Else .SetAttribute("sessionStart", "")
        '                If Not dr("SessionEndTerm") Is System.DBNull.Value Then .SetAttribute("sessionEnd", dr("SessionEndTerm")) Else .SetAttribute("sessionEnd", "")
        '                .SetAttribute("status", "AC")
        '                .SetAttribute("freezeStatus", "")
        '                .SetAttribute("budgetCode", "")
        '                .SetAttribute("budgetAmount", "0.00")
        '                .SetAttribute("budgetMonths", "0")
        '                .SetAttribute("childcareAmount", "")
        '                .SetAttribute("supplementalAmount", "0.00")
        '                .SetAttribute("programCost", "0.00")
        '                .SetAttribute("packageCode", "0.00")
        '                .SetAttribute("additionalAmount", "0.00")
        '                .SetAttribute("scheduleCosts", "0.00")
        '                .SetAttribute("enrollmentCode1", "")
        '                .SetAttribute("budgetCode1", "")
        '                .SetAttribute("enrollmentCode2", "")
        '                .SetAttribute("budgetCode2", "")
        '                .SetAttribute("enrollmentCode3", "")
        '                .SetAttribute("budgetCode3", "")
        '                .SetAttribute("enrollmentCode4", "")
        '                .SetAttribute("budgetCode4", "")
        '                .SetAttribute("enrollmentCode5", "")
        '                .SetAttribute("budgetCode5", "")
        '                .SetAttribute("parentIncome", "")
        '                .SetAttribute("studentIncome", "")
        '                .SetAttribute("numberOfDependents", "")
        '                .SetAttribute("parentContribution", "")
        '                .SetAttribute("studentContribution", "")
        '                .SetAttribute("parentContributionIM", "")
        '                .SetAttribute("studentContributionIM", "")
        '                .SetAttribute("efc", "")
        '                .SetAttribute("costOfEducation", "")
        '                .SetAttribute("enrollmentCode", "")
        '                .SetAttribute("sarId", "")
        '                .SetAttribute("hoursYear", "0.00")
        '                .SetAttribute("hoursExp", "0.00")
        '                .SetAttribute("weeksYear", "0")
        '                .SetAttribute("weeksExp", "0")
        '                .SetAttribute("codCitizenshipCode", "")
        '                .SetAttribute("needAmount", "0.00")
        '                .SetAttribute("institutionalNeedAmount", "0.00")
        '                .SetAttribute("afdcOrTanf", "")
        '                .SetAttribute("dependencyStatus", "")
        '                .SetAttribute("yearInSchool", "")
        '                '.SetAttribute("parentOrStudentMaritalStatus", "")
        '                If Not dr("MaritalStatus") Is System.DBNull.Value Then .SetAttribute("parentOrStudentMaritalStatus", dr("MaritalStatus")) Else .SetAttribute("parentOrStudentMaritalStatus", "")
        '                .SetAttribute("pellEligibility", "")
        '                .SetAttribute("coaOverride", "")
        '            End With
        '            lstStudents(intStudentTagCount).AppendChild(elmNew)
        '            Exit Do
        '        End If
        '        intAttrCount += 1
        '    Loop
        '    intStudentTagCount += 1
        'Loop
        'If boolStudentFound = False Then
        '    Dim elmRoot1 As XmlElement = xmlDoc.DocumentElement
        '    Dim dtgetStudentDetails As New DataTable
        '    dtgetStudentDetails = getStudentDetails(Studentid)
        '    xmlDoc = AppendStudentAttributeToExistingStudents(xmlDoc, elmRoot1, dtgetStudentDetails)
        'End If
        Dim elmRoot As XmlElement = xmlDoc.DocumentElement
        Dim boolStudentFound As Boolean = False
        Dim boolActivityTagExists As Boolean = False
        Dim strSession As String = ""
        'Create a list of the student
        Dim lstStudents As XmlNodeList = xmlDoc.GetElementsByTagName("student")
        ' visit each Student
        For Each node As XmlNode In lstStudents
            Dim lstChildren As XmlNodeList = node.ChildNodes
            ' Visit each child node 
            For Each dir As XmlNode In lstChildren

                If dir.LocalName.ToString.Trim = "financialAid" And Not node.Attributes("id").Value.ToString.Trim = "" And node.Attributes("id").Value.ToString.Trim = strSSN.ToString Then
                    boolStudentFound = True
                    boolActivityTagExists = True
                    Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "financialAid", "http://www.regenteducation.com/carbon/batchload")
                    With elmNew
                        If Not dr("SessionStartTerm") Is System.DBNull.Value Then .SetAttribute("sessionStart", dr("SessionStartTerm")) Else .SetAttribute("sessionStart", "")
                        If Not dr("SessionEndTerm") Is System.DBNull.Value Then .SetAttribute("sessionEnd", dr("SessionEndTerm")) Else .SetAttribute("sessionEnd", "")
                        .SetAttribute("status", "AC")
                        .SetAttribute("freezeStatus", "")
                        .SetAttribute("budgetCode", "")
                        .SetAttribute("budgetAmount", "0.00")
                        .SetAttribute("budgetMonths", "0")
                        .SetAttribute("childcareAmount", "")
                        .SetAttribute("supplementalAmount", "0.00")
                        .SetAttribute("programCost", "0.00")
                        .SetAttribute("packageCode", "0.00")
                        .SetAttribute("additionalAmount", "0.00")
                        .SetAttribute("scheduleCosts", "0.00")
                        .SetAttribute("enrollmentCode1", "")
                        .SetAttribute("budgetCode1", "")
                        .SetAttribute("enrollmentCode2", "")
                        .SetAttribute("budgetCode2", "")
                        .SetAttribute("enrollmentCode3", "")
                        .SetAttribute("budgetCode3", "")
                        .SetAttribute("enrollmentCode4", "")
                        .SetAttribute("budgetCode4", "")
                        .SetAttribute("enrollmentCode5", "")
                        .SetAttribute("budgetCode5", "")
                        .SetAttribute("parentIncome", "")
                        .SetAttribute("studentIncome", "")
                        .SetAttribute("numberOfDependents", "")
                        .SetAttribute("parentContribution", "")
                        .SetAttribute("studentContribution", "")
                        .SetAttribute("parentContributionIM", "")
                        .SetAttribute("studentContributionIM", "")
                        .SetAttribute("efc", "")
                        .SetAttribute("costOfEducation", "")
                        .SetAttribute("enrollmentCode", "")
                        .SetAttribute("sarId", "")
                        .SetAttribute("hoursYear", "0.00")
                        .SetAttribute("hoursExp", "0.00")
                        .SetAttribute("weeksYear", "0")
                        .SetAttribute("weeksExp", "0")
                        .SetAttribute("codCitizenshipCode", "")
                        .SetAttribute("needAmount", "0.00")
                        .SetAttribute("institutionalNeedAmount", "0.00")
                        .SetAttribute("afdcOrTanf", "")
                        .SetAttribute("dependencyStatus", "")
                        .SetAttribute("yearInSchool", "")
                        '.SetAttribute("parentOrStudentMaritalStatus", "")
                        If Not dr("MaritalStatus") Is System.DBNull.Value Then .SetAttribute("parentOrStudentMaritalStatus", dr("MaritalStatus")) Else .SetAttribute("parentOrStudentMaritalStatus", "")
                        .SetAttribute("pellEligibility", "")
                        .SetAttribute("coaOverride", "")
                    End With
                    node.InsertAfter(elmNew, dir)
                    Exit For
                End If
            Next
        Next

        'If Award tag is not found then we need to add the award tag
        If boolActivityTagExists = False Then
            lstStudents = xmlDoc.GetElementsByTagName("student")
            Dim intStudentTagCount As Integer = 0
            Do While intStudentTagCount < lstStudents.Count
                Dim curAttributes As XmlAttributeCollection = lstStudents(intStudentTagCount).Attributes
                Dim intAttrCount As Integer = 0
                Do While intAttrCount < curAttributes.Count
                    If Not curAttributes("id").InnerText Is Nothing AndAlso curAttributes("id").InnerText.ToString.Trim = strSSN.ToString.Trim Then
                        boolStudentFound = True
                        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "financialAid", "http://www.regenteducation.com/carbon/batchload")
                        With elmNew
                            If Not dr("SessionStartTerm") Is System.DBNull.Value Then .SetAttribute("sessionStart", dr("SessionStartTerm")) Else .SetAttribute("sessionStart", "")
                            If Not dr("SessionEndTerm") Is System.DBNull.Value Then .SetAttribute("sessionEnd", dr("SessionEndTerm")) Else .SetAttribute("sessionEnd", "")
                            .SetAttribute("status", "AC")
                            .SetAttribute("freezeStatus", "")
                            .SetAttribute("budgetCode", "")
                            .SetAttribute("budgetAmount", "0.00")
                            .SetAttribute("budgetMonths", "0")
                            .SetAttribute("childcareAmount", "")
                            .SetAttribute("supplementalAmount", "0.00")
                            .SetAttribute("programCost", "0.00")
                            .SetAttribute("packageCode", "0.00")
                            .SetAttribute("additionalAmount", "0.00")
                            .SetAttribute("scheduleCosts", "0.00")
                            .SetAttribute("enrollmentCode1", "")
                            .SetAttribute("budgetCode1", "")
                            .SetAttribute("enrollmentCode2", "")
                            .SetAttribute("budgetCode2", "")
                            .SetAttribute("enrollmentCode3", "")
                            .SetAttribute("budgetCode3", "")
                            .SetAttribute("enrollmentCode4", "")
                            .SetAttribute("budgetCode4", "")
                            .SetAttribute("enrollmentCode5", "")
                            .SetAttribute("budgetCode5", "")
                            .SetAttribute("parentIncome", "")
                            .SetAttribute("studentIncome", "")
                            .SetAttribute("numberOfDependents", "")
                            .SetAttribute("parentContribution", "")
                            .SetAttribute("studentContribution", "")
                            .SetAttribute("parentContributionIM", "")
                            .SetAttribute("studentContributionIM", "")
                            .SetAttribute("efc", "")
                            .SetAttribute("costOfEducation", "")
                            .SetAttribute("enrollmentCode", "")
                            .SetAttribute("sarId", "")
                            .SetAttribute("hoursYear", "0.00")
                            .SetAttribute("hoursExp", "0.00")
                            .SetAttribute("weeksYear", "0")
                            .SetAttribute("weeksExp", "0")
                            .SetAttribute("codCitizenshipCode", "")
                            .SetAttribute("needAmount", "0.00")
                            .SetAttribute("institutionalNeedAmount", "0.00")
                            .SetAttribute("afdcOrTanf", "")
                            .SetAttribute("dependencyStatus", "")
                            .SetAttribute("yearInSchool", "")
                            '.SetAttribute("parentOrStudentMaritalStatus", "")
                            If Not dr("MaritalStatus") Is System.DBNull.Value Then .SetAttribute("parentOrStudentMaritalStatus", dr("MaritalStatus")) Else .SetAttribute("parentOrStudentMaritalStatus", "")
                            .SetAttribute("pellEligibility", "")
                            .SetAttribute("coaOverride", "")
                        End With
                        lstStudents(intStudentTagCount).AppendChild(elmNew)
                        boolActivityTagExists = True
                        Exit Do
                    End If
                    intAttrCount += 1
                Loop
                intStudentTagCount += 1
            Loop
        End If

        'If student data is not found in the XML file then 
        ' create the student tag and then add address tag to it.
        If boolStudentFound = False Then
            Dim elmRoot1 As XmlElement = xmlDoc.DocumentElement
            Dim dtgetStudentDetails As New DataTable
            dtgetStudentDetails = getStudentDetails(Studentid)
            xmlDoc = AppendStudentAttributeToExistingStudents(xmlDoc, elmRoot1, dtgetStudentDetails)
            xmlDoc = AppendNameAttribute(xmlDoc, elmRoot1, dtgetStudentDetails)
            'As Student is created now add the address
            lstStudents = xmlDoc.GetElementsByTagName("student")
            ' visit each Student
            For Each node As XmlNode In lstStudents
                Dim lstChildren As XmlNodeList = node.ChildNodes
                ' Visit each child node 
                For Each dir As XmlNode In lstChildren
                    If dir.LocalName.ToString.Trim = "name" And Not node.Attributes("id").Value.ToString.Trim = "" And node.Attributes("id").Value.ToString.Trim = strSSN.ToString Then
                        boolStudentFound = True
                        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "financialAid", "http://www.regenteducation.com/carbon/batchload")
                        With elmNew
                            If Not dr("SessionStartTerm") Is System.DBNull.Value Then .SetAttribute("sessionStart", dr("SessionStartTerm")) Else .SetAttribute("sessionStart", "")
                            If Not dr("SessionEndTerm") Is System.DBNull.Value Then .SetAttribute("sessionEnd", dr("SessionEndTerm")) Else .SetAttribute("sessionEnd", "")
                            .SetAttribute("status", "AC")
                            .SetAttribute("freezeStatus", "")
                            .SetAttribute("budgetCode", "")
                            .SetAttribute("budgetAmount", "0.00")
                            .SetAttribute("budgetMonths", "0")
                            .SetAttribute("childcareAmount", "")
                            .SetAttribute("supplementalAmount", "0.00")
                            .SetAttribute("programCost", "0.00")
                            .SetAttribute("packageCode", "0.00")
                            .SetAttribute("additionalAmount", "0.00")
                            .SetAttribute("scheduleCosts", "0.00")
                            .SetAttribute("enrollmentCode1", "")
                            .SetAttribute("budgetCode1", "")
                            .SetAttribute("enrollmentCode2", "")
                            .SetAttribute("budgetCode2", "")
                            .SetAttribute("enrollmentCode3", "")
                            .SetAttribute("budgetCode3", "")
                            .SetAttribute("enrollmentCode4", "")
                            .SetAttribute("budgetCode4", "")
                            .SetAttribute("enrollmentCode5", "")
                            .SetAttribute("budgetCode5", "")
                            .SetAttribute("parentIncome", "")
                            .SetAttribute("studentIncome", "")
                            .SetAttribute("numberOfDependents", "")
                            .SetAttribute("parentContribution", "")
                            .SetAttribute("studentContribution", "")
                            .SetAttribute("parentContributionIM", "")
                            .SetAttribute("studentContributionIM", "")
                            .SetAttribute("efc", "")
                            .SetAttribute("costOfEducation", "")
                            .SetAttribute("enrollmentCode", "")
                            .SetAttribute("sarId", "")
                            .SetAttribute("hoursYear", "0.00")
                            .SetAttribute("hoursExp", "0.00")
                            .SetAttribute("weeksYear", "0")
                            .SetAttribute("weeksExp", "0")
                            .SetAttribute("codCitizenshipCode", "")
                            .SetAttribute("needAmount", "0.00")
                            .SetAttribute("institutionalNeedAmount", "0.00")
                            .SetAttribute("afdcOrTanf", "")
                            .SetAttribute("dependencyStatus", "")
                            .SetAttribute("yearInSchool", "")
                            '.SetAttribute("parentOrStudentMaritalStatus", "")
                            If Not dr("MaritalStatus") Is System.DBNull.Value Then .SetAttribute("parentOrStudentMaritalStatus", dr("MaritalStatus")) Else .SetAttribute("parentOrStudentMaritalStatus", "")
                            .SetAttribute("pellEligibility", "")
                            .SetAttribute("coaOverride", "")
                        End With
                        node.InsertAfter(elmNew, dir)
                        Exit For
                    End If
                Next
            Next
        End If
        Return xmlDoc
    End Function
    Public Function CreateStudentDataForUpdates(ByVal StudentId As String, ByVal CampusId As String) As String
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'In advantage students are created when a lead is enrolled.At that 
        'point of time award,academic and financial aid data is not available
        'so these elements are left out of student tag during serialization.

        'However the code will be added to Student Awards,academic session
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Dim dtgetStudentDetails As New DataTable
        Dim dtgetStudentAddress As New DataTable
        Dim dtgetStudentPhone As New DataTable
        Dim dtGetEmail As New DataTable
        Dim dtgetStudentMissingRequirement As New DataTable
        Dim strPhone As String = ""

        'Create instance of Students,StudentType,addressType,nameType objects
        Dim regStudents As students = New students()
        Dim regStudent As studentType = New studentType
        'Dim regAddressType As addressType = New addressType
        'Dim regNameType As nameType = New nameType
        'Dim regEmailType As emailType = New emailType
        'Dim regDemographicType As demographicType = New demographicType
        'Dim regTrackingType As trackingType = New trackingType

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Create instances of Student,address and name types 
        regStudents.student = Array.CreateInstance(regStudent.GetType, 1)
        'regStudent.address = Array.CreateInstance(regAddressType.GetType, 1)
        'regStudent.name = Array.CreateInstance(regNameType.GetType, 1)
        'regStudent.email = Array.CreateInstance(regEmailType.GetType, 1)
        'regStudent.tracking = Array.CreateInstance(regTrackingType.GetType, 1)

        dtgetStudentDetails = getStudentDetails(StudentId)
        'dtgetStudentAddress = getStudentAddress(StudentId)
        'dtgetStudentPhone = getStudentPhone(StudentId)
        'dtgetStudentMissingRequirement = getAllMissingRequirements(StudentId, CampusId)

        If dtgetStudentPhone.Rows.Count >= 1 Then
            strPhone = CType(dtgetStudentPhone.Rows(0)("Phone"), String)
        End If
        'regAddressType = PopulateStudentAddress(dtgetStudentAddress, strPhone)
        'regNameType = PopulateStudentName(dtgetStudentDetails)
        'regEmailType = PopulateEmail(dtgetStudentDetails)
        'regDemographicType = PopulateDemographicData(dtgetStudentDetails)
        'regTrackingType = PopulateDocumentTracking(dtgetStudentMissingRequirement)

        Dim dr As DataRow
        Dim strSession As String
        dr = dtgetStudentDetails.Rows(0)
        Dim strNumberOfCampus As String = MyAdvAppSettings.AppSettings("NumberofCampuses")
        If strNumberOfCampus.Length < 10 Then
            strNumberOfCampus = "0" + strNumberOfCampus.ToString
        End If
        'If Not dr("SessionApplied") Is System.DBNull.Value Then regTrackingType.session = dr("SessionApplied") Else regTrackingType.session = ""
        Dim strSSN As String = ""
        strSSN = dr("SSN")
        With regStudent
            '.name(0) = regNameType
            '.address(0) = regAddressType
            '.email(0) = regEmailType
            '.demographic = regDemographicType
            '.tracking(0) = regTrackingType
            .id = dr("SSN")
            .graduationDate = dr("GraduationDate")
            .status = "AC"
            If Not dr("StateOfResidence") Is System.DBNull.Value Then .stateOfResidence = dr("StateOfResidence") Else .stateOfResidence = ""
            .institutionCode = strNumberOfCampus
            If Not dr("SessionApplied") Is System.DBNull.Value Then .sessionApplied = dr("SessionApplied") Else .sessionApplied = ""
            strSession = .sessionApplied
            .sessionRemaining = "0.00"
            If Not dr("SessionStartTerm") Is System.DBNull.Value Then .sessionStart = dr("SessionStartTerm") Else .sessionStart = ""
            If Not dr("SessionEndTerm") Is System.DBNull.Value Then .sessionEnd = dr("SessionEndTerm") Else .sessionEnd = ""
            .eligibleCode = "A0"
            If Not dr("SessionApplied") Is System.DBNull.Value Then .eligibleSession = dr("SessionApplied") Else .eligibleSession = ""
            .hoursRemaining = ""
            If Not dr("CurriculumCode") Is System.DBNull.Value Then .curriculumCode = dr("CurriculumCode") Else .curriculumCode = ""
            .miscellaneousCode1 = ""
            .miscellaneousCode2 = ""
            .miscellaneousCode3 = ""
            .miscellaneousCode4 = ""
            ' If Not dr("EntranceInterviewDate") Is System.DBNull.Value Then .entranceInterviewDate = dr("EntranceInterviewDate") Else .entranceInterviewDate = ""
            'If Not dr("ExitInterviewDate") Is System.DBNull.Value Then .exitInterviewDate = dr("ExitInterviewDate") Else .exitInterviewDate = ""
            .acgEligibilityReasonCode = ""
            If Not dr("HighSchoolProgramCode") Is System.DBNull.Value Then .highSchoolProgramCode = dr("HighSchoolProgramCode") Else .highSchoolProgramCode = ""
            .nameType = "STUD"
            .nameCode = "MSTR"
            .addressType = "STUD"
            .addressCode = "MSTR"
            .emailType = "STUD"
            .emailCode = "MSTR"
            If Not dr("lenderId") Is System.DBNull.Value Then .lenderId = dr("lenderId") Else .lenderId = ""
        End With
        regStudents.student(0) = regStudent

        Dim serializer As System.Xml.Serialization.XmlSerializer
        Dim getFileName As String = AdvantageCommonValues.getStudentBatchFileName()
        Dim file As System.IO.FileStream = New System.IO.FileStream(getFileName, IO.FileMode.OpenOrCreate)
        serializer = New System.Xml.Serialization.XmlSerializer(GetType(students))
        serializer.Serialize(file, regStudents)
        file.Close()
        Return strSSN
    End Function
    Public Sub AddEmailXML(ByVal StudentId As String, _
                           ByVal filename As String, _
                           ByVal StudentAddressId As String, _
                           Optional ByVal EmailType As String = "", _
                           Optional ByVal EmailCode As String = "")

        Dim xmlDoc As XmlDocument = New XmlDocument()
        Dim dtStudentAddress As DataTable
        Dim dr As DataRow
        Dim strSSN As String = ""
        dtStudentAddress = getStudentDetails(StudentId)

        Try
            dr = dtStudentAddress.Rows(0)
        Catch ex As System.Exception
            Exit Sub
        End Try
        'If file with todays date exists then open the file and add the address info
        'If not then create the student tag and then add address to it
        If (File.Exists(filename)) Then
            xmlDoc.Load(filename)
            strSSN = getSSN(StudentId)
            xmlDoc = AddEmailForExistingStudent(xmlDoc, dr, strSSN, StudentId, EmailType, EmailCode)
            xmlDoc.Save(filename)
        Else
            'If file not found then create file with student data and then add address data 
            strSSN = CreateStudentDataForUpdates(StudentId, "")
            If File.Exists(filename) Then
                xmlDoc.Load(filename)
                xmlDoc = AddEmailForExistingStudent(xmlDoc, dr, strSSN, StudentId, EmailType, EmailCode)
                xmlDoc.Save(filename)
            End If
        End If
    End Sub
    Public Function AddEmailForExistingStudent(ByVal xmlDoc As XmlDocument, ByVal dr As DataRow, ByVal strSSN As String, ByVal Studentid As String, _
                                               Optional ByVal EmailType As String = "", Optional ByVal EmailCode As String = "") As XmlDocument
        'Get a reference to the root node
        'Dim elmRoot As XmlElement = xmlDoc.DocumentElement
        'Dim boolStudentFound As Boolean = False
        ''Create a list of the student
        'Dim lstStudents As XmlNodeList = xmlDoc.GetElementsByTagName("student")
        'Dim intStudentTagCount As Integer = 0
        'Do While intStudentTagCount < lstStudents.Count
        '    Dim curAttributes As XmlAttributeCollection = lstStudents(intStudentTagCount).Attributes
        '    Dim intAttrCount As Integer = 0
        '    Do While intAttrCount < curAttributes.Count
        '        If Not curAttributes("id").InnerText Is Nothing AndAlso curAttributes("id").InnerText.ToString.Trim = strSSN.ToString.Trim Then
        '            boolStudentFound = True
        '            Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "email", "http://www.regenteducation.com/carbon/batchload")
        '            With elmNew
        '                .SetAttribute("type", EmailType)
        '                .SetAttribute("code", EmailCode)
        '                If EmailType <> "" Then
        '                    If EmailType.ToLower = "home" Then
        '                        If Not dr("HomeEmail") Is System.DBNull.Value Then .SetAttribute("address", dr("HomeEmail")) Else .SetAttribute("address", "")
        '                    Else
        '                        If Not dr("WorkEmail") Is System.DBNull.Value Then .SetAttribute("address", dr("WorkEmail")) Else .SetAttribute("address", "")
        '                    End If

        '                Else
        '                    .SetAttribute("address", "")
        '                End If

        '            End With
        '            lstStudents(intStudentTagCount).AppendChild(elmNew)
        '            Exit Do
        '        End If
        '        intAttrCount += 1
        '    Loop
        '    intStudentTagCount += 1
        'Loop
        'If boolStudentFound = False Then
        '    Dim elmRoot1 As XmlElement = xmlDoc.DocumentElement
        '    Dim dtgetStudentDetails As New DataTable
        '    dtgetStudentDetails = getStudentDetails(Studentid)
        '    xmlDoc = AppendStudentAttributeToExistingStudents(xmlDoc, elmRoot1, dtgetStudentDetails)
        'End If
        Dim elmRoot As XmlElement = xmlDoc.DocumentElement
        Dim boolStudentFound As Boolean = False
        Dim boolActivityTagExists As Boolean = False
        Dim strSession As String = ""
        'Create a list of the student
        Dim lstStudents As XmlNodeList = xmlDoc.GetElementsByTagName("student")
        ' visit each Student
        For Each node As XmlNode In lstStudents
            Dim lstChildren As XmlNodeList = node.ChildNodes
            ' Visit each child node 
            For Each dir As XmlNode In lstChildren
                If dir.LocalName.ToString.Trim = "email" And Not node.Attributes("id").Value.ToString.Trim = "" And node.Attributes("id").Value.ToString.Trim = strSSN.ToString Then
                    boolStudentFound = True
                    boolActivityTagExists = True
                    If dir.Attributes("type").Value.ToString.Trim = EmailType.ToString.Trim And _
                       dir.Attributes("code").Value.ToString.Trim = EmailCode.ToString.Trim Then
                        With dir
                            If EmailType <> "" Then
                                If EmailType.ToLower = "home" Then
                                    If Not dr("HomeEmail") Is System.DBNull.Value Then .Attributes("address").Value = dr("HomeEmail") Else .Attributes("address").Value = ""
                                Else
                                    If Not dr("WorkEmail") Is System.DBNull.Value Then .Attributes("address").Value = dr("WorkEmail") Else .Attributes("address").Value = ""
                                End If
                            Else
                                .Attributes("address").Value = ""
                            End If
                        End With
                    Else
                        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "email", "http://www.regenteducation.com/carbon/batchload")
                        With elmNew
                            .SetAttribute("type", EmailType)
                            .SetAttribute("code", EmailCode)
                            If EmailType <> "" Then
                                If EmailType.ToLower = "home" Then
                                    If Not dr("HomeEmail") Is System.DBNull.Value Then .SetAttribute("address", dr("HomeEmail")) Else .SetAttribute("address", "")
                                Else
                                    If Not dr("WorkEmail") Is System.DBNull.Value Then .SetAttribute("address", dr("WorkEmail")) Else .SetAttribute("address", "")
                                End If
                            Else
                                .SetAttribute("address", "")
                            End If
                        End With
                        node.InsertAfter(elmNew, dir)
                    End If
                    Exit For
                End If
            Next
        Next

        'If Award tag is not found then we need to add the award tag
        If boolActivityTagExists = False Then
            lstStudents = xmlDoc.GetElementsByTagName("student")
            Dim intStudentTagCount As Integer = 0
            Do While intStudentTagCount < lstStudents.Count
                Dim curAttributes As XmlAttributeCollection = lstStudents(intStudentTagCount).Attributes
                Dim intAttrCount As Integer = 0
                Do While intAttrCount < curAttributes.Count
                    If Not curAttributes("id").InnerText Is Nothing AndAlso curAttributes("id").InnerText.ToString.Trim = strSSN.ToString.Trim Then
                        boolStudentFound = True
                        If EmailType <> "" Then
                            Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "email", "http://www.regenteducation.com/carbon/batchload")
                            With elmNew
                                .SetAttribute("type", EmailType)
                                .SetAttribute("code", EmailCode)
                                If EmailType <> "" Then
                                    If EmailType.ToLower = "home" Then
                                        If Not dr("HomeEmail") Is System.DBNull.Value Then .SetAttribute("address", dr("HomeEmail")) Else .SetAttribute("address", "")
                                    Else
                                        If Not dr("WorkEmail") Is System.DBNull.Value Then .SetAttribute("address", dr("WorkEmail")) Else .SetAttribute("address", "")
                                    End If

                                Else
                                    .SetAttribute("address", "")
                                End If

                            End With
                            lstStudents(intStudentTagCount).AppendChild(elmNew)
                            boolActivityTagExists = True
                        End If
                        Exit Do
                    End If
                    intAttrCount += 1
                Loop
                intStudentTagCount += 1
            Loop
        End If

        'If student data is not found in the XML file then 
        ' create the student tag and then add address tag to it.
        If boolStudentFound = False Then
            Dim elmRoot1 As XmlElement = xmlDoc.DocumentElement
            Dim dtgetStudentDetails As New DataTable
            dtgetStudentDetails = getStudentDetails(Studentid)
            xmlDoc = AppendStudentAttributeToExistingStudents(xmlDoc, elmRoot1, dtgetStudentDetails)
            xmlDoc = AppendNameAttribute(xmlDoc, elmRoot1, dtgetStudentDetails)
            'As Student is created now add the address
            lstStudents = xmlDoc.GetElementsByTagName("student")
            ' visit each Student
            For Each node As XmlNode In lstStudents
                Dim lstChildren As XmlNodeList = node.ChildNodes
                ' Visit each child node 
                For Each dir As XmlNode In lstChildren
                    If dir.LocalName.ToString.Trim = "name" And Not node.Attributes("id").Value.ToString.Trim = "" And node.Attributes("id").Value.ToString.Trim = strSSN.ToString Then
                        boolStudentFound = True
                        If EmailType <> "" Then
                            Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "email", "http://www.regenteducation.com/carbon/batchload")
                            With elmNew
                                .SetAttribute("type", EmailType)
                                .SetAttribute("code", EmailCode)
                                If EmailType <> "" Then
                                    If EmailType.ToLower = "home" Then
                                        If Not dr("HomeEmail") Is System.DBNull.Value Then .SetAttribute("address", dr("HomeEmail")) Else .SetAttribute("address", "")
                                    Else
                                        If Not dr("WorkEmail") Is System.DBNull.Value Then .SetAttribute("address", dr("WorkEmail")) Else .SetAttribute("address", "")
                                    End If
                                Else
                                    .SetAttribute("address", "")
                                End If
                            End With
                            node.InsertAfter(elmNew, dir)
                            Exit For
                        End If
                    End If
                Next
            Next
        End If
        Return xmlDoc
    End Function
    Public Sub AddCommentXML(ByVal StudentId As String, _
                             ByVal filename As String, _
                             ByVal StudentTrackId As String)

        Dim xmlDoc As XmlDocument = New XmlDocument()
        Dim dtTrackingData As DataTable
        Dim dr As DataRow
        Dim strSSN As String = ""
        dtTrackingData = getStudentDetails(StudentId)

        Try
            dr = dtTrackingData.Rows(0)
        Catch ex As System.Exception
            Exit Sub
        End Try
        If (File.Exists(filename)) Then
            xmlDoc.Load(filename)
            strSSN = getSSN(StudentId)
            xmlDoc = AddCommentTracking(xmlDoc, dr, strSSN, StudentId)
            xmlDoc.Save(filename)
        Else
            'If file not found then create file with student data and then add address data 
            strSSN = CreateStudentDataForUpdates(StudentId, "")
            If File.Exists(filename) Then
                xmlDoc.Load(filename)
                xmlDoc = AddCommentTracking(xmlDoc, dr, strSSN, StudentId)
                xmlDoc.Save(filename)
            End If
        End If
    End Sub
    Private Function AddCommentTracking(ByVal xmlDoc As XmlDocument, _
                                        ByVal dr As DataRow, _
                                        ByVal strSSN As String, _
                                        ByVal Studentid As String) As XmlDocument
        'Get a reference to the root node
        'Dim elmRoot As XmlElement = xmlDoc.DocumentElement
        ''Create a list of the student
        'Dim lstStudents As XmlNodeList = xmlDoc.GetElementsByTagName("student")
        'Dim intStudentTagCount As Integer = 0
        'Dim boolStudentFound As Boolean = False
        'Do While intStudentTagCount < lstStudents.Count
        '    Dim curAttributes As XmlAttributeCollection = lstStudents(intStudentTagCount).Attributes
        '    Dim intAttrCount As Integer = 0
        '    Do While intAttrCount < curAttributes.Count
        '        Dim strSession As String = ""
        '        Try
        '            strSession = curAttributes("sessionApplied").InnerText.ToString()
        '        Catch ex As System.Exception
        '            strSession = "NA"
        '        End Try
        '        If Not curAttributes("id").InnerText Is Nothing AndAlso curAttributes("id").InnerText.ToString.Trim = strSSN.ToString.Trim Then
        '            boolStudentFound = True
        '            Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "tracking", "http://www.regenteducation.com/carbon/batchload")
        '            With elmNew
        '                Dim objCommentType As New commentType
        '                .SetAttribute("session", strSession)
        '                .SetAttribute("blockNumber", dr("BlockNumber"))
        '                .SetAttribute("comment", dr("Comments"))
        '                .SetAttribute("type", dr("CommentType"))
        '            End With
        '            lstStudents(intStudentTagCount).AppendChild(elmNew)
        '            Exit Do
        '        End If
        '        intAttrCount += 1
        '    Loop
        '    intStudentTagCount += 1
        'Loop
        'If boolStudentFound = False Then
        '    Dim elmRoot1 As XmlElement = xmlDoc.DocumentElement
        '    Dim dtgetStudentDetails As New DataTable
        '    dtgetStudentDetails = getStudentDetails(Studentid)
        '    xmlDoc = AppendStudentAttributeToExistingStudents(xmlDoc, elmRoot1, dtgetStudentDetails)
        'End If
        Dim elmRoot As XmlElement = xmlDoc.DocumentElement
        Dim boolStudentFound As Boolean = False
        Dim boolActivityTagExists As Boolean = False
        Dim strSession As String = ""
        'Create a list of the student
        Dim lstStudents As XmlNodeList = xmlDoc.GetElementsByTagName("student")
        ' visit each Student
        For Each node As XmlNode In lstStudents
            Dim lstChildren As XmlNodeList = node.ChildNodes
            ' Visit each child node 
            For Each dir As XmlNode In lstChildren

                If dir.LocalName.ToString.Trim = "comment" And Not node.Attributes("id").Value.ToString.Trim = "" And node.Attributes("id").Value.ToString.Trim = strSSN.ToString Then
                    Try
                        strSession = node.Attributes("sessionApplied").Value.ToString.Trim
                    Catch ex As System.Exception
                        strSession = ""
                    End Try
                    boolStudentFound = True
                    boolActivityTagExists = True
                    'Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "comment", "http://www.regenteducation.com/carbon/batchload")
                    'With elmNew
                    '    Dim objCommentType As New commentType
                    '    .SetAttribute("session", strSession)
                    '    .SetAttribute("blockNumber", dr("BlockNumber"))
                    '    .SetAttribute("comment", dr("Comments"))
                    '    .SetAttribute("type", dr("CommentType"))
                    'End With
                    'node.InsertAfter(elmNew, dir)
                    With dir
                        .Attributes("session").Value = strSession
                        .Attributes("blockNumber").Value = dr("BlockNumber")
                        If Not dr("Comments") Is System.DBNull.Value Then .Attributes("comment").Value = dr("Comments") Else .Attributes("comment").Value = ""
                        .Attributes("type").Value = dr("CommentType")
                    End With
                    Exit For
                End If
            Next
        Next

        'If Award tag is not found then we need to add the award tag
        If boolActivityTagExists = False Then
            lstStudents = xmlDoc.GetElementsByTagName("student")
            Dim intStudentTagCount As Integer = 0
            Do While intStudentTagCount < lstStudents.Count
                Dim curAttributes As XmlAttributeCollection = lstStudents(intStudentTagCount).Attributes
                Dim intAttrCount As Integer = 0
                Do While intAttrCount < curAttributes.Count
                    If Not curAttributes("id").InnerText Is Nothing AndAlso curAttributes("id").InnerText.ToString.Trim = strSSN.ToString.Trim Then
                        Try
                            strSession = curAttributes("sessionApplied").InnerText.ToString.Trim
                        Catch ex As System.Exception
                            strSession = ""
                        End Try
                        boolStudentFound = True
                        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "comment", "http://www.regenteducation.com/carbon/batchload")
                        With elmNew
                            Dim objCommentType As New commentType
                            .SetAttribute("session", strSession)
                            .SetAttribute("blockNumber", dr("BlockNumber"))
                            .SetAttribute("comment", dr("Comments"))
                            .SetAttribute("type", dr("CommentType"))
                        End With
                        lstStudents(intStudentTagCount).AppendChild(elmNew)
                        boolActivityTagExists = True
                        Exit Do
                    End If
                    intAttrCount += 1
                Loop
                intStudentTagCount += 1
            Loop
        End If

        'If student data is not found in the XML file then 
        ' create the student tag and then add address tag to it.
        If boolStudentFound = False Then
            Dim elmRoot1 As XmlElement = xmlDoc.DocumentElement
            Dim dtgetStudentDetails As New DataTable
            dtgetStudentDetails = getStudentDetails(Studentid)
            xmlDoc = AppendStudentAttributeToExistingStudents(xmlDoc, elmRoot1, dtgetStudentDetails)
            xmlDoc = AppendNameAttribute(xmlDoc, elmRoot1, dtgetStudentDetails)
            'As Student is created now add the address
            lstStudents = xmlDoc.GetElementsByTagName("student")
            ' visit each Student
            For Each node As XmlNode In lstStudents
                Dim lstChildren As XmlNodeList = node.ChildNodes
                ' Visit each child node 
                For Each dir As XmlNode In lstChildren
                    If dir.LocalName.ToString.Trim = "name" And Not node.Attributes("id").Value.ToString.Trim = "" And node.Attributes("id").Value.ToString.Trim = strSSN.ToString Then
                        Try
                            strSession = node.Attributes("sessionApplied").Value.ToString.Trim
                        Catch ex As System.Exception
                            strSession = ""
                        End Try
                        boolStudentFound = True
                        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "comment", "http://www.regenteducation.com/carbon/batchload")
                        With elmNew
                            Dim objCommentType As New commentType
                            .SetAttribute("session", strSession)
                            .SetAttribute("blockNumber", dr("BlockNumber"))
                            .SetAttribute("comment", dr("Comments"))
                            .SetAttribute("type", dr("CommentType"))
                        End With
                        node.InsertAfter(elmNew, dir)
                        Exit For
                    End If
                Next
            Next
        End If
        Return xmlDoc
    End Function
    Public Sub AddDemographicXML(ByVal StudentId As String, _
                                 ByVal filename As String, _
                                 ByVal StudentTrackId As String)

        Dim xmlDoc As XmlDocument = New XmlDocument()
        Dim dtTrackingData As DataTable
        Dim dr As DataRow
        Dim strSSN As String = ""
        dtTrackingData = getStudentDetails(StudentId)
        Try
            dr = dtTrackingData.Rows(0)
        Catch ex As System.Exception
            Exit Sub
        End Try
        If (File.Exists(filename)) Then
            xmlDoc.Load(filename)
            strSSN = getSSN(StudentId)
            xmlDoc = AddDemographicForExistingStudent(xmlDoc, dr, strSSN, StudentId)
            xmlDoc.Save(filename)
        Else
            'If file not found then create file with student data and then add address data 
            strSSN = CreateStudentDataForUpdates(StudentId, "")
            If File.Exists(filename) Then
                xmlDoc.Load(filename)
                xmlDoc = AddDemographicForExistingStudent(xmlDoc, dr, strSSN, StudentId)
                xmlDoc.Save(filename)
            End If
        End If
    End Sub
    Private Function AddDemographicForExistingStudent(ByVal xmlDoc As XmlDocument, ByVal dr As DataRow, ByVal strSSN As String, ByVal StudentId As String) As XmlDocument
        Dim elmRoot As XmlElement = xmlDoc.DocumentElement
        Dim boolStudentFound As Boolean = False
        Dim boolActivityTagExists As Boolean = False
        Dim strSession As String = ""
        'Create a list of the student
        Dim lstStudents As XmlNodeList = xmlDoc.GetElementsByTagName("student")
        ' visit each Student
        For Each node As XmlNode In lstStudents
            Dim lstChildren As XmlNodeList = node.ChildNodes
            ' Visit each child node 
            For Each dir As XmlNode In lstChildren
                If dir.LocalName.ToString.Trim = "demographic" And Not node.Attributes("id").Value.ToString.Trim = "" And node.Attributes("id").Value.ToString.Trim = strSSN.ToString Then
                    boolStudentFound = True
                    boolActivityTagExists = True
                    '.alternateId = ""
                    'If Not dr("DOB") Is System.DBNull.Value Then .dateOfBirth = CDate(dr("DOB")).ToString("yyyy-MM-dd") Else .dateOfBirth = ""
                    'If Not dr("Sex") Is System.DBNull.Value Then .sex = dr("sex") Else .sex = ""
                    'If Not dr("ssn") Is System.DBNull.Value Then .ssn = dr("ssn") Else .ssn = ""
                    'If Not dr("maritalstatus") Is System.DBNull.Value Then .maritalStatus = dr("maritalstatus") Else .maritalStatus = ""
                    'If Not dr("citizen") Is System.DBNull.Value Then .citizenship = dr("citizen") Else .citizenship = ""
                    'If Not dr("race") Is System.DBNull.Value Then .ethnicity = dr("race") Else .ethnicity = ""
                    '.handicap = "NA"
                    '.deceasedDate = ""
                    '.pin = ""
                    '.status = "AC"
                    With dir
                        'If Not dr("SessionApplied") Is System.DBNull.Value Then .Attributes("session").Value = Mid(dr("SessionApplied"), 1, 4) Else .Attributes("session").Value = ""
                        'If Not dr("code") Is System.DBNull.Value Then .Attributes("code").Value = Mid(dr("code"), 1, 2) Else .Attributes("code").Value = ""
                        'If Not dr("status") Is System.DBNull.Value Then .Attributes("status").Value = Mid(dr("status"), 1, 1) Else .Attributes("status").Value = ""
                        'If Not dr("amount") Is System.DBNull.Value Then .Attributes("amount").Value = Mid(dr("amount"), 1, 5) Else .Attributes("amount").Value = ""
                        'If Not dr("subCode") Is System.DBNull.Value Then .Attributes("subCode").Value = Mid(dr("subCode"), 1, 6) Else .Attributes("subCode").Value = ""
                        '.Attributes("miscellaneousCode1").Value = ""
                        '.Attributes("miscellaneousCode2").Value = ""
                        'If Not dr("ActivityDate") Is System.DBNull.Value Then .Attributes("ActivityDate").Value = CDate(dr("ActivityDate")).ToString("yyyy-MM-dd") Else .Attributes("ActivityDate").Value = ""
                        .Attributes("alternateId").Value = ""
                        ' If Not dr("DOB") Is System.DBNull.Value Then .Attributes("dateOfBirth").Value = CDate(dr("DOB")).ToString("yyyy-MM-dd") Else .Attributes("dateOfBirth").Value = ""
                        If Not dr("Sex") Is System.DBNull.Value Then .Attributes("sex").Value = dr("sex") Else .Attributes("sex").Value = ""
                        If Not dr("ssn") Is System.DBNull.Value Then .Attributes("ssn").Value = dr("ssn") Else .Attributes("ssn").Value = ""
                        If Not dr("maritalstatus") Is System.DBNull.Value Then .Attributes("maritalStatus").Value = dr("maritalstatus") Else .Attributes("maritalStatus").Value = ""
                        If Not dr("citizen") Is System.DBNull.Value Then .Attributes("citizenship").Value = dr("citizen") Else .Attributes("citizenship").Value = ""
                        If Not dr("race") Is System.DBNull.Value Then .Attributes("ethnicity").Value = dr("race") Else .Attributes("ethnicity").Value = ""
                        .Attributes("handicap").Value = ""
                        '.Attributes("deceasedDate").Value = ""
                        .Attributes("pin").Value = ""
                        .Attributes("status").Value = "AC"
                        .Attributes("miscellaneous1").Value = ""
                        .Attributes("miscellaneous2").Value = ""
                        .Attributes("miscellaneous3").Value = ""
                        .Attributes("miscellaneous4").Value = ""
                        .Attributes("miscellaneous5").Value = ""
                        .Attributes("miscellaneous6").Value = ""
                        .Attributes("miscellaneous7").Value = ""
                        .Attributes("miscellaneous8").Value = ""
                        .Attributes("miscellaneous9").Value = ""
                        .Attributes("miscellaneous10").Value = ""
                        .Attributes("miscellaneous11").Value = ""
                        .Attributes("miscellaneous12").Value = ""
                        .Attributes("miscellaneous13").Value = ""
                        .Attributes("miscellaneous14").Value = ""
                        .Attributes("miscellaneous15").Value = ""
                        .Attributes("miscellaneous16").Value = ""
                        .Attributes("miscellaneous17").Value = ""
                        .Attributes("miscellaneous18").Value = ""
                        .Attributes("miscellaneous19").Value = ""
                        .Attributes("miscellaneous20").Value = ""
                        .Attributes("miscellaneous21").Value = ""
                        .Attributes("miscellaneous22").Value = ""
                        .Attributes("miscellaneous23").Value = ""
                        .Attributes("miscellaneous24").Value = ""
                        .Attributes("miscellaneous25").Value = ""
                        .Attributes("miscellaneous26").Value = ""
                        .Attributes("miscellaneous27").Value = ""
                        .Attributes("miscellaneous28").Value = ""
                        .Attributes("miscellaneous29").Value = ""
                        .Attributes("miscellaneous30").Value = ""
                    End With
                    Exit For
                End If
            Next
        Next

        'If demographic tag is not found then we need to add the award tag
        If boolActivityTagExists = False Then
            lstStudents = xmlDoc.GetElementsByTagName("student")
            Dim intStudentTagCount As Integer = 0
            Do While intStudentTagCount < lstStudents.Count
                Dim curAttributes As XmlAttributeCollection = lstStudents(intStudentTagCount).Attributes
                Dim intAttrCount As Integer = 0
                Do While intAttrCount < curAttributes.Count
                    If Not curAttributes("id").InnerText Is Nothing AndAlso curAttributes("id").InnerText.ToString.Trim = strSSN.ToString.Trim Then
                        boolStudentFound = True
                        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "demographic", "http://www.regenteducation.com/carbon/batchload")
                        With elmNew
                            .SetAttribute("alternateid", "")
                            If Not dr("DOB") Is System.DBNull.Value Then .SetAttribute("dateOfBirth", CDate(dr("DOB")).ToString("yyyy-MM-dd")) Else .SetAttribute("dateOfBirth", "")
                            If Not dr("Sex") Is System.DBNull.Value Then .SetAttribute("sex", dr("sex")) Else .SetAttribute("sex", "")
                            If Not dr("ssn") Is System.DBNull.Value Then .SetAttribute("ssn", dr("ssn")) Else .SetAttribute("ssn", "")
                            If Not dr("maritalstatus") Is System.DBNull.Value Then .SetAttribute("maritalStatus", dr("maritalstatus")) Else .SetAttribute("maritalStatus", "")
                            If Not dr("citizen") Is System.DBNull.Value Then .SetAttribute("citizenship", dr("citizen")) Else .SetAttribute("citizenship", "")
                            If Not dr("race") Is System.DBNull.Value Then .SetAttribute("ethnicity", dr("race")) Else .SetAttribute("ethnicity", "")
                            .SetAttribute("handicap", "NA")
                            '.SetAttribute("deceasedDate", "")
                            .SetAttribute("pin", "")
                            .SetAttribute("status", "AC")
                            .SetAttribute("miscellaneous1", "")
                            .SetAttribute("miscellaneous2", "")
                            .SetAttribute("miscellaneous3", "")
                            .SetAttribute("miscellaneous4", "")
                            .SetAttribute("miscellaneous5", "")
                            .SetAttribute("miscellaneous6", "")
                            .SetAttribute("miscellaneous7", "")
                            .SetAttribute("miscellaneous8", "")
                            .SetAttribute("miscellaneous9", "")
                            .SetAttribute("miscellaneous10", "")
                            .SetAttribute("miscellaneous11", "")
                            .SetAttribute("miscellaneous12", "")
                            .SetAttribute("miscellaneous13", "")
                            .SetAttribute("miscellaneous14", "")
                            .SetAttribute("miscellaneous15", "")
                            .SetAttribute("miscellaneous16", "")
                            .SetAttribute("miscellaneous17", "")
                            .SetAttribute("miscellaneous18", "")
                            .SetAttribute("miscellaneous19", "")
                            .SetAttribute("miscellaneous20", "")
                            .SetAttribute("miscellaneous21", "")
                            .SetAttribute("miscellaneous22", "")
                            .SetAttribute("miscellaneous23", "")
                            .SetAttribute("miscellaneous24", "")
                            .SetAttribute("miscellaneous25", "")
                            .SetAttribute("miscellaneous26", "")
                            .SetAttribute("miscellaneous27", "")
                            .SetAttribute("miscellaneous28", "")
                            .SetAttribute("miscellaneous29", "")
                            .SetAttribute("miscellaneous30", "")
                        End With
                        lstStudents(intStudentTagCount).AppendChild(elmNew)
                        boolActivityTagExists = True
                        Exit Do
                    End If
                    intAttrCount += 1
                Loop
                intStudentTagCount += 1
            Loop
        End If

        'If student data is not found in the XML file then 
        ' create the student tag and then add address tag to it.
        If boolStudentFound = False Then
            Dim elmRoot1 As XmlElement = xmlDoc.DocumentElement
            Dim dtgetStudentDetails As New DataTable
            dtgetStudentDetails = getStudentDetails(StudentId)
            xmlDoc = AppendStudentAttributeToExistingStudents(xmlDoc, elmRoot1, dtgetStudentDetails)
            xmlDoc = AppendNameAttribute(xmlDoc, elmRoot1, dtgetStudentDetails)
            'As Student is created now add the address
            lstStudents = xmlDoc.GetElementsByTagName("student")
            ' visit each Student
            For Each node As XmlNode In lstStudents
                Dim lstChildren As XmlNodeList = node.ChildNodes
                ' Visit each child node 
                For Each dir As XmlNode In lstChildren
                    If dir.LocalName.ToString.Trim = "name" And Not node.Attributes("id").Value.ToString.Trim = "" And node.Attributes("id").Value.ToString.Trim = strSSN.ToString Then
                        Try
                            strSession = node.Attributes("sessionApplied").Value.ToString.Trim
                        Catch ex As System.Exception
                            strSession = ""
                        End Try
                        boolStudentFound = True
                        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "demographic", "http://www.regenteducation.com/carbon/batchload")
                        With elmNew
                            .SetAttribute("alternateid", "")
                            If Not dr("DOB") Is System.DBNull.Value Then .SetAttribute("dateOfBirth", CDate(dr("DOB")).ToString("yyyy-MM-dd")) Else .SetAttribute("dateOfBirth", "")
                            If Not dr("Sex") Is System.DBNull.Value Then .SetAttribute("sex", dr("sex")) Else .SetAttribute("sex", "")
                            If Not dr("ssn") Is System.DBNull.Value Then .SetAttribute("ssn", dr("ssn")) Else .SetAttribute("ssn", "")
                            If Not dr("maritalstatus") Is System.DBNull.Value Then .SetAttribute("maritalStatus", dr("maritalstatus")) Else .SetAttribute("maritalStatus", "")
                            If Not dr("citizen") Is System.DBNull.Value Then .SetAttribute("citizenship", dr("citizen")) Else .SetAttribute("citizenship", "")
                            If Not dr("race") Is System.DBNull.Value Then .SetAttribute("ethnicity", dr("race")) Else .SetAttribute("ethnicity", "")
                            .SetAttribute("handicap", "NA")
                            '.SetAttribute("deceasedDate", "")
                            .SetAttribute("pin", "")
                            .SetAttribute("status", "AC")
                            .SetAttribute("miscellaneous1", "")
                            .SetAttribute("miscellaneous2", "")
                            .SetAttribute("miscellaneous3", "")
                            .SetAttribute("miscellaneous4", "")
                            .SetAttribute("miscellaneous5", "")
                            .SetAttribute("miscellaneous6", "")
                            .SetAttribute("miscellaneous7", "")
                            .SetAttribute("miscellaneous8", "")
                            .SetAttribute("miscellaneous9", "")
                            .SetAttribute("miscellaneous10", "")
                            .SetAttribute("miscellaneous11", "")
                            .SetAttribute("miscellaneous12", "")
                            .SetAttribute("miscellaneous13", "")
                            .SetAttribute("miscellaneous14", "")
                            .SetAttribute("miscellaneous15", "")
                            .SetAttribute("miscellaneous16", "")
                            .SetAttribute("miscellaneous17", "")
                            .SetAttribute("miscellaneous18", "")
                            .SetAttribute("miscellaneous19", "")
                            .SetAttribute("miscellaneous20", "")
                            .SetAttribute("miscellaneous21", "")
                            .SetAttribute("miscellaneous22", "")
                            .SetAttribute("miscellaneous23", "")
                            .SetAttribute("miscellaneous24", "")
                            .SetAttribute("miscellaneous25", "")
                            .SetAttribute("miscellaneous26", "")
                            .SetAttribute("miscellaneous27", "")
                            .SetAttribute("miscellaneous28", "")
                            .SetAttribute("miscellaneous29", "")
                            .SetAttribute("miscellaneous30", "")
                        End With
                        node.InsertAfter(elmNew, dir)
                        Exit For
                    End If
                Next
            Next
        End If
        Return xmlDoc
    End Function
    Public Sub AddActivityXML(ByVal StudentId As String, _
                              ByVal filename As String, _
                              ByVal StudentTrackId As String)

        Dim xmlDoc As XmlDocument = New XmlDocument()
        Dim dtTrackingData As DataTable
        Dim dr As DataRow
        Dim strSSN As String = ""
        dtTrackingData = getStudentActivityData(StudentTrackId, "append")

        Try
            dr = dtTrackingData.Rows(0)
        Catch ex As System.Exception
            Exit Sub
        End Try
        If (File.Exists(filename)) Then
            xmlDoc.Load(filename)
            strSSN = getSSN(StudentId)
            xmlDoc = AddActivityForExistingStudent(xmlDoc, dr, strSSN, StudentId)
            xmlDoc.Save(filename)
        Else
            'If file not found then create file with student data and then add address data 
            strSSN = CreateStudentDataForUpdates(StudentId, "")
            If File.Exists(filename) Then
                xmlDoc.Load(filename)
                xmlDoc = AddActivityForExistingStudent(xmlDoc, dr, strSSN, StudentId)
                xmlDoc.Save(filename)
            End If
        End If
    End Sub
    Private Function AddActivityForExistingStudent(ByVal xmlDoc As XmlDocument, _
                                                   ByVal dr As DataRow, _
                                                   ByVal strSSN As String, _
                                                   ByVal Studentid As String) As XmlDocument
        ''Get a reference to the root node
        'Dim elmRoot As XmlElement = xmlDoc.DocumentElement
        ''Create a list of the student
        'Dim lstStudents As XmlNodeList = xmlDoc.GetElementsByTagName("student")
        'Dim intStudentTagCount As Integer = 0
        'Dim boolStudentFound As Boolean = False
        'Do While intStudentTagCount < lstStudents.Count
        '    Dim curAttributes As XmlAttributeCollection = lstStudents(intStudentTagCount).Attributes
        '    Dim intAttrCount As Integer = 0
        '    Dim strSession As String = ""
        '    Try
        '        strSession = curAttributes("sessionApplied").InnerText.ToString()
        '    Catch ex As System.Exception
        '        strSession = "NA"
        '    End Try
        '    Do While intAttrCount < curAttributes.Count
        '        If Not curAttributes("id").InnerText Is Nothing AndAlso curAttributes("id").InnerText.ToString.Trim = strSSN.ToString.Trim Then
        '            boolStudentFound = True
        '            Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "Activity", "http://www.regenteducation.com/carbon/batchload")
        '            With elmNew
        '                .SetAttribute("session", strSession)
        '                If Not dr("Code") Is System.DBNull.Value Then
        '                    .SetAttribute("code", dr("Code"))
        '                Else
        '                    .SetAttribute("code", "")
        '                End If
        '                If Not dr("ActivityDate") Is System.DBNull.Value Then
        '                    .SetAttribute("date", CDate(dr("ActivityDate")).ToString("yyyy-MM-dd"))
        '                Else
        '                    .SetAttribute("Date", "")
        '                End If
        '                If Not dr("miscellaneousCode1") Is System.DBNull.Value Then .SetAttribute("miscellaneousCode1", dr("miscellaneousCode1")) Else .SetAttribute("miscellaneousCode1", "")
        '                .SetAttribute("miscellaneousCode2", "")
        '                .SetAttribute("miscellaneousCode3", "")
        '            End With
        '            lstStudents(intStudentTagCount).AppendChild(elmNew)
        '            Exit Do
        '        End If
        '        intAttrCount += 1
        '    Loop
        '    intStudentTagCount += 1
        'Loop
        'If boolStudentFound = False Then
        '    Dim elmRoot1 As XmlElement = xmlDoc.DocumentElement
        '    Dim dtgetStudentDetails As New DataTable
        '    dtgetStudentDetails = getStudentDetails(Studentid)
        '    xmlDoc = AppendStudentAttributeToExistingStudents(xmlDoc, elmRoot1, dtgetStudentDetails)
        'End If
        Dim elmRoot As XmlElement = xmlDoc.DocumentElement
        Dim boolStudentFound As Boolean = False
        Dim boolActivityTagExists As Boolean = False
        Dim strSession As String = ""
        'Create a list of the student
        Dim lstStudents As XmlNodeList = xmlDoc.GetElementsByTagName("student")
        ' visit each Student
        For Each node As XmlNode In lstStudents
            Dim lstChildren As XmlNodeList = node.ChildNodes
            ' Visit each child node 
            For Each dir As XmlNode In lstChildren

                If dir.LocalName.ToString.Trim = "activity" And Not node.Attributes("id").Value.ToString.Trim = "" And node.Attributes("id").Value.ToString.Trim = strSSN.ToString Then
                    Try
                        strSession = node.Attributes("sessionApplied").Value.ToString.Trim
                    Catch ex As System.Exception
                        strSession = ""
                    End Try
                    boolStudentFound = True
                    boolActivityTagExists = True
                    Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "Activity", "http://www.regenteducation.com/carbon/batchload")
                    With elmNew
                        .SetAttribute("session", strSession)
                        If Not dr("Code") Is System.DBNull.Value Then
                            .SetAttribute("code", dr("Code"))
                        Else
                            .SetAttribute("code", "")
                        End If
                        If Not dr("ActivityDate") Is System.DBNull.Value Then
                            .SetAttribute("date", CDate(dr("ActivityDate")).ToString("yyyy-MM-dd"))
                        Else
                            .SetAttribute("date", "")
                        End If
                        If Not dr("miscellaneousCode1") Is System.DBNull.Value Then .SetAttribute("miscellaneousCode1", dr("miscellaneousCode1")) Else .SetAttribute("miscellaneousCode1", "")
                        .SetAttribute("miscellaneousCode2", "")
                        .SetAttribute("miscellaneousCode3", "")
                    End With
                    node.InsertAfter(elmNew, dir)
                    Exit For
                End If
            Next
        Next

        'If Award tag is not found then we need to add the award tag
        If boolActivityTagExists = False Then
            lstStudents = xmlDoc.GetElementsByTagName("student")
            Dim intStudentTagCount As Integer = 0
            Do While intStudentTagCount < lstStudents.Count
                Dim curAttributes As XmlAttributeCollection = lstStudents(intStudentTagCount).Attributes
                Dim intAttrCount As Integer = 0
                Do While intAttrCount < curAttributes.Count
                    If Not curAttributes("id").InnerText Is Nothing AndAlso curAttributes("id").InnerText.ToString.Trim = strSSN.ToString.Trim Then
                        Try
                            strSession = curAttributes("sessionApplied").InnerText.ToString.Trim
                        Catch ex As System.Exception
                            strSession = ""
                        End Try
                        boolStudentFound = True
                        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "Activity", "http://www.regenteducation.com/carbon/batchload")
                        With elmNew
                            .SetAttribute("session", strSession)
                            If Not dr("Code") Is System.DBNull.Value Then
                                .SetAttribute("code", dr("Code"))
                            Else
                                .SetAttribute("code", "")
                            End If
                            If Not dr("ActivityDate") Is System.DBNull.Value Then
                                .SetAttribute("date", CDate(dr("ActivityDate")).ToString("yyyy-MM-dd"))
                            Else
                                .SetAttribute("date", "")
                            End If
                            If Not dr("miscellaneousCode1") Is System.DBNull.Value Then .SetAttribute("miscellaneousCode1", dr("miscellaneousCode1")) Else .SetAttribute("miscellaneousCode1", "")
                            .SetAttribute("miscellaneousCode2", "")
                            .SetAttribute("miscellaneousCode3", "")
                        End With
                        lstStudents(intStudentTagCount).AppendChild(elmNew)
                        boolActivityTagExists = True
                        Exit Do
                    End If
                    intAttrCount += 1
                Loop
                intStudentTagCount += 1
            Loop
        End If

        'If student data is not found in the XML file then 
        ' create the student tag and then add address tag to it.
        If boolStudentFound = False Then
            Dim elmRoot1 As XmlElement = xmlDoc.DocumentElement
            Dim dtgetStudentDetails As New DataTable
            dtgetStudentDetails = getStudentDetails(Studentid)
            xmlDoc = AppendStudentAttributeToExistingStudents(xmlDoc, elmRoot1, dtgetStudentDetails)
            xmlDoc = AppendNameAttribute(xmlDoc, elmRoot1, dtgetStudentDetails)
            'As Student is created now add the address
            lstStudents = xmlDoc.GetElementsByTagName("student")
            ' visit each Student
            For Each node As XmlNode In lstStudents
                Dim lstChildren As XmlNodeList = node.ChildNodes
                ' Visit each child node 
                For Each dir As XmlNode In lstChildren
                    If dir.LocalName.ToString.Trim = "name" And Not node.Attributes("id").Value.ToString.Trim = "" And node.Attributes("id").Value.ToString.Trim = strSSN.ToString Then
                        Try
                            strSession = node.Attributes("sessionApplied").Value.ToString.Trim
                        Catch ex As System.Exception
                            strSession = ""
                        End Try
                        boolStudentFound = True
                        Dim elmNew As XmlElement = xmlDoc.CreateNode(XmlNodeType.Element, "Activity", "http://www.regenteducation.com/carbon/batchload")
                        With elmNew
                            .SetAttribute("session", strSession)
                            If Not dr("Code") Is System.DBNull.Value Then
                                .SetAttribute("code", dr("Code"))
                            Else
                                .SetAttribute("code", "")
                            End If
                            If Not dr("ActivityDate") Is System.DBNull.Value Then
                                .SetAttribute("date", CDate(dr("ActivityDate")).ToString("yyyy-MM-dd"))
                            Else
                                .SetAttribute("date", "")
                            End If
                            If Not dr("miscellaneousCode1") Is System.DBNull.Value Then .SetAttribute("miscellaneousCode1", dr("miscellaneousCode1")) Else .SetAttribute("miscellaneousCode1", "")
                            .SetAttribute("miscellaneousCode2", "")
                            .SetAttribute("miscellaneousCode3", "")
                        End With
                        node.InsertAfter(elmNew, dir)
                        Exit For
                    End If
                Next
            Next
        End If
        Return xmlDoc
    End Function
    'Public Sub AddPhoneXML(ByVal StudentId As String, _
    '                          ByVal filename As String, _
    '                          ByVal StudentPhoneId As String)
    '    Dim xmlDoc As XmlDocument = New XmlDocument()
    '    Dim dtStudentAddress As DataTable
    '    Dim dr As DataRow
    '    dtStudentAddress = getStudentPhone(StudentPhoneId, "Append")
    '    dr = dtStudentAddress.Rows(0)
    '    If (File.Exists(filename)) Then
    '        xmlDoc.Load(filename)
    '        'Get a reference to the root node
    '        Dim elmRoot As XmlElement = xmlDoc.DocumentElement
    '        'Create a list of the student
    '        Dim lstStudents As XmlNodeList = xmlDoc.GetElementsByTagName("student")
    '        Dim intStudentTagCount As Integer = 0
    '        Do While intStudentTagCount < lstStudents.Count
    '            Dim curAttributes As XmlAttributeCollection = lstStudents(intStudentTagCount).Attributes
    '            Dim intAttrCount As Integer = 0
    '            Do While intAttrCount < curAttributes.Count
    '                Dim graddateattr As XmlAttribute = curAttributes("graduationdate")
    '                If curAttributes("ssn").InnerText = "123456789" Then
    '                    Dim lstAddress As XmlNodeList = lstStudents(intStudentTagCount).ChildNodes
    '                    Dim intAddressElementCount As Integer = 0
    '                    Do While intAddressElementCount < lstAddress.Count
    '                        Dim curaddAttributes As XmlAttributeCollection = lstAddress(intAddressElementCount).Attributes
    '                        Dim intAddrAttrCount As Integer = 0
    '                        Do While intAddrAttrCount < curaddAttributes.Count
    '                            Dim telAttribute As XmlAttribute = curaddAttributes("telephone2")

    '                        Loop
    '                    Loop
    '                End If
    '                intAttrCount += 1
    '            Loop
    '            intStudentTagCount += 1
    '        Loop
    '        xmlDoc.Save(filename)
    '    End If
    'End Sub
#End Region

#Region "Data Functions"
    Private Function getStudentDetails(ByVal StudentId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dsStudentDetails As New DataSet
        db.OpenConnection()

        With sb
            .Append(" Select Distinct " + vbCrLf)
            .Append(" firstname,middlename,lastname,Comments," + vbCrLf)
            .Append(" (select Top 1 MapId from syPrefixes where PrefixId=SD.Prefix) as Prefix," + vbCrLf)
            .Append(" (select Top 1 MapId from sySuffixes where SuffixId=SD.Suffix) as Suffix, " + vbCrLf)
            .Append(" workemail,homeemail,DOB,SSN," + vbCrLf)
            .Append(" (select Top 1 MapId from adGenders where GenderId=SD.Gender) as Sex, " + vbCrLf)
            .Append(" (select Top 1 MapId from adMaritalStatus where MaritalStatId=SD.MaritalStatus) as MaritalStatus, " + vbCrLf)
            .Append(" (select Top 1 MapId from adCitizenships where CitizenShipId=SD.Citizen) as Citizen, " + vbCrLf)
            .Append(" (select Top 1 MapId from adEthCodes where EthCodeId=SD.Race) as Race, " + vbCrLf)
            .Append(" Comments,CommentType,BlockNumber, " + vbCrLf)
            .Append(" DG.regentCode as DegreeCode,PV.MapId as CurriculumCode, " + vbCrLf)
            '.Append(" (select Top 1 regentTerm from arResults A,arClassSections B,arTerm C " + vbCrLf)
            '.Append(" where A.StuEnrollId=SE.StuEnrollId and A.TestId=B.ClsSectionId and B.TermId=C.TermId) as SessionApplied, " + vbCrLf)
            '.Append(" (select Top 1 regentStartTerm from arResults A,arClassSections B,arTerm C " + vbCrLf)
            '.Append(" where A.StuEnrollId=SE.StuEnrollId and A.TestId=B.ClsSectionId and B.TermId=C.TermId) as SessionStartTerm, " + vbCrLf)
            '.Append(" (select Top 1 regentEndTerm from arResults A,arClassSections B,arTerm C " + vbCrLf)
            '.Append(" where A.StuEnrollId=SE.StuEnrollId and A.TestId=B.ClsSectionId and B.TermId=C.TermId) as SessionEndTerm, " + vbCrLf)
            .Append("   (Case when " + vbCrLf)
            .Append("   	(select Top 1 regentTerm from arResults A,arClassSections B,arTerm C " + vbCrLf)
            .Append("   	where A.StuEnrollId=SE.StuEnrollId and A.TestId=B.ClsSectionId and B.TermId=C.TermId) Is NULL " + vbCrLf)
            .Append("   Then " + vbCrLf)
            .Append("   	(select Top 1 RegentTerm from arTerm  where StartDate >= SE.StartDate Order by StartDate asc) " + vbCrLf)
            .Append("   else " + vbCrLf)
            .Append("   	(select Top 1 regentTerm from arResults A,arClassSections B,arTerm C " + vbCrLf)
            .Append("   	where A.StuEnrollId=SE.StuEnrollId and A.TestId=B.ClsSectionId and B.TermId=C.TermId) " + vbCrLf)
            .Append("   END) as SessionApplied, " + vbCrLf)
            .Append("   (Case when " + vbCrLf)
            .Append("   	(select Top 1 regentStartTerm from arResults A,arClassSections B,arTerm C " + vbCrLf)
            .Append("   	where A.StuEnrollId=SE.StuEnrollId and A.TestId=B.ClsSectionId and B.TermId=C.TermId) Is NULL " + vbCrLf)
            .Append("   Then " + vbCrLf)
            .Append("   	(select Top 1 RegentStartTerm from arTerm  where StartDate >= SE.StartDate Order by StartDate asc) " + vbCrLf)
            .Append("   else " + vbCrLf)
            .Append("   	(select Top 1 RegentStartTerm from arResults A,arClassSections B,arTerm C " + vbCrLf)
            .Append("   	where A.StuEnrollId=SE.StuEnrollId and A.TestId=B.ClsSectionId and B.TermId=C.TermId) " + vbCrLf)
            .Append("   END) as SessionStartTerm,  " + vbCrLf)
            .Append("    (Case when " + vbCrLf)
            .Append("   	(select Top 1 regentEndTerm from arResults A,arClassSections B,arTerm C " + vbCrLf)
            .Append("   	where A.StuEnrollId=SE.StuEnrollId and A.TestId=B.ClsSectionId and B.TermId=C.TermId) Is NULL " + vbCrLf)
            .Append("   Then " + vbCrLf)
            .Append("   	(select Top 1 RegentEndTerm from arTerm  where StartDate >= SE.StartDate Order by StartDate asc) " + vbCrLf)
            .Append("   else " + vbCrLf)
            .Append("   	(select Top 1 RegentEndTerm from arResults A,arClassSections B,arTerm C " + vbCrLf)
            .Append("   	where A.StuEnrollId=SE.StuEnrollId and A.TestId=B.ClsSectionId and B.TermId=C.TermId) " + vbCrLf)
            .Append("   END) as SessionEndTerm, " + vbCrLf)
            .Append(" SE.ExpGradDate as GraduationDate,SE.StartDate as StartDate, " + vbCrLf)
            .Append(" (select Top 1 StateCode from arStudAddresses t1,syStates t2 where StudentId=SD.StudentId and t1.StateId=t2.StateId) as StateOfResidence, " + vbCrLf)
            .Append(" (select Top 1 AvailableDate from plExitInterview where EnrollmentId=SE.StuEnrollId) as ExitInterviewDate," + vbCrLf)
            .Append(" (select Top 1 t2.MapId from faStudentAwards t1,faLenders t2 " + vbCrLf)
            .Append(" where StuEnrollId=SE.StuEnrollId and t1.LenderId=t2.LenderId) as LenderId, " + vbCrLf)
            .Append(" EntranceInterviewDate, HighSchoolProgramCode " + vbCrLf)
            .Append(" from arStudent  SD,arStuEnrollments SE,arPrgVersions PV,arDegrees DG" + vbCrLf)
            .Append(" where SD.StudentId=SE.StudentId and SE.PrgVerID=PV.PrgVerID and PV.DEgreeId=DG.DegreeId and SD.StudentId=? " + vbCrLf)
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            dsStudentDetails = db.RunParamSQLDataSet(sb.ToString)
            Return dsStudentDetails.Tables(0)
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Private Function getStudentAddress(ByVal StudentId As String, Optional ByVal UseCase As String = "") As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dsStudentAddress As New DataSet
        db.OpenConnection()

        With sb
            .Append(" Select Distinct " + vbCrLf)
            .Append(" Address1,Address2,City,Zip," + vbCrLf)
            .Append(" (select Top 1 StateCode from syStates where StateId=SA.StateId) as StateDescrip," + vbCrLf)
            .Append(" (select Top 1 MapId from plAddressTypes where AddressTypeId=SA.AddressTypeId) as AddressDescrip, " + vbCrLf)
            .Append(" (select Top 1 MapId1 from plAddressTypes where AddressTypeId=SA.AddressTypeId) as AddressCode, " + vbCrLf)
            .Append(" (select Top 1 MapId from adCountries where CountryId=SA.CountryId) as CountryCode, " + vbCrLf)
            .Append(" (select Top 1 MapId from adCounties t1,arStudent t2 where t1.CountyId=t2.County and t2.StudentId=SA.StudentId) as CountyCode, " + vbCrLf)
            .Append(" (select Distinct Top 1 Phone from arStudentPhone where StudentId=SA.StudentId and default1=1) as telephone1, " + vbCrLf)
            .Append(" (select Distinct Top 1 Phone from arStudentPhone t1,syPhoneType t2 ")
            .Append(" where t1.PhoneTypeId=t2.PhoneTypeId and t2.PhoneTypeCode not like  '%Fax%' and ")
            .Append(" t1.StudentId=SA.StudentId and default1=0) as telephone2, ")
            .Append(" (select Distinct Top 1 Phone from arStudentPhone t1,syPhoneType t2 ")
            .Append(" where t1.PhoneTypeId=t2.PhoneTypeId and t2.PhoneTypeCode like '%Fax%' and ")
            .Append(" t1.StudentId=SA.StudentId and default1=0) as fax ")
            .Append(" from arStudAddresses SA" + vbCrLf)
            If UseCase = "" Then
                .Append(" where StudentId=? " + vbCrLf)
            Else
                .Append(" where StdAddressId=? " + vbCrLf)
            End If
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        Try
            dsStudentAddress = db.RunParamSQLDataSet(sb.ToString)
            Return dsStudentAddress.Tables(0)
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Private Function getSSN(ByVal Studentid As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dsStudentAddress As New DataSet
        Dim strSSN As String = ""
        db.OpenConnection()
        With sb
            .Append(" Select Distinct SSN " + vbCrLf)
            .Append(" from arStudent" + vbCrLf)
            .Append(" where StudentId=? ")
        End With
        db.AddParameter("@StudentId", Studentid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strSSN = db.RunParamSQLScalar(sb.ToString)
            Return strSSN
        Catch ex As System.Exception
            Return ""
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Public Function getStudentId(ByVal StuEnrollmentId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dsStudentAddress As New DataSet
        Dim strSSN As String = ""
        db.OpenConnection()
        With sb
            .Append(" Select Distinct StudentId " + vbCrLf)
            .Append(" from arStuEnrollments" + vbCrLf)
            .Append(" where StuEnrollId=? ")
        End With
        db.AddParameter("@StuEnrollId", StuEnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strSSN = db.RunParamSQLScalar(sb.ToString)
            Return strSSN
        Catch ex As System.Exception
            Return ""
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Private Function getStudentAwardData(ByVal StudentId As String, Optional ByVal useCase As String = "") As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dsStudentAddress As New DataSet
        db.OpenConnection()
        With sb
            .Append(" Select Distinct " + vbCrLf)
            .Append(" t4.RegentTerm as Session, " + vbCrLf)
            .Append(" t1.AwardCode as Code, " + vbCrLf)
            .Append(" t1.AwardSubCode as SubCode, " + vbCrLf)
            .Append(" t1.AWARDStatus as Status,t1.GrossAmount as Amount,t1.ModDate as ActivityDate,t5.SSN as SSN	 " + vbCrLf)
            .Append("        from " + vbCrLf)
            .Append("  faStudentAwards t1,arStuEnrollments t2, arResults t3, " + vbCrLf)
            .Append("  arTerm t4,arStudent t5,	syStatuses t6,arClassSections t7,saFundSources t8 " + vbCrLf)
            .Append(" where " + vbCrLf)
            .Append(" t1.StuEnrollId=t2.StuEnrollId and t2.StuEnrollId=t3.StuEnrollId  and " + vbCrLf)
            .Append(" t3.TestId=t7.ClsSectionId and t4.TermId=t7.TermId and " + vbCrLf)
            .Append(" t2.StudentId=t5.StudentId and t5.StudentStatus=t6.StatusId and " + vbCrLf)
            .Append(" t1.AwardTypeId=t8.FundSourceId  " + vbCrLf)
            If useCase = "" Then
                .Append(" and t5.StudentId=? " + vbCrLf)
            Else
                .Append(" and t1.StudentAwardId=? " + vbCrLf)
            End If
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            dsStudentAddress = db.RunParamSQLDataSet(sb.ToString)
            Return dsStudentAddress.Tables(0)
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Private Function getStudentFinAidData(ByVal StudentId As String, Optional ByVal useCase As String = "") As DataTable
    End Function
    Private Function getStudentTrackingData(ByVal StudentId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dsStudentAddress As New DataSet
        db.OpenConnection()
        With sb
            .Append(" Select Distinct trackCode,DueDate,notifiedDate,completedDate,Notificationcode,Description, ")
            .Append(" (select Top 1 regentTerm from arResults A,arClassSections B,arTerm C,arStuEnrollments D " + vbCrLf)
            .Append(" where A.StuEnrollId=D.StuEnrollId and A.TestId=B.ClsSectionId and B.TermId=C.TermId and D.StudentId=t1.StudentId) as Session " + vbCrLf)
            .Append(" from plStudentDocs t1,adReqs t2 ")
            .Append(" where t1.DocumentId = t2.adReqId and t1.StudentDocId=? ")
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            dsStudentAddress = db.RunParamSQLDataSet(sb.ToString)
            Return dsStudentAddress.Tables(0)
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Private Function getStudentActivityData(ByVal StudentId As String, Optional ByVal useCase As String = "") As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dsStudentAddress As New DataSet
        db.OpenConnection()
        With sb
            .Append(" Select Distinct " + vbCrLf)
            .Append(" Substring(t2.ExtraCurrDescrip,1,4) as MiscellaneousCode1," + vbCrLf)
            .Append(" t2.ActivityCode as Code,t1.ModDate as ActivityDate  " + vbCrLf)
            .Append("        from " + vbCrLf)
            .Append("  plStudentExtracurriculars t1,adExtraCurr t2 " + vbCrLf)
            .Append(" where " + vbCrLf)
            .Append(" t1.ExtraCurricularId = t2.ExtraCurrId   " + vbCrLf)
            If useCase = "" Then
                .Append(" and t1.StudentId=? ")
            Else
                .Append(" and t1.StuExtracurricularId=? ")
            End If
            .Append(" Order by t1.ModDate desc ")
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            dsStudentAddress = db.RunParamSQLDataSet(sb.ToString)
            Return dsStudentAddress.Tables(0)
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Private Function getStudentAcademicData(ByVal StudentId As String, Optional ByVal useCase As String = "") As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dsStudentPhone As New DataSet
        db.OpenConnection()

        With sb
            .Append(" select Distinct " + vbCrLf)
            .Append("	t4.RegentTerm as Session, " + vbCrLf)
            .Append("	t9.MapId as DegreeCode, " + vbCrLf)
            .Append("	t8.MapId as CurriculumCode, " + vbCrLf)
            .Append("	t2.StuEnrollId, " + vbCrLf)
            .Append("  (select Top 1 MapId from syStatusCodes x1,arStuEnrollments x2 where ")
            .Append("  x1.StatusCodeId=x2.StatusCodeId and x2.StuEnrollId=t2.StuEnrollId) as AcadmicStatus," + vbCrLf)
            'if status is mapped to dropped get withdrawalcode
            .Append("	(select Distinct Top 1 MapId from arStuEnrollments A,sySysStatus B,syStatusCodes C " + vbCrLf)
            .Append("	where A.StuEnrollId=t2.StuEnrollId and A.StatusCodeId=C.StatusCodeId and " + vbCrLf)
            .Append("	B.SysStatusId=C.SysStatusId and B.SysStatusId=12) as WithdrawalCode, " + vbCrLf)
            .Append("	(select Distinct Top 1 A.DateDetermined from arStuEnrollments A,sySysStatus B,syStatusCodes C " + vbCrLf)
            .Append("	where A.StuEnrollId=t2.StuEnrollId and A.StatusCodeId=C.StatusCodeId and " + vbCrLf)
            .Append("	B.SysStatusId=C.SysStatusId and B.SysStatusId=12) as WithdrawalDate,t2.ModDate " + vbCrLf)
            .Append(" from " + vbCrLf)
            .Append("	arStuEnrollments t2,arResults t3,arTerm t4, " + vbCrLf)
            .Append("	arStudent t5,syStatuses t6,arClassSections t7, " + vbCrLf)
            .Append("	arPrgVersions t8,arDegrees t9 " + vbCrLf)
            .Append(" where  " + vbCrLf)
            .Append("	t2.StuEnrollId=t3.StuEnrollId  and t2.PrgVerId=t8.PrgVerId and " + vbCrLf)
            .Append("	t3.TestId=t7.ClsSectionId and t4.TermId=t7.TermId and  " + vbCrLf)
            .Append("	t2.StudentId=t5.StudentId and t5.StudentStatus=t6.StatusId and  " + vbCrLf)
            .Append("	t8.DegreeId=t9.DegreeId and " + vbCrLf)
            .Append("	t5.StudentId=? " + vbCrLf)
            .Append(" order by t2.ModDate desc ")
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            dsStudentPhone = db.RunParamSQLDataSet(sb.ToString)
            Return dsStudentPhone.Tables(0)
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Private Function getStudentPhone(ByVal StudentId As String, Optional ByVal useCase As String = "") As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dsStudentPhone As New DataSet
        db.OpenConnection()

        With sb
            .Append(" Select Distinct " + vbCrLf)
            .Append(" Phone," + vbCrLf)
            .Append(" (select Top 1 PhoneTypeDescrip from syPhoneType where PhoneTypeId=SA.PhoneTypeId) as PhoneTypeDescrip" + vbCrLf)
            .Append(" from arStudentPhone SA" + vbCrLf)
            If useCase = "" Then
                .Append(" where StudentId=? " + vbCrLf)
            Else
                .Append(" where StudentPhoneId=? " + vbCrLf)
            End If
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            dsStudentPhone = db.RunParamSQLDataSet(sb.ToString)
            Return dsStudentPhone.Tables(0)
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Public Function InsertAwardCode(ByVal AwardCode As String, ByVal AwardCodeId As String, ByVal moduser As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dsStudentAddress As New DataSet
        db.OpenConnection()

        With sb
            .Append("insert into regAwardCodes(regAwardCodeId,regAwardCode,moduser,moddate) values(?,?,?,?) ")
        End With
        db.AddParameter("@regAwardCodeId", AwardCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@regAwardCode", AwardCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@moduser", moduser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@moddate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        Try
            db.RunParamFLSQLExecuteNoneQuery(sb.ToString)
            Return ""
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Public Function getAwardCode() As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim dsStudentAddress As New DataSet
        db.OpenConnection()

        With sb
            .Append("Select * from regAwardCodes ")
        End With
        Try
            ds = db.RunParamSQLDataSet(sb.ToString)
            Return ds.Tables(0)
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Public Function getAwardSubCode() As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim dsStudentAddress As New DataSet
        db.OpenConnection()

        With sb
            .Append("Select * from regAwardSubCodes  ")
        End With
        Try
            ds = db.RunParamSQLDataSet(sb.ToString)
            Return ds.Tables(0)
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Public Function InsertAwardSubCode(ByVal AwardCode As String, ByVal AwardCodeId As String, ByVal awardsubcodeid As String, ByVal moduser As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dsStudentAddress As New DataSet
        db.OpenConnection()

        With sb
            .Append("insert into regAwardCodes(regAwardSubCodeId,regAwardSubCode,regAwardCodeId,moduser,moddate) values(?,?,?,?,?) ")
        End With
        db.AddParameter("@regAwardSubCodeId", awardsubcodeid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@regAwardSubCode", AwardCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@regAwardCodeId", AwardCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@moduser", moduser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@moddate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        Try
            db.RunParamFLSQLExecuteNoneQuery(sb.ToString)
            Return ""
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Public Function updateRegentTerm(ByVal TermId As String, ByVal regentTermId As String, ByVal moduser As String, ByVal StartTerm As String, ByVal EndTerm As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dsStudentAddress As New DataSet
        db.OpenConnection()

        With sb
            .Append("update arTerms set regentTermId=?,ModUser=?,ModDate=?,regentStartTerm=?,regEndTerm=? where TermId=? ")
        End With
        db.AddParameter("@regentTermId", regentTermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ModUser", moduser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        If Not StartTerm = "" Then
            db.AddParameter("@regentStartTerm", StartTerm, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@regentStartTerm", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not EndTerm = "" Then
            db.AddParameter("@regentEndTerm", EndTerm, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@regentEndTerm", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            db.RunParamFLSQLExecuteNoneQuery(sb.ToString)
            Return ""
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Public Function updateRegentLender(ByVal LenderId As String, ByVal regentLenderId As String, ByVal moduser As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dsStudentAddress As New DataSet
        db.OpenConnection()

        With sb
            .Append("update falenders set regentlenderId=?,ModUser=?,ModDate=? where LenderId=? ")
        End With
        db.AddParameter("@regentlenderId", regentLenderId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ModUser", moduser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@TermId", LenderId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            db.RunParamFLSQLExecuteNoneQuery(sb.ToString)
            Return ""
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Public Function updateRegentActivityCode(ByVal ExtracurrId As String, ByVal ActivityCode As String, ByVal moduser As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dsStudentAddress As New DataSet
        db.OpenConnection()

        With sb
            .Append("update adExtraCurr set ActivityCode=?,ModUser=?,ModDate=? where ExtraCurrId=? ")
        End With
        db.AddParameter("@ActivityCode", ActivityCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ModUser", moduser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@ExtracurrId", ExtracurrId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            db.RunParamFLSQLExecuteNoneQuery(sb.ToString)
            Return ""
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Public Function updateRegentCurriculumCode(ByVal PrgVerId As String, ByVal ActivityCode As String, ByVal moduser As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dsStudentAddress As New DataSet
        db.OpenConnection()

        With sb
            .Append("update arPrgVersions set MapId=?,ModUser=?,ModDate=? where prgVerId=? ")
        End With
        db.AddParameter("@MapId", ActivityCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ModUser", moduser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@prgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            db.RunParamFLSQLExecuteNoneQuery(sb.ToString)
            Return ""
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Public Function updateRegentDegreeCode(ByVal DegreeId As String, ByVal regentDegreeCode As String, ByVal moduser As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dsStudentAddress As New DataSet
        db.OpenConnection()

        With sb
            .Append("update arDegrees set MapId=?,ModUser=?,ModDate=? where DegreeId=? ")
        End With
        db.AddParameter("@regentCode", regentDegreeCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ModUser", moduser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@DegreeId", DegreeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            db.RunParamFLSQLExecuteNoneQuery(sb.ToString)
            Return ""
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Public Function updateMaintenanceData(ByVal tblName As String, ByVal PKID As String, _
                                          ByVal Value2 As String, _
                                          ByVal fldvalue1 As String, _
                                          ByVal fldvalue2 As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        db.OpenConnection()
        '"arDegrees", txtDegreeId.Text, ddlRegentCode.SelectedValue, "MapId", "DegreeId", ddlSelfPaced.SelectedValue
        With sb
            .Append("update " & tblName & " set " & fldvalue1 & "=?,ModDate=? where " & fldvalue2 & "=? ")
        End With
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@fldvalue2", PKID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            db.RunParamFLSQLExecuteNoneQuery(sb.ToString)
            Return ""
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function

    Public Function updateRegentWithdrawalCode(ByVal StatusCodeId As String, ByVal regentWithdrawalCode As String, ByVal moduser As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dsStudentAddress As New DataSet
        db.OpenConnection()

        With sb
            .Append("update syStatusCodes set regentWithdrawalCode=?,ModUser=?,ModDate=? where StatusCodeId=? ")
        End With
        db.AddParameter("@regentWithdrawalCode", regentWithdrawalCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ModUser", moduser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@StatusCodeId", StatusCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            db.RunParamFLSQLExecuteNoneQuery(sb.ToString)
            Return ""
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Public Function getregentTerm(ByVal TermId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strGetRegentTerm As String = ""
        Dim dsStudentAddress As New DataSet
        Dim dt As New DataTable
        db.OpenConnection()

        With sb
            .Append("Select Distinct RegentTerm,regentStartTerm,regentEndTerm from arTerm where TermId=? ")
        End With
        db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            dt = db.RunParamSQLDataSet(sb.ToString).Tables(0)
            Return dt
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Public Function getAllMaintenanceDataByPrimaryKey(ByVal tblname As String, ByVal fldValue As String, ByVal pKValue As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strGetRegentTerm As String = ""
        Dim dsStudentAddress As New DataSet
        Dim dt As New DataTable
        db.OpenConnection()
        With sb
            .Append("Select Distinct MapId from " & tblname & " where " & fldValue & "=? ")
        End With
        db.AddParameter("@lenderId", pKValue, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strGetRegentTerm = db.RunParamSQLScalar(sb.ToString)
            Return strGetRegentTerm
        Catch ex As System.Exception
            Return ""
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function

    
    Public Function getAllAddressCode(ByVal tblname As String, ByVal fldValue As String, ByVal pKValue As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strGetRegentTerm As String = ""
        Dim dsStudentAddress As New DataSet
        Dim dt As New DataTable
        db.OpenConnection()
        With sb
            .Append("Select Distinct MapId1 from " & tblname & " where " & fldValue & "=? ")
        End With
        db.AddParameter("@lenderId", pKValue, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strGetRegentTerm = db.RunParamSQLScalar(sb.ToString)
            Return strGetRegentTerm
        Catch ex As System.Exception
            Return ""
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Public Function getregentLender(ByVal TermId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strGetRegentTerm As String = ""
        Dim dsStudentAddress As New DataSet
        Dim dt As New DataTable
        db.OpenConnection()

        With sb
            .Append("Select Distinct MapId from faLenders where LenderId=? ")
        End With
        db.AddParameter("@lenderId", TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strGetRegentTerm = db.RunParamSQLScalar(sb.ToString)
            Return strGetRegentTerm
        Catch ex As System.Exception
            Return ""
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Public Function getregentCurriculum(ByVal PrgVerId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strGetRegentTerm As String = ""
        Dim dsStudentAddress As New DataSet
        Dim dt As New DataTable
        db.OpenConnection()

        With sb
            .Append("Select Distinct MapId from arPrgVersions where PrgVerId=? ")
        End With
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strGetRegentTerm = db.RunParamSQLScalar(sb.ToString)
            Return strGetRegentTerm
        Catch ex As System.Exception
            Return ""
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Public Function getregentActivityCode(ByVal ExtraCurrId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strGetRegentTerm As String = ""
        Dim dsStudentAddress As New DataSet
        Dim dt As New DataTable
        db.OpenConnection()

        With sb
            .Append("Select Distinct ActivityCode from adExtraCurr where ExtraCurrId=? ")
        End With
        db.AddParameter("@ExtraCurrId", ExtraCurrId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strGetRegentTerm = db.RunParamSQLScalar(sb.ToString)
            Return strGetRegentTerm
        Catch ex As System.Exception
            Return ""
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Public Function getregentWithdrawalCode(ByVal StatusCodeId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strGetRegentTerm As String = ""
        Dim dsStudentAddress As New DataSet
        Dim dt As New DataTable
        db.OpenConnection()

        With sb
            .Append("Select Distinct RegentWithDrawalCode from syStatusCodes where StatusCodeId=? ")
        End With
        db.AddParameter("@StatusCodeId", StatusCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strGetRegentTerm = db.RunParamSQLScalar(sb.ToString)
            Return strGetRegentTerm
        Catch ex As System.Exception
            Return ""
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Public Function getregentDegreeCode(ByVal DegreeId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim strGetRegentTerm As String = ""
        Dim dsStudentAddress As New DataSet
        db.OpenConnection()

        With sb
            .Append("Select Distinct MapId from arDegrees where DegreeId=? ")
        End With
        db.AddParameter("@DegreeId", DegreeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strGetRegentTerm = db.RunParamSQLScalar(sb.ToString)
            Return strGetRegentTerm
        Catch ex As System.Exception
            Return ""
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
#End Region


#Region "Populate Student Objects"
    Private Function PopulateStudentAddress(ByVal dtAddress As DataTable, _
                                            ByVal strPhone As String) As addressType
        Dim objStudentAddress As New addressType
        Dim dtStudentPhone As New DataTable
        Dim dr As DataRow

        dr = dtAddress.Rows(0)
        With objStudentAddress
            If Not dr("AddressDescrip") Is System.DBNull.Value Then .type = dr("AddressDescrip") Else .type = "HOME"
            If Not dr("AddressCode") Is System.DBNull.Value Then .code = dr("AddressCode") Else .code = "HOME"
            If Not dr("Address1") Is System.DBNull.Value Then .line1 = Mid(dr("Address1"), 1, 30) Else .line1 = ""
            If Not dr("Address2") Is System.DBNull.Value Then .line2 = Mid(dr("Address2"), 1, 30) Else .line2 = ""
            .line3 = ""
            .line4 = ""
            If Not dr("City") Is System.DBNull.Value Then .city = Mid(dr("City"), 1, 20) Else .city = ""
            If Not dr("StateDescrip") Is System.DBNull.Value Then .state = Mid(dr("StateDescrip"), 1, 2) Else .state = ""
            If Not dr("Zip") Is System.DBNull.Value Then .zip = dr("Zip") Else .zip = ""
            If Not dr("CountryCode") Is System.DBNull.Value Then .countryCode = Mid(dr("CountryCode"), 1, 4) Else .countyCode = ""
            If Not dr("CountyCode") Is System.DBNull.Value Then .countyCode = Mid(dr("CountyCode"), 1, 4) Else .countyCode = ""
            .status = "AC"
            If Not dr("telephone1") Is System.DBNull.Value Then .telephone1 = dr("telephone1") Else .telephone1 = ""
            If Not dr("telephone2") Is System.DBNull.Value Then .telephone2 = dr("telephone2") Else .telephone2 = ""
            If Not dr("fax") Is System.DBNull.Value Then .fax = dr("fax") Else .fax = ""
        End With
        Return objStudentAddress
    End Function
    Private Function PopulateStudentName(ByVal dtName As DataTable) As nameType
        Dim objStudentName As New nameType
        Dim dr As DataRow
        dr = dtName.Rows(0)
        With objStudentName
            .type = "STUD"
            .code = "MSTR"
            .firstName = Mid(dr("FirstName"), 1, 20)
            If Not dr("middlename") Is System.DBNull.Value Then .middleName = Mid(dr("middleName").ToString, 1, 20) Else .middleName = ""
            .lastName = Mid(dr("LastName"), 1, 20)
            .informalName = Mid(dr("LastName"), 1, 20)
            .title = "STUDENT"
            If Not dr("Prefix") Is System.DBNull.Value Then .prefix = Mid(dr("Prefix").ToString, 1, 20) Else .prefix = ""
            If Not dr("Suffix") Is System.DBNull.Value Then .suffix = Mid(dr("Suffix").ToString, 1, 20) Else .suffix = ""
        End With
        Return objStudentName
    End Function
    Private Function PopulateEmail(ByVal dtName As DataTable) As emailType
        Dim objEmail As New emailType
        Dim dr As DataRow
        dr = dtName.Rows(0)
        With objEmail
            If Not dr("HomeEmail") Is System.DBNull.Value Then
                .address = dr("HomeEmail").ToString
                .type = "HOME"
                .code = "HOME"
            Else
                .address = ""
                .type = "HOME"
                .code = "HOME"
            End If
        End With
        Return objEmail
    End Function
    Private Function PopulateWORKEmail(ByVal dtName As DataTable) As emailType
        Dim objEmail As New emailType
        Dim dr As DataRow
        dr = dtName.Rows(0)
        With objEmail
            If Not dr("WorkEmail") Is System.DBNull.Value Then
                .address = dr("WorkEmail").ToString
                .type = "WORK"
                .code = "WORK"
            Else
                .address = ""
                .type = "WORK"
                .code = "WORK"
            End If
        End With
        Return objEmail
    End Function
    Private Function PopulateDocumentTracking(ByVal dtName As DataTable, Optional ByVal SessionApplied As String = "", Optional ByVal intRowCounter As Integer = 0) As trackingType
        Dim objDocumentTracking As New trackingType
        Dim dr As DataRow
        dr = dtName.Rows(intRowCounter)
        With objDocumentTracking
            .session = SessionApplied 'dr("Session")
            If Not dr("trackCode") Is System.DBNull.Value Then
                .trackCode = Mid(dr("trackCode"), 1, 2)
            Else
                .trackCode = ""
            End If
            If Not dr("DueDate") Is System.DBNull.Value Then
                .dueDate = CDate(dr("DueDate")).ToString("yyyy-MM-dd")
                .dueDateSpecified = True
            Else
                .dueDate = ""
                .dueDateSpecified = False
            End If
            If Not dr("transdate") Is System.DBNull.Value Then
                .notifiedDate = CDate(dr("transdate")).ToString("yyyy-MM-dd")
                .notifiedDateSpecified = True
            Else
                .notifiedDate = ""
                .notifiedDateSpecified = False
            End If
            If Not dr("completedDate") Is System.DBNull.Value Then
                .completedDate = CDate(dr("completedDate")).ToString("yyyy-MM-dd")
                .completedDateSpecified = True
            Else
                .completedDate = ""
                .completedDateSpecified = False
            End If
            If Not dr("notificationcode") Is System.DBNull.Value Then
                .notationCode = dr("notificationcode")
            Else
                .notationCode = ""
            End If
            If Not dr("note") Is System.DBNull.Value Then
                .note = dr("note")
            Else
                .note = ""
            End If
        End With
        Return objDocumentTracking
    End Function
    Private Function PopulateDemographicData(ByVal dtDemographicData As DataTable) As demographicType
        Dim objStudentDemographic As New demographicType
        Dim dr As DataRow
        dr = dtDemographicData.Rows(0)
        With objStudentDemographic
            .alternateId = ""
            If Not dr("DOB") Is System.DBNull.Value Then .dateOfBirth = CDate(dr("DOB")).ToString("yyyy-MM-dd") Else .dateOfBirth = ""
            If Not dr("Sex") Is System.DBNull.Value Then .sex = dr("sex") Else .sex = ""
            If Not dr("ssn") Is System.DBNull.Value Then .ssn = dr("ssn") Else .ssn = ""
            If Not dr("maritalstatus") Is System.DBNull.Value Then .maritalStatus = dr("maritalstatus") Else .maritalStatus = ""
            If Not dr("citizen") Is System.DBNull.Value Then .citizenship = dr("citizen") Else .citizenship = ""
            If Not dr("race") Is System.DBNull.Value Then .ethnicity = dr("race") Else .ethnicity = ""
            .handicap = "NA"
            .deceasedDate = ""
            .pin = ""
            .status = "AC"
            .miscellaneous1 = ""
            .miscellaneous2 = ""
            .miscellaneous3 = ""
            .miscellaneous4 = ""
            .miscellaneous5 = ""
            .miscellaneous6 = ""
            .miscellaneous7 = ""
            .miscellaneous8 = ""
            .miscellaneous9 = ""
            .miscellaneous10 = ""
            .miscellaneous11 = ""
            .miscellaneous12 = ""
            .miscellaneous13 = ""
            .miscellaneous14 = ""
            .miscellaneous15 = ""
            .miscellaneous16 = ""
            .miscellaneous17 = ""
            .miscellaneous18 = ""
            .miscellaneous19 = ""
            .miscellaneous20 = ""
            .miscellaneous21 = ""
            .miscellaneous22 = ""
            .miscellaneous23 = ""
            .miscellaneous24 = ""
            .miscellaneous25 = ""
            .miscellaneous26 = ""
            .miscellaneous27 = ""
            .miscellaneous28 = ""
            .miscellaneous29 = ""
            .miscellaneous30 = ""
        End With
        Return objStudentDemographic
    End Function
    Private Function PopulateFinancialAid(ByVal dtName As DataTable) As financialAidType
        Dim objFinAid As New financialAidType
        Dim dr As DataRow
        dr = dtName.Rows(0)
        With objFinAid
            If Not dr("SessionStartTerm") Is System.DBNull.Value Then .sessionStart = dr("SessionStartTerm") Else .sessionStart = ""
            If Not dr("SessionEndTerm") Is System.DBNull.Value Then .sessionEnd = dr("SessionEndTerm") Else .sessionEnd = ""
            .status = "AC"
            .freezeStatus = ""
            .budgetCode = ""
            .budgetAmount = "0.0"
            .budgetMonths = "0"
            .childcareAmount = ""
            .supplementalAmount = "0.00"
            .programCost = "0.00"
            .packageCode = "0.00"
            .additionalAmount = "0.00"
            .scheduleCosts = "0.00"
            .enrollmentCode1 = ""
            .budgetCode1 = ""
            .enrollmentCode2 = ""
            .budgetCode2 = ""
            .enrollmentCode3 = ""
            .budgetCode3 = ""
            .enrollmentCode4 = ""
            .budgetCode4 = ""
            .enrollmentCode5 = ""
            .budgetCode5 = ""
            .parentIncome = ""
            .studentIncome = ""
            .numberOfDependents = ""
            .parentContribution = ""
            .studentContribution = ""
            .parentContributionIM = ""
            .studentContributionIM = ""
            .efc = ""
            .costOfEducation = ""
            .enrollmentCode = ""
            .sarId = ""
            .hoursYear = "0.00"
            .hoursExp = "0.00"
            .weeksYear = "0"
            .weeksExp = "0"
            .codCitizenshipCode = ""
            .needAmount = "0.00"
            .institutionalNeedAmount = "0.00"
            .afdcOrTanf = ""
            .dependencyStatus = ""
            .yearInSchool = ""
            If Not dr("MaritalStatus") Is System.DBNull.Value Then .parentOrStudentMaritalStatus = dr("MaritalStatus") Else .parentOrStudentMaritalStatus = ""
            .pellEligibility = ""
            .coaOverride = "0.00"
        End With
        Return objFinAid
    End Function
    Private Function PopulateAward(ByVal dtAwardData As DataTable) As awardType
        Dim objAwardType As New awardType
        Dim dr As DataRow
        dr = dtAwardData.Rows(0)
        With objAwardType
            If Not dr("session") Is System.DBNull.Value Then .session = Mid(dr("session"), 1, 4) Else .session = ""
            If Not dr("code") Is System.DBNull.Value Then .code = dr("code") Else .code = ""
            If Not dr("status") Is System.DBNull.Value Then .status = dr("status") Else .status = ""
            If Not dr("amount") Is System.DBNull.Value Then .amount = dr("amount") Else .amount = ""
            If Not dr("subCode") Is System.DBNull.Value Then .subCode = Mid(dr("subCode"), 1, 6) Else .subCode = ""
            .miscellaneousCode1 = ""
            .miscellaneousCode2 = ""
            If Not dr("ActivityDate") Is System.DBNull.Value Then
                .activityDate = CDate(dr("ActivityDate")).ToString("yyyy-MM-dd") 'FormatDateTime(dr("ActivityDate"), DateFormat.ShortDate)
            Else
                .activityDate = ""
            End If
        End With
        Return objAwardType
    End Function
    Private Function PopulateTracking(ByVal dtTrackingData As DataTable) As trackingType
        Dim objtrackingType As New trackingType
        Dim dr As DataRow
        dr = dtTrackingData.Rows(0)
        With objtrackingType
            .session = ""
            .trackCode = ""
            .dueDate = ""
            .notifiedDate = ""
            .completedDate = ""
            .notationCode = ""
        End With
        Return objtrackingType
    End Function
    Private Function PopulateActivity(ByVal dtActivityData As DataTable, Optional ByVal SessionApplied As String = "", Optional ByVal intRowCounter As Integer = 0) As activityType
        Dim objactivityType As New activityType
        Dim dr As DataRow
        dr = dtActivityData.Rows(intRowCounter)
        With objactivityType
            .session = SessionApplied
            If Not dr("Code") Is System.DBNull.Value Then .code = dr("Code") Else .code = ""
            If Not dr("ActivityDate") Is System.DBNull.Value Then
                .date = CDate(dr("ActivityDate")).ToString("yyyy-MM-dd")
            Else
                .date = ""
            End If
            If Not dr("miscellaneousCode1") Is System.DBNull.Value Then .miscellaneousCode1 = dr("miscellaneousCode1") Else .miscellaneousCode1 = ""
            .miscellaneousCode2 = ""
            .miscellaneousCode3 = ""
        End With
        Return objactivityType
    End Function
    Private Function PopulateComment(ByVal dtStudentDetails As DataTable) As commentType
        Dim objCommentType As New commentType
        Dim dr As DataRow
        Dim drStudent As DataRow
        ' dr = dtCommentData.Rows(0)
        drStudent = dtStudentDetails.Rows(0)
        With objCommentType
            .session = drStudent("SessionApplied")
            .blockNumber = drStudent("BlockNumber")
            If Not drStudent("Comments") Is System.DBNull.Value Then .comment = drStudent("Comments") Else .comment = ""
            .type = drStudent("CommentType")
        End With
        Return objCommentType
    End Function
    Private Function PopulateAcademic(ByVal dtName As DataTable, ByVal i As Integer) As academicType
        Dim objAcademic As New academicType
        Dim dr As DataRow
        dr = dtName.Rows(i)
        With objAcademic
            .session = dr("Session")
            If Not dr("degreecode") Is System.DBNull.Value Then .degreeCode = dr("degreecode").ToString Else .degreeCode = ""
            If Not dr("curriculumCode") Is System.DBNull.Value Then .curriculumCode = dr("curriculumCode").ToString Else .curriculumCode = ""
            .appealCode = ""
            .appealdate = ""
            .sessionHoursAttempted = ""
            .sessionHoursEarned = ""
            .sessionHoursFunded = ""
            If Not dr("academicStatus") Is System.DBNull.Value Then .academicStatus = dr("academicStatus").ToString Else .academicStatus = ""
            .sessionGpa = ""
            .cumulativeGpa = ""
            .cumulativeQpa = ""
            'If Not dr("withdrawalCode") Is System.DBNull.Value Then .withdrawalCode = dr("withdrawalCode").ToString Else .withdrawalCode = ""
            'If Not dr("withdrawalDate") Is System.DBNull.Value Then .withdrawalDate = dr("withdrawalDate").ToString Else .withdrawalDate = ""
            .miscellaneousCode1 = ""
            .miscellaneousCode2 = ""
            .withdrawalCode = ""
            .withdrawalDate = ""
            .cumulativeTransferHours = ""
            .cipCode = ""
            .gradeLevel = ""
            .overrideHours = ""
            .hoursAdjustment = ""
            .miscellaneousHours1 = ""
            .miscellaneousHours2 = ""
        End With
        Return objAcademic
    End Function
#End Region



    'Public Function GetRequirementsGroupsByStudent(ByVal StudentId As String, ByVal CampusId As String, Optional ByVal ProgId As String = "", Optional ByVal ShiftId As String = "", Optional ByVal EnrollmentStatus As String = "") As DataTable
    '    Dim dstNorthwind As DataSet
    '    Dim conNorthwind As New OleDbConnection
    '    Dim dadNorthwind As OleDbDataAdapter
    '    Dim drowParent As DataRow
    '    Dim drowChild As DataRow
    '    Dim drowRootParent As DataRow
    '    Dim drowSchoolLevelSummary As DataRow
    '    Dim drowSchoolLevelDetails As DataRow
    '    Dim s3 As String
    '    Dim forCounter As Integer
    '    Dim sb, sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
    '    Dim drowRequirementAssignedDirectlyToPrgVersion, drowRequirementAssignedDirectlyToLeadGroup As DataRow
    '    Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder

    '    Dim dsGetEntranceTestId As New DataSet
    '    Dim dsGetLeadGrpId As New DataSet
    '    Dim dstGetRequirementsAssignedToProgramVersion As New DataSet
    '    Dim dstGetRequirementsDirectlyAssigned As New DataSet
    '    Dim dstGetLeadGroups As New DataSet
    '    Dim dsGetCmpGrps As New DataSet
    '    Dim intRowCounter As Integer
    '    Dim strLeadGrpId As String
    '    Dim dstOverrideTest As New DataSet
    '    Dim strEntrTestId As String
    '    Dim strCurrentDate As String = Date.Now.ToShortDateString

    '    dstNorthwind = New DataSet
    '    conNorthwind.ConnectionString = SingletonAppSettings.AppSettings("ConString")



    '    Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
    '    Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

    '    dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
    '    Dim strCampGrpId As String
    '    If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
    '        For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
    '            strCampGrpId &= row("CampGrpId").ToString & "','"
    '        Next
    '        strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
    '    End If

    '    dstGetLeadGroups = GetLeadGroupsByStudent(StudentId)
    '    If dstGetLeadGroups.Tables(0).Rows.Count >= 1 Then
    '        For Each row As DataRow In dstGetLeadGroups.Tables(0).Rows
    '            strLeadGrpId &= row("LeadGrpId").ToString & "','"
    '        Next
    '        strLeadGrpId = Mid(strLeadGrpId, 1, InStrRev(strLeadGrpId, "'") - 2)
    '    End If

    '    dstOverrideTest = GetOverridedTestByStudent(StudentId)
    '    If dstOverrideTest.Tables(0).Rows.Count >= 1 Then
    '        For Each row As DataRow In dstOverrideTest.Tables(0).Rows
    '            strEntrTestId &= row("EntrTestId").ToString & "','"
    '        Next
    '        strEntrTestId = Mid(strEntrTestId, 1, InStrRev(strEntrTestId, "'") - 2)
    '    End If

    '    'Query gets the requirement group summary assigned to LeadGroup and Program Version
    '    With sb
    '        .Append(" select ReqGrpId,Descrip  ")
    '        .Append(" from ")
    '        .Append(" (     ")
    '        'TestDocumentAttempted removed from the count query by Balaji on 4/23/2007
    '        .Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,IsNull(R5.TestPassed,0)+IsNULL(R5.DocsApproved,0) as AttemptedReqs,  ")
    '        .Append(" Case when LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else LeadGrpId end as LeadGrpId ")
    '        .Append(" from ")
    '        '*********************************************************************************************************************************************
    '        ' This Query gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted by Lead and Program Version 
    '        '*********************************************************************************************************************************************
    '        .Append(" (Select  Distinct t1.ReqGrpId,t2.Descrip,t2.CampGrpId,t3.NumReqs as Numreqs,  ")
    '        ''''''''''''' This Query gives the number of test passed by the lead and grouped by lead group ''''''''
    '        .Append(" (select Count(*) from ")
    '        .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
    '        .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
    '        .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId ")
    '        If Not strLeadGrpId = "" Then
    '            .Append("  and A4.LeadGrpId in 	('")
    '            .Append(strLeadGrpId)
    '            .Append(") ")
    '        End If
    '        .Append("  and ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=1) ")
    '        .Append(" R1,adLeadEntranceTest R2,arStudent R3 WHERE ")
    '        .Append(" R1.adReqId = R2.EntrTestId and R2.StudentId = R3.StudentId and R3.StudentId='" & StudentId & "' and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and R2.Pass=1  ")
    '        If Not strEntrTestId = "" Then
    '            .Append("  and R2.EntrTestId not in 	('")
    '            .Append(strEntrTestId)
    '            .Append(") ")
    '        End If
    '        .Append(" and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as TestPassed, ")
    '        ''''''''''''' This Query gives the number of documents approved by the lead and grouped by lead group ''''''''
    '        .Append(" (select Count(*) from  ")
    '        .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
    '        .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
    '        .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId  ")
    '        If Not strLeadGrpId = "" Then
    '            .Append(" and A4.LeadGrpId in 	('")
    '            .Append(strLeadGrpId)
    '            .Append(") ")
    '        End If
    '        .Append(" and ")
    '        .Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=3) R1,plStudentDocs R2,syDocStatuses R3,arStudent R4 WHERE ")
    '        .Append(" R1.adReqId = R2.DocumentId and R2.StudentId=R4.StudentId and R4.StudentId='" & StudentId & "'  ")
    '        If Not strEntrTestId = "" Then
    '            .Append("  and R2.DocumentId not in 	('")
    '            .Append(strEntrTestId)
    '            .Append(") ")
    '        End If
    '        .Append(" and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as DocsApproved, ")
    '        .Append(" t4.LeadGrpId from adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3,adLeadGroups t4,arStuEnrollments t5,arStudent t6  where ")
    '        .Append(" t1.ReqGrpId = t2.ReqGrpId and t2.ReqGrpId = t3.ReqGrpId and t3.LeadGrpId = t4.LeadGrpId and t1.PrgVerId=t5.PrgVerId and t5.StudentId=t6.StudentId and t6.StudentId='" & StudentId & "' ")
    '        If Not ProgId = "" Then
    '            .Append(" and t5.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
    '        End If
    '        If Not ShiftId = "" Then
    '            .Append(" and t5.ShiftId = '" & ShiftId & "' ")
    '        End If
    '        If Not EnrollmentStatus = "" Then
    '            .Append(" and t5.StatusCodeId='" & EnrollmentStatus & "' ")
    '        End If
    '        .Append(" and t2.IsMandatoryReqGrp <> 1 ")
    '        If Not strLeadGrpId = "" Then
    '            .Append(" and t3.LeadGrpId in 	('")
    '            .Append(strLeadGrpId)
    '            .Append(") ")
    '        End If
    '        .Append(" ) R5  ")
    '        'where LeadGrpId='" & LeadGrpId & "'
    '        If Not strCampGrpId = "" Then
    '            .Append("  where R5.CampGrpId in 	('")
    '            .Append(strCampGrpId)
    '            .Append(") ")
    '        End If
    '        .Append("    ) R8 ")
    '        .Append("   where  ")
    '        .Append(" (AttemptedReqs < NumReqs ")
    '        .Append("  or  ")
    '        .Append(" (select Count(*) from adReqGrpDef where ReqGrpId=R8.ReqGrpId ")
    '        .Append(" and LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where StuEnrollId in ")
    '        .Append(" (select Distinct StuEnrollId from arStuEnrollments where StudentId='" & StudentId & "')) ")
    '        .Append("  and IsRequired=1 and  ")
    '        .Append(" ( ")
    '        ' Test should have been failed
    '        .Append(" adReqId in ")
    '        .Append(" (select distinct EntrTestId from adLeadEntranceTest where StudentId='" & StudentId & "' ")
    '        .Append(" and Pass=0 and EntrTestId not in (select EntrTestId from adEntrTestOverride where ")
    '        .Append(" StudentId='" & StudentId & "' and override=1)) ")
    '        .Append(" or  ")
    '        ' or document should be not approved
    '        .Append(" adReqId in ")
    '        .Append(" (select distinct DocumentId from plStudentDocs t1,syDocStatuses t2 where ")
    '        .Append(" t1.StudentId='" & StudentId & "'  and t1.DocStatusId = t2.DocStatusId and ")
    '        .Append(" t2.SysDocStatusId <> 1 and DocumentId not in ")
    '        .Append(" (select EntrTestId from adEntrTestOverride where StudentId='" & StudentId & "' ")
    '        .Append(" and override=1)) ")
    '        .Append(" or ")
    '        ' or requirement should be overriden
    '        .Append(" adReqId in ")
    '        .Append(" (select Distinct EntrTestId from adEntrTestOverride where StudentId='" & StudentId & "'  ")
    '        .Append(" and override=1))) >=1) ")
    '        '**************************************************************************************************************************************************************
    '        'End of the  Query that gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted by Lead and Program Version
    '        '**************************************************************************************************************************************************************
    '    End With
    '    dadNorthwind = New OleDbDataAdapter(sb.ToString, conNorthwind)
    '    conNorthwind.Open()
    '    dadNorthwind.Fill(dstNorthwind, "RequirementGroup")
    '    Return dstNorthwind.Tables(0)
    'End Function
    'Public Function GetRequirementsByReqGroupAndStudent(ByVal ReqGrpId As String, ByVal StudentId As String, ByVal CampusId As String) As DataTable
    '    Dim dstNorthwind As DataSet
    '    Dim conNorthwind As New OleDbConnection
    '    Dim dadNorthwind As OleDbDataAdapter
    '    Dim drowParent As DataRow
    '    Dim drowChild As DataRow
    '    Dim drowRootParent As DataRow
    '    Dim drowSchoolLevelSummary As DataRow
    '    Dim drowSchoolLevelDetails As DataRow
    '    Dim s3 As String
    '    Dim forCounter As Integer
    '    Dim sb, sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
    '    Dim drowRequirementAssignedDirectlyToPrgVersion, drowRequirementAssignedDirectlyToLeadGroup As DataRow
    '    Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder

    '    Dim dsGetEntranceTestId As New DataSet
    '    Dim dsGetLeadGrpId As New DataSet
    '    Dim dstGetRequirementsAssignedToProgramVersion As New DataSet
    '    Dim dstGetRequirementsDirectlyAssigned As New DataSet
    '    Dim dstGetLeadGroups As New DataSet
    '    Dim dsGetCmpGrps As New DataSet
    '    Dim intRowCounter As Integer
    '    Dim strLeadGrpId As String
    '    Dim dstOverrideTest As New DataSet
    '    Dim strEntrTestId As String
    '    Dim strCurrentDate As String = Date.Now.ToShortDateString

    '    dstNorthwind = New DataSet
    '    conNorthwind.ConnectionString = SingletonAppSettings.AppSettings("ConString")



    '    Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
    '    Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

    '    dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
    '    Dim strCampGrpId As String
    '    If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
    '        For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
    '            strCampGrpId &= row("CampGrpId").ToString & "','"
    '        Next
    '        strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
    '    End If

    '    dstGetLeadGroups = GetLeadGroupsByStudent(StudentId)
    '    If dstGetLeadGroups.Tables(0).Rows.Count >= 1 Then
    '        For Each row As DataRow In dstGetLeadGroups.Tables(0).Rows
    '            strLeadGrpId &= row("LeadGrpId").ToString & "','"
    '        Next
    '        strLeadGrpId = Mid(strLeadGrpId, 1, InStrRev(strLeadGrpId, "'") - 2)
    '    End If

    '    'Query gets the requirement group summary assigned to LeadGroup and Program Version
    '    With sb
    '        .Append("    select distinct t1.adReqId,Descrip,adReqTypeId,  ")
    '        .Append("    t6.ReqGrpId as ReqGrpId ")
    '        .Append("    from ")
    '        .Append("   (select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965') t1,  ")
    '        .Append("   (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
    '        .Append("   (select Distinct LeadGrpId,adReqEffectiveDateId,IsRequired from adReqLeadGroups where LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "' )) t3, ")
    '        .Append("   (select Distinct ReqGrpId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3  where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and t3.StudentId= '" & StudentId & "' and ReqGrpId='" & ReqGrpId & "' and ReqGrpId is not null) t5, ")
    '        .Append("   (select Distinct adReqId,ReqGrpId,LeadGrpId from adReqGrpDef where ReqGrpId='" & ReqGrpId & "') t6, ")
    '        .Append("   (select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "') t7 ")
    '        .Append("   where t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
    '        .Append("   t5.ReqGrpId = t6.ReqGrpId And t3.LeadGrpId = t7.LeadGrpId And t6.LeadGrpId = t7.LeadGrpId And t1.adReqId = t6.adReqId ")
    '        If Not strCampGrpId = "" Then
    '            .Append("  and  t1.CampGrpId in 	('")
    '            .Append(strCampGrpId)
    '            .Append(") ")
    '        End If
    '    End With
    '    dadNorthwind = New OleDbDataAdapter(sb.ToString, conNorthwind)
    '    conNorthwind.Open()
    '    dadNorthwind.Fill(dstNorthwind, "RequirementGroup")
    '    Return dstNorthwind.Tables(0)
    'End Function
    Public Function getAllMissingRequirements(ByVal StudentId As String, ByVal campusId As String) As DataTable
        Dim dsrequirement As New DataTable
        Dim dsReqGroups As New DataTable
        Dim dsreqWithinReqGroups As New DataTable
        Dim ds1 As New DataSet
        Dim dt As DataTable = ds1.Tables.Add("RequirementsTable")
        dt.Columns.Add(New DataColumn("adReqId", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("Descrip", System.Type.GetType("System.String")))
        dsrequirement = GetRequirements(StudentId, campusId)
        dsReqGroups = GetRequirementsGroupsByStudent(StudentId, campusId)
        If dsrequirement.Rows.Count >= 1 Then
            Dim row As DataRow
            For Each dr As DataRow In dsrequirement.Rows
                row = dt.NewRow()
                row("adReqId") = dr("adReqId")
                row("Descrip") = dr("Descrip")
                dt.Rows.Add(row)
            Next
        End If
        If dsReqGroups.Rows.Count >= 1 Then
            For Each dr As DataRow In dsReqGroups.Rows
                dsreqWithinReqGroups = GetRequirementsByReqGroupAndStudent(dr("ReqGrpId"), StudentId, campusId)
                If dsreqWithinReqGroups.Rows.Count >= 1 Then
                    Dim row1 As DataRow
                    For Each dr1 As DataRow In dsreqWithinReqGroups.Rows
                        row1 = dt.NewRow()
                        row1("adReqId") = dr1("adReqId")
                        row1("Descrip") = dr1("Descrip")
                        dt.Rows.Add(row1)
                    Next
                End If
            Next
        End If
        Dim dtGetDates As DataTable = ds1.Tables.Add("RequirementsTableByDate")
        Dim dtGetDueDates As New DataTable
        dtGetDates.Columns.Add(New DataColumn("trackCode", System.Type.GetType("System.String")))
        dtGetDates.Columns.Add(New DataColumn("note", System.Type.GetType("System.String")))
        dtGetDates.Columns.Add(New DataColumn("duedate", System.Type.GetType("System.String")))
        dtGetDates.Columns.Add(New DataColumn("completedDate", System.Type.GetType("System.String")))
        dtGetDates.Columns.Add(New DataColumn("transdate", System.Type.GetType("System.String")))
        dtGetDates.Columns.Add(New DataColumn("notificationcode", System.Type.GetType("System.String")))
        If dt.Rows.Count >= 1 Then
            Dim Daterow As DataRow
            For Each dr2 As DataRow In dt.Rows
                Daterow = dtGetDates.NewRow
                dtGetDueDates = GetReqsByDates(dr2("adReqId"), StudentId)
                If dtGetDueDates.Rows.Count >= 1 Then
                    For Each dr3 As DataRow In dtGetDueDates.Rows
                        Daterow("trackCode") = dr3("trackCode")
                        Daterow("transdate") = dr3("notifiedDate")
                        Daterow("note") = dr3("description")
                        Daterow("duedate") = dr3("duedate")
                        Daterow("completedDate") = dr3("completedDate")
                        Daterow("notificationcode") = dr3("notificationcode")
                        dtGetDates.Rows.Add(Daterow)
                    Next
                Else
                    dtGetDates.Rows.Add(Daterow)
                End If
            Next
        End If
        Return dtGetDates
    End Function
    Public Function GetReqsByDates(ByVal DocumentId As String, ByVal StudentId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dsStudentAddress As New DataSet
        db.OpenConnection()
        With sb
            .Append(" Select Distinct " + vbCrLf)
            .Append(" DueDate,completedDate,trackCode,notifiedDate,NotificationCode,Description " + vbCrLf)
            .Append(" from plStudentDocs SA" + vbCrLf)
            .Append(" where StudentId=? and DocumentId =?" + vbCrLf)
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@DocumentId", DocumentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        Try
            dsStudentAddress = db.RunParamSQLDataSet(sb.ToString)
            Return dsStudentAddress.Tables(0)
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function
    Public Function GetRequirements(ByVal StudentId As String, Optional ByVal campusId As String = "", Optional ByVal ProgId As String = "", Optional ByVal ShiftId As String = "", Optional ByVal EnrollmentStatus As String = "") As DataTable
        Dim sbRequirements As New StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        Dim dsGetCmpGrps As New DataSet
        Dim dstGetDocumentStatus As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        Dim strCampGrpId As String
        Dim strDocumentStatus As String


        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusId)

        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        dstGetDocumentStatus = (New MissingItemsDB).GetDocumentStatus(campusId)
        If dstGetDocumentStatus.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dstGetDocumentStatus.Tables(0).Rows
                strDocumentStatus &= row("DocStatusId").ToString & "','"
            Next
            strDocumentStatus = Mid(strDocumentStatus, 1, InStrRev(strDocumentStatus, "'") - 2)
        End If

        With sbRequirements
            'Mandatory Documents not approved
            .Append(" select distinct Descrip,StudentId,t1.adReqId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement=1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("       (select Distinct DocumentId,DocStatusId,StudentId from plStudentDocs where StudentId='" & StudentId & "' ")
            If Not strDocumentStatus = "" Then
                .Append(" and DocStatusId in ('")
                .Append(strDocumentStatus)
                .Append(") ")
            End If
            .Append(" ) t3 ")
            .Append("   where t1.adReqId = t2.adReqId And t1.adReqId = t3.DocumentId ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append("   union ")
            ' Mandatory Test not passed
            .Append(" select distinct Descrip,StudentId,t1.adReqId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement=1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("		(select EntrTestId,StudentId from adLeadEntranceTest where  StudentId='" & StudentId & "' and Pass=0) t4 ")
            .Append(" where t1.adReqId = t2.adReqId And t1.adReqId = t4.EntrTestId  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" Union ")
            ' Mandatory Test overridden
            .Append(" select distinct Descrip,StudentId,t1.adReqId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where MandatoryRequirement=1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("		(select Distinct EntrTestId,StudentId from adEntrTestOverride where Override=1 and  StudentId='" & StudentId & "' ) t4 ")
            .Append(" where t1.adReqId = t2.adReqId And t1.adReqId = t4.EntrTestId  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" Union ")
            ' Get Documents assigned directly to program version
            .Append(" select distinct Descrip,StudentId,t1.adReqId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("   	(select Distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "' and ReqGrpId is null ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId='" & EnrollmentStatus & "' ")
            End If
            .Append("       ) t5, ")
            .Append("       (select Distinct DocumentId,DocStatusId,StudentId from plStudentDocs where StudentId='" & StudentId & "' ")
            If Not strDocumentStatus = "" Then
                .Append("  and DocStatusId in ('")
                .Append(strDocumentStatus)
                .Append(") ")
            End If
            .Append(" ) t6 ")
            .Append(" where ")
            .Append("	t1.adReqId = t2.adReqId and t1.adReqId=t5.adReqId and  ")
            .Append("   t1.adReqId = t6.DocumentId  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" Union ")
            ' Get Test assigned directly to program version
            .Append(" select distinct Descrip,StudentId,t1.adReqId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("   	(select Distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "' and ReqGrpId is null ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" ) t5, ")
            .Append("       (select Distinct EntrTestId,StudentId from adLeadEntranceTest where StudentId='" & StudentId & "' and Pass=0) t7 ")
            .Append(" where t1.adReqId = t2.adReqId and t1.adReqId=t5.adReqId and t1.adReqId = t7.EntrTestId  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" Union ")
            .Append(" select distinct Descrip,StudentId,t1.adReqId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId from adReqsEffectiveDates where getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("   	(select Distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "' and ReqGrpId is null ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" ) t5, ")
            .Append("       (select Distinct EntrTestId,StudentId from adEntrTestOverride where Override=1 and StudentId='" & StudentId & "') t7 ")
            .Append(" where t1.adReqId = t2.adReqId and t1.adReqId=t5.adReqId and t1.adReqId = t7.EntrTestId  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" Union ")
            ' Get docs assigned directly to lead group
            .Append(" select distinct Descrip,StudentId,t1.adReqId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId  from adReqsEffectiveDates where getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append(" adReqLeadGroups t3, ")
            .Append("       (select Distinct DocumentId,DocStatusId,StudentId from plStudentDocs where StudentId='" & StudentId & "' ")
            If Not strDocumentStatus = "" Then
                .Append("  and DocStatusId in ('")
                .Append(strDocumentStatus)
                .Append(") ")
            End If
            .Append(" ) t4 ")
            .Append("   where ")
            .Append("       t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and  ")
            ' Requirement should not be in requirement group assigned to program version
            .Append(" t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2,arStuEnrollments s3,arStudent s4 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId=s3.PrgVerId and s3.StudentId=s4.StudentId and s4.StudentId='" & StudentId & "' and s2.adReqId is null   ")
            If Not ProgId = "" Then
                .Append(" and s3.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and s3.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and s3.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" ) ")
            .Append(" and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "' ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" ) and ")
            .Append(" t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "' and ReqGrpId is  null ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" )  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" and t3.IsRequired = 1 and ")
            .Append(" t1.adReqId = t4.DocumentId ")
            .Append("  Union ")
            'Test Requirements assigned directly to lead group that are marked as required for the lead group
            ' and the requirements should either be failed or not approved
            .Append(" select distinct Descrip,StudentId,t1.adReqId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId  from adReqsEffectiveDates where getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("       adReqLeadGroups t3, ")
            .Append("       (select EntrTestId,StudentId from adLeadEntranceTest where StudentId='" & StudentId & "' and Pass=0) t5 ")
            .Append("       where t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            ' Requirement should not be in requirement group assigned to program version
            .Append("       t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2,arStuEnrollments s3,arStudent s4 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId=s3.PrgVerId and s3.StudentId=s4.StudentId and s4.StudentId='" & StudentId & "' and s2.adReqId is null ")
            If Not ProgId = "" Then
                .Append(" and s3.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and s3.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and s3.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" )    ")
            .Append("       and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "') and  ")
            ' Requirement should not be directly assigned to program version
            .Append("       t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "' and ReqGrpId is null ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" )  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append("       and t3.IsRequired = 1 and ")
            .Append("       t1.adReqId = t5.EntrTestId ")
            .Append("  Union ")
            'Test Requirements assigned directly to lead group that are marked as required for the lead group
            ' and the requirements should either be failed or not approved
            .Append(" select distinct Descrip,StudentId,t1.adReqId ")
            .Append(" from ")
            .Append("		(select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and  StatusId='" & strActiveGUID & "') t1, ")
            .Append("		(select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId  from adReqsEffectiveDates where getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("       adReqLeadGroups t3, ")
            .Append("       (select Distinct EntrTestId,StudentId from adEntrTestOverride where Override=1 and StudentId='" & StudentId & "') t5 ")
            .Append("       where t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            ' Requirement should not be in requirement group assigned to program version
            .Append("       t1.adReqId not in (select Distinct s1.adReqId from adReqGrpDef s1,adPrgVerTestDetails s2,arStuEnrollments s3,arStudent s4 where s1.ReqGrpId=s2.ReqGrpId and s2.PrgVerId=s3.PrgVerId and s3.StudentId=s4.StudentId and s4.StudentId='" & StudentId & "' and s2.adReqId is null ")
            If Not ProgId = "" Then
                .Append(" and s3.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and s3.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and s3.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append("           )    ")
            .Append("       and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "'  ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" ) and ")
            ' Requirement should not be directly assigned to program version
            .Append("       t1.adReqId not in (select distinct adReqId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3 where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "' and ReqGrpId is null ")
            If Not ProgId = "" Then
                .Append(" and t2.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t2.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t2.StatusCodeId = '" & EnrollmentStatus & "' ")
            End If
            .Append(" )  ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append("       and t3.IsRequired = 1 and ")
            .Append("       t1.adReqId = t5.EntrTestId ")
        End With
        '   return dataset
        Return db.RunParamSQLDataSet(sbRequirements.ToString).Tables(0)
    End Function
    Public Function GetRequirementsGroupsByStudent(ByVal StudentId As String, Optional ByVal CampusId As String = "", Optional ByVal ProgId As String = "", Optional ByVal ShiftId As String = "", Optional ByVal EnrollmentStatus As String = "") As DataTable
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim drowRootParent As DataRow
        Dim drowSchoolLevelSummary As DataRow
        Dim drowSchoolLevelDetails As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb, sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
        Dim drowRequirementAssignedDirectlyToPrgVersion, drowRequirementAssignedDirectlyToLeadGroup As DataRow
        Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder

        Dim dsGetEntranceTestId As New DataSet
        Dim dsGetLeadGrpId As New DataSet
        Dim dstGetRequirementsAssignedToProgramVersion As New DataSet
        Dim dstGetRequirementsDirectlyAssigned As New DataSet
        Dim dstGetLeadGroups As New DataSet
        Dim dsGetCmpGrps As New DataSet
        Dim intRowCounter As Integer
        Dim strLeadGrpId As String
        Dim dstOverrideTest As New DataSet
        Dim strEntrTestId As String
        Dim strCurrentDate As String = Date.Now.ToShortDateString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")



        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        dstGetLeadGroups = (New MissingItemsDB).GetLeadGroupsByStudent(StudentId)
        If dstGetLeadGroups.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dstGetLeadGroups.Tables(0).Rows
                strLeadGrpId &= row("LeadGrpId").ToString & "','"
            Next
            strLeadGrpId = Mid(strLeadGrpId, 1, InStrRev(strLeadGrpId, "'") - 2)
        End If

        dstOverrideTest = (New MissingItemsDB).GetOverridedTestByStudent(StudentId)
        If dstOverrideTest.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dstOverrideTest.Tables(0).Rows
                strEntrTestId &= row("EntrTestId").ToString & "','"
            Next
            strEntrTestId = Mid(strEntrTestId, 1, InStrRev(strEntrTestId, "'") - 2)
        End If

        'Query gets the requirement group summary assigned to LeadGroup and Program Version
        With sb
            .Append(" select ReqGrpId,Descrip  ")
            .Append(" from ")
            .Append(" (     ")
            'TestDocumentAttempted removed from the count query by Balaji on 4/23/2007
            .Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,IsNull(R5.TestPassed,0)+IsNULL(R5.DocsApproved,0) as AttemptedReqs,  ")
            .Append(" Case when LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            '*********************************************************************************************************************************************
            ' This Query gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted by Lead and Program Version 
            '*********************************************************************************************************************************************
            .Append(" (Select  Distinct t1.ReqGrpId,t2.Descrip,t2.CampGrpId,t3.NumReqs as Numreqs,  ")
            ''''''''''''' This Query gives the number of test passed by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId ")
            If Not strLeadGrpId = "" Then
                .Append("  and A4.LeadGrpId in 	('")
                .Append(strLeadGrpId)
                .Append(") ")
            End If
            .Append("  and ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=1) ")
            .Append(" R1,adLeadEntranceTest R2,arStudent R3 WHERE ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.StudentId = R3.StudentId and R3.StudentId='" & StudentId & "' and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and R2.Pass=1  ")
            If Not strEntrTestId = "" Then
                .Append("  and R2.EntrTestId not in 	('")
                .Append(strEntrTestId)
                .Append(") ")
            End If
            .Append(" and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as TestPassed, ")
            ''''''''''''' This Query gives the number of documents approved by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from  ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId  ")
            If Not strLeadGrpId = "" Then
                .Append(" and A4.LeadGrpId in 	('")
                .Append(strLeadGrpId)
                .Append(") ")
            End If
            .Append(" and ")
            .Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=3) R1,plStudentDocs R2,syDocStatuses R3,arStudent R4 WHERE ")
            .Append(" R1.adReqId = R2.DocumentId and R2.StudentId=R4.StudentId and R4.StudentId='" & StudentId & "'  ")
            If Not strEntrTestId = "" Then
                .Append("  and R2.DocumentId not in 	('")
                .Append(strEntrTestId)
                .Append(") ")
            End If
            .Append(" and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as DocsApproved, ")
            .Append(" t4.LeadGrpId from adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3,adLeadGroups t4,arStuEnrollments t5,arStudent t6  where ")
            .Append(" t1.ReqGrpId = t2.ReqGrpId and t2.ReqGrpId = t3.ReqGrpId and t3.LeadGrpId = t4.LeadGrpId and t1.PrgVerId=t5.PrgVerId and t5.StudentId=t6.StudentId and t6.StudentId='" & StudentId & "' ")
            If Not ProgId = "" Then
                .Append(" and t5.PrgVerId in (select Distinct PrgVerId from arPrgVersions where ProgId='" & ProgId & "') ")
            End If
            If Not ShiftId = "" Then
                .Append(" and t5.ShiftId = '" & ShiftId & "' ")
            End If
            If Not EnrollmentStatus = "" Then
                .Append(" and t5.StatusCodeId='" & EnrollmentStatus & "' ")
            End If
            .Append(" and t2.IsMandatoryReqGrp <> 1 ")
            If Not strLeadGrpId = "" Then
                .Append(" and t3.LeadGrpId in 	('")
                .Append(strLeadGrpId)
                .Append(") ")
            End If
            .Append(" ) R5  ")
            'where LeadGrpId='" & LeadGrpId & "'
            If Not strCampGrpId = "" Then
                .Append("  where R5.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append("    ) R8 ")
            .Append("   where  ")
            .Append(" (AttemptedReqs < NumReqs ")
            .Append("  or  ")
            .Append(" (select Count(*) from adReqGrpDef where ReqGrpId=R8.ReqGrpId ")
            .Append(" and LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where StuEnrollId in ")
            .Append(" (select Distinct StuEnrollId from arStuEnrollments where StudentId='" & StudentId & "')) ")
            .Append("  and IsRequired=1 and  ")
            .Append(" ( ")
            ' Test should have been failed
            .Append(" adReqId in ")
            .Append(" (select distinct EntrTestId from adLeadEntranceTest where StudentId='" & StudentId & "' ")
            .Append(" and Pass=0 and EntrTestId not in (select EntrTestId from adEntrTestOverride where ")
            .Append(" StudentId='" & StudentId & "' and override=1)) ")
            .Append(" or  ")
            ' or document should be not approved
            .Append(" adReqId in ")
            .Append(" (select distinct DocumentId from plStudentDocs t1,syDocStatuses t2 where ")
            .Append(" t1.StudentId='" & StudentId & "'  and t1.DocStatusId = t2.DocStatusId and ")
            .Append(" t2.SysDocStatusId <> 1 and DocumentId not in ")
            .Append(" (select EntrTestId from adEntrTestOverride where StudentId='" & StudentId & "' ")
            .Append(" and override=1)) ")
            .Append(" or ")
            ' or requirement should be overriden
            .Append(" adReqId in ")
            .Append(" (select Distinct EntrTestId from adEntrTestOverride where StudentId='" & StudentId & "'  ")
            .Append(" and override=1))) >=1) ")
            '**************************************************************************************************************************************************************
            'End of the  Query that gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted by Lead and Program Version
            '**************************************************************************************************************************************************************
        End With
        dadNorthwind = New OleDbDataAdapter(sb.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "RequirementGroup")
        Return dstNorthwind.Tables(0)
    End Function
    Public Function GetRequirementsByReqGroupAndStudent(ByVal ReqGrpId As String, ByVal StudentId As String, Optional ByVal CampusId As String = "") As DataTable
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim drowRootParent As DataRow
        Dim drowSchoolLevelSummary As DataRow
        Dim drowSchoolLevelDetails As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb, sb1, sbroot, sbReqAssignedDirectlyToPrgVersion, sbReqsAssignedDirectlyToLeadGroup As New StringBuilder
        Dim drowRequirementAssignedDirectlyToPrgVersion, drowRequirementAssignedDirectlyToLeadGroup As DataRow
        Dim sbMandatorySummary, sbMandatoryDetails As New StringBuilder

        Dim dsGetEntranceTestId As New DataSet
        Dim dsGetLeadGrpId As New DataSet
        Dim dstGetRequirementsAssignedToProgramVersion As New DataSet
        Dim dstGetRequirementsDirectlyAssigned As New DataSet
        Dim dstGetLeadGroups As New DataSet
        Dim dsGetCmpGrps As New DataSet
        Dim intRowCounter As Integer
        Dim strLeadGrpId As String
        Dim dstOverrideTest As New DataSet
        Dim strEntrTestId As String
        Dim strCurrentDate As String = Date.Now.ToShortDateString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")



        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        dstGetLeadGroups = (New MissingItemsDB).GetLeadGroupsByStudent(StudentId)
        If dstGetLeadGroups.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dstGetLeadGroups.Tables(0).Rows
                strLeadGrpId &= row("LeadGrpId").ToString & "','"
            Next
            strLeadGrpId = Mid(strLeadGrpId, 1, InStrRev(strLeadGrpId, "'") - 2)
        End If

        'Query gets the requirement group summary assigned to LeadGroup and Program Version
        With sb
            .Append("    select distinct t1.adReqId,Descrip,adReqTypeId,  ")
            .Append("    t6.ReqGrpId as ReqGrpId ")
            .Append("    from ")
            .Append("   (select Distinct adReqId,Descrip,adReqTypeId,CampGrpId from adReqs where adReqTypeId in (1,3) and StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965') t1,  ")
            .Append("   (select Distinct StartDate,EndDate,MinScore,adReqId,adReqEffectiveDateId from adReqsEffectiveDates where MandatoryRequirement <> 1 and getdate()>=StartDate and (getdate()<= EndDate or EndDate is NULL)) t2, ")
            .Append("   (select Distinct LeadGrpId,adReqEffectiveDateId,IsRequired from adReqLeadGroups where LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "' )) t3, ")
            .Append("   (select Distinct ReqGrpId from adPrgVerTestDetails t1,arStuEnrollments t2,arStudent t3  where t1.PrgVerId=t2.PrgVerId and t2.StudentId=t3.StudentId and t3.StudentId= '" & StudentId & "' and ReqGrpId='" & ReqGrpId & "' and ReqGrpId is not null) t5, ")
            .Append("   (select Distinct adReqId,ReqGrpId,LeadGrpId from adReqGrpDef where ReqGrpId='" & ReqGrpId & "') t6, ")
            .Append("   (select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId and t3.StudentId='" & StudentId & "') t7 ")
            .Append("   where t1.adReqId = t2.adReqId and t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            .Append("   t5.ReqGrpId = t6.ReqGrpId And t3.LeadGrpId = t7.LeadGrpId And t6.LeadGrpId = t7.LeadGrpId And t1.adReqId = t6.adReqId ")
            If Not strCampGrpId = "" Then
                .Append("  and  t1.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With
        dadNorthwind = New OleDbDataAdapter(sb.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "RequirementGroup")
        Return dstNorthwind.Tables(0)
    End Function


    'Private Sub UpdateAdvantageWithRegentStudentId(ByVal SSN As String)
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim dsStudentAddress As New DataSet
    '    Dim strSSN As String = ""
    '    db.ConnectionString = SingletonAppSettings.AppSettings("RegentConnectionString")
    '    db.OpenConnection()
    '    With sb
    '        .Append(" Select Distinct SSN " + vbCrLf)
    '        .Append(" from arStudent" + vbCrLf)
    '        .Append(" where StudentId=? ")
    '    End With
    '    db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    Try
    '        strSSN = db.RunParamSQLScalar(sb.ToString)
    '        Return strSSN
    '    Catch ex As System.Exception
    '        Return ""
    '    Finally
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)
    '        If db.Connection.State = ConnectionState.Open Then
    '            db.CloseConnection()
    '        End If
    '    End Try
    'End Sub
    'Private Sub GetRegentStudentId(ByVal SSN As String)
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim dsStudentAddress As New DataSet
    '    Dim strSSN As String = ""
    '    Dim strDBName As String = ""
    '    Dim strWhereClause As String = ""
    '    strDBName = SingletonAppSettings.AppSettings("RegentDBName").ToString.ToLower
    '    If Not strDBName = "" Then
    '        strDBName = strDBName & "ALLY.SAF_STD_MAS"
    '    End If
    '    db.ConnectionString = SingletonAppSettings.AppSettings("RegentConnectionString")
    '    db.OpenConnection()
    '    With sb
    '        .Append(" Select Distinct SAF_STD_ID " + vbCrLf)
    '        .Append(" from " + strDBName)
    '    End With
    '    db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    Try
    '        strSSN = db.RunParamSQLScalar(sb.ToString)
    '        Return strSSN
    '    Catch ex As System.Exception
    '        Return ""
    '    Finally
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)
    '        If db.Connection.State = ConnectionState.Open Then
    '            db.CloseConnection()
    '        End If
    '    End Try
    'End Sub
End Class
