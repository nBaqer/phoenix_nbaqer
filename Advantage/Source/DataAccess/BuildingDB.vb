Imports System.Data
Imports System.Data.OleDb
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class BuildingDB
    Public Function GetAllBuildings() As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        ''  Dim dr As OleDbDataReader

        Try

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            'With sb
            '    .Append("SELECT t1.StdAddressId,Address1,ST.StatusId,ST.Status, t1.default1,t1.AddressTypeId, ")
            '    .Append(" (select AddressDescrip from plAddressTypes where AddressTypeId=t1.AddressTypeId) as AddressType ")
            '    .Append("FROM arStudAddresses t1,syStatuses ST ")
            '    .Append("WHERE StudentId = ? and t1.StatusId = ST.StatusId and Address1 is not null ")
            '    If statusId = "True" Then
            '        .Append("AND    ST.Status = 'Active' ")
            '        .Append(" Order By t1.Address1 ")
            '    ElseIf statusId = "False" Then
            '        .Append("AND    ST.Status = 'InActive' ")
            '        .Append(" Order By t1.Address1 ")
            '    Else
            '        .Append("ORDER BY ST.Status,t1.Address1 asc")
            '    End If
            'End With

            With sb
                .Append("SELECT t1.BldgId,BldgDescrip,ST.StatusId, ")
                .Append(" (Case ST.Status when 'Active' then 1 else 0 end) As Status ")
                .Append("FROM arBuildings t1,syStatuses ST ")
                .Append("WHERE  t1.StatusId = ST.StatusId  ")
                .Append("ORDER BY ST.Status")
            End With

            db.OpenConnection()

            '            db.AddParameter("@statusid", statusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            Return ds

        Catch ex As Exception
            Throw New BaseException("Error retrieving student addresses - " & ex.InnerException.Message)
        Finally
            'Close Connection
            db.CloseConnection()

        End Try

    End Function
    Public Function DoesDefaultAddExist(ByVal StudentId As String) As Integer
        Dim ds As New DataSet
        'Dim da As OleDbDataAdapter
        'Dim sGrdSysDetailId As String
        Dim rowCount As Integer
        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("Select count(*) as Count from arStudAddresses a where a.StudentId = ? and default1 = 1 ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@stdId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try

        'Return the datatable in the dataset
        Return rowCount

    End Function
    Public Function GetBldgInfo(ByVal BldgId As String) As BuildingInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT CCT.BldgId, ")
            .Append("    CCT.Address1, ")
            .Append("    CCT.Address2, ")
            .Append("    CCT.BldgCode, ")
            .Append("    CCT.BldgDescrip, ")
            .Append("    CCT.BldgRooms, ")
            .Append("    CCT.BldgName, ")
            .Append("    CCT.BldgTitle, ")
            .Append("    CCT.BldgEmail, ")
            .Append("    CCT.BldgComments, ")
            .Append("    CCT.Zip, ")
            .Append("    (Select Status from syStatuses where StatusId=CCT.StatusId) As Status, ")
            .Append("    CCT.City, ")
            .Append("    CCT.Phone, ")
            .Append("    CCT.Fax, ")
            .Append("    CCT.StateId, ")
            .Append("    (Select StateDescrip from syStates where StateId=CCT.StateId) As State, ")
            .Append("    CCT.StatusId, ")
            .Append("    CCT.BldgOpen, ")
            .Append("    CCT.BldgClose, ")
            .Append("    CCT.CampGrpId, ")
            .Append("    CCT.CampusId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=CCT.CampGrpId) As CampGrp, ")
            .Append("    (Select CampDescrip from syCampuses where CampusId=CCT.CampusId) As Campus, ")
            .Append("    CCT.ForeignZip, ")
            .Append("    CCT.ForeignPhone, ")
            .Append("    CCT.ForeignFax, ")
            .Append("    CCT.Sun, ")
            .Append("    CCT.Mon, ")
            .Append("    CCT.Tue, ")
            .Append("    CCT.Wed, ")
            .Append("    CCT.Thu, ")
            .Append("    CCT.Fri, ")
            .Append("    CCT.Sat, ")
            .Append("    CCT.OtherState, ")
            .Append("    CCT.ModUser, ")
            .Append("    CCT.ModDate ")
            .Append("FROM  arBuildings CCT ")
            .Append("WHERE CCT.BldgId= ? ")
        End With

        ' Add the AcademicYearId to the parameter list
        db.AddParameter("@BldgId", BldgId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim Building As New BuildingInfo

        While dr.Read()

            '   set properties with data from DataReader
            With Building
                'get IsInDB
                .IsInDB = True
                '.StudentID = CType(dr("StudentId"), Guid).ToString()

                'get AcademicYearId
                .BldgId = CType(dr("BldgId"), Guid).ToString()

                If Not (dr("Address1") Is System.DBNull.Value) Then .Address1 = CType(dr("Address1"), String).ToString Else .Address1 = ""
                If Not (dr("Address2") Is System.DBNull.Value) Then .Address2 = CType(dr("Address2"), String).ToString Else .Address2 = ""
                If Not (dr("City") Is System.DBNull.Value) Then .City = CType(dr("City"), String).ToString Else .City = ""
                If Not (dr("StateId") Is System.DBNull.Value) Then .StateId = CType(dr("StateId"), Guid).ToString Else .StateId = Guid.Empty.ToString
                If Not (dr("State") Is System.DBNull.Value) Then .State = dr("State") Else .State = ""

                If Not (dr("Zip") Is System.DBNull.Value) Then .Zip = CType(dr("Zip"), String).ToString Else .Zip = ""

                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString Else .CampGrpId = Guid.Empty.ToString

                If Not (dr("CampGrp") Is System.DBNull.Value) Then .CampGrp = dr("CampGrp")

                If Not (dr("CampusId") Is System.DBNull.Value) Then .CampusId = CType(dr("CampusId"), Guid).ToString Else .CampusId = Guid.Empty.ToString

                If Not (dr("Campus") Is System.DBNull.Value) Then .Campus = dr("Campus")

                If Not (dr("ForeignZip") Is System.DBNull.Value) Then .ForeignZip = dr("ForeignZip") Else .ForeignZip = 0

                If Not (dr("ForeignPhone") Is System.DBNull.Value) Then .ForeignPhone = dr("ForeignPhone") Else .ForeignPhone = 0

                If Not (dr("ForeignFax") Is System.DBNull.Value) Then .ForeignFax = dr("ForeignFax") Else .ForeignFax = 0

                If Not (dr("OtherState") Is System.DBNull.Value) Then .OtherState = CType(dr("OtherState"), String).ToString Else .OtherState = ""

                If Not (dr("StatusId") Is System.DBNull.Value) Then .StatusId = CType(dr("StatusId"), Guid).ToString Else .StatusId = Guid.Empty.ToString

                If Not (dr("BldgOpen") Is System.DBNull.Value) Then .BldgOpen = CType(dr("BldgOpen"), String).ToString Else .BldgOpen = ""

                If Not (dr("BldgClose") Is System.DBNull.Value) Then .BldgClose = CType(dr("BldgClose"), String).ToString Else .BldgClose = ""

                If Not (dr("BldgTitle") Is System.DBNull.Value) Then .Title = CType(dr("BldgTitle"), String).ToString Else .Title = ""

                If Not (dr("BldgName") Is System.DBNull.Value) Then .Name = CType(dr("BldgName"), String).ToString Else .Name = ""

                If Not (dr("BldgEmail") Is System.DBNull.Value) Then .Email = CType(dr("BldgEmail"), String).ToString Else .Email = ""

                If Not (dr("BldgCode") Is System.DBNull.Value) Then .Code = CType(dr("BldgCode"), String).ToString Else .Code = ""

                If Not (dr("BldgComments") Is System.DBNull.Value) Then .Comments = CType(dr("BldgComments"), String).ToString Else .Comments = ""

                If Not (dr("BldgDescrip") Is System.DBNull.Value) Then .Descrip = CType(dr("BldgDescrip"), String).ToString Else .Descrip = ""

                If Not (dr("BldgRooms") Is System.DBNull.Value) Then .NoOfRooms = dr("BldgRooms") Else .NoOfRooms = 0

                If Not (dr("Sun") Is System.DBNull.Value) Then .Sun = dr("Sun") Else .Sun = 0

                If Not (dr("Mon") Is System.DBNull.Value) Then .Mon = dr("Mon") Else .Mon = 0

                If Not (dr("Tue") Is System.DBNull.Value) Then .Tue = dr("Tue") Else .Tue = 0

                If Not (dr("Wed") Is System.DBNull.Value) Then .Wed = dr("Wed") Else .Wed = 0

                If Not (dr("Thu") Is System.DBNull.Value) Then .Thur = dr("Thu") Else .Thur = 0

                If Not (dr("Fri") Is System.DBNull.Value) Then .Fri = dr("Fri") Else .Fri = 0

                If Not (dr("Sat") Is System.DBNull.Value) Then .Sat = dr("Sat") Else .Sat = 0

                If Not (dr("Phone") Is System.DBNull.Value) Then .Phone = CType(dr("Phone"), String).ToString Else .Phone = ""
                If Not (dr("Fax") Is System.DBNull.Value) Then .Fax = CType(dr("Fax"), String).ToString Else .Fax = ""

                'get ModUser
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate") Else .ModDate = Date.MinValue

                .ModUser = dr("ModUser")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return Building

    End Function
    Public Function UpdateBuilding(ByVal Building As BuildingInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim intForeignZip As Integer
        ''  Dim intForeignPhone As Integer
        Dim intDefault As Integer

        If Building.ForeignZip = 1 Then
            intForeignZip = 1
        Else
            intForeignZip = 0
        End If
        If Building.Default1 = 1 Then
            intDefault = 1
        Else
            intDefault = 0
        End If

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("Update arBuildings set Address1=?, ")
                .Append(" Address2=?,City=?,StateId=?,Zip=?, ")
                .Append(" BldgCode=?,BldgDescrip=?,BldgRooms=?,BldgName=?,BldgTitle=?,BldgEmail=?,BldgComments=?,BldgOpen=?,BldgClose=?, ")
                .Append("CampGrpId=?,CampusId=?,ModUser=?,ModDate=?,StatusId=?,ForeignZip=?,OtherState=?,ForeignPhone=?,ForeignFax=?,Phone=?,Fax=?, ")
                .Append("Sun=?,Mon=?,Tue=?,Wed=?,Thu=?,Fri=?,Sat=? ")
                .Append(" where BldgId = ? ")
            End With

            '   add parameters values to the query

            'Address
            If Building.Address1 = "" Then
                db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address1", Building.Address1, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Address2
            If Building.Address2 = "" Then
                db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", Building.Address2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'city
            If Building.City = "" Then
                db.AddParameter("@city", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@city", Building.City, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'StateId
            If Building.StateId = Guid.Empty.ToString Then
                db.AddParameter("@StateId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@State", Building.StateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Zip
            If Building.Zip = "" Then
                db.AddParameter("@zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@zip", Building.Zip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            If Building.Code = "" Then
                db.AddParameter("@Code", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Code", Building.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            If Building.Descrip = "" Then
                db.AddParameter("@descrip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@descrip", Building.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Rooms
            db.AddParameter("@NoOfRooms", Building.NoOfRooms, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            If Building.Name = "" Then
                db.AddParameter("@name", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@name", Building.Name, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            If Building.Title = "" Then
                db.AddParameter("@title", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@title", Building.Title, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Building.Email = "" Then
                db.AddParameter("@Email", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Email", Building.Email, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Building.Comments = "" Then
                db.AddParameter("@Comments", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Comments", Building.Comments, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Building.BldgOpen = "" Then
                db.AddParameter("@BldgOpen", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@BldgOpen", Building.BldgOpen, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Building.BldgClose = "" Then
                db.AddParameter("@BldgClose", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@BldgClose", Building.BldgClose, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Campgrp
            If Building.CampGrpId = "" Or Building.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", Building.CampGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            If Building.CampusId = "" Or Building.CampusId = Guid.Empty.ToString Then
                db.AddParameter("@CampusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CampusId", Building.CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'Status
            If Building.StatusId = "" Or Building.StatusId = Guid.Empty.ToString Then
                db.AddParameter("@Status", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Status", Building.StatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Foreign Zip
            db.AddParameter("@ForeignZip", Building.ForeignZip, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'Other State
            If Building.OtherState = "" Then
                db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@OtherState", Building.OtherState, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            db.AddParameter("@ForeignPhone", Building.ForeignPhone, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ForeignFax", Building.ForeignFax, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            'Phone
            If Building.Phone = "" Then
                db.AddParameter("@Phone", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Phone", Building.Phone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Fax
            If Building.Fax = "" Then
                db.AddParameter("@Fax", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Fax", Building.Fax, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Days
            db.AddParameter("@Sun", Building.Sun, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@Mon", Building.Mon, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@Tue", Building.Tue, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@Wed", Building.Wed, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@Thu", Building.Thur, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@Fri", Building.Fri, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@Sat", Building.Sat, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            'bldgId
            db.AddParameter("@Bldgid", Building.BldgId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Insert Only If Phone is not empty
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            db.ClearParameters()
            sb.Remove(0, sb.Length)

            Building.IsInDB = True
            Return ""
        Catch ex As OleDbException
            '   return an error to the client
            Return ex.Message
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        ''   execute the query
        'Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

        ''   If there were no updated rows then there was a concurrency problem
        'If rowCount = 1 Then
        '    '   return without errors
        '    Return ""
        'Else
        '    Return DALExceptions.BuildConcurrencyExceptionMessage()
        'End If

        'Catch ex As OleDbException
        '    '   return an error to the client
        '    Return DALExceptions.BuildErrorMessage(ex)

        'Finally
        '    'Close Connection
        '    db.CloseConnection()
        'End Try

    End Function
    Public Function AddBuilding(ByVal Building As BuildingInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim intForeignZip As Integer
        Dim intDefault As Integer

        If Building.ForeignZip = 1 Then
            intForeignZip = 1
        Else
            intForeignZip = 0
        End If
        If Building.Default1 = 1 Then
            intDefault = 1
        Else
            intDefault = 0
        End If

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            Dim sb4 As New StringBuilder
            With sb4
                .Append(" Insert into arBuildings(BldgId,Address1,Address2,")
                .Append(" City,StateId,Zip, ")
                .Append(" BldgCode,BldgDescrip,BldgRooms,BldgName,BldgTitle,BldgEmail,BldgComments,BldgOpen,BldgClose, ")
                .Append(" CampGrpId,CampusId,ModUser,ModDate,StatusId,ForeignZip,OtherState,ForeignPhone,ForeignFax,Phone,Fax, ")
                .Append(" Sun,Mon,Tue,Wed,Thu,Fri,Sat)")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            'bldgId
            db.AddParameter("@Bldgid", Building.BldgId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Address
            If Building.Address1 = "" Then
                db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address1", Building.Address1, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Address2
            If Building.Address2 = "" Then
                db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", Building.Address2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'city
            If Building.City = "" Then
                db.AddParameter("@city", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@city", Building.City, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'StateId
            If Building.StateId = "" Or Building.StateId = Guid.Empty.ToString Then
                db.AddParameter("@State", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@State", Building.StateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Building.Zip = "" Then
                db.AddParameter("@zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@zip", Building.Zip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Building.Code = "" Then
                db.AddParameter("@Code", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Code", Building.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            If Building.Descrip = "" Then
                db.AddParameter("@descrip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@descrip", Building.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Rooms
            db.AddParameter("@NoOfRooms", Building.NoOfRooms, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            If Building.Name = "" Then
                db.AddParameter("@name", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@name", Building.Name, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            If Building.Title = "" Then
                db.AddParameter("@title", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@title", Building.Title, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Building.Email = "" Then
                db.AddParameter("@Email", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Email", Building.Email, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Building.Comments = "" Then
                db.AddParameter("@Comments", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Comments", Building.Comments, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Building.BldgOpen = "" Then
                db.AddParameter("@BldgOpen", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@BldgOpen", Building.BldgOpen, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            If Building.BldgClose = "" Then
                db.AddParameter("@BldgClose", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@BldgClose", Building.BldgClose, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Campgrp
            If Building.CampGrpId = "" Or Building.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", Building.CampGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            If Building.CampusId = "" Or Building.CampusId = Guid.Empty.ToString Then
                db.AddParameter("@CampusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CampusId", Building.CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'Status
            If Building.StatusId = "" Or Building.StatusId = Guid.Empty.ToString Then
                db.AddParameter("@Status", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Status", Building.StatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Foreign Zip
            db.AddParameter("@ForeignZip", Building.ForeignZip, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'Other State
            If Building.OtherState = "" Then
                db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@OtherState", Building.OtherState, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            db.AddParameter("@ForeignPhone", Building.ForeignPhone, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ForeignFax", Building.ForeignFax, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'Phone
            If Building.Phone = "" Then
                db.AddParameter("@Phone", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Phone", Building.Phone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Fax
            If Building.Fax = "" Then
                db.AddParameter("@Fax", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Fax", Building.Fax, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Days
            db.AddParameter("@Sun", Building.Sun, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.AddParameter("@Mon", Building.Mon, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.AddParameter("@Tue", Building.Tue, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.AddParameter("@Wed", Building.Wed, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.AddParameter("@Thu", Building.Thur, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.AddParameter("@Fri", Building.Fri, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.AddParameter("@Sat", Building.Sat, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb4.ToString)
            db.ClearParameters()
            sb4.Remove(0, sb4.Length)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteBuilding(ByVal BldgId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM arBuildings ")
                .Append("WHERE BldgId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM arBuildings WHERE BldgId = ? ")
            End With

            '   add parameters values to the query

            '   AcademicYearId
            db.AddParameter("@BldgId", BldgId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   AcademicYearId
            db.AddParameter("@BldgId2", BldgId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

End Class
