Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class RoomDB
    Public Function GetRooms(ByVal BldgId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess
        Dim da As OleDbDataAdapter
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            '   build the sql query
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT t1.RoomId,t1.Descrip, t1.StatusId  ")
                .Append(" ,(select Distinct Status from syStatuses where StatusId=t1.StatusId) as Status ")
                .Append("FROM arRooms t1 ")
                .Append("WHERE t1.BldgId = ?")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ProgId", BldgId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "Bldgs")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds
    End Function
    Public Function GetBldgRooms(ByVal Building As String) As Integer
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim sGrdSysDetailId As String
        Dim rowCount As Integer
        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("Select count(*) as Count from arRooms where BldgId = ? ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@BldgId", Building, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            db.OpenConnection()

            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try

        'Return the datatable in the dataset
        Return rowCount
    End Function
End Class
