Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' FundSourcesDB.vb
'
' FundSourcesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class FundSourcesDB
    Public Function GetAllFundSources(ByVal showActiveOnly As String, Optional ByVal titleIV As Boolean = False) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   CCT.FundSourceId, CCT.StatusId,CCT.FundSourceCode,")
            .Append("         CCT.FundSourceDescrip,ST.StatusId,ST.Status,")
            .Append("         IsUsedInFA = (CASE WHEN (SELECT COUNT(*) FROM faStudentAwards WHERE AwardTypeId=CCT.FundSourceId) > 0 THEN 'True' ELSE 'False' END) ")
            .Append("FROM     saFundSources CCT, syStatuses ST ")
            .Append("WHERE    CCT.StatusId = ST.StatusId ")
            If titleIV Then
                .Append("AND    CCT.TitleIV = 1 ")
            End If
            If showActiveOnly = "True" Then
                .Append("AND    ST.Status = 'Active' ")
                .Append(" ORDER BY  CCT.FundSourceDescrip ")
            ElseIf showActiveOnly = "False" Then
                .Append("AND    ST.Status = 'Inactive' ")
                .Append(" ORDER BY  CCT.FundSourceDescrip ")
            Else
                .Append("ORDER BY ST.Status,CCT.FundSourceDescrip asc")
            End If
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllFundSourcesByCampusX(ByVal showActiveOnly As String, ByVal campusId As String, Optional ByVal nontitleIvOnly As boolean = false) As DataSet

        'connect to the database
        Dim db As New DataAccess
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   CCT.FundSourceId, CCT.StatusId,CCT.FundSourceCode,")
            .Append("         CCT.FundSourceDescrip,ST.StatusId,ST.Status,")
            .Append("         IsUsedInFA = (CASE WHEN (SELECT COUNT(*) FROM faStudentAwards WHERE AwardTypeId=CCT.FundSourceId) > 0 THEN 'True' ELSE 'False' END) ")
            .Append("FROM     saFundSources CCT, syStatuses ST ")
            .Append("WHERE    CCT.StatusId = ST.StatusId ")
            .Append("AND  CampGrpId IN ( SELECT CampGrpId FROM dbo.syCmpGrpCmps WHERE CampusId = ? ) ")
            If nontitleIvOnly Then
                .Append(" And TitleIV = 0 ")
            End If
            If showActiveOnly = "True" Then
                .Append("AND    ST.Status = 'Active' ")
                .Append(" ORDER BY  CCT.FundSourceDescrip ")
            Else
                .Append("ORDER BY ST.Status,CCT.FundSourceDescrip asc")
            End If
        End With

        ' Add the campusId 
        db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetFundSourceInfo(ByVal FundSourceId As String) As FundSourceInfo

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT CCT.FundSourceId, ")
            .Append("    CCT.FundSourceCode, ")
            .Append("    CCT.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=CCT.StatusId) As Status, ")
            .Append("    CCT.FundSourceDescrip, ")
            .Append("    CCT.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=CCT.CampGrpId) As CampGrpDescrip, ")
            .Append("    CCT.AwardTypeId, ")
            .Append("    CCT.AdvFundSourceId, ")
            .Append("    (Select Distinct Descrip from syAdvFundSources where AdvFundSourceId=CCT.AdvFundSourceId) as AdvFundSourceDescrip, ")
            .Append("    CCT.TitleIV, ")
            .Append("    IsUsedInFA = (CASE WHEN (SELECT COUNT(*) FROM faStudentAwards WHERE AwardTypeId=CCT.FundSourceId) > 0 THEN 'True' ELSE 'False' END), ")
            .Append("    CCT.ModUser, ")
            'New Code Added By Vijay Ramteke On June 09, 2010
            '.Append("    CCT.ModDate ")
            .Append("    CCT.ModDate, CCT.CutoffDate ")
            'New Code Added By Vijay Ramteke On June 09, 2010
            .Append("FROM  saFundSources CCT ")
            .Append("WHERE CCT.FundSourceId= ? ")
        End With

        ' Add the FundSourceId to the parameter list
        db.AddParameter("@FundSourceId", FundSourceId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim FundSourceInfo As New FundSourceInfo

        While dr.Read()

            '   set properties with data from DataReader
            With FundSourceInfo
                .FundSourceId = FundSourceId
                .IsInDB = True
                .Code = dr("FundSourceCode")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Description = dr("FundSourceDescrip")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
                If Not (dr("AwardTypeId") Is System.DBNull.Value) Then .AwardTypeId = dr("AwardTypeId") Else .AwardTypeId = 0
                If dr("TitleIV").ToString <> "" Then
                    .TitleIV = dr("TitleIV")
                End If
                .IsUsedInFA = dr("IsUsedInFA")
                If Not (dr("AdvFundSourceId") Is System.DBNull.Value) Then .AdvFundSourceId = dr("AdvFundSourceId") Else .AdvFundSourceId = ""
                'New Code Added By Vijay Ramteke On June 09, 2010
                Try
                    .CutoffDate = dr("CutoffDate")
                Catch ex As Exception
                    .CutoffDate = Nothing
                End Try
                'New Code Added By Vijay Ramteke On June 09, 2010
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return FundSourceInfo

    End Function
    Public Function UpdateFundSourceInfo(ByVal FundSourceInfo As FundSourceInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saFundSources Set FundSourceId = ?, FundSourceCode = ?, ")
                .Append(" StatusId = ?, FundSourceDescrip = ?, CampGrpId = ?, AwardTypeId = ?, TitleIV = ?,AdvFundSourceId=?, ")
                'New Code Added By Vijay Ramteke On June 09, 2010
                '.Append(" ModUser = ?, ModDate = ? ")
                .Append(" ModUser = ?, ModDate = ?, CutoffDate = ? ")
                'New Code Added By Vijay Ramteke On June 09, 2010
                .Append("WHERE FundSourceId = ? ;")
                .Append("Select count(*) from saFundSources where FundSourceId = ? ")
            End With

            '   add parameters values to the query

            '   FundSourceId
            db.AddParameter("@FundSourceId", FundSourceInfo.FundSourceId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   FundSourceCode
            db.AddParameter("@FundSourceCode", FundSourceInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", FundSourceInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   FundSourceDescrip
            db.AddParameter("@FundSourceDescrip", FundSourceInfo.Description, DataAccess.OleDbDataType.OleDbString, 100, ParameterDirection.Input)

            '   CampGrpId
            If FundSourceInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", FundSourceInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            db.AddParameter("@AwardType", FundSourceInfo.AwardTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.AddParameter("@TitleIV", Convert.ToBoolean(FundSourceInfo.TitleIV), DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)

            If FundSourceInfo.AdvFundSourceId = "" Then
                db.AddParameter("@AdvFundSourceId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AdvFundSourceId", FundSourceInfo.AdvFundSourceId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'New Code Added By Vijay Ramteke On June 09, 2010
            If FundSourceInfo.CutoffDate = Nothing Then
                db.AddParameter("@CutoffDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@CutoffDate", FundSourceInfo.CutoffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            'New Code Added By Vijay Ramteke On June 09, 2010

            '   FundSourceId
            db.AddParameter("@FundSourceId", FundSourceInfo.FundSourceId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@UpdatedFundSourceId", FundSourceInfo.FundSourceId, DataAccess.OleDbDataType.OleDbString,50 , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddFundSourceInfo(ByVal FundSourceInfo As FundSourceInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saFundSources (FundSourceId, FundSourceCode, StatusId,AwardTypeId,TitleIV,  ")
                'New Code Added By Vijay Ramteke On June 09, 2010
                '.Append("   FundSourceDescrip, CampGrpId, AdvFundSourceId,ModUser, ModDate) ")
                '.Append("VALUES (?,?,?,?,?,?,?,?,?,?) ")
                .Append("   FundSourceDescrip, CampGrpId, AdvFundSourceId,ModUser, ModDate, CutoffDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?) ")
                'New Code Added By Vijay Ramteke On June 09, 2010
            End With

            '   add parameters values to the query

            '   FundSourceId
            db.AddParameter("@FundSourceId", FundSourceInfo.FundSourceId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   FundSourceCode
            db.AddParameter("@FundSourceCode", FundSourceInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", FundSourceInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.AddParameter("@AwardType", FundSourceInfo.AwardTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.AddParameter("@TitleIV", Convert.ToBoolean(FundSourceInfo.TitleIV), DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)

            '   FundSourceDescrip
            db.AddParameter("@FundSourceDescrip", FundSourceInfo.Description, DataAccess.OleDbDataType.OleDbString, 100, ParameterDirection.Input)

            '   CampGrpId
            If FundSourceInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", FundSourceInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If FundSourceInfo.AdvFundSourceId = "" Then
                db.AddParameter("@AdvFundSourceId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AdvFundSourceId", FundSourceInfo.AdvFundSourceId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'CutoffDate
            'New Code Added By Vijay Ramteke On June 09, 2010
            If FundSourceInfo.CutoffDate = Nothing Then
                db.AddParameter("@CutoffDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@CutoffDate", FundSourceInfo.CutoffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            'New Code Added By Vijay Ramteke On June 09, 2010

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteFundSourceInfo(ByVal fundSourceId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saFundSources ")
                .Append("WHERE FundSourceId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("Select count(*) from saFundSources where FundSourceId = ? ")
            End With

            '   add parameters values to the query

            '   FundSourceId
            db.AddParameter("@FundSourceId", fundSourceId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   FundSourceId
            db.AddParameter("@FundSourceId", fundSourceId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetAwardTypes() As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        With sb
            .Append("SELECT distinct a.AwardTypeId, a.Descrip ")
            .Append("FROM saAwardTypes a ")

        End With

        db.OpenConnection()
        ds = db.RunSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    Public Function GetAdvantageFundSources() As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        With sb
            .Append("SELECT distinct a.AdvFundSourceId, a.Descrip ")
            .Append("FROM syAdvFundSources a order by a.Descrip ")
        End With

        db.OpenConnection()
        ds = db.RunSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    'New Code Added By Vijay Ramteke On June 09, 2010
    Public Function GetManualPostingCutoffDate(ByVal AwardTypeId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim CutoffDate As String = ""
        With sb
            .Append("SELECT CutoffDate ")
            .Append("FROM saFundSources Where FundSourceId=? ")
        End With
        db.OpenConnection()
        'Clear Parameter
        db.ClearParameters()
        db.AddParameter("@AwardTypeId", AwardTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString, "FundSources")
        CutoffDate = ds.Tables("FundSources").Rows(0)("CutoffDate").ToString

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return CutoffDate
    End Function
    'New Code Added By Vijay Ramteke On June 09, 2010


#Region "Get AdvAppsetting for Manage Config entry"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
End Class
