Public Class NameValueListDB

#Region "Private Variables and Objects"

#End Region

#Region "Private Methods"
    Private Function GetDDLSQL(ByVal ddl As NameValueListInfo) As String
        Dim sb As New StringBuilder
        Dim strTemplate As String
        Dim DisplayField As String = "%DisplayField%"
        Dim ValueField As String = "%ValueField%"
        Dim TableName As String = "%TableName%"
        Dim ddlDisplayField As String
        Dim ddlValueField As String
        Dim ddlTableName As String


        strTemplate = "SELECT %ValueField%,%DisplayField% " & _
                      "FROM %TableName% " & _
                      "ORDER BY %DisplayField%"
        

        Select Case ddl.ToString
            Case NameValueListInfo.DDLLists.Countries.ToString
                ddlTableName = "adCountries"
                ddlDisplayField = "CountryDescrip"
                ddlValueField = "CountryId"
            Case NameValueListInfo.DDLLists.States.ToString
                ddlTableName = "syStates"
                ddlDisplayField = "StateDescrip"
                ddlValueField = "StateId"
            Case NameValueListInfo.DDLLists.Statuses.ToString
                ddlTableName = "syStatuses"
                ddlDisplayField = "Status"
                ddlValueField = "StatusId"
        End Select

        'Replace the TableName token
        strTemplate = strTemplate.Replace(TableName, ddlTableName)
        'Replace the DisplayField token
        strTemplate = strTemplate.Replace(DisplayField, ddlDisplayField)
        'Replace the ValueField token
        strTemplate = strTemplate.Replace(ValueField, ddlValueField)

        Return strTemplate

    End Function
#End Region

#Region "Public Methods"

    Public Function GetDDL(ByVal ddl As NameValueListInfo, ByVal conn As OleDbConnection) As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strSQL As String

        strSQL = GetDDLSQL(ddl)

        ds = db.RunSQLDataSet(strSQL, Nothing, conn)

        Return ds.Tables(0)

    End Function

#End Region



End Class
