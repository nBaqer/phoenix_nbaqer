Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' GendersDB.vb
'
' GendersDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class GendersDB
    Public Function GetAllGenders() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   GE.GenderId, GE.StatusId, GE.GenderDescrip ")
            .Append("FROM     adGenders GE, syStatuses ST ")
            .Append("WHERE    GE.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY GE.GenderDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllGendersNoUnknownOptions() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   GE.GenderId, GE.StatusId, GE.GenderDescrip ")
            .Append("FROM     adGenders GE, syStatuses ST ")
            .Append("WHERE    GE.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' and GE.GenderCode <> 'U' ")
            .Append("ORDER BY GE.GenderDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
End Class
