﻿Imports System.Web.UI.WebControls
Imports FAME.Advantage.Common

Public Class StatusChangeHistoryDB

    Public Function GetStatusChangeHistory(ByVal stuEnrollId As String) As IList(Of StatusChangeHistoryObj)
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" SELECT   ")
            .Append("     (CASE WHEN SSC.DateOfChange IS NULL THEN SSC.ModDate ELSE SSC.DateOfChange END) AS DateOfChange  ")
            .Append("       , SC.StatusCodeDescrip    ")
            .Append(" 	  ,  SSC.Lda, SSC.RequestedBy, SSC.CaseNumber   ")
            .Append(" 	  , (CASE WHEN (SC.SysStatusId = 12) or (sc.sysstatusid = 8 and ssc.DropReasonId is not null) THEN  DR.Descrip   ") ' Dropped
            .Append(" 			ELSE CASE WHEN (SC.SysStatusId = 10) THEN  rea.Descrip  ") ' LOA
            .Append(" 				ELSE CASE WHEN (SC.SysStatusId = 11) THEN sus.Reason  ") 'Suspended
            .Append(" 				   ELSE CASE WHEN (SC.SysStatusId = 20 OR SC.SysStatusId = 23 OR SC.SysStatusId = 24) THEN pro.Reason END END END END) AS Details   ") 'Probation
            .Append(" 	   , SSC.ModUser ")
            .Append(" 	  , SC.SysStatusId ")
            .Append(" 	  , SSC.NewStatusId ")
            '.Append(" 	  , SC.StatusCodeDescrip ")
            .Append(" 	  , SSC.StudentStatusChangeId  ")
            .Append(" 	  , SSC.ModDate  ")
            .Append(" FROM        ")
            .Append("      syStudentStatusChanges SSC   ")
            .Append(" 	  JOIN syStatusCodes SC ON SC.StatusCodeId = SSC.NewStatusId  ")
            .Append(" 	  LEFT JOIN dbo.arDropReasons DR ON DR.DropReasonId = SSC.DropReasonId  ")
            .Append(" 	  LEFT JOIN [dbo].[arStudentLOAs] Loa ON Loa.StudentStatusChangeId = SSC.StudentStatusChangeId ")
            .Append(" 	  LEFT JOIN dbo.arLOAReasons rea ON rea.LOAReasonId = Loa.LOAReasonId  ")
            .Append(" 	  LEFT JOIN dbo.arStdSuspensions sus ON sus.StudentStatusChangeId = SSC.StudentStatusChangeId ")
            .Append(" 	  LEFT JOIN dbo.arStuProbWarnings pro ON pro.StudentStatusChangeId = SSC.StudentStatusChangeId ")
            .Append("  ")
            .Append(" WHERE       ")
            .Append("     SSC.StuEnrollId = @StuEnrollId  ")
            .Append("     ORDER BY  SSC.DateOfChange DESC, SSC.ModDate DESC")
        End With

        Dim list As IList(Of StatusChangeHistoryObj) = New List(Of StatusChangeHistoryObj)
        Dim command As SqlCommand = New SqlCommand(sb.ToString(), sqlconn)
        command.Parameters.AddWithValue("@StuEnrollId", stuEnrollId)
        sqlconn.Open()
        Try
            Dim reader As SqlDataReader = command.ExecuteReader()
            While (reader.Read())
                Dim res = New StatusChangeHistoryObj()

                res.DateOfChange = reader("DateOfChange")
                Dim tmp = reader("Details")
                res.Details = If(tmp Is DBNull.Value, String.Empty, tmp.ToString())
                Dim tmp1 = reader("Lda")
                res.Lda = If(tmp1 Is DBNull.Value, Nothing, tmp1)
                res.ModDate = reader("ModDate")
                res.ModUser = reader("ModUser")
                res.Status = reader("StatusCodeDescrip")
                res.StudentStatusChangeId = reader("StudentStatusChangeId")
                Dim tmp2 = reader("RequestedBy")
                res.RequestedBy = If(tmp2 Is DBNull.Value, Nothing, tmp2)
                Dim caseNumber = reader("CaseNumber")
                res.CaseNumber = If(caseNumber Is DBNull.Value, Nothing, caseNumber)
                'res.
                list.Add(res)
            End While

            Return list

        Finally
            sqlconn.Close()
        End Try
    End Function


    Public Function GetLOA(ByVal StuEnrollId As String, ByVal NewStatusId As String, ByVal cntLOA As Integer) As String
        '   connect to the database
        Dim db As New DataAccess
        Dim myAdvAppSettings As AdvAppSettings = New AdvAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        '   build the sql query
        Dim sb As New StringBuilder
        Dim strDetails As String = ""
        Dim strSD As String = ""
        Dim strED As String = ""
        With sb
            .Append("SELECT * from (SELECT SLOA.StartDate, SLOA.EndDate, LOAR.Descrip,ROW_NUMBER() OVER ( ORDER BY SLOA.StartDate) AS Seq ")
            .Append(" FROM syStudentStatusChanges SSC, arStudentLOAs SLOA, arLOAReasons LOAR ")
            .Append(" WHERE ")
            .Append(" SSC.StuEnrollId=SLOA.StuEnrollId ")
            .Append(" AND SLOA.LOAReasonId = LOAR.LOAReasonId ")
            '' New Code Added By VIjay Ramteke On October 01, 2010 Form Mantis Id 
            ''.Append(" AND CONVERT(VARCHAR,SSC.ModDate,101)=CONVERT(VARCHAR,SLOA.ModDate,101) ")
            .Append(" AND SSC.StudentStatusChangeId=SLOA.StudentStatusChangeId ")
            '' New Code Added By VIjay Ramteke On October 01, 2010 Form Mantis Id 
            .Append(" AND SSC.StuEnrollId=? AND SSC.NewStatusId=?) A where A.seq= ? ")
        End With
        ' Add the CampusId to the parameter list
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@NewStatusId", NewStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@cntLOA", cntLOA, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString, "StuLOA")
        Try
            strSD = CType(ds.Tables("StuLOA").Rows(0)("StartDate").ToString, DateTime).ToShortDateString
        Catch ex As Exception
            strSD = ""
        End Try
        Try
            strED = CType(ds.Tables("StuLOA").Rows(0)("EndDate").ToString, DateTime).ToShortDateString
        Catch ex As Exception
            strED = ""
        End Try
        Try
            strDetails = "( " + strSD + " - " + strED + " ) - " + ds.Tables("StuLOA").Rows(0)("Descrip").ToString
        Catch ex As Exception
            strDetails = ""
        End Try
        'Close Connection
        db.CloseConnection()

        'Return String
        Return strDetails
    End Function

    Public Function GetGradDate(ByVal stuEnrollId As String) As String
        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = New AdvAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        '   build the sql query
        Dim sb As New StringBuilder
        Dim strDetails As String
        With sb
            .Append("SELECT ")
            .Append(" ExpGradDate AS GradDate ")
            .Append(" FROM ")
            .Append(" arStuEnrollments ")
            .Append(" WHERE ")
            .Append(" StuEnrollId = ? ")
        End With
        ' Add the CampusId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString, "GradDate")
        Try
            strDetails = CType(ds.Tables("GradDate").Rows(0)("GradDate").ToString, DateTime).ToShortDateString
        Catch ex As Exception
            strDetails = ""
        End Try
        'Close Connection
        db.CloseConnection()

        'Return String
        Return strDetails
    End Function

    ''' <summary>
    ''' Calculate the last date of Attendance (LDA)
    ''' </summary>
    ''' <param name="stuEnrollId"></param>
    ''' <param name="studentStatusChangeId">Not used!</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetLdaDate(ByVal stuEnrollId As String, Optional ByVal studentStatusChangeId As String = "") As DateTime?
        '   connect to the database
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT MAX(LDA) AS LDA FROM  ")
            .Append("	(  SELECT MAX(AttendedDate)as LDA ")
            .Append("		FROM arExternshipAttendance ")
            .Append("		WHERE StuEnrollId= @StuEnrollId  ")
            .Append("		UNION ALL ")
            .Append("		SELECT MAX(MeetDate) as LDA ")
            .Append("		FROM atClsSectAttendance")
            .Append("		WHERE StuEnrollId= @StuEnrollId AND (Actual >=1.00 and  Actual <> 99.00 and Actual <> 999.00 and Actual <> 9999.00)   ")
            .Append("		UNION ALL ")
            .Append("		SELECT MAX(AttendanceDate) as LDA ")
            .Append("		FROM atAttendance WHERE EnrollId= @StuEnrollId and Actual >=1   ")
            .Append("		UNION ALL ")
            .Append("		SELECT MAX(RecordDate) as LDA ")
            .Append("		FROM arStudentClockAttendance ")
            .Append("		WHERE StuEnrollId= @StuEnrollId  and (ActualHours >=1.00 and  ActualHours <> 99.00 and ActualHours <> 999.00 and ActualHours <> 9999.00)  ")
            .Append("		UNION ALL ")
            .Append("		SELECT MAX(MeetDate) as LDA FROM atConversionAttendance ")
            .Append("		WHERE StuEnrollId= @StuEnrollId  and (Actual >=1.00 and  Actual <> 99.00 and Actual <> 999.00 and Actual <> 9999.00)    ")
            .Append("		UNION ALL ")
            .Append("		SELECT LDA ")
            .Append("		FROM arStuEnrollments ")
            .Append("		WHERE StuEnrollId=@StuEnrollId   ) AS LDAS   ")
        End With
        Dim command As SqlCommand = New SqlCommand(sb.ToString(), sqlconn)
        command.Parameters.AddWithValue("@StuEnrollId", stuEnrollId)
        sqlconn.Open()
        Try
            Dim lda As Object = command.ExecuteScalar()
            Dim result As DateTime? = If(lda Is DBNull.Value, Nothing, lda)
            Return result
        Finally
            sqlconn.Close()
        End Try
    End Function

    'Public Function GetAcademicProbation(ByVal StuEnrollId As String, ByVal NewStatusId As String) As String
    '    '   connect to the database
    '    Dim db As New DataAccess
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '    '   build the sql query
    '    Dim sb As New StringBuilder
    '    Dim strDetails As String = ""
    '    Dim strSD As String = ""
    '    Dim strED As String = ""
    '    With sb
    '        .Append("SELECT SPW.StartDate, SPW.EndDate, SPW.Reason ")
    '        .Append(" FROM arStuProbWarnings SPW, arProbWarningTypes PWT , syStudentStatusChanges SSC ")
    '        .Append(" WHERE ")
    '        .Append(" SSC.StuEnrollId=SPW.StuEnrollId ")
    '        .Append(" AND CONVERT(VARCHAR,SSC.ModDate,101)=CONVERT(VARCHAR,SPW.ModDate,101) ")
    '        .Append(" AND SPW.StuEnrollId = ? AND SSC.NewStatusId=?  AND SPW.ProbWarningTypeId in (1,2) AND SPW.ProbWarningTypeId = PWT.ProbWarningTypeId ")
    '    End With
    '    ' Add the CampusId to the parameter list
    '    db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@NewStatusId", NewStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    '   Execute the query
    '    Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString, "StuAP")

    '    'Close Connection
    '    db.CloseConnection()
    '    Try
    '        strSD = CType(ds.Tables("StuAP").Rows(0)("StartDate").ToString, DateTime).ToShortDateString
    '    Catch ex As Exception
    '        strSD = ""
    '    End Try
    '    Try
    '        strED = CType(ds.Tables("StuAP").Rows(0)("EndDate").ToString, DateTime).ToShortDateString
    '    Catch ex As Exception
    '        strED = ""
    '    End Try
    '    Try
    '        strDetails = "( " + strSD + " - " + strED + " ) - " + ds.Tables("StuAP").Rows(0)("Reason").ToString
    '    Catch ex As Exception
    '        strDetails = ""
    '    End Try
    '    'Return String
    '    Return strDetails
    'End Function

    '' Code modified by kamalesh Ahuja on 25th and 26 August to resolve mantis issue id 19485
    Public Function GetAcademicProbation(ByVal stuEnrollId As String, ByVal newStatusId As String) As String
        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = New AdvAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        '   build the sql query
        Dim sb As New StringBuilder
        Dim strDetails As String
        Dim strSd As String
        Dim strEd As String
        With sb
            .Append("SELECT SPW.StartDate, SPW.EndDate, SPW.Reason ")
            .Append(" FROM arStuProbWarnings SPW, arProbWarningTypes PWT , syStudentStatusChanges SSC ")
            .Append(" WHERE ")
            .Append(" SSC.StuEnrollId=SPW.StuEnrollId ")
            .Append(" AND SSC.StudentStatusChangeId=SPW.StudentStatusChangeId ")
            .Append(" AND SPW.StuEnrollId = ? AND SSC.NewStatusId=?  AND SPW.ProbWarningTypeId in (1,2) AND SPW.ProbWarningTypeId = PWT.ProbWarningTypeId ")
        End With
        ' Add the CampusId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@NewStatusId", newStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString, "StuAP")

        'Close Connection
        db.CloseConnection()
        Try
            strSd = CType(ds.Tables("StuAP").Rows(0)("StartDate").ToString, DateTime).ToShortDateString
        Catch ex As Exception
            strSd = ""
        End Try
        Try
            strEd = CType(ds.Tables("StuAP").Rows(0)("EndDate").ToString, DateTime).ToShortDateString
        Catch ex As Exception
            strEd = ""
        End Try
        Try
            strDetails = "( " + strSd + " - " + strEd + " ) - " + ds.Tables("StuAP").Rows(0)("Reason").ToString
        Catch ex As Exception
            strDetails = ""
        End Try
        'Return String
        Return strDetails
    End Function
    ''
    Public Function GetSuspension(ByVal stuEnrollId As String, ByVal newStatusId As String) As String
        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = New AdvAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        '   build the sql query
        Dim sb As New StringBuilder
        Dim strDetails As String = ""
        Dim strSD As String = ""
        Dim strED As String = ""
        With sb
            .Append("SELECT SS.StartDate, SS.EndDate, SS.Reason ")
            .Append(" FROM arStdSuspensions SS, syStudentStatusChanges SSC ")
            .Append(" WHERE ")
            .Append(" SSC.StuEnrollId=SS.StuEnrollId ")
            .Append(" and SSC.StudentStatusChangeId=SS.StudentStatusChangeId ")
            .Append(" AND SS.StuEnrollId = ? AND SSC.NewStatusId=? ")
        End With
        ' Add the CampusId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@NewStatusId", newStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString, "StuAP")

        'Close Connection
        db.CloseConnection()
        Try
            strSD = CType(ds.Tables("StuAP").Rows(0)("StartDate").ToString, DateTime).ToShortDateString
        Catch ex As Exception
            strSD = ""
        End Try
        Try
            strED = CType(ds.Tables("StuAP").Rows(0)("EndDate").ToString, DateTime).ToShortDateString
        Catch ex As Exception
            strED = ""
        End Try
        Try
            strDetails = "( " + strSD + " - " + strED + " ) - " + ds.Tables("StuAP").Rows(0)("Reason").ToString
        Catch ex As Exception
            strDetails = ""
        End Try
        'Return String
        Return strDetails
    End Function
    ''

    ''' <summary>
    ''' Return a string with Enrollment Name and Student Name for a particular Enrollment Id separate by ,
    ''' Example: Mercadeo a traves de Redes Sociales , Sandra  Diaz
    ''' </summary>
    ''' <param name="stuEnrollId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetEnrollmentNameAndStudentName(ByVal stuEnrollId As String) As String
        '   connect to the database
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT (PrgVerDescrip + ' , ' + COALESCE(FirstName, '') + ' ' + COALESCE(MiddleName, '') + ' ' + COALESCE(LastName, '')) AS Result  ")
            .Append("	FROM dbo.arStuEnrollments ")
            .Append("	JOIN dbo.arStudent ON arStudent.StudentId = arStuEnrollments.StudentId ")
            .Append("	JOIN dbo.arPrgVersions ON arPrgVersions.PrgVerId = arStuEnrollments.PrgVerId  ")
            .Append("	WHERE StuEnrollId = @StuEnrollId ")

        End With
        Dim command As SqlCommand = New SqlCommand(sb.ToString(), sqlconn)
        command.Parameters.AddWithValue("@StuEnrollId", stuEnrollId)
        sqlconn.Open()
        Try
            Dim result As String = command.ExecuteScalar()

            Return result
        Finally
            sqlconn.Close()
        End Try
    End Function

    Public Sub CreateTempStatusChangeHistoryTable()
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim command As SqlCommand = New SqlCommand("CSP_TempStatusChangeHistoryTable", sqlconn)
        command.CommandType = CommandType.StoredProcedure
        Try
            sqlconn.Open()
            Dim res = command.ExecuteNonQuery()
        Catch ex As Exception
            Throw New Exception(ex.ToString())
        Finally
            sqlconn.Close()
        End Try
    End Sub

    Public Function InsertInTempStatusChangeHistory(studentEnrollmentId As String, effectiveDate As DateTime, statusDescription As String, reason As String, requestedBy As String, changedBy As String, caseNo As String, dateOfChange As DateTime)
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connStr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim sqlconn As SqlConnection = New SqlConnection(connStr)
        Dim command As SqlCommand = New SqlCommand("ISP_TempStatusChangeHistoryTable", sqlconn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@StudentEnrollmentId", studentEnrollmentId)
        command.Parameters.AddWithValue("@EffectiveDate", effectiveDate)
        command.Parameters.AddWithValue("@StatusDescription", statusDescription)
        command.Parameters.AddWithValue("@Reason", reason)
        command.Parameters.AddWithValue("@RequestedBy", requestedBy)
        command.Parameters.AddWithValue("@ChangedBy", changedBy)
        command.Parameters.AddWithValue("@CaseNo", caseNo)
        command.Parameters.AddWithValue("@DateOfChange", dateOfChange)
        Try
            sqlconn.Open()
            Dim res = command.ExecuteNonQuery()
        Catch ex As Exception
            Throw New Exception(ex.ToString())
        Finally
            sqlconn.Close()
        End Try
    End Function
End Class


