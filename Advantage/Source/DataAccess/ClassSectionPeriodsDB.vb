Imports System.Data
Imports System.Data.OleDb
Imports System.Text
Imports System.Xml
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class ClassSectionPeriodsDB

    Public Function GetDropDownList(ByVal CampusId As String) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da1 As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder
        db.OpenConnection()
        'With strSQLString
        '    .Append("SELECT t1.TermId,t1.TermDescrip ")
        '    .Append("FROM arTerm t1  ")
        '    '.Append("WHERE t2.Status = 'Active' and t1.StatusId = t2.StatusId ")
        'End With

        'da1 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        'Try
        '    da1.Fill(ds, "TermDT")
        'Catch ex As System.Exception

        'End Try
        'strSQLString.Remove(0, strSQLString.Length)

        Dim da2 As New OleDbDataAdapter
        strSQLString.Remove(0, strSQLString.Length)

        With strSQLString
            .Append("Select ReqId,'(' + Code + ') ' + Descrip as  CodeAndDescrip from arReqs Where ReqTypeId = 1 ")
            .Append(" AND	CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
            .Append(" ORDER BY Descrip,Code")
        End With

        db.AddParameter("@cmpid2", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'db.OpenConnection()
        da2 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Try
            da2.Fill(ds, "CourseDT")
        Catch ex As System.Exception

        End Try

        'strSQLString.Remove(0, strSQLString.Length)

        Dim da4 As New OleDbDataAdapter

        With strSQLString
            .Append("Select WorkDaysID, WorkDaysDescrip from plWorkDays ORDER BY ViewOrder")
        End With
        da4 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Try
            da4.Fill(ds, "DayDT")
        Catch ex As System.Exception

        End Try

        strSQLString.Remove(0, strSQLString.Length)


        Dim da5 As New OleDbDataAdapter

        With strSQLString
            .Append("Select TimeIntervalID, TimeIntervalDescrip from cmTimeInterval order by TimeIntervalDescrip")
        End With
        da5 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Try
            da5.Fill(ds, "TimeIntervalList")
        Catch ex As System.Exception

        End Try

        strSQLString.Remove(0, strSQLString.Length)

        'Dim da6 As New OleDbDataAdapter

        'With strSQLString
        '    .Append("SELECT a.EmpId, a.LastName, a.FirstName ")
        '    .Append("FROM hrEmployees a ")
        '    .Append("WHERE a.Faculty = 1 ")
        'End With

        'da6 = db.RunParamSQLDataAdapter(strSQLString.ToString)

        'Try
        '    da6.Fill(ds, "EmployeeDT")
        'Catch ex As System.Exception

        'End Try

        'strSQLString.Remove(0, strSQLString.Length)

        'Dim da9 As New OleDbDataAdapter

        'With strSQLString
        '    .Append("SELECT a.ShiftId, a.ShiftDescrip ")
        '    .Append("FROM arShifts a ")
        'End With

        'da9 = db.RunParamSQLDataAdapter(strSQLString.ToString)

        'Try
        '    da9.Fill(ds, "ShiftDT")

        'Catch ex As System.Exception

        'End Try

        'strSQLString.Remove(0, strSQLString.Length)

        Dim da8 As New OleDbDataAdapter

        With strSQLString
            .Append("SELECT distinct a.UserId,a.FullName ")
            .Append("FROM syUsers a, syUsersRolesCampGrps b, syRoles c ")
            .Append("WHERE a.UserId = b.UserId ")
            .Append("AND b.RoleId = c.RoleId ")
            .Append("AND c.SysRoleId = 2 ")
            .Append("AND	CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
            .Append("ORDER BY a.FullName")
        End With

        db.AddParameter("@cmpid2", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        da8 = db.RunParamSQLDataAdapter(strSQLString.ToString)

        Try
            da8.Fill(ds, "InstructorDT")
        Catch ex As Exception

        End Try

        strSQLString.Remove(0, strSQLString.Length)
        db.ClearParameters()
        'Dim da7 As New OleDbDataAdapter

        'With strSQLString
        '    .Append("SELECT a.CampusId, a.CampDescrip ")
        '    .Append("FROM syCampuses a where a.CampusId = ? ")
        'End With
        'db.AddParameter("@cmpid", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'da7 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        'Try
        '    da7.Fill(ds, "CampusDT")
        'Catch ex As System.Exception

        'End Try
        'strSQLString.Remove(0, strSQLString.Length)

        Dim da10 As New OleDbDataAdapter

        With strSQLString
            .Append("SELECT StatusId,Status ")
            .Append("FROM syStatuses ")
        End With

        da10 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Try
            da10.Fill(ds, "StatusDT")
        Catch ex As Exception

        End Try
        strSQLString.Remove(0, strSQLString.Length)

        'Dim da11 As New OleDbDataAdapter

        'With strSQLString
        '    .Append("SELECT Descrip,GrdScaleId ")
        '    .Append("FROM arGradeScales ")
        'End With

        'da11 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        'Try
        '    da11.Fill(ds, "GrdScaleDT")
        'Catch ex As Exception

        'End Try
        'strSQLString.Remove(0, strSQLString.Length)

        Dim da19 As New OleDbDataAdapter

        With strSQLString
            .Append("SELECT P.PeriodId, P.PeriodDescrip, ")
            .Append(" (Case S.Status when 'Active' then 1 else 0 end) as Status ")
            .Append("FROM syPeriods P, syStatuses S ")
            .Append("WHERE ")
            .Append("       P.StatusId=S.StatusId ")
            '.Append("AND    S.Status = 'Active' ")
            .Append("AND    P.CampGrpId In (select CGC.CampGrpId from syCmpGrpCmps CGC, syCampGrps CG where CGC.CampusId = ? AND CGC.CampGrpId=CG.CampGrpId) ")
            .Append("ORDER BY   P.PeriodDescrip ")
        End With

        db.AddParameter("@cmpid2", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        da19 = db.RunParamSQLDataAdapter(strSQLString.ToString)

        Try
            da19.Fill(ds, "PeriodDT")

        Catch ex As System.Exception

        End Try

        strSQLString.Remove(0, strSQLString.Length)


        Dim da12 As New OleDbDataAdapter

        With strSQLString
            .Append("SELECT PrgVerId,PrgVerDescrip ")
            .Append("FROM arPrgVersions ")
            .Append("ORDER BY PrgVerDescrip ")
        End With

        da12 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Try
            da12.Fill(ds, "PrgVersionsDT")
        Catch ex As Exception

        End Try
        strSQLString.Remove(0, strSQLString.Length)


        db.CloseConnection()
        Return ds
    End Function
    Public Function GetDropDownList(ByVal CampusId As String, ByVal prgVerId As String) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da1 As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder

        With strSQLString
            .Append("SELECT t1.TermId,t1.TermDescrip ")
            .Append("FROM arTerm t1  ")
            '.Append("WHERE t2.Status = 'Active' and t1.StatusId = t2.StatusId ")
        End With
        db.OpenConnection()
        da1 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Try
            da1.Fill(ds, "TermDT")
        Catch ex As System.Exception

        End Try
        strSQLString.Remove(0, strSQLString.Length)


        Dim da2 As New OleDbDataAdapter
        With strSQLString
            If prgVerId = "" Then
                .Append("Select ReqId,Descrip,Code  from arReqs Where ReqTypeId = 1 ")
                .Append("ORDER BY Code,Descrip")

                da2 = db.RunParamSQLDataAdapter(strSQLString.ToString)
                Try
                    da2.Fill(ds, "CourseDT")
                Catch ex As System.Exception

                End Try

            Else
                'Special Case: retrieve only courses on the program version definition
                Dim dtCourses As DataTable = (New TermProgressDB).GetCoursesForProgramVersion(prgVerId)
                dtCourses.TableName = "CourseDT"
                ds.Tables.Add(dtCourses.Copy)
            End If
        End With
        strSQLString.Remove(0, strSQLString.Length)


        Dim da4 As New OleDbDataAdapter
        With strSQLString
            .Append("Select WorkDaysID, WorkDaysDescrip from plWorkDays ORDER BY ViewOrder")
        End With
        da4 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Try
            da4.Fill(ds, "DayDT")
        Catch ex As System.Exception

        End Try
        strSQLString.Remove(0, strSQLString.Length)


        Dim da5 As New OleDbDataAdapter
        With strSQLString
            .Append("Select TimeIntervalID, TimeIntervalDescrip from cmTimeInterval order by TimeIntervalDescrip")
        End With
        da5 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Try
            da5.Fill(ds, "TimeIntervalList")
        Catch ex As System.Exception

        End Try
        strSQLString.Remove(0, strSQLString.Length)


        Dim da6 As New OleDbDataAdapter
        With strSQLString
            .Append("SELECT a.EmpId, a.LastName, a.FirstName ")
            .Append("FROM hrEmployees a ")
            .Append("WHERE a.Faculty = 1 ")
        End With
        da6 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Try
            da6.Fill(ds, "EmployeeDT")
        Catch ex As System.Exception

        End Try
        strSQLString.Remove(0, strSQLString.Length)


        Dim da9 As New OleDbDataAdapter
        With strSQLString
            .Append("SELECT a.ShiftId, a.ShiftDescrip ")
            .Append("FROM arShifts a ")
        End With
        da9 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Try
            da9.Fill(ds, "ShiftDT")
        Catch ex As System.Exception

        End Try
        strSQLString.Remove(0, strSQLString.Length)


        Dim da8 As New OleDbDataAdapter
        With strSQLString
            .Append("SELECT distinct a.UserId,a.FullName ")
            .Append("FROM syUsers a, syUsersRolesCampGrps b, syRoles c ")
            .Append("WHERE a.UserId = b.UserId ")
            .Append("AND b.RoleId = c.RoleId ")
            .Append("AND c.SysRoleId = 2 ")
            .Append("AND	CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
            .Append("ORDER BY a.FullName")
        End With
        db.AddParameter("@cmpid2", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        da8 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Try
            da8.Fill(ds, "InstructorDT")
        Catch ex As Exception

        End Try
        strSQLString.Remove(0, strSQLString.Length)
        db.ClearParameters()


        Dim da7 As New OleDbDataAdapter
        With strSQLString
            .Append("SELECT a.CampusId, a.CampDescrip ")
            .Append("FROM syCampuses a where a.CampusId = ? ")
        End With
        db.AddParameter("@cmpid", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        da7 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Try
            da7.Fill(ds, "CampusDT")
        Catch ex As System.Exception

        End Try
        strSQLString.Remove(0, strSQLString.Length)
        db.ClearParameters()

        Dim da10 As New OleDbDataAdapter
        With strSQLString
            .Append("SELECT StatusId,Status ")
            .Append("FROM syStatuses ")
        End With
        da10 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Try
            da10.Fill(ds, "StatusDT")
        Catch ex As Exception

        End Try
        strSQLString.Remove(0, strSQLString.Length)


        Dim da11 As New OleDbDataAdapter
        With strSQLString
            .Append("SELECT Descrip,GrdScaleId ")
            .Append("FROM arGradeScales ")
        End With
        da11 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Try
            da11.Fill(ds, "GrdScaleDT")
        Catch ex As Exception

        End Try
        strSQLString.Remove(0, strSQLString.Length)


        Dim da12 As New OleDbDataAdapter
        With strSQLString
            If prgVerId = "" Then
                .Append("SELECT PrgVerId,PrgVerDescrip ")
                .Append("FROM arPrgVersions ")
                .Append("ORDER BY PrgVerDescrip ")
            Else
                'Special case: Retrieve only a particular program version
                .Append("SELECT PrgVerId,PrgVerDescrip ")
                .Append("FROM arPrgVersions ")
                .Append("WHERE PrgVerId=?")

                db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
        End With
        da12 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Try
            da12.Fill(ds, "PrgVersionsDT")
        Catch ex As Exception

        End Try
        strSQLString.Remove(0, strSQLString.Length)
        db.ClearParameters()

        db.CloseConnection()

        Return ds
    End Function
    Public Function GetClsSectDataList(ByVal Term As String, ByVal Course As String, ByVal Instructor As String, ByVal Shift As String, ByVal CampusId As String) As OleDbDataAdapter
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        Dim objClsSect As New OleDbDataAdapter
        Dim ds As New DataSet
        Dim count As Integer


        With strSQL
            .Append("Select a.ClsSectionId, a.TermId, a.ReqId, a.InstructorId,a.ShiftId, a.ClsSection, a.StartDate,a.EndDate, a.MaxStud, b.TermDescrip,c.Descrip,c.Code from arClassSections a, arTerm b, arReqs c ")
            .Append(" where (a.TermId = b.TermId) ")
            .Append(" and (a.ReqId = c.ReqId) ")
            .Append(" and (a.CampusId = ?) ")
            If Term.ToString <> "" Then
                .Append(" and (a.TermId = ?) ")
            End If
            If Course.ToString <> "" Then
                .Append(" and (a.ReqId = ?) ")
            End If
            If Instructor.ToString <> "" Then
                .Append(" and (a.InstructorId = ?) ")
            End If
            If Shift.ToString <> "" Then
                .Append(" and (a.ShiftId = ?) ")
            End If
            '.Append("and a.EndDate > ? ")
        End With
        db.AddParameter("@cmpid", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        If Term.ToString <> "" Then
            Dim TermGuid As Guid = XmlConvert.ToGuid(Term)
            db.AddParameter("@Term", TermGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        If Course.ToString <> "" Then
            Dim CourseGuid As Guid = XmlConvert.ToGuid(Course)

            db.AddParameter("@Course", CourseGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        If Instructor.ToString <> "" Then
            Dim InstructGuid As Guid = XmlConvert.ToGuid(Instructor)

            db.AddParameter("@Instructor", InstructGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        If Shift.ToString <> "" Then
            Dim ShiftGuid As Guid = XmlConvert.ToGuid(Shift)

            db.AddParameter("@Shift", ShiftGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        'db.AddParameter("@Sdate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        db.OpenConnection()
        Try
            objClsSect = db.RunParamSQLDataAdapter(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New System.Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        db.ClearParameters()
        Return objClsSect
    End Function
    Public Function GetCampusRooms(ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT a.BldgId, b.RoomId, b.Capacity,c.CampusId, b.Descrip ")
                .Append("FROM arBuildings a,arRooms b, syCmpGrpCmps c ")
                .Append("WHERE a.BldgId = b.BldgId ")
                .Append("AND a.CampGrpId = c.CampGrpId ")
                .Append("AND(c.CampusId = ?) order by b.Descrip ")

            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@CampId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "RoomDT")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception

        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function PopulateClsSectDataList(ByVal Term As String, ByVal Course As String, ByVal Instructor As String, ByVal Shift As String, ByVal CampusId As String, Optional ByVal dataset As DataSet = Nothing) As DataSet
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Try
            da = GetClsSectDataList(Term, Course, Instructor, Shift, CampusId)
        Catch ex As System.Exception
            Throw New System.Exception(ex.Message)
        End Try
        If dataset Is Nothing Then
            da.Fill(ds, "ClassSection")
            Return ds
        Else
            da.Fill(dataset, "ClassSection")
            Return dataset
        End If
        Return ds
    End Function

    Public Function PopulateClsSectDataList_SP(ByVal Term As String, ByVal Course As String, ByVal Instructor As String, ByVal Shift As String, _
                                               ByVal CampusId As String, Optional ByVal StartDate As Nullable(Of DateTime) = Nothing, _
                                               Optional ByVal EndDate As Nullable(Of DateTime) = Nothing, _
                                               Optional ByVal dataset As DataSet = Nothing) As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@campusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@TermId", Term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ReqId", Course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@InstrId", Instructor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ShiftId", Shift, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If StartDate Is Nothing Then
            db.AddParameter("@StartDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        Else
            db.AddParameter("@StartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If
        If EndDate Is Nothing Then
            db.AddParameter("@EndDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        Else
            db.AddParameter("@EndDate", EndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If


        Return db.RunParamSQLDataSet("dbo.usp_GetClassSections", Nothing, "SP").Tables(0)
    End Function

    Public Function GetTermModuleSummary(ByVal status As String, ByVal campusId As String) As DataSet
        Dim ds As DataSet
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder

        With strSQL
            .Append("SELECT t1.TermId,t1.StartDate,t3.Descrip,t2.ClsSection ")
            .Append("FROM arTerm t1, arClassSections t2, arReqs t3 ")
            .Append("WHERE t1.TermId = t2.TermId ")
            .Append("AND t2.ReqId = t3.ReqId ")
            .Append("AND t1.IsModule = 1 ")
            .Append("AND t2.CampusId = ? ")
        End With

        If status <> "" Then
            strSQL.Append("AND t1.StatusId = ? ")
        End If

        db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        If status <> "" Then
            db.AddParameter("@status", status, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        ds = db.RunParamSQLDataSet(strSQL.ToString)

        Return ds

    End Function

    Public Function GetTermModuleSummary(ByVal prgVerId As String, ByVal status As String, ByVal campusId As String) As DataSet
        Dim ds As DataSet
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder

        With strSQL
            .Append("SELECT t1.TermId,t1.StartDate,t3.Descrip,t2.ClsSection ")
            .Append("FROM arTerm t1, arClassSections t2, arReqs t3 ")
            .Append("WHERE t1.TermId = t2.TermId ")
            .Append("AND t2.ReqId = t3.ReqId ")
            .Append("AND t1.IsModule = 1 ")
            .Append("AND t2.CampusId = ? ")
            .Append("AND t2.ReqId IN( ")
            .Append("SELECT distinct t400.ReqId as ReqId ")
            .Append("FROM arReqs t3,arProgVerDef t400 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t400.PrgVerId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t700.ReqId as ReqId ")
            .Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t400.PrgVerId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t800.ReqId as ReqId ")
            .Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t400.PrgVerId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t900.ReqId as ReqId ")
            .Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append(" t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t400.PrgVerId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t1000.ReqId as ReqId ")
            .Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t400.PrgVerId = ? ")
            .Append("UNION ")
            .Append("SELECT distinct t1100.ReqId as ReqId ")
            .Append("FROM arReqs t3,arProgVerDef t400, arReqGrpDef t700, arReqGrpDef t800, arReqGrpDef t900, arReqGrpDef t1000, arReqGrpDef t1100 ")
            .Append("WHERE t3.ReqId = t400.ReqId AND ")
            .Append("t3.ReqTypeId = 2 AND ")
            .Append("t400.ReqId = t700.GrpId AND ")
            .Append("t700.ReqId = t800.GrpId AND ")
            .Append("t800.ReqId = t900.GrpId AND ")
            .Append("t900.ReqId = t1000.GrpId AND ")
            .Append("t1000.ReqId = t1100.GrpId AND ")
            .Append("t400.PrgVerId = ? ")
            .Append(") ")
        End With

        If status <> "" Then
            strSQL.Append("AND t1.StatusId = ? ")
        End If

        db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        If status <> "" Then
            db.AddParameter("@status", status, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        ds = db.RunParamSQLDataSet(strSQL.ToString)

        Return ds
    End Function

    Public Function AddClassSection(ByVal ClassSectionObject As ClassSectionInfo, ByVal user As String) As ClsSectionResultInfo
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        Dim count As Integer
        Dim resultInfo As New ClsSectionResultInfo

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   Validate uniqueness of class section.
        '   Get the number of class section with same values in these fields: ClsSection,TermId,ReqId and CampusId
        count = DoesClsSectionExist(ClassSectionObject, ClassSectionObject.TermId.ToString)

        If count >= 1 Then
            resultInfo.Exists = True
            Return resultInfo
            'Exit Function
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try
            With strSQL
                .Append("INSERT INTO arClassSections ")
                .Append("(ClsSectionId, TermId, ReqId, InstructorId,ShiftId,CampusId, GrdScaleId, ClsSection, StartDate, EndDate, StudentStartDate,CohortStartDate,MaxStud,ModUser,ModDate,LeadGrpId) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);")
                .Append("SELECT COUNT(*) FROM arClassSections WHERE ModDate=? ")
            End With


            ' Set the DateCreated time to now
            db.AddParameter("@clsSectid", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.AddParameter("@termid", ClassSectionObject.TermId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.AddParameter("@courseid", ClassSectionObject.CourseId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            If ClassSectionObject.InstructorId2.ToString = Guid.Empty.ToString Or ClassSectionObject.InstructorId2.ToString = "" Then
                db.AddParameter("@empid", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@empid", ClassSectionObject.InstructorId2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If ClassSectionObject.ShiftId2.ToString = Guid.Empty.ToString Or ClassSectionObject.ShiftId2.ToString = "" Then
                db.AddParameter("@shiftid", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@shiftid", ClassSectionObject.ShiftId2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If ClassSectionObject.CampusId.ToString = Guid.Empty.ToString Or ClassSectionObject.CampusId.ToString = "" Then
                db.AddParameter("@campusid", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@campusid", ClassSectionObject.CampusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            End If
            db.AddParameter("@grdscaleid", ClassSectionObject.GrdScaleId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            db.AddParameter("@section", ClassSectionObject.Section, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@startdate", ClassSectionObject.StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            db.AddParameter("@enddate", ClassSectionObject.EndDate, DataAccess.OleDbDataType.OleDbDateTime, 16, ParameterDirection.Input)
            ''Added by Saraswathi lakshmanan to insert null into the table if the value is minValue
            '19746: QA: Issue with the student group and scheduling on run schedules page. 
            ''Modified on Sept 17 2010
            If ClassSectionObject.StudentStartDate = Date.MinValue Then
                db.AddParameter("@StdStart", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, 16, ParameterDirection.Input)
            Else
                db.AddParameter("@StdStart", ClassSectionObject.StudentStartDate, DataAccess.OleDbDataType.OleDbDateTime, 16, ParameterDirection.Input)
            End If

            ''Added by Saraswathi on jan 22 2008
            ''StudentStartDate is saved as CohortStartDate
            db.AddParameter("@CohortStartDate", ClassSectionObject.StudentStartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

            db.AddParameter("@maxstud", ClassSectionObject.MaxStud, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)

            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Dim now As Date = Convert.ToDateTime(Date.Now.ToShortDateString)
            Dim strnow As Date
            strnow = Date.Now
            Dim sDate As String = strnow.Date.ToShortDateString
            Dim sHour As String = strnow.Hour.ToString
            Dim sMinute As String = strnow.Minute.ToString
            Dim iSecs As Integer = strnow.Second
            strnow = Convert.ToDateTime(sDate + " " + sHour + ":" + sMinute + ":" + iSecs.ToString)

            'ModDate
            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'lead Group Id
            If ClassSectionObject.LeadGrpId.ToString = Guid.Empty.ToString Or ClassSectionObject.LeadGrpId.ToString = "" Then
                db.AddParameter("@LeadGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@LeadGrpId", ClassSectionObject.LeadGrpId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            End If

            'ModDate
            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(strSQL.ToString, groupTrans)
            'db.RunParamSQLExecuteNoneQuery(strSQL.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If Not (rowCount = 1) Then

                '   rollback transaction
                groupTrans.Rollback()

                '   return an error message
                resultInfo.ErrorString = DALExceptions.BuildConcurrencyExceptionMessage()
                Return resultInfo
            End If

            db.ClearParameters()
            strSQL.Remove(0, strSQL.Length)

            With strSQL
                .Append("INSERT INTO arClassSectionTerms(ClsSectTermId,ClsSectionId,TermId,ModDate,ModUser) ")
                .Append("VALUES(?,?,?,?,?);")
                .Append("SELECT COUNT(*) FROM arClassSectionTerms WHERE ModDate=? ")
            End With

            'ClsSectTermId
            db.AddParameter("@ClsSectTermId", System.Guid.NewGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            'ClsSectionId
            db.AddParameter("@ClsSectionId", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            'TermId
            db.AddParameter("@TermId", ClassSectionObject.TermId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            'ModDate
            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'ModDate
            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            rowCount = db.RunParamSQLScalar(strSQL.ToString, groupTrans)

            '   If there were no updated rows then there was a concurrency problem
            If Not (rowCount = 1) Then

                '   rollback transaction
                groupTrans.Rollback()

                '   return an error message
                resultInfo.ErrorString = DALExceptions.BuildConcurrencyExceptionMessage()
                Return resultInfo
            End If


            ' 09/08/2011 JRobinson Clock Hour Project - Add new insert for arClsSectionTimeClockPolicy table

            db.ClearParameters()
            strSQL.Remove(0, strSQL.Length)

            With strSQL
                .Append("INSERT INTO arClsSectionTimeClockPolicy(ClsSectionPolicyId,ClsSectionId,AllowEarlyIn,AllowLateOut,AllowExtraHours,CheckTardyIn,MaxInBeforeTardy,AssignTardyInTime) ")
                .Append("VALUES(?,?,?,?,?,?,?,?);")
                '.Append("SELECT COUNT(*) FROM arClassSectionTerms WHERE ModDate=? ")
            End With

            'ClsSectionPolicyId
            db.AddParameter("@ClsSectionPolicyId", System.Guid.NewGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            'ClsSectionId
            db.AddParameter("@ClsSectionId", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            'AllowEarlyIn
            db.AddParameter("@AllowEarlyIn", ClassSectionObject.AllowEarlyIn, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'AllowLateOut
            db.AddParameter("@AllowLateOut", ClassSectionObject.AllowLateOut, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'AllowExtraHours
            db.AddParameter("@AllowExtraHours", ClassSectionObject.AllowExtraHours, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'CheckTardyIn
            db.AddParameter("@CheckTardyIn", ClassSectionObject.CheckTardyIn, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            'MaxInBeforeTardy
            db.AddParameter("@MaxInBeforeTardy", ClassSectionObject.MaxInBeforeTardy, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'AssignTardyInTime
            db.AddParameter("@AssignTardyInTime", ClassSectionObject.AssignTardyInTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)


            db.RunParamSQLExecuteNoneQuery(strSQL.ToString, groupTrans)



            groupTrans.Commit()

            resultInfo.ModDate = strnow
            resultInfo.NotExists = True

            Return resultInfo

        Catch ex As OleDbException

            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return an error to the client
            resultInfo.ErrorString = DALExceptions.BuildErrorMessage(ex)
            Return resultInfo

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function UpdateClassSection(ByVal ClassSectionObject As ClassSectionInfo, ByVal user As String) As ClsSectionResultInfo
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        Dim count As Integer
        Dim resultInfo As New ClsSectionResultInfo
        Dim strnow As Date

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try
            With strSQL
                .Append("UPDATE arClassSections SET ")
                .Append("TermId = ?")
                .Append(",ReqId = ?")
                .Append(",InstructorId = ?")
                .Append(",ShiftId = ?")
                .Append(",CampusId = ?")
                .Append(",GrdScaleId = ?")
                .Append(",ClsSection = ?")
                .Append(",StartDate = ?")
                .Append(",EndDate = ?")
                .Append(",StudentStartDate =?")
                ''CohortStartDate added by Saraswathi on jan 22 2009
                ''The StudentStartdate is saved as the cohortStart date
                .Append(",CohortStartDate = ?")


                .Append(",MaxStud = ?")
                .Append(",ModUser=? ")
                .Append(",ModDate=? ")
                .Append(",LeadGrpId = ? ")
                .Append(",InstrGrdBkWgtId=? ")
                .Append(" WHERE ClsSectionId = ? ")
                '.Append(" AND DATEDIFF(SECOND,ModDate, ?) > 2;")
                '.Append("Select count(*) from arClassSections where ModDate = ? ")
            End With


            db.AddParameter("@termid", ClassSectionObject.TermId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.AddParameter("@courseid", ClassSectionObject.CourseId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            If ClassSectionObject.InstructorId2.ToString = Guid.Empty.ToString Or ClassSectionObject.InstructorId2.ToString = "" Then
                db.AddParameter("@empid", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@empid", ClassSectionObject.InstructorId2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If ClassSectionObject.ShiftId2.ToString = Guid.Empty.ToString Or ClassSectionObject.ShiftId2.ToString = "" Then
                db.AddParameter("@shiftid", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@shiftid", ClassSectionObject.ShiftId2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            If ClassSectionObject.CampusId.ToString = Guid.Empty.ToString Or ClassSectionObject.CampusId.ToString = "" Then
                db.AddParameter("@campid", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@campusid", ClassSectionObject.CampusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            End If
            db.AddParameter("@grdscaleid", ClassSectionObject.GrdScaleId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            db.AddParameter("@section", ClassSectionObject.Section, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@startdate", ClassSectionObject.StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            db.AddParameter("@enddate", ClassSectionObject.EndDate, DataAccess.OleDbDataType.OleDbDateTime, 16, ParameterDirection.Input)
            ''Added by Saraswathi lakshmanan to insert null into the table if the value is minValue
            '19746: QA: Issue with the student group and scheduling on run schedules page. 
            ''Modified on Sept 17 2010
            If ClassSectionObject.StudentStartDate = Date.MinValue Then
                db.AddParameter("@StdStart", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, 16, ParameterDirection.Input)
            Else
                db.AddParameter("@StdStart", ClassSectionObject.StudentStartDate, DataAccess.OleDbDataType.OleDbDateTime, 16, ParameterDirection.Input)
            End If

            ''CohortStartDate added by Saraswathi on jan 22 2009
            ''The StudentStartdate is saved as the cohortStart date
            db.AddParameter("@CohortStartDate", ClassSectionObject.StudentStartDate, DataAccess.OleDbDataType.OleDbDateTime, 16, ParameterDirection.Input)


            db.AddParameter("@maxstud", ClassSectionObject.MaxStud, DataAccess.OleDbDataType.OleDbInteger, 16, ParameterDirection.Input)
            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            'Troy: I commented out the code below on 8/15/2005. I think all we really need to do is drop the 
            'milliseconds from the date since that is what is causing the problem in the first place.
            strnow = DateTime.Now 'Utilities.GetAdvantageDBDateTime(Date.Now)


            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            If ClassSectionObject.LeadGrpId.ToString = Guid.Empty.ToString Or ClassSectionObject.LeadGrpId.ToString = "" Then
                db.AddParameter("@LeadGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@LeadGrpId", ClassSectionObject.LeadGrpId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            End If

            If ClassSectionObject.InstrGrdBkWgtId.ToString = Guid.Empty.ToString Or ClassSectionObject.InstrGrdBkWgtId.ToString = "" Then
                db.AddParameter("@InstrGrdBkWgtId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@InstrGrdBkWgtId", ClassSectionObject.InstrGrdBkWgtId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)
            End If
            db.AddParameter("@clsSectid", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            ''   ModDate
            'db.AddParameter("@Original_ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input) 'ClassSectionObject.ModDate

            ''   ModDate
            'db.AddParameter("@Updated_ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(strSQL.ToString, groupTrans)

            ''   If there were no updated rows then there was a concurrency problem
            'If Not (rowCount = 1) Then

            '    groupTrans.Rollback()

            '    'Return DALExceptions.BuildConcurrencyExceptionMessage()
            '    resultInfo.ErrorString = DALExceptions.BuildConcurrencyExceptionMessage()
            '    Return resultInfo
            'End If

            db.ClearParameters()
            strSQL.Remove(0, strSQL.Length)

            With strSQL
                .Append("UPDATE     arClassSectionTerms ")
                .Append("SET        TermId=?,ModDate=?,ModUser=? ")
                .Append("WHERE      ClsSectionId=? AND TermId=? ;") ' 8/14/2013  AND DATEDIFF(SECOND,ModDate, ?) > 2  Update ModDate check to use a diff of two seconds
                '.Append("SELECT COUNT(*) FROM arClassSectionTerms WHERE ModDate = ?")
            End With

            '   TermId
            db.AddParameter("@TermId", ClassSectionObject.TermId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            '   ModDate
            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '   ClsSectionId
            db.AddParameter("@ClsSectionId", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            '   Original_TermId
            db.AddParameter("@Original_TermId", ClassSectionObject.OriginalTermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            ''   Original_ModDate
            'db.AddParameter("@Original_ModDate", ClassSectionObject.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            ''   ModDate
            'db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            rowCount = db.RunParamSQLScalar(strSQL.ToString, groupTrans)

            ''   If there were no updated rows then there was a concurrency problem
            'If Not (rowCount = 1) Then

            '    groupTrans.Rollback()

            '    'Return DALExceptions.BuildConcurrencyExceptionMessage()
            '    resultInfo.ErrorString = DALExceptions.BuildConcurrencyExceptionMessage()
            '    Return resultInfo
            'End If

            ' 09/12/2011 JRobinson Clock Hour Project - Add new insert/update for arClsSectionTimeClockPolicy table


            db.ClearParameters()
            strSQL.Remove(0, strSQL.Length)

            With strSQL
                .Append("SELECT COUNT(*) FROM  arClsSectionTimeClockPolicy ")
                .Append("WHERE  ClsSectionId=? ")

            End With

            'ClsSectionId
            db.AddParameter("@ClsSectionId", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            rowCount = db.RunParamSQLScalar(strSQL.ToString, groupTrans)

            db.ClearParameters()
            strSQL.Remove(0, strSQL.Length)

            If rowCount = 0 Then
                'insert
                With strSQL
                    .Append("INSERT INTO arClsSectionTimeClockPolicy(ClsSectionPolicyId,ClsSectionId,AllowEarlyIn,AllowLateOut,AllowExtraHours,CheckTardyIn,MaxInBeforeTardy,AssignTardyInTime) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?);")
                    '.Append("SELECT COUNT(*) FROM arClassSectionTerms WHERE ModDate=? ")
                End With

                'ClsSectionPolicyId
                db.AddParameter("@ClsSectionPolicyId", System.Guid.NewGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                'ClsSectionId
                db.AddParameter("@ClsSectionId", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                'AllowEarlyIn
                db.AddParameter("@AllowEarlyIn", ClassSectionObject.AllowEarlyIn, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                'AllowLateOut
                db.AddParameter("@AllowLateOut", ClassSectionObject.AllowLateOut, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                'AllowExtraHours
                db.AddParameter("@AllowExtraHours", ClassSectionObject.AllowExtraHours, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                'CheckTardyIn
                db.AddParameter("@CheckTardyIn", ClassSectionObject.CheckTardyIn, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                'MaxInBeforeTardy
                db.AddParameter("@MaxInBeforeTardy", ClassSectionObject.MaxInBeforeTardy, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                'AssignTardyInTime
                db.AddParameter("@AssignTardyInTime", ClassSectionObject.AssignTardyInTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)


                db.RunParamSQLExecuteNoneQuery(strSQL.ToString, groupTrans)

            Else
                ' update
                With strSQL
                    .Append("UPDATE     arClsSectionTimeClockPolicy ")
                    .Append("SET        ClsSectionId=?,AllowEarlyIn=?,AllowLateOut=?,AllowExtraHours=?,CheckTardyIn=?,MaxInBeforeTardy=?,AssignTardyInTime=? ")
                    .Append("WHERE      ClsSectionId=? ;")

                End With

                'ClsSectionId
                db.AddParameter("@ClsSectionId", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                'AllowEarlyIn
                db.AddParameter("@AllowEarlyIn", ClassSectionObject.AllowEarlyIn, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                'AllowLateOut
                db.AddParameter("@AllowLateOut", ClassSectionObject.AllowLateOut, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                'AllowExtraHours
                db.AddParameter("@AllowExtraHours", ClassSectionObject.AllowExtraHours, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                'CheckTardyIn
                db.AddParameter("@CheckTardyIn", ClassSectionObject.CheckTardyIn, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                'MaxInBeforeTardy
                db.AddParameter("@MaxInBeforeTardy", ClassSectionObject.MaxInBeforeTardy, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                'AssignTardyInTime
                db.AddParameter("@AssignTardyInTime", ClassSectionObject.AssignTardyInTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                'ClsSectionId
                db.AddParameter("@ClsSectionId_1", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)


                db.RunParamSQLExecuteNoneQuery(strSQL.ToString, groupTrans)

            End If



            '   return without errors

            groupTrans.Commit()

            resultInfo.ModDate = strnow

            Return resultInfo

        Catch ex As OleDbException

            groupTrans.Rollback()

            '   return an error to the client
            resultInfo.ErrorString = DALExceptions.BuildErrorMessage(ex)
            Return resultInfo

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function DeleteClassSection(ByVal ClsSectID As Guid, ByVal modDate As DateTime) As String
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        Dim rowCount As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()


        Try

            ' 09/12/2011 JRobinson Clock Hour Project - Add new delete for arClsSectionTimeClockPolicy table

            With strSQL
                .Append("SELECT COUNT(*) FROM arClsSectionTimeClockPolicy WHERE ClsSectionId=?")
            End With

            '   ClsSectionId
            db.AddParameter("@ClsSectionId", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            '   execute the query
            rowCount = db.RunParamSQLScalar(strSQL.ToString, groupTrans)

            If rowCount = 1 Then

                db.ClearParameters()
                strSQL.Remove(0, strSQL.Length)

                With strSQL
                    .Append("DELETE FROM arClsSectionTimeClockPolicy ")
                    .Append("WHERE ClsSectionId=?;")
                    .Append("SELECT COUNT(*) FROM arClsSectionTimeClockPolicy WHERE ClsSectionId=?")
                End With


                '   ClsSectionId
                db.AddParameter("@ClsSectionId", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                '   ClsSectionId
                db.AddParameter("@ClsSectionId_1", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

                '   execute the query
                rowCount = db.RunParamSQLScalar(strSQL.ToString, groupTrans)

                '   If the row was not deleted then there was a concurrency problem
                If Not (rowCount = 0) Then
                    groupTrans.Rollback()

                    Return DALExceptions.BuildConcurrencyExceptionMessage()
                End If
            End If

            db.ClearParameters()
            strSQL.Remove(0, strSQL.Length)

            With strSQL
                .Append("DELETE FROM arClassSectionTerms ")
                .Append("WHERE ClsSectionId=? AND TermId=?;")       'ModDate=?
                .Append("SELECT COUNT(*) FROM arClassSectionTerms WHERE ClsSectionId=?")
            End With

            '   ClsSectionId
            db.AddParameter("@ClsSectionId", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            '   ClsSectionId
            db.AddParameter("@ClsSectionId_1", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            '   execute the query
            rowCount = db.RunParamSQLScalar(strSQL.ToString, groupTrans)

            '   If the row was not deleted then there was a concurrency problem
            If Not (rowCount = 0) Then
                groupTrans.Rollback()

                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

            db.ClearParameters()
            strSQL.Remove(0, strSQL.Length)

            With strSQL
                .Append("DELETE FROM arClassSections WHERE ClsSectionId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM arClassSections WHERE ClsSectionId = ? ")
            End With
            db.AddParameter("@clssectionid", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@clssectionid2", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            '   execute the query
            rowCount = db.RunParamSQLScalar(strSQL.ToString, groupTrans)

            '   If the row was not deleted then there was a concurrency problem
            If Not (rowCount = 0) Then
                groupTrans.Rollback()

                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

            groupTrans.Commit()

            Return ""

        Catch ex As OleDbException
            groupTrans.Rollback()
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
        'db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        'db.ClearParameters()
        'strSQL.Remove(0, strSQL.Length)
    End Function

    Public Function DeleteClassSection(ByVal ClsSectID As Guid, ByVal modDate As DateTime, ByVal termId As String) As String
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        Dim rowCount As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            ' 09/12/2011 JRobinson Clock Hour Project - Add new delete for arClsSectionTimeClockPolicy table

            With strSQL
                .Append("SELECT COUNT(*) FROM arClsSectionTimeClockPolicy WHERE ClsSectionId=?")
            End With

            '   ClsSectionId
            db.AddParameter("@ClsSectionId", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            '   execute the query
            rowCount = db.RunParamSQLScalar(strSQL.ToString, groupTrans)

            If rowCount = 1 Then

                db.ClearParameters()
                strSQL.Remove(0, strSQL.Length)

                With strSQL
                    .Append("DELETE FROM arClsSectionTimeClockPolicy ")
                    .Append("WHERE ClsSectionId=?;")
                    .Append("SELECT COUNT(*) FROM arClsSectionTimeClockPolicy WHERE ClsSectionId=?")
                End With


                '   ClsSectionId
                db.AddParameter("@ClsSectionId", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                '   ClsSectionId
                db.AddParameter("@ClsSectionId_1", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

                '   execute the query
                rowCount = db.RunParamSQLScalar(strSQL.ToString, groupTrans)

                '   If the row was not deleted then there was a concurrency problem
                If Not (rowCount = 0) Then
                    groupTrans.Rollback()

                    Return DALExceptions.BuildConcurrencyExceptionMessage()
                End If
            End If

            db.ClearParameters()
            strSQL.Remove(0, strSQL.Length)

            With strSQL
                .Append("DELETE FROM arClassSectionTerms ")
                .Append("WHERE ClsSectionId=? AND TermId=?;")       ' AND ModDate=?
                .Append("SELECT COUNT(*) FROM arClassSectionTerms WHERE ClsSectionId=?")
            End With

            '   ClsSectionId
            db.AddParameter("@ClsSectionId", ClsSectID.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   TermId
            db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   ClsSectionId
            db.AddParameter("@ClsSectionId1", ClsSectID.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   execute the query
            rowCount = db.RunParamSQLScalar(strSQL.ToString, groupTrans)

            '   If the row was not deleted then there was a concurrency problem
            If Not (rowCount = 0) Then
                groupTrans.Rollback()

                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

            db.ClearParameters()
            strSQL.Remove(0, strSQL.Length)

            With strSQL
                .Append("DELETE FROM arClassSections ")
                .Append("WHERE ClsSectionId = ? AND ModDate = ?;")
                .Append("SELECT count(*) FROM arClassSections WHERE ClsSectionId = ? ")
            End With

            '   ClsSectionId
            db.AddParameter("@ClsSectionId", ClsSectID.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            '   ClsSectionId
            db.AddParameter("@ClsSectionId1", ClsSectID.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   execute the query
            rowCount = db.RunParamSQLScalar(strSQL.ToString, groupTrans)

            '   If the row was not deleted then there was a concurrency problem
            If Not (rowCount = 0) Then
                groupTrans.Rollback()

                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

            groupTrans.Commit()

            Return ""

        Catch ex As OleDbException
            groupTrans.Rollback()
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
        'db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        'db.ClearParameters()
        'strSQL.Remove(0, strSQL.Length)
    End Function


    Public Function AddOverridenConflicts(ByVal clsSectionId As String, ByVal clsSectionList As ArrayList, ByVal arrDeleteList As ArrayList, ByVal user As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try
            '   Delete entries in arOverridenConflicts
            With sb
                .Append("DELETE FROM arOverridenConflicts ")
                .Append("WHERE LeftClsSectionId=? OR RightClsSectionId=? ")
            End With

            db.AddParameter("@LeftClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@RightClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            db.ClearParameters()

            If arrDeleteList.Count > 0 Then
                For i As Integer = 0 To arrDeleteList.Count - 1
                    db.AddParameter("@LeftClsSectionId", arrDeleteList(i).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@RightClsSectionId", arrDeleteList(i).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                    db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

                    db.ClearParameters()
                Next
            End If

            sb.Remove(0, sb.Length)

            '   Add entries in arOverridenConflicts
            If clsSectionList.Count > 0 Then

                With sb
                    .Append("INSERT INTO ")
                    .Append("   arOverridenConflicts(OverridenConflictId,LeftClsSectionId,RightClsSectionId,ModDate,ModUser) ")
                    .Append("VALUES(?,?,?,?,?)")
                End With

                Dim conflictInfo As OverridenConflictInfo
                Dim strnow As Date
                strnow = Date.Now

                Dim sDate As String = strnow.Date.ToShortDateString
                Dim sHour As String = strnow.Hour.ToString
                Dim sMinute As String = strnow.Minute.ToString
                Dim iSecs As Integer = strnow.Second
                strnow = Convert.ToDateTime(sDate + " " + sHour + ":" + sMinute + ":" + iSecs.ToString)

                For i As Integer = 0 To clsSectionList.Count - 1
                    conflictInfo = clsSectionList(i)

                    db.AddParameter("@OverridenConflictId", conflictInfo.OverridenConflictId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@LeftClsSectionId", conflictInfo.LeftClsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@RightClsSectionId", conflictInfo.RightClsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                    db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                    db.ClearParameters()
                Next

            End If

            groupTrans.Commit()

            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        End Try


    End Function

    Public Sub DeleteTerm(ByVal termId As String)
        Dim msg As String
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder

        With strSQL
            .Append("DELETE FROM arTerm ")
            .Append("WHERE TermId = ? ")
        End With

        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(strSQL.ToString)

    End Sub

    Public Function AddSingleClsSectMeeting(ByVal ClsSectMeeting As ClsSectMeetingInfo, ByVal user As String) As ClsSectMtgIdObjInfo
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        Dim resinfo As New ClsSectMtgIdObjInfo

        ' 09/12/2011 JRobinson Clock Hour Project - Add new fields 

        Try
            With strSQL
                .Append("INSERT INTO arClsSectMeetings ")
                .Append("(ClsSectMeetingId, ClsSectionId, PeriodId, AltPeriodId, RoomId, StartDate, EndDate,ModUser,ModDate,InstructionTypeID,BreakDuration) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?)")
            End With
            ' Set the DateCreated time to now
            db.AddParameter("@clsSectid", ClsSectMeeting.ClsSectMtgId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.AddParameter("@clsSectid", ClsSectMeeting.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.AddParameter("@Period", ClsSectMeeting.PeriodId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            'db.AddParameter("@AltPeriod", ClsSectMeeting.AltPeriodId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            If ClsSectMeeting.AltPeriodId.ToString = "00000000-0000-0000-0000-000000000000" Or ClsSectMeeting.AltPeriodId.ToString = String.Empty Then
                db.AddParameter("@AltPeriodId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            Else
                db.AddParameter("@AltPeriodId", ClsSectMeeting.AltPeriodId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            End If
            db.AddParameter("@Room", ClsSectMeeting.Room, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.AddParameter("@Sdate", ClsSectMeeting.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@Edate", ClsSectMeeting.EndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            'Dim strnow As Date = Convert.ToDateTime(Date.Now.ToShortDateString)
            Dim strnow As Date
            strnow = Date.Now

            Dim sDate As String = strnow.Date.ToShortDateString
            Dim sHour As String = strnow.Hour.ToString
            Dim sMinute As String = strnow.Minute.ToString
            Dim iSecs As Integer = strnow.Second
            strnow = Convert.ToDateTime(sDate + " " + sHour + ":" + sMinute + ":" + iSecs.ToString)

            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.AddParameter("@InstructionTypeId", ClsSectMeeting.InstructionTypeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.AddParameter("@BreakDuration", ClsSectMeeting.BreakDuration, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
            db.ClearParameters()
            strSQL.Remove(0, strSQL.Length)
            '   return without errors
            resinfo.ModDate = strnow
            resinfo.ClsSectMtgId = ClsSectMeeting.ClsSectMtgId
            Return resinfo

        Catch ex As OleDbException
            '   return an error to the client
            'Return DALExceptions.BuildErrorMessage(ex)
            resinfo.ErrorString = DALExceptions.BuildErrorMessage(ex)
            Return resinfo
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Function UpdateSingleClsSectMtg(ByVal ClsSectMeeting As ClsSectMeetingInfo, ByVal user As String) As ClsSectMtgIdObjInfo
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        Dim resultInfo As New ClsSectMtgIdObjInfo
        Dim strnow As Date

        ' 09/12/2011 JRobinson Clock Hour Project - Add new fields 

        'Retrieve elements from the array list.
        Try
            With strSQL
                .Append("UPDATE arClsSectMeetings SET ")
                .Append("PeriodId = ?")
                .Append(",AltPeriodId = ?")
                .Append(",RoomId = ?")
                .Append(",StartDate = ? ")
                .Append(",EndDate = ? ")
                .Append(",ModUser=? ")
                .Append(",ModDate=? ")
                .Append(",InstructionTypeID=? ")
                .Append(" ,BreakDuration=?  ")
                .Append(" WHERE ClsSectMeetingId = ? ;")
                '.Append(" AND ModDate = ? ;")
                .Append("Select count(*) from arClsSectMeetings where ModDate = ? ")
                .Append("And ClsSectMeetingId = ? ")
            End With

            db.AddParameter("@dayid", ClsSectMeeting.PeriodId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            'db.AddParameter("@dayid", ClsSectMeeting.AltPeriodId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            If ClsSectMeeting.AltPeriodId.ToString = Guid.Empty.ToString Or ClsSectMeeting.AltPeriodId.ToString = "" Then
                db.AddParameter("@altperiod", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@altperiod", ClsSectMeeting.AltPeriodId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            End If
            db.AddParameter("@roomid", ClsSectMeeting.Room, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.AddParameter("@timeintid", ClsSectMeeting.StartDate, DataAccess.OleDbDataType.OleDbDateTime, 16, ParameterDirection.Input)
            db.AddParameter("@etimeintid", ClsSectMeeting.EndDate, DataAccess.OleDbDataType.OleDbDateTime, 16, ParameterDirection.Input)
            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            strnow = Date.Now

            Dim sDate As String = strnow.Date.ToShortDateString
            Dim sHour As String = strnow.Hour.ToString
            Dim sMinute As String = strnow.Minute.ToString
            Dim iSecs As Integer = strnow.Second
            strnow = Convert.ToDateTime(sDate + " " + sHour + ":" + sMinute + ":" + iSecs.ToString)

            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.AddParameter("@InstructionTypeId", ClsSectMeeting.InstructionTypeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)


            db.AddParameter("@BreakDuration", ClsSectMeeting.BreakDuration, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)


            db.AddParameter("@clsSectid", ClsSectMeeting.ClsSectMtgId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            '   ModDate
            'db.AddParameter("@Original_ModDate", ClsSectMeeting.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.AddParameter("@clsSectid", ClsSectMeeting.ClsSectMtgId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(strSQL.ToString)
            db.ClearParameters()
            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                'Return ""

                resultInfo.ModDate = strnow
                resultInfo.ClsSectMtgId = ClsSectMeeting.ClsSectMtgId

                Return resultInfo
            Else
                'Return DALExceptions.BuildConcurrencyExceptionMessage()
                resultInfo.ErrorString = DALExceptions.BuildConcurrencyExceptionMessage()
                Return resultInfo
            End If

        Catch ex As OleDbException

            '   return an error to the client
            'DisplayOleDbErrorCollection(ex)
            resultInfo.ErrorString = DALExceptions.BuildErrorMessage(ex)
            Return resultInfo

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Sub DeleteClsSectMeeting(ByVal ClsSectID As Guid)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("DELETE FROM arClsSectMeetings WHERE ClsSectionId = ?")
        End With
        db.AddParameter("@clssectionid", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Sub
    Public Sub DeleteOneClsSectMeeting(ByVal ClsSectID As Guid)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("DELETE FROM arClsSectMeetings WHERE ClsSectMeetingId = ?")
        End With
        db.AddParameter("@clssectionid", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Sub
    Public Sub DeleteOneClsSectMeeting_SP(ByVal ClsSectMtgID As Guid)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@clssectionmtgid", ClsSectMtgID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery("dbo.usp_DeleteOneClsSectMeeting", Nothing, "SP")
        'Return db.RunParamSQLDataSet("usp_GetAllTerms", Nothing, "SP").Tables(0)






    End Sub

    Public Function GetSingleClsSect(ByVal ClsSectId As Guid) As DataSet
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim strSQLString As New StringBuilder
        With strSQLString
            .Append("Select * from arClassSections")
            .Append(" where ClsSectionId = ? ")
        End With
        Dim objSingleClsSect As New OleDbDataAdapter
        db.OpenConnection()
        db.AddParameter("@clssectId", ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        objSingleClsSect = db.RunParamSQLDataAdapter(strSQLString.ToString)
        objSingleClsSect.Fill(ds, "SingleClsSect")
        strSQLString.Remove(0, strSQLString.Length)

        db.ClearParameters()
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetClassSection(ByVal ClsSectId As Guid) As DataSet
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        ' 08/23/2011 JRobinson Clock Hour Project - Add new fields for arClsSectionTimeClockPolicy table
        db.OpenConnection()

        With sb
            .Append("SELECT CCT.*, ")
            .Append("    (Select TermDescrip from arTerm where TermId=CCT.TermId) As Term, ")
            .Append("    (Select Descrip from arReqs where ReqId=CCT.ReqId) As Course, ")
            .Append("    (Select ShiftDescrip from arShifts where ShiftId=CCT.ShiftId) As Shift, ")
            .Append("    (Select FullName from syUsers where UserId=CCT.InstructorId) As Instructor, ")
            .Append("    (Select Descrip from arGradeScales where GrdScaleId=CCT.GrdScaleId) As GrdScale, ")
            .Append("    (Select CampDescrip from syCampuses where CampusId=CCT.CampusId) As Campus, ")
            .Append("    (Select Descrip from adLeadGroups where LeadGrpID=CCT.LeadGrpID) As LeadGrp, ")
            .Append("    TCP.AllowEarlyIn, TCP.AllowLateOut, TCP.AllowExtraHours, TCP.CheckTardyIn, TCP.MaxInBeforeTardy, TCP.AssignTardyInTime ")

            .Append(" FROM arClassSections CCT ")
            .Append(" LEFT OUTER JOIN arClsSectionTimeClockPolicy TCP ON TCP.ClsSectionId = CCT.ClsSectionId ")
            .Append(" WHERE CCT.ClsSectionId = ? ")
        End With

        db.AddParameter("@clssectId", ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

        Dim da As New OleDbDataAdapter
        da = db.RunParamSQLDataAdapter(sb.ToString)
        da.Fill(ds, "SingleClsSect")

        sb.Remove(0, sb.Length)
        db.ClearParameters()


        With sb
            .Append("SELECT RightClsSectionId ")
            .Append("FROM arOverridenConflicts ")
            .Append("WHERE LeftClsSectionId = ? ")
        End With

        db.AddParameter("@ClsSectionId", ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

        Dim da1 As New OleDbDataAdapter
        da1 = db.RunParamSQLDataAdapter(sb.ToString)
        da1.Fill(ds, "OverridenConflicts")
        sb.Remove(0, sb.Length)

        db.ClearParameters()
        db.CloseConnection()

        Return ds
    End Function

    Public Function GetOverridenConflicts(ByVal ClsSectId As String) As DataTable
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.OpenConnection()

        With sb
            .Append("SELECT RightClsSectionId ")
            .Append("FROM arOverridenConflicts ")
            .Append("WHERE LeftClsSectionId = ? ")
        End With

        db.AddParameter("@ClsSectionId", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim da1 As New OleDbDataAdapter
        da1 = db.RunParamSQLDataAdapter(sb.ToString)
        da1.Fill(ds, "OverridenConflicts")

        db.CloseConnection()

        Return ds.Tables(0)
    End Function

    Public Sub DeleteOverridenConflicts(ByVal ClsSectId As String)
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.OpenConnection()

        With sb
            .Append("DELETE FROM arOverridenConflicts ")
            .Append("WHERE LeftClsSectionId = ? OR RightClsSectionId = ? ")
        End With

        db.AddParameter("@LeftClsSectionId", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@RightClsSectionId", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)

        db.CloseConnection()
    End Sub

    Public Function DeleteOverridenConflicts(ByVal arrDeleteList As ArrayList) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try
            '   Delete entries in arOverridenConflicts
            With sb
                .Append("DELETE FROM arOverridenConflicts ")
                .Append("WHERE LeftClsSectionId=? OR RightClsSectionId=? ")
            End With

            If arrDeleteList.Count > 0 Then
                For i As Integer = 0 To arrDeleteList.Count - 1
                    db.AddParameter("@LeftClsSectionId", arrDeleteList(i).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@RightClsSectionId", arrDeleteList(i).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                    db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

                    db.ClearParameters()
                Next
            End If

            sb.Remove(0, sb.Length)

            groupTrans.Commit()

            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        End Try

    End Function
    Public Function GetClsSectForTermModule(ByVal termId As String) As DataSet
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim strSQLString As New StringBuilder

        With strSQLString
            .Append("SELECT * FROM arClassSections ")
            .Append("WHERE TermId = ? ")
        End With

        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Return db.RunParamSQLDataSet(strSQLString.ToString)

    End Function

    Public Function GetSingleTerm(ByVal termId As String) As DataSet
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim strSQLString As New StringBuilder

        With strSQLString
            .Append("SELECT TermId,TermCode,TermDescrip,StartDate,EndDate,StatusId ")
            .Append("FROM arTerm ")
            .Append("WHERE TermId = ? ")
        End With

        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Return db.RunParamSQLDataSet(strSQLString.ToString)

    End Function

    Public Function GetClsSectMtgs(ByVal ClsSectId As Guid) As DataSet
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim strSQLString As New StringBuilder
        Dim objSingleClsSectMtg As New OleDbDataAdapter

        db.OpenConnection()

        'Commented out by Troy on 9/3/2005 10:27PM. The way the query is it will not return records if they are missing
        'anything used in the join criteria such as the time intervals or day of meeting. This means that if a record
        'is missing the day of meeting it will not be returned.
        'With strSQLString
        '    .Append("Select a.*, b.TimeIntervalDescrip As StartTime, c.TimeIntervalDescrip As EndTime ")
        '    .Append(" from arClsSectMeetings a, cmTimeInterval b, cmTimeInterval c, plWorkDays d ")
        '    .Append("where ClsSectionId = ? and a.TimeIntervalId = b.TimeIntervalId and a.EndIntervalId = c.TimeIntervalId ")
        '    .Append("and a.WorkDaysId = d.WorkDaysId ")
        '    .Append("order by d.ViewOrder")
        'End With
        With strSQLString
            .Append("SELECT a.*, ")
            .Append("(SELECT TimeIntervalDescrip ")
            .Append(" FROM cmTimeInterval b ")
            .Append(" WHERE b.TimeIntervalId = a.TimeIntervalId) AS StartTime, ")
            .Append(" (SELECT TimeIntervalDescrip ")
            .Append("  FROM cmTimeInterval c ")
            .Append("  WHERE c.TimeIntervalId = a.EndIntervalId) AS EndTime ")
            .Append("FROM arClsSectMeetings a, plWorkdays d ")
            .Append("WHERE a.WorkDaysId = d.WorkDaysId ")
            .Append("AND ClsSectionId = ? ")
            .Append("ORDER BY d.ViewOrder")
        End With



        db.AddParameter("@clssectId", ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

        objSingleClsSectMtg = db.RunParamSQLDataAdapter(strSQLString.ToString)
        objSingleClsSectMtg.Fill(ds, "ClsSectMtgs")
        strSQLString.Remove(0, strSQLString.Length)
        db.ClearParameters()
        db.CloseConnection()
        Return ds
    End Function

    Public Function GetClsSectMtgsWithPeriods(ByVal ClsSectId As Guid) As DataSet
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim strSQLString As New StringBuilder
        Dim objSingleClsSectMtg As New OleDbDataAdapter

        db.OpenConnection()


        With strSQLString
            .Append("Select ")
            .Append("       (select PeriodDescrip from syPeriods where PeriodId=a.PeriodId) as PeriodDescrip, ")
            .Append("		(select PeriodDescrip from syPeriods where PeriodId=a.AltPeriodId) as AltPeriodDescrip, ")
            .Append("		(select Descrip from arRooms where RoomId=a.RoomId) as RoomDescrip, ")
            '.Append("		(select CourseTypeDescrip from arCourseTypes where CourseTypeId=a.CourseTypeId) as CourseTypeDescrip, ")
            .Append("       a.* ")
            .Append("FROM arClsSectMeetings a ")
            .Append("WHERE ClsSectionId = ? ")
            .Append("ORDER BY a.StartDate, a.EndDate ")
        End With



        db.AddParameter("@clssectId", ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

        objSingleClsSectMtg = db.RunParamSQLDataAdapter(strSQLString.ToString)
        objSingleClsSectMtg.Fill(ds, "ClsSectMtgs")
        strSQLString.Remove(0, strSQLString.Length)
        db.ClearParameters()
        db.CloseConnection()
        Return ds
    End Function

    Public Function GetClsSectMtgsWithPeriods_SP(ByVal ClsSectId As Guid) As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@clssectId", ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Return db.RunParamSQLDataSet("dbo.usp_GetClsSectMtgsWithPeriods", Nothing, "SP").Tables(0)




    End Function

    Public Function DoesClsSectionExist(ByVal ClsSection As ClassSectionInfo, ByVal Term As String) As Integer


        Dim ds As New DataSet

        Dim da As OleDbDataAdapter

        Dim RowCount As Integer

        Dim sGrdSysDetailId As String



        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database

            Dim db As New DataAccess

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New System.Text.StringBuilder

            With sb

                .Append("Select count(*) as Count ")
                .Append("from arClassSections a ")
                .Append("where a.ClsSection = ? and a.TermId = ? ")
                .Append("and a.ReqId = ? ")
                .Append("and a.CampusId = ? ")

            End With


            ' Add the PrgVerId and ChildId to the parameter list

            db.AddParameter("@Section", ClsSection.Section, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@term", Term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@clssectionid", ClsSection.CourseId.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@cmpid", ClsSection.CampusId.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query

            '   execute the query
            RowCount = CInt(db.RunParamSQLScalar(sb.ToString))


            'Close Connection

            db.CloseConnection()


        Catch ex As System.Exception

            Throw New BaseException(ex.InnerException.ToString)

        End Try


        'Return the datatable in the dataset
        Return RowCount

    End Function

    Public Function GetStartEndDates(ByVal TermId As String) As String
        Dim da As OleDbDataAdapter
        Dim sGrade As String
        Dim Sdate As String
        Dim Edate As String
        Dim SDateEDate As String
        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder



            With sb
                .Append("SELECT a.StartDate, a.EndDate ")
                .Append("FROM arTerm a ")
                .Append("WHERE a. TermId = ?  ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StdId", TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)



            '   build the sql query

            '   Execute the query
            db.OpenConnection()

            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                Sdate = dr("StartDate")
                Edate = dr("EndDate")
            End While
            SDateEDate = Sdate & "," & Edate

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return SDateEDate

    End Function

    Public Function DoesClsSectHaveGradesPosted(ByVal ClsSectId As String) As Integer


        Dim ds As New DataSet

        Dim da As OleDbDataAdapter

        Dim RowCount As Integer

        Dim sGrdSysDetailId As String

        Dim rowcount2 As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        '   connect to the database

        Dim db As New DataAccess

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New System.Text.StringBuilder

        With sb

            .Append("Select count(*) as Count from arGrdBkResults a where a.ClsSectionId = ?")

        End With


        ' Add the stdenrollid and testid to the parameter list

        db.AddParameter("@Testid", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()

        Try
            'Execute the query
            RowCount = db.RunParamSQLScalar(sb.ToString)
        Catch ex As System.Exception

            Throw New BaseException(ex.InnerException.ToString)

        End Try

        Dim da2 As New OleDbDataAdapter
        sb.Remove(0, sb.Length)

        If RowCount = 0 Then
            With sb
                .Append("Select count(*) as Count from arResults a where a.TestId = ? and a.GrdSysDetailId is not null ")
            End With

            Try
                'db.OpenConnection()
                rowcount2 = db.RunParamSQLScalar(sb.ToString)
            Catch ex As System.Exception

                Throw New BaseException(ex.InnerException.ToString)

            End Try
            db.CloseConnection()
            Return rowcount2
        End If



        'Close Connection

        db.CloseConnection()
        'Return the datatable in the dataset
        Return RowCount


    End Function
    Public Function GetMaxCapOnRoom(ByVal Room As String) As Integer
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim sGrdSysDetailId As String
        Dim rowCount As Integer
        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("Select Capacity from arRooms where RoomId = ? ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@BldgId", Room, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            db.OpenConnection()

            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try

        'Return the datatable in the dataset
        Return rowCount
    End Function
    Public Function GetDaysInPeriod(ByVal PeriodId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT a.WorkDayId, c.StartTimeId, c.EndTimeId ")
                .Append("FROM syPeriodsWorkDays a, syPeriods c ")
                .Append("WHERE a.PeriodId = ? ")
                .Append("AND a.PeriodId = c.PeriodId ")

            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@CampId", PeriodId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "PeriodDT")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception

        End Try

        'Return the datatable in the dataset
        Return ds

    End Function


    Public Function GetAllPeriods_SP(ByVal showActiveOnly As Boolean, ByVal campusId As String) As DataTable

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@showActiveOnly", showActiveOnly, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet("dbo.usp_GetAllPeriods", Nothing, "SP").Tables(0)

    End Function

    Public Function GetAllRooms_SP(ByVal showActiveOnly As Boolean, ByVal campusId As String) As DataTable
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@showActiveOnly", showActiveOnly, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet("dbo.usp_GetAllRooms", Nothing, "SP").Tables(0)
    End Function

    Public Function GetInstructionTypes_SP(ByVal showActiveOnly As Boolean, ByVal campusId As String) As DataTable
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@showActiveOnly", showActiveOnly, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet("dbo.usp_GetInstructionTypes", Nothing, "SP").Tables(0)
    End Function

    Public Function GetTimeClocks_SP(ByVal showActiveOnly As Boolean, ByVal campusId As String) As DataTable
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@showActiveOnly", showActiveOnly, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet("dbo.usp_GetTimeClocks", Nothing, "SP").Tables(0)
    End Function


End Class
