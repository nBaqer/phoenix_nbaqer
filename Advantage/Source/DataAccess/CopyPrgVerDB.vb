Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class CopyPrgVerDB
    Public Function GetAllCampGrps() As DataSet

        'connect to the database
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim da As New OleDbDataAdapter
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            With sb

                .Append("select JC.CampGrpId,JC.CampGrpDescrip ")
                .Append("FROM    syCampGrps JC,syStatuses ST ")
                .Append("WHERE   JC.StatusId = ST.StatusId ")
                .Append(" AND    ST.Status = 'Active' ")
                .Append(" Order By JC.CampGrpDescrip")

            End With

            '   Execute the query
            db.AddParameter("@edate1", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@edate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "ProgVer")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function CreateProgramVersion(ByVal Code As String, ByVal Descrip As String, ByVal CampGrp As String, ByVal PrgVersion As String, ByVal user As String, ByRef createdId As Guid) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim status As String
        Dim sGuid As String = System.Guid.NewGuid.ToString
        Dim sDateGuid As String = System.Guid.NewGuid.ToString
        Dim sPrgVerFeeId As String = System.Guid.NewGuid.ToString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Try
            db.OpenConnection()
            'Set the connection string
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim outputParams = New List(Of DataAccess.Parameter)
            Dim strSQL As String = "CopyProgramVersion"
            db.AddParameter("@prgVersion", PrgVersion, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@Code", Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@Description", Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@CampGrp", CampGrp, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@user", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@NewPrgVerId", createdId, DataAccess.OleDbDataType.OleDbGuid, 50, ParameterDirection.Output)
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString, Nothing, "SP", outputParams:=outputParams)
            createdId = outputParams.Where(Function(p) p.ParameterName = "@NewPrgVerId").Select(Function(p) p.ParameterValue).FirstOrDefault()
            db.ClearParameters()

            Return ""
        Catch ex As OleDbException

            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try
    End Function

    Public Function GetAllCampGrpsForUser(ByVal UserId As String) As DataSet

        'connect to the database
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim da As New OleDbDataAdapter
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            With sb
                .Append("SELECT DISTINCT urcg.CampGrpId, cg.CampGrpDescrip ")
                .Append("FROM dbo.syUsersRolesCampGrps urcg ")
                .Append("INNER JOIN dbo.syUsers u ON u.UserId = urcg.UserId ")
                .Append("INNER JOIN dbo.syRoles r ON r.RoleId = urcg.RoleId ")
                .Append("INNER JOIN dbo.syCampGrps cg ON cg.CampGrpId = urcg.CampGrpId ")
                .Append("INNER JOIN dbo.syStatuses s ON s.StatusId = cg.StatusId ")
                .Append("WHERE urcg.UserId = ? ")
                .Append("AND s.Status = 'Active' ")
                .Append("ORDER BY cg.CampGrpDescrip")
            End With

            '   Execute the query
            db.AddParameter("@UserId", UserId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "ProgVer")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function


End Class



