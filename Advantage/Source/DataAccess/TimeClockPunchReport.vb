Imports FAME.Advantage.Common

Public Class TimeClockPunchReport
#Region "Private Data Members"

    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")

#End Region

#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

#End Region



#Region "Public Methods"

    Public Function GetTimeClockpunchesWeekly(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String = ""
        Dim strOrderBy As String = ""
        Dim strFrom As String = ""
        Dim strStudentId As String = ""
        Dim arrValues() As String
        Dim icount As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        strFrom = " ,syCmpGrpCmps E,syCampuses F,syCampGrps "

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
            'If paramInfo.FilterList.IndexOf("FundSourceId") > -1 Then            

        End If
        Dim NoOfDays As Integer
        Dim FromDate As String
        If paramInfo.FilterOther <> "" Then
            arrValues = paramInfo.FilterOther.Split(";")
            For icount = 0 To arrValues.Length - 1
                If arrValues(icount).Contains("NoOfDays=") Then
                    NoOfDays = CInt(arrValues(icount).Substring(arrValues(icount).IndexOf("=") + 1))
                    NoOfDays = NoOfDays - 1
                ElseIf arrValues(icount).Contains("FromDate=") Then
                    FromDate = Format(CDate(arrValues(icount).Substring(arrValues(icount).IndexOf("=") + 1)), "MM/dd/yyyy")
                End If
            Next
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        If StudentIdentifier = "SSN" Then
            strStudentId = "arStudent.SSN AS StudentIdentifier,"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStudentId = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
        ElseIf StudentIdentifier = "StudentId" Then
            strStudentId = "arStudent.StudentNumber AS StudentIdentifier,"
        End If

        Dim sqlquery As String
        Try


            With sb

                .Append("  Select arStuEnrollments.StuEnrollId,")
                ''SSN Added
                .Append(strStudentId)

                .Append(" isNull(arStudent.LastName,' ')+','+ isNull(arStudent.FirstName,' ') + ' '+ isNull(arStudent.MiddleName,' ') as StudentName,arStuEnrollments.EnrollMentId,arStuEnrollments.BadgeNumber, BadgeId,convert(varchar,PunchTime,101) PunchDate, ")
                .Append("DATENAME(WEEKDAY,PunchTime) RecordDay, (Select convert(varchar,PunchTime,108)  from arStudentTimeClockPunches ")
                .Append(" where PunchType=1 and Status=1 and BadgeId=A.BadgeId and PunchTime=A.PunchTime) as PunchIn")
                .Append(" ,(Select convert(varchar,PunchTime,108)  from arStudentTimeClockPunches ")
                .Append(" where PunchType=2 and Status=1 and BadgeId=A.BadgeId and PunchTime=A.PunchTime) as PunchOut,")
                .Append(" convert(varchar,SC.RecordDate,101)RecordDate, SC.SchedHours, SC.ActualHours")
                .Append(" from  ")
                .Append(" arStudentTimeClockPunches A,arStuEnrollments arStuEnrollments,arStudent arStudent,arSTudentClockAttendance SC")
                .Append(strFrom)
                .Append(" where  ")
                .Append(" ActualHours<>9999.0 and ActualHours<>999.0 and ActualHours is not Null  and ")
                ''commented on Feb 8 2009
                ''18433: QA: Time clock punches report is not pulling up the previous punches when the badge id is changed. 
                ''When a scheduleid is changed, the previous punches cannot be seen.
                ''.Append("  SC.ScheduleId in (Select ScheduleId from arStudentSchedules where StuENrollId=arStuEnrollments.StuEnrollId) and ")
                .Append(" CAST( convert(varchar,PunchTime,101) as Date)>= CAST('")
                .Append(FromDate + "' as Date) ")
                .Append(" and CAST(convert(varchar,PunchTime,101) as Date)<= DATEADD(day," + NoOfDays.ToString + ",'" + FromDate + "')")
                .Append(" and SC.RecordDate>='" + FromDate + "' and SC.RecordDate<=DATEADD(day," + NoOfDays.ToString + ",'" + FromDate + "') ")
                .Append(" and SC.RecordDate=convert(varchar,PunchTime,101) and ")
                '.Append(" Convert(Int, A.BadgeId) = Convert(Int, arStuEnrollments.BadgeNumber) And ")
                .Append("      arStuEnrollments.StuEnrollId=A.StuEnrollID  and")
                .Append(" arStuEnrollments.StudentId = arStudent.StudentId And ")
                .Append(" A.Status=1 and arStuEnrollments.StuEnrollId=SC.StuEnrollid ")
                .Append(" AND arStuEnrollments.CampusId=F.CampusId ")
                .Append(" AND F.CampusId=E.CampusId AND    E.CampGrpId = syCampGrps.CampGrpId ")
                .Append(strWhere)
                .Append(" order by arStuEnrollments.StuEnrollID,punchtime; ")


                .Append("  Select arStuEnrollments.StuEnrollId ,")
                .Append(" Sum(ActualHours) ActualHours,Sum(SchedHours) SchedHours ")
                .Append(" from  ")
                .Append(" arStuEnrollments arStuEnrollments,arStudent arStudent,arSTudentClockAttendance SC")
                .Append(strFrom)
                .Append(" where  ")
                .Append(" ActualHours<>9999.0 and ActualHours<>999.0 and ActualHours is not Null  and ")
                '' .Append("  SC.ScheduleId in (Select ScheduleId from arStudentSchedules where StuENrollId=arStuEnrollments.StuEnrollId) and ")

                .Append(" SC.RecordDate>='" + FromDate + "' and SC.RecordDate<=DATEADD(day," + NoOfDays.ToString + ",'" + FromDate + "') ")
                .Append(" and arStuEnrollments.StudentId=arStudent.StudentId  and ")
                .Append("  arStuEnrollments.StuEnrollId=SC.StuEnrollid ")
                .Append(" AND arStuEnrollments.CampusId=F.CampusId ")
                .Append(" AND F.CampusId=E.CampusId AND    E.CampGrpId = syCampGrps.CampGrpId ")
                .Append(strWhere)
                .Append(" Group by arStuEnrollments.StuEnrollId ")

                .Append(" ; Select SC.StuEnrollid,convert(varchar,PunchTime,101)RecordDate ")
                .Append(" from  ")
                .Append(" arStudentTimeClockPunches A,arStuEnrollments arStuEnrollments,arStudent arStudent,arSTudentClockAttendance SC")
                .Append(strFrom)
                .Append(" where  ")
                .Append(" ActualHours<>9999.0 and ActualHours<>999.0 and ActualHours is not Null  and ")
                ''   .Append("  SC.ScheduleId in (Select ScheduleId from arStudentSchedules where StuENrollId=arStuEnrollments.StuEnrollId) and ")
                .Append(" CAST(convert(varchar,PunchTime,101) as DATE)>=CAST( '")
                .Append(FromDate + "' as DATE) ")
                .Append(" and CAST(convert(varchar,PunchTime,101) as DATE)<= DATEADD(day," + NoOfDays.ToString + ",'" + FromDate + "')")
                .Append(" and SC.RecordDate>='" + FromDate + "' and SC.RecordDate<=DATEADD(day," + NoOfDays.ToString + ",'" + FromDate + "') ")
                .Append(" and SC.RecordDate=convert(varchar,PunchTime,101)")
                ''  .Append("  and Convert(int,A.BadgeId)=Convert(int,arStuEnrollments.BadgeNumber)  ")
                .Append("      and arStuEnrollments.StuEnrollId=A.StuEnrollID ")
                .Append(" and arStuEnrollments.StudentId=arStudent.StudentId  and ")
                .Append(" A.Status=1 and arStuEnrollments.StuEnrollId=SC.StuEnrollid ")
                .Append(" AND arStuEnrollments.CampusId=F.CampusId ")
                .Append(" AND F.CampusId=E.CampusId AND    E.CampGrpId = syCampGrps.CampGrpId ")
                .Append(strWhere)
                .Append(" group by SC.StuEnrollId,convert(varchar,PunchTime,101)")


            End With

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            ds = db.RunParamSQLDataSet(sb.ToString)

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Return ds
        End Try
        'Dim dt As DataTable
        'dt = ds.Tables(0)

        'dt.Columns.Add(New DataColumn("AbsentHours", System.Type.GetType("System.String")))
        'dt.Columns.Add(New DataColumn("MakeUpHours", System.Type.GetType("System.String")))


        ''  Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        'Dim ds1 As New DataSet
        'Dim dt As New DataTable("dtTimeClockPunches")

        'If ds.Tables.Count > 0 Then
        '    dt.Columns.Add(New DataColumn("StuEnrollId", System.Type.GetType("System.String")))
        '    dt.Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
        '    dt.Columns.Add(New DataColumn("BadgeId", System.Type.GetType("System.String")))
        '    dt.Columns.Add(New DataColumn("RecordDate", System.Type.GetType("System.String")))
        '    dt.Columns.Add(New DataColumn("RecordDay", System.Type.GetType("System.String")))
        '    dt.Columns.Add(New DataColumn("PunchIn", System.Type.GetType("System.String")))
        '    dt.Columns.Add(New DataColumn("PunchOut", System.Type.GetType("System.String")))
        '    dt.Columns.Add(New DataColumn("SchedHours", System.Type.GetType("System.String")))
        '    dt.Columns.Add(New DataColumn("ActualHours", System.Type.GetType("System.String")))
        '    dt.Columns.Add(New DataColumn("AbsentHours", System.Type.GetType("System.String")))
        '    dt.Columns.Add(New DataColumn("MakeUpHours", System.Type.GetType("System.String")))

        'Dim row As DataRow
        'Dim i As Integer

        'For i = 0 To ds.Tables(0).Rows.Count - 1


        '    '        row("StuEnrollId") = ds.Tables(0).Rows(i)("StuEnrollId").ToString
        '    '        row("StudentName") = ds.Tables(0).Rows(i)("StudentName").ToString
        '    '        row("BadgeId") = ds.Tables(0).Rows(i)("BadgeId")
        '    '        row("RecordDate") = ds.Tables(0).Rows(i)("PunchDate")
        '    '        row("RecordDay") = ds.Tables(0).Rows(i)("RecordDay")
        '    '        row("PunchIn") = ds.Tables(0).Rows(i)("PunchIn")
        '    '        row("PunchOut") = ds.Tables(0).Rows(i)("PunchOut")
        '    '        row("SchedHours") = ds.Tables(0).Rows(i)("SchedHours")

        '    If Not ds.Tables(0).Rows(i)("ActualHours") Is System.DBNull.Value Then
        '        If CDec(ds.Tables(0).Rows(i)("ActualHours")) = 0.0 Then
        '            ds.Tables(0).Rows(i)("AbsentHours") = ds.Tables(0).Rows(i)("SchedHours")
        '            ds.Tables(0).Rows(i)("MakeUpHours") = "0.00"
        '        ElseIf CDec(ds.Tables(0).Rows(i)("ActualHours")) = 9999.0 Then
        '            ds.Tables(0).Rows(i)("ActualHours") = "0.00"
        '            ds.Tables(0).Rows(i)("AbsentHours") = "0.00"
        '            ds.Tables(0).Rows(i)("MakeUpHours") = "0.00"
        '        ElseIf CDec(ds.Tables(0).Rows(i)("ActualHours")) = 999.0 Then
        '            ds.Tables(0).Rows(i)("ActualHours") = "0.00"
        '            ds.Tables(0).Rows(i)("AbsentHours") = "0.00"
        '            ds.Tables(0).Rows(i)("MakeUpHours") = "0.00"
        '        Else
        '            If CDec(ds.Tables(0).Rows(i)("SchedHours")) > CDec(ds.Tables(0).Rows(i)("ActualHours")) Then
        '                ds.Tables(0).Rows(i)("AbsentHours") = CDec(ds.Tables(0).Rows(i)("SchedHours")) - CDec(ds.Tables(0).Rows(i)("ActualHours"))
        '                ds.Tables(0).Rows(i)("MakeUpHours") = "0.00"
        '            End If
        '            If CDec(ds.Tables(0).Rows(i)("ActualHours")) > CDec(ds.Tables(0).Rows(i)("SchedHours")) Then
        '                ds.Tables(0).Rows(i)("AbsentHours") = "0.00"
        '                ds.Tables(0).Rows(i)("MakeUpHours") = CDec(ds.Tables(0).Rows(i)("ActualHours")) - CDec(ds.Tables(0).Rows(i)("SchedHours"))
        '            End If
        '            End If
        '    End If
        '    '        dt.Rows.Add(row)
        'Next i
        'dt.AcceptChanges()
        '    ds1.Tables.Add(dt)
        'End If

        Return ds
    End Function
#End Region

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

End Class
