Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common
Imports System.Diagnostics.Eventing.Reader

Public Class ReportingAgencyDB
    Public Function GetDataGridInfo() As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim da As New OleDbDataAdapter
        Dim ds As New DataSet
        Dim count As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT RptFldId, Descrip ")
            .Append("FROM  syRptFields")
        End With

        '   Execute the query       
        db.OpenConnection()
        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "RptFlds")

        Catch ex As Exception

        End Try
        sb.Remove(0, sb.Length)
        db.ClearParameters()

        'Close Connection
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetMaxRptAgencyFldId() As Integer
        Dim ds As DataSet
        Dim maxindex As Integer

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder
            Dim obj As New Object

            '   build the sql query
            With sb

                .Append("Select MAX(RptAgencyFldId) ")
                .Append("FROM syRptAgencyFields ")

            End With

            If Not IsDBNull(db.RunParamSQLScalar(sb.ToString())) Then
                Return Convert.ToInt16(db.RunParamSQLScalar(sb.ToString()))
            Else
                Return 0
            End If

            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException("Error obtaining max RptAgencyFldId  - " & ex.InnerException.Message)
        End Try

    End Function
    Public Function GetMaxRptAgencyFldValId() As Integer
        Dim ds As DataSet
        Dim maxindex As Integer

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder
            Dim obj As New Object

            '   build the sql query
            With sb

                .Append("Select MAX(RptAgencyFldValId) ")
                .Append("FROM syRptAgencyFldValues ")

            End With

            If Not IsDBNull(db.RunParamSQLScalar(sb.ToString())) Then
                Return Convert.ToInt16(db.RunParamSQLScalar(sb.ToString()))
            Else
                Return 0
            End If

            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException("Error obtaining max RptAgencyFldValId  - " & ex.InnerException.Message)
        End Try

    End Function
    Public Function GetReportingAgencies() As DataSet

        'connect to the database
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim da As New OleDbDataAdapter
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            With sb

                .Append("SELECT t1.RptAgencyId,t1.Descrip ")
                .Append("FROM syRptAgencies t1 ")
                .Append("ORDER BY t1.Descrip ")
            End With

            '   Execute the query
            'db.AddParameter("@edate1", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'db.AddParameter("@edate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "StdTerms")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds


    End Function
    Public Function AddReportAgencyFields(ByVal AgencyId As String, ByVal RptFldId As String) As String
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        Dim resinfo As New ClsSectMtgIdObjInfo
        Dim maxAgencyFldId As Integer

        maxAgencyFldId = GetMaxRptAgencyFldId()

        Try
            With strSQL
                .Append("INSERT INTO syRptAgencyFields ")
                .Append("(RptAgencyFldId, RptAgencyId, RptFldId) ")
                .Append("VALUES(?,?,?)")
            End With
            ' Set the DateCreated time to now
            db.AddParameter("@ReqTypeId", maxAgencyFldId + 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.AddParameter("@rptagencyfldid", AgencyId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@rptagency", RptFldId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
            db.ClearParameters()
            strSQL.Remove(0, strSQL.Length)


            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteReportAgencyFields(ByVal AgencyId As String, ByVal RptFldId As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("DELETE FROM syRptAgencyFields ")
            .Append("WHERE RptAgencyId = ? ")
            .Append("AND RptFldId = ? ")
        End With

        db.AddParameter("@campgrpid", AgencyId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@campusid", RptFldId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function
    Public Function GetAgencyFields(ByVal AgencyId As String) As DataSet

        'connect to the database
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim da As New OleDbDataAdapter
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            With sb

                .Append("SELECT t1.RptAgencyFldId, t1.RptAgencyId,t1.RptFldId,t2.Descrip ")
                .Append("FROM syRptAgencyFields t1, syRptFields t2 ")
                .Append("WHERE t1.RptAgencyId = ? and t1.RptFldId = t2.RptFldId ")
            End With

            '   Execute the query
            db.AddParameter("@edate1", AgencyId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "AgencyFields")


        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        sb.Remove(0, sb.Length)
        db.ClearParameters()


        Dim da2 As New OleDbDataAdapter

        With sb
            .Append("SELECT RptFldId, Descrip ")
            .Append("FROM  syRptFields")
        End With


        da2 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da2.Fill(ds, "RptFlds")
        Catch ex As System.Exception

        End Try
        sb.Remove(0, sb.Length)
        'Close Connection
        db.CloseConnection()

        Return ds

    End Function
    Public Function GetAgencyFieldValues(ByVal AgencyFldId As String) As DataSet

        'connect to the database
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim da As New OleDbDataAdapter
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            With sb

                .Append("SELECT t1.RptAgencyFldValId,t1.RptAgencyFldId, t1.AgencyDescrip ")
                .Append("FROM syRptAgencyFldValues t1 ")
                .Append("WHERE t1.RptAgencyFldId = ? ")
            End With

            '   Execute the query
            db.AddParameter("@edate1", AgencyFldId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "AgencyFieldValues")


        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        sb.Remove(0, sb.Length)
        db.ClearParameters()


        Dim da2 As New OleDbDataAdapter

        With sb
            .Append("SELECT RptFldId, Descrip ")
            .Append("FROM  syRptFields")
        End With


        da2 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da2.Fill(ds, "RptFlds")
        Catch ex As System.Exception

        End Try
        sb.Remove(0, sb.Length)
        'Close Connection
        db.CloseConnection()

        Return ds

    End Function
    Public Function UpdateAgencyFieldValues(ByVal dt As DataTable) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   all updates must be encapsulated as one transaction
        Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            '   build the sql query for the adLeadGrpReqGroups data adapter
            Dim sb As New StringBuilder

            With sb
                '   with subqueries
                .Append(" Select RptAgencyFldValId, RptAgencyFldId,AgencyDescrip ")
                .Append(" from syRptAgencyFldValues ")
            End With

            '   build select command
            Dim ReqGroupsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle StudentAwards table
            Dim ReqGroupsDataAdapter As New OleDbDataAdapter(ReqGroupsSelectCommand)

            '   build insert, update and delete commands for ReqGroups table
            Dim cb1 As New OleDbCommandBuilder(ReqGroupsDataAdapter)

            '   insert added rows in adReqs table
            ReqGroupsDataAdapter.Update(dt.Select(Nothing, Nothing, DataViewRowState.Added))


            '   delete rows in LeadGrpReqGrps table
            ReqGroupsDataAdapter.Update(dt.Select(Nothing, Nothing, DataViewRowState.Deleted))


            '   update rows in LeadGrpReqGrps table
            ReqGroupsDataAdapter.Update(dt.Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))


            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            Return ""

        Catch ex As OleDb.OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message`
            Return DALExceptions.BuildErrorMessage(ex)

        Catch ex As Exception
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return ex.Message

        Finally

            '   close connection
            connection.Close()
        End Try

    End Function
    Public Function InsertValues(ByVal strValueId As String, Optional ByVal selectedRptAgencyFldValId As ArrayList = Nothing, Optional ByVal UpdTable As String = "", Optional bUpdIPEDS As Boolean = True) As Integer
        '   connect to the database
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb1 As New StringBuilder
        With sb1
            .Append("Delete from syRptAgencySchoolMapping where SchoolDescripId = ? ")
        End With
        db.AddParameter("@ValueId", strValueId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'execute query
        db.RunParamSQLExecuteNoneQuery(sb1.ToString)
        db.ClearParameters()
        sb1.Remove(0, sb1.Length)

        '   Insert one record per each Item in the Selected Group
        Dim x As Integer = 0
        Try
            If Not selectedRptAgencyFldValId Is Nothing Then
                Dim intRptAgencyCount As Integer

                intRptAgencyCount = selectedRptAgencyFldValId.Count
                While x < intRptAgencyCount
                    With sb
                        .Append("INSERT INTO SyRptAgencySchoolMapping(MappingId,RptAgencyFldValId,SchoolDescripId) ")
                        .Append("VALUES(?,?,?)")
                    End With
                    'add parameters
                    Dim intRptAgencyFldValId As Integer = CType(DirectCast(selectedRptAgencyFldValId.Item(x), String), Integer)
                    db.AddParameter("@MappingId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@RptAgencyFldValId", intRptAgencyFldValId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
                    db.AddParameter("@SchoolDescripId", strValueId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    If Not intRptAgencyFldValId = 0 Then
                        db.RunParamSQLExecuteNoneQuery(sb.ToString)
                    End If

                    db.ClearParameters()
                    sb.Remove(0, sb.Length)

                    ' DE 1131 Janet Robinson 04/25/2011
                    If bUpdIPEDS Then 'DE8804 - it is trying to update all the agencies value to the IPEDS field (when called from standalone maintenance page, not from Maintenance grid page
                        BuildIPEDSValue(intRptAgencyFldValId, strValueId, UpdTable)
                    End If

                    'Increment value of x and y
                    x = x + 1
                End While
                Return 0
            End If
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Sub BuildIPEDSValue(ByVal RptAgencyFldValId As Integer,
                            ByVal FieldId As String, Optional ByVal UpdTable As String = "", Optional ByVal tran As OleDbTransaction = Nothing)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim iIpedsSequence As Int16
        Dim isTest as Boolean = False 
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb1 As New StringBuilder
        Select Case RptAgencyFldValId.ToString
            Case Is = "30" 'Men
                With sb1
                    .Append("update adGenders set IPEDSValue=?,IPEDSSequence=1 where GenderId=?  ")
                End With
            Case Is = "31" 'Women
                With sb1
                    .Append("update adGenders set IPEDSValue=?,IPEDSSequence=2 where GenderId =?")
                End With
            Case Is = "29" 'Women
                With sb1
                    .Append("update adEthCodes set IPEDSSequence=1,IPEDSValue=? where EthCodeId = ?")
                End With
            Case Is = "24" 'Black or African American
                With sb1
                    .Append("update adEthCodes set IPEDSSequence=5,IPEDSValue=? where EthCodeId =?")
                End With
            Case Is = "27" 'American Indian or Alaska Native
                With sb1
                    .Append("update adEthCodes set IPEDSSequence=3,IPEDSValue=? where EthCodeId =? ")
                End With
            Case Is = "23" 'Women
                With sb1
                    .Append("update adEthCodes set IPEDSSequence=4,IPEDSValue=? where EthCodeId =?")
                End With
            Case Is = "26" 'Hispanic/Latino
                With sb1
                    .Append("update adEthCodes set IPEDSSequence=2,IPEDSValue=? where EthCodeId =?")
                End With
            Case Is = "25" 'White
                With sb1
                    .Append("update adEthCodes set IPEDSSequence=7,IPEDSValue=? where EthCodeId =?")
                End With
            Case Is = "28" 'Race/ethnicity unknown
                With sb1
                    .Append("update adEthCodes set IPEDSSequence=9,IPEDSValue=? where EthCodeId =?")
                End With
            Case Is = "82" 'Native Hawaiian or Other Pacific Islander
                With sb1
                    .Append("update adEthCodes set IPEDSSequence=6,IPEDSValue=? where EthCodeId =?")
                End With
            Case Is = "83" 'Two or more races
                With sb1
                    .Append("update adEthCodes set IPEDSSequence=8,IPEDSValue=? where EthCodeId =?")
                End With
            Case Is = "58" 'Women
                With sb1
                    .Append("update arProgTypes set IPEDSSequence=1,IPEDSValue=? where ProgTypId=? ")
                End With
            Case Is = "59" 'Women
                With sb1
                    .Append("update arProgTypes set IPEDSSequence=2,IPEDSValue=? where ProgTypId=?")
                End With
            Case Is = "60" 'Women
                With sb1
                    .Append("update arProgTypes set IPEDSSequence=3,IPEDSValue=? where ProgTypId=?")
                End With
            Case Is = "11" 'First-Time
                With sb1
                    .Append("update adDegCertSeeking set IPEDSSequence=1,IPEDSValue=? where DegCertSeekingId=?")
                End With
            Case Is = "12" 'Other
                With sb1
                    .Append("update adDegCertSeeking set IPEDSSequence=2,IPEDSValue=? where DegCertSeekingId=?")
                End With
            Case Is = "10" 'First-Professional
                With sb1
                    .Append("update adDegCertSeeking set IPEDSSequence=3,IPEDSValue=? where DegCertSeekingId=?")
                End With
            Case Is = "62" 'Part Time
                With sb1
                    .Append("update arAttendTypes set IPEDSSequence=1,IPEDSValue=? where AttendTypeId =?")
                End With
            Case Is = "61" 'Full Time
                With sb1
                    .Append("update arAttendTypes set IPEDSSequence=2,IPEDSValue=? where AttendTypeId =?")
                End With
            Case Is = "63" 'US Citizen
                With sb1
                    .Append("update adCitizenships set IPEDSSequence=1,IPEDSValue=? where CitizenShipId=?")
                End With
            Case Is = "64" 'Eligible Non Citizen
                With sb1
                    .Append("update adCitizenships set IPEDSSequence=1,IPEDSValue=? where CitizenShipId=?")
                End With
            Case Is = "65" 'Non Citizen
                With sb1
                    .Append("update adCitizenships set IPEDSSequence=1,IPEDSValue=? where CitizenShipId=?")
                End With

                'Degrees
            Case Is = "143", "144" ' Bachelors degree or other degrees
                With sb1
                    .Append("update arDegrees set IPEDSValue=? where DegreeId=?")
                End With
            Case Is = "145", "146", "147"  'Tuition: in-state tuition,or in-district tuition, or out-of-state tuition
                With sb1
                    .Append("update saTuitionCategories set IPEDSValue=? where TuitionCategoryId=?")
                End With
            Case Is = "148", "149", "150", "151", "152", "153" 'Income Levels
                With sb1
                    .Append("update syFamilyIncome set IPEDSValue=? where FamilyIncomeId=?")
                End With
            Case Is = "35", "36", "37" 'Housing Type
                With sb1
                    .Append(" update arHousing set IPEDSValue=? where HousingId=?")
                End With
            Case Is = "15", "16", "17", "18", "19"
                With sb1
                    .Append(" update arDropReasons set IPEDSValue=? where DropReasonId=?")
                End With
            Case Is = "66", "67", "68", "69", "70", "71"
                With sb1
                    .Append(" update saFundSources set IPEDSValue=? where FundSourceId=?")
                End With
            Case Is = "161", "162", "163", "164", "165", "166", "167"
                With sb1
                    .Append(" update adReqs set IPEDSValue=? where adReqId=?")
                End With
                isTest = True
            Case Is = "154", "155", "156", "157", "158", "159", "160"
                Select Case RptAgencyFldValId.ToString
                    Case Is = "154"
                        iIpedsSequence = 1
                    Case Is = "155"
                        iIpedsSequence = 2
                    Case Is = "156"
                        iIpedsSequence = 3
                    Case Is = "157"
                        iIpedsSequence = 4
                    Case Is = "158"
                        iIpedsSequence = 5
                    Case Is = "159"
                        iIpedsSequence = 6
                    Case Is = "160"
                        iIpedsSequence = 7
                End Select
                With sb1
                    .Append(" update arProgCredential set  IPEDSSequence=" & iIpedsSequence & ",IPEDSvalue=? where Credentialid=?")
                End With
                ' DE 1131 Janet Robinson 04/25/2011
            Case Is = "0" 'Select  - reset IPEDS fields back to NULL
                If UpdTable <> "" Then
                    Select Case UpdTable
                        Case "adGenders"
                            With sb1
                                .Append("update adGenders set IPEDSSequence=NULL,IPEDSValue=NULL where GenderId=?")
                            End With
                        Case "adEthCodes"
                            With sb1
                                .Append("update adEthCodes set IPEDSSequence=NULL,IPEDSValue=NULL where EthCodeId=?")
                            End With
                        Case "arProgTypes"
                            With sb1
                                .Append("update arProgTypes set IPEDSSequence=NULL,IPEDSValue=NULL where ProgTypId=?")
                            End With
                        Case "adDegCertSeeking"
                            With sb1
                                .Append("update adDegCertSeeking set IPEDSSequence=NULL,IPEDSValue=NULL where DegCertSeekingId=?")
                            End With
                        Case "arAttendTypes"
                            With sb1
                                .Append("update arAttendTypes set IPEDSSequence=NULL,IPEDSValue=NULL where AttendTypeId=?")
                            End With
                        Case "adCitizenships"
                            With sb1
                                .Append("update adCitizenships set IPEDSSequence=NULL,IPEDSValue=NULL where CitizenshipId=?")
                            End With
                        Case "arDegrees"
                            With sb1
                                .Append("update arDegrees set IPEDSValue=NULL where DegreeId=?")
                            End With
                        Case "saTuitionCategories"
                            With sb1
                                .Append("update saTuitionCategories set IPEDSValue=NULL where TuitionCategoryId=?")
                            End With
                        Case "syFamilyIncome"
                            With sb1
                                .Append("update syFamilyIncome set IPEDSValue=NULL where FamilyIncomeId=?")
                            End With
                        Case "arHousing"
                            With sb1
                                .Append(" update arHousing set IPEDSValue=NULL where HousingId=?")
                            End With
                        Case "arDropReasons"
                            With sb1
                                .Append(" update arDropReasons set IPEDSValue=NULL where DropReasonId=?")
                            End With
                        Case "saFundSources"
                            With sb1
                                .Append(" update saFundSources set IPEDSValue=NULL where FundSourceId=?")
                            End With
                        Case "adReqs"
                            With sb1
                                .Append(" update adReqs set IPEDSValue=NULL where adReqId=?")
                            End With
                        Case "arProgCredential"
                            With sb1
                                .Append("update arProgCredential set IPEDSSequence=NULL,IPEDSValue=NULL where CredentialId=?")
                            End With
                    End Select
                End If

        End Select
        If RptAgencyFldValId > 0 Then
            db.AddParameter("@RptAgencyFldValId", RptAgencyFldValId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        ' DE 1131 end
        db.AddParameter("@FieldId", FieldId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.OpenConnection()

        '''' Code modified by Kamalesh Ahuja  on 17 th September 2010 to resolve mantis issue id 19737
        Try
             If (sb1.Length > 0) Then
                    If Not IsNothing(tran) AndAlso IsTest = False Then
                        db.RunParamSQLExecuteNoneQuery(sb1.ToString, tran)
                    Else
                        db.RunParamSQLExecuteNoneQuery(sb1.ToString)
                    End If
            End If
        '''''''
            db.ClearParameters()
            sb1.Remove(0, sb1.Length)
        Catch ex As Exception
        Finally
            db.CloseConnection()
        End Try
    End Sub
    Public Sub UpdateGainfulEmploymentValues(ByVal RptAgencyFldValId As Integer, ByVal FieldId As String, Optional ByVal UpdTable As String = "", Optional ByVal tran As OleDbTransaction = Nothing)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim db As New DataAccess
        Dim sb1 As New StringBuilder
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Select Case UpdTable
            Case "arAttendTypes"
                Select Case RptAgencyFldValId
                    Case Is = 0
                        sb1.Append("update arAttendTypes set GESequence = NULL, GERptAgencyFldValId = NULL  where AttendTypeId = ? ")
                    Case 168 To 171
                        sb1.Append("update arAttendTypes set GESequence = " & (RptAgencyFldValId - 168 + 1).ToString() & ", GERptAgencyFldValId = ?  where AttendTypeId = ? ")
                End Select
            Case "arProgCredential"
                Select Case RptAgencyFldValId
                    Case Is = 0
                        sb1.Append("update arProgCredential set GESequence = NULL, GERptAgencyFldValId = NULL  where CredentialId = ? ")
                    Case 172 To 179
                        sb1.Append("update arProgCredential set  GESequence =" & (RptAgencyFldValId - 172 + 1).ToString() & ", GERptAgencyFldValId = ?  where Credentialid = ? ")
                End Select
        End Select

        If RptAgencyFldValId > 0 Then
            db.AddParameter("@RptAgencyFldValId", RptAgencyFldValId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        db.AddParameter("@FieldId", FieldId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        If (sb1.Length > 0) Then
            If Not IsNothing(tran) Then
                db.RunParamSQLExecuteNoneQuery(sb1.ToString, tran)
            Else
                db.RunParamSQLExecuteNoneQuery(sb1.ToString)
            End If

        End If
        '''''''
        db.ClearParameters()
        sb1.Remove(0, sb1.Length)
    End Sub
End Class
