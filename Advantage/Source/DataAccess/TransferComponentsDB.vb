Imports FAME.Advantage.Common

Public Class TransferComponentsDB

    Public Function GetPartiallyCompletedCourses(ByVal stuEnrollId As String) As DataSet
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Return db.RunParamSQLDataSetUsingSP("usp_AR_GetPartiallyCompletedCourses")
    End Function

    Public Function GetStudentsWithPartiallyCompletedCourses(reqId As String, termId As String) As DataSet
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@ReqId", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Return db.RunParamSQLDataSetUsingSP("usp_AR_GetStudentsWithPartiallyCompletedCourse")
    End Function

    Public Function GetCompletedComponents(ByVal resultId As String) As DataSet
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@ResultId", resultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Return db.RunParamSQLDataSetUsingSP("usp_AR_GetCompletedComponents")
    End Function

    Public Function GetAvailableClassesForPartialTransferByStudent(ByVal resultId As String) As DataSet
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@ResultId", resultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Return db.RunParamSQLDataSetUsingSP("usp_AR_GetAvailableClassesForPartialTransfer_ByStudent")
    End Function

    Public Function GetScheduleConflictsTransferPartialForStudent(ByVal classList As String) As DataTable
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim db As New SQLDataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@CurrentClassSectionId", classList, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        Try
            Return db.RunParamSQLDataSet_SP("dbo.USP_ScheduleConflicts_TransferPartial_Student").Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    
    Public Function GetAvailableClassesForPartialTransferByClass(ByVal reqId As String, ByVal campusId As String) As DataSet
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@ReqId", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Return db.RunParamSQLDataSetUsingSP("usp_AR_GetAvailableClassesForPartialTransfer_ByClass")
    End Function

    Public Function TransferCompletedComponents(ByVal newClsSectionId As String, ByVal oldClsSectionId As String, _
                                                ByVal stuEnrollId As String, ByVal user As String) As String
        Try
            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.AddParameter("@NewClsSectionId", newClsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@OldClsSectionId", oldClsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@User", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery("usp_AR_TransferCompletedComponentsByCourse", Nothing, "SP")
            Return String.Empty
        Catch ex As Exception
            Return "Error transferring student course components - " & ex.Message
        End Try

    End Function

    Public Function ArchiveStudentCourse(ByVal clsSectionId As String, ByVal stuEnrollId As String, ByVal user As String) As String
        Try
            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@User", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery("usp_AR_ArchiveStudentCourse", Nothing, "SP")
            Return String.Empty
        Catch ex As Exception
            Return "Error archiving student course - " & ex.Message
        End Try
    End Function

    Public Function ArchiveStudentCourseComponents(ByVal clsSectionId As String, ByVal stuEnrollId As String, ByVal user As String) As String
        Try
            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.AddParameter("@RClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@User", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery("usp_AR_ArchiveStudentCourseComponents", Nothing, "SP")
            Return String.Empty
        Catch ex As Exception
            Return "Error archiving student course components - " & ex.Message
        End Try
    End Function

End Class

