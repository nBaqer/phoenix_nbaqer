﻿Imports System
Imports System.Data
Imports System.Data.Sql
Imports System.Collections
Imports System.Diagnostics
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports FAME.AdvantageV1.Common
Imports FAME.Advantage.Common

Public NotInheritable Class PortalDataAccess
    Implements IDisposable

#Region "Private Variables and Objects"
    'ADO.NET Objects needed for data access
    Private mConnection As SqlConnection
    Private mCommand As SqlCommand
    Private mDataReader As SqlDataReader
    'Private m_XMLReader As XmlReader
    Private mDataAdapter As SqlDataAdapter
    Private mDataSet As DataSet
    Private mObject As Object

    Public MPageAddress As String
    'We are going to handle parameters internally and this array is going to hold them
    Private ReadOnly mParameterList As ArrayList = New ArrayList

    'Connection string variables
    'Private myAdvAppSettings As PortalAdvAppSettings = GetAdvAppSettings()
    Private mConnectionString As String = ConfigurationManager.ConnectionStrings("ClientConnectionString").ConnectionString
    'Private m_ConnectionString As String = System.Configuration.ConfigurationManager.AppSettings("ConString")
    'Private m_Server As String
    'Private m_Database As String
    'Private m_UserName As String
    'Private m_Password As String

    'Command timeout variable
    Private mCommandTimeout As Integer

    'Naming, Cleanup and Exception variables
    Private ReadOnly mModuleName As String
    Private mDisposedBoolean As Boolean
    Private Const M_EXCEPTION_MESSAGE As String = "Data Application Error. Detail Error Information can be found in the Application Log"

    Public Enum sqlDataType
        sqlString
        sqlChar
        sqlInteger
        sqlDateTime
        sqlDecimal
        sqlMoney
        'added Enum for Guid Datatype - Corey Masson - Jan 7, 2004
        sqlGuid
        'added Enum for Boolean Datatype - Corey Masson - Jan 19, 2004
        sqlBoolean
    End Enum


#End Region

#Region "Public Constructors"
    Public Sub New()
        MyBase.New()

        mModuleName = Me.GetType.ToString

        'Modified by Balaji on 07/14/2009 as reports with more than 10000 records timed out
        'reason: ran the Lead Master Detail report query in SQL Server and sql server took 54 seconds to return results
        'The default command time out was set to 30 seconds and Advantage timed out as the query took more than 30 seconds to return results
        'so changing the timeout seconds from 30 seconds to 300 seconds (5 minutes)
        mCommandTimeout = 600 '30   ' seconds
    End Sub

    Public Sub New(ByVal connString As String)
        mConnectionString = ConfigurationManager.ConnectionStrings("ClientConnectionString").ConnectionString
    End Sub

#End Region

#Region "Public Properties"

    'ConnectionString
    Public Property ConnectionString() As String
        Get
            Try
                Return mConnection.ConnectionString
            Catch
                Return ""
            End Try
        End Get
        Set(ByVal Value As String)
            mConnectionString = ConfigurationManager.AppSettings("ClientConnectionString")

        End Set
    End Property

    Public ReadOnly Property Connection() As SqlConnection
        Get
            mConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("ClientConnectionString").ConnectionString)
            Return mConnection
        End Get
    End Property

    Public Property CommandTimeout() As Integer
        Get
            Return mCommandTimeout
        End Get
        Set(ByVal Value As Integer)
            ' only accept legitimate values - 0 is not allowed since we don't want unlimited command
            '	execution time, for security purposes
            If Value > 0 Then
                mCommandTimeout = Value
            End If
        End Set
    End Property

#End Region

#Region "Public Methods"
    Public Function StartTransaction() As SqlTransaction
        mConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("ClientConnectionString").ConnectionString)
        mConnection.Open()
        Return mConnection.BeginTransaction()
    End Function
#End Region

#Region "DataAccess Methods"

#Region "Close connection method"
    Public Sub CloseConnection()
        mConnection.Close()
    End Sub
#End Region

#Region "Open connection method"
    Public Sub OpenConnection()
        mConnection = New SQLConnection(ConfigurationManager.ConnectionStrings("ClientConnectionString").ConnectionString)
        mConnection.Open()
    End Sub
#End Region

#Region "RunSQLScalar - Standard SQL is run with ExecuteScalar"
    'The RunSQLScalar function accepts a SQL statement that is required 
    Public Function RunSQLScalar(ByVal SQL As String) As Object
        'Validate the SQL String to be larger than 10 characters
        ValidateSQLStatement(SQL)

        'We include all called object in the try block to catch any excepitons that could occur ( even in the creation of the connection)
        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it.")
            End If

            'Set a new Connecton
            mConnection = New SQLConnection(ConfigurationManager.ConnectionStrings("ClientConnectionString").ConnectionString)
            mConnection.Open()
            'Set a new Command that accepts an SQL statement and the connection. 
            'The command.commandtype does not have to be set since it defaults to text
            mCommand = New SQLCommand(SQL, mConnection)
            mCommand.CommandTimeout = mCommandTimeout

            'ExecuteScalar
            mObject = mCommand.ExecuteScalar
            Return mObject
        Catch ExceptionObject As Exception
            'Any exception will be logged through our private logexception function
            'LogException(ExceptionObject)
            'The exception is passed back to the calling code, with our custom message and specific exception information
            Throw New Exception(M_EXCEPTION_MESSAGE, ExceptionObject)
        Finally
            'Immediately Close the Connection after use to free up resources
            mConnection.Close()
        End Try
    End Function
#End Region

#Region "RunSQLDataSet - Standard SQL is run and returns a DataSet"

    'The runSQLDataSet function accepts a SQL statement that is required and an optional table name
    Public Function RunSQLDataSet(ByVal SQL As String, Optional ByVal TableName As String = Nothing, Optional ByVal conn As SqlConnection = Nothing) As DataSet

        'Validate the SQL String to be larger than 10 characters
        ValidateSQLStatement(SQL)

        'We include all called object in the try block to catch any excepitons that could occur ( even in the creation of the connection)
        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it.")
            End If

            'Set a new Connecton if one is not passed in
            If conn Is Nothing Then
                mConnection = New SQLConnection(ConfigurationManager.ConnectionStrings("ClientConnectionString").ConnectionString)
            Else
                mConnection = conn
            End If

            'Set a new Command that accepts an SQL statement and the connection. 
            'The command.commandtype does not have to be set since it defaults to text
            mCommand = New SQLCommand(SQL, mConnection)
            mCommand.CommandTimeout = mCommandTimeout

            'Set a new DataSet
            mDataSet = New DataSet
            'Set a new DataAdapter that will run the SQL statement
            mDataAdapter = New SQLDataAdapter(mCommand)
            'Depending on table name passed, Fill the new DataSet with the returned Data in a table
            If TableName = Nothing Then
                mDataAdapter.Fill(mDataSet)
            Else
                mDataAdapter.Fill(mDataSet, TableName)
            End If

            Return mDataSet

        Catch ExceptionObject As Exception
            'Any exception will be logged through our private logexception function
            'LogException(ExceptionObject)
            'The exception is passed back to the calling code, with our custom message and specific exception information
            Throw New Exception(M_EXCEPTION_MESSAGE, ExceptionObject)
        Finally
            'Immediately Close the Connection after use to free up resources
            mConnection.Close()
        End Try

    End Function
    Public Function RunSQLLeadMasterDetail(ByVal SQL As String, Optional ByVal TableName As String = Nothing) As dsLeadMasterDetail

        'Validate the SQL String to be larger than 10 characters
        ValidateSQLStatement(SQL)

        'We include all called object in the try block to catch any excepitons that could occur ( even in the creation of the connection)
        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it.")
            End If

            'Set a new Connecton
            mConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("ClientConnectionString").ConnectionString)
            'Set a new Command that accepts an SQL statement and the connection. 
            'The command.commandtype does not have to be set since it defaults to text
            mCommand = New SqlCommand(SQL, mConnection)
            mCommand.CommandTimeout = mCommandTimeout

            'Set a new DataSet
            Dim dsLead = New dsLeadMasterDetail
            'Set a new DataAdapter that will run the SQL statement
            mDataAdapter = New SqlDataAdapter(mCommand)
            'Depending on table name passed, Fill the new DataSet with the returned Data in a table
            If TableName = Nothing Then
                mDataAdapter.Fill(dsLead, "adLeads")
            Else
                mDataAdapter.Fill(dsLead, TableName)
            End If

            Return dsLead

        Catch ExceptionObject As Exception
            'Any exception will be logged through our private logexception function
            'LogException(ExceptionObject)
            'The exception is passed back to the calling code, with our custom message and specific exception information
            Throw New Exception(M_EXCEPTION_MESSAGE, ExceptionObject)
        Finally
            'Immediately Close the Connection after use to free up resources
            mConnection.Close()
        End Try

    End Function

#End Region

#Region "runSQLDataReader - Standard SQL is run and returns a DataReader"

    'The runSQLDataReader function accepts a SQL statement that is required
    Public Function RunSQLDataReader(ByVal SQL As String) As SqlDataReader

        'Validate the SQL String to be larger than 10 characters
        ValidateSQLStatement(SQL)

        'We include all called object in the try block to catch any excepitons that could occur ( even in the creation of the connection)
        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it.")
            End If

            'Set a new Connecton
            mConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("ClientConnectionString").ConnectionString)
            'Set a new Command that accepts an SQL statement and the connection. 
            'The command.commandtype does not have to be set since it defaults to text
            mCommand = New SqlCommand(SQL, mConnection)
            mCommand.CommandTimeout = mCommandTimeout

            'We need to open the connection for the DataReader explicitly
            mConnection.Open()
            'Run the Execute Reader method of the Command Object
            mDataReader = mCommand.ExecuteReader(CommandBehavior.CloseConnection)

            Return mDataReader

        Catch ExceptionObject As Exception
            'Any exception will be logged through our private logexception function
            'LogException(ExceptionObject)
            'If an exception occurs, close the connection now!
            mConnection.Close()
            'The exception is passed back to the calling code, with our custom message and specific exception information
            Throw New Exception(M_EXCEPTION_MESSAGE, ExceptionObject)
        End Try

    End Function

#End Region

#Region "RunParamSQLScalar - Parameterized SQL is run with ExecuteScalar"
    'The RunParamSQLScalar function accepts a SQL statement that is required
    Public Function RunParamSQLScalar(ByVal SQL As String, Optional ByVal tran As SqlTransaction = Nothing) As Object
        'Validate SQL statement
        ValidateSQLStatement(SQL)

        'Setting the objects to handle parameters
        Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
        Dim prParameter As sqlParameter            'will contain the converted SQLParameter
        'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
        Dim usedEnumerator As IEnumerator = mParameterList.GetEnumerator()

        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it")
            End If

            'Set a new connection
            If tran Is Nothing Then
                mConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("ClientConnectionString").ConnectionString)
            End If

            'Define the command object and set commandtype to process Stored Procedure
            mCommand = New SqlCommand(SQL, mConnection)
            mCommand.CommandTimeout = mCommandTimeout

            If Not (tran Is Nothing) Then
                mCommand.Transaction = tran
            End If
            'Move through the privateParameterList wiht the help of the enumerator
            Do While (usedEnumerator.MoveNext())
                'prUsedParameter = Nothing
                'Get parameter in privateParameterList
                prUsedParameter = usedEnumerator.Current
                'Convert paramter to SQLParameter
                prParameter = ConvertParameters(prUsedParameter)
                'Add converted parameter to the privateCommand object that imports data through the DataAdapter
                mCommand.Parameters.Add(prParameter)
            Loop

            If tran Is Nothing Then
                mConnection.Open()
            End If
            mObject = mCommand.ExecuteScalar

            Return mObject

            '   Changed by Anatoly on 3/22/2004.
            '   Catch only sqlExceptions
        Catch Ex As sqlException
            '   this error is specific for the sql Provider of MSSQL. 
            If Ex.ErrorCode = -2147217873 Then
                '   get error type
                'Select Case DALExceptions.ExceptionType(Ex)
                '    Case "547"  '   this is a referential integrity exception
                '        Throw New ReferentialIntegrityException("Referential Integrity Exception", Ex)
                '    Case "2601" '   this is a unique index violation exception
                '        Throw New UniqueIndexException("Unique Index Violation Exception", Ex)
                '    Case Else   '   do not intercept exception
                Throw Ex
                'End Select
                'Else
                '    Dim objEx As New FAME.ExceptionLayer.BaseException("DataAccess Layer Error:", Ex)
                '    Throw objEx
            End If
            '   end of changes by Anatoly

        Catch ExceptionObject As Exception
            'An exception will be logged thorugh our private logexception funciton
            'LogException(ExceptionObject)
            'The exception is passed back to the calling code, with our custom message and specific exception information
            mConnection.Close()
            Throw New Exception(M_EXCEPTION_MESSAGE, ExceptionObject)
        Finally
            If tran Is Nothing Then
                mConnection.Close()
            End If
        End Try
    End Function
#End Region

#Region "RunParamSQLDataSet - Parameterized SQL is run and returns a DataSet"
    'The RunParamSQLDataSet function accepts a SQL statement that is required and an optinal table name
    Public Function RunParamSQLDataSet(ByVal SQL As String, Optional ByVal TableName As String = Nothing, Optional ByVal TextType As String = "") As DataSet
        'Validate SQL statement
        ValidateSQLStatement(SQL)

        'Setting the objects to handle parameters
        Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
        Dim prParameter As sqlParameter            'will contain the converted SQLParameter
        'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
        Dim usedEnumerator As IEnumerator = mParameterList.GetEnumerator()

        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it")
            End If

            'Set a new connection and DataSet
            mConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("ClientConnectionString").ConnectionString)
            Dim prDataSet As New DataSet

            If mConnection.State = ConnectionState.Closed Then
                mConnection.Open()
            End If

            'Define the command object and set commandtype to process Stored Procedure
            mCommand = New SqlCommand(SQL, mConnection)
            If TextType = "SP" Then mCommand.CommandType = CommandType.StoredProcedure
            mCommand.CommandTimeout = mCommandTimeout

            'Move through the privateParameterList with the help of the enumerator
            Do While (usedEnumerator.MoveNext())
                'prUsedParameter = Nothing
                'Get parameter in privateParameterList
                prUsedParameter = usedEnumerator.Current
                'Convert paramter to SQLParameter
                prParameter = ConvertParameters(prUsedParameter)
                'Add converted parameter to the privateCommand object that imports data through the DataAdapter
                mCommand.Parameters.Add(prParameter)
            Loop
            'Have the DataAdapter run the Stored Procedure
            mDataAdapter = New SqlDataAdapter(mCommand)
            'Depending on table name passed, create the DataSet with or without specifically naming the table
            If TableName = Nothing Then
                mDataAdapter.Fill(prDataSet)
            Else
                mDataAdapter.Fill(prDataSet, TableName)
            End If

            Return prDataSet

        Catch exceptionObject As Exception
            'An exception will be logged thorugh our private logexception funciton
            'LogException(ExceptionObject)
            'The exception is passed to the calling code
            Debug.WriteLine(exceptionObject.Message)
            If (Not exceptionObject.InnerException Is Nothing) Then
                Debug.WriteLine(exceptionObject.InnerException.Message)
            End If
            Throw New Exception(M_EXCEPTION_MESSAGE, exceptionObject)
        Finally
            'Always close the connection as soon as possible(only then will object be allowed to go out of scope)
            If mConnection.State = ConnectionState.Open Then
                mConnection.Close()
            End If
        End Try
    End Function
    Public Function RunParamSQLDataSetUsingSP(ByVal SQL As String, Optional ByVal TableName As String = Nothing) As DataSet
        'Validate SQL statement
        'ValidateSQLStatement(SQL)

        'Setting the objects to handle parameters
        Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
        Dim prParameter As sqlParameter            'will contain the converted SQLParameter
        'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
        Dim usedEnumerator As IEnumerator = mParameterList.GetEnumerator()

        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it")
            End If

            'Set a new connection and DataSet
            mConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("ClientConnectionString").ConnectionString)
            Dim prDataSet As New DataSet

            'Define the command object and set commandtype to process Stored Procedure
            mCommand = New SqlCommand(SQL, mConnection)
            mCommand.CommandType = CommandType.StoredProcedure

            'Move through the privateParameterList with the help of the enumerator
            Do While (usedEnumerator.MoveNext())
                'prUsedParameter = Nothing
                'Get parameter in privateParameterList
                prUsedParameter = usedEnumerator.Current
                'Convert paramter to SQLParameter
                prParameter = ConvertParameters(prUsedParameter)
                'Add converted parameter to the privateCommand object that imports data through the DataAdapter
                mCommand.Parameters.Add(prParameter)
            Loop
            'Have the DataAdapter run the Stored Procedure
            mDataAdapter = New SqlDataAdapter(mCommand)
            'Depending on table name passed, create the DataSet with or without specifically naming the table
            If TableName = Nothing Then
                mDataAdapter.Fill(prDataSet)
            Else
                mDataAdapter.Fill(prDataSet, TableName)
            End If

            Return prDataSet

        Catch ExceptionObject As Exception
            'An exception will be logged thorugh our private logexception funciton
            'LogException(ExceptionObject)
            'The exception is passed to the calling code
            Throw New Exception(M_EXCEPTION_MESSAGE, ExceptionObject)
        Finally
            'Always close the connection as soon as possible(only then will object be allowed to go out of scope)
            mConnection.Close()
        End Try
    End Function
#End Region

#Region "RunParamSQLDataReader - Parameterized SQL is run and returns a DataReader"
    'The RunParamSQLDataReader function accepts a SQL statement that is requirede
    Public Function RunParamSQLDataReader(ByVal SQL As String) As SqlDataReader
        'Validate Stored Procedure
        ValidateSQLStatement(SQL)

        'Setting the objects to handle parameters
        Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
        Dim prParameter As sqlParameter            'will contain the converted SQLParameter
        'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
        Dim usedEnumerator As IEnumerator = mParameterList.GetEnumerator()

        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it")
            End If

            'Set a new connection and DataSet
            mConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("ClientConnectionString").ConnectionString)

            'Define the command object and set commandtype to process Stored Procedure
            mCommand = New SqlCommand(SQL, mConnection)
            mCommand.CommandTimeout = mCommandTimeout

            'Move through the privateParameterList wiht the help of the enumerator
            Do While (usedEnumerator.MoveNext())
                prUsedParameter = Nothing
                'Get parameter in privateParameterList
                prUsedParameter = usedEnumerator.Current
                'Convert paramter to SQLParameter
                prParameter = ConvertParameters(prUsedParameter)
                'Add converted parameter to the privateCommand object that imports data through the DataAdapter
                mCommand.Parameters.Add(prParameter)
            Loop

            mConnection.Open()
            mDataReader = mCommand.ExecuteReader(CommandBehavior.CloseConnection)

            Return mDataReader

        Catch ExceptionObject As Exception
            'An exception will be logged thorugh our private logexception funciton
            'LogException(ExceptionObject)
            'The exception is passed back to the calling code, with our custom message and specific exception information
            mConnection.Close()
            Throw New Exception(M_EXCEPTION_MESSAGE, ExceptionObject)
        End Try
    End Function
#End Region

#Region "RunParamSQLDataAdapter - Parameterized SQL is run and returns a DataAdapter"
    'The RunParamSQLDataSet function accepts a SQL statement that is required
    Public Function RunParamSQLDataAdapter(ByVal SQL As String) As SqlDataAdapter
        'Validate SQL statement
        ValidateSQLStatement(SQL)

        'Setting the objects to handle parameters
        Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
        Dim prParameter As sqlParameter            'will contain the converted SQLParameter
        'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
        Dim usedEnumerator As IEnumerator = mParameterList.GetEnumerator()

        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it")
            End If

            'This method should be used in conjuntion with the constructor that 
            'specifies DataAdapter and opens the connection. At this point the connection
            'should be already opened.

            'Define the command object and set commandtype to process Stored Procedure
            mCommand = New SqlCommand(SQL, mConnection)
            mCommand.CommandTimeout = mCommandTimeout

            'Move through the privateParameterList with the help of the enumerator
            Do While (usedEnumerator.MoveNext())
                'prUsedParameter = Nothing
                'Get parameter in privateParameterList
                prUsedParameter = usedEnumerator.Current
                'Convert paramter to SQLParameter
                prParameter = ConvertParameters(prUsedParameter)
                'Add converted parameter to the privateCommand object that imports data through the DataAdapter
                mCommand.Parameters.Add(prParameter)
            Loop
            'Have the DataAdapter run the SQL Statement
            mDataAdapter = New SqlDataAdapter(mCommand)

            Return mDataAdapter

        Catch ExceptionObject As Exception
            'An exception will be logged thorugh our private logexception funciton
            'LogException(ExceptionObject)
            'The exception is passed to the calling code
            Throw New Exception(M_EXCEPTION_MESSAGE, ExceptionObject)
        Finally
            'Always close the connection as soon as possible(only then will object be allowed to go out of scope)
            'm_Connection.Close()
        End Try
    End Function
#End Region

#Region "Execute a none returning parameterized query"
    Public Sub RunParamSQLExecuteNoneQuery(ByVal SQL As String, Optional ByVal tran As SqlTransaction = Nothing, _
                                           Optional ByVal TextType As String = "", _
                                           Optional ByVal TargetDB As String = "")
        'Validate Stored Procedure
        ValidateSQLStatement(SQL)

        'Setting the objects to handle parameters
        Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
        Dim prParameter As sqlParameter            'will contain the converted SQLParameter
        'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
        Dim usedEnumerator As IEnumerator = mParameterList.GetEnumerator()

        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it")
            End If

            'Set a new connection and DataSet
            If tran Is Nothing Then
                If TargetDB = "Tenant" Then
                    mConnection = New SqlConnection("Provider=SQLsql;" & ConfigurationManager.ConnectionStrings("TenantAuthDBConnection").ToString)
                Else
                    mConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("ClientConnectionString").ConnectionString)
                End If
            End If

            'Define the command object and set commandtype to process Stored Procedure
            mCommand = New SqlCommand(SQL, mConnection)
            If TextType = "SP" Then mCommand.CommandType = CommandType.StoredProcedure
            mCommand.CommandTimeout = mCommandTimeout

            If Not (tran Is Nothing) Then
                mCommand.Transaction = tran
            End If
            'Move through the privateParameterList wiht the help of the enumerator
            Do While (usedEnumerator.MoveNext())
                'prUsedParameter = Nothing
                'Get parameter in privateParameterList
                prUsedParameter = usedEnumerator.Current
                'Convert paramter to SQLParameter
                prParameter = ConvertParameters(prUsedParameter)
                'Add converted parameter to the privateCommand object that imports data through the DataAdapter
                mCommand.Parameters.Add(prParameter)
            Loop

            If tran Is Nothing Then
                mConnection.Open()
            End If
            mCommand.ExecuteNonQuery()

            '   Changed by Anatoly on 2/27/2004.
            '   Catch only sqlExceptions
        Catch Ex As sqlException
            '   this error is specific for the sql Provider of MSSQL. 
            If Ex.ErrorCode = -2147217873 Then
                '   get error type
                'Select Case DALExceptions.ExceptionType(Ex)
                '    Case "547"  '   this is a referential integrity exception
                '        Throw New ReferentialIntegrityException("Referential Integrity Exception", Ex)
                '    Case "2601" '   this is a unique index violation exception
                '        Throw New UniqueIndexException("Unique Index Violation Exception", Ex)
                '    Case Else   '   do not intercept exception
                Throw Ex
                'End Select
                'Else
                '    Dim objEx As New FAME.ExceptionLayer.BaseException("DataAccess Layer Error:", Ex)
                '    Throw objEx
            Else
                Throw Ex
            End If

            '   end of changes by Anatoly

        Catch exceptionObject As Exception
            'An exception will be logged thorugh our private logexception funciton
            'LogException(ExceptionObject)
            'The exception is passed back to the calling code, with our custom message and specific exception information

            Dim objEx As New BaseException("DataAccess Layer Error:", exceptionObject)
            Throw objEx
        Finally
            If tran Is Nothing Then
                mConnection.Close()
            End If
        End Try
    End Sub

#End Region

#Region "RunSchemaInfo - Runs a SQL regular statement and returns a Datatable"
    'The RunSchemaInfoForAuditing function accepts a SQL statement that is required
    Public Function RunSchemaInfo(ByVal SQL As String) As DataTable
        Dim dtSchema As DataTable
        'Validate the SQL String to be larger than 10 characters
        ValidateSQLStatement(SQL)

        'We include all called object in the try block to catch any excepitons that could occur ( even in the creation of the connection)
        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it.")
            End If

            'Set a new Connecton.
            mConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("ClientConnectionString").ConnectionString)

            'Set a new Command that accepts an SQL statement and the connection. 
            'The command.commandtype does not have to be set since it defaults to text
            mCommand = New SqlCommand(SQL, mConnection)
            mCommand.CommandTimeout = mCommandTimeout

            'We need to open the connection for the DataReader explicitly
            mConnection.Open()
            'Run the Execute Reader method of the Command Object
            mDataReader = mCommand.ExecuteReader(CommandBehavior.KeyInfo Or CommandBehavior.SchemaOnly)
            dtSchema = mDataReader.GetSchemaTable
            Return dtSchema

        Catch exceptionObject As Exception
            'Any exception will be logged through our private logexception function
            'LogException(ExceptionObject)
            'The exception is passed back to the calling code, with our custom message and specific exception information
            Throw New Exception(M_EXCEPTION_MESSAGE, exceptionObject)
        Finally
            mConnection.Close()
        End Try

    End Function
#End Region

#End Region
#Region "Execute a non returning parameterized query"
    Public Function RunParamFLSQLExecuteNoneQuery(ByVal SQL As String, Optional ByVal tran As SqlTransaction = Nothing) As Object
        'Validate Stored Procedure
        ValidateSQLStatement(SQL)
        'RunParamFLSQLExecuteNoneQuery = Nothing

        'Setting the objects to handle parameters
        Dim prUsedParameter As Parameter        'will return the specific parameter in the privateParameterList
        Dim prParameter As sqlParameter       'will contain the converted SQLParameter
        'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
        Dim usedEnumerator As IEnumerator = mParameterList.GetEnumerator()
        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it")
            End If

            'Set a new connection and DataSet
            If tran Is Nothing Then mConnection = New SQLConnection(ConfigurationManager.ConnectionStrings("ClientConnectionString").ConnectionString)

            'Define the command object and set commandtype to process Stored Procedures
            mCommand = New SQLCommand(SQL, mConnection)
            mCommand.CommandTimeout = mCommandTimeout

            If Not (tran Is Nothing) Then mCommand.Transaction = tran

            'Move through the privateParameterList with the help of the enumerator
            Do While (usedEnumerator.MoveNext())
                'prUsedParameter = Nothing
                'Get parameter in privateParameterList
                prUsedParameter = usedEnumerator.Current
                'Convert paramter to SQLParameter
                prParameter = ConvertParameters(prUsedParameter)
                'Add converted parameter to the privateCommand object that imports data through the DataAdapter
                mCommand.Parameters.Add(prParameter)
            Loop

            If tran Is Nothing Then mConnection.Open()
            RunParamFLSQLExecuteNoneQuery = mCommand.ExecuteNonQuery()

            '   Catch only sqlExceptions
        Catch ex As sqlException
            Throw
            '   this error is specific for the sql Provider of MSSQL. 
            'If Ex.ErrorCode = -2147217873 Then
            '    Throw Ex
            'Else
            '    Throw Ex
            'End If
        Catch exceptionObject As Exception
            Dim objEx As New Exception("clsDataAccess Layer Error:", exceptionObject)
            Throw objEx
        Finally
            If tran Is Nothing Then mConnection.Close()
        End Try
    End Function
#End Region
#Region "AddParameter Public Method and Support functions"

    Public Sub AddParameter(ByVal parameterName As String, _
                            Optional ByVal value As Object = Nothing, _
                            Optional ByVal OleType As SqlDbType = Nothing, _
                            Optional ByVal Size As Integer = Nothing, _
                            Optional ByVal Direction As ParameterDirection = ParameterDirection.Input)

        Dim buildDataType As SqlDbType
        Dim buildParameter As Parameter = Nothing
        Select Case OleType
            Case SqlDbType.VarChar
                buildDataType = SqlDbType.VarChar
            Case SqlDbType.Char
                buildDataType = SqlDbType.Char
            Case SqlDbType.Int
                buildDataType = SqlDbType.Int
            Case SqlDbType.DateTime
                buildDataType = SqlDbType.Date
            Case SqlDbType.Decimal
                buildDataType = SqlDbType.Decimal
            Case SqlDbType.Money
                buildDataType = SqlDbType.Money
                ' added sqlDataType Guid - Corey Masson - Jan 7, 2004
            Case SqlDbType.UniqueIdentifier
                buildDataType = SqlDbType.UniqueIdentifier
                'added sqlDataType Boolean - Corey Masson - Jan 19, 2004
            Case SqlDbType.Bit
                buildDataType = SqlDbType.Bit
        End Select

        buildParameter = New Parameter(parameterName, value, buildDataType, Size, Direction)
        mParameterList.Add(buildParameter)

    End Sub

    Public Class Parameter
        Public ParameterName As String
        Public ParameterValue As Object
        Public ParameterDataType As sqlDataType
        Public ParameterSize As Integer
        Public ParameterDirectionUsed As ParameterDirection

        Sub New(ByVal passedParameterName As String, _
                Optional ByVal passedValue As Object = Nothing, _
                Optional ByVal passedSQLDBType As SqlDbType = Nothing, _
                Optional ByVal passedSize As Integer = Nothing, _
                Optional ByVal passedDirection As ParameterDirection = ParameterDirection.Input)

            ParameterName = passedParameterName
            ParameterValue = passedValue
            ParameterDataType = passedSQLDBType
            ParameterSize = passedSize
            ParameterDirectionUsed = passedDirection

        End Sub

    End Class

    Private Function ConvertParameters(ByVal passedParameter As Parameter) As SqlParameter

        Dim returnsqlParameter As sqlParameter = New sqlParameter

        returnsqlParameter.ParameterName = passedParameter.ParameterName
        returnsqlParameter.Value = passedParameter.ParameterValue
        returnsqlParameter.SQLDBType = passedParameter.ParameterDataType
        returnsqlParameter.Size = passedParameter.ParameterSize
        returnsqlParameter.Direction = passedParameter.ParameterDirectionUsed

        Return returnsqlParameter

    End Function

    Public Sub ClearParameters()
        Try
            mParameterList.Clear()
        Catch parameterException As Exception
            Throw New Exception(M_EXCEPTION_MESSAGE & " Parameter List did not clear", parameterException)
        End Try
    End Sub

#End Region

#Region "Exception Logging"
    'We call our eventlog by passing it the exception we caught
    'Private Sub LogException(ByRef ExceptionObject As Exception)

    '    Dim eventLogMessage As String           'this is the Message we will pass to the log

    '    Try
    '        'Create the Message to be passed from the exception 
    '        eventLogMessage = "An error occured in the following module: " & m_ModuleName & _
    '                          " The Source was: " & ExceptionObject.Source & vbCrLf & _
    '                          " With the Message: " & ExceptionObject.Message & vbCrLf & _
    '                          " Stack Tace: " & ExceptionObject.StackTrace & vbCrLf & _
    '                          " Target Site: " & ExceptionObject.TargetSite.ToString
    '        'Define the Eventlog as an Application Log entry
    '        'Dim localEventLog As New EventLog("Application")
    '        'Write the entry to the Application Event log, using this Module's name, the message an make it an error message with an ID of 55
    '        EventLog.WriteEntry(m_ModuleName, eventLogMessage, EventLogEntryType.Error, 55)
    '    Catch eventLogException As Exception
    '        'If the eventlog fails (like forgetting to make the ASPNET user account a member of the debugger group, we pass the error
    '        Throw New Exception(m_ExceptionMessage & " - EventLog Error: " & eventLogException.Message, eventLogException)
    '    End Try

    'End Sub

#End Region

#Region "Validations"
    Private Sub ValidateSQLStatement(ByRef sqlStatement As String)
        'SQL Statement must be at least 10 characters ( "Select * form x" )
        If Len(sqlStatement) < 10 Then
            Throw New Exception(M_EXCEPTION_MESSAGE & " The SQL Statement must be provided and at least 10 characters long")
        End If
    End Sub

#End Region

#Region "Overloaded Dispose and Finalize"
    Public Sub Dispose() Implements IDisposable.Dispose
        If (Not mDisposedBoolean) Then
            'Call cleanup code in Finalize
            finalize()
            'Record that object has been disposed
            mDisposedBoolean = True

            'Finalize does not need to be called.
            GC.SuppressFinalize(Me)
        End If
    End Sub

    Protected Overrides Sub finalize()
        'Perform cleanup code here
        If Not mConnection Is Nothing Then
            'm_Connection.Dispose()
            mConnection = Nothing
        End If
    End Sub
#End Region

    'Private Function GetAdvAppSettings() As PortalAdvAppSettings
    '    'Dim MyAdvAppSettings As PortalAdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        myAdvAppSettings = New PortalAdvAppSettings
    '    End If
    '    Return myAdvAppSettings
    'End Function
End Class




