Imports FAME.Advantage.Common

Public Class InputMasksDB
    Private Function GetSQL(ByVal tableName As String, ByVal zipLen As Integer) As String
        Dim strTemplate As String

        If tableName <> "faLenders" Then
            strTemplate = "SELECT COUNT(*) " & _
                                  "FROM %TableName% " & _
                                  "WHERE LEN(Zip) = %ZipLen% " & _
                                  "AND ForeignZip = 0"

        Else
            strTemplate = "SELECT COUNT(*) " & _
                          "FROM %TableName% " & _
                          "WHERE LEN(PayZip) = %ZipLen% " & _
                          "OR LEN(Zip) = %ZipLen% " & _
                          "AND ForeignAddress = 0 " & _
                          "OR ForeignPayAddress = 0"
        End If

        'Replace the tokens with the values passed in
        strTemplate = strTemplate.Replace("%TableName%", tableName)
        strTemplate = strTemplate.Replace("%ZipLen%", zipLen)

        Return strTemplate

    End Function

    Public Function GetInputMasks() As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT InputMaskId, Item, Mask, ModUser, ModDate ")
            .Append("FROM syInputMasks ")
        End With

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Function GetInputMaskForItem(ByVal itemId As Integer) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim s As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        With sb
            .Append("SELECT Mask ")
            .Append("FROM syInputMasks ")
            .Append("WHERE InputMaskId = ?")
        End With

        db.AddParameter("@itemid", itemId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
        s = CType(db.RunParamSQLScalar(sb.ToString), String)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return s

    End Function

    Public Sub UpdateInputMask(ByVal inputMaskId As Integer, ByVal mask As String)
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        With sb
            .Append("UPDATE syInputMasks ")
            .Append("SET Mask = ? ")
            .Append("WHERE InputMaskId = ?")
        End With

        db.AddParameter("@mask", mask, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@inputmaskid", inputMaskId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    End Sub

    Public Function UpdateInputMaskInfo(ByVal iMaskInfo As InputMaskInfo, ByVal user As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        Try
            With sb
                .Append("UPDATE syInputMasks ")
                .Append("SET Mask = ?, ModUser = ?, ModDate = ? ")
                .Append("WHERE InputMaskId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("SELECT COUNT(*) FROM syInputMasks WHERE ModDate = ? and InputMaskId = ? ")
            End With

            db.AddParameter("@Mask", iMaskInfo.Mask, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   InputMaskId
            db.AddParameter("@InputMaskId", iMaskInfo.InputMaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Original ModDate
            db.AddParameter("@Original_ModDate", iMaskInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   InputMaskId
            db.AddParameter("@InputMaskId", iMaskInfo.InputMaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            'db.RunParamSQLExecuteNoneQuery(sb.ToString)
            'db.ClearParameters()

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Function UpdateInputMaskDS(ByVal ds As DataSet) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   all updates must be encapsulated as one transaction
        Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
        connection.Open()

        Try
            '   build the sql query for the StudentAwardSchedules data adapter
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT InputMaskId, Item, Mask, ModUser, ModDate FROM syInputMasks")
            End With

            '   build select command
            Dim InputMaskCommand As New OleDbCommand(sb.ToString, connection)

            '   Create adapter to handle syInputMasks table
            Dim InputMaskDataAdapter As New OleDbDataAdapter(InputMaskCommand)

            '   build insert, update and delete commands for syInputMasks table
            Dim cb As New OleDbCommandBuilder(InputMaskDataAdapter)

            '   insert added rows in syInputMasks table
            InputMaskDataAdapter.Update(ds.Tables("InputMasks").Select(Nothing, Nothing, DataViewRowState.Added))

            '   delete rows in syInputMasks table
            InputMaskDataAdapter.Update(ds.Tables("InputMasks").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   update rows in syInputMasks table
            InputMaskDataAdapter.Update(ds.Tables("InputMasks").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   return no errors
            Return ""

        Catch ex As OleDb.OleDbException
            '   something went wrong
            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Catch ex As Exception
            '   something went wrong
            '   return error message
            Return ex.Message

        Finally

            '   close connection
            connection.Close()
        End Try
    End Function

    Public Function GetCountOfRecordsWithSpecifiedZipLength(ByVal tableName As String, ByVal zipLen As Integer) As Integer
        Dim strSQL As String
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        strSQL = GetSQL(tableName, zipLen)

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Return CInt(db.RunParamSQLScalar(strSQL))

    End Function

End Class
