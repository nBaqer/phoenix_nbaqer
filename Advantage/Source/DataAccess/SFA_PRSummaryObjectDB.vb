Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFA_PRSummaryObjectDB

	Public Shared Function GetReportDatasetRaw(ByVal RptParamInfo As ReportParamInfoIPEDS) As DataSet
		' get list of students to include, based on report parameters
		Dim StudentList As String = IPEDSDB.GetStudentList(RptParamInfo)

		' if there are no students to include, return empty dataset
		If StudentList = "" Then
			Return New DataSet
		End If

		Dim sb As New System.Text.StringBuilder
		Dim dsRaw As New DataSet
		Dim db As DataAccess = IPEDSDB.DataAccessIPEDS()

		' get students and data for report Part 1
		With sb
			.Append("SELECT DISTINCT arStudent.StudentId, ")

			' get count of Financial Aid payments made to the student during date range described 
			'	by report parameters
			.Append("(SELECT COUNT(*) ")
			.Append("	FROM arStuEnrollments, faStudentAwards, faStudentAwardSchedule, saPmtDisbRel, saTransactions ")
			.Append("	WHERE arStuEnrollments.StuEnrollId = faStudentAwards.StuEnrollId AND ")
			.Append("	      faStudentAwards.StudentAwardId = faStudentAwardSchedule.StudentAwardId AND ")
			.Append("	      faStudentAwardSchedule.AwardScheduleId = saPmtDisbRel.AwardScheduleId AND ")
			.Append("	      saPmtDisbRel.TransactionId = saTransactions.TransactionId AND ")
			.Append("	      arStuEnrollments.StudentId = arStudent.StudentId AND ")
			.Append("	      saTransactions.TransDate >= '" & FmtRptDateParam(RptParamInfo.CohortStartDate) & "' AND ")
            .Append("	      saTransactions.TransDate <= '" & FmtRptDateParam(RptParamInfo.RptEndDate) & "'")
            .Append("         and saTransactions.Voided=0 ")
			.Append(") AS FinAidCount ")
            .Append("FROM arStudent ")

			' append list of appropriate students to include
			.Append("WHERE arStudent.StudentId IN (" & StudentList & ") ")

			' get list of enrollments and relevant info for same list of students
            .Append(";" & IPEDSDB.GetSQL_EnrollmentInfo(StudentList, RptParamInfo.FilterProgramIDs))
        End With

		' run query and andd returned data to raw DataSet
		dsRaw = db.RunSQLDataSet(sb.ToString)
		With dsRaw
			.Tables(0).TableName = TblNameStudents
			.Tables(1).TableName = TblNameEnrollments
		End With

		' get students and data for report Part 2
		With sb
			.Remove(0, .Length)

			.Append("SELECT ")

			' retrieve all needed columns
			.Append("arStuEnrollments.StudentId, ")

			' cast the FundSourceId as VarChar so that it may be used in string variables
			'	in code which processes raw data
			.Append("LOWER(CAST(saFundSources.FundSourceId AS VarChar(100))) AS FundSourceId, ")

			.Append("saFundSources.TitleIV, ")
			.Append("(SELECT saAwardTypes.AwardTypeId ")
			.Append("	FROM saAwardTypes ")
			.Append("	WHERE saAwardTypes.AwardTypeId = saFundSources.AwardTypeId ")
			.Append(") AS AwardTypeId, ")
			.Append("(SELECT saAwardTypes.Descrip ")
			.Append("	FROM saAwardTypes  ")
			.Append("	WHERE saAwardTypes.AwardTypeId = saFundSources.AwardTypeId  ")
			.Append(") AS AwardTypeDescrip, ")
			.Append("faStudentAwardSchedule.Amount ")
			.Append("FROM ")
			.Append("arStuEnrollments, faStudentAwards, saFundSources, ")
			.Append("faStudentAwardSchedule, saPmtDisbRel, saTransactions ")

			' establish necessary relationships
			.Append("WHERE ")
			.Append("arStuEnrollments.StuEnrollId = faStudentAwards.StuEnrollId AND ")
			.Append("faStudentAwards.StudentAwardId = faStudentAwardSchedule.StudentAwardId AND ")
			.Append("faStudentAwards.AwardTypeId = saFundSources.FundSourceId AND ")
			.Append("faStudentAwardSchedule.AwardScheduleId = saPmtDisbRel.AwardScheduleId AND ")
			.Append("saPmtDisbRel.TransactionId = saTransactions.TransactionId AND ")
            .Append("saTransactions.Voided=0 AND ")

			' ensure only get payments made to the student during date range described by report parameters
			.Append("saTransactions.TransDate >= '" & FmtRptDateParam(RptParamInfo.CohortStartDate) & "' AND ")
			.Append("saTransactions.TransDate <= '" & FmtRptDateParam(RptParamInfo.RptEndDate) & "' AND ")

			' append list of appropriate students to include
			.Append("arStuEnrollments.StudentId IN (" & StudentList & ") ")

			' sort by Student, Fund Source, and Award Type
			.Append("ORDER BY ")
			.Append("arStuEnrollments.StudentId, saFundSources.FundSourceId, saAwardTypes.AwardTypeId")
		End With

		' run query and add returned data to raw DataSet
		dsRaw.Tables.Add(db.RunSQLDataSet(sb.ToString, "Awards").Tables(0).Copy)

		' return raw dataset
		Return dsRaw

	End Function

End Class
