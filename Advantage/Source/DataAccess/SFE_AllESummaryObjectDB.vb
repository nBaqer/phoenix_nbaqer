Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFE_AllESummaryObjectDB

	Public Shared Function GetReportDatasetRaw(ByVal rptParamInfo As ReportParamInfoIPEDS) As DataSet
		' get list of students to include, based on report parameters
		Dim StudentList As String = IPEDSDB.GetStudentList(rptParamInfo)

		' if there are no students to include, return empty dataset
		If StudentList = "" Then
			Return New DataSet
		End If

		Dim sb As New System.Text.StringBuilder
		Dim dsRaw As New DataSet

		' get Students
		With sb
			.Append("SELECT DISTINCT arStudent.StudentId, ")

			' retrieve all other needed columns
			.Append("(SELECT syRptAgencyFldValues.RptAgencyFldValId ")
			.Append("	FROM adGenders, ")
			.Append("		 syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
			.Append("	WHERE adGenders.GenderId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("	      syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("	      syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("	      syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("	      syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
			.Append("	      adGenders.GenderId = arStudent.Gender) AS RptAgencyFldValId_Gender, ")
			.Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
			.Append("	FROM adGenders, ")
			.Append("		 syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
			.Append("	WHERE adGenders.GenderId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("	      syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("	      syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("	      syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("	      syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
			.Append("	      adGenders.GenderId = arStudent.Gender) AS GenderDescrip, ")
			.Append("(SELECT syRptAgencyFldValues.RptAgencyFldValId ")
			.Append("	FROM adEthCodes, ")
			.Append("		 syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
			.Append("	WHERE adEthCodes.EthCodeId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("	      syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("	      syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("	      syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("	      syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
			.Append("	      adEthCodes.EthCodeId = arStudent.Race) AS RptAgencyFldValId_EthCode, ")
			.Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
			.Append("	FROM adEthCodes, ")
			.Append("		 syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
			.Append("	WHERE adEthCodes.EthCodeId = syRptAgencySchoolMapping.SchoolDescripId AND ")
			.Append("	      syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
			.Append("	      syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
			.Append("	      syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
			.Append("	      syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
			.Append("	      adEthCodes.EthCodeId = arStudent.Race) AS EthCodeDescrip ")
			.Append("FROM arStudent ")

			' append list of appropriate students to include
			.Append("WHERE arStudent.StudentId IN (" & StudentList & ") ")

			' apply sort on Gender and Race
			.Append("ORDER BY RptAgencyFldValId_Gender, RptAgencyFldValId_EthCode ")

			' get list of enrollments and relevant info for same list of students
            .Append(";" & IPEDSDB.GetSQL_EnrollmentInfo(StudentList, rptParamInfo.FilterProgramIDs))
		End With

		' run query, add returned data to raw dataset
		dsRaw = IPEDSDB.DataAccessIPEDS().RunSQLDataSet(sb.ToString)

		' set table names for each part of returned data
		With dsRaw
			.Tables(0).TableName = TblNameStudents
			.Tables(1).TableName = TblNameEnrollments
		End With

		'return the raw dataset
		Return dsRaw

	End Function

End Class
