Imports FAME.Advantage.Common

Public Class StuLedgerDB

#Region "Private Data Members"
    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")

#End Region


#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

#End Region


#Region "Public Methods"

    Public Function GetStudentLedger(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strWhere As String
        Dim strOrderBy As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= "AND " & paramInfo.FilterList
        End If

        'Get StuEnrollId from paramInfo.FilterOther
        If paramInfo.FilterOther <> "" Then
            strWhere &= "AND " & paramInfo.FilterOther
        End If

        'Order By Clause.
        If paramInfo.OrderBy <> "" Then
            strOrderBy &= " ORDER BY " & paramInfo.OrderBy
        End If

        Dim strNewWhere As String = strWhere
        If (strWhere.ToUpper.IndexOf("TRANSDATE") > 0) Then
            strNewWhere = strWhere.Substring(0, strWhere.ToUpper.Substring(0, strWhere.ToUpper.IndexOf("TRANSDATE") - 1).LastIndexOf("AND"))
            strWhere = strWhere.Replace(strNewWhere, "")
        Else
            strWhere = ""
        End If
        If StudentIdentifier = "SSN" Then
            With sb
                .Append("SELECT arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
                .Append("arStudent.StudentId,arStudent.SSN AS StudentIdentifier,arStuEnrollments.StuEnrollId, ")
                '.Append("upper(CampDescrip) as  CampDescrip,(select count(CampusId) from SyCampuses) as CampusCount ")
                ''Count (campusid) changed to count(*) By Saraswathi to prevent sql2000 error
                ''.Append("upper(CampDescrip) as  CampDescrip,(select count(*) from SyCampuses) as CampusCount ")
                .Append("upper(CampDescrip) as  CampDescrip,(select count(*) from SyCampuses) as CampusCount ," & IIf(paramInfo.ShowRptCosts = True, "1", "0") & " as ShowCosts ," & IIf(paramInfo.ShowRptExpectedFunding = True, "1", "0") & " as ShowExpectedFunding," & IIf(paramInfo.ShowRptCategoryBreakdown = True, "1", "0") & " as ShowCategoryBreakdown  ")
                ''Added By Vijay Ramteke on Feb 08, 2010 For Mantis Id 17900
                .Append(" FROM arStuEnrollments,arStudent,syCampuses ")
                .Append("WHERE arStudent.StudentId=arStuEnrollments.StudentId ")
                .Append(" AND syCampuses.CampusId=arStuEnrollments.CampusId and syCampuses.CampusId='" & paramInfo.CampusId & "' ")
                .Append(strNewWhere)
                .Append(strOrderBy)
            End With
        ElseIf StudentIdentifier = "EnrollmentId" Then
            With sb
                .Append("SELECT arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
                .Append("arStudent.StudentId,arStuEnrollments.EnrollmentId AS StudentIdentifier,arStuEnrollments.StuEnrollId, ")
                '.Append(" upper(CampDescrip) as  CampDescrip,(select count(CampusId) from SyCampuses) as CampusCount   ")
                ''Count (campusid) changed to count(*) By Saraswathi to prevent sql2000 error
                ''.Append("upper(CampDescrip) as  CampDescrip,(select count(*) from SyCampuses) as CampusCount ")
                .Append("upper(CampDescrip) as  CampDescrip,(select count(*) from SyCampuses) as CampusCount ," & IIf(paramInfo.ShowRptCosts = True, "1", "0") & " as ShowCosts ," & IIf(paramInfo.ShowRptExpectedFunding = True, "1", "0") & " as ShowExpectedFunding," & IIf(paramInfo.ShowRptCategoryBreakdown = True, "1", "0") & " as ShowCategoryBreakdown  ")
                ''Added By Vijay Ramteke on Feb 08, 2010 For Mantis Id 17900
                .Append(" FROM arStuEnrollments,arStudent,syCampuses ")
                .Append("WHERE arStudent.StudentId=arStuEnrollments.StudentId ")
                .Append(" AND syCampuses.CampusId=arStuEnrollments.CampusId and syCampuses.CampusId='" & paramInfo.CampusId & "' ")
                .Append(strNewWhere)
                .Append(strOrderBy)
            End With
        ElseIf StudentIdentifier = "StudentId" Then
            With sb
                .Append("SELECT arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
                .Append("arStudent.StudentId,arStudent.StudentNumber AS StudentIdentifier,arStuEnrollments.StuEnrollId, ")
                '  .Append(" upper(CampDescrip)  as  CampDescrip,(select count(CampusId) from SyCampuses) as CampusCount  ")
                ''Count (campusid) changed to count(*) By Saraswathi to prevent sql2000 error
                ''.Append("upper(CampDescrip) as  CampDescrip,(select count(*) from SyCampuses) as CampusCount ")
                .Append("upper(CampDescrip) as  CampDescrip,(select count(*) from SyCampuses) as CampusCount ," & IIf(paramInfo.ShowRptCosts = True, "1", "0") & " as ShowCosts ," & IIf(paramInfo.ShowRptExpectedFunding = True, "1", "0") & " as ShowExpectedFunding," & IIf(paramInfo.ShowRptCategoryBreakdown = True, "1", "0") & " as ShowCategoryBreakdown  ")
                ''Added By Vijay Ramteke on Feb 08, 2010 For Mantis Id 17900
                .Append(" FROM arStuEnrollments,arStudent,syCampuses ")
                .Append("WHERE arStudent.StudentId=arStuEnrollments.StudentId ")
                .Append(" AND syCampuses.CampusId=arStuEnrollments.CampusId and syCampuses.CampusId='" & paramInfo.CampusId & "' ")
                .Append(strNewWhere)
                .Append(strOrderBy)
            End With
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        Try
            Dim dtStuInfo As DataTable = ds.Tables(0)
            If dtStuInfo.Rows.Count > 0 Then
                dtStuInfo.TableName = "StudentInformation"
                dtStuInfo.Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
                dtStuInfo.Columns.Add(New DataColumn("Balance", System.Type.GetType("System.Decimal")))
                dtStuInfo.Columns.Add(New DataColumn("StuEnrollIdStr", System.Type.GetType("System.String")))
                '
                Dim stuId As String = dtStuInfo.Rows(0)("StudentId").ToString
                Dim stuEnrollId As String

                If dtStuInfo.Rows.Count > 1 Then
                    stuEnrollId = Guid.Empty.ToString
                Else
                    stuEnrollId = dtStuInfo.Rows(0)("StuEnrollId").ToString
                End If
                '
                Dim objTrans As New TransactionsDB
                ' Get Student Legder
                Dim ds2 As DataSet = objTrans.GetStudentLedger(stuId, stuEnrollId, Guid.Empty.ToString, strWhere)

                For Each dr As DataRow In ds2.Tables(0).Rows
                    If dr("RecordType") < 0 Then
                        dr.Delete()
                    End If
                Next
                ds2.AcceptChanges()
                ds.Tables.Add(ds2.Tables(0).Copy)

                Dim dtStuLedger As DataTable = ds.Tables(1)
                dtStuLedger.TableName = "StudentLedger"

                dtStuLedger.Columns.Add(New DataColumn("Charge", System.Type.GetType("System.Decimal")))
                dtStuLedger.Columns.Add(New DataColumn("Payment", System.Type.GetType("System.Decimal")))
                dtStuLedger.Columns.Add(New DataColumn("StuEnrollIdStr", System.Type.GetType("System.String")))
                '
                '    If dtStuLedger.Rows.Count > 0 Then
                '        Dim lastRow As Integer = dtStuLedger.Rows.Count - 1
                '        '
                '        'Change sign of Payment so it always prints positive.
                '        For Each dr As DataRow In dtStuLedger.Rows
                '            dr("StuEnrollIdStr") = dr("StuEnrollId").ToString
                '            '
                '            If Not (dr("TransAmount") Is System.DBNull.Value) Then
                '                If dr("TransAmount") > 0 Then
                '                    dr("Charge") = dr("TransAmount")
                '                Else
                '                    dr("Payment") = dr("TransAmount") * (-1)
                '                End If
                '            End If
                '        Next
                '        '
                '        'Set up student name as: "LastName, FirstName MI."
                '        Dim stuName As String
                '        For Each dr As DataRow In dtStuInfo.Rows
                '            dr("StuEnrollIdStr") = dr("StuEnrollId").tostring
                '            stuName = dr("LastName")
                '            If Not (dr("FirstName") Is System.DBNull.Value) Then
                '                If dr("FirstName") <> "" Then
                '                    stuName &= ", " & dr("FirstName")
                '                End If
                '            End If
                '            If Not (dr("MiddleName") Is System.DBNull.Value) Then
                '                If dr("MiddleName") <> "" Then
                '                    stuName &= " " & dr("MiddleName") & "."
                '                End If
                '            End If
                '            dr("StudentName") = stuName
                '            dr("Balance") = Convert.ToDecimal(dtStuLedger.Rows(lastRow)("Balance"))
                '        Next
                '        '
                '    Else
                '        'If no transactions were found, then clear the Student Info. collection of rows. 
                '        dtStuInfo.Rows.Clear()
                '    End If
                'Added by Vijay Ramteke on Feb 02, 2010
                'To fetch the Expected Funding
                Dim dsExpextedFunding As DataSet = GetStudentExpectedFunding(stuId, stuEnrollId)
                ds.Tables.Add(dsExpextedFunding.Tables(0).Copy)
                'To fetch the Cost
                ''''Code Commented by Kamalesh Ahuja on 14 April 2010 to fix mantis id 18484 '''
                'Added by Vijay Ramteke on Feb 05, 2010
                'Dim strDate As String
                'If strWhere <> "" Then
                '    strDate = strWhere.Substring(strWhere.ToUpper.LastIndexOf("AND") + 3, strWhere.ToUpper.Length - strWhere.ToUpper.LastIndexOf("AND") - 3)
                'Else
                '    strDate = "'" & Date.Now.ToString("MM/dd/yyyy") & "'"
                'End If
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim dsCost As DataSet = GetStudentCosts(stuId, stuEnrollId, strWhere)
                ds.Tables.Add(dsCost.Tables(0).Copy)
                'Added by Vijay Ramteke on Feb 05, 2010
                'To fetch the Cost

                'To fetch the Category Breakdown
                'Added by Vijay Ramteke on Feb 08, 2010
                Dim dsCategoryBreakdown As DataSet = GetStudentCategoryBreakdown(stuId, stuEnrollId, strWhere)
                ds.Tables.Add(dsCategoryBreakdown.Tables(0).Copy)
                'Added by Vijay Ramteke on Feb 08, 2010
                'To fetch the Category Breakdown
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    Public Function GetStudentExpectedFunding(ByVal strStudentId As String, ByVal strStuEnrollId As String) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        'Added by Vijay Ramteke on Feb 02, 2010
        'To fetch the Expected Funding
        With sb
            .Append(" Select LastName, FirstName,MiddleName, StudentIdentifier, Status, PrgVerDescrip, FundSource, StuEnrollId, ExpectedDate, SUM(Amount-DisbAmount) as Amount From ( ")
            .Append(" SELECT arStudent.LastName, arStudent.FirstName, arStudent.MiddleName, arStudent.SSN, syStatusCodes.StatusCodeDescrip AS Status, ")
            .Append(" arPrgVersions.PrgVerDescrip, saFundSources.FundSourceDescrip AS FundSource, ")
            If StudentIdentifier = "SSN" Then
                .Append(" arStudent.SSN AS StudentIdentifier, ")
            ElseIf StudentIdentifier = "EnrollmentId" Then
                .Append(" arStuEnrollments.EnrollmentId AS StudentIdentifier, ")
            ElseIf StudentIdentifier = "StudentId" Then
                .Append(" arStudent.StudentNumber AS StudentIdentifier , ")
            End If
            .Append(" faStudentAwardSchedule.ExpectedDate, faStudentAwardSchedule.Amount, ")
            .Append(" isnull((SELECT SUM(saPmtDisbRel.Amount) FROM saPmtDisbRel RIGHT OUTER JOIN saTransactions ON saTransactions.TransactionId = saPmtDisbRel.TransactionId ")
            .Append(" WHERE (faStudentAwardSchedule.AwardScheduleId = saPmtDisbRel.AwardScheduleId) AND (saTransactions.Voided = 0)),0) AS DisbAmount, arStuEnrollments.StuEnrollId ")
            .Append(" FROM faStudentAwards INNER JOIN arStudent INNER JOIN arStuEnrollments ON arStudent.StudentId = arStuEnrollments.StudentId ON ")
            .Append(" faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId INNER JOIN syStatusCodes ON arStuEnrollments.StatusCodeId = syStatusCodes.StatusCodeId INNER JOIN ")
            .Append(" arPrgVersions ON arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId INNER JOIN saFundSources ON faStudentAwards.AwardTypeId = saFundSources.FundSourceId INNER JOIN ")
            .Append(" faStudentAwardSchedule ON faStudentAwards.StudentAwardId = faStudentAwardSchedule.StudentAwardId ")
            If Not strStuEnrollId = Guid.Empty.ToString Then
                .Append(" WHERE  arStuEnrollments.StuEnrollId='" & strStuEnrollId & "'")
            Else
                .Append(" WHERE  arStuEnrollments.StuEnrollId IN (Select StuEnrollId from arStuEnrollments where StudentId='" & strStudentId & "')")
            End If
            .Append("  Union  ")
            .Append(" SELECT arStudent.LastName, arStudent.FirstName, arStudent.MiddleName, arStudent.SSN, syStatusCodes.StatusCodeDescrip AS Status, ")
            .Append(" arPrgVersions.PrgVerDescrip, 'Payment Plan' AS FundSource, ")
            If StudentIdentifier = "SSN" Then
                .Append(" arStudent.SSN AS StudentIdentifier, ")
            ElseIf StudentIdentifier = "EnrollmentId" Then
                .Append(" arStuEnrollments.EnrollmentId AS StudentIdentifier, ")
            ElseIf StudentIdentifier = "StudentId" Then
                .Append(" arStudent.StudentNumber AS StudentIdentifier , ")
            End If
            .Append(" faStuPaymentPlanSchedule.ExpectedDate, faStuPaymentPlanSchedule.Amount,isnull((SELECT SUM(saPmtDisbRel.Amount) FROM saPmtDisbRel RIGHT OUTER JOIN saTransactions ON saTransactions.TransactionId = saPmtDisbRel.TransactionId ")
            .Append(" WHERE (faStuPaymentPlanSchedule.PayPlanScheduleId = saPmtDisbRel.PayPlanScheduleId) AND (saTransactions.Voided = 0)),0) AS DisbAmount, arStuEnrollments.StuEnrollId ")
            .Append(" FROM faStudentPaymentPlans INNER JOIN arStudent INNER JOIN arStuEnrollments ON arStudent.StudentId = arStuEnrollments.StudentId INNER JOIN ")
            .Append(" syStatusCodes ON arStuEnrollments.StatusCodeId = syStatusCodes.StatusCodeId INNER JOIN ")
            .Append(" arPrgVersions ON arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId ON faStudentPaymentPlans.StuEnrollId = arStuEnrollments.StuEnrollId INNER JOIN ")
            .Append(" faStuPaymentPlanSchedule ON faStudentPaymentPlans.PaymentPlanId = faStuPaymentPlanSchedule.PaymentPlanId LEFT OUTER JOIN ")
            .Append(" saTransactions  INNER JOIN saPmtDisbRel  ON saTransactions.TransactionId = saPmtDisbRel.TransactionId ON faStuPaymentPlanSchedule.PayPlanScheduleId = saPmtDisbRel.PayPlanScheduleId ")
            If Not strStuEnrollId = Guid.Empty.ToString Then
                .Append(" WHERE  arStuEnrollments.StuEnrollId='" & strStuEnrollId & "'")
            Else
                .Append(" WHERE  arStuEnrollments.StuEnrollId IN (Select StuEnrollId from arStuEnrollments where StudentId='" & strStudentId & "')")
            End If

            .Append(" ) X Group by LastName, FirstName,MiddleName, StudentIdentifier,Status, PrgVerDescrip, FundSource, StuEnrollId, ExpectedDate ")
            .Append(" Order by PrgVerDescrip, FundSource, LastName, FirstName, MiddleName, StudentIdentifier, ExpectedDate ")
        End With

        Dim ds As DataSet = db.RunSQLDataSet(sb.ToString, "ExpectedFunding")
        Dim dtTemp As New DataTable("ExpectedFunding")
        dtTemp = ds.Tables(0).Clone()
        dtTemp.Clear()
        Dim drNonZeroAmt() As DataRow = ds.Tables("ExpectedFunding").Select("Amount<>0", "FundSource,ExpectedDate")

        For Each dr As DataRow In drNonZeroAmt
            dtTemp.ImportRow(dr)
        Next
        ds.Tables.RemoveAt(0)
        ds.Tables.Add(dtTemp)

        Return ds
        'Added by Vijay Ramteke on Feb 02, 2010
    End Function
    Public Function GetStudentCosts(ByVal strStudentId As String, ByVal strStuEnrollId As String, ByVal strWhere As String) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        'Added by Vijay Ramteke on Feb 05, 2010
        'To fetch the Costs
        With sb
            .Append(" Select X.LastName, X.FirstName, X.MiddleName, X.SSN, X.Status, X.PrgVerDescrip, X.StuEnrollId, X.StudentIdentifier, SUM(X.TotalCost) AS TotalCost, SUM(X.ChargeTillDate) AS ChargeTillDate, SUM(X.PaymentsTillDate) AS PaymentsTillDate, SUM(X.AccountBalance) AS AccountBalance FROM ( ")
            .Append(" SELECT arStudent.LastName, arStudent.FirstName, arStudent.MiddleName, arStudent.SSN, '' AS Status, ")
            .Append(" '' AS PrgVerDescrip, '' As StuEnrollId, ")
            If StudentIdentifier = "SSN" Then
                .Append(" arStudent.SSN AS StudentIdentifier, ")
            ElseIf StudentIdentifier = "EnrollmentId" Then
                .Append(" '' AS StudentIdentifier, ")
            ElseIf StudentIdentifier = "StudentId" Then
                .Append(" arStudent.StudentNumber AS StudentIdentifier , ")
            End If
            .Append(" Isnull(arPrgVersions.TotalCost,0) as TotalCost, ")
            '.Append(" isnull((Select Sum(TransAmount) from saTransactions Where saTransactions.StuEnrollId=arStuEnrollments.StuEnrollId and Voided=0 and (TransTypeId=0 Or TransTypeId=1) and TransAmount > 0 and TransDate<= " & strDate & "),0) As ChargeTillDate, ")
            '.Append(" isnull((Select Sum(TransAmount) from saTransactions Where saTransactions.StuEnrollId=arStuEnrollments.StuEnrollId and Voided=0 and (TransTypeId=2 Or TransTypeId=1) and TransAmount < 0 and TransDate<= " & strDate & "),0) As PaymentsTillDate, ")

            '' Code modified by Kamalesh Ahuja on April 14, 2010 to fix Mantis Issue Id 18484
            .Append(" isnull((Select Sum(TransAmount) from saTransactions Where saTransactions.StuEnrollId=arStuEnrollments.StuEnrollId and Voided=0 and (TransTypeId=0 Or TransTypeId=1) and TransAmount > 0 " & strWhere & "),0) As ChargeTillDate, ")
            .Append(" isnull((Select Sum(TransAmount) from saTransactions Where saTransactions.StuEnrollId=arStuEnrollments.StuEnrollId and Voided=0 and (TransTypeId=2 Or TransTypeId=1) and TransAmount < 0 " & strWhere & "),0) As PaymentsTillDate, ")
            '' New Filed added by Vijay Ramteke on April 07, 2010
            .Append(" isnull((Select Sum(TransAmount) from saTransactions Where saTransactions.StuEnrollId=arStuEnrollments.StuEnrollId and Voided=0 " & strWhere & "),0) As AccountBalance ")
            '' New Filed added by Vijay Ramteke on April 07, 2010
            '''''''''''''''''''''''''''''''''''''''''''''''''''            
            .Append(" FROM arStudent INNER JOIN arStuEnrollments ON arStudent.StudentId = arStuEnrollments.StudentId ")
            .Append(" INNER JOIN syStatusCodes ON arStuEnrollments.StatusCodeId = syStatusCodes.StatusCodeId ")
            .Append(" INNER JOIN arPrgVersions ON arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId  ")
            If Not strStuEnrollId = Guid.Empty.ToString Then
                .Append(" WHERE  arStuEnrollments.StuEnrollId='" & strStuEnrollId & "'")
            Else
                .Append(" WHERE  arStuEnrollments.StuEnrollId IN (Select StuEnrollId from arStuEnrollments where StudentId='" & strStudentId & "')")
            End If
            .Append(" ) X ")
            .Append(" Group by X.PrgVerDescrip, X.LastName, X.FirstName, X.MiddleName, X.StuEnrollId, X.StudentIdentifier, X.SSN, X.Status Order by X.PrgVerDescrip ")
        End With

        Dim ds As DataSet = db.RunSQLDataSet(sb.ToString, "Costs")

        Return ds
        'Added by Vijay Ramteke on Feb 05, 2010
    End Function
    Public Function GetStudentCategoryBreakdown(ByVal strStudentId As String, ByVal strStuEnrollId As String, ByVal strWhere As String) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        'Added by Vijay Ramteke on Feb 05, 2010
        'To fetch the Category Breakdown
        With sb
            .Append(" SELECT X.Description, SUM(X.ChargedAmount) as ChargedAmount, SUM(X.Received) as Received, SUM((-1)*X.CreaditAdj) as CreaditAdj, SUM(X.DebitAdj) as DebitAdj, X.PrgVerDescrip, X.StudentIdentifier, X.LastName, X.FirstName, X.MiddleName, X.StuEnrollId FROM ")
            .Append(" (SELECT arStudent.LastName, arStudent.FirstName, arStudent.MiddleName, arStuEnrollments.StuEnrollId, arPrgVersions.PrgVerDescrip,saTransCodes.TransCodeDescrip AS Description, ")
            If StudentIdentifier = "SSN" Then
                .Append(" arStudent.SSN AS StudentIdentifier, ")
            ElseIf StudentIdentifier = "EnrollmentId" Then
                .Append(" arStuEnrollments.EnrollmentId AS StudentIdentifier, ")
            ElseIf StudentIdentifier = "StudentId" Then
                .Append(" arStudent.StudentNumber AS StudentIdentifier , ")
            End If
            .Append(" ISNULL((SELECT SUM(TransAmount) FROM saTransactions TI INNER JOIN saTransCodes TCI ON TI.TransCodeId=TCI.TransCodeId Where TI.StuEnrollId=arStuEnrollments.StuEnrollId and TI.Voided=0 and TI.TransTypeId=0 and TI.TransactionId=saTransactions.TransactionId ),0) As ChargedAmount, ")
            '.Append(" ISNULL((SELECT SUM(Amount) FROM saAppliedPayments API INNER JOIN saTransactions TI ON TI.TransactionId=API.ApplyToTransId INNER JOIN saTransCodes TCI ON TI.TransCodeId=TCI.TransCodeId WHERE  TI.StuEnrollId=arStuEnrollments.StuEnrollId and TI.Voided=0 and TI.TransTypeId=2 and TI.TransactionId=saTransactions.TransactionId),0) As Received, ")
            .Append(" ISNULL((SELECT SUM(TransAmount) FROM saTransactions TI INNER JOIN saTransCodes TCI ON TI.TransCodeId=TCI.TransCodeId Where TI.StuEnrollId=arStuEnrollments.StuEnrollId and TI.Voided=0 and TI.TransTypeId=2 and TI.TransactionId=saTransactions.TransactionId ),0) As Received, ")
            .Append(" ISNULL((SELECT SUM(TransAmount) FROM saTransactions TI INNER JOIN saTransCodes TCI ON TI.TransCodeId=TCI.TransCodeId Where TI.StuEnrollId=arStuEnrollments.StuEnrollId and TI.Voided=0 and TI.TransTypeId=1 And TI.TransAmount<0 and TI.TransactionId=saTransactions.TransactionId ),0) As CreaditAdj, ")
            .Append(" ISNULL((SELECT SUM(TransAmount) FROM saTransactions TI INNER JOIN saTransCodes TCI ON TI.TransCodeId=TCI.TransCodeId Where TI.StuEnrollId=arStuEnrollments.StuEnrollId and TI.Voided=0 and TI.TransTypeId=1 And TI.TransAmount>0 and TI.TransactionId=saTransactions.TransactionId),0) As DebitAdj ")
            .Append(" FROM arStudent INNER JOIN arStuEnrollments ON arStudent.StudentId=arStuEnrollments.StudentId ")
            .Append(" INNER JOIN arPrgVersions ON arStuEnrollments.PrgVerId=arPrgVersions.PrgVerId ")
            .Append(" INNER JOIN saTransactions ON saTransactions.StuEnrollId=arStuEnrollments.StuEnrollId ")
            .Append(" INNER JOIN saTransCodes ON saTransactions.TransCodeId=saTransCodes.TransCodeId  ")
            If Not strStuEnrollId = Guid.Empty.ToString Then
                .Append(" WHERE  arStuEnrollments.StuEnrollId='" & strStuEnrollId & "'")
            Else
                .Append(" WHERE  arStuEnrollments.StuEnrollId IN (Select StuEnrollId from arStuEnrollments where StudentId='" & strStudentId & "')")
            End If
            .Append(" ) X  ")
            .Append(" Group by X.Description, X.PrgVerDescrip, X.LastName, X.FirstName, X.MiddleName, X.StuEnrollId, X.StudentIdentifier Order by X.Description ")
        End With

        Dim ds As DataSet = db.RunSQLDataSet(sb.ToString, "CategoryBreakdown")

        Return ds
        'Added by Vijay Ramteke on Feb 05, 2010
    End Function
#End Region

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

End Class
