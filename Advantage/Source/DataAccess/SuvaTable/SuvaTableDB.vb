﻿' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' SuvaTable.vb
'
' Data Access Logic for Get Suva types from Catalog. 
'
' This class will be used to retrieve/update data related to Table arSuvaType.
' ===============================================================================
' Copyright (C) 2003-2013 FAME Inc.
' All rights reserved.
' ===============================================================================
Imports FAME.Advantage.Common

Namespace SuvaTable

    Public Class SuvaTableDB

        Public Function GetAllFromSuvaCatalog() As IList(Of SuvaCat)

            'Create the returned list
            Dim catList As IList(Of SuvaCat) = New List(Of SuvaCat)

            'create the command Get Conexion String
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            'Create The query
            Dim sql As String = "SELECT ProgramVersionTypeId, codeProgramVersionType, DescriptionProgramVersionType FROM arProgramVersionType"

            'create the reader
            Dim reader As OleDbDataReader = db.RunSQLDataReader(sql)
            Try
                'Fill the rsuvacat
                If reader.HasRows Then
                    Do While reader.Read()
                        Dim cat As SuvaCat = SuvaCat.Factory()
                        cat.IdSuva = reader.GetInt32(0)
                        cat.Code = reader.GetString(1)
                        cat.Description = reader.GetString(2)
                        catList.Add(cat)
                    Loop
                End If
            Finally
                reader.Close()
            End Try

            ' Return values
            return catlist

        End Function

    End Class

End Namespace
