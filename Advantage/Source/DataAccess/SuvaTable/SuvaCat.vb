﻿Namespace SuvaTable

    ''' <summary>
    ''' Summary description for DDItemClassDto
    ''' </summary>
    Public Class SuvaCat

        #Region "Factory"

        Public Shared Function Factory() As SuvaCat
            Dim dd = New SuvaCat()
            Return dd
        End Function

        #End Region

        #Region "Private Fields"
        Private mIdSuva As Integer
        Private mCode As String
        Private mDescription As String
        #End Region

        #Region "Properties"

        Public Property IdSuva() As Integer
            Get
                Return mIdSuva
            End Get
            Set(value As Integer)
                mIdSuva = value
            End Set
        End Property

        Public Property Code() As String
            Get
                Return mCode
            End Get
            Set(value As String)
                mCode = value
            End Set
        End Property

        Public Property Description() As String
            Get
                Return mDescription
            End Get
            Set(value As String)
                mDescription = value
            End Set
        End Property

        #End Region

    End Class

End Namespace
