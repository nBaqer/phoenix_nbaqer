Imports FAME.Advantage.Common

Public Class CopyClsSectDB
    Public Function GetDropDownList() As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da1 As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder
        With strSQLString
            .Append("SELECT t1.TermId,t1.TermDescrip ")
            .Append("FROM arTerm t1 ")
            '.Append("WHERE t2.Status = 'Active' and t1.StatusId = t2.StatusId ")
        End With
        db.OpenConnection()
        da1 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Try
            da1.Fill(ds, "TermDT")
        Catch ex As Exception

        End Try
        strSQLString.Remove(0, strSQLString.Length)

        Dim da7 As New OleDbDataAdapter

        With strSQLString
            .Append("SELECT a.CampusId, a.CampDescrip ")
            .Append("FROM syCampuses a ")
        End With
        da7 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Try
            da7.Fill(ds, "CampusDT")
        Catch ex As Exception

        End Try
        strSQLString.Remove(0, strSQLString.Length)

        Dim da6 As New OleDbDataAdapter

        With strSQLString
            .Append("SELECT a.EmpId, a.LastName, a.FirstName ")
            .Append("FROM hrEmployees a ")
            .Append("WHERE a.Faculty = 1 ")
        End With
        da6 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Try
            da6.Fill(ds, "InstructorDT")
        Catch ex As Exception

        End Try

        strSQLString.Remove(0, strSQLString.Length)
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetStartEndDates(ByVal term As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim ds As DataSet

        '   build the sql query
        With sb
            .Append("select a.StartDate,a.EndDate ")
            .Append("FROM   arTerm a ")
            .Append("WHERE a.TermId = ? ")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@TermId", term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)
        '   return dataset

        db.CloseConnection()

        Return ds
    End Function
    Public Function GetDataGridInfo(ByVal Term As String, ByVal Term2 As String, ByVal CampusId As String) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim da As New OleDbDataAdapter
        Dim ds As New DataSet
        ''  Dim count As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT b.ClsSectionId, b.TermId, b.ReqId, b.InstructorId, b.GrdScaleId,b.ShiftId, b.StartDate, b.EndDate,b.ClsSection,b.MaxStud,c.Code,c.Descrip,d.TermDescrip, ")
            .Append("(Select FullName from syUsers where UserId = b.InstructorId) as FullName, ")
            .Append(" ISNULL(TCP.AllowEarlyIn,0) AS AllowEarlyIn, ISNULL(TCP.AllowLateOut,0) AS AllowLateOut, ")
            .Append("  ISNULL(TCP.AllowExtraHours,0) AS AllowExtraHours, ISNULL(TCP.CheckTardyIn,0) AS CheckTardyIn, ")
            .Append(" ISNULL(TCP.MaxInBeforeTardy,'1899-12-30 00:00:00.000') AS MaxInBeforeTardy, ISNULL(TCP.AssignTardyInTime,'1899-12-30 00:00:00.000') AS AssignTardyInTime ")
            .Append("FROM  arClassSections b ")
            .Append(" INNER JOIN arReqs c ON c.ReqId = b.ReqId ")
            .Append(" INNER JOIN arTerm d ON b.TermId = d.TermId ")
            .Append(" LEFT OUTER JOIN arClsSectionTimeClockPolicy TCP ON TCP.ClsSectionId = b.ClsSectionId ")
            .Append("WHERE b.TermId = ?  ")
            .Append("AND b.CampusId = ? ")
            .Append(" and  b.ClsSectionId not in ")
            .Append("(select e.ClsSectionId from arClassSections e,arClassSections a, arReqs c, arTerm d WHERE  ")
            .Append(" e.Reqid=a.Reqid and e.ClsSection=a.ClsSection and  e.TermId = ? and ")
            .Append(" a.TermId = ?  and ")
            .Append(" a.ReqId = c.ReqId  and a.TermId = d.TermId AND a.CampusId = ? ) order by c.Descrip ")
            '.Append(" (SELECT a.ReqId FROM  arClassSections a, arReqs c, arTerm d WHERE ")
            '.Append(" a.TermId = ? ")
            '.Append(" and a.ReqId = c.ReqId ")
            '.Append(" and a.TermId = d.TermId AND a.CampusId = ? ) order by c.Descrip ")

        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@term1", Term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campus", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@term1", Term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@term2", Term2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campus2", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   Execute the query       
        db.OpenConnection()
        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "Unschedule")

        Catch ex As Exception

        End Try
        sb.Remove(0, sb.Length)
        db.ClearParameters()

        'Close Connection
        db.CloseConnection()
        Return ds
    End Function

    Public Function GetCourseCount(ByVal Term As String, ByVal Term2 As String, ByVal CampusId As String) As Integer
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim count As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT count(*) as count ")
            .Append(" FROM  arClassSections b1, arReqs c1, arTerm d1 ")
            .Append("WHERE b1.TermId = ? and b1.ReqId = c1.ReqId and b1.TermId = d1.TermId ")
            If CampusId <> "" Then
                .Append("AND b1.CampusId = ? ")
            End If
            .Append(" and  b1.ClsSection not in ")
            .Append(" (SELECT a2.ClsSection FROM  arClassSections a2, arReqs c2, arTerm d2 WHERE ")
            .Append(" a2.TermId = ? ")
            .Append(" and a2.ReqId = c2.ReqId ")
            .Append(" and a2.TermId = d2.TermId ")
            .Append("  AND c1.ReqId=c2.ReqId ")
            If CampusId <> "" Then
                .Append(" AND a2.CampusId = ? ) ")
            End If

        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@term1", Term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If CampusId <> "" Then
            db.AddParameter("@campus", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        db.AddParameter("@term2", Term2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If CampusId <> "" Then
            db.AddParameter("@campus2", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        '   Execute the query       
        db.OpenConnection()
        count = db.RunParamSQLScalar(sb.ToString)

        sb.Remove(0, sb.Length)
        db.ClearParameters()

        'Close Connection
        db.CloseConnection()
        Return count
    End Function
End Class
