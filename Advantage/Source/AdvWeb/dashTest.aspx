﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="dashTest.aspx.vb" Inherits="dashTest" %>

<%--<%@ Register TagPrefix="fame" TagName="StatusBar" Src="~/StatusBar.ascx" %>--%>
<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script type="text/javascript" src="Scripts/Advantage.Client.js"></script>
    <!-- 
	<script type="text/javascript" src="Scripts/Storage/storageCache.js"></script>
	<script type="text/javascript" src="Scripts/FileSaver.js"></script>
	<script type="text/javascript" src="Scripts/common-util.js"></script>
	<script type="text/javascript" src="Scripts/DataSources/transactionDataSource.js"></script>
	<script type="text/javascript" src="Scripts/viewModels/transactions.js"></script>
	<script type="text/javascript"  src="Scripts/common/kendoControlSetup.js"></script>
		-->
    <link href="css/AD/dashboard.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ValidationSummary ID="AdvantageErrorDisplay" runat="server"
        EnableViewState="true" Style="margin: 10px;" ValidationGroup="Main" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:HiddenField runat="server" ID="hdnUserId" ClientIDMode="Static" />
    <!-- Begin dashboard -->
    <div id="dashboardWrap">
        <!-- Dashboard Tool-bar -->
        <div id="dash-toolbar" class="k-toolbar dashtoolbar"></div>
        <!-- Dashboard two columns -->
        <div id="dashScrollbar">
            <div id="fameDashboard">
                <div class="panel-wrap">
                    <div id="left-content"></div>
                    <div id="right-content"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Configuration Windows -->
    <div id="dash-window-config" style="display: none">
        <div id="dash-config-tab">
            <ul>
                <li class="k-state-active">Select Widgets</li>
                <li>Options</li>
            </ul>
            <div>
                <table id="dashWidgetGrid">
                    <colgroup>
                        <col class="photo" />
                        <col class="details" />
                        <col />
                    </colgroup>
                    <thead>
                        <tr>
                            <th class="dashconfigcolumn1">
                                <input type="checkbox" id="dash-config-checkall" />
                            </th>
                            <th class="dashconfigcolumn2" >Widget Image</th>
                            <th>Widget Descriptions</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!-- Options Tab -->
            <div>
                <div class="paneldashoptionconfig k-block">
                    <div class="k-header">Select your default tab in your Dashboard </div>

                    <div id="dash-config-optioncbox" class="dash-config-cssdropdown">
                        <input id="dash-config-optioncb" />
                    </div>
                </div>

                <div style="height: 450px; display: inline-block"></div>

            </div>
        </div>

        <!-- Panel lower -->
        <div id="paneldashconfig" class="paneldashconfig k-block">

            <div style="float: right">
                <button id="dash-config-ok" type="button" class="k-button buttondashconfig">OK</button>
                <button id="dash-config-clear" type="button" class="k-button buttondashconfig">Clear</button>
                <button id="dash-config-cancel" type="button" class="k-button buttondashconfig">Cancel</button>

            </div>
        </div>
    </div>

    <script id="dashWidgetGrid-template" type="text/x-kendo-template">
	<tr>
	  <td class="dashconfigcolumn1">
     <input class="checkbox" type="checkbox" #= wSelected? "checked=checked":"" #>
     </td>
			<td class="dashconfigcolumn2">
				<img src="#: wImage #" >
			</td>
			<td >
				<div><b>Name: </b> #: wCode #</div>
				<div><b>Module:</b> #: wClassif # </div>
				<div><b>Description:</b> #: wDescription # </div>
			</td> 
  </tr>
    </script>

    <script id="dashWidgetGrid-alttemplate" type="text/x-kendo-template">
		<tr class='k-alt'>
	   <td class="dashconfigcolumn1">
      <input class="checkbox" type="checkbox" #= wSelected? "checked=checked":"" #>
     </td>
			<td class="dashconfigcolumn2">
				<img src="#: wImage #" >
			</td>
			<td>
				<div><b>Name: </b> #: wCode #</div>
				<div><b>Module:</b> #: wClassif # </div>
				<div><b>Description:</b> #: wDescription # </div>
			</td> 
  </tr>
    </script>


    <script>
        $(document).ready(function () {
            var manager = new dash.DashBoard();

        });

    </script>
</asp:Content>

