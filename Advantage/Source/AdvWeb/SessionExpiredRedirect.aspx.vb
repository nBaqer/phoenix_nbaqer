﻿Imports System.Diagnostics

Partial Class SessionExpiredRedirect
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblApplicationHeaderVersion.Text = "Advantage - " & GetBuildVersion()
        Session.Abandon()
    End Sub
    Protected Sub lnkLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLogin.Click
        Session.Abandon()
        Response.Redirect(FormsAuthentication.LoginUrl)
    End Sub
    Private Function GetBuildVersion() As String
        Dim VersionString As String = String.Empty
        Try
            'Dim MyAssemblyName As AssemblyName = AssemblyName.GetAssemblyName(Server.MapPath("~/Bin/Common.dll"))
            'VersionString = MyAssemblyName.Version.ToString()
            Dim myFileVersionInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Server.MapPath("~/Bin/Common.dll"))
            VersionString = myFileVersionInfo.FileVersion.ToString()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            VersionString = String.Empty
        End Try
        Return VersionString
    End Function
End Class
