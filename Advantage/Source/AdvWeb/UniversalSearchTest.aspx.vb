﻿
Imports System.Activities.Expressions

Partial Class UniversalSearchTest
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim te As HiddenField = Nothing
        Dim val As String

        Master.ShowHideStatusBarControl(True)


        te = CType(Page.Master.FindControl("hdnCurrentEntityId"), HiddenField)
        If te IsNot Nothing Then
            val = te.Value
            AdvantageSession.MasterStudentId = te.Value
            lblEntityId.Text = te.Value
        End If

        te = CType(Page.Master.FindControl("hdnCurrentEntityName"), HiddenField)
        If te IsNot Nothing Then
            val = te.Value
            AdvantageSession.MasterName = te.Value
            lblEntityName.Text = te.Value
        End If



    End Sub
End Class
