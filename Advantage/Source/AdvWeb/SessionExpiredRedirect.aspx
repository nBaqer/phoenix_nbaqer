﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SessionExpiredRedirect.aspx.vb" Inherits="SessionExpiredRedirect" %>
<%@ Register Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Session Expired</title>
    <link rel="Stylesheet" type="text/css" href="css/Login.css" />
</head>
<body>
  <form id="form1" runat="server" enableviewstate="False">
    <!-- <telerik:RadScriptManager ID="scriptmanager1" runat="server"></telerik:RadScriptManager> -->
      <div class="clwyPageHeader"> 
           
             <div class="clwyToolbarHeaderInfo">           
            <div class="clwyToolbarHeaderVersionInfo">
                <asp:Label ID="lblApplicationHeaderVersion" runat="server" EnableViewState="False" ViewStateMode="Disabled" />
            </div>
            <div class="clwyToolbarHeaderActionsInfo">&nbsp;</div>
        </div>
        <div class="clwyLoginContent">
            <table width="60%" align="center">
                <tr>
                    <td align="left">
                        <asp:Label ID="lblText" runat="server" style=" font-family:Arial; font-size:15pt; font-weight:bold; text-align:left; color:Navy"  EnableViewState="False" ViewStateMode="Disabled">Session Expired</asp:Label>
                    </td>
                </tr>
                <tr height="20">
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label1" runat="server" 
                            style=" font-family:Arial; font-size:10pt; font-weight:normal; text-align:left;" 
                            EnableViewState="False" ViewStateMode="Disabled">You have been redirected to this page due to inactivity.</asp:Label>
                    </td>
                </tr>
                 <tr height="20">
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label2" runat="server" 
                            style=" font-family:Arial; font-size:10pt; font-weight:normal; text-align:left;" 
                            EnableViewState="False" ViewStateMode="Disabled">Would you like to visit the following page?</asp:Label>
                    </td>
                </tr>
                 <tr height="5">
                    <td>
                        &nbsp;
                    </td>
                </tr>
                 <tr>
                    <td align="left">
                        <asp:LinkButton ID="lnkLogin" runat="server" Text="Login" PostBackUrl="#" 
                            EnableViewState="False" ViewStateMode="Disabled"></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </div>
    </div>
      <div class="clwyLoginPageFooterLabel">
        <table Height="40" width="100%" style="background-color:#3C62A0; padding-bottom:2px;">
        <tr>
            <td>
                <asp:Label ID="lblfooter" runat="server" EnableViewState="False" ViewStateMode="Disabled" style="width:250px;margin:auto;color:white;font-size:x-small;">
                    Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>
                </asp:Label>
            </td>
        </tr>
     </table>
        </div>
 
    </form>
</body>
</html>
