﻿
Imports FAME.Advantage.Common

Partial Class Reports_ReportHome
    Inherits BasePage

    Protected MyAdvAppSettings As AdvAppSettings
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        If Not Page.IsPostBack Then
            If Not Session("UserName") Is Nothing Then
                hdnUserName.Value = Session("UserName").ToString()
                hdnUserId.Value = Session("UserId").ToString()
            End If

            If (Not MyAdvAppSettings.AppSettings("ShowNACCASReports") Is Nothing) Then

                If Not String.IsNullOrEmpty(MyAdvAppSettings.AppSettings("ShowNACCASReports").ToString.ToLower) Then
                    hdnShowNACCASReports.Value = MyAdvAppSettings.AppSettings("ShowNACCASReports").ToString.ToLower
                Else
                    hdnShowNACCASReports.Value = "no"
                End If
            Else
                hdnShowNACCASReports.Value = "no"
            End If


            For Each tag As HtmlMeta In From tag1 In Page.Header.Controls.OfType(Of HtmlMeta)() Where (tag1.Name = "compat_mode")
                tag.Content = "IE-Edge"
            Next

            AdvantageSession.PageBreadCrumb = "Reports Home /"
        End If
    End Sub

End Class
