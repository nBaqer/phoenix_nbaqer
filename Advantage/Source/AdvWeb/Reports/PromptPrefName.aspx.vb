' ===============================================================================
' PromptPrefName
' Prompts the user to enter a preference name
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
' Paramters
'   s   : starting value to populate the pref name

Partial Class Reports_PromptPrefName
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ' initialize any startup value
            txtValue.Text = Request.Params("s")

            Dim js As New StringBuilder()
            js.Append("var strData=document.getElementById('txtValue').value;" & vbCrLf)
            js.Append("if (strData==null || strData=='') " + vbCrLf)
            js.Append("     window.alert('Please enter a value'); " + vbCrLf)
            js.Append("else { " + vbCrLf)
            js.Append("     window.returnValue=strData; " + vbCrLf)
            js.Append("     window.close();" + vbCrLf)
            js.Append("}" + vbCrLf)
            btnOK.Attributes.Add("onclick", js.ToString())
            btnCancel.Attributes.Add("onclick", "window.returnValue=''; window.close();")
        End If
    End Sub
End Class
