' ===============================================================================
' AdHocReportViewer
' Tests viewing an adhoc report
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' 08/16/2013 - John G/Felipe R - Several changes made to handle Telerik gridview
' ===============================================================================
' Page params include
'   rid     : the resource/report id to be run
'   print   : 1 to display a printer friendly report
'   fit     : 1 to make the report width 100% of the page width
Imports System.Data
Imports Fame.AdvantageV1.BusinessFacade.Reports
Imports Fame.AdvantageV1.Common.Reports
Imports System.IO
Imports System.Web
Imports Fame.Advantage.Api.Library.Models

Imports Telerik.Web.UI

<Serializable()>
Partial Class Reports_AdHocReportViewer
    Inherits Page

    Dim info As AdHocRptInfo 'retrieves info for this report
    Dim isPdfExport As Boolean = False
    Dim masterIndex As Integer = 0
    Dim masterDataTable As DataTable
    Private Property dsReport() As DataSet
        Get
            If (Session("dsReport") Is Nothing) Then
                Return New DataSet()
            Else
                Return DirectCast(HttpContext.Current.Session("dsReport"), DataSet)
            End If
        End Get
        Set(value As DataSet)
            Session("dsReport") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        Try
            If Not Page.IsPostBack Then
                'CreateLogoImage()

                'btnToExcel.Attributes.Add("OnClick", "getInnerHtml();")
                'btnToWord.Attributes.Add("OnClick", "getInnerHtml();")

                If Request.Params("rid") Is Nothing Then
                    RPTCommon.DisplayErrorMessage(Me, "No report specified")
                    Return
                End If

                Dim rid As String = Request.Params("rid")
                ViewState("rid") = rid ' save to the viewstate

                ' only show the SQL button if we are logged in as SA
                Me.btnShowSQL.Visible = (RPTCommon.GetUserNameFromUserId(RPTCommon.GetCurrentUserId()) = "sa")

                BindReport(rid)

                ' if the param "print" = 1 then hid the top buttons to make it a printer friendly report
                If Not Request.Params("print") Is Nothing AndAlso Request.Params("print") = 1 Then
                    Page.ClientScript.RegisterStartupScript(Page.GetType, "btPrint:OnClick", "<script language='javascript'>DoPrint();</script>")
                End If

                'If Not Request.Params("del") Is Nothing AndAlso Request.Params("del") = "1" Then
                '    AdHocRptFacade.DeleteReport(rid, RPTCommon.GetCurrentUserId())
                'End If
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub


#Region "Binding Methods"
    ''' <summary>
    ''' Bind the report to the screen
    ''' </summary>
    ''' <param name="ReportId"></param>
    ''' <remarks>
    ''' </remarks>
    Protected Sub BindReport(ByVal reportId As String)
        Try
            info = AdHocRptFacade.GetReportInfo(reportId)

            Session("adhocinfo") = info

            If info Is Nothing Then Return

            ' set the filter in the info object so it gets added when we run the report
            info.Filter = Session("rptparams")

            ' Run the report.  The return is a dataset with an array of tables.  Because of the
            ' way the sql is made, each odd indexed table contains actual data and 
            ' each even table contains unusable count information.  For more info, see
            ' the documentation within AdHocRptFacade.RunReport
            ' jguirado Parallel the information for optimization.
            Dim ds As DataSet = Nothing

            ds = AdHocRptFacade.RunReport(info, False, False)

            SaveParamsToViewState(info)

            ' do some error checking
            If ds Is Nothing Then
                Return
            Else
                dsReport = ds
            End If

            'checking for the group by here if there is group by then have that many tables in the ds
            Dim grpBy As String
            grpBy = ""
            For i As Integer = 0 To info.Fields.Length - 1
                Dim fld As AdHocFieldInfo = info.Fields(i)
                If fld.GroupBy Then
                    If (Not String.IsNullOrEmpty(fld.FKColDescrip)) Then
                        grpBy = fld.FKColDescrip
                    Else
                        grpBy = fld.FieldName
                    End If
                    Exit For
                End If
            Next
            Dim dsNew As DataSet
            dsNew = New DataSet()
            If (Not (grpBy = "")) Then
                'spliting the table to have one table per grp
                If (ds.Tables.Count = 1) Then
                    'removing the ' in grpby column
                    Dim foundRows as DataRow() =  ds.Tables(0).Select(grpBy +" Like '%''%'")
                    If(foundRows.Count() > 0) Then
                        Dim MyDataRow As DataRow
                        For Each MyDataRow in foundRows
                            MyDataRow(grpBy) = MyDataRow(grpBy).ToString().Replace("'", "")
                        Next
                        ds.Tables(0).AcceptChanges()
                    End If
                    Dim dv As DataView
                    dv = New DataView(ds.Tables(0))
                    Dim dtDistinctValues As DataTable
                    dtDistinctValues = dv.ToTable(True, grpBy)
                    'Create new DataTable for each of the distinct column
                    Dim cnt As Integer = 1
                    For Each dr As DataRow In dtDistinctValues.Rows
                        dv.RowFilter = String.Empty
                        'dim grpData As String  = dr(grpby).ToString
                        dv.RowFilter = "[" + grpBy + "] = '" + dr(grpBy).ToString + "'"
                        Dim dtNew As DataTable
                        dtNew = dv.ToTable()
                        dtNew.TableName = dtNew.TableName + cnt.ToString
                        cnt = cnt + 1
                        dsNew.Tables.Add(dtNew)
                    Next
                End If
                dsReport = dsNew
            Else
                dsNew = ds
            End If

            ' by binding to ds.Tables instead of ds, each table within ds is bound to the repeater.

            rptReport.DataSource = dsNew.Tables
            rptReport.DataBind()

            'show "No Records" message if there are not table returned
            FindControl("divNoRecords").Visible = (dsNew.Tables.Count = 0)

            'Get the school logo from the API 
            Dim campusId = AdvantageSession.UserState.CampusId.ToString
            Dim token As TokenResponse
            token = CType(Session("AdvantageApiToken"), TokenResponse)
            Dim SchoolLogo As New Fame.Advantage.Api.Library.SystemCatalog.SchoolLogo(token.ApiUrl, token.Token)
            Dim SchoolLogoImageByte() As Byte


            If (Not String.IsNullOrEmpty(campusId)) Then
                Dim result = SchoolLogo.RequestSchoolLogo(campusId)
                If result IsNot Nothing AndAlso result.Length > 0 Then
                    SchoolLogoImageByte = result
                    If SchoolLogoImageByte IsNot Nothing AndAlso SchoolLogoImageByte.Length > 0 Then

                        Dim imgLogo As Image = CType(FindControl("imgSchoolLogo"), Image)
                        imgLogo.ImageUrl = "data:image;base64," + Convert.ToBase64String(SchoolLogoImageByte)
                        imgLogo.Visible = True

                    End If
                End If

            End If


            'jguirado: Optimization, get out from the next loop the report Header..............................................................
            'Insert Report Header......... Image
            Dim strHttp As String = CommonWebUtilities.GetProtocol()
            Dim strFormatString As String
            If strHttp.ToLower = "http://" Then
                strFormatString = "http://{0}{1}{2}"
            Else
                strFormatString = "https://{0}{1}{2}"
            End If


            'Insert Report Header......... String School Name
            Dim schoolName As String = ConfigurationManager.AppSettings("SchoolName")
            If Not String.IsNullOrEmpty(schoolName) Then
                CType(Me.FindControl("lblSchoolName"), Label).Text = schoolName.ToUpper()
            End If
            ' End of Report Header..............................................................................................................

            ' Bind all Information.... 
            For i As Integer = 0 To rptReport.Items.Count - 1
                '    If i Mod 2 = 1 Then
                '        ' this is an even table with no "real" data, so just hide it
                '        rptReport.Items(i).Visible = False
                '    Else
                ' this is an odd with real data, do the bind via the helper function "_BindReport"
                Dim rptItem As RepeaterItem = rptReport.Items(i)
                Dim dt As DataTable = dsNew.Tables(i)
                _BindReport(i, rptItem, info, dt)
                'End If
            Next
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            RPTCommon.DisplayErrorMessage(Me, ex.Message)
        End Try
    End Sub
    ''' <summary>
    ''' Helper function for BindReport.
    ''' Binds a datatable to the gridview within a specific repeateritem.
    ''' </summary>
    ''' <param name="rptItem"></param>
    ''' <param name="info"></param>
    ''' <param name="dt"></param>
    ''' <remarks></remarks>
    Protected Sub _BindReport(ByVal i As Integer, ByVal rptItem As RepeaterItem, ByVal info As AdHocRptInfo, ByVal dt As DataTable)

        'DisplayHeader(rptItem, info, dt)


        ' display the report for each group as a Telerik RadGrid
        Dim gvReport As RadGrid = rptItem.FindControl("gvReport")
        gvReport.Attributes.Add("data-repeater-id", i)

        gvReport.DataSource = dt

        'initialize page object to keep track of paging for Telerik Grid
        'ReportPager(Of String, DataTable).GetInstance(String.Format("{0}_ReportPager", Page.UniqueID)).Add(gvReport.UniqueID, dt)

        'show the summary in the footer for each grouping depending on whether the 
        'showgroupbysummary property is set or not
        If info.ShowGroupBySummary = True Then
            If Not dt.TableName.Contains("Summary") Then
                gvReport.ShowFooter = True
            End If
        Else
            If Not dt.TableName.Contains("Summary") Then
                gvReport.ShowFooter = False
            End If
        End If

        'display the overall summary in the summary grid depending on whether
        'the showsummary property is set or not.
        If info.ShowSummary = True Then
            If dt.TableName.Contains("Summary") Then
                gvReport.ShowFooter = True
            End If
        Else
            If dt.TableName.Contains("Summary") Then
                gvReport.Visible = False
            End If
        End If

        gvReport.DataBind()

    End Sub

    ''' <summary>
    ''' Display the header in the Telerik GridView
    ''' </summary>
    ''' <param name="lblTit"></param>
    ''' <param name="lblRecords"></param>
    ''' <param name="dt"></param>
    ''' <remarks></remarks>
    Protected Sub DisplayTelerikHeader(ByVal lblReportName As Label, ByVal lblTit As Label, ByVal lblRecords As Label, ByVal strSummary As String, ByVal dt As DataTable)

        ' Display the report name

        info = Session("adhocinfo")

        If lblReportName IsNot Nothing And info IsNot Nothing Then
            lblReportName.Text = info.ReportName.ToUpper() & strSummary
        End If

        'Make sure we have a valid label object.
        'If so, set the grouping text for the grid
        If lblTit IsNot Nothing Then

            'make sure we have an adhoc info object
            'if so, continue to setting the group by text
            If info IsNot Nothing Then

                If Not dt.TableName.ToLower.Contains("summary") Then
                    Dim groupby As String = GetGrouping(info, dt)
                    If groupby <> "" Then
                        lblTit.Text = groupby
                    Else
                        lblTit.Text = info.ReportName
                    End If
                Else
                    lblTit.Text = info.ReportName
                End If

            End If

            'Make sure we have a valid label for record count.
            'If so, set the text to the number of records in data source
            If lblRecords IsNot Nothing Then
                lblRecords.Text = "Total Records: " & dt.Rows.Count.ToString() & ControlChars.NewLine
            End If
        End If

    End Sub


    ''' <summary>
    ''' Returns a string with how this report is grouped (AKA: sql GroupBy)
    ''' </summary>
    ''' <param name="info"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function GetGrouping(ByVal info As AdHocRptInfo, ByVal dt As DataTable) As String
        If info Is Nothing Or info.Fields Is Nothing Then
            Return ""
        End If

        'Dim fmt() As String = ViewState("fmt")
        Dim sb As New StringBuilder()
        For i As Integer = 0 To info.Fields.Length - 1
            Dim fld As AdHocFieldInfo = info.Fields(i)
            If fld.GroupBy Then
                If dt.Rows.Count > 0 Then
                    'BEN: 10/11/2006 - Fix bug when converting the field to a string

                    '' Code mofified By kamalesh Ahuja on 17th September 2010 to resolve error when grouping 
                    '' by foriegn key field
                    ''sb.Append(fld.Header + ": " + dt.Rows(0)(fld.FieldName).ToString() + ",")
                    Try
                        sb.Append(fld.Header + ": " + dt.Rows(0)(fld.FieldName).ToString() + ",")
                    Catch ex As Exception
                        Dim exTracker = New AdvApplicationInsightsInitializer()
                        exTracker.TrackExceptionWrapper(ex)

                        ''''' Code modified by kamalesh Ahuja on 30th Sept 2010 to resolve mantis issue id 19743
                        ''sb.Append(fld.Header + ": " + dt.Rows(0)(fld.FKColDescrip).ToString() + ",")
                        Try
                            sb.Append(fld.Header + ": " + dt.Rows(0)(fld.FKColDescrip).ToString() + ",")
                        Catch ex1 As Exception
                            exTracker.TrackExceptionWrapper(ex1)


                        End Try
                        ''''''''''''''''
                    End Try
                    ''''

                    'If fmt(i) <> "" Then
                    'sb.Append(fld.Header + ": " + String.Format(fmt(i), dt.Rows(0)(fld.FieldName)) + ",")
                    'Else
                    'sb.Append(fld.Header + ": " + dt.Rows(0)(fld.FieldName).ToString() + ",")
                    'End If
                Else
                    sb.Append("{None},")
                End If
            End If

        Next
        If sb.Length > 0 Then sb.Remove(sb.Length - 1, 1) ' remove the last comma
        Return sb.ToString()
    End Function

    ''' <summary>
    ''' Author: John G 
    ''' Date Created: 08/14/2013 
    ''' Description:  Added this new method to replace the three methods that would previously format
    '''               the ms grid.  This one now works against the Telerik Grid, and formats the data
    '''               depending on whether it is a header, footer, or data item  
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub FormatTelerikRow(ByVal sender As Object, ByVal e As GridItemEventArgs)


        Dim dt As DataTable = sender.DataSource 'the underlying data table
        Dim colName As String 'the column name we are working with

        'get the arrays from viewstate that hold the summary types,
        'formatting, and whether the columns should be visible or not
        Dim stypes() As Integer = ViewState("stypes")
        Dim fmt() As String = ViewState("fmt")
        Dim visibles() As Boolean = ViewState("visibles")
        Dim headers() As String = ViewState("headers")
        Dim widths() As Integer = ViewState("widths")


        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'If this is a page item, hide it if it is the summary grid
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If (e.Item.ItemType = GridItemType.Pager) Then

            If dt.TableName = "Summary1" Or dt.TableName = "Summary2" Then
                e.Item.Display = False
            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'If this is a header title, call the
            ' method to handle this
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ElseIf e.Item.ItemType = GridItemType.CommandItem Then

            Dim strSummary As String = ""
            If dt.TableName = "Summary1" Or dt.TableName = "Summary2" Then
                strSummary = " - Summary "
            End If

            Dim lblrp As Label = e.Item.FindControl("lblReportName")
            Dim lblt As Label = e.Item.FindControl("lblTitle")
            Dim lblrc As Label = e.Item.FindControl("lblRecords")
            If lblt IsNot Nothing Then DisplayTelerikHeader(lblrp, lblt, lblrc, strSummary, dt)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'If this is a footer, set the aggregate totals 
            'for the footer
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ElseIf (e.Item.ItemType = GridItemType.Footer) Then

            ' retrieve the item into a Telerik footer item
            Dim gridfooter As GridFooterItem = e.Item

            ' Compute the summary stats for each table by iterating
            ' through the stypes array to find the columns that should
            ' be aggregated
            For i As Integer = 0 To stypes.Length - 1
                If dt.Columns.Count > i Then
                    colName = dt.Columns(i).ColumnName

                    ' if this column should not be shown, make it go away
                    ' and don't calculate any aggregates.
                    If Not visibles(i) Then

                        gridfooter(colName).Text = ""
                        gridfooter(colName).Text = ""
                        gridfooter(colName).Width = 0
                        gridfooter(colName).Visible = False

                        'For j As Integer = e.Row.Cells(i).Controls.Count - 1 To 0 Step -1
                        '    e.Row.Cells(i).Controls.RemoveAt(j)
                        'Next
                    Else 'visible so calc
                        Dim sb As New StringBuilder()

                        If (stypes(i) And SummaryTypes.Count) = SummaryTypes.Count Then
                            Dim sql As String
                            Dim res As Object
                            Try
                                sql = String.Format("COUNT({0})", colName)
                                res = dt.Compute(sql, Nothing)
                            Catch ex As Exception
                                Dim exTracker = New AdvApplicationInsightsInitializer()
                                exTracker.TrackExceptionWrapper(ex)

                                sql = String.Format("COUNT([{0}])", colName)
                                res = dt.Compute(sql, Nothing)
                            End Try

                            Dim fmtres As String = ""
                            If res IsNot DBNull.Value Then fmtres = String.Format(res, fmt(i))
                            sb.Append(String.Format("<div>Count: {0}</div>", fmtres))
                        End If
                        If (stypes(i) And SummaryTypes.Max) = SummaryTypes.Max Then
                            Dim sql As String = String.Format("MAX({0})", colName)
                            Dim res As Object = dt.Compute(sql, Nothing)
                            Dim fmtres As String = ""
                            If res IsNot DBNull.Value Then fmtres = String.Format(res, fmt(i))
                            sb.Append(String.Format("<div>Max: {0}</div>", fmtres))
                        End If
                        If (stypes(i) And SummaryTypes.Min) = SummaryTypes.Min Then
                            Dim sql As String = String.Format("MIN({0})", colName)
                            Dim res As Object = dt.Compute(sql, Nothing)
                            Dim fmtres As String = ""
                            If res IsNot DBNull.Value Then fmtres = String.Format(res, fmt(i))
                            sb.Append(String.Format("<div>Min: {0}</div>", fmtres))
                        End If
                        If (stypes(i) And SummaryTypes.Avg) = SummaryTypes.Avg Then
                            Dim sql As String = String.Format("AVG({0})", colName)
                            Dim res As Object = dt.Compute(sql, Nothing)
                            Dim fmtres As String = ""
                            If res IsNot DBNull.Value Then fmtres = String.Format(res, fmt(i))
                            sb.Append(String.Format("<div>Avg: {0}</div>", fmtres))
                        End If
                        If (stypes(i) And SummaryTypes.Sum) = SummaryTypes.Sum And Not (colName.Equals("TransAmount")) And Not (colName.Equals("TransAmoun")) Then
                            Dim sql As String = String.Format("SUM({0})", colName)
                            Dim res As Object = dt.Compute(sql, Nothing)
                            Dim fmtres As String = ""
                            If res IsNot DBNull.Value Then fmtres = String.Format(res, fmt(i))
                            sb.Append(String.Format("<div>Sum: {0}</div>", fmtres))
                        End If
                        gridfooter.VerticalAlign = VerticalAlign.Top
                        gridfooter(colName).Text = sb.ToString()

                    End If
                End If

            Next

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'If this is a header, format the data for the header
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ElseIf e.Item.ItemType = GridItemType.Header Then

            'retrieve the griditem as Telerik GridHeader item
            Dim gridHeader As GridHeaderItem = e.Item

            'iterate through the headers array, and set the the
            'gridheader column text to the friendly name from the
            'headers array
            For i As Integer = 0 To headers.Length - 1
                If dt.Columns.Count > i Then
                    colName = dt.Columns(i).ColumnName

                    'if this column should not be shown,
                    'make it go away
                    If Not visibles(i) Then
                        gridHeader(colName).Text = ""
                        gridHeader(colName).Width = 0
                        gridHeader(colName).Visible = False
                    Else
                        gridHeader(colName).Text = headers(i) 'friendly name

                        'set the column width to the width array value
                        If widths(i) > 0 Then
                            gridHeader(colName).Width = widths(i)
                        End If
                    End If
                End If
            Next

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' it is a data item so format the data
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ElseIf (TypeOf (e.Item) Is GridDataItem) Then

            'retrieve the griditem as a Telerik GridItem
            Dim gridItem As GridDataItem = e.Item

            'if this is a summary table, don't show the row.  This is because
            'we bring back a table in the dataset that has all of the groupings
            'together as one table. We don't want to show these rows, only the
            'footer which has the summary aggregrate data.  We should consider
            'changing this functionality so that we do not have to bring back
            'more data than necessary.
            If dt.TableName = "Summary1" Or dt.TableName = "Summary2" Then
                e.Item.Display = False
            Else 'format the data depending on format array value

                'iterate through the formatting array, and only modify the proper cell
                'if there is a format that should be applied
                For i As Integer = 0 To fmt.Length - 1
                    If dt.Columns.Count > i Then
                        colName = dt.Columns(i).ColumnName

                        'if it should not be shown, make it go away.
                        'if not, format it.
                        If Not visibles(i) Then
                            gridItem(colName).Width = 0
                            gridItem(colName).Visible = False
                            gridItem(colName).Text = ""
                        Else

                            If (gridItem.DataItem(colName) IsNot DBNull.Value) Then
                                If (fmt(i)) <> "" Then
                                    gridItem(colName).Text = String.Format(gridItem.DataItem(colName), fmt(i))
                                Else
                                    gridItem(colName).Text = gridItem.DataItem(colName).ToString()
                                End If
                            Else
                                gridItem(colName).Text = "&nbsp;&nbsp;&nbsp;"
                            End If
                        End If
                    End If
                Next
            End If
        End If
    End Sub

#End Region

#Region "ViewState Report Params"
    ''' <summary>
    ''' Saves 
    ''' </summary>
    ''' <param name="info"></param>
    ''' <remarks></remarks>
    Protected Sub SaveParamsToViewState(ByVal info As AdHocRptInfo)
        ViewState("fmt") = GetFormatStrings(info)
        ViewState("stypes") = GetSummaryTypes(info)
        ViewState("headers") = GetHeaderText(info)
        ViewState("widths") = GetWidths(info)
        ' (BEN NEW)
        ViewState("visibles") = GetVisibles(info)
    End Sub

    ' (BEN NEW)
    Protected Function GetVisibles(ByVal info As AdHocRptInfo) As Boolean()
        If info.Fields Is Nothing Then Return Nothing
        Dim c As Integer = info.Fields.Length
        Dim res(c - 1) As Boolean
        c = 0
        For i As Integer = 0 To info.Fields.Length - 1
            res(i) = info.Fields(i).Visible
        Next
        Return res
    End Function

    Protected Function GetWidths(ByVal info As AdHocRptInfo) As Integer()
        If info.Fields Is Nothing Then Return Nothing
        Dim c As Integer = info.Fields.Length
        Dim res(c - 1) As Integer
        For i As Integer = 0 To info.Fields.Length - 1
            res(i) = info.Fields(i).Width
        Next
        Return res
    End Function

    Protected Function GetHeaderText(ByVal info As AdHocRptInfo) As String()
        If info.Fields Is Nothing Then Return Nothing
        Dim c As Integer = info.Fields.Length
        Dim res(c - 1) As String
        For i As Integer = 0 To info.Fields.Length - 1
            res(i) = info.Fields(i).Header
        Next
        Return res
    End Function

    ''' <summary>
    ''' Gets an integer array of all the summary types for all the columns
    ''' </summary>
    ''' <param name="info"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function GetSummaryTypes(ByVal info As AdHocRptInfo) As Integer()
        If info.Fields Is Nothing Then Return Nothing
        Dim c As Integer = info.Fields.Length
        Dim res(c - 1) As Integer
        For i As Integer = 0 To info.Fields.Length - 1
            res(i) = info.Fields(i).SummaryType
        Next
        Return res
    End Function

    ''' <summary>
    ''' Returns a string array of all the format strings for all visible columns
    ''' </summary>
    ''' <param name="info"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function GetFormatStrings(ByVal info As AdHocRptInfo) As String()
        If info.Fields Is Nothing Then Return Nothing
        Dim c As Integer = info.Fields.Length
        Dim res(c - 1) As String
        For i As Integer = 0 To info.Fields.Length - 1
            res(i) = info.Fields(i).FormatString
        Next
        Return res
    End Function
#End Region

#Region "Event Handlers"

    ''' <summary>
    ''' NeedDataSource gets used for paging, refresh, or any request for data rehydration.
    ''' <code>Created: 8/15/2013 Felipe Ramos</code>
    ''' </summary>
    ''' <param name="sender">RadGrid</param>
    ''' <param name="e">GridNeedDataSourceEventArgs</param>
    Protected Sub gvReport_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs)

        Dim gvReport As RadGrid = CType(sender, RadGrid)
        If Not gvReport Is Nothing Then
            Dim index As Integer = 0
            If (Not gvReport.Attributes("data-repeater-id") Is Nothing) Then
                index = CType(gvReport.Attributes("data-repeater-id"), Integer)
            End If

            If (masterIndex = 0) Then
                masterDataTable = dsReport.Tables(index)
            Else
                masterDataTable.Merge(dsReport.Tables(index))
            End If

            gvReport.DataSource = masterDataTable
            masterIndex += 1
        End If

    End Sub

    ''' <summary>
    ''' Telerik item data bound event
    ''' update the rows as needed depending on type
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gvReport_ItemDataBound(sender As Object, e As GridItemEventArgs)
        FormatTelerikRow(sender, e)
    End Sub


    Protected Sub btnExportWord_Click(sender As Object, e As EventArgs)
        isPdfExport = False
        For Each rptItem As RepeaterItem In rptReport.Items
            Dim gvReport As RadGrid = rptItem.FindControl("gvReport")
            gvReport.ExportToWord()
        Next

    End Sub

    Protected Sub btnExportExcel_Click(sender As Object, e As EventArgs)
        isPdfExport = False
        For Each rptItem As RepeaterItem In rptReport.Items
            Dim gvReport As RadGrid = rptItem.FindControl("gvReport")
            gvReport.ExportToExcel()
        Next
    End Sub
    Protected Sub btnShowSQL_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnShowSQL.Click
        If Me.txtDebug.Visible Then
            btnShowSQL.Text = "Hide SQL"
            txtDebug.Visible = False
            txtDebug.Text = ""
        Else
            txtDebug.Visible = True
            btnShowSQL.Text = "Show SQL"
            Dim rid As String = ViewState("rid")
            If rid Is Nothing Or rid = "" Then
                txtDebug.Text = ""
            Else
                Dim info As AdHocRptInfo = AdHocRptFacade.GetReportInfo(rid)
                info.Filter = Session("rptparams")
                txtDebug.Text = AdHocRptFacade.GetReportSql(info)
            End If
        End If
    End Sub

    'Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click
    '    ExportToExcel(Session("ds"), 0, Response, "Report")
    'End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        Try

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub

#End Region

    Protected Sub Page_PreRenderComplete(sender As Object, e As EventArgs) Handles Me.PreRenderComplete
        Try

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Console.WriteLine(ex.Message)
        End Try
    End Sub

    Protected Sub btnExportPdf_OnClick(sender As Object, e As EventArgs)
        info = Session("adhocinfo")
        isPdfExport = True
        For Each rptItem As RepeaterItem In rptReport.Items

            Dim gvReport As RadGrid = rptItem.FindControl("gvReport")
            If (Not info Is Nothing) Then
                'gvReport.ExportSettings.Pdf.PageTitle = info.ReportName
                gvReport.ExportSettings.Pdf.PageFooter.MiddleCell.Text = "<span>" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss") + "</span> <?page-number?>"
                gvReport.ExportSettings.Pdf.PageFooter.MiddleCell.TextAlign = GridPdfPageHeaderFooterCell.CellTextAlign.Left
            End If
            gvReport.ExportToPdf()

        Next

    End Sub

    Protected Sub gvReport_OnItemCreated(sender As Object, e As GridItemEventArgs)
        If (isPdfExport And TypeOf (e.Item) Is GridHeaderItem) Then
            Dim headerItem As GridHeaderItem = CType(e.Item, GridHeaderItem)
            headerItem.Style("font-size") = "10pt"
        ElseIf isPdfExport Then
            e.Item.Style("font-size") = "8pt"
        End If

    End Sub
End Class
